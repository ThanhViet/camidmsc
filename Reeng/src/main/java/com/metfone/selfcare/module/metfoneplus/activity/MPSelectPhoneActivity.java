package com.metfone.selfcare.module.metfoneplus.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.module.metfoneplus.adapter.PhoneLinkedAdapter;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class MPSelectPhoneActivity extends BaseSlidingFragmentActivity
        implements ClickListener.IconListener {
    private static final String TAG = "MPSelectPhoneActivity";

    @BindView(R.id.layout_root_select_phone)
    LinearLayout mLayoutRootSelectPhone;
    @BindView(R.id.rv_phone_list)
    RecyclerView rvPhoneList;
    @BindView(R.id.layout_container_select_phone)
    RelativeLayout mLayoutContainerSelectPhone;

    private CamIdUserBusiness mCamIdUserBusiness;
    private PhoneLinkedAdapter mPhoneLinkedAdapter;
    private List<Object> objectList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp_select_phone);

        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        showLoadingDialog("", R.string.loading);
        mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

        ButterKnife.bind(this);

        Utilities.adaptViewForInserts(mLayoutRootSelectPhone);
        Utilities.adaptViewForInsertBottom(mLayoutContainerSelectPhone);

        mPhoneLinkedAdapter = new PhoneLinkedAdapter(new ArrayList<>(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhoneLinkedAdapter.ContentViewHolder holder = (PhoneLinkedAdapter.ContentViewHolder) view.getTag();
                updatePhoneToPhoneService(holder.phoneLinked.getUserServiceId());
            }
        });

        rvPhoneList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvPhoneList.setAdapter(mPhoneLinkedAdapter);

        getPhoneLinkedList();
    }

    private void getPhoneLinkedList() {
        List<Service> services = mCamIdUserBusiness.getServices();
        for (Service s : services) {
            int serviceId = s.getServiceId();
            // Add title for group: Mobile vs FTTH
            if (serviceId == 1 && s.getPhoneLinked().size() > 0) {
                objectList.add("Mobile");
            }

            if (serviceId == 2 && s.getPhoneLinked().size() > 0) {
                objectList.add("FTTH");
            }

            List<PhoneLinked> phoneLinkeds = s.getPhoneLinked();
            // Giới hạn chỉ cho hiển thị 5 số mỗi loại
            // giá trị này có thể thay đổi theo cấu hình từ server
            for (int i = 0; i < Math.min(phoneLinkeds.size(), 5); i++) {
                objectList.add(phoneLinkeds.get(i));
            }
        }

        mPhoneLinkedAdapter.replaceData(objectList);
        hideLoadingDialog();
    }

    @Override
    public void goToHome() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent, false);
        clearBackStack();
        finish();
    }


    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        super.onIconClickListener(view, entry, menuId);
    }

    private void updatePhoneToPhoneService(int userServiceID) {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.updateMetfone(null, null, userServiceID, new ApiCallback<AddServiceResponse>() {
            @Override
            public void onResponse(Response<AddServiceResponse> response) {
                if (response.body() != null && response.body().getData() != null) {
                    mCamIdUserBusiness.setPhoneService(response.body().getData().getServices());
                    mCamIdUserBusiness.setUser(response.body().getData().getUser());
                    mCamIdUserBusiness.setService(response.body().getData().getServices());
                    mCamIdUserBusiness.setForceUpdateMetfone(true);
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }

            @Override
            public void onError(Throwable error) {
                android.util.Log.e(TAG, "onError: " + error);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCamIdUserBusiness.setAddOrSelectPhoneCalled(false);
    }

    @Override
    public void onBackPressed() {
        DeepLinkHelper.uriDeepLink = null;
        super.onBackPressed();
        mCamIdUserBusiness.setAddOrSelectPhoneCalled(false);
    }
}
package com.metfone.selfcare.module.newdetails.model;

/**
 * Created by HaiKE on 8/31/17.
 */

public class CategorySectionModel {
    private boolean isHeader = false;
    private String header;
    private CategoryModel categoryModel;

    public CategorySectionModel(boolean isHeader, String header) {
        this.isHeader = isHeader;
        this.header = header;
    }

    public CategorySectionModel(CategoryModel model) {
        this.categoryModel = model;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public String getHeader() {
        return header;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }
}

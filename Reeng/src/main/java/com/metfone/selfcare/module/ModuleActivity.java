package com.metfone.selfcare.module;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.fragment.home.WapHomeFragment;
import com.metfone.selfcare.fragment.musickeeng.TabStrangerFragment;
import com.metfone.selfcare.fragment.onmedia.OnMediaHotFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.fragment.home.MusicHomeFragment;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.myviettel.fragment.MyViettelFragment;
import com.metfone.selfcare.module.security.fragment.SecurityFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCHomeFragment;
import com.metfone.selfcare.module.tiin.maintiin.fragment.TabTiinFragment;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.home.fragment.TabMovieFragmentV2;
import com.metfone.selfcare.v5.home.fragment.TabNewsFragmentV2;
import com.metfone.selfcare.v5.home.fragment.TabVideoFragmentV2;

import java.io.Serializable;
import java.util.ArrayList;

public class ModuleActivity extends BaseSlidingFragmentActivity implements OnMediaInterfaceListener {
    protected final String TAG = getClass().getSimpleName();
    protected Fragment mFragment;
    protected int currentTabId;
    private ApplicationController mApp;

    public static void startActivityWap(BaseSlidingFragmentActivity activity, String id) {
        Intent intent = new Intent(activity, ModuleActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.HOME.FUNCTION.TAB_WAPVIEW);
        intent.putExtra("id", id);
        activity.startActivity(intent);
    }

    public static void startActivity(BaseSlidingFragmentActivity activity, int type) {
        Intent intent = new Intent(activity, ModuleActivity.class);
        intent.putExtra(Constants.KEY_TYPE, type);
        activity.startActivity(intent);
    }

    public static void startActivity(BaseSlidingFragmentActivity activity, int type,int pos) {
        Intent intent = new Intent(activity, ModuleActivity.class);
        intent.putExtra(Constants.KEY_TYPE, type);
        intent.putExtra(Constants.KEY_POSITION, pos);
        activity.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = (ApplicationController) getApplication();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.bg_mocha));
            }
        }
        setContentView(R.layout.activity_module);
        Intent intent = getIntent();
        int type = intent.getIntExtra(Constants.KEY_TYPE, 0);
        final int index = intent.getIntExtra(Constants.KEY_POSITION, 0);
        String idTabWap = intent.getStringExtra("id");
        goNextTab(type, idTabWap, null, index);

    }

    @Override
    public void onBackPressed() {
        if (WapHomeFragment.self() != null && showingCustomViewWap) {
            boolean backWapCustomWap = WapHomeFragment.self().hideCustomView();
            if (backWapCustomWap) return;
        }
        super.onBackPressed();
    }

    public void goNextTab(int tabId, String idTabWap, Bundle args, int indexVideo) {
        switch (tabId) {
            case Constants.HOME.FUNCTION.TAB_VIDEO:
                mFragment = TabVideoFragmentV2.newInstance(indexVideo);
                break;
            case Constants.HOME.FUNCTION.TAB_MUSIC:
                mFragment = MusicHomeFragment.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_HOT:
                mFragment = OnMediaHotFragment.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_STRANGER:
//                mFragment = TabStrangerFragment.newInstance(true);
                mFragment = TabStrangerFragment.newInstance(true,indexVideo);
                break;
            case Constants.HOME.FUNCTION.TAB_MOVIE:
                mFragment = TabMovieFragmentV2.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_NEWS:
                mFragment = TabNewsFragmentV2.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_SECURITY:
                mFragment = SecurityFragment.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_WAPVIEW:
                mFragment = WapHomeFragment.newInstance(idTabWap);
                break;
            case Constants.HOME.FUNCTION.TAB_TIIN:
                mFragment = TabTiinFragment.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_MY_VIETTEL:
                mFragment = MyViettelFragment.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_SELFCARE:
                mFragment = SCHomeFragment.newInstance();
                break;

                //TODO esport
//            case Constants.HOME.FUNCTION.TAB_ESPORT:
//                mFragment = EsportFragment.newInstance();
//                break;
            default:
                mFragment = null;
                break;

        }

        if (mFragment != null) {
            currentTabId = tabId;
            try {
                if (!mFragment.isAdded())
                    if (args != null) {
                        if (mFragment.getArguments() == null) {
                            mFragment.setArguments(args);
                        } else {
                            mFragment.getArguments().putAll(args);
                        }
                    }
            } catch (IllegalStateException e) {
                Log.e(TAG, e);
            } catch (RuntimeException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
            replaceFragment(currentTabId, mFragment);
        }
    }

    public void replaceFragment(final int tabId, final Fragment fragment) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                transactionFragment(tabId, fragment);
            }
        });
    }

    public void transactionFragment(final int tabId, final Fragment fragment) {
        try {
            getSupportFragmentManager()
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.activity_right_to_left_enter, R.anim.activity_right_to_left_exit)
                    .add(R.id.tabContent, fragment, String.valueOf(tabId))
                    .commitAllowingStateLoss();
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void navigateToWebView(FeedModelOnMedia feed) {
        NavigateActivityHelper.navigateToWebView(this, feed, false);
    }

    @Override
    public void navigateToVideoView(FeedModelOnMedia feed) {
        Video video = Video.convertFeedContentToVideo(feed.getFeedContent());
        if (video == null) return;
        mApp.getApplicationComponent().providesUtils().openVideoDetail(this, video);
    }

    @Override
    public void navigateToMovieView(FeedModelOnMedia feed) {
        if (feed != null && feed.getFeedContent() != null) {
            Movie movie = FeedContent.convert2Movie(feed.getFeedContent());
            if (movie != null)
                playMovies(movie);
            else
                Utilities.processOpenLink(mApp, this, feed.getFeedContent().getLink());
        }
    }

    @Override
    public void navigateToProcessLink(FeedModelOnMedia feed) {
        Utilities.processOpenLink(mApp, this, feed.getFeedContent().getLink());
    }

    @Override
    public void navigateToComment(FeedModelOnMedia feed) {
        boolean showMenuCopy = mApp.getFeedBusiness().checkFeedToShowMenuCopy(feed);
        NavigateActivityHelper.navigateToOnMediaDetail(this, feed.getFeedContent().getUrl(),
                Constants.ONMEDIA.COMMENT, Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_TAB_HOT, showMenuCopy);
    }

    @Override
    public void navigateToAlbum(FeedModelOnMedia feed) {
//        mApp.getFeedBusiness().setFeedPlayList(feed);
        /*NavigateActivityHelper.navigateToOnMediaDetail(this, feed.getFeedContent().getUrl(),
                Constants.ONMEDIA.ALBUM_DETAIL, -1, false);*/
        Utilities.processOpenLink(mApp, this, feed.getFeedContent().getLink());
    }

    @Override
    public void navigateToListShare(String url) {
        NavigateActivityHelper.navigateToOnMediaLikeOrShare(this, url, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        setActivityForResult(false);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case Constants.ACTION.STRANGER_MUSIC_SELECT_SONG: {
                    if (mFragment != null && mFragment instanceof TabStrangerFragment) {
                        ((TabStrangerFragment) mFragment).setCurrentTab(0);
                    }
                    Serializable serializable = data.getSerializableExtra(Constants.KEENG_MUSIC.SONG_OBJECT);
                    if (serializable instanceof MediaModel) {
                        MediaModel mediaModel = (MediaModel) serializable;
                        handlerPostStrangerMusic(mediaModel);
                    }
                }
                break;

                case Constants.ACTION.ACTION_WRITE_STATUS: {
                    boolean isEdit = data.getBooleanExtra(OnMediaActivityNew.EDIT_SUCCESS, false);
                    boolean isPost = data.getBooleanExtra(OnMediaActivityNew.POST_SUCCESS, false);
                    if (isEdit) {
                        mApp.getFeedBusiness().notifyNewFeed(true, false);
                    } else if (isPost) {
                        mApp.getFeedBusiness().notifyNewFeed(true, true);
                    } else {
                        //TODO vao day thi cu notify roi cho len dau, unknown case
                        mApp.getFeedBusiness().notifyNewFeed(true, true);
                    }
                }
                break;

                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT: {
                    int threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                }
                break;
            }
        }
    }

    private void handlerPostStrangerMusic(MediaModel songModel) {
        showLoadingDialog(null, getString(R.string.waiting));
        MusicBusiness.onCreateStrangerRoomListener listener = new MusicBusiness.onCreateStrangerRoomListener() {
            @Override
            public void onResponse(ArrayList<StrangerMusic> strangerMusics, boolean isConfide) {
                hideLoadingDialog();
                mApp.getMusicBusiness().setSelectedConfide(false);
                if (mFragment instanceof TabStrangerFragment)
                    ((TabStrangerFragment) mFragment).updateStrangerModels(strangerMusics, 0);
            }

            @Override
            public void onError(int errorCode) {
                hideLoadingDialog();
                String msgError = mApp.getMusicBusiness().getMessageErrorByErrorCode(errorCode, null, false);
                showToast(msgError, Toast.LENGTH_LONG);
            }
        };
        mApp.getMusicBusiness().createRoomStrangerMusic(songModel, listener);
    }

    private boolean showingCustomViewWap = false;

    @SuppressLint("SourceLockedOrientationActivity")
    public void setShowingCustomViewWap(boolean showingCustomViewWap) {
        this.showingCustomViewWap = showingCustomViewWap;
        if (showingCustomViewWap) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            showStatusBarWap(false);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            showStatusBarWap(true);
        }
    }

    public void notifyOnStrangeChange(ArrayList<StrangerMusic> strangerMusics, boolean isConfide) {
        if (mFragment instanceof TabStrangerFragment)
            ((TabStrangerFragment) mFragment).updateStrangerModels(strangerMusics, isConfide ? 1 : 0);
    }
}

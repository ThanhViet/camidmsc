/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/26
 */

package com.metfone.selfcare.module.security.adapter;

import android.graphics.Paint;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.module.keeng.base.BaseViewHolder;
import com.metfone.selfcare.module.security.helper.SecurityHelper;
import com.metfone.selfcare.module.security.listener.SecurityListener;
import com.metfone.selfcare.module.security.model.LogModel;
import com.metfone.selfcare.module.security.model.NewsModel;
import com.metfone.selfcare.module.security.model.PhoneNumberModel;
import com.metfone.selfcare.module.security.model.VulnerabilityModel;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Date;

public class SecurityDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = SecurityDetailAdapter.class.getSimpleName();
    private final int TYPE_EMPTY = 0;
    private final int TYPE_HEADER_SPAM_SMS = 1;
    private final int TYPE_SPAM_SMS = 2;
    private final int TYPE_SPAM_BLACK_LIST = 3;
    private final int TYPE_SPAM_WHITE_LIST = 4;
    private final int TYPE_FIREWALL_WHITE_LIST = 5;
    private final int TYPE_FIREWALL_SMS = 6;
    private final int TYPE_NEWS = 7;
    private final int TYPE_VULNERABILITY = 8;
    private final int TYPE_SMS_DETAIL = 9;
    private BaseSlidingFragmentActivity activity;
    private ArrayList<Object> data;
    private int tabId;
    private SecurityListener listener;
    private RecyclerClickListener recyclerClickListener;
    private boolean isSelectMode;

    public SecurityDetailAdapter(BaseSlidingFragmentActivity activity, int tabId, ArrayList<Object> data) {
        this.activity = activity;
        this.tabId = tabId;
        this.data = data;
    }

    public void setSelectMode(boolean selectMode) {
        isSelectMode = selectMode;
    }

    public void setRecyclerClickListener(RecyclerClickListener recyclerClickListener) {
        this.recyclerClickListener = recyclerClickListener;
    }

    public void setListener(SecurityListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        switch (tabId) {
            case Constants.TAB_SECURITY_FIREWALL_IGNORE: {
                return TYPE_FIREWALL_WHITE_LIST;
            }

            case Constants.TAB_SECURITY_FIREWALL_HISTORY: {
                return TYPE_FIREWALL_SMS;
            }

            case Constants.TAB_SECURITY_SPAM_SMS: {
                if (position == 0) return TYPE_HEADER_SPAM_SMS;
                return TYPE_SPAM_SMS;
            }

            case Constants.TAB_SECURITY_SPAM_BLOCK: {
                return TYPE_SPAM_BLACK_LIST;
            }

            case Constants.TAB_SECURITY_SPAM_IGNORE: {
                return TYPE_SPAM_WHITE_LIST;
            }

            case Constants.TAB_SECURITY_CENTER_DETAIL: {
                return TYPE_NEWS;
            }

            case Constants.TAB_SECURITY_VULNERABILITY: {
                return TYPE_VULNERABILITY;
            }

            case Constants.TAB_SECURITY_SPAM_SMS_DETAIL: {
                return TYPE_SMS_DETAIL;
            }

            case Constants.TAB_SECURITY_FIREWALL_SMS_DETAIL: {
                return TYPE_SMS_DETAIL;
            }
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_HEADER_SPAM_SMS: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_header_spam_sms, parent, false);
            }
            break;

            case TYPE_SPAM_SMS: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_sms, parent, false);
            }
            break;

            case TYPE_SPAM_BLACK_LIST: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_phonenumber, parent, false);
            }
            break;

            case TYPE_SPAM_WHITE_LIST: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_phonenumber, parent, false);
            }
            break;

            case TYPE_FIREWALL_WHITE_LIST: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_phonenumber, parent, false);
            }
            break;

            case TYPE_FIREWALL_SMS: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_sms, parent, false);
            }
            break;

            case TYPE_NEWS: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_news, parent, false);
            }
            break;

            case TYPE_VULNERABILITY: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_vulnerability, parent, false);
            }
            break;

            case TYPE_SMS_DETAIL: {
                view = LayoutInflater.from(activity).inflate(R.layout.holder_security_sms_detail, parent, false);
            }
            break;

            default: {
                Log.i(TAG, "unknown viewHolder");
                view = LayoutInflater.from(activity).inflate(R.layout.holder_empty, parent, false);
            }
            break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemHolder, int position) {
        int viewType = getItemViewType(position);
        if (itemHolder instanceof BaseViewHolder) {
            Object object = getItem(position);
            BaseViewHolder holder = (BaseViewHolder) itemHolder;
            switch (viewType) {
                case TYPE_HEADER_SPAM_SMS: {
                    TextView btnDelete = holder.getView(R.id.button_delete);
                    if (btnDelete != null) {
                        btnDelete.setPaintFlags(btnDelete.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                        btnDelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (listener != null) listener.onDeleteAllSpamSmsClick();
                            }
                        });
                    }
                }
                break;
                case TYPE_SPAM_SMS: {
                    if (object instanceof LogModel) {
                        final LogModel item = (LogModel) object;
                        holder.convertView.setSelected(item.isSelected());
                        holder.setImageResource(R.id.icon, item.isSelected() ? R.drawable.ic_security_selected : R.drawable.ic_security_sms_spam);
                        TextView tvTitle = holder.getView(R.id.tv_title);
                        if (tvTitle != null) {
                            tvTitle.setText(SecurityHelper.getName(item.getSrc(), true));
                            tvTitle.setTypeface(null, item.isReaded() ? Typeface.NORMAL : Typeface.BOLD);
                        }
                        String description = item.getContent();
                        if (TextUtils.isEmpty(description)) {
                            holder.setVisible(R.id.tv_description, false);
                        } else {
                            holder.setVisible(R.id.tv_description, true);
                            holder.setText(R.id.tv_description, description);
                        }
                        long timerOfLog = item.getTimeInt();
                        if (timerOfLog > 0) {
                            long currentTime = new Date().getTime();
                            holder.setVisible(R.id.tv_datetime, true);
                            holder.setText(R.id.tv_datetime, TimeHelper.formatCommonTime(timerOfLog, currentTime, activity.getResources()));
                        } else {
                            holder.setVisible(R.id.tv_datetime, false);
                        }
                        setViewClick(holder.getConvertView(), position, item);
                    }
                }
                break;
                case TYPE_SPAM_BLACK_LIST: {
                    if (object instanceof PhoneNumberModel) {
                        final PhoneNumberModel item = (PhoneNumberModel) object;
                        holder.convertView.setSelected(item.isSelected());
                        holder.setImageResource(R.id.icon, item.isSelected() ? R.drawable.ic_security_selected : R.drawable.ic_security_sms_spam);
                        holder.setText(R.id.tv_title, item.getName());
                        holder.setVisible(R.id.tv_description, false);
                        holder.setVisible(R.id.tv_datetime, false);
                        setViewClick(holder.getConvertView(), position, item);
                        holder.setVisible(R.id.button_delete, !isSelectMode);
                        holder.setOnClickListener(R.id.button_delete, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (listener != null) listener.onSpamBlackListRemove(item);
                            }
                        });
                    }
                }
                break;
                case TYPE_SPAM_WHITE_LIST: {
                    if (object instanceof PhoneNumberModel) {
                        final PhoneNumberModel item = (PhoneNumberModel) object;
                        holder.convertView.setSelected(item.isSelected());
                        holder.setImageResource(R.id.icon, item.isSelected() ? R.drawable.ic_security_selected : R.drawable.ic_security_number_ignore);
                        holder.setText(R.id.tv_title, item.getName());
                        holder.setVisible(R.id.tv_description, false);
                        holder.setVisible(R.id.tv_datetime, false);
                        setViewClick(holder.getConvertView(), position, item);
                        holder.setVisible(R.id.button_delete, !isSelectMode);
                        holder.setOnClickListener(R.id.button_delete, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (listener != null) listener.onSpamWhiteListRemove(item);
                            }
                        });
                    }
                }
                break;
                case TYPE_FIREWALL_WHITE_LIST: {
                    if (object instanceof PhoneNumberModel) {
                        final PhoneNumberModel item = (PhoneNumberModel) object;
                        holder.convertView.setSelected(item.isSelected());
                        holder.setImageResource(R.id.icon, item.isSelected() ? R.drawable.ic_security_selected : R.drawable.ic_security_number_ignore);
                        holder.setText(R.id.tv_title, item.getName());
                        holder.setVisible(R.id.tv_description, false);
                        holder.setVisible(R.id.tv_datetime, false);
                        setViewClick(holder.getConvertView(), position, item);
                        holder.setVisible(R.id.button_delete, !isSelectMode);
                        holder.setOnClickListener(R.id.button_delete, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (listener != null) listener.onFirewallWhiteListRemove(item);
                            }
                        });
                    }
                }
                break;
                case TYPE_FIREWALL_SMS: {
                    if (object instanceof LogModel) {
                        final LogModel item = (LogModel) object;
                        holder.convertView.setSelected(item.isSelected());
                        holder.setImageResource(R.id.icon, item.isSelected() ? R.drawable.ic_security_selected : R.drawable.ic_security_sms_firewall);
                        TextView tvTitle = holder.getView(R.id.tv_title);
                        if (tvTitle != null) {
                            tvTitle.setText(item.getDest());
                            tvTitle.setTypeface(null, item.isReaded() ? Typeface.NORMAL : Typeface.BOLD);
                        }
                        String description = item.getContent();
                        if (TextUtils.isEmpty(description)) {
                            holder.setVisible(R.id.tv_description, false);
                        } else {
                            holder.setVisible(R.id.tv_description, true);
                            holder.setText(R.id.tv_description, description);
                        }
                        long timerOfLog = item.getTimeInt();
                        if (timerOfLog > 0) {
                            long currentTime = new Date().getTime();
                            holder.setVisible(R.id.tv_datetime, true);
                            holder.setText(R.id.tv_datetime, TimeHelper.formatCommonTime(timerOfLog, currentTime, activity.getResources()));
                        } else {
                            holder.setVisible(R.id.tv_datetime, false);
                        }
                        setViewClick(holder.getConvertView(), position, item);
                    }
                }
                break;
                case TYPE_NEWS: {
                    if (object instanceof NewsModel) {
                        final NewsModel item = (NewsModel) object;
                        holder.setText(R.id.tv_title, item.getTitle());
                        String description = item.getShortContent();
                        if (TextUtils.isEmpty(description)) {
                            holder.setVisible(R.id.tv_description, false);
                        } else {
                            holder.setVisible(R.id.tv_description, true);
                            holder.setText(R.id.tv_description, description);
                        }
                        setViewClick(holder.getConvertView(), position, item);
                        ImageView ivCover = holder.getView(R.id.iv_cover);
                        if (ivCover != null) {
                            try {
                                Glide.with(activity).load(Base64.decode(item.getImage(), Base64.DEFAULT)).into(ivCover);
                            } catch (OutOfMemoryError e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                break;
                case TYPE_VULNERABILITY: {
                    if (object instanceof VulnerabilityModel) {
                        final VulnerabilityModel item = (VulnerabilityModel) object;
                        holder.setImageResource(R.id.icon, item.getResIcon());
                        holder.setText(R.id.tv_title, item.getResName());
                        holder.setText(R.id.tv_description, item.getResHint());
                        setViewClick(holder.getConvertView(), position, item);
                    }
                }
                break;
                case TYPE_SMS_DETAIL: {
                    if (object instanceof LogModel) {
                        final LogModel item = (LogModel) object;
                        holder.setText(R.id.tv_message_content, item.getContent());
                        holder.setVisible(R.id.layout_info_received, false);
                        long timerOfLog = item.getTimeInt();
                        if (timerOfLog > 0) {
                            holder.setVisible(R.id.tv_message_time, true);
                            holder.setText(R.id.tv_message_time, TimeHelper.formatTimeInDay(timerOfLog));
                        } else {
                            holder.setVisible(R.id.tv_message_time, false);
                        }
                        Object previousObj = getItem(position - 1);
                        if (previousObj instanceof LogModel) {
                            if (TimeHelper.checkSameDay(((LogModel) previousObj).getTimeInt(), item.getTimeInt())) {
                                holder.setVisible(R.id.tv_message_separator_date, false);
                            } else {
                                holder.setVisible(R.id.tv_message_separator_date, true);
                                holder.setText(R.id.tv_message_separator_date, TimeHelper.formatTimeCallDetail(activity.getResources(), item.getTimeInt()));
                            }
                        } else {
                            holder.setVisible(R.id.tv_message_separator_date, true);
                            holder.setText(R.id.tv_message_separator_date, TimeHelper.formatTimeCallDetail(activity.getResources(), item.getTimeInt()));
                        }
                        setViewClick(holder.getConvertView(), position, item);
                    }
                }
                break;

                default:
                    break;
            }
        }
    }

    public Object getItem(int position) {
        if (tabId == Constants.TAB_SECURITY_SPAM_SMS) position--;
        if (data != null && data.size() > position && position >= 0) {
            return data.get(position);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (data != null)
            size += data.size();
        if (tabId == Constants.TAB_SECURITY_SPAM_SMS && size > 0) size++;
        if (tabId == Constants.TAB_SECURITY_VULNERABILITY && size == 0) size++;
        return size;
    }

    public void setViewClick(View itemView, final int position, final Object object) {
        if (itemView != null) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerClickListener != null) {
                        recyclerClickListener.onClick(v, position, object);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (recyclerClickListener != null) {
                        recyclerClickListener.onLongClick(v, position, object);
                        return true;
                    }
                    return false;
                }
            });
        }
    }

}

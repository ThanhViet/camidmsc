package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class WsGetListRankDetailRequest extends BaseRequest<WsGetListRankDetailRequest.WsRequest> {
    @SerializedName("sessionId")
    @Expose
    private String sessionId;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("wsCode")
    @Expose
    private String wsCode;
    @SerializedName("apiKey")
    @Expose
    private String apiKey;
    @SerializedName("wsRequest")
    @Expose
    private WsRequest wsRequest;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class WsRequest {

        @SerializedName("page")
        @Expose
        private Integer page;
        @SerializedName("limit")
        @Expose
        private Integer limit;
        @SerializedName("language")
        @Expose
        private String language;
    }
}

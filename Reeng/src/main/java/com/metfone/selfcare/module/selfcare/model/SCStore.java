package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCStore implements Serializable {

//    "name": "Pathein Ayeyarwaddy Store",
//            "addr": "Strand Rd . Aung Ayar Blog , Ward (2).",
//            "openTime": "8AM-8PM",
//            "provinceName": "Ayeyarwady",
//            "districtName": "Pathein",
//            "isdn": "966",
//            "distance": 642.3207666712694,
//            "latitude": 16.78867,
//            "longitude": 94.7265

    @SerializedName("name")
    private String name;

    @SerializedName("address")
    private String addr;

    @SerializedName("openTime")
    private String openTime;

    @SerializedName("branch")
    private String provinceName;

    @SerializedName("township")
    private String districtName;

    @SerializedName("isdn")
    private String isdn;

    @SerializedName("distance")
    private Double distance;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private String longitude;

    public SCStore(String name, String addr, String latitude, String longitude) {
        this.name = name;
        this.addr = addr;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "SCStore{" +
                "name='" + name + '\'' +
                ", addr='" + addr + '\'' +
                ", openTime='" + openTime + '\'' +
                ", provinceName='" + provinceName + '\'' +
                ", districtName='" + districtName + '\'' +
                ", isdn='" + isdn + '\'' +
                ", distance='" + distance + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}

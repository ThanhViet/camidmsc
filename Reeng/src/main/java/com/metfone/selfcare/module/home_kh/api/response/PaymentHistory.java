package com.metfone.selfcare.module.home_kh.api.response;

import java.util.ArrayList;

public class PaymentHistory {
    private int totalPaymentHistorys;
    private ArrayList<PaymentHistoryItem> paymentHistoryList;

    public int getTotalPaymentHistorys() {
        return totalPaymentHistorys;
    }

    public void setTotalPaymentHistorys(int totalPaymentHistorys) {
        this.totalPaymentHistorys = totalPaymentHistorys;
    }

    public ArrayList<PaymentHistoryItem> getPaymentHistoryList() {
        if (paymentHistoryList == null) return new ArrayList<>();
        return paymentHistoryList;
    }

    public void setPaymentHistoryList(ArrayList<PaymentHistoryItem> paymentHistoryList) {
        this.paymentHistoryList = paymentHistoryList;
    }
}


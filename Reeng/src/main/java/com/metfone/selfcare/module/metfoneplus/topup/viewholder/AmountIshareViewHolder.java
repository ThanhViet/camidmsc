package com.metfone.selfcare.module.metfoneplus.topup.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.metfoneplus.topup.model.AmountIshareMetfoneModel;
import com.metfone.selfcare.module.metfoneplus.topup.model.AmountMetfoneModel;

import java.util.List;

public class AmountIshareViewHolder extends BaseViewHolder {
    public AmountIshareViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
    }


    private TextView tvAmount;
    private ConstraintLayout viewRoot;

    @Override
    public void initViewHolder(View v) {
        if (mData == null || mData.size() == 0) return;
        if (mData.get(0) instanceof AmountIshareMetfoneModel) {
            tvAmount = v.findViewById(R.id.tvAmount);
            viewRoot = v.findViewById(R.id.viewRoot);
        }
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        if (mData.get(0) instanceof AmountIshareMetfoneModel) {
            AmountIshareMetfoneModel amountMetfoneModel = (AmountIshareMetfoneModel) mData.get(pos);
            tvAmount.setText("$ " + amountMetfoneModel.getAmount());
            viewRoot.setBackgroundResource(amountMetfoneModel.isCheckSelect() ? R.drawable.bg_boder_red : R.drawable.bg_boder);
        }
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null) {
            itemViewClickListener.onItemViewClickListener(pos, mData);
        }
    }
}

package com.metfone.selfcare.module.search.event;

public class KeySearchChangeEvent {
    private String keySearch;
    private int source;
    private long currentTime;

    public String getKeySearch() {
        return keySearch;
    }

    public void setKeySearch(String keySearch) {
        currentTime = System.currentTimeMillis();
        this.keySearch = keySearch;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    @Override
    public String toString() {
        return "KeySearchChangeEvent{" +
                "keySearch='" + keySearch + '\'' +
                ", source=" + source +
                ", currentTime=" + currentTime +
                '}';
    }
}

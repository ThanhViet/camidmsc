package com.metfone.selfcare.module.tiin.activitytiin;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.exoplayer2.Player;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.common.utils.player.MochaPlayer;
import com.metfone.selfcare.common.utils.player.MochaPlayerUtil;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.NetworkChangeReceiver;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.newdetails.view.VideoViewCustom;
import com.metfone.selfcare.module.tiin.AppProvider;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.adapter.ChildDetailTiinAdapter;
import com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.fragment.MainTiinDetailFragment;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.ui.tabvideo.listener.FullPlayerListener;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.VideoFullScreenPlayerDialog;
import com.metfone.selfcare.ui.view.tab_video.VideoPlaybackControlView;
import com.metfone.selfcare.util.Log;

import java.util.Stack;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TiinDetailActivity extends BaseSlidingFragmentActivity implements MvpView, NetworkChangeReceiver.NetworkStateListener,
        FullPlayerListener.ProviderFullScreen, FullPlayerListener.OnActionFullScreenListener
        , FullPlayerListener.OnFullScreenListener {
    private static final String TAG = TiinDetailActivity.class.getSimpleName();
    public BaseFragment currentFragment = null;
    Stack<MainTiinDetailFragment> mTiinDetailFragmentStack = new Stack<>();
    SharedPref mPref;
    boolean fromTiin = false;
    private ProgressDialog mProgressDialog;
    private Unbinder mUnBinder;
    private Stack<VideoViewCustom> mVideoPlayingList = new Stack<>();
    private FeedBusiness mFeedBusiness;
    //todo media
    MochaPlayer mPlayer;
    TiinModel newsModel;
    private String playerName;
    public boolean isPlaying = false;
    NetworkChangeReceiver mNetWorkReceiver = null;

    public Video currentVideo;
    private VideoFullScreenPlayerDialog dialogFullScreenPlayer;
    private ChildDetailTiinAdapter.NewsDetailVideoHolder currentHolder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUnBinder = ButterKnife.bind(this);
        setContentView(R.layout.activity_news_detail);
        mPref = new SharedPref(this);
        mNetWorkReceiver = new NetworkChangeReceiver(this);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        fromTiin = intent.getBooleanExtra(ConstantTiin.FULL_CONTENT_TIIN, false);
        newsModel = (TiinModel) intent.getSerializableExtra(ConstantTiin.INTENT_MODULE);
        if (newsModel == null) {
            finish();
            return;
        }
        processItemClick(newsModel, true, fromTiin);
        initMochaPlayer();
    }

    private void initMochaPlayer() {
        playerName = TiinDetailActivity.TAG;
        mPlayer = MochaPlayerUtil.getInstance().providePlayerBy(playerName);
        mPlayer.addListener(eventListener);
        mPlayer.addControllerListener(callBackListener);
        mPlayer.getPlayerView().setEnabled(true);
        mPlayer.getPlayerView().setUseController(true);
        mPlayer.getPlayerView().getController().getQualityView().setVisibility(View.GONE);
        mPlayer.getPlayerView().getController().getViewMore().setVisibility(View.GONE);
        mPlayer.getPlayerView().getController().getViewFullSreen().setVisibility(View.VISIBLE);

        mPlayer.getPlayerView().enableFast(true);
        mPlayer.getPlayerView().getController().setVisibility(View.VISIBLE);
        if (newsModel != null && newsModel.getPid() != 94) {
            mPlayer.getPlayerView().getController().setCheckVideoNew(true);
        }
        mPlayer.updatePlaybackState();
    }

    public void playVideoDetailTiin(NewsDetailModel video, ChildDetailTiinAdapter.NewsDetailVideoHolder holder) {
        if (holder == null) {
            return;
        }
        currentHolder = holder;
        currentVideo = new Video();
        currentVideo.setLink(video.getMedia());
        currentVideo.setTitle("");
        currentVideo.setImagePath(video.getContent());
        currentVideo.setOriginalPath(video.getMedia());
//        currentVideo.setId(0 + "");
//        currentVideo.setTotalLike(0);
//        currentVideo.setTotalComment(0);
//        currentVideo.setIsPrivate(0);
        stopVideoDetail();
        if (mPlayer.getPlayerView() != null) {
            mPlayer.removerPlayerViewFromOldParent();
        }
        if (mPlayer != null && !mPlayer.getPlayWhenReady()) {
            addFrame(holder.layoutVideo);
            mPlayer.prepare(video.getContent());
            mPlayer.setPlayWhenReady(true);
            isPlaying = true;
        }
    }

    public void stopVideoDetail() {
        if (mPlayer != null && mPlayer.getPlayWhenReady()) {
            mPlayer.setPlayWhenReady(false);
        }
    }

    public void releaseVideo() {
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);
            mPlayer.removerPlayerViewFromOldParent();
            mPlayer.release();
        }
    }

    private void addFrame(ViewGroup frame) {
        if (mPlayer.getPlayerView() != null && frame != null) {
            mPlayer.addPlayerViewTo(frame);
        }
    }

    private VideoPlaybackControlView.CallBackListener callBackListener = new VideoPlaybackControlView.DefaultCallbackListener() {

        @Override
        public void onFullScreen() {
//            handlerFullScreen();
            if (currentVideo == null) {
                return;
            }
            if (dialogFullScreenPlayer != null) dialogFullScreenPlayer.dismiss();
            dialogFullScreenPlayer = new VideoFullScreenPlayerDialog(TiinDetailActivity.this);
            final boolean isLandscape = currentVideo != null && currentVideo.isVideoLandscape();
            //TiinDetailActivity.this.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            isFullScreen = true;
            dialogFullScreenPlayer.setOnActionFullScreenListener(TiinDetailActivity.this);
            dialogFullScreenPlayer.setOnFullScreenListener(TiinDetailActivity.this);
            dialogFullScreenPlayer.setProviderFullScreen(TiinDetailActivity.this);
            dialogFullScreenPlayer.setCurrentVideo(currentVideo);
            dialogFullScreenPlayer.setPlayerName(playerName);
            dialogFullScreenPlayer.setVideoNews(true);
            dialogFullScreenPlayer.setMovies(false);
            dialogFullScreenPlayer.setLandscape(isLandscape);
            dialogFullScreenPlayer.show();
        }

        @Override
        public void onPlayPause(boolean state) {
//            changeStatusPlayer(state);
        }

        @Override
        public void onHaveSeek(boolean flag) {
            super.onHaveSeek(flag);
            if (mPlayer != null)
                mPlayer.onHaveSeek(flag);
        }

        @Override
        public void onMoreClick() {
            super.onMoreClick();
//            handlerMore();
        }

        @Override
        public void onQuality() {
            super.onQuality();
//            handlerQuality();
        }

        @Override
        public void onReplay() {
            super.onReplay();
            mPlayer.reload();
        }
    };
    private Player.EventListener eventListener = new Player.DefaultEventListener() {
        int currentState;

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG, "onPlayerStateChanged playWhenReady: " + playWhenReady + ", playbackState: " + playbackState);
//            if(currentState != playbackState && playbackState == Player.STATE_ENDED && mPlayer != null){
//
//            }
            isPlaying = playWhenReady;
            currentState = playbackState;
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            outState.clear();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopVideoDetail();
    }

    public void showFragment(int tabId, Bundle bundle) {
        switch (tabId) {
            case ConstantTiin.TAB_DETAIL_TIIN:
                currentFragment = MainTiinDetailFragment.newInstance(bundle);
                break;

            default:
                break;
        }

        if (currentFragment != null) {
            if (!getSupportFragmentManager().getFragments().contains(currentFragment)) {
                try {
                    if (!currentFragment.isAdded()) {
                        if (currentFragment.getArguments() == null) {
                            currentFragment.setArguments(bundle);
                        } else {
                            currentFragment.getArguments().putAll(bundle);
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.disallowAddToBackStack();
                        if (!mTiinDetailFragmentStack.isEmpty()) {
                            transaction.setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right);
                        }
                        transaction.add(R.id.fragment_container, currentFragment, BaseFragment.TAG)
                                .commitAllowingStateLoss();
                        if (currentFragment != null && currentFragment instanceof MainTiinDetailFragment) {
                            addReadNewsToStack((MainTiinDetailFragment) currentFragment);
                        }
                    }
                } catch (IllegalStateException e) {
                    Log.e(TAG, "showFragment", e);
                } catch (RuntimeException e) {
                    Log.e(TAG, "showFragment", e);
                } catch (Exception e) {
                    Log.e(TAG, "showFragment", e);
                }
            }
        }
    }

    @Override
    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    @Override
    public void onBackPressed() {
        if (mTiinDetailFragmentStack.size() > 1) {
            goToPrevTab();
        } else {
            if (mPlayer != null) {
                mPlayer.getControlView().setCheckVideoNew(false);
                MochaPlayerUtil.getInstance().removerPlayerBy(playerName);
            }
            clearVideoViewsStack();
            super.onBackPressed();
        }
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public boolean isNetworkConnected() {
        return CommonUtils.isConnected(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        if (mUnBinder != null) {
            try {
                mUnBinder.unbind();
            } catch (Exception e) {
                Log.e(TAG, "onDestroy: " + e.toString());
            }
        }
        clearVideoViewsStack();
        clearBaseFragmentStack();
        currentFragment = null;
        mFeedBusiness = null;
        if (mPlayer != null && callBackListener != null)
            mPlayer.removeControllerListener(callBackListener);
        if (mPlayer != null && eventListener != null)
            mPlayer.removeListener(eventListener);
        releaseVideo();
        if (mNetWorkReceiver != null) {
            mNetWorkReceiver.unregister();
        }
        mNetWorkReceiver = null;
        super.onDestroy();
    }

    public void onFragmentDetached(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right)
                    .remove(fragment)
                    .commit();
        }
    }

    public void processItemClick(TiinModel model, boolean fromOutSide, boolean fromTiin) {
        if (!processItemClick(model, fromTiin) && fromOutSide) {
            finish();
        }
    }

    public boolean processItemClick(TiinModel model, boolean fromTiin) {
        if (model != null) {
            if (model.getPid() == 94 /*Video*/) {
                playVideo(model);
                return false;
            } else {
                readTiin(model, fromTiin);
                return true;
            }
        }
        return false;
    }

    private void playVideo(TiinModel data) {
        Video video = ConvertHelper.convertTiinToVideoMocha(data);
        ApplicationController.self().getApplicationComponent().providesUtils().openVideoDetail(this, video);
    }

    public void readTiin(TiinModel model, boolean fromTiin) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ConstantTiin.FULL_CONTENT_TIIN, fromTiin);
        bundle.putSerializable(ConstantTiin.INTENT_MODULE, model);
        showFragment(ConstantTiin.TAB_DETAIL_TIIN, bundle);
//        saveMarkRead(model.getId());
//        trackingEvent(R.string.read_tiin,model.getTitle(),model.getId()+"");
    }

    public void goToPrevTab() {
        try {
            if (mTiinDetailFragmentStack.size() == 1) {
                currentFragment = null;
                onBackPressed();
                AppProvider.CLICK_CLOSE = 1;
                return;
            }
            FragmentManager mFragmentManager = getSupportFragmentManager();
            int entryCount = mFragmentManager.getFragments().size();
            if (entryCount > 0) {
                for (int i = entryCount - 1; i >= 0; i--) {
                    Fragment fragment = mFragmentManager.getFragments().get(i);
                    if (fragment != null) {
                        if (fragment instanceof MainTiinDetailFragment) {
                            mTiinDetailFragmentStack.remove(fragment);
                            onFragmentDetached(fragment.getTag());
                            try {
                                if (i > 0) {
                                    for (int j = i - 1; j >= 0; j--) {
                                        Fragment fragmentPrev = mFragmentManager.getFragments().get(j);

                                        if (fragmentPrev instanceof BaseFragment) {
                                            if (fragmentPrev instanceof MainTiinDetailFragment) {
                                                currentFragment = (BaseFragment) fragmentPrev;
                                            }
                                        }
                                        break;
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "goToPrevTab : " + e.toString());
                            }
                            AppProvider.CLICK_CLOSE = AppProvider.CLICK_CLOSE - 1;
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "goToPrevTab : " + ex.toString());
        }
    }

    public void clearMainNewsDetailStacks() {
        mTiinDetailFragmentStack.clear();
    }

    private void clearVideoViewsStack() {
        mVideoPlayingList.clear();
    }

    private void addReadNewsToStack(MainTiinDetailFragment fragment) {
        mTiinDetailFragmentStack.push(fragment);
    }

    private void clearBaseFragmentStack() {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            while (!mTiinDetailFragmentStack.isEmpty()) {
                Fragment fragment = mTiinDetailFragmentStack.pop();
                if (fragment != null) {
                    transaction
                            .disallowAddToBackStack()
                            .setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right)
                            .remove(fragment);
                }
            }
            transaction.commit();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onStop() {
        if (mFeedBusiness != null) {
            mFeedBusiness.notifyNewFeed(true, false);
        }
        super.onStop();
    }

    @Override
    public void onNetworkStateChange(boolean hasConnect) {
        if (hasConnect) {
            if (!mTiinDetailFragmentStack.isEmpty()) {
                MainTiinDetailFragment fragment = mTiinDetailFragmentStack.peek();
                if (fragment != null) {
                    fragment.loadDataAfterReconnect();
                }
            }

        } else {
            //ToastUtils.makeText(this, getString(R.string.connection_error));
            CommonUtils.showNetworkDisconnect(this);
        }
    }

    @Override
    public void handlerLikeVideo(Video currentVideo) {

    }

    @Override
    public void handlerShareVideo(Video currentVideo) {

    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onDismiss() {
        if (currentHolder == null) {
            return;
        }
        if (!isFinishing())
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        if (currentVideo != null && currentVideo.isVideoLandscape()) {
//            TiinDetailActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }
        addFrame(currentHolder.layoutVideo);
    }

    @Override
    public void onPlayNextVideoForward(Video nextVideo) {

    }

    @Override
    public void onPlayEpisode(Video video, int position) {

    }

    @Override
    public void onChangeSubtitleAudio(Video video) {

    }

    @Override
    public Video provideVideoForward() {
        return null;
    }

    @Override
    public Video provideCurrentVideo() {
        return currentVideo;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            int threadId;
            switch (requestCode) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }
}

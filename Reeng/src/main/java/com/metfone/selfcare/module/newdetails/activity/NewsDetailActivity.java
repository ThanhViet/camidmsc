package com.metfone.selfcare.module.newdetails.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.exoplayer2.Player;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.player.MochaPlayer;
import com.metfone.selfcare.common.utils.player.MochaPlayerUtil;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.newdetails.ChildDetailNews.adapter.NewsDetailAdapter;
import com.metfone.selfcare.module.newdetails.MainDetailNews.fragment.MainNewsDetailFragment;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.NetworkChangeReceiver;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.ui.tabvideo.listener.FullPlayerListener;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.VideoFullScreenPlayerDialog;
import com.metfone.selfcare.ui.view.tab_video.VideoPlaybackControlView;
import com.metfone.selfcare.util.Log;

import java.util.Stack;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NewsDetailActivity extends BaseSlidingFragmentActivity implements MvpView, NetworkChangeReceiver.NetworkStateListener, FullPlayerListener.ProviderFullScreen, FullPlayerListener.OnActionFullScreenListener
        , FullPlayerListener.OnFullScreenListener {
    private static final String TAG = NewsDetailActivity.class.getSimpleName();
    public BaseFragment currentFragment = null;
    Stack<MainNewsDetailFragment> mNewsDetailFragmentStack = new Stack<>();
    SharedPref mPref;
    boolean fullData = false;
    NetworkChangeReceiver mNetWorkReceiver = null;
    private ProgressDialog mProgressDialog;
    private Unbinder mUnBinder;
    private FeedBusiness mFeedBusiness;

    //todo media
    MochaPlayer mPlayer;
    private String playerName;
    public boolean isPlaying = false;
    private NewsModel newsModel;

    private Video currentVideo;
    private VideoFullScreenPlayerDialog dialogFullScreenPlayer;
    private NewsDetailAdapter.NewsHotDetailVideoHolder currentHolder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean checkNightMode = SharedPrefs.getInstance().get(SharedPrefs.NIGHT_MODE, Boolean.class);
        changeMode(checkNightMode);
        mUnBinder = ButterKnife.bind(this);
        setContentView(R.layout.activity_news_detail);
        mPref = new SharedPref(this);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        mNetWorkReceiver = new NetworkChangeReceiver(this);
        mFeedBusiness = ((ApplicationController) getApplication()).getFeedBusiness();
        fullData = intent.getBooleanExtra(CommonUtils.FULL_DATA_DETAIL, false);
        newsModel = (NewsModel) intent.getSerializableExtra(CommonUtils.KEY_NEWS_ITEM_SELECT);
        if (newsModel == null) {
            finish();
            return;
        }
        processItemClick(newsModel, true, fullData);
        initMochaPlayer();
    }

    private void initMochaPlayer() {
        playerName = NewsDetailActivity.TAG;
        mPlayer = MochaPlayerUtil.getInstance().providePlayerBy(playerName);
        mPlayer.addListener(eventListener);
        mPlayer.addControllerListener(callBackListener);
        mPlayer.getPlayerView().setEnabled(true);
        mPlayer.getPlayerView().setUseController(true);

        mPlayer.getPlayerView().getController().getQualityView().setVisibility(View.GONE);
        mPlayer.getPlayerView().getController().getViewMore().setVisibility(View.GONE);
        mPlayer.getPlayerView().getController().getViewFullSreen().setVisibility(View.VISIBLE);
        mPlayer.getPlayerView().enableFast(true);
        mPlayer.getPlayerView().getController().setVisibility(View.VISIBLE);
        if (newsModel != null && newsModel.getPid() != 135) {
            mPlayer.getControlView().setCheckVideoNew(true);
        }
        mPlayer.updatePlaybackState();
    }

    public void playVideoDetailNew(NewsDetailModel video, NewsDetailAdapter.NewsHotDetailVideoHolder holder) {
        if (holder == null) {
            return;
        }
        currentHolder = holder;
        currentVideo = new Video();
        currentVideo.setLink(video.getMedia());
        currentVideo.setTitle("");
        currentVideo.setImagePath(video.getContent());
        currentVideo.setOriginalPath(video.getMedia());
//        currentVideo.setId(0 + "");
//        currentVideo.setTotalLike(0);
//        currentVideo.setTotalComment(0);
//        currentVideo.setIsPrivate(0);
        stopVideoDetail();
        if (mPlayer.getPlayerView() != null) {
            mPlayer.removerPlayerViewFromOldParent();
        }
        if (mPlayer != null && !mPlayer.getPlayWhenReady()) {
            addFrame(holder.layoutVideo);
            mPlayer.prepare(video.getMedia());
            mPlayer.setPlayWhenReady(true);
            isPlaying = true;
        }
    }

    public void stopVideoDetail() {
        if (mPlayer != null && mPlayer.getPlayWhenReady()) {
            mPlayer.setPlayWhenReady(false);
        }
    }

    public void releaseVideo() {
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);
            mPlayer.removerPlayerViewFromOldParent();
            mPlayer.release();
        }
    }

    private void addFrame(ViewGroup frame) {
        if (mPlayer.getPlayerView() != null && frame != null) {
            mPlayer.addPlayerViewTo(frame);
        }
    }

    private VideoPlaybackControlView.CallBackListener callBackListener = new VideoPlaybackControlView.DefaultCallbackListener() {

        @Override
        public void onFullScreen() {
            if (currentVideo == null) {
                return;
            }
            if (dialogFullScreenPlayer != null) dialogFullScreenPlayer.dismiss();
            dialogFullScreenPlayer = new VideoFullScreenPlayerDialog(NewsDetailActivity.this);
            final boolean isLandscape = currentVideo != null && currentVideo.isVideoLandscape();
            //NewsDetailActivity.this.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            isFullScreen = true;
            dialogFullScreenPlayer.setOnActionFullScreenListener(NewsDetailActivity.this);
            dialogFullScreenPlayer.setOnFullScreenListener(NewsDetailActivity.this);
            dialogFullScreenPlayer.setProviderFullScreen(NewsDetailActivity.this);
            dialogFullScreenPlayer.setCurrentVideo(currentVideo);
            dialogFullScreenPlayer.setPlayerName(playerName);
            dialogFullScreenPlayer.setVideoNews(true);
            dialogFullScreenPlayer.setMovies(false);
            dialogFullScreenPlayer.setLandscape(isLandscape);
            dialogFullScreenPlayer.show();
        }

        @Override
        public void onPlayPause(boolean state) {
//            changeStatusPlayer(state);
        }

        @Override
        public void onHaveSeek(boolean flag) {
            super.onHaveSeek(flag);
            if (mPlayer != null)
                mPlayer.onHaveSeek(flag);
        }

        @Override
        public void onMoreClick() {
            super.onMoreClick();
//            handlerMore();
        }

        @Override
        public void onQuality() {
            super.onQuality();
//            handlerQuality();
        }

        @Override
        public void onReplay() {
            super.onReplay();
            mPlayer.reload();
        }
    };
    private Player.EventListener eventListener = new Player.DefaultEventListener() {
        int currentState;

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG, "onPlayerStateChanged playWhenReady: " + playWhenReady + ", playbackState: " + playbackState);
//            if (currentState != playbackState && playbackState == Player.STATE_ENDED) {
//
//            }
            isPlaying = playWhenReady;
            currentState = playbackState;
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        stopVideoDetail();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            outState.clear();
        }
    }

    @Override
    public void onNetworkStateChange(boolean hasConnect) {
        Log.d(TAG, "Network connection: " + hasConnect);
        if (hasConnect) {
            if (!mNewsDetailFragmentStack.isEmpty()) {
                MainNewsDetailFragment fragment = mNewsDetailFragmentStack.peek();
                if (fragment != null) {
                    fragment.loadDataAfterReconnect();
                }
            }

        } else {
            //ToastUtils.makeText(this, getString(R.string.connection_error));
            CommonUtils.showNetworkDisconnect(this);
        }
    }

    public void showFragment(int tabId, Bundle bundle) {
        if (tabId == CommonUtils.TAB_NEWS_DETAIL) {
            currentFragment = MainNewsDetailFragment.newInstance();
        }

        if (currentFragment != null) {
            if (!getSupportFragmentManager().getFragments().contains(currentFragment)) {
                try {
                    if (!currentFragment.isAdded()) {
                        if (currentFragment.getArguments() == null) {
                            currentFragment.setArguments(bundle);
                        } else {
                            currentFragment.getArguments().putAll(bundle);
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.disallowAddToBackStack();
                        if (!mNewsDetailFragmentStack.isEmpty()) {
                            transaction.setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right);
                        }
                        transaction.add(R.id.fragment_container, currentFragment, currentFragment.TAG).commitAllowingStateLoss();
                        if (currentFragment != null && currentFragment instanceof MainNewsDetailFragment) {
                            addReadNewsToStack((MainNewsDetailFragment) currentFragment);
                        }
                    }
                } catch (IllegalStateException e) {
                    Log.e(TAG, "showFragment: " + e.toString());
                } catch (RuntimeException e) {
                    Log.e(TAG, "showFragment: " + e.toString());
                } catch (Exception ex) {
                    Log.e(TAG, "showFragment: " + ex.toString());
                }
            }
        }
    }

    @Override
    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    @Override
    public void onBackPressed() {
        if (mNewsDetailFragmentStack.size() > 1) {
            goToPrevTab();
        } else {
            if (mPlayer != null) {
                mPlayer.getControlView().setCheckVideoNew(false);
                MochaPlayerUtil.getInstance().removerPlayerBy(playerName);
            }
            super.onBackPressed();
        }
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void openActivityOnTokenExpire() {
    }

    @Override
    public void onError(@StringRes int resId) {
        onError(getString(resId));
    }

    @Override
    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar("Some Error Occurred!");
        }
    }

    @Override
    public boolean isNetworkConnected() {
        return CommonUtils.isConnected(getApplicationContext());
    }

    private void showSnackBar(String message) {
        Log.i(TAG, message + "");
    }

    @Override
    protected void onDestroy() {
        if (mUnBinder != null) {
            try {
                mUnBinder.unbind();
            } catch (Exception e) {
                Log.e(TAG, "onDestroy: " + e.toString());
            }
        }
        clearBaseFragmentStack();
        currentFragment = null;
        mFeedBusiness = null;
        if (mNetWorkReceiver != null) {
            mNetWorkReceiver.unregister();
        }
        mNetWorkReceiver = null;
        if (mPlayer != null && callBackListener != null)
            mPlayer.removeControllerListener(callBackListener);
        if (mPlayer != null && eventListener != null)
            mPlayer.removeListener(eventListener);
        releaseVideo();
        super.onDestroy();
    }

    public void onFragmentDetached(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right)
                    .remove(fragment)
                    .commit();
        }

    }

    public void processItemClick(NewsModel model, boolean fromOutSide, boolean fullData) {
        if (!processItemClick(model, fullData) && fromOutSide) {
            finish();
        }
    }

    public boolean processItemClick(NewsModel model, boolean fullData) {
        if (model != null) {
            if (model.getCid() == CommonUtils.CATE_RADIO_STORY) { // truyện radio
                readNewsNative(model.getUrl());
                return false;
            } else {
                if (model.getPid() == 151  /*Radio*/
                        || model.getPid() == 0 /*Tin webview*/) {
                    readNewsNative(model.getUrl());
                    return false;
                } else if (model.getPid() == 135 /*Video*/) {
                    playVideo(model);
                    return false;
                } else {
                    readNews(model, fullData);
                    return true;
                }
            }
        }
        return false;
    }

    private void playVideo(NewsModel data) {
        Video videoModel = ConvertHelper.convertNetnewsToVideoMocha(data);
        ApplicationController.self().getApplicationComponent().providesUtils().openVideoDetail(this, videoModel);
    }

    public void readNewsNative(String url) {
        try {
            if (TextUtils.isEmpty(url)) return;
            if (!url.contains(CommonUtils.DOMAIN)) {
                url = CommonUtils.DOMAIN + url;
            }
            UrlConfigHelper.gotoWebViewOnMedia(ApplicationController.self(), this, url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readNews(NewsModel model, boolean fullData) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommonUtils.KEY_NEWS_ITEM_SELECT, model);
        bundle.putBoolean(CommonUtils.FULL_DATA_DETAIL, fullData);
        showFragment(CommonUtils.TAB_NEWS_DETAIL, bundle);
        saveMarkRead(model.getID());
//        if (fullData) {
//            fullData = false;
//        }
//        trackingEvent("Đọc bài NetNews", model.getTitle(), "" + model.getID());
    }

    public void saveMarkRead(int id) {
        if (mPref == null) return;
        String str = mPref.getString(SharedPref.KEY_MARK_READ, "");
        if (!str.contains(id + "")) {
            str += id + ",";
            mPref.putString(SharedPref.KEY_MARK_READ, str);
        }
    }

    public void stopVideo() {
    }

    public void goToPrevTab() {
        try {
            if (mNewsDetailFragmentStack.size() == 1) {
                currentFragment = null;
                AppStateProvider.CLICK_CLOSE = 1;
                onBackPressed();
                return;
            }
            FragmentManager mFragmentManager = getSupportFragmentManager();
            int entryCount = mFragmentManager.getFragments().size();
            if (entryCount > 0) {
                for (int i = entryCount - 1; i >= 0; i--) {
                    Fragment fragment = mFragmentManager.getFragments().get(i);
                    if (fragment != null) {
                        if (fragment instanceof MainNewsDetailFragment) {
                            mNewsDetailFragmentStack.remove(fragment);
                            onFragmentDetached(fragment.getTag());
                            try {
                                if (i > 0) {
                                    for (int j = i - 1; j >= 0; j--) {
                                        Fragment fragmentPrev = mFragmentManager.getFragments().get(j);

                                        if (fragmentPrev instanceof BaseFragment) {
                                            if (fragmentPrev instanceof MainNewsDetailFragment) {
                                                currentFragment = (BaseFragment) fragmentPrev;
                                            }
                                        }
                                        break;
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "goToPrevTab : " + e.toString());
                            }
                            AppStateProvider.CLICK_CLOSE = AppStateProvider.CLICK_CLOSE - 1;
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "goToPrevTab : " + ex.toString());
        }
    }

    public void clearMainNewsDetailStacks() {
        mNewsDetailFragmentStack.clear();
    }

    public void stopCurrentVideo() {
//        VideoViewCustom videoViewCustom = popVideoViewStack();
//        if (videoViewCustom != null) {
//            videoViewCustom.stopPlayback();
//        }
    }

    private void addReadNewsToStack(MainNewsDetailFragment fragment) {
        mNewsDetailFragmentStack.push(fragment);
    }

    private void clearBaseFragmentStack() {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            while (!mNewsDetailFragmentStack.isEmpty()) {
                Fragment fragment = mNewsDetailFragmentStack.pop();
                if (fragment != null) {
                    transaction
                            .disallowAddToBackStack()
                            .setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right)
                            .remove(fragment);
                }
            }
            transaction.commit();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onStop() {
        if (mFeedBusiness != null) {
            mFeedBusiness.notifyNewFeed(true, false);
        }
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            int threadId;
            switch (requestCode) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void handlerLikeVideo(Video currentVideo) {

    }

    @Override
    public void handlerShareVideo(Video currentVideo) {

    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onDismiss() {
        if (currentHolder == null) {
            return;
        }
        if (!isFinishing())
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        if (currentVideo != null && currentVideo.isVideoLandscape()) {
//            NewsDetailActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }
        addFrame(currentHolder.layoutVideo);
    }

    @Override
    public void onPlayNextVideoForward(Video nextVideo) {

    }

    @Override
    public void onPlayEpisode(Video video, int position) {

    }

    @Override
    public void onChangeSubtitleAudio(Video video) {

    }

    @Override
    public Video provideVideoForward() {
        return null;
    }

    @Override
    public Video provideCurrentVideo() {
        return currentVideo;
    }
}

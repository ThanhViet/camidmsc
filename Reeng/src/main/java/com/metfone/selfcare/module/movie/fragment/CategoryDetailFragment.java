/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackProgressV2;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.movie.adapter.CategoryDetailAdapter;
import com.metfone.selfcare.module.movie.event.CategoryDetailEvent;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnTabListener;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.home.fragment.BaseHomeFragment;
import com.metfone.selfcare.v5.widget.LoadingViewV5;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;

public class CategoryDetailFragment extends BaseHomeFragment implements OnInternetChangedListener
        , SwipeRefreshLayout.OnRefreshListener, TabMovieListener.OnAdapterClick, BaseAdapter.OnLoadMoreListener
        , OnClickMoreItemListener, OnTabListener {

    private static final int LIMIT = 30;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingViewV5 loadingView;
    private int position = -1;
    private boolean isLoading;
    private boolean canLoadMore;
    private ArrayList<Object> data;
    private CategoryDetailAdapter adapter;
    private MovieApi movieApi;
    private SubtabInfo tabInfo = null;
    private int offset = 0;
    private ListenerUtils mListenerUtils;
    private GridLayoutManager layoutManager;

    public MovieApi getMovieApi() {
        if (movieApi == null) movieApi = new MovieApi();
        return movieApi;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_category_detail;
    }

    @Override
    public void onCreateView() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            position = bundle.getInt(Constants.TabVideo.POSITION);
            position = bundle.getInt(Constants.KEY_POSITION);
            Serializable serializable = bundle.getSerializable(Constants.KEY_DATA);
            if (serializable instanceof SubtabInfo)
                tabInfo = (SubtabInfo) serializable;
        }
        loadingView.setOnClickRetryListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData(true);
            }
        });
        if (loadingView != null && tabInfo != null) {
            loadingView.setCategoryId(tabInfo.getCategoryId());
        }
        swipeRefreshLayout.setColorSchemeColors(getColor());
        swipeRefreshLayout.setOnRefreshListener(this);
        if (data == null) data = new ArrayList<>();
        else data.clear();
        adapter = new CategoryDetailAdapter(activity, MoviePagerModel.TYPE_CATEGORY);
        adapter.setTabInfo(tabInfo);
        adapter.setItems(data);
        adapter.setListener(this);
        layoutManager = new CustomGridLayoutManager(activity, 2);
        BaseAdapter.setupGridRecycler(activity, recyclerView, layoutManager, adapter, 2, R.dimen.v5_spacing_normal, true);
        BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_movie);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        if (canLazyLoad()) {
            new Handler().postDelayed(() -> loadData(Utilities.isEmpty(data)), Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData(Utilities.isEmpty(data));
        }
    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        if (isLoading) {
            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        } else {
            loadData(Utilities.isEmpty(data));
        }
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad()) {
            loadData(Utilities.isEmpty(data));
        }
    }

    public void hideRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    private void loadData(boolean showLoading) {
        if (isLoading) return;
        isLoading = true;
        canLoadMore = false;
        if (showLoading) {
            if (loadingView != null) loadingView.showLoading();
        }
        if (tabInfo != null) {
            getMovieApi().getListMovieBySubtabInfo(tabInfo, offset, LIMIT, new ApiCallbackV2<ArrayList<Object>>() {
                @Override
                public void onSuccess(String msg, ArrayList<Object> result) throws JSONException {
                    if (recyclerView != null) recyclerView.stopScroll();
                    if (data == null) data = new ArrayList<>();
                    if (offset == 0) data.clear();
                    if (Utilities.notEmpty(result)) {
                        canLoadMore = result.size() >= LIMIT;
                        data.addAll(result);
                    }
//                    if (showLoading && tabInfo != null && MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId())) {
//                        ShowButtonDeleteEvent event = new ShowButtonDeleteEvent();
//                        event.setShow(!data.isEmpty());
//                        EventBus.getDefault().post(event);
//                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                    if (Utilities.isEmpty(data)) {
                        if (loadingView != null) loadingView.showLoadedEmpty();
                    } else {
                        if (loadingView != null) loadingView.showLoadedSuccess();
                    }
                }

                @Override
                public void onError(String s) {
                    if (data == null) data = new ArrayList<>();
                    if (adapter != null) adapter.notifyDataSetChanged();
                    if (Utilities.isEmpty(data)) {
                        if (loadingView != null) loadingView.showLoadedError();
                    } else {
                        if (loadingView != null) loadingView.showLoadedSuccess();
                    }
                }

                @Override
                public void onComplete() {
                    isLoading = false;
                    isDataInitiated = true;
                    hideRefresh();
                }
            });
        } else {
            isLoading = false;
            if (loadingView != null) loadingView.showLoadedEmpty();
            hideRefresh();
        }
    }

    @Override
    public void onClickTitleBox(Object item, int position) {
        if (item instanceof SubtabInfo) {
            NavigateActivityHelper.navigateToTabMoviesCategoryDetail(activity, (SubtabInfo) item);
        }
    }

    @Override
    public void onClickRefresh() {

    }

    @Override
    public void playFilm(Object item, int position, boolean isPlayNow) {

    }

    @Override
    public void onScrollData() {

    }

    @Override
    public void onClickMovieItem(Object item, int position) {
        if (item instanceof Movie) {
            activity.playMovies((Movie) item);
        } else if (item instanceof MoviePagerModel) {
            MoviePagerModel provisional = (MoviePagerModel) item;
            if (provisional.getObject() instanceof Movie) {
                activity.playMovies((Movie) provisional.getObject());
            }
        } else if (item instanceof MovieWatched) {
            activity.playMovies(MovieWatched.convertToMovie((MovieWatched) item));
        }
    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {
        if (tabInfo != null) {
            if (tabInfo.getType() == MovieKind.TYPE_LOCAL) {
                if (MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId())) {
                    DialogUtils.showOptionMovieWatched(activity, item, this);
                    return;
                } else if (MovieKind.CATEGORYID_GET_LIKED.equals(tabInfo.getCategoryId())
                        || MovieKind.CATEGORYID_GET_LIKED_SERIES.equals(tabInfo.getCategoryId())
                        || MovieKind.CATEGORYID_GET_LIKED_ODD.equals(tabInfo.getCategoryId())) {
                    DialogUtils.showOptionMovieLiked(activity, item, this);
                    return;
                } else if (MovieKind.CATEGORYID_GET_LATER.equals(tabInfo.getCategoryId())) {
                    DialogUtils.showOptionMovieLater(activity, item, this);
                    return;
                }
            }
            if (item instanceof Movie)
                DialogUtils.showOptionMovieItem(activity, (Movie) item, this);
        }
    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {
//        if (data != null) data.remove(item);
//        if (adapter != null) adapter.notifyDataSetChanged();
//        if (Utilities.isEmpty(data)) {
//            if (loadingView != null) loadingView.showLoadedEmpty();
//        } else {
//            if (loadingView != null) loadingView.showLoadedSuccess();
//        }
        if (item instanceof Movie) {
            deleteMovie(false, (Movie) item);
            FeedModelOnMedia feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) item);
            WSOnMedia rest = new WSOnMedia(app);
            rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.UNLIKE, "", feed.getBase64RowId(), "", FeedModelOnMedia.ActionFrom.mochavideo,
                    response -> {
                        Log.i(TAG, "onResponse: " + response);
                    }, volleyError -> {
                        Log.e(TAG, "onErrorResponse: ");
                    });
        }
    }

    @Override
    public void onClickSliderBannerItem(Object item, int position) {

    }

    @Override
    public void onLoadMore() {
        if (canLoadMore && !isLoading) {
            offset = offset + LIMIT;
            Log.i(TAG, "onLoadMore loadData ...");
            loadData(false);
        }
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE: {
                    FeedModelOnMedia feed = null;
                    if (object instanceof Movie) {
                        feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) object);
                    } else if (object instanceof MovieWatched) {
                        Movie movie = MovieWatched.convertToMovie((MovieWatched) object);
                        feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia(movie);
                    }
                    if (feed != null) {
                        new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) throws JSONException {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                }
                break;
                case Constants.MENU.MENU_REMOVE_FAVORITE:
                    if (tabInfo != null && (MovieKind.CATEGORYID_GET_LIKED.equals(tabInfo.getCategoryId())
                            || MovieKind.CATEGORYID_GET_LIKED_SERIES.equals(tabInfo.getCategoryId())
                            || MovieKind.CATEGORYID_GET_LIKED_ODD.equals(tabInfo.getCategoryId()))) {
//                        if (data != null) data.remove(object);
//                        if (adapter != null) adapter.notifyDataSetChanged();
//                        if (Utilities.isEmpty(data)) {
//                            if (loadingView != null) loadingView.showLoadedEmpty();
//                        } else {
//                            if (loadingView != null) loadingView.showLoadedSuccess();
//                        }
                    }
                    if (object instanceof Movie) {
                        deleteMovie(false, (Movie) object);
                        FeedModelOnMedia feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) object);
                        new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.UNLIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, null);
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Movie) {
                        getMovieApi().insertWatchLater((Movie) object, new ApiCallbackV2<String>() {

                            @Override
                            public void onError(String s) {

                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onSuccess(String msg, String result) throws JSONException {
                                if (activity != null)
                                    activity.showToast(R.string.add_later_success);
                            }
                        });
                    }
                    break;
                case Constants.MENU.MENU_REMOVE_LATER:
//                    if (tabInfo != null && MovieKind.CATEGORYID_GET_LATER.equals(tabInfo.getCategoryId())) {
//                        if (data != null) data.remove(object);
//                        if (adapter != null) adapter.notifyDataSetChanged();
//                        if (Utilities.isEmpty(data)) {
//                            if (loadingView != null) loadingView.showLoadedEmpty();
//                        } else {
//                            if (loadingView != null) loadingView.showLoadedSuccess();
//                        }
//                    }
                    if (object instanceof Movie) {
                        deleteMovie(false, (Movie) object);
                    }
                    break;
                case Constants.MENU.MENU_REMOVE_WATCHED:
                    if (tabInfo != null && MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId())) {
                        if (data != null) data.remove(object);
                        if (adapter != null) adapter.notifyDataSetChanged();
                        if (Utilities.isEmpty(data)) {
                            if (loadingView != null) loadingView.showLoadedEmpty();
                        } else {
                            if (loadingView != null) loadingView.showLoadedSuccess();
                        }
                    }
                    if (object instanceof Movie) {
                        getMovieApi().deleteLogWatched(false, (Movie) object, null);
                    } else if (object instanceof MovieWatched) {
                        getMovieApi().deleteLogWatched(false, MovieWatched.convertToMovie((MovieWatched) object), null);
                    }
                    break;
            }
        }
    }

    private void scrollToTop() {
        if (layoutManager != null && recyclerView != null) {
            try {
                recyclerView.stopScroll();
                layoutManager.smoothScrollToPosition(recyclerView, null, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTabReselected(int currentPosition) {
        Log.i(TAG, "onTabReselected currentPosition: " + currentPosition + ", position: " + position + ", isVisibleToUser: " + isVisibleToUser);
        if (currentPosition == position) {
            scrollToTop();
        }
    }

    @Override
    public void onTabSelected(int currentPosition) {
        Log.i(TAG, "onTabSelected currentPosition: " + currentPosition + ", position: " + position + ", isVisibleToUser: " + isVisibleToUser);
        if (currentPosition == position && isVisibleToUser) {
            new Handler().postDelayed(() -> onRefresh(), 600L);
        }
    }

    @Override
    public void onDisableLoading() {

    }

    private void deleteMovie(final boolean isDeleteAll, final Movie movie) {
        if (tabInfo == null || Utilities.isEmpty(data)) return;
        String msg = "";
        if (tabInfo.getType() == MovieKind.TYPE_LOCAL && MovieKind.CATEGORYID_GET_LIKED.equals(tabInfo.getCategoryId())) {
            if (isDeleteAll)
                msg = String.format(getString(R.string.delete_all_movie_from_category), getString(R.string.film_liked));
            else if (movie != null)
                msg = String.format(getString(R.string.delete_movie_from_category), movie.getName(), getString(R.string.film_liked));
        } else if (tabInfo.getType() == MovieKind.TYPE_LOCAL && MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId())) {
            if (isDeleteAll)
                msg = String.format(getString(R.string.delete_all_movie_from_category), getString(R.string.film_watched));
            else if (movie != null)
                msg = String.format(getString(R.string.delete_movie_from_category), movie.getName(), getString(R.string.film_watched));
        } else if (tabInfo.getType() == MovieKind.TYPE_LOCAL && MovieKind.CATEGORYID_GET_LIKED_ODD.equals(tabInfo.getCategoryId())) {
            if (isDeleteAll)
                msg = String.format(getString(R.string.delete_all_movie_from_category), getString(R.string.film_liked));
            else if (movie != null)
                msg = String.format(getString(R.string.delete_movie_from_category), movie.getName(), getString(R.string.film_liked));
        } else if (tabInfo.getType() == MovieKind.TYPE_LOCAL && MovieKind.CATEGORYID_GET_LIKED_SERIES.equals(tabInfo.getCategoryId())) {
            if (isDeleteAll)
                msg = String.format(getString(R.string.delete_all_movie_from_category), getString(R.string.film_liked));
            else if (movie != null)
                msg = String.format(getString(R.string.delete_movie_from_category), movie.getName(), getString(R.string.film_liked));
        } else if (tabInfo.getType() == MovieKind.TYPE_LOCAL && MovieKind.CATEGORYID_GET_LATER.equals(tabInfo.getCategoryId())) {
            if (isDeleteAll)
                msg = String.format(getString(R.string.delete_all_movie_from_category), getString(R.string.movie_watch_later));
            else if (movie != null)
                msg = String.format(getString(R.string.delete_movie_from_category), movie.getName(), getString(R.string.movie_watch_later));
        }
        if (!TextUtils.isEmpty(msg)) {
            DialogConfirm dialog = new DialogConfirm(activity, true);
            dialog.setUseHtml(true);
            dialog.setMessage(msg);
            dialog.setPositiveLabel(getString(R.string.delete));
            dialog.setNegativeLabel(getString(R.string.cancel));
            dialog.setPositiveListener(result -> {
                if (tabInfo != null && tabInfo.getType() == MovieKind.TYPE_LOCAL
                        && MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId())) {
                    activity.showLoadingDialog("", R.string.loading);
                    getMovieApi().deleteLogWatched(isDeleteAll, movie, new ApiCallbackV2<String>() {
                        @Override
                        public void onSuccess(String lastId, String s) {
                            onDeleteSuccess(isDeleteAll, movie);
                        }

                        @Override
                        public void onError(String s) {
                            if (activity != null && !activity.isFinishing()) activity.showToast(s);
                        }

                        @Override
                        public void onComplete() {
                            if (activity != null && !activity.isFinishing())
                                activity.hideLoadingDialog();
                        }
                    });
                } else if (tabInfo != null && tabInfo.getType() == MovieKind.TYPE_LOCAL
                        && MovieKind.CATEGORYID_GET_LIKED.equals(tabInfo.getCategoryId())) {
                    activity.showLoadingDialog("", R.string.loading);
                    getMovieApi().deleteMovieLiked(isDeleteAll, movie, new ApiCallbackV2<String>() {
                        @Override
                        public void onSuccess(String lastId, String s) {
                            onDeleteSuccess(isDeleteAll, movie);
                        }

                        @Override
                        public void onError(String s) {
                            if (activity != null && !activity.isFinishing()) activity.showToast(s);
                        }

                        @Override
                        public void onComplete() {
                            if (activity != null && !activity.isFinishing())
                                activity.hideLoadingDialog();
                        }
                    });
                } else if (tabInfo != null && tabInfo.getType() == MovieKind.TYPE_LOCAL
                        && (MovieKind.CATEGORYID_GET_LIKED_ODD.equals(tabInfo.getCategoryId()) || MovieKind.CATEGORYID_GET_LIKED_SERIES.equals(tabInfo.getCategoryId()))) {
                    activity.showLoadingDialog("", R.string.loading);
                    getMovieApi().deleteMovieLiked(isDeleteAll, movie, new ApiCallbackV2<String>() {
                        @Override
                        public void onSuccess(String lastId, String s) {
                            onDeleteSuccess(isDeleteAll, movie);
                        }

                        @Override
                        public void onError(String s) {
                            if (activity != null && !activity.isFinishing()) activity.showToast(s);
                        }

                        @Override
                        public void onComplete() {
                            if (activity != null && !activity.isFinishing())
                                activity.hideLoadingDialog();
                        }
                    });
                } else if (tabInfo != null && tabInfo.getType() == MovieKind.TYPE_LOCAL
                        && (MovieKind.CATEGORYID_GET_LATER.equals(tabInfo.getCategoryId()))) {
                    activity.showLoadingDialog("", R.string.loading);
                    getMovieApi().deleteWatchLater(false, movie, new ApiCallbackProgressV2<String>() {
                        @Override
                        public void onProgress(float progress) {

                        }

                        @Override
                        public void onSuccess(String msg, String result) throws JSONException {
                            onDeleteSuccess(isDeleteAll, movie);
                        }

                        @Override
                        public void onError(String s) {
                            if (activity != null && !activity.isFinishing()) activity.showToast(s);
                        }

                        @Override
                        public void onComplete() {
                            if (activity != null && !activity.isFinishing())
                                activity.hideLoadingDialog();
                        }
                    });
                }
            });
            dialog.show();
        }
    }

    private void onDeleteSuccess(boolean isDeleteAll, Movie movie) {
        if (activity != null && !activity.isFinishing()) {
            activity.showToast(R.string.deleted_successfully);
            if (isDeleteAll) {
                if (data != null) data.clear();
                if (adapter != null) adapter.notifyDataSetChanged();
                if (loadingView != null) loadingView.showLoadedEmpty();
                activity.finish();
            } else {
                if (data != null) {
                    int position = data.indexOf(movie);
                    if (position >= 0) {
                        data.remove(position);
                        if (adapter != null) adapter.notifyDataSetChanged();
                        if (Utilities.isEmpty(data)) {
                            if (loadingView != null) loadingView.showLoadedEmpty();
                        } else {
                            if (loadingView != null) loadingView.showLoadedSuccess();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final CategoryDetailEvent event) {
        Log.i(TAG, "onMessageEvent CategoryDetailEvent: " + event);
        if (event.getTabInfo() != null && Utilities.equals(tabInfo, event.getTabInfo())) {
            if (event.isDeleteAll()) {
                deleteMovie(true, null);
            } else if (event.isScrollToTop()) {
                if (recyclerView != null) recyclerView.scrollToPosition(0);
            }
        }
    }

}

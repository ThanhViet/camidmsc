/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/13
 *
 */

package com.metfone.selfcare.module.share.task;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.share.listener.ShareImagesOnFacebookListener;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class DownloadImagesForShareFacebookTask extends AsyncTask<Void, Integer, ArrayList<Bitmap>> {
    private final String TAG = "DownloadImagesForShareFacebookTask";
    private WeakReference<ApplicationController> application;
    private ShareImagesOnFacebookListener listener;
    private ArrayList<FeedContent.ImageContent> list;

    public DownloadImagesForShareFacebookTask(ApplicationController application, ArrayList<FeedContent.ImageContent> list) {
        this.application = new WeakReference<>(application);
        this.list = list;
    }

    public void setListener(ShareImagesOnFacebookListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) listener.onPrepareDownload();
    }

    @Override
    protected ArrayList<Bitmap> doInBackground(Void... voids) {
        if (Utilities.notNull(application) && Utilities.notEmpty(list)) {
            if (application.get().isDataReady()) {
                ArrayList<Bitmap> results = new ArrayList<>();
                for (FeedContent.ImageContent item : list) {
                    if (item != null) {
                        Bitmap bitmap;
                        if (!TextUtils.isEmpty(item.getFilePath())) {
                            bitmap = ImageBusiness.getImageBitmapFromFile(item.getFilePath());
                        } else {
                            bitmap = ImageBusiness.downloadImageBitmap(item.getImageUrl(application.get()));
                        }
                        if (bitmap != null) {
                            results.add(bitmap);
                        }
                    }
                }
                return results;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(@Nullable ArrayList<Bitmap> results) {
        if (listener != null) listener.onCompletedDownload(results);
    }

}

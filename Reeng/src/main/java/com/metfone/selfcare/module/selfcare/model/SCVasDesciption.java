package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCVasDesciption implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("description")
    private String description;

    @SerializedName("shortDesCription")
    private String shortDesCription;

    @SerializedName("language")
    private String language;

    @SerializedName("popup")
    private String popup;

    @SerializedName("unsPopup")
    private String unsPopup;

    public String getUnsPopup() {
        return unsPopup;
    }

    public void setUnsPopup(String unsPopup) {
        this.unsPopup = unsPopup;
    }

    public String getPopup() {
        return popup;
    }

    public void setPopup(String popup) {
        this.popup = popup;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDesCription() {
        return shortDesCription;
    }

    public void setShortDesCription(String shortDesCription) {
        this.shortDesCription = shortDesCription;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "SCVasDesciption{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", shortDesCription='" + shortDesCription + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}

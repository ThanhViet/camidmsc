package com.metfone.selfcare.module.selfcare.event;

public class SCAccountEvent{
    public static final int LOGOUT = 1;
    public static final int CHANGE_AVATAR = 2;
    public static final int CHANGE_NAME = 3;
    public static final int UPDATE_INFO = 4;

    int event;

    public SCAccountEvent(int event) {
        this.event = event;
    }

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }
}
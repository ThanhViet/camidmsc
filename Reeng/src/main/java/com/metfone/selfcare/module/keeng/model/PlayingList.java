package com.metfone.selfcare.module.keeng.model;

import com.metfone.selfcare.helper.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlayingList implements Serializable {
    public static final int TYPE_RANDOM = -1;
    public static final int TYPE_FULL_DATA = 0;
    public static final int TYPE_SONG = 1;
    public static final int TYPE_ALBUM = 2;
    public static final int TYPE_PLAYLIST = Constants.TYPE_PLAYLIST;
    public static final int TYPE_TOPIC = 10;
    public static final int TYPE_SONG_BXH_VN = Constants.TYPE_SONG_BXH_VN;
    public static final int TYPE_SONG_BXH_CA = Constants.TYPE_SONG_BXH_CA;
    public static final int TYPE_SONG_BXH_AM = Constants.TYPE_SONG_BXH_AM;

    private static final long serialVersionUID = 5137582106401091315L;
    private long id = -1;
    private String name;
    private String identify;
    private String singer;
    private int typePlaying = TYPE_FULL_DATA;
    private List<AllModel> mediaList;

    private int source = 0;
    private long userId;
    private boolean hasLossless = false;

    public PlayingList() {
        addSongList(null);
    }

    public PlayingList(List<AllModel> songList, int typePlaying) {
        addSongList(songList);
        this.typePlaying = typePlaying;
    }

    public PlayingList(List<AllModel> songList, int typePlaying, int source) {
        addSongList(songList);
        this.typePlaying = typePlaying;
        this.source = source;
        if (mediaList != null) {
            for (int i = 0; i < mediaList.size(); i++) {
                if (mediaList.get(i) != null)
                    mediaList.get(i).setSource(source);
            }
        }
    }

    public PlayingList(AllModel item, List<AllModel> songList) {
        super();
        this.typePlaying = TYPE_FULL_DATA;
        addSongList(songList);
        if (item != null) {
            this.id = item.getId();
            this.name = item.getName();
            this.singer = item.getSinger();
            this.identify = item.identify;
        }
    }

    public PlayingList(int typePlaying, long orignal_id, String name, String singer, String identify) {
        super();
        this.typePlaying = typePlaying;
        this.id = orignal_id;
        this.name = name;
        this.singer = singer;
        this.identify = identify;
    }

    private void addSongList(List<AllModel> list) {
        if (mediaList == null)
            mediaList = new ArrayList<>();
        else
            mediaList.clear();
        if (list != null)
            mediaList.addAll(list);
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public int getTypePlaying() {
        return typePlaying;
    }

    public void setTypePlaying(int typePlaying) {
        this.typePlaying = typePlaying;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public List<AllModel> getSongList() {
        if (mediaList == null)
            mediaList = new ArrayList<>();
        return mediaList;
    }

    public void setSongList(List<AllModel> songList) {
        addSongList(songList);
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean isHasLossless() {
        return hasLossless;
    }

    public void setHasLossless(boolean hasLossless) {
        this.hasLossless = hasLossless;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", identify='" + identify + '\'' +
                ", singer='" + singer + '\'' +
                ", typePlaying=" + typePlaying +
                ", mediaList=" + mediaList +
                ", source=" + source +
                ", userId=" + userId +
                ", hasLossless=" + hasLossless +
                '}';
    }

}

package com.metfone.selfcare.module.selfcare.event;

import android.view.View;

import com.metfone.selfcare.module.selfcare.model.SCAirTimeInfo;
import com.metfone.selfcare.module.selfcare.model.SCBanner;
import com.metfone.selfcare.module.selfcare.model.SCDeeplink;
import com.metfone.selfcare.module.selfcare.model.SCPackage;
import com.metfone.selfcare.module.selfcare.model.SCRecommentPackage;
import com.metfone.selfcare.module.selfcare.model.SCStore;
import com.metfone.selfcare.module.selfcare.model.SCSubListModel;
import com.metfone.selfcare.module.selfcare.model.SCTelecomRewardModel;

public class AbsInterface {
    public interface OnPackageListener {
        void onPackageClick(SCPackage item);
    }

    public interface OnPackageRegisterListener {
        void onPackageClick(SCPackage item);
        void onRegisterClick(SCPackage item, View btnRegister);
        void onDeeplinkClick(SCDeeplink item);
    }

    public interface OnPackageHeaderListener {
        void onPackageClick(SCPackage item);
        void onHeaderClick(SCRecommentPackage item);
    }

    public interface OnStoreListener {
        void onStoreClick(SCStore item);
    }

    public interface OnDeeplinkListener {
        void onDeepLinkClick(SCDeeplink item);
    }

    public interface OnBannerListener {
        void onBannerClick(SCBanner item);
    }

    public interface OnAccountListener {
        void onAccountClick(SCSubListModel item);
    }

    public interface OnSCHomeListener {
        void onUserInfoClick();
        void onBannerClick(SCBanner item);
        void onAccountClick();

        void onTopupClick();
        void onPackageListClick();
        void onServicesClick();
        void onUtilitiesClick();
        void onStoreClick();
        void onHistoryClick();
        void onLoyaltyClick();
    }

    public interface OnLoyaltyListener {
        void onTelecomRewardClick(int pos);
        void onAboutClick();
        void onTermClick();
        void onReferFriendClick();
        void onPointHistoryClick();
    }

    public interface OnTelecomRewardListener {
        void onTelecomRewardClick(SCTelecomRewardModel model, View view);
        void onTelecomRewardDetailClick(SCTelecomRewardModel model);
    }

    public interface onAirTimeClickListener
    {
        void onBorrowClick(SCAirTimeInfo model);
    }
}

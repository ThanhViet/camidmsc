package com.metfone.selfcare.module.home_kh.notification.model;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.imageview.CircleImageView;

import java.util.ArrayList;

import butterknife.BindView;

public class KhNotificationAdapter extends BaseAdapter<KhNotificationAdapter.NotificationHolder, KhNotificationItem> {

    public interface OnNotificationClickCallback {
        void onNotificationClicked(KhNotificationItem item, int position);

        void onClearAllNotificationsClicked();
    }

    private Activity activity;

    private OnNotificationClickCallback callback;

    public KhNotificationAdapter(Activity activity, ArrayList<KhNotificationItem> list, OnNotificationClickCallback p) {
        super(activity, list);
        this.activity = activity;
        this.callback = p;
    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationHolder(layoutInflater.inflate(R.layout.item_notification_kh, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHolder holder, int position) {
        holder.bindData(items.get(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public class NotificationHolder extends BaseAdapter.ViewHolder {

        @BindView(R.id.box_title)
        View box_title;
        @BindView(R.id.tv_group_name)
        TextView tv_group_name;
        @BindView(R.id.tv_clear_all)
        TextView tv_clear_all;
        @BindView(R.id.tvNotifyTitle)
        TextView tvNotifyTitle;
        @BindView(R.id.tvNotifyTime)
        TextView tvNotifyTime;
        @BindView(R.id.ic_notify)
        CircleImageView ic_notify;
        @BindView(R.id.ic_notify_type)
        ImageView ic_notifyType;

        public NotificationHolder(View view) {
            super(view);
            view.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (callback != null) {
                        callback.onNotificationClicked(getItem(getAdapterPosition()), getAdapterPosition());
                    }
                }
            });
        }

        @Override
        public void bindData(Object item, int position) {
            super.bindData(item, position);
            if (item instanceof KhNotificationItem) {
                KhNotificationItem model = (KhNotificationItem) item;
                tvNotifyTime.setText((model.time != null) ? model.time : "");
                String time = (DateConvert.formatDateTimeNotification(activity, model.time));
                tvNotifyTime.setText(time);

                String title = "";
                if (LocaleManager.isKmLanguage(activity)) {
                    title = StringUtils.isEmpty(model.titleKh) ? model.titleEn : model.titleKh;
                } else {
                    title = model.titleEn;
                }
                if (model.readStatus == KhNotificationItem.Status.READ) {
                    tvNotifyTitle.setAlpha(0.5f);
                } else {
                    tvNotifyTitle.setAlpha(1.0f);
                }
                tvNotifyTitle.setText(title);
                box_title.setVisibility(View.GONE);
                if (model.actionType != null && (model.actionType.equals(KhNotificationItem.ActionType.METFONE_TOPUP) || model.actionType.equals(KhNotificationItem.ActionType.METFONE_SERVICE))) {
                    Glide.with(activity).load(R.drawable.kh_notic_metfone).error(R.drawable.df_image_home).into(ic_notify);
                    ic_notifyType.setVisibility(View.GONE);
                } else {
                    int resId = 0;
                    if (model.actionType != null) {
                        resId = KhNotificationItem.getTypeIcon(model.actionType);
                    }
                    if (resId > 0) {
                        ic_notifyType.setImageResource(resId);
                        ic_notifyType.setVisibility(View.VISIBLE);
                    } else {
                        ic_notifyType.setVisibility(View.GONE);
                    }
                    ImageBusiness.setCoverNotification(ic_notify, model.iconUrl, R.drawable.df_image_home);
                }

                if (position == 0) {
                    box_title.setVisibility(View.VISIBLE);
                    tv_group_name.setText(R.string.kh_noti_new);
                } else if (position == 1 || position == 2) {
                    box_title.setVisibility(View.GONE);
                } else {
                    if (position == 3) {
                        box_title.setVisibility(View.VISIBLE);
//                        String todayDate = DateConvert.getTodayDate(activity, "MMMM d, yyyy");
//                        if (todayDate.equalsIgnoreCase(time)) {
//                            tv_group_name.setText(R.string.kh_noti_today);
//                        } else {
//                            tv_group_name.setText(time);
//                        }
                        tv_group_name.setText(R.string.kh_noti_today);
                    } else {
//                        String previousTime = (DateConvert.formatDateTimeNotification(activity, ((KhNotificationItem) items.get(position - 1)).time));
//                        if (previousTime.equalsIgnoreCase(time)) {
//                            box_title.setVisibility(View.GONE);
//                        } else {
//                            box_title.setVisibility(View.VISIBLE);
//                            tv_group_name.setText(time);
//                        }
                        box_title.setVisibility(View.GONE);
                    }
                }

                if (position == 0) {
                    tv_clear_all.setVisibility(View.VISIBLE);
                    tv_clear_all.setOnClickListener(new OnSingleClickListener() {
                        @Override
                        public void onSingleClick(View view) {
                            if (callback != null) {
                                callback.onClearAllNotificationsClicked();
                            }
                        }
                    });
                } else {
                    tv_clear_all.setVisibility(View.GONE);
                }
            }
        }
    }
}

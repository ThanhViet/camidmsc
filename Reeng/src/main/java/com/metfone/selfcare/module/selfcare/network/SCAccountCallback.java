package com.metfone.selfcare.module.selfcare.network;

import com.metfone.selfcare.util.Log;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by thanhnt72 on 2/15/2019.
 */

public class SCAccountCallback implements Callback {

    private static final String TAG = SCAccountCallback.class.getSimpleName();

    SCAccountApiListener listener;

    public SCAccountCallback(SCAccountApiListener listener) {
        this.listener = listener;
    }

    @Override
    public void onFailure(Call call, IOException e) {
        try {
            if (listener != null)
                listener.onError(call.execute().code(), call.execute().message());
        } catch (Exception e1) {
            if (listener != null)
                listener.onError(-1, "onFailure exception");
        }
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        // String loginResponseString = response.body().string();
        try {
            if (listener != null) {
                if (response.body() != null) {
                    String r = response.body().string();
                    int code = response.code();
//                    if (code == 200 || code == 201) {
//                        Log.i(TAG, "response: " + r);
//                        listener.onSuccess(r);
//                    } else {
//                        listener.onError(code, response.message());
//                    }
                    if (code == 200 || code == 201) {
                        Log.i(TAG, "response: " + r);
                        listener.onSuccess(r);
                    } else {
                        if(code == 401 || code == 403)
                            listener.onError(code, response.message());
                        else
//                            listener.onError(code, r);
                            listener.onSuccess(r);
                    }
                } else {
                    listener.onError(-1, "response.body() null");
                }
            }
        } catch (Exception e) {
            if (listener != null)
                listener.onError(-1, "onResponse exception");
        }
        // Log.i(TAG, "loginResponseString: " + loginResponseString);
    }

    public interface SCAccountApiListener {
        public void onSuccess(String response);

        public void onError(int code, String message);
    }

}

package com.metfone.selfcare.module.games;

public interface OnActionCallBack {
    void onCallBack(String key, Object data);
}

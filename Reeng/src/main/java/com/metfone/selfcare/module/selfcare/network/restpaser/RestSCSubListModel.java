package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCSubListModel;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCSubListModel extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("content")
    private ArrayList<SCSubListModel> data;

    public ArrayList<SCSubListModel> getContent() {
        return data;
    }

    public void setData(ArrayList<SCSubListModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCSubModel [data=" + data + "] errror " + getErrorCode();
    }
}

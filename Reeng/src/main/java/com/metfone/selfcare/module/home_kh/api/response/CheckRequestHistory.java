package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckRequestHistory {
    @SerializedName("column")
    @Expose
    String column;
    @SerializedName("oldValue")
    @Expose
    String oldValue;
    @SerializedName("newValue")
    @Expose
    String newValue;
    @SerializedName("updateDate")
    @Expose
    String updateDate;
    @SerializedName("updateUser")
    @Expose
    String updateUser;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
}


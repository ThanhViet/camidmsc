/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/4/22
 *
 */

package com.metfone.selfcare.module.movie.holder;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.FirebaseEventConstant;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.ui.view.LinkTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Utilities;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.metfone.selfcare.business.UserInfoBusiness.isLogin;

public class FilmDetailHolder implements NumberCallback, LinkTextView.OnReadMoreListener, LinkTextView.OnLinkListener {

    @Nullable
    @BindView(R.id.frVideo)
    FrameLayout frVideo;
    @Nullable
    @BindView(R.id.frController)
    FrameLayout frController;
    @Nullable
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @Nullable
    @BindView(R.id.tvNumberSeen)
    TextView tvNumberSeen;
    @Nullable
    @BindView(R.id.tvPublish)
    TextView tvPublish;
    @Nullable
    @BindView(R.id.tv_imdb)
    TextView tvIMDb;
    @Nullable
    @BindView(R.id.tvDescriptionNew)
//    LinkTextView tvDescription;
    TextView tvDescription; // tat read more cua description
    @Nullable
    @BindView(R.id.ivHear)
    AppCompatImageView ivHear;
    @Nullable
    @BindView(R.id.tvNumberHear)
    TextView tvNumberHear;
    @Nullable
    @BindView(R.id.tvNumberComment)
    TextView tvNumberComment;
    @Nullable
    @BindView(R.id.tvNumberShare)
    TextView tvNumberShare;
    @Nullable
    @BindView(R.id.ivSave)
    AppCompatImageView ivSave;
    @Nullable
    @BindView(R.id.rec_film_info)
    RecyclerView recyclerFilmInfo;
    @BindView(R.id.tvPlay)
    TextView tvPlay;
    @BindView(R.id.ivRate)
    ImageView ivRate;
    @BindView(R.id.tv_continue_watching)
    TextView tv_continue_watching;

    private BaseSlidingFragmentActivity activity;
    private Video currentVideo;
    private OnItemVideoClickedListener onItemVideoClickedListener;
    private final String LANGUAGE_KM = "km";

    public FilmDetailHolder(BaseSlidingFragmentActivity activity, View view) {
        ButterKnife.bind(this, view);
//        tvDescription.setOnReadMoreListener(this);
//        tvDescription.setOnLinkListener(this);
        this.activity = activity;
    }

    private static void numberToShorten(TextView textView, final String id, final long numberView, Executor executor, @NonNull NumberCallback numberCallback) {
        final Handler handler = new Handler();
        final WeakReference<TextView> textViewRef = new WeakReference<>(textView);
        final WeakReference<NumberCallback> numberCallbackRef = new WeakReference<>(numberCallback);

        executor.execute(new Runnable() {
            @Override
            public void run() {
                TextView textView = textViewRef.get();
                NumberCallback numberCallback = numberCallbackRef.get();
                if (textView == null || numberCallback == null) return;

                final String numberStr = Utilities.shortenLongNumber(numberView);

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        TextView textView = textViewRef.get();
                        NumberCallback numberCallback = numberCallbackRef.get();
                        if (textView == null || numberCallback == null) return;

                        numberCallback.onNumberCompleted(textView, id, numberStr);
                    }
                });
            }
        });
    }

    public void setOnItemVideoClickedListener(@Nullable OnItemVideoClickedListener onItemVideoClickedListener) {
        this.onItemVideoClickedListener = onItemVideoClickedListener;
    }

    @NonNull
    public ViewGroup getFrameVideo() {
        return frVideo;
    }

    @Override
    public void onNumberCompleted(TextView textView, String id, String number) {
        if (currentVideo == null || Utilities.isEmpty(id)) return;
        if (tvNumberHear == textView && Utilities.equals(id, currentVideo.getId())) {
//            tvNumberHear.setText(number);
        } else if (tvNumberComment == textView && Utilities.equals(id, currentVideo.getId())) {
            tvNumberComment.setText(number);
        } else if (tvNumberShare == textView && Utilities.equals(id, currentVideo.getId())) {
//            tvNumberShare.setText(number);
        } else if (tvNumberSeen == textView && Utilities.equals(id, currentVideo.getId())) {
            if (currentVideo.getTotalView() <= 0) {
//                tvNumberSeen.setVisibility(View.GONE);
            } else {
                tvNumberSeen.setVisibility(View.VISIBLE);
                tvNumberSeen.setText(String.format(currentVideo.getTotalView() <= 1 ?
                        ApplicationController.self().getString(R.string.view) :
                        ApplicationController.self().getString(R.string.video_views), number));
            }
        } else if (tvPublish == textView && Utilities.equals(id, currentVideo.getId())) {
            if (currentVideo.getPublishTime() > 0) {
                if (tvPublish != null) {
                    tvPublish.setVisibility(View.VISIBLE);
                    tvPublish.setText(DateTimeUtils.calculateTime(activity.getResources(), currentVideo.getPublishTime()));
                }
            } else {
//                if (tvPublish != null) tvPublish.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onLink(String content, int type) {
        if (type == Constants.SMART_TEXT.TYPE_MOCHA) {
            DeepLinkHelper.getInstance().openSchemaLink(activity, content);
        } else if (type == Constants.SMART_TEXT.TYPE_URL) {
            Utilities.openLink(activity, content);
        }
    }

    @Override
    public void onReadMore() {
        if (tvDescription == null || currentVideo == null) return;
        currentVideo.setCollapse(false);
//        tvDescription.asyncSetText(getDescription(currentVideo), currentVideo.isCollapse());
        tvDescription.setText(currentVideo.getDescription());
        if (recyclerFilmInfo != null) recyclerFilmInfo.setVisibility(View.VISIBLE);
    }

    public FrameLayout getControllerFrame() {
        return frController;
    }

    public void bindVideoOnlyTitle(Video video){
        if(tvTitle == null){
            return;
        }
        tvTitle.setText(video.getTitle());
    }

    public void bindVideo(@Nullable Video video) {
        if (video == null || Utilities.isEmpty(video.getId()) || tvDescription == null) return;
        currentVideo = video;
        if (TextUtils.isEmpty(video.getDescription())) {
            tvDescription.setVisibility(View.GONE);
            recyclerFilmInfo.setVisibility(View.VISIBLE);
        } else {
            tvDescription.setVisibility(View.VISIBLE);
//            tvDescription.setColorReadMore(R.color.v5_text_2);
//            tvDescription.asyncSetText(getDescription(video), video.isCollapse());
            tvDescription.setText(video.getDescription());
            recyclerFilmInfo.setVisibility(video.isCollapse() ? View.GONE : View.VISIBLE);
        }
        tvTitle.setText(video.getTitle());
        bindVideoAction(video);
    }

    public void setContinueToWatch(Video video){
        if(tvPlay != null){
            tvPlay.setText(R.string.resume);
        }
        if(tv_continue_watching != null){
            tv_continue_watching.setVisibility(View.VISIBLE);
            tv_continue_watching.setText(activity.getString(R.string.continue_to_watch) + " " + video.getTitle());
        }
    }

    public void bindVideoAction(@NonNull Video video) {
        if (video == null) return;
        if (ivSave != null)
            ivSave.setImageResource(video.isSave() ? R.drawable.ic_del_option : R.drawable.ic_add_option);

        ExecutorService executor = Executors.newFixedThreadPool(5);
        numberToShorten(tvNumberHear, video.getId(), video.getTotalLike(), executor, this);
        numberToShorten(tvNumberComment, video.getId(), video.getTotalComment(), executor, this);
//        numberToShorten(tvNumberShare, video.getId(), video.getTotalShare(), executor, this);
        numberToShorten(tvNumberSeen, video.getId(), video.getTotalView(), executor, this);
        numberToShorten(tvPublish, video.getId(), video.getPublishTime(), executor, this);
        if (TextUtils.isEmpty(video.getImdb())) {
            tvIMDb.setVisibility(View.GONE);
        } else {
            tvIMDb.setVisibility(View.VISIBLE);
            tvIMDb.setText(video.getImdb());
        }
        executor.shutdown();
    }

    public void hideSaveVideo() {
        ivSave.setVisibility(View.GONE);
    }

    @OnClick(R.id.llControllerHear)
    public void onLlControllerHearClicked() {
        if(currentVideo != null){
            if(currentVideo.isLike()){
                ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_LIKE);
            }else{
                ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_UNLIKE);
            }
        }
        if (isLogin() == EnumUtils.LoginVia.NOT){
            new UserInfoBusiness(activity).showLoginDialog(activity, R.string.info, R.string.content_popup_login);
            return;
        }
        if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
            activity.showDialogLogin();
        } else {
            if (currentVideo == null) return;
            currentVideo.setLike(!currentVideo.isLike());
            currentVideo.setTotalLike(currentVideo.isLike() ? currentVideo.getTotalLike() + 1 : currentVideo.getTotalLike() - 1);
            ApplicationController.self().getApplicationComponent().providerListenerUtils().notifyVideoLikeChangedData(currentVideo);
            bindVideoAction(currentVideo);
        }
    }

    @OnClick(R.id.llControllerComment)
    public void onLlControllerCommentClicked() {
        if (currentVideo == null) return;
        ApplicationController.self().getApplicationComponent().providesUtils().openCommentVideo(activity, currentVideo);
        if (onItemVideoClickedListener != null) {
            onItemVideoClickedListener.onBtnCommentClicked(currentVideo);
        }
    }

    @OnClick(R.id.llControllerShare)
    public void onLlControllerShareClicked() {
        ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_SHARE);
        if (onItemVideoClickedListener != null && currentVideo != null) {
            onItemVideoClickedListener.onBtnShareClicked(currentVideo);
        }
    }

    @OnClick(R.id.tvDescription)
    public void onDescriptionClicked() {
        onReadMore();
    }

    public interface OnItemVideoClickedListener {
        void onBtnCommentClicked(@NonNull Video video);

        void onBtnShareClicked(@NonNull Video video);
    }

    public void rate(boolean isRated) {
        if (ivRate != null) {
            ivRate.setImageResource(isRated ? R.drawable.ic_rate_new_red : R.drawable.ic_rate_new_white);
        }
    }

    public void setLike(boolean isLike) {
        if (ivHear != null) {
            ivHear.setImageResource(isLike ? R.drawable.ic_v5_heart_active : R.drawable.ic_favior__new);
        }
    }
}

interface NumberCallback {
    void onNumberCompleted(TextView textView, String id, String number);
}
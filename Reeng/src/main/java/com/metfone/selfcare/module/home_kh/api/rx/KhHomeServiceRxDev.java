package com.metfone.selfcare.module.home_kh.api.rx;

import com.metfone.selfcare.module.home_kh.api.response.AccountPointRankResponse;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.PartnerGiftRedeemHistoryResponse;
import com.metfone.selfcare.module.home_kh.api.response.PointTransferHistoryResponse;
import com.metfone.selfcare.module.home_kh.api.response.RankDefineResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsCheckForceUpdateAppResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetAllPartnerGiftSearchResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetAllTeLeComGiftResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetPartnerGiftDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemGiftAndReturnExchangeCodeResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemPointResponse;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 *
 */
public interface KhHomeServiceRxDev {

    @POST("UserRouting")
    Observable<KHBaseResponse<RankDefineResponse>> wsGetRankDefineInfo(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<AccountRankInfoResponse>> wsGetAccountRankInfo(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<AccountPointRankResponse>> wsGetAccountPointInfo(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<PartnerGiftRedeemHistoryResponse>> wsGetPartnerGiftRedeemHistory(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<PointTransferHistoryResponse>> wsGetPointTransferHistory(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<WsGetAllPartnerGiftSearchResponse>> wsGtAllPartnerGiftSearch(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<WsGetAllTeLeComGiftResponse>> wsGetAllTeLeComGiftResponse(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<WsGetPartnerGiftDetailResponse>> wsGetPartnerGiftDetail(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<WsRedeemPointResponse>> wsRedeemPoint(@Body RequestBody requestBody);
    @POST("UserRouting")
    Observable<KHBaseResponse<WsRedeemGiftAndReturnExchangeCodeResponse>> wsRedeemGiftAndReturnExchangeCode(@Body RequestBody requestBody);

    @POST("UserRouting")
    Observable<KHBaseResponse<WsCheckForceUpdateAppResponse>> wsCheckForceUpdateApp(@Body RequestBody requestBody);

}
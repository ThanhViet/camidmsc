package com.metfone.selfcare.module.movienew.common;

public class Constant {
    public static final String DATA_SEARCH = "data_search";
    public static final String HOME = "home";
    public static final String RESULT = "result";
    public static final String FIRST_OPEN = "open_first";
    public static final String LANGUAGE_KM = "km";
}

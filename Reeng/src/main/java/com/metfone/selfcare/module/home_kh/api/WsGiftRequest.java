package com.metfone.selfcare.module.home_kh.api;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGiftRequest extends BaseRequest<WsGiftRequest.Request> {
    public class Request{
        public String programeCode;
        public String msisdn;
        public String refID;
        public String description;
        public String language;
        public String giftRewardId;
    }
}

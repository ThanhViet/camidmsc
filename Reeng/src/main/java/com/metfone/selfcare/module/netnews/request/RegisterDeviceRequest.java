package com.metfone.selfcare.module.netnews.request;

/**
 * Created by HaiKE on 10/10/17.
 */

public class RegisterDeviceRequest {
    String imei;
    String uuid;
    String registrationid;
    int type;
    String msisdn;
    String revision;

    public RegisterDeviceRequest(String imei, String uuid, String registrationid, int type, String msisdn, String revision) {
        this.imei = imei;
        this.uuid = uuid;
        this.registrationid = registrationid;
        this.type = type;
        this.msisdn = msisdn;
        this.revision = revision;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getRegistrationid() {
        return registrationid;
    }

    public void setRegistrationid(String registrationid) {
        this.registrationid = registrationid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}

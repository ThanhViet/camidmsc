/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/26
 */

package com.metfone.selfcare.module.security.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.security.event.SecurityPageEvent;
import com.metfone.selfcare.module.security.helper.SecurityApi;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FirewallSettingFragment extends BaseFragment {
    @BindView(R.id.tv_description)
    TextView tvDesc;
    @BindView(R.id.button_setting)
    TextView btnSetting;
    private Unbinder unbinder;
    private boolean enableFirewallMode;

    public static FirewallSettingFragment newInstance() {
        Bundle args = new Bundle();
        FirewallSettingFragment fragment = new FirewallSettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick(R.id.button_setting)
    public void onSettingClick() {
        btnSetting.setText(enableFirewallMode ? R.string.security_enable : R.string.security_disable);
        mActivity.showLoadingDialog("", R.string.processing);
        SecurityApi.getInstance().setupFirewallSms(enableFirewallMode ? "0" : "1", new ApiCallbackV2<String>() {
            @Override
            public void onSuccess(String lastId, String s) throws JSONException {
                SharedPref.newInstance(mActivity).putString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, enableFirewallMode ? "0" : "1");
                enableFirewallMode = !enableFirewallMode;
            }

            @Override
            public void onError(String s) {
                mActivity.showToast(s);
                if (btnSetting != null)
                    btnSetting.setText(enableFirewallMode ? R.string.security_disable : R.string.security_enable);
            }

            @Override
            public void onComplete() {
                mActivity.hideLoadingDialog();
            }
        });
    }

    @Override
    public String getName() {
        return "FirewallSettingFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_security_firewall_setting;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                SecurityPageEvent event = new SecurityPageEvent();
                event.setSelectTab(true);
                event.setPositionTab(1);
                EventBus.getDefault().post(event);
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.home_tab_security)), start, end, flags);
        strBuilder.removeSpan(span);
    }

    private void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        enableFirewallMode = "1".equals(SharedPref.newInstance(mActivity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, ""));
        btnSetting.setText(enableFirewallMode ? R.string.security_disable : R.string.security_enable);
        tvDesc.setMovementMethod(LinkMovementMethod.getInstance());
        setTextViewHTML(tvDesc, mActivity.getString(R.string.security_firewall_setting_desc));
    }

}

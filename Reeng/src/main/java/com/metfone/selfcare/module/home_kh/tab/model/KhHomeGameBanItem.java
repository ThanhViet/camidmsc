package com.metfone.selfcare.module.home_kh.tab.model;

public class KhHomeGameBanItem implements IHomeModelType{

    @Override
    public int getItemType() {
        return IHomeModelType.GAME_ITEM;
    }
}

package com.metfone.selfcare.module.home_kh.onboarding.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.airbnb.lottie.LottieAnimationView;
import com.metfone.selfcare.R;

public class FrmKhOnBoardingPage1 extends FrmKhOnBoarding {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_onboarding_page1;
    }

    @Override
    protected int getPageIndex() {
        return 0;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LottieAnimationView lt = view.findViewById(R.id.obd_img_wc);
        lt.setProgress(0);
        lt.playAnimation();
    }
}

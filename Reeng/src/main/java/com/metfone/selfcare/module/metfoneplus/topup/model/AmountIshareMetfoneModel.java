package com.metfone.selfcare.module.metfoneplus.topup.model;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AmountIshareMetfoneModel {
    private int amount;
    private boolean isCheckSelect;

    public AmountIshareMetfoneModel(int amount, boolean isCheckSelect) {
        this.amount = amount;
        this.isCheckSelect = isCheckSelect;
    }

    public static ArrayList<AmountIshareMetfoneModel> createAmountValue() {
        ArrayList<AmountIshareMetfoneModel> metfoneModels = new ArrayList<>();
        metfoneModels.add(new AmountIshareMetfoneModel(1, false));
        metfoneModels.add(new AmountIshareMetfoneModel(2, false));
        metfoneModels.add(new AmountIshareMetfoneModel(5, false));
        metfoneModels.add(new AmountIshareMetfoneModel(10, false));
        return metfoneModels;
    }
}



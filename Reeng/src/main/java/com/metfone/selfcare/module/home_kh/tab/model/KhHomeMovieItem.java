package com.metfone.selfcare.module.home_kh.tab.model;

import android.widget.LinearLayout;

import com.metfone.selfcare.module.movienew.model.HomeData;

import java.util.ArrayList;
import java.util.List;

public class KhHomeMovieItem implements IHomeModelType {
    public long id;
    public long id_group;
    public String name;
    public String description;
    public String image_path;
    public String poster_path;
    public String link_wap;

    @Override
    public int getItemType() {
        return IHomeModelType.CUSTOM_TOPIC;
    }

    public static ArrayList<KhHomeMovieItem> convertHomeDataToBanner(List<HomeData> homeData) {
        ArrayList<KhHomeMovieItem> result = new ArrayList<>();
        if (homeData != null) {
            for (HomeData data : homeData) {
                KhHomeMovieItem item = new KhHomeMovieItem();
                item.id = data.getId();
                item.id_group = data.getIdGroup();
                item.name = data.getName();
                item.description = data.getDescription();
                item.image_path = data.getImagePath();
                item.poster_path = data.getPosterPath();
                item.link_wap = data.getLinkWap();
                result.add(item);
            }
        }
        return result;
    }
}

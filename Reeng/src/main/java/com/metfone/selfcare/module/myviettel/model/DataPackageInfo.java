/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/2
 *
 */

package com.metfone.selfcare.module.myviettel.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataPackageInfo implements Serializable {
    private static final long serialVersionUID = 8103444159895733631L;

    @SerializedName("id")
    private String id;
    @SerializedName("image")
    private String image;
    @SerializedName("promotion_price")
    private int promotionPrice;
    @SerializedName("price")
    private long price;
    @SerializedName("confirm_reg")
    private String confirmReg;
    @SerializedName("label_reg")
    private String labelReg;
    @SerializedName("type")
    private String type;
    @SerializedName("detail")
    private String detail;
    @SerializedName("display")
    private String display;
    @SerializedName("pack_code")
    private String packageCode;

    public String getId() {
        if (id == null) id = "";
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(int promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getConfirmReg() {
        return confirmReg;
    }

    public void setConfirmReg(String confirmReg) {
        this.confirmReg = confirmReg;
    }

    public String getLabelReg() {
        return labelReg;
    }

    public void setLabelReg(String labelReg) {
        this.labelReg = labelReg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDisplay() {
        if (display == null) display = "";
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getPackageCode() {
        if (packageCode == null) packageCode = "";
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", image='" + image + '\'' +
                ", promotionPrice=" + promotionPrice +
                ", price=" + price +
                ", confirmReg='" + confirmReg + '\'' +
                ", labelReg='" + labelReg + '\'' +
                ", type='" + type + '\'' +
                ", detail='" + detail + '\'' +
                ", display='" + display + '\'' +
                ", packageCode='" + packageCode + '\'' +
                '}';
    }
}

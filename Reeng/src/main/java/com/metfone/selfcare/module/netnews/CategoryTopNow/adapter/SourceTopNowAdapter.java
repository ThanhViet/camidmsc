package com.metfone.selfcare.module.netnews.CategoryTopNow.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.DateUtilitis;
import com.metfone.selfcare.ui.glide.ImageLoader;

import java.util.List;

/**
 * Created by HaiKE on 8/19/17.
 */

public class SourceTopNowAdapter extends BaseQuickAdapter<NewsModel, BaseViewHolder> {

    private Context context;

    public SourceTopNowAdapter(Context context, int id, List<NewsModel> datas) {
        super(id, datas);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder holder, NewsModel model) {

        if(model != null)
        {
            if(holder.getView(R.id.tv_title) != null)
                holder.setText(R.id.tv_title, model.getTitle());

            if(holder.getView(R.id.tv_datetime) != null) {
                holder.setVisible(R.id.tv_datetime, true);
                if(model.getTimeStamp() > 0){
                    holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, model.getTimeStamp()));
                }else {
                    holder.setText(R.id.tv_datetime, Html.fromHtml(model.getDatePub()));
                }
            }
            if (holder.getView(R.id.tv_desc) != null) {
                holder.setVisible(R.id.tv_desc, true);
                holder.setText(R.id.tv_desc, model.getShapo());
            }
            if(holder.getView(R.id.tv_category) != null){
                holder.setText(R.id.tv_category,model.getSourceName());
            }
            if(holder.getView(R.id.iv_cover) != null)
            {
                ImageBusiness.setImageNew(model.getImage169(), (ImageView) holder.getView(R.id.iv_cover));
            }
        }
    }


}

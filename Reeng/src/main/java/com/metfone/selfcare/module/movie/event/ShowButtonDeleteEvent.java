/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/14
 *
 */

package com.metfone.selfcare.module.movie.event;

public class ShowButtonDeleteEvent {
    boolean isShow;

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    @Override
    public String toString() {
        return "{" +
                "isShow=" + isShow +
                '}';
    }
}

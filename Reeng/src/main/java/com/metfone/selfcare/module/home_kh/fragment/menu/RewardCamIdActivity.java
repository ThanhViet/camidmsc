package com.metfone.selfcare.module.home_kh.fragment.menu;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.manager.SupportRequestManagerFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.module.home_kh.fragment.reward.RewardCamIdKHFragment;
import com.metfone.selfcare.util.Utilities;

public class RewardCamIdActivity extends BaseSlidingFragmentActivity {
    FrameLayout reward_container;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        setContentView(R.layout.activity_reward_camid_setting);
        reward_container = findViewById(R.id.reward_container);
        Utilities.adaptViewForInsertBottom(reward_container);
        FragmentTransaction mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.reward_container, RewardCamIdKHFragment.newInstance());
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        int size = getSupportFragmentManager().getFragments().size();
        if (size > 0) {
            Fragment fragment = getSupportFragmentManager().getFragments().get(size - 1);
            if (fragment instanceof SupportRequestManagerFragment) {
                if (size - 2 >= 0) {
                    Fragment nextFragment = getSupportFragmentManager().getFragments().get(size - 2);
                    if (nextFragment instanceof RewardCamIdKHFragment) {
                        super.onBackPressed();
                    } else {
                        popBackStackFragment();
                    }
                }
            } else {
                if (fragment instanceof RewardCamIdKHFragment) {
                    super.onBackPressed();
                } else {
                    popBackStackFragment();
                }
            }
        } else {
            super.onBackPressed();
        }
    }
}

package com.metfone.selfcare.module.metfoneplus.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.module.metfoneplus.CircleOverlayView;
import com.metfone.selfcare.module.metfoneplus.HalfCircleHigherOverlayView;
import com.metfone.selfcare.module.metfoneplus.HalfCircleOverlayView;
import com.metfone.selfcare.module.metfoneplus.fragment.MPExchangeGuideLineFragment;
import com.metfone.selfcare.util.Utilities;

public class ExchangeGuidelineActivity extends BaseSlidingFragmentActivity {
    private CircleOverlayView circleOverlayView;
    private HalfCircleOverlayView halfCircleOverlayView;
    private HalfCircleHigherOverlayView halfCircleHigherOverlayView;
    private ConstraintLayout ctlAnimation;
    private AppCompatTextView tvMoreService;
    private ImageView imgArrow;
    private FrameLayout flDialog;
    private TextView tvSkip, tvUDidIt, tvTutor, tvTutor2, tvDone;
    private static ExchangeGuidelineActivity mSelf;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSelf = this;

        // set SystemUI & hide navigation
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);

        setContentView(R.layout.activity_exchange_guideline);
        initView();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.root_frame, MPExchangeGuideLineFragment.newInstance())
                .commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        mSelf = this;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSelf = null;
    }

    public static ExchangeGuidelineActivity self() {
        return mSelf;
    }

    private void initView() {
        ctlAnimation = findViewById(R.id.ctl_animation);
        tvMoreService = findViewById(R.id.text_more_service);
        imgArrow = findViewById(R.id.arrow_up);
        flDialog = findViewById(R.id.frame_dialog_confirm);
        tvSkip = findViewById(R.id.tv_skip);
        tvUDidIt = findViewById(R.id.tv_you_did_it);
        tvTutor = findViewById(R.id.tv_tutor);
        tvTutor2 = findViewById(R.id.tv_tutor2);
        tvDone = findViewById(R.id.tv_done);
        circleOverlayView = findViewById(R.id.circle_over_layout);
        halfCircleOverlayView = findViewById(R.id.haft_circle_over_layout);
        halfCircleHigherOverlayView = findViewById(R.id.haft_circle_higher_over_layout);

        circleOverlayView.setVisibility(View.GONE);
        halfCircleOverlayView.setVisibility(View.GONE);
        halfCircleHigherOverlayView.setVisibility(View.GONE);



    }

    public HalfCircleHigherOverlayView getHalfCircleHigherOverlayView() {
        return halfCircleHigherOverlayView;
    }

    public CircleOverlayView getCircleOverlayView() {
        return circleOverlayView;
    }

    public HalfCircleOverlayView getHalfCircleOverlayView() {
        return halfCircleOverlayView;
    }

    public void setVisibilityCtlAnimation(int visibility) {
        if (ctlAnimation != null) {
            ctlAnimation.setVisibility(visibility);
        }
    }

    public AppCompatTextView getTvMoreService() {
        return tvMoreService;
    }

    public ImageView getImgArrow() {
        return imgArrow;
    }

    public FrameLayout getFlDialog() {
        return flDialog;
    }

    public TextView getTvSkip() {
        return tvSkip;
    }

    public TextView getTvUDidIt() {
        return tvUDidIt;
    }

    public TextView getTvTutor() {
        return tvTutor;
    }

    public TextView getTvTutor2() {
        return tvTutor2;
    }

    public TextView getTvDone() {
        return tvDone;
    }
}
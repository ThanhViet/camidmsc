/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.tab_home.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.home.ItemMoreHome;
import com.metfone.selfcare.module.tab_home.holder.FeatureDetailHolder;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;

public class FeatureAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    private final int TYPE_FEATURE = 1;
    private TabHomeListener.OnAdapterClick listener;

    public FeatureAdapter(Activity act, TabHomeListener.OnAdapterClick listener) {
        super(act);
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object object = getItem(position);
        if (object instanceof ItemMoreHome) {
            return TYPE_FEATURE;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_FEATURE)
            return new FeatureDetailHolder(layoutInflater.inflate(R.layout.holder_tab_home_feature_detail, parent, false), listener);
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (holder instanceof FeatureDetailHolder) {
            holder.bindData(getItem(position), position);
        } else {
            holder.bindData(getItem(position), position);
        }
    }

}

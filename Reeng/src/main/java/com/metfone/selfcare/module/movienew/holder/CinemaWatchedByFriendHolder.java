/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/3
 *
 */

package com.metfone.selfcare.module.movienew.holder;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.Movie2;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;

public class CinemaWatchedByFriendHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.img_friend_avatar)
    CircleImageView imgFriendAvatar;

    @BindView(R.id.txt_friend_name)
    TextView txtFriendName;

    @BindView(R.id.img_play)
    CircleImageView imgPlay;

    @BindView(R.id.txt_play)
    TextView txtPlay;

    @BindView(R.id.tv_title)
    CamIdTextView tvTitle;

    @BindView(R.id.img_movie)
    AppCompatImageView imgMovie;

    @BindView(R.id.card_view)
    CardView cvImage;

    private TabMovieListener.OnAdapterClick listener;
    private View rootView;
    private Activity activity;
    private String mNameWatched;
    private String mPhoneNumber;
    private TabMovieListener.IPlayVideoNow mPlayNowCallback;
    private ContactBusiness mContactBusiness;

    public CinemaWatchedByFriendHolder(View view, Activity activity, TabMovieListener.OnAdapterClick listener,
                                       String nameWatched, TabMovieListener.IPlayVideoNow iPlayVideoNow) {
        super(view);
        this.activity = activity;
        this.rootView = view;
        this.listener = listener;
        this.mNameWatched = nameWatched;
        this.mPlayNowCallback = iPlayVideoNow;
        mContactBusiness = ApplicationController.getApp().getContactBusiness();
    }

    public void setNameWatched(String nameWatched) {
        if (nameWatched != null && nameWatched.contains(" ")) {
            this.mNameWatched = nameWatched.substring(0, nameWatched.indexOf(" "));
        } else {
            this.mNameWatched = nameWatched;
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    @Override
    public void bindData(Object item, int position) {
        MoviePagerModel moviePagerModel = (MoviePagerModel) item;
        ArrayList<Object> data = moviePagerModel.getList();
        if(Utilities.notEmpty(data)) {
            if (mContactBusiness.getRecentPhoneNumber() != null &&
                    mContactBusiness.getRecentPhoneNumber().size() > 0) {
                String phoneNumber = mContactBusiness.getRecentPhoneNumber().get(0).getJidNumber();
                phoneNumber = phoneNumber.replace("+855", "0");
                imgFriendAvatar.setImageBitmap(ContactBusiness.retrieveContactPhoto(activity, phoneNumber));
            }
            tvTitle.setText(activity.getString(R.string.cinema_watched_by_friend));
            txtFriendName.setText(mNameWatched + " " + activity.getString(R.string.watched_recently));
            if (data.get(0) instanceof  HomeData) {
                HomeData homeData = (HomeData) data.get(0);
                Glide.with(activity)
                        .load(homeData.getImagePath()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                        .into(imgMovie);
                imgPlay.setOnClickListener(v -> mPlayNowCallback.onPlayNow(new Movie(homeData)));
                txtPlay.setOnClickListener(v -> mPlayNowCallback.onPlayNow(new Movie(homeData)));
            } else if (data.get(0) instanceof ListLogWatchResponse.Result){
                ListLogWatchResponse.Result result = (ListLogWatchResponse.Result) data.get(0);
                Glide.with(activity)
                        .load(result.getImagePath()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                        .into(imgMovie);
                imgPlay.setOnClickListener(v -> listener.playFilm(item, position,false));
                txtPlay.setOnClickListener(v -> listener.playFilm(item, position,false));
                cvImage.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        listener.onClickMovieItem(item, position);
                    }
                });
            }
        }
    }
}
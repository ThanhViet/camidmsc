package com.metfone.selfcare.module.netnews.MainNews.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.model.FavouriteModel;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

public class MenuCategoryAdapterV5 extends RecyclerView.Adapter<BaseViewHolder> {
    private List<CategoryModel> dataCategory;
    private Context mContext;
    AbsInterface.OnTabNewItemListener listener;

    public void setDataCategory(List<CategoryModel> dataCategory) {
        this.dataCategory = dataCategory;
        notifyDataSetChanged();
    }

    public MenuCategoryAdapterV5(List<CategoryModel> model, Context mContext, AbsInterface.OnTabNewItemListener listener) {
        this.mContext = mContext;
        this.dataCategory = model;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.holder_category_new, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final CategoryModel model = dataCategory.get(position);
        if(holder.getView(R.id.tv_content) != null){
            holder.setText(R.id.tv_content,model.getName());
            final TextView tvContent = holder.getView(R.id.tv_content);
            FavouriteModel data = SharedPrefs.getInstance().get("FAVOURITE", FavouriteModel.class);
            if(data != null && data.getData().size() > 0) {
                for (String i : data.getData()) {
                    if (i.equals(String.valueOf(model.getId()))) {
                        tvContent.setSelected(true);
                    }
                }
            }
//            if(model.isFollow()){
//                tvContent.setSelected(true);
//            }
            holder.setOnClickListener(R.id.tv_content, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(model.isFollow()){
                        tvContent.setSelected(false);
                        model.setFollow(false);
                    }else{
                        tvContent.setSelected(true);
                        model.setFollow(true);
                    }
                    listener.onItemClickFavourite(model);

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataCategory == null ? 0 : dataCategory.size();
    }


}

package com.metfone.selfcare.module.home_kh.tab.adapter.rewardtopic;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.home_kh.tab.adapter.events.OnClickHomeReward;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardDetailItem;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

import java.text.DecimalFormat;

import butterknife.BindView;

public class RewardTopicListHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_discount)
    @Nullable
    TextView tv_discount;
    @BindView(R.id.tv_date)
    @Nullable
    TextView tv_date;

    @BindView(R.id.tv_desc)
    @Nullable
    TextView tv_desc;

    @BindView(R.id.iv_cover)
    @Nullable
    AppCompatImageView iv_cover;
    @BindView(R.id.ic_avatar)
    @Nullable
    CircleImageView ic_avatar;

    @BindView(R.id.layout_root)
    @Nullable
    ViewGroup viewRoot;

    private OnClickHomeReward listener;
    private Activity activity;

    public RewardTopicListHolder(View view, Activity activity, OnClickHomeReward listener) {
        super(view);
        this.listener = listener;
        this.activity = activity;

        int widthIvCoverInDesign = 287;
        int heightIvCoverInDesign = 108;
        float ratioIvCoverInDesign = (float) heightIvCoverInDesign / widthIvCoverInDesign;
        ViewGroup.LayoutParams ivCoverLayoutParams = iv_cover.getLayoutParams();
        //The same margin in layout holder
        int marginInDp = 10;
        int ivCoverWidth = TabHomeUtils.getWidthKhHomeRewardTopic() - Utilities.convertDpToPixel(marginInDp*2);
        ivCoverLayoutParams.height = (int) (ivCoverWidth * ratioIvCoverInDesign);
        iv_cover.setLayoutParams(ivCoverLayoutParams);

        ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
        layoutParams.width = TabHomeUtils.getWidthKhHomeRewardTopic();
        viewRoot.setLayoutParams(layoutParams);
        viewRoot.requestLayout();
    }

    @Override
    public void bindData(Object item, int position) {
        KhHomeRewardDetailItem homeData = (KhHomeRewardDetailItem) item;
        if (homeData.giftObject != null) {
            String url = homeData.giftObject.avatar;
            if (homeData.giftObject.imageUrl != null) {
                Glide.with(activity)
                        .asBitmap()
                        .load(url)
                        .listener(new RequestListener<Bitmap>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                //The same margin in layout holder
                                int marginInDp = 10;
                                int ivCoverWidth = TabHomeUtils.getWidthKhHomeRewardTopic() - Utilities.convertDpToPixel(marginInDp*2);
                                int ivCoverHeight = (int) (((float) resource.getHeight() / resource.getWidth()) * ivCoverWidth);
                                Bitmap scaledBitmap = Bitmap.createScaledBitmap(resource, ivCoverWidth, ivCoverHeight, true);
                                iv_cover.setImageBitmap(scaledBitmap);
                                return true;
                            }
                        })
                        .into(iv_cover);
            }

            assert ic_avatar != null;
            if (homeData.giftObject.iconUrl != null) {
                Glide.with(activity)
                        .load(homeData.giftObject.iconUrl)
                        .into(ic_avatar);
            }

            viewRoot.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) {
                        listener.onClickRewardItem(homeData, getAdapterPosition());
                    }
                }
            });

//            ApplicationController.self().get

            tv_discount.setText(homeData.giftObject.discountRate + " " + activity.getString(R.string.kh_dicount));
            tv_date.setText(DateConvert.formatDateTime(activity, homeData.giftObject.expireDate));
            tv_desc.setText(activity.getString(R.string.kh_free));
            tv_desc.setText(homeData.giftObject.giftPoint == 0 ? ResourceUtils.getString(R.string.free) :
                    String.format(tv_desc.getContext().getString(R.string.reward_item_point), formatPoint(homeData.giftObject.giftPoint)));
        }
    }

    private String formatPoint(int point) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(point).replaceAll(",", "\\.");
    }
}

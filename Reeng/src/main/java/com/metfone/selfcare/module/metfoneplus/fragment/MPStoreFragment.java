package com.metfone.selfcare.module.metfoneplus.fragment;

import android.Manifest;
import android.graphics.Insets;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowInsets;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.module.metfoneplus.adapter.StoreAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPLocationServiceRequestDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPSupportStoreDialog;
import com.metfone.selfcare.module.metfoneplus.holder.StoreViewHolder;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsFindStoreByAddrResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetNearestStoreResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PERMISSION.PERMISSION_REQUEST_LOCATION;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPStoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPStoreFragment extends MPBaseFragment implements MPMapsFragment.OnMapListener {
    public static final String TAG = "MPStoreFragment";

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.bottom_sheet_list_store)
    RecyclerView mListStore;
    @BindView(R.id.bottom_sheet_layout_no_store)
    LinearLayout mLayoutNoStore;
    @BindView(R.id.bottom_sheet_store_list_store)
    RelativeLayout mBottomSheetStoreListStore;
    @BindView(R.id.bottom_sheet_layout_list_store)
    LinearLayout mBottomSheetLayoutListStore;

    private MPMapsFragment mMPMapsFragment;
    private StoreAdapter mStoreAdapter;
    private BottomSheetBehavior mBottomSheetBehavior;
    private boolean mIsLoadStoreFromSearchStore = false;

    private View.OnClickListener mOnItemStoreTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            StoreViewHolder storeViewHolder = (StoreViewHolder) view.getTag();
            MPSupportStoreDialog supportStoreDialog = MPSupportStoreDialog.newInstance(storeViewHolder.mStore);
            supportStoreDialog.show(getParentFragmentManager(), MPSupportStoreDialog.TAG);
        }
    };

    public MPStoreFragment() {
        // Required empty public constructor
    }


    public static MPStoreFragment newInstance() {
        MPStoreFragment fragment = new MPStoreFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_store;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mActionBarTitle.setText(getString(R.string.m_p_store_title));
        Utilities.adaptViewForInserts(mLayoutActionBar);

        //set bottom space, make it not over android navigator
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom() + Utilities.getNavigationBarHeight(mParentActivity));

        mBottomSheetBehavior = BottomSheetBehavior.from(mBottomSheetStoreListStore);
        adaptViewForInsertPeekHeight(view, mBottomSheetBehavior);
        Utilities.adaptViewForInsertBottom(mBottomSheetStoreListStore);
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.GONE);
        }

        initMap();
        mStoreAdapter = new StoreAdapter(mParentActivity, new ArrayList<Store>(), mOnItemStoreTapped);
        mListStore.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListStore.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mParentActivity, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mParentActivity, R.drawable.divider_list_store)));
        mListStore.addItemDecoration(dividerItemDecoration);
        mListStore.setAdapter(mStoreAdapter);

        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_LOCATION);
        }
    }

    private void initMap() {
        mMPMapsFragment = MPMapsFragment.newInstance();
        mMPMapsFragment.setOnTaskCompleted(this);
        replaceMPMapFragmentTransactionAllowLoss(R.id.m_p_store_layout_map, mMPMapsFragment, MPMapsFragment.TAG);
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        getCamIdUserBusiness().setStores(null);
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }
        popBackStackFragment();
    }

    @OnClick(R.id.bottom_sheet_list_store_my_location)
    void onMyLocation() {
        mIsLoadStoreFromSearchStore = false;
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_LOCATION);
        } else {
            if (!Utilities.isLocationEnabled(mParentActivity)) {
                MPLocationServiceRequestDialog dialog = MPLocationServiceRequestDialog.newInstance();
                dialog.show(getParentFragmentManager(), MPLocationServiceRequestDialog.TAG);
                return;
            }
            mMPMapsFragment.getMyLocation();
        }
    }

    @OnClick(R.id.m_p_store_search)
    void gotoStoreSearchScreen() {
        gotoMPStoreSearchFragment();
    }

    @Override
    public void onGetAddressCompleted(String result) {
        mMPMapsFragment.removeLocationCallback();
    }

    @Override
    public void onGetLocationCompleted(LatLng latLng) {
        getNearestStore(latLng.latitude, latLng.longitude);
        getCamIdUserBusiness().setStores(null);
    }

    @Override
    public void onRequestLocationPermission() {

    }

    private void getNearestStore(double latitude, double longitude) {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetNearestStore(latitude, longitude, new MPApiCallback<WsGetNearestStoreResponse>() {
            @Override
            public void onResponse(Response<WsGetNearestStoreResponse> response) {
                if (response.body() != null
                        && response.body().getResult() != null
                        && !mIsLoadStoreFromSearchStore) {
                    updateListStore(response.body().getResult().getWsResponse());
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void findStoreByAddress(String districtId, double latitude, double longitude, String provinceId) {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsFindStoreByAddr(districtId, latitude, longitude, provinceId, new MPApiCallback<WsFindStoreByAddrResponse>() {
            @Override
            public void onResponse(Response<WsFindStoreByAddrResponse> response) {
                if (response.body() != null
                        && response.body().getResult().getWsResponse() != null
                        && !mIsLoadStoreFromSearchStore) {
                    updateListStore(response.body().getResult().getWsResponse());
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            if (PermissionHelper.declinedPermission(mParentActivity.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                findStoreByAddress(null, Constants.INVALID_LAT, Constants.INVALID_LNG, Constants.PROVINCE_ID);
            }
        }
    }

    public void updateListStore(List<Store> stores) {
        if (stores == null || stores.size() == 0) {
            mStoreAdapter.replaceData(new ArrayList<Store>());
            mLayoutNoStore.setVisibility(View.VISIBLE);
            mListStore.setVisibility(View.GONE);
        } else {
            mStoreAdapter.replaceData(stores);
            mLayoutNoStore.setVisibility(View.GONE);
            mListStore.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        if (mMPMapsFragment != null) {
            mMPMapsFragment.removeLocationCallback();
            mMPMapsFragment.setOnTaskCompleted(null);
            mMPMapsFragment = null;
        }
        super.onDestroyView();
        getCamIdUserBusiness().setStores(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCamIdUserBusiness().getStores() != null && getCamIdUserBusiness().getStores().size() != 0) {
            mIsLoadStoreFromSearchStore = true;
            updateListStore(getCamIdUserBusiness().getStores());
        } else {
            mIsLoadStoreFromSearchStore = false;
        }
    }

    public void adaptViewForInsertPeekHeight(View view, BottomSheetBehavior bottomSheetBehavior) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            // Prepare original top padding of the view
            int bottomSheetOriginalPeekHeight = bottomSheetBehavior.getPeekHeight();
            view.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                @Override
                public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        // If Q, update peek height according to gesture inset bottom
                        Insets gestureInsets = windowInsets.getSystemGestureInsets();
                        bottomSheetBehavior.setPeekHeight(gestureInsets.bottom * getRatioWidthHeight());
                    } else {
                        // If not Q, update peek height according to system window inset bottom
                        bottomSheetBehavior.setPeekHeight(windowInsets.getSystemWindowInsetBottom() * getRatioWidthHeight());
                    }
                    return windowInsets;
                }
            });
        }
    }

    private int getRatioWidthHeight() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float ratio = metrics.heightPixels / metrics.widthPixels;
        if (ratio > 1.75) {
            return 3;
        } else {
            return 4;
        }
    }
}
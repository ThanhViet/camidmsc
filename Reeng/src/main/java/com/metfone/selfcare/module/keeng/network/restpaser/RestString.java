/*
 * Copyright (c) 2017.
 * www.bigzun.com
 */

package com.metfone.selfcare.module.keeng.network.restpaser;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RestString extends AbsResultData implements Serializable {

	private static final long serialVersionUID = -3274300222493809584L;

	@SerializedName("data")
	private String data = "";

	@SerializedName("short_code")
	private String shortCode = "";

	@SerializedName("command")
	private String command = "";

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public boolean isSuccess(Context context) {
		//TODO tuyet doi khong duoc sua ham nay
		if (!TextUtils.isEmpty(getData(context)) && "1".equals(getData())) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "RestString {data=" + data + "} error { " + getError() + "}";
	}

	/*
	 * Kiem tra xem co sai token ko
	 *
	 * @param context
	 *
	 * @return
	 */
	public String getData(Context context) {
		if (isWrongToken()) {
//			AutoLoginAsync.autoLoginManual(context);
		}
		return data;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
}
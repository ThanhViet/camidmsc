package com.metfone.selfcare.module.selfcare.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.selfcare.event.SCAccountEvent;
import com.metfone.selfcare.module.selfcare.model.SCPackage;
import com.metfone.selfcare.module.selfcare.model.SCVasPackageRef;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCPackageDetail;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCImageLoader;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.HyperlinkListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

public class SCPackageDetailFragment extends BaseFragment {

    private ImageView imvImage;
    private ImageView imvCover;
    private TextView tvShortDes;
    private TextView tvName;
    private TextView tvDescription;
    private TextView tvTitle;
    private TextView btnSubmit;
    private ImageView btnBack;
    private LoadingViewSC loadingView;

    private SCPackage packageData;
    private int type;
    boolean isShowDownload = false;
    String linkDownload = "";

    public static SCPackageDetailFragment newInstance(Bundle bundle) {
        SCPackageDetailFragment fragment = new SCPackageDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCPackageDetailFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_package_detail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

        loadData();

        return view;
    }

    private void loadData() {
        loadingView.loadBegin();

        Bundle bundle = getArguments();
        packageData = (SCPackage) bundle.getSerializable(Constants.KEY_DATA);
        type = bundle.getInt(SCConstants.PREFERENCE.KEY_FROM_SOURCE);
        if (packageData == null) return;
        tvTitle.setText(packageData.getName());
        tvName.setText(packageData.getName());
        tvShortDes.setText(packageData.getShortDes());
        SCImageLoader.setImage(mActivity, imvImage, packageData.getIconUrl());

        if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
            if (packageData.isRegister())
                btnSubmit.setText(mActivity.getString(R.string.sc_unsubcribe));
            else
                btnSubmit.setText(mActivity.getString(R.string.sc_subcribe));
        } else {
            btnSubmit.setText(mActivity.getString(R.string.sc_register));
        }

        final WSSCRestful restful = new WSSCRestful(mActivity);
        restful.getPackageDetail(packageData.getCode(), new Response.Listener<RestSCPackageDetail>() {
            @Override
            public void onResponse(RestSCPackageDetail result) {
//                super.onResponse(result);

                if (result != null) {
                    if (result.getStatus() == 200 && result.getData() != null) {
                        loadingView.loadFinish();
                        SCImageLoader.setImage(mActivity, imvCover, result.getData().getIconUrl());
                        if (result.getData().getVasDescriptions() != null && result.getData().getVasDescriptions().size() > 0 && !TextUtils.isEmpty(result.getData().getVasDescriptions().get(0).getDescription()))
//                            tvDescription.setText(Html.fromHtml(result.getData().getVasDescriptions().get(0).getDescription()));
                            setTextViewHTML(tvDescription, result.getData().getVasDescriptions().get(0).getDescription());

                        if (result.getData().isRegisterable()) {
//                            if (packageData.isRegister()) {
//                                btnSubmit.setVisibility(View.GONE);
//                            } else {
                            btnSubmit.setVisibility(View.VISIBLE);
//                            }
                        } else {
                            btnSubmit.setVisibility(View.GONE);
                        }

                        for (SCVasPackageRef item : result.getData().getVasPackageRefs()) {
                            if ("Android".equals(item.getOsType()) && !TextUtils.isEmpty(item.getHref())) {
                                isShowDownload = true;
                                linkDownload = item.getHref();
                                break;
                            }
                        }
                        if (isShowDownload) {
                            btnSubmit.setVisibility(View.VISIBLE);
                            btnSubmit.setText(mActivity.getString(R.string.sc_download));
                        }

                    } else if (result.getStatus() == 401 || result.getStatus() == 403) {
                        //Login lai
                        loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                    } else {
                        //Fail
                        loadingView.loadError();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (volleyError != null && volleyError.networkResponse != null) {
                    int errorCode = volleyError.networkResponse.statusCode;
                    if (errorCode == 401 || errorCode == 403) {
                        //Login lai
                        loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                    } else {
                        //Fail
                        loadingView.loadError();
                    }
                } else {
                    //Fail
                    loadingView.loadError();
                }
            }
        });
    }

    private void initView(View view) {
        imvImage = view.findViewById(R.id.imvImage);
        imvCover = view.findViewById(R.id.imvCover);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvName = view.findViewById(R.id.tvName);
        tvDescription = view.findViewById(R.id.tvDescription);
        tvShortDes = view.findViewById(R.id.tvShortDes);
        loadingView = view.findViewById(R.id.loading_view);
        btnSubmit = view.findViewById(R.id.btnSubmit);
        btnBack = view.findViewById(R.id.btnBack);

        btnSubmit.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (packageData != null) {
                    if (isShowDownload) {
                        UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(ApplicationController.self(), mActivity, linkDownload);
                    } else {
                        if (packageData.getVasDescriptions() != null && packageData.getVasDescriptions().size() > 0 && !TextUtils.isEmpty(packageData.getVasDescriptions().get(0).getPopup())) {
                            DialogConfirm dialogConfirm = new DialogConfirm(mActivity, false);
                            dialogConfirm.setLabel(mActivity.getString(R.string.confirm));
                            dialogConfirm.setMessage(packageData.isRegister() ? packageData.getVasDescriptions().get(0).getUnsPopup() : packageData.getVasDescriptions().get(0).getPopup());
                            dialogConfirm.setUseHtml(true);
                            dialogConfirm.setNegativeLabel(mActivity.getString(R.string.cancel));
                            dialogConfirm.setPositiveLabel(mActivity.getString(R.string.ok));
                            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                                @Override
                                public void onPositive(Object result) {
                                    doSubmit();
                                }
                            });
                            dialogConfirm.setHyperLinkListener(new HyperlinkListener() {
                                @Override
                                public void onClickHyperLink(String link) {
                                    UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(ApplicationController.self(), mActivity, link);
                                }
                            });
                            dialogConfirm.setButtonTextColor(mActivity.getResources().getColor(R.color.sc_primary));
                            if (!dialogConfirm.isShowing())
                                dialogConfirm.show();
                        } else {
                            doSubmit();
                        }
                    }
                }

            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        loadingView.setLoadingErrorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login lai
                /*Intent intent = new Intent(getActivity(), SCAccountActivity.class);
                mActivity.startActivity(intent);*/
            }
        });
    }

    private void doSubmit() {
        if (packageData != null) {
            int actionType = 0;
            if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                actionType = packageData.isRegister() ? 1 : 0;
//                if(actionType == 1)
//                {
//                    ToastUtils.makeText(mActivity, mActivity.getString(R.string.e666_not_support_function));
//                    return;
//                }
            }
            btnSubmit.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_gray));
            btnSubmit.setText(mActivity.getString(R.string.sc_processcing));

            WSSCRestful restful = new WSSCRestful(mActivity);
            restful.registerPackage(packageData.getCode(), actionType, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int code = jsonObject.optInt("errorCode", -1);
                        String message = jsonObject.optString("message");
                        mActivity.showToast(message);

                        if (code == 200) {
                            EventBus.getDefault().postSticky(new SCAccountEvent(SCAccountEvent.UPDATE_INFO));
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnSubmit.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
                                    if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                                        packageData.setRegister(!packageData.isRegister());
                                        if (packageData.isRegister())
                                            btnSubmit.setText(mActivity.getString(R.string.sc_unsubcribe));
                                        else
                                            btnSubmit.setText(mActivity.getString(R.string.sc_subcribe));
//                                        btnSubmit.setText(mActivity.getString(R.string.sc_unsubcribe));
                                    } else {
                                        btnSubmit.setText(mActivity.getString(R.string.sc_buy_again));
                                    }
                                }
                            });
                        } else {
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnSubmit.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
                                    if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                                        if (packageData.isRegister())
                                            btnSubmit.setText(mActivity.getString(R.string.sc_unsubcribe));
                                        else
                                            btnSubmit.setText(mActivity.getString(R.string.sc_subcribe));
                                    } else {
                                        btnSubmit.setText(mActivity.getString(R.string.sc_register));
                                    }
                                }
                            });
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnSubmit.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
                                if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                                    if (packageData.isRegister())
                                        btnSubmit.setText(mActivity.getString(R.string.sc_unsubcribe));
                                    else
                                        btnSubmit.setText(mActivity.getString(R.string.sc_subcribe));
                                } else {
                                    btnSubmit.setText(mActivity.getString(R.string.sc_register));
                                }
                            }
                        });
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mActivity.showToast(volleyError.getMessage());
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnSubmit.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
                            if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                                if (packageData.isRegister())
                                    btnSubmit.setText(mActivity.getString(R.string.sc_unsubcribe));
                                else
                                    btnSubmit.setText(mActivity.getString(R.string.sc_subcribe));
                            } else {
                                btnSubmit.setText(mActivity.getString(R.string.sc_register));
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
//        App.getInstance().cancelPendingRequests(WSSCRestful.GET_PACKAGE_DETAIL);
        super.onDestroyView();
    }

    public void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                UrlConfigHelper.getInstance(view.getContext()).gotoWebViewOnMedia(ApplicationController.self(), mActivity, span.getURL());
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }
}

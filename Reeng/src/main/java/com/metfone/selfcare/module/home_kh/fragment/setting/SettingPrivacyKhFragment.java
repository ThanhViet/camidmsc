package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.widget.SwitchButton;

public class SettingPrivacyKhFragment extends BaseSettingKhFragment implements View.OnClickListener {
    private static final String TAG = SettingPrivacyKhFragment.class.getSimpleName();
    private ReengAccountBusiness mAccountBusiness;
    private SettingBusiness mSettingBusiness;
    private SettingKhFragment.OnFragmentSettingListener mListener;
    private TextView mTvwLockAppDes;
    private ConstraintLayout mViewLockApp, mViewSendSeen, mViewShowBirthDay, mViewBlockList, mViewRemindBirthday,
            mViewWatchingCinemaActivity;
    private SwitchButton mToggleSendSeen, mToggleShowBirthDay, mToggleRemindFriendBirthday,
            mToggleWatchingCinemaActivity;

    public static SettingPrivacyKhFragment newInstance() {
        return new SettingPrivacyKhFragment();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        try {
            mListener = (SettingKhFragment.OnFragmentSettingListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }

    @Override
    public String getName() {
        return "SettingPrivacyKhFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_privacy_setting_kh;
    }

    @Override
    protected void initView(View view) {
        findComponentViews(view);
        setViewListener();
        setTitle(R.string.title_privacy);
    }

    private void findComponentViews(View rootView) {
//        initActionbar(inflater);
        mViewLockApp = rootView.findViewById(R.id.setting_lock_app);
        mViewSendSeen = rootView.findViewById(R.id.setting_enable_seen);
        mViewShowBirthDay = rootView.findViewById(R.id.setting_show_birthday);
        mViewRemindBirthday = rootView.findViewById(R.id.setting_remind_friend_birthday);
        mViewWatchingCinemaActivity = rootView.findViewById(R.id.setting_watching_cinema_activity);
        mViewBlockList = rootView.findViewById(R.id.setting_block_list);
        mToggleSendSeen = rootView.findViewById(R.id.setting_enable_seen_toggle);
        mToggleShowBirthDay = rootView.findViewById(R.id.setting_show_birthday_toggle);
        mToggleRemindFriendBirthday = rootView.findViewById(R.id.setting_remind_friend_birthday_toggle);
        mToggleWatchingCinemaActivity = rootView.findViewById(R.id.setting_watching_cinema_activity_toggle);
        mTvwLockAppDes = rootView.findViewById(R.id.setting_lock_app_tv);
        drawDetail();
    }

//    private void initActionbar(LayoutInflater inflater) {
//        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
//        View mViewActionBar = mParentActivity.getToolBarView();
//        mTvwTitle = (EllipsisTextView) mViewActionBar.findViewById(R.id.ab_title);
//        mImgBack = (ImageView) mViewActionBar.findViewById(R.id.ab_back_btn);
//        ImageView mImgOption = (ImageView) mViewActionBar.findViewById(R.id.ab_more_btn);
//        mImgOption.setVisibility(View.GONE);
//        mTvwTitle.setText(mRes.getString(R.string.setting_private));
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_lock_app:
                mListener.navigateToLockApp();
                break;
            case R.id.setting_enable_seen:
                mToggleSendSeen.setChecked(!mToggleSendSeen.isChecked());
//                processSettingSendSeen();
                break;
            case R.id.setting_show_birthday:
//            case R.id.setting_show_birthday_toggle:
                mToggleShowBirthDay.setChecked(!mToggleShowBirthDay.isChecked());
//                processShowBirthDay();
                break;
            case R.id.setting_block_list:
//                mListener.navigateToBlockList();
                break;
            case R.id.setting_remind_friend_birthday:
                mToggleRemindFriendBirthday.setChecked(!mToggleRemindFriendBirthday.isChecked());
//                processRemindFriendBirthday();
                break;
            case R.id.setting_watching_cinema_activity:
                mToggleWatchingCinemaActivity.setChecked(!mToggleWatchingCinemaActivity.isChecked());
                break;
        }
    }

    private void setViewListener() {
//        mImgBack.setOnClickListener(this);
        mViewLockApp.setOnClickListener(this);
        mViewSendSeen.setOnClickListener(this);
        mViewShowBirthDay.setOnClickListener(this);
        mViewRemindBirthday.setOnClickListener(this);
        mViewBlockList.setOnClickListener(this);
        mViewWatchingCinemaActivity.setOnClickListener(this);

//        mToggleSendSeen.setOnClickListener(this);
//        mToggleShowBirthDay.setOnClickListener(this);
//        mToggleRemindFriendBirthday.setOnClickListener(this);

        mToggleSendSeen.setOnCheckedChangeListener((view, isChecked) -> processSettingSendSeen());
        mToggleShowBirthDay.setOnCheckedChangeListener((view, isChecked) -> processShowBirthDay());
        mToggleRemindFriendBirthday.setOnCheckedChangeListener((view, isChecked) -> processRemindFriendBirthday());
        mToggleWatchingCinemaActivity.setOnCheckedChangeListener(((view, isChecked) -> processWatchingCinemaActivity()));
    }

    private void drawDetail() {
        mToggleSendSeen.setChecked(mSettingBusiness.getPrefEnableSeen());
        mToggleShowBirthDay.setChecked(mAccountBusiness.isPermissionShowBirthday());
        mToggleRemindFriendBirthday.setChecked(mSettingBusiness.getPrefBirthdayReminder());
        mTvwLockAppDes.setText(getString(isSupportFingerPrintAuth() ? R.string.lock_app_title_desc_with_fingerprint : R.string.lock_app_title_desc_new));

        // TODO:  [START] Cambodia version
        mToggleWatchingCinemaActivity.setChecked(mSettingBusiness.getPrefEnableWatchingCinemaActivity());
        // TODO:  [END] Cambodia version
    }

    private boolean isSupportFingerPrintAuth() {
        try {
            if (!Config.Features.FLAG_SUPPORT_FINGERPRINT_AUTH || getActivity() == null) {
                return false;
            }
            FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(getActivity());
            return fingerprintManager.isHardwareDetected();
        } catch (Exception e) {
            return false;
        }
    }

    private void processShowBirthDay() {
        final boolean state = mToggleShowBirthDay.isChecked();
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
            mToggleShowBirthDay.setOnCheckedChangeListener(null);
            mToggleShowBirthDay.setChecked(!state);
            mToggleShowBirthDay.setOnCheckedChangeListener((view, isChecked) -> processShowBirthDay());
            return;
        }
        mParentActivity.showLoadingDialog("", R.string.waiting);
        ProfileRequestHelper.onResponseSettingPermissionListener listener = new ProfileRequestHelper.onResponseSettingPermissionListener() {
            @Override
            public void onResponse() {
                mParentActivity.showToast(R.string.change_setting_success);
                mParentActivity.hideLoadingDialog();
                mAccountBusiness.updatePermissionBirthday(state);
//                mToggleShowBirthDay.setChecked(state);
            }

            @Override
            public void onError(int errorCode) {
                mParentActivity.hideLoadingDialog();
                Log.d(TAG, "onError: " + errorCode);
                mParentActivity.showToast(getString(R.string.e601_error_but_undefined), Toast.LENGTH_SHORT);
                if (isAdded()) {
                    mToggleShowBirthDay.setOnCheckedChangeListener(null);
                    mToggleShowBirthDay.setChecked(!state);
                    mToggleShowBirthDay.setOnCheckedChangeListener((view, isChecked) -> processShowBirthDay());
                }
            }
        };
        ProfileRequestHelper.getInstance(mApplication).setPermission(mAccountBusiness.getCurrentAccount(), state ? 1 : 0, listener);
    }

    private void processWatchingCinemaActivity() {
//        final boolean state = mToggleWatchingCinemaActivity.isChecked();
//        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
//            mParentActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
//            mToggleWatchingCinemaActivity.setOnCheckedChangeListener(null);
//            mToggleWatchingCinemaActivity.setChecked(!state);
//            mToggleWatchingCinemaActivity.setOnCheckedChangeListener((view, isChecked) -> processWatchingCinemaActivity());
//            return;
//        }
//        mParentActivity.showLoadingDialog("", R.string.waiting);
        // TODO: [START] Cambodia version
        boolean state = mToggleWatchingCinemaActivity.isChecked();
        mSettingBusiness.setPrefEnableWatchingCinemaActivity(state);
        // TODO: [END] Cambodia version
    }

    private void processRemindFriendBirthday() {
        final boolean state = mToggleRemindFriendBirthday.isChecked();
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
            mToggleRemindFriendBirthday.setOnCheckedChangeListener(null);
            mToggleRemindFriendBirthday.setChecked(!state);
            mToggleRemindFriendBirthday.setOnCheckedChangeListener((view, isChecked) -> processRemindFriendBirthday());
            return;
        }
        mParentActivity.showLoadingDialog("", R.string.waiting);
        ProfileRequestHelper.onResponseSettingPermissionListener listener = new ProfileRequestHelper.onResponseSettingPermissionListener() {
            @Override
            public void onResponse() {
                mParentActivity.showToast(R.string.change_setting_success);
                mParentActivity.hideLoadingDialog();
                mSettingBusiness.setPrefBirthdayReminder(state);
//                mToggleRemindFriendBirthday.setOnCheckedChangeListener(null);
//                mToggleRemindFriendBirthday.setChecked(!mToggleRemindFriendBirthday.isChecked());
//                mToggleRemindFriendBirthday.setOnCheckedChangeListener((view, isChecked) -> processRemindFriendBirthday());

            }

            @Override
            public void onError(int errorCode) {
                mParentActivity.hideLoadingDialog();
                Log.d(TAG, "onError: " + errorCode);
                mParentActivity.showToast(getString(R.string.e601_error_but_undefined), Toast.LENGTH_SHORT);
                if (isAdded()) {
                    mToggleRemindFriendBirthday.setOnCheckedChangeListener(null);
                    mToggleRemindFriendBirthday.setChecked(!state);
                    mToggleRemindFriendBirthday.setOnCheckedChangeListener((view, isChecked) -> processRemindFriendBirthday());
                }
            }
        };
        ProfileRequestHelper.getInstance(mApplication).setRemindFriendBirthday(mAccountBusiness.getCurrentAccount(), state ? 1 : 0, listener);
    }

    private void processSettingSendSeen() {
        boolean state = mToggleSendSeen.isChecked();
        mSettingBusiness.setPrefEnableSeen(state);
//        mToggleSendSeen.setChecked(!mToggleSendSeen.isChecked());
    }

    public interface OnFragmentSettingListener {
        void navigateToBlockList();

        void navigateToLockApp();
    }
}
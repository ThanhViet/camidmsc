package com.metfone.selfcare.module.home_kh.fragment.menu;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountPointRankResponse;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.RankDefineResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.base.BaseDialogKhFragment;
import com.metfone.selfcare.module.home_kh.dialog.KhDialogConfirm;
import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointContainerFragment;
import com.metfone.selfcare.module.home_kh.fragment.reward.RewardCamIdKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.setting.BaseSettingKhFragment;
import com.metfone.selfcare.module.home_kh.fragment.setting.ChangeLanguageKhDialogFragment;
import com.metfone.selfcare.module.home_kh.fragment.setting.SettingKhFragment;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.KHAccountPoint;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.model.RankDefine;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.movienew.common.Constant;
import com.metfone.selfcare.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class MenuKhFragment extends BaseSettingKhFragment {
    private ReengAccountBusiness mAccountBusiness;
    private ContactBusiness mContactBusiness;
    private Resources mRes;
    private SettingKhFragment.OnFragmentSettingListener mListener;

    @BindView(R.id.point_label)
    AppCompatTextView pointLabel;

    @BindView(R.id.point_value)
    AppCompatTextView pointValue;

    @BindView(R.id.point)
    TextView point;

    @BindView(R.id.point_expired)
    AppCompatTextView pointExpired;

    @BindView(R.id.ic_account_rank)
    AppCompatImageView icAccountRank;

    @BindView(R.id.chart)
    ExtendedPieChart chart;

    protected ApplicationController app;

    private ConstraintLayout layoutTop;
    private LinearLayoutCompat viewBarCode;
    private LinearLayoutCompat viewNearbyFriends;
    private ConstraintLayout viewInfo;
    private LinearLayoutCompat viewShareMore;
    private LinearLayoutCompat viewLogout;
    private LinearLayoutCompat viewSetting;
    private LinearLayoutCompat viewLanguage;
    private View viewSpaceTop;

    private KhHomeClient homeKhClient;
    private AccountRankDTO accountRankDTO;

    private int totalAccumulate;
    private int totalAvailability;
    private List<RankDefine> listRank = new ArrayList<>();
    private List<KHAccountPoint> listAccumulate = new ArrayList<>();
    private List<KHAccountPoint> listAvailability = new ArrayList<>();
    private RankDefine nextRank;

    public static MenuKhFragment newInstance() {
        return new MenuKhFragment();
    }

    private static final int[] COLORS = {ResourceUtils.getColor(R.color.color_sms_out), ResourceUtils.getColor(R.color.color_data), ResourceUtils.getColor(R.color.color_call_out)};

    @Override
    protected void initView(View view) {
        setTitle(R.string.kh_menu_more_title_screen);
        pointValue.setText("0");
        point.setText(getString(R.string.point));
        getListRank();


        chart.setHoleColor(Color.TRANSPARENT);
        chart.setDrawCenterText(false);
        chart.setDrawSlicesUnderHole(false);
        chart.setDrawEntryLabels(false);
        chart.setDrawMarkers(false);
        chart.setDrawRoundedSlices(false);
        chart.setDescription(null);
        chart.getLegend().setEnabled(false);
        chart.setRotationEnabled(false);
        chart.setHoleRadius(80);


        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(20, "", "20"));
        entries.add(new PieEntry(200, "", "200"));
        entries.add(new PieEntry(50, "", "50"));
//        if (source.getTotal() == 0) {
//            entries.add(new PieEntry(1, "", ""));
//        } else {
//            entries.add(new PieEntry(source.getTotal() - source.getCurrent(), "", source));
//        }
        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setColors(COLORS);
        dataSet.setDrawValues(false);
        dataSet.setSelectionShift(0);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(15f);
        data.setValueTextColor(Color.WHITE);
//        data.setValueTypeface(tfLight);

        chart.setData(data);

        // undo all highlights
//        chart.highlightValues(null);

        chart.invalidate();


        layoutTop = view.findViewById(R.id.layout_top);
        viewBarCode = view.findViewById(R.id.btn_qrcode);
        viewNearbyFriends = view.findViewById(R.id.btn_nearby_friend);
        viewInfo = view.findViewById(R.id.layout_info);
        viewShareMore = view.findViewById(R.id.btn_share_more);
        viewSetting = view.findViewById(R.id.btn_setting);
        viewLanguage = view.findViewById(R.id.btn_language);
        viewLogout = view.findViewById(R.id.btn_logout);
        viewSpaceTop = view.findViewById(R.id.viewSpaceTop);

        onShowViewLogged();

        // TODO: comment out
        viewInfo.setVisibility(View.GONE);
    }

    private void onShowViewLogged() {
        if (isLogin() != LoginVia.NOT) {
            layoutTop.setVisibility(View.VISIBLE);
            viewBarCode.setVisibility(View.VISIBLE);
            viewNearbyFriends.setVisibility(View.VISIBLE);
            viewInfo.setVisibility(View.VISIBLE);
            viewShareMore.setVisibility(View.VISIBLE);
            viewSetting.setVisibility(View.VISIBLE);
            viewLanguage.setVisibility(View.VISIBLE);
            viewLogout.setVisibility(View.VISIBLE);
            viewSpaceTop.setVisibility(View.GONE);
        } else {
            layoutTop.setVisibility(View.GONE);
            viewBarCode.setVisibility(View.GONE);
            viewNearbyFriends.setVisibility(View.GONE);
            viewInfo.setVisibility(View.GONE);
            viewShareMore.setVisibility(View.GONE);
            viewSetting.setVisibility(View.VISIBLE);
            viewLanguage.setVisibility(View.VISIBLE);
            viewLogout.setVisibility(View.GONE);
            viewSpaceTop.setVisibility(View.VISIBLE);
        }

    }

    public LoginVia isLogin() {
        String mochaToken = ApplicationController.self().getReengAccountBusiness().getToken();
        String openIDToken = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_ACCESS_TOKEN, String.class);
        if (mochaToken == null) mochaToken = "";
        if (openIDToken == null) openIDToken = "";

        if (mochaToken.isEmpty() && openIDToken.isEmpty()) {
            return LoginVia.NOT;
        } else if (mochaToken.isEmpty()) {
            return LoginVia.OPEN_ID;
        } else {
            return LoginVia.MOCHA_OPEN_ID;
        }
    }

    public enum LoginVia {
        MOCHA, OPEN_ID, MOCHA_OPEN_ID, NOT
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mContactBusiness = mApplication.getContactBusiness();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mRes = mParentActivity.getResources();
        app = (ApplicationController) mParentActivity.getApplication();
        try {
            mListener = (SettingKhFragment.OnFragmentSettingListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }

    @Override
    public String getName() {
        return "MenuKhFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_kh_menu;
    }

    @OnClick({R.id.btn_qrcode, R.id.btn_nearby_friend, R.id.btn_share_more, R.id.btn_setting, R.id.btn_language, R.id.btn_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.icBackToolbar:
                mParentActivity.onBackPressed();
            case R.id.btn_qrcode:
//                NavigateActivityHelper.navigateToQRScan(mActivity);
                NavigateActivityHelper.navigateToQRScanKh(mActivity);
                break;
            case R.id.btn_nearby_friend:
                NavigateActivityHelper.navigateToSettingKhActivity(mActivity, Constants.SETTINGS.NEAR_YOU);
                break;
            case R.id.btn_share_more:
                NavigateActivityHelper.navigateToSettingKhActivity(mActivity, Constants.SETTINGS.SHARE_AND_GET_MORE_SCREEN);
                break;
            case R.id.btn_setting:
                NavigateActivityHelper.navigateToSettingKhActivity(mActivity, Constants.SETTINGS.SETTING_KH_SCREEN);
                break;
            case R.id.btn_language:
                ChangeLanguageKhDialogFragment languageFragment = ChangeLanguageKhDialogFragment.newInstance();
                languageFragment.show(getChildFragmentManager(), "");
                break;
            case R.id.btn_logout:
                showDeactiveAccountPopup();
                break;
        }
    }

    public void showDeactiveAccountPopup() {
//        Resources res = acitivity.getResources();
//        new DialogConfirm(acitivity, true).setLabel(res.getString(R.string.logout)).setMessage(res.getString(R
//                .string.logout_body)).
//                setNegativeLabel(res.getString(R.string.bk_backup)).setPositiveLabel(res.getString(R.string.logout)).
//                setPositiveListener(new PositiveListener<Object>() {
//                    @Override
//                    public void onPositive(Object result) {
//                        if (mListener != null)
//                            mListener.deactiveAccount();
//                    }
//                })
//                .setNegativeListener(new NegativeListener() {
//                    @Override
//                    public void onNegative(Object result) {
//                        DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, "mocha://backup");
//                    }
//                }).show();

        KhDialogConfirm dialogConfirm =
                KhDialogConfirm.newInstance(getString(R.string.logout),
                        getString(R.string.logout_body)
                        , KhDialogConfirm.CONFIRM_TYPE, R.string.bk_backup, R.string.logout);
        dialogConfirm.setSelectListener(new BaseDialogKhFragment.DialogListener() {
            @Override
            public void dialogRightClick(int value) {
                if (mListener != null)
                    mListener.deactiveAccount();
            }

            @Override
            public void dialogLeftClick() {
                DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, "mocha://backup_kh");
            }
        });
        dialogConfirm.setDialogListener(new BaseDialogKhFragment.IDialogListener() {
            @Override
            public void dialogDismiss() {
                dialogConfirm.dismissAllowingStateLoss();
            }

            @Override
            public void dialogCancel() {

            }
        });
        dialogConfirm.show(getChildFragmentManager(), "");
    }

    /**
     * get list point
     */
    private void getListRank() {
        if (listRank.isEmpty()) {
            if (homeKhClient == null) {
                homeKhClient = new KhHomeClient();
            }
            homeKhClient.wsGetRankDefineInfo(new KhApiCallback<KHBaseResponse<RankDefineResponse>>() {
                @Override
                public void onSuccess(KHBaseResponse<RankDefineResponse> body) {
                    listRank.clear();
                    listRank.addAll(body.getData().getListRank());
                    getRankAccount();
                    logT("wsGetRankDefineInfo > onSuccess");
                }

                @Override
                public void onFailed(String status, String message) {
                    if (getActivity() != null)
                        com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                    Log.e("wsGetRankDefineInfo", status + " " + message);
                }

                @Override
                public void onFailure(Call<KHBaseResponse<KHBaseResponse<RankDefineResponse>>> call, Throwable t) {
                    t.printStackTrace();
                    logT("wsGetRankDefineInfo > onFailure");
                }
            });
        } else {
            logT("getListRank > getRankAccount =============>");
            getRankAccount();
        }

    }

    /**
     * get info list point of acc
     */
    private void wsGetAccountPointInfo() {

        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountPointInfo(new KhApiCallback<KHBaseResponse<AccountPointRankResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountPointRankResponse> body) {
                logT("wsGetAccountPointInfo > onSuccess =============>");
                subPoint(body.getData().getListPoint());
            }

            @Override
            public void onFailed(String status, String message) {
                logT("wsGetAccountPointInfo > onFailed =============>");
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logT("wsGetAccountPointInfo > onFailed =============>" + status + " " + message);
                Log.e("wsGetAccountPointInfo", status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountPointRankResponse>>> call, Throwable t) {
                t.printStackTrace();
                logT("wsGetAccountPointInfo > onFailure =============>");
            }
        });
    }

    private void subPoint(List<KHAccountPoint> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        ArrayList<KHAccountPoint> pointsPast = new ArrayList<>();
        totalAccumulate = 0;
        totalAvailability = 0;

        listAvailability.clear();
        listAccumulate.clear();
        KHAccountPoint minExpired = list.get(0);
        for (KHAccountPoint item : list) {
            minExpired = item.getExpireDate() < minExpired.getExpireDate() ? item : minExpired;
            if (item.getPointType() == PointType.ACTIVE.id) {
                totalAccumulate = totalAccumulate + item.getPointValue();
                listAccumulate.add(item);
            } else if (item.getPointType() == PointType.PAST.id) {
                totalAvailability = totalAvailability + item.getPointValue();
                listAvailability.add(item);
                pointsPast.add(item);
            }
        }
        pointExpired.setText(subPointsExpired(pointsPast));
        // show percent up rank level
        /*if (nextRank != null && nextRank.getRankId() == KhUserRank.RewardRank.PLATINUM.id) {
            tvPercent.setText(String.format(tvPercent.getContext().getResources().getString(R.string.percent_gold), 100, nextRank.getRankName()));
            goldPb.setProgress(100);
        } else if (nextRank != null && nextRank.getMinPoint() >= 0) {
            long minPoint = nextRank.getMinPoint();
            int percent = (int) (totalAccumulate * 100 / minPoint);
            tvPercent.setText(String.format(tvPercent.getContext().getResources().getString(R.string.percent_gold), percent, nextRank.getRankName()));
            goldPb.setProgress(percent);
        }*/
        pointValue.setText(formatPoint(totalAvailability));
        if(totalAvailability > 1){
            point.setText(getString(R.string.points));
        }else{
            point.setText(getString(R.string.point));
        }
    }

    public String subPointsExpired(ArrayList<KHAccountPoint> points) {
        try {
            int totalPoint = 0;
            AtomicBoolean sortResult = new AtomicBoolean(true);
            SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DateConvert.DATE_REWARD_INPUT);
            SimpleDateFormat dayFormat = new SimpleDateFormat(DateConvert.DATE_REWARD_OUTPUT_EXPIRED);
            // sap xep diem theo ngay het han tang dan
            Collections.sort(points, (point1, point2) -> {
                try {
                    Date date1 = dateTimeFormat.parse(point1.getPointExpireDate());
                    Date date2 = dateTimeFormat.parse(point2.getPointExpireDate());
                    return date1.before(date2) ? -1 : 1;
                } catch (ParseException e) {
                    e.printStackTrace();
                    sortResult.set(false);
                }
                return 0;
            });
            // Exception khi sort do sai dinh dang
            if (!sortResult.get()) {
                return "";
            }
            Date currentDate = new Date();
            Date minDate = null;
            // tim ngay het han gan nhat tinh tu ngay hien tai
            for (KHAccountPoint point : points) {
                Date date = dateTimeFormat.parse(point.getPointExpireDate());
                if (date.after(currentDate)) {
                    minDate = date;
                    break;
                }
            }
            // tinh tong diem co ngay het han la ngay gan nhat
            for (KHAccountPoint point : points) {
                Date date = dateTimeFormat.parse(point.getPointExpireDate());
                if (dayFormat.format(minDate).equals(dayFormat.format(date))) {
                    totalPoint += point.getPointValue();
                }
            }
            SimpleDateFormat resultFormat =  new SimpleDateFormat(DateConvert.DATE_REWARD_OUTPUT_EXPIRED);
            if(LocaleManager.getLanguage(getContext()).equals(Constant.LANGUAGE_KM)){
                resultFormat = new SimpleDateFormat(DateConvert.DATE_REWARD_OUTPUT_EXPIRED_KM);
            }
            return String.format(getActivity().getResources().getString(R.string.points_expired)
                    , formatPoint(totalPoint), resultFormat.format(minDate));
        } catch (Exception e) {
            Log.d(TAG, "subPointsExpired error : " + e.toString());
        }
        return "";
    }

    /**
     * get info rank of acc
     */
    private void getRankAccount() {

        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                logT("wsGetAccountRankInfo > onSuccess");
                initRankInfo(body.getData().getAccountRankDTO());
            }

            @Override
            public void onFailed(String status, String message) {
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                t.printStackTrace();
                logT("wsGetAccountRankInfo > onFailure");
            }
        });
    }

    private void logT(String m) {
        Log.e("TVV-Menu", m);
    }

    private void initRankInfo(AccountRankDTO account) {
        accountRankDTO = account;
        if (account != null) {
//            tvRankName.setText(account.rankName);
            int rankID = account.rankId;
            KhUserRank.RewardRank myRank = KhUserRank.RewardRank.getById(rankID);
            if (myRank != null) {
                icAccountRank.setImageDrawable(myRank.drawable);
                /*if (rankID == KhUserRank.RewardRank.PLATINUM.id) {
                    icNextRank.setImageDrawable(myRank.drawable);
                } else {
                    KhUserRank.RewardRank nextRank = KhUserRank.RewardRank.getById(rankID + 1);
                    if (nextRank != null)
                        icNextRank.setImageDrawable(nextRank.drawable);
                }*/
            }


        }
//        getPointNextRank();
        wsGetAccountPointInfo();
    }

    @OnClick(R.id.layout_top)
    public void showHistory() {
        Intent intent = new Intent(getActivity(),RewardCamIdActivity.class);
        startActivity(intent);
    }
}

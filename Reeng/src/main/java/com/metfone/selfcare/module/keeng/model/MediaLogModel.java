package com.metfone.selfcare.module.keeng.model;

import com.metfone.selfcare.helper.Constants;

import java.io.Serializable;

/**
 * Created by namnh40 on 7/18/2017.
 */

public class MediaLogModel implements Serializable {

    public static final int SRC_RELATE = 0; // relate, other ...
    public static final int SRC_OFFLINE = 1; //offline download from keeng
    public static final int SRC_SEARCH = 2; //search
    public static final int SRC_RANK = 3; //rank detail
    public static final int SRC_MIX = 4; // play random
    public static final int SRC_SINGER = 5; //singer detail
    public static final int SRC_FEED = 6; //feed on tab connection, feed profile
    public static final int SRC_ALBUM = 8; //album detail
    public static final int SRC_PLAYLIST = 9; // playlist detail
    public static final int SRC_TOPIC = 11; //topic detail
    public static final int SRC_HOT_HOME = 12; //home music
    public static final int SRC_FAVORITE = 13; //favorite profile
    public static final int SRC_NOTIFICATION = 14; //click push
    public static final int SRC_TOP_HIT = 15; //top hit detail
    public static final int SRC_FLASH_HOT = 16; //flash hot music
    public static final int SRC_CATEGORY = 17; //category detail
    public static final int SRC_EDM_HOME = 18; //home edm
    public static final int SRC_EDM_FLASH = 19; //flash hot edm
    public static final int SRC_INBOX = 20; // inbox in profile
    public static final int SRC_UPLOAD = 21; // upload in profile
    public static final int SRC_SONG_LIST = 22; // song hot list
    public static final int SRC_VIDEO_LIST = 23; // video hot list
    public static final int SRC_RADIO_STATION_LIST = 24; // radio station list
    public static final int SRC_EDM_SONG_LIST = 25; // edm song hot list
    public static final int SRC_EDM_VIDEO_LIST = 26; // edm video hot list
    public static final int SRC_HISTORY = 27; // history profile
    public static final int SRC_YOUTUBE_LIST = 28; // video youtube list
    public static final int SRC_OTHER_APP = 29; //click from other app
    public static final int SRC_DEEP_LINK = 30; //click from deep link
    public static final int SRC_MOVIES_FLASH = 31; //click from movie flash
    public static final int SRC_KEENG_AWARD = 32; //click from award

    public static final int ACT_IGNORE = 1;
    public static final int ACT_FINISH = 2;
    public static final int ACT_REPEAT = 3;
    private static final long serialVersionUID = -608513355636646718L;
    /*
    •	id : ID của bài hát trước
    •	item_type : 3 – Video ; 1 – MP3.
    •	pc : số giây đã nghe bài đó.
    •	time_play : số giây mà bài hát được phát từ lúc click play
    •	action : 1 – bỏ qua; 2 – nghe hết ; 3 – Repeat
    •	location : 1 – Bài hát nghe Offline (đã tải của KEENG);
    2 – nghe từ mục search. 3 – Top list; 4 – Nghe ngẫu nhiên;
    5 – Nghe từ trang Ca Sỹ; 6 – Nghe từ bạn bè chia sẻ;
    7 – Nghe từ Playlist của khách hàng.
    * */

    long id = 0;
    int item_type = 0;
    int pc = 0;
    long time_play = 0;
    int action = 0;
    int location = 0;
    int duration = 0;
    int isSeek = 0;
    String device_id = "";
    String buffer = "";
    int client_type = Constants.CLIENT_ANDROID;
    String state = "";
    String time = "";

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTimePlay(long time_play) {
        this.time_play = time_play;
    }

    public void setDeviceId(String device_id) {
        this.device_id = device_id;
    }

    public void setSeek(boolean isSeek) {
        this.isSeek = isSeek ? 1 : 0;
    }

    public void setSeek(int isSeek) {
        this.isSeek = isSeek;
    }

    public int getItemType() {
        return item_type;
    }

    public void setItemType(int item_type) {
        this.item_type = item_type;
    }

    public void setPercentListen(int percentListen) {
        this.pc = percentListen;
    }

    public void setPc(int pc) {
        this.pc = pc;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setBuffer(String buffer) {
        this.buffer = buffer;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}


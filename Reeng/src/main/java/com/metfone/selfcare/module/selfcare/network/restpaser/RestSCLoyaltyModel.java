package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCLoyaltyModel;

import java.io.Serializable;

public class RestSCLoyaltyModel extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private SCLoyaltyModel data;

    public SCLoyaltyModel getData() {
        return data;
    }

    public void setData(SCLoyaltyModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCLoyaltyModel [data=" + data + "] errror " + getErrorCode();
    }
}

package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

public class SectionListDataAdapter<T> extends BaseAdapter<BaseViewHolder> {

    protected AbsInterface.OnItemListener onClick;
    private List<T> itemsList;
    private boolean canViewAll = false;
    private String tag;

    public SectionListDataAdapter(Context context, List<T> itemsList, AbsInterface.OnItemListener onClick) {
        super(context);
        this.itemsList = itemsList;
        this.onClick = onClick;
    }

    public SectionListDataAdapter(Context context, List<T> itemsList) {
        super(context);
        this.itemsList = itemsList;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isCanViewAll() {
        return canViewAll;
    }

    public void setCanViewAll(boolean canViewAll) {
        this.canViewAll = canViewAll;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_child_album_home, null);
        return new BaseViewHolder(view);
    }

    public T getItem(int position) {
        return (null != itemsList && itemsList.size() > position ? itemsList.get(position) : null);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        if (canViewAll && itemsList != null && itemsList.size() >= Constants.NO_MIN_ITEMS)
            return itemsList.size() + 1;
        return (null != itemsList ? itemsList.size() : 0);
    }

    public List<T> getItemsList() {
        return itemsList;
    }
}
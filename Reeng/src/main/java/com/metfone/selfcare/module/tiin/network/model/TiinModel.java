package com.metfone.selfcare.module.tiin.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.Filterable;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TiinModel implements Serializable, Filterable {
    private final static long serialVersionUID = -7706989315033118800L;
    @SerializedName("ID")
    @Expose
    private int id;
    @SerializedName("Cid")
    @Expose
    private int cid;
    @SerializedName("Pid")
    @Expose
    private int pid;
    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("Image169")
    @Expose
    private String image169;
    @SerializedName("Content")
    @Expose
    private String content;
    @SerializedName("Shapo")
    @Expose
    private String shapo;
    @SerializedName("DatePub")
    @Expose
    private long datePub;
    @SerializedName("Like")
    @Expose
    private int like;
    @SerializedName("Comment")
    @Expose
    private int comment;
    @SerializedName("Reads")
    @Expose
    private int reads;
    @SerializedName("Type")
    @Expose
    private int type;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("CategoryAlias")
    @Expose
    private String categoryAlias;
    @SerializedName("ParentCategory")
    @Expose
    private String parentCategory;
    @SerializedName("ParentCategoryAlias")
    @Expose
    private String parentCategoryAlias;
    @SerializedName("FanCount")
    @Expose
    private int fanCount;
    @SerializedName("StarId")
    @Expose
    private int starId;
    @SerializedName("NameStar")
    @Expose
    private String nameStar;
    @SerializedName("AuthorName")
    @Expose
    private String authorName;
    @SerializedName("SourceName")
    @Expose
    private String sourceName;
    @SerializedName("VideoId")
    @Expose
    private int videoId;
    @SerializedName("Position")
    @Expose
    private int position;
    @SerializedName("TypeIcon")
    @Expose
    private int typeIcon;
    @SerializedName("Facebook")
    @Expose
    private String facebook;
    @SerializedName("MediaUrl")
    @Expose
    private String mediaUrl;
    @SerializedName("ThematicName")
    @Expose
    private String thematicName = "";
    @SerializedName("Body")
    @Expose
    private List<NewsDetailModel> body;
    @SerializedName("BodySize")
    @Expose
    private int bodySize;
    @SerializedName("Header")
    @Expose
    private String header;
    @SerializedName("LatestTitle")
    @Expose
    private String latestTitle;
    @SerializedName("Quote")
    @Expose
    private String quote;
    @SerializedName("Poster")
    @Expose
    private String poster;
    @SerializedName("unixTime")
    @Expose
    private long unixTime;
    @SerializedName("ThematicId")
    @Expose
    private int thematicId;
    @SerializedName("UrlOrigin")
    @Expose
    private String sourceUrl = "";
    @SerializedName("HasTagSlug")
    @Expose
    private String hasTagSlug = "";
    @SerializedName("HasTagName")
    @Expose
    private String hasTagName = "";

    private boolean isLike = false;

    public TiinModel() {
    }

    public String getHasTagSlug() {
        return hasTagSlug;
    }

    public void setHasTagSlug(String hasTagSlug) {
        this.hasTagSlug = hasTagSlug;
    }

    public String getHasTagName() {
        return hasTagName;
    }

    public void setHasTagName(String hasTagName) {
        this.hasTagName = hasTagName;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public String getUrlOrigin() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public long getUnixTime() {
        return unixTime;
    }

    public void setUnixTime(long unixTime) {
        this.unixTime = unixTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        if (title == null) title = "";
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage169() {
        return image169;
    }

    public void setImage169(String image169) {
        this.image169 = image169;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getShapo() {
        if (shapo == null) shapo = "";
        return shapo;
    }

    public void setShapo(String shapo) {
        this.shapo = shapo;
    }

    public long getDatePub() {
        return datePub;
    }

    public void setDatePub(long datePub) {
        this.datePub = datePub;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }

    public int getReads() {
        return reads;
    }

    public void setReads(int reads) {
        this.reads = reads;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryAlias() {
        return categoryAlias;
    }

    public void setCategoryAlias(String categoryAlias) {
        this.categoryAlias = categoryAlias;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getParentCategoryAlias() {
        return parentCategoryAlias;
    }

    public void setParentCategoryAlias(String parentCategoryAlias) {
        this.parentCategoryAlias = parentCategoryAlias;
    }

    public int getFanCount() {
        return fanCount;
    }

    public void setFanCount(int fanCount) {
        this.fanCount = fanCount;
    }

    public int getStarId() {
        return starId;
    }

    public void setStarId(int starId) {
        this.starId = starId;
    }

    public String getNameStar() {
        return nameStar;
    }

    public void setNameStar(String nameStar) {
        this.nameStar = nameStar;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTypeIcon() {
        return typeIcon;
    }

    public void setTypeIcon(int typeIcon) {
        this.typeIcon = typeIcon;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getThematicName() {
        return thematicName;
    }

    public void setThematicName(String thematicName) {
        this.thematicName = thematicName;
    }

    public List<NewsDetailModel> getBody() {
        if (body == null) body = new ArrayList<>();
        return body;
    }

    public void setBody(List<NewsDetailModel> body) {
        this.body = body;
    }

    public int getBodySize() {
        return bodySize;
    }

    public void setBodySize(int bodySize) {
        this.bodySize = bodySize;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getLatestTitle() {
        return latestTitle;
    }

    public void setLatestTitle(String latestTitle) {
        this.latestTitle = latestTitle;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public String getIdFilter() {
        return String.valueOf(getId());
    }

    public int getThematicId() {
        return thematicId;
    }

    public void setThematicId(int thematicId) {
        this.thematicId = thematicId;
    }

}

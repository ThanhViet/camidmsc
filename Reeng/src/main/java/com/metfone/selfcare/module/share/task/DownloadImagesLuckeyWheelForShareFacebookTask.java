/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/13
 *
 */

package com.metfone.selfcare.module.share.task;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import androidx.annotation.Nullable;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.share.listener.ShareImagesOnFacebookListener;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;

public class DownloadImagesLuckeyWheelForShareFacebookTask extends AsyncTask<Void, Integer, Bitmap> {
    private final String TAG = "DownloadImagesForShareFacebookTask";
    private WeakReference<ApplicationController> application;
    private ShareImagesOnFacebookListener listener;
    private String urlImage;

    public DownloadImagesLuckeyWheelForShareFacebookTask(ApplicationController application, String urlImage) {
        this.application = new WeakReference<>(application);
        this.urlImage = urlImage;
    }

    public void setListener(ShareImagesOnFacebookListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) listener.onPrepareDownload();
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        if (Utilities.notNull(application) && Utilities.notEmpty(urlImage)) {
            Bitmap bitmap = null;
            if (application.get().isDataReady()) {
                bitmap = ImageBusiness.downloadImageBitmap(urlImage);
            }
            return bitmap;
        }
        return null;
    }

    @Override
    protected void onPostExecute(@Nullable Bitmap results) {
        if (listener != null) listener.onCompletedDownload(results);
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.holder.SuggestDataDetailHolder;
import com.metfone.selfcare.module.myviettel.listener.OnDataChallengeListener;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;

public class SuggestDataAdapter extends BaseAdapter<BaseAdapter.ViewHolder, DataChallenge> {
    public static final int TYPE_NORMAL = 1;

    private OnDataChallengeListener listener;

    public SuggestDataAdapter(Activity activity) {
        super(activity);
    }

    public void setListener(OnDataChallengeListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_NORMAL) {
            return new SuggestDataDetailHolder(layoutInflater.inflate(R.layout.holder_suggest_data_mvt_dc, parent, false), activity, listener);
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(getItem(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        DataChallenge item = getItem(position);
        if (item != null) return TYPE_NORMAL;
        return TYPE_EMPTY;
    }
}

package com.metfone.selfcare.module.sc_umoney.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.sc_umoney.network.model.Subcirption;

public class SubcriptionResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("data")
    @Expose
    private Subcirption data;

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public Subcirption getData() {
        return data;
    }
}

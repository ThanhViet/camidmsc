package com.metfone.selfcare.module.home_kh.tab.adapter.gift;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeGiftItem;

import butterknife.BindView;

public class KhHomeGiftAdapter extends BaseAdapter<KhHomeGiftAdapter.ViewHolder, KhHomeGiftItem.GiftItem> {

    private EventListener listener;
    public KhHomeGiftAdapter(Activity activity, EventListener listener) {
        super(activity);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gift_tab_home, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }



    class ViewHolder extends BaseAdapter.ViewHolder{
        @BindView(R.id.imgBanner)
        public ImageView imageView;
        public ViewHolder(View view) {
            super(view);
        }

        public void bind(KhHomeGiftItem.GiftItem item){
            imageView.setImageResource(item.resourceBanner);
            if(item.isActive){
                itemView.setOnClickListener(v-> {
                    listener.onGiftClicked(item);
                });
            }
        }

    }

    public interface EventListener{
        void onGiftClicked(KhHomeGiftItem.GiftItem giftItem);
    }
}

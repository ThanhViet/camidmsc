package com.metfone.selfcare.module.tiin.base;

public interface MvpView {
    void showLoading();
    void hideLoading();
    boolean isNetworkConnected();
}

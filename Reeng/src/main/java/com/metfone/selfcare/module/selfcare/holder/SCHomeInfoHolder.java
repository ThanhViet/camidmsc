package com.metfone.selfcare.module.selfcare.holder;

import android.content.Context;

import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.rd.PageIndicatorView;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.adapter.SCHomeInfoAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCLoyaltyModel;
import com.metfone.selfcare.module.selfcare.model.SCSubListModel;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.ui.imageview.CircleImageView;

import java.util.ArrayList;

public class SCHomeInfoHolder extends BaseViewHolder {
    Context mContext;
    PageIndicatorView pageIndicatorView;
    ViewPager viewPager;
    SCHomeInfoAdapter homeInfoAdapter;
    ArrayList<SCSubListModel> subList = new ArrayList<>();
    SCLoyaltyModel loyaltyModel;

    RelativeLayout layoutInfo;
    CircleImageView imvAvatar;
    TextView tvAvatarDefault;
    TextView tvContactAvatar;
    ImageView imvCover;
    TextView tvName;
    TextView tvStatus;
    TextView tvRank;
    TextView tvPoint;
    //    TextView tvNextRank;
    ImageView imvNextRank;
    View imvDot;
    SeekBar progressPoint;

    public SCHomeInfoHolder(Context mContext, View view, final AbsInterface.OnSCHomeListener listener) {
        super(view);
        this.mContext = mContext;

        layoutInfo = view.findViewById(R.id.layout_info);
        imvAvatar = view.findViewById(R.id.imvAvatar);
        tvAvatarDefault = view.findViewById(R.id.tv_avatar_default);
        tvContactAvatar = view.findViewById(R.id.tv_contact_avatar);
        imvCover = view.findViewById(R.id.imvCover);
        tvName = view.findViewById(R.id.tvName);
        tvStatus = view.findViewById(R.id.tvStatus);
        tvRank = view.findViewById(R.id.tvRank);
        tvPoint = view.findViewById(R.id.tvPoint);
//        tvNextRank = view.findViewById(R.id.tvNextRank);
        imvNextRank = view.findViewById(R.id.imvNextRank);
        imvDot = view.findViewById(R.id.imvDot);
        progressPoint = view.findViewById(R.id.progress);
        progressPoint.setPadding(0, 0, 0, 0);
        progressPoint.getThumb().mutate().setAlpha(0);

        viewPager = view.findViewById(R.id.viewPagerSC);
        pageIndicatorView = view.findViewById(R.id.pageIndicatorSCView);

        homeInfoAdapter = new SCHomeInfoAdapter(mContext);
        homeInfoAdapter.setOnClickListener(listener);

        viewPager.setAdapter(homeInfoAdapter);
//        viewPager.setOffscreenPageLimit(3);
        viewPager.setCurrentItem(0);
        pageIndicatorView.setViewPager(viewPager);
        pageIndicatorView.setCount(subList.size());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (subList.size() > 0 && subList.get(position) != null && !TextUtils.isEmpty(subList.get(position).getIsdn())) {
                    SCUtils.setCurrentPhoneNumber(subList.get(position).getIsdn());
                    SCUtils.setCurrentAccount(subList.get(position));

                    pageIndicatorView.setSelection(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        layoutInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (listener != null)
//                    listener.onLoyaltyClick();
                if (listener != null)
                    listener.onUserInfoClick();
            }
        });

        imvAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onUserInfoClick();
            }
        });
    }

    public void setUserInfo(String username, String fullname, String avatar) {
        //Load data
//        tvName.setText(TextUtils.isEmpty(fullname) ? username : fullname);

        ApplicationController mApplication = ApplicationController.self();
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        if (accountBusiness != null && accountBusiness.getCurrentAccount() != null) {
            tvName.setText(accountBusiness.getCurrentAccount().getName());
            tvStatus.setText(accountBusiness.getCurrentAccount().getStatus());
        }

        if (!mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
            if (account != null) {
                String userName = account.getName();
                String lAvatar = account.getLastChangeAvatar();
                if (!TextUtils.isEmpty(lAvatar)) {
                    mApplication.getAvatarBusiness().setMyAvatar(imvAvatar,
                            tvContactAvatar, tvAvatarDefault, account, null);
                }
            }
        } else {
            ImageBusiness.setResource(imvAvatar, R.drawable.ic_tab_home_avatar_default);
        }

//        avatar = mApplication.getPref().getString(SCConstants.PREFERENCE.SC_AVATAR, "");
//        SCImageLoader.setAvatar(mContext, imvAvatar, avatar);
    }

    public void setSubList(ArrayList<SCSubListModel> subList) {
        this.subList = subList;

        //Load sublist
        homeInfoAdapter.setDatas(subList);
        viewPager.setAdapter(homeInfoAdapter);
        homeInfoAdapter.notifyDataSetChanged();

        //Set so mac dinh la so dau tien
        if (subList.size() > 0 && !TextUtils.isEmpty(subList.get(0).getIsdn())) {
            SCUtils.setCurrentPhoneNumber(subList.get(0).getIsdn());
            SCUtils.setCurrentAccount(subList.get(0));
        }
    }

    public void setLoyaltyModel(final SCLoyaltyModel loyaltyModel) {
        this.loyaltyModel = loyaltyModel;

        //Load loyalty
//        if (loyaltyModel != null) {
////            tvRank.setText(mContext.getString(R.string.loyalty_member, loyaltyModel.getName()));
//            tvRank.setText(loyaltyModel.getName());
//
//            double point = 0;
//            for (int i = 0; i < loyaltyModel.getBalances().size(); i++) {
//                SCLoyaltyModel.Balance balance = loyaltyModel.getBalances().get(i);
//                if (balance.getLoyaltyBalanceCode().equals("MYTEL_LOYALTY_POINTS")) {
//                    point = balance.getBalance();
//                }
//            }
//            tvPoint.setText(SCUtils.numberFormatNoDecimal(point) + "/" + SCUtils.numberFormatNoDecimal(point + loyaltyModel.getRemainScore()) + " point");
//            if (SCConstants.RANK_TYPE.DIAMOND.equals(loyaltyModel.getName().toUpperCase())) {
//                tvPoint.setVisibility(View.GONE);
//                imvDot.setVisibility(View.GONE);
////                tvNextRank.setVisibility(View.GONE);
//                imvNextRank.setVisibility(View.GONE);
//                progressPoint.setVisibility(View.GONE);
//            } else {
//                progressPoint.setVisibility(View.VISIBLE);
//                tvPoint.setVisibility(View.VISIBLE);
//                imvDot.setVisibility(View.VISIBLE);
//                imvNextRank.setVisibility(View.VISIBLE);
////                tvNextRank.setVisibility(View.VISIBLE);
////                tvNextRank.setText(loyaltyModel.getNextRank());
////                tvNextRank.setCompoundDrawablesWithIntrinsicBounds(0, SCUtils.getRankResource(loyaltyModel.getNextRank()), 0, 0);
//                imvNextRank.setImageResource(SCUtils.getRankResource(loyaltyModel.getNextRank()));
//            }
//
//            if (point == 0)
//                point = 1;
//            progressPoint.setMax((int) (point + loyaltyModel.getRemainScore()));
//            progressPoint.setProgress((int) point);
//            progressPoint.setEnabled(false);
//        }
    }
}

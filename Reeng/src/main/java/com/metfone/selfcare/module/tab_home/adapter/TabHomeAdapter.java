package com.metfone.selfcare.module.tab_home.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.tab_home.holder.BoxContentHolder;
import com.metfone.selfcare.module.tab_home.holder.FeatureHolder;
import com.metfone.selfcare.module.tab_home.holder.QuickContactHolder;
import com.metfone.selfcare.module.tab_home.holder.SlideBannerHolder;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.util.Log;

public class TabHomeAdapter extends BaseAdapter<BaseAdapter.ViewHolder, TabHomeModel> {

    private TabHomeListener.OnAdapterClick listener;

    public TabHomeAdapter(Activity act) {
        super(act);
    }

    public void setListener(TabHomeListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        TabHomeModel item = getItem(position);
        if (item != null) return item.getType();
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TabHomeModel.TYPE_SLIDER_BANNER:
                return new SlideBannerHolder(layoutInflater.inflate(R.layout.holder_box_banner_tab_home, parent, false), activity, listener, false);
            case TabHomeModel.TYPE_FEATURE:
                return new FeatureHolder(layoutInflater.inflate(R.layout.holder_box_feature_tab_home, parent, false), activity, listener);
            case TabHomeModel.TYPE_QUICK_CONTACT:
                return new QuickContactHolder(layoutInflater.inflate(R.layout.holder_box_contact_tab_home, parent, false), activity, listener);
            case TabHomeModel.TYPE_BOX_VIDEO:
            case TabHomeModel.TYPE_BOX_MUSIC:
            case TabHomeModel.TYPE_BOX_MOVIE:
            case TabHomeModel.TYPE_BOX_NEWS:
            case TabHomeModel.TYPE_BOX_COMIC:
            case TabHomeModel.TYPE_BOX_TIIN:
                return new BoxContentHolder(layoutInflater.inflate(R.layout.holder_box_content_tab_home, parent, false), activity, listener);
        }
        Log.d(TAG, "onCreateViewHolder other viewType: " + viewType);
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TabHomeModel item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

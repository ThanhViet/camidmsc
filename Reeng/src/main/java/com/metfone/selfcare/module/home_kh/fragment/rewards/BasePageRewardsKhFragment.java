package com.metfone.selfcare.module.home_kh.fragment.rewards;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.fragment.rewards.adapter.ItemDecorationGrid;
import com.metfone.selfcare.module.home_kh.fragment.rewards.adapter.SelectPointAdapter;
import com.metfone.selfcare.module.home_kh.fragment.setting.BaseSettingKhFragment;
import com.metfone.selfcare.module.home_kh.model.GiftRate;
import com.metfone.selfcare.module.home_kh.model.LstBlock;
import com.metfone.selfcare.module.home_kh.model.RewardChange;
import com.metfone.selfcare.module.home_kh.model.Rewards;
import com.metfone.selfcare.module.home_kh.model.TelecomGift;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

public abstract class BasePageRewardsKhFragment extends BaseSettingKhFragment
        implements SelectPointAdapter.ISelectPoint {
    protected static final String DATA = "DATA";
    protected static final String MAX = "MAX";
    protected Rewards rewards;
    protected SelectPointAdapter selectPointAdapter;
    protected GiftRate giftRate;
    protected int pointValue;
    protected int maxPoint;
    protected Unbinder unbinder;
    protected DetailType detailType;
    private String unit = "$";

    protected double sbValue;
    protected double sbRate;

    @Nullable
    @BindView(R.id.sb_points)
    protected AppCompatSeekBar sbPoints;

    @Nullable
    @BindView(R.id.layout_value)
    protected CardView layoutValue;

    @Nullable
    @BindView(R.id.tv_points_max)
    protected AppCompatTextView tvPointsMax;

    @Nullable
    @BindView(R.id.tv_points_min)
    protected AppCompatTextView tvPointsMin;

    @Nullable
    @BindView(R.id.rec_options)
    protected RecyclerView rvOptions;

    @Nullable
    @BindView(R.id.tv_hints)
    protected AppCompatTextView tvHint;

    @Nullable
    @BindView(R.id.point_select)
    protected AppCompatTextView pointSelect;

    @Nullable
    @BindView(R.id.point_select_value)
    protected AppCompatTextView pointSelectValue;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            rewards = (Rewards) bundle.getSerializable(DATA);
            maxPoint = bundle.getInt(MAX);
            if (pointSelect != null) {
                pointSelect.setText(ResourceUtils.getString(R.string.reward_shop_points, 0));
            }
            if (tvPointsMax != null) {
                tvPointsMax.setText(String.format(getString(R.string.reward_shop_points), String.valueOf(maxPoint - maxPoint % 10)));
            }
            if (tvPointsMin != null) {
                tvPointsMin.setText("0");
            }
            if (rewards != null) {
                initData(rewards);
                initSeekBar();
                selectPointAdapter.setSelectedPos(0);
                sbPoints.setProgress(1);
            }
            if(maxPoint < 10){
                tvPointsMin.setText("0");
                pointSelectValue.setText("0");
                pointSelect.setText("0");
                selectPointAdapter.setSelectedPos(-1);
                EventBus.getDefault().post(new RewardsKHFragment() {
                });
            }
        }
    }

    protected void initSeekBar() {
        if (sbPoints == null) {
            return;
        }
        setMaxPoint(maxPoint);
        sbPoints.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (giftRate != null) {
                    int newValue = i * 10;
                    pointValue = Math.min(newValue, maxPoint);
//                    double value = (newValue * giftRate.getValue() / giftRate.getPoint());
                    if (tvPointsMin != null)
                        tvPointsMin.setText(String.valueOf(newValue));
                    if (pointSelect != null) {
                        pointSelect.setText(ResourceUtils.getString(R.string.reward_shop_points, (newValue / 10) * 10));
                        if (newValue == 0) {
                            EventBus.getDefault().post(new RewardChange(true));
                        } else {
                            EventBus.getDefault().post(new RewardChange(false));
                            if(selectPointAdapter != null && selectPointAdapter.getData() != null){
                                selectPointAdapter.setSelectedPos(-1);
                                for(int x = 0; x < selectPointAdapter.getData().size();x++){
                                    if(newValue == selectPointAdapter.getData().get(x).getPoint()){
                                        selectPointAdapter.setSelectedPos(x);
                                        break;
                                    }
                                }
                            }
                            selectPointAdapter.notifyDataSetChanged();
                        }
                    }
//                    if (pointSelectValue != null) {
//                        if (detailType == DetailType.BALANCE) {
//                            pointSelectValue.setText(unit + " " + Utilities.formatDouble(value));
//                        } else {
//                            pointSelectValue.setText(Utilities.formatDouble(value) + " " + unit);
//                        }
//                    }

                    sbValue = (newValue * sbRate);
                    System.out.println("TuanHM getNewValue " + newValue);
                    System.out.println("TuanHM getSbRate0 " + sbRate);
                    System.out.println("TuanHM getSbValue " + sbValue);

                    if (pointSelectValue != null) {
                        if (detailType == DetailType.BALANCE) {
                            pointSelectValue.setText(unit + " " + Utilities.formatDouble(sbValue));
                        } else {
                            pointSelectValue.setText(Utilities.formatDouble(sbValue) + " " + unit);
                        }
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    protected void initData(Rewards rewards) {
        if (rvOptions == null) {
            return;
        }
        if (rewards.getTelecomGifts() != null && !rewards.getTelecomGifts().isEmpty()) {
            TelecomGift telecomGifts = rewards.getTelecomGifts().get(0);
            giftRate = telecomGifts.getGiftRate();
            if (tvHint != null) {
                tvHint.setText(telecomGifts.getGiftDesc());
                if (telecomGifts.getGiftDesc() != null && !telecomGifts.getGiftDesc().isEmpty()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tvHint.setText(Html.fromHtml(telecomGifts.getGiftDesc(), Html.FROM_HTML_MODE_COMPACT).toString());
                    } else {
                        tvHint.setText(Html.fromHtml(telecomGifts.getGiftDesc()).toString());
                    }
                } else {
                    tvHint.setText("");
                }
            }

            selectPointAdapter = new SelectPointAdapter(telecomGifts.getLstBlock(),detailType, this);
            if (!telecomGifts.getLstBlock().isEmpty()) {
                selectPointAdapter.setSelectedPosDefault();
                setProgressBar(telecomGifts.getLstBlock().get(0));
                pointValue = telecomGifts.getLstBlock().get(0).getPoint();
                unit = telecomGifts.getLstBlock().get(0).getUnit();
            }
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2,
                    LinearLayoutManager.VERTICAL, false);
            rvOptions.setLayoutManager(gridLayoutManager);
            rvOptions.addItemDecoration(new ItemDecorationGrid(
                    getResources().getDimensionPixelSize(R.dimen.dimen18dp),
                    2));
            rvOptions.setAdapter(selectPointAdapter);
            if (sbPoints != null) {
                sbPoints.setProgress(telecomGifts.getLstBlock().get(0).getPoint());
            }
            if (tvPointsMin != null){
                tvPointsMin.setText(String.valueOf(telecomGifts.getLstBlock().get(0).getPoint()));
            }
            if(maxPoint < 10){
                tvPointsMin.setText("0");
                sbPoints.setProgress(0);
            }

        }


        sbRate = rewards.getTelecomGifts().get(0).getLstBlock().get(0).getValue() / rewards.getTelecomGifts().get(0).getLstBlock().get(0).getPoint();

//        sbRate = giftRate.getValue() / giftRate.getPoint();
        System.out.println("TuanHM getValue " + giftRate.getValue());
        System.out.println("TuanHM getPoint " + giftRate.getPoint());
        System.out.println("TuanHM getSbRate " + sbRate);
    }
    public boolean isEnoughPoint(){
        try {
            return maxPoint >= Integer.parseInt(tvPointsMin.getText().toString());
        }catch (Exception e){
            return false;
        }
    }

    public void setMaxPoint(int maxPoint) {
        this.maxPoint = maxPoint;
        int sub = maxPoint % 10;
        int max = maxPoint / 10;
        if (sbPoints != null)
            sbPoints.setMax(max);
        if (tvPointsMax != null) {
            tvPointsMax.setText(String.format(getString(R.string.reward_shop_points), String.valueOf(maxPoint - sub)));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void itemClick(int position) {
        if (selectPointAdapter != null) {
            LstBlock item = selectPointAdapter.getData().get(position);
            if(item.getPoint() > maxPoint){
                selectPointAdapter.setSelectedPos(-1);
                selectPointAdapter.notifyDataSetChanged();
                ToastUtils.showShort(R.string.not_enough_point);
                return;
            }else {
                setProgressBar(item);
                selectPointAdapter.setSelectedPos(position);
                selectPointAdapter.notifyDataSetChanged();
            }
        }
    }

    private void setProgressBar(LstBlock item) {
        if (sbPoints != null)
            sbPoints.setProgress((int) (item.getPoint() / 10));
        if (pointSelect != null)
            pointSelect.setText(ResourceUtils.getString(R.string.reward_shop_points, item.getPoint()));
        if (pointSelectValue != null) {
            if (detailType == DetailType.BALANCE) {
                pointSelectValue.setText(item.getUnit() + " " + item.getValue());
            } else {
                pointSelectValue.setText(item.getValue() + " " + item.getUnit());
            }
        }
    }

    public String getGiftId(){
        return rewards.getTelecomGifts().get(0).getGiftId();
    }


    @Override
    public String getName() {
        return "";
    }

    @Override
    protected void initView(View view) {

    }
}

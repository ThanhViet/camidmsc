package com.metfone.selfcare.module.sc_umoney.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.sc_umoney.network.model.Info;

public class InfoResponse {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("errorCode")
    @Expose
    private int errorCode;
    @SerializedName("data")
    @Expose
    private Info data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public Info getData() {
        return data;
    }

    public void setData(Info data) {
        this.data = data;
    }
}

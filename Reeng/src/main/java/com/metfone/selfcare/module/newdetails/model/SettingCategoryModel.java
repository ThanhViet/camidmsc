package com.metfone.selfcare.module.newdetails.model;

import androidx.fragment.app.Fragment;

/**
 * Created by HaiKE on 9/19/17.
 */

public class SettingCategoryModel {
    private Fragment fragment;
    private int categoryId;
    private String title;

    public SettingCategoryModel(Fragment fragment, String title, int categoryId) {
        this.fragment = fragment;
        this.categoryId = categoryId;
        this.title = title;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

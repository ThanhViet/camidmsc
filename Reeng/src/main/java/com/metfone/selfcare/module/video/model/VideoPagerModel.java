/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/25
 *
 */

package com.metfone.selfcare.module.video.model;

import java.io.Serializable;
import java.util.ArrayList;

public class VideoPagerModel implements Serializable {

    public static final int TYPE_SUGGEST_LIST = 1;
    public static final int TYPE_CHANNEL_LIST = 2;
    public static final int TYPE_NEW_VIDEO_LIST = 3;
    public static final int TYPE_FRIEND_LIST = 4;
    public static final int TYPE_HEADER_LIST_VIDEO = 5;
    public static final int TYPE_VIDEO_NORMAL = 6;
    public static final int TYPE_VIDEO_BANNER = 7;
    public static final int TYPE_ADS = 10;

    private static final long serialVersionUID = -3662759050854304941L;

    private Object object;
    private ArrayList<Object> list;
    private String id;
    private int type;
    private String title;
    private long currentTime;
    private boolean showViewAll;
    private boolean showLine;

    public VideoPagerModel() {
        currentTime = System.currentTimeMillis();
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public ArrayList<Object> getList() {
        if (list == null) list = new ArrayList<>();
        return list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isShowViewAll() {
        return showViewAll;
    }

    public void setShowViewAll(boolean showViewAll) {
        this.showViewAll = showViewAll;
    }

    public boolean isShowLine() {
        return showLine;
    }

    public void setShowLine(boolean showLine) {
        this.showLine = showLine;
    }
}

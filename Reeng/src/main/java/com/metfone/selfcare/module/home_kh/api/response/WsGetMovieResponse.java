package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetMovieResponse {
    public String errorCode;
    public String errorMessage;
    @SerializedName("result")
    public Response response;

    public static class Response{
        @SerializedName("result")
        public List<KhHomeMovieItem> result;
    }
}

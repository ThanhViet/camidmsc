package com.metfone.selfcare.module.keeng.widget;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.metfone.selfcare.module.keeng.utils.Log;

/**
 * Created by namnh40 on 7/25/2017.
 * https://stackoverflow.com/questions/41650721/attempt-to-invoke-virtual-method-android-os-handler-android-support-v4-app-frag
 */

public abstract class CustomFragmentPagerAdapter extends FragmentPagerAdapter {
    final String TAG = "CustomFragmentPagerAdapter";

    public CustomFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
//            if (container != null)
            super.finishUpdate(container);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        try {
            super.destroyItem(container, position, object);
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }
}

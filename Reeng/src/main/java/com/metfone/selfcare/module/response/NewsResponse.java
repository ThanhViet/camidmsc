package com.metfone.selfcare.module.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.NewsModel;

import java.util.ArrayList;

public class NewsResponse extends ErrorResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<NewsModel> data = new ArrayList<>();

    public ArrayList<NewsModel> getData() {
        return data;
    }

    public void setData(ArrayList<NewsModel> data) {
        this.data = data;
    }
}


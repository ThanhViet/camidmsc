package com.metfone.selfcare.module.metfoneplus.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.camid.CheckComplaintNotification;
import com.metfone.selfcare.model.camid.HistoryComplaint;
import com.metfone.selfcare.model.camid.ProcessComplaint;
import com.metfone.selfcare.model.camid.TotalComplaint;
import com.metfone.selfcare.module.metfoneplus.activity.AddFeedbackActivity;
import com.metfone.selfcare.module.metfoneplus.adapter.HistoryComplaintAdapter;
import com.metfone.selfcare.module.metfoneplus.adapter.TotalComplaintAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.DialogSelectDate;
import com.metfone.selfcare.module.metfoneplus.dialog.MPConfirmationCallDialog;
import com.metfone.selfcare.module.metfoneplus.holder.HistoryComplaintViewHolder;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckComplaintNotificationResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCloseComplainResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComplaintHistoryResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProcessListResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsRateComplainResponse;
import com.metfone.selfcare.ui.ratingbar.SimpleRatingBar;
import com.metfone.selfcare.util.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPFeedbackUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPFeedbackUsFragment extends MPBaseFragment implements HistoryComplaintAdapter.ComplaintListener {
    public static final String TAG = MPFeedbackUsFragment.class.getSimpleName();
    public static String ADD_INTERNET = "add_internet";
    public static String ADD_MOBILE = "add_mobile";
    public static String ADD_UPDATE = "add_update";
    public static String ADD_CLOSE = "add_close";

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_option)
    RelativeLayout mLayoutActionBarOption;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.action_bar_option_more)
    AppCompatImageView ivActionBarOption;
    @BindView(R.id.m_p_feedback_us_history_complaint_list)
    RecyclerView mHistoryComplaintList;
    @BindView(R.id.tvFeedbackSelectDate)
    TextView tvFeedbackSelectDate;
    @BindView(R.id.spTotalComplaint)
    Spinner spTotalComplaint;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    String totalComplaint = "200";
    Date currentDate;
    String mDateFrom, mDateTo;
    Calendar calendar = Calendar.getInstance();
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private FragmentManager mFragmentManager;
    private HistoryComplaintAdapter mHistoryComplaintAdapter;
    private ArrayList<TotalComplaint> totalComplaintArrayList;
    private TotalComplaintAdapter totalComplaintAdapter;
    UserInfoBusiness userInfoBusiness;
    ArrayList<ServicesModel> servicesModelList = new ArrayList<>();
    public MPFeedbackUsFragment() {
        // Required empty public constructor
    }

    public static MPFeedbackUsFragment newInstance() {
        MPFeedbackUsFragment fragment = new MPFeedbackUsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
        userInfoBusiness = new UserInfoBusiness(mParentActivity);
        List<Services> services = userInfoBusiness.getServiceList();
        servicesModelList = UserInfoBusiness.convertServicesToServiceModelList(services);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_feedback_us;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mLayoutActionBar);
        mActionBarTitle.setText(getString(R.string.m_p_feedback_us_title));
        mLayoutActionBarOption.setVisibility(View.VISIBLE);
        ivActionBarOption.setBackgroundResource(R.drawable.ic_add);
        mHistoryComplaintAdapter = new HistoryComplaintAdapter(mParentActivity.getApplicationContext(),
                new ArrayList<HistoryComplaint>(),
                new ArrayList<ProcessComplaint>(),
                (v) -> {
                    HistoryComplaintViewHolder holder = (HistoryComplaintViewHolder) v.getTag();
                    mHistoryComplaintAdapter.updateLayoutView(holder.position, !holder.mHistoryComplaint.isExpandView());
                }, this);
        mHistoryComplaintList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mHistoryComplaintList.setNestedScrollingEnabled(true);
        mHistoryComplaintList.setAdapter(mHistoryComplaintAdapter);
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }

        currentDate = Calendar.getInstance().getTime();
        calendar.setTime(currentDate);
        calendar.add(Calendar.MONTH, -1);
        mDateTo = simpleDateFormat.format(currentDate);
        mDateFrom = simpleDateFormat.format(calendar.getTime());
        tvFeedbackSelectDate.setText(mDateFrom + " - " + mDateTo);

        setSpinnerTotalComplaint();
        callRefresh();
        getCheckNotification();

        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(false);
            callRefresh();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getWSGetProcessList();
//        getCheckNotification();
        showToastValue();
    }

    private void callRefresh() {
        getWSComplaintHistoryList(mDateFrom, mDateTo, totalComplaint);
    }

    public void getWSGetProcessList() {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        try {
            new MetfonePlusClient().wsGetProcessList(new MPApiCallback<WsGetProcessListResponse>() {
                @Override
                public void onResponse(Response<WsGetProcessListResponse> response) {
                    if (response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null) {
                        mHistoryComplaintAdapter.setProcessComplaintHashMap(response.body().getResult().getWsResponse().getProcessComplaints());
                        callRefresh();
                    } else {
                        mParentActivity.hideLoadingDialog();
                    }

                }

                @Override
                public void onError(Throwable error) {
                    Log.e(TAG, "onError: ", error);
                    mParentActivity.hideLoadingDialog();
                }
            });
        } catch (Exception e) {
            mParentActivity.hideLoadingDialog();
            com.metfone.selfcare.util.Log.e(TAG, "Error: ", e);
        }
    }

    private void getWSComplaintHistoryList(String fromDate, String toDate, String totalComplaint) {
        mParentActivity.showLoadingDialog("", "");
        try {
           String isdn = "";
            for ( ServicesModel service: servicesModelList ) {
                if (isdn.equals(""))
                    isdn = service.getPhone_number();
                else
                    isdn = isdn +"," +service.getPhone_number();
            }
            new MetfonePlusClient().wsGetComplaintHistoryList(fromDate, toDate, totalComplaint,isdn, new MPApiCallback<WsGetComplaintHistoryResponse>() {
                @Override
                public void onResponse(Response<WsGetComplaintHistoryResponse> response) {
                    if (response.body() != null && response.body().getResult().getWsResponse() != null) {
                        mHistoryComplaintAdapter.replaceData(response.body().getResult().getWsResponse().getHistoryComplaints());
                    }
                    mParentActivity.hideLoadingDialog();
                }

                @Override
                public void onError(Throwable error) {
                    Log.e(TAG, "onError: ", error);
                    mParentActivity.hideLoadingDialog();
                }
            });
        } catch (Exception e) {
            mParentActivity.hideLoadingDialog();
            com.metfone.selfcare.util.Log.e(TAG, "Error: ", e);
        }
    }

    @OnClick(R.id.action_bar_option)
    void addFeedback() {
        Intent intent = new Intent(getActivity(), AddFeedbackActivity.class);
        mParentActivity.startActivity(intent);
    }

    @OnClick(R.id.tvFeedbackSelectDate)
    void addFeedbackSelectDate() {
        DialogSelectDate dialog = DialogSelectDate.newInstance(mDateFrom, mDateTo);
        dialog.dialogSelectDateListener = (dFrom, dTo) -> {
            mDateFrom = dFrom;
            mDateTo = dTo;
            tvFeedbackSelectDate.setText(mDateFrom + " - " + mDateTo);
            getWSComplaintHistoryList(mDateFrom, mDateTo, totalComplaint);
        };

        dialog.show(getParentFragmentManager(), DialogSelectDate.TAG);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.action_bar_back)
    void onBack() {
        popBackStackFragment();
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }
    }

    @Override
    public void closeComplaintClick(int position, HistoryComplaint historyComplaint) {
        mParentActivity.showLoadingDialog("", "");
        String complaintId = String.valueOf(historyComplaint.getComplainId());
        Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_comp_close);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView tvDialogTitle = dialog.findViewById(R.id.tvDialogTitle);
        TextView tvDialogContent = dialog.findViewById(R.id.tvDialogContent);
        TextView btnDialogYes = dialog.findViewById(R.id.btnDialogYes);
        TextView btnDialogNo = dialog.findViewById(R.id.btnDialogNo);
        tvDialogTitle.setText(R.string.tab_mobile_ftth_expected_deadline_129);
        tvDialogContent.setText(R.string.confirm_feedback);
        btnDialogNo.setOnClickListener(v -> {
            dialog.dismiss();
            mParentActivity.hideLoadingDialog();
        });
        btnDialogYes.setOnClickListener(v -> {
            dialog.dismiss();
            getCloseComplaint(position, complaintId);
        });
        mParentActivity.hideLoadingDialog();
        dialog.setCancelable(true);
        dialog.show();
    }

    public void getCloseComplaint(int position, String complaintId) {
        mParentActivity.showLoadingDialog("", "");
        try {
            new MetfonePlusClient().closeComplaint(complaintId, new MPApiCallback<WsCloseComplainResponse>() {
                @Override
                public void onResponse(Response<WsCloseComplainResponse> response) {
                    if (response != null && response.code() == 200) {
                        mHistoryComplaintAdapter.closeComplaintSuccess(position);
                        callRefresh();
                        Toast.makeText(getContext(), R.string.feedback_close_full, Toast.LENGTH_SHORT).show();
                        dialogRateComplaint(complaintId);
                    }
                    mParentActivity.hideLoadingDialog();
                }

                @Override
                public void onError(Throwable error) {
                    Log.e(TAG, "onError: ", error);
                    mParentActivity.hideLoadingDialog();
                }
            });
        } catch (Exception e) {
            mParentActivity.hideLoadingDialog();
            com.metfone.selfcare.util.Log.e(TAG, "Error: ", e);
        }
    }

    @Override
    public void callSupport(int position, HistoryComplaint historyComplaint) {
        MPConfirmationCallDialog confirmationCallDialog = MPConfirmationCallDialog
                .newInstance(getString(R.string.call_thanks_content_feedback)
                        , historyComplaint.getStaffPhone(), R.string.m_p_dialog_confirmation_call_y, R.string.m_p_dialog_confirmation_call_n);
        confirmationCallDialog.show(getParentFragmentManager(), MPConfirmationCallDialog.TAG);
    }

    public void dialogRateComplaint(String complaintId) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_comp_rating);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView tvDialogTitle = dialog.findViewById(R.id.tvDialogTitle);
        TextView tvDialogContent = dialog.findViewById(R.id.tvDialogContent);
        SimpleRatingBar ratingBar = dialog.findViewById(R.id.ratingBar);
        TextView btnDialogDone = dialog.findViewById(R.id.btnDialogDone);
        tvDialogTitle.setText(R.string.enjoying_feedback);
        tvDialogContent.setText(R.string.rate_feedback);
        btnDialogDone.setOnClickListener(v -> {
            dialog.dismiss();
            int ratingValue = (int) ratingBar.getRating();
            wsRateComplain(complaintId, String.valueOf(ratingValue));
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void dialogRateSuccess() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_comp_succes);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView tvDialogTitle = dialog.findViewById(R.id.tvDialogTitle);
        TextView tvDialogContent = dialog.findViewById(R.id.tvDialogContent);
        TextView btnDialogDone = dialog.findViewById(R.id.btnDialogDone);
        tvDialogTitle.setText(R.string.rate_thanks_feedback);
        tvDialogContent.setText(R.string.rate_thanks_content_feedback);
        btnDialogDone.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void setSpinnerTotalComplaint() {
        totalComplaintArrayList = new ArrayList<>();
        totalComplaintArrayList.add(new TotalComplaint("200"));
        totalComplaintArrayList.add(new TotalComplaint("100"));
        totalComplaintArrayList.add(new TotalComplaint("50"));
        totalComplaintArrayList.add(new TotalComplaint("10"));
        totalComplaintArrayList.add(new TotalComplaint("5"));
        totalComplaintAdapter = new TotalComplaintAdapter(getContext(), totalComplaintArrayList);
        spTotalComplaint.setAdapter(totalComplaintAdapter);
        spTotalComplaint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TotalComplaint clickedItem = (TotalComplaint) parent.getItemAtPosition(position);
                totalComplaint = clickedItem.totalComplaint;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void wsRateComplain(String complaintId, String rate) {
        try {
            new MetfonePlusClient().wsRateComplain(complaintId, rate, new MPApiCallback<WsRateComplainResponse>() {
                @Override
                public void onResponse(Response<WsRateComplainResponse> response) {
                    if (response.body() != null) {
                        dialogRateSuccess();
                        callRefresh();
                    }
                }

                @Override
                public void onError(Throwable error) {
                    Log.e(TAG, "onError: ", error);
                }
            });
        } catch (Exception e) {
            mParentActivity.hideLoadingDialog();
            com.metfone.selfcare.util.Log.e(TAG, "Error: ", e);
        }
    }

    public void dialogNotification(CheckComplaintNotification checkComplaintNotification) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_complaint);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView tvDialogTitle = dialog.findViewById(R.id.tvDialogTitle);
        TextView tvDialogContent = dialog.findViewById(R.id.tvDialogContent);
        TextView tvDialogQuestion = dialog.findViewById(R.id.tvDialogQuestion);
        TextView btnDialogYes = dialog.findViewById(R.id.btnDialogYes);
        TextView btnDialogNo = dialog.findViewById(R.id.btnDialogNo);
        tvDialogQuestion.setVisibility(View.VISIBLE);
        tvDialogTitle.setText(R.string.feedback_close_full_title);
        String content=getString(R.string.feedback_close_full_content);
        tvDialogContent.setText(content.replaceAll("_1_",checkComplaintNotification.preResult));
        tvDialogQuestion.setText(R.string.feedback_close_full_close);
        btnDialogYes.setOnClickListener(v -> {
            dialogRateComplaint(String.valueOf(checkComplaintNotification.complainId));
            callRefresh();
            dialog.dismiss();
        });
        btnDialogNo.setOnClickListener(v -> {
            mParentActivity.showLoadingDialog("", "");
            gotoMPFeedbackUpdate(
                    String.valueOf(checkComplaintNotification.complainId),
                    checkComplaintNotification.errorPhone,
                    checkComplaintNotification.complainerPhone,
                    checkComplaintNotification.acceptDate,
                    checkComplaintNotification.proLimitDate,
                    checkComplaintNotification.staffInfo,
                    checkComplaintNotification.serviceTypeName,
                    checkComplaintNotification.groupTypeName,
                    checkComplaintNotification.staffPhone,
                    checkComplaintNotification.compContent
            );
            mParentActivity.hideLoadingDialog();
            dialog.dismiss();
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void getCheckNotification() {
        mParentActivity.showLoadingDialog("", "");
        try {
            new MetfonePlusClient().wsCheckComplaintNotification(new MPApiCallback<WsCheckComplaintNotificationResponse>() {
                @Override
                public void onResponse(Response<WsCheckComplaintNotificationResponse> response) {
                    if (response.body() != null && response.body().getResult() != null
                            && response.body().getResult().getWsResponse() != null
                            && response.body().getResult().getWsResponse().checkComplaintNotification != null) {
                        dialogNotification(response.body().getResult().getWsResponse().checkComplaintNotification);
                        Log.d("PhanTien", "onResponse: " + response.body().getResult().getWsResponse().checkComplaintNotification.compTypeId);
                    }
                    mParentActivity.hideLoadingDialog();
                }

                @Override
                public void onError(Throwable error) {
                    Log.e(TAG, "onError: ", error);
                    mParentActivity.hideLoadingDialog();
                }
            });
        } catch (Exception e) {
            mParentActivity.hideLoadingDialog();
            com.metfone.selfcare.util.Log.e(TAG, "Error: ", e);
        }

    }


    public void showToastValue() {
        SharedPreferences sharedPref = getContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        boolean internet = sharedPref.getBoolean(ADD_INTERNET, false);
        boolean mobile = sharedPref.getBoolean(ADD_MOBILE, false);
        boolean update = sharedPref.getBoolean(ADD_UPDATE, false);
        boolean close = sharedPref.getBoolean(ADD_CLOSE, false);
        if (internet || mobile) {
            callRefresh();
            Toast.makeText(getContext(), R.string.feedback_success_full, Toast.LENGTH_LONG).show();
            sharedPref.edit().clear().commit();
        }

        if (update) {
            callRefresh();
            Toast.makeText(getContext(), R.string.feedback_success_full_update, Toast.LENGTH_LONG).show();
            sharedPref.edit().clear().commit();
        }
    }
}
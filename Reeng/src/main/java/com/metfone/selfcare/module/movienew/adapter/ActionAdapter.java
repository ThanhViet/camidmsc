package com.metfone.selfcare.module.movienew.adapter;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.movienew.listener.OnClickGenres;
import com.metfone.selfcare.module.movienew.model.HomeData;

import java.util.ArrayList;
import java.util.Collection;

public class ActionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnClickGenres {
    private static final int ITEM_MULTIPLE = 1;
    private static final int ITEM = 0;
    private ArrayList<HomeData> mArrayList;
    private Activity activity;
    private ItemHolder.OnclickItemAction mOnclickItemAction;
    private ArrayList<HomeData> itemArr = new ArrayList<>();

    public ActionAdapter(ArrayList<HomeData> mArrayList, Activity activity, ItemHolder.OnclickItemAction onclickItemAction) {
        this.mArrayList = mArrayList;
        this.activity = activity;
        this.mOnclickItemAction = onclickItemAction;
        if(mArrayList.size() > 3){
            itemArr= (ArrayList<HomeData>) mArrayList.subList(3,mArrayList.size());
        }
    }

    public void updateData(ArrayList<HomeData> data){
        mArrayList = data;
        if(mArrayList.size() > 3){
            itemArr= new ArrayList<HomeData>(mArrayList.subList(3,mArrayList.size()));
        }
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {

            case ITEM_MULTIPLE:
                View viewItem = inflater.inflate(R.layout.item_multiple_action, parent, false);
                viewHolder = new MultipleType(viewItem);
                break;

            case ITEM:
                View view = inflater.inflate(R.layout.item_action, parent, false);
                viewHolder = new ActionType(view);
                break;

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {

            case ITEM_MULTIPLE:
                DisplayMetrics displayMetrics = new DisplayMetrics();
                activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;

                // img1
                if (mArrayList.size() < 1) return;
                Glide.with(activity)
                        .load(mArrayList.get(0).getPosterPath()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                        .into(((MultipleType) holder).img1);
                ((MultipleType) holder).img1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnclickItemAction.clickItemAction(mArrayList.get(0), position);
                    }
                });

                //img2
                if (mArrayList.size() < 2) {
                    android.view.ViewGroup.LayoutParams layoutParams2 = ((MultipleType) holder).img2.getLayoutParams();
                    layoutParams2.width = (width / 3) - 58;
                    ((MultipleType) holder).img2.setLayoutParams(layoutParams2);
                    ((MultipleType)holder).cardView2.setVisibility(View.INVISIBLE);

                    android.view.ViewGroup.LayoutParams layoutParams3 = ((MultipleType) holder).img3.getLayoutParams();
                    layoutParams3.width = (width / 3) - 58;
                    ((MultipleType) holder).img3.setLayoutParams(layoutParams3);
                    ((MultipleType)holder).cardView3.setVisibility(View.INVISIBLE);
                    return;
                }
                Glide.with(activity)
                        .load(mArrayList.get(1).getPosterPath()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                        .into(((MultipleType) holder).img2);
                ((MultipleType) holder).img2.setOnClickListener(v -> mOnclickItemAction.clickItemAction(mArrayList.get(1), position));
                android.view.ViewGroup.LayoutParams layoutParams2 = ((MultipleType) holder).img2.getLayoutParams();
                layoutParams2.width = (width / 3) - 58;
                ((MultipleType) holder).img2.setLayoutParams(layoutParams2);

                // img3
                if (mArrayList.size() < 3) {
                    android.view.ViewGroup.LayoutParams layoutParams3 = ((MultipleType) holder).img3.getLayoutParams();
                    layoutParams3.width = (width / 3) - 58;
                    ((MultipleType) holder).img3.setLayoutParams(layoutParams3);
                    ((MultipleType)holder).cardView3.setVisibility(View.INVISIBLE);
                    return;
                }
                Glide.with(activity)
                        .load(mArrayList.get(2).getPosterPath()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                        .into(((MultipleType) holder).img3);
                ((MultipleType) holder).img3.setOnClickListener(v -> mOnclickItemAction.clickItemAction(mArrayList.get(2), position));
                android.view.ViewGroup.LayoutParams layoutParams3 = ((MultipleType) holder).img3.getLayoutParams();
                layoutParams3.width = (width / 3) - 58;
                ((MultipleType) holder).img3.setLayoutParams(layoutParams3);
                break;

            case ITEM:
                ItemAdapter itemAdapter = new ItemAdapter(activity, itemArr, mOnclickItemAction);
                itemAdapter.setListener(this);
                ((ActionType) holder).recyclerView.setLayoutManager(new GridLayoutManager(activity, 3));
                ((ActionType) holder).recyclerView.setAdapter(itemAdapter);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_MULTIPLE;
        } else {
            return ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    @Override
    public void onClick(Object item, int position, boolean isChecked) {

    }

    protected class MultipleType extends RecyclerView.ViewHolder {

        private AppCompatImageView img1;
        private AppCompatImageView img2;
        private AppCompatImageView img3;
        private CardView cardView2;
        private CardView cardView3;


        public MultipleType(@NonNull View itemView) {
            super(itemView);
            img1 = itemView.findViewById(R.id.img1);
            img2 = itemView.findViewById(R.id.img2);
            img3 = itemView.findViewById(R.id.img3);
            cardView2 = itemView.findViewById(R.id.cardView2);
            cardView3 = itemView.findViewById(R.id.cardView3);
        }
    }


    protected class ActionType extends RecyclerView.ViewHolder {

        private RecyclerView recyclerView;

        public ActionType(@NonNull View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.recyclerItemAction);
        }
    }

}

// Adapter listView gird view item child
class ItemAdapter extends com.metfone.selfcare.adapter.BaseAdapter<BaseAdapter.ViewHolder, Object> {

    private OnClickGenres listener;
    private ArrayList<?> list;
    private ItemHolder.OnclickItemAction mOnclickItemAction;

    public ItemAdapter(Activity activity, ArrayList<?> list, ItemHolder.OnclickItemAction onclickItemAction) {
        super(activity);
        this.list = list;
        this.mOnclickItemAction = onclickItemAction;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemHolder(layoutInflater.inflate(R.layout.item_action_child, parent, false), listener, activity, list);

    }

    public void setListener(OnClickGenres listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Integer item = (Integer) getItem(position);
        holder.bindData(item, position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnclickItemAction.clickItemAction(list.get(position), position);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }
}


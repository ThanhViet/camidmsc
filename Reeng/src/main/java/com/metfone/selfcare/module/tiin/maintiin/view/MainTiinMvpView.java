package com.metfone.selfcare.module.tiin.maintiin.view;

import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.network.model.Category;

import java.util.ArrayList;

public interface MainTiinMvpView extends MvpView {
    void loadDataSuccess(boolean flag);

    void bindData(ArrayList<Category> categoryResponse);

    void bindDataCategory(String list);
}

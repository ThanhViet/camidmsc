package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCSubModel;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCSubModel extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCSubModel> data;

    public ArrayList<SCSubModel> getData() {
        return data;
    }

    public void setData(ArrayList<SCSubModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCSubModel [data=" + data + "] errror " + getErrorCode();
    }
}

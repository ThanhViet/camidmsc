package com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.presenter;

import com.metfone.selfcare.module.tiin.base.MvpPresenter;

public interface IMainDetailTiinMvpPresenter extends MvpPresenter {
    void getDataSibling(int id, int page, int num);
    void onDestroyView();
    void addItemSeen(String id);
}

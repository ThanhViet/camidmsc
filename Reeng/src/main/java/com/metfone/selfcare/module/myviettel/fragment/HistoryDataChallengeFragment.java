/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/30
 *
 */

package com.metfone.selfcare.module.myviettel.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.myviettel.activity.DataChallengeActivity;
import com.metfone.selfcare.module.myviettel.adapter.HistoryDataChallengeAdapter;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;
import com.metfone.selfcare.module.myviettel.model.DataChallengeProvisional;
import com.metfone.selfcare.module.myviettel.network.ApiCallback;
import com.metfone.selfcare.module.myviettel.network.MyViettelApi;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.IllegalFormatConversionException;
import java.util.Locale;

public class HistoryDataChallengeFragment extends BaseFragment {
    private final int LIMIT = 20;
    private ProgressBar loadingView;
    private View viewEmpty;
    private View viewError;
    private RecyclerView recyclerView;
    private Spinner spDateTime;
    private EditText editStartDate;
    private EditText editEndDate;
    private ArrayList<DataChallenge> datas;
    private HistoryDataChallengeAdapter adapter;
    private ArrayList<String> dataSpinner;
    private ArrayAdapter<String> adapterSpinner;
    private LinearLayoutManager layoutManager;
    private long startTime = 0L;
    private long endTime = 0L;
    private int currentOffset = 0;
    private boolean canLoadMore;
    private boolean isLoading;
    private int currentState = -1;
    private SimpleDateFormat sdfToTitle = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

    public static HistoryDataChallengeFragment newInstance() {
        Bundle args = new Bundle();
        HistoryDataChallengeFragment fragment = new HistoryDataChallengeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "HistoryDataChallengeFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_history_data_challenge;
    }

    private MyViettelApi getMyViettelApi() {
        if (mActivity instanceof DataChallengeActivity) {
            return ((DataChallengeActivity) mActivity).getMyViettelApi();
        }
        return new MyViettelApi();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            loadingView = view.findViewById(R.id.loading_view);
            viewEmpty = view.findViewById(R.id.layout_empty);
            viewError = view.findViewById(R.id.tv_error);
            recyclerView = view.findViewById(R.id.recycler_view);
            spDateTime = view.findViewById(R.id.spinner_datetime);
            editStartDate = view.findViewById(R.id.edit_start_date);
            editEndDate = view.findViewById(R.id.edit_end_date);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (editStartDate != null) {
            editStartDate.setText(getDefaultStartDate());
            editStartDate.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    showDateDialog(editStartDate);
                }
            });
            editStartDate.addTextChangedListener(new TextWatcher() {
                private String oldDate = "";

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String newDate = "";
                    if (s != null) newDate = s.toString().trim();
                    if (!newDate.equals(oldDate) && isVisibleToUser) {
                        onSubmitClick(true);
                    }
                    oldDate = newDate;
                }
            });
        }
        if (editEndDate != null) {
            editEndDate.setText(getDefaultEndDate());
            editEndDate.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    showDateDialog(editEndDate);
                }
            });
            editEndDate.addTextChangedListener(new TextWatcher() {
                private String oldDate = "";

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String newDate = "";
                    if (s != null) newDate = s.toString().trim();
                    if (!newDate.equals(oldDate) && isVisibleToUser) {
                        onSubmitClick(true);
                    }
                    oldDate = newDate;
                }
            });
        }
        if (datas == null) datas = new ArrayList<>();
        adapter = new HistoryDataChallengeAdapter(mActivity);
        adapter.setData(datas);
        layoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        BaseAdapter.setupVerticalRecycler(mActivity, recyclerView, layoutManager, adapter, false);
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (layoutManager != null) {
                        int totalItemCount = layoutManager.getItemCount();
                        int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                        if (totalItemCount == lastVisibleItem + 1) {
                            onLoadMore();
                        }
                    }
                }
            });
        }
        if (spDateTime != null) {
            if (dataSpinner == null) dataSpinner = new ArrayList<>();
            else dataSpinner.clear();
            //todo 0 – Thành công, 1 – Thất Bại, 2 – Đang chờ, 3 – Tất cả
            dataSpinner.add(mActivity.getString(R.string.commission_state_3));
            dataSpinner.add(mActivity.getString(R.string.commission_state_0));
            dataSpinner.add(mActivity.getString(R.string.commission_state_1));
            dataSpinner.add(mActivity.getString(R.string.commission_state_2));
            adapterSpinner = new ArrayAdapter<>(mActivity, android.R.layout.simple_spinner_dropdown_item, dataSpinner);
            adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spDateTime.setAdapter(adapterSpinner);
            spDateTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int newState;
                    if (position == 0)
                        newState = 3;
                    else if (position == 1)
                        newState = 0;
                    else if (position == 2)
                        newState = 1;
                    else if (position == 3)
                        newState = 2;
                    else
                        newState = -1;

                    if (newState != currentState) {
                        currentState = newState;
                        if (isVisibleToUser)
                            onSubmitClick(true);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        if (viewError != null) viewError.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onSubmitClick(true);
            }
        });
    }

    public String getDefaultEndDate() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        return sdfToTitle.format(date);
    }

    public String getDefaultStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date date = calendar.getTime();
        return sdfToTitle.format(date);
    }

    public long convertStringToTime(String text) {
        if (!TextUtils.isEmpty(text)) {
            try {
                Date date = sdfToTitle.parse(text);
                return date.getTime();
            } catch (Exception e) {
            }
        }
        return -1;
    }

    private void showDateDialog(final EditText editText) {
        if (editText == null) return;
        Calendar c = Calendar.getInstance();
        long dateLong = convertStringToTime(editText.getText().toString());
        if (dateLong > 0) {
            c.setTimeInMillis(dateLong);
        }
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        Calendar maxCalender = Calendar.getInstance();
        Context context = new ContextWrapper(getActivity()) {
            private Resources wrappedResources;

            @Override
            public Resources getResources() {
                Resources r = super.getResources();
                if (wrappedResources == null) {
                    wrappedResources = new Resources(r.getAssets(), r.getDisplayMetrics(), r.getConfiguration()) {
                        @NonNull
                        @Override
                        public String getString(int id, Object... formatArgs) throws NotFoundException {
                            try {
                                return super.getString(id, formatArgs);
                            } catch (IllegalFormatConversionException ifce) {
                                Log.e("DatePickerDialogFix", "IllegalFormatConversionException Fixed!", ifce);
                                String template = super.getString(id);
                                template = template.replaceAll("%" + ifce.getConversion(), "%s");
                                return String.format(getConfiguration().locale, template, formatArgs);
                            }
                        }
                    };
                }
                return wrappedResources;
            }
        };

        com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener listener = new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                Calendar cal = Calendar.getInstance();
                cal.set(selectedYear, selectedMonth, selectedDay);
                editText.setText(sdfToTitle.format(cal.getTime()));
            }
        };
        new SpinnerDatePickerDialogBuilder()
                .context(context)
                .callback(listener)
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(mYear, mMonth, mDay)
                .maxDate(maxCalender.get(Calendar.YEAR), maxCalender.get(Calendar.MONTH), maxCalender.get(Calendar.DAY_OF_MONTH))
                .build()
                .show();

    }

    private void loadData(final boolean showLoading) {
        if (isLoading) return;
        if (showLoading) {
            currentOffset = 0;
            if (loadingView != null) loadingView.setVisibility(View.VISIBLE);
            if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        }
        if (viewEmpty != null) viewEmpty.setVisibility(View.GONE);
        if (viewError != null) viewError.setVisibility(View.GONE);
        canLoadMore = false;
        isLoading = true;
        getMyViettelApi().getHistoryDC(startTime, endTime, LIMIT, currentOffset, currentState
                , new ApiCallback<DataChallengeProvisional>() {
                    @Override
                    public void onSuccess(String msg, DataChallengeProvisional result) throws Exception {
                        if (datas == null) datas = new ArrayList<>();
                        if (currentOffset == 0) datas.clear();
                        if (Utilities.notEmpty(result.getData())) {
                            datas.addAll(result.getData());
                            canLoadMore = result.getData().size() >= LIMIT - 1;
                        }
                        if (adapter != null) {
                            adapter.setTotalCommission(result.getTotalCommission());
                            adapter.setTotalTransaction(result.getTotalTransaction());
                            adapter.notifyDataSetChanged();
                        }
                        if (Utilities.isEmpty(datas)) {
                            if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
                            if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                        } else {
                            if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(String s) {
                        if (Utilities.isEmpty(datas)) {
                            if (viewError != null) viewError.setVisibility(View.VISIBLE);
                            if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (loadingView != null) loadingView.setVisibility(View.GONE);
                        isLoading = false;
                        isDataInitiated = true;
                    }
                });
    }

    private void onLoadMore() {
        Log.i(TAG, "onLoadMore nao ...");
        if (canLoadMore) {
            canLoadMore = false;
            currentOffset += LIMIT;
            loadData(false);
        }
    }

    public void onSubmitClick(boolean showToast) {
        if (editStartDate != null && editEndDate != null) {
            String startDateStr = editStartDate.getText().toString();
            String endDateStr = editEndDate.getText().toString();
            if (TextUtils.isEmpty(startDateStr)) {
                editStartDate.requestFocus();
                return;
            }
            if (TextUtils.isEmpty(endDateStr)) {
                editEndDate.requestFocus();
                return;
            }

            Date startDte = null;
            try {
                startDte = sdfToTitle.parse(startDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date endDte = null;
            try {
                endDte = sdfToTitle.parse(endDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (startDte == null || endDte == null) return;
            if (startDte.getTime() > endDte.getTime()) {
                if (showToast) mActivity.showToast(R.string.msg_error_pick_date_2);
                editStartDate.requestFocus();
                return;
            }
            if ((endDte.getTime() - startDte.getTime()) > 2678400000L) {
                if (showToast) mActivity.showToast(R.string.msg_error_pick_date_mvt_dc);
                editStartDate.requestFocus();
                return;
            }
            startTime = startDte.getTime();
            endTime = endDte.getTime();
        }
        isLoading = false;
        loadData(true);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad())
            onSubmitClick(false);
    }
}

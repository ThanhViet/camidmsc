package com.metfone.selfcare.module.keeng.holder;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.adapter.home.SlidingBannerAdapter;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.ui.view.indicator.OverflowPagerIndicator;
import com.metfone.selfcare.ui.view.indicator.SimpleSnapHelper;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

public class SlidingBannerHolder extends BaseViewHolder {

    private RecyclerView recycler;
    private OverflowPagerIndicator indicator;

    private ArrayList<Object> data;
    private SlidingBannerAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int next;

    public SlidingBannerHolder(Context context, AbsInterface.OnItemListener listener, View view) {
        super(view);
        recycler = view.findViewById(R.id.recycler_top);
        indicator = view.findViewById(R.id.indicator_top);
        data = new ArrayList<>();
        layoutManager = new CustomLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        adapter = new SlidingBannerAdapter(context, listener);
        recycler.setLayoutManager(layoutManager);
        recycler.setNestedScrollingEnabled(false);
        recycler.setAdapter(adapter);
        recycler.setHasFixedSize(true);
        indicator.attachToRecyclerView(recycler);
        new SimpleSnapHelper(indicator).attachToRecyclerView(recycler);
        recycler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (recycler == null) return;
                if (Utilities.notEmpty(data)) {
                    try {
                        int current = layoutManager.findFirstVisibleItemPosition();
                        if (next == 0) {
                            next = 1;
                        } else if (current == 0 && next == -1) {
                            next = 1;
                        } else if (current == data.size() - 1 && next == 1) {
                            next = -1;
                        }
                        int positionNext = current + next;
                        layoutManager.smoothScrollToPosition(recycler, null, positionNext);
                        indicator.onPageSelected(positionNext);
                    } catch (Exception e) {
                    }
                }
                recycler.postDelayed(this, Constants.CHANGE_BANNER_TIME);
            }
        }, Constants.CHANGE_BANNER_TIME);
    }

    public void bindData(List<AllModel> items) {
        data.clear();
        data.addAll(items);
        adapter.bindData(data);
    }

}
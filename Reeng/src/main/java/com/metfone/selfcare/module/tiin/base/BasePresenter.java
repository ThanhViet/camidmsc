package com.metfone.selfcare.module.tiin.base;

public class BasePresenter implements MvpPresenter {
    public static final String TAG = "BasePresenter";
    private MvpView mMvpView;


    @Override
    public void onAttach(MvpView mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public MvpView getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    @Override
    public void handleApiError(Exception error) {
    }

    @Override
    public void setUserAsLoggedOut() {
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.onAttach(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }
}

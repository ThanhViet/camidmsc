/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/3
 *
 */

package com.metfone.selfcare.module.tab_home.holder;

import androidx.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickSliderBanner;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;

import butterknife.BindView;

public class SlideBannerDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;

    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;

    @BindView(R.id.iv_live_stream)
    @Nullable
    View ivLiveStream;

    private Object data;

    public SlideBannerDetailHolder(View view, final OnClickSliderBanner listener) {
        super(view);
        if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null && data != null) {
                    listener.onClickSliderBannerItem(data, getAdapterPosition());
                }
            }
        });
    }

    @Override
    public void bindData(Object item, int position) {
        data = item;
        if (item instanceof Content) {
            Content model = (Content) item;
            if (tvTitle != null) tvTitle.setText(model.getName());
            if (model.getResImage() > 0)
                ImageBusiness.setResource(ivCover, model.getResImage());
            else
                ImageBusiness.setBannerHome(model.getImage(), ivCover);
            if (ivLiveStream != null)
                ivLiveStream.setVisibility(model.isLive() ? View.VISIBLE : View.GONE);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel provisional = (TabHomeModel) item;
            if (provisional.getObject() instanceof Content) {
                Content model = (Content) provisional.getObject();
                if (tvTitle != null) tvTitle.setText(model.getName());
                if (model.getResImage() > 0)
                    ImageBusiness.setResource(ivCover, model.getResImage());
                else
                    ImageBusiness.setBannerHome(model.getImage(), ivCover);
                if (ivLiveStream != null)
                    ivLiveStream.setVisibility(model.isLive() ? View.VISIBLE : View.GONE);
            } else {
                data = null;
                if (ivLiveStream != null) ivLiveStream.setVisibility(View.GONE);
            }
        } else if (item instanceof Movie) {
            Movie model = (Movie) item;
            if (tvTitle != null) tvTitle.setText(model.getName());
            ImageBusiness.setCoverMovie(model.getImagePath(), ivCover);
            if (ivLiveStream != null) ivLiveStream.setVisibility(View.GONE);
        }
    }

}
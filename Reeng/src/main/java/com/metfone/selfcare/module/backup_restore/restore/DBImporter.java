package com.metfone.selfcare.module.backup_restore.restore;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.backup_restore.BackupMessageModel;
import com.metfone.selfcare.module.backup_restore.BackupSecurityHelper;
import com.metfone.selfcare.module.backup_restore.BackupThreadMessageModel;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class DBImporter {
    public static final String TAG = DBImporter.class.getSimpleName();

    private static DBImporter mInstance = null;

    public static final String BACKUP_FILE_NAME = "backup_message.txt";
    public static final String BACKUP_ENCRYPT_NAME = "backup_message";
    public static final String BACKUP_ZIP_NAME = "backup_message.zip";

    private static final int LIMIT_COUNT_PER_QUERY_SESSION = 1000;
    private DBImportAsync mDbImportAsync = null;

    private DownloadTask mDowloadBackupFileTask = null;

    private DBImporter() {
        Log.i(TAG, "singleton creator!!");
    }

    protected static DBImporter getInstance() {
        if (mInstance == null) {
            synchronized (DBImporter.class) {
                if (mInstance == null) {
                    mInstance = new DBImporter();
                }
            }
        }
        return mInstance;
    }

    private static int getTotalLineNumberFile(File file) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            int lines = 0;
            while (reader.readLine() != null) lines++;
            reader.close();
            return lines;
        } catch (Exception e) {
            return 0;
        }
    }

    //import message text and image
    public void startRestore(final RestoreProgressListener listener, String urlPath) {
        if (isRunning()) return;
        clearData();
        mDowloadBackupFileTask = new DownloadTask(listener, new DownloadListener() {
            @Override
            public void onSuccess() {
                mDbImportAsync = new DBImportAsync(listener);
                mDbImportAsync.execute();
                mDowloadBackupFileTask = null;
            }

            @Override
            public void onFail() {
                mDowloadBackupFileTask = null;
            }
        });
        mDowloadBackupFileTask.execute(urlPath);
    }

    interface DownloadListener {
        void onSuccess();

        void onFail();
    }

    public boolean isRunning() {
        return mDbImportAsync != null && mDbImportAsync.isRunning() || mDowloadBackupFileTask != null && mDowloadBackupFileTask.isRunning();
    }

    public interface RestoreProgressListener {
        void onStartDownload();

        void onDownloadProgress(int percent);

        void onDownloadComplete();

        void onDowloadFail(String message);

        void onStartRestore();

        void onRestoreProgress(int percent);

        void onRestoreComplete(int messageCount, int threadMessageCount);

        void onRestoreFail(String message);
    }

    public void cancelRestore() {
        try {
            if (mDowloadBackupFileTask != null && mDowloadBackupFileTask.isRunning()) {
                mDowloadBackupFileTask.cancel(true);
                mDowloadBackupFileTask = null;
            }

            if (mDbImportAsync != null && mDbImportAsync.isRunning()) {
                mDbImportAsync.cancel(true);
                mDbImportAsync = null;
            }
            clearData();
        } catch (Exception e) {
        }
    }

    public static void clearData() {
        try {
            File txtFile = new File(ApplicationController.self().getFilesDir(), BACKUP_FILE_NAME);
            File zipFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ZIP_NAME);
            File encryptFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ENCRYPT_NAME);
            if (txtFile.exists()) {
                txtFile.delete();
            }

            if (zipFile.exists()) {
                zipFile.delete();
            }

            if (encryptFile.exists()) {
                encryptFile.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "clearDataBeforeExportDB: Exception", e);
        }
    }

    private static class DBImportAsync extends AsyncTask<Void, Integer, Boolean> {
        public DBImportAsync(RestoreProgressListener listener) {
            mListener = listener;
        }

        private boolean isRunning = false;
        RestoreProgressListener mListener;
        int mMessageCount = 0;
        int mThreadMessageCount = 0;

        @Override
        protected void onPreExecute() {
            if (mListener != null) {
                mListener.onStartRestore();
            }
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            isRunning = true;

            try {

                if (!hasFile(BACKUP_ENCRYPT_NAME)) {
                    // download backup file
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBImporter: Khong co backup file sau khi download");
                    }
                    return false;
                }
                // decrypt file by AES
                File encrytedFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ENCRYPT_NAME);
                if (!encrytedFile.exists()) {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBImporter: Khong co file ma hoa");
                    }
                    return false;
                }
                InputStream inputStream = new FileInputStream(encrytedFile);
                byte[] data = BackupSecurityHelper.getBytesFromInputStream(inputStream);
                byte[] encrypted = BackupSecurityHelper.AESCrypt.getInStance().decrypt(data);
                File zipFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ZIP_NAME);
                FileOutputStream out = new FileOutputStream(zipFile);
                out.write(encrypted);
                inputStream.close();
                out.close();

                /*// decrypt file by RSA
                File encrytedFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ENCRYPT_NAME);
                if (!encrytedFile.exists()) return false;
                InputStream inputStream = new FileInputStream(encrytedFile);
                byte[] data = BackupSecurityHelper.getBytesFromInputStream(inputStream);
                byte[] encrypted = BackupSecurityHelper.RSAUtils.decryptPrivateKey(data);
                File zipFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ZIP_NAME);
                FileOutputStream out = new FileOutputStream(zipFile);
                out.write(encrypted);
                inputStream.close();
                out.close();*/

                //unzip file
                if (!zipFile.exists()) {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBImporter: Khong co file sau khi giai ma");
                    }
                    return false;
                }
                RestoreUtils.unzipToInternalStorage(zipFile);

                //import to DB
                File messageFile = new File(ApplicationController.self().getFilesDir(), BACKUP_FILE_NAME);
                if (!messageFile.exists()) {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBImporter: Khong co file sau khi giai nen");
                    }
                    return false;
                }
                //deleteDB
                ApplicationController.self().getMessageBusiness().deleteAllMessages();

                int totalLine = getTotalLineNumberFile(messageFile);
                int currentPercent = 0;
                FileInputStream message_fis = new FileInputStream(messageFile);//ApplicationController.self().openFileInput(BACKUP_FILE_NAME);
                InputStreamReader message_isr = new InputStreamReader(message_fis);
                BufferedReader message_bufferedReader = new BufferedReader(message_isr);
                String line = message_bufferedReader.readLine();
                Gson gson = new Gson();
                while (!TextUtils.isEmpty(line)) {
                    //write database
                    if (line.startsWith("1")) {
                        line = line.replaceFirst("1", "");
                        BackupThreadMessageModel threadMessageModel = gson.fromJson(line, BackupThreadMessageModel.class);
                        ThreadMessage threadMessage = new ThreadMessage();
                        threadMessage = RestoreUtils.convertToThreadMessage(threadMessage, threadMessageModel);
                        if (threadMessage != null) {
                            ApplicationController app = ApplicationController.self();
                            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                                    && threadMessageModel.getIsStranger() == 1) {
                                app.getStrangerBusiness()
                                        .createOrUpdateStrangerPhoneNumber(threadMessage.getSoloNumber(),
                                                app.getReengAccountBusiness().getUserName(), threadMessage.getThreadName(),
                                                null, "music_stranger", true);
                                Log.i(TAG, "create stranger " + threadMessage.getSoloNumber() + " name: " + threadMessage.getThreadName());
                            } else if(threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT){
                                threadMessage.setState(ThreadMessageConstant.STATE_NEED_GET_INFO);
                            }
                            app.getMessageBusiness().insertRestoredThreadMessage(threadMessage);
                        }
                        //message
                        line = message_bufferedReader.readLine();
                        long lastID = -1;
                        ArrayList<BackupMessageModel> list = new ArrayList<>();
                        while (!TextUtils.isEmpty(line) && !line.startsWith("1")) {
                            //write database
                            BackupMessageModel messageModel = gson.fromJson(line, BackupMessageModel.class);
                            if (messageModel != null) {
                                list.add(messageModel);
                            }
                            if (list.size() >= LIMIT_COUNT_PER_QUERY_SESSION) {
                                lastID = ApplicationController.self().getMessageBusiness().insertRestoreMessageList(list, threadMessage.getId());
                                list.clear();
                            }
                            mMessageCount++;
                            if (mThreadMessageCount + mMessageCount <= totalLine) {
                                int percent = (int) (((mThreadMessageCount + mMessageCount + 1) * 100.0f) / totalLine);
                                if (percent > currentPercent) {
                                    currentPercent = percent;
                                    publishProgress(currentPercent);
                                }
                            }
                            line = message_bufferedReader.readLine();
                        }
                        if (list != null && list.size() > 0) {
                            lastID = ApplicationController.self().getMessageBusiness().insertRestoreMessageList(list, threadMessage.getId());
                            list.clear();
                        }

                        if (lastID > -1) {
                            threadMessage.setLastMessageId((int) lastID);
                            ApplicationController.self().getMessageBusiness().updateThreadMessageDataSource(threadMessage);
                            mThreadMessageCount++;
                        }
                    }
                }
                message_bufferedReader.close();
                message_isr.close();
                message_fis.close();

                messageFile.delete();
                zipFile.delete();
                encrytedFile.delete();
            } catch (Exception e) {
                Log.e(TAG, "Has exception while restoring", e);
                if (LogDebugHelper.getInstance() != null) {
                    LogDebugHelper.getInstance().logDebugContent("DBImporter: Import loi: " + e.toString());
                }
                return false;
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (mListener != null && values != null && values.length > 0) {
                int percent = values[0];
                mListener.onRestoreProgress(percent);
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            isRunning = false;
            if (aBoolean) {
                if (mListener != null) {
                    mListener.onRestoreComplete(mMessageCount, mThreadMessageCount);
                    mListener = null;
                }
            } else {
                if (mListener != null) {
                    mListener.onRestoreFail("Restore fail!!!");
                    mListener = null;
                }
            }
        }

        boolean isRunning() {
            return isRunning;
        }

        @Override
        protected void onCancelled() {
            isRunning = false;
            super.onCancelled();
            if (mListener != null) {
                mListener.onRestoreFail("Restore was cancelled!");
                mListener = null;
            }
        }

        @Override
        protected void onCancelled(Boolean aBoolean) {
            isRunning = false;
            super.onCancelled(aBoolean);
            if (mListener != null) {
                mListener.onRestoreFail("Restore was cancelled!");
                mListener = null;
            }
        }

        boolean hasFile(String fileName) {
            try {
                File file = new File(ApplicationController.self().getFilesDir(), fileName);
                return file.exists();
            } catch (Exception e) {
                return false;
            }
        }

    }

    private static class DownloadTask extends AsyncTask<String, Integer, Boolean> {

        private boolean isRunning;

        public DownloadTask(RestoreProgressListener listener, DownloadListener downloadListener) {
            mListener = listener;
            mDownloadListener = downloadListener;
        }

        DownloadListener mDownloadListener;
        RestoreProgressListener mListener;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mListener != null) {
                mListener.onStartDownload();
            }
        }

        @Override
        protected Boolean doInBackground(String... sUrl) {
            isRunning = true;
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "downloadBackupFile: " + "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage());
                    connection.disconnect();
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBImporter: Loi ket noi :" + connection.getResponseCode());
                    }
                    return false;
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                File file = new File(ApplicationController.self().getFilesDir(), BACKUP_ENCRYPT_NAME);
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                output = new FileOutputStream(file);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                Log.e(TAG, "downloadBackupFile: " + e.toString());
                if (LogDebugHelper.getInstance() != null) {
                    LogDebugHelper.getInstance().logDebugContent("DBImporter: dowloadBackupFile :" + e.toString());
                }
                return false;
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                    if (connection != null)
                        connection.disconnect();
                } catch (Exception ignored) {
                }
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            if (mListener != null && values != null && values.length > 0) {
                int percent = values[0];
                mListener.onDownloadProgress(percent);
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                if (mListener != null) {
                    mListener.onDownloadComplete();
                }
                if (mDownloadListener != null) {
                    mDownloadListener.onSuccess();
                }
            } else {
                if (mDownloadListener != null) {
                    mDownloadListener.onFail();
                }
                if (mListener != null) {
                    mListener.onDowloadFail("Download false");
                }
            }
            mDownloadListener = null;
            mListener = null;
            isRunning = false;
        }

        boolean isRunning() {
            return isRunning;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            isRunning = false;
            mDownloadListener = null;
            mListener = null;
        }

        @Override
        protected void onCancelled(Boolean aBoolean) {
            super.onCancelled(aBoolean);
            isRunning = false;
            mDownloadListener = null;
            mListener = null;
        }
    }

    public static long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize, availableBlocks;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            blockSize = stat.getBlockSizeLong();
            availableBlocks = stat.getAvailableBlocksLong();
        } else {
            blockSize = stat.getBlockSize();
            availableBlocks = stat.getAvailableBlocks();
        }
        return availableBlocks * blockSize;
    }
}

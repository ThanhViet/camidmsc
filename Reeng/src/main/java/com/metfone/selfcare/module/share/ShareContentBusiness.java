/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/15
 *
 */

package com.metfone.selfcare.module.share;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Telephony;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.TransferFileBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TagHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.module.share.adapter.ShareContentAdapter;
import com.metfone.selfcare.module.share.adapter.ShareImageOnSocialAdapter;
import com.metfone.selfcare.module.share.listener.GetAllContactsListener;
import com.metfone.selfcare.module.share.listener.SearchContactsListener;
import com.metfone.selfcare.module.share.listener.ShareBusinessListener;
import com.metfone.selfcare.module.share.listener.ShareContentListener;
import com.metfone.selfcare.module.share.listener.ShareImagesOnFacebookListener;
import com.metfone.selfcare.module.share.listener.ShareImagesOnOtherAppListener;
import com.metfone.selfcare.module.share.task.DownloadImagesForShareFacebookTask;
import com.metfone.selfcare.module.share.task.DownloadImagesForShareOtherAppTask;
import com.metfone.selfcare.module.share.task.GetAllContactsTask;
import com.metfone.selfcare.module.share.task.SearchContactsTask;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.snackbar.CustomSnackBar;
import com.metfone.selfcare.ui.snackbar.Gravity;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static com.metfone.selfcare.business.UserInfoBusiness.isLogin;

public final class ShareContentBusiness implements GetAllContactsListener, ShareContentListener
        , TransferFileBusiness.UploadImageSocialOnMediaListener, SearchContactsListener {
    public static final int TYPE_SHARE_CONTENT = 1;
    public static final int TYPE_FORWARD_MESSAGE = 2;
    public static final int TYPE_SHARE_FROM_OTHER_APP = 3;
    public static final int TYPE_SHARE_FROM_DEEP_LINK = 4;
    public static final int TYPE_SHARE_TO_FRIEND = 5;
    public static final int TYPE_SHARE_FROM_IMAGE_PREVIEW = 6;
    public static final int TIME_DELAY_DISMISS = 250;
    private static final String TAG = "ShareContentBusiness";
    private BaseSlidingFragmentActivity activity;
    private String link;
    private String serviceType;
    private String titleShare;
    private ArrayList<ContactProvisional> listContacts;
    private ArrayList<ContactProvisional> data;
    private ShareContentAdapter adapter;
    private Object content;
    private GetAllContactsTask getAllContactsTask;
    private ApplicationController application;
    private ShareBottomDialog dialogChooseContacts;
    private ReengSearchView searchView;
    private View layoutToolbar;
    private SearchContactsTask searchContactsTask;
    private Handler handlerSharing;
    private HashMap<String, ContactProvisional> mapState = new HashMap<>();
    private ShareBottomDialog dialogPostOnSocial;
    private View loadingView;
    private ImageView ivCoverContent;
    private TextView tvTitleContent;
    private View previewContentView;
    private TagsCompletionView editStatus;
    private TextView tvTitleDialog;
    private View sheetView;
    private FeedContent currentFeedContent;
    private boolean enablePostContent = true;
    private View btnPostContent;
    private boolean isLandscape;
    private boolean isShareImage;
    private boolean isMaxHeightPopup;
    private boolean isSearchViewFocus;
    private int heightScreen, widthScreen;
    private String keySearch;
    private boolean isNetworkUrl;
    private ReengMessageConstant.MessageType messageType;
    private ArrayList<FeedContent.ImageContent> listImages;
    private ArrayList<ReengMessage> listMessages;
    private boolean hasPostOnSocial;
    private int typeSharing = 0;
    private String titleDialogChooseContact;
    private BaseMPConfirmDialog confirmDialogMessenger;
    private Runnable hideKeyboard = new Runnable() {
        @Override
        public void run() {
            Log.i(TAG, "Runnable hideKeyboard");
            InputMethodUtils.hideSoftKeyboard(searchView, activity);
            InputMethodUtils.hideSoftKeyboard(editStatus, activity);
            InputMethodUtils.hideSoftKeyboard(activity);
        }
    };
    private KeyboardVisibilityEventListener keyboardVisibilityListener = new KeyboardVisibilityEventListener() {
        @Override
        public void onVisibilityChanged(boolean isOpen) {
            Log.d(TAG, "KeyboardVisibilityEvent isOpen: " + isOpen);
            if (isOpen && isSearchViewFocus && !isMaxHeightPopup && dialogChooseContacts != null && dialogChooseContacts.isShowing()) {
                BottomSheetBehavior behavior = dialogChooseContacts.getBottomSheetBehavior();
                if (behavior != null) {
                    if (layoutToolbar != null) {
                        layoutToolbar.setVisibility(View.VISIBLE);
                        layoutToolbar = null;
                    }
                    behavior.setPeekHeight(Math.max(widthScreen, heightScreen));
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                isMaxHeightPopup = true;
            }
        }
    };
    private Comparator comparatorNameUnmark;
    private Comparator comparatorKeySearchUnmark;
    private Comparator comparatorContacts;
    private Comparator comparatorThreadMessages;
    private boolean isPlayingState;
    // phai set trang thai playing cua player video vao truoc
    private ShareBusinessListener shareBusinessListener;
    private Runnable runnableDismiss = () -> {
        if (shareBusinessListener != null)
            shareBusinessListener.onDismissShareDialog(isPlayingState);
    };
    private CustomSnackBar oldSnackBar;

    public ShareContentBusiness(BaseSlidingFragmentActivity activity, Object object) {
        Log.e(TAG, "ShareContentBusiness object: " + object);
        this.application = ApplicationController.self();
        this.activity = activity;
        this.content = object;
        this.link = null;
        this.serviceType = null;
        this.isShareImage = false;
        this.titleShare = activity.getResources().getString(R.string.share);
        if (object instanceof String) {
            link = (String) object;
        } else if (object instanceof Video) {
            link = ((Video) object).getLink();
            if (((Video) object).isMovie())
                link = swapDomainMovies(application, link);
            else
                link = swapDomainMusic(application, link);
            titleShare = activity.getResources().getString(R.string.title_share_video_other);
            serviceType = "video";
        } else if (object instanceof PlayListModel) {
            link = ((PlayListModel) object).getUrl();
            link = swapDomainMusic(application, link);
        } else if (object instanceof AllModel) {
            link = ((AllModel) object).getUrl();
            link = swapDomainMusic(application, link);
        } else if (object instanceof Movie) {
            link = ((Movie) object).getLinkWap();
            link = swapDomainMovies(application, link);
        } else if (object instanceof MovieWatched) {
            link = ((MovieWatched) object).getLink();
            link = swapDomainMovies(application, link);
        } else if (object instanceof NewsModel) {
            link = ShareUtils.getLinkShare((NewsModel) object);
        } else if (object instanceof TiinModel) {
            link = ShareUtils.getLinkShareTiin((TiinModel) object);
        } else if (object instanceof MediaModel) {
            link = ((MediaModel) object).getUrl();
            link = swapDomainMusic(application, link);
        } else if (object instanceof ReengMessage) {
            ReengMessage msg = (ReengMessage) object;
            messageType = msg.getMessageType();
            if (messageType == ReengMessageConstant.MessageType.text) {
                ArrayList<TagMocha> listTagForward = msg.getListTagContent();
                link = TagHelper.getTextTagCopy(msg.getContent(), listTagForward, application);
            } else if (messageType == ReengMessageConstant.MessageType.image) {
                isShareImage = true;
                float ratio = 1;
                try {
                    ratio = Float.parseFloat(msg.getVideoContentUri());
                } catch (Exception e) {//
                }
                FeedContent.ImageContent imageContent = new FeedContent.ImageContent(msg.getFileId(), msg.getDirectLinkMedia(), msg.getDirectLinkMedia(), ratio);
                imageContent.setFilePath(msg.getFilePath());
                listImages = new ArrayList<>();
                listImages.add(imageContent);
                link = msg.getFilePath();
            } else if (messageType == ReengMessageConstant.MessageType.shareContact) {
                link = msg.getContent() + " " + msg.getFileName();
            } else if (messageType == ReengMessageConstant.MessageType.shareLocation) {
                //address: msg.getContent()
                //latitude: msg.getFilePath()
                //longitude: msg.getImageUrl()
                String googleMapUrl = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.LOCATION_GOOGLE_MAP_URL);
                if (Utilities.notEmpty(googleMapUrl)) {
                    link = googleMapUrl.replace("latitude_param", msg.getFilePath());
                    link = link.replace("longitude_param", msg.getImageUrl());
                } else {
                    link = msg.getContent();
                }
            } else if (messageType == ReengMessageConstant.MessageType.inviteShareMusic) {
                MediaModel mediaModel = msg.getSongModel(application.getMusicBusiness());
                if (mediaModel != null) {
                    link = mediaModel.getUrl();
                }
            } else if (messageType == ReengMessageConstant.MessageType.watch_video) {
                MediaModel mediaModel = msg.getSongModel(application.getMusicBusiness());
                if (mediaModel != null) {
                    link = mediaModel.getUrl();
                }
            }
        } else if (object instanceof FeedModelOnMedia) {
            FeedModelOnMedia feed = (FeedModelOnMedia) object;
            FeedContent feedContent = feed.getFeedContent();
            if (feedContent != null) {
                String itemType = feedContent.getItemType();
                String itemSubType = feedContent.getItemSubType();
                if (FeedContent.ITEM_TYPE_SONG.equals(itemType)
                        || FeedContent.ITEM_TYPE_ALBUM.equals(itemType)
                        || FeedContent.ITEM_TYPE_VIDEO.equals(itemType)
                        || FeedContent.ITEM_TYPE_NEWS.equals(itemType)
                        || FeedContent.ITEM_TYPE_AUDIO.equals(itemType)
                        || FeedContent.ITEM_TYPE_FILM.equals(itemType)
                ) {
                    link = feedContent.getLink();
                } else if (FeedContent.ITEM_TYPE_IMAGE.equals(itemType)) {
                    link = feedContent.getUrl();
                } else if (FeedContent.ITEM_TYPE_TOTAL.equals(itemType)) {
                    link = feedContent.getOpenLink();
                } else if (FeedContent.ITEM_TYPE_BANNER.equals(itemType)) {
                    link = feedContent.getUrl();
                } else if (FeedContent.ITEM_TYPE_PROFILE_AVATAR.equals(itemType)) {
                    if (Utilities.notEmpty(feedContent.getListImage())) {
                        isShareImage = true;
                        listImages = feedContent.getListImage();
                        FeedContent.ImageContent imageContent = listImages.get(0);
                        if (imageContent != null)
                            link = imageContent.getImageUrl(application);
                    }

                } else if (FeedContent.ITEM_TYPE_PROFILE_COVER.equals(itemType)) {
                    if (Utilities.notEmpty(feedContent.getListImage())) {
                        isShareImage = true;
                        listImages = feedContent.getListImage();
                        FeedContent.ImageContent imageContent = listImages.get(0);
                        if (imageContent != null)
                            link = imageContent.getImageUrl(application);
                    }
                } else if (FeedContent.ITEM_TYPE_PROFILE_ALBUM.equals(itemType)) {
                    if (Utilities.notEmpty(feedContent.getListImage())) {
                        isShareImage = true;
                        listImages = feedContent.getListImage();
                        FeedContent.ImageContent imageContent = listImages.get(0);
                        if (imageContent != null)
                            link = imageContent.getImageUrl(application);
                    }
                } else if (FeedContent.ITEM_TYPE_PROFILE_STATUS.equals(itemType)) {
                    link = feedContent.getDescription();
                } else if (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType)) {
                    if (FeedContent.ITEM_SUB_TYPE_SOCIAL_AUDIO.equals(itemSubType)) {
                        link = feedContent.getContentUrl();
                    } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_VIDEO.equals(itemSubType)) {
                        link = feedContent.getContentUrl();
                    } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_CHANNEL.equals(itemSubType)) {
                        link = feedContent.getContentUrl();
                    } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_MOVIE.equals(itemSubType)) {
                        link = feedContent.getContentUrl();
                    } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType)) {
                        link = feedContent.getContentUrl();
                    } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(itemSubType)) {
                        link = feedContent.getContentStatus();
                    } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_PUBLISH_VIDEO.equals(itemSubType)) {
                        link = feedContent.getContentUrl();
                    } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(itemSubType)) {
                        if (Utilities.notEmpty(feedContent.getListImage())) {
                            isShareImage = true;
                            listImages = feedContent.getListImage();
                            FeedContent.ImageContent imageContent = listImages.get(0);
                            if (imageContent != null)
                                link = imageContent.getImageUrl(application);
                        }
                    }
                }
            }
        } else if (object instanceof MoviePagerModel) {
            Object tmp = ((MoviePagerModel) object).getObject();
            if (tmp instanceof Movie) {
                link = ((Movie) tmp).getLinkWap();
                link = swapDomainMovies(application, link);
            }
        }

        isNetworkUrl = URLUtil.isNetworkUrl(link);
        init();
    }

    public ShareContentBusiness(BaseSlidingFragmentActivity activity, ArrayList<ReengMessage> listMsg, boolean hasPostOnSocial) {
        Log.e(TAG, "ShareContentBusiness listMsg: " + listMsg);
        this.application = ApplicationController.self();
        this.activity = activity;
        this.listMessages = listMsg;
        this.hasPostOnSocial = hasPostOnSocial;
        init();
    }

    private static String swapDomainMovies(ApplicationController application, String url) {
        String result = url;
//        if (application != null) {
//            String domain = TextHelper.getDomain(url);
//            String domainOriginal = "";
//            String domainSwap = "";
//            if (Utilities.notEmpty(domain)) {
//                ContentConfigBusiness configBusiness = application.getConfigBusiness();
//                domainOriginal = configBusiness.getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_KMOVIES_SHARING_ORIGINAL);
//                domainSwap = configBusiness.getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_KMOVIES_SHARING_SWAP);
//                if (Utilities.notEmpty(domainOriginal) && Utilities.notEmpty(domainSwap)) {
//                    String[] domainOriginalArr = domainOriginal.split("\\|");
//                    for (String tmp : domainOriginalArr) {
//                        Log.d(TAG, "swapDomainMovies domain: " + tmp);
//                        if (domain.equals(tmp)) {
//                            result = result.replaceFirst(domain, domainSwap);
//                            break;
//                        }
//                    }
//                }
//            }
//            Log.e(TAG, "swapDomainMovies domain: " + domain + ", domainOriginal: " + domainOriginal + ", domainSwap: " + domainSwap + "\nresult: " + result);
//        }
        result = result.replace("5dmax.com.kh", "5dmax.mocha.com.kh/index.php");
        if (result.charAt(result.length() - 1) == '/') {
            result = result.substring(0, result.length() - 1);
        }

        return result;
    }

    private static String swapDomainMusic(ApplicationController application, String url) {
        String result = url;
        if (application != null) {
            String domain = TextHelper.getDomain(url);
            String domainOriginal = "";
            String domainSwap = "";
            if (Utilities.notEmpty(domain)) {
                ContentConfigBusiness configBusiness = application.getConfigBusiness();
                domainOriginal = configBusiness.getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_KMUSIC_SHARING_ORIGINAL);
                domainSwap = configBusiness.getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_KMUSIC_SHARING_SWAP);
                if (Utilities.notEmpty(domainOriginal) && !"-".equals(domainOriginal)
                        && Utilities.notEmpty(domainSwap) && !"-".equals(domainSwap)) {
                    String[] domainOriginalArr = domainOriginal.split("\\|");
                    for (String tmp : domainOriginalArr) {
                        Log.d(TAG, "swapDomainMusic domain: " + tmp);
                        if (domain.equals(tmp)) {
                            result = result.replaceFirst(domain, domainSwap);
                            break;
                        }
                    }
                }
            }
            Log.e(TAG, "swapDomainMusic domain: " + domain + ", domainOriginal: " + domainOriginal + ", domainSwap: " + domainSwap + "\nresult: " + result);
        }
        return result;
    }

    private void init() {
        data = new ArrayList<>();
        this.widthScreen = ScreenManager.getWidth(activity);
        this.heightScreen = ScreenManager.getHeight(activity);
        this.isLandscape = widthScreen > heightScreen;
        this.adapter = new ShareContentAdapter(activity, isLandscape, messageType, typeSharing);
        this.adapter.setListener(this);
        this.adapter.setItems(data);
        if (tvTitleDialog != null && Utilities.notEmpty(titleDialogChooseContact))
            tvTitleDialog.setText(titleDialogChooseContact);
        handlerSharing = new Handler(Looper.getMainLooper());
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "width:" + widthScreen);
            Log.d(TAG, "height:" + heightScreen);
            Log.d(TAG, "isLandscape:" + isLandscape);
            Log.d(TAG, "orientation:" + activity.getResources().getConfiguration().orientation);
            Log.d(TAG, "link:" + link);
            ScreenManager.isLandscape(activity);
        }
    }

    public void showPopupShareContent() {
        dismissChooseContacts();
        if (activity == null || activity.isFinishing()) return;
        if (isShareImage && Utilities.isEmpty(listImages)) return;
        else if (!isShareImage && TextUtils.isEmpty(link)) return;
        if (handlerSharing != null && runnableDismiss != null) {
            handlerSharing.removeCallbacks(runnableDismiss);
        }
        dialogChooseContacts = new ShareBottomDialog(activity);
        dialogChooseContacts.setKeyboardVisibilityListener(keyboardVisibilityListener);
        if (isShareImage) {
            sheetView = activity.getLayoutInflater().inflate(isLandscape ? R.layout.dialog_share_image_mocha_land
                    : R.layout.dialog_share_image_mocha, null, false);
        } else if (isNetworkUrl) {
            sheetView = activity.getLayoutInflater().inflate(isLandscape ? R.layout.dialog_share_content_land
                    : R.layout.dialog_share_content, null, false);
        } else {
            sheetView = activity.getLayoutInflater().inflate(isLandscape ? R.layout.dialog_share_text_land
                    : R.layout.dialog_share_text, null, false);
        }
        layoutToolbar = sheetView.findViewById(R.id.layout_toolbar);
        tvTitleDialog = sheetView.findViewById(R.id.tv_title);
        View btnClose = sheetView.findViewById(R.id.button_close);
        View btnNewGroup = sheetView.findViewById(R.id.button_new_group);
        View btnSocial = sheetView.findViewById(R.id.button_post_on_social);
        View btnCopy = sheetView.findViewById(R.id.button_copy_link);
        View btnFacebook = sheetView.findViewById(R.id.button_share_facebook);
        View btnOther = sheetView.findViewById(R.id.button_share_with_other_app);
        searchView = sheetView.findViewById(R.id.search_view);
        loadingView = sheetView.findViewById(R.id.loading);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        if (btnNewGroup != null) btnNewGroup.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                UserInfoBusiness userInfoBusiness = new UserInfoBusiness(activity);
                if (isLogin() == EnumUtils.LoginVia.NOT) {
                    userInfoBusiness.showLoginDialog(activity, R.string.info, R.string.content_popup_login);
                } else if (StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getJidNumber())) {
                    userInfoBusiness.showAddPhoneDialog(activity, R.string.info, R.string.content_popup_add_phone);
                } else {
                    createNewGroup();
                }
            }
        });
        if (btnSocial != null) btnSocial.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                // share to app default sms

                sendSMS(link);
                dismissAll();
            }
        });
        if (btnCopy != null) btnCopy.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity != null) {
                    TextHelper.copyToClipboard(activity, link);
                    activity.showToast(isNetworkUrl ? R.string.copy_link_successfully : R.string.copy_to_clipboard);
                    logClickCopy();
                }
                dismissChooseContacts();
            }
        });
        if (btnFacebook != null) btnFacebook.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logClickFacebook();
                if (!Utilities.isPackageInstalled(activity, "com.facebook.orca")) {
                    confirmDialogMessenger = new BaseMPConfirmDialog(R.string.info,
                            R.string.warning_messenger_was_not_installed, R.string.download_app, R.string.cancel,
                            v -> {
                                try {
                                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.facebook.orca")));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.facebook.orca")));
                                }
                                if (confirmDialogMessenger != null) {
                                    confirmDialogMessenger.dismiss();
                                }
                            }, v -> {
                        if (confirmDialogMessenger != null) {
                            confirmDialogMessenger.dismiss();
                        }
                    });
                    confirmDialogMessenger.show(activity.getSupportFragmentManager(), null);
                } else if (isShareImage) {
                    downloadAndShareImageMochaOnFacebook();
                } else {
                    String title = "";
                    String image = "";
                    if (content instanceof Video) {
                        title = ((Video) content).getTitle();
                        image = ((Video) content).getImagePath();
                    }
                    ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(link))
                            .setContentTitle(title)
                            .setImageUrl(Uri.parse(image))
                            .build();
                    MessageDialog.show(activity, shareLinkContent);
                    dismissChooseContacts();
                }
//                ShareLinkContent content = new ShareLinkContent.Builder()
//                        .setContentUrl(Uri.parse(link))
//                        .build();
//                MessageDialog.show(activity, content);
            }
        });
        if (btnOther != null) btnOther.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logClickShareOther();
                if (isShareImage) {
                    downloadAndShareImageMochaOnOtherApp();
                } else {
                    ShareUtils.shareWithIntent(activity, link, titleShare);
                    dismissChooseContacts();
                }
            }
        });
        if (btnClose != null) btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logClickClosePopupChooseContact();
                dismissChooseContacts();
            }
        });

        searchView.setHint(R.string.hint_search_share_content);
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard();
            }
            return false;
        });
        searchView.addTextChangedListener(new TextWatcher() {
            private String oldKeySearch;
            private Runnable runnableSearch = new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "runnableSearch run doSearch");
                    searchContact(keySearch);
                }
            };

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(charSequence)) {
                    keySearch = "";
                } else {
                    keySearch = charSequence.toString().trim();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.i(TAG, "afterTextChanged keySearch= '" + keySearch + "', oldKeySearch= '" + oldKeySearch + "'");
                if (handlerSharing != null) handlerSharing.removeCallbacks(runnableSearch);
                if (TextUtils.isEmpty(keySearch)) {
                    if (data != null) {
                        data.clear();
                        if (Utilities.notEmpty(listContacts)) data.addAll(listContacts);
                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                } else {
                    if (!keySearch.equals(oldKeySearch)) {
                        Log.i(TAG, "handlerSharing.postDelayed runnableSearch");
                        if (handlerSharing != null) handlerSharing.postDelayed(runnableSearch, 300);
                    }
                }
                oldKeySearch = keySearch;
            }
        });
        searchView.setOnFocusChangeListener((v, hasFocus) -> {
            Log.d(TAG, "onFocusChange hasFocus: " + hasFocus);
            isSearchViewFocus = hasFocus;
        });
        hideKeyboardWhenTouch(sheetView);
        BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, false);
        recyclerView.setNestedScrollingEnabled(true);
        dialogChooseContacts.setContentView(sheetView);
        dialogChooseContacts.setOnDismissListener(dialogInterface -> {
            Log.d(TAG, "showPopupShareContent dialogChooseContacts onDismiss");
            isSearchViewFocus = false;
            doSharingWhenDismiss();
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
                handlerSharing.postDelayed(runnableDismiss, TIME_DELAY_DISMISS);
            }
        });
        dialogChooseContacts.setOnShowListener(dialogInterface -> {
            final BottomSheetBehavior behavior = dialogChooseContacts.getBottomSheetBehavior();
            behavior.setPeekHeight(Utilities.getScreenHeight(activity) * 2 / 3);
            setHeightBottomDialog(dialogChooseContacts, false);
            logShowPopupChooseContact();
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
            }
            if (shareBusinessListener != null)
                shareBusinessListener.onShowShareDialog();
        });
        dialogChooseContacts.show();
        getAllContacts();
        if (content instanceof NewsModel) {
            NewsModel newsModel = (NewsModel) content;
        }
        searchView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (keyboardShown(searchView.getRootView())) {
                    dialogChooseContacts.setHeight(Utilities.getScreenHeight(activity));
                    dialogChooseContacts.getBottomSheetBehavior().setPeekHeight(Utilities.getScreenHeight(activity));
                } else {
                    Log.d("keyboard", "keyboard Down");
                    dialogChooseContacts.setHeight(Utilities.getScreenHeight(activity) * 2 / 3);
                    dialogChooseContacts.getBottomSheetBehavior().setPeekHeight(Utilities.getScreenHeight(activity) * 2 / 3);
                }
            }
        });
    }

    public void showPopupForwardMessage() {
        dismissChooseContacts();
        if (activity == null || activity.isFinishing() || messageType == null || !(content instanceof ReengMessage)) {
            return;
        }
        if (handlerSharing != null && runnableDismiss != null) {
            handlerSharing.removeCallbacks(runnableDismiss);
        }
        dialogChooseContacts = new ShareBottomDialog(activity);
        dialogChooseContacts.setKeyboardVisibilityListener(keyboardVisibilityListener);
        if (messageType == ReengMessageConstant.MessageType.text
                || messageType == ReengMessageConstant.MessageType.image
                || messageType == ReengMessageConstant.MessageType.shareContact
                || messageType == ReengMessageConstant.MessageType.shareLocation
                || messageType == ReengMessageConstant.MessageType.inviteShareMusic
                || messageType == ReengMessageConstant.MessageType.watch_video) {
            sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_forward_message, null, false);
        } else {
            sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_forward_message_no_social, null, false);
        }
        tvTitleDialog = sheetView.findViewById(R.id.tv_title);
        View btnClose = sheetView.findViewById(R.id.button_close);
        View btnNewGroup = sheetView.findViewById(R.id.button_new_group);
        View btnSocial = sheetView.findViewById(R.id.button_post_on_social);
        View btnOther = sheetView.findViewById(R.id.button_share_with_other_app);
        searchView = sheetView.findViewById(R.id.search_view);
        loadingView = sheetView.findViewById(R.id.loading);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        if (btnNewGroup != null) btnNewGroup.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                createNewGroup();
            }
        });
        if (btnSocial != null) btnSocial.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (messageType == ReengMessageConstant.MessageType.text
                        || messageType == ReengMessageConstant.MessageType.shareContact
                        || messageType == ReengMessageConstant.MessageType.shareLocation
                        || messageType == ReengMessageConstant.MessageType.inviteShareMusic
                        || messageType == ReengMessageConstant.MessageType.watch_video) {
                    showPopupPostContentOnSocial();
                } else if (messageType == ReengMessageConstant.MessageType.image)
                    showPopupPostImageOnSocial();
            }
        });
        if (btnOther != null) btnOther.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logClickShareOther();
                if (messageType == ReengMessageConstant.MessageType.image) {
                    ShareUtils.shareImageFileWithIntent(activity, link, titleShare);
                } else {
                    ShareUtils.shareWithIntent(activity, link, titleShare);
                }
                dismissChooseContacts();
            }
        });
        if (btnClose != null) btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logClickClosePopupChooseContact();
                dismissChooseContacts();
            }
        });

        searchView.setHint(R.string.hint_search_share_content);
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard();
            }
            return false;
        });
        searchView.addTextChangedListener(new TextWatcher() {
            private String oldKeySearch;
            private Runnable runnableSearch = new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "runnableSearch run doSearch keySearch= '" + keySearch + "'");
                    searchContact(keySearch);
                }
            };

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(charSequence)) {
                    keySearch = "";
                } else {
                    keySearch = charSequence.toString().trim();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.i(TAG, "afterTextChanged keySearch= '" + keySearch + "', oldKeySearch= '" + oldKeySearch + "'");
                if (handlerSharing != null) handlerSharing.removeCallbacks(runnableSearch);
                if (TextUtils.isEmpty(keySearch)) {
                    if (data != null) {
                        data.clear();
                        if (Utilities.notEmpty(listContacts)) data.addAll(listContacts);
                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                } else {
                    if (!keySearch.equals(oldKeySearch)) {
                        Log.i(TAG, "handlerSharing.postDelayed runnableSearch");
                        if (handlerSharing != null) handlerSharing.postDelayed(runnableSearch, 300);
                    }
                }
                oldKeySearch = keySearch;
            }
        });
        hideKeyboardWhenTouch(sheetView);
        BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, false);
        recyclerView.setNestedScrollingEnabled(true);
        dialogChooseContacts.setContentView(sheetView);
        dialogChooseContacts.setOnShowListener(dialogInterface -> {
            final BottomSheetBehavior behavior = dialogChooseContacts.getBottomSheetBehavior();
            behavior.setPeekHeight(Math.max(widthScreen, heightScreen));
//            dialogChooseContacts.getBottomSheetBehavior().setPeekHeight(Math.max(widthScreen, heightScreen));
            setHeightBottomDialog(dialogChooseContacts, true);
            logShowPopupChooseContact();
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
            }
            if (shareBusinessListener != null)
                shareBusinessListener.onShowShareDialog();
        });
        dialogChooseContacts.setOnDismissListener(dialogInterface -> {
            Log.d(TAG, "showPopupForwardMessage dialogChooseContacts onDismiss");
            doSharingWhenDismiss();
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
                handlerSharing.postDelayed(runnableDismiss, TIME_DELAY_DISMISS);
            }
        });

        dialogChooseContacts.show();
        getAllContacts();
    }

    public void showPopupShareMessages() {
        dismissChooseContacts();
        if (activity == null || activity.isFinishing() || Utilities.isEmpty(listMessages)) {
            return;
        }
        if (handlerSharing != null && runnableDismiss != null) {
            handlerSharing.removeCallbacks(runnableDismiss);
        }
        dialogChooseContacts = new ShareBottomDialog(activity);
        dialogChooseContacts.setKeyboardVisibilityListener(keyboardVisibilityListener);
        sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_share_messages, null, false);
        tvTitleDialog = sheetView.findViewById(R.id.tv_title);
        ImageView btnClose = sheetView.findViewById(R.id.button_close);
        View btnNewGroup = sheetView.findViewById(R.id.button_new_group);
        View btnSocial = sheetView.findViewById(R.id.button_post_on_social);
        View layoutSocial = sheetView.findViewById(R.id.layout_on_social);
        searchView = sheetView.findViewById(R.id.search_view);
        loadingView = sheetView.findViewById(R.id.loading);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
        if (layoutSocial != null)
            layoutSocial.setVisibility(hasPostOnSocial ? View.VISIBLE : View.GONE);
        if (btnNewGroup != null) btnNewGroup.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                createNewGroup();
            }
        });
        if (btnClose != null) btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logClickClosePopupChooseContact();
                dismissChooseContacts();
            }
        });
        if (btnSocial != null) btnSocial.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity == null || activity.isFinishing() || Utilities.isEmpty(listMessages))
                    return;
                ReengMessage msg = listMessages.get(0);
                if (msg != null && !TextUtils.isEmpty(msg.getFilePath())) {
                    link = msg.getFilePath();
                    isNetworkUrl = URLUtil.isNetworkUrl(link);
                    if (msg.getMessageType() == ReengMessageConstant.MessageType.text) {
                        showPopupPostContentOnSocial();
                    } else if (msg.getMessageType() == ReengMessageConstant.MessageType.image) {
                        listImages = new ArrayList<>();
                        for (ReengMessage reengMessage : listMessages) {
                            if (reengMessage != null && reengMessage.getMessageType() == ReengMessageConstant.MessageType.image) {
                                FeedContent.ImageContent imageContent = new FeedContent.ImageContent();
                                imageContent.setFilePath(reengMessage.getFilePath());
                                listImages.add(imageContent);
                            }
                        }
                        showPopupPostImageOnSocial();
                    }
                }
            }
        });
        searchView.setHint(R.string.hint_search_share_content);
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard();
            }
            return false;
        });
        searchView.addTextChangedListener(new TextWatcher() {
            private String oldKeySearch;
            private Runnable runnableSearch = new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "runnableSearch run doSearch keySearch= '" + keySearch + "'");
                    searchContact(keySearch);
                }
            };

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(charSequence)) {
                    keySearch = "";
                } else {
                    keySearch = charSequence.toString().trim();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.i(TAG, "afterTextChanged keySearch= '" + keySearch + "', oldKeySearch= '" + oldKeySearch + "'");
                if (handlerSharing != null) handlerSharing.removeCallbacks(runnableSearch);
                if (TextUtils.isEmpty(keySearch)) {
                    if (data != null) {
                        data.clear();
                        if (Utilities.notEmpty(listContacts)) data.addAll(listContacts);
                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                } else {
                    if (!keySearch.equals(oldKeySearch)) {
                        Log.i(TAG, "handlerSharing.postDelayed runnableSearch");
                        if (handlerSharing != null) handlerSharing.postDelayed(runnableSearch, 300);
                    }
                }
                oldKeySearch = keySearch;
            }
        });
        hideKeyboardWhenTouch(sheetView);
        BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, false);
        recyclerView.setNestedScrollingEnabled(true);
        dialogChooseContacts.setContentView(sheetView);
        dialogChooseContacts.setOnShowListener(dialogInterface -> {
            dialogChooseContacts.getBottomSheetBehavior().setPeekHeight(Utilities.getScreenHeight(activity) * 2 / 3);
//            setHeightBottomDialog(dialogChooseContacts, true);
            logShowPopupChooseContact();
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
            }
            if (shareBusinessListener != null)
                shareBusinessListener.onShowShareDialog();
        });
        dialogChooseContacts.setOnDismissListener(dialogInterface -> {
            Log.d(TAG, "showPopupForwardMessage dialogChooseContacts onDismiss");
            doSharingWhenDismiss();
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
                handlerSharing.postDelayed(runnableDismiss, TIME_DELAY_DISMISS);
            }
        });
        dialogChooseContacts.show();
        getAllContacts();
    }

    private void showPopupPostContentOnSocial() {
        dismissChooseContacts();
        dismissPostOnSocial();
        if (activity == null || activity.isFinishing()) return;
        if (handlerSharing != null && runnableDismiss != null) {
            handlerSharing.removeCallbacks(runnableDismiss);
        }
        dialogPostOnSocial = new ShareBottomDialog(activity);
        dialogPostOnSocial.setKeyboardVisibilityListener(keyboardVisibilityListener);
        sheetView = activity.getLayoutInflater().inflate(isLandscape ? R.layout.dialog_share_content_on_social_land
                : R.layout.dialog_share_content_on_social, null, false);
        layoutToolbar = sheetView.findViewById(R.id.layout_toolbar);
        tvTitleDialog = sheetView.findViewById(R.id.tv_title);
        View btnClose = sheetView.findViewById(R.id.button_close);
        btnPostContent = sheetView.findViewById(R.id.button_post);
        loadingView = sheetView.findViewById(R.id.loading);
        previewContentView = sheetView.findViewById(R.id.layout_preview_content);
        tvTitleContent = sheetView.findViewById(R.id.tv_title_content);
        ivCoverContent = sheetView.findViewById(R.id.iv_cover_content);
        editStatus = sheetView.findViewById(R.id.edit_status);
        EllipsisTextView tvProfileName = sheetView.findViewById(R.id.tv_profile_name);
        TextView tvContactAvatar = sheetView.findViewById(R.id.tv_contact_avatar);
        ImageView ivProfileAvatar = sheetView.findViewById(R.id.iv_profile_avatar);
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(activity);
        com.metfone.selfcare.model.account.UserInfo userInfo = userInfoBusiness.getUser();
        if (ivProfileAvatar != null) {
//            ReengAccount account = application.getReengAccountBusiness().getCurrentAccount();
//            if (account != null && !application.getReengAccountBusiness().isAnonymousLogin()) {
//                userName = account.getName();
//                String lAvatar = account.getLastChangeAvatar();
//                if (TextUtils.isEmpty(lAvatar)) {
//                    ivProfileAvatar.setVisibility(View.VISIBLE);
//                    if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
//                    ImageBusiness.setResource(ivProfileAvatar, R.drawable.ic_user_v5);
//                } else {
//                    application.getAvatarBusiness().setMyAvatar(ivProfileAvatar, tvContactAvatar
//                            , null, account, null);
//                }
//            } else {
//                ivProfileAvatar.setVisibility(View.VISIBLE);
//                if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
//                ImageBusiness.setResource(ivProfileAvatar, R.drawable.ic_user_v5);
//            }
            if (TextUtils.isEmpty(userInfo.getAvatar())) {
                initRankInfo(userInfoBusiness.getAccountRankDTO().rankId, ivProfileAvatar);
            } else {
                Glide.with(activity)
                        .load(ImageUtils.convertBase64ToBitmap(userInfo.getAvatar()))
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivProfileAvatar);
            }
        }

        String userName = userInfo.getFull_name();
        if (tvProfileName != null) tvProfileName.setText(userName);
        if (btnPostContent != null) btnPostContent.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                postContentOnSocial();
            }
        });
        if (btnClose != null) btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logClickClosePopupOnSocial();
                dismissPostOnSocial();
            }
        });
        dialogPostOnSocial.setContentView(sheetView);
        dialogPostOnSocial.setOnShowListener(dialogInterface -> {
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
            }
            if (shareBusinessListener != null)
                shareBusinessListener.onShowShareDialog();
            setHeightBottomDialog(dialogPostOnSocial, true);
            if (dialogPostOnSocial != null && dialogPostOnSocial.isShowing() && handlerSharing != null) {
                handlerSharing.postDelayed(() -> InputMethodUtils.showSoftKeyboardNew(activity, editStatus), 300);
            }
            logShowPopupOnSocial();
        });
        dialogPostOnSocial.setOnDismissListener(dialogInterface -> {
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
                handlerSharing.postDelayed(runnableDismiss, TIME_DELAY_DISMISS);
            }
        });
        dialogPostOnSocial.show();
        if (isNetworkUrl) {
            loadMetaDataUrl(link);
        } else {
            editStatus.setText(Html.fromHtml(link));
        }
    }

    private void initRankInfo(int rankID, ImageView ivProfileAvatar) {
        if (activity != null) {
            if (rankID > 1) {
                EnumUtils.AvatarRank myRank = EnumUtils.AvatarRank.getById(rankID);
                if (myRank != null) {
                    Glide.with(activity)
                            .load(myRank.drawable)
                            .centerCrop()
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(ivProfileAvatar);
                } else {
                    Glide.with(activity)
                            .load(ContextCompat.getDrawable(activity, R.drawable.ic_avatar_member))
                            .centerCrop()
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(ivProfileAvatar);
                }
            } else {
                ivProfileAvatar.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_avatar_member));
//            imgCrown.setVisibility(View.GONE);
            }
        }
    }

    private void showPopupPostImageOnSocial() {
        dismissChooseContacts();
        dismissPostOnSocial();
        if (activity == null || activity.isFinishing()) return;
        if (handlerSharing != null && runnableDismiss != null) {
            handlerSharing.removeCallbacks(runnableDismiss);
        }
        currentFeedContent = new FeedContent();
        currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE);
        if (Utilities.notEmpty(listImages)) {
            currentFeedContent.getListImage().clear();
            currentFeedContent.getListImage().addAll(listImages);
        }
        dialogPostOnSocial = new ShareBottomDialog(activity);
        dialogPostOnSocial.setKeyboardVisibilityListener(keyboardVisibilityListener);
        sheetView = activity.getLayoutInflater().inflate(isLandscape ? R.layout.dialog_share_image_on_social_land : R.layout.dialog_share_image_on_social, null, false);
        layoutToolbar = sheetView.findViewById(R.id.layout_toolbar);
        tvTitleDialog = sheetView.findViewById(R.id.tv_title);
        View btnClose = sheetView.findViewById(R.id.button_close);
        btnPostContent = sheetView.findViewById(R.id.button_post);
        loadingView = sheetView.findViewById(R.id.loading);
        editStatus = sheetView.findViewById(R.id.edit_status);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
        EllipsisTextView tvProfileName = sheetView.findViewById(R.id.tv_profile_name);
        TextView tvContactAvatar = sheetView.findViewById(R.id.tv_contact_avatar);
        ImageView ivProfileAvatar = sheetView.findViewById(R.id.iv_profile_avatar);
        ShareImageOnSocialAdapter adapter = new ShareImageOnSocialAdapter(activity);
        adapter.setItems(currentFeedContent.getListImage());
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, false);

        String userName = "";
        if (ivProfileAvatar != null) {
            ReengAccount account = application.getReengAccountBusiness().getCurrentAccount();
            if (account != null && !application.getReengAccountBusiness().isAnonymousLogin()) {
                userName = account.getName();
                String lAvatar = account.getLastChangeAvatar();
                if (TextUtils.isEmpty(lAvatar)) {
                    ivProfileAvatar.setVisibility(View.VISIBLE);
                    if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
                    ImageBusiness.setResource(ivProfileAvatar, R.drawable.ic_user_v5);
                } else {
                    application.getAvatarBusiness().setMyAvatar(ivProfileAvatar, tvContactAvatar
                            , null, account, null);
                }
            } else {
                ivProfileAvatar.setVisibility(View.VISIBLE);
                if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
                ImageBusiness.setResource(ivProfileAvatar, R.drawable.ic_user_v5);
            }
        }
        if (tvProfileName != null) tvProfileName.setText(userName);
        if (btnPostContent != null) btnPostContent.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                postImageOnSocial(link);
            }
        });
        if (btnClose != null) btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logClickClosePopupOnSocial();
                dismissPostOnSocial();
            }
        });
        dialogPostOnSocial.setContentView(sheetView);
        dialogPostOnSocial.setOnShowListener(dialogInterface -> {
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
            }
            if (shareBusinessListener != null)
                shareBusinessListener.onShowShareDialog();
            setHeightBottomDialog(dialogPostOnSocial, true);
            if (dialogPostOnSocial != null && dialogPostOnSocial.isShowing() && handlerSharing != null) {
                handlerSharing.postDelayed(() -> InputMethodUtils.showSoftKeyboard(activity, editStatus), 300);
            }
            logShowPopupOnSocial();
        });
        dialogPostOnSocial.setOnDismissListener(dialogInterface -> {
            delayHideKeyboard();
            if (handlerSharing != null && runnableDismiss != null) {
                handlerSharing.removeCallbacks(runnableDismiss);
                handlerSharing.postDelayed(runnableDismiss, TIME_DELAY_DISMISS);
            }
        });
        dialogPostOnSocial.show();
    }

    private void cancelGetAllContacts() {
        if (getAllContactsTask != null) {
            getAllContactsTask.setListener(null);
            getAllContactsTask.cancel(true);
            getAllContactsTask = null;
        }
    }

    private void cancelSearchContact() {
        if (searchContactsTask != null) {
            searchContactsTask.setListener(null);
            searchContactsTask.cancel(true);
            searchContactsTask = null;
        }
    }

    private void getAllContacts() {
        cancelGetAllContacts();
        initComparatorSearchContacts();
        if (application.getReengAccountBusiness().isAnonymousLogin()) return;
        getAllContactsTask = new GetAllContactsTask(application);
        getAllContactsTask.setComparatorContacts(comparatorContacts);
        getAllContactsTask.setComparatorThreadMessages(comparatorThreadMessages);
        getAllContactsTask.setListener(this);
        getAllContactsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void searchContact(String keySearch) {
        cancelSearchContact();
        if (application.getReengAccountBusiness().isAnonymousLogin()) return;
        searchContactsTask = new SearchContactsTask(application);
        searchContactsTask.setComparatorContact(comparatorContacts);
        searchContactsTask.setComparatorMessage(comparatorThreadMessages);
        searchContactsTask.setComparatorKeySearch(comparatorKeySearchUnmark);
        searchContactsTask.setComparatorName(comparatorNameUnmark);
        searchContactsTask.setListener(this);
        searchContactsTask.setData(listContacts);
        searchContactsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, keySearch);
    }

    private void setStateOpenChat(final ContactProvisional item) {
        if (handlerSharing != null && item != null) handlerSharing.postDelayed(() -> {
            if (item != null) {
                item.setState(ContactProvisional.OPEN_CHAT);
                updateState();
            }
        }, 2000);
    }

    private void doSharing(final ContactProvisional item) {
        if (activity == null || activity.isFinishing() || item == null) return;
        item.setState(ContactProvisional.SENT);
        //item.setState(ContactProvisional.OPEN_CHAT);
        mapState.put(item.getKey(), item);
        updateState();
        boolean sendTextSuccess = false;
        final MessageBusiness messageBusiness = ApplicationController.self().getMessageBusiness();
        ThreadMessage threadTmp = null;
        if (item.getContact() instanceof ThreadMessage) {
            threadTmp = (ThreadMessage) item.getContact();
        } else if (item.getContact() instanceof PhoneNumber) {
            threadTmp = messageBusiness.findExistingOrCreateNewThread(((PhoneNumber) item.getContact()).getJidNumber());
        } else if (item.getContact() instanceof String) {
            threadTmp = messageBusiness.findExistingOrCreateNewThread((String) item.getContact());
        }
        if (threadTmp != null) {
            if (Utilities.notEmpty(listMessages)) {
                for (ReengMessage msg : listMessages) {
                    if (msg.getMessageType() == ReengMessageConstant.MessageType.file) {
                        String filePath = msg.getFilePath();
                        String fileName = msg.getFileName();
                        String fileType = ReengMessageConstant.FileType.fromString(FileHelper.getExtensionFile(filePath)).toString();
                        messageBusiness.createAndSendMessageFile(activity, threadTmp, filePath, fileType, fileName);
                        boolean check = Utilities.notEmpty(filePath) && Utilities.notEmpty(fileName) && Utilities.notEmpty(fileType);
                        if (check) sendTextSuccess = true;
                    } else {
                        boolean check = messageBusiness.sendNewMessage(activity, threadTmp, msg, null);
                        if (check) sendTextSuccess = true;
                    }
                }
            } else if (content instanceof ReengMessage) {
                ReengMessage reengMessage = (ReengMessage) content;
                if (ReengMessageConstant.MessageType.watch_video == messageType
                        || ReengMessageConstant.MessageType.inviteShareMusic == messageType) {
                    reengMessage.setContent(link);
                    reengMessage.setMessageType(ReengMessageConstant.MessageType.text);
                }
                if (reengMessage.isForwardingMessage()) {
                    sendTextSuccess = messageBusiness.sendForwardingMessage(activity, threadTmp, reengMessage, null);
                } else {
                    if (ReengMessageConstant.MessageType.image == messageType)
                        reengMessage.setDirectLinkMedia("");
                    sendTextSuccess = messageBusiness.sendNewMessage(activity, threadTmp, reengMessage, null);
                }
            } else {
                if (isShareImage) {
                    for (FeedContent.ImageContent imageContent : listImages) {
                        ReengMessage reengMessage = new ReengMessage();
                        reengMessage.setForwardingMessage(true);
                        reengMessage.setMessageType(ReengMessageConstant.MessageType.image);
                        reengMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
                        reengMessage.setFileType("image");
                        if (imageContent.getImgUrl() != null && (imageContent.getImgUrl().startsWith("http://") || imageContent.getImgUrl().startsWith("https://")))
                            reengMessage.setDirectLinkMedia(imageContent.getImgUrl());
                        else reengMessage.setDirectLinkMedia("/" + imageContent.getImgUrl());
                        reengMessage.setFileId(imageContent.getIdImage());
                        reengMessage.setVideoContentUri(String.valueOf(imageContent.getRatioImage()));
                        reengMessage.setStatus(ReengMessageConstant.STATUS_NOT_LOAD);
                        boolean check = messageBusiness.sendForwardingMessage(activity, threadTmp, reengMessage, null);
                        if (check) sendTextSuccess = true;
                    }
                } else {
                    if (content instanceof Video) {
                        link = String.format(getString(R.string.kh_cinema_content_share), ((Video) content).getTitle(), link);
                    } else if (content instanceof Movie) {
                        link = String.format(getString(R.string.kh_cinema_content_share), ((Movie) content).getName(), link);
                    }
                    sendTextSuccess = messageBusiness.sendMessageText(activity, threadTmp, link, null, null, false);
                }
            }
        }
        if (sendTextSuccess) {
            //setStateOpenChat(item);
        } else {
            activity.showToast(R.string.msg_share_content_error);
            item.setState(ContactProvisional.RETRY);
            mapState.put(item.getKey(), item);
            updateState();
        }
    }

    private void updateState() {
        if (mapState == null) return;
        if (Utilities.notEmpty(listContacts)) {
            for (ContactProvisional item : listContacts) {
                ContactProvisional tmp = mapState.get(item.getKey());
                if (tmp != null) item.setState(tmp.getState());
            }
        }
        if (Utilities.notEmpty(data)) {
            for (int i = 0; i < data.size(); i++) {
                ContactProvisional item = data.get(i);
                ContactProvisional tmp = mapState.get(item.getKey());
                if (tmp != null) item.setState(tmp.getState());
                if (adapter != null) adapter.notifyItemChanged(i);
            }
        }
    }

    private void createNewGroup() {
        dismissChooseContacts();
        if (application == null || activity == null || activity.isFinishing()) return;
        Intent intent = new Intent(activity, ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP);
        bundle.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, -1);
        bundle.putBoolean(Constants.CHOOSE_CONTACT.DATA_AUTO_FORWARD, true);
        ArrayList<ReengMessage> listMessages = new ArrayList<>();
        if (Utilities.notEmpty(this.listMessages)) {
            listMessages.addAll(this.listMessages);
        } else if (content instanceof ReengMessage) {
            ReengMessage reengMessage = (ReengMessage) content;
            if (reengMessage.isForwardingMessage()) {
                //reengMessage.setForwardingMessage(true);
                if (ReengMessageConstant.MessageType.watch_video == messageType
                        || ReengMessageConstant.MessageType.inviteShareMusic == messageType) {
                    reengMessage.setContent(link);
                    reengMessage.setMessageType(ReengMessageConstant.MessageType.text);
                }
                //reengMessage.setDirection(ReengMessageConstant.Direction.send);
                reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
            }
            listMessages.add(reengMessage);
        } else {
            if (isShareImage) {
                for (FeedContent.ImageContent imageContent : listImages) {
                    if (imageContent != null) {
                        ReengMessage msg = new ReengMessage();
                        msg.setForwardingMessage(true);
                        msg.setMessageType(ReengMessageConstant.MessageType.image);
                        msg.setChatMode(ReengMessageConstant.MODE_IP_IP);
                        msg.setFileType("image");
                        msg.setDirectLinkMedia("/" + imageContent.getImgUrl());
                        msg.setFileId(imageContent.getIdImage());
                        msg.setVideoContentUri(String.valueOf(imageContent.getRatioImage()));
                        msg.setStatus(ReengMessageConstant.STATUS_NOT_LOAD);
                        listMessages.add(msg);
                    }
                }
            } else {
                ReengMessage reengMessage = new ReengMessage();
                reengMessage.setContent(link);
                reengMessage.setMessageType(ReengMessageConstant.MessageType.text);
                reengMessage.setStatus(ReengMessageConstant.STATUS_LOADING);
                listMessages.add(reengMessage);
            }
        }
        bundle.putSerializable(Constants.CHOOSE_CONTACT.DATA_LIST_REENG_MESSAGE, listMessages);
        intent.putExtras(bundle);
        activity.startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP, true);
        if (typeSharing == TYPE_SHARE_CONTENT) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(R.string.ga_share_content_click_create_group), "");
        } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(R.string.ga_share_content_click_create_group), "");
        } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(R.string.ga_share_content_click_create_group), "");
        } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(R.string.ga_share_content_click_create_group), "");
        } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(R.string.ga_share_content_click_create_group), "");
        }
    }

    private void setHeightBottomDialog(final ShareBottomDialog dialog, boolean isFullScreen) {
        isMaxHeightPopup = false;
        final BottomSheetBehavior behavior = dialog.getBottomSheetBehavior();
        if (behavior != null) {
            if (isLandscape) {
                behavior.setPeekHeight(Math.min(widthScreen, heightScreen));
                isMaxHeightPopup = true;
            } else if (isFullScreen) {
                behavior.setPeekHeight(Math.max(widthScreen, heightScreen));
                setupFullHeight(dialog);
                isMaxHeightPopup = true;
            } else {
                behavior.setPeekHeight(Math.max(widthScreen, heightScreen) * 2 / 3);
                isMaxHeightPopup = false;
            }
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    Log.d(TAG, "bottomSheetBehavior onStateChanged newState: " + newState);
                    if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        if (!isMaxHeightPopup) {
                            hideKeyboard();
                            if (layoutToolbar != null) {
                                layoutToolbar.setVisibility(View.VISIBLE);
                                layoutToolbar = null;
                            }
                            if (behavior != null) {
                                behavior.setPeekHeight(Math.max(widthScreen, heightScreen));
                            }
                            isMaxHeightPopup = true;
                        }
                    } else if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                        dismissChooseContacts();
                        dismissPostOnSocial();
                    } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                        if (layoutToolbar != null) {
                            layoutToolbar.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    //Log.d(TAG, "bottomSheetBehavior onSlide slideOffset: " + slideOffset);
                }
            });
            if (isMaxHeightPopup) {
                if (layoutToolbar != null) {
                    layoutToolbar.setVisibility(View.VISIBLE);
                    layoutToolbar = null;
                }
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        if (layoutParams != null) {
            layoutParams.height = heightScreen;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void loadMetaDataUrl(String url) {
        if (application == null) return;
        currentFeedContent = null;
        enablePostContent = false;
        if (loadingView != null) loadingView.setVisibility(View.VISIBLE);
        if (previewContentView != null) previewContentView.setVisibility(View.GONE);
        new WSOnMedia(application).getMetaData(url, response -> {
            enablePostContent = true;
            if (dialogPostOnSocial == null || !dialogPostOnSocial.isShowing()) return;
            if (loadingView != null) loadingView.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                    if (jsonObject.getInt(Constants.HTTP.REST_CODE) == HTTPCode.E200_OK) {
                        String jsonContent = jsonObject.getString("content");
                        currentFeedContent = new Gson().fromJson(jsonContent, FeedContent.class);
                        if (FeedContent.ITEM_TYPE_FILM.equals(currentFeedContent.getItemType())) {
                            currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
                            currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_MOVIE);
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
            if (currentFeedContent == null) {
                if (editStatus != null) editStatus.setText(Html.fromHtml(link));
            } else {
                if (TextUtils.isEmpty(currentFeedContent.getContentUrl()))
                    currentFeedContent.setContentUrl(currentFeedContent.getUrl());
                if (previewContentView != null)
                    previewContentView.setVisibility(View.VISIBLE);
                ImageBusiness.setImageShareContent(ivCoverContent, currentFeedContent.getImageUrl());
                if (tvTitleContent != null)
                    tvTitleContent.setText(currentFeedContent.getItemName());
            }
        }, volleyError -> {
            Log.e(TAG, volleyError);
            enablePostContent = true;
            if (dialogPostOnSocial == null || !dialogPostOnSocial.isShowing()) return;
            if (loadingView != null) loadingView.setVisibility(View.GONE);
            if (editStatus != null) editStatus.setText(Html.fromHtml(link));
        }, true);
    }

    private void postContentOnSocial() {
        if (activity == null || activity.isFinishing()) return;
        if (typeSharing == TYPE_SHARE_CONTENT) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social1), getString(R.string.ga_share_content_click_post), "");
        } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social2), getString(R.string.ga_share_content_click_post), "");
        } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social3), getString(R.string.ga_share_content_click_post), "");
        } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social4), getString(R.string.ga_share_content_click_post), "");
        } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social5), getString(R.string.ga_share_content_click_post), "");
        }
        if (enablePostContent) {
            handlePostOnMedia();
        }
    }

    private void postImageOnSocial(String filePath) {
        if (activity == null || activity.isFinishing() || TextUtils.isEmpty(filePath)) return;
        if (typeSharing == TYPE_SHARE_CONTENT) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social1), getString(R.string.ga_share_content_click_post), "");
        } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social2), getString(R.string.ga_share_content_click_post), "");
        } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social3), getString(R.string.ga_share_content_click_post), "");
        } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social4), getString(R.string.ga_share_content_click_post), "");
        } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
            activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social5), getString(R.string.ga_share_content_click_post), "");
        }
        hideKeyboard();
        if (!NetworkHelper.isConnectInternet(activity)) {
            activity.showToast(R.string.no_connectivity_check_again);
            return;
        }

        if (Utilities.notEmpty(listMessages) && Utilities.notEmpty(listImages)) {
            if (btnPostContent != null) btnPostContent.setEnabled(false);
            activity.showLoadingDialog("", R.string.processing);
            ArrayList<String> listPaths = new ArrayList<>();
            for (FeedContent.ImageContent item : listImages) {
                if (item != null && !TextUtils.isEmpty(item.getFilePath()))
                    listPaths.add(item.getFilePath());
            }
            application.getTransferFileBusiness().uploadImageSocialOnMedia(listPaths, this);
        } else if (content instanceof ReengMessage && !((ReengMessage) content).isForwardingMessage() && Utilities.notEmpty(listImages)) {
            if (btnPostContent != null) btnPostContent.setEnabled(false);
            activity.showLoadingDialog("", R.string.processing);
            ArrayList<String> listPaths = new ArrayList<>();
            for (FeedContent.ImageContent item : listImages) {
                if (item != null && !TextUtils.isEmpty(item.getFilePath()))
                    listPaths.add(item.getFilePath());
            }
            application.getTransferFileBusiness().uploadImageSocialOnMedia(listPaths, this);
        } else if (currentFeedContent != null && Utilities.notEmpty(currentFeedContent.getListImage())) {
            handlePostOnMedia();
        } else {
            if (!isShareImage) {
                if (btnPostContent != null) btnPostContent.setEnabled(false);
                activity.showLoadingDialog("", R.string.processing);
                ArrayList<String> listPaths = new ArrayList<>();
                listPaths.add(filePath);
                application.getTransferFileBusiness().uploadImageSocialOnMedia(listPaths, this);
            }
        }
    }

    private void handlePostOnMedia() {
        if (activity == null || activity.isFinishing()) return;
        hideKeyboard();
        if (!NetworkHelper.isConnectInternet(activity)) {
            activity.showToast(R.string.no_connectivity_check_again);
            activity.hideLoadingDialog();
            return;
        }
        if (btnPostContent != null) btnPostContent.setEnabled(false);
        FeedBusiness feedBusiness = application.getFeedBusiness();
        final ArrayList<TagMocha> mListTagFake = FeedBusiness.getListTagFromListPhoneNumber(editStatus.getUserInfo());
        String messageContent = TextHelper.trimTextOnMedia(editStatus.getTextTag());
        if (currentFeedContent == null) {
            currentFeedContent = new FeedContent();
            currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
            currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS);
        }
        String urlKey = feedBusiness.generateUrlSocial();
        FeedModelOnMedia.ActionLogApp actionLogApp = FeedModelOnMedia.ActionLogApp.POST;
        currentFeedContent.setContentStatus(messageContent);
        currentFeedContent.setContentListTag(mListTagFake);
        currentFeedContent.setUrl(urlKey);
        activity.showLoadingDialog("", R.string.processing);
        new WSOnMedia(application).logAppV6(urlKey, "", currentFeedContent, actionLogApp,
                "", "", "", null,
                response -> {
                    Log.i(TAG, "logAppV6: " + response);
                    boolean check = false;
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                            check = (jsonObject.getInt(Constants.HTTP.REST_CODE) == HTTPCode.E200_OK);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                    if (check)
                        handlePostContentSuccess();
                    else
                        handlePostContentFailed();
                }, volleyError -> {
                    Log.i(TAG, "Response error" + volleyError.getMessage());
                    handlePostContentFailed();
                });
    }

    private void handlePostContentSuccess() {
        if (activity == null || activity.isFinishing()) return;
        if (currentFeedContent != null) {
            if (FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(currentFeedContent.getItemSubType())) {
                LuckyWheelHelper.getInstance(application).doMission(Constants.LUCKY_WHEEL.ITEM_SHARE_ONMEDIA);
            } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(currentFeedContent.getItemSubType())) {
                LuckyWheelHelper.getInstance(application).doMission(Constants.LUCKY_WHEEL.ITEM_UPLOAD_ALBUM);
            } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(currentFeedContent.getItemSubType())) {
                LuckyWheelHelper.getInstance(application).doMission(Constants.LUCKY_WHEEL.ITEM_STATUS);
            }
        }
        FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
        feedModelOnMedia.setFeedContent(currentFeedContent);
        feedModelOnMedia.setIsLike(0);
        feedModelOnMedia.setIsShare(0);
        feedModelOnMedia.setBase64RowId("");
        long countShare = feedModelOnMedia.getFeedContent().getCountShare();
        feedModelOnMedia.getFeedContent().setCountShare(countShare + 1);
        UserInfo mUserInfo = new UserInfo(application.getReengAccountBusiness().getJidNumber()
                , application.getReengAccountBusiness().getUserName());
        feedModelOnMedia.setUserInfo(mUserInfo);
        feedModelOnMedia.setActionType(FeedModelOnMedia.ActionLogApp.POST);
        feedModelOnMedia.getFeedContent().setUserInfo(mUserInfo);
        feedModelOnMedia.getFeedContent().setStamp(System.currentTimeMillis());
        feedModelOnMedia.setTimeStamp(System.currentTimeMillis());
        feedModelOnMedia.setTimeServer(System.currentTimeMillis());
        ListenerHelper.getInstance().onPostFeedSuccess(feedModelOnMedia);
        activity.hideLoadingDialog();
        if (btnPostContent != null) btnPostContent.setEnabled(true);
        activity.showToast(R.string.share_content_successfully);
        dismissPostOnSocial();
    }

    private void handlePostContentFailed() {
        if (activity == null || activity.isFinishing()) return;
        activity.hideLoadingDialog();
        if (btnPostContent != null) btnPostContent.setEnabled(true);
        activity.showToast(R.string.e601_error_but_undefined);
    }

    private void hideKeyboard() {
        Log.d(TAG, "hideKeyboard");
//        if (activity != null && !activity.isFinishing()) {
//            activity.runOnUiThread(hideKeyboard);
//        }
        if (handlerSharing != null) handlerSharing.post(hideKeyboard);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void hideKeyboardWhenTouch(View view) {
        if (view == null || activity == null || activity.isFinishing()) {
            return;
        }
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                hideKeyboard();
                return false;
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideKeyboardWhenTouch(innerView);
            }
        }
    }

    private void showSnackDoSharing(final ContactProvisional item) {
        if (oldSnackBar != null && oldSnackBar.isShown()) oldSnackBar.dismiss();
        final CustomSnackBar snackBar = CustomSnackBar.make(sheetView);
        snackBar.setDuration(3000);
        handlerSharing.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    ContactProvisional tmp = mapState.get(item.getKey());
                    if (tmp != null && tmp.getState() == ContactProvisional.UNDO) {
                        mapState.remove(item.getKey());
                        doSharing(item);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        }, 3000);
        snackBar.setText(R.string.msg_sent_content);
        snackBar.setAction(R.string.btn_share_content_state_undo, view -> {
            if (snackBar != null) {
                snackBar.setEnableListener(false);
                snackBar.dismiss();
            }
            item.setState(ContactProvisional.SEND);
            mapState.put(item.getKey(), item);
            updateState();
            if (typeSharing == TYPE_SHARE_CONTENT) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(R.string.ga_share_content_click_undo), "");
            } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(R.string.ga_share_content_click_undo), "");
            } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(R.string.ga_share_content_click_undo), "");
            } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(R.string.ga_share_content_click_undo), "");
            } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(R.string.ga_share_content_click_undo), "");
            }
        });
        snackBar.setEnableListener(true);
        snackBar.setOnSnackBarListener(new CustomSnackBar.OnSnackBarListener() {
            @Override
            public void onShowSnack() {
                Log.d(TAG, "onShowSnack");
            }

            @Override
            public void onDismissSnack() {
                Log.d(TAG, "onDismissSnack");
                try {
                    ContactProvisional tmp = mapState.get(item.getKey());
                    if (tmp != null && tmp.getState() == ContactProvisional.UNDO) {
                        mapState.remove(item.getKey());
                        doSharing(item);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        });
        snackBar.setGravity(Gravity.BOTTOM);
        oldSnackBar = snackBar;
        //snackBar.show();
    }

    public void delayHideKeyboard() {
        Handler handler = new Handler();
        handler.postDelayed(() -> InputMethodUtils.hideSoftKeyboard(activity), 100);
    }

    @Override
    public void onShareToContact(final ContactProvisional item, final int position) {
        if (activity == null || activity.isFinishing()) return;
        switch (item.getState()) {
            case ContactProvisional.SEND: {
                item.setState(ContactProvisional.UNDO);
                mapState.put(item.getKey(), item);
                updateState();
                showSnackDoSharing(item);
//                if (!isMaxHeightPopup && dialogChooseContacts != null) {
//                    BottomSheetBehavior behavior = dialogChooseContacts.getBottomSheetBehavior();
//                    if (behavior != null) behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                }
                break;
            }
            case ContactProvisional.UNDO: {
                item.setState(ContactProvisional.SEND);
                mapState.put(item.getKey(), item);
                updateState();
                break;
            }
            case ContactProvisional.RETRY: {
                doSharing(item);
                if (typeSharing == TYPE_SHARE_CONTENT) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(R.string.ga_share_content_click_retry), "");
                } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(R.string.ga_share_content_click_retry), "");
                } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(R.string.ga_share_content_click_retry), "");
                } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(R.string.ga_share_content_click_retry), "");
                } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(R.string.ga_share_content_click_retry), "");
                }
                break;
            }
            case ContactProvisional.SENT: {
                if (item.getContact() instanceof ThreadMessage) {
                    NavigateActivityHelper.navigateToChatDetail(activity, (ThreadMessage) item.getContact());
                } else if (item.getContact() instanceof PhoneNumber) {
                    ThreadMessage threadTmp = ApplicationController.self().getMessageBusiness().findExistingOrCreateNewThread(((PhoneNumber) item.getContact()).getJidNumber());
                    if (threadTmp == null) {
                        NavigateActivityHelper.navigateToContactDetail(activity, (PhoneNumber) item.getContact());
                    } else {
                        NavigateActivityHelper.navigateToChatDetail(activity, threadTmp);
                    }
                } else if (item.getContact() instanceof String) {
                    ThreadMessage threadTmp = ApplicationController.self().getMessageBusiness().findExistingOrCreateNewThread((String) item.getContact());
                    if (threadTmp != null) {
                        NavigateActivityHelper.navigateToChatDetail(activity, threadTmp);
                    }
                }
                if (typeSharing == TYPE_SHARE_CONTENT) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(R.string.ga_share_content_click_open_chat), "");
                } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(R.string.ga_share_content_click_open_chat), "");
                } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(R.string.ga_share_content_click_open_chat), "");
                } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(R.string.ga_share_content_click_open_chat), "");
                } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                    activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(R.string.ga_share_content_click_open_chat), "");
                }
                dismissChooseContacts();
                break;
            }
            default:
                break;
        }
    }

    private void dismissChooseContacts() {
        cancelGetAllContacts();
        cancelSearchContact();
        hideKeyboard();
        if (dialogChooseContacts != null) dialogChooseContacts.dismiss();
    }

    private void dismissPostOnSocial() {
        hideKeyboard();
        if (dialogPostOnSocial != null) dialogPostOnSocial.dismiss();
    }

    private void downloadAndShareImageMochaOnFacebook() {
        if (application == null || activity == null || activity.isFinishing() || Utilities.isEmpty(listImages))
            return;
        DownloadImagesForShareFacebookTask task = new DownloadImagesForShareFacebookTask(application, listImages);
        task.setListener(new ShareImagesOnFacebookListener() {
            @Override
            public void onPrepareDownload() {
                if (activity != null) activity.showLoadingDialog("", R.string.processing);
            }

            @Override
            public void onCompletedDownload(ArrayList<Bitmap> list) {
                if (activity == null || activity.isFinishing()) return;
                activity.hideLoadingDialog();
                if (Utilities.isEmpty(list)) {
                    activity.showToast(R.string.e601_error_but_undefined);
                } else {
                    ShareUtils.shareImageBitmapToFacebook(activity, list);
                    dismissChooseContacts();
                }
            }

            @Override
            public void onCompletedDownload(Bitmap bitmap) {

            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void downloadAndShareImageMochaOnOtherApp() {
        if (application == null || activity == null || activity.isFinishing() || Utilities.isEmpty(listImages))
            return;
        DownloadImagesForShareOtherAppTask task = new DownloadImagesForShareOtherAppTask(application, listImages);
        task.setListener(new ShareImagesOnOtherAppListener() {
            @Override
            public void onPrepareDownload() {
                if (activity != null) activity.showLoadingDialog("", R.string.processing);
            }

            @Override
            public void onCompletedDownload(ArrayList<Uri> list) {
                if (activity == null || activity.isFinishing()) return;
                activity.hideLoadingDialog();
                if (Utilities.isEmpty(list)) {
                    activity.showToast(R.string.e601_error_but_undefined);
                } else {
                    ShareUtils.shareImageWithIntent(activity, list, titleShare);
                    dismissChooseContacts();
                }
            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onPrepareGetAllContacts() {
        if (loadingView != null) loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFinishedGetAllContacts(ArrayList<ContactProvisional> list) {
        if (loadingView != null && dialogChooseContacts != null && dialogChooseContacts.isShowing())
            loadingView.setVisibility(View.GONE);
        listContacts = list;
        if (data != null) {
            data.clear();
            data.addAll(listContacts);
        }
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    @Override
    public void onUploadCompleted(ArrayList<String> response) {
        if (activity == null || activity.isFinishing()) return;
        if (BuildConfig.DEBUG) Log.i(TAG, "onUploadCompleted response: " + response);
        if (Utilities.notEmpty(response)) {
            if (currentFeedContent != null) {
                currentFeedContent.getListImage().clear();
                for (int i = 0; i < response.size(); i++) {
                    try {
                        String resp = response.get(i);
                        JSONObject jsonObject = new JSONObject(resp);
                        String idImg = jsonObject.optString("desc");
                        String linkImg = jsonObject.optString("link");
                        String thumb = jsonObject.optString("thumb");
                        thumb = TextUtils.isEmpty(thumb) ? linkImg : thumb;
                        float ratio = BigDecimal.valueOf(jsonObject.getDouble("ratio")).floatValue();
                        currentFeedContent.addImageContent(idImg, linkImg, thumb, ratio);
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                }
                if (Utilities.notEmpty(currentFeedContent.getListImage())) {
                    handlePostOnMedia();
                    return;
                }
            }
        }
        if (btnPostContent != null) btnPostContent.setEnabled(true);
        activity.showToast(R.string.e601_error_but_undefined);
        activity.hideLoadingDialog();
    }

    @Override
    public void onUploadFailed(int code, String msg, ArrayList<String> listSuccess) {
        if (activity == null || activity.isFinishing()) return;
        if (BuildConfig.DEBUG)
            Log.i(TAG, "onUploadFailed code: " + code + ", msg: " + msg + ", listSuccess: " + listSuccess);
        if (btnPostContent != null) btnPostContent.setEnabled(true);
        activity.showToast(R.string.e601_error_but_undefined);
        activity.hideLoadingDialog();
    }

    @Override
    public void onPrepareSearchContacts() {

    }

    @Override
    public void onFinishedSearchContacts(String keySearch, ArrayList<ContactProvisional> list) {
        if (activity != null) {
            if (data != null) {
                data.clear();
                data.addAll(list);
            }
            if (adapter != null) adapter.notifyDataSetChanged();
        }
    }

    private void initComparatorSearchContacts() {
        comparatorNameUnmark = SearchUtils.getComparatorNameForSearch();
        comparatorKeySearchUnmark = SearchUtils.getComparatorKeySearchForSearch();
        comparatorContacts = SearchUtils.getComparatorContactForSearch();
        comparatorThreadMessages = SearchUtils.getComparatorMessageForSearch();
    }

    private void doSharingWhenDismiss() {
        if (mapState != null) {
            CopyOnWriteArraySet<String> keySet = new CopyOnWriteArraySet<>(mapState.keySet());
            for (String key : keySet) {
                try {
                    ContactProvisional tmp = mapState.get(key);
                    if (tmp != null && tmp.getState() == ContactProvisional.UNDO) {
                        mapState.remove(key);
                        doSharing(tmp);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        }
    }

    private String getString(int resString) {
        if (activity != null && resString > 0) return activity.getString(resString);
        return "";
    }

    private void logShowPopupChooseContact() {
        if (activity != null) {
            if (typeSharing == TYPE_SHARE_CONTENT) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(R.string.ga_share_content_display), "");
            } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(R.string.ga_share_content_display), "");
            } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(R.string.ga_share_content_display), "");
            } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(R.string.ga_share_content_display), "");
            } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(R.string.ga_share_content_display), "");
            }
        }
    }

    private void logShowPopupOnSocial() {
        if (activity != null) {
            if (typeSharing == TYPE_SHARE_CONTENT) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social1), getString(R.string.ga_share_content_display), "");
            } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social2), getString(R.string.ga_share_content_display), "");
            } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social3), getString(R.string.ga_share_content_display), "");
            } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social4), getString(R.string.ga_share_content_display), "");
            } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social5), getString(R.string.ga_share_content_display), "");
            }
        }
    }

    private void logClickClosePopupOnSocial() {
        if (activity != null) {
            if (typeSharing == TYPE_SHARE_CONTENT) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social1), getString(R.string.ga_share_content_click_close), "");
            } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social2), getString(R.string.ga_share_content_click_close), "");
            } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social3), getString(R.string.ga_share_content_click_close), "");
            } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social4), getString(R.string.ga_share_content_click_close), "");
            } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_post_on_social5), getString(R.string.ga_share_content_click_close), "");
            }
        }
    }

    private void logClickClosePopupChooseContact() {
        if (activity != null) {
            if (typeSharing == TYPE_SHARE_CONTENT) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(R.string.ga_share_content_click_close), "");
            } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(R.string.ga_share_content_click_close), "");
            } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(R.string.ga_share_content_click_close), "");
            } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(R.string.ga_share_content_click_close), "");
            } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(R.string.ga_share_content_click_close), "");
            }
        }
    }

    private void logClickCopy() {
        if (activity != null) {
            if (typeSharing == TYPE_SHARE_CONTENT) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(isNetworkUrl ? R.string.ga_share_content_click_copy_link : R.string.ga_share_content_click_copy), "");
            } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(isNetworkUrl ? R.string.ga_share_content_click_copy_link : R.string.ga_share_content_click_copy), "");
            } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(isNetworkUrl ? R.string.ga_share_content_click_copy_link : R.string.ga_share_content_click_copy), "");
            } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(isNetworkUrl ? R.string.ga_share_content_click_copy_link : R.string.ga_share_content_click_copy), "");
            } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(isNetworkUrl ? R.string.ga_share_content_click_copy_link : R.string.ga_share_content_click_copy), "");
            }
        }
    }

    private void logClickFacebook() {
        if (activity != null) {
            if (typeSharing == TYPE_SHARE_CONTENT) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(R.string.ga_share_content_click_facebook), "");
            } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(R.string.ga_share_content_click_facebook), "");
            } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(R.string.ga_share_content_click_facebook), "");
            } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(R.string.ga_share_content_click_facebook), "");
            } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(R.string.ga_share_content_click_facebook), "");
            }
        }
    }

    private void logClickShareOther() {
        if (activity != null) {
            if (typeSharing == TYPE_SHARE_CONTENT) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_share_content), getString(R.string.ga_share_content_click_other_app), "");
            } else if (typeSharing == TYPE_FORWARD_MESSAGE) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_forward_message), getString(R.string.ga_share_content_click_other_app), "");
            } else if (typeSharing == TYPE_SHARE_FROM_OTHER_APP) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_other_app), getString(R.string.ga_share_content_click_other_app), "");
            } else if (typeSharing == TYPE_SHARE_FROM_DEEP_LINK) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_from_deeplink), getString(R.string.ga_share_content_click_other_app), "");
            } else if (typeSharing == TYPE_SHARE_TO_FRIEND) {
                activity.trackingEvent(getString(R.string.ga_share_dialog_to_friend), getString(R.string.ga_share_content_click_other_app), "");
            }
        }
    }

    public void setTypeSharing(int typeSharing) {
        this.typeSharing = typeSharing;
        init();
    }

    public int getTypeSharing() {
        return typeSharing;
    }

    public void setTitleDialogChooseContact(String titleDialogChooseContact) {
        this.titleDialogChooseContact = titleDialogChooseContact;
    }

    public void dismissAll() {
        dismissChooseContacts();
        dismissPostOnSocial();
    }

    public void setPlayingState(boolean playingState) {
        isPlayingState = playingState;
    }

    public void setShareBusinessListener(ShareBusinessListener shareBusinessListener) {
        this.shareBusinessListener = shareBusinessListener;
    }

    private void changeColorStatusbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            activity.getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    private void sendSMS(String content) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(activity);

            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, content);

            if (defaultSmsPackageName != null) {
                sendIntent.setPackage(defaultSmsPackageName);
            }
            activity.startActivity(sendIntent);

        } else {
            Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address", "");
            smsIntent.putExtra("sms_body", content);
            activity.startActivity(smsIntent);
        }
    }

    private boolean keyboardShown(View rootView) {
        final int softKeyboardHeight = 100;
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
        int heightDiff = rootView.getBottom() - r.bottom;
        return heightDiff > softKeyboardHeight * dm.density;
    }
}

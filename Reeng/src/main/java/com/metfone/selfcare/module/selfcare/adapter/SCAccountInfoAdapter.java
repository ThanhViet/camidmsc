package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.model.SCAccountDataInfo;

import java.util.ArrayList;

public class SCAccountInfoAdapter extends BaseAdapter<BaseViewHolder> {

    private ArrayList<SCAccountDataInfo.SCValue> data;

    public SCAccountInfoAdapter(Context context) {
        super(context);
    }

    public void setItemsList(ArrayList<SCAccountDataInfo.SCValue> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_account_info, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        SCAccountDataInfo.SCValue model = data.get(position);
        if(model != null)
        {
            holder.setText(R.id.tvName, TextUtils.isEmpty(model.getTitle()) ? "" : model.getTitle());
            holder.setText(R.id.tvContent, TextUtils.isEmpty(model.getValue()) ? "" : model.getValue());
            if(TextUtils.isEmpty(model.getExp()))
            {
                holder.getView(R.id.tvExpire).setVisibility(View.GONE);
            }
            else
            {
                holder.getView(R.id.tvExpire).setVisibility(View.VISIBLE);
                holder.setText(R.id.tvExpire, TextUtils.isEmpty(model.getExp()) ? "" : model.getExp());
            }
        }
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }
}
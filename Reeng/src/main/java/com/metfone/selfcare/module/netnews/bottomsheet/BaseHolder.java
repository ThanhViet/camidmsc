package com.metfone.selfcare.module.netnews.bottomsheet;

import android.view.View;

import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import butterknife.ButterKnife;

/**
 * Created by HaiKE on 05/09/2015.
 */
public class BaseHolder extends BaseViewHolder {
    public BaseHolder(View itemView) {

        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

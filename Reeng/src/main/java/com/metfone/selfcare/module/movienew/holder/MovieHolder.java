/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movienew.holder;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.movienew.model.HomeData;

import butterknife.BindView;

public class MovieHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.img_movie)
    @Nullable
    ImageView imgMovie;

    private OnClickContentMovie listener;
    private Activity activity;
    private String blockName;

    public MovieHolder(View view, Activity activity, OnClickContentMovie listener,String blockName) {
        super(view);
        this.listener = listener;
        this.activity = activity;
        this.blockName = blockName;
    }

    @Override
    public void bindData(Object item, int position) {
        HomeData homeData = (HomeData) item;
        Glide.with(activity)
                .load(homeData.getPosterPath()).error(R.drawable.cinema_loading_bg).placeholder(R.drawable.cinema_loading_bg)
                .into(imgMovie);
        imgMovie.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) {
                    if(!StringUtils.isEmpty(blockName)){
                        ApplicationController.self().getFirebaseEventBusiness().logOpenFilmTopic(blockName);
                    }
                    listener.onClickMovieItem(homeData, getAdapterPosition());
                }
            }
        });
    }
}
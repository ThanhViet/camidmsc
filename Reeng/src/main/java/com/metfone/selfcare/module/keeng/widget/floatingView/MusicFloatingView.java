package com.metfone.selfcare.module.keeng.widget.floatingView;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.KeengPlayerActivity;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.SharedPref;

/**
 * ChatHead Service
 */
public class MusicFloatingView extends Service implements FloatingViewListener, PlayMusicController.OnPlayMusicStateChange {
    private static final String TAG = "MusicFloatingView";
    public static final String EXTRA_CUTOUT_SAFE_AREA = "cutout_safe_area";

    private FloatingViewManager mFloatingViewManager;
    private RotateAnimationCustom rotateAnimation;
    private ImageView imvChatHead;
    private ImageView btnPlay;

    private ApplicationController mApplication;
    private SharedPref pref;
    private MediaModel currentSong;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = (ApplicationController) getApplicationContext();
        pref = SharedPref.newInstance(this);
        if (rotateAnimation == null) {
            rotateAnimation = new RotateAnimationCustom(0.0f, 1.0f * 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotateAnimation.setDuration(5000);
            rotateAnimation.setInterpolator(new LinearInterpolator());
            rotateAnimation.setRepeatCount(Animation.INFINITE);
            rotateAnimation.setRepeatMode(Animation.INFINITE);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        if (mFloatingViewManager != null) {
//            return START_STICKY;
//        }

        if (startId == Service.START_STICKY) {
            final DisplayMetrics metrics = new DisplayMetrics();
            final WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(metrics);
            final LayoutInflater inflater = LayoutInflater.from(this);
            final View root = inflater.inflate(R.layout.layout_keeng_floating, null, false);
            imvChatHead = root.findViewById(R.id.imvChatHead);
            btnPlay = root.findViewById(R.id.btnPlay);
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Onclick");
                    Intent intent = new Intent(MusicFloatingView.this, KeengPlayerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    stopService();
                }
            });

            mFloatingViewManager = new FloatingViewManager(this, this);
            mFloatingViewManager.setFixedTrashIconImage(R.drawable.ic_trash_fixed);
            mFloatingViewManager.setActionTrashIconImage(R.drawable.ic_trash_action);
            mFloatingViewManager.setSafeInsetRect((Rect) intent.getParcelableExtra(EXTRA_CUTOUT_SAFE_AREA));
            final FloatingViewManager.Options options = new FloatingViewManager.Options();
            options.overMargin = (int) (16 * metrics.density);
            final int offset = (int) (48 + 8 * metrics.density);
//        options.floatingViewX = 0;
//        options.floatingViewY = metrics.heightPixels - offset - 300;

            //Lay du lieu vi tri cache
            int radius = getResources().getDimensionPixelOffset(R.dimen.floating_size);
            int posX = pref.getInt(Constants.KEY_FLOATING_VIEW_POS_X, -radius);
            int posY = pref.getInt(Constants.KEY_FLOATING_VIEW_POS_Y, metrics.heightPixels - offset - 300);
            if (posX < metrics.widthPixels / 2 - radius / 2) {
                posX = -radius;
            } else {
                posX = metrics.widthPixels;
            }
            options.floatingViewX = posX;
            options.floatingViewY = posY;

            mFloatingViewManager.addViewToWindow(root, options);

            //Load du lieu music
            loadData();
            //Add listener music
            PlayMusicController.addPlayMusicStateChange(this);

            return super.onStartCommand(intent, flags, startId);
        } else {
            return Service.START_NOT_STICKY;
        }

//        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        destroy();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind");
        return null;
    }

    @Override
    public void onFinishFloatingView() {
        Log.d(TAG, "FloatingView has been deleted.");
        //Stop Music
        mApplication.getPlayMusicController().closeMusic();

        //Stop service
        stopService();
    }

    @Override
    public void onTouchFinished(boolean isFinishing, int x, int y) {
        if (isFinishing) {
            Log.d(TAG, "FloatingView will be deleted soon");
        } else {
            Log.d(TAG, "Current position x:" + x + ", y:" + y);
            pref.putInt(Constants.KEY_FLOATING_VIEW_POS_X, x);
            pref.putInt(Constants.KEY_FLOATING_VIEW_POS_Y, y);
        }
    }

    private void destroy() {
        PlayMusicController.removePlayMusicStateChange(this);
        if (mFloatingViewManager != null) {
            mFloatingViewManager.removeAllViewToWindow();
            mFloatingViewManager = null;
        }
        stopService();
    }

    public static void stop(Context context) {
        if (context == null) return;
        context.stopService(new Intent(context, MusicFloatingView.class));
    }

    private void stopService() {
        stopForeground(true);
        stopSelf();
    }

    //Xu ly nhac
    @Override
    public void onChangeStateRepeat(int state) {

    }

    @Override
    public void onChangeStateNone() {

    }

    @Override
    public void onChangeStateGetData() {
        setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
    }

    @Override
    public void onChangeStatePreparing(MediaModel song) {
        setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
    }

    @Override
    public void onChangeStatePlaying(MediaModel song) {
        setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
    }

    @Override
    public void onActionFromUser(MediaModel song) {

    }

    @Override
    public void onCloseMusic() {
        stopService();
    }

    @Override
    public void onCallLoadDataFail(String message) {

    }

    @Override
    public void onUpdateSeekBarProgress(int percent, int currentMediaPosition) {

    }

    @Override
    public void onUpdateSeekBarBuffering(int percent) {

    }

    @Override
    public void onChangeStateInfo(int state) {

    }

    @Override
    public void onUpdateData(MediaModel song) {
        loadData();
    }

    public void setStateMediaPlayer(int stateMediaPlayer) {
        switch (stateMediaPlayer) {
            case Constants.PLAY_MUSIC.PLAYING_NONE:
            case Constants.PLAY_MUSIC.PLAYING_GET_INFO:
            case Constants.PLAY_MUSIC.PLAYING_PREPARING:
            case Constants.PLAY_MUSIC.PLAYING_RETRIEVING:
                btnPlay.setImageResource(R.drawable.exo_icon_play);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PLAYING:
                btnPlay.setImageResource(R.drawable.exo_icon_pause);
                playAnimation();
                break;
            case Constants.PLAY_MUSIC.PLAYING_PAUSED:
                btnPlay.setImageResource(R.drawable.exo_icon_play);
                pauseAnimation();
                break;
            default:
                btnPlay.setImageResource(R.drawable.exo_icon_play);
                break;
        }
    }

    private void playAnimation() {
        if (rotateAnimation != null)
            rotateAnimation.setPause(false);
    }

    private void pauseAnimation() {
        if (rotateAnimation != null)
            rotateAnimation.setPause(true);
    }

    private void startAnimation() {
        stopAnimation();

        if (rotateAnimation != null)
            rotateAnimation.setPause(true);

        if (imvChatHead != null) {
            imvChatHead.clearAnimation();
            imvChatHead.startAnimation(rotateAnimation);
        }
    }

    private void stopAnimation() {
        if (imvChatHead != null)
            imvChatHead.clearAnimation();
        if (rotateAnimation != null)
            rotateAnimation.setPause(true);
    }

    private void loadData() {
        currentSong = mApplication.getPlayMusicController().getCurrentSong();
        if (currentSong != null) {
            ImageBusiness.setImageNoAnime(currentSong.getImage(), R.drawable.ic_keeng, imvChatHead);

            startAnimation();
            setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
        }
    }
}

package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.model.metphone.BuyServiceCategoryModel;
import com.metfone.selfcare.module.metfoneplus.adapter.BuyServiceCategoryAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceCategoryViewHolder;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPSupportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPSupportFragment extends MPBaseFragment {
    public static final String TAG = "MPSupportFragment";

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.list_support)
    RecyclerView mListSupport;

    private BuyServiceCategoryAdapter mSupportListAdapter;
    private ArrayList<BuyServiceCategoryModel> models;

    public MPSupportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MPSupportFragment.
     */
    public static MPSupportFragment newInstance() {
        MPSupportFragment fragment = new MPSupportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_support;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mLayoutActionBar);
        mActionBarTitle.setText(getString(R.string.m_p_support_title));

        prepareData();
        mSupportListAdapter = new BuyServiceCategoryAdapter(getContext(), models, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BuyServiceCategoryViewHolder holder = (BuyServiceCategoryViewHolder) view.getTag();
                if (models.get(holder.mPosition).categoryName == R.string.m_p_support_contact_us_title) {
//                    addFragmentTransactionAllowLoss(MPContactUsFragment.newInstance(), MPContactUsFragment.TAG);
                    gotoMPContactUs();
                } else {
//                    addFragmentTransactionAllowLoss(MPStoreFragment.newInstance(), MPStoreFragment.TAG);
                    gotoMPStoreFragment();
                }
            }
        });

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mParentActivity, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mParentActivity, R.drawable.divider_list_service)));

        mListSupport.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListSupport.setHasFixedSize(true);
        mListSupport.setAdapter(mSupportListAdapter);
        mListSupport.addItemDecoration(dividerItemDecoration);
    }

    private void prepareData() {
        models = new ArrayList<>();
        models.add(new BuyServiceCategoryModel("",
                R.drawable.ic_contact_us_24, R.string.m_p_support_contact_us_title, R.string.m_p_support_contact_us_content,
                new int[]{ContextCompat.getColor(mParentActivity, R.color.m_p_yellow_gradient_top),
                        ContextCompat.getColor(mParentActivity, R.color.m_p_yellow_gradient_bottom)}));

        models.add(new BuyServiceCategoryModel("",
                R.drawable.ic_find_store_24, R.string.m_p_support_find_store_title, R.string.m_p_support_find_store_content,
                new int[]{ContextCompat.getColor(mParentActivity, R.color.m_p_violet_gradient_top),
                        ContextCompat.getColor(mParentActivity, R.color.m_p_violet_gradient_bottom)}));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        if(mParentActivity instanceof HomeActivity){
            ((HomeActivity)mParentActivity).setBottomNavigationBarColor(R.color.m_home_tab_background_2, true);
        }
        popBackStackFragment();
    }
}
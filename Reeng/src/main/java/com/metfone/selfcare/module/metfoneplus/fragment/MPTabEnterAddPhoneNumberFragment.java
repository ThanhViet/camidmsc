package com.metfone.selfcare.module.metfoneplus.fragment;

import static android.app.Activity.RESULT_OK;
import static com.metfone.selfcare.business.UserInfoBusiness.setSystemBarTheme;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;

import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.activity.OTPAddPhoneLoginActivity;
import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.fragment.login.EnterPhoneNumberFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.CheckPhoneInfo;
import com.metfone.selfcare.model.account.CheckPhoneRequest;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.module.metfoneplus.listener.OnClickShowFTTH;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.CheckCarrierResponse;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.login.SelectCountryActivity;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.jivesoftware.smack.Connection;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MPTabEnterAddPhoneNumberFragment extends MPBaseFragment implements ClickListener.IconListener{

    private static final String TAG = MPTabEnterAddPhoneNumberFragment.class.getSimpleName();
    private static final String PHONE_NUMBER_PARAM = "param1";// so nhan dien dc (dau vao)
    private static final String REGION_CODE_PARAM = "param2";
    private static final String NUMBER_JID_PARAM = "number_jid_param";
    public static final String NEED_CHANGE_PARAM = "need_change_color";

    RelativeLayout rlContainer;
    AppCompatEditText etNumber;
    CamIdTextView btnSkip;
    CamIdTextView tvpl_enterNumber;
    RoundTextView btnContinue;
    LinearLayout llCountryCode;
    ImageView ivFlCountry;
    TextView tvCountry;
    CamIdTextView tvEnterNumberDes;
    LinearLayout lnl_view;
    CamIdTextView mTvMetfone;
    ProgressBar pbLoading;

    int TYPE;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private ApplicationController mApplication;
    private String mCurrentRegionCode = "VN";// mac dinh vn
    private String mCurrentNumberJid;
    private ClickListener.IconListener mClickHandler;
    private boolean isSaveInstanceState = false;
    private ArrayList<Region> mRegions;
    private String numberInput;
    private int position;
    private String fromSource = "";
    BaseMPConfirmDialog baseMPConfirmDialog;
    private boolean isLoginDone = false;
    UserInfoBusiness userInfoBusiness;
    //--------------------End---------------------

    @Override
    public void onAttach(@NonNull Context context) {
        this.mParentActivity = (HomeActivity) context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        super.onAttach(context);
    }

    public static MPTabEnterAddPhoneNumberFragment newInstance() {
        MPTabEnterAddPhoneNumberFragment fragment = new MPTabEnterAddPhoneNumberFragment();
        return fragment;
    }

    private OnClickShowFTTH onClickShowFTTH;

    public void setListener(OnClickShowFTTH showFTTHListener) {
        onClickShowFTTH = showFTTHListener;
    }

    private RadioButton btnSwitchInternet;
    private RadioButton btnSwitchMobile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userInfoBusiness = new UserInfoBusiness(getContext());
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE );
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_phone_metfone;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        rlContainer = view.findViewById(R.id.rlContainer);
        etNumber = view.findViewById(R.id.etNumber);
        btnSkip = view.findViewById(R.id.btnSkip);
        tvpl_enterNumber = view.findViewById(R.id.tvpl_enterNumber);
        btnContinue = view.findViewById(R.id.btnContinue);
        llCountryCode = view.findViewById(R.id.llCountryCode);
        ivFlCountry = view.findViewById(R.id.ivFlCountry);
        tvCountry = view.findViewById(R.id.tvCountry);
        tvEnterNumberDes = view.findViewById(R.id.tvEnterNumberDes);
        lnl_view = view.findViewById(R.id.lnl_view);
        mTvMetfone = view.findViewById(R.id.tv_metfone);
        pbLoading = view.findViewById(R.id.pbLoading);
        btnSwitchMobile = view.findViewById(R.id.btnSwitchMobile);
        btnSwitchInternet = view.findViewById(R.id.btnSwitchInternet);
        btnSwitchInternet.setOnClickListener(this::handleSwitchInternet);

        UIUtil.showKeyboard(getContext(),etNumber);
        setViewListeners();
        etNumber.requestFocus();
        tvEnterNumberDes.setText(getText(R.string.link_your));
        setupUI(rlContainer);
        setSpinnerView();
        setColor();

    }

    private void handleSwitchInternet(View view) {
        if (onClickShowFTTH != null) onClickShowFTTH.showFTTH();
        btnSwitchMobile.setChecked(true);
        btnSwitchInternet.setChecked(false);
    }


    @Override
    public void onPause() {
        super.onPause();
        InputMethodUtils.hideSoftKeyboard(etNumber, getContext());
        mClickHandler = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mClickHandler = this;
        isSaveInstanceState = false;
        enableOrDisableContinueButton();
        UIUtil.showKeyboard(getContext(),etNumber);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER_PARAM, numberInput);
        outState.putString(REGION_CODE_PARAM, mCurrentRegionCode);
        outState.putString(NUMBER_JID_PARAM, mCurrentNumberJid);
        outState.putString(Constants.KEY_POSITION, fromSource);
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }

    public void updateRegionCode(int index) {
        mRegions.get(position).setSelected(false);

        position = index;
        Region region = mRegions.get(index);
        region.setSelected(true);
        mCurrentRegionCode = region.getRegionCode();
        String patch = "file:///android_asset/fl_country/" + region.getRegionCode().toLowerCase() + ".png";
        Glide.with(this)
                .load(Uri.parse(patch))
                .apply(new RequestOptions()
                        .error(R.color.gray)
                        .fitCenter())
                .into(ivFlCountry);

        String text = region.getRegionName();
        text = text.split("\\(")[1];
        text = text.substring(0, text.length() - 1);
        tvCountry.setText(text);
    }

    /**
     * find all View
     */

    private void setViewListeners() {
        enableOrDisableContinueButton();
        setPhoneNumberEditTextListener();
    }

    private void setSpinnerView() {
        mRegions = mApplication.getLoginBusiness().getAllRegions();
        updateRegionCode(35);
    }

    /**
     * listener for change of phoneNumber EditText -> enable or disable button
     */
    private void setPhoneNumberEditTextListener() {
        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                enableOrDisableContinueButton();
            }

            @Override
            public void afterTextChanged(Editable s) {
                mTvMetfone.setVisibility(View.GONE);
            }
        });

        etNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String phone = Objects.requireNonNull(etNumber.getText()).toString().trim();
                    if (TextUtils.isEmpty(phone)) {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                    } else if (phone.length() < 8 || phone.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                    } else {
                        if (ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
                            checkCarrier(etNumber.getText().toString());
                        } else {
                            checkPhone(etNumber.getText().toString());
                        }
                    }
                }
                return false;
            }
        });
    }

    /**
     * enable or disable continue Button
     */
    private void enableOrDisableContinueButton() {
        if (etNumber.getText().toString().trim().length() >= Constants.CONTACT.MIN_LENGTH_NUMBER) {
            setEnableButton(true);
        } else {
            setEnableButton(false);
        }
    }

    private void setEnableButton(boolean enableButton) {
        if (btnContinue != null) {
            btnContinue.setClickable(enableButton);
            if (enableButton) {
                btnContinue.setTextColor(ContextCompat.getColor(getContext(), R.color.v5_text_7));
                btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(getContext(), R.color.button_continue),
                        ContextCompat.getColor(getContext(), R.color.button_continue));
            } else {
                btnContinue.setTextColor(ContextCompat.getColor(getContext(), R.color.v5_text_3));
                btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(getContext(), R.color.v5_cancel),
                        ContextCompat.getColor(getContext(), R.color.v5_cancel));
            }
        }

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_EXIT:
                //                mLoginActivity.finish();
                break;
            default:
                break;
        }
    }

    private void setColor() {
            drawBackground();
            setSystemBarTheme(Objects.requireNonNull(getActivity()),true);
            tvEnterNumberDes.setTextColor(Color.WHITE);
            tvpl_enterNumber.setTextColor(Color.WHITE);
            UserInfoBusiness.setCursorColor(etNumber, Color.WHITE);
            btnSkip.setVisibility(View.GONE);
    }

    // xu ly khi gui yc sinh ma
    private void processClickContinue(int checkPhoneCode) {
        Intent intent = new Intent(getContext(), OTPAddPhoneLoginActivity.class);
        String phoneNumber = Objects.requireNonNull(etNumber.getText()).toString();
        if (phoneNumber.startsWith("0")) {
            intent.putExtra(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
        } else {
            intent.putExtra(EnumUtils.PHONE_NUMBER_KEY, "0" + phoneNumber);
        }
        intent.putExtra(EnumUtils.KEY_TYPE, TYPE);
        intent.putExtra(EnumUtils.CHECK_PHONE_CODE_TYPE, checkPhoneCode);
        startActivityForResult(intent, EnumUtils.MY_REQUEST_CODE);
    }

    private void drawBackground() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            rlContainer.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_frg_set_up_information));
        } else {
            rlContainer.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_frg_set_up_information));
        }
    }

    @OnClick({R.id.btnContinue, R.id.btnSkip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinue:
                String phone = etNumber.getText().toString().trim();
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                } else if (phone.length() < 8 || phone.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                } else {
                    if (ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
                        checkCarrier(etNumber.getText().toString());
                    } else {
                        checkPhone(etNumber.getText().toString());
                    }
                }
                break;
            case R.id.btnSkip:
//                finish();
                break;
        }
    }


    private void checkPhone(String phoneNumber) {

        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        CheckPhoneInfo checkPhoneInfo = new CheckPhoneInfo(phoneNumber);

        CheckPhoneRequest checkPhoneRequest = new CheckPhoneRequest(checkPhoneInfo, "", "", "", "");
        apiService.checkPhone(checkPhoneRequest).enqueue(new Callback<com.metfone.selfcare.model.account.BaseResponse<String>>() {
            @Override
            public void onResponse(Call<com.metfone.selfcare.model.account.BaseResponse<String>> call, retrofit2.Response<com.metfone.selfcare.model.account.BaseResponse<String>> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(),etNumber);
                    }
                }, 200);
                if (response.body() != null) {
                    if (response.body().getCode().equals("00") || response.body().getCode().equals("42") || response.body().getCode().equals("68")) {
                        //case exist in camID
                        baseMPConfirmDialog = new BaseMPConfirmDialog(R.drawable.iv_camid_account,
                                R.string.m_p_dialog_combine_phone_title,
                                R.string.m_p_dialog_combine_phone_content,
                                R.string.m_p_dialog_combine_phone_top_btn,
                                R.string.m_p_dialog_combine_phone_bottom_btn,
                                view -> {
                                    //letdoit
                                    processClickContinue(42);
                                    if (baseMPConfirmDialog != null) {
                                        baseMPConfirmDialog.dismiss();
                                    }
                                }, view -> {
                            //back

                            if (baseMPConfirmDialog != null) {
                                baseMPConfirmDialog.dismiss();
                            }
                        });
                        baseMPConfirmDialog.setBgWhite(false);
                        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
                            baseMPConfirmDialog.setBgWhite(true);
                        }
//                        baseMPConfirmDialog.show(getSupportFragmentManager(), null);
                        baseMPConfirmDialog.show(getParentFragmentManager(), null);
                    } else {
                        processClickContinue(0);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), getString(R.string.service_error));
            }
        });
    }

    @OnClick(R.id.llCountryCode)
    public void onViewClicked() {
        SelectCountryActivity.startActivityForResult(mParentActivity, position, LoginActivity.REQUEST_CODE_COUNTRY);
    }

    private void getUserInformation() {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        FireBaseHelper.getInstance(ApplicationController.self()).checkServiceAndRegister(true);
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                        reengAccount.setToken(token);
                        if (reengAccount != null) {
                            reengAccount.setName(userInfo.getFull_name());
                            reengAccount.setEmail(userInfo.getEmail());
                            reengAccount.setBirthday(userInfo.getDate_of_birth());
                            reengAccount.setAddress(userInfo.getAddress());
                            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                        }
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
                        mCurrentRegionCode = "KH";
                        if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
                            if (userInfo.getPhone_number().startsWith("0")) {
                                mCurrentNumberJid = "+855" + userInfo.getPhone_number().substring(1);
                            } else {
                                mCurrentNumberJid = "+855" + userInfo.getPhone_number();
                            }
                            String originToken = token.substring(7);
                            generateOldMochaOTP(originToken);
                        } else {
                            NavigateActivityHelper.navigateToHomeScreenActivity(mParentActivity, true, false);
                        }
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getMessage());
                    }

                } else {
                    ToastUtils.showToast(getContext(), "Get user information fail");
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), getString(R.string.service_error));
            }
        });
    }

    private void generateOldMochaOTP(String token) {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(getContext()).detectSubscription(json);
                        doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(getContext(), "generateOldMochaOTP Failed");
                }
            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), error.getMessage());
            }
        });

    }

    public void setupUI(View view) {

        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(getActivity());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
                        }
                    }, 100);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
        mApplication.getXmppManager().manualDisconnect();
        new LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(pbLoading != null)
                pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) Objects.requireNonNull(getActivity()).getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);

            try {
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    if(pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    RestoreManager.setRestoring(true);
                    userInfoBusiness.autoBackUp(mApplication, new DBImporter.RestoreProgressListener() {
                        @Override
                        public void onStartDownload() {

                        }

                        @Override
                        public void onDownloadProgress(int percent) {

                        }

                        @Override
                        public void onDownloadComplete() {

                        }

                        @Override
                        public void onDowloadFail(String message) {

                        }

                        @Override
                        public void onStartRestore() {

                        }

                        @Override
                        public void onRestoreProgress(int percent) {

                        }

                        @Override
                        public void onRestoreComplete(int messageCount, int threadMessageCount) {
                            try {
                                SharedPreferences pref = getContext().getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                                if (pref != null) {
                                    pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
                                    com.metfone.selfcare.util.Log.i(TAG, "setPassedRestorePhase");
                                }
                            } catch (Exception e) {
                            }
                            RestoreManager.setRestoring(false);
                            mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
                            mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
                            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
                            NavigateActivityHelper.navigateToHomeScreenActivity(mParentActivity, true, false);
                        }

                        @Override
                        public void onRestoreFail(String message) {
                            RestoreManager.setRestoring(false);
                            NavigateActivityHelper.navigateToHomeScreenActivity(mParentActivity, true, false);
                        }
                    }, mParentActivity);
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_complete));
                } else {
                    if(pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    ToastUtils.showToast(getContext(), responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_fail));
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        }
    }

    private void checkCarrier(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.equals("")) return;
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.checkCarrier(phoneNumber, new ApiCallback<CheckCarrierResponse>() {
            @Override
            public void onResponse(Response<CheckCarrierResponse> response) {
                if (response.body() != null) {
                    String code = response.body().getCode();
                    android.util.Log.d(TAG, "onResponse: " + response.body().getMessage());

                    if ("00".equals(code)) {
                        if (response.body().getData().getMetfone()) {
                            mTvMetfone.setVisibility(View.GONE);
                            checkPhone(phoneNumber);
                        } else {
                            mTvMetfone.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mTvMetfone.setText(View.VISIBLE);
                        mTvMetfone.setText(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                android.util.Log.e(TAG, "onError: ", error);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EnumUtils.MY_REQUEST_CODE && resultCode == RESULT_OK && data != null){
            int result= data.getIntExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
            Intent returnIntent = new Intent();
            android.util.Log.d(TAG, "onActivityResult: " + result);
            returnIntent.putExtra(EnumUtils.RESULT, result);
            Objects.requireNonNull(getActivity()).setResult(RESULT_OK, returnIntent);
            if (result ==  EnumUtils.KeyBackTypeDef.DONE){
//                finish();
                popBackStackFragment();
            }
        }
    }
}
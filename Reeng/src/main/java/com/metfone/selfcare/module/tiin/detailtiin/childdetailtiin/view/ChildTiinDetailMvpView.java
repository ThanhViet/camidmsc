package com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.view;

import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.util.List;

public interface ChildTiinDetailMvpView extends MvpView {
    void loadDataSuccess(boolean flag);

    void bindData(TiinModel response);

    void bindDataRelate(List<TiinModel> response);

    void binDataSibling(List<TiinModel> response);

    void loadDataStatus(boolean flag);

    void loadDataFeed(FeedModelOnMedia model);
}

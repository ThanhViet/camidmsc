package com.metfone.selfcare.module.metfoneplus.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.metfone.selfcare.R;

public class MPDialogFragment extends DialogFragment {
    public static final String TAG = MPDialogFragment.class.getSimpleName();

    private static final String TITLE_PARAM = "title_param";
    private static final String TITLE_STRING_PARAM = "title_string_param";
    private static final String CONTENT_PARAM = "content_param";
    private static final String CONTENT_STRING_PARAM = "content_string_param";
    private static final String DRAWABLE_IMAGE_PARAM = "drawable_image_param";
    private static final String LOTTIE_ASSERT_NAME_PARAM = "lottie_assert_name_param";
    private static final String NAME_TOP_BUTTON_PARAM = "name_top_button_param";
    private static final String NAME_MIDDLE_BUTTON_PARAM = "name_middle_button_param";
    private static final String NAME_BOTTOM_BUTTON_PARAM = "name_bottom_button_param";
    private static final String DRAWABLE_TOP_BUTTON_PARAM = "drawable_top_button_param";
    private static final String DRAWABLE_MIDDLE_BUTTON_PARAM = "drawable_middle_button_param";
    private static final String DRAWABLE_BOTTOM_BUTTON_PARAM = "drawable_bottom_button_param";
    private static final String TIME_DELAY_AUTO_DISMISS_PARAM = "time_delay_auto_dismiss_param";
    private static final String CANCELABLE_TOUCH_OUTSIDE_PARAM = "cancelable_touch_outside_param";

    private Context mContext;

    private AppCompatImageView mImage;
    private LottieAnimationView mLottieView;
    private AppCompatTextView mTitle;
    private AppCompatTextView mContent;
    private RelativeLayout mButtonTopLayout;
    private View mButtonTopBackground;
    private AppCompatButton mButtonTop;
    private RelativeLayout mButtonMiddleLayout;
    private View mButtonMiddleBackground;
    private AppCompatButton mButtonMiddle;
    private RelativeLayout mButtonBottomLayout;
    private View mButtonBottomBackground;
    private AppCompatButton mButtonBottom;
    private RelativeLayout rlContainer;
    private LinearLayout lnlContent;

    private ButtonOnClickListener mButtonOnClickListener;

    public MPDialogFragment() {

    }

    public static MPDialogFragment newInstance(int idStringTitle, String strTitle,
                                               int idStringContent, String strContent,
                                               int idDrawableImage,
                                               String lottieAssertName,
                                               int idStringNameButtonTop, int idDrawableButtonTop,
                                               int idStringNameButtonMiddle, int idDrawableButtonMiddle,
                                               int idStringNameButtonBottom, int idDrawableButtonBottom,
                                               int timeDelayAutoDismiss,
                                               boolean cancelableTouchOutside) {
        Bundle args = new Bundle();
        args.putInt(TITLE_PARAM, idStringTitle);
        args.putString(TITLE_STRING_PARAM, strTitle);
        args.putInt(CONTENT_PARAM, idStringContent);
        args.putString(CONTENT_STRING_PARAM, strContent);
        args.putInt(DRAWABLE_IMAGE_PARAM, idDrawableImage);
        args.putString(LOTTIE_ASSERT_NAME_PARAM, lottieAssertName);
        args.putInt(NAME_TOP_BUTTON_PARAM, idStringNameButtonTop);
        args.putInt(DRAWABLE_TOP_BUTTON_PARAM, idDrawableButtonTop);
        args.putInt(NAME_MIDDLE_BUTTON_PARAM, idStringNameButtonMiddle);
        args.putInt(DRAWABLE_MIDDLE_BUTTON_PARAM, idDrawableButtonMiddle);
        args.putInt(NAME_BOTTOM_BUTTON_PARAM, idStringNameButtonBottom);
        args.putInt(DRAWABLE_BOTTOM_BUTTON_PARAM, idDrawableButtonBottom);
        args.putInt(TIME_DELAY_AUTO_DISMISS_PARAM, timeDelayAutoDismiss);
        args.putBoolean(CANCELABLE_TOUCH_OUTSIDE_PARAM, cancelableTouchOutside);

        MPDialogFragment fragment = new MPDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFullScreen);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            getDialog().getWindow().setDimAmount(0.7f);
        }
        View view = inflater.inflate(R.layout.dialog_fragment_mp, container, false);

        mImage = view.findViewById(R.id.dialog_mp_img);
        mLottieView = view.findViewById(R.id.dialog_mp_lottie_view);
        mTitle = view.findViewById(R.id.dialog_mp_title);
        mContent = view.findViewById(R.id.dialog_mp_content);

        mButtonTopLayout = view.findViewById(R.id.dialog_mp_top_layout_button);
        mButtonTopBackground = view.findViewById(R.id.dialog_mp_top_background_button);
        mButtonTop = view.findViewById(R.id.dialog_mp_top_title_button);

        mButtonMiddleLayout = view.findViewById(R.id.dialog_mp_middle_layout_button);
        mButtonMiddleBackground = view.findViewById(R.id.dialog_mp_middle_background_button);
        mButtonMiddle = view.findViewById(R.id.dialog_mp_middle_title_button);

        mButtonBottomLayout = view.findViewById(R.id.dialog_mp_bottom_layout_button);
        mButtonBottomBackground = view.findViewById(R.id.dialog_mp_bottom_background_button);
        mButtonBottom = view.findViewById(R.id.dialog_mp_bottom_title_button);

        rlContainer = view.findViewById(R.id.rlContainer);
        lnlContent = view.findViewById(R.id.lnlContent);

        initialDialog();

        return view;
    }

    private void initialDialog() {
        if (getArguments() != null) {
            Bundle bundle = getArguments();

            int drawableImage = bundle.getInt(DRAWABLE_IMAGE_PARAM);
            if (drawableImage == -1) {
                mImage.setVisibility(View.GONE);
            } else {
                mImage.setVisibility(View.VISIBLE);
                mLottieView.setVisibility(View.GONE);
                mImage.setImageResource(drawableImage);
            }

            String lottieAssertName = bundle.getString(LOTTIE_ASSERT_NAME_PARAM);
            if (lottieAssertName == null) {
                mLottieView.setVisibility(View.GONE);
                mLottieView.cancelAnimation();
            } else {
                mImage.setVisibility(View.GONE);
                mLottieView.setVisibility(View.VISIBLE);
                mLottieView.setAnimation(lottieAssertName);
                mLottieView.setRepeatCount(LottieDrawable.INFINITE);
                mLottieView.playAnimation();
            }

            int title = bundle.getInt(TITLE_PARAM);
            String strTitle = bundle.getString(TITLE_STRING_PARAM, null);
            if (title == -1 && strTitle == null) {
                mTitle.setVisibility(View.GONE);
            } else {
                mTitle.setVisibility(View.VISIBLE);
                if (title != -1) {
                    mTitle.setText(title);
                } else {
                    mTitle.setText(strTitle);
                }
            }

            int content = bundle.getInt(CONTENT_PARAM);
            String strContent = bundle.getString(CONTENT_STRING_PARAM, null);
            if (content == -1 && strContent == null) {
                mContent.setVisibility(View.GONE);
            } else {
                mContent.setVisibility(View.VISIBLE);
                if (content != -1) {
                    mContent.setText(content);
                } else {
                    mContent.setText(strContent);
                }
            }

            int nameTopButton = bundle.getInt(NAME_TOP_BUTTON_PARAM);
            int drawableTopButton = bundle.getInt(DRAWABLE_TOP_BUTTON_PARAM);
            if (nameTopButton == -1) {
                mButtonTopLayout.setVisibility(View.GONE);
            } else {
                mButtonTopLayout.setVisibility(View.VISIBLE);
                mButtonTop.setText(nameTopButton);
                mButtonTop.setOnClickListener((v) -> {
                    if (mButtonOnClickListener != null) {
                        mButtonOnClickListener.onTopButtonClick();
                    }
                });

                if (drawableTopButton == -1) {
                    mButtonTopBackground.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_red_button_corner_6));
                } else {
                    mButtonTopBackground.setBackground(ContextCompat.getDrawable(mContext, drawableTopButton));
                }
            }

            int nameMiddleButton = bundle.getInt(NAME_MIDDLE_BUTTON_PARAM);
            int drawableMiddleButton = bundle.getInt(DRAWABLE_MIDDLE_BUTTON_PARAM);
            if (nameMiddleButton == -1) {
                mButtonMiddleLayout.setVisibility(View.GONE);
            } else {
                mButtonMiddleLayout.setVisibility(View.VISIBLE);
                mButtonMiddle.setText(nameMiddleButton);
                mButtonMiddle.setOnClickListener((v) -> {
                    if (mButtonOnClickListener != null) {
                        mButtonOnClickListener.onMiddleButtonClick();
                    }
                });

                if (drawableMiddleButton == -1) {
                    mButtonMiddleBackground.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_yellow_button_corner_6));
                } else {
                    mButtonMiddleBackground.setBackground(ContextCompat.getDrawable(mContext, drawableMiddleButton));
                }
            }

            int nameBottomButton = bundle.getInt(NAME_BOTTOM_BUTTON_PARAM);
            int drawableBottomButton = bundle.getInt(DRAWABLE_BOTTOM_BUTTON_PARAM);
            if (nameBottomButton == -1) {
                mButtonBottomLayout.setVisibility(View.GONE);
            } else {
                mButtonBottomLayout.setVisibility(View.VISIBLE);
                mButtonBottom.setText(nameBottomButton);
                mButtonBottom.setOnClickListener((v) -> {
                    if (mButtonOnClickListener != null) {
                        mButtonOnClickListener.onBottomButtonClick();
                    }
                });

                if (drawableBottomButton == -1) {
                    mButtonBottomBackground.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_transparent_button_corner_6));
                } else {
                    mButtonBottomBackground.setBackground(ContextCompat.getDrawable(mContext, drawableBottomButton));
                }
            }

            if (bundle.getBoolean(CANCELABLE_TOUCH_OUTSIDE_PARAM, false)) {
                rlContainer.setOnClickListener((v) -> dismissAllowingStateLoss());
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            int timeDelayAutoDismiss = getArguments().getInt(TIME_DELAY_AUTO_DISMISS_PARAM);
            if (timeDelayAutoDismiss != -1) {
                new Handler(Looper.getMainLooper()).postDelayed(this::dismissAllowingStateLoss, timeDelayAutoDismiss);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mLottieView != null) {
            mLottieView.cancelAnimation();
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mButtonOnClickListener != null) {
            mButtonOnClickListener.onDialogDismiss();
        }
    }

    public void addButtonOnClickListener(ButtonOnClickListener buttonOnClickListener) {
        this.mButtonOnClickListener = buttonOnClickListener;
    }

    public interface ButtonOnClickListener {
        default void onTopButtonClick() {

        }

        default void onMiddleButtonClick() {

        }

        default void onBottomButtonClick() {

        }

        default void onDialogDismiss() {

        }
    }

    public final static class Builder {
        private int title = -1;
        private String strTitle = null;
        private int content = -1;
        private String strContent = null;
        private int drawableImage = -1;
        private String lottieAssertName = null;
        private int nameButtonTop = -1;
        private int nameButtonMiddle = -1;
        private int nameButtonBottom = -1;
        private int drawableButtonTop = -1;
        private int drawableButtonMiddle = -1;
        private int drawableButtonBottom = -1;
        private int timeDelayAutoDismiss = -1;
        private boolean cancelableOnTouchOutside = false;

        public Builder() {

        }

        public Builder setTitle(int idStringTitle) {
            this.title = idStringTitle;
            return this;
        }

        public Builder setContent(int idStringContent) {
            this.content = idStringContent;
            return this;
        }

        public Builder setContent(String content) {
            this.strContent = content;
            return this;
        }

        public Builder setImage(int idDrawableImage) {
            this.drawableImage = idDrawableImage;
            return this;
        }

        public Builder setLottieAssertName(String lottieAssertName) {
            this.lottieAssertName = lottieAssertName;
            return this;
        }

        public Builder setButtonTop(int idStringNameButtonTop, int idDrawableButtonTop) {
            this.nameButtonTop = idStringNameButtonTop;
            this.drawableButtonTop = idDrawableButtonTop;
            return this;
        }

        public Builder setNameButtonTop(int idStringNameButtonTop) {
            this.nameButtonTop = idStringNameButtonTop;
            return this;
        }

        public Builder setDrawableButtonTop(int idDrawableButtonTop) {
            this.drawableButtonTop = idDrawableButtonTop;
            return this;
        }

        public Builder setButtonMiddle(int idStringNameButtonMiddle, int idDrawableButtonMiddle) {
            this.nameButtonMiddle = idStringNameButtonMiddle;
            this.drawableButtonMiddle = idDrawableButtonMiddle;
            return this;
        }

        public Builder setNameButtonMiddle(int idStringNameButtonMiddle) {
            this.nameButtonMiddle = idStringNameButtonMiddle;
            return this;
        }

        public Builder setDrawableButtonMiddle(int idDrawableButtonMiddle) {
            this.drawableButtonMiddle = idDrawableButtonMiddle;
            return this;
        }

        public Builder setButtonBottom(int idStringNameButtonBottom, int idDrawableButtonBottom) {
            this.nameButtonBottom = idStringNameButtonBottom;
            this.drawableButtonBottom = idDrawableButtonBottom;
            return this;
        }

        public Builder setNameButtonBottom(int idStringNameButtonBottom) {
            this.nameButtonBottom = idStringNameButtonBottom;
            return this;
        }

        public Builder setDrawableButtonBottom(int idDrawableButtonBottom) {
            this.drawableButtonBottom = idDrawableButtonBottom;
            return this;
        }

        public Builder setTimeDelayAutoDismiss(int timeDelayAutoDismiss) {
            this.timeDelayAutoDismiss = timeDelayAutoDismiss;
            return this;
        }

        public Builder setCancelableOnTouchOutside(boolean cancelableOnTouchOutside) {
            this.cancelableOnTouchOutside = cancelableOnTouchOutside;
            return this;
        }

        public MPDialogFragment build() {
            return MPDialogFragment.newInstance(this.title
                    , this.strTitle
                    , this.content
                    , this.strContent
                    , this.drawableImage
                    , this.lottieAssertName
                    , this.nameButtonTop
                    , this.drawableButtonTop
                    , this.nameButtonMiddle
                    , this.drawableButtonMiddle
                    , this.nameButtonBottom
                    , this.drawableButtonBottom
                    , this.timeDelayAutoDismiss
                    , this.cancelableOnTouchOutside);
        }
    }


    /*
    * fix bug firebase: Fragment MPAddPhoneFragment{1805acd} (33cb8ccb-1557-4d09-a956-eca272bd2090)} has not been attached yet.
    * */
    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException ignored) {
        }
    }
}

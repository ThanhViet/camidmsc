package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Data;

public class WsGetListGiftReceivedRequest extends BaseRequest<WsGetListGiftReceivedRequest.Request> {
    public static class Request {
    }
    public static class WsRequest extends Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("language")
        String language;
        @SerializedName("custId")
        public String custId;

        public WsRequest(String isdn, String language) {
            this.isdn = isdn;
            this.language = language;
        }
    }
}

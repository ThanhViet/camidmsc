/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/7
 *
 */

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class SearchHistoryProvisional extends Provisional {

    private ArrayList<SearchHistory> data;

    public SearchHistoryProvisional() {
    }

    public ArrayList<SearchHistory> getData() {
        return data;
    }

    public void setData(ArrayList<SearchHistory> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return Utilities.notEmpty(data) ? 1 : 0;
    }
}

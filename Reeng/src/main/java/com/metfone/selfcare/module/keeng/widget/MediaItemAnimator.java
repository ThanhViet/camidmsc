package com.metfone.selfcare.module.keeng.widget;

import android.animation.AnimatorSet;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import android.view.animation.OvershootInterpolator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MediaItemAnimator extends DefaultItemAnimator {
    // private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR =
    // new DecelerateInterpolator();
    // private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new
    // AccelerateInterpolator();
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);

    Map<RecyclerView.ViewHolder, AnimatorSet> likeAnimationsMap = new HashMap<>();
    Map<RecyclerView.ViewHolder, AnimatorSet> heartAnimationsMap = new HashMap<>();

    private int lastAddAnimatedItem = -2;

    @Override
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
        return true;
    }

    @NonNull
    @Override
    public ItemHolderInfo recordPreLayoutInformation(@NonNull RecyclerView.State state, @NonNull RecyclerView.ViewHolder viewHolder, int changeFlags, @NonNull List<Object> payloads) {
        if (changeFlags == FLAG_CHANGED) {
            for (Object payload : payloads) {
                if (payload instanceof String) {
                    return new FeedItemHolderInfo((String) payload);
                }
            }
        }

        return super.recordPreLayoutInformation(state, viewHolder, changeFlags, payloads);
    }

//    @Override
//    public boolean animateAdd(RecyclerView.ViewHolder viewHolder) {
//        if (viewHolder.getItemViewType() != BaseAdapterMedia.ITEM_LOAD_MORE) {
//            if (viewHolder.getLayoutPosition() > lastAddAnimatedItem) {
//                lastAddAnimatedItem++;
//                if (viewHolder instanceof MediaSocialHolder)
//                    runEnterAnimation((MediaSocialHolder) viewHolder);
//                return false;
//            }
//        }
//
//        dispatchAddFinished(viewHolder);
//        return false;
//    }

//    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
//    private void runEnterAnimation(final MediaSocialHolder holder) {
//        final int screenHeight = DeviceUtils.getScreenHeight(holder.itemView.getContext());
//        holder.itemView.setTranslationY(screenHeight);
//        holder.itemView.animate().translationY(0).setInterpolator(new DecelerateInterpolator(3.f)).setDuration(700).setListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                dispatchAddFinished(holder);
//            }
//        }).start();
//    }

//    @Override
//    public boolean animateChange(@NonNull RecyclerView.ViewHolder oldHolder, @NonNull RecyclerView.ViewHolder newHolder, @NonNull ItemHolderInfo preInfo, @NonNull ItemHolderInfo postInfo) {
//        cancelCurrentAnimationIfExists(newHolder);
//
//        if (preInfo instanceof FeedItemHolderInfo) {
//            // FeedItemHolderInfo feedItemHolderInfo = (FeedItemHolderInfo)
//            // preInfo;
//            MediaSocialHolder holder = (MediaSocialHolder) newHolder;
//            animateHeartButton(holder);
//            if (holder.getItem() != null)
//                updateLikesCounter(holder, (int) holder.getItem().getTotal_like());
//        }
//
//        return false;
//    }

    private void cancelCurrentAnimationIfExists(RecyclerView.ViewHolder item) {
        if (likeAnimationsMap.containsKey(item)) {
            likeAnimationsMap.get(item).cancel();
        }
        if (heartAnimationsMap.containsKey(item)) {
            heartAnimationsMap.get(item).cancel();
        }
    }

//    private void animateHeartButton(final MediaSocialHolder holder) {
//        AnimatorSet animatorSet = new AnimatorSet();
//
//        // ObjectAnimator rotationAnim = ObjectAnimator.ofFloat(holder.btnLike,
//        // "rotation", 0f, 360f);
//        // rotationAnim.setDuration(300);
//        // rotationAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
//
//        ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(holder.tvLike, "scaleX", 0.2f, 1f);
//        bounceAnimX.setDuration(300);
//        bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);
//
//        ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(holder.tvLike, "scaleY", 0.2f, 1f);
//        bounceAnimY.setDuration(300);
//        bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);
//        bounceAnimY.addListener(new AnimatorListenerAdapter() {
//            @SuppressWarnings("deprecation")
//            @Override
//            public void onAnimationStart(Animator animation) {
//                holder.tvLike.setTextColor(holder.tvLike.getResources().getColor(R.color.item_content));
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                heartAnimationsMap.remove(holder);
//                dispatchChangeFinishedIfAllAnimationsEnded(holder);
//            }
//        });
//
//        animatorSet.play(bounceAnimX).with(bounceAnimY);
//        animatorSet.start();
//
//        heartAnimationsMap.put(holder, animatorSet);
//    }
//
//    private void updateLikesCounter(MediaSocialHolder holder, int toValue) {
//        String likesCountTextFrom = Utilities.formatNumber(toValue - 1);
//        holder.tvLike.setText(likesCountTextFrom);
//    }
//    private void dispatchChangeFinishedIfAllAnimationsEnded(MediaSocialHolder holder) {
//        if (likeAnimationsMap.containsKey(holder) || heartAnimationsMap.containsKey(holder)) {
//            return;
//        }
//
//        dispatchAnimationFinished(holder);
//    }

    @Override
    public void endAnimation(RecyclerView.ViewHolder item) {
        super.endAnimation(item);
        cancelCurrentAnimationIfExists(item);
    }

    @Override
    public void endAnimations() {
        super.endAnimations();
        for (AnimatorSet animatorSet : likeAnimationsMap.values()) {
            animatorSet.cancel();
        }
    }

    public static class FeedItemHolderInfo extends ItemHolderInfo {
        public String updateAction;

        public FeedItemHolderInfo(String updateAction) {
            this.updateAction = updateAction;
        }
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.listener;

import java.util.ArrayList;

public interface SearchContactsListener {
    void onPrepareSearchContacts();

    void onFinishedSearchContacts(String keySearch, ArrayList<Object> list);
}

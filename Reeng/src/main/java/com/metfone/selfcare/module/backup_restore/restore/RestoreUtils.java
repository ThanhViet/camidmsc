package com.metfone.selfcare.module.backup_restore.restore;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.backup_restore.BackupMessageModel;
import com.metfone.selfcare.module.backup_restore.BackupThreadMessageModel;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class RestoreUtils {
    public static final String TAG = RestoreUtils.class.getSimpleName();

    public static ReengMessage convertToReengMessage(BackupMessageModel backupMessageModel, ReengMessage reengMessage, ThreadMessage threadMessage) {
        //set atttribute here
        reengMessage.setPacketId(backupMessageModel.getpId());
        reengMessage.setMessageType(ReengMessageConstant.MessageType.fromString("text"));
        reengMessage.setSender(backupMessageModel.getFrom());
        reengMessage.setReceiver(backupMessageModel.getTo());
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setChatMode(backupMessageModel.getcMode());
        reengMessage.setContent(backupMessageModel.getBody());
        reengMessage.setReplyDetail(backupMessageModel.getReply());
        reengMessage.setSenderName(backupMessageModel.getsName());
        reengMessage.setSenderAvatar(backupMessageModel.getsAvatar());
        reengMessage.setTime(backupMessageModel.getT());
        reengMessage.setTagContent(backupMessageModel.getTag());
        reengMessage.setStatus(backupMessageModel.getStatus());
        reengMessage.setReadState(backupMessageModel.getReadState());

        return reengMessage;
    }

    public static ThreadMessage convertToThreadMessage(ThreadMessage threadMessage, BackupThreadMessageModel backupThreadMessageModel) {
        threadMessage.setName(backupThreadMessageModel.getName());
        threadMessage.setState(backupThreadMessageModel.getState());
        threadMessage.setThreadType(backupThreadMessageModel.getType() - 100);
        threadMessage.setServerId(backupThreadMessageModel.getSvId());

        String phones = backupThreadMessageModel.getNumbers();
        if (!TextUtils.isEmpty(phones)) {
            ArrayList<String> phoneNumbers = new ArrayList<>();
            String numbers[] = phones.split(",");
            for (String num : numbers) {
                phoneNumbers.add(num);
            }
            threadMessage.setPhoneNumbers(phoneNumbers);
        }

        threadMessage.setNumOfUnreadMessage(backupThreadMessageModel.getUnread());
        threadMessage.setDraftMessage(backupThreadMessageModel.getDraft());
        threadMessage.setLastTimeSaveDraft(backupThreadMessageModel.gettDraft());
        threadMessage.setGroupAvatar(backupThreadMessageModel.getAvt());
        threadMessage.setTimeOfLast(backupThreadMessageModel.gettChange());
        String admins = backupThreadMessageModel.getAdmins();
        if (!TextUtils.isEmpty(admins)) {
            ArrayList<String> adminNumbers = new ArrayList<>();
            String adminNums[] = admins.split(",");
            for (String num : adminNums) {
                adminNumbers.add(num);
            }
            threadMessage.setAdminNumbers(adminNumbers);
        }

        threadMessage.setPinMessage(backupThreadMessageModel.getPin());
        threadMessage.setGroupClass(backupThreadMessageModel.getCls());

        return threadMessage;
    }

    public static void unzipToInternalStorage(File zipFile) {
        FileInputStream fin = null;
        ZipInputStream zin = null;
        try {
            fin = new FileInputStream(zipFile);
            zin = new ZipInputStream(fin);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null) {
                if (ze.isDirectory()) {
                    File f = new File(ApplicationController.self().getFilesDir(), ze.getName());
                    if (!f.isDirectory()) {
                        f.mkdirs();
                    }
                } else {
                    byte[] buffer = new byte[1024];
                    int read = 0;
                    FileOutputStream fout = new FileOutputStream(new File(ApplicationController.self().getFilesDir(), ze.getName()));
                    while ((read = zin.read(buffer)) != -1) {
                        fout.write(buffer, 0, read);
                    }

                    zin.closeEntry();
                    fout.close();
                }

            }
        } catch (Exception e) {
        } finally {
            try {
                if (zin != null)
                    zin.close();
            } catch (Exception e) {
                Log.e(TAG, "unzipToInternalStorage Exception", e);
            }
            try {
                if (fin != null)
                    fin.close();
            } catch (Exception e) {
                Log.e(TAG, "unzipToInternalStorage Exception", e);
            }
        }
    }
}

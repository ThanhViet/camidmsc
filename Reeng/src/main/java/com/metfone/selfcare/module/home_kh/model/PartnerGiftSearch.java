package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PartnerGiftSearch implements Serializable {
    @SerializedName("giftTypeName")
    String giftTypeName;
    @SerializedName("id")
    String id;
    @SerializedName("isShortCut")
    int isShortCut;
    @SerializedName("partner")
    String partner;
    @SerializedName("iconUrl")
    String iconUrl;
    @SerializedName("isType")
    int isType;
    @SerializedName("gifts")
    List<GiftItems> listGiftItems;

    public String getGiftTypeName() {
        return giftTypeName;
    }

    public void setGiftTypeName(String giftTypeName) {
        this.giftTypeName = giftTypeName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIsShortCut() {
        return isShortCut;
    }

    public void setIsShortCut(int isShortCut) {
        this.isShortCut = isShortCut;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public int getIsType() {
        return isType;
    }

    public void setIsType(int isType) {
        this.isType = isType;
    }

    public List<GiftItems> getListGiftItems() {
        return listGiftItems;
    }

    public void setListGiftItems(List<GiftItems> listGiftItems) {
        this.listGiftItems = listGiftItems;
    }
}
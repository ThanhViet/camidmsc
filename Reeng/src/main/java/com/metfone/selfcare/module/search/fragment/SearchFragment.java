package com.metfone.selfcare.module.search.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.home.HomeContactsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.home.HomeContactsHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.listeners.SimpleContactsHolderListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.SearchMovieResponse;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.keeng.network.restpaser.RestSearchSuggest;
import com.metfone.selfcare.module.newdetails.activity.NewsDetailActivity;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.search.activity.SearchAllActivity;
import com.metfone.selfcare.module.search.event.KeySearchChangeEvent;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.listener.SearchContactsListener;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.search.model.CreateThreadChat;
import com.metfone.selfcare.module.search.model.SearchHistory;
import com.metfone.selfcare.module.search.network.ApiCallback;
import com.metfone.selfcare.module.search.network.SearchApi;
import com.metfone.selfcare.module.search.task.SearchContactsTask;
import com.metfone.selfcare.module.search.utils.ConvertHelper;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.activitytiin.TiinDetailActivity;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public abstract class SearchFragment extends BaseFragment implements SearchAllListener.OnClickBoxThreadChat
        , SearchAllListener.OnClickBoxVideo, SearchAllListener.OnClickBoxNews, SearchAllListener.OnClickBoxMovies
        , SearchAllListener.OnClickBoxMusic, OnChannelChangedDataListener, SearchContactsListener, SearchAllListener.OnClickBoxTiin, SearchAllListener.OnAdapterClick, SearchAllListener.OnClickReward {
    protected Unbinder unbinder;
    protected int source;
    protected ApplicationController mApplication;
    protected ArrayList<Object> data;
    protected String keySearch = "";
    protected SearchAllActivity searchActivity;
    protected ApiCallback<ArrayList<Video>> apiCallbackVideo;
    protected ApiCallback<ArrayList<Channel>> apiCallbackChannel;
    protected ApiCallback<ArrayList<NewsModel>> apiCallbackNews;
    protected ApiCallback<ArrayList<Movie>> apiCallbackMovies;
    protected ApiCallback<ArrayList<Movie>> mApiCallbackMovies;
    protected ApiCallback<ArrayList<RestSearchSuggest.Group>> apiCallbackMusic;
    protected ApiCallback<ArrayList<SearchModel>> apiCallbackSong;
    protected ApiCallback<ArrayList<SearchModel>> apiCallbackMV;
    protected ApiCallback<ArrayList<TiinModel>> apiCallbackTiin;
    protected ApiCallback<ArrayList<SearchRewardResponse.Result.ItemReward>> apiCallbackReward;

    protected boolean isSearchingChat;
    protected boolean isSearchingVideo;
    protected boolean isSearchingChannel;
    protected boolean isSearchingNews;
    protected boolean isSearchingMovies;
    protected boolean isSearchingRewards;
    protected boolean isSearchingMusic;
    protected boolean isSearchingTiin;
    protected SearchContactsTask searchContactsTask;
    protected boolean isRefresh = true;
    protected boolean mustLoadData;
    protected long currentTimeSearch = 0L;
    protected int currentTabId;
    protected Comparator<String> comparatorNameUnmark;
    protected Comparator<String> comparatorKeySearchUnmark;
    protected Comparator<Object> comparatorContacts;
    protected Comparator<Object> comparatorThreadMessages;
    private HomeContactsAdapter recentAdapter;

    protected boolean hasReachedFilm = false;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.empty_progress)
    View progressEmpty;
    @BindView(R.id.empty_text)
    TextView tvEmpty;
    @BindView(R.id.tvEmptyTitle)
    TextView tvEmptyTitle;
    @BindView(R.id.llEmptyLayout)
    LinearLayout llEmptyLayout;
    @BindView(R.id.rvRecent)
    RecyclerView rvRecent;
    @BindView(R.id.llRecent)
    LinearLayout llRecent;
    @BindView(R.id.empty_retry_text1)
    TextView tvEmptyRetry1;
    @BindView(R.id.empty_retry_text2)
    TextView tvEmptyRetry2;
    @BindView(R.id.empty_retry_button)
    ImageView btnEmptyRetry;
    @BindView(R.id.empty_layout)
    View viewEmpty;
    @BindView(R.id.progress_loading)
    View progressLoading;
    @BindView(R.id.tvResultCount)
    TextView tvResultCount;

    protected boolean isLoading() {
        return isSearchingChat || isSearchingChannel || isSearchingVideo || isSearchingMovies
                || isSearchingMusic || isSearchingNews || isSearchingTiin || isSearchingRewards;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_search_all;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        searchActivity = (SearchAllActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mApplication = (ApplicationController) mActivity.getApplication();
        Bundle bundle = getArguments();
        if (bundle != null) {
            source = bundle.getInt(Constants.KEY_POSITION);
            currentTabId = bundle.getInt(Constants.KEY_TYPE);
        }
        initListener();
    }

    protected void initListener() {
        if (mApplication != null) mApplication.getListenerUtils().addListener(this);
        if (swipeRefreshLayout != null) swipeRefreshLayout.setEnabled(false);
        InputMethodUtils.hideKeyboardWhenTouch(recyclerView, searchActivity);
        InputMethodUtils.hideKeyboardWhenTouch(viewEmpty, searchActivity);
        if (btnEmptyRetry != null) btnEmptyRetry.setOnClickListener(v -> {
            isRefresh = true;
            doSearch();
        });
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) unbinder.unbind();
        cancelSearch();
        super.onDestroyView();
    }

    protected void showProgressLoading() {
        Log.i(TAG, "showProgressLoading");
        if (searchActivity != null && isAdded() && !searchActivity.isShowDataSuggest())
            searchActivity.showDataResult();
        //if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(true);
        if (viewEmpty != null) viewEmpty.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.VISIBLE);
    }

    protected void showLoading() {
        Log.i(TAG, "showLoading");
        if (searchActivity != null && isAdded() && !searchActivity.isShowDataSuggest())
            searchActivity.showDataResult();
        //if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(true);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.VISIBLE);
        if (llEmptyLayout != null) llEmptyLayout.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
    }

    protected void showLoadedEmpty() {
        Log.i(TAG, "showLoadedEmpty");
        if (searchActivity != null && isAdded() && !searchActivity.isShowDataSuggest())
            searchActivity.showDataResult();
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (llEmptyLayout != null) {
            llEmptyLayout.setVisibility(View.VISIBLE);
            tvEmptyTitle.setText(getString(R.string.could_not_found_search) + " \"" + keySearch + "\"");
            tvEmpty.setText(R.string.not_found_search_description);
//            switch (currentTabId) {
//                case SearchUtils.TAB_SEARCH_CHAT:
//                    icEmpty.setImageResource(R.drawable.ic_empty_contact);
//                    break;
//                case SearchUtils.TAB_SEARCH_CHANNEL_VIDEO:
//                    icEmpty.setImageResource(R.drawable.ic_empty_channel);
//                    break;
//                case SearchUtils.TAB_SEARCH_VIDEO:
//                case SearchUtils.TAB_SEARCH_MV:
//                    icEmpty.setImageResource(R.drawable.ic_empty_video);
//                    break;
//                case SearchUtils.TAB_SEARCH_MOVIES:
//                    icEmpty.setImageResource(R.drawable.ic_empty_film);
//                    break;
//                case SearchUtils.TAB_SEARCH_MUSIC:
//                    icEmpty.setImageResource(R.drawable.ic_empty_music);
//                    break;
//                case SearchUtils.TAB_SEARCH_NEWS:
//                case SearchUtils.TAB_SEARCH_TIIN:
//                    icEmpty.setImageResource(R.drawable.ic_empty_news);
//                    break;
//                default:
//                    if (searchActivity.getKeySearch().isEmpty()) {
//                        tvEmpty.setText(R.string.no_history_search_hint);
//                        tvEmptyTitle.setText(R.string.no_history_search);
//                    }
//                    icEmpty.setImageResource(R.drawable.ic_empty_history);
//                    break;
//            }
        }
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
    }

    protected void showLoadedSuccess(boolean isSuggest) {
        Log.i(TAG, "showLoadedSuccess");
        if (searchActivity != null && isAdded() && !searchActivity.isShowDataSuggest())
            searchActivity.showDataResult();
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        if (recyclerView != null) {
            recyclerView.setVisibility(View.VISIBLE);
            //if (isSuggest) recyclerView.smoothScrollToPosition(0);
        }
        if (viewEmpty != null) viewEmpty.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
    }

    protected void showLoadedError() {
        Log.i(TAG, "showLoadedError");
        if (searchActivity != null && isAdded() && !searchActivity.isShowDataSuggest())
            searchActivity.showDataResult();
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.VISIBLE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (llEmptyLayout != null) llEmptyLayout.setVisibility(View.GONE);
        if (NetworkHelper.isConnectInternet(mActivity)) {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.VISIBLE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        } else {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.VISIBLE);
        }
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
    }

    protected void searchChat() {
        if (searchContactsTask != null) {
            searchContactsTask.setListener(null);
            searchContactsTask.cancel(true);
            searchContactsTask = null;
        }
        if (mApplication == null/* || mApplication.getReengAccountBusiness().isAnonymousLogin()*/)
            return;
        searchContactsTask = new SearchContactsTask(mApplication);
        searchContactsTask.setListener(this);
        searchContactsTask.setComparatorContacts(comparatorContacts);
        searchContactsTask.setComparatorThreadMessages(comparatorThreadMessages);
        searchContactsTask.setComparatorKeySearchUnmark(comparatorKeySearchUnmark);
        searchContactsTask.setComparatorNameUnmark(comparatorNameUnmark);
        searchContactsTask.setCheckJoined(false);
        searchContactsTask.setHasCreateNewThread(true);
        searchContactsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, keySearch);
    }

    protected void searchChannelVideo(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabVideo()) {
            isSearchingChannel = true;
            lastRequest = getSearchApi().searchChannelVideo(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , page * SearchUtils.LIMIT_REQUEST_SEARCH, apiCallbackChannel, true);
        }
    }

    protected void searchVideo(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabVideo()) {
            isSearchingVideo = true;
            lastRequest = getSearchApi().searchVideo(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , page * SearchUtils.LIMIT_REQUEST_SEARCH, apiCallbackVideo, true);
        }
    }

    protected void searchNews(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabNews()) {
            isSearchingNews = true;
            lastRequest = getSearchApi().searchNews(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , page, apiCallbackNews, true);
        }
    }

    protected void searchMovies(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabMovie() && !hasReachedFilm) {
            isSearchingMovies = true;
            lastRequest = getSearchApi().searchMovies(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , page * SearchUtils.LIMIT_REQUEST_SEARCH, apiCallbackMovies, true);
        }
    }
    protected void searchFilm(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabMovie() && !hasReachedFilm) {
            isSearchingMovies = true;
            lastRequest = getSearchApi().searchMoviesCinema(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                , page * SearchUtils.LIMIT_REQUEST_SEARCH, mApiCallbackMovies, true);
        }
    }

    protected void searchRewards(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabRewards()) {
            isSearchingRewards = true;
            lastRequest = getSearchApi().searchRewards(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , page, apiCallbackReward, true);
        }
    }

    protected void searchMusic(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabMusic()) {
            isSearchingMusic = true;
            lastRequest = getSearchApi().searchSuggestMusic(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , "song", apiCallbackMusic, true);
            lastRequest = getSearchApi().searchSuggestMusic(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , "(song or video)", apiCallbackMusic, true);
        }
    }

    protected void searchSong(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabMusic()) {
            isSearchingMusic = true;
            lastRequest = getSearchApi().searchMusic(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , page * SearchUtils.LIMIT_REQUEST_SEARCH, "song", apiCallbackSong, true);
        }
    }

    protected void searchMV(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabMusic()) {
            isSearchingMusic = true;
            lastRequest = getSearchApi().searchMusic(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , page * SearchUtils.LIMIT_REQUEST_SEARCH, "video", apiCallbackMV, true);
        }
    }

    protected void searchTiin(Http lastRequest, int page) {
        if (lastRequest != null) lastRequest.cancel();
        if (mApplication != null && mApplication.getConfigBusiness().isEnableTabTiin()) {
            isSearchingTiin = true;
            lastRequest = getSearchApi().searchTiin(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH
                    , page, apiCallbackTiin, true);
        }
    }

    @Override
    public void onClickMenuMore(Object data) {
        if (searchActivity != null && mApplication != null) {
            searchActivity.hideSoftKeyboard();
            mApplication.getApplicationComponent().providesUtils().openRewardDetail(searchActivity, (SearchRewardResponse.Result.ItemReward) data);
            SearchHistory model = new SearchHistory();
            model.setData(keySearch);
            searchActivity.clickItemDetail(model);
        }
    }

    @Override
    public void onClickReward(SearchRewardResponse.Result.ItemReward item) {
        if (searchActivity != null && mApplication != null) {
            searchActivity.hideSoftKeyboard();
            mApplication.getApplicationComponent().providesUtils().openRewardDetail(searchActivity, item);
            SearchHistory model = new SearchHistory();
            model.setData(keySearch);
            searchActivity.clickItemDetail(model);
        }
    }

    @Override
    public void onClickRewardMore() {
        if (searchActivity != null) searchActivity.openTabRewards();
    }

    @Override
    public void onClickVideoItem(Video item) {
        if (searchActivity != null && mApplication != null) {
            searchActivity.hideSoftKeyboard();
            mApplication.getApplicationComponent().providesUtils().openVideoDetail(searchActivity, item);
            SearchHistory model = new SearchHistory();
            model.setData(keySearch);
            searchActivity.clickItemDetail(model);
        }
    }

    @Override
    public void onClickChannelItem(Channel item) {
        if (searchActivity != null && mApplication != null) {
            searchActivity.hideSoftKeyboard();
            mApplication.getApplicationComponent().providesUtils().openChannelInfo(searchActivity, item);
            SearchHistory model = new SearchHistory();
            model.setData(item);
            searchActivity.clickItemDetail(model);
        }
    }

    @Override
    public void onClickSubscribeChannelItem(Channel item) {
        if (mApplication != null) {
            mApplication.getApplicationComponent().providerChannelApi().callApiSubOrUnsubChannel(item.getId(), item.isFollow());
            mApplication.getListenerUtils().notifyChannelSubscriptionsData(item);
        }
    }

    @Override
    public void onClickVideoMore() {
        if (searchActivity != null) searchActivity.openTabVideo();
    }

    @Override
    public void onClickChannelMore() {
        if (searchActivity != null) searchActivity.openTabChannel();
    }

    @Override
    public void onClickNewsItem(NewsModel item) {
        if (searchActivity != null) {
            searchActivity.hideSoftKeyboard();
            Intent intent = new Intent(searchActivity, NewsDetailActivity.class);
            intent.putExtra("fromNetNews", true);
            intent.putExtra(CommonUtils.KEY_NEWS_ITEM_SELECT, item);
            searchActivity.startActivity(intent);
            SearchHistory model = new SearchHistory();
            model.setData(keySearch);
            searchActivity.clickItemDetail(model);
        }
    }

    @Override
    public void onClickNewsMore() {
        if (searchActivity != null) searchActivity.openTabNews();
    }

    @Override
    public void onClickMoviesItem(Movie item) {
        if (searchActivity != null && mApplication != null) {
            searchActivity.hideSoftKeyboard();
            mApplication.getApplicationComponent().providesUtils().openMovieDetail(searchActivity, item);
            SearchHistory model = new SearchHistory();
            model.setData(keySearch);
            searchActivity.clickItemDetail(model);
        }
    }

    @Override
    public void onClickMoviesMore() {
        if (searchActivity != null) searchActivity.openTabMovies();
    }

    @Override
    public void onClickMusicItem(SearchModel item) {
        if (searchActivity == null || searchActivity.isFinishing() || item == null) return;
        searchActivity.hideSoftKeyboard();
        switch (item.getType()) {
            case Constants.TYPE_SONG: {
                searchActivity.setMediaToPlaySong(ConvertHelper.convertToMedia(item, UrlConfigHelper.getInstance(searchActivity).getDomainImageSearch()));
            }
            break;
            case Constants.TYPE_ALBUM:
            case Constants.TYPE_ALBUM_VIDEO: {
                searchActivity.gotoAlbumDetail(ConvertHelper.convertToMedia(item, UrlConfigHelper.getInstance(searchActivity).getDomainImageSearch()));
            }
            break;
            case Constants.TYPE_VIDEO: {
                searchActivity.setMediaToPlayVideo(ConvertHelper.convertToMedia(item, UrlConfigHelper.getInstance(searchActivity).getDomainImageSearch()));
            }
            break;
            case Constants.TYPE_PLAYLIST:
            case Constants.TYPE_PLAYLIST_VIDEO: {
                searchActivity.gotoPlaylistDetail(ConvertHelper.convertToPlaylist(item));
            }
            break;
            case Constants.TYPE_SINGER: {
                searchActivity.gotoSingerDetail(ConvertHelper.convertToSinger(item, UrlConfigHelper.getInstance(searchActivity).getDomainImageSearch()));
            }
            break;
            case Constants.TYPE_MOVIE: {
                searchActivity.playMovies(ConvertHelper.convertToMovies(item));
            }
            break;
        }
        SearchHistory model = new SearchHistory();
        model.setData(keySearch);
        searchActivity.clickItemDetail(model);
    }

    @Override
    public void onClickMusicMore() {
        if (searchActivity != null) searchActivity.openTabMusic();
    }

    @Override
    public void onClickMVMore() {
        if (searchActivity != null) searchActivity.openTabMV();
    }

    @Override
    public void onClickThreadChatMore() {
        if (searchActivity != null) searchActivity.openTabChat();
    }

    @Override
    public void onClickCreateThreadMessage(CreateThreadChat item) {
        if (searchActivity != null && mApplication != null) {
            searchActivity.hideSoftKeyboard();
            String number = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(item.getKeySearchChat());
            if (Utilities.isEmpty(number)) number = item.getKeySearchChat();
            String region = mApplication.getReengAccountBusiness().getRegionCode();
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().getPhoneNumberProtocol(mApplication.getPhoneUtil(), number, region);
            if (phoneNumberProtocol == null) {
                searchActivity.showToast(searchActivity.getString(R.string.msg_not_phone_number), Toast.LENGTH_SHORT);
            } else {
                final String jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                        mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                if (Utilities.notEmpty(jidNumber) && jidNumber.equals(myNumber)) {
                    searchActivity.showToast(searchActivity.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
                } else if (!PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(),
                        phoneNumberProtocol)) {
                    searchActivity.showToast(searchActivity.getString(R.string.msg_not_phone_number), Toast.LENGTH_SHORT);
                } else {
                    ArrayList<String> listNumbers = new ArrayList<>();
                    listNumbers.add(jidNumber);
                    searchActivity.showLoadingDialog(null, searchActivity.getString(R.string.waiting));
                    ContactRequestHelper.getInstance(mApplication).getInfoContactsFromNumbers(listNumbers, new
                            ContactRequestHelper.onResponseInfoContactsListener() {
                                @Override
                                public void onResponse(ArrayList<PhoneNumber> responses) {
                                    if (searchActivity != null && mApplication != null) {
                                        searchActivity.hideLoadingDialog();
                                        ContactBusiness contactBusiness = mApplication.getContactBusiness();
                                        if (responses != null && !responses.isEmpty()) {// so dung mocha
                                            PhoneNumber phoneNumber = responses.get(0);
                                            //  cap nhat danh sach nonContact
                                            contactBusiness.insertOrUpdateNonContact(phoneNumber, true, true);
                                            NavigateActivityHelper.navigateToChatDetail(searchActivity, mApplication.getMessageBusiness().findExistingOrCreateNewThread(jidNumber));
                                        } else {
                                            PhoneNumber phoneNumber = new PhoneNumber();
                                            phoneNumber.setJidNumber(jidNumber);
                                            phoneNumber.setState(Constants.CONTACT.NONE);
                                            contactBusiness.insertOrUpdateNonContact(phoneNumber, true);
                                            ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(jidNumber);
                                            thread.setIsReeng(false);
                                            NavigateActivityHelper.navigateToChatDetail(searchActivity, thread);
                                        }
                                        new Handler().postDelayed(() -> {
                                            if (searchActivity != null && !searchActivity.isFinishing())
                                                searchActivity.finish();
                                        }, 150);
                                    }
                                }

                                @Override
                                public void onError(int errorCode) {
                                    if (searchActivity != null) {
                                        searchActivity.hideLoadingDialog();
                                        searchActivity.showToast(searchActivity.getString(R.string.e500_internal_server_error), Toast.LENGTH_SHORT);
                                    }
                                }
                            });
                }
            }
        }
    }

    @Override
    public void onClickThreadChatItem(Object item) {
        if (searchActivity != null && mApplication != null) {
            searchActivity.hideSoftKeyboard();
            if (item instanceof ThreadMessage) {
                NavigateActivityHelper.navigateToChatDetail(searchActivity, (ThreadMessage) item);
                if (((ThreadMessage) item).getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    SearchHistory model = new SearchHistory();
                    model.setData(item);
                    searchActivity.clickItemDetail(model);
                }
                searchActivity.finish();
            } else if (item instanceof PhoneNumber) {
                ThreadMessage threadTmp = mApplication.getMessageBusiness().findExistingOrCreateNewThread(((PhoneNumber) item).getJidNumber());
                if (threadTmp == null) {
                    NavigateActivityHelper.navigateToContactDetail(searchActivity, (PhoneNumber) item);
                } else {
                    NavigateActivityHelper.navigateToChatDetail(mActivity, threadTmp);
                }
                SearchHistory model = new SearchHistory();
                model.setData(item);
                searchActivity.clickItemDetail(model);
//                searchActivity.finish();
            } else if (item instanceof ContactProvisional) {
                Object obj = ((ContactProvisional) item).getContact();
                if (obj instanceof ThreadMessage) {
                    NavigateActivityHelper.navigateToChatDetail(searchActivity, (ThreadMessage) obj);
                    if (((ThreadMessage) obj).getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                        SearchHistory model = new SearchHistory();
                        model.setData(obj);
                        searchActivity.clickItemDetail(model);
                    }
//                    searchActivity.finish();
                } else if (obj instanceof PhoneNumber) {
                    ThreadMessage threadTmp = mApplication.getMessageBusiness().findExistingOrCreateNewThread(((PhoneNumber) obj).getJidNumber());
                    if (threadTmp == null) {
                        NavigateActivityHelper.navigateToContactDetail(searchActivity, (PhoneNumber) obj);
                    } else {
                        NavigateActivityHelper.navigateToChatDetail(mActivity, threadTmp);
                    }
                    SearchHistory model = new SearchHistory();
                    model.setData(obj);
                    searchActivity.clickItemDetail(model);
//                    searchActivity.finish();
                }
            }
        }
    }

    @Override
    public void onClickTiinItem(TiinModel item) {
        if (searchActivity != null) {
            Intent intent = new Intent(searchActivity, TiinDetailActivity.class);
            intent.putExtra(ConstantTiin.FULL_CONTENT_TIIN, false);
            intent.putExtra(ConstantTiin.INTENT_MODULE, item);
            searchActivity.startActivity(intent);
            SearchHistory model = new SearchHistory();
            model.setData(keySearch);
            searchActivity.clickItemDetail(model);
        }
    }

    @Override
    public void onClickTiinMore() {
        if (searchActivity != null) searchActivity.openTabTiin();
    }

    @Override
    public void onClickLogin() {
        if (searchActivity != null) {
            searchActivity.loginFromAnonymous();
        }
    }

    @Override
    public void onPrepareSearchContacts() {
        isSearchingChat = true;
    }

    public abstract void cancelSearch();

    public abstract void doSearch();

    @Override
    public void onChannelCreate(Channel channel) {

    }

    @Override
    public void onChannelUpdate(Channel channel) {

    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mustLoadData && Utilities.notEmpty(keySearch)) {
            isRefresh = true;
            doSearch();
        }
    }

    protected abstract void resetData();

    public void updateKeySearch(KeySearchChangeEvent event) {
        Log.d(TAG, "updateKeySearch: " + event);
        if (currentTimeSearch < event.getCurrentTime()) {
            currentTimeSearch = event.getCurrentTime();
            keySearch = event.getKeySearch();
            cancelSearch();
            isRefresh = true;
            //SearchSubmitFragment tìm kiếm riêng lẻ nên reset lại title của các tab khác
            if (this instanceof SearchSubmitFragment) {
                resetTabBarTitle();
            }
            if (Utilities.notEmpty(keySearch)) {
                mustLoadData = !getUserVisibleHint();
                if (getUserVisibleHint()) {
                    doSearch();
                }
            } else {
                resetData();
            }
        }
    }

    protected void initComparatorSearchChat() {
        comparatorNameUnmark = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.compareToIgnoreCase(s1);
            }
        };
        comparatorKeySearchUnmark = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.compareToIgnoreCase(s1);
            }
        };
        comparatorContacts = new Comparator<Object>() {
            @Override
            public int compare(Object obj1, Object obj2) {
                String name1 = "";
                String name2 = "";
                if (obj1 instanceof ContactProvisional) {
                    name1 = ((ContactProvisional) obj1).getName();
                    if (name1 == null) name1 = "";
                    else
                        name1 = name1.trim().toLowerCase(Locale.US);
                }
                if (obj2 instanceof ContactProvisional) {
                    name2 = ((ContactProvisional) obj2).getName();
                    if (name2 == null) name2 = "";
                    else
                        name2 = name2.trim().toLowerCase(Locale.US);
                }
                return name1.compareToIgnoreCase(name2);
            }

        };
        comparatorThreadMessages = new Comparator<Object>() {
            @Override
            public int compare(Object obj1, Object obj2) {
                Long time1 = 0L;
                Long time2 = 0L;
                try {
                    if (obj1 instanceof ContactProvisional) {
                        if (((ContactProvisional) obj1).getContact() instanceof ThreadMessage) {
                            time1 = ((ThreadMessage) ((ContactProvisional) obj1).getContact()).getLastChangeThread();
                        }
                    }
                    if (obj2 instanceof ContactProvisional) {
                        if (((ContactProvisional) obj2).getContact() instanceof ThreadMessage) {
                            time2 = ((ThreadMessage) ((ContactProvisional) obj2).getContact()).getLastChangeThread();
                        }
                    }
                    return time2.compareTo(time1);
                } catch (Exception e) {
                    return 0;
                }
            }
        };
    }

    protected void updateTabBarTitle(int resultSize, int currentTabId) {
        if (searchActivity != null && isAdded()) {
            String totalResult = resultSize > SearchUtils.LIMIT_REQUEST_SEARCH ?
                    getString(R.string.search_max_result) :
                    String.valueOf(resultSize);
            if (tvResultCount != null) {
                if (resultSize > 0 && this instanceof SearchSubmitFragment) {
                    tvResultCount.setVisibility(View.VISIBLE);
                    tvResultCount.setText(Html.fromHtml(getString((resultSize == 1) ? R.string.search_tab_result : R.string.search_tab_results, totalResult)));
                } else if (this instanceof SearchSuggestFragment) {
                    searchActivity.totalAllResult += resultSize;
                    tvResultCount.setVisibility(View.INVISIBLE);
                    tvResultCount.setText(getString(R.string.search_tab_results_new, searchActivity.totalAllResult, keySearch));
                }
            }
            searchActivity.updateTabBarTitle(currentTabId, totalResult);
        }
    }

    protected void resetTabBarTitle() {
        if (searchActivity != null && isAdded()) {
            searchActivity.resetTabBarTitle();
        }
    }

    public void initRecentList() {
        initAdapter();
        initData();
    }

    @SuppressLint("CheckResult")
    private void initData() {
        Observable.fromCallable(() -> {
            ContactBusiness contactBusiness = mApplication.getContactBusiness();
            return contactBusiness.getRecentPhoneNumber();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((listRecent) -> {
                    recentAdapter.setData(new ArrayList<>(listRecent));
                    recentAdapter.notifyDataSetChanged();
                }, throwable -> {
                });
    }

    private void initAdapter() {
        recentAdapter = new HomeContactsAdapter(mApplication, new ArrayList<>());
        recentAdapter.setClickListener(new SimpleContactsHolderListener() {
            @Override
            public void onAudioCall(Object entry) {
                super.onAudioCall(entry);
                HomeContactsHelper.getInstance(mApplication).handleCallContactsClick(searchActivity, entry);
            }

            @Override
            public void onVideoCall(Object entry) {
                super.onVideoCall(entry);
                mApplication.getCallBusiness().checkAndStartVideoCall(searchActivity, (PhoneNumber) entry, false);
            }

            @Override
            public void onActionLabel(Object entry) {
                super.onActionLabel(entry);
                if (entry instanceof PhoneNumber) {
                    PhoneNumber phone = (PhoneNumber) entry;
                    InviteFriendHelper.getInstance().showInviteFriendPopup(mApplication, searchActivity,
                            phone.getName(), phone.getJidNumber(), false);
                }
            }

            @Override
            public void onAvatarClick(Object entry) {
                super.onAvatarClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(searchActivity, entry);
            }

            @Override
            public void onItemClick(Object entry) {
                super.onItemClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(searchActivity, entry);
            }
        });
        rvRecent.setLayoutManager(new LinearLayoutManager(searchActivity, LinearLayoutManager.VERTICAL, false));
        rvRecent.setItemAnimator(new DefaultItemAnimator());
        rvRecent.setAdapter(recentAdapter);
    }

    public void updateRecentListStatus(boolean needShowRecentList) {
        if (llRecent != null && recentAdapter != null) {
            llRecent.setVisibility(recentAdapter.getItemCount() > 0 && needShowRecentList ? View.VISIBLE : View.GONE);
        }
    }

    private SearchApi mSearchApi;

    public synchronized SearchApi getSearchApi() {
        if (mSearchApi == null) {
            synchronized (SearchApi.class) {
                mSearchApi = new SearchApi();
            }
        }
        return mSearchApi;
    }
}

package com.metfone.selfcare.module.movie.holder;

import android.app.Activity;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.esport.util.Utilities;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.movie.adapter.CustomTopicAdapter;
import com.metfone.selfcare.module.movie.listener.LoadmoreListener;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.movienew.model.CustomTopic;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CustomTopicHolder extends BaseAdapter.ViewHolder{

    @BindView(R.id.recycler_view)
    RecyclerView rvCustomTopic;
    private Activity activity;
    private TabMovieListener.OnAdapterClick listener;
    private LoadmoreListener.ILoadmoreAtPositionListener mLoadmoreListener;
    private LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv mLoadmoreAtPositionRclvInsideRclv;
    private int mAdapterPosition;
    private int mLoadmorePosition;
    private CustomTopicAdapter mCustomTopicAdapter;

    public CustomTopicHolder(View view, Activity activity, TabMovieListener.OnAdapterClick listener,
                             LoadmoreListener.ILoadmoreAtPositionListener loadmoreAtPositionListener,
                             LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv loadmoreAtPositionRclvInsideRclv) {
        super(view);
        this.activity = activity;
        this.listener = listener;
        this.mLoadmoreListener = loadmoreAtPositionListener;
        this.mLoadmoreAtPositionRclvInsideRclv = loadmoreAtPositionRclvInsideRclv;
    }

    @Override
    public void bindData(Object item, int position) {
        mAdapterPosition = getAdapterPosition();
        MoviePagerModel moviePagerModel = (MoviePagerModel) item;
        List<Object> homeDatas = moviePagerModel.getList();
        if (Utilities.notEmpty(homeDatas)) {
            List<CustomTopic> customTopicList = (List<CustomTopic>) (Object) ((MoviePagerModel) homeDatas.get(0)).getList();
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, RecyclerView.VERTICAL, false);
            rvCustomTopic.setLayoutManager(linearLayoutManager);
            mCustomTopicAdapter = new CustomTopicAdapter(getListCustomTopicContainFilm(customTopicList), ((MoviePagerModel) homeDatas.get(0)).getTitle(),
                    activity, listener, new LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv() {
                @Override
                public void loadmoreAtPositionOfRclvInsideRclv(int position, RecyclerView.Adapter adapter) {
                    mLoadmorePosition = position;
                    mLoadmoreAtPositionRclvInsideRclv.loadmoreAtPositionOfRclvInsideRclv(mAdapterPosition, adapter);
                }
            });
            rvCustomTopic.setAdapter(mCustomTopicAdapter);
        }
    }

    private List<CustomTopic> getListCustomTopicContainFilm(List<CustomTopic> topicList) {
        List<CustomTopic> list = new ArrayList<>();
        for (CustomTopic customTopic : topicList) {
            if (customTopic.getListCustomFilm() != null && customTopic.getListCustomFilm().size() > 0) {
                list.add(customTopic);
            }
        }
        return list;
    }
}

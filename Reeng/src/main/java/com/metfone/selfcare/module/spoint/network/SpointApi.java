package com.metfone.selfcare.module.spoint.network;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.HttpHelper;

public class SpointApi extends BaseApi {
    private static final String GET_SPOINT = "/ReengBackendBiz/accumulate/v6/list/accumulate";
    private static final String HISTORY_SPOINT = "/ReengBackendBiz/accumulate/history";
    private static final String CHANGE_SPOINT = "/ReengBackendBiz/accumulate/spointplus/convert";
    private static final String REFRESH_CAPTCHA = "/ReengBackendBiz/captcha/gen";
    private static final String RENT_MOVIE = "/ReengBackendBiz/accumulate/spointplus/gift/change";

    private String domain;

    public SpointApi(ApplicationController app) {
        super(app);
        domain = getDomainFile();
    }

    @Override
    protected Http.Builder get(String baseUrl, String url) {
        Http.Builder builder = super.get(baseUrl, url);
//        String msisdn = getReengAccountBusiness().getJidNumber();
//        if (msisdn != null)
//            builder.putParameter("msisdn", msisdn);
//        if (getReengAccountBusiness().isAnonymousLogin()) {
//            builder.putParameter("loginStatus", "0");
//        } else {
//            builder.putParameter("loginStatus", "1");
//        }
        builder.setTimeOut(60);
        return builder;
    }

    @Override
    protected Http.Builder post(String baseUrl, String url) {
        return super.post(baseUrl, url);
    }

    public void getSpoint(HttpCallBack httpCallBack, String pointType) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //todo token, timestamp luon co
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(getReengAccountBusiness().getCurrentLanguage())
                .append(myAccount.getToken())
                .append(timeStamp);
        get(domain, GET_SPOINT)
                .putParameter(Constants.HTTP.REST_MSISDN, myAccount.getJidNumber())
                .putParameter("pointType", pointType)
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage())
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .withCallBack(httpCallBack)
                .execute();
    }

    public void historySpoint(HttpCallBack httpCallBack, int pointType, int transType) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //todo token, timestamp luon co
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(getReengAccountBusiness().getCurrentLanguage())
                .append(myAccount.getToken())
                .append(timeStamp);
        get(domain, HISTORY_SPOINT)
                .withCallBack(httpCallBack)
                .putParameter(Constants.HTTP.REST_MSISDN, myAccount.getJidNumber())
                .putParameter("pointType", String.valueOf(pointType))
                .putParameter("transType", String.valueOf(transType))
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage())
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .execute();
    }

    public void changeSpointToSpointPlus(HttpCallBack httpCallBack, int spoint, long id, String sid, String captcha){
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //todo token, timestamp luon co
        sb.append(myAccount.getJidNumber())
                .append(spoint)
                .append("Android")
                .append(Config.REVISION)
                .append(myAccount.getToken())
                .append(timeStamp);
        post(domain,CHANGE_SPOINT)
                .withCallBack(httpCallBack)
                .putParameter(Constants.HTTP.REST_MSISDN, myAccount.getJidNumber())
                .putParameter("spoint", String.valueOf(spoint))
                .putParameter("clientType", "Android")
                .putParameter("id", String.valueOf(id))
                .putParameter("sid", sid)
                .putParameter("captcha", captcha)
                .putParameter("revision", Config.REVISION)
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .execute();
    }

    public void refreshCaptcha(HttpCallBack httpCallBack, String sid){
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //todo token, timestamp luon co
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(myAccount.getToken())
                .append(timeStamp);
        post(domain,REFRESH_CAPTCHA)
                .withCallBack(httpCallBack)
                .putParameter("isReferesh","1")
                .putParameter("sid", sid)
                .putParameter(Constants.HTTP.REST_MSISDN, myAccount.getJidNumber())
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .execute();
    }

    public void rentMovie(HttpCallBack httpCallBack, long id){
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //todo token, timestamp luon co
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(myAccount.getToken())
                .append(timeStamp);
        post(domain,RENT_MOVIE)
                .withCallBack(httpCallBack)
                .putParameter(Constants.HTTP.REST_MSISDN, myAccount.getJidNumber())
                .putParameter("id", String.valueOf(id))
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .execute();
    }
}

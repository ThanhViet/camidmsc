/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.metfone.selfcare.module.newdetails.fragment;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.netnews.activity.NetNewsActivity;
import com.metfone.selfcare.module.newdetails.activity.NewsDetailActivity;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;
import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment implements MvpView, OnInternetChangedListener {
    public final String TAG = getClass().getSimpleName();

    protected boolean isVisibleToUser;
    protected boolean isDataInitiated;
    protected boolean isViewInitiated;

    public BaseSlidingFragmentActivity activity;
    private Unbinder mUnBinder;
    public SharedPref pref;
    public final static int MAX_RETRY = 3;
    public int countRetry = 0;

    @Nullable
    @BindView(R.id.refresh)
    protected SwipeRefreshLayout layout_refresh;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        Log.e(TAG, "onCreate --------------");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isViewInitiated = false;
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isViewInitiated = true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseSlidingFragmentActivity) {
            activity = (BaseSlidingFragmentActivity) context;
        }
        pref = new SharedPref(context);
        Log.e(TAG, "onAttach --------------");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume --------------");
    }

    @Override
    public void showLoading() {
        showRefresh();
    }

    @Override
    public void hideLoading() {
        hideRefresh();
    }

    protected void showRefresh() {
        if (layout_refresh != null) {
            layout_refresh.setRefreshing(true);
        }
    }

    protected void hideRefresh() {
        if (layout_refresh != null) {
            layout_refresh.setRefreshing(false);
            layout_refresh.destroyDrawingCache();
            layout_refresh.clearAnimation();
        }
    }

    @Override
    public void onError(@StringRes int resId) {
        onError(getString(resId));
    }

    @Override
    public void onInternetChanged() {

    }

    @Override
    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar("Some Error Occurred!");
        }
    }

    @Override
    public boolean isNetworkConnected() {
        return CommonUtils.isConnected(activity);
    }

    private void showSnackBar(String message) {
        Log.i(TAG, message + "");
    }

    @Override
    public void onDetach() {
        activity = null;
        Log.e(TAG, "onDetach --------------");
        super.onDetach();
    }

    @Override
    public void hideKeyboard() {
        if (activity != null) {
            activity.hideKeyboard();
        }
    }

    @Override
    public void openActivityOnTokenExpire() {
//        if (mNewsDetailActivity != null) {
//            mNewsDetailActivity.openActivityOnTokenExpire();
//        }
    }

    public BaseSlidingFragmentActivity getBaseActivity() {
        return activity;
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    protected abstract void setUp(View view);

    @Override
    public void onDestroy() {
        if (mUnBinder != null) {
            try {
                mUnBinder.unbind();
            } catch (Exception e) {
            }
        }
        Log.e(TAG, "onDestroy --------------");
        isViewInitiated = false;
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        if (mUnBinder != null) {
            try {
                mUnBinder.unbind();
            } catch (Exception e) {
            }
        }
        mUnBinder = null;
        Log.e(TAG, "onDestroyView --------------");
        super.onDestroyView();
    }

    //Xu ly nhanh
    public void readNews(NewsModel model) {
        try {
            if (getNewsDetailActivity() != null) {
                getNewsDetailActivity().processItemClick(model, false);
            } else if (getNetNewsActivity() != null) {
                getNetNewsActivity().processItemClick(model);
            } else if (getBaseActivity() != null && (getBaseActivity() instanceof HomeActivity || getBaseActivity() instanceof ModuleActivity)) {
                Intent intent = new Intent(getBaseActivity(), NewsDetailActivity.class);
                intent.putExtra(CommonUtils.KEY_NEWS_ITEM_SELECT, model);
                getBaseActivity().startActivity(intent);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart --------------");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop --------------");
    }

    public void paddingView(boolean flag) {
    }

    public NewsDetailActivity getNewsDetailActivity() {
        if (activity != null && activity instanceof NewsDetailActivity) {
            return (NewsDetailActivity) activity;
        }
        return null;
    }

    public NetNewsActivity getNetNewsActivity() {
        if (activity != null && activity instanceof NetNewsActivity) {
            return (NetNewsActivity) activity;
        }
        return null;
    }

    boolean isRunningAnimationMoveTop;

    public void showMoveTop(final View imvMoveTop) {
        if (imvMoveTop == null) return;
        if (imvMoveTop.getVisibility() == View.VISIBLE) return;
        if (isRunningAnimationMoveTop) return;
        isRunningAnimationMoveTop = true;
        imvMoveTop.setY(ViewUtils.getScreenHeight(getBaseActivity()));
        imvMoveTop.setVisibility(View.VISIBLE);
        imvMoveTop.animate().translationY(0)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        if (imvMoveTop != null) {
                            imvMoveTop.animate().setListener(null);
                        }
                        isRunningAnimationMoveTop = false;
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                }).setDuration(500).start();
    }

    public void hideMoveTop(final View imvMoveTop) {
        if (imvMoveTop == null) return;
        if (imvMoveTop.getVisibility() == View.GONE) return;
        if (isRunningAnimationMoveTop) return;
        isRunningAnimationMoveTop = true;
        imvMoveTop.animate().translationY(imvMoveTop.getHeight())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        if (imvMoveTop != null) {
                            imvMoveTop.setVisibility(View.GONE);
                            imvMoveTop.animate().setListener(null);
                        }
                        isRunningAnimationMoveTop = false;
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                })
                .setDuration(300).start();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
    }

    public boolean canLazyLoad() {
        return isVisibleToUser && isViewInitiated && !isDataInitiated;
    }

    public void clickItemOptionNew(Object object, int menuId) {
        if (getBaseActivity() != null && !getBaseActivity().isFinishing() && object != null) {
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        getBaseActivity().showDialogLogin();
                    } else {
                        ShareUtils.openShareMenu(getBaseActivity(), object);
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        getBaseActivity().showDialogLogin();
                    } else {
                        FeedModelOnMedia feed = null;
                        if (object instanceof NewsModel) {
                            feed = FeedModelOnMedia.convertNewsToFeedModelOnMedia((NewsModel) object);
                        }
                        if (feed != null) {
                            if (object instanceof NewsModel) {
                                if (((NewsModel) object).isLike()) {
                                    new WSOnMedia(ApplicationController.self()).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                            , FeedModelOnMedia.ActionLogApp.UNLIKE, "", feed.getBase64RowId(), ""
                                            , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                                @Override
                                                public void onError(String s) {

                                                }

                                                @Override
                                                public void onComplete() {

                                                }

                                                @Override
                                                public void onSuccess(String msg, String result) throws JSONException {
                                                    if (getBaseActivity() != null)
                                                        getBaseActivity().showToast(R.string.del_favorite);
                                                    ((NewsModel) object).setLike(false);
                                                }
                                            });
                                } else {
                                    new WSOnMedia(ApplicationController.self()).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                            , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                            , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                                @Override
                                                public void onError(String s) {

                                                }

                                                @Override
                                                public void onComplete() {

                                                }

                                                @Override
                                                public void onSuccess(String msg, String result) throws JSONException {
                                                    if (getBaseActivity() != null)
                                                        getBaseActivity().showToast(R.string.add_favorite_success);
                                                    ((NewsModel) object).setLike(true);
                                                }
                                            });
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }
}

package com.metfone.selfcare.module.netnews.HomeNews.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

public class HomeNewCategoryAdapterV5 extends RecyclerView.Adapter<BaseViewHolder> {
   private List<CategoryModel> dataCategory;
   private Context mContext;
    AbsInterface.OnHomeNewsItemListener listener;

    public HomeNewCategoryAdapterV5(List<CategoryModel> model, Context mContext, AbsInterface.OnHomeNewsItemListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.dataCategory = model;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  view = LayoutInflater.from(mContext).inflate(R.layout.holder_content_new, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final CategoryModel model = dataCategory.get(position);
        if(holder.getView(R.id.tv_content) != null){
            holder.setText(R.id.tv_content,model.getName());
            holder.setOnClickListener(R.id.tv_content, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemCategoryHeaderClick(HomeNewsAdapterV5.TYPE_CATEGORY,model.getId(),model.getName());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataCategory == null ? 0 : dataCategory.size();
    }

}

package com.metfone.selfcare.module.metfoneplus.model;

public class CheckTutor {
    Boolean isTutor;

    public Boolean getTutor() {
        return isTutor;
    }

    public void setTutor(Boolean tutor) {
        isTutor = tutor;
    }
}

package com.metfone.selfcare.module.metfoneplus.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.util.RetrofitInstance;

import retrofit2.Response;

public class MPCombinePhoneDialog extends MPHeroDialog {
    public static final String TAG = "MPCombinePhoneDialog";
    private static final String ARG_PHONE_NUMBER = "phoneNumber";
    private static final String ARG_OTP = "otp";
    private static final String ARG_USER_SERVICE_ID = "userServiceId";
    private static final int TIME_DELAY_AUTO_NEXT_SCREEN = 2000;

    private int mUserServiceId;

    private OnCombinePhoneListener mOnCombinePhoneListener;
    private Handler mHandler = new Handler();
    private CamIdUserBusiness mCamIdUserBusiness;

    public static MPCombinePhoneDialog newInstance(String phoneNumber, String otp, int userServiceId) {
        MPCombinePhoneDialog combinePhoneDialog = new MPCombinePhoneDialog();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_PHONE_NUMBER, phoneNumber);
        bundle.putString(ARG_OTP, otp);
        bundle.putInt(ARG_USER_SERVICE_ID, userServiceId);
        combinePhoneDialog.setArguments(bundle);
        return combinePhoneDialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        try {
            mOnCombinePhoneListener = (OnCombinePhoneListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnCombinePhoneListener");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lavAnimations.setImageAssetsFolder("lottie/img_dialog_man_thinking.json");
        lavAnimations.playAnimation();
        lavAnimations.setVisibility(View.VISIBLE);
        mTitle.setText(R.string.m_p_dialog_combine_phone_title);
        mContent.setText(R.string.m_p_dialog_combine_phone_content);

        setTopButton(R.string.m_p_dialog_combine_phone_top_btn, R.drawable.bg_red_button_corner_6, buttonTop -> {
            updatePhoneToPhoneService();
        });

        setBottomButton(R.string.m_p_dialog_combine_phone_bottom_btn, R.drawable.bg_transparent_button_corner_6, buttonBottom -> {
            mOnCombinePhoneListener.onBack();
            dismissAllowingStateLoss();
        });
    }

    private void updatePhoneToPhoneService() {
        if (getArguments() != null) {
            mUserServiceId = getArguments().getInt(ARG_USER_SERVICE_ID, -1);

            RetrofitInstance retrofitInstance = new RetrofitInstance();
            retrofitInstance.updateMetfone(null, null, mUserServiceId, new ApiCallback<AddServiceResponse>() {
                @Override
                public void onResponse(Response<AddServiceResponse> response) {
                    if (response.body() != null && response.body().getData() != null) {
                        mCamIdUserBusiness.setPhoneService(response.body().getData().getServices());

                        updateUiDialog();

                        mHandler.postDelayed(() -> {
                            mOnCombinePhoneListener.onUpdateMetfoneSuccess();
                            dismissAllowingStateLoss();
                        }, TIME_DELAY_AUTO_NEXT_SCREEN);
                    }
                }

                @Override
                public void onError(Throwable error) {
                    Log.e(TAG, "onError: " + error);
                }
            });
        }
    }

    private void updateUiDialog() {
        lavAnimations.setImageAssetsFolder("lottie/img_dialog_women_success.json");
        lavAnimations.playAnimation();
        lavAnimations.setVisibility(View.VISIBLE);
        mTitle.setText(R.string.m_p_dialog_combine_phone_title_success);
        mContent.setText(R.string.m_p_dialog_combine_phone_content_success);
        setButtonTopVisibility(View.GONE);
        setButtonBottomVisibility(View.GONE);
    }

    public interface OnCombinePhoneListener {
        void onUpdateMetfoneSuccess();
        void onBack();
    }
}

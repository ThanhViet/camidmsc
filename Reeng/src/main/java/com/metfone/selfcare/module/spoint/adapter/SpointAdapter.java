package com.metfone.selfcare.module.spoint.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.spoint.SpointListener;
import com.metfone.selfcare.module.spoint.holder.ItemSpointHolder;
import com.metfone.selfcare.module.spoint.holder.TitleSpointHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SpointAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int TYPE_TEXT = 1;
    public static final int TYPE_SPOINT = 2;
    public static final int TYPE_LINE = 3;
    public static final int TYPE_EMPTY = 0;
    private Activity activity;
    private LayoutInflater layoutInflater;
    private List<AccumulatePointItem> data;
    private SpointListener.OnSpointListener listener;

    public SpointAdapter(Activity activity, List<AccumulatePointItem> data, SpointListener.OnSpointListener listener) {
        this.activity = activity;
        this.data = data;
        this.listener = listener;
        layoutInflater = LayoutInflater.from(this.activity);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_TEXT:
                return new TitleSpointHolder(layoutInflater.inflate(R.layout.holder_text_spoint, parent, false), activity);
            case TYPE_SPOINT:
                return new ItemSpointHolder(layoutInflater.inflate(R.layout.holder_item_spoint, parent, false), activity, listener);
            case TYPE_LINE:
                return new BaseViewHolder(layoutInflater.inflate(R.layout.item_line,parent,false));
            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        if (holder instanceof TitleSpointHolder) {
            ((TitleSpointHolder) holder).bindData(data.get(position));
        } else if (holder instanceof ItemSpointHolder) {
            ((ItemSpointHolder) holder).bindData(data.get(position));
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (data != null && data.size() > 0) {
            if (data.get(position).getTypeView() == 1) {
                return TYPE_TEXT;
            } else if (data.get(position).getTypeView() == 2) {
                return TYPE_LINE;
            } else {
                return TYPE_SPOINT;
            }
        } else {
            return TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        } else {
            return data.size();
        }
    }

    public static class EmptyHolder extends BaseViewHolder {

        public EmptyHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.holder_empty, parent, false));
        }
    }

}

package com.metfone.selfcare.module.games.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabGame.GameBaseResponseData;
import com.metfone.selfcare.model.tabGame.GameByNameRequest;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.module.games.activity.PlayGameActivity;
import com.metfone.selfcare.module.games.api.GameApi;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

import java.util.ArrayList;

public class GameTrendingAdapter extends RecyclerView.Adapter<GameTrendingAdapter.GameTrendingHolder> implements OnItemClick, GameApi.ListenerGameApi {
    private final ArrayList<GameModel> listGameTrending;
    private final Context context;

    private OnItemClick callBack;

    public GameTrendingAdapter(ArrayList<GameModel> listGameTrending, Context context) {
        this.listGameTrending = listGameTrending;
        this.context = context;
    }

    public void setOnItemClick(OnItemClick event) {
        callBack = event;
    }

    @NonNull
    @Override
    public GameTrendingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_game_new, parent, false);
        return new GameTrendingHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GameTrendingHolder holder, int position) {
        GameModel gameData = listGameTrending.get(position);
        Glide.with(context)
                .asBitmap().error(R.drawable.df_image_home_poster)
                .load(gameData.getIconURL())
                .fitCenter()
                .into(holder.ivGameIcon);
        holder.tvGameName.setText(gameData.getName());
    }

    @Override
    public int getItemCount() {
        return listGameTrending.size();
    }

    @Override
    public void onItemClick(String data) {
        openGame(data);
    }

    private void openGame(String name) {
        GameApi api = GameApi.getInstance();
        api.listenerGameApi = this;
        api.callApi(GameApi.GET_GAMES, new GameByNameRequest(name));
    }

    @Override
    public void onPreRequest() {

    }

    @Override
    public void onSuccess(Object responseData) {
        if (responseData instanceof GameBaseResponseData) {
            GameBaseResponseData data = (GameBaseResponseData) responseData;
            Intent intent = new Intent(context, PlayGameActivity.class);
            intent.putExtra("OPEN_GAME", data.getItems()[0].getGames()[0]);
            context.startActivity(intent);
        }
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onFailure(String exp) {

    }

    public class GameTrendingHolder extends RecyclerView.ViewHolder {
        private final RoundedImageView ivGameIcon;
        private final TextView tvGameName;

        public GameTrendingHolder(@NonNull View itemView) {
            super(itemView);
            ivGameIcon = itemView.findViewById(R.id.iv_cover);
            tvGameName = itemView.findViewById(R.id.tvTitleGame);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callBack != null) {
                        callBack.onItemClick(tvGameName.getText() + "");
                    }
                }
            });
        }
    }
}

package com.metfone.selfcare.module.home_kh.fragment.rewardsdetail;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountPointRankResponse;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetPartnerGiftDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemGiftAndReturnExchangeCodeResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.RewardBaseFragment;
import com.metfone.selfcare.module.home_kh.fragment.confirm.DialogConfirmFragmentKH;
import com.metfone.selfcare.module.home_kh.fragment.confirm.DialogConfirmQrCodeKH;
import com.metfone.selfcare.module.home_kh.fragment.confirm.DialogConfirmRedeemKH;
import com.metfone.selfcare.module.home_kh.fragment.confirm.DialogGetLinkRedeemKH;
import com.metfone.selfcare.module.home_kh.fragment.reward_map.RewardsMapKHFragment;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.Exchange;
import com.metfone.selfcare.module.home_kh.model.GiftDetail;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.home_kh.model.KHAccountPoint;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.model.RedeemItem;
import com.metfone.selfcare.module.home_kh.model.StoreListDetail;
import com.metfone.selfcare.module.home_kh.notification.detail.RedirectNotificationActivity;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.search.activity.RewardDetailActivity;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;

public class RewardsDetailKHFragment extends RewardBaseFragment implements RewardsDetailAdapter.IGiftsItemClick {
    private static final String DATA = "DATA";
    private static final String FROM = "FROM";
    private static final String SHOW_SPACE = "show_space";
    private KhHomeClient homeKhClient;

    private Unbinder unbinder;
    private GiftItems giftItems;
    private RedeemItem redeemItem;
    private SearchRewardResponse.Result.ItemReward rewardItem;

    @BindView(R.id.container_detail)
    FrameLayout container_detail;

    @BindView(R.id.icon)
    AppCompatImageView icon;

    @BindView(R.id.backdrop)
    AppCompatImageView backdrop;

    @BindView(R.id.tv_name)
    AppCompatTextView tvName;
    View space_bottom;

    @BindView(R.id.btn_confirm)
    AppCompatButton btnConfirm;

    @BindView(R.id.tv_discount)
    AppCompatTextView tvDiscount;

    @BindView(R.id.tv_points_value)
    AppCompatTextView tvPointsValue;

    @BindView(R.id.tv_des)
    AppCompatTextView tvDes;

    @BindView(R.id.tv_time_label)
    AppCompatTextView tvTimeLabel;

    @BindView(R.id.tv_time)
    AppCompatTextView tvTime;

    @BindView(R.id.rv_available)
    RecyclerView rvAvailable;

    @BindView(R.id.tv_view_all)
    AppCompatTextView viewAll;

    private RewardsDetailAdapter rewardsDetailAdapter;
    private int totalAvailability = 0;
    private DialogConfirmFragmentKH dialogConfirm;
    private DialogConfirmRedeemKH dialogConfirmRedeemKH;
    private DialogGetLinkRedeemKH dialogGetLinkRedeemKH;
    private DialogConfirmQrCodeKH dialogConfirmQrCodeKH;
    public static int FROM_ACTIVE = 1000;
    public static int FROM_SEARCH = 1001;
    boolean isLoginSocial;
    boolean showSpace = false;

    public static RewardsDetailKHFragment newInstance(GiftItems giftItems) {
        RewardsDetailKHFragment fragment = new RewardsDetailKHFragment();
        Bundle args = new Bundle();
        args.putSerializable(DATA, giftItems);
        fragment.setArguments(args);
        return fragment;
    }

    public static RewardsDetailKHFragment newInstance(GiftItems giftItems, boolean showSpace) {
        RewardsDetailKHFragment fragment = new RewardsDetailKHFragment();
        Bundle args = new Bundle();
        args.putSerializable(DATA, giftItems);
        args.putBoolean(SHOW_SPACE, showSpace);
        fragment.setArguments(args);
        return fragment;
    }

    public static RewardsDetailKHFragment newInstance(int type, RedeemItem redeemItem) {
        RewardsDetailKHFragment fragment = new RewardsDetailKHFragment();
        Bundle args = new Bundle();
        args.putSerializable(DATA, redeemItem);
        args.putInt(FROM, type);
        fragment.setArguments(args);
        return fragment;
    }

    public static RewardsDetailKHFragment newInstance(int type, SearchRewardResponse.Result.ItemReward item) {
        RewardsDetailKHFragment fragment = new RewardsDetailKHFragment();
        Bundle args = new Bundle();
        args.putInt(FROM, type);
        args.putSerializable(DATA, item);
        fragment.setArguments(args);
        return fragment;
    }

    public static final String TAG = RewardsDetailKHFragment.class.getSimpleName();
    private AccountRankDTO accountRankDTO;

    @Override
    protected boolean needShowBottomMenu() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
        isLoginSocial = new UserInfoBusiness(getContext()).getLogInMode() != EnumUtils.LoginModeTypeDef.PHONE_NUMBER;
        space_bottom = getActivity().findViewById(R.id.space_bottom);
        if (getArguments() != null) {
            showSpace = getArguments().getBoolean(SHOW_SPACE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        if ((space_bottom == null && mActivity instanceof HomeActivity) || showSpace) {
//            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) btnConfirm.getLayoutParams();
//            params.setMargins(params.leftMargin, 0, params.rightMargin, params.bottomMargin + getNavigationBarHeight());
//            btnConfirm.setLayoutParams(params);
            container_detail.setPadding(container_detail.getPaddingLeft(), container.getPaddingTop(), container.getPaddingRight(), getNavigationBarHeight());
        }
        return view;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            int from = bundle.getInt(FROM);
            if (from == FROM_ACTIVE) {
                redeemItem = (RedeemItem) bundle.getSerializable(DATA);
            } else if (from == FROM_SEARCH) {
                rewardItem = (SearchRewardResponse.Result.ItemReward) bundle.getSerializable(DATA);
            } else {
                giftItems = (GiftItems) bundle.getSerializable(DATA);
            }
            getRankAccount();
        }
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_reward_details;
    }


    private void logH(String m) {
        Log.e("H-Reward", m);
    }

    private void wsGetPartnerGiftDetail(String gifId) {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetPartnerGiftDetail(new KhApiCallback<KHBaseResponse<WsGetPartnerGiftDetailResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<WsGetPartnerGiftDetailResponse> body) {
                logH("wsGetPartnerGiftDetail > onSuccess");
                initData(body.getData().getGiftDetail());
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsGetPartnerGiftDetail > onFailed > " + "status: " + status + "message: " + message);
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsGetPartnerGiftDetailResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetPartnerGiftDetail > onFailure");
            }
        }, gifId);
    }

    @OnClick(R.id.tv_view_all)
    public void showAllDescription() {
        tvDes.setMaxLines(Integer.MAX_VALUE);
        viewAll.setVisibility(View.GONE);
    }

    private void initData(GiftDetail giftDetail) {
        if (giftDetail != null) {
            tvName.setText(giftDetail.getPartnerName());
            Glide.with(backdrop)
                    .load(giftDetail.getImageUrl())
                    .into(backdrop);
            Glide.with(icon)
                    .load(giftDetail.getIconUrl())
                    .circleCrop()
                    .into(icon);
            tvDiscount.setText(String.format(ResourceUtils.getString(R.string.reward_detail_discount), giftDetail.getDiscountRate()));
            if (giftDetail.getGiftPoint() == 0) {
                tvPointsValue.setText(ResourceUtils.getString(R.string.free));
                tvPointsValue.setTextColor(ResourceUtils.getColor(R.color.color_tab_enable));
            } else {
                tvPointsValue.setText(formatPoint(giftDetail.getGiftPoint()));
            }
            if (giftDetail.getDescription() != null && !giftDetail.getDescription().isEmpty()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tvDes.setText(Html.fromHtml(giftDetail.getDescription(), Html.FROM_HTML_MODE_COMPACT).toString());
                } else {
                    tvDes.setText(Html.fromHtml(giftDetail.getDescription()).toString());
                }
            } else {
                tvDes.setText("");
            }

            initRv(giftDetail.getStoreListDetail());
            if (giftDetail.getIsRedeemed() == 0 || giftDetail.getIsRedeemed() == 1 && giftDetail.getIsUsed() == 1) {
                btnConfirm.setText(ResourceUtils.getString(R.string.kh_redeem));
            } else {
                btnConfirm.setText(ResourceUtils.getString(R.string.kh_menu_qr_code));
            }
            if (giftDetail.getIsRedeemed() == 0 || giftDetail.getIsRedeemed() == 1 && giftDetail.getIsUsed() == 1) {
                btnConfirm.setText(ResourceUtils.getString(R.string.kh_redeem));
                btnConfirm.setBackgroundDrawable(ResourceUtils.getDrawable(R.drawable.bgr_btn_redeem));
                tvTimeLabel.setText(ResourceUtils.getString(R.string.expire_date));
                tvTimeLabel.setTextColor(ResourceUtils.getColor(R.color.white));
                tvTime.setText(DateConvert.formatDateTime(getActivity(), giftDetail.getExpireDate()));
                tvTime.setTextColor(ResourceUtils.getColor(R.color.white));
                tvTime.setAlpha(0.5f);
                if (accountRankDTO != null && giftDetail.getRankingList() != null) {
                    boolean enableRedeem = totalAvailability >= giftDetail.getGiftPoint() && giftDetail.getRankingList().contains(String.valueOf(accountRankDTO.rankId));
                    btnConfirm.setEnabled(enableRedeem);
                    btnConfirm.setSelected(enableRedeem);
                } else {
                    btnConfirm.setEnabled(false);
                    btnConfirm.setSelected(false);
                }
                btnConfirm.setOnClickListener(v -> showDialogConfirm(giftDetail));
            } else {
                if (giftDetail.getIsCoupon() == 1) {
                    btnConfirm.setText(ResourceUtils.getString(R.string.get_link));
                    btnConfirm.setOnClickListener(v -> showDialogConfirmRedeem(giftDetail.getLinkCoupon(),giftDetail.getImageCoupon(),giftDetail.getRedeemCode()));
                } else {
                    btnConfirm.setText(ResourceUtils.getString(R.string.kh_menu_qr_code));
                    btnConfirm.setOnClickListener(v -> showDialogConfirmQrCode(giftDetail.getGiftQrCode(), giftDetail.getExchangeCode()));
                }
                btnConfirm.setBackgroundDrawable(ResourceUtils.getDrawable(R.drawable.bgr_btn_qr_code));
                tvTimeLabel.setText(ResourceUtils.getString(R.string.kh_redeem_rewards_valid_until));
                tvTimeLabel.setTextColor(ResourceUtils.getColor(R.color.color_time_valid_label));
                tvTime.setText(DateConvert.formatDateTime(getActivity(), giftDetail.getValidUntil()));
                tvTime.setTextColor(ResourceUtils.getColor(R.color.color_time_valid));
            }
        }
        btnConfirm.setVisibility(View.VISIBLE);
    }

    private void initRv(List<StoreListDetail> storeListDetail) {
        Activity activity = getActivity();
        if (activity != null) {
            rewardsDetailAdapter = new RewardsDetailAdapter(storeListDetail, this);
            LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(activity,
                    LinearLayoutManager.VERTICAL, false);
            DividerItemDecoration divider = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
            Drawable mDivider = ContextCompat.getDrawable(activity, R.drawable.reward_divider);
            divider.setDrawable(mDivider);
            rvAvailable.setLayoutManager(verticalLayoutManager);
            rvAvailable.addItemDecoration(divider);
            rvAvailable.setAdapter(rewardsDetailAdapter);
        }
    }


    @OnClick(R.id.icBackToolbar)
    void onBack() {
        if (mActivity instanceof RedirectNotificationActivity
                || mActivity instanceof DeepLinkActivity
                || mActivity instanceof RewardDetailActivity) {
            mActivity.finish();
            return;
        }
        popBackStackFragment();
    }

    private String formatPoint(int point) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(point).replaceAll(",", "\\.");
    }

    @Override
    public void itemClick(int position) {
        replaceFragment(R.id.container_detail, RewardsMapKHFragment.newInstance(rewardsDetailAdapter.getData().get(position)), RewardsMapKHFragment.TAG);
    }

    /**
     * get info rank of acc
     */
    private void
    getRankAccount() {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                logH("wsGetAccountRankInfo > onSuccess");
                accountRankDTO = body.getData().getAccountRankDTO();
                wsGetAccountPointInfo();
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsGetAccountRankInfo > onFailed");
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logH("wsGetAccountRankInfo > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetAccountRankInfo > onFailure");
            }
        });
    }

    /**
     * get info list point of acc
     */
    private void wsGetAccountPointInfo() {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountPointInfo(new KhApiCallback<KHBaseResponse<AccountPointRankResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountPointRankResponse> body) {
                logH("wsGetAccountPointInfo > onSuccess");
                subPoint(body.getData().getListPoint());
            }

            @Override
            public void onFailed(String status, String message) {
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logH("wsGetAccountPointInfo > onFailed");
                logH("wsGetAccountPointInfo > " + status + " " + message);
                subPoint(null);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountPointRankResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetAccountPointInfo > onFailure");
            }
        });
    }

    private void subPoint(List<KHAccountPoint> list) {
        if (giftItems != null) {
            wsGetPartnerGiftDetail(giftItems.getGiftId());
        } else if (redeemItem != null) {
            wsGetPartnerGiftDetail(redeemItem.getGiftId());
        } else if (rewardItem != null) {
            wsGetPartnerGiftDetail(rewardItem.getGiftId());
        }
        if (list == null || list.isEmpty()) {
            return;
        }
        int totalAccumulate = 0;
        totalAvailability = 0;
        for (KHAccountPoint item : list) {
            if (item.getPointType() == PointType.ACTIVE.id) {
                totalAccumulate = totalAccumulate + item.getPointValue();
            } else if (item.getPointType() == PointType.PAST.id) {
                totalAvailability = totalAvailability + item.getPointValue();
            }
        }

    }

    private void wsRedeemGiftAndReturnExchangeCode(String gifId) {
        String phone = "";
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getActivity());
        UserInfo currentUser = userInfoBusiness.getUser();
        if (currentUser != null) {
            phone = currentUser.getPhone_number();
        }
        wsRedeemGiftAndReturnExchangeCode(gifId, String.valueOf(accountRankDTO.rankId), phone);
    }

    /**
     * get info list point of acc
     */
    private void wsRedeemGiftAndReturnExchangeCode(String gifId, String rankId, String phone) {

        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsRedeemGiftAndReturnExchangeCode(new KhApiCallback<KHBaseResponse<WsRedeemGiftAndReturnExchangeCodeResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<WsRedeemGiftAndReturnExchangeCodeResponse> body) {
                logH("WsRedeemGiftAndReturnExchangeCodeResponse > onSuccess");
                Exchange exchange = body.getData().getExChange();
                if (exchange != null) {
                    if (exchange.getIsCoupon() == 1) {
                        showDialogConfirmRedeem(exchange.getLinkCoupon(), exchange.getImageCoupon(), exchange.getRedeemCode());
                        wsGetPartnerGiftDetail(gifId);
                    } else {
                        showDialogConfirmRedeem(exchange.getGiftQrCode(), exchange.getRedeemCode());
                        wsGetPartnerGiftDetail(gifId);
                    }
                } else if (getActivity() != null) {
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), body.getData().getMessage());
                }
            }

            @Override
            public void onFailed(String status, String message) {
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logH("WsRedeemGiftAndReturnExchangeCodeResponse > onFailed");
                logH("WsRedeemGiftAndReturnExchangeCodeResponse > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsRedeemGiftAndReturnExchangeCodeResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("WsRedeemGiftAndReturnExchangeCodeResponse > onFailure");
            }
        }, gifId, rankId, phone);
    }

    private void showDialogConfirm(GiftDetail giftDetail) {
        if (dialogConfirm == null) {
            String content = String.format(ResourceUtils.getString(R.string.scan_qrcode_redeem_discount), giftDetail.getDiscountRate(), giftDetail.getGiftPoint());
            dialogConfirm = DialogConfirmFragmentKH.newInstance(giftDetail.getGiftName(), content);
            dialogConfirm.setInterface(() -> {
                wsRedeemGiftAndReturnExchangeCode(giftDetail.getGiftId());
            });
        } else {
            dialogConfirm.dismiss();
        }
        dialogConfirm.show(getChildFragmentManager(), "DialogConfirm");
    }

    private void showDialogConfirmRedeem(String base64, String code) {
        if (dialogConfirmRedeemKH == null) {
            dialogConfirmRedeemKH = DialogConfirmRedeemKH.newInstance(base64, code);
        } else {
            dialogConfirmRedeemKH.dismiss();
        }
        dialogConfirmRedeemKH.show(getChildFragmentManager(), "DialogConfirmRedeemKH");
    }

    private void showDialogConfirmRedeem(String link, String image, String code) {
        if (dialogGetLinkRedeemKH == null) {
            dialogGetLinkRedeemKH = DialogGetLinkRedeemKH.newInstance(link, image, code);
        } else {
            dialogGetLinkRedeemKH.dismiss();
        }
        dialogGetLinkRedeemKH.show(getChildFragmentManager(), "DialogLinkConfirmRedeemKH");
    }

    private void showDialogConfirmQrCode(String base64, String code) {
        if (dialogConfirmQrCodeKH == null) {
            dialogConfirmQrCodeKH = DialogConfirmQrCodeKH.newInstance(base64, code);
        } else {
            dialogConfirmQrCodeKH.dismiss();
        }
        dialogConfirmQrCodeKH.show(getChildFragmentManager(), "dialogConfirmQrCodeKH");
    }
}
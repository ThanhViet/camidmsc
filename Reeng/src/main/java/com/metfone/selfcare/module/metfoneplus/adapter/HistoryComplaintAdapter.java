package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.HistoryComplaint;
import com.metfone.selfcare.model.camid.ProcessComplaint;
import com.metfone.selfcare.module.metfoneplus.holder.HistoryComplaintViewHolder;

import java.util.HashMap;
import java.util.List;

public class HistoryComplaintAdapter extends RecyclerView.Adapter<HistoryComplaintViewHolder> {
    public static final int RECEIVED = 0;
    public static final int PROCESSING = 1;
    public static final int CONFIRM = 2;
    public static final int CLOSED = 3;
    private Context mContext;
    private List<HistoryComplaint> mHistoryComplaintList;
    private HashMap<Integer, String> mProcessComplaints;
    private View.OnClickListener mComplaintClickListener;
    private ComplaintListener complaintListener;

    public HistoryComplaintAdapter(Context context, List<HistoryComplaint> historyComplaints, List<ProcessComplaint> processComplaintList, View.OnClickListener onClickListener, ComplaintListener complaintListener) {
        this.mContext = context;
        this.mHistoryComplaintList = historyComplaints;
        this.mProcessComplaints = new HashMap<>();
        this.mComplaintClickListener = onClickListener;
        this.complaintListener = complaintListener;
        setProcessComplaintHashMap(processComplaintList);
    }

    private void setList(List<HistoryComplaint> historyComplaintList) {
        this.mHistoryComplaintList = historyComplaintList;
    }

    public void replaceData(List<HistoryComplaint> historyComplaintList) {
        setList(historyComplaintList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HistoryComplaintViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_complaint, parent, false);
        return new HistoryComplaintViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryComplaintViewHolder holder, int position) {
        HistoryComplaint hc = mHistoryComplaintList.get(position);

        holder.tvPreResult.setText(hc.getPreResult());
        holder.tvCompContent.setText(hc.getCompContent());
        holder.tvComplaintId.setText("ID: " + hc.getComplainId());
        holder.tvAcceptDate.setText(hc.getAcceptDate());
        holder.tvProLimitDate.setText(hc.getProLimitDate());
        holder.tvStaffInfo.setText(hc.getStaffInfo());
        holder.tvServiceTypeName.setText(hc.getServiceTypeName());
        holder.tvEndDate.setText(hc.getEndDate());


        holder.tvStaffPhone.setText(hc.getStaffPhone());
        holder.tvStaffPhone.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        holder.tvStaffPhone.setOnClickListener(v -> {
            if (complaintListener != null) {
                complaintListener.callSupport(position, hc);
            }
        });

        if (hc.isExpandView()) {
            holder.clContentExpand.setVisibility(View.VISIBLE);
            holder.ivArrowState.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24);
        } else {
            holder.clContentExpand.setVisibility(View.GONE);
            holder.ivArrowState.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);
            holder.btnCloseComplaint.setVisibility(View.GONE);
        }

        /*0: Received, 1: Processing, 2: Confirm, 3: CLOSED*/
        int status = hc.getStatus();
        switch (status) {
            case RECEIVED: {
                holder.tvStatus.setText(R.string.tab_mobile_ftth_expected_deadline_128);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.color_received));
                holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_ellipse_received, 0, 0, 0);
                break;
            }
            case PROCESSING: {
                holder.tvStatus.setText(R.string.tab_mobile_ftth_expected_deadline_144);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.color_processing));
                holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_ellipse_process, 0, 0, 0);
                break;
            }
            case CONFIRM: {
                holder.tvStatus.setText(R.string.tab_mobile_ftth_expected_deadline_146);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.color_confirm));
                holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_ellipse_confirm, 0, 0, 0);
                break;
            }
            case CLOSED: {
                holder.tvStatus.setText(R.string.tab_mobile_ftth_expected_deadline_130);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.color_close));
                holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_ellipse_closed, 0, 0, 0);
                break;
            }
        }

        if (hc.getStatus() == CLOSED) {
            holder.btnCloseComplaint.setVisibility(View.GONE);
            holder.tvEndDate.setVisibility(View.VISIBLE);
            holder.tvPreResult.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_ellipse_success, 0, 0, 0);
        } else {
            holder.btnCloseComplaint.setVisibility(View.VISIBLE);
            holder.tvEndDate.setVisibility(View.GONE);
            holder.tvPreResult.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_ellipse_error, 0, 0, 0);
        }

        holder.position = position;
        holder.mHistoryComplaint = hc;
        holder.clLayoutTile.setTag(holder);
        holder.clLayoutTile.setOnClickListener(mComplaintClickListener);

        holder.btnCloseComplaint.setOnClickListener(v -> {
            if (complaintListener != null) {
                complaintListener.closeComplaintClick(position, hc);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mHistoryComplaintList != null) {
            return mHistoryComplaintList.size();
        }
        return -1;
    }

    public void updateLayoutView(int position, boolean isExpand) {
        mHistoryComplaintList.get(position).setExpandView(isExpand);
        notifyItemChanged(position);
    }

    public void setProcessComplaintHashMap(List<ProcessComplaint> processComplaints) {
        for (ProcessComplaint p : processComplaints) {
            this.mProcessComplaints.put(p.getParamId(), p.getParamCode());
        }
    }

    public void closeComplaintSuccess(int position) {
        mHistoryComplaintList.get(position).setStatus(CLOSED);
        notifyItemChanged(position);
    }

    public interface ComplaintListener {
        void closeComplaintClick(int position, HistoryComplaint historyComplaint);

        void callSupport(int position, HistoryComplaint historyComplaint);
    }
}

package com.metfone.selfcare.module.metfoneplus.fragment;

import static com.metfone.selfcare.helper.Constants.PERMISSION.PERMISSION_REQUEST_LOCATION;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.metfone.esport.common.Common;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.AccountOcsValue;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.WsGetGiftListResponse;
import com.metfone.selfcare.module.home_kh.api.WsGetGiftSearchResponse;
import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointContainerFragment;
import com.metfone.selfcare.module.home_kh.fragment.reward.RewardCamIdKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewards.RewardsKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardsdetail.RewardsDetailKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.FilterShopFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.RewardsShopKHFragment;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;
import com.metfone.selfcare.module.home_kh.tab.model.GiftChildDetailItem;
import com.metfone.selfcare.module.home_kh.tab.model.GiftSearchObject;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeGiftItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardCategoryList;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardDetailItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardSearchItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardTopics2;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.metfoneplus.adapter.MPHomeAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.listener.OnClickShowFTTH;
import com.metfone.selfcare.module.metfoneplus.model.MPHomeServiceItem;
import com.metfone.selfcare.module.metfoneplus.model.MPHomeServiceList;
import com.metfone.selfcare.module.movie.event.UpdateWatchedEvent;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsAccountInfoResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountsOcsDetailResponse;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Response;

public class MPTabMobileFragment extends MPBaseFragment implements TabHomeKhListener.OnAdapterClick{

    private static final String TAG = MPTabMobileFragment.class.getSimpleName();
    FrameLayout pbLoading;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private FragmentManager mFragmentManager;
    private RadioButton btnSwitchMobile;
    private RadioButton btnSwitchInternet;
    private HomeActivity mParentActivity;
    private CamIdUserBusiness mCamIdUserBusiness;
    private ApplicationController mApplication;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LoadingView loadingView;
    private KhHomeClient homeKhClient;
    private ArrayList<IHomeModelType> homeData;
    private RecyclerView recyclerView;
    private MPHomeAdapter mpHomeAdapter;
    private LinearLayout btnInforDetail;
    private ConstraintLayout inforLayout;
    private TextView tvName, tvPhoneNumber, tvBasicValue, tvExchangeValue, tvOneWay, tvTwoWay;
    private ImageView ivRankUser;

    private ImageView ivHeaderInfo;


    public MPTabMobileFragment() {
    }

    public static MPTabMobileFragment newInstance() {
        MPTabMobileFragment fragment = new MPTabMobileFragment();
        return fragment;
    }

    private OnClickShowFTTH onClickShowFTTH;

    public void setListener(OnClickShowFTTH showFTTHListener) {
        onClickShowFTTH = showFTTHListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_tab_mobile;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
            initView(view);
    }

    public void loadData(){
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            mParentActivity.showError(mRes.getString(R.string.error_internet_disconnect), null);
        } else {
            getWSGetAccountsOcsDetail();
        }
        String phoneService = getCamIdUserBusiness().getPhoneService();
        if (!"".equals(phoneService)) {
            if ("".equals(getCamIdUserBusiness().getMetfoneSessionId())) {
                autoLogin(phoneService);
            } else {
                getWSAccountInfo();
            }
        }

//        tvName.setText((new UserInfoBusiness(getContext())).getUser().getFull_name());

        if (homeData == null) homeData = new ArrayList<>();
        else homeData.clear();
        List<MPHomeServiceItem> serviceList = new ArrayList<>();
        for (int i = 0 ; i < 6; i++) {
            MPHomeServiceItem item = new MPHomeServiceItem(i, "", "");
            item.setType(IHomeModelType.MP_SERVICE);
            serviceList.add(item);
        }
        MPHomeServiceList cate = new MPHomeServiceList();
        cate.serviceItems = serviceList;
        homeData.add(cate);
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        wsGtAllPartnerGiftSearch();
        getWsGetListGiftBuyMost();

    }
    public void initView(View view) {
        loadingView = view.findViewById(R.id.loading_view);
        recyclerView = view.findViewById(R.id.recycler_view_mp);
        btnInforDetail = view.findViewById(R.id.btnInforDetail);
        inforLayout = view.findViewById(R.id.infor_layout);
        tvName = view.findViewById(R.id.tvName);
        tvPhoneNumber = view.findViewById(R.id.tvPhoneNumber);
        tvBasicValue = view.findViewById(R.id.tvBasicValue);
        tvExchangeValue = view.findViewById(R.id.tvExchangeValue);
        tvOneWay = view.findViewById(R.id.tvOneWay);
        tvTwoWay = view.findViewById(R.id.tvTwoWay);
        btnSwitchMobile = view.findViewById(R.id.btnSwitchMobile);
        btnSwitchInternet = view.findViewById(R.id.btnSwitchInternet);
        pbLoading = view.findViewById(R.id.view_loading);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        ivRankUser = view.findViewById(R.id.iv_logo_member);

        ivHeaderInfo = view.findViewById(R.id.iv_header_infor);

        ivHeaderInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(R.id.root_view_mobile_mp, RewardCamIdKHFragment.newInstance(), RewardCamIdKHFragment.TAG);
            }
        });

        btnSwitchInternet.setOnClickListener(this::handleSwitchInternet);
        btnInforDetail.setOnClickListener(this::clickInforDetail);


        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setEnabled(false);

        mpHomeAdapter = new MPHomeAdapter(getActivity());
        if (homeData == null) homeData = new ArrayList<>();
        else homeData.clear();
        mpHomeAdapter.setItems(homeData);
        mpHomeAdapter.setListener(this);
        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(getContext(), LinearLayout.VERTICAL, false);
        BaseAdapter.setupVerticalRecyclerView(getContext(), recyclerView, layoutManager, mpHomeAdapter, true, 5);
    }

    private void clickInforDetail(View view) {
        gotoMPDetailFragment(false);
    }
    private void handleSwitchInternet(View view) {
//        mFragmentManager.beginTransaction()
//                .replace(R.id.root_view_mobile_mp, MPTabInternetFragment.newInstance())
//                .addToBackStack(TAG)
//                .commit();

        if (onClickShowFTTH != null) onClickShowFTTH.showFTTH();

        btnSwitchMobile.setChecked(true);
        btnSwitchInternet.setChecked(false);

    }


    @Override
    public void onAttach(@NonNull Context context) {
        this.mParentActivity = (HomeActivity) context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        super.onAttach(context);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(final UpdateWatchedEvent event) {
        Log.i(TAG, "onEvent UpdateWatchedEvent: " + event);
        EventHelper.removeStickyEvent(event);
    }

    private void drawProfile(AccountRankDTO accountRankDTO, UserInfo userInfo) {
        tvName.setText(userInfo.getFull_name());
        KhUserRank.RewardRank myRank = KhUserRank.RewardRank.getById(accountRankDTO.rankId);
        if (myRank != null) {
            ivRankUser.setImageDrawable(myRank.drawable);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void loadInfoUser(final LoadInfoUser event) {
        if (event.loadType == LoadInfoUser.LoadType.LOADING) {
        } else if (event.loadType == LoadInfoUser.LoadType.NOT_LOGIN) {
        } else {
            drawProfile(event.rankDTO, event.userInfo);
        }
    }

    @Subscribe
    public void onReloadDataMetfonePlus(ReloadDataMetfonePlus event) {
        try {
            String phoneService = getCamIdUserBusiness().getPhoneService();
            if (!"".equals(phoneService)) {
                if (!"".equals(getCamIdUserBusiness().getMetfoneSessionId())) {
                    getWSAccountInfo();
                }
            }
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private void setupAdapter() {

        Collections.sort(homeData, (o1, o2) -> (o1.getItemType() > o2.getItemType()) ? 1 : -1);

        if (mpHomeAdapter != null) {
            mpHomeAdapter.setItems(homeData);
            mpHomeAdapter.notifyDataSetChanged();
        }
    }

    private void updateUICard() {
        if (inforLayout.getVisibility() == View.GONE) inforLayout.setVisibility(View.VISIBLE);
        tvBasicValue.setText("$"+String.valueOf(mBasicValue));

        if (mExchangeValue > 0) {
            tvExchangeValue.setText("$"+String.valueOf(mExchangeValue));
        } else if (mDataValue != null && Double.parseDouble(mDataValue) > 0) {
            tvExchangeValue.setText(mDataValue + "MB");
        } else {
            tvExchangeValue.setText("$0.0");
        }
    }
    private double mExchangeValue = 0.0;
    private double mBasicValue = 0.0;
    private String mDataValue = "";

    public void autoLogin(String userName) {
        MetfonePlusClient client = new MetfonePlusClient();
        client.autoLogin(userName, new MPApiCallback<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response) {
                if (response.body() != null && response.body().getUsername() != null) {
                    getWSAccountInfo();
                    getWSGetAccountsOcsDetail();
                }
            }

            @Override
            public void onError(Throwable error) {

            }
        });
    }
    public void getWSAccountInfo() {
        pbLoading.setVisibility(View.VISIBLE);
        MetfonePlusClient client = new MetfonePlusClient();
        client.wsAccountInfo(new MPApiCallback<WsAccountInfoResponse>() {
            @Override
            public void onResponse(Response<WsAccountInfoResponse> response) {
                if (response.body() != null && response.body().getResult() != null
                        && response.body().getResult().getWsResponse() != null
                        && response.body().getResult().getErrorCode().equals("0")) {
                    WsAccountInfoResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    mExchangeValue = wsResponse.getExchange();
                    mBasicValue = wsResponse.getBasic();
                    mDataValue = wsResponse.getData();
                    if (mDataValue != null && mDataValue.contains(",")) {
                        mDataValue = mDataValue.replace(",", "");
                    }
                    updateUICard();
                }
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onError(Throwable error) {
                pbLoading.setVisibility(View.GONE);
            }
        });
    }

    private void getWSGetAccountsOcsDetail() {
        pbLoading.setVisibility(View.VISIBLE);
        MetfonePlusClient client = new MetfonePlusClient();
        client.wsGetAccountsOcsDetail(new MPApiCallback<WsGetAccountsOcsDetailResponse>() {
            @Override
            public void onResponse(Response<WsGetAccountsOcsDetailResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    if (response.body().getResult().getWsResponse() == null) {
                        ToastUtils.showToast(mParentActivity, response.body().getResult().getUserMsg());
                        pbLoading.setVisibility(View.GONE);
                        return;
                    }
                    List<WsGetAccountsOcsDetailResponse.Response> wsResponse = response.body().getResult().getWsResponse();
                    if (wsResponse != null) {
                        for (WsGetAccountsOcsDetailResponse.Response res : wsResponse) {
                            try {
                                if (res.getTitle().equals(getString(R.string.m_p_detail_title_compare_api_general_info))) {
                                    tvPhoneNumber.setText("0"+getCamIdUserBusiness().getMetfoneUsernameIsdn().trim());
                                    if (res.getAccountOcsValues() != null) {
                                        updateUIMetfoneCard(res.getAccountOcsValues());
                                    }
                                }
                            } catch (Exception exception) {
                            }

                        }
                    }

                }
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onError(Throwable error) {
                pbLoading.setVisibility(View.GONE);
            }
        });
    }
    private void updateUIMetfoneCard(List<AccountOcsValue> accountOcsValue) {
        for (AccountOcsValue value : accountOcsValue) {
            if (value.getTitle().equals(getString(R.string.m_p_detail_block_one_way_date_compare_api))) {
                tvOneWay.setText(value.getValue());
            } else if (value.getTitle().equals(getString(R.string.m_p_detail_block_two_way_date_compare_api))) {
                tvTwoWay.setText(value.getValue());
            }
        }
    }

    // api 2
    private void wsGtAllPartnerGiftSearch() {
        pbLoading.setVisibility(View.VISIBLE);
        homeKhClient.wsGtAllPartnerGiftSearch(new MPApiCallback<WsGetGiftSearchResponse>() {
            @Override
            public void onResponse(retrofit2.Response<WsGetGiftSearchResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    WsGetGiftSearchResponse.Response wsResponse = response.body().getResult().getWsResponse();

                    List<KhHomeRewardSearchItem> categoryItems = new ArrayList<>();
                    // default has metfone
                    String metfoneTitle = "";
                    String moreTitle = "";
                    if (getActivity() != null) {
                        metfoneTitle = getActivity().getString(R.string.metfone);
                        moreTitle = getActivity().getString(R.string.rewards_more);
                    } else {
                        metfoneTitle = ApplicationController.self().getString(R.string.metfone);
                        moreTitle = ApplicationController.self().getString(R.string.rewards_more);
                    }
                    KhHomeRewardSearchItem metfone = new KhHomeRewardSearchItem(990999, "metfone", metfoneTitle);
                    metfone.setType(IHomeModelType.REWARD_CATEGORY);
                    categoryItems.add(0, metfone);
                    int i = 0;
                    if (wsResponse.object != null && !wsResponse.object.isEmpty()) {
                        for (GiftSearchObject x : wsResponse.object) {
                            if (x.gifts != null && x.gifts.size() > 0) {
                                i++;
                                KhHomeRewardSearchItem item = new KhHomeRewardSearchItem();
                                item.giftObject = x;
                                item.setType(IHomeModelType.REWARD_CATEGORY);
                                categoryItems.add(item);
                                if (i > 2) break;
                            }
                        }
                    }

                    KhHomeRewardSearchItem addMore = new KhHomeRewardSearchItem(991999, "more", moreTitle);
                    addMore.setType(IHomeModelType.REWARD_CATEGORY);
                    categoryItems.add(addMore);

                    // remove old cate
                    KhHomeRewardCategoryList rewardCate = null;
                    for (IHomeModelType item : homeData) {
                        if (item.getItemType() == IHomeModelType.REWARD_CATEGORY) {
                            rewardCate = (KhHomeRewardCategoryList) item;
                            break;
                        }
                    }
                    if (rewardCate != null) {
                        homeData.remove(rewardCate);
                    }

                    KhHomeRewardCategoryList cate = new KhHomeRewardCategoryList();
                    cate.categoryItems = categoryItems;

                    homeData.add(cate);
                    setupAdapter();
                    pbLoading.setVisibility(View.GONE);
                } else {
                    Log.e(TAG, "getWSGetAccountRank - onResponse: error");
                    pbLoading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
                pbLoading.setVisibility(View.GONE);
            }
        });
    }
    // api 11
    private void getWsGetListGiftBuyMost() {
        pbLoading.setVisibility(View.VISIBLE);
        homeKhClient.wsGetListGiftBuyMost(new MPApiCallback<WsGetGiftListResponse>() {
            @Override
            public void onResponse(retrofit2.Response<WsGetGiftListResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    WsGetGiftListResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    // Get value of item have "title" = "ACCOUNTS"
                    Log.e(TAG, "getWsGetListGiftBuyMost - onResponse: OK " + wsResponse.object.size());

                    List<KhHomeRewardDetailItem> categoryItems = new ArrayList<>();
                    int i = 0;
                    if (wsResponse.object != null && !wsResponse.object.isEmpty()) {
                        for (GiftChildDetailItem x : wsResponse.object) {
                            KhHomeRewardDetailItem item = new KhHomeRewardDetailItem();
                            item.giftObject = x;
                            item.setType(IHomeModelType.REWARD_TOPIC_2);
                            categoryItems.add(item);
                            i++;
                            if (i > 3) break;
                        }
                    }

                    // remove old topic
                    KhHomeRewardTopics2 rewardCate = null;
                    for (IHomeModelType item : homeData) {
                        if (item.getItemType() == IHomeModelType.REWARD_TOPIC_2) {
                            rewardCate = (KhHomeRewardTopics2) item;
                            break;
                        }
                    }
                    if (rewardCate != null) {
                        homeData.remove(rewardCate);
                    }

                    KhHomeRewardTopics2 cate = new KhHomeRewardTopics2();
                    cate.categoryItems = categoryItems;

                    homeData.add(cate);
                    setupAdapter();
                    pbLoading.setVisibility(View.GONE);
                } else {
                    Log.e(TAG, "getWSGetAccountRank - onResponse: error");
                    pbLoading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
                pbLoading.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClickChannelItem(Object item, int position) {

    }

    @Override
    public void onClickMoreChannelItem(Object item, int position) {

    }

    @Override
    public void onClickComicItem(Object item, int position) {

    }

    @Override
    public void onClickMoreComicItem(Object item, int position) {

    }

    @Override
    public void onClickMovieItem(Object item, int position) {

    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {

    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {

    }

    @Override
    public void onClickMusicItem(Object item, int position) {

    }

    @Override
    public void onClickMoreMusicItem(Object item, int position) {

    }

    @Override
    public void onClickNewsItem(Object item, int position) {

    }

    @Override
    public void onClickMoreNewsItem(Object item, int position) {

    }

    @Override
    public void onClickTiinItem(Object item, int position) {

    }

    @Override
    public void onClickMoreTiinItem(Object item, int position) {

    }

    @Override
    public void onClickVideoItem(Object item, int position) {

    }

    @Override
    public void onClickMoreVideoItem(Object item, int position) {

    }

    @Override
    public void onClickChannelVideoItem(Object item, int position) {

    }

    @Override
    public void onGameItemClicked() {

    }

    @Override
    public void onGameTabClick() {

    }

    @Override
    public void onNewYearGameClick() {

    }

    @Override
    public void onLuckyWheelGameClick() {

    }

    @Override
    public void onClickSliderBannerItem(Object item, int position) {

    }

    @Override
    public void onClickTitleBox(IHomeModelType item, int position) {
        if (item.getItemType() == IHomeModelType.REWARD_CATEGORY) {
            showRewardShops(null);
        }
    }

    @Override
    public void onClickSignUp() {

    }

    @Override
    public void onScrollData(IHomeModelType homeModelType) {

    }

    @Override
    public void onClickRewardItem(Object item, int position) {
        if (item instanceof KhHomeRewardDetailItem) {
            GiftItems g = new GiftItems();
            g.setGiftId(((KhHomeRewardDetailItem) item).giftObject.giftId);

            FragmentManager mFragmentManager = getParentFragmentManager();
            FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                    R.anim.decelerate_slide_out_left_fragment,
                    R.anim.decelerate_slide_in_left_fragment,
                    R.anim.decelerate_slide_out_right
            );
            mFragmentTransaction.replace(R.id.root_view_mobile_mp, RewardsDetailKHFragment.newInstance(g, true), RewardsDetailKHFragment.TAG);
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            mFragmentTransaction.addToBackStack(RewardsDetailKHFragment.TAG);
            mFragmentTransaction.commitAllowingStateLoss();
        }
    }

    private void showRewardShops(String categoryPosition) {
        FragmentManager mFragmentManager = getParentFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                R.anim.decelerate_slide_out_left_fragment,
                R.anim.decelerate_slide_in_left_fragment,
                R.anim.decelerate_slide_out_right
        );

        mFragmentTransaction.replace(R.id.root_view_mobile_mp, RewardsShopKHFragment.newInstance(categoryPosition, false), RewardsShopKHFragment.TAG);

        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(RewardsShopKHFragment.TAG);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onClickRewardCategoryItem(Object item, int position) {
        if (item instanceof KhHomeRewardSearchItem) {
            if (((KhHomeRewardSearchItem) item).id == 991999) {
                // TODO for MORE
                AccountRankDTO rankDto = ApplicationController.self().getAccountRankDTO();
                if (rankDto != null) {
                    replaceFragment(R.id.root_view_mobile_mp,
                            FilterShopFragment.newInstance(RewardsShopKHFragment.CATEGORY,
                                    0,
                                    new ArrayList<>(),
                                    String.valueOf(rankDto.rankId), true)
                            , HistoryPointContainerFragment.TAG);
                } else {
                }

            } else if (((KhHomeRewardSearchItem) item).id == 990999) {
                // TODO for Metfone

                replaceFragment(R.id.root_view_mobile_mp, RewardsKHFragment.newInstance(), HistoryPointContainerFragment.TAG);
            } else {
                homeKhClient.wsAppLog(new MPApiCallback<BaseResponse>() {
                    @Override
                    public void onResponse(retrofit2.Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            android.util.Log.d(TAG, String.format("wsLogApp => succeed [%s]", Constants.LOG_APP.REWARD_SHOP));
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        android.util.Log.d(TAG, "wsLogApp => failed");
                    }
                }, Constants.LOG_APP.REWARD_SHOP);
                showRewardShops(String.valueOf(position));
            }
        }
        if (item instanceof MPHomeServiceItem) {
            // TODO for service 1
            switch (((MPHomeServiceItem) item).id) {
                case 0:
                    goToTopUpScreen();
                    break;
                case 1:
                    gotoBuyServicesScreen();
                    break;
                case 2:
                    gotoMPSupportFragment();
                    mParentActivity.setBottomNavigationBarColor(R.color.m_home_tab_background, true);
                    break;
                case 3:
                    gotoIShareFragment();
                    break;
                case 5:
                    gotoExchangeDamagedFragment();
                    break;
                case 4:
                    if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                PERMISSION_REQUEST_LOCATION);
                    } else {
                        gotoBuyPhoneNumberFragment(false);
                    }
                    break;
            }

        }
    }
    protected void replaceFragment(int container, Fragment fragment, String tag) {
        FragmentTransaction mFragmentTransaction = getParentFragmentManager().beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                R.anim.decelerate_slide_out_left_fragment,
                R.anim.decelerate_slide_in_left_fragment,
                R.anim.decelerate_slide_out_right
        );

        mFragmentTransaction.replace(container, fragment);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(tag);
        mFragmentTransaction.commitAllowingStateLoss();
    }
    @Override
    public void onGiftClicked(KhHomeGiftItem.GiftItem giftItem) {
    }

    private void goToTopUpScreen() {
        gotoMPTopUpFragment();
        mParentActivity.setBottomNavigationBarColor(R.color.m_home_tab_background, true);
    }

    private void gotoBuyServicesScreen() {
        gotoMPServiceFragment(MPServiceFragment.TAB_BUY_SERVICE);
    }

    private void openRewardCamID() {

    }
}
package com.metfone.selfcare.module.netnews.HomeNews.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.DateUtilitis;

public class HomeNewLargeAdapterV5 extends RecyclerView.Adapter<BaseViewHolder> {
    private final int SECTIONE_NORMAL = 2;
    private final int SECTIONE_EMPTY = 0;
    private HomeNewsModel model;
    private Context mContext;
    private int type;
    AbsInterface.OnHomeNewsItemListener listener;

    public HomeNewLargeAdapterV5(HomeNewsModel model, Context mContext, int type, AbsInterface.OnHomeNewsItemListener listener) {
        this.model = model;
        this.mContext = mContext;
        this.type = type;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case SECTIONE_NORMAL:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_large_news, parent, false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final NewsModel tiinModel = getItem(position);
        if (holder.getView(R.id.iv_cover) != null) {
            ImageBusiness.setImageNew(tiinModel.getImage(), holder.getView(R.id.iv_cover));
        }
        if (holder.getView(R.id.tv_title) != null) {
            holder.setText(R.id.tv_title, tiinModel.getTitle());
        }
        if (holder.getView(R.id.tv_datetime) != null) {
            holder.setVisible(R.id.tv_datetime, true);
            if (tiinModel.getTimeStamp() > 0) {
                holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, tiinModel.getTimeStamp()));
            } else {
                holder.setText(R.id.tv_datetime, tiinModel.getDatePub());
            }
        }
        if (holder.getView(R.id.tv_category) != null) {
            holder.setText(R.id.tv_category, tiinModel.getSourceName());
            holder.setOnClickListener(R.id.tv_category, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemCategoryClick(tiinModel.getSourceName(), tiinModel.getSid());
                }
            });
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(tiinModel);
            }
        });
        if (holder.getView(R.id.button_option) != null) {
            holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickBtnMore(tiinModel);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null) {

            return SECTIONE_NORMAL;
        }
        return SECTIONE_EMPTY;
    }

    public NewsModel getItem(int position) {
        try {
            return model.getData().get(position);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
        return null;
    }

}

package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCDeeplink;
import com.metfone.selfcare.module.selfcare.model.SCPackage;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCImageLoader;

import java.util.ArrayList;

public class SCPackageAdapter<T> extends BaseAdapter<BaseViewHolder> {

    private ArrayList<T> data;
    private int type = SCConstants.SCREEN_TYPE.TYPE_PACKAGE;
    private AbsInterface.OnPackageRegisterListener listener;

    public SCPackageAdapter(Context context, AbsInterface.OnPackageRegisterListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(ArrayList<T> data) {
        this.data = data;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_package, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder holder, int position) {
        if(type == SCConstants.SCREEN_TYPE.TYPE_PACKAGE)
        {
            final SCPackage model = (SCPackage) data.get(position);
            if(model != null)
            {
                holder.setText(R.id.tvName, TextUtils.isEmpty(model.getName()) ? "" : model.getName());
                holder.setText(R.id.tvDescription, model.getShortDes() + "");
                SCImageLoader.setImage(mContext, (ImageView) holder.getView(R.id.imvImage), model.getIconUrl());

                if(model.isRegisterable())
                {
                    if(model.isRegister())
                    {
                        holder.getView(R.id.btnBuy).setVisibility(View.GONE);
                    }
                    else
                    {
                        holder.getView(R.id.btnBuy).setVisibility(View.VISIBLE);
                        holder.setText(R.id.btnBuy, mContext.getString(R.string.sc_register));
                    }
                }
                else
                    holder.getView(R.id.btnBuy).setVisibility(View.GONE);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listener != null)
                            listener.onPackageClick(model);
                    }
                });

                holder.getView(R.id.btnBuy).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onRegisterClick(model, holder.getView(R.id.btnBuy));
                        }
                    }
                });
            }
        }
        else if(type == SCConstants.SCREEN_TYPE.TYPE_SERVICES)
        {
            final SCPackage model = (SCPackage) data.get(position);
            if(model != null)
            {
                holder.setText(R.id.tvName, TextUtils.isEmpty(model.getName()) ? "" : model.getName());
                holder.setText(R.id.tvDescription, model.getShortDes() + "");
                SCImageLoader.setImage(mContext, (ImageView) holder.getView(R.id.imvImage), model.getIconUrl());
                if(model.isRegisterable())
                {
                    holder.getView(R.id.btnBuy).setVisibility(View.VISIBLE);
                    if(model.isRegister())
                    {
                        holder.setText(R.id.btnBuy, mContext.getString(R.string.sc_unsubcribe));
                    }
                    else
                    {
                        holder.setText(R.id.btnBuy, mContext.getString(R.string.sc_subcribe));
                    }
                }
                else
                    holder.getView(R.id.btnBuy).setVisibility(View.GONE);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listener != null)
                            listener.onPackageClick(model);
                    }
                });

                holder.getView(R.id.btnBuy).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onRegisterClick(model, holder.getView(R.id.btnBuy));
                        }
                    }
                });
            }
        }
        else if(type == SCConstants.SCREEN_TYPE.TYPE_UTILITIES)
        {
            final SCDeeplink model = (SCDeeplink) data.get(position);
            if(model != null)
            {
                holder.setText(R.id.tvName, TextUtils.isEmpty(model.getName()) ? "" : model.getName());
                SCImageLoader.setImage(mContext, (ImageView) holder.getView(R.id.imvImage), model.getImagineUrl());
                holder.getView(R.id.tvDescription).setVisibility(View.GONE);
                holder.getView(R.id.btnBuy).setVisibility(View.GONE);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listener != null)
                            listener.onDeeplinkClick(model);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }
}
package com.metfone.selfcare.module.selfcare.network.request;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterPackageRequest {
    private String isdn;
    private int actionType;
    private String language;
    private String serviceCode;

    public RegisterPackageRequest(String isdn, int actionType, String language, String serviceCode) {
        this.isdn = isdn;
        this.actionType = actionType;
        this.language = language;
        this.serviceCode = serviceCode;
    }

    public String getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msisdn", isdn);
//            jsonObject.put("actionType", actionType);
//            jsonObject.put("language", language);
//            jsonObject.put("serviceCode", serviceCode);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getIsdn() {
        return isdn;
    }

    public int getActionType() {
        return actionType;
    }

    public String getLanguage() {
        return language;
    }

    public String getServiceCode() {
        return serviceCode;
    }
}

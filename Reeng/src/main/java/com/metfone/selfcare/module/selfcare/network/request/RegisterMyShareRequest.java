package com.metfone.selfcare.module.selfcare.network.request;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class RegisterMyShareRequest implements Serializable {
    private String msisdn;
    private String balanceTranfer;
    private String receiverMsisdn;
    private String otp;

    public RegisterMyShareRequest(String msisdn, String balanceTranfer, String receiverMsisdn) {
        this.msisdn = msisdn;
        this.balanceTranfer = balanceTranfer;
        this.receiverMsisdn = receiverMsisdn;
    }

    public String getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("balanceTranfer", balanceTranfer);
            jsonObject.put("msisdn", msisdn);
            jsonObject.put("receiverMsisdn", receiverMsisdn);
            if(!TextUtils.isEmpty(otp))
                jsonObject.put("otpCode", otp);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getBalanceTranfer() {
        return balanceTranfer;
    }

    public void setBalanceTranfer(String balanceTranfer) {
        this.balanceTranfer = balanceTranfer;
    }

    public String getReceiverMsisdn() {
        return receiverMsisdn;
    }

    public void setReceiverMsisdn(String receiverMsisdn) {
        this.receiverMsisdn = receiverMsisdn;
    }
}

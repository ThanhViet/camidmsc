package com.metfone.selfcare.module.metfoneplus.dialog;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.selfcare.R;

public class BaseMPConfirmDialog extends MPHeroDialog {
    private String lottieFile;
    private int drawableId;
    private int titleId;
    private int contentId;
    private int topButtonStringId;
    private int bottomButtonStringId;
    private View.OnClickListener topButtonClickListener;
    private View.OnClickListener bottomButtonClickListener;
    private String title;
    private String content;
    boolean imageVisibility = true;
    boolean bgWhite;

    public void setBgWhite(boolean bgWhite) {
        this.bgWhite = bgWhite;
    }

    public BaseMPConfirmDialog(int drawableId, int titleId, int contentId, int topButtonStringId, int bottomButtonStringId, View.OnClickListener topButtonClickListener, View.OnClickListener bottomButtonClickListener) {
        this.drawableId = drawableId;
        this.titleId = titleId;
        this.contentId = contentId;
        this.topButtonStringId = topButtonStringId;
        this.bottomButtonStringId = bottomButtonStringId;
        this.topButtonClickListener = topButtonClickListener;
        this.bottomButtonClickListener = bottomButtonClickListener;
    }

    public BaseMPConfirmDialog(int titleId, int contentId, int topButtonStringId, int bottomButtonStringId, View.OnClickListener topButtonClickListener, View.OnClickListener bottomButtonClickListener) {
        this.titleId = titleId;
        this.contentId = contentId;
        this.topButtonStringId = topButtonStringId;
        this.bottomButtonStringId = bottomButtonStringId;
        this.topButtonClickListener = topButtonClickListener;
        this.bottomButtonClickListener = bottomButtonClickListener;
        this.imageVisibility = false;
    }

    public BaseMPConfirmDialog(String lottieFile, int titleId, int contentId, int topButtonStringId, int bottomButtonStringId, View.OnClickListener topButtonClickListener, View.OnClickListener bottomButtonClickListener) {
        this.lottieFile = lottieFile;
        this.titleId = titleId;
        this.contentId = contentId;
        this.topButtonStringId = topButtonStringId;
        this.bottomButtonStringId = bottomButtonStringId;
        this.topButtonClickListener = topButtonClickListener;
        this.bottomButtonClickListener = bottomButtonClickListener;
    }

    public BaseMPConfirmDialog(int drawableId, int titleId, int contentId, int topButtonStringId, int bottomButtonStringId, View.OnClickListener topButtonClickListener, View.OnClickListener bottomButtonClickListener, boolean imageVisibility) {
        this.drawableId = drawableId;
        this.titleId = titleId;
        this.contentId = contentId;
        this.topButtonStringId = topButtonStringId;
        this.bottomButtonStringId = bottomButtonStringId;
        this.topButtonClickListener = topButtonClickListener;
        this.bottomButtonClickListener = bottomButtonClickListener;
        this.imageVisibility = imageVisibility;
    }

    public BaseMPConfirmDialog(int drawableId, String title, String content) {
        this.drawableId = drawableId;
        this.title = title;
        this.content = content;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateUiDialog();

    }

    private void updateUiDialog() {
        if (drawableId == R.drawable.img_dialog_1){
            flImage.setVisibility(View.VISIBLE);
            lavAnimations.setVisibility(View.VISIBLE);
        }else {
            flImage.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.VISIBLE);
            mImage.setImageResource(drawableId);
        }
        if (bgWhite){
            lnlContent.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.bg_white_corner_6));
            mTitle.setTextColor(Color.parseColor("#CC212121"));
            mContent.setTextColor(Color.parseColor("#80212121"));
            mButtonBottom.setTextColor(Color.parseColor("#80212121"));
        }

        if (imageVisibility){
            flImage.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.VISIBLE);
            if(!StringUtils.isEmpty(lottieFile)){
                mImage.setVisibility(View.GONE);
                lavAnimations.setVisibility(View.VISIBLE);
                lavAnimations.setAnimation(lottieFile);
            }
        }else {
            flImage.setVisibility(View.GONE);
            mImage.setVisibility(View.GONE);
        }
        mImage.setImageResource(drawableId);
        mTitle.setText(titleId);
        mContent.setText(contentId);
        setButtonTopVisibility(View.VISIBLE);
        setButtonBottomVisibility(View.VISIBLE);
        setTopButton(topButtonStringId, R.drawable.bg_red_button_corner_6, topButtonClickListener);
        setBottomButton(bottomButtonStringId, R.drawable.bg_transparent_button_corner_6, bottomButtonClickListener);
    }

    private void updateUiDialog(int drawableId, int titleId, int contentId) {
        mImage.setImageResource(drawableId);
        mTitle.setText(titleId);
        mContent.setText(contentId);
        setButtonTopVisibility(View.GONE);
        setButtonBottomVisibility(View.GONE);
    }
}

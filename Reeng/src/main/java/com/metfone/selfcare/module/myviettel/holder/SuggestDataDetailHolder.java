/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.myviettel.listener.OnDataChallengeListener;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;

import butterknife.BindView;

public class SuggestDataDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.button_submit)
    @Nullable
    View btnSubmit;

    private DataChallenge data;

    public SuggestDataDetailHolder(View view, final Activity activity, final OnDataChallengeListener listener) {
        super(view);
        if (btnSubmit != null) {
            btnSubmit.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (data != null && listener != null) {
                        listener.onClickInviteDataPackage(data);
                    }
                }
            });
        }
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof DataChallenge) {
            data = (DataChallenge) item;
            if (tvTitle != null) {
                tvTitle.setText(data.getName());
            }
            if (tvDesc != null) {
                tvDesc.setText(data.getDescription());
            }
        } else {
            data = null;
        }
    }

}
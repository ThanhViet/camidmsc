package com.metfone.selfcare.module.spoint.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PackageModel implements Serializable {
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("sms_command")
    @Expose
    private String smsCommand;
    @SerializedName("sms_codes")
    @Expose
    private String smsCodes;
    @SerializedName("type")
    @Expose
    private String type;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSmsCommand() {
        return smsCommand;
    }

    public void setSmsCommand(String smsCommand) {
        this.smsCommand = smsCommand;
    }

    public String getSmsCodes() {
        return smsCodes;
    }

    public void setSmsCodes(String smsCodes) {
        this.smsCodes = smsCodes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

package com.metfone.selfcare.module.home_kh.fragment.rewardshop.event;

import com.metfone.selfcare.module.home_kh.fragment.rewardshop.model.FilterItems;

public class SelectedFilter {
    private int positionSelected;
    int type;
    private FilterItems filterItems;

    public SelectedFilter(int positionSelected, int type, FilterItems filterItems) {
        this.positionSelected = positionSelected;
        this.type = type;
        this.filterItems = filterItems;
    }

    public int getPositionSelected() {
        return positionSelected;
    }

    public void setPositionSelected(int positionSelected) {
        this.positionSelected = positionSelected;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public FilterItems getFilterItems() {
        return filterItems;
    }

    public void setFilterItems(FilterItems filterItems) {
        this.filterItems = filterItems;
    }
}

package com.metfone.selfcare.module.sc_umoney.complete;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.sc_umoney.main_umoney.TabUmoneyActivity;
import com.metfone.selfcare.module.sc_umoney.network.model.FieldMap;
import com.metfone.selfcare.module.sc_umoney.network.model.FieldMapModel;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

public class CompleteUmoneyFragment extends Fragment {
    TextView tvBackHome;
    TabUmoneyActivity activity;
    FieldMapModel fieldMapModel;
    TextView tvTransactionId, tvAmount, tvFee, tvDes, tvBalance, tvTransactionType;
    String des = "";

    public static CompleteUmoneyFragment newInstance(Bundle args) {
        CompleteUmoneyFragment fragment = new CompleteUmoneyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sc_umoney_congratulation, container, false);
        activity = (TabUmoneyActivity) getActivity();
        initView(view);
        loadData();
        initEvent();
        return view;
    }

    private void loadData() {
        Bundle bundle = getArguments();
        fieldMapModel = (FieldMapModel) bundle.getSerializable(Constants.UMONEY.UMONEY_COMPLETE);
        if (fieldMapModel != null) {
            for (FieldMap model : fieldMapModel.getFieldMap()) {
                if(model.getFieldName().equalsIgnoreCase(Constants.UMONEY.TRANSACTION_TYPE)){
                    tvTransactionType.setText("  "+model.getValue());
                    continue;
                }
                if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.AMOUNT)) {
                    tvAmount.setText("  "+model.getValue() + " LAK");
                    continue;
                }
                if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.TRANSACTION_ID)) {
                    tvTransactionId.setText("  "+model.getValue());
                    continue;
                }
                if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.TRANSACTION_FEE)) {
                    tvFee.setText("  "+model.getValue() + " LAK");
                    continue;
                }
                if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.BALANCE)) {
                    tvBalance.setText("  "+model.getValue() + " LAK");
                }
            }
        }
    }

    private void initView(View view) {
        tvBackHome = view.findViewById(R.id.tv_back_home);
        tvTransactionId = view.findViewById(R.id.tv_transaction_id);
        tvAmount = view.findViewById(R.id.tv_amount);
        tvFee = view.findViewById(R.id.tv_fee);
        tvDes = view.findViewById(R.id.tv_des);
        tvBalance = view.findViewById(R.id.tv_balance);
        tvTransactionType = view.findViewById(R.id.tv_transaction_type);
    }

    private void initEvent() {
        tvBackHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showFragment(SCConstants.UMONEY.TAB_HOME, null, true);
            }
        });

    }
}

package com.metfone.selfcare.module.metfoneplus.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.register_payment.ui.fragment.mp_tab_internet.MPTabInternetFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.EnterAddPhoneNumberActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.fragment.HomePagerFragment;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.module.metfoneplus.activity.MPVeryfiPhoneActivity;
import com.metfone.selfcare.module.metfoneplus.listener.OnClickShowFTTH;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;

import java.util.List;
import java.util.Objects;

import retrofit2.Response;

public class MPRootFragment extends Fragment implements MPAddPhoneMetfoneFragment.OnFragmentInteractionListener, OnClickShowFTTH {
    private static final String TAG = MPRootFragment.class.getSimpleName();

    private FragmentManager mFragmentManager;
    public ApplicationController mApplication;
    public HomeActivity mParentActivity;
    public Resources mRes;
    private CamIdUserBusiness mCamIdUserBusiness;
    private Fragment fragment;

    public MPRootFragment() {
        // Required empty public constructor
    }


    public static MPRootFragment newInstance() {
        MPRootFragment fragment = new MPRootFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static int statusLogin = 0;
    int oldStatusLogin = -1;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mParentActivity = (HomeActivity) context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mApplication.getResources();
        this.mCamIdUserBusiness = mApplication.getCamIdUserBusiness();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onResume() {
        super.onResume();

        if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
            statusLogin = 0;
        } else if (StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getToken())) {
            statusLogin = 1;
        } else if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getPhoneService())) {
            statusLogin = 2;
        } else {
            statusLogin = 3;
        }

        if (oldStatusLogin != statusLogin) {
            oldStatusLogin = statusLogin;
            showMobile();
        }
    }

    public void showTabMobile() {
        fragment = MPTabMobileFragment.newInstance();
        ((MPTabMobileFragment) fragment).setListener(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.root_frame, fragment)
                .commit();
    }

    public void showLogin() {
        fragment = MPTabMobileLoginSignUpFragment.newInstance();
        ((MPTabMobileLoginSignUpFragment) fragment).setListener(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.root_frame, fragment)
                .commit();
    }

    public void showLinkPhone() {
        fragment = MPTabEnterAddPhoneNumberFragment.newInstance();
        ((MPTabEnterAddPhoneNumberFragment) fragment).setListener(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.root_frame, fragment)
                .commit();
    }


    public void showAddPhone() {
        fragment = MPAddPhoneMetfoneFragment.newInstance();
        ((MPAddPhoneMetfoneFragment) fragment).setListener(this, this);
        mFragmentManager.beginTransaction()
                .replace(R.id.root_frame, fragment)
                .commit();
    }

    @Override
    public void showFTTH() {
        mFragmentManager.beginTransaction()
                .replace(R.id.root_frame, MPTabInternetFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void showMobile() {
        setSystemBarTheme(requireActivity(), true);
        if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
            showLogin();
        } else if (StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getToken())) {
            showLinkPhone();
        } else if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getPhoneService())) {
            handleServiceList();
        } else {
            showTabMobile();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public static final void setSystemBarTheme(final Activity pActivity, final boolean pIsDark) {
        // Fetch the current flags.
        final int lFlags = pActivity.getWindow().getDecorView().getSystemUiVisibility();
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        pActivity.getWindow().getDecorView().setSystemUiVisibility(pIsDark ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
                : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
    }

    boolean mIsHaveMetfonePlusEqual1 = false;

    private void handleServiceList() {
        List<Service> services = mCamIdUserBusiness.getServices();

        int sumServiceNumber = 0;
        int userServiceId = -1;
        for (Service s : services) {
            List<PhoneLinked> phoneLinkeds = s.getPhoneLinked();
            for (int i = 0; i < phoneLinkeds.size(); i++) {
                PhoneLinked phoneLinked = phoneLinkeds.get(i);
                sumServiceNumber++;
                userServiceId = phoneLinked.getUserServiceId();
                if (phoneLinked.getMetfonePlus() == 1) {
                    mIsHaveMetfonePlusEqual1 = true;
                    mCamIdUserBusiness.setPhoneService(phoneLinked.getPhoneNumber());
                }
            }
        }

        if (sumServiceNumber > 1 && !mIsHaveMetfonePlusEqual1) {
            if (!mCamIdUserBusiness.isAddOrSelectPhoneCalled()) {
                NavigateActivityHelper.navigateToSelectPhone(mParentActivity);
                mCamIdUserBusiness.setAddOrSelectPhoneCalled(true);
            }
        } else if (sumServiceNumber == 1) {
            updatePhoneToPhoneService(userServiceId);
        } else {
            if (!mCamIdUserBusiness.isAddOrSelectPhoneCalled()) {
                showAddPhone();
            }
        }
    }


    private void updatePhoneToPhoneService(int userServiceId) {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.updateMetfone(null, null, userServiceId, new ApiCallback<AddServiceResponse>() {
            @Override
            public void onResponse(Response<AddServiceResponse> response) {
                if (response.body() != null && response.body().getData() != null) {
                    mCamIdUserBusiness.setForceUpdateMetfone(true);
                    mCamIdUserBusiness.setPhoneService(response.body().getData().getServices());
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
                mCamIdUserBusiness.setPhoneService("");
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mp_root, container, false);
        try {
            HomePagerFragment.self().getBottomNavigationBar().setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mFragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Log.e(TAG, "addOnBackStackChangedListener - onBackStackChanged: ");
            }
        });
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        Log.e(TAG, "setMenuVisibility: " + menuVisible + " forceUpdate = " + getCamIdUserBusiness().isForceUpdateMetfone());
        if (menuVisible && getCamIdUserBusiness().isForceUpdateMetfone()) {
            List<Fragment> fragmentsP = getParentFragmentManager().getFragments();
            for (Fragment fragment : fragmentsP) {
                if (fragment instanceof MPExchangeFragment) {
                    mCamIdUserBusiness.setForceUpdateMetfone(false);
                    ((MPExchangeFragment) fragment).autoLogin(mCamIdUserBusiness.getPhoneService());
                }

                if (fragment instanceof MPTabMobileFragment) {
                    mCamIdUserBusiness.setForceUpdateMetfone(false);
                    ((MPTabMobileFragment) fragment).autoLogin(mCamIdUserBusiness.getPhoneService());
                }
            }
        }
    }

    public CamIdUserBusiness getCamIdUserBusiness() {
        if (mCamIdUserBusiness == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        }
        return mCamIdUserBusiness;
    }

    public void reloadAccount() {
        if (getParentFragmentManager().findFragmentById(R.id.root_frame) instanceof MPTabInternetFragment) {
            ((MPTabInternetFragment) Objects.requireNonNull(getParentFragmentManager().findFragmentById(R.id.root_frame))).getUserInformation(false);
        }
    }

    @Override
    public void skipToHome() {

    }

    private MPVerifyPhoneFragment mMPVerifyPhoneFragment;

    @Override
    public void goToVerifyScreen(String phoneNumber, String codeCheckPhone) {
        Intent intent = new Intent(getActivity(), MPVeryfiPhoneActivity.class);
        intent.putExtra("PHONE_NUMBER", phoneNumber);
        intent.putExtra("CODE_CHECK_PHONE", phoneNumber);
        requireActivity().startActivity(intent);
//        mMPVerifyPhoneFragment = MPVerifyPhoneFragment.newInstance(phoneNumber, codeCheckPhone);
//        mFragmentManager.beginTransaction()
//                .replace(R.id.root_frame, mMPVerifyPhoneFragment)
//                .addToBackStack(null)
//                .commit();
    }
}

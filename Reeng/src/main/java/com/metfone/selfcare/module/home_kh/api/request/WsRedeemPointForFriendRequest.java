package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.module.home_kh.fragment.rewards.DetailType;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsRedeemPointForFriendRequest extends BaseRequest<WsRedeemPointRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends WsRedeemPointRequest.Request {
        @SerializedName("giftId")
        String giftId;
        @SerializedName("pointAmount")
        String pointAmount;
        @SerializedName("sourceIsdn")
        String isdn;
        @SerializedName("destIsdn")
        String desIsdn;
        @SerializedName("datatype")
        String datatype;
        @SerializedName("transferType")
        String transferType;

        public WsRequest(String giftId, String pointAmount, String isdn, String desIsdn, String datatype, String transferType) {
            this.giftId = giftId;
            this.pointAmount = pointAmount;
            this.isdn = isdn;
            this.desIsdn = desIsdn;
            this.datatype = datatype;
            this.transferType = transferType;
        }


        public WsRequest(String isdn, String desIsdn, DetailType type, int point) {
            this.isdn = isdn;
            this.desIsdn = desIsdn;
            this.pointAmount = String.valueOf(point);
            this.transferType = "POINT";
            this.giftId = type.giftId;
            this.datatype = type.dataType;
        }

        public WsRequest(String isdn, String desIsdn, DetailType type, int point, String giftId) {
            this.isdn = isdn;
            this.desIsdn = desIsdn;
            this.pointAmount = String.valueOf(point);
            this.transferType = "POINT";
            this.giftId = giftId;
            this.datatype = type.dataType;
        }
    }
}
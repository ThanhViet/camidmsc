package com.metfone.selfcare.module.movienew.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RewardHolder extends BaseViewHolder {

    RelativeLayout rootLayout;
    AppCompatImageView ivCover;
    CircleImageView ivAvatar;
    AppCompatTextView tvDiscount;
    AppCompatTextView tvDesc;
    AppCompatTextView tvDate;
    Context mContext;

    public RewardHolder(@NonNull View itemView, Context context, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
        mContext = context;
    }

    @Override
    public void initViewHolder(View v) {
        rootLayout = v.findViewById(R.id.layout_root);
        ivCover = v.findViewById(R.id.iv_cover);
        ivAvatar = v.findViewById(R.id.ic_avatar);
        tvDiscount = v.findViewById(R.id.tv_discount);
        tvDesc = v.findViewById(R.id.tv_desc);
        tvDate = v.findViewById(R.id.tv_date);
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        if(obj.size()>0){
            SearchRewardResponse.Result.ItemReward reward = (SearchRewardResponse.Result.ItemReward) obj.get(pos);
            if (reward.getImageList() != null && reward.getImageList().size() > 0) {
                Picasso.with(context).load(reward.
                        getImageList().get(0)).into(ivCover);
            } else {
                Picasso.with(context).load(R.drawable.df_image_home_poster).into(ivCover);
            }
            tvDesc.setText(mContext.getString(R.string.kh_free));
            Picasso.with(mContext).load(reward.getImage()).into(ivAvatar);
            tvDiscount.setText(reward.getDiscountRate() + " " + mContext.getString(R.string.kh_dicount));
            tvDate.setText(DateConvert.formatDateTime(mContext, reward.getExpireDate()));
        }
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null)
            itemViewClickListener.onItemViewClickListener(pos, mData);
    }
}

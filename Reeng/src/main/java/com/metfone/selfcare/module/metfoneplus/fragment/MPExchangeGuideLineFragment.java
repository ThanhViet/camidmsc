package com.metfone.selfcare.module.metfoneplus.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.fragment.HomePagerFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.metfoneplus.CircleOverlayView;
import com.metfone.selfcare.module.metfoneplus.HalfCircleOverlayView;
import com.metfone.selfcare.module.metfoneplus.activity.ExchangeGuidelineActivity;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.ui.CusScrollViewDisableScroll;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import me.itangqi.waveloadingview.WaveLoadingView;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_FIRST_TIME_OPEN_TAB_METFONE;

//Just copy from MPExchangeFragment.java and custom it
public class MPExchangeGuideLineFragment extends MPBaseFragment implements View.OnDragListener {
    public static final String TAG = "MPExchangeFragment";
    private static final String DEFAULT_EXCHANGE_BALANCE = "250";
    private static final String DEFAULT_BASIC_BALANCE = "$5";
    private static final int BALANCE_EXCHANGE = 1001;
    private static final int BALANCE_DATA = 1002;
    private static final int BALANCE_BASIC = 1003;
    private static final int TIME_DURATION_WAVE_LOADING_ANIMATION = 4000;
    private CusScrollViewDisableScroll mScrollView;
    private AppCompatImageView mExchangeLayerBackground;
    private AppCompatImageView mExchangeLayerElephant;
    private AppCompatImageView mBasicBalanceLayer1;
    private AppCompatImageView mBasicBalanceLayer2;
    private AppCompatImageView mBasicBalanceLayer3;
    private AppCompatImageView mBasicBalanceLayer4;
    private AppCompatImageView mLayoutBasicBalanceValue;
    private FrameLayout mLayoutChooseExchangePackage;
    private FrameLayout mLayoutDialogConfirm;
    private LinearLayout mLayoutNotificationExchangeSuccess;
    private CircleOverlayView mCircleOverLayout;
    private HalfCircleOverlayView mHalfCircleOverlayView;
    private RelativeLayout mLayoutExchangePackage1;
    private RelativeLayout mLayoutExchangePackage2;
    private RelativeLayout mLayoutExchangePackage4;
    private RelativeLayout mLayoutExchangePackage5;
    private RelativeLayout mLayoutExchangePackage8;
    private AppCompatTextView mTxtAddBalanceValue;
    private AppCompatTextView mTxtExchangeValue;
    private AppCompatTextView mTxtBasicBalanceValue;
    private AppCompatTextView mTxtBalanceType;
    private AppCompatTextView mDialogOriginalValue;
    private AppCompatTextView mDialogExchangeValue;
    private AppCompatTextView mNotifyOriginalValue;
    private AppCompatTextView mNotifyExchangeValue;
    private WaveLoadingView mWaveLoadingView;
    private RelativeLayout mLayoutChooseExchangePackageDropEntered;
    private RelativeLayout mLayoutBasicBalanceAnimation;
    private RelativeLayout mLayoutBasicBalanceOnDragEntered;
    private AppCompatImageView mImageExchangeValue;
    private ConstraintLayout mLayoutMoreServices;
    private RelativeLayout rltRootLayout;
    private LottieAnimationView swipeAnimation, touchAnimation;
    private RelativeLayout mLayoutTypeBalanceStroke;
    private RelativeLayout mLayoutTypeBalanceBackground;
    private LinearLayout mLayoutBasicExchange;
    private LottieAnimationView mLottieFountain;
    private LottieAnimationView mLottieExchangePackage1;
    private LottieAnimationView mLottieExchangePackage2;
    private LottieAnimationView mLottieExchangePackage4;
    private LottieAnimationView mLottieExchangePackage5;
    private LottieAnimationView mLottieExchangePackage8;
    private LottieAnimationView mLottieFountainLeft;
    private LottieAnimationView mLottieFountainRight;

    private TextView mTvSkip;
    private ImageView mImgArrow;
    private TextView mTvMoreService;
    private TextView mTvTutor;
    private TextView mTvUDidIt;

    private AnimatorSet mBasicBalanceAnimatorSet;
    private Handler mHandler;
    private Activity mParentActivity;
    private String mOriginalValueStr = "";
    private String mExchangeValueWithValidityStr = "";
    private int mExchangeValueStr = 0;
    private String mServiceCode;
    private int mOriginalMilestones = 0;
    private int mNewMilestones = 0;
    private boolean mIsRegisterExchange = false;
    private boolean mIsBasicBalanceClick = true;
    private boolean mIsLayoutBasicBalanceAnimationShowing = false;
    private int mCurrentBalanceType = -1;
    private CountDownTimer count;

    /**
     * The Exchange value get from api wsAccountInfo
     */
    private double mExchangeValue = 250.0;
    /**
     * The Basic value get from api wsAccountInfo
     */
    private double mBasicValue = 5;
    /**
     * The Data value get from api wsAccountInfo
     */
    private String mDataValue;

    /**
     * Color Stroke of LayoutTypeBalance
     */
    private int mInitialStrokeColor;
    /**
     * Color Background of LayoutTypeBalance
     */
    private int mInitialBackgroundColor;
    private static MPExchangeGuideLineFragment fragment;
    private int mStartId = -1;
    private View.OnTouchListener mLayoutBasicBalanceAnimationTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());
                ClipData dragData = new ClipData(
                        (CharSequence) view.getTag(),
                        new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN},
                        item);

                View.DragShadowBuilder builder = new View.DragShadowBuilder(view);
                view.startDrag(dragData, builder, view, 0);
                view.setVisibility(View.INVISIBLE);
                return true;
            }
            return false;
        }
    };
    /**
     * Callback for MotionLayout transition
     */

    private Runnable mResetLayoutChooseExchangeRunnable = new Runnable() {
        @Override
        public void run() {
            if (mLayoutDialogConfirm.getVisibility() == View.VISIBLE) {
                return;
            }
            resetLayoutChooseExchangePackage();
        }
    };

    /**
     * Callback for MoreServices bottom sheet
     */
    public static MPExchangeGuideLineFragment self() {
        if(fragment != null){
            return fragment;
        }
        return  MPExchangeGuideLineFragment.newInstance();
    }

    public static MPExchangeGuideLineFragment newInstance() {
        fragment = new MPExchangeGuideLineFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mParentActivity = (Activity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mp_exchange_guide_line;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initAllView(view);
        Utilities.adaptViewForInserts(mScrollView);
        initLayoutDialogConfirm();
        rltRootLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_transparent));
        mHandler = new Handler();
        mScrollView.setEnableScrolling(false);
        mWaveLoadingView.setProgressValue(30);
        handleRippleBackground();

        mLayoutBasicBalanceAnimation.setOnTouchListener(mLayoutBasicBalanceAnimationTouchListener);
        mLayoutChooseExchangePackageDropEntered.setOnDragListener(this);
        mLayoutBasicBalanceOnDragEntered.setOnDragListener(this);

        mInitialStrokeColor = getResources().getColor(R.color.m_p_layout_type_balance_stroke_150);
        mInitialBackgroundColor = getResources().getColor(R.color.m_p_layout_type_balance_background_150);
    }

    private void initAllView(View view) {
        rltRootLayout = view.findViewById(R.id.rlt_root_layout);
        mScrollView = view.findViewById(R.id.scroll_view);
        mExchangeLayerBackground = view.findViewById(R.id.img_exchange_layer_1_background);
        mExchangeLayerElephant = view.findViewById(R.id.img_exchange_layer_2_elephant);
        mBasicBalanceLayer1 = view.findViewById(R.id.img_bg_basic_balance_layer_1);
        mBasicBalanceLayer2 = view.findViewById(R.id.img_bg_basic_balance_layer_2);
        mBasicBalanceLayer3 = view.findViewById(R.id.img_bg_basic_balance_layer_3);
        mBasicBalanceLayer4 = view.findViewById(R.id.img_bg_basic_balance_layer_4);
        mLayoutBasicBalanceValue = view.findViewById(R.id.layout_basic_balance_value);
        mLayoutChooseExchangePackage = view.findViewById(R.id.layout_choose_exchange_package);
        mLayoutDialogConfirm = ExchangeGuidelineActivity.self().getFlDialog();
        mLayoutNotificationExchangeSuccess = view.findViewById(R.id.layout_notification_exchange_success);
        mLayoutExchangePackage1 = view.findViewById(R.id.layout_exchange_package_1);
        mLayoutExchangePackage2 = view.findViewById(R.id.layout_exchange_package_2);
        mLayoutExchangePackage4 = view.findViewById(R.id.layout_exchange_package_4);
        mLayoutExchangePackage5 = view.findViewById(R.id.layout_exchange_package_5);
        mLayoutExchangePackage8 = view.findViewById(R.id.layout_exchange_package_8);
        mTxtAddBalanceValue = view.findViewById(R.id.txt_add_balance_value);
        mTxtExchangeValue = view.findViewById(R.id.txt_exchange_value);
        mTxtBasicBalanceValue = view.findViewById(R.id.txt_basic_balance_value);
        mTxtBalanceType = view.findViewById(R.id.txt_balance_type);
        mDialogOriginalValue = mLayoutDialogConfirm.findViewById(R.id.txt_dialog_original_value);
        mDialogExchangeValue = mLayoutDialogConfirm.findViewById(R.id.txt_dialog_exchange_value);
        mNotifyOriginalValue = view.findViewById(R.id.txt_notify_original_value);
        mNotifyExchangeValue = view.findViewById(R.id.txt_notify_exchange_value);
        mWaveLoadingView = view.findViewById(R.id.wave_loading_view);
        mLayoutChooseExchangePackageDropEntered = view.findViewById(R.id.layout_choose_exchange_package_drop_entered);
        mLayoutBasicBalanceAnimation = view.findViewById(R.id.layout_basic_balance_animation);
        mLayoutBasicBalanceOnDragEntered = view.findViewById(R.id.layout_basic_balance_on_drag_entered);
        mImageExchangeValue = view.findViewById(R.id.img_exchange_value);
        mLayoutMoreServices = view.findViewById(R.id.layout_bottom_sheet_more_service);
        mCircleOverLayout = ExchangeGuidelineActivity.self().getCircleOverlayView();
        mHalfCircleOverlayView = ExchangeGuidelineActivity.self().getHalfCircleOverlayView();
        mLayoutTypeBalanceStroke = view.findViewById(R.id.layout_type_balance_stroke);
        ExchangeGuidelineActivity.self().setVisibilityCtlAnimation(View.VISIBLE);
        mLayoutTypeBalanceBackground = view.findViewById(R.id.layout_type_balance_background);
        mLayoutBasicExchange = view.findViewById(R.id.layout_basic_exchange);
        mLottieFountain = view.findViewById(R.id.lottie_fountain);
        mTvMoreService = ExchangeGuidelineActivity.self().getTvMoreService();
        mLottieExchangePackage1 = view.findViewById(R.id.lottie_exchange_package_1);
        mLottieExchangePackage2 = view.findViewById(R.id.lottie_exchange_package_2);
        mLottieExchangePackage4 = view.findViewById(R.id.lottie_exchange_package_4);
        mLottieExchangePackage5 = view.findViewById(R.id.lottie_exchange_package_5);
        mLottieExchangePackage8 = view.findViewById(R.id.lottie_exchange_package_8);
        mLottieFountainLeft = view.findViewById(R.id.lottie_fountain_left);
        mLottieFountainRight = view.findViewById(R.id.lottie_fountain_right);
        mTvSkip = ExchangeGuidelineActivity.self().getTvSkip();
        mImgArrow = ExchangeGuidelineActivity.self().getImgArrow();
        mTvTutor = ExchangeGuidelineActivity.self().getTvTutor();
        mTvUDidIt = ExchangeGuidelineActivity.self().getTvUDidIt();
        swipeAnimation = view.findViewById(R.id.swipe_animation);
        touchAnimation = view.findViewById(R.id.touch_aimation);
        mLayoutExchangePackage1.setOnClickListener(this::onServicePackageClick);
        mLayoutExchangePackage2.setOnClickListener(this::onServicePackageClick);
        mLayoutExchangePackage4.setOnClickListener(this::onServicePackageClick);
        mLayoutExchangePackage5.setOnClickListener(this::onServicePackageClick);
        mLayoutExchangePackage8.setOnClickListener(this::onServicePackageClick);
        //TODO detail fragment
//        mLayoutTypeBalanceStroke.setOnClickListener((v) -> gotoMPDetailFragment(true));
//        mMoreServiceMotionLayout.addTransitionListener(transitionListener);
        mLayoutTypeBalanceStroke.setVisibility(View.INVISIBLE);
        mLayoutMoreServices.setClickable(false);
        mTxtBasicBalanceValue.setText(DEFAULT_BASIC_BALANCE);

        mTvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DynamicSharePref.getInstance().put(PREF_FIRST_TIME_OPEN_TAB_METFONE, false);
                mParentActivity.finish();
            }
        });

        swipeAnimation.setVisibility(View.VISIBLE);
        swipeAnimation.playAnimation();
        mTvSkip.setVisibility(View.VISIBLE);
        mImgArrow.setVisibility(View.VISIBLE);
        mTvMoreService.setVisibility(View.VISIBLE);
        mCircleOverLayout.setVisibility(View.VISIBLE);
        mCircleOverLayout.startAnimation();
        mTvTutor.setVisibility(View.VISIBLE);
    }

    @Override
    public void popBackStackFragment() {
        return;
    }

    /**
     * Initialize dialog confirm "Register this ....?"
     */


    private void initLayoutDialogConfirm() {
        mLayoutDialogConfirm.findViewById(R.id.btn_dialog_confirm_exchange_no).setOnClickListener(view1 -> {
            mLayoutDialogConfirm.setVisibility(View.GONE);
            mLayoutMoreServices.setVisibility(View.VISIBLE);
            mHalfCircleOverlayView.setVisibility(View.GONE);
            mCircleOverLayout.setVisibility(View.VISIBLE);
            mCircleOverLayout.startAnimation();
            mTvMoreService.setVisibility(View.VISIBLE);
            mImgArrow.setVisibility(View.VISIBLE);
            mTvSkip.setVisibility(View.VISIBLE);
            mLayoutChooseExchangePackage.setVisibility(View.VISIBLE);
            mTvTutor.setVisibility(View.VISIBLE);
            mTvTutor.setText(getActivity().getString(R.string.drag_circle_to_big_circle));
        });
        mLayoutDialogConfirm.findViewById(R.id.btn_dialog_confirm_exchange_confirm).setOnClickListener(view12 -> {
            getWSDoActionService(mServiceCode);
            handleProgressWaveLoading(250);
            mLayoutDialogConfirm.setVisibility(View.GONE);
            mHalfCircleOverlayView.setVisibility(View.GONE);
            mCircleOverLayout.setVisibility(View.VISIBLE);
            mCircleOverLayout.startAnimation();
            mTvSkip.setVisibility(View.VISIBLE);
            mTvUDidIt.setVisibility(View.VISIBLE);
            mLayoutChooseExchangePackage.setVisibility(View.GONE);
            count = new CountDownTimer(3000, 3000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    mTvUDidIt.setVisibility(View.GONE);
                    mTvTutor.setVisibility(View.VISIBLE);
                    mTvTutor.setText(getActivity().getString(R.string.touch_to_view_account_detail));
                    touchAnimation.setVisibility(View.VISIBLE);
                    touchAnimation.playAnimation();
                }
            }.start();
            mLayoutTypeBalanceStroke.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    gotoMPDetailFragment(true);
                    MPDetailsFragment f = MPDetailsFragment.newInstance();
                    f.setIsShowAnimation(true);
                    ExchangeGuidelineActivity.self().getFlDialog().setVisibility(View.GONE);
                    ExchangeGuidelineActivity.self().getCircleOverlayView().setVisibility(View.GONE);
                    ExchangeGuidelineActivity.self().getHalfCircleOverlayView().setVisibility(View.GONE);
                    ExchangeGuidelineActivity.self().getTvSkip().setVisibility(View.GONE);
                    ExchangeGuidelineActivity.self().getImgArrow().setVisibility(View.GONE);
                    ExchangeGuidelineActivity.self().getTvMoreService().setVisibility(View.GONE);
                    ExchangeGuidelineActivity.self().getTvTutor().setVisibility(View.GONE);
                    getParentFragmentManager().beginTransaction()
                            .replace(R.id.root_frame, f)
                            .commit();
                    mTvUDidIt.setVisibility(View.GONE);
                    count.cancel();
                }
            });


        });
    }

    void onServicePackageClick(View view) {
        resetStateAnimationOfExchangePackage();
        mCircleOverLayout.setVisibility(View.GONE);
        mTvTutor.setVisibility(View.GONE);
        mTvUDidIt.setVisibility(View.GONE);
        mTvMoreService.setVisibility(View.GONE);
        switch (view.getId()) {
            case R.id.layout_exchange_package_1:
                mExchangeValueStr = 150;
                mServiceCode = Constants.WSCODE.SERVICE_PACKAGE_1;
                mOriginalValueStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_1);
                mExchangeValueWithValidityStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_1_validity);
                mLottieExchangePackage1.playAnimation();
                break;
            case R.id.layout_exchange_package_2:
                mExchangeValueStr = 300;
                mServiceCode = Constants.WSCODE.SERVICE_PACKAGE_2;
                mOriginalValueStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_2);
                mExchangeValueWithValidityStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_2_validity);
                mLottieExchangePackage2.playAnimation();
                break;
            case R.id.layout_exchange_package_4:
                mExchangeValueStr = 650;
                mServiceCode = Constants.WSCODE.SERVICE_PACKAGE_4;
                mOriginalValueStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_4);
                mExchangeValueWithValidityStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_4_validity);
                mLottieExchangePackage4.playAnimation();
                break;
            case R.id.layout_exchange_package_5:
                mExchangeValueStr = 1000;
                mServiceCode = Constants.WSCODE.SERVICE_PACKAGE_5;
                mOriginalValueStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_5);
                mExchangeValueWithValidityStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_5_validity);
                mLottieExchangePackage5.playAnimation();
                break;
            case R.id.layout_exchange_package_8:
                mExchangeValueStr = 4000;
                mServiceCode = Constants.WSCODE.SERVICE_PACKAGE_8;
                mOriginalValueStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_8);
                mExchangeValueWithValidityStr = getActivity().getString(R.string.m_p_dialog_confirm_exchange_package_8_validity);
                mLottieExchangePackage8.playAnimation();
                break;
        }
        handleDisplayDialogConfirmExchange();
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        switch (dragEvent.getAction()) {
            case DragEvent.ACTION_DRAG_ENDED:
                View layoutBasicBalance = (View) dragEvent.getLocalState();
                layoutBasicBalance.setVisibility(View.VISIBLE);
                if (view.getId() == R.id.layout_choose_exchange_package_drop_entered) {
                    return false;
                }
                return true;
            case DragEvent.ACTION_DRAG_ENTERED:
                handleShowLayoutChooseExchangePackage();
                mTvMoreService.setVisibility(View.GONE);
                mImgArrow.setVisibility(View.GONE);
                mTvTutor.setText(getActivity().getString(R.string.select_one_to_exchange_money));
                swipeAnimation.setVisibility(View.GONE);
                touchAnimation.setVisibility(View.GONE);
                mLayoutDialogConfirm.setVisibility(View.GONE);
                if (view.getId() == R.id.layout_choose_exchange_package_drop_entered
                        && !mIsLayoutBasicBalanceAnimationShowing) {
                    if (mCurrentBalanceType == BALANCE_EXCHANGE) {
                        handleShowLayoutChooseExchangePackage();
                    } else if (mCurrentBalanceType == BALANCE_DATA) {
//                        gotoBuyServicesScreen();
                    }
                    mIsLayoutBasicBalanceAnimationShowing = true;
                }
                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                mIsBasicBalanceClick = false;
                return true;
            case DragEvent.ACTION_DRAG_STARTED:
                mIsBasicBalanceClick = true;
                mIsLayoutBasicBalanceAnimationShowing = false;
                return true;
            case DragEvent.ACTION_DROP:
            case DragEvent.ACTION_DRAG_LOCATION:
                return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsBasicBalanceClick = true;
        mIsLayoutBasicBalanceAnimationShowing = false;

        String phoneService = getCamIdUserBusiness().getPhoneService();
        if (!"".equals(phoneService)) {
            if ("".equals(getCamIdUserBusiness().getMetfoneSessionId())) {
                autoLogin(phoneService);
            } else {
                getWSAccountInfo();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause: ");
        mIsLayoutBasicBalanceAnimationShowing = false;
        mHandler.removeCallbacks(mResetLayoutChooseExchangeRunnable);
    }

    /**
     * Reset state animation of all exchange package
     */
    private void resetStateAnimationOfExchangePackage() {
        mLottieExchangePackage1.setProgress(0);
        mLottieExchangePackage2.setProgress(0);
        mLottieExchangePackage4.setProgress(0);
        mLottieExchangePackage5.setProgress(0);
        mLottieExchangePackage8.setProgress(0);
        mLottieExchangePackage1.cancelAnimation();
        mLottieExchangePackage2.cancelAnimation();
        mLottieExchangePackage4.cancelAnimation();
        mLottieExchangePackage5.cancelAnimation();
        mLottieExchangePackage8.cancelAnimation();
    }

    /**
     * Handle display dialog confirm "Register this ....?"
     */
    private void handleDisplayDialogConfirmExchange() {
        mLayoutMoreServices.setVisibility(View.INVISIBLE);
        mDialogOriginalValue.setText(mOriginalValueStr);
        mDialogExchangeValue.setText(mExchangeValueWithValidityStr);

        // show dialog confirm
        mLayoutDialogConfirm.setVisibility(View.VISIBLE);
        mTvMoreService.setVisibility(View.GONE);
        mImgArrow.setVisibility(View.GONE);
        mHalfCircleOverlayView.setVisibility(View.VISIBLE);
        mHalfCircleOverlayView.startAnimation();

    }

    /**
     * See more Android Ripple Background
     */
    private void handleRippleBackground() {
        List<AppCompatImageView> basicBalanceLayerList = new ArrayList<>();
        basicBalanceLayerList.add(mBasicBalanceLayer1);
        basicBalanceLayerList.add(mBasicBalanceLayer2);
        basicBalanceLayerList.add(mBasicBalanceLayer3);
        basicBalanceLayerList.add(mBasicBalanceLayer4);

        mBasicBalanceAnimatorSet = new AnimatorSet();
        mBasicBalanceAnimatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();

        int rippleDurationTime = 6000;
        int rippleDelay = rippleDurationTime / basicBalanceLayerList.size(); // 6000: duration time

        for (int i = 0; i < basicBalanceLayerList.size(); i++) {
            ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(basicBalanceLayerList.get(i),
                    PropertyValuesHolder.ofFloat("ScaleX", 1.0f, 2.0f),
                    PropertyValuesHolder.ofFloat("ScaleY", 1.0f, 2.0f),
                    PropertyValuesHolder.ofFloat("Alpha", 1.0f, 0f));
            animator.setRepeatCount(ObjectAnimator.INFINITE);
            animator.setRepeatMode(ObjectAnimator.RESTART);
            animator.setStartDelay(i * rippleDelay);
            animator.setDuration(rippleDurationTime);
            animatorList.add(animator);
        }

        mBasicBalanceAnimatorSet.playTogether(animatorList);
        mBasicBalanceAnimatorSet.start();
    }

    /**
     * - Tổng công tơ mét: Tổng giá trị của tát cả các gói đổi tiền mà khách hang có một cách tương đương
     * <p>
     * - Cách tính tổng công tơ mét của Metfone dựa trên số Exchanged Money hiện tại
     * - Money hiện tại:
     * + Exchanged money < 150$: Tổng công tơ mét = 150
     * + $150 < Exchanged money <= $300: Tổng công tơ mét = 300
     * + $300 < Exchanged Money <= $1000: Tổng công tơ mét = 1000
     * + $1000 < Exchanged money: Tổng cơ tơ mét = 5000
     * (Nếu tỷ lệ tiền trên Tổng công tơ mét lớn hơn 100%, đồ họa hiển thị full công tơ mét cho đến khi dưới 100%)
     *
     * @param value exchange money
     */
    private void handleProgressWaveLoading(int value) {
        int range;
        int progress = value;

        if (progress < 150) {
            range = 150 - 0;
            progress = progress * 100 / range;
        } else if (progress > 150 && progress <= 300) {
            range = 300 - 150;
            progress = progress - 150;
            progress = progress * 100 / range;
        } else if (progress > 300 && progress <= 600) {
            range = 600 - 300;
            progress = progress - 300;
            progress = progress * 100 / range;
        } else if (progress > 600 && progress <= 1000) {
            range = 1000 - 600;
            progress = progress - 600;
            progress = progress * 100 / range;
        } else if (progress > 1000 && progress <= 2000) {
            range = 2000 - 1000;
            progress = progress - 1000;
            progress = progress * 100 / range;
        } else {
            progress = 100;
        }

        mWaveLoadingView.setProgressValue(70);

    }

    /**
     * @param exchangeValue      the Exchange value
     * @param isRegisterExchange true when user using exchange money 1, 2, 4, 5, 8$ to Exchange balance
     *                           false other case
     */
    private void handleBackgroundLayoutTypeBalance(int exchangeValue, boolean isRegisterExchange) {
        int finalStrokeColor;
        int finalBackgroundColor;
        int waveLoadingColor;
        Drawable drawableStroke;
        Drawable drawableBackground;
        mNewMilestones = 0;
        finalStrokeColor = getResources().getColor(R.color.m_p_layout_type_balance_stroke_150);
        finalBackgroundColor = getResources().getColor(R.color.m_p_layout_type_balance_background_150);
        waveLoadingColor = ContextCompat.getColor(mApplication, R.color.m_p_layout_type_balance_wave_150);
        drawableStroke = ContextCompat.getDrawable(mParentActivity, R.drawable.bg_layout_type_balance_stroke_150);
        drawableBackground = ContextCompat.getDrawable(mParentActivity, R.drawable.bg_layout_type_balance_background_150);
        if (isRegisterExchange) {
            mLottieFountain.setRepeatCount(LottieDrawable.INFINITE);
            mLottieFountain.playAnimation();
            handleLayoutNotificationExchange(Constants.WSCODE.WS_DO_ACTION_SERVICE_RESULT_SUCCESS);

            GradientDrawable drawableStrokeOfLayout = (GradientDrawable) mLayoutTypeBalanceStroke.getBackground();
            GradientDrawable drawableBackgroundOfLayout = (GradientDrawable) mLayoutTypeBalanceBackground.getBackground();

            if (mOriginalMilestones != mNewMilestones) {
                mOriginalMilestones = mNewMilestones;
                Utilities.changeWaveLoadingColor(mWaveLoadingView, TIME_DURATION_WAVE_LOADING_ANIMATION, mWaveLoadingView.getWaveColor(), getResources().getColor(android.R.color.transparent))
                        .addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mWaveLoadingView.setWaveColor(ContextCompat.getColor(mApplication, android.R.color.transparent));
                            }
                        });
                Utilities.changeGradientDrawableColor(drawableStrokeOfLayout, TIME_DURATION_WAVE_LOADING_ANIMATION, mInitialStrokeColor, finalStrokeColor);
                Utilities.changeGradientDrawableColor(drawableBackgroundOfLayout, TIME_DURATION_WAVE_LOADING_ANIMATION, mInitialBackgroundColor, finalBackgroundColor);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mWaveLoadingView.setWaveColor(waveLoadingColor);
                        mLayoutTypeBalanceStroke.setVisibility(View.VISIBLE);
                        handleProgressWaveLoading(exchangeValue);
                    }
                }, TIME_DURATION_WAVE_LOADING_ANIMATION + 1000);
            } else {
                mLayoutTypeBalanceStroke.setVisibility(View.VISIBLE);
                handleProgressWaveLoading(exchangeValue);
            }

            mIsRegisterExchange = false;
        } else {
            mTxtExchangeValue.setText(DEFAULT_EXCHANGE_BALANCE);
            mLayoutTypeBalanceBackground.setBackground(drawableBackground);
            mLayoutTypeBalanceStroke.setVisibility(View.VISIBLE);
            mLayoutTypeBalanceStroke.setBackground(drawableStroke);
            mWaveLoadingView.setWaveColor(waveLoadingColor);
            mLayoutBasicExchange.setVisibility(View.VISIBLE);
//            handleProgressWaveLoading(exchangeValue);
            mOriginalMilestones = mNewMilestones;
        }

        mInitialStrokeColor = finalStrokeColor;
        mInitialBackgroundColor = finalBackgroundColor;
    }

    private void handleShowLayoutBasicExchangeDefault() {
        mLayoutTypeBalanceBackground.setBackground(ContextCompat.getDrawable(mParentActivity, R.drawable.bg_layout_type_balance_background_150));
        mLayoutTypeBalanceStroke.setBackground(ContextCompat.getDrawable(mParentActivity, R.drawable.bg_layout_type_balance_stroke_150));
        mWaveLoadingView.setWaveColor(ContextCompat.getColor(mApplication, R.color.m_p_layout_type_balance_wave_150));
        mTxtExchangeValue.setText(DEFAULT_EXCHANGE_BALANCE);
        mTxtBasicBalanceValue.setText(DEFAULT_BASIC_BALANCE);
        mLayoutBasicExchange.setVisibility(View.VISIBLE);
        mLayoutTypeBalanceStroke.setVisibility(View.VISIBLE);
    }

    private void handleShowLayoutChooseExchangePackage() {
        if (mLayoutChooseExchangePackage.getVisibility() == View.VISIBLE) {
            return;
        }
//        setVisibleMoreService(false);
        mTvTutor.setText(getActivity().getString(R.string.select_one_to_exchange_money));
        mTvTutor.setVisibility(View.VISIBLE);
        swipeAnimation.setVisibility(View.GONE);
        handleShowLayoutFountainLeftRight(true);
        mLayoutChooseExchangePackage.setVisibility(View.VISIBLE);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();

        ObjectAnimator scaleAnimatorCoin1 = ObjectAnimator.ofPropertyValuesHolder(mLayoutExchangePackage1,
                PropertyValuesHolder.ofFloat("ScaleX", 0f, 1.0f),
                PropertyValuesHolder.ofFloat("ScaleY", 0f, 1.0f));
        scaleAnimatorCoin1.setStartDelay(0);
        scaleAnimatorCoin1.setDuration(1000);
        animatorList.add(scaleAnimatorCoin1);
        scaleAnimatorCoin1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                redrawExchangeValue(mLayoutExchangePackage1);
            }
        });

        ObjectAnimator scaleAnimatorCoin4 = ObjectAnimator.ofPropertyValuesHolder(mLayoutExchangePackage4,
                PropertyValuesHolder.ofFloat("ScaleX", 0f, 1.0f),
                PropertyValuesHolder.ofFloat("ScaleY", 0f, 1.0f));
        scaleAnimatorCoin4.setStartDelay(0);
        scaleAnimatorCoin4.setDuration(1000);
        animatorList.add(scaleAnimatorCoin4);
        scaleAnimatorCoin4.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                redrawExchangeValue(mLayoutExchangePackage1);
            }
        });

        ObjectAnimator scaleAnimatorCoin2 = ObjectAnimator.ofPropertyValuesHolder(mLayoutExchangePackage2,
                PropertyValuesHolder.ofFloat("ScaleX", 0f, 1.0f),
                PropertyValuesHolder.ofFloat("ScaleY", 0f, 1.0f));
        scaleAnimatorCoin2.setStartDelay(500);
        scaleAnimatorCoin2.setDuration(1000);
        animatorList.add(scaleAnimatorCoin2);
        scaleAnimatorCoin2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                redrawExchangeValue(mLayoutExchangePackage1);
            }
        });

        ObjectAnimator scaleAnimatorCoin5 = ObjectAnimator.ofPropertyValuesHolder(mLayoutExchangePackage5,
                PropertyValuesHolder.ofFloat("ScaleX", 0f, 1.0f),
                PropertyValuesHolder.ofFloat("ScaleY", 0f, 1.0f));
        scaleAnimatorCoin5.setStartDelay(500);
        scaleAnimatorCoin5.setDuration(1000);
        animatorList.add(scaleAnimatorCoin5);
        scaleAnimatorCoin5.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                redrawExchangeValue(mLayoutExchangePackage1);
            }
        });

        ObjectAnimator scaleAnimatorCoin8 = ObjectAnimator.ofPropertyValuesHolder(mLayoutExchangePackage8,
                PropertyValuesHolder.ofFloat("ScaleX", 0f, 1.0f),
                PropertyValuesHolder.ofFloat("ScaleY", 0f, 1.0f));
        scaleAnimatorCoin8.setStartDelay(1000);
        scaleAnimatorCoin8.setDuration(1000);
        animatorList.add(scaleAnimatorCoin8);
        scaleAnimatorCoin8.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                redrawExchangeValue(mLayoutExchangePackage1);
            }
        });

        animatorSet.playTogether(animatorList);
        animatorSet.start();

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                //k can focus vao item 1
                //onServicePackageClick(mLayoutExchangePackage1);
            }
        });
//        mHandler.postDelayed(mResetLayoutChooseExchangeRunnable, TIME_CLOSE_LAYOUT_CHOOSE_EXCHANGE_DELAY);
    }

    private void handleShowLayoutFountainLeftRight(boolean isShow) {
        if (isShow) {
            mLottieFountainLeft.setVisibility(View.VISIBLE);
            mLottieFountainRight.setVisibility(View.VISIBLE);
            mLottieFountainLeft.playAnimation();
            mLottieFountainRight.playAnimation();
        } else {
            mLottieFountainLeft.setVisibility(View.INVISIBLE);
            mLottieFountainRight.setVisibility(View.INVISIBLE);
            mLottieFountainLeft.cancelAnimation();
            mLottieFountainRight.cancelAnimation();
        }
    }

    private void handleLayoutNotificationExchange(String errorCode) {
        if (errorCode.equals(Constants.WSCODE.WS_DO_ACTION_SERVICE_RESULT_SUCCESS)) {
            mNotifyOriginalValue.setText(mOriginalValueStr);
            mNotifyExchangeValue.setText(String.format(getActivity().getString(R.string.m_p_exchange_notification_exchange_success_3), String.valueOf(mExchangeValueStr)));
            mLayoutNotificationExchangeSuccess.setVisibility(View.VISIBLE);

            mTxtAddBalanceValue.setText(String.format(getActivity().getString(R.string.m_p_add_balance_value), String.valueOf(mExchangeValueStr)));
            mTxtAddBalanceValue.setVisibility(View.VISIBLE);
        } else if (errorCode.equals(Constants.WSCODE.WS_DO_ACTION_SERVICE_RESULT_NOT_ENOUGH_MONEY)) {

        } else {

        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTxtExchangeValue.setText(String.valueOf(mExchangeValue + mExchangeValueStr));
                mTxtAddBalanceValue.setVisibility(View.GONE);
                mLayoutMoreServices.setVisibility(View.VISIBLE);
                mLottieFountain.setRepeatCount(0);

                mLottieFountain.addAnimatorListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLottieFountain.cancelAnimation();
                    }
                });
            }
        }, TIME_DURATION_WAVE_LOADING_ANIMATION + 1000);
    }

    private void resetLayoutChooseExchangePackage() {
//        mIsShowLayoutBasicBalanceAnimation = false;
        handleShowLayoutFountainLeftRight(false);
        mLayoutChooseExchangePackage.setVisibility(View.INVISIBLE);
        resetStateAnimationOfExchangePackage();

        mLayoutExchangePackage1.setScaleX(0F);
        mLayoutExchangePackage1.setScaleY(0F);
        mLayoutExchangePackage2.setScaleX(0F);
        mLayoutExchangePackage2.setScaleY(0F);
        mLayoutExchangePackage4.setScaleX(0F);
        mLayoutExchangePackage4.setScaleY(0F);
        mLayoutExchangePackage5.setScaleX(0F);
        mLayoutExchangePackage5.setScaleY(0F);
        mLayoutExchangePackage8.setScaleX(0F);
        mLayoutExchangePackage8.setScaleY(0F);

        mIsLayoutBasicBalanceAnimationShowing = false;
        mHandler.removeCallbacks(mResetLayoutChooseExchangeRunnable);
    }

    /**
     * Trong trường hợp TK đổi tiền còn 33$, tài khoản data còn 33MB, tài khoản gốc =0$ chuyển sang màu đỏ, và các nút có nhấp nháy hiển thị nội dung như sau để thông báo cho KH đổi tiền/mua data/top-up:
     * + đối với màn exchange: Drag the Basic Balance to the big circle for exchanging money.
     * + đối với màn data: Drag the Basic Balance to the big circle for buying  more data.
     * + đối với màn basic: Tap here to top-up.
     *
     * @param isZero false if TK đổi tiền > 33$, tài khoản data > 33MB, tài khoản gốc > 0$
     *               typeBalance = BALANCE_EXCHANGE || BALANCE_BASIC || BALANCE_DATA
     */
    private void changeUI(boolean isZero, int typeBalance) {
        mCurrentBalanceType = typeBalance;
        // Background
        mExchangeLayerBackground.setSelected(isZero);
        mExchangeLayerElephant.setSelected(isZero);

        // Show value in small circle (Basic balance)

        mLayoutBasicBalanceValue.setImageDrawable(ContextCompat.getDrawable(mParentActivity, R.drawable.bg_exchange_data_basic_balance_value));
        mTxtBasicBalanceValue.setVisibility(View.VISIBLE);
        mTxtBalanceType.setText(R.string.m_p_exchange_exchange);
        mImageExchangeValue.setVisibility(View.VISIBLE);
        mTxtExchangeValue.setText(String.valueOf(mExchangeValue));
        handleBackgroundLayoutTypeBalance((int) mExchangeValue, mIsRegisterExchange);
        // Balance
        mTxtExchangeValue.setText(String.valueOf(mExchangeValue));
        handleBackgroundLayoutTypeBalance((int) mExchangeValue, mIsRegisterExchange);
        // Basic balance
        mBasicBalanceLayer1.setSelected(isZero);
        mBasicBalanceLayer2.setSelected(isZero);
        mBasicBalanceLayer3.setSelected(isZero);
        mBasicBalanceLayer4.setSelected(isZero);
        mLayoutBasicBalanceValue.setSelected(isZero);


        // Exchange: Drag the Basic Balance to the big circle for exchanging money.
        // Data: Drag the Basic Balance to the big circle for buying  more data.
        // Basic: Tap here to top-up.
        if (mCurrentBalanceType == BALANCE_BASIC) {
            mLayoutBasicBalanceAnimation.setOnTouchListener(null);
            mLayoutChooseExchangePackageDropEntered.setOnDragListener(null);
            mLayoutBasicBalanceOnDragEntered.setOnDragListener(this);
//            mLayoutBasicBalanceAnimation.setOnClickListener((v) -> goToTopUpScreen());
        } else {
            mLayoutBasicBalanceAnimation.setOnTouchListener(mLayoutBasicBalanceAnimationTouchListener);
            mLayoutChooseExchangePackageDropEntered.setOnDragListener(this);
            mLayoutBasicBalanceOnDragEntered.setOnDragListener(this);
            mLayoutBasicBalanceAnimation.setOnClickListener(null);
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        Log.e(TAG, "setMenuVisibility: " + menuVisible);
        if (!menuVisible && mIsLayoutBasicBalanceAnimationShowing) {
            resetLayoutChooseExchangePackage();
        }
    }

    /**
     * AutoLogin
     *
     * @param userName phone_number,
     */
    public void autoLogin(String userName) {
        getWSAccountInfo();
        MetfonePlusClient client = new MetfonePlusClient();
        client.autoLogin(userName, new MPApiCallback<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response) {
                if (response.body() != null && response.body().getUsername() != null) {
//                    getWSAccountInfo();
                }
            }

            @Override
            public void onError(Throwable error) {

            }
        });
    }

    public void getWSAccountInfo() {
//        mParentActivity.showLoadingDialog("", R.string.phone_number_invalid);
        changeUI(false, BALANCE_EXCHANGE);
        MetfonePlusClient client = new MetfonePlusClient();

    }

    private void getWSDoActionService(String serviceCode) {
//        mParentActivity.showLoadingDialog(null, R.string.mp_confirm_informatinformationion_successfull);

        mIsRegisterExchange = true;
        // UpdateUI after exchange money
        getWSAccountInfo();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (count != null) {
            count.cancel();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSelectTab(MPExchangeGuideLineFragment o) {
        if (DynamicSharePref.getInstance().get(PREF_FIRST_TIME_OPEN_TAB_METFONE, Boolean.class, true)) {
            swipeAnimation.setVisibility(View.VISIBLE);
            swipeAnimation.playAnimation();
            mTvSkip.setVisibility(View.VISIBLE);
            mImgArrow.setVisibility(View.VISIBLE);
            mTvMoreService.setVisibility(View.VISIBLE);
            mCircleOverLayout.setVisibility(View.VISIBLE);
            mCircleOverLayout.startAnimation();
            mTvTutor.setVisibility(View.VISIBLE);
            EventBus.getDefault().removeStickyEvent(o);
        }

    }

    private void redrawExchangeValue(View v) {
        if (v != null && v instanceof RelativeLayout && ((RelativeLayout) v).getChildCount() > 1) {
            View subView = ((RelativeLayout) v).getChildAt(1);
            if (subView instanceof TextView) {
                subView.invalidate();
            }
        }
    }

}
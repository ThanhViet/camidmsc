package com.metfone.selfcare.module.keeng.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;


public class MediaNewHolder extends BaseViewHolder {
    public TextView tvName, tvSinger, tvListenNo, tvPosition;
    public ImageView image;
    public View btnOption;
    private View viewSocial;

    public MediaNewHolder(View itemView) {
        super(itemView);
        image = (ImageView) itemView.findViewById(R.id.image);
        tvName = (TextView) itemView.findViewById(R.id.title);
        tvSinger = (TextView) itemView.findViewById(R.id.singer);
        tvListenNo = (TextView) itemView.findViewById(R.id.listen_no);
        tvPosition = (TextView) itemView.findViewById(R.id.position);
        btnOption = itemView.findViewById(R.id.button_option);
//        viewSocial = itemView.findViewById(R.id.layout_social);
        if (viewSocial != null)
            viewSocial.setVisibility(View.GONE);
    }
}

package com.metfone.selfcare.module.home_kh.api;

import com.metfone.esport.util.Log;

import java.io.Serializable;

public class WsGiftSpinResponse{
    private String errorCode;
    private Object errorMessage;
    private Result result;

    public String getErrorCode() { return errorCode; }
    public void setErrorCode(String value) { this.errorCode = value; }

    public Object getErrorMessage() { return errorMessage; }
    public void setErrorMessage(Object value) { this.errorMessage = value; }

    public Result getResult() { return result; }
    public void setResult(Result value) { this.result = value; }

    public class Result {
        private Long status;
        private String code;
        private String message;
        private Prize prize;
        private Long countSpin;
        private Long countTotal;
        private Object prizeInfoList;
        private SocialShareInfo socialShareInfo;

        public Long getStatus() { return status; }
        public void setStatus(Long value) { this.status = value; }

        public String getCode() { return code; }
        public void setCode(String value) { this.code = value; }

        public String getMessage() { return message; }
        public void setMessage(String value) { this.message = value; }

        public Prize getPrize() { return prize; }
        public void setPrize(Prize value) { this.prize = value; }

        public Long getCountSpin() { return countSpin; }
        public void setCountSpin(Long value) { this.countSpin = value; }

        public Long getCountTotal() { return countTotal; }
        public void setCountTotal(Long value) { this.countTotal = value; }

        public Object getPrizeInfoList() { return prizeInfoList; }
        public void setPrizeInfoList(Object value) { this.prizeInfoList = value; }

        public SocialShareInfo getSocialShareInfo() {
            return socialShareInfo;
        }
        public void setSocialShareInfo(SocialShareInfo socialShareInfo) {
            this.socialShareInfo = socialShareInfo;
        }
    }

    public class Prize implements Serializable {
        private Long giftPrizeID;
        private Long giftProgrameID;
        private Long spinType;
        private String prizeName;
        private Long status;
        private String description;
        private String prizeType;
        private Long prizeValue;
        private String prizeUnit;
        private String image;
        private Long expiredTime;
        private Long redeemTime;
        private String qrCode;

        public Long getGiftPrizeID() { return giftPrizeID; }
        public void setGiftPrizeID(Long value) { this.giftPrizeID = value; }

        public Long getGiftProgrameID() { return giftProgrameID; }
        public void setGiftProgrameID(Long value) { this.giftProgrameID = value; }

        public Long getSpinType() { return spinType; }
        public void setSpinType(Long value) { this.spinType = value; }

        public String getPrizeName() { return prizeName; }
        public void setPrizeName(String value) { this.prizeName = value; }

        public Long getStatus() { return status; }
        public void setStatus(Long value) { this.status = value; }

        public String getDescription() { return description; }
        public void setDescription(String value) { this.description = value; }

        public String getPrizeType() { return prizeType; }
        public void setPrizeType(String value) { this.prizeType = value; }

        public Long getPrizeValue() { return prizeValue; }
        public void setPrizeValue(Long value) { this.prizeValue = value; }

        public String getPrizeUnit() { return prizeUnit; }
        public void setPrizeUnit(String value) { this.prizeUnit = value; }

        public String getImage() { return image; }
        public void setImage(String value) { this.image = value; }

        public Long getExpiredTime() { return expiredTime; }
        public void setExpiredTime(Long value) { this.expiredTime = value; }

        public Long getRedeemTime() { return redeemTime; }
        public void setRedeemTime(Long value) { this.redeemTime = value; }

        public String getQrCode() {
            return qrCode;
        }

        public void setQrCode(String qrCode) {
            this.qrCode = qrCode;
        }
    }

    public class SocialShareInfo implements Serializable {
        private Long id;
        private String shareKey;
        private String shareTitle;
        private String shareContent;
        private String sharePhoto;
        private String shareVideo;
        private String language;
        private Integer status;
        private Long createTime;
        private Long updateTime;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getShareKey() {
            return shareKey;
        }

        public void setShareKey(String shareKey) {
            this.shareKey = shareKey;
        }

        public String getShareTitle() {
            return shareTitle;
        }

        public void setShareTitle(String shareTitle) {
            this.shareTitle = shareTitle;
        }

        public String getShareContent() {
            return shareContent;
        }

        public void setShareContent(String shareContent) {
            this.shareContent = shareContent;
        }

        public String getSharePhoto() {
            return sharePhoto;
        }

        public void setSharePhoto(String sharePhoto) {
            this.sharePhoto = sharePhoto;
        }

        public String getShareVideo() {
            return shareVideo;
        }

        public void setShareVideo(String shareVideo) {
            this.shareVideo = shareVideo;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Long createTime) {
            this.createTime = createTime;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }
    }
}

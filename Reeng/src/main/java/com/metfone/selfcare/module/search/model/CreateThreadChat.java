/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/20
 *
 */

package com.metfone.selfcare.module.search.model;

import java.io.Serializable;

public class CreateThreadChat implements Serializable {
    private String name;
    private String keySearchChat;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeySearchChat() {
        return keySearchChat;
    }

    public void setKeySearchChat(String keySearchChat) {
        this.keySearchChat = keySearchChat;
    }
}

package com.metfone.selfcare.module.metfoneplus.dialog;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.airbnb.lottie.LottieAnimationView;
import com.metfone.selfcare.R;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.metfone.selfcare.business.UserInfoBusiness.fastblur;
import static com.metfone.selfcare.business.UserInfoBusiness.takeScreenShot;
import static com.metfone.selfcare.business.UserInfoBusiness.takeScreenShotFull;
import static com.metfone.selfcare.module.home_kh.util.ResourceUtils.getResources;

public class MPHeroDialog extends DialogFragment {
    public static final String TAG = MPHeroDialog.class.getSimpleName();
    protected AppCompatImageView mImage;
    protected AppCompatTextView mTitle;
    protected AppCompatTextView mContent;
    protected RelativeLayout mButtonTopLayout;
    protected View mButtonTopBackground;
    protected AppCompatButton mButtonTop;
    protected RelativeLayout mButtonMiddleLayout;
    protected View mButtonMiddleBackground;
    protected AppCompatButton mButtonMiddle;
    protected RelativeLayout mButtonBottomLayout;
    protected View mButtonBottomBackground;
    protected AppCompatButton mButtonBottom;
    protected RelativeLayout rlContainer;
    protected LinearLayout lnlContent;
    protected FrameLayout flImage;
    protected LottieAnimationView lavAnimations;
    public MPHeroDialog() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogConfirmFullScreen);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog() != null && getDialog().getWindow() != null){
//            getDialog().getWindow().setDimAmount(0.7f);
            WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
            lp.dimAmount=0.0f;
            getDialog().getWindow().setAttributes(lp);
            getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0x00000004));

//            Observable.defer(new Callable<ObservableSource<?>>() {
//                @Override
//                public ObservableSource<Bitmap> call() throws Exception {
//                    Bitmap map=takeScreenShotFull(requireActivity());
//                    Bitmap fast=fastblur(map, 10);
//                    return Observable.just(fast);
//                }
//            }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Object>() {
//                @Override
//                public void onSubscribe(Disposable d) {
//
//                }
//
//                @Override
//                public void onNext(Object o) {
//                    final Drawable draw=new BitmapDrawable(getResources(), (Bitmap) o);
//                    if (getDialog() != null && getDialog().getWindow() != null)
//                        getDialog().getWindow().setBackgroundDrawable(draw);
//                }
//
//                @Override
//                public void onError(Throwable e) {
//
//                }
//
//                @Override
//                public void onComplete() {
//
//                }
//            });
//            WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
//            lp.alpha=0.5f;
//            getDialog().getWindow().setAttributes(lp);
        }
        View view = inflater.inflate(R.layout.dialog_mp_hero, container, false);

        mImage = view.findViewById(R.id.dialog_hero_img);
        mTitle = view.findViewById(R.id.dialog_hero_title);
        mContent = view.findViewById(R.id.dialog_hero_content);

        mButtonTopLayout = view.findViewById(R.id.dialog_hero_top_layout_button);
        mButtonTopBackground = view.findViewById(R.id.dialog_hero_top_background_button);
        mButtonTop = view.findViewById(R.id.dialog_hero_top_title_button);

        mButtonMiddleLayout = view.findViewById(R.id.dialog_hero_middle_layout_button);
        mButtonMiddleBackground = view.findViewById(R.id.dialog_hero_middle_background_button);
        mButtonMiddle = view.findViewById(R.id.dialog_hero_middle_title_button);

        mButtonBottomLayout = view.findViewById(R.id.dialog_hero_bottom_layout_button);
        mButtonBottomBackground = view.findViewById(R.id.dialog_hero_bottom_background_button);
        mButtonBottom = view.findViewById(R.id.dialog_hero_bottom_title_button);

        rlContainer = view.findViewById(R.id.rlContainer);
        lnlContent = view.findViewById(R.id.lnlContent);

        flImage = view.findViewById(R.id.flImage);
        lavAnimations = view.findViewById(R.id.lavAnimations);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewDefault();
    }

    private void setupViewDefault() {
        mImage.setVisibility(View.GONE);
        mButtonTopBackground.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_red_button_corner_6));
        mButtonBottomBackground.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_transparent_button_corner_6));
        mButtonTopLayout.setVisibility(View.GONE);
        mButtonMiddleLayout.setVisibility(View.GONE);
        mButtonBottomBackground.setVisibility(View.GONE);
    }

    public void setTopButton(int idStringRes, int idDrawableRes, View.OnClickListener onClickListener) {
        mButtonTop.setText(idStringRes);
        mButtonTopBackground.setBackground(ContextCompat.getDrawable(getActivity(), idDrawableRes));
        mButtonTopLayout.setVisibility(View.VISIBLE);
        mButtonTop.setOnClickListener(onClickListener);
    }

    public void setMiddleButton(int idStringRes, int idDrawableRes, View.OnClickListener onClickListener) {
        mButtonMiddle.setText(idStringRes);
        mButtonMiddleBackground.setBackground(ContextCompat.getDrawable(getActivity(), idDrawableRes));
        mButtonMiddleLayout.setVisibility(View.VISIBLE);
        mButtonMiddle.setOnClickListener(onClickListener);
    }

    public void setBottomButton(int idStringRes, int idDrawableRes, View.OnClickListener onClickListener) {
        mButtonBottom.setText(idStringRes);
        mButtonBottomBackground.setBackground(ContextCompat.getDrawable(getActivity(), idDrawableRes));
        mButtonBottomLayout.setVisibility(View.VISIBLE);
        mButtonBottom.setOnClickListener(onClickListener);
    }

    public void setButtonTopVisibility(int visibility) {
        mButtonTopLayout.setVisibility(visibility);
    }

    public void setButtonMiddleVisibility(int visibility) {
        mButtonMiddleLayout.setVisibility(visibility);
    }

    public void setButtonBottomVisibility(int visibility) {
        mButtonBottomLayout.setVisibility(visibility);
    }

}

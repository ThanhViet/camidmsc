/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/12
 *
 */

package com.metfone.selfcare.module.video.model;

import java.io.Serializable;

public class TabModel implements Serializable {

    public static final int TYPE_RECOMMEND_VIDEO = 1;
    public static final int TYPE_HOT_VIDEO = 2;
    public static final int TYPE_NEW_VIDEO = 3;
    public static final int TYPE_FOLLOWING = 4;
    public static final int TYPE_TOP_CHANNEL = 5;
    public static final int TYPE_TOP_MONEY = 6;

    private static final long serialVersionUID = -7684702607194357264L;
    private String id;
    private int type;
    private String title;
    private boolean loadChannel = false;

    public TabModel() {
    }

    public TabModel(String id, int type, String title) {
        this.id = id;
        this.type = type;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isLoadChannel() {
        return loadChannel;
    }

    public void setLoadChannel(boolean loadChannel) {
        this.loadChannel = loadChannel;
    }

    @Override
    public String toString() {
        return "TabModel{" +
                "id=" + id +
                ", type=" + type +
                ", title='" + title + '\'' +
                '}';
    }
}

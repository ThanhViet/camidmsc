package com.metfone.selfcare.module.spoint;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.spoint.fragment.SpointFragment;
import com.metfone.selfcare.module.spoint.fragment.SpointPlusFragment;
import com.metfone.selfcare.module.spoint.fragment.StatisticalSpointFragment;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;

public class SpointActivity extends BaseSlidingFragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AppCompatImageView ivBack, ivInfo;
    private ViewPagerDetailAdapter adapter;
    public static final String SPOINT = "0";
    public static final String SPOINT_PLUS = "1";
    View progressEmpty;
    TextView tvEmpty;
    TextView tvEmptyRetry1;
    TextView tvEmptyRetry2;
    ImageView btnEmptyRetry;
    View viewEmpty;
    private int currentTab = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acvitity_spoint);
        initView();
        loadData();
        initEvent();
    }

    private void initView() {
        progressEmpty = findViewById(R.id.empty_progress);
        tvEmpty = findViewById(R.id.empty_text);
        tvEmptyRetry1 = findViewById(R.id.empty_retry_text1);
        tvEmptyRetry2 = findViewById(R.id.empty_retry_text2);
        btnEmptyRetry = findViewById(R.id.empty_retry_button);
        viewEmpty = findViewById(R.id.empty_layout);
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);
        ivBack = findViewById(R.id.iv_back);
        ivInfo = findViewById(R.id.iv_info);
    }

    private void loadData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            currentTab = getIntent().getExtras().getInt(Constants.GAME.ID_GAME);
        }
        adapter = new ViewPagerDetailAdapter(getSupportFragmentManager());
        adapter.addFragment(SpointFragment.newInstance(SPOINT), getString(R.string.f_spoint));
        adapter.addFragment(SpointFragment.newInstance(SPOINT_PLUS), getString(R.string.f_spoint_plus));
        adapter.addFragment(StatisticalSpointFragment.newInstance(), getString(R.string.f_statistical));
        if (viewPager != null) {
            viewPager.setAdapter(adapter);
            viewPager.setOffscreenPageLimit(3);
            viewPager.setCurrentItem(currentTab);
            viewPager.addOnPageChangeListener(this);
            if (tabLayout != null) {
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.addOnTabSelectedListener(this);
            }
        }
    }
    public void refreshData(){
        if(adapter == null) return;
        for (int i = 0; i < adapter.getCount(); i++) {
            if(adapter.getItem(i) instanceof SpointFragment){
                ((SpointFragment) adapter.getItem(i)).loadData();
            }else if(adapter.getItem(i) instanceof StatisticalSpointFragment){
                ((StatisticalSpointFragment) adapter.getItem(i)).loadData();
            }
        }

    }

    private void initEvent() {
        ivBack.setOnClickListener(this);
        ivInfo.setOnClickListener(this);
        btnEmptyRetry.setOnClickListener(this);
    }
    public void showLoading() {
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.VISIBLE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
    }

    public void showLoadedSuccess() {
        if (viewEmpty != null) viewEmpty.setVisibility(View.GONE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
    }

    public void showLoadedError() {
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.VISIBLE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (NetworkHelper.isConnectInternet(this)) {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.VISIBLE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        } else {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_info:
                break;
            case R.id.empty_retry_button:
                refreshData();
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}

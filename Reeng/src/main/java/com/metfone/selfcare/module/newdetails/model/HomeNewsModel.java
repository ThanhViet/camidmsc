package com.metfone.selfcare.module.newdetails.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HaiKE on 9/15/17.
 */

public class HomeNewsModel implements Serializable {

    private final static long serialVersionUID = 2795922927516382880L;
    @SerializedName("Header")
    @Expose
    private String header;
    @SerializedName("Position")
    @Expose
    private int position;
    @SerializedName("TypeDisplay")
    @Expose
    private int typeDisplay;
    @SerializedName("CategoryID")
    @Expose
    private int categoryId;
    @SerializedName("data")
    @Expose
    private List<NewsModel> data = new ArrayList<>();

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<NewsModel> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<NewsModel> data) {
        this.data = data;
    }

    public List<NewsModel> getDataTwo() {
        List<NewsModel> dataTwo = new ArrayList<>();
        if(data == null){
            data = new ArrayList<>();
        }
        if (data.size() > 1) {
            dataTwo.add(data.get(1));
        }
        if (data.size() > 2) {
            dataTwo.add(data.get(2));
        }
        return dataTwo;
    }

    public List<NewsModel> getDataTwoEnd() {
        List<NewsModel> dataTwo = new ArrayList<>();
        if(data == null){
            data = new ArrayList<>();
        }
        if (data.size() > 2) {
            dataTwo.add(data.get(data.size() - 2));
        }
        if (data.size() > 3) {
            dataTwo.add(data.get(data.size() - 1));
        }
        return dataTwo;
    }

    public List<NewsModel> getFirstFromThree() {
        List<NewsModel> listThree = new ArrayList<>();
        if(data == null){
            data = new ArrayList<>();
        }
        if(data.size() > 3) {
            for (int i = 3; i < data.size(); i++) {
                listThree.add(data.get(i));
            }
        }
        return listThree;
    }
    public List<NewsModel> getDataOtherOne(){
        List<NewsModel> listThree = new ArrayList<>();
        if(data == null){
            data = new ArrayList<>();
        }
        if(data.size() > 1) {
            for (int i = 1; i < data.size(); i++) {
                listThree.add(data.get(i));
            }
        }
        return listThree;
    }

    public int getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(int typeDisplay) {
        this.typeDisplay = typeDisplay;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "HomeNewsModel{" +
                "header='" + header + '\'' +
                ", position=" + position +
                ", data=" + data +
                '}';
    }
}
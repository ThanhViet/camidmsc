package com.metfone.selfcare.module.home_kh.tab;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.CollectionUtils;
import com.google.gson.reflect.TypeToken;
import com.metfone.esport.entity.repsonse.HomeResponse;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.request.WsGetHotMovieRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetTopMovieCategoryRequest;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieResponse;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeBanner;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Response;

public class TabHomeKhApi extends BaseApi {

    private KhHomeClient homeClient;
    public TabHomeKhApi() {
        super(ApplicationController.self());
        homeClient = new KhHomeClient();
    }

    public void getBanner(@Nullable final ApiCallbackV2<KhHomeBanner> listener, int ref_id) {
        long currentTime = TimeHelper.getCurrentTime();
        String domain = getDomainOnMedia();
        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LAST_TIME_DATA_TAB_HOME, System.currentTimeMillis());
        ReengAccount reengAccount = getReengAccount();
        String accountToken = reengAccount != null ? reengAccount.getToken() : "";
        String msisdn = reengAccount != null ? reengAccount.getJidNumber() : "";
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String countryCode = getReengAccountBusiness().getRegionCode();
        String languageCode = getReengAccountBusiness().getCurrentLanguage();
        String vip = getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP;
        String timestamp = String.valueOf(currentTime);
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(domain);
        sb.append(clientType);
        sb.append(revision);
        sb.append(countryCode);
        sb.append(languageCode);
        sb.append(vip);
        sb.append(accountToken);
        sb.append(currentTime);
        String security = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);

        Http.Builder builder;
//        builder = get(domain, Url.OnMedia.API_GET_TAB_HOME);
        // TODO: [START] Cambodia version
        // String baseUrl = ""http://mock.toprate.io""
//        String baseUrl = "http://171.234.213.151:9123";
//        String baseUrl = "http://36.37.242.22";
//        builder = get(baseUrl, "/v2/api/get-hot?limit=10&offset=0");
//        builder.putParameter("msisdn", msisdn);
//        builder.putParameter("domain", domain);
//        builder.putParameter("clientType", clientType);
//        builder.putParameter("revision", revision);
//        builder.putParameter("countryCode", countryCode);
//        builder.putParameter("languageCode", languageCode);
//        builder.putParameter("vip", vip);
//        builder.putParameter("uuid", Utilities.getUuidApp());
//        builder.putParameter("timestamp", timestamp);
//        builder.putParameter("security", security);
//        builder.putParameter("loginStatus", getReengAccountBusiness().isAnonymousLogin() ? "0" : "1");
//        builder.putParameter("ref_id", String.valueOf(ref_id));
//        builder.withCallBack(new HttpCallBack() {
//            @Override
//            public void onSuccess(String response) throws Exception {
//                JSONObject jsonObject = new JSONObject(response);
//                List<KhHomeMovieItem> recommend = gson.fromJson(jsonObject.optString("result"), new TypeToken<List<KhHomeMovieItem>>() {
//                }.getType());
//                KhHomeBanner khHomeBanner = null;
//                if (recommend != null && recommend.size() > 0) {
//                    khHomeBanner = new KhHomeBanner();
//                    khHomeBanner.recommend = recommend;
////                    SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW);
//                } else {
////                    SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW, tmp);
//                }
//                if (listener != null) {
//                    if (khHomeBanner != null)
//                        listener.onSuccess("", khHomeBanner);
//                    else
//                        listener.onError(application.getString(R.string.e601_error_but_undefined));
//                }
//            }
//
//            @Override
//            public void onFailure(String message) {
//                Log.d(TAG, "getHome onFailure: " + message);
//                if (listener != null)
//                    listener.onError(message);
//            }
//
//            @Override
//            public void onCompleted() {
//                Log.d(TAG, "getHome onCompleted");
//            }
//        });
//        builder.setTimeOut(30);
//        builder.execute();


        WsGetHotMovieRequest.Request request = new WsGetHotMovieRequest.Request(
                String.valueOf(ref_id),
                Constants.HTTP.PLATFORM,
                languageCode,
                getReengAccountBusiness().isAnonymousLogin() ? "0" : "1",
                Utilities.getUuidApp(),
                revision,
                security,
                clientType,
                countryCode,
                domain,
                msisdn,
                vip,
                timestamp
        );

        homeClient.wsGetHotMovie(new MPApiCallback<WsGetMovieResponse>() {
            @Override
            public void onResponse(Response<WsGetMovieResponse> response) {
                WsGetMovieResponse.Response responseRaw = response.body().response;
                if(responseRaw != null){
                    List<KhHomeMovieItem> recommend = responseRaw.result;
                    KhHomeBanner khHomeBanner = null;
                    if (recommend != null && recommend.size() > 0) {
                        khHomeBanner = new KhHomeBanner();
                        khHomeBanner.recommend = recommend;
//                    SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW);
                    } else {
//                    SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW, tmp);
                    }
                    if (listener != null) {
                        if (khHomeBanner != null) {
                            try {
                                listener.onSuccess("", khHomeBanner);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else
                            listener.onError(application.getString(R.string.e601_error_but_undefined));
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                Log.d(TAG, "getHome onFailure: " + error.getMessage());
                if (listener != null)
                    listener.onError(error.getMessage());
            }
        }, request);
    }

    public void getBannerForHome(ApiCallbackV2<HomeData> callbackV2){

    }

    public void getPopularLiveChannel(@Nullable final ApiCallbackV2<List<VideoInfoResponse>> listener) {
        long currentTime = TimeHelper.getCurrentTime();
//        String domain = getDomainOnMedia();
        String domain = "api1camid.metfone.com.kh/";
        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LAST_TIME_DATA_TAB_HOME, System.currentTimeMillis());
        ReengAccount reengAccount = getReengAccount();
        String msisdn = reengAccount != null ? reengAccount.getJidNumber() : "";
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String languageCode = getReengAccountBusiness().getCurrentLanguage();

        Http.Builder builder;
        String baseUrl = "http://api1camid.metfone.com.kh";
        builder = get(baseUrl, "/camidApiService/app/getDataHome");
        builder.putHeader("languageCode", languageCode);
        builder.putParameter("userId", msisdn);
        builder.putParameter("domain", domain);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                JSONObject jsonObject = new JSONObject(response);
                List<HomeResponse> data = gson.fromJson(jsonObject.optString("data"), new TypeToken<List<HomeResponse>>() {
                }.getType());
                if (listener != null) {
                    if (CollectionUtils.isNotEmpty(data)) {
                        for (HomeResponse res : data) {
                            if ((res.type.equalsIgnoreCase("PopularLiveChannel") || res.title.equalsIgnoreCase("PopularLiveChannel")) && CollectionUtils.isNotEmpty(res.listVideo)) {
                                listener.onSuccess("", res.listVideo);
                            } else {
                                listener.onError("");
                            }
                        }
                    } else
                        listener.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onFailure(String message) {
                Log.d(TAG, "getHome onFailure: " + message);
                if (listener != null)
                    listener.onError(message);
            }

            @Override
            public void onCompleted() {
                Log.d(TAG, "getHome onCompleted");
            }
        });
        builder.setTimeOut(30);
        builder.execute();
    }

    public void getCustomTopic(@Nullable final ApiCallbackV2<List<KhHomeMovieItem>> listener) {
        long currentTime = TimeHelper.getCurrentTime();
        String domain = getDomainOnMedia();
        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LAST_TIME_DATA_TAB_HOME, System.currentTimeMillis());
        ReengAccount reengAccount = getReengAccount();
        String accountToken = reengAccount != null ? reengAccount.getToken() : "";
        String msisdn = reengAccount != null ? reengAccount.getJidNumber() : "";
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String countryCode = getReengAccountBusiness().getRegionCode();
        String languageCode = getReengAccountBusiness().getCurrentLanguage();
        String vip = getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP;
        String timestamp = String.valueOf(currentTime);
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(domain);
        sb.append(clientType);
        sb.append(revision);
        sb.append(countryCode);
        sb.append(languageCode);
        sb.append(vip);
        sb.append(accountToken);
        sb.append(currentTime);
        String security = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);

//        Http.Builder builder;
//        // TODO: [START] Cambodia version
////        builder = get(domain, Url.OnMedia.API_GET_TAB_HOME);
//        // "http://mock.toprate.io"
////        String baseUrl = "http://171.234.213.151:9123";
//        builder = get(MovieApi.DOMAIN_MOVIE, "/v2/api/get-top-category?categoryid=868459&limit=1000&offset=0");
//        // TODO: [END] Cambodia version
//        builder.putParameter("msisdn", msisdn);
//        builder.putParameter("domain", domain);
//        builder.putParameter("clientType", clientType);
//        builder.putParameter("revision", revision);
//        builder.putParameter("countryCode", countryCode);
//        builder.putParameter("languageCode", languageCode);
//        builder.putParameter("vip", vip);
//        builder.putParameter("uuid", Utilities.getUuidApp());
//        builder.putParameter("timestamp", timestamp);
//        builder.putParameter("security", security);
//        builder.putParameter("loginStatus", getReengAccountBusiness().isAnonymousLogin() ? "0" : "1");
//        builder.withCallBack(new HttpCallBack() {
//            @Override
//            public void onSuccess(String response) throws Exception {
//                JSONObject jsonObject = new JSONObject(response);
//                List<KhHomeMovieItem> tmp = gson.fromJson(jsonObject.optString("result"), new TypeToken<List<KhHomeMovieItem>>() {
//                }.getType());
//                if (tmp == null)
//                    SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW);
//                else
//                    SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW, tmp);
//                if (listener != null) {
//                    if (tmp != null) {
//                        listener.onSuccess("", tmp);
//                    }
//                    else
//                        listener.onError(application.getString(R.string.e601_error_but_undefined));
//                }
//            }
//
//            @Override
//            public void onFailure(String message) {
//                Log.d(TAG, "getHome onFailure: " + message);
//                if (listener != null)
//                    listener.onError(message);
//            }
//
//            @Override
//            public void onCompleted() {
//                Log.d(TAG, "getHome onCompleted");
//            }
//        });
//        builder.setTimeOut(30);
//        builder.execute();

        WsGetTopMovieCategoryRequest.Request request = new WsGetTopMovieCategoryRequest.Request();
        request.msisdn = msisdn;
        request.revision = revision;
        request.platform = Constants.HTTP.PLATFORM;
        request.clientType = clientType;
        request.domain = domain;
        request.language = languageCode;
        request.countryCode = countryCode;
        request.loginStatus = getReengAccountBusiness().isAnonymousLogin() ? "0" : "1";
        request.security = security;
        request.vip = vip;
        request.uuid = Utilities.getUuidApp();
        request.timestamp = timestamp;

        homeClient.wsGetTopMoviesCategory(new MPApiCallback<WsGetMovieResponse>() {
            @Override
            public void onResponse(Response<WsGetMovieResponse> response) {
                List<KhHomeMovieItem> tmp = response.body().response.result;
                if (tmp == null)
                    SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW);
                else
                    SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW, tmp);
                if (listener != null) {
                    if (tmp != null) {
                        try {
                            listener.onSuccess("", tmp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                        listener.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onError(Throwable error) {
                Log.d(TAG, "getHome onFailure: " + error.getMessage());
                if (listener != null)
                    listener.onError(error.getMessage());
            }
        }, request);
    }

}

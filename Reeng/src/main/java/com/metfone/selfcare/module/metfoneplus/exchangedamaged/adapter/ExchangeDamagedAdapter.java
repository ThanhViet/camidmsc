package com.metfone.selfcare.module.metfoneplus.exchangedamaged.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.camid.ChangeCardDamaged;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExchangeDamagedAdapter extends RecyclerView.Adapter<ExchangeDamagedAdapter.ExchangeDamagedHolder> {
    private Context context;
    private List<ChangeCardDamaged> changeCardDamageds;
    private IExchangeDamagedListener  iExchangeDamagedListener;
    public ExchangeDamagedAdapter(Context context,List<ChangeCardDamaged> changeCardDamageds,IExchangeDamagedListener  iExchangeDamagedListener) {
        this.context = context;
        this.changeCardDamageds = changeCardDamageds;
        this.iExchangeDamagedListener = iExchangeDamagedListener;
    }

    @NonNull
    @Override
    public ExchangeDamagedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exchange_damaged, parent, false);
        return new ExchangeDamagedHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExchangeDamagedHolder holder, int position) {
            holder.bindView(changeCardDamageds.get(position),position);
    }

    @Override
    public int getItemCount() {
       return changeCardDamageds.size();
    }

    public class ExchangeDamagedHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivStatus)
        ImageView ivStatus;
        @BindView(R.id.tvCode)
        TextView tvCode;
        @BindView(R.id.tvSerial)
        TextView tvSerial;
        @BindView(R.id.ivArrowDown)
        ImageView ivArrowDown;
        @BindView(R.id.tvCreatedDate)
        TextView tvCreatedDate;
        @BindView(R.id.lnAttackFile)
        LinearLayout lnAttackFile;
        @BindView(R.id.ivExchangeDamagedImage)
        ImageView ivExchangeDamagedImage;
        @BindView(R.id.ivExchangeDamagedVideo)
        ImageView ivExchangeDamagedVideo;
        @BindView(R.id.rlStatus1)
        RelativeLayout rlStatus1;
        @BindView(R.id.rlVideo)
        RelativeLayout rlVideo;
        @BindView(R.id.tvStatus1)
        TextView tvStatus1;
        @BindView(R.id.tvStatusDate1)
        TextView tvStatusDate1;
        @BindView(R.id.rlStatus2)
        RelativeLayout rlStatus2;
        @BindView(R.id.tvStatus2)
        TextView tvStatus2;
        @BindView(R.id.tvStatusDate2)
        TextView tvStatusDate2;
        @BindView(R.id.rlStatus3)
        RelativeLayout rlStatus3;
        @BindView(R.id.tvStatus3)
        TextView tvStatus3;
        @BindView(R.id.tvStatusDate3)
        TextView tvStatusDate3;
        @BindView(R.id.frArrowDown)
        RelativeLayout frArrowDown;
        @BindView(R.id.clContent)
        LinearLayout clContent;
        public ExchangeDamagedHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


        public void bindView(ChangeCardDamaged model, int pos) {
            tvCode.setText(model.getPhoneNumber());
            tvSerial.setText(context.getString(R.string.exchange_damaged_serial) +": " + model.getSerial());
            tvCreatedDate.setText(model.getRequestDateStr());
            byte[] decodedString = Base64.decode(model.getDataImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            ivExchangeDamagedImage.setImageBitmap(decodedByte);
            if (!TextUtils.isEmpty(model.getPathVideo())) {
                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(model.getPathVideo(), MediaStore.Video.Thumbnails.MICRO_KIND);
                ivExchangeDamagedVideo.setImageBitmap(bMap);
                rlVideo.setVisibility(View.VISIBLE);
            } else {
                rlVideo.setVisibility(View.GONE);
            }
            if(model.getStatus().equals("0")){
                ImageViewCompat.setImageTintList(ivStatus, ColorStateList.valueOf(ContextCompat.getColor(context, R.color.thread_blue)));
            } else if (model.getStatus().equals("1")){
                ImageViewCompat.setImageTintList(ivStatus, ColorStateList.valueOf(ContextCompat.getColor(context, R.color.green)));
            } else {
                ImageViewCompat.setImageTintList(ivStatus, ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red)));
            }
            if (model.isExpand()){
                rlStatus1.setVisibility(View.VISIBLE);
                rlStatus2.setVisibility(View.VISIBLE);
                rlStatus3.setVisibility(View.VISIBLE);
                lnAttackFile.setVisibility(View.VISIBLE);
                ivArrowDown.setRotation(180);
                if (model.getStatus().equals("0")){
                    rlStatus1.setVisibility(View.GONE);
                    rlStatus2.setVisibility(View.GONE);
                    rlStatus3.setVisibility(View.GONE);
                } else if (model.getStatus().equals("1")){
                    rlStatus1.setVisibility(View.VISIBLE);
                    rlStatus2.setVisibility(View.VISIBLE);
                    rlStatus3.setVisibility(View.GONE);
                    tvStatus1.setText(context.getString(R.string.exchange_damaged_approve_time));
                    tvStatusDate1.setText(model.getUpdateDateStr());
                    tvStatus2.setText(context.getString(R.string.exchange_damaged_approve_by));
                    tvStatusDate2.setText(model.getUpdateUser());
                } else {
                    rlStatus1.setVisibility(View.VISIBLE);
                    rlStatus2.setVisibility(View.VISIBLE);
                    rlStatus3.setVisibility(View.VISIBLE);
                    tvStatus1.setText(context.getString(R.string.exchange_damaged_reject_time));
                    tvStatusDate1.setText(model.getUpdateDateStr());
                    tvStatus2.setText(context.getString(R.string.exchange_damaged_reject_by));
                    tvStatusDate2.setText(model.getUpdateUser());
                    tvStatus3.setText(context.getString(R.string.exchange_damaged_reason));
                    tvStatusDate3.setText(model.getUpdateReason());
                }
            } else {
                rlStatus1.setVisibility(View.GONE);
                rlStatus2.setVisibility(View.GONE);
                rlStatus3.setVisibility(View.GONE);
                lnAttackFile.setVisibility(View.GONE);
                ivArrowDown.setRotation(0);
            }
            clContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iExchangeDamagedListener.selectContact(pos);
                }
            });
            rlVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(model.getPathVideo()));
                    intent.setDataAndType(Uri.parse(model.getPathVideo()), "video/mp4");
                    context.startActivity(intent);
                }
            });
            ivExchangeDamagedImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                        FileOutputStream stream = null;
                        File photo = File.createTempFile(
                                "CamID_share_photo" ,  /* prefix */
                                ".png",  /* suffix */
                                storageDir /* directory */
                        );
                        if (photo.exists()) {
                            photo.delete();
                        }
                        FileOutputStream fos = new FileOutputStream(photo, true);
                        fos.write(Base64.decode(model.getDataImage(), Base64.DEFAULT));
                        fos.close();
                        Uri uri = Uri.parse(photo.getAbsolutePath());
                        Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                        intent.setDataAndType(uri, "image/png");
                        context.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    public interface IExchangeDamagedListener {
        void selectContact(int position);
    }
}

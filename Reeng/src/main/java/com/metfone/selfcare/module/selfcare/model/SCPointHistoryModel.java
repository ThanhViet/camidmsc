package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCPointHistoryModel implements Serializable {
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("updateTime")
    @Expose
    private String updateTime;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("type")
    @Expose
    private String type;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SCPointHistoryModel{" +
                "reason='" + reason + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", amount='" + amount + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}

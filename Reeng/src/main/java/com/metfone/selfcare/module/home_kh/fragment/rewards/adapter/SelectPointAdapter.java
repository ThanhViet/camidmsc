package com.metfone.selfcare.module.home_kh.fragment.rewards.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.fragment.rewards.DetailType;
import com.metfone.selfcare.module.home_kh.model.LstBlock;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

public class SelectPointAdapter extends RecyclerView.Adapter<SelectPointAdapter.FilterVH> {

    private List<LstBlock> data = new ArrayList<>();
    private ISelectPoint iSelectPoint;
    private DetailType type;
    private int selectedPosition = -2;

    public void setSelectedPos(int pos) {
        this.selectedPosition = pos;
    }

    public void setSelectedPosDefault() {
        if (!data.isEmpty()) {
            selectedPosition = 0;
        }
    }

    public SelectPointAdapter(List<LstBlock> data, DetailType type, ISelectPoint iFilterClick) {
        this.data = data;
        this.type = type;
        this.iSelectPoint = iFilterClick;
    }

    @NonNull
    @Override
    public FilterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_redeem_reward_option, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FilterVH holder, int position) {
        holder.bindData(position);
    }


    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public List<LstBlock> getData() {
        return data;
    }

    class FilterVH extends RecyclerView.ViewHolder {
        private AppCompatTextView tvContent;
        private CardView cardView;

        public FilterVH(View itemView) {
            super(itemView);
            tvContent = itemView.findViewById(R.id.tv_content);
            cardView = itemView.findViewById(R.id.card_view);
        }

        public void bindData(int position) {
            LstBlock item = data.get(position);
            cardView.setSelected(position == selectedPosition);
            if (type == DetailType.BALANCE) {
                tvContent.setText(ResourceUtils.getString(R.string.point_option, String.valueOf(item.getPoint()), item.getUnit(), Utilities.formatDouble(item.getValue())));

            } else {
                tvContent.setText(ResourceUtils.getString(R.string.point_option, String.valueOf(item.getPoint()), Utilities.formatDouble(item.getValue()), item.getUnit()));
            }
            cardView.setOnClickListener(view -> {
                selectedPosition = position;
                notifyDataSetChanged();
                if (iSelectPoint != null) {
                    iSelectPoint.itemClick(position);
                }
            });
        }
    }

    public interface ISelectPoint {
        void itemClick(int position);
    }
}

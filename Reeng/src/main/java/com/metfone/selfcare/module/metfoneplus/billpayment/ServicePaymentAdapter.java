package com.metfone.selfcare.module.metfoneplus.billpayment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.ItemGroupServicePaymentBinding;
import com.metfone.selfcare.databinding.ItemServiceBillPaymentBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ServicePaymentAdapter extends RecyclerView.Adapter<ServicePaymentAdapter.ViewHolder> {
    private List<ServicePaymentGroupModel> groupModels;
    private ObservableField<ServicePaymentModel> selectedItem;
    private EventListener listener;
    public ServicePaymentAdapter(List<ServicePaymentGroupModel> groupModels, EventListener listener){
        selectedItem = new ObservableField<>();
        this.groupModels = groupModels;
        this.listener = listener;
    }
    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group_service_payment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        holder.bindData(groupModels.get(position));
    }

    @Override
    public int getItemCount() {
        return groupModels.size();
    }

    public ObservableField<ServicePaymentModel> getSelectedItem() {
        return selectedItem;
    }

    public ServicePaymentModel getSelectedItemObject(){
        return selectedItem.get();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
        }

        public void bindData(ServicePaymentGroupModel groupModel){
            ItemGroupServicePaymentBinding binding = ItemGroupServicePaymentBinding.bind(itemView);
            binding.tvTitle.setText(groupModel.groupTitle);
            SubAdapter adapter = new SubAdapter(groupModel.services);
            binding.recyclerView.setAdapter(adapter);
        }
    }

    public void onItemClicked(ServicePaymentModel servicePaymentModel){
        listener.onServicePaymentClicked(servicePaymentModel);
        selectedItem.set(servicePaymentModel);
    }

    class SubAdapter extends RecyclerView.Adapter<SubAdapter.ViewHolder> {
        private List<ServicePaymentModel> servicePaymentModels;
        public SubAdapter(List<ServicePaymentModel> servicePaymentModels){
            this.servicePaymentModels = servicePaymentModels;
        }
        @NonNull
        @NotNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service_bill_payment, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
            holder.bindData(servicePaymentModels.get(position));
        }

        @Override
        public int getItemCount() {
            return servicePaymentModels.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{

            public ViewHolder(@NonNull @NotNull View itemView) {
                super(itemView);
            }

            public void bindData(ServicePaymentModel servicePaymentModel){
                ItemServiceBillPaymentBinding binding = ItemServiceBillPaymentBinding.bind(itemView);
                binding.setObj(servicePaymentModel);
                binding.setHandler(ServicePaymentAdapter.this);
                binding.imgService.setImageResource(servicePaymentModel.getImg());
            }
        }
    }

    interface EventListener{
        void onServicePaymentClicked(ServicePaymentModel servicePaymentModel);
    }
}

package com.metfone.selfcare.module.games.viewmodel;

import com.metfone.selfcare.module.games.api.GameApi;
import static com.metfone.selfcare.module.games.api.GameApi.GET_GAMES;
import static com.metfone.selfcare.module.games.api.GameApi.GET_GAMES_PLAY_MOST_TIME;
import static com.metfone.selfcare.module.games.api.GameApi.GET_GAME_TOP_TEN;
import static com.metfone.selfcare.module.games.api.GameApi.GET_PLAYED_GAMES;
import static com.metfone.selfcare.module.games.fragment.GameHomePageFragment.*;

import android.util.Log;

public class GameHomePageModel extends BaseViewModel {
    private final String TAG = "GameHomePageModel";



    public void getRecentlyList() {
        GameApi.getInstance().getPlayedGames(1, new GameApi.ListenerGameApi() {
            @Override
            public void onPreRequest() {
                isLoadingRecentlyList = false;
            }

            @Override
            public void onSuccess(Object responseData) {
                callBack.onCallBack(GET_PLAYED_GAMES, responseData);
                isLoadingRecentlyList = false;
            }

            @Override
            public void onError(String message) {
                isLoadingRecentlyList = true;
            }

            @Override
            public void onFailure(String exp) {
                isLoadingRecentlyList = true;
            }
        });
    }

    public void getNewGames() {
        GameApi.getInstance().getGameTopTen(new GameApi.ListenerGameApi() {
            @Override
            public void onPreRequest() {
                isLoadingNewGames = false;
            }

            @Override
            public void onSuccess(Object responseData) {
                callBack.onCallBack(GET_GAME_TOP_TEN, responseData);
                isLoadingNewGames = false;
            }

            @Override
            public void onError(String message) {
                isLoadingNewGames = true;
            }

            @Override
            public void onFailure(String exp) {
                isLoadingNewGames = true;
            }
        });
    }

    public void getTrendingGames() {
        GameApi.getInstance().getGamePlayMostTime(1, 1, 10, new GameApi.ListenerGameApi() {
            @Override
            public void onPreRequest() {
                isLoadingTrendingGames = false;
            }

            @Override
            public void onSuccess(Object responseData) {
                callBack.onCallBack(GET_GAMES_PLAY_MOST_TIME, responseData);
                isLoadingTrendingGames = false;
            }

            @Override
            public void onError(String message) {
                isLoadingTrendingGames = true;
            }

            @Override
            public void onFailure(String exp) {
                isLoadingTrendingGames = true;
            }
        });
    }

    public void getListGame() {
        GameApi.getInstance().getGameByName("", new GameApi.ListenerGameApi() {
            @Override
            public void onPreRequest() {
                isLoadingListGame = false;
            }

            @Override
            public void onSuccess(Object responseData) {
                callBack.onCallBack(GET_GAMES, responseData);
                isLoadingListGame = false;
            }

            @Override
            public void onError(String message) {
                isLoadingListGame = true;
            }

            @Override
            public void onFailure(String exp) {
                isLoadingListGame = true;
            }
        });
    }
}

package com.metfone.selfcare.module.tab_home.utils;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.model.home.ItemMoreHome;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import org.json.JSONObject;

public class ConvertHelper extends com.metfone.selfcare.module.keeng.utils.ConvertHelper {

    public static Movie convert2Movie(KhHomeMovieItem model) {
        Movie item = new Movie();
        item.setId(String.valueOf(model.id));
        item.setName(model.name);
        item.setLinkWap(model.link_wap);
        item.setPosterPath(model.poster_path);
        item.setImagePath(model.image_path);
        return item;
    }

    public static Movie convert2Movie(Content model) {
        Movie item = new Movie();
        item.setId(model.getId());
        item.setName(model.getName());
        item.setLinkWap(model.getUrl());
        item.setPosterPath(model.getImage());
        item.setImagePath(model.getImage());
        return item;
    }

    public static Video convert2Video(Content model) {
        Video item = new Video();
        item.setId(model.getId());
        item.setTitle(model.getName());
        item.setLink(model.getUrl());
        item.setImagePath(model.getImage());
        item.setPublishTime(model.getPublishedTime());
        item.setTotalView(model.getTotalViews());
        int duration = model.getDurationInt();
        if (duration > 0)
            item.setDuration(DateTimeUtils.getDuration(duration));
        else
            item.setDuration(model.getDuration());
        Channel channel = new Channel();
        channel.setId(String.valueOf(model.getChannelId()));
        channel.setUrlImage(model.getChannelAvatar());
        channel.setName(model.getChannelName());
        item.setChannel(channel);
        return item;
    }

    public static AllModel convert2Music(Content model) {
        AllModel item = new AllModel();
        item.setId(model.getIdLong());
        item.setName(model.getName());
        item.setUrl(model.getUrl());
        item.setType(model.getItemTypeInt());
        item.setImagePath(model.getImage());
        return item;
    }

    public static Channel convert2ChannelVideo(Content model) {
        Channel item = new Channel();
        item.setId(model.getId());
        item.setName(model.getName());
        item.setUrl(model.getUrl());
        return item;
    }

    public static NewsModel convert2News(Content model) {
        NewsModel item = new NewsModel();
        item.setID(model.getIdInt());
        item.setCid(model.getCid());
        item.setPid(model.getPid());
        item.setTimeStamp(model.getTimeStamp());
        item.setTitle(model.getName());
        item.setShapo(model.getDescription());
        item.setUrl(model.getUrl());
        item.setBody(model.getBody());
        item.setType(model.getItemTypeInt());
        if (TextUtils.isEmpty(model.getImage())) {
            if (model.getListImage() != null && model.getListImage().size() != 0) {
                item.setImage(model.getListImage().get(0));
                item.setImage169(model.getListImage().get(0));
            }
        } else {
            item.setImage(model.getImage());
            item.setImage169(model.getImage());
        }
        return item;
    }

    public static TiinModel convert2Tiin(Content model) {
        TiinModel item = new TiinModel();
        item.setId(model.getIdInt());
        item.setCid(model.getCid());
        item.setPid(model.getPid());
        item.setUnixTime(model.getTimeStamp());
        item.setTitle(model.getName());
        item.setShapo(model.getDescription());
        item.setUrl(model.getUrl());
        item.setBody(model.getBody());
        item.setType(model.getItemTypeInt());
        if (TextUtils.isEmpty(model.getImage())) {
            if (model.getListImage() != null && model.getListImage().size() != 0) {
                item.setImage(model.getListImage().get(0));
                item.setImage169(model.getListImage().get(0));
            }
        } else {
            item.setImage(model.getImage());
            item.setImage169(model.getImage());
        }
        return item;
    }

    public static String convert2Log(Content model, LogApi.ContentType contentType) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tab_type", LogApi.TabType.TAB_HOME.VALUE);
            jsonObject.put("content_type", contentType.VALUE);
            jsonObject.put("content_id", model.getId());
            jsonObject.put("position", model.getPosition());
            jsonObject.put("link", model.getUrl());
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String convert2Log(ThreadMessage model, LogApi.ContentType contentType) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tab_type", LogApi.TabType.TAB_HOME.VALUE);
            jsonObject.put("content_type", contentType.VALUE);
            if (model.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                jsonObject.put("content_id", model.getSoloNumber());
            } else {
                jsonObject.put("content_id", model.getServerId());
            }
            jsonObject.put("position", LogApi.POS_HOME_CONTACT);
            jsonObject.put("link", "");
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String convert2Log(ItemMoreHome model, LogApi.ContentType contentType) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tab_type", LogApi.TabType.TAB_HOME.VALUE);
            jsonObject.put("content_type", contentType.VALUE);
            jsonObject.put("content_id", model.getFuncName());
            jsonObject.put("position", LogApi.POS_HOME_FEATURE);
            if (LogApi.FuncName.FUNC_WAP.VALUE.equals(model.getFuncName())) {
                ConfigTabHomeItem tabWap = ApplicationController.self().getConfigBusiness().getConfigTabHomeWap(model.getDeepLink());
                if (tabWap != null)
                    jsonObject.put("link", tabWap.getLink());
            } else {
                jsonObject.put("link", model.getDeepLink());
            }
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}

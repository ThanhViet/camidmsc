package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetListReward extends BaseRequest<WsGetListReward.Request> {
    public class Request {
        @SerializedName("page")
        public String page;
        @SerializedName("limit")
        public String limit;
        @SerializedName("giftId")
        public String giftId;
        @SerializedName("giftName")
        public String giftName;
        @SerializedName("gifttype")
        public String gifttype;
        @SerializedName("status")
        public String status = "1";

    }
}

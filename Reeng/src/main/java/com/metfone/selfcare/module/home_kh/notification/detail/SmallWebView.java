package com.metfone.selfcare.module.home_kh.notification.detail;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class SmallWebView extends WebView {
    private int realHeight = 0;

    public SmallWebView(Context context) {
        super(context);
    }

    public SmallWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmallWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SmallWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public int getRealHeight() {
        if (realHeight == 0)
            realHeight = computeVerticalScrollRange();
        return realHeight;
    }

}

package com.metfone.selfcare.module.newdetails.animation;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;

public class AnimationFactory {

    public static class ScaleInAnimation implements BaseAnimation {
        private static final float DEFAULT_SCALE_FROM = .5f;
        private final float mFrom;

        public ScaleInAnimation() {
            this(DEFAULT_SCALE_FROM);
        }

        public ScaleInAnimation(float from) {
            mFrom = from;
        }

        @Override
        public Animator[] getAnimators(View view) {
            ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", mFrom, 1f);
            ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", mFrom, 1f);
            return new ObjectAnimator[]{scaleX, scaleY};
        }
    }

    public static class AlphaInAnimation implements BaseAnimation {
        private static final float DEFAULT_ALPHA_FROM = 0f;
        private final float mFrom;

        public AlphaInAnimation() {
            this(DEFAULT_ALPHA_FROM);
        }

        public AlphaInAnimation(float from) {
            mFrom = from;
        }

        @Override
        public Animator[] getAnimators(View view) {
            return new Animator[]{ObjectAnimator.ofFloat(view, "alpha", mFrom, 1f)};
        }
    }

    public static class SlideInBottomAnimation implements BaseAnimation {
        @Override
        public Animator[] getAnimators(View view) {
            return new Animator[]{
                    ObjectAnimator.ofFloat(view, "translationY", view.getMeasuredHeight(), 0)
            };
        }
    }

    public static class SlideInLeftAnimation implements BaseAnimation {
        @Override
        public Animator[] getAnimators(View view) {
            return new Animator[]{
                    ObjectAnimator.ofFloat(view, "translationX", -view.getRootView().getWidth(), 0)
            };
        }
    }

    public static class SlideInRightAnimation implements BaseAnimation {
        @Override
        public Animator[] getAnimators(View view) {
            return new Animator[]{
                    ObjectAnimator.ofFloat(view, "translationX", view.getRootView().getWidth(), 0)
            };
        }
    }

}

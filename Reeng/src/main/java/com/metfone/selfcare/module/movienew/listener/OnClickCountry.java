/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/2
 *
 */

package com.metfone.selfcare.module.movienew.listener;

public interface OnClickCountry {
    void onClick(Object item, int position, boolean isChecked);
}

package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

/**
 * ViewHolder for History charge Data: Dae, Money, Data
 */
public class DataChargeChildrenViewHolder extends RecyclerView.ViewHolder {
    public AppCompatTextView mDate;
    public AppCompatTextView mMoney;
    public AppCompatTextView mData;

    public DataChargeChildrenViewHolder(View view) {
        super(view);
        mDate = view.findViewById(R.id.value_date_history_charge_5);
        mMoney = view.findViewById(R.id.value_money_history_charge_5);
        mData = view.findViewById(R.id.value_data_history_charge_5);
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.SearchHistory;
import com.metfone.selfcare.module.search.utils.ImageBusiness;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.OnClick;

public class HistorySearchHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_des)
    TextView tvDes;
    @BindView(R.id.icon_search)
    ImageView iconSearch;
    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;
    @BindView(R.id.tv_avatar)
    TextView tvAvatar;
    @BindView(R.id.icon_delete)
    ImageView ivDelete;
    @BindView(R.id.button_action_video)
    ImageView ivVideoCall;
    @BindView(R.id.button_action_call)
    ImageView ivCall;

    private SearchAllListener.OnAdapterClick listener;
    private SearchHistory data;
    private ApplicationController mApplication;
    private int sizeAvatar;
    private Activity activity;

    public HistorySearchHolder(View view, Activity activity, SearchAllListener.OnAdapterClick listener) {
        super(view);
        this.listener = listener;
        this.activity = activity;
        mApplication = (ApplicationController) activity.getApplication();
        sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.search_size_avatar_history);
    }

    public void bindData(Object item, int position) {
        if (item instanceof SearchHistory) {
            data = (SearchHistory) item;
            tvTitle.setText(data.getName());
            iconSearch.setVisibility(data.isShowIconSearch() ? View.VISIBLE : View.GONE);
            ivAvatar.setVisibility(data.isShowAvatar() ? View.VISIBLE : View.GONE);
            tvAvatar.setVisibility(View.GONE);
            ivVideoCall.setVisibility(View.GONE);
            ivCall.setVisibility(View.GONE);
            tvDes.setVisibility(View.GONE);
            if (data.getType() == SearchHistory.TYPE_CONTACT && data.getContact() != null) {
                PhoneNumber phoneNumber = data.getContact();
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(ivAvatar, tvAvatar, phoneNumber, sizeAvatar);
                if ("-1".equals(phoneNumber.getContactId())) {
                    if (ivVideoCall != null) ivVideoCall.setVisibility(View.GONE);
                    if (ivCall != null) ivCall.setVisibility(View.GONE);
                } else {
                    if (phoneNumber.isReeng()) {
                        tvDes.setVisibility(View.VISIBLE);
                        tvDes.setText(data.getContact().getJidNumber());
                        if (ivVideoCall != null) ivVideoCall.setVisibility(View.VISIBLE);
                        if (ivCall != null) ivCall.setVisibility(View.VISIBLE);
                    } else if (mApplication.getReengAccountBusiness().getCurrentAccount() != null &&
                            mApplication.getReengAccountBusiness().isViettel() && phoneNumber.isViettel()) {
                        tvDes.setVisibility(View.VISIBLE);
                        tvDes.setText(data.getContact().getJidNumber());
                        if (ivVideoCall != null) ivVideoCall.setVisibility(View.GONE);
                        if (ivCall != null) ivCall.setVisibility(View.VISIBLE);
                    } else {
                        if (ivVideoCall != null) ivVideoCall.setVisibility(View.GONE);
                        if (ivCall != null) ivCall.setVisibility(View.GONE);
                    }
                }
            } else if (data.getType() == SearchHistory.TYPE_CHANNEL && data.getChannel() != null) {
                ImageBusiness.setChannelVideo(data.getChannel().getUrlImage(), ivAvatar);
            } else if (data.getType() == SearchHistory.TYPE_CHAT && data.getThreadChat() != null) {
                ThreadMessage threadMessage = data.getThreadChat();
                String numberFriend = threadMessage.getSoloNumber();
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(numberFriend);
                StrangerPhoneNumber stranger = threadMessage.getStrangerPhoneNumber();
                boolean showCallFree;
                if (phoneNumber != null) {
                    mApplication.getAvatarBusiness().setPhoneNumberAvatar(ivAvatar, tvAvatar, phoneNumber, sizeAvatar);
                    showCallFree = phoneNumber.isReeng();
                } else {
                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(numberFriend);
                    showCallFree = nonContact != null && nonContact.isReeng();
                    if (threadMessage.isStranger()) {
                        if (stranger != null) {
                            mApplication.getAvatarBusiness().setStrangerAvatar(ivAvatar, tvAvatar,
                                    stranger, stranger.getPhoneNumber(), null, null, sizeAvatar);
                        } else {
                            mApplication.getAvatarBusiness().setUnknownNumberAvatar(ivAvatar, tvAvatar, numberFriend, sizeAvatar);
                        }
                    } else {
                        mApplication.getAvatarBusiness().setUnknownNumberAvatar(ivAvatar, tvAvatar, numberFriend, sizeAvatar);
                    }
                }
                if (!threadMessage.isStranger()) {
                    if ((showCallFree && mApplication.getReengAccountBusiness().isCallEnable())
                            || mApplication.getReengAccountBusiness().isEnableCallOut()) {
                        tvDes.setVisibility(View.VISIBLE);
                        tvDes.setText(numberFriend);
                        if (ivCall != null) ivCall.setVisibility(View.VISIBLE);
                        if (!showCallFree) {
                            if (ivVideoCall != null) ivVideoCall.setVisibility(View.GONE);
                        } else if (ivVideoCall != null) ivVideoCall.setVisibility(View.VISIBLE);
                    }
                }
            }
        } else {
            data = null;
            iconSearch.setVisibility(View.GONE);
            ivAvatar.setVisibility(View.GONE);
            tvAvatar.setVisibility(View.GONE);
            tvTitle.setText("");
        }
    }

    @OnClick({R.id.layout_root})
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxHistory && data != null) {
            ((SearchAllListener.OnClickBoxHistory) listener).onClickHistorySearchItem(data);
        }
    }

    @OnClick({R.id.icon_delete})
    public void onClickDeleteItem() {
        if (listener instanceof SearchAllListener.OnClickBoxHistory && data != null) {
            ((SearchAllListener.OnClickBoxHistory) listener).onClickDeleteItemHistorySearch(data);
        }
    }

    @OnClick({R.id.button_action_call})
    public void onClickCallItem() {
        if (mApplication != null && activity instanceof BaseSlidingFragmentActivity && !activity.isFinishing() && data != null) {
            if (data.getThreadChat() != null)
                mApplication.getCallBusiness().checkAndStartCall((BaseSlidingFragmentActivity) activity, data.getThreadChat().getSoloNumber(), false);
            else if (data.getContact() != null)
                mApplication.getCallBusiness().checkAndStartCall((BaseSlidingFragmentActivity) activity, data.getContact().getJidNumber(), false);
        }
    }

    @OnClick({R.id.button_action_video})
    public void onClickVideoCallItem() {
        if (mApplication != null && activity instanceof BaseSlidingFragmentActivity && !activity.isFinishing() && data != null) {
            if (data.getThreadChat() != null)
                mApplication.getCallBusiness().checkAndStartVideoCall((BaseSlidingFragmentActivity) activity, data.getThreadChat(), false);
            else if (data.getContact() != null)
                mApplication.getCallBusiness().checkAndStartVideoCall((BaseSlidingFragmentActivity) activity, data.getContact(), false);
        }
    }
}
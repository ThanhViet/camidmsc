/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataChallenge implements Serializable {

    private static final long serialVersionUID = -3321532298843906837L;

    @SerializedName("name")
    private String name;
    @SerializedName("packageFee")
    private long packageFee;    //giá gói data
    @SerializedName("descriptionUssd")
    private String description;

    //todo thông tin chi tiết tiền thưởng
    @SerializedName("isdnB")
    private String isdnB;   //Số thuê bao được mời
    @SerializedName("dataPackage")
    private String dataPackage; //Gói data đã mời
    @SerializedName("invitationTime")
    private String invitationTime;  //Thời gian mời
    @SerializedName("commision")
    private String commision;   //Hoa hồng được hưởng
    @SerializedName("status")
    private String status; //todo 0: mời thành công ,1: mời thất bại, 2 : đang chờ
    @SerializedName("price")
    private long price;
    @SerializedName("reason")
    private String reason;  //Lý do mời thất bại

    public String getName() {
        return name;
    }

    public long getPackageFee() {
        return packageFee;
    }

    public String getDescription() {
        return description;
    }

    public String getIsdnB() {
        return isdnB;
    }

    public String getDataPackage() {
        return dataPackage;
    }

    public String getInvitationTime() {
        return invitationTime;
    }

    public String getCommision() {
        return commision;
    }

    public long getCommisionLong() {
        try {
            return Long.parseLong(commision);
        } catch (Exception e) {

        }
        return 0;
    }

    public String getStatus() {
        return status;
    }

    public long getPrice() {
        return price;
    }

    public String getReason() {
        return reason;
    }
}

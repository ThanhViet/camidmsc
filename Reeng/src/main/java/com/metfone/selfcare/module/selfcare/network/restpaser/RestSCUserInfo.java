package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCUserInfo;

import java.io.Serializable;

public class RestSCUserInfo extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private SCUserInfo data;

    public SCUserInfo getData() {
        return data;
    }

    public void setData(SCUserInfo data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCUserInfo [data=" + data + "] errror " + getErrorCode();
    }
}

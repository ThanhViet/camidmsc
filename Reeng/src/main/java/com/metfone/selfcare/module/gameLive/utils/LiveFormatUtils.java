package com.metfone.selfcare.module.gameLive.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LiveFormatUtils {

    public static String formatNumber(int number) {
        DecimalFormat df = new DecimalFormat("###,###,###");
        DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance();
        sym.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(sym);

        return df.format(number);
    }

    public static String getStartTimeStr(long time) {
        Date d = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("E, d/M");
        String ret = sdf.format(d);
        if (ret.contains("Sun")) {
            ret = ret.replaceAll("Sun", "CHỦ NHẬT");
        } else {
            ret = ret.replaceAll("Mon", "2").replaceAll("Tue", "3").replaceAll("Wed", "4").replaceAll("Thu", "5")
                    .replaceAll("Fri", "6").replaceAll("Sat", "7");
            ret = "Thứ " + ret;
        }
        return ret;
    }

}

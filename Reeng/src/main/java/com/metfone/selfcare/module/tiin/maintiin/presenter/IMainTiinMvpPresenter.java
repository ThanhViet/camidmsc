package com.metfone.selfcare.module.tiin.maintiin.presenter;

import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.module.tiin.base.MvpPresenter;

public interface IMainTiinMvpPresenter extends MvpPresenter {
    void loadApiCategory();

    void loadCategory(SharedPrefs mPref);
}

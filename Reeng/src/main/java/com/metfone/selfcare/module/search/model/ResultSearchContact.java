/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/24
 *
 */

package com.metfone.selfcare.module.search.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ResultSearchContact implements Serializable {
    private ArrayList<Object> result;
    private String keySearch;
    private String oldKeySearch;
    private long timeSearch;

    public ArrayList<Object> getResult() {
        return result;
    }

    public void setResult(ArrayList<Object> result) {
        this.result = result;
    }

    public String getKeySearch() {
        return keySearch;
    }

    public void setKeySearch(String keySearch) {
        this.keySearch = keySearch;
    }

    public String getOldKeySearch() {
        return oldKeySearch;
    }

    public void setOldKeySearch(String oldKeySearch) {
        this.oldKeySearch = oldKeySearch;
    }

    public long getTimeSearch() {
        return timeSearch;
    }

    public void setTimeSearch(long timeSearch) {
        this.timeSearch = timeSearch;
    }

    @Override
    public String toString() {
        return "ResultSearchContact{" +
                "result=" + result +
                ", keySearch='" + keySearch + '\'' +
                ", oldKeySearch='" + oldKeySearch + '\'' +
                ", timeSearch=" + timeSearch +
                '}';
    }
}

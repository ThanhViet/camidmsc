package com.metfone.selfcare.module.keeng.widget.floatingView;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.KeengPlayerActivity;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;

public class KeengFloatingViewService extends Service implements PlayMusicController.OnPlayMusicStateChange {
    protected final String TAG = getClass().getSimpleName();
    protected final int TIME_SHOW_REMOVE_VIEW = 300;
    private ApplicationController mApplication;
    private MediaModel currentSong;
    private CountDownTimer countDownTimerMoveLeft;
    private CountDownTimer countDownTimerMoveRight;

    private WindowManager windowManager;
    private RelativeLayout layoutChatHead;
    private RelativeLayout layoutRemove;

    private ImageView imvChatHead, imvRemove;
    private ImageView btnPlay;
    private int x_init_cord, y_init_cord, x_init_margin, y_init_margin;
    private Point szWindow = new Point();
    private boolean isLeft = true;
    private RotateAnimationCustom rotateAnimation;
    private TouchManager touchManager;
    private int layoutRemoveWidth = 0, layoutRemoveHeight = 0;



    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        Log.d(TAG, "MusicFloatingView.onCreate()");
        mApplication = (ApplicationController) getApplicationContext();

        rotateAnimation = new RotateAnimationCustom(0.0f, 1.0f * 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(5000);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        rotateAnimation.setRepeatMode(Animation.INFINITE);
    }

    private void loadData() {
        currentSong = mApplication.getPlayMusicController().getCurrentSong();
        if (currentSong != null) {
            ImageBusiness.setImageNoAnime(currentSong.getImage(), R.drawable.ic_keeng, imvChatHead);

            startAnimation();
            setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void handleStart() {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PRIORITY_PHONE;
        }

        layoutRemove = (RelativeLayout) inflater.inflate(R.layout.layout_keeng_floating_remove, null);
        WindowManager.LayoutParams paramRemove = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);
        paramRemove.gravity = Gravity.TOP | Gravity.LEFT;

        layoutRemove.setVisibility(View.GONE);
        imvRemove = layoutRemove.findViewById(R.id.imvRemove);
        windowManager.addView(layoutRemove, paramRemove);


        layoutChatHead = (RelativeLayout) inflater.inflate(R.layout.layout_keeng_floating, null);
        imvChatHead = layoutChatHead.findViewById(R.id.imvChatHead);
        btnPlay = layoutChatHead.findViewById(R.id.btnPlay);

        loadData();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            windowManager.getDefaultDisplay().getSize(szWindow);
        } else {
            int w = windowManager.getDefaultDisplay().getWidth();
            int h = windowManager.getDefaultDisplay().getHeight();
            szWindow.set(w, h);
        }

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = - ViewUtils.dpToPx(60) / 5;
        params.y = 200;
        windowManager.addView(layoutChatHead, params);

        onTouchChatHeadAdvance();
    }

    private void onTouchChatHeadAdvance() {
        touchManager = new TouchManager(layoutChatHead, new TouchManager.BoundsChecker() {
            @Override
            public float stickyLeftSide(float screenWidth) {
                return - layoutChatHead.getWidth() / 5;
            }

            @Override
            public float stickyRightSide(float screenWidth) {
                return screenWidth - layoutChatHead.getWidth() + layoutChatHead.getWidth() / 5;
            }

            @Override
            public float stickyTopSide(float screenHeight) {
                return 0;
            }

            @Override
            public float stickyBottomSide(float screenHeight) {
                return screenHeight - layoutChatHead.getHeight() - getStatusBarHeight();
            }
        });
        touchManager.screenWidth(szWindow.x);
        touchManager.screenHeight(szWindow.y);
        touchManager.callback(new TouchManager.Callback() {

            @Override
            public void onClick(float x, float y) {
                Log.i(TAG, "State: onClick");
            }

            @Override
            public void onLongClick(float x, float y) {
                Log.i(TAG, "State: onLongClick + x: " + x + " y: " + y);
            }

            @Override
            public void onTouchOutside() {
                Log.i(TAG, "State: onTouchOutside");
            }

            @Override
            public void onTouched(float x, float y) {
                Log.i(TAG, "State: onTouched + x: " + x + " y: " + y);

                //touch 3s thi hien layout remove
                layoutRemoveWidth = imvRemove.getLayoutParams().width;
                layoutRemoveHeight = imvRemove.getLayoutParams().height;
                handlerRemove.postDelayed(runnableRemove, TIME_SHOW_REMOVE_VIEW);


            }

            @Override
            public void onMoved(float diffX, float diffY) {
                Log.i(TAG, "State: onMoved + diffY: " + diffX + " diffY: " + diffY);

                //Check vi tri neu cham vao layout remove
//                if(isIntersectWithTrash())
//                {
//
//                }
//                else
//                {
//
//                }
            }

            @Override
            public void onReleased(float x, float y) {
                Log.i(TAG, "State: onReleased + x: " + x + " y: " + y);

                //Tha tay ra thi an layout remove di
                layoutRemove.setVisibility(View.GONE);
                imvRemove.getLayoutParams().width = layoutRemoveWidth;
                imvRemove.getLayoutParams().height = layoutRemoveHeight;
            }

            @Override
            public void onAnimationCompleted() {
                Log.i(TAG, "State: onAnimationCompleted");
            }
        });
    }

    Handler handlerRemove = new Handler();
    Runnable runnableRemove = new Runnable() {

        @Override
        public void run() {
            layoutRemove.setVisibility(View.VISIBLE);
            showLayoutRemove();
        }
    };

    private void showLayoutRemove() {
        Log.d(TAG, "Show layout remove");
        if (windowManager == null) return;

        WindowManager.LayoutParams params = (WindowManager.LayoutParams) layoutRemove.getLayoutParams();
        int x_cord_remove = (szWindow.x - layoutRemoveWidth) / 2;
        int y_cord_remove = szWindow.y - (layoutRemoveHeight + getStatusBarHeight()) - ViewUtils.dpToPx(20);

        params.x = x_cord_remove;
        params.y = y_cord_remove;

        windowManager.updateViewLayout(layoutRemove, params);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void onTouchChatHead() {
        layoutChatHead.setOnTouchListener(new View.OnTouchListener() {
            long time_start = 0, time_end = 0;
            boolean isLongclick = false, inBounded = false;
            int remove_img_width = 0, remove_img_height = 0;

            Handler handler_longClick = new Handler();
            Runnable runnable_longClick = new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    Log.d(TAG, "Into runnable_longClick");

                    isLongclick = true;
                    layoutRemove.setVisibility(View.VISIBLE);
                    showLayoutRemove();
                }
            };

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) layoutChatHead.getLayoutParams();

                int x_cord = (int) event.getRawX();
                int y_cord = (int) event.getRawY();
                int x_cord_Destination, y_cord_Destination;

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        time_start = System.currentTimeMillis();
                        handler_longClick.postDelayed(runnable_longClick, TIME_SHOW_REMOVE_VIEW);

                        remove_img_width = imvRemove.getLayoutParams().width;
                        remove_img_height = imvRemove.getLayoutParams().height;

                        x_init_cord = x_cord;
                        y_init_cord = y_cord;

                        x_init_margin = layoutParams.x;
                        y_init_margin = layoutParams.y;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        int x_diff_move = x_cord - x_init_cord;
                        int y_diff_move = y_cord - y_init_cord;

                        x_cord_Destination = x_init_margin + x_diff_move;
                        y_cord_Destination = y_init_margin + y_diff_move;

                        if (isLongclick) {
                            int x_bound_left = szWindow.x / 2 - (int) (remove_img_width * 1.5);
                            int x_bound_right = szWindow.x / 2 + (int) (remove_img_width * 1.5);
                            int y_bound_top = szWindow.y - (int) (remove_img_height * 1.5);

                            if ((x_cord >= x_bound_left && x_cord <= x_bound_right) && y_cord >= y_bound_top) {
                                inBounded = true;

                                int x_cord_remove = (int) ((szWindow.x - (remove_img_height * 1.2)) / 2);
                                int y_cord_remove = (int) (szWindow.y - ((remove_img_width * 1.2) + getStatusBarHeight()));

                                if (imvRemove.getLayoutParams().height == remove_img_height) {
                                    imvRemove.getLayoutParams().height = (int) (remove_img_height * 1.2);
                                    imvRemove.getLayoutParams().width = (int) (remove_img_width * 1.2);

                                    WindowManager.LayoutParams param_remove = (WindowManager.LayoutParams) layoutRemove.getLayoutParams();
                                    param_remove.x = x_cord_remove;
                                    param_remove.y = y_cord_remove;

                                    windowManager.updateViewLayout(layoutRemove, param_remove);
                                }

                                layoutParams.x = x_cord_remove + (Math.abs(layoutRemove.getWidth() - layoutChatHead.getWidth())) / 2;
                                layoutParams.y = y_cord_remove + (Math.abs(layoutRemove.getHeight() - layoutChatHead.getHeight())) / 2;

                                windowManager.updateViewLayout(layoutChatHead, layoutParams);
                                break;
                            } else {
                                inBounded = false;
                                imvRemove.getLayoutParams().height = remove_img_height;
                                imvRemove.getLayoutParams().width = remove_img_width;

                                WindowManager.LayoutParams param_remove = (WindowManager.LayoutParams) layoutRemove.getLayoutParams();
                                int x_cord_remove = (szWindow.x - layoutRemove.getWidth()) / 2;
                                int y_cord_remove = szWindow.y - (layoutRemove.getHeight() + getStatusBarHeight());

                                param_remove.x = x_cord_remove;
                                param_remove.y = y_cord_remove;

                                windowManager.updateViewLayout(layoutRemove, param_remove);
                            }

                        }


                        layoutParams.x = x_cord_Destination;
                        layoutParams.y = y_cord_Destination;

                        windowManager.updateViewLayout(layoutChatHead, layoutParams);
                        break;
                    case MotionEvent.ACTION_UP:
                        isLongclick = false;
                        layoutRemove.setVisibility(View.GONE);
                        imvRemove.getLayoutParams().height = remove_img_height;
                        imvRemove.getLayoutParams().width = remove_img_width;
                        handler_longClick.removeCallbacks(runnable_longClick);

                        if (inBounded) {
                            onClickRemove();
                            inBounded = false;
                            break;
                        }


                        int x_diff = x_cord - x_init_cord;
                        int y_diff = y_cord - y_init_cord;

                        if (Math.abs(x_diff) < 5 && Math.abs(y_diff) < 5) {
                            time_end = System.currentTimeMillis();
                            if ((time_end - time_start) < 300) {
                                onClickChatHead();
                            }
                        }

                        y_cord_Destination = y_init_margin + y_diff;

                        int BarHeight = getStatusBarHeight();
                        if (y_cord_Destination < 0) {
                            y_cord_Destination = 0;
                        } else if (y_cord_Destination + (layoutChatHead.getHeight() + BarHeight) > szWindow.y) {
                            y_cord_Destination = szWindow.y - (layoutChatHead.getHeight() + BarHeight);
                        }
                        layoutParams.y = y_cord_Destination;

                        inBounded = false;
                        resetPosition(x_cord);

                        break;
                    default:
                        Log.d(TAG, "layoutChatHead.setOnTouchListener  -> event.getAction() : default");
                        break;
                }
                return true;
            }
        });
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);

        if (windowManager == null)
            return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            windowManager.getDefaultDisplay().getSize(szWindow);
        } else {
            int w = windowManager.getDefaultDisplay().getWidth();
            int h = windowManager.getDefaultDisplay().getHeight();
            szWindow.set(w, h);
        }

        WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) layoutChatHead.getLayoutParams();

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d(TAG, "MusicFloatingView.onConfigurationChanged -> landscap");
            if (layoutParams.y + (layoutChatHead.getHeight() + getStatusBarHeight()) > szWindow.y) {
                layoutParams.y = szWindow.y - (layoutChatHead.getHeight() + getStatusBarHeight());
                windowManager.updateViewLayout(layoutChatHead, layoutParams);
            }

            if (layoutParams.x != 0 && layoutParams.x < szWindow.x) {
                resetPosition(szWindow.x);
            }
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d(TAG, "MusicFloatingView.onConfigurationChanged -> portrait");

            if (layoutParams.x > szWindow.x) {
                resetPosition(szWindow.x);
            }
        }
    }

    private void resetPosition(int x_cord_now) {
        if (x_cord_now <= szWindow.x / 2) {
            isLeft = true;
            moveToLeft(x_cord_now);
        } else {
            isLeft = false;
            moveToRight(x_cord_now);
        }
    }

    private void moveToLeft(final int x_cord_now) {
        if (windowManager == null) return;
        final int x = szWindow.x - x_cord_now - 40;

        countDownTimerMoveLeft = new CountDownTimer(500, 5) {
            WindowManager.LayoutParams mParams = (WindowManager.LayoutParams) layoutChatHead.getLayoutParams();

            public void onTick(long t) {
                long step = (500 - t) / 5;
                mParams.x = 0 - (int) (double) bounceValue(step, x);
                if (windowManager != null)
                    windowManager.updateViewLayout(layoutChatHead, mParams);
            }

            public void onFinish() {
                mParams.x = -20;
                if (windowManager != null)
                    windowManager.updateViewLayout(layoutChatHead, mParams);
            }
        }.start();
    }

    private void moveToRight(final int x_cord_now) {
        if (windowManager == null) return;
        countDownTimerMoveRight = new CountDownTimer(500, 5) {
            WindowManager.LayoutParams mParams = (WindowManager.LayoutParams) layoutChatHead.getLayoutParams();

            public void onTick(long t) {
                long step = (500 - t) / 5;
                mParams.x = szWindow.x + (int) (double) bounceValue(step, x_cord_now) - layoutChatHead.getWidth();
                if (windowManager != null)
                    windowManager.updateViewLayout(layoutChatHead, mParams);
            }

            public void onFinish() {
                mParams.x = szWindow.x - layoutChatHead.getWidth() + 5;
                if (windowManager != null)
                    windowManager.updateViewLayout(layoutChatHead, mParams);
            }
        }.start();
    }

    private double bounceValue(long step, long scale) {
        return scale * Math.exp(-0.055 * step) * Math.cos(0.08 * step);
    }

    private int getStatusBarHeight() {
        return (int) Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "MusicFloatingView.onStartCommand() -> startId=" + startId);

        if (startId == Service.START_STICKY) {
            handleStart();
            PlayMusicController.addPlayMusicStateChange(this);
            return super.onStartCommand(intent, flags, startId);
        } else {
            return Service.START_NOT_STICKY;
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        PlayMusicController.removePlayMusicStateChange(this);

        if (countDownTimerMoveLeft != null) {
            countDownTimerMoveLeft.cancel();
            countDownTimerMoveLeft = null;
        }

        if (countDownTimerMoveRight != null) {
            countDownTimerMoveRight.cancel();
            countDownTimerMoveRight = null;
        }

        if (layoutChatHead != null) {
            windowManager.removeView(layoutChatHead);
        }

        if (layoutRemove != null) {
            windowManager.removeView(layoutRemove);
        }

        windowManager = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        Log.d(TAG, "MusicFloatingView.onBind()");
        return null;
    }

    private void onClickChatHead() {
        Intent it = new Intent(this, KeengPlayerActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(it);

        stopForeground(true);
        stopSelf();
    }

    private void onClickRemove() {
        //Stop Music
        mApplication.getPlayMusicController().closeMusic();

        //Stop service
//        stopService(new Intent(KeengFloatingViewService.this, KeengFloatingViewService.class));
        stopService();
    }

    private void stopService() {
        stopAnimation();
        stopForeground(true);
        stopSelf();
    }

    public static void stop(Context context) {
        if (context == null) return;
        context.stopService(new Intent(context, KeengFloatingViewService.class));
    }


    @Override
    public void onChangeStateRepeat(int state) {

    }

    @Override
    public void onChangeStateNone() {

    }

    @Override
    public void onChangeStateGetData() {
        setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
    }

    @Override
    public void onChangeStatePreparing(MediaModel song) {
        setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
    }

    @Override
    public void onChangeStatePlaying(MediaModel song) {
        setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
    }

    @Override
    public void onActionFromUser(MediaModel song) {

    }

    @Override
    public void onCloseMusic() {
        stopService();
    }

    @Override
    public void onCallLoadDataFail(String message) {

    }

    @Override
    public void onUpdateSeekBarProgress(int percent, int currentMediaPosition) {

    }

    @Override
    public void onUpdateSeekBarBuffering(int percent) {

    }

    @Override
    public void onChangeStateInfo(int state) {

    }

    @Override
    public void onUpdateData(MediaModel song) {
        loadData();
    }

    public void setStateMediaPlayer(int stateMediaPlayer) {
        switch (stateMediaPlayer) {
            case Constants.PLAY_MUSIC.PLAYING_NONE:
            case Constants.PLAY_MUSIC.PLAYING_GET_INFO:
            case Constants.PLAY_MUSIC.PLAYING_PREPARING:
            case Constants.PLAY_MUSIC.PLAYING_RETRIEVING:
                btnPlay.setImageResource(R.drawable.exo_icon_play);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PLAYING:
                btnPlay.setImageResource(R.drawable.exo_icon_pause);
                playAnimation();
                break;
            case Constants.PLAY_MUSIC.PLAYING_PAUSED:
                btnPlay.setImageResource(R.drawable.exo_icon_play);
                pauseAnimation();
                break;
            default:
                btnPlay.setImageResource(R.drawable.exo_icon_play);
                break;
        }
    }


    private void playAnimation() {
        if (rotateAnimation != null)
            rotateAnimation.setPause(false);
    }

    private void pauseAnimation() {
        if (rotateAnimation != null)
            rotateAnimation.setPause(true);
    }

    private void startAnimation() {
        stopAnimation();
        if (imvChatHead != null)
            imvChatHead.clearAnimation();
        if (rotateAnimation != null)
            rotateAnimation.setPause(true);
        if (imvChatHead != null)
            imvChatHead.startAnimation(rotateAnimation);
    }

    private void stopAnimation() {
        if (imvChatHead != null)
            imvChatHead.clearAnimation();
        if (rotateAnimation != null)
            rotateAnimation.setPause(true);
    }
}
package com.metfone.selfcare.module.tiin.category.view;

import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.util.List;

public interface CategoryTiinMvp extends MvpView {
    void loadDataSuccess(boolean flag);

    void binDataCategory(List<TiinModel> response);

    void bindDataEvent(TiinEventModel response);
}

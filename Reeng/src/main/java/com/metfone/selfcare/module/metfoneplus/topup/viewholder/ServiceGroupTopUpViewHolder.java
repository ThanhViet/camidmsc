package com.metfone.selfcare.module.metfoneplus.topup.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.model.ServiceGroupTopUpModel;

public class ServiceGroupTopUpViewHolder extends RecyclerView.ViewHolder {
    private ServiceTopUpViewHolder.EventListener eventListener;
    public ServiceGroupTopUpViewHolder(@NonNull View view, ServiceTopUpViewHolder.EventListener eventListener) {
        super(view);
        this.eventListener = eventListener;
    }

    public void bindView(ServiceGroupTopUpModel groupTopUpModel) {
        TextView tvTitle = itemView.findViewById(R.id.tvTitle);
        TextView tvDescription = itemView.findViewById(R.id.tvDescription);
        RecyclerView recyclerView = itemView.findViewById(R.id.recyclerView);
        tvTitle.setText(groupTopUpModel.getTitle());
        tvDescription.setText(groupTopUpModel.getDescription());
        ServiceTopUpViewHolder serviceTopUpViewHolder = new ServiceTopUpViewHolder(itemView.getRootView(), eventListener);
        BaseAdapter adapter = new BaseAdapter(groupTopUpModel.getTopUpModels(), itemView.getContext(), R.layout.item_service_topup, serviceTopUpViewHolder);
        recyclerView.setAdapter(adapter);
    }

}

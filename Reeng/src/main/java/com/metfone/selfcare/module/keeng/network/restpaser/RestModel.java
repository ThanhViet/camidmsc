/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.module.keeng.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.AllModel;

import java.io.Serializable;

public class RestModel extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("data")
    private AllModel data;

    public AllModel getData() {
        return data;
    }

    public void setData(AllModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestModel [data=" + data + "] errro " + getError();
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.video.event;

public class TabVideoReselectedEvent {
    private boolean reselectedHome;
    private int indexTabSelected = -1;

    public TabVideoReselectedEvent() {
    }

    public boolean isReselectedHome() {
        return reselectedHome;
    }

    public TabVideoReselectedEvent setReselectedHome(boolean reselectedHome) {
        this.reselectedHome = reselectedHome;
        return this;
    }

    public int getIndexTabSelected() {
        return indexTabSelected;
    }

    public TabVideoReselectedEvent setIndexTabSelected(int indexTabSelected) {
        this.indexTabSelected = indexTabSelected;
        return this;
    }

    @Override
    public String toString() {
        return "TabVideoReselectedEvent{" +
                "reselectedHome=" + reselectedHome +
                ", indexTabSelected=" + indexTabSelected +
                '}';
    }
}

package com.metfone.selfcare.module.newdetails.MainDetailNews.view;

import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.NewsResponse;

/**
 * Created by HaiKE on 8/21/17.
 */

public interface IMainNewsDetailView extends MvpView {
    void loadDataSuccess(boolean flag);
    void bindDataRelate(NewsResponse response);
}

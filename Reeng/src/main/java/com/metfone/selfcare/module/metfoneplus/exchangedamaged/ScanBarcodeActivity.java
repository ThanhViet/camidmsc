package com.metfone.selfcare.module.metfoneplus.exchangedamaged;

import android.content.Intent;
import android.os.Bundle;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ScanBarcodeActivity extends BaseSlidingFragmentActivity {
    @BindView(R.id.vScannerBarcodeZxing)
    ZXingScannerView vScannerBarcodeZxing;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_barcode);
        ButterKnife.bind(this);
        vScannerBarcodeZxing.setResultHandler(rawResult -> {
            String barcode = rawResult.getText();
            if (barcode != null) {
                Intent intent = new Intent();
                intent.putExtra("QR_CODE", barcode.trim());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        vScannerBarcodeZxing.setAutoFocus(true);
        vScannerBarcodeZxing.startCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();
        vScannerBarcodeZxing.stopCamera();
    }
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.video.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.reflect.TypeToken;
import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.AdsRegisterVip;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.model.tab_video.VideoBannerItem;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.video.adapter.VideoPagerAdapter;
import com.metfone.selfcare.module.video.listener.TabVideoListener;
import com.metfone.selfcare.module.video.model.TabModel;
import com.metfone.selfcare.module.video.model.VideoPagerModel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnTabListener;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.home.fragment.BaseHomeFragment;
import com.metfone.selfcare.v5.home.fragment.TabVideoFragmentV2;
import com.vtm.adslib.AdsNativeHelper;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;

public class VideoPagerFragment extends BaseHomeFragment implements OnInternetChangedListener
        , SwipeRefreshLayout.OnRefreshListener, TabVideoListener.OnAdapterClick
        , BaseAdapter.OnLoadMoreListener, OnClickMoreItemListener, OnTabListener {

    private static final int LIMIT = 20;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingView loadingView;

    private int position = -1;
    private String categoryId = "";
    private boolean isTabHot = false;
    private VideoApi mVideoApi;

    private boolean isLoadingVideo;
    private boolean isLoadingChannel;
    private boolean isLoadingSuggest;
    private boolean isLoadingNewVideo;
    private boolean canLoadMore;
    private boolean isLoadedVideo;
    private boolean isLoadedChannel;
    private boolean isLoadedSuggest;
    private boolean isLoadedNewVideo;
    private boolean onRefresh = false;

    private VideoPagerModel dataSuggest;
    private VideoPagerModel dataChannel;
    private VideoPagerModel dataNew;
    private VideoPagerModel dataFriend;
    private VideoPagerModel banner;
    private VideoPagerModel headerVideo;
    private ArrayList<VideoPagerModel> data;
    private VideoPagerAdapter adapter;
    private ListenerUtils mListenerUtils;
    private int offset;
    private String lastId;
    private LinearLayoutManager layoutManager;

    public static Bundle arguments(int position, int currentPosition, String categoryId, String contentType) {
        return new Bundler()
                .putInt(Constants.TabVideo.POSITION, position)
                .putInt(Constants.TabVideo.CURRENT_POSITION, currentPosition)
                .putString(Constants.TabVideo.CATEGORY_ID, categoryId)
                .putString(Constants.TabVideo.TYPE, contentType)
                .get();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_pager_content_v2;
    }

    @Override
    public void onCreateView() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryId = bundle.getString(Constants.TabVideo.CATEGORY_ID);
            position = bundle.getInt(Constants.TabVideo.POSITION);
            isTabHot = TabVideoFragmentV2.CONTENT_TYPE_HOT.equals(bundle.getString(Constants.TabVideo.TYPE));
        }
        Log.d(TAG, "onCreateView isTabHot: " + isTabHot + ", categoryId: " + categoryId + ", position: " + position);
        loadingView.setOnClickRetryListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData(true);
                loadChannel();
                if (isTabHot) {
                    loadSuggest();
                    loadNewVideo();
                    loadFriendsAreWatching();
                }
            }
        });
        swipeRefreshLayout.setColorSchemeColors(getColor());
        swipeRefreshLayout.setOnRefreshListener(this);
        if (data == null) data = new ArrayList<>();
        else data.clear();
        adapter = new VideoPagerAdapter(activity);
        adapter.setItems(data);
        adapter.setListener(this);
        layoutManager = new CustomLinearLayoutManager(activity, LinearLayout.VERTICAL, false);
        BaseAdapter.setupVerticalRecyclerView(activity, recyclerView, layoutManager, adapter, true, isTabHot ? 3 : 2);
        BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_video);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        mVideoApi = app.getApplicationComponent().providerVideoApi();
        if (canLazyLoad()) {
            new Handler().postDelayed(() -> {
                loadData(Utilities.isEmpty(data));
                loadChannel();
                if (isTabHot) {
                    loadSuggest();
                    loadNewVideo();
                    loadFriendsAreWatching();
                }
            }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData(Utilities.isEmpty(data));
            loadChannel();
            if (isTabHot) {
                loadSuggest();
                loadNewVideo();
                loadFriendsAreWatching();
            }
        }
    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        if (isLoadingVideo) {
            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        } else {
            onRefresh = true;
            offset = 0;
            lastId = "";
            loadData(Utilities.isEmpty(data));
            loadChannel();
            if (isTabHot) {
                loadSuggest();
                loadNewVideo();
                loadFriendsAreWatching();
            }
            AdsManager.getInstance().reloadAdsNative();
        }
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad()) {
            loadData(Utilities.isEmpty(data));
            loadChannel();
            if (isTabHot) {
                loadSuggest();
                loadNewVideo();
                loadFriendsAreWatching();
            }
        }
    }

    public void hideRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    int index = 0;
    int totalAds = 0;
    private void loadData(boolean showLoading) {
        isLoadedVideo = false;
        if (isLoadingVideo || mVideoApi == null) return;
        isLoadingVideo = true;
        canLoadMore = false;
        if (showLoading) {
            if (loadingView != null) loadingView.showLoading();
            offset = 0;
            lastId = "";

            index = 0;
            totalAds = 0;
        }
        String type = "";
        if (isTabHot) type = "1";
        mVideoApi.getVideosByCategoryV2(categoryId, offset, LIMIT, lastId, type, new ApiCallbackV2<ArrayList<Object>>() {
            @Override
            public void onSuccess(String title, ArrayList<Object> result) {
                if (recyclerView != null) recyclerView.stopScroll();
                if (data == null) data = new ArrayList<>();
                boolean addHeader = false;
                if (offset == 0) {
                    data.clear();
                    if (isTabHot) {
                        if (banner != null) data.add(banner);
                        if (dataSuggest != null) data.add(dataSuggest);
                        if (dataChannel != null) data.add(dataChannel);
                        if (dataNew != null) data.add(dataNew);
                        if (dataFriend != null) data.add(dataFriend);
                        addHeader = true;
                    } else {
                        if (dataChannel != null) {
                            data.add(dataChannel);
                            addHeader = true;
                        }
                    }
                    headerVideo = new VideoPagerModel();
                    headerVideo.setId(categoryId);
                    headerVideo.setType(VideoPagerModel.TYPE_HEADER_LIST_VIDEO);
                    if (TextUtils.isEmpty(title)) {
                        if (res != null)
                            headerVideo.setTitle(res.getString(isTabHot ? R.string.box_hot_video : R.string.box_new_video));
                    } else {
                        headerVideo.setTitle(title);
                    }
                }
                if (Utilities.notEmpty(result)) {
                    if (addHeader) data.add(headerVideo);
                    for (Object item : result) {
                        if (item instanceof VideoBannerItem) {
                            if (Utilities.notEmpty(data) && data.get(0) != null && data.get(0).getType() == VideoPagerModel.TYPE_VIDEO_BANNER) {
                                data.remove(0);
                            }
                            banner = new VideoPagerModel();
                            banner.setType(VideoPagerModel.TYPE_VIDEO_BANNER);
                            banner.setObject(item);
                            data.add(0, banner);
                        } else if (item != null) {
                            VideoPagerModel model = new VideoPagerModel();
                            model.setType(VideoPagerModel.TYPE_VIDEO_NORMAL);
                            model.setObject(item);
                            data.add(model);
                            index ++;
                            //add ads
                            if(AdsUtils.checkShowAds() && AdsNativeHelper.getInstance().canShowAd())
                            {
                                if (totalAds <= 8 && index % 5 == 2)
                                {
                                    VideoPagerModel adsModel = new VideoPagerModel();
                                    adsModel.setType(VideoPagerModel.TYPE_ADS);
                                    data.add(adsModel);
                                    totalAds ++;
                                }
                            }
                        }
                    }
                    canLoadMore = result.size() >= LIMIT;
                }
                if (adapter != null) adapter.notifyDataSetChanged();
                if (Utilities.isEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedEmpty();
                } else {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            }

            @Override
            public void onError(String s) {
                if (offset == 0) {
                    if (data == null) data = new ArrayList<>();
                    data.clear();
                    if (isTabHot) {
                        if (banner != null) data.add(banner);
                        if (dataSuggest != null) data.add(dataSuggest);
                        if (dataChannel != null) data.add(dataChannel);
                        if (dataNew != null) data.add(dataNew);
                        if (dataFriend != null) data.add(dataFriend);
                    } else {
                        if (dataChannel != null) data.add(0, dataChannel);
                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                }
                if (Utilities.isEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedError();
                } else {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            }

            @Override
            public void onComplete() {
                onRefresh = false;
                isLoadedVideo = true;
                isLoadingVideo = false;
                isDataInitiated = true;
                hideRefresh();
            }
        });
    }

    private void loadSuggest() {
        isLoadedSuggest = false;
        if (isLoadingSuggest || mVideoApi == null) return;
        isLoadingSuggest = true;
        mVideoApi.getSuggestVideo(categoryId, 0, LIMIT, onRefresh, new ApiCallbackV2<ArrayList<Video>>() {
            @Override
            public void onSuccess(String title, ArrayList<Video> result) {
                if (data == null) data = new ArrayList<>();
                int index = 0;
                for (int i = 0; i < data.size(); i++) {
                    if (data.get(i).getType() == VideoPagerModel.TYPE_VIDEO_BANNER) {
                        index++;
                    } else if (data.get(i).getType() == VideoPagerModel.TYPE_SUGGEST_LIST) {
                        //index++;
                        data.remove(i);
                        break;
                    } else if (i > 4) {
                        break;
                    }
                }
                if (Utilities.notEmpty(result)) {
                    dataSuggest = new VideoPagerModel();
                    dataSuggest.setId(categoryId);
                    dataSuggest.setType(VideoPagerModel.TYPE_SUGGEST_LIST);
                    if (TextUtils.isEmpty(title)) {
                        if (res != null)
                            dataSuggest.setTitle(res.getString(R.string.suggest_for_you));
                    } else
                        dataSuggest.setTitle(title);
                    Collections.shuffle(result);
                    dataSuggest.getList().addAll(result);
                    dataSuggest.setShowViewAll(true);
                    data.add(index, dataSuggest);
                } else {
                    dataSuggest = null;
                }
                if (adapter != null) adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {
                isLoadedSuggest = true;
                isLoadingSuggest = false;
                isDataInitiated = true;
                if (Utilities.notEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            }
        });
    }

    private void loadChannel() {
        isLoadedChannel = false;
        if (isLoadingChannel || mVideoApi == null) return;
        isLoadingChannel = true;
        mVideoApi.getHotChannel(isTabHot, categoryId, 0, LIMIT, new ApiCallbackV2<ArrayList<Channel>>() {
            @Override
            public void onSuccess(String title, ArrayList<Channel> result) {
                if (data == null) data = new ArrayList<>();
                int index = 0;
                for (int i = 0; i < data.size(); i++) {
                    if (data.get(i).getType() == VideoPagerModel.TYPE_VIDEO_BANNER) {
                        index++;
                    } else if (data.get(i).getType() == VideoPagerModel.TYPE_SUGGEST_LIST) {
                        index++;
                    } else if (data.get(i).getType() == VideoPagerModel.TYPE_CHANNEL_LIST) {
                        data.remove(i);
                        break;
                    } else if (i > 4) {
                        break;
                    }
                }
                boolean addHeader = !isTabHot && data != null && data.size() > 1 && isLoadedVideo && headerVideo != null;
                if (Utilities.notEmpty(result)) {
                    dataChannel = new VideoPagerModel();
                    dataChannel.setId(categoryId);
                    dataChannel.setType(VideoPagerModel.TYPE_CHANNEL_LIST);
                    if (TextUtils.isEmpty(title)) {
                        if (res != null)
                            dataChannel.setTitle(res.getString(R.string.trending_channel));
                    } else
                        dataChannel.setTitle(title);
                    //Collections.shuffle(result);
                    dataChannel.getList().addAll(result);
                    data.add(index, dataChannel);
                    if (addHeader) {
                        index = 0;
                        for (int i = 0; i < data.size(); i++) {
                            if (data.get(i).getType() == VideoPagerModel.TYPE_VIDEO_BANNER) {
                                index++;
                            } else if (data.get(i).getType() == VideoPagerModel.TYPE_CHANNEL_LIST) {
                                index++;
                            } else if (data.get(i).getType() == VideoPagerModel.TYPE_HEADER_LIST_VIDEO) {
                                data.remove(i);
                                break;
                            } else if (i > 4) {
                                break;
                            }
                        }
                        data.add(index, headerVideo);
                    }
                } else {
                    dataChannel = null;
                }
                if (adapter != null) adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {
                isLoadedChannel = true;
                isLoadingChannel = false;
                isDataInitiated = true;
                if (Utilities.notEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            }
        });
    }

    private void loadNewVideo() {
        isLoadedNewVideo = false;
        if (isLoadingNewVideo || mVideoApi == null) return;
        isLoadingNewVideo = true;
        String type = "";
        if (isTabHot) type = "2";
        mVideoApi.getVideosByCategoryV2(categoryId, offset, LIMIT, lastId, type, new ApiCallbackV2<ArrayList<Object>>() {
            @Override
            public void onSuccess(String title, ArrayList<Object> result) {
                if (data == null) data = new ArrayList<>();
                int index = 0;
                for (int i = 0; i < data.size(); i++) {
                    if (data.get(i).getType() == VideoPagerModel.TYPE_VIDEO_BANNER) {
                        index++;
                    } else if (data.get(i).getType() == VideoPagerModel.TYPE_SUGGEST_LIST) {
                        index++;
                    } else if (data.get(i).getType() == VideoPagerModel.TYPE_CHANNEL_LIST) {
                        index++;
                    } else if (data.get(i).getType() == VideoPagerModel.TYPE_NEW_VIDEO_LIST) {
                        data.remove(i);
                        break;
                    } else if (i > 4) {
                        break;
                    }
                }
                if (Utilities.notEmpty(result)) {
                    dataNew = new VideoPagerModel();
                    dataNew.setId(categoryId);
                    dataNew.setType(VideoPagerModel.TYPE_NEW_VIDEO_LIST);
                    if (TextUtils.isEmpty(title)) {
                        if (res != null)
                            dataNew.setTitle(res.getString(R.string.box_new_video));
                    } else
                        dataNew.setTitle(title);
                    dataNew.getList().addAll(result);
                    dataNew.setShowViewAll(true);
                    data.add(index, dataNew);
                } else {
                    dataNew = null;
                    banner = null;
                }
                if (adapter != null) adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {
                isLoadedNewVideo = true;
                isLoadingNewVideo = false;
                isDataInitiated = true;
                if (Utilities.notEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            }
        });
    }

    private void loadFriendsAreWatching() {
    }

    @Override
    public void onClickTitleBox(VideoPagerModel item, int position) {
        int type = item.getType();
        TabModel model = null;
        switch (type) {
            case VideoPagerModel.TYPE_SUGGEST_LIST:
                model = new TabModel(item.getId(), TabModel.TYPE_RECOMMEND_VIDEO, item.getTitle());
                break;
            case VideoPagerModel.TYPE_NEW_VIDEO_LIST:
                model = new TabModel(item.getId(), TabModel.TYPE_NEW_VIDEO, item.getTitle());
                break;
            case VideoPagerModel.TYPE_HEADER_LIST_VIDEO:
                model = new TabModel(item.getId(), TabModel.TYPE_HOT_VIDEO, item.getTitle());
                break;
            case VideoPagerModel.TYPE_CHANNEL_LIST:
                //model = new TabModel(item.getId(), TabModel.TYPE_TOP_CHANNEL, item.getTitle());
                break;
        }
        if (model != null) NavigateActivityHelper.navigateToTabVideoCategoryDetail(activity, model);
    }

    @Override
    public void onClickChannelItem(Object item, int position) {
        if (item instanceof Channel) {
            app.getApplicationComponent().providesUtils().openChannelInfo(activity, (Channel) item);
        }
    }

    @Override
    public void onClickMoreChannelItem(Object item, int position) {
        if (item instanceof Channel) {
            DialogUtils.showOptionChannelItem(activity, item, this);
        }
    }

    private void onClickVideo(final Video video) {
        if (video == null || activity == null || app == null) return;
        if (Utilities.equals(categoryId, "1001")) {
            ArrayList<String> ids = SharedPrefs.getInstance().get(Constants.TabVideo.CACHE_IDS, new TypeToken<ArrayList<String>>() {
            }.getType());
            String id = video.getId();
            if (Utilities.isEmpty(ids)) ids = new ArrayList<>();
            if (Utilities.notEmpty(id) && !ids.contains(id)) {
                ids.add(id);
                if (ids.size() > 50) {
                    ids.remove(0);
                }
                SharedPrefs.getInstance().put(Constants.TabVideo.CACHE_IDS, ids);
            }
        }
        video.setCategoryId(categoryId);
        AdsRegisterVip ads = app.getReengAccountBusiness().showDialogAdsRegisterVip(categoryId);
        if (ads != null) {
//            Log.i(TAG, "ads: " + new Gson().toJson(ads));
//            ReportHelper.reportError(app, "ADS_VIDEO_THANHNT", "ads: " + new Gson().toJson(ads));
            String data = "timestamp=" + System.currentTimeMillis() + "|cateId=" + categoryId;
            ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_POPUP_VIP, data);

            ReportHelper.showDialogFromTabVideo(app, activity, ads,
                    result -> app.getApplicationComponent().providesUtils().openVideoDetail(activity, video));
        } else {
            app.getApplicationComponent().providesUtils().openVideoDetail(activity, video);
        }
    }

    @Override
    public void onClickVideoItem(Object item, int position) {
        if (item instanceof VideoPagerModel) {
            VideoPagerModel model = (VideoPagerModel) item;
            if (model.getObject() instanceof Video) {
                onClickVideo((Video) model.getObject());
            }
        } else if (item instanceof Video) {
            onClickVideo((Video) item);
        }
    }

    @Override
    public void onClickMoreVideoItem(Object item, int position) {
        if (item instanceof VideoPagerModel) {
            VideoPagerModel model = (VideoPagerModel) item;
            if (model.getObject() instanceof Video) {
                final Video video = (Video) model.getObject();
                DialogUtils.showOptionVideoItem(activity, video, this);
            }
        } else if (item instanceof Video) {
            DialogUtils.showOptionVideoItem(activity, (Video) item, this);
        }
    }

    @Override
    public void onClickChannelVideoItem(Object item, int position) {
        if (item instanceof VideoPagerModel) {
            VideoPagerModel model = (VideoPagerModel) item;
            if (model.getObject() instanceof Video) {
                Channel channel = ((Video) model.getObject()).getChannel();
                if (channel != null)
                    app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
            }
        } else if (item instanceof Video) {
            Channel channel = ((Video) item).getChannel();
            if (channel != null)
                app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
        }
    }

    @Override
    public void onLoadMore() {
        if (canLoadMore && !isLoadingVideo) {
            offset = offset + LIMIT;
            Log.i(TAG, "onLoadMore loadData ...");
            loadData(false);
        }
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            if (menuId != Constants.MENU.MENU_EXIT && ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                activity.showDialogLogin();
                return;
            }
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_SAVE_VIDEO:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).saveVideoFromMenu((Video) object);
                        activity.showToast(R.string.videoSavedToLibrary);
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).watchLaterVideoFromMenu((Video) object);
                        activity.showToast(R.string.add_later_success);
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (object instanceof Video) {
                        FeedModelOnMedia feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia((Video) object);
                        new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                    break;
            }
        }
    }

    private void scrollToTop() {
        if (layoutManager != null && recyclerView != null) {
            try {
                recyclerView.stopScroll();
                layoutManager.smoothScrollToPosition(recyclerView, null, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTabReselected(int currentPosition) {
        Log.i(TAG, "onTabReselected currentPosition: " + currentPosition + ", position: " + position + ", isVisibleToUser: " + isVisibleToUser);
        if (currentPosition == position) {
            scrollToTop();
        }
    }

    @Override
    public void onTabSelected(int currentPosition) {
        Log.i(TAG, "onTabSelected currentPosition: " + currentPosition + ", position: " + position + ", isVisibleToUser: " + isVisibleToUser);
        if (currentPosition == position && isVisibleToUser) {
            new Handler().postDelayed(this::onRefresh, 600L);
        }
    }

    @Override
    public void onDisableLoading() {

    }
}

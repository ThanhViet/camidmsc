package com.metfone.selfcare.module.newdetails.ChildDetailNews.presenter;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.newdetails.ChildDetailNews.fragment.NewsDetailFragment;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.NewsContentResponse;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.viettel.util.LogDebugHelper;

import org.json.JSONException;

/**
 * Created by Huongnd on 9/25/18.
 */

public class NewsDetailPresenter implements INewsDetailPresenter {
    public static final String TAG = NewsDetailPresenter.class.getSimpleName();
    private MvpView mMvpView;
    NetNewsApi mNewsApi;
    long startTimeLoadContent;
    long startTimeLoadRelate;
    String apiUrlLoadContent = "";

    public NewsDetailPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void saveFontSizeCache(int size) {
    }

    @Override
    public void loadData(NewsModel model) {
        startTimeLoadContent = System.currentTimeMillis();
        apiUrlLoadContent = mNewsApi.getApiUrl(NetNewsApi.GET_NEWS_CONTENT)
                + "/" + model.getPid() + "/" + model.getCid() + "/" + model.getID();
        mNewsApi.getNewsContent(model, mContentCallback);
    }

    @Override
    public void loadDataRelate(NewsModel model, int page, long unixTime) {
        startTimeLoadRelate = System.currentTimeMillis();
        mNewsApi.getNewsHot(model, page, unixTime, mLoadNewsHotCallback);
    }

    @Override
    public void loadNewsStatus(String url) {
        mNewsApi.getDetailUrl(url, false, new ApiCallbackV2<RestAllFeedsModel>() {
            @Override
            public void onSuccess(String msg, RestAllFeedsModel result) throws JSONException {
                if (!isViewAttached() || !(mMvpView instanceof NewsDetailFragment)) {
                    return;
                }
                if (result != null && Utilities.notEmpty(result.getData())) {
                    ((NewsDetailFragment) mMvpView).loadDataFeed(result.getData().get(0));
                }
                ((NewsDetailFragment) mMvpView).loadStatusSucess(true);
            }

            @Override
            public void onError(String s) {
                if (!isViewAttached() || !(mMvpView instanceof NewsDetailFragment)) {
                    return;
                }
                ((NewsDetailFragment) mMvpView).loadStatusSucess(false);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void handleApiError(Exception error) {

    }

    @Override
    public void setUserAsLoggedOut() {

    }

    @Override
    public void onAttach(MvpView mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    private HttpCallBack mContentCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(mMvpView instanceof NewsDetailFragment)) {
                return;
            }
            Gson gson = new Gson();
            NewsContentResponse childNewsResponse = gson.fromJson(data, NewsContentResponse.class);
            ((NewsDetailFragment) mMvpView).bindData(childNewsResponse);
            ((NewsDetailFragment) mMvpView).loadDataSuccess(true);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_DETAIL, (endTime - startTimeLoadContent) + "", startTimeLoadContent + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            LogDebugHelper.getInstance().logDebugContent("News detail :" + data + " | " + apiUrlLoadContent);
            if (childNewsResponse.getError() != null || childNewsResponse.getData() == null || childNewsResponse.getData().getBody().size() == 0) {
                LogDebugHelper.getInstance().logDebugContent("News detail error:" + data + " | " + apiUrlLoadContent);
                ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_NEWS_DETAIL, "News detail error:" + data + " | " + apiUrlLoadContent);
            }
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            long endTime = System.currentTimeMillis();
            Log.d(TAG, "loadContent: onFailure - " + message);
            if (!isViewAttached() || !(mMvpView instanceof NewsDetailFragment)) {
                return;
            }
            ((NewsDetailFragment) mMvpView).loadDataSuccess(false);
            ((NewsDetailFragment) mMvpView).bindData(null);

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_DETAIL, (endTime - startTimeLoadContent) + "", startTimeLoadContent + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_NEWS_DETAIL, "News detail error:" + message + " | " + apiUrlLoadContent);
            LogDebugHelper.getInstance().logDebugContent("News detail error:" + message + " | " + apiUrlLoadContent);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    HttpCallBack mLoadNewsHotCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(mMvpView instanceof NewsDetailFragment)) {
                return;
            }
            Gson gson = new Gson();
            NewsResponse childNewsResponse = gson.fromJson(data, NewsResponse.class);

            ((NewsDetailFragment) mMvpView).bindDataRelate(childNewsResponse);
            ((NewsDetailFragment) mMvpView).loadDataRelateSuccess(true);

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_RELATE, (endTime - startTimeLoadRelate) + "", startTimeLoadRelate + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            long endTime = System.currentTimeMillis();
            Log.d(TAG, "loadNewsHot: onFailure - " + message);
            if (!isViewAttached() || !(mMvpView instanceof NewsDetailFragment)) {
                return;
            }

            ((NewsDetailFragment) mMvpView).loadDataRelateSuccess(false);

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_RELATE, (endTime - startTimeLoadRelate) + "", startTimeLoadRelate + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

}
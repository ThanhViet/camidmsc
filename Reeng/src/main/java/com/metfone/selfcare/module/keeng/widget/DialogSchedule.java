package com.metfone.selfcare.module.keeng.widget;

import android.content.Context;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.AlarmUtils;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.keeng.utils.SharedPref;


/**
 * Created by namnh40 on 10/19/2017.
 */

public abstract class DialogSchedule extends BottomSheetDialog {
    public static final String SCHEDULE_TURN_OFF = "SCHEDULE_TURN_OFF";
    public static final String SCHEDULE_TURN_OFF_TIME = "SCHEDULE_TURN_OFF_TIME";
    public static final String SCHEDULE_STOP_MUSIC = "SCHEDULE_STOP_MUSIC";
    public static final String SCHEDULE_STOP_MUSIC_TIME = "SCHEDULE_STOP_MUSIC_TIME";

    int value = 0;
    CountDownTimer countDownSchedule;
    SharedPref mPref;
    TextView tvTime;
    TextView tvCountTime;
    SeekBar seekBar;
    TextView btnSubmit;
    TextView btnCancel;

    public DialogSchedule(@NonNull Context context) {
        super(context);
        init();
    }

    public DialogSchedule(@NonNull Context context, @StyleRes int theme) {
        super(context, theme);
        init();
    }

    public DialogSchedule(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    private void init() {
        setContentView(R.layout.dialog_schedule);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvCountTime = (TextView) findViewById(R.id.tv_count_time);
        seekBar = (SeekBar) findViewById(R.id.seekbar_timer);
        btnSubmit = (TextView) findViewById(R.id.button_submit);
        btnCancel = (TextView) findViewById(R.id.button_dismiss);
        setCancelable(true);
        setListener();
    }

    private void setListener() {
        mPref = SharedPref.newInstance(getContext());
        boolean flag = mPref.getBoolean(SCHEDULE_STOP_MUSIC, true);
        long now = System.currentTimeMillis();
        long time = mPref.getLong(SCHEDULE_STOP_MUSIC_TIME, now);
        final long temp = time - now;
        if (temp <= 0) {
            flag = false;
        }
        btnSubmit.setEnabled(true);
        final boolean finalFlag = flag;
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalFlag) {
                    mPref.putBoolean(SCHEDULE_STOP_MUSIC, false);
                    mPref.putLong(SCHEDULE_STOP_MUSIC_TIME, 0);
                    AlarmUtils.scheduleStopMusic(getContext(), 0, true);
                    onCancelSchedule();
                    dismiss();
                } else {
                    long timeOff = System.currentTimeMillis() + value * DateTimeUtils.MINUTE;
                    AlarmUtils.scheduleStopMusic(getContext(), timeOff, false);
                    mPref.putBoolean(SCHEDULE_STOP_MUSIC, true);
                    mPref.putLong(SCHEDULE_STOP_MUSIC_TIME, timeOff);
                    onSubmitSchedule();
                    setListener();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        if (flag) {
            tvCountTime.setVisibility(View.VISIBLE);
            tvTime.setVisibility(View.GONE);
            seekBar.setVisibility(View.GONE);
            btnSubmit.setText(R.string.cancel);
            btnSubmit.setBackgroundResource(R.color.gray);
            tvCountTime.setText(DateTimeUtils.formatmmss(temp));
            countDownSchedule = new CountDownTimer(temp, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (tvCountTime != null)
                        tvCountTime.setText(DateTimeUtils.formatmmss(millisUntilFinished));
                }

                @Override
                public void onFinish() {
                    dismiss();
                }
            };
            countDownSchedule.start();
        } else {
            tvCountTime.setVisibility(View.GONE);
            tvTime.setVisibility(View.VISIBLE);
            seekBar.setVisibility(View.VISIBLE);
            btnSubmit.setText(R.string.accept);
            btnSubmit.setBackgroundResource(R.drawable.bg_button_green);
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (tvTime != null) {
                        value = (int) ((((double) progress) / 100) * 60);
                        if (value <= 0) {
                            value = 1;
                        }
                        tvTime.setText(value + " " + getContext().getString(R.string.minutes));
                        btnSubmit.setEnabled(true);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
            value = getStopMusicSeekBar(flag, temp);
            seekBar.setProgress(getSeekBar());
            tvTime.setText(value + " " + getContext().getString(R.string.minutes));
        }
    }

    public int getStopMusicSeekBar(boolean flag, long temp) {
        if (flag) {
            int value = (int) (temp / DateTimeUtils.MINUTE);
            if (temp % DateTimeUtils.MINUTE > 0)
                return value + 1;
            else
                return value;
        }
        return 30;
    }

    public int getSeekBar() {
        return (int) ((((double) value) * 100) / 60);
    }

    @Override
    public void dismiss() {
        if (countDownSchedule != null)
            countDownSchedule.cancel();
        super.dismiss();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        dismiss();
    }

    public abstract void onCancelSchedule();
    public abstract void onSubmitSchedule();
}
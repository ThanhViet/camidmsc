/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.search.adapter.SearchAllAdapter;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.utils.ImageBusiness;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.view.tab_video.SubscribeChannelLayout;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.OnClick;

public class ChannelVideoDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    RoundedImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_view_all)
    @Nullable
    TextView tvViewAll;
    @BindView(R.id.tv_number_follow)
    @Nullable
    TextView tvNumberFollow;
    @BindView(R.id.button_subscribe)
    @Nullable
    SubscribeChannelLayout btnSubscribe;
    private SearchAllListener.OnAdapterClick listener;
    private Channel data;
    private Activity activity;

    public ChannelVideoDetailHolder(View view, Activity activity, final SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
        this.activity = activity;
        if (type == SearchAllAdapter.TYPE_CHANNEL) {
            int width = SearchUtils.getWidthChannelVideo();
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = width;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
//        if (btnSubscribe != null)
//            btnSubscribe.setSubscribeChannelListener(new SubscribeChannelLayout.SubscribeChannelListener() {
//                @Override
//                public void onOpenApp(Channel channel, boolean isInstall) {
//
//                }
//
//                @Override
//                public void onSub(Channel channel) {
//                    if (listener instanceof SearchAllListener.OnClickBoxVideo)
//                        ((SearchAllListener.OnClickBoxVideo) listener).onClickSubscribeChannelItem(channel);
//                }
//            });
    }

    private String getTextFollow(long numFollow) {
        if (numFollow <= 0) return "";
        if (activity != null) {
            if (numFollow == 1)
                return String.format(activity.getString(R.string.onmedia_follower), "1");
            else
                return String.format(activity.getString(R.string.onmedia_followers), Utilities.shortenLongNumber(numFollow));
        }
        return String.valueOf(numFollow);
    }

    public void bindData(Object item, int position, String keySearch) {
        if (item instanceof Channel) {
            data = (Channel) item;
            if (tvViewAll != null) tvViewAll.setVisibility(View.GONE);
            if (tvTitle != null) {
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(data.getName());
            }
            if (tvNumberFollow != null) {
                tvNumberFollow.setText(getTextFollow(data.getNumFollow()));
                tvNumberFollow.setVisibility((data.getNumFollow() > 0) ? View.VISIBLE : View.GONE);
            }
//            if (btnSubscribe != null) btnSubscribe.setChannel(data);
            ImageBusiness.setChannelVideo(data.getUrlImage(), ivCover);
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxVideo && data != null) {
            ((SearchAllListener.OnClickBoxVideo) listener).onClickChannelItem(data);
        }
    }
}
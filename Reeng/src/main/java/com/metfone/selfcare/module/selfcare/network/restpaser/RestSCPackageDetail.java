package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCPackage;

import java.io.Serializable;

public class RestSCPackageDetail extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private SCPackage data;

    public SCPackage getData() {
        return data;
    }

    public void setData(SCPackage data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCPackageDetail [data=" + data + "] errror " + getErrorCode();
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.holder.MoreDataDetailHolder;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;

public class MoreDataAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    public static final int TYPE_NORMAL = 2;

    private OnMyViettelListener listener;

    public MoreDataAdapter(Activity activity) {
        super(activity);
    }

    public void setListener(OnMyViettelListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_NORMAL) {
            return new MoreDataDetailHolder(layoutInflater.inflate(R.layout.holder_more_data_detail_mvt, parent, false), listener);
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(getItem(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item != null) return TYPE_NORMAL;
        return 0;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movienew.holder;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;

public class MovieContinueHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_resume)
    LinearLayout layout_resume;
    @BindView(R.id.img_movie)
    @Nullable
    ImageView imgMovie;
    @BindView(R.id.txt_friend_name)
    @Nullable
    AppCompatTextView txtFilmName;
    @BindView(R.id.progress_bar_time_seek)
    @Nullable
    ProgressBar pbTimeSeek;

    private TabMovieListener.OnAdapterClick listener;
    private Activity activity;

    public MovieContinueHolder(View view, Activity activity, TabMovieListener.OnAdapterClick listener) {
        super(view);
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    public void bindData(Object item, int position) {
        ListLogWatchResponse.Result resultItem = (ListLogWatchResponse.Result) item;
        Glide.with(activity)
                .load(resultItem.getImagePath()).error(R.drawable.cinema_loading_bg).placeholder(R.drawable.cinema_loading_bg)
                .into(imgMovie);
        imgMovie.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) {
                    listener.onClickMovieItem(resultItem, getAdapterPosition());
                }
            }
        });

        txtFilmName.setText(resultItem.getName());
        if(!StringUtils.isEmpty(resultItem.getTimeSeek())){
            try{
                double timeSeek = Double.parseDouble(resultItem.getTimeSeek());
                double duration = Double.parseDouble(resultItem.getDuration());
                if(timeSeek > 0){
                    int progress = (int) ((timeSeek/60000)*100/(duration/60));
                    pbTimeSeek.setVisibility(View.VISIBLE);
                    pbTimeSeek.setProgress(progress);
                }else{
                    pbTimeSeek.setVisibility(View.INVISIBLE);
                }
            }catch (Exception e){

            }
        }else{
            pbTimeSeek.setVisibility(View.INVISIBLE);
        }
        layout_resume.setOnClickListener(v->{
            listener.playFilm(resultItem,position,true);
        });
    }
}
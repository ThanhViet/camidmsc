package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCBanner;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCBanner extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCBanner> data;

    public ArrayList<SCBanner> getData() {
        return data;
    }

    public void setData(ArrayList<SCBanner> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCBanner [data=" + data + "] errror " + getErrorCode();
    }
}

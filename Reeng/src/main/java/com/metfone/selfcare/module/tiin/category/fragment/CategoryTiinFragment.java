package com.metfone.selfcare.module.tiin.category.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.CustomLoadMoreView;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.activitytiin.TiinActivity;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.category.adapter.CategoryTiinAdapter;
import com.metfone.selfcare.module.tiin.category.presenter.CategoryTiinPresenter;
import com.metfone.selfcare.module.tiin.category.presenter.ICategoryTiinMvpPresenter;
import com.metfone.selfcare.module.tiin.category.view.CategoryTiinMvp;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CategoryTiinFragment extends BaseFragment implements CategoryTiinMvp, BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener, TiinListener.onCategoryItemListener {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerviewCategory;
    @BindView(R.id.loadingView)
    View loadingView;

    Unbinder unbinder;
    View notDataView, errorView;
    private LinearLayoutManager layoutManager;
    private List<TiinModel> datas;
    private CategoryTiinAdapter adapter;
    private int categoryId = -1;
    private int currentPage = 1;
    private ICategoryTiinMvpPresenter mPresenter;
    private boolean isRefresh;
    private String title;
    private String hashTag; //biến truyền vào tham số gọi api hashTag

    public static CategoryTiinFragment newInstance(Bundle bundle) {
        CategoryTiinFragment fragment = new CategoryTiinFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_tiin, container, false);
        if (mPresenter == null) {
            mPresenter = new CategoryTiinPresenter();
        }
        unbinder = ButterKnife.bind(this, view);
        mPresenter.onAttach(this);
        setUp();

        return view;
    }

    private void setUp() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            title = bundle.getString(ConstantTiin.KEY_TITLE);
            tvTitle.setVisibility(View.VISIBLE);
            categoryId = bundle.getInt(ConstantTiin.KEY_CATEGORY, -1);
            hashTag = bundle.getString(ConstantTiin.KEY_HASHTAG, "");
            tvTitle.setText(title);
        }
        datas = new ArrayList<>();
        isRefresh = true;
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorNewBg));
        }
        if (datas == null || datas.size() == 0) {
            loadingView.setVisibility(View.VISIBLE);
            if (categoryId == 9 || categoryId == 8 || categoryId == 2 || categoryId == 11 || categoryId == 4 || categoryId == 10 || categoryId == 3 || categoryId == 6 || categoryId == 93 || categoryId == 12 || categoryId == 5 || categoryId == 94 || categoryId == 120) {
                mPresenter.getCategory(categoryId, currentPage, 10);
            } else if (!TextUtils.isEmpty(hashTag)) {
                mPresenter.getHashTag(currentPage, hashTag);
            } else if (categoryId == -1) {
                mPresenter.getMostView(0, 0, currentPage, 10);
            } else {
                if (categoryId != -1) {
                    mPresenter.getListEvent(categoryId, currentPage, 10);
                }
            }

        }

        if (categoryId == 101 || categoryId == 6) { //todo anh, nguoi dep
            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            recyclerviewCategory.setLayoutManager(staggeredGridLayoutManager);
            recyclerviewCategory.setPadding(recyclerviewCategory.getPaddingLeft(), recyclerviewCategory.getTop(), getResources().getDimensionPixelOffset(R.dimen.v5_spacing_normal), recyclerviewCategory.getPaddingBottom());
            adapter = new CategoryTiinAdapter(getBaseActivity(), R.layout.holder_lady_new, datas, categoryId, this);
        } else if (categoryId == 94) { //todo video
            layoutManager = new LinearLayoutManager(getTiinActivity());
            if (recyclerviewCategory.getItemDecorationCount() <= 0) {
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerviewCategory.setLayoutManager(layoutManager);
                recyclerviewCategory.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
            adapter = new CategoryTiinAdapter(getTiinActivity(), R.layout.holder_large_video_tiin, datas, categoryId, this);
        } else if (categoryId == 9 || categoryId == 8 || categoryId == 2 || categoryId == 120 || categoryId == 11 || categoryId == 4 || categoryId == 10 || categoryId == 3 || categoryId == 93 || categoryId == 12 || categoryId == 5) {
            layoutManager = new LinearLayoutManager(getTiinActivity());
            if (recyclerviewCategory.getItemDecorationCount() <= 0) {
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerviewCategory.setLayoutManager(layoutManager);
                recyclerviewCategory.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
            adapter = new CategoryTiinAdapter(getTiinActivity(), R.layout.holder_large_tiin, datas, categoryId, this);
        } else {
            layoutManager = new LinearLayoutManager(getTiinActivity());
            if (recyclerviewCategory.getItemDecorationCount() <= 0) {
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerviewCategory.setLayoutManager(layoutManager);
                recyclerviewCategory.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
            adapter = new CategoryTiinAdapter(getTiinActivity(), R.layout.holder_special_tiin, datas, categoryId, this);
        }
        recyclerviewCategory.setHasFixedSize(true);
        recyclerviewCategory.setItemAnimator(new DefaultItemAnimator());
        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(this);
        adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
        recyclerviewCategory.setAdapter(adapter);

        notDataView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_nodata, (ViewGroup) recyclerviewCategory.getParent(), false);
        notDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
        errorView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_failed, (ViewGroup) recyclerviewCategory.getParent(), false);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });

        layout_refresh.setOnRefreshListener(this);
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        if (loadingView != null) {
            loadingView.setVisibility(View.GONE);
        }
        hideRefresh();
        if (!flag) {
            loadingFail();
        }
    }

    public void loadingFail() {
        if (isRefresh) {
            adapter.setEmptyView(errorView);
        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public void binDataCategory(List<TiinModel> response) {
        if (datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if (response != null) {

            loadingComplete(response);
        } else {
            loadingFail();
        }

    }

    @Override
    public void bindDataEvent(TiinEventModel response) {
        if (datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if (response != null) {

            loadingComplete(response.getData());
        } else {
            loadingFail();
        }
    }

    public void loadingComplete(List<TiinModel> response) {
        int mCurrentCounter = response.size();
//        if(mCurrentCounter > 0)
//            unixTime = response.get(response.size() - 1).getUnixTime();
        if (isRefresh) {
            if (mCurrentCounter == 0) {
                adapter.setEmptyView(notDataView);
            } else {
                datas.clear();
                adapter.setNewData(response);
                datas.addAll(response);
            }
        } else {
            if (mCurrentCounter == 0) {
                adapter.loadMoreEnd();
            } else {
                adapter.addData(response);
                datas.addAll(response);
                adapter.loadMoreComplete();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onLoadMoreRequested() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                currentPage++;
                isRefresh = false;
                if (categoryId == 9 || categoryId == 8 || categoryId == 2 || categoryId == 11 || categoryId == 4 || categoryId == 10 || categoryId == 3 || categoryId == 6 || categoryId == 93 || categoryId == 12 || categoryId == 5 || categoryId == 94) {
                    mPresenter.getCategory(categoryId, currentPage, 10);
                } else if (!TextUtils.isEmpty(hashTag)) {
                    mPresenter.getHashTag(currentPage, hashTag);
                } else if (categoryId == -1) {
                    mPresenter.getMostView(0, 0, currentPage, 10);
                } else {
                    if (categoryId != -1) {
                        mPresenter.getListEvent(categoryId, currentPage, 10);
                    }
                }
            }
        }, 1000);
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        if (categoryId == 9 || categoryId == 8 || categoryId == 2 || categoryId == 11 || categoryId == 4 || categoryId == 10 || categoryId == 3 || categoryId == 6 || categoryId == 93 || categoryId == 12 || categoryId == 5 || categoryId == 94) {
            mPresenter.getCategory(categoryId, currentPage, 10);
        } else if (!TextUtils.isEmpty(hashTag)) {
            mPresenter.getHashTag(currentPage, hashTag);
        } else if (categoryId == -1) {
            mPresenter.getMostView(0, 0, currentPage, 10);
        } else {
            if (categoryId != -1) {
                mPresenter.getListEvent(categoryId, currentPage, 10);
            }
        }
    }

    @Override
    public void onItemClick(TiinModel model) {
        readTiin(model);
    }

    @OnClick(R.id.btnBack)
    public void onClickBack() {
        getTiinActivity().finish();
    }

    @Override
    public void onItemClickMore(TiinModel model) {
        DialogUtils.showOptionTiinItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionTiin(object, menuId);
            }
        });
    }

    @OnClick(R.id.ivSearch)
    public void onItemClickSearch() {
        Utilities.openSearch(getBaseActivity(), Constants.TAB_TIIN_HOME);
    }

    @Override
    public void onItemClickHashTag(TiinModel model) {
        Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
        intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY);
        intent.putExtra(ConstantTiin.KEY_CATEGORY, ConstantTiin.CATEGOTY_HASHTAG);
        intent.putExtra(ConstantTiin.KEY_HASHTAG, model.getHasTagSlug());
        intent.putExtra(ConstantTiin.KEY_TITLE, model.getHasTagName());
        getBaseActivity().startActivity(intent);
    }
}

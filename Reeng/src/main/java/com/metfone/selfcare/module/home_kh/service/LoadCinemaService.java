package com.metfone.selfcare.module.home_kh.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.module.movie.model.HomeCinemaDataCache;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.movienew.model.Category;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.HomeResult;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;
import com.metfone.selfcare.module.movienew.model.evenbus.UpdateDataFromService;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LoadCinemaService extends Service {
    private static final String TAG = "LoadCinemaService";
    public static final String BROAD_CAST_UPDATE_DATA_CINEMA = "data_cinema";
    private static final int TOTAL_API = 5; // tang len khi load them API
    private UserInfoBusiness userInfoBusiness;
    private UserInfo userInfo;
    private MoviePagerModel[] mMoviePagerModels;
    private int doneAllAPI = TOTAL_API;
    private String mLabelAvailable = "";
    private static LoadCinemaService self;
    private static Handler mHandler;
    private static int mPage = 1;
    private ArrayList<Http> requests = new ArrayList<>();
    private static boolean isRunning = false;
    private String mLastRecentContact;
    private Movie mCurrentMovie;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        self = this;
        loadData();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    public static void start(Context context) {
        if (context == null) return;
        stop(context);
        if (!isMyServiceRunning(context) && LoadCinemaService.self() == null) {
            final Intent intent = new Intent(context, LoadCinemaService.class);
            mPage = 1;
            try{
                context.startService(intent);
            }catch (Exception e){
                isRunning = false;
            }
        }
    }

    public static LoadCinemaService self() {
        return self;
    }

    public static boolean isMyServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            List<ActivityManager.RunningServiceInfo> list = manager.getRunningServices(Integer.MAX_VALUE);
            for (int i = 0; i < list.size(); i++) {
                ActivityManager.RunningServiceInfo service = list.get(i);
                if (service != null && service.service != null && LoadCinemaService.class.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void stop(Context context) {
        self = null;
        if (context == null) return;
        context.stopService(new Intent(context, LoadCinemaService.class));
        Log.d(TAG, "stop");
    }

    @Override
    public boolean stopService(Intent name) {
        self = null;
        return super.stopService(name);
    }

    private void loadData() {
        Log.d(TAG, "loadData : " + System.currentTimeMillis());
        HomeCinemaDataCache.reset();
        HomeCinemaDataCache.setMoviePagerModels(null);
        if (requests != null && requests.size() > 0) {
            for (Http http : requests) {
                if (http != null) {
                    http.cancel();
                }
            }
            requests.clear();
        }
        isRunning = true;
        mMoviePagerModels = new MoviePagerModel[10];
        userInfoBusiness = new UserInfoBusiness(this);
        userInfo = userInfoBusiness.getUser();
        getCategoryTitle();
        getMoviesForYou(mMoviePagerModels);
        getHomeMovieList(userInfo != null ? String.valueOf(userInfo.getUser_id()) : "1", mMoviePagerModels, false);
        getContinueWatchingList(mMoviePagerModels, false, true);
        if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.MOCHA_OPEN_ID) {
            ContactBusiness contactBusiness = ApplicationController.self().getContactBusiness();
            if (!contactBusiness.isInitArrayListReady()) {
                contactBusiness.initArrayListPhoneNumber();
            }
            if (contactBusiness.getRecentPhoneNumber() != null &&
                    contactBusiness.getRecentPhoneNumber().size() > 0 &&
                    !TextUtils.isEmpty(contactBusiness.getRecentPhoneNumber().get(0).getName())) {
                mLastRecentContact = contactBusiness.getRecentPhoneNumber().get(0).getJidNumber();
                if(contactBusiness.getRecentPhoneNumber().get(0).getName() != null){
                    HomeCinemaDataCache.setWatchedFriendName(contactBusiness.getRecentPhoneNumber().get(0).getName());
                }else{
                    HomeCinemaDataCache.setWatchedFriendName(contactBusiness.getRecentPhoneNumber().get(0).getJidNumber());
                }
                getWatchedByFriend(mMoviePagerModels, contactBusiness.getRecentPhoneNumber().get(0).getJidNumber());
            }else{
                updateDone();
            }
        }else{
            updateDone();
        }
    }

    private void getCategoryTitle() {
        Log.d(TAG, "getCategoryTitle start " + System.currentTimeMillis());
        MovieApi.getInstance().getCategoryByIds("868462", new ApiCallbackV2<List<Category>>() {
            @Override
            public void onSuccess(String msg, List<Category> result) throws JSONException {
                Log.d(TAG, "getCategoryTitle end " + System.currentTimeMillis());
                if (result != null && result.size() > 0) {
                    mLabelAvailable = result.get(0).getCategoryName();
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {
                updateDone();
            }
        });
    }

    public void getWatchedByFriend(MoviePagerModel[] moviePagerModels, String phoneNumber) {
        Http http = MovieApi.getInstance().getListLogWatchFriend(phoneNumber, new ApiCallbackV2<ListLogWatchResponse>() {
            @Override
            public void onSuccess(String msg, ListLogWatchResponse result) throws JSONException {
                if (result != null) {
                    if (result.getResult().size() > 0) {
                        MoviePagerModel model = new MoviePagerModel();
                        model.setType(MoviePagerModel.TYPE_WATCHED_BY_FRIEND);
                        model.setTitle(getString(R.string.cinema_watched_by_friend));
                        model.getList().addAll(result.getResult());
                        moviePagerModels[7] = model;
                    }
                }
            }

            @Override
            public void onError(String s) {
            }

            @Override
            public void onComplete() {
                updateDone();
            }
        });
        requests.add(http);
    }

    private void getMoviesForYou(MoviePagerModel[] moviePagerModels) {
        Log.d(TAG, "getMoviesForYou start " + System.currentTimeMillis());
        MovieApi.getInstance().getFilmForUser(new ApiCallbackV2<ArrayList<HomeData>>() {
            @Override
            public void onSuccess(String msg, ArrayList<HomeData> result) throws JSONException {
                Log.d(TAG, "getMoviesForYou end " + System.currentTimeMillis());
                if (result != null && result.size() > 0) {
                    MoviePagerModel model = new MoviePagerModel();
                    model.setType(MoviePagerModel.TYPE_BOX_CONTENT);
                    model.setTitle(getString(R.string.cinema_movie_for_you));
                    model.setMovieKind(MovieKind.TYPE_MOVIE4YOU);
                    SubtabInfo subtabInfo = new SubtabInfo();
                    subtabInfo.setType(result.get(0).getCategories().get(0).getType());
                    subtabInfo.setCategoryId(String.valueOf(result.get(0).getCategories().get(0).getId()));
                    subtabInfo.setCategoryName(result.get(0).getCategories().get(0).getCategoryName());
                    model.setSubtabInfo(subtabInfo);
                    MoviePagerModel gridItem = new MoviePagerModel();
                    gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
                    gridItem.getList().addAll(result);
                    model.getList().add(gridItem);
                    moviePagerModels[3] = model;
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {
                updateDone();
            }
        });
    }

    private void getContinueWatchingList(MoviePagerModel[] moviePagerModels, boolean isLoadWatchedByFriend, boolean getHomeMovieList) {
        Log.d(TAG, "getContinueWatchingList start " + System.currentTimeMillis());
        MovieApi.getInstance().getListLogWatch(new ApiCallbackV2<ListLogWatchResponse>() {
            @Override
            public void onSuccess(String msg, ListLogWatchResponse result) throws JSONException {
                Log.d(TAG, "getContinueWatchingList end " + System.currentTimeMillis());
                if (result != null) {
                    if (result.getResult().size() > 0) {
                        MoviePagerModel model = new MoviePagerModel();
                        if (isLoadWatchedByFriend) {
                            model.setType(MoviePagerModel.TYPE_WATCHED_BY_FRIEND);
                            model.setTitle(getString(R.string.cinema_watched_by_friend));
                            model.getList().addAll(result.getResult());
                            moviePagerModels[7] = model;
                        } else {
                            model.setType(MoviePagerModel.TYPE_BOX_WATCHED);
                            model.setTitle(getString(R.string.cinema_continue_watching));
                            model.setMovieKind(MovieKind.TYPE_WATCHED);
                            MoviePagerModel gridItem = new MoviePagerModel();
                            gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
                            gridItem.getList().addAll(removeDuplicateFilm(result.getResult()));
                            model.getList().add(gridItem);
                            moviePagerModels[2] = model;
                        }
                    }
                }
            }

            @Override
            public void onError(String s) {
            }

            @Override
            public void onComplete() {
                updateDone();
            }
        });
    }

    private void getHomeMovieList(String ref_id, MoviePagerModel[] moviePagerModels, Boolean checkRefresh) {
        Log.d(TAG, "getHomeMovieList start " + System.currentTimeMillis());
        MovieApi.getInstance().getHomeAppV2(Constants.LIMIT_LOAD_HOME,
                (mPage - 1) * Constants.LIMIT_LOAD_HOME, new ApiCallbackV2<HomeResult>() {
                    @Override
                    public void onSuccess(String msg, HomeResult result) throws JSONException {
                        Log.d(TAG, "getHomeMovieList end " + System.currentTimeMillis());
                        try {
                            if (result != null) {
                                Field[] fields = HomeResult.class.getDeclaredFields();
                                for (Field item : fields) {
                                    if (item != null) {
                                        item.setAccessible(true);
                                        String name = item.getName();
                                        List<HomeData> homeDatas = (List<HomeData>) item.get(result);
                                        if (homeDatas != null) {
                                            int size = homeDatas.size();
                                            if (size > 0) {
                                                MoviePagerModel model = new MoviePagerModel();
                                                switch (name) {
                                                    //TODO Hien tai su dung recently add theo yeu cau KH
//                                                    case MovieKind.TYPE_RECOMMEND:
//                                                        model.setType(MoviePagerModel.TYPE_SLIDER_BANNER);
//                                                        model.setTitle(name);
//                                                        model.getList().addAll(homeDatas);
//                                                        moviePagerModels[0] = model;
//                                                        break;
                                                    case MovieKind.TYPE_TRENDING:
                                                        model.setType(MoviePagerModel.TYPE_BOX_CONTENT);
                                                        model.setTitle(getString(R.string.cinema_trending));
                                                        model.setMovieKind(MovieKind.TYPE_TRENDING);
                                                        SubtabInfo subtabInfo = new SubtabInfo();
                                                        if (!Utilities.isEmpty(homeDatas) && !Utilities.isEmpty(homeDatas.get(0).getCategories())) {
                                                            subtabInfo.setType(homeDatas.get(0).getCategories().get(0).getType());
                                                            subtabInfo.setCategoryId(String.valueOf(homeDatas.get(0).getCategories().get(0).getId()));
                                                            subtabInfo.setCategoryName(homeDatas.get(0).getCategories().get(0).getCategoryName());
                                                        }
                                                        model.setSubtabInfo(subtabInfo);
                                                        if (homeDatas.size() > 0) {
                                                            MoviePagerModel gridItem = new MoviePagerModel();
                                                            gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
                                                            gridItem.getList().addAll(homeDatas);
                                                            model.getList().add(gridItem);
                                                        }
                                                        moviePagerModels[1] = model;
                                                        break;
                                                    case MovieKind.TYPE_RECENTLYADDED:
                                                        model.setType(MoviePagerModel.TYPE_BOX_CONTENT);
                                                        model.setTitle(getString(R.string.cinema_recently_added));
                                                        model.setMovieKind(MovieKind.TYPE_RECENTLYADDED);
                                                        subtabInfo = new SubtabInfo();
                                                        if (!Utilities.isEmpty(homeDatas) && !Utilities.isEmpty(homeDatas.get(0).getCategories())) {
                                                            subtabInfo.setType(homeDatas.get(0).getCategories().get(0).getType());
                                                            subtabInfo.setCategoryId(String.valueOf(homeDatas.get(0).getCategories().get(0).getId()));
                                                            subtabInfo.setCategoryName(homeDatas.get(0).getCategories().get(0).getCategoryName());
                                                        }
                                                        model.setSubtabInfo(subtabInfo);
                                                        if (homeDatas.size() > 0) {
                                                            MoviePagerModel gridItem = new MoviePagerModel();
                                                            gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
                                                            gridItem.getList().addAll(homeDatas);
                                                            model.getList().add(gridItem);
                                                        }
                                                        moviePagerModels[4] = model;
                                                        //TODO Hien tai su dung recently add theo yeu cau KH
                                                        MoviePagerModel bannerModel = new MoviePagerModel();
                                                        bannerModel.setType(MoviePagerModel.TYPE_SLIDER_BANNER);
                                                        bannerModel.setTitle(name);
                                                        bannerModel.getList().addAll(homeDatas);
                                                        moviePagerModels[0] = bannerModel;
                                                        break;
                                                    case MovieKind.TYPE_AVAILABLE:
                                                        List<HomeData> arrData = homeDatas;
                                                        for (HomeData homeData : arrData) {
                                                            if (StringUtils.isEmpty(homeData.getTrailerUrl())) {
                                                                homeDatas.remove(homeData);
                                                            }
                                                        }
                                                        if (homeDatas.size() > 0) {
                                                            model.setType(MoviePagerModel.TYPE_AVAILABLE_NOW);
                                                            model.setTitle(mLabelAvailable);
                                                            model.getList().add(homeDatas.get(0));
                                                            moviePagerModels[5] = model;
                                                        }
                                                        break;
                                                    case MovieKind.TYPE_CUSTOM_TOPIC:
                                                        model.setType(MoviePagerModel.TYPE_CUSTOM_TOPIC);
                                                        if (homeDatas.size() > 0) {
                                                            MoviePagerModel gridItem = new MoviePagerModel();
                                                            gridItem.setType(MoviePagerModel.TYPE_CUSTOM_TOPIC);
                                                            gridItem.getList().addAll(homeDatas);
                                                            model.getList().add(gridItem);
                                                        }
                                                        moviePagerModels[6] = model;
                                                        break;
                                                }
                                            } else {
                                                switch (name) {
                                                    case MovieKind.TYPE_TRENDING:
                                                        if(moviePagerModels[1] != null){
                                                            moviePagerModels[1].getList().clear();
                                                        }
                                                        break;
                                                    case MovieKind.TYPE_RECENTLYADDED:
                                                        if(moviePagerModels[4] != null){
                                                            moviePagerModels[4].getList().clear();
                                                        }
                                                        break;
                                                    case MovieKind.TYPE_CUSTOM_TOPIC:
                                                        if(moviePagerModels[6] != null){
                                                            ((MoviePagerModel) moviePagerModels[6].getList().get(0)).getList().clear();
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String s) {
                    }

                    @Override
                    public void onComplete() {
                        updateDone();
                    }
                });
    }

    private ArrayList<ListLogWatchResponse.Result> removeDuplicateFilm(ArrayList<ListLogWatchResponse.Result> data) {
        if (data == null) {
            return null;
        }
        ArrayList<ListLogWatchResponse.Result> results = new ArrayList<>();
        HashMap<String, ListLogWatchResponse.Result> hashMap = new HashMap<>();
        for (ListLogWatchResponse.Result item : data) {
            if (item.getIdGroup() == null || item.getIdGroup().toString().equals("0")) {
                results.add(item);
            } else {
                if (hashMap.get(item.getIdGroup().toString()) == null) {
                    results.add(item);
                    hashMap.put(item.getIdGroup().toString(), item);
                }
            }
        }
        return results;
    }

    private synchronized void updateDone() {
        if (doneAllAPI > 0) {
            doneAllAPI--;
            if (doneAllAPI == 0) {
                Log.d(TAG, "Done data : " + System.currentTimeMillis());
                HomeCinemaDataCache.setMoviePagerModels(mMoviePagerModels);
                sendDataToCinema();
                stopSelf();
                self = null;
                isRunning = false;
            }
        }
    }

    public static boolean isRunning(){
        return isRunning;
    }

    private void sendDataToCinema() {
        EventBus.getDefault().postSticky(new UpdateDataFromService());
    }
}

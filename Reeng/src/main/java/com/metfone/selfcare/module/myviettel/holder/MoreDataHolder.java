/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.adapter.MoreDataAdapter;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;
import com.metfone.selfcare.module.myviettel.model.MoreDataProvisional;

import java.util.ArrayList;

import butterknife.BindView;

public class MoreDataHolder extends BaseAdapter.ViewHolder {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private MoreDataAdapter adapter;
    private ArrayList<Object> datas;

    public MoreDataHolder(Activity activity, View view, OnMyViettelListener listener) {
        super(view);
        datas = new ArrayList<>();
        adapter = new MoreDataAdapter(activity);
        adapter.setListener(listener);
        adapter.setItems(datas);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, true);
    }

    public void bindData(Object item, int position) {
        if (item instanceof MoreDataProvisional) {
            MoreDataProvisional data = (MoreDataProvisional) item;
            if (tvTitle != null)
                tvTitle.setText(data.isHasRegistered() ? R.string.buy_more_data : R.string.register_now);
            if (datas != null) {
                datas.clear();
                datas.addAll(data.getList());
            }
            if (adapter != null) adapter.notifyDataSetChanged();
        }
    }
}

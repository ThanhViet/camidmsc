package com.metfone.selfcare.module.keeng.widget.buttonSheet.player;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener;

/**
 * Created by namnh40 on 3/21/2017.
 */

public class BottomSheetPlayerHolder extends BaseHolder {
    TextView tvTitle;
    TextView tvRank;
    ImageView imvPlay;
    ImageView btnDelete;
    MediaModel item;
    BaseListener.OnClickBottomPlayerSheet mListener;

    public BottomSheetPlayerHolder(final View itemView, BaseListener.OnClickBottomPlayerSheet listener) {
        super(itemView);
        this.mListener = listener;
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        tvRank = (TextView) itemView.findViewById(R.id.tvRank);
        imvPlay = (ImageView) itemView.findViewById(R.id.imvPlay);
        btnDelete = (ImageView) itemView.findViewById(R.id.btnDelete);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onClickSheet(getAdapterPosition());
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onDeleteItem(getAdapterPosition());
            }
        });
    }

    public void bind(MediaModel item, boolean isPlay) {
        this.item = item;
        tvTitle.setText(item.getName());
        tvRank.setText((getAdapterPosition() + 1) + "");

        if(isPlay)
        {
            tvTitle.setTextColor(itemView.getContext().getResources().getColor(R.color.red));
            tvRank.setVisibility(View.GONE);
            imvPlay.setVisibility(View.VISIBLE);
        }
        else
        {
            tvTitle.setTextColor(itemView.getContext().getResources().getColor(R.color.item_content));
            tvRank.setVisibility(View.VISIBLE);
            imvPlay.setVisibility(View.GONE);
        }
    }
}

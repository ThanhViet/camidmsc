package com.metfone.selfcare.module.netnews.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BackupActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.netnews.CategoryNews.fragment.CategoryNewsFragment;
import com.metfone.selfcare.module.netnews.CategoryTopNow.fragment.SourceTopNowFragment;
import com.metfone.selfcare.module.netnews.EditCategoryNews.fragment.EditChildCategoriesFragment;
import com.metfone.selfcare.module.netnews.EventDetailNews.fragment.EventDetailFragment;
import com.metfone.selfcare.module.netnews.EventNews.fragment.EventFragment;
import com.metfone.selfcare.module.netnews.bottomsheet.BottomSheetAdapter;
import com.metfone.selfcare.module.netnews.bottomsheet.BottomSheetData;
import com.metfone.selfcare.module.newdetails.MainDetailNews.fragment.MainNewsDetailFragment;
import com.metfone.selfcare.module.newdetails.activity.NewsDetailActivity;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.BottomSheetModel;
import com.metfone.selfcare.module.newdetails.model.EventModel;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.NetworkChangeReceiver;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.ui.glide.ImageLoader;
import com.metfone.selfcare.util.Log;

import java.util.List;
import java.util.Stack;

public class NetNewsActivity extends BaseSlidingFragmentActivity implements MvpView, NetworkChangeReceiver.NetworkStateListener, AbsInterface.OnClickBottomSheet {
    private final String TAG = BackupActivity.class.getSimpleName();
    SharedPref mPref;
    ApplicationController mApplication;
    private ProgressDialog mProgressDialog;
    public BaseFragment currentFragment = null;
    public BaseFragment originalFragment = null;
    Stack<MainNewsDetailFragment> mNewsDetailFragmentStack = new Stack<>();

    boolean isShowToast;
    NetworkChangeReceiver mNetWorkReceiver = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        mApplication = (ApplicationController) getApplicationContext();
        mPref = new SharedPref(this);
        mNetWorkReceiver = new NetworkChangeReceiver(this);
        setUp(intent);
        checkNetwork();
    }

    private void checkNetwork() {
        if (!CommonUtils.isConnected(this)) {
            showOffline();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void setUp(Intent intent) {
        int tab = intent.getIntExtra(CommonUtils.KEY_TAB, -1);
        Bundle bundle = new Bundle();
        int id;
        String name;
        switch (tab) {
            case CommonUtils.TAB_NEWS_BY_EVENT:
                id = intent.getIntExtra(CommonUtils.KEY_CATEGORY_ID, -1);
                name = intent.getStringExtra(CommonUtils.KEY_CATEGORY_NAME);
                bundle.putInt(CommonUtils.KEY_CATEGORY_ID, id);
                bundle.putString(CommonUtils.KEY_CATEGORY_NAME, name);
                showFragment(CommonUtils.TAB_NEWS_BY_EVENT, bundle, false);
                break;

            case CommonUtils.TAB_EVENT:
                showFragment(CommonUtils.TAB_EVENT, null, false);
                break;

            case CommonUtils.TAB_SOURCE_TOP_NOW:
                id = intent.getIntExtra(CommonUtils.KEY_CATEGORY_ID, -1);
                name = intent.getStringExtra(CommonUtils.KEY_CATEGORY_NAME);
                bundle.putInt(CommonUtils.KEY_CATEGORY_ID, id);
                bundle.putString(CommonUtils.KEY_CATEGORY_NAME, name);
                showFragment(CommonUtils.TAB_SOURCE_TOP_NOW, bundle, false);
                break;

            case CommonUtils.TAB_SEARCH:
                HomeNewsModel eventData = (HomeNewsModel) intent.getSerializableExtra(CommonUtils.KEY_EVENT_DATA);
                bundle.putSerializable(CommonUtils.KEY_EVENT_DATA, eventData);
                showFragment(CommonUtils.TAB_SEARCH, bundle, false);
                break;

            case CommonUtils.TAB_SETTING_CATEGORY:
                showFragment(CommonUtils.TAB_SETTING_CATEGORY, null, false);
                break;

            case CommonUtils.TAB_CATEGORY_NEWS:
                id = intent.getIntExtra(CommonUtils.KEY_CATEGORY_ID, -1);
                name = intent.getStringExtra(CommonUtils.KEY_CATEGORY_NAME);
                bundle.putInt(CommonUtils.KEY_CATEGORY_ID, id);
                bundle.putString(CommonUtils.KEY_CATEGORY_NAME, name);
                showFragment(CommonUtils.TAB_CATEGORY_NEWS, bundle, false);
                break;

            default:
                break;
        }

    }

    public void showFragment(int tabId, Bundle bundle, boolean enterAnimation) {
        switch (tabId) {
            case CommonUtils.TAB_NEWS_DETAIL:
                currentFragment = MainNewsDetailFragment.newInstance();
                addReadNewsToStack((MainNewsDetailFragment) currentFragment);
                break;
            case CommonUtils.TAB_NEWS_DETAIL_NATIVE:
                //currentFragment = FinestWebViewFragment.newInstance(bundle);
                break;
            case CommonUtils.TAB_SOURCE_TOP_NOW:
                currentFragment = SourceTopNowFragment.newInstance(bundle);
                break;
            case CommonUtils.TAB_CATEGORY_NEWS:
                currentFragment = CategoryNewsFragment.newInstance(bundle);
                break;
            case CommonUtils.TAB_SETTING_CATEGORY:
                currentFragment = EditChildCategoriesFragment.newInstance();
                break;
            case CommonUtils.TAB_EVENT:
                currentFragment = EventFragment.newInstance();
                break;
            case CommonUtils.TAB_NEWS_BY_EVENT:
                currentFragment = EventDetailFragment.newInstance();
                break;
        }

        if (currentFragment != null) {
            if (!getSupportFragmentManager().getFragments().contains(currentFragment)) {
                try {
                    if (!currentFragment.isAdded()) {
                        if (currentFragment.getArguments() == null) {
                            currentFragment.setArguments(bundle);
                        } else {
                            currentFragment.getArguments().putAll(bundle);
                        }
                        getSupportFragmentManager()
                                .beginTransaction()
                                .disallowAddToBackStack()
                                .setCustomAnimations(enterAnimation ? R.anim.fragment_slide_left : R.anim.activity_right_to_left_enter, R.anim.fragment_slide_right)
                                .add(R.id.fragment_container, currentFragment)
                                .commitAllowingStateLoss();

                        if (originalFragment == null) {
                            originalFragment = currentFragment;
                        }
                    }
                } catch (IllegalStateException e) {
                    Log.e(TAG, "showFragment", e);
                } catch (RuntimeException e) {
                    Log.e(TAG, "showFragment", e);

                } catch (Exception ex) {
                    Log.e(TAG, "showFragment", ex);
                }
            }
        }
    }

    public void onFragmentAttached() {

    }

    public void showKeyboard(View view) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1 && view.hasFocus()) {
//            view.clearFocus();
        }
        view.requestFocus();

        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);

    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void onFragmentDetached(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right)
                    .remove(fragment)
                    .commit();
        }

    }

//    @Override
//    public void onBackPressed() {
//        goPrevTab();
//    }


    @Override
    public void onBackPressed() {
        if (currentFragment instanceof EditChildCategoriesFragment) {
            ((EditChildCategoriesFragment) currentFragment).onBackClick();
            return;
        }
        super.onBackPressed();
    }

    public void goPrevTab() {
        try {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            int entryCount = mFragmentManager.getFragments().size();
            if (entryCount > 0) {
                for (int i = entryCount - 1; i >= 0; i--) {
                    Fragment fragment = mFragmentManager.getFragments().get(i);
                    if (fragment != null) {
                        if (originalFragment != null && fragment == originalFragment) {
                            if (fragment instanceof EditChildCategoriesFragment) {
                                ((EditChildCategoriesFragment) fragment).onBackClick();
                            } else {
                                exitActivity();
                            }
                            break;
                        } else {
                            mNewsDetailFragmentStack.remove(fragment);
                            onFragmentDetached(fragment);
                            try {
                                if (i > 0) {
                                    for (int j = i - 1; j >= 0; j--) {
                                        Fragment fragmentPrev = mFragmentManager.getFragments().get(j);

                                        if (fragmentPrev instanceof BaseFragment) {
                                            currentFragment = (BaseFragment) fragmentPrev;
                                        }
                                        break;
                                    }
                                }
                            } catch (Exception e) {
//                                Log.e(TAG, e);
                                Log.e(TAG, "goPrevTab", e);

                            }
                            break;
                        }
                    }
                }
            } else {
                exitActivity();
            }
        } catch (Exception ex) {
//            Log.e(TAG, ex);
            Log.e(TAG, "goPrevTab", ex);

        }
    }

    private void exitActivity() {
        finish();
    }

    @Override
    protected void onDestroy() {
        Log.d("MainActivity", "onDestroy");
        currentFragment = null;
        originalFragment = null;
        if (mNetWorkReceiver != null) {
            mNetWorkReceiver.unregister();
        }
        mNetWorkReceiver = null;
        super.onDestroy();
    }

    //Xu ly bottom sheet
    BottomSheetDialog mBottomSheet;

    private void dismissBottomSheetFirst() {
        if (mBottomSheet != null)
            mBottomSheet.dismiss();
    }

    public void showPopupMore(List<BottomSheetModel> datas, final NewsModel item) {
        dismissBottomSheetFirst();
        if (item == null || datas == null || datas.isEmpty())
            return;
        mBottomSheet = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_bottom_sheet, null);
        TextView tvTitle = (TextView) sheetView.findViewById(R.id.tvTitle);
        TextView tvSinger = (TextView) sheetView.findViewById(R.id.tvSinger);
        ImageView image = (ImageView) sheetView.findViewById(R.id.image);
        RecyclerView mRecycler = (RecyclerView) sheetView.findViewById(R.id.recycler);
        ImageView btnFacebook = sheetView.findViewById(R.id.btnFacebook);
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String linkShareFB = CommonUtils.DOMAIN + item.getUrl();
                if (linkShareFB.contains("?alias=app"))
                    linkShareFB = linkShareFB.replace("?alias=app", "");
                CommonUtils.shareFB(NetNewsActivity.this, linkShareFB, null);
                //AppTracker.sendEvent("V3_MORE_CLICK", "SHARE FACEBOOK", item.getTitle());
            }
        });

        tvTitle.setText(item.getTitle());
        tvSinger.setText(item.getCategory());
        ImageLoader.setImage(this, item.getImage(), image);

        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        BottomSheetAdapter adapter = new BottomSheetAdapter(this, datas, this);
        mRecycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        mBottomSheet.setContentView(sheetView);
        mBottomSheet.show();
    }

    @Override
    public void onClickSheet(BottomSheetModel item) {
        if (item == null) {
            return;
        }
        Bundle bundle;
        switch (item.getType()) {

            case BottomSheetData.COPY_LINK:
                String link = CommonUtils.DOMAIN + item.getNewsModel().getUrl();
                if (link.contains("?alias=app"))
                    link = link.replace("?alias=app", "");
                TextHelper.copyToClipboard(this, link);
                ToastUtils.makeText(this, getResources().getString(R.string.copy_to_clipboard));
                //AppTracker.sendEvent("V3_MORE_CLICK", "COPY LINK", item.getTitle());
                break;
            /*case BottomSheetData.READ_ORIGINAL_NEWS:
                try {
                    String linkWebView = CommonUtils.DOMAIN + item.getNewsModel().getUrl();
                    if (linkWebView.contains("?alias=app"))
                        linkWebView = linkWebView.replace("?alias=app", "");

                    new FinestWebView.Builder(this).titleDefault("NetNews")
                            .showSwipeRefreshLayout(false)
                            .webViewJavaScriptEnabled(true)
                            .show(linkWebView);
                } catch (Exception ex) {
                    Log.e(TAG, "READ_ORIGINAL_NEWS", ex);

                }
                break;*/
            case BottomSheetData.READ_NEWS:
                readNews(item.getNewsModel());

                //AppTracker.sendEvent("V3_READ_NEWS", "MORE RADIO", item.getTitle());
                break;

            case BottomSheetData.SHARE:
                String linkShare = CommonUtils.DOMAIN + item.getNewsModel().getUrl();
                if (linkShare.contains("?alias=app"))
                    linkShare = linkShare.replace("?alias=app", "");
                CommonUtils.share(this, linkShare);
                //AppTracker.sendEvent("V3_MORE_CLICK", "SHARE", item.getTitle());
                break;
            case BottomSheetData.SHARE_FACEBOOK:
                String linkShareFB = CommonUtils.DOMAIN + item.getNewsModel().getUrl();
                if (linkShareFB.contains("?alias=app"))
                    linkShareFB = linkShareFB.replace("?alias=app", "");
                CommonUtils.shareFB(this, linkShareFB, null);
                //AppTracker.sendEvent("V3_MORE_CLICK", "SHARE FACEBOOK", item.getTitle());
                break;
        }

        dismissBottomSheetFirst();
    }

    public void showOffline() {
        /*Snackbar snackbar = Snackbar
                .make(findViewById(android.R.id.content), R.string.connection_error, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        snackbar.show();*/
    }

    @Override
    public void onStart() {
        super.onStart();
        //EventBus.getDefault().register(this);
        //AppTracker.sendEvent("V3_VISIT", "VISIT_APP", "");
    }

    @Override
    protected void onStop() {
        super.onStop();
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public void processItemClick(NewsModel model) {
        if (model != null) {
            if (model.getCid() == CommonUtils.CATE_RADIO_STORY) { // truyện radio
                readNewsNative(model.getUrl());
            } else {
                if (model.getPid() == 151  /*Radio*/
                        || model.getPid() == 0 /*Tin webview*/) {
                    readNewsNative(model.getUrl());
                } else if (model.getPid() == 135 /*Video*/) {
                    playVideo(model);
                } else {
                    readNews(model);
                }
            }
        }
    }

    private void playVideo(NewsModel data) {
        Video videoModel = ConvertHelper.convertNetnewsToVideoMocha(data);
        ApplicationController.self().getApplicationComponent().providesUtils().openVideoDetail(this, videoModel);
    }

    public void readNewsNative(String url) {
        try {
            if (TextUtils.isEmpty(url)) return;
            if (!url.contains(CommonUtils.DOMAIN)) {
                url = CommonUtils.DOMAIN + url;
            }
            UrlConfigHelper.gotoWebViewOnMedia(mApplication, this, url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readNews(NewsModel model) {
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra(CommonUtils.KEY_NEWS_ITEM_SELECT, model);
        startActivity(intent);
    }

    public void loadEvent(EventModel model) {
        Bundle bundle = new Bundle();
        bundle.putInt(CommonUtils.KEY_CATEGORY_ID, model.getCategoryID());
        bundle.putString(CommonUtils.KEY_CATEGORY_NAME, model.getEventName());
        showFragment(CommonUtils.TAB_NEWS_BY_EVENT, bundle, true);
    }

    public void saveMarkRead(int id) {
        if (mPref == null) return;
        String str = mPref.getString(SharedPref.KEY_MARK_READ, "");
        if (!str.contains(id + "")) {
            str += id + ",";
            mPref.putString(SharedPref.KEY_MARK_READ, str);
        }
    }

    public boolean isShowToast() {
        return isShowToast;
    }

    public void setShowToast(boolean showToast) {
        isShowToast = showToast;
    }


    private void addReadNewsToStack(MainNewsDetailFragment fragment) {
        mNewsDetailFragmentStack.push(fragment);
    }

    public BaseFragment getCurrentFragment() {
        return currentFragment;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions == null || permissions.length == 0) {
            Log.i(TAG, "onRequestPermissionsResult - permissions is null or size 0");
            return;
        }

        if (grantResults == null || grantResults.length == 0) {
            Log.i(TAG, "onRequestPermissionsResult - grantResults is null or size 0");
            return;
        }

        switch (requestCode) {

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    public void onNetworkStateChange(boolean hasConnect) {
        Log.d(TAG, "Network connection: " + hasConnect);
        if (hasConnect) {
            if (!mNewsDetailFragmentStack.isEmpty()) {
                MainNewsDetailFragment fragment = mNewsDetailFragmentStack.peek();
                if (fragment != null) {
                    fragment.loadDataAfterReconnect();
                }
            }

        } else {
            //ToastUtils.makeText(this, getString(R.string.connection_error));
            CommonUtils.showNetworkDisconnect(this);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            outState.clear();
        }
    }

    @Override
    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void openActivityOnTokenExpire() {

    }

    @Override
    public void onError(@StringRes int resId) {
        onError(getString(resId));
    }

    @Override
    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar("Some Error Occurred!");
        }
    }

    @Override
    public boolean isNetworkConnected() {
        return CommonUtils.isConnected(getApplicationContext());
    }

    private void showSnackBar(String message) {
        Log.i(TAG, message + "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            int threadId;
            switch (requestCode) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }
}


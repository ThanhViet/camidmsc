/*
package com.metfone.selfcare.module.selfcare.fragment.account;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.metfone.selfcare.activity.ImageBrowserActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.module.selfcare.activity.AfterLoginMyIDActivity;
import com.metfone.selfcare.module.selfcare.activity.SCAccountActivity;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.loginhelper.dialogselectphone.SelectPhoneAdapter;
import com.metfone.selfcare.module.selfcare.model.SCNumberVerify;
import com.metfone.selfcare.module.selfcare.network.SCAccountCallback;
import com.metfone.selfcare.module.selfcare.network.SelfCareAccountApi;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.dialog.BottomSheetMenu;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.IllegalFormatConversionException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.metfone.selfcare.helper.TimeHelper.BIRTHDAY_DEFAULT;

*/
/**
 * Created by thanhnt72 on 2/22/2019.
 *//*


public class SCMyAccountInfoFragment extends SCBaseFragment {

    @BindView(R.id.tilFullName)
    TextInputLayout tilFullName;
    @BindView(R.id.tilPhone)
    TextInputLayout tilPhone;
    @BindView(R.id.tilEmail)
    TextInputLayout tilEmail;
    @BindView(R.id.tilIdentify)
    TextInputLayout tilIdentify;
    @BindView(R.id.btnUpdateInfo)
    RoundTextView btnUpdateInfo;
    @BindView(R.id.btnChangePass)
    RoundTextView btnChangePass;
    @BindView(R.id.btnLogout)
    RoundTextView btnLogout;
    Unbinder unbinder;

    EditText etFullName;
    EditText etPhone;
    EditText etIdentity;
    EditText etBirthday;
    EditText etEmail;
    @BindView(R.id.llDetail)
    LinearLayout llDetail;
    @BindView(R.id.loading_view)
    LoadingViewSC loadingView;
    @BindView(R.id.ivBGAvatar)
    AspectImageView ivBGAvatar;
    @BindView(R.id.ivSCAvatar)
    CircleImageView ivSCAvatar;
    @BindView(R.id.ivChangeAvatar)
    CircleImageView ivChangeAvatar;
    @BindView(R.id.tvSCName)
    TextView tvSCName;
    @BindView(R.id.btnAddNumber)
    RoundTextView btnAddNumber;
    @BindView(R.id.rvListNumber)
    RecyclerView rvListNumber;
    @BindView(R.id.llAddNumber)
    LinearLayout llAddNumber;
    @BindView(R.id.llListNumber)
    LinearLayout llListNumber;
    @BindView(R.id.group_sex)
    RadioGroup groupSex;
    @BindView(R.id.tilBirthday)
    TextInputLayout tilBirthday;


    private String currentEmail, currentIdenti, currentName, currentContactId, currentGender;
    private long currentBirthday;
    private String contactIdEmail;
    private boolean gotoHome;


    private DatePickerDialog mDatePickerDialog;

    private ArrayList<SCNumberVerify> listNumber = new ArrayList<>();
    private SelectPhoneAdapter mAdapter;
    private boolean dateSet;

    public static SCMyAccountInfoFragment newInstance(boolean goToHome) {
        SCMyAccountInfoFragment fragment = new SCMyAccountInfoFragment();
        Bundle args = new Bundle();
        args.putBoolean("gotohome", goToHome);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return SCMyAccountInfoFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_my_account_info;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        if (getArguments() != null)
            gotoHome = getArguments().getBoolean("gotohome");
        initEditText();

        String fullname = mApp.getPref().getString(SCConstants.PREFERENCE.SC_FULL_NAME, "");
        String username = mApp.getPref().getString(SCConstants.PREFERENCE.SC_USER_NAME, "");
        tvSCName.setText(TextUtils.isEmpty(fullname) ? username : fullname);

        rvListNumber.setLayoutManager(new LinearLayoutManager(mApp));
        mAdapter = new SelectPhoneAdapter(mActivity, new ArrayList<SCNumberVerify>());
        mAdapter.setFromMyAccount(true);
        rvListNumber.setAdapter(mAdapter);
        mAdapter.setVerifyNumberListener(new SelectPhoneAdapter.VerifyNumberListener() {
            @Override
            public void onVerifyNumber(SCNumberVerify number) {
                if (!number.isVerify()) {
                    Intent intent = new Intent(getActivity(), AfterLoginMyIDActivity.class);
                    intent.putExtra("number", number);
                    mActivity.startActivityForResult(intent, Constants.ACTION.VERIFY_NUMBER);
                } else
                    mActivity.showToast(R.string.e601_error_but_undefined);
            }
        });

        setActionBar(rootView);
        drawViewDetail();
        drawBirthdayAndGender();

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDatePickerDialog != null && mDatePickerDialog.isShowing()) {
                    return;
                }
                showDateDialog();
            }
        });

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawViewDetail();
            }
        });

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return rootView;
    }

    private void showDateDialog() {
        Calendar c = Calendar.getInstance();
        Date date = TimeHelper.getDateFromBirthDayMytelFromProfile(etBirthday.getText().toString());

        long dateLong = BIRTHDAY_DEFAULT;
        if (date != null) dateLong = date.getTime();
        if (dateLong <= 0) {
            c.setTimeInMillis(883644281000l);
        } else
            c.setTimeInMillis(dateLong);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        //
        Calendar minCalender = Calendar.getInstance();

        Context context = new ContextWrapper(getActivity()) {

            private Resources wrappedResources;

            @Override
            public Resources getResources() {
                Resources r = super.getResources();
                if (wrappedResources == null) {
                    wrappedResources = new Resources(r.getAssets(), r.getDisplayMetrics(), r.getConfiguration()) {
                        @NonNull
                        @Override
                        public String getString(int id, Object... formatArgs) throws NotFoundException {
                            try {
                                return super.getString(id, formatArgs);
                            } catch (IllegalFormatConversionException ifce) {
                                Log.e("DatePickerDialogFix", "IllegalFormatConversionException Fixed!", ifce);
                                String template = super.getString(id);
                                template = template.replaceAll("%" + ifce.getConversion(), "%s");
                                return String.format(getConfiguration().locale, template, formatArgs);
                            }
                        }
                    };
                }
                return wrappedResources;
            }
        };


        if (Build.VERSION.SDK_INT >= 18) {
            int minYear = minCalender.get(Calendar.YEAR) - 100;
            int maxYear = minCalender.get(Calendar.YEAR) - 12;
            com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener listener = new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                    onMyDateSet(selectedYear, selectedMonth, selectedDay);
                }
            };

            dateSet = false;
            new SpinnerDatePickerDialogBuilder()
                    .context(context)
                    .callback(listener)
                    .spinnerTheme(R.style.NumberPickerStyle)
                    .showTitle(true)
                    .showDaySpinner(true)
                    .defaultDate(mYear, mMonth, mDay)
                    .maxDate(maxYear, 11, 31)
                    .minDate(minYear, 0, 1)
                    .build()
                    .show();
        } else {
            minCalender.add(Calendar.YEAR, -100);

            Calendar maxCalender = Calendar.getInstance();
            maxCalender.add(Calendar.YEAR, -12);
            maxCalender.set(Calendar.MONTH, Calendar.DECEMBER);
            maxCalender.set(Calendar.DAY_OF_MONTH, 31);


            mDatePickerDialog = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            mDatePickerDialog.dismiss();
                            onMyDateSet(year, month, dayOfMonth);
                        }
                    }, mYear, mMonth, mDay);
            DatePicker datePicker = mDatePickerDialog.getDatePicker();
            datePicker.setMinDate(minCalender.getTimeInMillis());
            datePicker.setMaxDate(maxCalender.getTimeInMillis());
            dateSet = false;
            mDatePickerDialog.show();
        }

    }

    private void onMyDateSet(int selectedYear, int selectedMonth, int selectedDay) {
        Log.d(TAG, "onDateSet");
        if (!dateSet) {
            dateSet = true;
            String month = String.valueOf(selectedMonth + 1);
            if (selectedMonth < 9) month = String.format(Locale.US, "%02d", selectedMonth + 1);
            String day = String.valueOf(selectedDay);
            if (selectedDay < 9) day = String.format(Locale.US, "%02d", selectedDay);
            StringBuilder dateString = new StringBuilder().append(day)
                    .append("-").append(month).append("-").append(selectedYear);
            etBirthday.setText(dateString.toString());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodUtils.hideSoftKeyboard(mActivity);
    }

    public void drawViewDetail() {
        llDetail.setVisibility(View.GONE);
        loadingView.loadBegin();
//        mActivity.showLoadingSelfCare("", R.string.loading);
        SelfCareAccountApi.getInstant(mApp).getInfoAccount(new SCAccountCallback.SCAccountApiListener() {
            @Override
            public void onSuccess(final String response) {
//                mActivity.hideLoadingDialog();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.optInt("errorCode", -1);
                            if (code == 0) {
                                llDetail.setVisibility(View.VISIBLE);
                                loadingView.loadFinish();
                                JSONObject jsResult = jsonObject.optJSONObject("result");
                                if (jsResult != null) {
                                    String name = getStringJSON(jsResult, "fullName");
                                    String email = "";
                                    String cmnd = "";
                                    String avatar = "";
                                    JSONArray jaAvatar = jsResult.optJSONArray("externalRefs");
                                    if (jaAvatar != null && jaAvatar.length() > 0) {
                                        JSONObject jsAva = jaAvatar.getJSONObject(0);
                                        if (jsAva != null)
                                            avatar = getStringJSON(jsAva, "href");

                                    }

                                    String birthDayStr = getStringJSON(jsResult, "birthDate");
                                    long birthDay = 0;
                                    Date d = TimeHelper.getDateFromBirthDayMytel(birthDayStr);
                                    if (d != null)
                                        birthDay = d.getTime();

                                    String gender = getStringJSON(jsResult, "gender");

                                    JSONArray jaCMND = jsResult.optJSONArray("individualIndentifications");
                                    if (jaCMND != null && jaCMND.length() > 0) {
                                        JSONObject jsCmnd = jaCMND.getJSONObject(0);
                                        if (jsCmnd != null)
                                            cmnd = getStringJSON(jsCmnd, "indentificationId");

                                    }

                                    JSONArray jaContact = jsResult.optJSONArray("contactMediums");
                                    if (jaContact != null && jaContact.length() > 0) {
                                        for (int i = 0; i < jaContact.length(); i++) {
                                            JSONObject jsoContact = jaContact.getJSONObject(i);
                                            if ("Email".equalsIgnoreCase(jsoContact.optString("type"))) {
                                                JSONObject jsC = jsoContact.optJSONObject("medium");
                                                if (jsC != null) {
                                                    email = getStringJSON(jsC, "emailAddress");
                                                    currentContactId = jsoContact.optString("id");
                                                    break;
                                                }

                                            }
                                        }
                                    }
                                    onDrawViewDetail(name, email, cmnd, avatar, birthDay, gender);
                                } else {
                                    //Fail
                                    loadingView.loadError();
                                }
                            } else if (code == 401 || code == 403) {
                                //Login lai
                                loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                            } else {
                                //Fail
                                loadingView.loadError();
                            }

                        } catch (Exception e) {
                            Log.e(TAG, e);
                            if (loadingView != null)
                                loadingView.loadError();
                        }
                    }
                });

            }

            @Override
            public void onError(final int code, String message) {
                Log.e(TAG, "code: " + code + " msg: " + message);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (code == 401 || code == 403) {
                            //Login lai
                            if (loadingView != null)
                                loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                        } else {
                            //Fail
                            if (loadingView != null)
                                loadingView.showRetry();
                        }
                    }
                });

                mActivity.hideLoadingDialog();
            }
        });

        getListNumber();

    }

    private void getListNumber() {
        SelfCareAccountApi.getInstant(mApp).getListPhoneNumberAfterLogin(new ApiCallbackV2<ArrayList<SCNumberVerify>>() {
            @Override
            public void onSuccess(String lastId, final ArrayList<SCNumberVerify> scNumberVerifies) throws JSONException {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (llListNumber != null) {
                            llListNumber.setVisibility(View.VISIBLE);
                            listNumber = scNumberVerifies;
                            mAdapter.setListTopic(listNumber);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                });

            }

            @Override
            public void onError(String s) {
//                llListNumber.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private String getStringJSON(JSONObject response, String key) throws Exception {
        if (response.isNull(key))
            return "";
        return response.optString(key);
    }

    private void onDrawViewDetail(String name, String email, String cmnd, String avatar, long birthday, String gender) {
        currentEmail = email;
        currentIdenti = cmnd;
        currentName = name;
        currentBirthday = birthday;
        currentGender = gender;

        String scAvatar = mApp.getPref().getString(SCConstants.PREFERENCE.SC_AVATAR, "");
        if (!TextUtils.isEmpty(avatar) && !avatar.equals(scAvatar)) {
            mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_AVATAR, avatar).apply();
            mApp.getAvatarBusiness().downloadAndSaveAvatar(mApp, null, avatar);
            EventBus.getDefault().postSticky(new SCAccountActivity.SCAccountEvent(SCAccountActivity.SCAccountEvent.CHANGE_AVATAR));
        } else {
            drawAvatar(avatar, false);
        }

        boolean needSetUserInfo = false;
        ReengAccount account = mApp.getReengAccountBusiness().getCurrentAccount();
        String scName = mApp.getPref().getString(SCConstants.PREFERENCE.SC_FULL_NAME, "");
        if (!TextUtils.isEmpty(name) && !name.equals(scName)) {
            mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_FULL_NAME, name).apply();
            account.setName(name);
            needSetUserInfo = true;
        }


        long scBirthday = mApp.getPref().getLong(SCConstants.PREFERENCE.SC_BIRHDAY_TMP, 0L);
        if (birthday > 0 && birthday != scBirthday) {
            mApp.getPref().edit().putLong(SCConstants.PREFERENCE.SC_BIRHDAY_TMP, birthday).apply();
            String birthDayStr = String.valueOf(TimeHelper.formatTimeBirthdayString(birthday));
            account.setBirthday(String.valueOf(birthday));
            account.setBirthdayString(birthDayStr);
            needSetUserInfo = true;
        }


        int newGender = "male".equals(gender) ? Constants.CONTACT.GENDER_MALE : Constants.CONTACT.GENDER_FEMALE;
        if (newGender != account.getGender()) {
            account.setGender(newGender);
            needSetUserInfo = true;
        }

        if (!TextUtils.isEmpty(name)) {
            tvSCName.setText(name);
            etFullName.setText(name);
        }
        etEmail.setText(email);
        etIdentity.setText(cmnd);
        etBirthday.setText(TimeHelper.formatBirthDayMytel(birthday));
        if ("male".equals(currentGender)) {
            groupSex.check(R.id.radio_sex_male);
        } else {
            groupSex.check(R.id.radio_sex_female);
        }

        if (needSetUserInfo)
            ProfileRequestHelper.getInstance(mApp).setUserInfo(account, null);

    }

    public void drawAvatar(String avatar, boolean upload) {
        if (!TextUtils.isEmpty(avatar)) {
            Glide.with(mApp).asBitmap().load(avatar).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    try {
                        ivSCAvatar.setImageBitmap(resource);
//                        ImageHelper.blurAvatar(mApp, resource, bmBlurDefault, ivBGAvatar);
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }

                }
            });
            if (upload) {
                mApp.getReengAccountBusiness().processUploadAvatarTask(avatar, false);
                SelfCareAccountApi.getInstant(mApp).uploadAvatar(new File(avatar), new SCAccountCallback.SCAccountApiListener() {
                    @Override
                    public void onSuccess(String response) {
                        Log.i(TAG, "onSuccess updateAvatar : " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optInt("errorCode", -1) == 0) {
                                JSONObject js = jsonObject.optJSONObject("result");
                                if (js != null) {
                                    EventBus.getDefault().postSticky(new SCAccountActivity.SCAccountEvent(SCAccountActivity.SCAccountEvent.CHANGE_AVATAR));

                                    String url = js.optString("url");
                                    mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_AVATAR, url).apply();
                                    EventBus.getDefault().post(new SCAccountActivity.SCAccountEvent(SCAccountActivity.SCAccountEvent.CHANGE_AVATAR));
                                } else
                                    mActivity.showToast(R.string.e601_error_but_undefined);
                            } else
                                mActivity.showToast(R.string.e601_error_but_undefined);

                        } catch (Exception e) {
                            Log.e(TAG, e);
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }

                    @Override
                    public void onError(int code, String message) {
                        Log.i(TAG, "onError updateAvatar code: " + code + " - message: " + message);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });

            }
        } */
/*else
            ImageHelper.blurAvatar(mApp, null, bmBlurDefault, ivBGAvatar);*//*

    }

    private void initEditText() {
        etFullName = tilFullName.getEditText();
        etPhone = tilPhone.getEditText();
        etEmail = tilEmail.getEditText();
        etIdentity = tilIdentify.getEditText();
        etBirthday = tilBirthday.getEditText();

        etPhone.setFocusable(false);
        etPhone.setEnabled(false);
        etPhone.setCursorVisible(false);
        etPhone.setKeyListener(null);
        etPhone.setTextColor(ContextCompat.getColor(mActivity, R.color.sc_color_text_normal));
        etPhone.setText(mApp.getReengAccountBusiness().getJidNumber());

        etBirthday.setFocusable(false);
        etBirthday.setCursorVisible(false);
        etBirthday.setKeyListener(null);


    }

    private void drawBirthdayAndGender() {
        ReengAccount reengAccount = mApp.getReengAccountBusiness().getCurrentAccount();
        currentGender = reengAccount.getGender() == 1 ? "male" : "female";
        if ("male".equals(currentGender)) {
            groupSex.check(R.id.radio_sex_male);
        } else {
            groupSex.check(R.id.radio_sex_female);
        }
        long birthDay = reengAccount.getBirthdayLong();
        currentBirthday = birthDay;
        etBirthday.setText(TimeHelper.formatBirthDayMytel(birthDay));
    }

    private void setActionBar(View view) {
        LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        Toolbar abView = view.findViewById(R.id.tool_bar_fragment);
        abView.removeAllViews();
        abView.addView(mLayoutInflater.inflate(
                R.layout.ab_detail, null), new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        (abView.findViewById(R.id.ab_more_btn)).setVisibility(View.GONE);
        ImageView mImBack = abView.findViewById(R.id.ab_back_btn);
        mImBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        EllipsisTextView mTvwTitle = abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mActivity.getString(R.string.sc_account_detail));

        RoundTextView tvSave = view.findViewById(R.id.tvSave);
        tvSave.setVisibility(View.VISIBLE);
        tvSave.setText(mActivity.getString(R.string.save));
        tvSave.setBackgroundColorRound(ContextCompat.getColor(mActivity, R.color.sc_primary));
        tvSave.setTextColor(ContextCompat.getColor(mActivity, R.color.white));
        tvSave.setStroke(ContextCompat.getColor(mActivity, R.color.white), 0);
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doUpdateInfo();
            }
        });

        if (gotoHome) {
            mImBack.setVisibility(View.GONE);
            int padding = mActivity.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_20);
            mTvwTitle.setPadding(padding, 0, 0, 0);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SelfCareAccountApi.getInstant(mApp).canncelPendingRequest(SelfCareAccountApi.TAG_GET_INFO_ACCOUNT);
        unbinder.unbind();

        EventBus.getDefault().unregister(this);
    }

    @OnClick({R.id.btnUpdateInfo, R.id.btnChangePass, R.id.btnLogout, R.id.ivChangeAvatar, R.id.btnAddNumber, R.id.llAddNumber})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdateInfo:
                doUpdateInfo();
                break;
            case R.id.btnChangePass:
                doChangePass();
                break;
            case R.id.btnLogout:
                doLogout();
                break;
            case R.id.ivChangeAvatar:
                //TODO change avatar
                showPopupChangeAvatar();
                break;
            case R.id.btnAddNumber:
                handleAddNumber();
                break;
            case R.id.llAddNumber:
                handleAddNumber();
        }
    }

    private void handleAddNumber() {
        Intent intent = new Intent(getActivity(), AfterLoginMyIDActivity.class);
        mActivity.startActivityForResult(intent, Constants.ACTION.ADD_NUMBER_SC);
    }

    private void doChangePass() {
        Intent intent = new Intent(getActivity(), SCAccountActivity.class);
        intent.putExtra(SCAccountActivity.FRAGMENT, SCAccountActivity.FRAGMENT_CHANGE_PASS);
        mActivity.startActivity(intent);
    }

    private void doLogout() {
        */
/*//*
/TODO logout
        SharedPreferences.Editor editor = mApp.getPref().edit();
        editor.remove(SCConstants.PREFERENCE.SC_AVATAR);
        editor.remove(SCConstants.PREFERENCE.SC_USER_NAME);
        editor.remove(SCConstants.PREFERENCE.SC_FULL_NAME);
        editor.remove(SCConstants.PREFERENCE.SC_KEY_ACCESS_TOKEN);
        editor.apply();


        Intent intent = new Intent(mActivity, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mActivity.startActivity(intent);*//*

//        EventBus.getDefault().post(new SCAccountActivity.SCAccountEvent(true));

        doActiveAccount();
    }

    private void showPopupChangeAvatar() {
        */
/*ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu selectGallery = new ItemContextMenu(mParentActivity, mRes.getString(R.string
                .select_from_gallery),
                -1, null, Constants.MENU.SELECT_GALLERY);
        ItemContextMenu capture = new ItemContextMenu(mParentActivity, mRes.getString(R.string.capture_image),
                -1, null, Constants.MENU.CAPTURE_IMAGE);
        listItem.add(capture);
        listItem.add(selectGallery);
        PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(), null, listItem, this);*//*


        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu openCamera = new ItemContextMenu(mApp, mActivity.getString(R.string.camera),
                R.drawable.ic_bottom_camera, null, Constants.MENU.CAPTURE_IMAGE);
        ItemContextMenu openGallery = new ItemContextMenu(mApp, mActivity.getString(R.string.select_from_gallery),
                R.drawable.ic_bottom_image, null, Constants.MENU.SELECT_GALLERY);
        listItem.add(openCamera);
        listItem.add(openGallery);
        new BottomSheetMenu(mActivity, true)
                .setListItem(listItem)
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onItemClick(int itemId, Object entry) {
                        switch (itemId) {
                            case Constants.MENU.CAPTURE_IMAGE:
                                if (PermissionHelper.declinedPermission(mActivity, Manifest.permission.CAMERA)) {
                                    PermissionHelper.requestPermissionWithGuide(mActivity,
                                            Manifest.permission.CAMERA,
                                            Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
                                } else {
                                    takePhoto();
                                }
                                break;
                            case Constants.MENU.SELECT_GALLERY:
                                mApp.getReengAccountBusiness().removeFileFromProfileDir();
                                Intent i = new Intent(mActivity, ImageBrowserActivity.class);
                                i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
                                i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
                                i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
                                i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
                                i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
                                mActivity.startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
                                break;
                        }
                    }
                }).show();
    }

    private void takePhoto() {
        try {
            mApp.getReengAccountBusiness().removeFileFromProfileDir();
            String time = String.valueOf(System.currentTimeMillis());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File takePhotoFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "tmp" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApp, takePhotoFile));
            intent.putExtra("return-data", true);
            mApp.getPref().edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, takePhotoFile.toString()).apply();
            mActivity.setActivityForResult(true);
            mActivity.setTakePhotoAndCrop(true);
            mActivity.startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            mActivity.showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            mActivity.showToast(R.string.prepare_photo_fail);
        }
    }

    private int countCallback = 0;
    private int totalCallback = 0;

    private void doUpdateInfo() {
        countCallback = 0;
        totalCallback = 0;
        String newName = etFullName.getText().toString().trim();
        String newEmail = etEmail.getText().toString().trim();
        String newIdenti = etIdentity.getText().toString().trim();
        String newGender = "female";
        if (groupSex.getCheckedRadioButtonId() == R.id.radio_sex_male) {
            newGender = "male";
        }


        ReengAccount reengAccount = mApp.getReengAccountBusiness().getCurrentAccount();
        if (TextUtils.isEmpty(newName) || newName.equals(currentName)) {
            newName = "";
        } else {
            reengAccount.setName(newName);
        }

        if (TextUtils.isEmpty(newGender) || newGender.equals(currentGender)) {
            newGender = "";
        } else {
            reengAccount.setGender("male".equals(newGender) ? Constants.CONTACT.GENDER_MALE : Constants.CONTACT.GENDER_FEMALE);
        }

        String newBirthday = etBirthday.getText().toString().trim();
        if (!TextUtils.isEmpty(newBirthday)) {
            Date dateFromProfile = TimeHelper.getDateFromBirthDayMytelFromProfile(newBirthday);
            if (dateFromProfile != null) {
                long currentBirthday = reengAccount.getBirthdayLong();
                if (currentBirthday != dateFromProfile.getTime()) {
                    String birthDay = String.valueOf(TimeHelper.formatTimeBirthdayString(dateFromProfile));
                    reengAccount.setBirthday(String.valueOf(dateFromProfile.getTime()));
                    reengAccount.setBirthdayString(birthDay);
                    newBirthday = TimeHelper.formatBirthDayMytelFromProfile(dateFromProfile.getTime());
                } else
                    newBirthday = "";
            } else
                newBirthday = "";
        } else {
            newBirthday = "";
        }


        if ((!TextUtils.isEmpty(newName) && !newName.equals(currentName))
                || (!TextUtils.isEmpty(newGender) && !newGender.equals(currentGender))
                || (!TextUtils.isEmpty(newBirthday) && !newBirthday.equals(currentBirthday))) {
            totalCallback++;
            setUserInfo(reengAccount);
        }

        if (!TextUtils.isEmpty(newEmail) && !newEmail.equals(currentEmail)) {
            totalCallback++;
        } else
            newEmail = "";

        if (!TextUtils.isEmpty(newIdenti) && !newIdenti.equals(currentIdenti)) {
            totalCallback++;
        } else
            newIdenti = "";

        if (TextUtils.isEmpty(newName) && TextUtils.isEmpty(newEmail)
                && TextUtils.isEmpty(newIdenti) && TextUtils.isEmpty(newGender)
                && TextUtils.isEmpty(newBirthday)) {
            if (gotoHome) mActivity.goToHome();
            return;
        }
        mActivity.showLoadingSelfCare("", R.string.loading);
        try {
            SelfCareAccountApi.getInstant(mApp).updateInfoAccount(currentContactId, newName, newEmail, newIdenti,
                    newGender, newBirthday,
                    callBackName, callBackEmail, callBackIdenti);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    private void doLastRequest() {
        currentName = etFullName.getText().toString();
        currentIdenti = etIdentity.getText().toString();
        currentEmail = etEmail.getText().toString();
        String birthDayStr = etBirthday.getText().toString();
        Date d = TimeHelper.getDateFromBirthDayMytelFromProfile(birthDayStr);
        if (d != null)
            currentBirthday = d.getTime();
        mActivity.hideLoadingDialog();
        mActivity.showToast(R.string.sc_update_info_success);
        if (gotoHome)
            mActivity.goToHome();
    }

    private void setUserInfo(final ReengAccount account) {
        ProfileRequestHelper.onResponseUserInfoListener listener = new ProfileRequestHelper
                .onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
            }

            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "onError: " + errorCode);
            }
        };
        ProfileRequestHelper.getInstance(mApp).setUserInfo(account, listener);


    }

    private SCAccountCallback.SCAccountApiListener callBackName = new SCAccountCallback.SCAccountApiListener() {
        @Override
        public void onSuccess(String response) {
            Log.i(TAG, "onSuccess updateName: " + response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optInt("errorCode", -1) == 0) {
                    EventBus.getDefault().postSticky(new SCAccountActivity.SCAccountEvent(SCAccountActivity.SCAccountEvent.CHANGE_NAME));
                    mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_FULL_NAME, etFullName.getText().toString().trim()).apply();
                    EventBus.getDefault().post(new SCAccountActivity.SCAccountEvent(SCAccountActivity.SCAccountEvent.CHANGE_NAME));
                    countCallback++;
                    if (countCallback == totalCallback)
                        doLastRequest();
*/
/*
                    ReengAccount reengAccount = mApp.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setName(etFullName.getText().toString().trim());
                    ProfileRequestHelper.getInstance(mApp).setUserInfo(reengAccount, null);*//*

                } else
                    onErrorUpdateName();
            } catch (Exception e) {
                Log.e(TAG, e);
                onErrorUpdateName();
            }
        }

        @Override
        public void onError(int code, String message) {
            Log.i(TAG, "onError updateName code: " + code + " - message: " + message);
            onErrorUpdateName();
        }
    };

    void onErrorUpdateName() {
        mActivity.hideLoadingDialog();
        mActivity.showToast(R.string.sc_update_name_fail);
    }

    void onErrorUpdateEmail() {
        mActivity.hideLoadingDialog();
        mActivity.showToast(R.string.sc_update_email_fail);
    }

    void onErrorUpdateNRC() {
        mActivity.hideLoadingDialog();
        mActivity.showToast(R.string.sc_update_nrc_fail);
    }

    private SCAccountCallback.SCAccountApiListener callBackEmail = new SCAccountCallback.SCAccountApiListener() {
        @Override
        public void onSuccess(String response) {
            Log.i(TAG, "onSuccess updateEmail: " + response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optInt("errorCode", -1) == 0) {
                    countCallback++;
                    if (countCallback == totalCallback)
                        doLastRequest();
                } else
                    onErrorUpdateEmail();
            } catch (Exception e) {
                Log.e(TAG, e);
                onErrorUpdateEmail();
            }
        }

        @Override
        public void onError(int code, String message) {
            Log.i(TAG, "onError updateEmail code: " + code + " - message: " + message);
            onErrorUpdateEmail();
        }
    };

    private SCAccountCallback.SCAccountApiListener callBackIdenti = new SCAccountCallback.SCAccountApiListener() {
        @Override
        public void onSuccess(String response) {
            Log.i(TAG, "onSuccess updateIdentity: " + response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.optInt("errorCode", -1) == 0) {
                    countCallback++;
                    if (countCallback == totalCallback)
                        doLastRequest();
                } else
                    onErrorUpdateNRC();
            } catch (Exception e) {
                Log.e(TAG, e);
                onErrorUpdateNRC();
            }
        }

        @Override
        public void onError(int code, String message) {
            Log.i(TAG, "onError updateIdentity code: " + code + " - message: " + message);
            onErrorUpdateNRC();
        }
    };

    private void doActiveAccount() {
        if (NetworkHelper.isConnectInternet(mApp)) {
            if (mApp.getReengAccountBusiness().isAnonymousLogin()) {
                SelfCareAccountApi.getInstant(mApp).logoutMytel();
            } else {
                if (mApp.getXmppManager().isAuthenticated()) {
                    mActivity.deactiveAccount();
                } else {
                    mActivity.showToast(getString(R.string.e604_error_connect_server), Toast.LENGTH_SHORT);
                }
            }
        } else {
            mActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(final SCAccountActivity.SCAccountEvent event) {
        if (event != null) {
            if (event.getEvent() == SCAccountActivity.SCAccountEvent.UPDATE_INFO) {
                getListNumber();
            }
            EventBus.getDefault().removeStickyEvent(event);
        }
    }
}
*/

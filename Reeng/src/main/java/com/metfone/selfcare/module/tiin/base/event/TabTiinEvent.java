package com.metfone.selfcare.module.tiin.base.event;

public class TabTiinEvent {
    private int position;
    private boolean reSelected;
    public TabTiinEvent setReSelect(boolean reSelected) {
        this.reSelected = reSelected;
        return this;
    }

    public TabTiinEvent() {
    }

    public TabTiinEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isReSelected() {
        return reSelected;
    }

    public void setReSelected(boolean reSelected) {
        this.reSelected = reSelected;
    }
}

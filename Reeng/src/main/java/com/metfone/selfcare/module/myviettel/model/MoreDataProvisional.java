/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.model;

import java.util.ArrayList;

public class MoreDataProvisional {
    private boolean hasRegistered;

    private ArrayList<DataPackageInfo> list;

    public ArrayList<DataPackageInfo> getList() {
        return list;
    }

    public void setList(ArrayList<DataPackageInfo> list) {
        this.list = list;
    }

    public boolean isHasRegistered() {
        return hasRegistered;
    }

    public void setHasRegistered(boolean hasRegistered) {
        this.hasRegistered = hasRegistered;
    }
}

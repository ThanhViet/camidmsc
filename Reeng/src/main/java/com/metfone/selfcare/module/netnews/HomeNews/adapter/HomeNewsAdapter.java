package com.metfone.selfcare.module.netnews.HomeNews.adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.netnews.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;
import com.metfone.selfcare.module.newdetails.model.TopNowModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.ui.view.indicator.OverflowPagerIndicator;
import com.metfone.selfcare.ui.view.indicator.SimpleSnapHelper;

import java.util.List;

/**
 * Created by HaiKE on 9/15/17.
 */

public class HomeNewsAdapter extends BaseAdapter<BaseViewHolder> {

    public static final int TYPE_LARGE = 0;
    public static final int TYPE_NORMAL = 1;
    public static final int TYPE_NEWS_EVENT = 5;
    public static final int TYPE_NEWS_FOLLOW = 3;
    public static final int TYPE_NEWS_QUOTE = 4;
    public static final int TYPE_NEWS_CATEGORY = 2;
    public static final int TYPE_NEWS_CATEGORY_NORMAL = 10;
    public static final int TYPE_NEWS_TV = 7;
    public static final int TYPE_NEWS_RADIO = 6;
    public static final int TYPE_NONE = -1;

    private Context context;
    AbsInterface.OnHomeNewsItemListener listener;
    List<HomeNewsModel> datas;
    List<TopNowModel> categoryDatas;
    int countDatasNoCategory = 0;
    int countDataNormal = 0;

    public HomeNewsAdapter(Context context, List<HomeNewsModel> datas, List<TopNowModel> categoryDatas, AbsInterface.OnHomeNewsItemListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
        this.datas = datas;
        this.categoryDatas = categoryDatas;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        BaseViewHolder baseHolder;
        View v;
        switch (viewType) {
            case TYPE_LARGE:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_large, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;
            case TYPE_NORMAL:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_normal, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;
            case TYPE_NEWS_EVENT:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_event, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;

            case TYPE_NEWS_FOLLOW:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_follow, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;

            case TYPE_NEWS_QUOTE:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_quote, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;
            case TYPE_NEWS_CATEGORY:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_category, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;
            case TYPE_NEWS_CATEGORY_NORMAL:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_category_normal, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;
            case TYPE_NEWS_TV:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_tv, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;
            case TYPE_NEWS_RADIO:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_home_radio, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;

            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_empty, viewGroup, false);
                baseHolder = new BaseViewHolder(v);
                break;
        }
        return baseHolder;
    }

    public void onRefresh() {
    }


    @Override
    public void onBindViewHolder(final BaseViewHolder holder, int position) {
        try {
            if (position == 0) {
                HomeNewsModel model = null;
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i).getPosition() == TYPE_LARGE) {
                        model = datas.get(i);
                        break;
                    }
                }
                if (model == null) return;

                if (model.getData().size() > 0) {
                    if (holder.getView(R.id.tvTitle) != null) {
                        TiinUtilities.setText(holder.getView(R.id.tvTitle), model.getData().get(0).getTitle(), model.getData().get(0).getTypeIcon());
                        if (checkMarkRead(model.getData().get(0).getID())) {
                            TextView textView = holder.getView(R.id.tvTitle);
                            textView.setTextColor(textView.getTextColors().withAlpha(120));
                        }
                    }
                    if (holder.getView(R.id.tvSapo) != null)
                        holder.setText(R.id.tvSapo, model.getData().get(0).getShapo());
                    if (holder.getView(R.id.tvCategory) != null)
                        holder.setText(R.id.tvCategory, TextUtils.isEmpty(model.getData().get(0).getSourceName()) ? model.getData().get(0).getCategory() : model.getData().get(0).getSourceName());
                    if (holder.getView(R.id.imvDot) != null) {
                        holder.setVisible(R.id.imvDot, false);
                    }
                    if (holder.getView(R.id.tvDate) != null) {
                        holder.setVisible(R.id.tvDate, false);
                        holder.setText(R.id.tvDate, model.getData().get(0).getDatePub());
                    }
                    if (holder.getView(R.id.imvImage) != null)
                        ImageBusiness.setImageNew(model.getData().get(0).getImage(), (ImageView) holder.getView(R.id.imvImage));

                    final HomeNewsModel finalModel = model;
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemClick(finalModel.getData().get(0));

                            if (checkMarkRead(finalModel.getData().get(0).getID())) {
                                TextView textView = holder.getView(R.id.tvTitle);
                                textView.setTextColor(textView.getTextColors().withAlpha(120));
                            }
                        }
                    });

                    if (holder.getView(R.id.tvCategory) != null) {
                        final HomeNewsModel finalModel1 = model;
                        holder.getView(R.id.tvCategory).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onItemCategoryClick(finalModel1.getData().get(0).getSourceName(), finalModel1.getData().get(0).getSid());
                            }
                        });
                    }

                    if (model.getData().get(0).getIsNghe() == 1 && CommonUtils.FLAG_SUPPORT_RADIO) {
                        holder.getView(R.id.btnListen).setVisibility(View.VISIBLE);
                        holder.setText(R.id.btnListen, context.getString(R.string.listen) + " - " + model.getData().get(0).getDuration());
                        holder.getView(R.id.btnListen).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onItemListenClick(finalModel.getData().get(0));
                            }
                        });
                    } else {
                        holder.getView(R.id.btnListen).setVisibility(View.GONE);
                    }
                }
            } else if (position == 6) { //su kien dang nong
                HomeNewsModel model = null;
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i).getPosition() == TYPE_NEWS_EVENT) {
                        model = datas.get(i);
                        break;
                    }
                }
                if (model == null) return;

                if (holder.getView(R.id.tvTitle) != null && model.getHeader() != null)
                    holder.setText(R.id.tvTitle, model.getHeader().toUpperCase());

                if (holder.getView(R.id.tvTitle1) != null && model.getData().size() > 0) {
                    holder.setText(R.id.tvTitle1, model.getData().get(0).getTitle().toUpperCase());
                    if (holder.getView(R.id.tvContent1) != null)
                        holder.setText(R.id.tvContent1, model.getData().get(0).getLatestTitle());
                    if (holder.getView(R.id.layout_content1) != null)
                        holder.getView(R.id.layout_content1).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(0).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle1);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content1) != null)
                        holder.getView(R.id.layout_content1).setVisibility(View.GONE);
                }

//                if (holder.getView(R.id.layout_ads) != null)
//                    bindAds(holder.getView(R.id.layout_ads));

                if (model.getData().size() > 0) {
                    String[] listImage = model.getData().get(0).getImage169().split(",");
                    if (listImage.length > 1) {
                        if (holder.getView(R.id.imvImage11) != null) {
//                            ImageLoader.setNewsImage(context, listImage[1], (ImageView) holder.getView(R.id.imvImage11));
                            ImageBusiness.setImageNew(listImage[1], (ImageView) holder.getView(R.id.imvImage11));
                        }
                    } else if (listImage.length > 0) {
                        if (holder.getView(R.id.imvImage11) != null) {
//                            ImageLoader.setNewsImage(context, listImage[0], (ImageView) holder.getView(R.id.imvImage11));
                            ImageBusiness.setImageNew(listImage[0], (ImageView) holder.getView(R.id.imvImage11));
                        }
                    }
//                    if (listImage.length > 1) {
//                        if (holder.getView(R.id.imvImage12) != null) {
//                            ImageLoader.setNewsImage(context, listImage[1], (ImageView) holder.getView(R.id.imvImage12));
//                        }
//                    }
//                    if (listImage.length > 2) {
//                        if (holder.getView(R.id.imvImage13) != null) {
//                            ImageLoader.setNewsImage(context, listImage[2], (ImageView) holder.getView(R.id.imvImage13));
//                        }
//                    }
                }

                if (holder.getView(R.id.tvTitle2) != null && model.getData().size() > 1) {
                    holder.setText(R.id.tvTitle2, model.getData().get(1).getTitle().toUpperCase());
                    if (holder.getView(R.id.tvContent2) != null)
                        holder.setText(R.id.tvContent2, model.getData().get(1).getLatestTitle());
                    if (holder.getView(R.id.layout_content2) != null)
                        holder.getView(R.id.layout_content2).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(1).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle2);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content2) != null)
                        holder.getView(R.id.layout_content2).setVisibility(View.GONE);
                }

                if (model.getData().size() > 1) {
                    String[] listImage = model.getData().get(1).getImage().split(",");
                    if (listImage.length > 1) {
                        if (holder.getView(R.id.imvImage21) != null) {
//                            ImageLoader.setNewsImage(context, listImage[1], (ImageView) holder.getView(R.id.imvImage21));
                            ImageBusiness.setImageNew(listImage[1], (ImageView) holder.getView(R.id.imvImage21));
                        }
                    } else if (listImage.length > 0) {
                        if (holder.getView(R.id.imvImage21) != null) {
//                            ImageLoader.setNewsImage(context, listImage[0], (ImageView) holder.getView(R.id.imvImage21));
                            ImageBusiness.setImageNew(listImage[0], (ImageView) holder.getView(R.id.imvImage21));
                        }
                    }
//                    if (listImage.length > 1) {
//                        if (holder.getView(R.id.imvImage22) != null) {
//                            ImageLoader.setNewsImage(context, listImage[1], (ImageView) holder.getView(R.id.imvImage22));
//                        }
//                    }
//                    if (listImage.length > 2) {
//                        if (holder.getView(R.id.imvImage23) != null) {
//                            ImageLoader.setNewsImage(context, listImage[2], (ImageView) holder.getView(R.id.imvImage23));
//                        }
//                    }
                }

                if (holder.getView(R.id.tvTitle3) != null && model.getData().size() > 2) {
                    holder.setText(R.id.tvTitle3, model.getData().get(2).getTitle().toUpperCase());
                    if (holder.getView(R.id.tvContent3) != null)
                        holder.setText(R.id.tvContent3, model.getData().get(2).getLatestTitle());
                    if (holder.getView(R.id.layout_content3) != null)
                        holder.getView(R.id.layout_content3).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(2).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle3);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content3) != null)
                        holder.getView(R.id.layout_content3).setVisibility(View.GONE);
                }

                if (model.getData().size() > 2) {
                    String[] listImage = model.getData().get(2).getImage().split(",");
                    if (listImage.length > 1) {
                        if (holder.getView(R.id.imvImage31) != null) {
//                            ImageLoader.setNewsImage(context, listImage[1], (ImageView) holder.getView(R.id.imvImage31));
                            ImageBusiness.setImageNew(listImage[1], (ImageView) holder.getView(R.id.imvImage31));
                        }
                    } else if (listImage.length > 0) {
                        if (holder.getView(R.id.imvImage31) != null) {
//                            ImageLoader.setNewsImage(context, listImage[0], (ImageView) holder.getView(R.id.imvImage31));
                            ImageBusiness.setImageNew(listImage[0], (ImageView) holder.getView(R.id.imvImage31));
                        }

                    }
//                    if (listImage.length > 1) {
//                        if (holder.getView(R.id.imvImage32) != null) {
//                            ImageLoader.setNewsImage(context, listImage[1], (ImageView) holder.getView(R.id.imvImage32));
//                        }
//                    }
//                    if (listImage.length > 2) {
//                        if (holder.getView(R.id.imvImage33) != null) {
//                            ImageLoader.setNewsImage(context, listImage[2], (ImageView) holder.getView(R.id.imvImage33));
//                        }
//                    }
                }

                final HomeNewsModel finalModel = model;

                if (holder.getView(R.id.layout_content1) != null) {
                    holder.getView(R.id.layout_content1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0) {
                                listener.onItemEventClick(finalModel.getData().get(0));
                            }

                            if (checkMarkRead(finalModel.getData().get(0).getID())) {
                                TextView textView = holder.getView(R.id.tvTitle1);
                                textView.setTextColor(textView.getTextColors().withAlpha(120));
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content2) != null) {
                    holder.getView(R.id.layout_content2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 1) {
                                listener.onItemEventClick(finalModel.getData().get(1));
                            }

                            if (checkMarkRead(finalModel.getData().get(1).getID())) {
                                TextView textView = holder.getView(R.id.tvTitle2);
                                textView.setTextColor(textView.getTextColors().withAlpha(120));
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content3) != null) {
                    holder.getView(R.id.layout_content3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 2) {
                                listener.onItemEventClick(finalModel.getData().get(2));
                            }

                            if (checkMarkRead(finalModel.getData().get(2).getID())) {
                                TextView textView = holder.getView(R.id.tvTitle3);
                                textView.setTextColor(textView.getTextColors().withAlpha(120));
                            }
                        }
                    });
                }

                if (holder.getView(R.id.tvTitle) != null) {
                    holder.getView(R.id.tvTitle).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0) {
                                listener.onItemEventHeaderClick();
                            }
                        }
                    });
                }
            } else if (position < countDatasNoCategory - 4)//Tin binh thuong
            {
                HomeNewsModel model = null;
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i).getPosition() == TYPE_NORMAL) {
                        model = datas.get(i);
                        break;
                    }
                }
                if (model == null) return;

                int index = position;
                if (position < 6)
                    index = position - 1;
//                if (position > 4 && position < 8)
//                    index = position - 2;
                else if (position > 6)
                    index = position - 2;

                if (index < model.getData().size()) {
                    if (holder.getView(R.id.tvTitle) != null) {
                        TiinUtilities.setText(holder.getView(R.id.tvTitle), model.getData().get(index).getTitle(), model.getData().get(index).getTypeIcon());
                        if (checkMarkRead(model.getData().get(index).getID())) {
                            TextView textView = holder.getView(R.id.tvTitle);
                            textView.setTextColor(textView.getTextColors().withAlpha(120));
                        }
                    }

                    if (holder.getView(R.id.tvCategory) != null) {
                        String tvCate = null;
                        if (model.getData().get(index).isLocalAreaNews()) {
                            tvCate = model.getData().get(index).getLocalAreaName();
                        }
                        if (TextUtils.isEmpty(tvCate)) {
                            tvCate = TextUtils.isEmpty(model.getData().get(index).getSourceName()) ? model.getData().get(index).getCategory() : model.getData().get(index).getSourceName();
                        }
                        holder.setText(R.id.tvCategory, tvCate);
                        final HomeNewsModel finalModel1 = model;
                        final int finalIndex1 = index;
                        holder.getView(R.id.tvCategory).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onItemCategoryClick(finalModel1.getData().get(finalIndex1).getSourceName(), finalModel1.getData().get(finalIndex1).getSid());
                            }
                        });
                    }
                    if (holder.getView(R.id.imvDot) != null) {
                        holder.setVisible(R.id.imvDot, false);
                    }
                    if (holder.getView(R.id.tvDate) != null) {
                        holder.setText(R.id.tvDate, model.getData().get(index).getDatePub());
                        holder.setVisible(R.id.tvDate, false);
                    }
                    if (holder.getView(R.id.imvImage) != null)
//                        ImageLoader.setNewsImage(context, model.getData().get(index).getImage(), (ImageView) holder.getView(R.id.imvImage));
                        ImageBusiness.setImageNew(model.getData().get(index).getImage(), (ImageView) holder.getView(R.id.imvImage));
                    final int finalIndex = index;
                    final HomeNewsModel finalModel = model;
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemClick(finalModel.getData().get(finalIndex));

                            if (checkMarkRead(finalModel.getData().get(finalIndex).getID())) {
                                TextView textView = holder.getView(R.id.tvTitle);
                                textView.setTextColor(textView.getTextColors().withAlpha(120));
                            }
                        }
                    });

                    if (model.getData().get(index).getIsNghe() == 1 && CommonUtils.FLAG_SUPPORT_RADIO) {
                        holder.getView(R.id.btnListen).setVisibility(View.VISIBLE);
                        holder.setText(R.id.btnListen, context.getString(R.string.listen) + " - " + model.getData().get(index).getDuration());
                        holder.getView(R.id.btnListen).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onItemListenClick(finalModel.getData().get(finalIndex));
                            }
                        });
                    } else {
                        holder.getView(R.id.btnListen).setVisibility(View.GONE);
                    }

                    if (holder.getView(R.id.line) != null) {
                        if (position == countDatasNoCategory - 4 - 1 || position == 6 - 1) {
                            holder.setVisible(R.id.line, false);
                        } else {
                            holder.setVisible(R.id.line, true);
                        }
                    }
                }
            } else if (position == countDatasNoCategory - 3) { // Tin dang duoc quan tam + bxh + lich thi dau
                //Load data du lieu dang duoc quan tam
                HomeNewsModel model = null;
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i).getPosition() == TYPE_NEWS_FOLLOW) {
                        model = datas.get(i);
                        break;
                    }
                }
                if (model == null) return;

                if (holder.getView(R.id.tvTitle) != null && model.getHeader() != null)
                    holder.setText(R.id.tvTitle, model.getHeader().toUpperCase());

                if (holder.getView(R.id.tvTitle1) != null && model.getData().size() > 0) {
                    holder.setText(R.id.tvTitle1, model.getData().get(0).getTitle());
                    if (holder.getView(R.id.layout_content1) != null)
                        holder.getView(R.id.layout_content1).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(0).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle1);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content1) != null)
                        holder.getView(R.id.layout_content1).setVisibility(View.GONE);
                }

                if (holder.getView(R.id.tvTitle2) != null && model.getData().size() > 1) {
                    holder.setText(R.id.tvTitle2, model.getData().get(1).getTitle());
                    if (holder.getView(R.id.layout_content2) != null)
                        holder.getView(R.id.layout_content2).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(1).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle2);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content2) != null)
                        holder.getView(R.id.layout_content2).setVisibility(View.GONE);
                }

                if (holder.getView(R.id.tvTitle3) != null && model.getData().size() > 2) {
                    holder.setText(R.id.tvTitle3, model.getData().get(2).getTitle());
                    if (holder.getView(R.id.layout_content3) != null)
                        holder.getView(R.id.layout_content3).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(2).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle3);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content3) != null)
                        holder.getView(R.id.layout_content3).setVisibility(View.GONE);
                }

                if (holder.getView(R.id.tvTitle4) != null && model.getData().size() > 3) {
                    holder.setText(R.id.tvTitle4, model.getData().get(3).getTitle());
                    if (holder.getView(R.id.layout_content4) != null)
                        holder.getView(R.id.layout_content4).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(3).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle4);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content4) != null)
                        holder.getView(R.id.layout_content4).setVisibility(View.GONE);
                }

                if (holder.getView(R.id.tvTitle5) != null && model.getData().size() > 4) {
                    holder.setText(R.id.tvTitle5, model.getData().get(4).getTitle());
                    if (holder.getView(R.id.layout_content5) != null)
                        holder.getView(R.id.layout_content5).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(4).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle5);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content5) != null)
                        holder.getView(R.id.layout_content5).setVisibility(View.GONE);
                }

                if (holder.getView(R.id.tvTitle6) != null && model.getData().size() > 5) {
                    holder.setText(R.id.tvTitle6, model.getData().get(5).getTitle());
                    if (holder.getView(R.id.layout_content6) != null)
                        holder.getView(R.id.layout_content6).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(5).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle6);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content6) != null)
                        holder.getView(R.id.layout_content6).setVisibility(View.GONE);
                }

                final HomeNewsModel finalModel = model;

                if (holder.getView(R.id.layout_content1) != null) {
                    holder.getView(R.id.layout_content1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0) {
                                listener.onItemClick(finalModel.getData().get(0));

                                if (checkMarkRead(finalModel.getData().get(0).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle1);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content2) != null) {
                    holder.getView(R.id.layout_content2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 1) {
                                listener.onItemClick(finalModel.getData().get(1));

                                if (checkMarkRead(finalModel.getData().get(1).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle2);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content3) != null) {
                    holder.getView(R.id.layout_content3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 2) {
                                listener.onItemClick(finalModel.getData().get(2));

                                if (checkMarkRead(finalModel.getData().get(2).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle3);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content4) != null) {
                    holder.getView(R.id.layout_content4).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 3) {
                                listener.onItemClick(finalModel.getData().get(3));

                                if (checkMarkRead(finalModel.getData().get(3).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle4);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content5) != null) {
                    holder.getView(R.id.layout_content5).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 4) {
                                listener.onItemClick(finalModel.getData().get(4));

                                if (checkMarkRead(finalModel.getData().get(4).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle5);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content6) != null) {
                    holder.getView(R.id.layout_content6).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 5) {
                                listener.onItemClick(finalModel.getData().get(5));

                                if (checkMarkRead(finalModel.getData().get(5).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle6);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.tvTitle) != null) {
                    holder.getView(R.id.tvTitle).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0)
                                listener.onItemCategoryHeaderClick(TYPE_NEWS_FOLLOW, -1, "Đang được quan tâm");
                        }
                    });
                }
            }
            //Tv
            else if (position == countDatasNoCategory - 4) {
                //not support TV, need to remove
                HomeNewsModel model = null;
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i).getPosition() == TYPE_NEWS_TV) {
                        model = datas.get(i);
                        break;
                    }
                }
                if (model == null) return;
                if (holder.getView(R.id.tvHeader) != null) {
                    holder.setText(R.id.tvHeader, "VIDEO");
                    final HomeNewsModel finalModel1 = model;
                    holder.getView(R.id.tvHeader).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.onItemCategoryHeaderClick(TYPE_NEWS_CATEGORY, 135, "VIDEO");
                        }
                    });
                }
                final RecyclerView recycler = holder.getView(R.id.rec_top);
                final OverflowPagerIndicator indicatorTop = holder.getView(R.id.indicator_top);

                final int[] next = {0};
                final CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                recycler.setLayoutManager(layoutManager);
                recycler.setNestedScrollingEnabled(false);
                recycler.setHasFixedSize(true);
                recycler.setRecycledViewPool(new RecyclerView.RecycledViewPool());
                recycler.setOnFlingListener(null);
                new SimpleSnapHelper(indicatorTop).attachToRecyclerView(recycler);
                final HomeNewsModel finalModel = model;
                recycler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (recycler == null) return;
                        try {
                            int current = layoutManager.findFirstVisibleItemPosition();
                            if (next[0] == 0) {
                                next[0] = 1;
                            } else if (current == 0 && next[0] == -1) {
                                next[0] = 1;
                            } else if (current == finalModel.getData().size() - 1 && next[0] == 1) {
                                next[0] = -1;
                            }
                            int positionNext = current + next[0];
                            layoutManager.smoothScrollToPosition(recycler, null, positionNext);
                            indicatorTop.onPageSelected(positionNext);
                        } catch (Exception e) {
                        }

                        recycler.postDelayed(this, Constants.CHANGE_BANNER_TIME);
                    }
                }, Constants.CHANGE_BANNER_TIME);
                VideoHomeNewAdapter adapter = new VideoHomeNewAdapter(model, context, listener);
                recycler.setAdapter(adapter);
                indicatorTop.attachToRecyclerView(recycler);
               /* if (model == null) return;

                if (holder.getView(R.id.tvTitle) != null && model.getData().size() > 0)
                    holder.setText(R.id.tvTitle, model.getData().get(0).getTitle());

                if (holder.getView(R.id.imvImage) != null && model.getData().size() > 0)
                    ImageLoader.setNewsImage(context, model.getData().get(0).getImage(), (ImageView) holder.getView(R.id.imvImage));

                if (holder.getView(R.id.tvTitle1) != null && model.getData().size() > 1)
                    holder.setText(R.id.tvTitle1, model.getData().get(1).getTitle());

                if (holder.getView(R.id.imvImage1) != null && model.getData().size() > 1)
                    ImageLoader.setNewsImage(context, model.getData().get(1).getImage(), (ImageView) holder.getView(R.id.imvImage1));

                if (holder.getView(R.id.tvTitle2) != null && model.getData().size() > 2)
                    holder.setText(R.id.tvTitle2, model.getData().get(2).getTitle());

                if (holder.getView(R.id.imvImage2) != null && model.getData().size() > 2)
                    ImageLoader.setNewsImage(context, model.getData().get(2).getImage(), (ImageView) holder.getView(R.id.imvImage2));

                if (holder.getView(R.id.tvTitle3) != null && model.getData().size() > 3)
                    holder.setText(R.id.tvTitle3, model.getData().get(3).getTitle());

                if (holder.getView(R.id.imvImage3) != null && model.getData().size() > 3)
                    ImageLoader.setNewsImage(context, model.getData().get(3).getImage(), (ImageView) holder.getView(R.id.imvImage3));

                if (holder.getView(R.id.tvTitle4) != null && model.getData().size() > 4)
                    holder.setText(R.id.tvTitle4, model.getData().get(4).getTitle());

                if (holder.getView(R.id.imvImage4) != null && model.getData().size() > 4)
                    ImageLoader.setNewsImage(context, model.getData().get(4).getImage(), (ImageView) holder.getView(R.id.imvImage4));

                final HomeNewsModel finalModel = model;
                if (holder.getView(R.id.imvImage) != null) {
                    holder.getView(R.id.imvImage).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0)
                                listener.onItemVideoClick(finalModel.getData().get(0));
                        }
                    });
                }

                if (holder.getView(R.id.tvTitle) != null) {
                    holder.getView(R.id.tvTitle).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0)
                                listener.onItemVideoClick(finalModel.getData().get(0));
                        }
                    });
                }

                if (holder.getView(R.id.layout_root1) != null) {
                    holder.getView(R.id.layout_root1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 1)
                                listener.onItemVideoClick(finalModel.getData().get(1));
                        }
                    });
                }

                if (holder.getView(R.id.layout_root2) != null) {
                    holder.getView(R.id.layout_root2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 2)
                                listener.onItemVideoClick(finalModel.getData().get(2));
                        }
                    });
                }

                if (holder.getView(R.id.layout_root3) != null) {
                    holder.getView(R.id.layout_root3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 3)
                                listener.onItemVideoClick(finalModel.getData().get(3));
                        }
                    });
                }

                if (holder.getView(R.id.layout_root4) != null) {
                    holder.getView(R.id.layout_root4).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 4)
                                listener.onItemVideoClick(finalModel.getData().get(4));
                        }
                    });
                }

                if (holder.getView(R.id.imvTV) != null) {
                    holder.getView(R.id.imvTV).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemHeaderVideoClick();
                        }
                    });
                }*/
            } else if (position == countDatasNoCategory - 2) //Quote
            {
                HomeNewsModel model = null;
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i).getPosition() == TYPE_NEWS_QUOTE) {
                        model = datas.get(i);
                        break;
                    }
                }
                if (model == null) return;

                if (model.getData().size() > 0) {
                    if (holder.getView(R.id.tvContent) != null && !TextUtils.isEmpty(model.getData().get(0).getQuote())) {
                        holder.setText(R.id.tvContent, model.getData().get(0).getQuote());
//                        TextView textView = holder.getView(R.id.tvContent);
//                        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/UTM_Centur.ttf");
//                        textView.setTypeface(type);
                    }
                    if (holder.getView(R.id.tvArtist) != null && !TextUtils.isEmpty(model.getData().get(0).getPoster()))
                        holder.setText(R.id.tvArtist, model.getData().get(0).getPoster());

                    final HomeNewsModel finalModel = model;
                    holder.getView(R.id.btnReadMore).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0) {
                                listener.onItemClick(finalModel.getData().get(0));
                            }
                        }
                    });
                }
            }
            //Radio
            else if (position == countDatasNoCategory - 1) {
                // not support so we need to remove it
                /*HomeNewsModel model = null;
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i).getPosition() == TYPE_NEWS_RADIO) {
                        model = datas.get(i);
                        break;
                    }
                }
                if (model == null) return;

                if (holder.getView(R.id.tvHeader) != null && model.getData().size() > 0)
                    holder.setText(R.id.tvHeader, Html.fromHtml(model.getData().get(0).getDatePub()));

                if (holder.getView(R.id.btnListen) != null && model.getData().size() > 0)
                    holder.setText(R.id.btnListen, "Nghe" + " - " + model.getData().get(0).getDuration());

                if (holder.getView(R.id.tvTitle) != null && model.getData().size() > 0)
                    holder.setText(R.id.tvTitle, model.getData().get(0).getTitle());

                if (holder.getView(R.id.imvImage) != null && model.getData().size() > 0)
                    ImageLoader.setNewsImage(context, model.getData().get(0).getImage(), (ImageView) holder.getView(R.id.imvImage));


                if (holder.getView(R.id.tvHeader1) != null && model.getData().size() > 1)
                    holder.setText(R.id.tvHeader1, Html.fromHtml(model.getData().get(1).getDatePub()));

                if (holder.getView(R.id.btnListen1) != null && model.getData().size() > 1)
                    holder.setText(R.id.btnListen1, "Nghe" + " - " + model.getData().get(1).getDuration());

                if (holder.getView(R.id.tvTitle1) != null && model.getData().size() > 1)
                    holder.setText(R.id.tvTitle1, model.getData().get(1).getTitle());

                if (holder.getView(R.id.imvImage1) != null && model.getData().size() > 1)
                    ImageLoader.setNewsImage(context, model.getData().get(1).getImage(), (ImageView) holder.getView(R.id.imvImage1));


                if (holder.getView(R.id.tvHeader2) != null && model.getData().size() > 2)
                    holder.setText(R.id.tvHeader2, Html.fromHtml(model.getData().get(2).getDatePub()));

                if (holder.getView(R.id.btnListen2) != null && model.getData().size() > 2)
                    holder.setText(R.id.btnListen2, "Nghe" + " - " + model.getData().get(2).getDuration());

                if (holder.getView(R.id.tvTitle2) != null && model.getData().size() > 2)
                    holder.setText(R.id.tvTitle2, model.getData().get(2).getTitle());

                if (holder.getView(R.id.imvImage2) != null && model.getData().size() > 2)
                    ImageLoader.setNewsImage(context, model.getData().get(2).getImage(), (ImageView) holder.getView(R.id.imvImage2));


                if (holder.getView(R.id.tvHeader3) != null && model.getData().size() > 3)
                    holder.setText(R.id.tvHeader3, Html.fromHtml(model.getData().get(3).getDatePub()));

                if (holder.getView(R.id.btnListen3) != null && model.getData().size() > 3)
                    holder.setText(R.id.btnListen3, "Nghe" + " - " + model.getData().get(3).getDuration());

                if (holder.getView(R.id.tvTitle3) != null && model.getData().size() > 3)
                    holder.setText(R.id.tvTitle3, model.getData().get(3).getTitle());

                if (holder.getView(R.id.imvImage3) != null && model.getData().size() > 3)
                    ImageLoader.setNewsImage(context, model.getData().get(3).getImage(), (ImageView) holder.getView(R.id.imvImage3));


                if (holder.getView(R.id.tvHeader4) != null && model.getData().size() > 4)
                    holder.setText(R.id.tvHeader4, Html.fromHtml(model.getData().get(4).getDatePub()));

                if (holder.getView(R.id.btnListen4) != null && model.getData().size() > 4)
                    holder.setText(R.id.btnListen4, "Nghe" + " - " + model.getData().get(4).getDuration());

                if (holder.getView(R.id.tvTitle4) != null && model.getData().size() > 4)
                    holder.setText(R.id.tvTitle4, model.getData().get(4).getTitle());

                if (holder.getView(R.id.imvImage4) != null && model.getData().size() > 4)
                    ImageLoader.setNewsImage(context, model.getData().get(4).getImage(), (ImageView) holder.getView(R.id.imvImage4));

                final HomeNewsModel finalModel = model;
                if (holder.getView(R.id.layout_root) != null) {
                    holder.getView(R.id.layout_root).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0)
                                listener.onItemRadioClick(finalModel.getData().get(0));
                        }
                    });
                }

                if (holder.getView(R.id.layout_root1) != null) {
                    holder.getView(R.id.layout_root1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 1)
                                listener.onItemRadioClick(finalModel.getData().get(1));
                        }
                    });
                }

                if (holder.getView(R.id.layout_root2) != null) {
                    holder.getView(R.id.layout_root2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 2)
                                listener.onItemRadioClick(finalModel.getData().get(2));
                        }
                    });
                }

                if (holder.getView(R.id.layout_root3) != null) {
                    holder.getView(R.id.layout_root3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 3)
                                listener.onItemRadioClick(finalModel.getData().get(3));
                        }
                    });
                }

                if (holder.getView(R.id.layout_root4) != null) {
                    holder.getView(R.id.layout_root4).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 4)
                                listener.onItemRadioClick(finalModel.getData().get(4));
                        }
                    });
                }

                if (holder.getView(R.id.btnListen) != null) {
                    holder.getView(R.id.btnListen).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0)
                                listener.onItemRadioClick(finalModel.getData().get(0));
                        }
                    });
                }

                if (holder.getView(R.id.btnListen1) != null) {
                    holder.getView(R.id.btnListen1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 1)
                                listener.onItemRadioClick(finalModel.getData().get(1));
                        }
                    });
                }

                if (holder.getView(R.id.btnListen2) != null) {
                    holder.getView(R.id.btnListen2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 2)
                                listener.onItemRadioClick(finalModel.getData().get(2));
                        }
                    });
                }

                if (holder.getView(R.id.btnListen3) != null) {
                    holder.getView(R.id.btnListen3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 3)
                                listener.onItemRadioClick(finalModel.getData().get(3));
                        }
                    });
                }

                if (holder.getView(R.id.btnListen4) != null) {
                    holder.getView(R.id.btnListen4).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 4)
                                listener.onItemRadioClick(finalModel.getData().get(4));
                        }
                    });
                }

                if (holder.getView(R.id.imvRadio) != null) {
                    holder.getView(R.id.imvRadio).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemHeaderRadioClick();
                        }
                    });
                }*/
            } else if (position - countDatasNoCategory == categoryDatas.size()) { //footer


            } else //Tin chuyen muc
            {
                if (position - countDatasNoCategory >= categoryDatas.size()) return;
                final TopNowModel model = categoryDatas.get(position - countDatasNoCategory);

                if (holder.getView(R.id.tvHeader) != null)
                    holder.setText(R.id.tvHeader, model.getCategoryName() != null ? model.getCategoryName().toUpperCase() : context.getString(R.string.app_name));

                if (holder.getView(R.id.tvTitle) != null) {
                    TiinUtilities.setText(holder.getView(R.id.tvTitle), model.getData().get(0).getTitle(), model.getData().get(0).getTypeIcon());
                    if (checkMarkRead(model.getData().get(0).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                }

                if (holder.getView(R.id.tvCategory) != null && model.getData().size() > 0) {
                    holder.setText(R.id.tvCategory, model.getData().get(0).getSourceName());
                    holder.getView(R.id.tvCategory).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemCategoryClick(model.getData().get(0).getSourceName(), model.getData().get(0).getSid());
                        }
                    });
                }

                if (holder.getView(R.id.tvSapo) != null)
                    holder.setText(R.id.tvSapo, model.getData().get(0).getShapo());

                if (holder.getView(R.id.imvImage) != null && model.getData().size() > 0)
//                    ImageLoader.setNewsImage(context, model.getData().get(0).getImage(), (ImageView) holder.getView(R.id.imvImage));
                    ImageBusiness.setImageNew(model.getData().get(0).getImage(), (ImageView) holder.getView(R.id.imvImage));

                if (holder.getView(R.id.tvTitle1) != null && model.getData().size() > 1) {
                    TiinUtilities.setText(holder.getView(R.id.tvTitle1), model.getData().get(1).getTitle(), model.getData().get(1).getTypeIcon());
                    if (holder.getView(R.id.layout_content1) != null)
                        holder.getView(R.id.layout_content1).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(1).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle1);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content1) != null)
                        holder.getView(R.id.layout_content1).setVisibility(View.GONE);
                }

                if (holder.getView(R.id.tvCategory1) != null && model.getData().size() > 1) {
                    holder.setText(R.id.tvCategory1, model.getData().get(1).getSourceName().toUpperCase());
                    holder.getView(R.id.tvCategory1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemCategoryClick(model.getData().get(1).getSourceName(), model.getData().get(1).getSid());
                        }
                    });
                }

                if (holder.getView(R.id.imvImage1) != null && model.getData().size() > 1)
//                    ImageLoader.setNewsImage(context, model.getData().get(1).getImage(), (ImageView) holder.getView(R.id.imvImage1));
                    ImageBusiness.setImageNew(model.getData().get(1).getImage(), (ImageView) holder.getView(R.id.imvImage1));


                if (holder.getView(R.id.tvTitle2) != null && model.getData().size() > 2) {
                    TiinUtilities.setText(holder.getView(R.id.tvTitle2), model.getData().get(2).getTitle(), model.getData().get(2).getTypeIcon());
                    if (holder.getView(R.id.layout_content2) != null)
                        holder.getView(R.id.layout_content2).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(2).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle2);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content2) != null)
                        holder.getView(R.id.layout_content2).setVisibility(View.GONE);
                }

                if (holder.getView(R.id.tvCategory2) != null && model.getData().size() > 2) {
                    holder.setText(R.id.tvCategory2, model.getData().get(2).getSourceName());
                    holder.getView(R.id.tvCategory2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemCategoryClick(model.getData().get(2).getCategory(), model.getData().get(2).getSid());
                        }
                    });
                }

                if (holder.getView(R.id.imvImage2) != null && model.getData().size() > 2)
//                    ImageLoader.setNewsImage(context, model.getData().get(2).getImage(), (ImageView) holder.getView(R.id.imvImage2));
                    ImageBusiness.setImageNew(model.getData().get(2).getImage(), (ImageView) holder.getView(R.id.imvImage2));

                if (holder.getView(R.id.tvTitle3) != null && model.getData().size() > 3) {
                    TiinUtilities.setText(holder.getView(R.id.tvTitle3), model.getData().get(3).getTitle(), model.getData().get(3).getTypeIcon());
                    if (holder.getView(R.id.layout_content3) != null)
                        holder.getView(R.id.layout_content3).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(3).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle3);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content3) != null)
                        holder.getView(R.id.layout_content3).setVisibility(View.GONE);
                }

//                if (holder.getView(R.id.imvImage3) != null && model.getData().size() > 3)
//                    ImageLoader.setNewsImage(context, model.getData().get(3).getImage(), (ImageView) holder.getView(R.id.imvImage3));

                if (holder.getView(R.id.tvTitle4) != null && model.getData().size() > 4) {
                    TiinUtilities.setText(holder.getView(R.id.tvTitle4), model.getData().get(4).getTitle(), model.getData().get(4).getTypeIcon());
                    if (holder.getView(R.id.layout_content4) != null)
                        holder.getView(R.id.layout_content4).setVisibility(View.VISIBLE);

                    if (checkMarkRead(model.getData().get(4).getID())) {
                        TextView textView = holder.getView(R.id.tvTitle4);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                } else {
                    if (holder.getView(R.id.layout_content4) != null)
                        holder.getView(R.id.layout_content4).setVisibility(View.GONE);
                }

                final TopNowModel finalModel = model;
                if (holder.getView(R.id.layout_content1) != null) {
                    holder.getView(R.id.layout_content1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 1) {
                                listener.onItemClick(finalModel.getData().get(1));

                                if (checkMarkRead(model.getData().get(1).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle1);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content2) != null) {
                    holder.getView(R.id.layout_content2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 2) {
                                listener.onItemClick(finalModel.getData().get(2));

                                if (checkMarkRead(model.getData().get(2).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle2);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content3) != null) {
                    holder.getView(R.id.layout_content3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 3) {
                                listener.onItemClick(finalModel.getData().get(3));
                                if (checkMarkRead(model.getData().get(3).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle3);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.layout_content4) != null) {
                    holder.getView(R.id.layout_content4).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 4) {
                                listener.onItemClick(finalModel.getData().get(4));

                                if (checkMarkRead(model.getData().get(4).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle4);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.imvImage) != null) {
                    holder.getView(R.id.imvImage).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0) {
                                listener.onItemClick(finalModel.getData().get(0));

                                if (checkMarkRead(model.getData().get(0).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.tvTitle) != null) {
                    holder.getView(R.id.tvTitle).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (finalModel.getData().size() > 0) {
                                listener.onItemClick(finalModel.getData().get(0));

                                if (checkMarkRead(model.getData().get(0).getID())) {
                                    TextView textView = holder.getView(R.id.tvTitle);
                                    textView.setTextColor(textView.getTextColors().withAlpha(120));
                                }
                            }
                        }
                    });
                }

                if (holder.getView(R.id.tvHeader) != null) {
                    holder.getView(R.id.tvHeader).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onItemCategoryHeaderClick(TYPE_NEWS_CATEGORY, finalModel.getCategoryID(), finalModel.getCategoryName());
                        }
                    });
                }
            }

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) holder.itemView.getLayoutParams();
            if (position == getItemCount() - 1) {
                params.topMargin = context.getResources().getDimensionPixelSize(R.dimen.padding_news);
                params.bottomMargin = context.getResources().getDimensionPixelSize(R.dimen.padding_news);
                holder.itemView.setLayoutParams(params);
            } else if (position >= (countDatasNoCategory) || position == countDatasNoCategory - 2) {
                params.topMargin = context.getResources().getDimensionPixelSize(R.dimen.padding_news);
                holder.itemView.setLayoutParams(params);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        countDataNormal = 0;
        int index = 0;
        for (int i = 0; i < datas.size(); i++) {
            HomeNewsModel model = datas.get(i);
            if (model.getPosition() == TYPE_NORMAL) {
                index += model.getData().size();
                countDataNormal += model.getData().size();
            } else if (model.getPosition() == TYPE_LARGE || model.getPosition() == TYPE_NEWS_QUOTE || model.getPosition() == TYPE_NEWS_EVENT || model.getPosition() == TYPE_NEWS_FOLLOW || model.getPosition() == TYPE_NEWS_RADIO || model.getPosition() == TYPE_NEWS_TV) {
                index++;
            }
        }

        countDatasNoCategory = index;
        return index + categoryDatas.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_LARGE;
        else if (position == 6)
            return TYPE_NEWS_EVENT;
        else if (position < countDatasNoCategory - 4)
            return TYPE_NORMAL;
        else if (position == countDatasNoCategory - 4)
            return TYPE_NEWS_TV;
        else if (position == countDatasNoCategory - 3)
            return TYPE_NEWS_FOLLOW;
        else if (position == countDatasNoCategory - 2)
            return TYPE_NEWS_QUOTE;
        else if (position == countDatasNoCategory - 1)
            return TYPE_NEWS_RADIO;
        else {
            if (position - countDatasNoCategory < categoryDatas.size()) {
                TopNowModel model = categoryDatas.get(position - countDatasNoCategory);
                if (model.getTypeDisplay() == 1)
                    return TYPE_NEWS_CATEGORY;
                else
                    return TYPE_NEWS_CATEGORY_NORMAL;
            }
            return TYPE_NEWS_CATEGORY;
        }
    }

    public boolean checkMarkRead(int id) {
        return false;
    }


    public void onDestroy() {
    }

    private void removeViewParent(View view) {
        if (view != null && view.getParent() != null && view.getParent() instanceof ViewGroup) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }
}
package com.metfone.selfcare.module.saving.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Promotion implements Serializable {
    @SerializedName("deeplink_url")
    private String deeplinkUrl = "";
    @SerializedName("deeplink_label")
    private String deeplinkLabel = "";
    @SerializedName("desc")
    private String desc = "";
    @SerializedName("icon")
    private String imageUrl = "";

    public String getDeeplinkUrl() {
        return deeplinkUrl;
    }

    public void setDeeplinkUrl(String deeplinkUrl) {
        this.deeplinkUrl = deeplinkUrl;
    }

    public String getDeeplinkLabel() {
        return deeplinkLabel;
    }

    public void setDeeplinkLabel(String deeplinkLabel) {
        this.deeplinkLabel = deeplinkLabel;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "{" +
                "deeplinkUrl='" + deeplinkUrl + '\'' +
                ", deeplinkLabel='" + deeplinkLabel + '\'' +
                ", desc='" + desc + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}

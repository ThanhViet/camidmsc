package com.metfone.selfcare.module.metfoneplus.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;

public class BaseMPSuccessDialog extends BaseCustomDialog {
    private int drawableId;
    private int titleId;
    private int contentId;
    private String title;
    private String content;
    private boolean visibilityTop;
    private boolean bgWhite;

    public void setBgWhite(boolean bgWhite) {
        this.bgWhite = bgWhite;
    }

    public BaseMPSuccessDialog(Activity activity, int drawableId, int titleId, int contentId) {
        super(activity);
        this.drawableId = drawableId;
        this.titleId = titleId;
        this.contentId = contentId;
    }

    public BaseMPSuccessDialog(Activity activity,int drawableId, String title, String content) {
        super(activity);
        this.drawableId = drawableId;
        this.title = title;
        this.content = content;
    }

    public BaseMPSuccessDialog(Activity activity, int drawableId, int titleId, String content) {
        super(activity);
        this.drawableId = drawableId;
        this.titleId = titleId;
        try {
            this.title = activity.getString(titleId);
        }catch (Exception e){

        }
        this.content = content;
    }

    public BaseMPSuccessDialog(Activity activity, int drawableId, String title, String content, boolean visibilityTop) {
        super(activity);
        this.drawableId = drawableId;
        this.title = title;
        this.content = content;
        this.visibilityTop = visibilityTop;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getWindow() != null){
            getWindow().setLayout(UserInfoBusiness.getWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
//            getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.transparent)));

        }
        setCancelable(true);
        if (visibilityTop){
            updateUiDialog(true);
        }else {
            updateUiDialog();
        }
    }

    private void updateUiDialog() {
        if (title == null){
            this.title = getActivity().getString(titleId);
        }
        if (bgWhite){
            lnlContent.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_white_corner_6));
            mTitle.setTextColor(Color.parseColor("#CC212121"));
            mContent.setTextColor(Color.parseColor("#80212121"));
        }
        if (drawableId == R.drawable.img_dialog_1){
            flImage.setVisibility(View.VISIBLE);
            lavAnimations.setVisibility(View.VISIBLE);
        }else {
            flImage.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.VISIBLE);
            mImage.setImageResource(drawableId);
        }
        mTitle.setText(title);
        mContent.setText(content);
        setButtonTopVisibility(View.GONE);
        setButtonBottomVisibility(View.GONE);
    }

//    @Override
//    public void onDismiss(final DialogInterface dialog) {
//        super.onDismiss(dialog);
//        final Activity activity = getActivity();
//        if (activity instanceof DialogInterface.OnDismissListener) {
//            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
//        }
//    }

    public void updateUiDialog(boolean visibilityTop) {
        if (bgWhite){
            lnlContent.setBackgroundColor(Color.WHITE);
            mTitle.setTextColor(Color.parseColor("#CC212121"));
            mContent.setTextColor(Color.parseColor("#80212121"));
            if (getWindow() != null) getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.transparent)));
        }
        if (drawableId == R.drawable.img_dialog_1){
            flImage.setVisibility(View.VISIBLE);
            lavAnimations.setVisibility(View.VISIBLE);
        }else {
            flImage.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.VISIBLE);
            mImage.setImageResource(drawableId);
        }
        if (title == null){
            this.title = getActivity().getString(titleId);
        }
        mTitle.setText(title);
        mContent.setText(content);
        if(visibilityTop){
            setButtonTopVisibility(View.VISIBLE);
            setTopButton(R.string.done, R.drawable.bg_red_button_corner_6, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
        }else {
            setButtonTopVisibility(View.GONE);
        }
        setButtonBottomVisibility(View.GONE);
    }

    private void updateUiDialog(int drawableId, int titleId, int contentId) {
        mImage.setImageResource(drawableId);
        mTitle.setText(titleId);
        mContent.setText(contentId);
        setButtonTopVisibility(View.GONE);
        setButtonBottomVisibility(View.GONE);
    }
}
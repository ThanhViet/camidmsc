package com.metfone.selfcare.module.home_kh.tab.model;

import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.selfcare.R;

import java.util.List;

public class KhHomeChannelTopic implements IHomeModelType {
    public static final int titleRes = R.string.kh_home_live_channel;

    public List<VideoInfoResponse> khChannels;

    public KhHomeChannelTopic(List<VideoInfoResponse> khChannels) {
        this.khChannels = khChannels;
    }

    @Override
    public int getItemType() {
        return IHomeModelType.POPULAR_LIVE_CHANNEL;
    }


}

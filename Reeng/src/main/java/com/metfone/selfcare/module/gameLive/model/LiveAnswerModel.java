package com.metfone.selfcare.module.gameLive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LiveAnswerModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("questionID")
    @Expose
    private int questionID;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("is_true")
    @Expose
    private int is_true;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIs_true() {
        return is_true;
    }

    public void setIs_true(int is_true) {
        this.is_true = is_true;
    }

    @Override
    public String toString() {
        return "LiveAnswerModel{" +
                "id=" + id +
                ", questionID=" + questionID +
                ", title='" + title + '\'' +
                ", is_true=" + is_true +
                '}';
    }
}

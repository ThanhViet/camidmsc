/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/20
 */

package com.metfone.selfcare.module.saving.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.saving.fragment.SavingHistoryDetailFragment;
import com.metfone.selfcare.module.saving.fragment.SavingSearchFragment;
import com.metfone.selfcare.module.saving.fragment.SavingStatisticsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SavingActivity extends BaseSlidingFragmentActivity {
    protected final String TAG = getClass().getSimpleName();
    protected Fragment mFragment;
    protected int currentTabId;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @OnClick(R.id.button_back)
    public void onBackClick() {
        finish();
    }

    public void setTitle(int resTitle) {
        if (tvTitle != null) tvTitle.setText(resTitle);
    }

    public void setTitle(String title) {
        if (tvTitle != null) tvTitle.setText(title);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saving);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        int type = intent.getIntExtra(Constants.KEY_TYPE, 0);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_TITLE, intent.getStringExtra(Constants.KEY_TITLE));
        bundle.putString(Constants.KEY_START_DATE, intent.getStringExtra(Constants.KEY_START_DATE));
        bundle.putString(Constants.KEY_END_DATE, intent.getStringExtra(Constants.KEY_END_DATE));

        goNextTab(type, bundle);
    }

    public void goNextTab(int tabId, Bundle args) {
        switch (tabId) {
            case Constants.HOME.FUNCTION.TAB_SAVING_STATISTICS:
                mFragment = SavingStatisticsFragment.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_SAVING_SEARCH:
                mFragment = SavingSearchFragment.newInstance();
                break;
            case Constants.HOME.FUNCTION.TAB_SAVING_HISTORY_DETAIL:
                mFragment = SavingHistoryDetailFragment.newInstance();
                break;
            default:
                mFragment = null;
                break;

        }

        if (mFragment != null) {
            currentTabId = tabId;
            try {
                if (!mFragment.isAdded())
                    if (args != null) {
                        if (mFragment.getArguments() == null) {
                            mFragment.setArguments(args);
                        } else {
                            mFragment.getArguments().putAll(args);
                        }
                    }
            } catch (IllegalStateException e) {
                Log.e(TAG, e);
            } catch (RuntimeException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
            replaceFragment(currentTabId, mFragment);
        }
    }

    public void replaceFragment(final int tabId, final Fragment fragment) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                transactionFragment(tabId, fragment);
            }
        });
    }

    public void transactionFragment(final int tabId, final Fragment fragment) {
        try {
            getSupportFragmentManager()
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.activity_right_to_left_enter, R.anim.activity_right_to_left_exit)
                    .add(R.id.tabContent, fragment, String.valueOf(tabId))
                    .commitAllowingStateLoss();
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
        setActivityForResult(false);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null && requestCode == Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT) {
            int threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
            NavigateActivityHelper.navigateToChatDetail(SavingActivity.this, threadId, ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT);
        }
    }
}

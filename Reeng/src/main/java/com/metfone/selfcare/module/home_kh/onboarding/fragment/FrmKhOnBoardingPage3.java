package com.metfone.selfcare.module.home_kh.onboarding.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.airbnb.lottie.LottieAnimationView;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.utils.SharedPref;

public class FrmKhOnBoardingPage3 extends FrmKhOnBoarding {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_onboarding_page3;
    }

    @Override
    protected int getPageIndex() {
        return 2;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btnNotNow).setOnClickListener(v -> {
            saveOnBoardingShowed();
            mActivity.gotoHome(false);
        });

        view.findViewById(R.id.btnLetDoThis).setOnClickListener(v -> {
            saveOnBoardingShowed();
            mActivity.gotoHome(true);
        });

        LottieAnimationView lt = view.findViewById(R.id.obd_img_signup);
        lt.setProgress(0);
        lt.playAnimation();
    }

    private void saveOnBoardingShowed() {
        DynamicSharePref.getInstance().put(Constants.KEY_IS_FIRST_TAB_HOME, true);
        DynamicSharePref.getInstance().put(Constants.PREFERENCE.PREF_ON_BOARDING_SHOWED, true);
    }
}

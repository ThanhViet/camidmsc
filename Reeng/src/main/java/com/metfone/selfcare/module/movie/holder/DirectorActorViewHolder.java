package com.metfone.selfcare.module.movie.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabMovie.Actor;
import com.metfone.selfcare.model.tabMovie.Director;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;

import java.util.List;

public class DirectorActorViewHolder extends BaseViewHolder {
    TextView tvDirector;

    public DirectorActorViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
    }

    @Override
    public void initViewHolder(View v) {
        tvDirector = v.findViewById(R.id.tvDirector);
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        if (mData.get(pos) instanceof Director) {
            Director director = (Director) mData.get(pos);
            if (pos != mData.size() - 1) {
                tvDirector.setText(director.getName().trim() + ", ");
            } else {
                tvDirector.setText(director.getName() != null ? director.getName().trim() : director.getName());
            }
        }
        if (mData.get(pos) instanceof Actor) {
            Actor actor = (Actor) mData.get(pos);
            if (pos != mData.size() - 1) {
                tvDirector.setText(actor.getName().trim() + ", ");
            } else {
                tvDirector.setText(actor.getName() != null ? actor.getName().trim() : actor.getName());
            }
        }
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null) {
            itemViewClickListener.onItemViewClickListener(pos, mData);
        }
    }
}

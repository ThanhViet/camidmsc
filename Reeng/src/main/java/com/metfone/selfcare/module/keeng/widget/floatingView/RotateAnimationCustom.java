package com.metfone.selfcare.module.keeng.widget.floatingView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;

public class RotateAnimationCustom extends RotateAnimation {
    boolean isPause = false;
    long mElapsedAtPause = 0;

    public RotateAnimationCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RotateAnimationCustom(float fromDegrees, float toDegrees) {
        super(fromDegrees, toDegrees);
        init();
    }

    public RotateAnimationCustom(float fromDegrees, float toDegrees, float pivotX, float pivotY) {
        super(fromDegrees, toDegrees, pivotX, pivotY);
        init();
    }

    public RotateAnimationCustom(float fromDegrees, float toDegrees, int pivotXType, float pivotXValue, int pivotYType, float pivotYValue) {
        super(fromDegrees, toDegrees, pivotXType, pivotXValue, pivotYType, pivotYValue);
        init();
    }

    private void init() {

    }

    @Override
    public boolean getTransformation(long currentTime, Transformation outTransformation) {
        if (isPause && mElapsedAtPause == 0) {
            mElapsedAtPause = currentTime - getStartTime();
        }
        if (isPause)
            setStartTime(currentTime - mElapsedAtPause);
        return super.getTransformation(currentTime, outTransformation);
    }

    public boolean isPause() {
        return isPause;
    }

    public void setPause(boolean isPause) {
        if(isPause)
            mElapsedAtPause = 0;
        this.isPause = isPause;
    }
}

package com.metfone.selfcare.module.home_kh.tab.model;

import androidx.annotation.DrawableRes;

import java.util.ArrayList;

public class KhHomeGiftItem implements IHomeModelType{
    public ArrayList<GiftItem> giftItems;
    @Override
    public int getItemType() {
        return IHomeModelType.GIFT;
    }

    public static class GiftItem{
        public int resourceBanner;
        public GiftType giftType;
        public boolean isActive;
        public GiftItem(@DrawableRes int resourceBanner, GiftType giftType, boolean isActive){
            this.resourceBanner = resourceBanner;
            this.giftType = giftType;
            this.isActive = isActive;
        }
    }

    public static enum GiftType{
        KHMER_NEW_YEAR,
        COMING_SOON
    }
}

package com.metfone.selfcare.module.keeng.interfaces;

import android.view.View;

import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.model.RankModel;
import com.metfone.selfcare.module.keeng.model.Topic;

/**
 * Created by HaiKE on 8/31/16.
 */
public abstract class AbsInterface {

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public interface OnItemListener {
        void onHeaderClick(View view, String tag);

        void onItemClick(AllModel item);

        void onItemClick(PlayListModel item);

        void onItemTopicClick(Topic item);

        void onItemSingerClick(Topic item);

        void onItemRankClick(RankModel item, int position);

        void onItemRankHeaderClick(RankModel item);

        void onItemClickOption(AllModel item);

        void onItemTopHitClick(Topic item);
    }

}

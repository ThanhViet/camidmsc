package com.metfone.selfcare.module.metfoneplus.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.mytelsupportsdk.InitSDKExeption;
import com.mytelsupportsdk.MyTelSupportSDK;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;

public class MPCallUsDialog extends DialogFragment {
    public static final String TAG = MPCallUsDialog.class.getSimpleName();
    private boolean isFromInternet = false;
    private View.OnClickListener mHotlineListeners = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            MPConfirmationCallDialog confirmationCallDialog;
            if (isFromInternet){
                confirmationCallDialog = MPConfirmationCallDialog
                        .newInstance(getString(R.string.m_p_dialog_confirmation_call_content_internet)
                                , getString(R.string.m_p_metfone_customer_support_internet), R.string.m_p_dialog_confirmation_call_yes, R.string.m_p_dialog_confirmation_call_close);
            } else {
                confirmationCallDialog = MPConfirmationCallDialog
                        .newInstance(getString(R.string.m_p_dialog_confirmation_call_content)
                                , getString(R.string.m_p_metfone_customer_support), R.string.m_p_dialog_confirmation_call_yes, R.string.m_p_dialog_confirmation_call_close);
            }
            confirmationCallDialog.show(getParentFragmentManager(), MPConfirmationCallDialog.TAG);
        }
    };

    private View.OnClickListener mVideoListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                mMyTelSupportSDK.showVideoCallFragment(SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class));
            } catch (InitSDKExeption initSDKExeption) {
                initSDKExeption.printStackTrace();
            }
            dismissAllowingStateLoss();
        }
    };

    private View.OnClickListener mNoThanksListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismiss();
        }
    };

    /**
     * Info for call video using MyTelSupportSDK
     */
    private final String userName = "metfone";
    private final String api_key = "668762ef591dd5cc38c5eb3cb3be263075d07a03650ef414e0b98d5808f4318f";
    private final String apiBaseUrl = "https://myccpublic.metfone.com.kh:6210";
    private final String sipProxy = "metfone.mycc.vn";
    private final String chatDomain = "8a43e6d5-b0b2-452d-83a3-013e0c6de22f";
    private final String chatScriptUrl = "https://myccpublic.metfone.com.kh/assets//js/IpccChat.js";
    private final String url_login = "https://myccpublic.metfone.com.kh:8001/";
    private final String hashing_key = "Metfone1!2@3#";
    private final String accID = "2d48d112ad36aaaad3b90f510e31f2fd";

    private MyTelSupportSDK mMyTelSupportSDK;

    public MPCallUsDialog() {

    }

    public static MPCallUsDialog newInstance(boolean isFromInternet) {
        MPCallUsDialog callUsDialog = new MPCallUsDialog();
        Bundle bundle = new Bundle();
        bundle.putBoolean("IS_FROM_INTERNET", isFromInternet);
        callUsDialog.setArguments(bundle);
        return callUsDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFullScreen);

        initMyTelSupportSDK();
    }

    private void initMyTelSupportSDK() {
         /*
        -	baseUrl:  https://myccpublic.metfone.com.kh:8001
        -	sipProxy: sbc2.metfone.com.kh
        -	domainHtml: 8a43e6d5-b0b2-452d-83a3-013e0c6de22f
        -	chatUrl:  https://myccpublic.metfone.com.kh/assets//js/IpccChat.js
        */

        mMyTelSupportSDK = MyTelSupportSDK.instance;
        mMyTelSupportSDK.init(getParentFragmentManager(), getActivity());
        mMyTelSupportSDK.setTokenIvalidListener(new MyTelSupportSDK.TokenIvalidListener() {
            @Override
            public void tokenIvalid() {

            }
        });

        mMyTelSupportSDK.setDefaultDomain(sipProxy);
        mMyTelSupportSDK.setDefaultChat(chatDomain, chatScriptUrl);
        mMyTelSupportSDK.setDefaultBaseUrl(apiBaseUrl);
        mMyTelSupportSDK.setAccountConfig(accID, hashing_key);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_mp_call_us, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.dialog_mp_call_us_top_layout_button)
                .setOnClickListener(mHotlineListeners);

        view.findViewById(R.id.dialog_mp_call_us_middle_layout_button)
                .setOnClickListener(mVideoListener);

        view.findViewById(R.id.dialog_mp_call_us_bottom_title_button)
                .setOnClickListener(mNoThanksListener);
        isFromInternet = getArguments().getBoolean("IS_FROM_INTERNET",false);
        if (isFromInternet){
            ((TextView)view.findViewById(R.id.tvHotline)).setText(R.string.m_p_dialog_call_us_btn_hotline_internet);
        } else
            ((TextView)view.findViewById(R.id.tvHotline)).setText(R.string.m_p_dialog_call_us_btn_hotline);

    }

    @Override
    public void onPause() {
        super.onPause();
        dismissAllowingStateLoss();
    }
}

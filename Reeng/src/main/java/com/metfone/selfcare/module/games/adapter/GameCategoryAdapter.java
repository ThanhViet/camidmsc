package com.metfone.selfcare.module.games.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.model.tabGame.GameBaseResponseData;
import com.metfone.selfcare.model.tabGame.GameByNameRequest;
import com.metfone.selfcare.model.tabGame.GameCategoryModel;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.module.games.activity.PlayGameActivity;
import com.metfone.selfcare.module.games.api.GameApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class GameCategoryAdapter extends RecyclerView.Adapter<GameCategoryAdapter.GameCategoryHolder> implements OnItemClick, GameApi.ListenerGameApi {

    private final List<GameCategoryModel> listCategory;
    private final Context context;
    private OnItemClick callBack;

    public String titleGameSelected;
    public long idGameSelected;



    public GameCategoryAdapter(List<GameCategoryModel> listCategory, Context context) {
        this.listCategory = listCategory;
        this.context = context;
    }

    public void setOnItemClick(OnItemClick event) {
        callBack = event;
    }

    @NonNull
    @Override
    public GameCategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_game_category, parent, false);
        return new GameCategoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GameCategoryHolder holder, int position) {
        String currentLang = LocaleManager.getLanguage(context);
        GameCategoryModel categoryData = listCategory.get(position);

        switch (currentLang) {
            case "en":
                holder.tvCategoryName.setText(categoryData.getName());
                break;
            case "km":
                holder.tvCategoryName.setText(categoryData.getNameKM());
                break;
        }
//
//        if (currentLang == "en") {
//
//
//        } else if (currentLang == "km"){
//            holder.tvCategoryName.setText(categoryData.getNameKM());
//
//        }
        System.out.println("TuanHM en " + categoryData.getName());
        System.out.println("TuanHM km " + categoryData.getNameKM());
        System.out.println("TuanHM language " +  "U" +currentLang + "U");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.rvGamesInCategory.setLayoutManager(layoutManager);
        ArrayList<GameModel> listGame = new ArrayList<>(Arrays.asList(categoryData.getGames()));
        GameAdapter adapterListGame = new GameAdapter(listGame, context);
        adapterListGame.setOnItemClick(this::onItemClick);
        holder.rvGamesInCategory.setAdapter(adapterListGame);
    }

    @Override
    public int getItemCount() {
        return listCategory.size();
    }

    @Override
    public void onItemClick(String data) {
//        titleGameSelected = data;
//        System.out.println(titleGameSelected + ": TuanHM Title");
//        for (int i = 0; i < listCategory.size(); i ++) {
//            for (int j = 0; j < listCategory.get(i).getGames().length; j ++) {
//                if (listCategory.get(i).getGames()[j].getName().equals(titleGameSelected)) {
//                    idGameSelected = listCategory.get(i).getGames()[j].getID();
//                    System.out.println(idGameSelected + ": TuanHM ID");
//                }
//            }
//        }

        openGame(data);
    }

    private void openGame(String name) {
        GameApi api = GameApi.getInstance();
        api.listenerGameApi = this;
        api.callApi(GameApi.GET_GAMES, new GameByNameRequest(name));
    }

    @Override
    public void onPreRequest() {

    }

    @Override
    public void onSuccess(Object responseData) {
        if (responseData instanceof GameBaseResponseData) {
            GameBaseResponseData data = (GameBaseResponseData) responseData;
            Intent intent = new Intent(context, PlayGameActivity.class);
            intent.putExtra("OPEN_GAME", data.getItems()[0].getGames()[0]);
            context.startActivity(intent);
        }
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onFailure(String exp) {

    }

    public class GameCategoryHolder extends RecyclerView.ViewHolder {
        private final TextView tvCategoryName;
        private final RecyclerView rvGamesInCategory;

        public GameCategoryHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryName = itemView.findViewById(R.id.tv_category_name);
            rvGamesInCategory = itemView.findViewById(R.id.rv_games_in_category);
        }
    }


}



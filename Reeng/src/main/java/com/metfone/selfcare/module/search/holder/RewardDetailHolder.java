package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.home_kh.tab.adapter.events.OnClickHomeReward;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;
import com.squareup.picasso.Picasso;
import java.text.DecimalFormat;

import butterknife.BindView;

public class RewardDetailHolder extends BaseAdapter.ViewHolder {

//    @BindView(R.id.layout_root)
//    RelativeLayout rootLayout;
//
//    @BindView(R.id.iv_cover)
//    AppCompatImageView ivCover;
//
//    @BindView(R.id.ic_avatar)
//    CircleImageView ivAvatar;
//
//    @BindView(R.id.tv_discount)
//    AppCompatTextView tvDiscount;
//
//    @BindView(R.id.tv_desc)
//    AppCompatTextView tvDesc;
//
//    @BindView(R.id.tv_date)
//    AppCompatTextView tvDate;
//
//    private SearchAllListener.OnClickReward listener;
//    private Activity activity;
//
//
//    public RewardDetailHolder(View view, Activity activity, SearchAllListener.OnClickReward listener, int type) {
//        super(view);
//        this.listener = listener;
//        this.activity = activity;
//        ViewGroup.LayoutParams layoutParams = rootLayout.getLayoutParams();
//        rootLayout.requestLayout();
//    }
//
//    @Override
//    public void bindData(Object item, int position) {
//        super.bindData(item, position);
//        SearchRewardResponse.Result.ItemReward reward = (SearchRewardResponse.Result.ItemReward) item;
//        if (reward != null) {
//            if (reward.getImageList() != null && reward.getImageList().size() > 0) {
//                Picasso.with(activity).load(reward.
//                        getImageList().get(0)).into(ivCover);
//            } else {
//                //TODO resize image
//                Picasso.with(activity).load(R.drawable.df_image_home_poster).into(ivCover);
//            }
//            tvDesc.setText(activity.getString(R.string.kh_free));
//            Picasso.with(activity).load(reward.getImage()).into(ivAvatar);
//            tvDiscount.setText(reward.getDiscountRate() + " " + activity.getString(R.string.kh_dicount));
//            tvDate.setText(DateConvert.formatDateTime(activity, reward.getExpireDate()));
//            rootLayout.setOnClickListener(new OnSingleClickListener() {
//                @Override
//                public void onSingleClick(View view) {
//                    if (listener != null) {
//                        listener.onClickMenuMore(item);
//                    }
//                }
//            });
//            rootLayout.setOnClickListener(new OnSingleClickListener() {
//                @Override
//                public void onSingleClick(View view) {
//                    if(listener != null){
//                        listener.onClickReward((SearchRewardResponse.Result.ItemReward) item);
//                    }
//                }
//            });
//        }
//
//    }

    @BindView(R.id.layout_parent)
    LinearLayout rootLayout;

    @BindView(R.id.icon_reward_shop)
    AppCompatImageView ivCover;

    @BindView(R.id.ic_avatar)
    CircleImageView ivAvatar;

    @BindView(R.id.tv_discount_reward_shop)
    AppCompatTextView tvDiscount;

    @BindView(R.id.tv_point_reward_shop)
    AppCompatTextView tvDesc;

    @BindView(R.id.tv_sub_reward_shop)
    AppCompatTextView tvSubReward;

    private SearchAllListener.OnClickReward listener;
    private Activity activity;


    public RewardDetailHolder(View view, Activity activity, SearchAllListener.OnClickReward listener, int type) {
        super(view);
        this.listener = listener;
        this.activity = activity;
        ViewGroup.LayoutParams layoutParams = rootLayout.getLayoutParams();
        rootLayout.requestLayout();
    }

    @Override
    public void bindData(Object item, int position) {
        super.bindData(item, position);
        SearchRewardResponse.Result.ItemReward reward = (SearchRewardResponse.Result.ItemReward) item;
        if (reward != null) {
            //The same in layout xml
            int widthOfIvCoverInDp = 167;
            if (reward.getImageList() != null && reward.getImageList().size() > 0) {
                Glide.with(activity)
                    .asBitmap()
                    .load(reward.getImageList().get(0))
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, com.bumptech.glide.request.target.Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            int widthInPx = Utilities.convertDpToPixel(widthOfIvCoverInDp);
                            int heightInPx = (int) (((float) resource.getHeight() / resource.getWidth()) * widthInPx);
                            Bitmap scaledBitmap = Bitmap.createScaledBitmap(resource, widthInPx, heightInPx, true);
                            ivCover.setImageBitmap(scaledBitmap);
                            return true;
                        }
                    })
                    .into(ivCover);
            } else {
                Glide.with(activity)
                    .asBitmap()
                    .load(R.drawable.df_image_home_poster)
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, com.bumptech.glide.request.target.Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            int widthInPx = Utilities.convertDpToPixel(widthOfIvCoverInDp);
                            int heightInPx = (int) (((float) resource.getHeight() / resource.getWidth()) * widthInPx);
                            Bitmap scaledBitmap = Bitmap.createScaledBitmap(resource, widthInPx, heightInPx, true);
                            ivCover.setImageBitmap(scaledBitmap);
                            return true;
                        }
                    })
                    .into(ivCover);
            }

            int giftPoint = Integer.parseInt(reward.getScore());
            String descStr = (giftPoint == 0) ? ResourceUtils.getString(R.string.free) :
                String.format(activity.getString(R.string.reward_item_point), formatPoint(giftPoint));
            tvDesc.setText(descStr);
            Picasso.with(activity).load(reward.getImage()).into(ivAvatar);
            tvDiscount.setText(reward.getDiscountRate() + " " + activity.getString(R.string.kh_dicount));
            tvSubReward.setText(reward.getGiftName());
            rootLayout.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) {
                        listener.onClickMenuMore(item);
                    }
                }
            });
            rootLayout.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if(listener != null){
                        listener.onClickReward((SearchRewardResponse.Result.ItemReward) item);
                    }
                }
            });
        }

    }

    private String formatPoint(int point) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(point).replaceAll(",", "\\.");
    }
}

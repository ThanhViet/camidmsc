/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.home_kh.tab.adapter;

import android.app.Activity;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeCustomTopic;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;

import java.util.ArrayList;

import butterknife.BindView;

public class HorizontalRecyclerHolder extends BaseAdapter.ViewHolder {


    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;

    private ArrayList<Object> list;
    private CategoryDetailAdapter adapter;
    private KhHomeCustomTopic data;

    public HorizontalRecyclerHolder(View view, Activity activity, final TabHomeKhListener.OnAdapterClick listener, int viewType) {
        super(view);
        list = new ArrayList<>();
        adapter = new CategoryDetailAdapter(activity, viewType);
        adapter.setWidthItem(TabHomeUtils.getWidthPosterMovie());
        adapter.setListener(listener);
        adapter.setItems(list);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, R.drawable.divider_movie);
    }

    @Override
    public void bindData(Object item, int position) {
        if (list == null) list = new ArrayList<>();
        else list.clear();
        if (item instanceof KhHomeCustomTopic) {
            data = (KhHomeCustomTopic) item;
            list.addAll(data.topics);
        } else {
            data = null;
        }
        if (adapter != null) adapter.notifyDataSetChanged();
    }

}

package com.metfone.selfcare.module.netnews.HomeNews.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.netnews.HomeNews.fragment.HomeNewsFragment;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.newdetails.model.FavouriteModel;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.response.HomeNewsResponse;
import com.metfone.selfcare.util.Log;

/**
 * Created by HaiKE on 8/19/17.
 */

public class HomeNewsPresenter extends BasePresenter implements IHomeNewsPresenter {
    public static final String TAG = HomeNewsPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;
    long startTime;
    String apiUrl;
    private int sumHomeV5;
    private int sumHomeCacheV5;
    private int sumHomeCareV5, sumHomeCareCacheV5;

    public HomeNewsPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadDataV5(boolean refresh) {
        startTime = System.currentTimeMillis();
        mNewsApi.getNewsHomeV5(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                long endTime = System.currentTimeMillis();
                if (!isViewAttached() || !(getMvpView() instanceof HomeNewsFragment)) {
                    return;
                }
                sumHomeV5 = data.hashCode();
                if ((sumHomeV5 != sumHomeCacheV5 && sumHomeV5 != 0) || refresh) {
                    Gson gson = new Gson();
                    HomeNewsResponse childNewsResponse = gson.fromJson(data, HomeNewsResponse.class);
                    ((HomeNewsFragment) getMvpView()).bindData(childNewsResponse, true);
                    ((HomeNewsFragment) getMvpView()).loadDataSuccess(true);
                    ((HomeNewsFragment) getMvpView()).saveDataV5Cache(data);
                    LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_HOME, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
                    if (childNewsResponse.getError() != null) {
                        ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_NEWS_HOME, "News home error:" + data + " | " + apiUrl);
                    }

                }

            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                long endTime = System.currentTimeMillis();
                Log.d(TAG, "loadData: onFailure - " + message);
                if (!isViewAttached() || !(getMvpView() instanceof HomeNewsFragment)) {
                    return;
                }
                ((HomeNewsFragment) getMvpView()).loadDataSuccess(false);
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_HOME, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_NEWS_HOME, "News home error:" + message + " | " + apiUrl);
            }
        });
    }

    @Override
    public void loadDataCacheV5(SharedPref pref) {
        if (!isViewAttached()) {
            return;
        }
        String data = SharedPrefs.getInstance().get(SharedPrefs.HOME_NEW_V5, String.class);
        sumHomeCacheV5 = data.hashCode();
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
            HomeNewsResponse childNewsResponse = gson.fromJson(data, HomeNewsResponse.class);
            if (childNewsResponse != null) {
                String dataCare = SharedPrefs.getInstance().get(SharedPrefs.HOME_NEW_CARE_V5, String.class);
                FavouriteModel dataFavorite = SharedPrefs.getInstance().get("FAVOURITE", FavouriteModel.class);
                if (!TextUtils.isEmpty(dataCare) && dataFavorite != null && dataFavorite.getData().size() > 0) {
                    HomeNewsModel modelCare = gson.fromJson(dataCare, HomeNewsModel.class);
                    childNewsResponse.getData().add(0, modelCare);
                }
                ((HomeNewsFragment) getMvpView()).bindData(childNewsResponse, false);
                ((HomeNewsFragment) getMvpView()).loadDataSuccess(true);

            }
        }
    }

//    @Override
//    public void loadCategoryV5(final Context context) {
//        mNewsApi.getNewsCategory(new HttpCallBack() {
//            @Override
//            public void onSuccess(String data) throws Exception {
//                if (!isViewAttached() || !(getMvpView() instanceof HomeNewsFragment)) {
//                    return;
//                }
//                Gson gson = new Gson();
//                CategoryResponse categoryResponse = gson.fromJson(data, CategoryResponse.class);
//                if (categoryResponse != null && categoryResponse.getData() != null) {
//                    ((HomeNewsFragment) getMvpView()).bindCategoryV5(categoryResponse.getData());
//                } else {
//                    List<CategoryModel> categoryList = CommonUtils.getListCategoryNews(context);
//                    ((HomeNewsFragment) getMvpView()).bindCategoryV5(categoryList);
//                }
//            }
//
//            @Override
//            public void onFailure(String message) {
//                if (!isViewAttached() || !(getMvpView() instanceof HomeNewsFragment)) {
//                    return;
//                }
//                List<CategoryModel> categoryList = CommonUtils.getListCategoryNews(context);
//                ((HomeNewsFragment) getMvpView()).bindCategoryV5(categoryList);
//                super.onFailure(message);
//            }
//        });
//    }

    @Override
    public void loadCanCareV5() {
        String idCategory = "";
        FavouriteModel data = SharedPrefs.getInstance().get("FAVOURITE", FavouriteModel.class);
        if (data != null && data.getData() != null) {
            for (int i = 0; i < data.getData().size(); i++) {
                idCategory += data.getData().get(i);
                if (i != data.getData().size() - 1) {
                    idCategory += ",";
                }
            }
            if (!TextUtils.isEmpty(idCategory)) {
                mNewsApi.getNewCanCareHomeV5(idCategory, new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        if (!isViewAttached() || !(getMvpView() instanceof HomeNewsFragment)) {
                            return;
                        }
                        sumHomeCareV5 = data.hashCode();
                        if (sumHomeCareV5 != sumHomeCareCacheV5 && sumHomeCareV5 != 0) {
                            Gson gson = new Gson();
                            HomeNewsModel dataCare = gson.fromJson(data, HomeNewsModel.class);
                            ((HomeNewsFragment) getMvpView()).bindCanCareV5(dataCare);
                            ((HomeNewsFragment) getMvpView()).saveDataCareV5Cache(data);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                    }
                });
            }
        }

    }

//    @Override
//    public void loadDataCareCacheV5(SharedPref pref) {
//        if (!isViewAttached() || !(getMvpView() instanceof HomeNewsFragment)) {
//            return;
//        }
//        String data = SharedPrefs.getInstance().get(SharedPrefs.HOME_NEW_CARE_V5, String.class);
//        if (!TextUtils.isEmpty(data)) {
//            sumHomeCareCacheV5 = data.hashCode();
//            Gson gson = new Gson();
//            HomeNewsModel dataCare = gson.fromJson(data, HomeNewsModel.class);
//            ((HomeNewsFragment) getMvpView()).bindCanCareV5(dataCare);
//        }
//    }
}

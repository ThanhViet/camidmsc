package com.metfone.selfcare.module.newdetails.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.ArrayList;

public class SharedPref {
    public static final String PREF_NAME = "NetNews_preferences";
    //Key app SharedPref constant here

    public static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    public static final String PREF_KEY_PLAY_MODE = "PREF_KEY_PLAY_MODE";
    public static final String PREF_KEY_NEWS_CATEGORY = "PREF_KEY_NEWS_CATEGORY";
    public static final String PREF_KEY_VIDEO_CATEGORY = "PREF_KEY_VIDEO_CATEGORY";
    public static final String PREF_KEY_RADIO_CATEGORY = "PREF_KEY_RADIO_CATEGORY";
    public static final String PREF_KEY_SOURCE_LIST = "PREF_KEY_SOURCE_LIST";
    public static final String PREF_FONT_SIZE = "PREF_FONT_SIZE";
    public static final String PREF_SETTING = "PREF_SETTING";
    public static final String PREF_LAST_NEWS = "PREF_LAST_NEWS";
    public static final String PREF_TIME_CACHE = "PREF_TIME_CACHE";
    public static final String PREF_KEY_NEWS_TAB_CUSTOMIZED = "NEWS_TAB_CUSTOMIZED";
    public static final String PREF_KEY_HOME_DATA_CACHE = "PREF_HOME_DATA_CACHE";
    public static final String PREF_KEY_HOME_CATE_CACHE = "PREF_HOME_CATE_CACHE";

    public static final String KEY_MARK_READ = "KEY_MARK_READ";
    public static final String KEY_NOT_SHOW_TIP_MANAGE_TAB = "KEY_NOT_SHOW_TIP_MANAGE_TAB";

    private SharedPreferences pref;
    private Editor editor;
    private boolean autoCommit = true;

    public SharedPref(Context activity) {
        this.autoCommit = true;
        // pref = PreferenceManager.getDefaultSharedPreferences(mActivity);
        pref = activity.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        editor = pref.edit();
    }

    public static SharedPref newInstance(Context context) {
        return new SharedPref(context);
    }

    public SharedPref(Context activity, String name) {
        this.autoCommit = true;
        // pref = PreferenceManager.getDefaultSharedPreferences(mActivity);
        pref = activity.getSharedPreferences(name, Activity.MODE_PRIVATE);
        editor = pref.edit();
    }

    public SharedPref(Context activity, boolean autoCommit) {
        this.autoCommit = autoCommit;
        // pref = PreferenceManager.getDefaultSharedPreferences(mActivity);
        pref = activity
                .getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        editor = pref.edit();
    }

    // String----------------------------------------------------------------//
    public void putString(String key, String value) {
        editor.putString(key, value);
        if (autoCommit) {
            commit();
        }

    }

    public void clear() {
        if (pref != null)
            pref.edit().clear().apply();
    }

    public String getString(String key, String defaultValue) {
        return pref.getString(key, defaultValue);
    }

    // Int------------------------------------------------------------------//
    public void putInt(String key, int value) {
        editor.putInt(key, value);
        if (autoCommit) {
            commit();
        }
    }

    public int getInt(String key, int defaultValue) {
        return pref.getInt(key, defaultValue);
    }

    // Long-----------------------------------------------------------------//
    public void putLong(String key, long value) {
        editor.putLong(key, value);
        if (autoCommit) {
            commit();
        }
    }

    public long getLong(String key, long defaultValue) {
        return pref.getLong(key, defaultValue);
    }

    // Float------------------------------------------------------------------//
    public void putFloat(String key, float value) {
        editor.putFloat(key, value);
        if (autoCommit) {
            commit();
        }
    }

    public float getFloat(String key, float defaultValue) {
        return pref.getFloat(key, defaultValue);
    }

    // Boolean-------------------------------------------------------------//
    public boolean getBoolean(String key, boolean defaultValue) {
        return pref.getBoolean(key, defaultValue);
    }

    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        if (autoCommit) {
            commit();
        }
    }

    // ListString----------------------------------------------------------------//
    public int putListString(String key, ArrayList<String> listString) {
        for (int i = 0; i < listString.size(); i++)
            editor.putString("List#String" + i + key, listString.get(i));
        if (autoCommit)
            commit();
        return listString.size();
    }

    public ArrayList<String> getListString(String key, int sizeList,
                                           String defaultValue) {
        ArrayList<String> result = new ArrayList<String>();
        for (int i = 0; i < sizeList; i++)
            result.add(pref.getString("List#String" + i + key, defaultValue));
        return result;
    }

    // ListInt----------------------------------------------------------------//
    public int putListInt(String key, int[] listInt) {
        for (int i = 0; i < listInt.length; i++)
            editor.putInt("List#Int" + i + key, listInt[i]);
        if (autoCommit)
            commit();
        return listInt.length;
    }

    public int[] getListInt(String key, int sizeList, int defaultValue) {
        int[] result = new int[sizeList];
        for (int i = 0; i < sizeList; i++)
            result[i] = pref.getInt("List#Int" + i + key, defaultValue);
        return result;
    }

    // Commnit-------------------------------------------------------------//
    public void commit() {
        editor.commit();
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }
    // ---------------------------------------------------------------------//

}

package com.metfone.selfcare.module.home_kh.notification.model;

import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class KhNotificationReadResponse extends BaseResponse<KhNotificationReadResponse.Response> {
    public class Response {
        public int updated;
    }
}


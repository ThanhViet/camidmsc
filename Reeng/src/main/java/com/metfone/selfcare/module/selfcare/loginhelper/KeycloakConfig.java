package com.metfone.selfcare.module.selfcare.loginhelper;

public class KeycloakConfig {
    public static final String CLIENT_ID = "superapp-client";
    public static final String CLIENT_SECRET = "a2d5a415-1c9f-421d-84d4-5f6e0e814752";
    public static final String REDIRECT_URI = "mytel://com.mytel.mocha.auth";
    public static final String AUTH_ENDPOINT = "https://id.mytel.com.mm/auth/realms/cim/protocol/openid-connect/auth";
    public static final String TOKEN_ENDPOINT = "https://id.mytel.com.mm/auth/realms/cim/protocol/openid-connect/token";
}

package com.metfone.selfcare.module.selfcare.holder;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.adapter.SCPostageDetailItemAdapter;
import com.metfone.selfcare.module.selfcare.model.SCPostageDetailInfo;

public class SCPostageDetailHolder extends BaseViewHolder {
    private TextView tvTitle;
    private RecyclerView recyclerView;
    private SCPostageDetailItemAdapter adapter;
    private LinearLayoutManager layoutManager;
    private SCPostageDetailInfo data;
    private int postType;

    public SCPostageDetailHolder(Context context, View view, int postType) {
        super(view);
        this.postType = postType;

        tvTitle = view.findViewById(R.id.tvTitle);
        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCPostageDetailItemAdapter(context, postType);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);
    }

    public void setData(SCPostageDetailInfo data) {
        this.data = data;
        tvTitle.setText(data.getTitle());
        adapter.setItemsList(data.getData());
        adapter.notifyDataSetChanged();
    }
}

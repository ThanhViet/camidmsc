package com.metfone.selfcare.module.search.fragment;

import android.content.Intent;
import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.SearchMovieResponse;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.search.activity.RewardDetailActivity;
import com.metfone.selfcare.module.search.adapter.SearchDetailAdapter;
import com.metfone.selfcare.module.search.network.ApiCallback;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class SearchSubmitFragment extends SearchFragment implements BaseAdapter.OnLoadMoreListener {
    int currentPage;
    private SearchDetailAdapter adapter;
    private Http lastRequest;
    private boolean canLoadMore = false;

    public static SearchSubmitFragment newInstance() {
        Bundle args = new Bundle();
        SearchSubmitFragment fragment = new SearchSubmitFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static SearchSubmitFragment newInstance(Bundle bundle) {
        SearchSubmitFragment fragment = new SearchSubmitFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SearchAllSubmitFragment";
    }

    @Override
    protected void initListener() {
        super.initListener();
        data = new ArrayList<>();
        adapter = new SearchDetailAdapter(searchActivity);
        adapter.setParentType(Constants.TAB_SEARCH_ALL);
        adapter.setItems(data);
        adapter.setListener(this);

        switch (currentTabId) {
            case SearchUtils.TAB_SEARCH_VIDEO:
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, R.drawable.divider_default);
                BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
                apiCallbackVideo = new ApiCallback<ArrayList<Video>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<Video> result) {
                        SearchUtils.filterSearchVideo(keySearch, result, false);
                        onSearchSuccess(result);
                    }

                    @Override
                    public void onError(String s) {
                        onSearchFail();
                    }

                    @Override
                    public void onComplete() {
                        isSearchingVideo = false;
                        updateTabBarTitle(data == null ? 0 : data.size(), SearchUtils.TAB_SEARCH_VIDEO);
                    }
                };
                break;

            case SearchUtils.TAB_SEARCH_CHANNEL_VIDEO:
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, true);
                BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
                apiCallbackChannel = new ApiCallback<ArrayList<Channel>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<Channel> result) {
                        SearchUtils.filterSearchChannel(keySearch, result, false);
                        onSearchSuccess(result);
                    }

                    @Override
                    public void onError(String s) {
                        onSearchFail();
                    }

                    @Override
                    public void onComplete() {
                        isSearchingChannel = false;
                        updateTabBarTitle(data == null ? 0 : data.size(), SearchUtils.TAB_SEARCH_CHANNEL_VIDEO);
                    }
                };
                break;

            case SearchUtils.TAB_SEARCH_MOVIES:
                BaseAdapter.setupGridRecycler(searchActivity, recyclerView, null, adapter, 3, R.dimen.padding_16, true);
                BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
                mApiCallbackMovies = new ApiCallback<ArrayList<Movie>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<Movie> result) {
//                        SearchUtils.filterSearchMovies(keySearch, result, false);
                        onSearchSuccess(result);
                        if (result.size() < SearchUtils.LIMIT_REQUEST_SEARCH) {
                            hasReachedFilm = true;
                        }
                    }

                    @Override
                    public void onError(String s) {
                        onSearchFail();
                    }

                    @Override
                    public void onComplete() {
                        isSearchingMovies = false;
                        updateTabBarTitle(data == null ? 0 : data.size(), SearchUtils.TAB_SEARCH_MOVIES);
                    }
                };
                break;

            case SearchUtils.TAB_SEARCH_MUSIC:
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, R.drawable.divider_default);
                BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
                apiCallbackSong = new ApiCallback<ArrayList<SearchModel>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<SearchModel> result) {
                        SearchUtils.filterSearchMusic(keySearch, result, false, false);
                        onSearchSuccess(result);
                    }

                    @Override
                    public void onError(String s) {
                        onSearchFail();
                    }

                    @Override
                    public void onComplete() {
                        isSearchingMusic = false;
                        updateTabBarTitle(data == null ? 0 : data.size(), SearchUtils.TAB_SEARCH_MUSIC);
                    }
                };
                break;

            case SearchUtils.TAB_SEARCH_NEWS:
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, R.drawable.divider_default);
                BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
                apiCallbackNews = new ApiCallback<ArrayList<NewsModel>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<NewsModel> result) {
                        SearchUtils.filterSearchNews(keySearch, result, false);
                        onSearchSuccess(result);
                    }

                    @Override
                    public void onError(String s) {
                        onSearchFail();
                    }

                    @Override
                    public void onComplete() {
                        isSearchingNews = false;
                        updateTabBarTitle(data == null ? 0 : data.size(), SearchUtils.TAB_SEARCH_NEWS);
                    }
                };
                break;

            case SearchUtils.TAB_SEARCH_MV:
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, R.drawable.divider_default);
                BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
                apiCallbackMV = new ApiCallback<ArrayList<SearchModel>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<SearchModel> result) {
                        SearchUtils.filterSearchMusic(keySearch, result, false, false);
                        onSearchSuccess(result);
                    }

                    @Override
                    public void onError(String s) {
                        onSearchFail();
                    }

                    @Override
                    public void onComplete() {
                        isSearchingMusic = false;
                        updateTabBarTitle(data == null ? 0 : data.size(), SearchUtils.TAB_SEARCH_MV);
                    }
                };
                break;

            case SearchUtils.TAB_SEARCH_CHAT:
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, false);
                /* Hide search chat, only search contact */
//                initComparatorSearchChat();
//                initRecentList();
                break;
            case SearchUtils.TAB_SEARCH_TIIN:
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, R.drawable.divider_default);
                BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
                apiCallbackTiin = new ApiCallback<ArrayList<TiinModel>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<TiinModel> result) {
                        SearchUtils.filterSearchTiin(keySearch, result, false);
                        onSearchSuccess(result);
                    }

                    @Override
                    public void onError(String s) {
                        onSearchFail();
                    }

                    @Override
                    public void onComplete() {
                        isSearchingTiin = false;
                        updateTabBarTitle(data == null ? 0 : data.size(), SearchUtils.TAB_SEARCH_TIIN);
                    }
                };
                break;
            case SearchUtils.TAB_SEARCH_REWARD:
                //TODO SEARCH_REWARDS
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, R.drawable.divider_vert);
                BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
                apiCallbackReward = new ApiCallback<ArrayList<SearchRewardResponse.Result.ItemReward>>() {

                    @Override
                    public void onSuccess(String msg, ArrayList<SearchRewardResponse.Result.ItemReward> result) throws Exception {
                        onSearchSuccess(result);
                        isSearchingRewards = false;
                        updateTabBarTitle(data == null ? 0 : data.size(), SearchUtils.TAB_SEARCH_REWARD);
                    }

                    @Override
                    public void onError(String s) {

                    }

                    @Override
                    public void onComplete() {

                    }
                };
                break;

            default:
                BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, null, adapter, false);
                break;
        }
    }

    @Override
    public void cancelSearch() {
        if (lastRequest != null) lastRequest.cancel();
        if (searchContactsTask != null) {
            searchContactsTask.setListener(null);
            searchContactsTask.cancel(true);
            searchContactsTask = null;
        }

        isSearchingChat = false;
        isSearchingChannel = false;
        isSearchingVideo = false;
        isSearchingNews = false;
        isSearchingMovies = false;
        isSearchingMusic = false;
        isSearchingTiin = false;
    }

    @Override
    public void doSearch() {
        Log.d(TAG, "doSearch currentTabId: " + currentTabId + " - keySearch: " + keySearch);
        mustLoadData = false;
        canLoadMore = false;
        if (searchActivity != null && isAdded()) searchActivity.setShowDataSuggest(false);
        if (isRefresh) {
            showLoading();
            currentPage = 0;
            hasReachedFilm = false;
        }
        switch (currentTabId) {
            case SearchUtils.TAB_SEARCH_CHAT:
                searchChat();
                break;
            case SearchUtils.TAB_SEARCH_VIDEO:
                searchVideo(lastRequest, currentPage);
                break;
            case SearchUtils.TAB_SEARCH_CHANNEL_VIDEO:
                searchChannelVideo(lastRequest, currentPage);
                break;
            case SearchUtils.TAB_SEARCH_MOVIES:
                searchFilm(lastRequest, currentPage);
                break;
            case SearchUtils.TAB_SEARCH_MUSIC:
                searchSong(lastRequest, currentPage);
                break;
            case SearchUtils.TAB_SEARCH_NEWS:
                if (isRefresh) currentPage = 1;
                searchNews(lastRequest, currentPage);
                break;
            case SearchUtils.TAB_SEARCH_MV:
                searchMV(lastRequest, currentPage);
                break;
            case SearchUtils.TAB_SEARCH_TIIN:
                if (isRefresh) currentPage = 1;
                searchTiin(lastRequest, currentPage);
            case SearchUtils.TAB_SEARCH_REWARD: {
                if (currentPage == 0) {
                    currentPage = 1;
                }
                searchRewards(lastRequest, currentPage);
                break;
            }
        }
    }

    protected void onSearchSuccess(ArrayList results) {
        if (isRefresh || currentPage == 0) {
            isRefresh = false;
            if (data == null) data = new ArrayList<>();
            else data.clear();
        }
        if (Utilities.notEmpty(results)) {
            canLoadMore = results.size() > 10;
            data.addAll(results);
        }
        if (adapter != null) {
            adapter.setKeySearch(keySearch);
            adapter.notifyDataSetChanged();
        }
        if (Utilities.isEmpty(data)) {
            showLoadedEmpty();
        } else {
            showLoadedSuccess(false);
        }
    }

    protected void onSearchFail() {
        if (Utilities.isEmpty(data)) {
            showLoadedError();
        }
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        boolean change = false;
        if (Utilities.notEmpty(data)) {
            for (Object item : data) {
                if (item instanceof Channel && ((Channel) item).getId().equals(channel.getId())) {
                    ((Channel) item).setFollow(channel.isFollow());
                    ((Channel) item).setNumFollow(channel.getNumfollow());
                    change = true;
                }
            }
            if (change && adapter != null) adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoadMore() {
        Log.d(TAG, "onLoadMore canLoadMore: " + canLoadMore + ", currentPage: " + currentPage);
        if (canLoadMore) {
            currentPage++;
            doSearch();
        }
    }

    @Override
    public void onFinishedSearchContacts(String keySearch, ArrayList<Object> list) {
        isSearchingChat = false;
        if (searchActivity != null && isAdded() && !searchActivity.isShowDataSuggest()) {
            onSearchSuccess(list);
            int listSize = list == null ? 0 : list.size();
            updateTabBarTitle(listSize, SearchUtils.TAB_SEARCH_CHAT);
            updateRecentListStatus(listSize == 0);
        }
    }

    @Override
    protected void resetData() {
        if (data == null) data = new ArrayList<>();
        else data.clear();
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickMenuMore(Object data) {
        if (searchActivity != null) {
            searchActivity.onClickMenuMore(data);
            SearchRewardResponse.Result.ItemReward reward = (SearchRewardResponse.Result.ItemReward) data;
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.KEY_ITEM_REWARD, reward);
            Intent intent = new Intent(searchActivity, RewardDetailActivity.class);
            intent.putExtra(Constants.KEY_BUNDLE_REWARD, bundle);
            startActivity(intent);
        }
    }
}

package com.metfone.selfcare.module.keeng;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.util.Log;

public class App {
    private static final String TAG = "Keeng App";
    private RequestQueue mRequestQueue;
    private static App mInstance = null;
    public static App getInstance() {
        if (mInstance == null) {
            synchronized (App.class) {
                if (mInstance == null) mInstance = new App();

            }
        }
        return mInstance;
    }

    public Context getBaseContext()
    {
        return ApplicationController.self().getApplicationContext();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            synchronized (Volley.class) {
                if (mRequestQueue == null)
                    mRequestQueue = Volley.newRequestQueue(ApplicationController.self().getApplicationContext());
                VolleyLog.DEBUG = false;
            }
        }
        return mRequestQueue;
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            Log.i(TAG, "cancelPendingRequests: " + tag.toString());
            mRequestQueue.cancelAll(tag);
        }
    }
}

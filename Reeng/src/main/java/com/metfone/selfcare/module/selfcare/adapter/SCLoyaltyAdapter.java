package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCLoyaltyModel;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;

public class SCLoyaltyAdapter extends BaseAdapter<BaseViewHolder> {
    private final int TYPE_WELCOME = 1;
    private final int TYPE_POINT = 2;
    private final int TYPE_TELECOM = 3;
    private final int TYPE_VOUCHER = 4;
    private final int TYPE_TIER_PROCESS = 5;
    private final int TYPE_ABOUT = 6;
    private final int TYPE_EMPTY = 0;
    AbsInterface.OnLoyaltyListener listener;
    SCLoyaltyModel loyaltyModel;

    public SCLoyaltyAdapter(Context context, AbsInterface.OnLoyaltyListener listener) {
        super(context);
        this.listener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_WELCOME:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_loyalty_welcome, null);
                break;
            case TYPE_POINT:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_loyalty_info, null);
                break;
            case TYPE_TELECOM:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_loyalty_telecom, null);
                break;
//            case TYPE_VOUCHER:
//                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_loyalty_voucher, null);
//                break;
            case TYPE_TIER_PROCESS:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_loyalty_tier_process, null);
                break;
            case TYPE_ABOUT:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_loyalty_about, null);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case TYPE_WELCOME:
//                String username = ApplicationController.self().getPref().getString(SCConstants.PREFERENCE.SC_FULL_NAME, "Mytel");
                //Thieu viet hoa chu cai dau
//                ((TextView)holder.getView(R.id.tvTitle)).setText(mContext.getString(R.string.sc_loyalty_welcome, username));
                break;

            case TYPE_POINT:
                if (loyaltyModel != null) {
                    SCLoyaltyModel.Balance loyaltyPoint = null;
                    SCLoyaltyModel.Balance exchangePoint = null;
                    for (int i = 0; i < loyaltyModel.getBalances().size(); i++) {
                        SCLoyaltyModel.Balance temp = loyaltyModel.getBalances().get(i);
                        if ("MYTEL_LOYALTY_POINTS".equals(temp.getLoyaltyBalanceCode())) {
                            loyaltyPoint = temp;
                        } else if ("TELCO_EXCHANGEABLE_POINTS".equals(temp.getLoyaltyBalanceCode())) {
                            exchangePoint = temp;
                        }
                    }

                    if (loyaltyPoint != null) {
                        ((ImageView) holder.getView(R.id.tvLevelPoint)).setImageResource(SCUtils.getRankResource(loyaltyModel.getName()));
                        ((TextView) holder.getView(R.id.tvLevel)).setText(mContext.getString(R.string.loyalty_member, loyaltyModel.getName()));
                        ((TextView) holder.getView(R.id.tvLevelContent)).setText(mContext.getString(R.string.sc_points_new_level, SCUtils.numberFormat(loyaltyModel.getRemainScore()) + "", loyaltyModel.getNextRank()));
                        if (SCConstants.RANK_TYPE.DIAMOND.equals(loyaltyModel.getName().toUpperCase()))
                            ((TextView) holder.getView(R.id.tvLevelContent)).setVisibility(View.GONE);
                        else
                            ((TextView) holder.getView(R.id.tvLevelContent)).setVisibility(View.VISIBLE);
                    }

                    if (exchangePoint != null) {
                        ((TextView) holder.getView(R.id.tvLoyaltyPoint)).setText(SCUtils.numberFormat(exchangePoint.getBalance()) + "");
//                        ((TextView)holder.getView(R.id.tvLevel)).setText("Exchangable Points");
//                        ((TextView)holder.getView(R.id.tvLevelContent)).setText(mContext.getString(R.string.sc_points_expired, loyaltyModel.getRemainScore() + "", loyaltyModel.getNextRank()));
                    } else {
                        ((TextView) holder.getView(R.id.tvLoyaltyPoint)).setText("0");
                    }

                    holder.getView(R.id.tvPointHistory).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (listener != null)
                                listener.onPointHistoryClick();
                        }
                    });

                    holder.getView(R.id.tvLevelAbout).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (listener != null)
                                listener.onAboutClick();
                        }
                    });
                }
                break;
            case TYPE_TELECOM:
                holder.getView(R.id.layoutTitle).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.onTelecomRewardClick(0);
                    }
                });

                holder.getView(R.id.btnData).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.onTelecomRewardClick(0);
                    }
                });

                holder.getView(R.id.btnSMS).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.onTelecomRewardClick(1);
                    }
                });

                holder.getView(R.id.btnVoice).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.onTelecomRewardClick(2);
                    }
                });
                break;
            case TYPE_TIER_PROCESS:

                if (loyaltyModel != null) {
                    String level = loyaltyModel.getName();
                    switch (level.toUpperCase()) {
                        case SCConstants.RANK_TYPE.SILVER:
                            holder.getView(R.id.btnSilver).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                            break;
                        case SCConstants.RANK_TYPE.GOLD:
                            holder.getView(R.id.btnGold).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                            break;
                        case SCConstants.RANK_TYPE.PLATINUM:
                            holder.getView(R.id.btnPlatinum).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                            break;
                        case SCConstants.RANK_TYPE.DIAMOND:
                            holder.getView(R.id.btnDiamond).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                            break;
                        default:
                            holder.getView(R.id.btnWelcome).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                            break;
                    }
                }

                holder.getView(R.id.btnWelcome).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((TextView) holder.getView(R.id.tvContent)).setText(mContext.getString(R.string.sc_welcome_des));

                        holder.getView(R.id.btnWelcome).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                        holder.getView(R.id.btnSilver).setBackgroundColor(0);
                        holder.getView(R.id.btnGold).setBackgroundColor(0);
                        holder.getView(R.id.btnPlatinum).setBackgroundColor(0);
                        holder.getView(R.id.btnDiamond).setBackgroundColor(0);
                    }
                });
                holder.getView(R.id.btnSilver).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((TextView) holder.getView(R.id.tvContent)).setText(mContext.getString(R.string.sc_silver_des));
                        holder.getView(R.id.btnSilver).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                        holder.getView(R.id.btnWelcome).setBackgroundColor(0);
                        holder.getView(R.id.btnGold).setBackgroundColor(0);
                        holder.getView(R.id.btnPlatinum).setBackgroundColor(0);
                        holder.getView(R.id.btnDiamond).setBackgroundColor(0);
                    }
                });
                holder.getView(R.id.btnGold).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((TextView) holder.getView(R.id.tvContent)).setText(mContext.getString(R.string.sc_gold_des));
                        holder.getView(R.id.btnGold).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                        holder.getView(R.id.btnSilver).setBackgroundColor(0);
                        holder.getView(R.id.btnWelcome).setBackgroundColor(0);
                        holder.getView(R.id.btnPlatinum).setBackgroundColor(0);
                        holder.getView(R.id.btnDiamond).setBackgroundColor(0);
                    }
                });
                holder.getView(R.id.btnPlatinum).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((TextView) holder.getView(R.id.tvContent)).setText(mContext.getString(R.string.sc_platinum_des));
                        holder.getView(R.id.btnPlatinum).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                        holder.getView(R.id.btnSilver).setBackgroundColor(0);
                        holder.getView(R.id.btnGold).setBackgroundColor(0);
                        holder.getView(R.id.btnWelcome).setBackgroundColor(0);
                        holder.getView(R.id.btnDiamond).setBackgroundColor(0);
                    }
                });
                holder.getView(R.id.btnDiamond).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((TextView) holder.getView(R.id.tvContent)).setText(mContext.getString(R.string.sc_diamond_des));
                        holder.getView(R.id.btnDiamond).setBackgroundColor(mContext.getResources().getColor(R.color.sc_bg_rank));
                        holder.getView(R.id.btnSilver).setBackgroundColor(0);
                        holder.getView(R.id.btnGold).setBackgroundColor(0);
                        holder.getView(R.id.btnPlatinum).setBackgroundColor(0);
                        holder.getView(R.id.btnWelcome).setBackgroundColor(0);
                    }
                });
                break;
            case TYPE_ABOUT:
                if (holder.getView(R.id.tvTemp) instanceof TextView) {
                    TextView tvTemp = (TextView) holder.getView(R.id.tvTemp);
                    tvTemp.setPaintFlags(tvTemp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                    holder.getView(R.id.tvRefer).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (listener != null)
                                listener.onReferFriendClick();
                        }
                    });

                    tvTemp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (listener != null)
                                listener.onTermClick();
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_WELCOME;
        else if (position == 1)
            return TYPE_POINT;
        else if (position == 2)
            return TYPE_TELECOM;
        else if (position == 3)
            return TYPE_VOUCHER;
        else if (position == 4)
            return TYPE_TIER_PROCESS;
        else if (position == 5)
            return TYPE_ABOUT;
        else
            return TYPE_EMPTY;
    }

    public void setLoyaltyModel(SCLoyaltyModel loyaltyModel) {
        this.loyaltyModel = loyaltyModel;
    }
}

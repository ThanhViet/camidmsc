package com.metfone.selfcare.module.tiin.categorynow.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.CustomLoadMoreView;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.activitytiin.TiinActivity;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.categorynow.adapter.CategoryNowTiinAdapter;
import com.metfone.selfcare.module.tiin.categorynow.presenter.CategoryNowTiinPresenter;
import com.metfone.selfcare.module.tiin.categorynow.presenter.ICategoryNowTiinMvpPresenter;
import com.metfone.selfcare.module.tiin.categorynow.view.CategoryNowTiinMvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CategoryNowTiinFragment extends BaseFragment implements CategoryNowTiinMvpView, TiinListener.onCategoryNow, BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.btnBack)
    ImageView btnBack;
    Unbinder unbinder;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerViewCate;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.loadingView)
    View loadingView;
    private ICategoryNowTiinMvpPresenter mPresenter;
    private CategoryNowTiinAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private List<TiinEventModel> datasNow = new ArrayList<>();
    private String title;
    private int currentPage = 1;
    private boolean isRefresh;
    View notDataView, errorView;

    public static CategoryNowTiinFragment newInstance(Bundle bundle) {
        CategoryNowTiinFragment fragment = new CategoryNowTiinFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_tiin, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (mPresenter == null) {
            mPresenter = new CategoryNowTiinPresenter();
        }
        mPresenter.onAttach(this);
        setUp();
        return view;
    }

    private void setUp() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            title = bundle.getString(ConstantTiin.KEY_TITLE);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
        }
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorNewBg));
        }
        isRefresh = true;
        mPresenter.getCategoryEvent(currentPage, 3);
        adapter = new CategoryNowTiinAdapter(getTiinActivity(), R.layout.holder_tiin_event, datasNow, this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewCate.setHasFixedSize(true);
        recyclerViewCate.setLayoutManager(linearLayoutManager);
        recyclerViewCate.setItemAnimator(new DefaultItemAnimator());
        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(this);
        adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
        recyclerViewCate.setAdapter(adapter);

        notDataView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_nodata, (ViewGroup) recyclerViewCate.getParent(), false);
        notDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
        errorView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_failed, (ViewGroup) recyclerViewCate.getParent(), false);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });

        layout_refresh.setOnRefreshListener(this);
    }

    @OnClick(R.id.btnBack)
    public void onClickBack() {
        getBaseActivity().onBackPressed();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void loadingFail() {
        if (isRefresh) {
            adapter.setEmptyView(errorView);
        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        if (loadingView != null) {
            loadingView.setVisibility(View.GONE);
        }
        hideRefresh();
        if (!flag) {
            loadingFail();
        }
    }

    @Override
    public void bindData(List<TiinEventModel> response) {
        if (datasNow == null)
            datasNow = new ArrayList<>();

        hideRefresh();
        if (response != null) {

            loadingComplete(response);
        } else {
            loadingFail();
        }
    }

    public void loadingComplete(List<TiinEventModel> response) {
        int mCurrentCounter = response.size();
//        if(mCurrentCounter > 0)
//            unixTime = response.get(response.size() - 1).getUnixTime();
        if (isRefresh) {
            if (mCurrentCounter == 0) {
                adapter.setEmptyView(notDataView);
            } else {
                datasNow.clear();
                adapter.setNewData(response);
                datasNow.addAll(response);
            }
        } else {
            if (mCurrentCounter == 0) {
                adapter.loadMoreEnd();
            } else {
                adapter.addData(response);
                datasNow.addAll(response);
                adapter.loadMoreComplete();
            }
        }
    }

    @Override
    public void onItemClick(TiinModel model) {
        readTiin(model);
    }

    @Override
    public void onItemClickHeader(String title, int id) {
        Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
        intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY);
        intent.putExtra(ConstantTiin.KEY_CATEGORY, id);
        intent.putExtra(ConstantTiin.KEY_TITLE, title);
        getBaseActivity().startActivity(intent);
    }

    @Override
    public void onItemClickHashTag(TiinModel model) {
        Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
        intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY);
        intent.putExtra(ConstantTiin.KEY_CATEGORY, ConstantTiin.CATEGOTY_HASHTAG);
        intent.putExtra(ConstantTiin.KEY_HASHTAG, model.getHasTagSlug());
        intent.putExtra(ConstantTiin.KEY_TITLE, model.getHasTagName());
        getBaseActivity().startActivity(intent);
    }

    @Override
    public void onLoadMoreRequested() {
        currentPage++;
        isRefresh = false;
        mPresenter.getCategoryEvent(currentPage, 3);
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        mPresenter.getCategoryEvent(currentPage, 3);
    }

    @Override
    public void onItemClickMore(TiinModel model) {
        DialogUtils.showOptionTiinItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionTiin(object, menuId);
            }
        });
    }

    @OnClick(R.id.ivSearch)
    public void onItemClickSearch() {
        Utilities.openSearch(getBaseActivity(), Constants.TAB_TIIN_HOME);
    }
}

package com.metfone.selfcare.module.metfoneplus.dialog;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.model.BaseDataRequest;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.BaseUserResponseData;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.account.VerifyInfo;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.WsSubscriberUpdateCustomerInfoRequest;
import com.metfone.selfcare.network.metfoneplus.response.WsSubUpdateInfoCusResponse;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MPConfirmUpdateInformationBothDialog extends BaseCustomDialog {
    public static final String TAG = "MPConfirmInformationDialog";
    UserInfoBusiness userInfoBusiness;
    UserInfo userInfo;
    UserIdentityInfo userIdentityInfo;
    VerifyInfo verifyInfo;
    boolean isBoth = false;
    boolean isSuccess = false;
    private BaseMPSuccessDialog baseMPSuccessDialog;
    private String msgSuccessBccs;

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public void setVerify(VerifyInfo verifyInfo) {
        this.verifyInfo = verifyInfo;
    }

    public void setUserIdentityInfo(UserIdentityInfo userIdentityInfo) {
        this.userIdentityInfo = userIdentityInfo;
    }

    public MPConfirmUpdateInformationBothDialog(@NonNull Activity activity) {
        super(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getWindow() != null)
            getWindow().setLayout(UserInfoBusiness.getWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
        userInfoBusiness = new UserInfoBusiness(getActivity());
        mImage.setImageResource(R.drawable.img_dialog_3);
        mImage.setVisibility(View.GONE);
        flImage.setVisibility(View.VISIBLE);
        lavAnimations.setAnimation("lottie/img_dialog_women_thinking.json");
        lavAnimations.playAnimation();
        lavAnimations.setVisibility(View.VISIBLE);
        mTitle.setText(getContext().getString(R.string.tilte_dialog_confirm_update_to_both_service,
                userIdentityInfo.getPhone()));
        mButtonBottom.setTextColor(Color.parseColor("#80FFFFFF"));
        setTopButton(R.string.yes, R.drawable.bg_red_button_corner_6, buttonTop -> {
            mButtonTop.setEnabled(false);
            mButtonBottom.setEnabled(false);
            if (userInfo != null && verifyInfo != null && userIdentityInfo != null) {
                isBoth = true;
                updateUserIdentity(verifyInfo, userIdentityInfo);
            } else {
                ToastUtils.showToast(getActivity(), getActivity().getString(R.string.error));
            }
        });

        setBottomButton(R.string.no, R.drawable.bg_transparent_button_corner_6, buttonBottom -> {
            mButtonTop.setEnabled(false);
            mButtonBottom.setEnabled(false);
            dismiss();
        });
    }

    private void updateUserIdentity(VerifyInfo verifyInfo, UserIdentityInfo userIdentityInfo) {
        progressBar.setVisibility(View.VISIBLE);

        WsSubscriberUpdateCustomerInfoRequest request = MetfonePlusClient.createBaseRequest(
                new WsSubscriberUpdateCustomerInfoRequest(),
                Constants.WSCODE.WS_SUBSCRIBE_UPDATE_CUSTOMER_INFO);
        WsSubscriberUpdateCustomerInfoRequest.Request subReq = request.new Request();
        updateMPRequest(userIdentityInfo, subReq, verifyInfo);
        Log.e("ttt", "updateUserIdentity: " + subReq.toString());
        MetfonePlusClient client = new MetfonePlusClient();
        client.wsSubscriberUpdateInfoCus(subReq, new MPApiCallback<WsSubUpdateInfoCusResponse>() {
            @Override
            public void onResponse(Response<WsSubUpdateInfoCusResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null && response.body().getResult() != null) {
                    WsSubUpdateInfoCusResponse baseResponse = response.body();
                    if (baseResponse.getResult() != null) {
                        if ("0".equals(baseResponse.getResult().getErrorCode())) {
                            if (baseResponse.getResult().getWsResponse().getErrorCode().equals("0")) {
                                msgSuccessBccs = baseResponse.getResult().getWsResponse().getErrorDescription();
                                updateUser(userInfo);
                            } else {
                                dismiss();
                                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                                        R.drawable.image_error_dialog,
                                        getContext().getString(R.string.title_sorry),
                                        baseResponse.getResult().getWsResponse().getErrorDescription(),
                                        false);
                                baseMPSuccessDialog.show();
                            }
                        } else {
                            dismiss();
                            baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                                    R.drawable.image_error_dialog,
                                    getContext().getString(R.string.title_sorry),
                                    baseResponse.getResult().getUserMsg(),
                                    false);
                            baseMPSuccessDialog.show();
                        }
                    } else {
                        updateUserIdentity(verifyInfo, userIdentityInfo); // retry
                       // dismiss();
                       // baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
//                                R.drawable.image_error_dialog,
//                                getContext().getString(R.string.title_sorry),
//                                getContext().getString(R.string.service_error),
//                                false);
                      //  baseMPSuccessDialog.show();
                    }
                } else {
                    updateUserIdentity(verifyInfo, userIdentityInfo);
//                    dismiss();
//                    baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
//                            R.drawable.image_error_dialog,
//                            getContext().getString(R.string.title_sorry),
//                            getContext().getString(R.string.service_error),
//                            false);
//                    baseMPSuccessDialog.show();
                }
            }

            @Override
            public void onError(Throwable error) {
                progressBar.setVisibility(View.GONE);
                dismiss();
                Log.e("Update", "onFailure: updateUserIdentity");
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                        R.drawable.image_error_dialog,
                        getContext().getString(R.string.title_sorry),
                        getContext().getString(R.string.service_error),
                        false);
                baseMPSuccessDialog.show();
            }
        });
    }

    private void updateUser(UserInfo userInfo) {
        progressBar.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.updateUser(token, new BaseDataRequest<>(userInfo, "string", "string", "string", "string")).enqueue(new Callback<BaseResponse<BaseUserResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseUserResponseData>> call, Response<BaseResponse<BaseUserResponseData>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    BaseResponse<BaseUserResponseData> baseResponse = response.body();
                    if ("00".equals(baseResponse.getCode())) {
                        if (baseResponse.getData() != null) {
                            if (response.body().getData().getUser() != null) {
                                userInfoBusiness.setUser(baseResponse.getData().getUser());
                            }
                            if (response.body().getData().getServices() != null) {
                                userInfoBusiness.setServiceList(baseResponse.getData().getServices());
                            }
                            userInfoBusiness.setIdentifyProviderList(baseResponse.getData().getIdentifyProviders());
                            verifyInformation(verifyInfo);
                        } else {
                            progressBar.setVisibility(View.GONE);
                            dismiss();
                            baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                                    R.drawable.image_error_dialog,
                                    getContext().getString(R.string.title_sorry),
                                    baseResponse.getMessage(),
                                    false);
                            baseMPSuccessDialog.show();
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        dismiss();
                        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                                R.drawable.image_error_dialog,
                                getContext().getString(R.string.title_sorry),
                                baseResponse.getMessage(),
                                false);
                        baseMPSuccessDialog.show();
                    }
                } else {
                    updateUser(userInfo);
//                    progressBar.setVisibility(View.GONE);
//                    dismiss();
//                    baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
//                            R.drawable.image_error_dialog,
//                            getContext().getString(R.string.title_sorry),
//                            getContext().getString(R.string.service_error),
//                            false);
//                    baseMPSuccessDialog.show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseUserResponseData>> call, Throwable t) {
                Log.e("Update", "onFailure: updateUser");
                progressBar.setVisibility(View.GONE);
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                        R.drawable.image_error_dialog,
                        getContext().getString(R.string.title_sorry),
                        getContext().getString(R.string.service_error),
                        false);
                baseMPSuccessDialog.show();
                dismiss();
            }
        });
    }

    private void verifyInformation(VerifyInfo verifyInfo) {
        progressBar.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.verifyID(token, new BaseDataRequest<>(verifyInfo, "string", "string", "string", "string")).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.code() == ERROR_UNAUTHORIZED) {
                    dismiss();
                    return;
                }
                if (response.body() != null) {
                    //comment to test
                    if ("00".equals(response.body().getCode())) {
                        updateUiDialog();
                    } else {
                        dismiss();
                        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                                R.drawable.image_error_dialog,
                                getContext().getString(R.string.title_sorry),
                                response.body().getMessage(),
                                false);
                        baseMPSuccessDialog.show();
                    }
                } else {
//                    dismiss();
//                    baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
//                            R.drawable.image_error_dialog,
//                            getContext().getString(R.string.title_sorry),
//                            getContext().getString(R.string.service_error),
//                            false);
//                    baseMPSuccessDialog.show();
                    verifyInformation(verifyInfo);
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                dismiss();
                Log.e("Update", "onFailure: verify KYC");
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                        R.drawable.image_error_dialog,
                        getContext().getString(R.string.title_sorry),
                        getContext().getString(R.string.service_error),
                        false);
                baseMPSuccessDialog.show();
            }
        });
    }

    private void updateUiDialog() {
        isSuccess = true;
        String msg = getContext().getString(R.string.mp_confirm_update_to_cam_successfull);
        if (isBoth) {
            msg = msgSuccessBccs;
        }
        mImage.setVisibility(View.GONE);
        mImage.setImageResource(R.drawable.img_dialog_1);
        lavAnimations.setVisibility(View.VISIBLE);
        lavAnimations.cancelAnimation();
        lavAnimations.setAnimation("lottie/img_dialog_women_success.json");
        lavAnimations.playAnimation();
        mTitle.setText(R.string.successfully);
        mContent.setText(msg);
        setButtonTopVisibility(View.GONE);
        setButtonBottomVisibility(View.GONE);
    }

    private void updateMPRequest(UserIdentityInfo userIdentityInfo,
                                 WsSubscriberUpdateCustomerInfoRequest.Request subReq,
                                 VerifyInfo verifyInfo) {
        subReq.setLanguage(LocaleManager.getLanguage(ApplicationController.self().getApplicationContext()));
        subReq.setLang("en_US");
        subReq.setIsdn(userIdentityInfo.getPhone());
        subReq.setSubName(userIdentityInfo.getSubName());
        subReq.setSubGender(userIdentityInfo.getSubGender());
        subReq.setSubDateBirth(userIdentityInfo.getSubDateBirth());
        subReq.setRelationship(userIdentityInfo.getRelationship());

        subReq.setFullAddress(userIdentityInfo.getAddress());
        subReq.setProvince(userIdentityInfo.getProvince());
        subReq.setDistrict(userIdentityInfo.getDistrict());
        subReq.setStreet(userIdentityInfo.getStreetName());
        subReq.setHomeNo(userIdentityInfo.getHome());

        subReq.setFullName(userIdentityInfo.getName());
        subReq.setDob(userIdentityInfo.getBirthDate());
        subReq.setGender(userIdentityInfo.getSex());
        subReq.setIdType(Integer.parseInt(userIdentityInfo.getIdType()));
        subReq.setIdNumber(userIdentityInfo.getIdNo());
        subReq.setIssueDate(userIdentityInfo.getIdIssueDate());

        subReq.setExpireDate(userIdentityInfo.getIdExpireDate());
        subReq.setVisaDate(userIdentityInfo.getExpireVisa());
        subReq.setContact(userIdentityInfo.getTelFax());
        subReq.setNationality(userIdentityInfo.getNationality());
        subReq.setIsScan(userIdentityInfo.getIsScan());
        subReq.setCommune("");

        if (verifyInfo != null) {
            subReq.setImage1Name("front_side");
            subReq.setImage2Name("back_side");
            subReq.setImage3Name("portrait");
            subReq.setImage1Data(verifyInfo.getImage_front());
            subReq.setImage2Data(verifyInfo.getImage_back());
            subReq.setImage3Data(verifyInfo.getImage_selfie());
        }
    }

    @Override
    public void dismiss() {
        if (progressBar.getVisibility() == View.GONE) {
            super.dismiss();
            if (isSuccess) {
                NavigateActivityHelper.navigateToSetUpProfileClearTop(getActivity());
                getActivity().finish();
            }
        }
    }
}

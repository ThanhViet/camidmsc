/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/8
 *
 */

package com.metfone.selfcare.module.myviettel.activity;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NonViettelActivity extends BaseSlidingFragmentActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.button_back)
    View btnBack;
    @BindView(R.id.button_help)
    View btnHelp;
    @BindView(R.id.tv_avatar_default)
    TextView tvAvatarDefault;
    @BindView(R.id.tv_contact_avatar)
    TextView tvContactAvatar;
    @BindView(R.id.iv_profile_avatar)
    CircleImageView ivProfileAvatar;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.tv_name)
    EllipsisTextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.button_learn_more)
    View btnLearnMore;

    private ApplicationController application;
    private String title;
    private String linkHelp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_viettel);
        ButterKnife.bind(this);
        application = (ApplicationController) getApplication();
        if (savedInstanceState != null) {
            title = savedInstanceState.getString(Constants.KEY_TITLE);
            linkHelp = savedInstanceState.getString(Constants.KEY_POSITION);
        } else if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                title = bundle.getString(Constants.KEY_TITLE);
                linkHelp = bundle.getString(Constants.KEY_POSITION);
            }
        }
        initListener();
    }

    private void initListener() {
        if (tvTitle != null) {
            if (Utilities.notEmpty(title)) tvTitle.setText(title);
            tvTitle.setSelected(true);
        }
        ImageBusiness.setResource(ivCover, R.drawable.bg_profile_mvt);
        if (btnBack != null) {
            btnBack.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    finish();
                }
            });
        }
        if (btnLearnMore != null) btnLearnMore.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (application == null) return;
                String link = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_LINK_SWITCH_TO_VIETTEL);
                if (!TextUtils.isEmpty(link) && !"-".equals(link)) {
                    DeepLinkHelper.getInstance().openSchemaLink(NonViettelActivity.this, link);
                }
            }
        });
        if (btnHelp != null) {
            if (!TextUtils.isEmpty(linkHelp) && !"-".equals(linkHelp))
                btnHelp.setVisibility(View.VISIBLE);
            else
                btnHelp.setVisibility(View.GONE);
            btnHelp.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    DeepLinkHelper.getInstance().openSchemaLink(NonViettelActivity.this, linkHelp);
                }
            });
        }
        if (application != null) {
            if (tvDesc != null) {
                String desc = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_MSG_SWITCH_TO_VIETTEL);
                if (TextUtils.isEmpty(desc))
                    desc = application.getString(R.string.msg_switch_to_viettel_network);
                tvDesc.setText(Html.fromHtml(desc));
            }
            if (application.getReengAccountBusiness().isAnonymousLogin()) {
                if (ivProfileAvatar != null) ivProfileAvatar.setVisibility(View.VISIBLE);
                if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
                if (tvAvatarDefault != null) tvAvatarDefault.setVisibility(View.GONE);
                ImageBusiness.setResource(ivProfileAvatar, R.drawable.ic_tab_home_avatar_default);
            } else {
                ReengAccount account = application.getReengAccountBusiness().getCurrentAccount();
                if (account != null) {
                    String lAvatar = account.getLastChangeAvatar();
                    if (TextUtils.isEmpty(lAvatar)) {
                        if (ivProfileAvatar != null) ivProfileAvatar.setVisibility(View.VISIBLE);
                        if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
                        if (tvAvatarDefault != null) tvAvatarDefault.setVisibility(View.GONE);
                        ImageBusiness.setResource(ivProfileAvatar, R.drawable.ic_tab_home_avatar_default);
                    } else {
                        application.getAvatarBusiness().setMyAvatar(ivProfileAvatar, tvContactAvatar
                                , tvAvatarDefault, account, null);
                    }
                    if (tvName != null) tvName.setText(account.getName());
                    if (tvPhone != null) tvPhone.setText(account.getJidNumber());
                }
            }
        }
    }

}

package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.holder.SCHomeBannerHolder;
import com.metfone.selfcare.module.selfcare.holder.SCHomeControlHolder;
import com.metfone.selfcare.module.selfcare.holder.SCHomeInfoHolder;
import com.metfone.selfcare.module.selfcare.model.SCBanner;
import com.metfone.selfcare.module.selfcare.model.SCLoyaltyModel;
import com.metfone.selfcare.module.selfcare.model.SCSubListModel;

import java.util.ArrayList;

public class SCHomeAdapter extends BaseAdapter<BaseViewHolder> {
    private final int TYPE_INFO = 1;
    private final int TYPE_CONTROL = 2;
    private final int TYPE_BANNER = 3;
    private final int TYPE_EMPTY = 0;
    AbsInterface.OnSCHomeListener listener;

    ArrayList<SCSubListModel> subList = new ArrayList<>();
    SCLoyaltyModel loyaltyModel;
    String username = "";
    String fullname = "";
    String avatar = "";
    ArrayList<SCBanner> bannerList = new ArrayList<>();

    public SCHomeAdapter(Context context, AbsInterface.OnSCHomeListener listener) {
        super(context);
        this.listener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_INFO:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_home_info, null);
                return new SCHomeInfoHolder(mContext, view, listener);
            case TYPE_CONTROL:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_home_control, null);
                return new SCHomeControlHolder(mContext, view, listener);
            case TYPE_BANNER:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_sc_home_banner, null);
                return new SCHomeBannerHolder(mContext, view, listener);
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case TYPE_INFO:
                if (holder instanceof SCHomeInfoHolder) {
                    ((SCHomeInfoHolder) holder).setUserInfo(username, fullname, avatar);
                    ((SCHomeInfoHolder) holder).setSubList(subList);
                    ((SCHomeInfoHolder) holder).setLoyaltyModel(loyaltyModel);
                }
                break;
            case TYPE_CONTROL:

                break;
            case TYPE_BANNER:
                if (holder instanceof SCHomeBannerHolder)
                    ((SCHomeBannerHolder) holder).setData(bannerList);
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_INFO;
        else if (position == 1)
            return TYPE_CONTROL;
        else if (position == 2)
            return TYPE_BANNER;
        else
            return TYPE_EMPTY;
    }

    public void setUserInfo(String username, String fullname, String avatar) {
        this.username = username;
        this.fullname = fullname;
        this.avatar = avatar;
    }

    public void setLoyaltyModel(SCLoyaltyModel loyaltyModel) {
        this.loyaltyModel = loyaltyModel;
    }

    public void setSubList(ArrayList<SCSubListModel> subList) {
        this.subList = subList;
    }

    public void setBannerList(ArrayList<SCBanner> bannerList) {
        this.bannerList = bannerList;
    }
}

package com.metfone.selfcare.module.tiin.childtiin.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.CustomLoadMoreView;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.event.TabTiinEvent;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.childtiin.adapter.ChildTiinAdapter;
import com.metfone.selfcare.module.tiin.childtiin.presenter.ChildTiinPresenter;
import com.metfone.selfcare.module.tiin.childtiin.presenter.IChildTiinMvpPresenter;
import com.metfone.selfcare.module.tiin.childtiin.view.ChildTiinMvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ChildTiinFragment extends BaseFragment implements ChildTiinMvpView, SwipeRefreshLayout.OnRefreshListener, TiinListener.onCategoryItemListener, BaseQuickAdapter.RequestLoadMoreListener {

    IChildTiinMvpPresenter mPresenter;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    Unbinder unbinder;
    @BindView(R.id.loadingView)
    View loadingView;
    @BindView(R.id.loadingFail)
    View loadingFail;

    View notDataView, errorView;
    private LinearLayoutManager layoutManager;
    private List<TiinModel> datas = new ArrayList<>();
    private ChildTiinAdapter adapter;
    private int category = 0;
    private int currentPage = 1;
    private boolean isRefresh;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private long unixTime = 0;
    private ListenerUtils listenerUtils;

    public static ChildTiinFragment newInstance(int category) {
        Bundle args = new Bundle();
        args.putInt("category", category);
        ChildTiinFragment fragment = new ChildTiinFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child_tiin, container, false);
        if (mPresenter == null) {
            mPresenter = new ChildTiinPresenter();
        }
        mPresenter.onAttach(this);
        unbinder = ButterKnife.bind(this, view);
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
        setUp();
        return view;
    }

    private void setUp() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            category = bundle.getInt("category", 0);
        }
        isRefresh = true;
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorNewBg));
        }
        if (loadingView != null) {
            loadingView.setVisibility(View.VISIBLE);
        }
        unixTime = 0;
        mPresenter.getCategoryNew(category, currentPage, 10, unixTime);
        if (category == 101 || category == 6) { //todo anh,dep
            adapter = new ChildTiinAdapter(getBaseActivity(), R.layout.holder_lady_new, datas, category, this);
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getTop(), getResources().getDimensionPixelOffset(R.dimen.v5_spacing_normal), recyclerView.getPaddingBottom());
            recyclerView.setLayoutManager(staggeredGridLayoutManager);
        } else if (category == 94) { //todo video
            layoutManager = new LinearLayoutManager(getTiinActivity());
            if (recyclerView.getItemDecorationCount() <= 0) {
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
            adapter = new ChildTiinAdapter(getTiinActivity(), R.layout.holder_large_video_tiin, datas, category, this);
        } else {

            layoutManager = new LinearLayoutManager(getTiinActivity());
            if (recyclerView.getItemDecorationCount() <= 0) {
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
            adapter = new ChildTiinAdapter(getTiinActivity(), R.layout.holder_large_tiin, datas, category, this);

        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(this);
        adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
        recyclerView.setAdapter(adapter);

        notDataView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_nodata, (ViewGroup) recyclerView.getParent(), false);
        notDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
        errorView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_failed, (ViewGroup) recyclerView.getParent(), false);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });

        layout_refresh.setOnRefreshListener(this);
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        if (loadingView != null) {
            loadingView.setVisibility(View.GONE);
        }
        hideRefresh();
        if (!flag) {
            loadingFail();
        } else {
//            isLoadSuccess = flag;
        }
    }

    public void loadingFail() {
        if (isRefresh) {
            adapter.setEmptyView(errorView);
        } else {
            adapter.loadMoreFail();
        }
    }


    @Override
    public void bindData(List<TiinModel> response) {
        if (datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if (response != null) {
            loadingComplete(response);
        } else {
            loadingFail();
        }

    }

    private void loadingComplete(List<TiinModel> response) {
        int mCurrentCounter = response.size();
        if (mCurrentCounter > 0)
            unixTime = response.get(response.size() - 1).getUnixTime();
        if (isRefresh) {
            if (mCurrentCounter == 0) {
                adapter.setEmptyView(notDataView);
            } else {
                datas.clear();
                datas.addAll(response);
                adapter.setNewData(datas);
                if (layoutManager != null)
                    layoutManager.scrollToPosition(0);
            }
        } else {
            if (mCurrentCounter == 0) {
                adapter.loadMoreEnd();
            } else {
                adapter.addData(response);
                adapter.loadMoreComplete();
            }
        }
    }

    @Override
    public void onLoadMoreRequested() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                currentPage++;
                isRefresh = false;
                mPresenter.getCategoryNew(category, currentPage, 10, unixTime);
            }
        }, 1000);
    }

    private void scrollToTop() {
        if (layoutManager != null && recyclerView != null) {
            try {
                if (category == 6) {
                    if (staggeredGridLayoutManager != null) {
                        staggeredGridLayoutManager.smoothScrollToPosition(recyclerView, null, 0);
                    }
                } else {
                    layoutManager.smoothScrollToPosition(recyclerView, null, 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(getBaseActivity()) || recyclerView == null || mPresenter == null)
            return;
        onRefresh();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (mPresenter != null) {
            mPresenter.onDetach();
        }
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        unixTime = 0;
        mPresenter.getCategoryNew(category, currentPage, 10, unixTime);
    }

    @Override
    public void onItemClick(TiinModel model) {
        readTiin(model);
    }

    @OnClick(R.id.tvLoadFail)
    public void loadFailClick() {
        loadingFail.setVisibility(View.GONE);
        onRefresh();
    }

    public void scrollToPosition(int position) {
        Log.e("---Duong----","ChildTiinFragment");
        if (layoutManager != null && recyclerView != null) {
            recyclerView.stopScroll();
            layoutManager.smoothScrollToPosition(recyclerView, null, 0);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(TabTiinEvent tab) {
        if (tab.getPosition() != 0) {
            scrollToTop();
        }
    }

    @Override
    public void onItemClickMore(TiinModel model) {
        DialogUtils.showOptionTiinItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionTiin(object, menuId);
            }
        });
    }

    @Override
    public void onItemClickHashTag(TiinModel model) {

    }
}

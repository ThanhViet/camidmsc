package com.metfone.selfcare.module.games.api;

import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.model.BaseDataRequest;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.tabGame.BestScoreGameIDRequest;
import com.metfone.selfcare.model.tabGame.GameBaseResponseData;
import com.metfone.selfcare.model.tabGame.GameByNameRequest;
import com.metfone.selfcare.model.tabGame.GameListResponseData;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.model.tabGame.GamePlayMostTimeRequest;
import com.metfone.selfcare.model.tabGame.GamePlayedRequest;
import com.metfone.selfcare.model.tabGame.SaveGameRequest;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.util.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static org.jivesoftware.smack.packet.ReengMessagePacket.SubType.toast;

import android.widget.Toast;

public class GameApi {
    private final String TAG = "GameUtils";
    public static final String GET_GAMES = "v1/game/games"; // danh sach game hien thi
    public static final String GET_BEST_SCORE = "v1/game/best-score"; // lay best score theo game
    public static final String GET_PLAYED_GAMES = "v1/game/played"; // cac game da choi
    public static final String GET_GAMES_PLAY_MOST_TIME = "v1/game/play-most-time"; // danh sach game choi nhieu lan nhat
    public static final String GET_BEST_SCORE_GAMES = "v1/game/best-score-games"; // danh sach game diem cao trong range = day/week/month
    public static final String SAVE_GAME = "v1/game/save-game"; // ghi history choi game
    public static final String GET_GAME_CATEGORIES = "v1/game/categories"; // danh sach category game
    public static final String GET_GAME_TOP_TEN = "v1/game/top-ten"; // danh sach top 10 game
    private static GameApi INSTANCE;
    public ListenerGameApi listenerGameApi;

    public static GameApi getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new GameApi();
        }
        return INSTANCE;
    }

    private GameApi() {}


    public void getGameByName(String name, final ListenerGameApi callBack) {

        callBack.onPreRequest();
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        GameByNameRequest gameByNameRequest = new GameByNameRequest(name);
        BaseDataRequest<GameByNameRequest> request = new BaseDataRequest<GameByNameRequest>(gameByNameRequest, "", "", "", "");
        apiService.getGameByName(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {   //
            @Override
            public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                   Response<BaseResponse<GameBaseResponseData>> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        callBack.onSuccess(response.body().getData());
                    } else
                        callBack.onError(response.body().getMessage());
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                callBack.onFailure(t.getMessage());
            }
        });
    }

    public void getBestScore(String gameID, final ListenerGameApi callBack){
        callBack.onPreRequest();
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        BestScoreGameIDRequest bestScoreGameIDRequest = new BestScoreGameIDRequest(gameID);
        BaseDataRequest<BestScoreGameIDRequest> request = new BaseDataRequest<BestScoreGameIDRequest>(bestScoreGameIDRequest, "", "", "", "");
        apiService.getBestScoreByGameID(token, request).enqueue(new Callback<BaseResponse<GameModel>>() {
            @Override
            public void onResponse(Call<BaseResponse<GameModel>> call,
                                   Response<BaseResponse<GameModel>> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())){
                        callBack.onSuccess(response.body().getData());
                    } else
                        callBack.onError(response.body().getMessage());
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<GameModel>> call, Throwable t) {
                callBack.onFailure(t.getMessage());
            }
        });
    }

    public void getPlayedGames(int range, final ListenerGameApi callBack){
        callBack.onPreRequest();
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        GamePlayedRequest gamePlayedRequest = new GamePlayedRequest(range);
        BaseDataRequest<GamePlayedRequest> request = new BaseDataRequest<GamePlayedRequest>(gamePlayedRequest, "", "", "", "");
        apiService.getPlayedGames(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                   Response<BaseResponse<GameBaseResponseData>> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())){
                        callBack.onSuccess(response.body().getData());
                    } else
                        callBack.onError(response.body().getMessage());
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                callBack.onFailure(t.getMessage());
            }
        });
    }

    public void getGamePlayMostTime(int range, int pageIndex, int pageSize, final ListenerGameApi callBack){
        callBack.onPreRequest();
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        GamePlayMostTimeRequest gamePlayMostTimeRequest = new GamePlayMostTimeRequest(range, pageIndex, pageSize);
        BaseDataRequest<GamePlayMostTimeRequest> request = new BaseDataRequest<GamePlayMostTimeRequest>(gamePlayMostTimeRequest, "", "", "", "");
        apiService.getPlayMostTimeGames(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                   Response<BaseResponse<GameBaseResponseData>> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())){
                        callBack.onSuccess(response.body().getData());
                    } else
                        callBack.onError(response.body().getMessage());
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                callBack.onFailure(t.getMessage());
            }
        });
    }

    public void getBestScoreGames(int range, final ListenerGameApi callBack){
        callBack.onPreRequest();
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        GamePlayedRequest gamePlayedRequest = new GamePlayedRequest(range);
        BaseDataRequest<GamePlayedRequest> request = new BaseDataRequest<GamePlayedRequest>(gamePlayedRequest, "", "", "", "");
        apiService.getBestScoreGames(token, request).enqueue(new Callback<BaseResponse<GameListResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<GameListResponseData>> call,
                                   Response<BaseResponse<GameListResponseData>> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())){
                        callBack.onSuccess(response.body().getData());
                    } else
                        callBack.onError(response.body().getMessage());
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<GameListResponseData>> call, Throwable t) {
                callBack.onFailure(t.getMessage());
            }
        });
    }

    public void saveGame(String gameID, String playerName, int score, final ListenerGameApi callBack){
        callBack.onPreRequest();
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        SaveGameRequest gameSaveGameRequest = new SaveGameRequest(gameID, playerName, score);
        BaseDataRequest<SaveGameRequest> request = new BaseDataRequest<SaveGameRequest>(gameSaveGameRequest, "", "", "", "");
        apiService.saveGame(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                   Response<BaseResponse<GameBaseResponseData>> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())){
                        callBack.onSuccess(response.body().getData());
                    } else
                        callBack.onError(response.body().getMessage());
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                callBack.onFailure(t.getMessage());
            }
        });
    }

    public void getGameCategories(final ListenerGameApi callBack){
        callBack.onPreRequest();
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        BaseDataRequest<Object> request = new BaseDataRequest<Object>(null, "", "", "", "");
        apiService.getGameCategories(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                   Response<BaseResponse<GameBaseResponseData>> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())){
                        callBack.onSuccess(response.body().getData());
                    } else
                        callBack.onError(response.body().getMessage());
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                callBack.onFailure(t.getMessage());
            }
        });
    }

    public void getGameTopTen(final ListenerGameApi callBack){
        callBack.onPreRequest();
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        BaseDataRequest<Object> request = new BaseDataRequest<Object>(null, "", "", "", "");
        apiService.getGameTopTen(token, request).enqueue(new Callback<BaseResponse<GameListResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<GameListResponseData>> call,
                                   Response<BaseResponse<GameListResponseData>> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())){
                        callBack.onSuccess(response.body().getData());
                    } else
                        callBack.onError(response.body().getMessage());
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<GameListResponseData>> call, Throwable t) {
                callBack.onFailure(t.getMessage());
            }
        });
    }

    public void callApi(String type, Object wsRequest){
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        listenerGameApi.onPreRequest();
        switch (type){
            case GET_GAMES: {
                GameByNameRequest gameByNameRequest = (GameByNameRequest) wsRequest;
                BaseDataRequest<GameByNameRequest> request = new BaseDataRequest<GameByNameRequest>(gameByNameRequest, "", "", "", "");
                apiService.getGameByName(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                           Response<BaseResponse<GameBaseResponseData>> response) {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())) {
                                listenerGameApi.onSuccess(response.body().getData());
                            } else
                                listenerGameApi.onError(response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                        listenerGameApi.onFailure(t.getMessage());
                    }
                });
            }
            break;
            case GET_BEST_SCORE: {
                BestScoreGameIDRequest bestScoreGameIDRequest = (BestScoreGameIDRequest) wsRequest;
                BaseDataRequest<BestScoreGameIDRequest> request = new BaseDataRequest<BestScoreGameIDRequest>(bestScoreGameIDRequest, "", "", "", "");
                apiService.getBestScoreByGameID(token, request).enqueue(new Callback<BaseResponse<GameModel>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameModel>> call,
                                           Response<BaseResponse<GameModel>> response) {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())){
                                listenerGameApi.onSuccess(response.body().getData());
                            } else
                                listenerGameApi.onError(response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse<GameModel>> call, Throwable t) {
                        listenerGameApi.onFailure(t.getMessage());
                    }
                });
            }
            break;
            case GET_PLAYED_GAMES: {
                GamePlayedRequest gamePlayedRequest = (GamePlayedRequest) wsRequest;
                BaseDataRequest<GamePlayedRequest> request = new BaseDataRequest<GamePlayedRequest>(gamePlayedRequest, "", "", "", "");
                apiService.getPlayedGames(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                           Response<BaseResponse<GameBaseResponseData>> response) {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())){
                                listenerGameApi.onSuccess(response.body().getData());
                            } else
                                listenerGameApi.onError(response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                        listenerGameApi.onFailure(t.getMessage());
                    }
                });
            }
            break;
            case GET_GAMES_PLAY_MOST_TIME: {
                GamePlayMostTimeRequest gamePlayMostTimeRequest = (GamePlayMostTimeRequest) wsRequest;
                BaseDataRequest<GamePlayMostTimeRequest> request = new BaseDataRequest<GamePlayMostTimeRequest>(gamePlayMostTimeRequest, "", "", "", "");
                apiService.getPlayMostTimeGames(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                           Response<BaseResponse<GameBaseResponseData>> response) {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())){
                                listenerGameApi.onSuccess(response.body().getData());
                            } else
                                listenerGameApi.onError(response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                        listenerGameApi.onFailure(t.getMessage());
                    }
                });
            }
            break;
            case GET_BEST_SCORE_GAMES: {
                GamePlayedRequest gamePlayedRequest = (GamePlayedRequest) wsRequest;
                BaseDataRequest<GamePlayedRequest> request = new BaseDataRequest<GamePlayedRequest>(gamePlayedRequest, "", "", "", "");
                apiService.getBestScoreGames(token, request).enqueue(new Callback<BaseResponse<GameListResponseData>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameListResponseData>> call,
                                           Response<BaseResponse<GameListResponseData>> response) {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())){
                                listenerGameApi.onSuccess(response.body().getData());
                            } else
                                listenerGameApi.onError(response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse<GameListResponseData>> call, Throwable t) {
                        listenerGameApi.onFailure(t.getMessage());
                    }
                });
            }
            break;
            case SAVE_GAME: {
                SaveGameRequest gameSaveGameRequest = (SaveGameRequest) wsRequest;
                BaseDataRequest<SaveGameRequest> request = new BaseDataRequest<SaveGameRequest>(gameSaveGameRequest, "", "", "", "");
                apiService.saveGame(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                           Response<BaseResponse<GameBaseResponseData>> response) {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())){
                                listenerGameApi.onSuccess(response.body().getData());
                            } else
                                listenerGameApi.onError(response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                        listenerGameApi.onFailure(t.getMessage());
                    }
                });
            }
            break;
            case GET_GAME_CATEGORIES: {
                BaseDataRequest<Object> request = new BaseDataRequest<Object>(null, "", "", "", "");
                apiService.getGameCategories(token, request).enqueue(new Callback<BaseResponse<GameBaseResponseData>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameBaseResponseData>> call,
                                           Response<BaseResponse<GameBaseResponseData>> response) {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())){
                                listenerGameApi.onSuccess(response.body().getData());
                            } else
                                listenerGameApi.onError(response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse<GameBaseResponseData>> call, Throwable t) {
                        listenerGameApi.onFailure(t.getMessage());
                    }
                });
            }
            break;
            case GET_GAME_TOP_TEN: {
                BaseDataRequest<Object> request = new BaseDataRequest<Object>(null, "", "", "", "");
                apiService.getGameTopTen(token, request).enqueue(new Callback<BaseResponse<GameListResponseData>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameListResponseData>> call,
                                           Response<BaseResponse<GameListResponseData>> response) {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())){
                                listenerGameApi.onSuccess(response.body().getData());
                            } else
                                listenerGameApi.onError(response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<BaseResponse<GameListResponseData>> call, Throwable t) {
                        listenerGameApi.onFailure(t.getMessage());
                    }
                });
            }
            break;
        }
    }

    public interface ListenerGameApi{
        void onPreRequest();
        void onSuccess(Object responseData);
        void onError(String message);
        void onFailure(String exp);
    }
}

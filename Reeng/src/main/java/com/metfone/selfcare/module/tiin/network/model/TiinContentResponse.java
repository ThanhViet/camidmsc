package com.metfone.selfcare.module.tiin.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TiinContentResponse {
    @SerializedName("data")
    @Expose
    private TiinModel data;
    @SerializedName("error")
    @Expose
    private boolean error;

    public TiinModel getData() {
        return data;
    }

    public void setData(TiinModel data) {
        this.data = data;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "TiinContentResponse{" +
                "data=" + data +
                '}';
    }
}

package com.metfone.selfcare.module.home_kh.fragment.reward.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.model.RedeemItem;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class RewardPagerRvAdapter extends RecyclerView.Adapter<RewardPagerRvAdapter.Holder> {

    private static final int TYPE_LOADING = 1000;
    private List<RedeemItem> listData = new ArrayList<>();
    private int pointType;
    private Drawable placeHolder;
    private boolean isLoading;
    private String dateOutputFormat;
    private IActiveClick iActiveClick;

    public RewardPagerRvAdapter(List<RedeemItem> noteList) {
        listData = noteList;
    }

    public RewardPagerRvAdapter(List<RedeemItem> noteList, IActiveClick iActiveClick) {
        listData = noteList;
        this.iActiveClick = iActiveClick;
    }

    public void removeData(){
        listData.clear();
        notifyDataSetChanged();
    }
    public void setLoading(boolean isLoading) {
        if (this.isLoading != isLoading) {
            int position = getItemCount();
            this.isLoading = isLoading;
            if (isLoading) {
                notifyItemInserted(position + 1);
            } else {
                notifyItemRemoved(position);
            }
        }
    }

    public void setType(int pointType) {
        this.pointType = pointType;
        if (PointType.ACTIVE.id == pointType) {
            placeHolder = PointType.ACTIVE.drawable;
            dateOutputFormat = DateConvert.DATE_GIFT_REDEEM_OUTPUT;
        } else if (PointType.PAST.id == pointType) {
            placeHolder = PointType.PAST.drawable;
            dateOutputFormat = DateConvert.DATE_REWARD_OUTPUT;
        }
    }

    public void addData(List<RedeemItem> data) {
        if (data == null || data.isEmpty()) {
            return;
        }
        if (listData == null) {
            listData = new ArrayList<>();
        }
        this.listData.clear();
        listData.addAll(data);
        notifyDataSetChanged();
    }

    public void add(List<RedeemItem> data) {
        if (data == null || data.isEmpty()) {
            return;
        }
        if (listData == null) {
            listData = new ArrayList<>();
        }
        listData.addAll(data);
        notifyItemRangeInserted(getItemCount() - data.size(), data.size());
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewType == TYPE_LOADING ? new LoadingHolder(parent) : new NoteVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rewards_kh, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return isLoading ? listData.size() + 1 : listData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return isLoading && position == listData.size() ? TYPE_LOADING : super.getItemViewType(position);
    }

    abstract static class Holder extends RecyclerView.ViewHolder {

        public Holder(@NonNull View itemView) {
            super(itemView);
        }

        public abstract void onBind(int position);

        protected static View inflate(@NonNull ViewGroup parent, @LayoutRes int layoutRes) {
            return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        }

    }

    class NoteVH extends Holder {
        private AppCompatImageView icon;
        private AppCompatTextView tvDiscount;
        private AppCompatTextView tvTime;
        private AppCompatTextView tvPoint;
        private ConstraintLayout parent;

        public NoteVH(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.reward_point_icon);
            tvDiscount = itemView.findViewById(R.id.discount);
            tvTime = itemView.findViewById(R.id.time);
            tvPoint = itemView.findViewById(R.id.point);
            parent = itemView.findViewById(R.id.parent);
        }

        @Override
        public void onBind(int position) {
            RedeemItem item = listData.get(position);
            Context context = tvDiscount.getContext();
            String date = DateConvert.convert(item.getExpireDate(), DateConvert.DATE_GIFT_INPUT, dateOutputFormat);
            if (pointType == PointType.PAST.id) {
                tvDiscount.setAlpha((float) 0.5);
                tvPoint.setAlpha((float) 0.5);
                tvTime.setAlpha((float) 0.5);
                tvTime.setText(date);
            } else if (pointType == PointType.ACTIVE.id) {
                tvTime.setText(String.format(ResourceUtils.getString(R.string.valid_until), date));
            }
            tvDiscount.setText(String.format(context.getString(R.string.reward_item_discount), item.getDiscountRate()));
            tvPoint.setText(item.getRedeemPoint() > 0 ? String.format(context.getString(R.string.reward_item_point), formatPoint(item.getRedeemPoint())) : ResourceUtils.getString(R.string.free));
            Glide.with(context)
                    .load(item.getIconUrl())
                    .circleCrop()
                    .placeholder(placeHolder)
                    .into(icon);
            parent.setOnClickListener(view -> {
                if (iActiveClick != null) {
                    iActiveClick.clickItem(listData.get(position));
                }
            });
        }
    }

    private String formatPoint(double point) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(point).replaceAll(",", "\\.");
    }

    private static final class LoadingHolder extends Holder {

        public LoadingHolder(@NonNull ViewGroup parent) {
            super(inflate(parent, R.layout.item_loading_kh));
        }

        @Override
        public void onBind(int position) {
            Log.e("Tranhoass", "bind loading");
        }
    }

    public interface IActiveClick {
        void clickItem(RedeemItem item);
    }
}
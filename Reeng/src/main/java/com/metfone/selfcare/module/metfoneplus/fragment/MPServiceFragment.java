package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.module.metfoneplus.adapter.ServiceVPAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.util.FragmentUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MPServiceFragment extends MPBaseFragment implements MPBuyServiceFragment.OnServiceListener {
    public static final String TAG = MPServiceFragment.class.getSimpleName();
    private static final String TAB_POSITION_PARAM = "tab_position_param";
    public static final int TAB_BUY_SERVICE = 0;
    public static final int TAB_MY_SERVICE = 1;

    @BindView(R.id.layout_buy_service)
    LinearLayout mLayoutBuyService;
    @BindView(R.id.frame_layout_buy_service_detail)
    FrameLayout mFrameLayoutBuyServiceDetail;
    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_back)
    RelativeLayout mActionBarBack;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.tab_detail)
    TabLayout mDetailTab;
    @BindView(R.id.view_pager_service)
    ViewPager mViewpagerService;

    private String[] mTitleServiceTabs;
    private ServiceVPAdapter mServiceVPAdapter;
    private List<MPBaseFragment> mMPMetfoneFragments;
    private MPBuyServiceFragment mMPBuyServiceFragment;
    private MPMyServiceFragment mMPMyServiceFragment;
    private boolean mIsMyServiceTabSelected = false;

    public MPServiceFragment() {

    }

    public static MPServiceFragment newInstance(int tabPosition) {
        MPServiceFragment fragment = new MPServiceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TAB_POSITION_PARAM, tabPosition);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_service;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActionBarTitle.setText(getString(R.string.m_p_buy_service_title));
        Utilities.adaptViewForInserts(mLayoutActionBar);
        initTabLayout();
    }

    private void initTabLayout() {
        mTitleServiceTabs = mParentActivity.getResources().getStringArray(R.array.m_p_service_type);

        mMPBuyServiceFragment = MPBuyServiceFragment.newInstance();
        mMPBuyServiceFragment.setOnServiceListener(this);

        mMPMyServiceFragment = MPMyServiceFragment.newInstance();
        mMPMetfoneFragments = new ArrayList<>();
        mMPMetfoneFragments.add(mMPBuyServiceFragment);
        mMPMetfoneFragments.add(mMPMyServiceFragment);

        mServiceVPAdapter = new ServiceVPAdapter(getChildFragmentManager(),
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
                mTitleServiceTabs,
                mMPMetfoneFragments);

        mViewpagerService.setAdapter(mServiceVPAdapter);
        mViewpagerService.setOffscreenPageLimit(2);
        mDetailTab.setupWithViewPager(mViewpagerService);

        if (getArguments() != null) {
            if (getArguments().getInt(TAB_POSITION_PARAM, 0) == TAB_MY_SERVICE) {
                mViewpagerService.setCurrentItem(TAB_MY_SERVICE);
            } else {
                mViewpagerService.setCurrentItem(TAB_BUY_SERVICE);
            }
        }

        mDetailTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText() != null && tab.getText().equals(getString(R.string.m_p_detail_my_services))) {
                    //comment to fix duplicate call API because it was called at onResume() of MPMyServiceFragment
//                    mMPMyServiceFragment.getCurrentUsedServices();
                    mIsMyServiceTabSelected = true;
                    logApp(Constants.LOG_APP.TAB_MY_SERVICE);
                } else {
                    mIsMyServiceTabSelected = false;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabUnselected: ");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabReselected: ");
            }
        });
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        if(mParentActivity instanceof HomeActivity){
            handlePopBackStackFragment();
        }else{
            mParentActivity.finish();
        }
    }

    public void handlePopBackStackFragment() {
        if (mActionBarTitle.getText().equals(getString(R.string.m_p_buy_service_title)) || mIsMyServiceTabSelected) {
            popBackStackFragment();
        } else {
            mMPBuyServiceFragment.handleShowLisCategoryListGroup(true);
        }
    }

    public void handlePopBackStackFromDeepLink(){
        if (mActionBarTitle.getText().equals(getString(R.string.m_p_buy_service_title)) || mIsMyServiceTabSelected) {
            getActivity().finish();
        } else {
            mMPBuyServiceFragment.handleShowLisCategoryListGroup(true);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCategorySelected(int idResName) {
        mActionBarTitle.setText(idResName);
    }

    @Override
    public void onDetailItemSelected(Object object, String serviceGroupId, String serviceCode) {
        if (object instanceof ExchangeItem) {
            MPBuyServiceDetailFragment fragment = MPBuyServiceDetailFragment.newInstance((ExchangeItem) object, serviceGroupId, serviceCode);
            FragmentUtils.replaceFragmentWithAnimationDefault(getParentFragmentManager(), R.id.frame_layout_buy_service_detail, fragment);
        } else if (object instanceof ServiceGroup) {
            MPBuyServiceDetailFragment fragment = MPBuyServiceDetailFragment.newInstance((ServiceGroup) object, serviceGroupId, serviceCode);
            FragmentUtils.replaceFragmentWithAnimationDefault(getParentFragmentManager(), R.id.frame_layout_buy_service_detail, fragment);
        }
    }
}

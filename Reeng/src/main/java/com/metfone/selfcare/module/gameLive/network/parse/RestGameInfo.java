package com.metfone.selfcare.module.gameLive.network.parse;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.gameLive.model.LiveGameModel;

import java.io.Serializable;

public class RestGameInfo extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("numberLucky")
    private String numberLucky;

    @SerializedName("link_game")
    private String link_game;

    @SerializedName("timeserver")
    private long timeserver;

    @SerializedName("result")
    private LiveGameModel result;

    public LiveGameModel getResult() {
        return result;
    }

    public String getNumberLucky() {
        return numberLucky;
    }

    public void setNumberLucky(String numberLucky) {
        this.numberLucky = numberLucky;
    }

    public String getLink_game() {
        return link_game;
    }

    public void setLink_game(String link_game) {
        this.link_game = link_game;
    }

    public long getTimeserver() {
        return timeserver;
    }

    public void setTimeserver(long timeserver) {
        this.timeserver = timeserver;
    }

    @Override
    public String toString() {
        return "RestGameInfo{" +
                "numberLucky='" + numberLucky + '\'' +
                ", timeserver=" + timeserver +
                '}';
    }
}

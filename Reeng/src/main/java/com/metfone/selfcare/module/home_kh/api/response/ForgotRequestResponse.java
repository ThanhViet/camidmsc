package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotRequestResponse {
    @SerializedName("errorCode")
    @Expose
    String code;
    @SerializedName("errorMessage")
    @Expose
    String message;
    @SerializedName("result")
    @Expose
    ForgotRequestResult request;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ForgotRequestResult getRequest() {
        return request;
    }

    public void setRequest(ForgotRequestResult request) {
        this.request = request;
    }
}


package com.metfone.selfcare.module.netnews.request;

/**
 * Created by HaiKE on 8/19/17.
 */

public class VideoRequest {

    int cateId;
    int pid;
    int contentId;
    int num;
    int page;
    long unixTime;

    public VideoRequest(int pid, int cateId, int page, long unixTime) {
        this.cateId = cateId;
        this.pid = pid;
        this.page = page;
        this.unixTime = unixTime;
    }

    public VideoRequest(int pid, int cateId, int page, int num)
    {
        this.page = page;
        this.num = num;
        this.cateId =cateId;
        this.pid = pid;
    }

    public VideoRequest(int contentId, int page)
    {
        this.contentId = contentId;
        this.page = page;
    }

    public VideoRequest(int contentId, int page, long unixTime) {
        this.contentId = contentId;
        this.page = page;
        this.unixTime = unixTime;
    }

    public long getUnixTime() {
        return unixTime;
    }

    public void setUnixTime(long unixTime) {
        this.unixTime = unixTime;
    }

    public VideoRequest(int contentId)
    {
        this.contentId = contentId;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}

package com.metfone.selfcare.module.keeng.holder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class SlidingBannerDetailHolder extends BaseAdapterV2.ViewHolder {

    @BindView(R.id.iv_cover)
    ImageView ivCover;

    @BindView(R.id.icon_play)
    ImageView iconPlay;

    private AllModel model;
    private AbsInterface.OnItemListener listener;

    public SlidingBannerDetailHolder(AbsInterface.OnItemListener listener, LayoutInflater layoutInflater, ViewGroup parent) {
        super(layoutInflater.inflate(R.layout.holder_sliding_banner_detail_music, parent, false));
        this.listener = listener;
    }

    @Override
    public void bindData(ArrayList<Object> items, int position) {
        Object item = items.get(position);
        if (item instanceof AllModel) {
            model = (AllModel) item;
            ImageManager.showImageNormalV2(model.getImage(), ivCover, ApplicationController.self().getRoundedCornersTransformation());
            iconPlay.setVisibility(model.getType() == Constants.TYPE_VIDEO ? View.VISIBLE : View.GONE);
        }
    }

    @OnClick(R.id.root_item_sliding_banner_music)
    public void onViewClicked() {
        if (model != null && listener != null) {
            model.setSource(MediaLogModel.SRC_FLASH_HOT);
            listener.onItemClick(model);
        }
    }
}

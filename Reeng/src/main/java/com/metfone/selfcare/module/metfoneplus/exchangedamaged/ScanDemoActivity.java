package com.metfone.selfcare.module.metfoneplus.exchangedamaged;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ScanDemoActivity extends BaseSlidingFragmentActivity {
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.action_bar_option)
    RelativeLayout action_bar_option;
    @BindView(R.id.action_bar_back)
    RelativeLayout action_bar_back;
    @BindView(R.id.action_bar_option_img)
    ImageView action_bar_option_img;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiti_exchange_damaged_more);
        ButterKnife.bind(this);
        action_bar_back.setVisibility(View.INVISIBLE);
        action_bar_option_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_white_24dp));

        action_bar_title.setText(getString(R.string.exchange_damaged_title_serial));
        action_bar_option.setVisibility(View.VISIBLE);
    }
    @OnClick(R.id.action_bar_option)
    public void onBack(){
        onBackPressed();
    }
    @Override
    public void onResume() {
        super.onResume();
    }


}
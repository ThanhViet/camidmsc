package com.metfone.selfcare.module.home_kh.fragment.confirm;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;

public class DialogConfirmQrCodeKH extends DialogFragment {

    private static final String BASE64 = "BASE64";
    private static final String CODE = "CODE";
    private AppCompatImageView qrCode;
    private AppCompatTextView back;
    private AppCompatTextView content;
    private FrameLayout container;

    public static DialogConfirmQrCodeKH newInstance(String base64, String code) {
        DialogConfirmQrCodeKH dialog = new DialogConfirmQrCodeKH();
        Bundle args = new Bundle();
        args.putString(BASE64, base64);
        args.putString(CODE, code);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            if (dialog.getWindow() != null) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(true);
        }
        return inflater.inflate(R.layout.fragment_dialog_confirm_qrcode, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        qrCode = view.findViewById(R.id.qrcode);
        back = view.findViewById(R.id.back);
        content = view.findViewById(R.id.content);
        container = view.findViewById(R.id.container);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Bitmap bitmap = convertBitmap(bundle.getString(BASE64, ""));
            content.setText(bundle.getString(CODE, ""));
            if (bitmap != null) {
                qrCode.setImageBitmap(bitmap);
            }
        }
        back.setOnClickListener(view12 -> {
            dismiss();
        });
        container.setOnClickListener(view12 -> {
            dismiss();
        });
    }

    private Bitmap convertBitmap(String base64) {
        if (TextUtils.isEmpty(base64)) {
            return null;
        }
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
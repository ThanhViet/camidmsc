package com.metfone.selfcare.module.metfoneplus.exchangedamaged.fragment;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.QRCodeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.model.camid.ChangeCardDamaged;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.exchangedamaged.ScanBarcodeActivity;
import com.metfone.selfcare.module.metfoneplus.exchangedamaged.ScanDemoActivity;
import com.metfone.selfcare.module.metfoneplus.exchangedamaged.adapter.ExchangeDamagedAdapter;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddFeedbackFragment;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsChangeCardListResponse;
import com.metfone.selfcare.ui.view.CamIdEditText;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.metfone.selfcare.helper.Constants.PERMISSION.PERMISSION_REQUEST_LOCATION;
import static com.metfone.selfcare.helper.Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_AVATAR_FILE_CROP;

public class AddExchangeDamagedFragment extends MPBaseFragment implements  View.OnClickListener {
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.tvMore)
    TextView tvMore;
    @BindView(R.id.action_bar_option)
    RelativeLayout action_bar_option;
    @BindView(R.id.action_bar_option_img)
    ImageView action_bar_option_img;
    @BindView(R.id.ivAfterCapture)
    ImageView ivAfterCapture;
    @BindView(R.id.ivAfterVideo)
    ImageView ivAfterVideo;
    @BindView(R.id.tvSerial)
    EditText tvSerial;
    @BindView(R.id.tvPhoneNumber)
    EditText tvPhoneNumber;
    File image;
    File videoUri;
    ImageInfo videoInfo ;
    String video;
    private int ACTION_SCAN_QR = 1992;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_add_exchange_damaged;
    }

    public static AddExchangeDamagedFragment newInstance() {
        Bundle args = new Bundle();
        AddExchangeDamagedFragment fragment = new AddExchangeDamagedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(view);
        initViews();
    }
    @Override
    public void onResume() {
        super.onResume();
    }


    private void initViews() {
        action_bar_title.setText(getString(R.string.exchange_damaged_title_add_card));
        tvMore.setPaintFlags(tvMore.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
    }
    @OnClick(R.id.action_bar_back)
    public void onBack() {
        if(mParentActivity instanceof HomeActivity){
            popBackStackFragment();
        }else{
            mParentActivity.finish();
        }
    }

    @Override
    public void popBackStackFragment() {
        super.popBackStackFragment();
        updateBottomMenuColor(R.color.m_home_tab_background);
    }


    @OnClick({R.id.rlCamera,R.id.rlVideo,R.id.ivQRCode,R.id.btn_next,R.id.tvMore})
    public void handleClick(View view) {
        if (view.getId() == R.id.rlCamera) {
            takeAPhoto();
        } else if (view.getId() == R.id.rlVideo) {
            takeAVideo();
        }
//        else if (view.getId() == R.id.ivAfterVideo){
//            File file = new File(video);
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            Uri photoURI = Uri.parse("https://www.rmp-streaming.com/media/big-buck-bunny-360p.mp4");
//            intent.setDataAndType(photoURI, "video/mp4");
//            startActivity(intent);
//        }else if (view.getId() == R.id.ivAfterCapture){
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(Uri.fromFile(image), "video/*");
//            startActivity(intent);
//        }
        else if (view.getId() == R.id.ivQRCode) {
            if (DeviceHelper.isBackCameraAvailable(mParentActivity)) {
                Intent qrCode = new Intent(mParentActivity, ScanBarcodeActivity.class);
                qrCode.putExtra("KEY_FROM_SOURCE", Constants.QR_SOURCE.SCAN_SERIAL);
                startActivityForResult(qrCode,ACTION_SCAN_QR);
            } else {
                mParentActivity.showToast(R.string.qr_err_camera_not_found);
            }
        }
        else if (view.getId() == R.id.btn_next) {
            if (tvPhoneNumber.getText().toString().isEmpty()){
                showToast(getString(R.string.exchange_damaged_error_phone));
                return;
            }
            if (tvSerial.getText().toString().isEmpty()){
                showToast(getString(R.string.exchange_damaged_error_serial));
                return;
            }
            if (image == null){
                showToast(getString(R.string.exchange_damaged_error_image));
                return;
            }
            if (videoInfo == null){
                showToast(getString(R.string.exchange_damaged_error_video));
                return;
            }
            if (videoInfo.getDurationInSecond()  > 5){
                showToast(getString(R.string.exchange_damaged_error_video_duration));
                return;
            }
           mParentActivity.showLoadingDialog("", mRes.getString(R.string.processing), false);
           String serial = tvSerial.getText().toString().trim();
           String phoneNumber = tvPhoneNumber.getText().toString().trim();
           String imageData = convertImageToBase64();
           String videoData = convertVideoToBase64();
           addExchangeDamagedList(serial,phoneNumber,imageData,videoData);

        }
        else if (view.getId() == R.id.tvMore) {
           startActivity(new Intent(mParentActivity, ScanDemoActivity.class));
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.ACTION.ACTION_TAKE_PHOTO) {
                Glide.with(ivAfterCapture)
                        .load(image.getAbsoluteFile())
                        .into(ivAfterCapture);
                ivAfterCapture.setVisibility(View.VISIBLE);
            } else if (requestCode == Constants.ACTION.ACTION_TAKE_VIDEO){
                video = FileHelper.getRealPathVideoFromURI(mParentActivity, data.getData());
                Log.i(TAG, "ACTION_TAKE_VIDEO OK " + video);
                if (video != null && !TextUtils.isEmpty(video)) {
                    FileHelper.refreshGallery(mApplication, video);
                    videoInfo = FileHelper.getMediaInfo(mApplication, video);
                    Bitmap bMap = ThumbnailUtils.createVideoThumbnail(video, MediaStore.Video.Thumbnails.MICRO_KIND);
                    ivAfterVideo.setImageBitmap(bMap);
                    ivAfterVideo.setVisibility(View.VISIBLE);
                } else {
                    showToast(R.string.prepare_photo_fail);
                }
            } else if (requestCode == ACTION_SCAN_QR && data != null){
                 String serial = data.getStringExtra("QR_CODE");
                 tvSerial.setText(serial);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_TAKE_PHOTO) {
            if (PermissionHelper.allowedPermission(mParentActivity.getApplicationContext(), Manifest.permission.CAMERA)) {
                takeAPhoto();
            }
        }
    }

    public void takeAPhoto() {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(mParentActivity,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takePhotoA();
        }
    }

    private void takePhotoA() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File takePhotoFile = createImageFile();
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApplication, takePhotoFile));
            startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
        }
    }

    public void takeAVideo() {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(mParentActivity,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takeVideoA();
        }
    }

    private void takeVideoA() {
        try {
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            startActivityForResult(takeVideoIntent, Constants.ACTION.ACTION_TAKE_VIDEO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
        }
    }

    public void showToast(final int stringResourceId) {
            com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), getResources().getString(stringResourceId));

    }

    public void showToast(final String stringResourceId) {
        com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(),stringResourceId);

    }
    private File createImageFile() {
        String time = String.valueOf(System.currentTimeMillis());
        File storageDir = mParentActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
         try {
             image = File.createTempFile(
                    time,  /* prefix */
                    ".jpg",  /* suffix */
                    storageDir /* directory */
            );
            return image;
        } catch (IOException e) {
            return null;
        }
    }

    private void addExchangeDamagedList(String serial,String phoneNumber,String dataImage,String video) {
        new MetfonePlusClient().wsAddChangeCard(serial,phoneNumber,dataImage,video,new MPApiCallback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response) {
                mParentActivity.hideLoadingDialog();
                if (response != null && response.body() != null && response.body().getResult() != null && response.body().getResult().getMessage() != null) {
                    if (response.body().getResult().getMessage().equals("Success")){
                        onBack();
                    } else
                        showToast(response.body().getResult().getMessage());
                } else {
                    showToast(mParentActivity.getString(R.string.service_error));
                }

            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
                showToast(mParentActivity.getString(R.string.service_error));
            }
        });
    }

    private String convertImageToBase64(){
        Bitmap bm = BitmapFactory.decodeFile(image.getAbsolutePath());
        float aspectRatio = bm.getWidth() /
                (float) bm.getHeight();
        Bitmap resized = Bitmap.createScaledBitmap(bm, Math.round(480 * aspectRatio), 480, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.JPEG, 100, baos); // bm is the bitmap object
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    private String convertVideoToBase64(){
        InputStream inputStream = null;
        try
        {
            Uri uri = Uri.fromFile(new File(video));
            inputStream = mParentActivity.getContentResolver().openInputStream(uri);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int len = 0;
        try
        {
            while ((len = inputStream.read(buffer)) != -1)
            {
                byteBuffer.write(buffer, 0, len);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        String videoData="";
        videoData = Base64.encodeToString(byteBuffer.toByteArray(), Base64.DEFAULT);
        return videoData;
    }
}

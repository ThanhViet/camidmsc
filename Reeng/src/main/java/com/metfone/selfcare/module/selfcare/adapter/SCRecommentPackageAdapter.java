package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.holder.SCRecommentPackageHolder;
import com.metfone.selfcare.module.selfcare.model.SCRecommentPackage;

import java.util.ArrayList;

public class SCRecommentPackageAdapter extends BaseAdapter<BaseViewHolder> {

    private ArrayList<SCRecommentPackage> data = new ArrayList<>();
    private AbsInterface.OnPackageHeaderListener listener;

    public SCRecommentPackageAdapter(Context context, AbsInterface.OnPackageHeaderListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(ArrayList<SCRecommentPackage> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_my_package_title, null);
        return new SCRecommentPackageHolder(mContext, view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        SCRecommentPackageHolder viewHolder = (SCRecommentPackageHolder) holder;
        viewHolder.setData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }
}

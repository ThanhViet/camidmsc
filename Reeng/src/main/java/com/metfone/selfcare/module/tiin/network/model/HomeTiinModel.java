package com.metfone.selfcare.module.tiin.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.NewsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HomeTiinModel implements Serializable {
    @SerializedName("Type")
    @Expose
    private String type ="";
    @SerializedName("Header")
    @Expose
    private String header ="";
    @SerializedName("Position")
    @Expose
    private int position=-1;
    @SerializedName("data")
    @Expose
    private List<TiinModel> data = new ArrayList<>();
    @SerializedName("CategoryID")
    @Expose
    private int categoryID = -1;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName = "";
    @SerializedName("TypeDisplay")
    @Expose
    private int typeDisplay = -1;

    public HomeTiinModel() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<TiinModel> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }
    public List<TiinModel> getDataOtherOne(){
        List<TiinModel> listThree = new ArrayList<>();
        if(data == null){
            data = new ArrayList<>();
        }
        if(data.size() > 1) {
            for (int i = 1; i < data.size(); i++) {
                listThree.add(data.get(i));
            }
        }
        return listThree;
    }

    public void setData(List<TiinModel> data) {
        this.data = data;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(int typeDisplay) {
        this.typeDisplay = typeDisplay;
    }
    @Override
    public String toString() {
        return "HomeTiinModel{" +
                "header='" + header + '\'' +
                ", position=" + position +
                ", data=" + data +
                ", categoryID=" + categoryID +
                ", categoryName=" + categoryName +
                ", typeDisplay=" + typeDisplay +
                '}';
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/25
 */

package com.metfone.selfcare.module.security.helper;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.security.event.SecurityDetailEvent;
import com.metfone.selfcare.module.security.model.LogModel;
import com.metfone.selfcare.module.security.model.NewsModel;
import com.metfone.selfcare.module.security.model.SecurityModel;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SecurityApi extends BaseApi {

    private static final String TAG = "SecurityApi";

    private static SecurityApi mInstance = new SecurityApi();
    private String domain;

    private SecurityApi() {
        super(ApplicationController.self());
        domain = getDomainFile();
    }

    public static SecurityApi getInstance() {
        if (mInstance == null)
            mInstance = new SecurityApi();
        mInstance.domain = mInstance.getDomainFile();
        return mInstance;
    }

    public void getConfig(@Nullable final ApiCallbackV2<SecurityModel> apiCallback) {
        //sms_firewall_mode : =1 là bật tường lửu tin nhắn, =0 là tắt tường lửa tin nhắn
        String msisdn = getReengAccountBusiness().getJidNumber();
        String accountToken = getReengAccountBusiness().getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);
        get(domain, Url.File.API_SECURITY_GET_CONFIG)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter(Constants.HTTP.REST_LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage())
                .putParameter(Constants.HTTP.REST_COUNTRY_CODE, getReengAccountBusiness().getRegionCode())
                .putParameter("timestamp", String.valueOf(currentTime))
                .putParameter("security", dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("code")) {
                            String code = jsonObject.optString("code");
                            if ("200".equals(code)) {
                                SecurityModel data = gson.fromJson(new JSONObject(response).optString("data")
                                        , new TypeToken<SecurityModel>() {
                                        }.getType());
                                if (data != null) {
                                    SharedPref.newInstance(application).putString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, data.getSmsFirewallMode());
                                    SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_WHITE_LIST, data.getSmsFirewallWhitelist());
                                    SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_WHITE_LIST, data.getSmsSpamWhitelist());
                                    SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_BLACK_LIST, data.getSmsSpamBlacklist());
                                } else {
                                    SharedPref.newInstance(application).putString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, "");
                                    SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_WHITE_LIST, null);
                                    SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_WHITE_LIST, null);
                                    SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_BLACK_LIST, null);
                                }
                                //todo post event update firewall mode
                                SecurityDetailEvent event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_FIREWALL_SETTING);
                                EventBus.getDefault().post(event);
                                //todo post event update firewall white list
                                event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_FIREWALL_IGNORE);
                                EventBus.getDefault().post(event);
                                //todo post event update spam black list
                                event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_SPAM_BLOCK);
                                EventBus.getDefault().post(event);
                                //todo post event update spam white list
                                event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_SPAM_IGNORE);
                                EventBus.getDefault().post(event);
                                if (apiCallback != null) apiCallback.onSuccess("", data);
                                SharedPref.newInstance(application).putLong(Constants.PREFERENCE.PREF_LAST_TIME_GET_SECURITY_DATA, System.currentTimeMillis());
                                return;
                            }
                        }
                        if (apiCallback != null) apiCallback.onSuccess("", null);
//                        if (apiCallback != null)
//                            apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    }

                    @Override
                    public void onFailure(String message) {
                        if (!NetworkHelper.isConnectInternet(application)) {
                            message = application.getResources().getString(R.string.error_internet_disconnect);
                        }
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        if (apiCallback != null) apiCallback.onComplete();
                    }

                })
                .setTag("TAG_SECURITY_GET_CONFIG")
                .execute();
    }

    public void getLog(@Nullable final ApiCallbackV2<ArrayList<LogModel>> apiCallback) {
        /*
        chú ý: log này chỉ trả về 1 lần , sau đó trên server sẽ xóa đi
        client sau khi get về thì append vào gian hiện tại
        * */
        String msisdn = getReengAccountBusiness().getJidNumber();
        String accountToken = getReengAccountBusiness().getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);
        get(domain, Url.File.API_SECURITY_GET_LOG)
                .putParameter("msisdn", msisdn)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter(Constants.HTTP.REST_LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage())
                .putParameter(Constants.HTTP.REST_COUNTRY_CODE, getReengAccountBusiness().getRegionCode())
                .putParameter("timestamp", String.valueOf(currentTime))
                .putParameter("security", dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("code")) {
                            String code = jsonObject.optString("code");
                            if ("200".equals(code)) {
                                Type type = new TypeToken<ArrayList<LogModel>>() {
                                }.getType();
                                ArrayList<LogModel> data = gson.fromJson(new JSONObject(response).optString("data"), type);
                                if (data != null) {
                                    ArrayList<LogModel> spamSmsList = null;
                                    try {
                                        String spamSmsTmp = SharedPref.newInstance(application).getString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, "");
                                        if (!TextUtils.isEmpty(spamSmsTmp)) {
                                            spamSmsList = gson.fromJson(spamSmsTmp, type);
                                        }
                                    } catch (Exception e) {
                                    }
                                    if (spamSmsList == null)
                                        spamSmsList = new ArrayList<>();

                                    ArrayList<LogModel> firewallSmsList = null;
                                    try {
                                        String firewallSmsTmp = SharedPref.newInstance(application).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, "");
                                        if (!TextUtils.isEmpty(firewallSmsTmp)) {
                                            firewallSmsList = gson.fromJson(firewallSmsTmp, type);
                                        }
                                    } catch (Exception e) {
                                    }
                                    if (firewallSmsList == null)
                                        firewallSmsList = new ArrayList<>();

                                    for (int i = 0; i < data.size(); i++) {
                                        if (data.get(i) != null) {
                                            if (data.get(i).isLogSpam()) {
                                                data.get(i).setReaded(false);
                                                spamSmsList.add(data.get(i));
                                            } else if (data.get(i).isLogFirewall()) {
                                                data.get(i).setReaded(false);
                                                firewallSmsList.add(data.get(i));
                                            }
                                        }
                                    }
                                    SharedPref.newInstance(application).putString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, gson.toJson(spamSmsList));
                                    SharedPref.newInstance(application).putString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, gson.toJson(firewallSmsList));
                                }
                                //todo post event update firewall sms
                                SecurityDetailEvent event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_FIREWALL_HISTORY);
                                EventBus.getDefault().post(event);
                                //todo post event update spam sms
                                event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_SPAM_SMS);
                                EventBus.getDefault().post(event);
                                if (apiCallback != null) apiCallback.onSuccess("", data);
                                return;
                            }
                        }
//                        if (apiCallback != null) apiCallback.onSuccess("", data);
                        if (apiCallback != null)
                            apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    }

                    @Override
                    public void onFailure(String message) {
                        if (!NetworkHelper.isConnectInternet(application)) {
                            message = application.getResources().getString(R.string.error_internet_disconnect);
                        }
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        if (apiCallback != null) apiCallback.onComplete();
                    }

                })
                .setTag("TAG_SECURITY_GET_LOG")
                .execute();
    }

    public void setupFirewallSms(String mode, @Nullable final ApiCallbackV2<String> apiCallback) {
        String msisdn = getReengAccountBusiness().getJidNumber();
        String accountToken = getReengAccountBusiness().getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(mode);
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);
        post(domain, Url.File.API_SECURITY_FIREWALL)
                .putParameter("msisdn", msisdn)
                .putParameter("mode", mode)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter(Constants.HTTP.REST_LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage())
                .putParameter(Constants.HTTP.REST_COUNTRY_CODE, getReengAccountBusiness().getRegionCode())
                .putParameter("timestamp", String.valueOf(currentTime))
                .putParameter("security", dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("code")) {
                            String code = jsonObject.optString("code");
                            if ("200".equals(code)) {
                                if (apiCallback != null) apiCallback.onSuccess("", response);
                                return;
                            }
                        }
                        if (apiCallback != null)
                            apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    }

                    @Override
                    public void onFailure(String message) {
                        if (!NetworkHelper.isConnectInternet(application)) {
                            message = application.getResources().getString(R.string.error_internet_disconnect);
                        }
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        if (apiCallback != null) apiCallback.onComplete();
                    }

                })
                .setTag("TAG_SECURITY_SET_FIREWALL_SMS")
                .execute();
    }

    public void setupFirewallSmsWhitelist(String mode, final ArrayList<String> list, @Nullable final ApiCallbackV2<String> apiCallback) {
        String msisdn = getReengAccountBusiness().getJidNumber();
        String accountToken = getReengAccountBusiness().getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(list);
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);
        post(domain, Url.File.API_SECURITY_FIREWALL_WHITELIST)
                .putParameter("msisdn", msisdn)
                .putParameter("whitelist", String.valueOf(list))
                .putParameter("mode", mode)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter(Constants.HTTP.REST_LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage())
                .putParameter(Constants.HTTP.REST_COUNTRY_CODE, getReengAccountBusiness().getRegionCode())
                .putParameter("timestamp", String.valueOf(currentTime))
                .putParameter("security", dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("code")) {
                            String code = jsonObject.optString("code");
                            if ("200".equals(code)) {
                                SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_WHITE_LIST, list);
                                //todo post event update firewall white list
                                SecurityDetailEvent event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_FIREWALL_IGNORE);
                                EventBus.getDefault().post(event);
                                if (apiCallback != null) apiCallback.onSuccess("", response);
                                return;
                            }
                        }
                        if (apiCallback != null)
                            apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    }

                    @Override
                    public void onFailure(String message) {
                        if (!NetworkHelper.isConnectInternet(application)) {
                            message = application.getResources().getString(R.string.error_internet_disconnect);
                        }
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        if (apiCallback != null) apiCallback.onComplete();
                    }

                })
                .setTag("TAG_SECURITY_SET_FIREWALL_WHITE_LIST")
                .execute();
    }

    public void setupSpamSmsWhitelist(String mode, final ArrayList<String> list, @Nullable final ApiCallbackV2<String> apiCallback) {
        String msisdn = getReengAccountBusiness().getJidNumber();
        String accountToken = getReengAccountBusiness().getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(list);
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);
        post(domain, Url.File.API_SECURITY_SPAM_WHITELIST)
                .putParameter("msisdn", msisdn)
                .putParameter("whitelist", String.valueOf(list))
                .putParameter("mode", mode)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter(Constants.HTTP.REST_LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage())
                .putParameter(Constants.HTTP.REST_COUNTRY_CODE, getReengAccountBusiness().getRegionCode())
                .putParameter("timestamp", String.valueOf(currentTime))
                .putParameter("security", dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("code")) {
                            String code = jsonObject.optString("code");
                            if ("200".equals(code)) {
                                SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_WHITE_LIST, list);
                                //todo post event update spam white list
                                SecurityDetailEvent event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_SPAM_IGNORE);
                                EventBus.getDefault().post(event);
                                if (apiCallback != null) apiCallback.onSuccess("", response);
                                return;
                            }
                        }
                        if (apiCallback != null)
                            apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    }

                    @Override
                    public void onFailure(String message) {
                        if (!NetworkHelper.isConnectInternet(application)) {
                            message = application.getResources().getString(R.string.error_internet_disconnect);
                        }
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        if (apiCallback != null) apiCallback.onComplete();
                    }

                })
                .setTag("TAG_SECURITY_SET_SPAM_WHITE_LIST")
                .execute();
    }

    public void setupSpamSmsBlacklist(String mode, final ArrayList<String> list, @Nullable final ApiCallbackV2<String> apiCallback) {
        String msisdn = getReengAccountBusiness().getJidNumber();
        String accountToken = getReengAccountBusiness().getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(list);
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);
        post(domain, Url.File.API_SECURITY_SPAM_BLACKLIST)
                .putParameter("msisdn", msisdn)
                .putParameter("blacklist", String.valueOf(list))
                .putParameter("mode", mode)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter(Constants.HTTP.REST_LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage())
                .putParameter(Constants.HTTP.REST_COUNTRY_CODE, getReengAccountBusiness().getRegionCode())
                .putParameter("timestamp", String.valueOf(currentTime))
                .putParameter("security", dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("code")) {
                            String code = jsonObject.optString("code");
                            if ("200".equals(code)) {
                                SharedPref.newInstance(application).putListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_BLACK_LIST, list);
                                //todo post event update spam black list
                                SecurityDetailEvent event = new SecurityDetailEvent();
                                event.setReloadData(true);
                                event.setTabId(Constants.TAB_SECURITY_SPAM_BLOCK);
                                EventBus.getDefault().post(event);
                                if (apiCallback != null) apiCallback.onSuccess("", response);
                                return;
                            }
                        }
                        if (apiCallback != null)
                            apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    }

                    @Override
                    public void onFailure(String message) {
                        if (!NetworkHelper.isConnectInternet(application)) {
                            message = application.getResources().getString(R.string.error_internet_disconnect);
                        }
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        if (apiCallback != null) apiCallback.onComplete();
                    }

                })
                .setTag("TAG_SECURITY_SET_SPAM_BLACK_LIST")
                .execute();
    }

    public void getNews(@Nullable final ApiCallbackV2<ArrayList<NewsModel>> apiCallback) {
        String msisdn = getReengAccountBusiness().getJidNumber();
        String accountToken = getReengAccountBusiness().getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);
        get(domain, Url.File.API_SECURITY_GET_NEWS)
                .putParameter("msisdn", msisdn)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter(Constants.HTTP.REST_LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage())
                .putParameter(Constants.HTTP.REST_COUNTRY_CODE, getReengAccountBusiness().getRegionCode())
                .putParameter("timestamp", String.valueOf(currentTime))
                .putParameter("security", dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        ArrayList<NewsModel> data = gson.fromJson(new JSONObject(response).optString("lst_news")
                                , new TypeToken<ArrayList<NewsModel>>() {
                                }.getType());
                        if (apiCallback != null) apiCallback.onSuccess("", data);
                        SharedPref.newInstance(application).putString(Constants.PREFERENCE.PREF_SECURITY_NEWS_LIST, gson.toJson(data));
                    }

                    @Override
                    public void onFailure(String message) {
                        if (!NetworkHelper.isConnectInternet(application)) {
                            message = application.getResources().getString(R.string.error_internet_disconnect);
                        }
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        if (apiCallback != null) apiCallback.onComplete();
                    }

                })
                .setTag("TAG_SECURITY_GET_NEWS")
                .execute();
    }

}

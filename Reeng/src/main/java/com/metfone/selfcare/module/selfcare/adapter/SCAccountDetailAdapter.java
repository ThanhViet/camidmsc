package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.holder.SCAccountInfoHolder;
import com.metfone.selfcare.module.selfcare.holder.SCAccountServiceHolder;
import com.metfone.selfcare.module.selfcare.model.SCAccountDetail;

public class SCAccountDetailAdapter extends BaseAdapter<BaseViewHolder> {
    public final static int TYPE_INFO = 0;
    public final static int TYPE_PACKAGE = 1;
    public final static int TYPE_SERVICE = 2;
    public final static int TYPE_EMPTY = 3;

    private SCAccountDetail data;
    private AbsInterface.OnPackageListener listener;

    public SCAccountDetailAdapter(Context context, AbsInterface.OnPackageListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(SCAccountDetail data) {
        this.data = data;
    }

    @Override
    public int getItemViewType(int position) {
//        if(data.getAccountDataInfo().size() == 0 && data.getSevicesList().size() == 0)
//            return TYPE_EMPTY;
//        else if(data.getAccountDataInfo().size() > 0 && data.getSevicesList().size() == 0)
//            return TYPE_INFO;
//        else if(data.getAccountDataInfo().size() == 0 && data.getSevicesList().size() >= 0)
//            return TYPE_SERVICE;
//        else if(data.getAccountDataInfo().size() > 0 && data.getSevicesList().size() >= 0)
//        {
//            if(position < data.getAccountDataInfo().size())
//                return TYPE_INFO;
//            else
//                return TYPE_SERVICE;
//        }
//        return TYPE_EMPTY;

        if(data.getAccountDataInfo().size() == 0)
        {
            if(position == 0)
            {
                if(data.getSevicesList().size() > 0)
                    return TYPE_SERVICE;
                else
                    return TYPE_PACKAGE;
            }
            else if(position > 0)
            {
                return TYPE_PACKAGE;
            }
        }
        else
        {
            if(position < data.getAccountDataInfo().size())
            {
                return TYPE_INFO;
            }
            else
            {
                if(position == data.getAccountDataInfo().size())
                {
                    if(data.getSevicesList().size() > 0)
                        return TYPE_SERVICE;
                    else
                        return TYPE_PACKAGE;
                }
                else if(position > data.getAccountDataInfo().size())
                {
                    return TYPE_PACKAGE;
                }
            }
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(viewType == TYPE_INFO)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_account_detail_info, null);
            return new SCAccountInfoHolder(mContext, view);
        }
        else if(viewType == TYPE_SERVICE)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_account_detail_sevices, null);
            return new SCAccountServiceHolder(mContext, view, listener, TYPE_SERVICE);
        }
        else if(viewType == TYPE_PACKAGE)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_account_detail_sevices, null);
            return new SCAccountServiceHolder(mContext, view, listener, TYPE_PACKAGE);
        }
        else
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, null);
            return new BaseViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        if(holder instanceof SCAccountInfoHolder)
        {
            SCAccountInfoHolder viewHolder = (SCAccountInfoHolder) holder;
            viewHolder.setData(data.getAccountDataInfo().get(position));
        }
        else if(holder instanceof SCAccountServiceHolder)
        {
            SCAccountServiceHolder viewHolder = (SCAccountServiceHolder) holder;
            int viewType = getItemViewType(position);
            if(viewType == TYPE_PACKAGE)
                viewHolder.setData(data.getPackageList());
            else
                viewHolder.setData(data.getSevicesList());
        }
    }

    @Override
    public int getItemCount() {
        if(data == null) return 0;
//        if(data.getSevicesList().size() > 0)
//        {
//            return data.getAccountDataInfo().size() + 1;
//        }
//        return data.getAccountDataInfo().size();

        return data.getAccountDataInfo().size()
                + (data.getPackageList().size() > 0 ? 1 : 0)
                + (data.getSevicesList().size() > 0 ? 1 : 0);
    }
}

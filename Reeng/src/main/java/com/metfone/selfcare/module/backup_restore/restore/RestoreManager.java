package com.metfone.selfcare.module.backup_restore.restore;

import com.metfone.selfcare.common.api.FileApi;
import com.metfone.selfcare.common.api.FileApiImpl;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Config;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class RestoreManager {

    private static boolean isRestore = false;

    public static boolean isRestore() {
        return isRestore;
    }

    public static boolean isRunning() {
        return DBImporter.getInstance().isRunning();
    }

    public static void setRestoring(boolean restore) {
        isRestore = restore;
    }

    public static void restoreMessages(DBImporter.RestoreProgressListener listener, String urlPath) {
        if (!Config.Features.FLAG_SUPPORT_BACKUP_MESSAGES) return;
        DBImporter.getInstance().startRestore(listener, urlPath);
    }

    public static void cancelRestore() {
        DBImporter.getInstance().cancelRestore();
    }

    public static void getBackupFileInfo(HttpCallBack callBack) {
        FileApi fileApi = new FileApiImpl();
        fileApi.getBackupInfo(callBack);
    }

    public static void deleteBackupFileOnServer(String fileId) {
        FileApi fileApi = new FileApiImpl();
        fileApi.deleteBackupFile(fileId);
    }

    public static long getAvailableInternalMemorySize() {
        return DBImporter.getAvailableInternalMemorySize();
    }
}

package com.metfone.selfcare.module.metfoneplus.topup.model.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class SearchFTTHRequest extends BaseRequest<SearchFTTHRequest.Request> {
    public class Request {
        @SerializedName("requestId")
        public String requestId;

        @SerializedName("isdn")
        public String isdn;

        @SerializedName("language")
        private String language;

        public String getRequestId() {
            return requestId;
        }

        public void setRequestId(String requestId) {
            this.requestId = requestId;
        }

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        @Override
        public String toString() {
            return "searchFTTHRequest{" +
                    ", language='" + language + '\'' +
                    ", requestId='" + requestId + '\'' +
                    '}';
        }
    }
}

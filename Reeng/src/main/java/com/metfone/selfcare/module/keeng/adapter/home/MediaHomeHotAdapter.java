package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;

import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.holder.MediaHolder;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

/**
 * Created by namnh40 on 2/21/2017.
 */

public class MediaHomeHotAdapter extends BaseAdapterRecyclerView {
    protected List<AllModel> datas;

    public MediaHomeHotAdapter(Context context, List<AllModel> datas, String ga_source) {
        super(context, ga_source);
        this.datas = datas;
    }

    @Override
    public int getItemCount() {
        if (datas != null && datas.size() > 0)
            return datas.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        AllModel item = getItem(position);
        if (item == null) {
            return ITEM_EMPTY;
        }
        switch (item.getType()) {
            case Constants.TYPE_SONG:
                return ITEM_MEDIA_SONG_HOME_HOT;
            case Constants.TYPE_ALBUM:
                return ITEM_MEDIA_ALBUM_HOME_HOT;
            case Constants.TYPE_VIDEO:
                return ITEM_MEDIA_VIDEO_HOME_HOT;
//            default:
//                return ITEM_MEDIA_ALBUM_HOME;
        }
        return ITEM_EMPTY;
    }

    @Override
    public AllModel getItem(int position) {
        if (datas != null)
            try {
                return datas.get(position);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (holder instanceof MediaHolder) {
            MediaHolder itemHolder = (MediaHolder) holder;
            itemHolder.bind(mContext, getItem(position));
        }
    }
}

package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceGroupViewHolder;
import com.metfone.selfcare.module.metfoneplus.listener.ServiceListener;

import java.util.List;

public class BuyServiceGroupAdapter extends RecyclerView.Adapter<BuyServiceGroupViewHolder> {
    private Context mContext;
    private List<ExchangeItem> mExchangeList;
    private List<ServiceGroup> mServiceGroupList;
    private ServiceListener serviceListener;

    public BuyServiceGroupAdapter(Context context,
                                  List<ExchangeItem> exchangeList, List<ServiceGroup> serviceGroupList,
                                  ServiceListener serviceListener) {
        this.mContext = context;
        this.mExchangeList = exchangeList;
        this.mServiceGroupList = serviceGroupList;
        this.serviceListener = serviceListener;
    }

    public void replaceExchangeData(List<ExchangeItem> exchangeList) {
        this.mExchangeList = exchangeList;
        this.mServiceGroupList = null;
        notifyDataSetChanged();
    }

    public void replaceServiceGroupData(List<ServiceGroup> serviceGroupList) {
        this.mServiceGroupList = serviceGroupList;
        this.mExchangeList = null;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BuyServiceGroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mp_item_buy_services_group, parent, false);
        return new BuyServiceGroupViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyServiceGroupViewHolder holder, int position) {
        if (mExchangeList != null) {
            ExchangeItem item = mExchangeList.get(position);
            holder.mGroupName.setText(item.getGroupName());
            holder.mGroupDescription.setText(item.getShortDes());
            holder.mGroupValid.setText(item.getValidDes());
            holder.mExchangeItem = item;
        } else {
            ServiceGroup item = mServiceGroupList.get(position);
            holder.mGroupName.setText(item.getName());
            holder.mGroupDescription.setText(item.getShortDes());
            holder.mGroupValid.setText(item.getValidity());
            holder.mServiceGroup = item;
        }

        int mod = position % 4;
        if (mod == 0) {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_blue_corner_6);
        } else if (mod == 1) {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_violet_corner_6);
        } else if (mod == 2) {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_yellow_corner_6);
        } else {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_dark_blue_corner_6);
        }

        holder.itemView.setTag(holder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(serviceListener != null){
                    serviceListener.serviceClick(position,holder);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mExchangeList != null) {
            return mExchangeList.size();
        } else if (mServiceGroupList != null) {
            return mServiceGroupList.size();
        }
        return 0;
    }
}
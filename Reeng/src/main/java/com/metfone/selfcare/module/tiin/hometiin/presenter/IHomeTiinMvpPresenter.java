package com.metfone.selfcare.module.tiin.hometiin.presenter;

import com.metfone.selfcare.module.tiin.base.MvpPresenter;
import com.metfone.selfcare.module.tiin.hometiin.view.HomeTiinMvpView;

public interface IHomeTiinMvpPresenter extends MvpPresenter {
    void getHome();
    void getHomeCache();
}

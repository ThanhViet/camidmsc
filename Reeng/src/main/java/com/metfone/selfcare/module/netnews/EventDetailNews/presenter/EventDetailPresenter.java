package com.metfone.selfcare.module.netnews.EventDetailNews.presenter;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.module.netnews.EventDetailNews.fragment.EventDetailFragment;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.netnews.request.NewsRequest;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Log;

public class EventDetailPresenter extends BasePresenter implements IEventDetailPresenter {
    public static final String TAG = EventDetailPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;

    public EventDetailPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadData(int cateId, int page) {
        mNewsApi.getNewsByEvent(new NewsRequest(cateId, page, CommonUtils.NUM_SIZE), callback);
    }

    HttpCallBack callback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            if (!isViewAttached() || !(getMvpView() instanceof EventDetailFragment)) {
                return;
            }
            Gson gson = new Gson();
            NewsResponse childNewsResponse = gson.fromJson(data, NewsResponse.class);

            ((EventDetailFragment) getMvpView()).bindData(childNewsResponse);
            ((EventDetailFragment) getMvpView()).loadDataSuccess(true);
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof EventDetailFragment)) {
                return;
            }
            ((EventDetailFragment) getMvpView()).loadDataSuccess(false);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
}

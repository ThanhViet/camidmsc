package com.metfone.selfcare.module.home_kh.fragment.setting.share;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingNotificationModel {
    @SerializedName("errorCode")
    @Expose
    public String errorCode;
    @SerializedName("errorMessage")
    @Expose
    public Object errorMessage;
    @SerializedName("result")
    @Expose
    public Result result;

    public String getErrorCode() {
        return errorCode;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public Result getResult() {
        return result;
    }

    public class Result {

        @SerializedName("errorCode")
        @Expose
        public String errorCode;
        @SerializedName("message")
        @Expose
        public String message;
        @SerializedName("object")
        @Expose
        public Object object;
        @SerializedName("userMsg")
        @Expose
        public String userMsg;
        @SerializedName("wsResponse")
        @Expose
        public String wsResponse;

        public String getErrorCode() {
            return errorCode;
        }

        public String getMessage() {
            return message;
        }

        public Object getObject() {
            return object;
        }

        public String getUserMsg() {
            return userMsg;
        }

        public String getWsResponse() {
            return wsResponse;
        }
    }
}
package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.camid.ProvinceDistrict;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.module.metfoneplus.adapter.StoreAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPSupportStoreDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.NumberPickerBottomSheetDialogFragment;
import com.metfone.selfcare.module.metfoneplus.holder.StoreViewHolder;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsFindStoreByAddrResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetNearestStoreResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProvinceDistrictResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPOrderNewSimFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPOrderNewSimFragment extends MPBaseFragment implements View.OnClickListener {
    public static final String TAG = MPOrderNewSimFragment.class.getSimpleName();
    private static final String MES = "mes";

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    TextView mActionBarTitle;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.recyclerView)
    RecyclerView mListProvinceDistrict;
    private String mes;
    private StoreAdapter mStoreAdapter;

    private View.OnClickListener mOnItemStoreTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            StoreViewHolder storeViewHolder = (StoreViewHolder) view.getTag();
            MPSupportStoreDialog supportStoreDialog = MPSupportStoreDialog.newInstance(storeViewHolder.mStore);
            supportStoreDialog.show(getParentFragmentManager(), MPSupportStoreDialog.TAG);
        }
    };

    public MPOrderNewSimFragment() {
        // Required empty public constructor
    }


    public static MPOrderNewSimFragment newInstance(String mes) {
        Bundle args = new Bundle();
        MPOrderNewSimFragment fragment = new MPOrderNewSimFragment();

        args.putSerializable(MES, mes);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findStoreByAddress(Constants.INVALID_LAT, Constants.INVALID_LNG);
        if (getArguments() != null) {
            mes = (String) getArguments().getString(MES);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_order_new_sim;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActionBarTitle.setText(getString(R.string.order_with_new_sim));
        Utilities.adaptViewForInserts(mLayoutActionBar);

       // DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mParentActivity, DividerItemDecoration.VERTICAL);
       // dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mParentActivity, R.drawable.divider_list_store)));
        date.setText(mes);
        mStoreAdapter = new StoreAdapter(mParentActivity, new ArrayList<Store>(), mOnItemStoreTapped);
        mListProvinceDistrict.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListProvinceDistrict.setHasFixedSize(true);
    //    mListProvinceDistrict.addItemDecoration(dividerItemDecoration);
        mListProvinceDistrict.setAdapter(mStoreAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.action_bar_back)
    void onBack() {
        popBackStackFragment();
    }

    private void findStoreByAddress(double latitude, double longitude) {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetNearestStore(latitude, longitude, new MPApiCallback<WsGetNearestStoreResponse>() {
            @Override
            public void onResponse(Response<WsGetNearestStoreResponse> response) {
                if (response.body() != null) {
                    updateListStore(response.body().getResult().getWsResponse());
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void updateListStore(List<Store> stores) {
        if (stores == null || stores.size() == 0) {
            mStoreAdapter.replaceData(new ArrayList<Store>());
           // mLayoutNoStore.setVisibility(View.VISIBLE);
            getCamIdUserBusiness().setStores(null);
        } else {
            mStoreAdapter.replaceData(stores);
//            mLayoutNoStore.setVisibility(View.GONE);
            getCamIdUserBusiness().setStores(stores);
        }
    }


    @Override
    public void onClick(View v) {

    }
}
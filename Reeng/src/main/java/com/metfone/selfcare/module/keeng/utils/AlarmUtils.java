/*
 * Copyright (c) 2017.
 * www.bigzun.com
 */

package com.metfone.selfcare.module.keeng.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.metfone.selfcare.BuildConfig;

/**
 * Created by namnh40 on 9/22/2017.
 */

public final class AlarmUtils {
    public static final String TAG = "AlarmUtils";
    public static final String ACTION_ALARM_CANCEL = BuildConfig.APPLICATION_ID + ".alarm.cancel";
    public static final String ACTION_ALARM_CANCEL_STOP_MUSIC = BuildConfig.APPLICATION_ID + ".alarm.cancel.stopmusic";
    public static final String ACTION_ALARM_STOP_MUSIC = BuildConfig.APPLICATION_ID + ".alarm.stopmusic";
    public static final String ACTION_ALARM_EXIT_APP = BuildConfig.APPLICATION_ID + ".alarm.exitapp";

    public static void scheduleExitApp(Context context, int minute, boolean cancel) {
        Log.e("AlarmUtils", "scheduleExitApp minute:" + minute);
        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, KeengAlarmReceiver.class);
            if (cancel) {
                intent.setAction(ACTION_ALARM_CANCEL);
            } else {
                intent.setAction(ACTION_ALARM_EXIT_APP);
            }
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            assert alarmManager != null;
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + minute * DateTimeUtils.MINUTE, AlarmManager.INTERVAL_DAY, pendingIntent);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    /**
     * @param minute: minutes
     */
    public static void scheduleStopMusic(Context context, int minute, boolean cancel) {
        Log.e("AlarmUtils", "scheduleStopMusic minute:" + minute);
        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, KeengAlarmReceiver.class);
            if (cancel) {
                intent.setAction(ACTION_ALARM_CANCEL_STOP_MUSIC);
            } else {
                intent.setAction(ACTION_ALARM_STOP_MUSIC);
            }
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            assert alarmManager != null;
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + minute * DateTimeUtils.MINUTE, AlarmManager.INTERVAL_DAY, pendingIntent);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    /**
     * @param time: millis seconds
     */
    public static void scheduleStopMusic(Context context, long time, boolean cancel) {
        Log.e("AlarmUtils", "scheduleStopMusic time:" + time);
        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, KeengAlarmReceiver.class);
            if (cancel) {
                intent.setAction(ACTION_ALARM_CANCEL_STOP_MUSIC);
            } else {
                intent.setAction(ACTION_ALARM_STOP_MUSIC);
            }
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            assert alarmManager != null;
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, AlarmManager.INTERVAL_DAY, pendingIntent);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

//    public static void scheduleOpenApp(Context context, int hour, int minute) {
//        Log.e("AlarmUtils", "scheduleOpenApp hour:" + hour + ", minute:" + minute);
//        try {
//            Calendar calendar = Calendar.getInstance();
//            calendar.set(Calendar.HOUR_OF_DAY, hour);
//            calendar.set(Calendar.MINUTE, minute);
//            calendar.set(Calendar.SECOND, 0);
//            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//            Intent intent = new Intent(context, AlarmReceiver.class);
//            intent.setAction(Constants.ACTION_ALARM_OPEN_APP);
//            intent.putExtra(Constants.KEY_DATA, "Hẹn giờ mở app mới  .............");
//            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//            assert alarmManager != null;
//            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
//        } catch (Exception e) {
//            Log.e(TAG, e);
//        }
//    }
}

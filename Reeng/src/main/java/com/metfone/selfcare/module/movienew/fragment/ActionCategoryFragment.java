package com.metfone.selfcare.module.movienew.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.movienew.adapter.ActionAdapter;
import com.metfone.selfcare.module.movienew.adapter.ItemHolder;
import com.metfone.selfcare.module.movienew.model.ActionFilmModel;
import com.metfone.selfcare.module.movienew.model.Category;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.util.RetrofitInstance;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

public class ActionCategoryFragment extends BaseFragment implements ItemHolder.OnclickItemAction {
    public static final String ID_KEY = "categoryID";
    public static final String NAME_KEY = "categoryName";
    public static final String TYPE_KEY = "type";
    public static int CATEGORY = 1;
    public static int COUNTRY = 2;

    public static ActionCategoryFragment newInstance(String id, String name, int type) {
        Bundle bundle = new Bundle();
        bundle.putString(ID_KEY, id);
        bundle.putString(NAME_KEY, name);
        bundle.putInt(TYPE_KEY, type);
        ActionCategoryFragment fragment = new ActionCategoryFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @BindView(R.id.recyclerAction)
    RecyclerView recyclerAction;
    @BindView(R.id.icBack)
    AppCompatImageView icBack;
    @BindView(R.id.titleLayout)
    AppCompatTextView titleLayout;
    private int type = CATEGORY;
    private String mCategory;
    private String mCategoryID;
    private Unbinder unbinder;
    private RetrofitInstance retrofitInstance = new RetrofitInstance();
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private UserInfo userInfo = userInfoBusiness.getUser();
    private ArrayList<HomeData> result = new ArrayList<>();
    private ActionAdapter actionAdapter;
    private int page = 1;
    private final int limit = 30;
    private boolean hasReached = false;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.action_category_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listener();
        loadData();
        actionAdapter = new ActionAdapter(result, getActivity(), this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        BaseAdapter.setupVerticalRecycler(activity, recyclerAction, linearLayoutManager, actionAdapter, R.drawable.divider_movie);
        recyclerAction.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!loading && !hasReached) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            page++;
                            loadData();
                        }
                    }
                }
            }
        });
    }

    private void loadData() {
        if (type == CATEGORY) {
            loading = true;
            if(page == 1){
                activity.showLoadingDialog("","");
            }
            MovieApi.getInstance().getListFilmOfCategory(mCategoryID, page, limit, new ApiCallbackV2<ActionFilmModel>() {
                @Override
                public void onSuccess(String msg, ActionFilmModel resultData) throws JSONException {
                    if (resultData == null || resultData.getHomeResult() == null || resultData.getHomeResult().size() == 0) {
                        hasReached = true;
                        return;
                    }
                    result.addAll(resultData.getHomeResult());
                    actionAdapter.updateData(result);
                    // Fix crash firebase ViewGroup.setVisibility(int)' on a null object reference
                    if(recyclerAction != null){
                        if (result.size() == 0) {
                            recyclerAction.setVisibility(View.INVISIBLE);
                        } else {
                            recyclerAction.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onError(String s) {
                    if(page > 1){
                        page--;
                    }
                }

                @Override
                public void onComplete() {
                    loading = false;
                    activity.hideLoadingDialog();
                }
            });
        } else if (type == COUNTRY) {
            loading = true;
            if(page == 1){
                activity.showLoadingDialog("","");
            }
            MovieApi.getInstance().getTopCountry(mCategoryID, page, limit, new ApiCallbackV2<ActionFilmModel>() {
                @Override
                public void onSuccess(String msg, ActionFilmModel resultData) throws JSONException {
                    if (resultData == null || resultData.getHomeResult() == null || resultData.getHomeResult().size() == 0) {
                        hasReached = true;
                        return;
                    }
                    result.addAll(resultData.getHomeResult());
                    actionAdapter.updateData(result);
                    if(recyclerAction != null){
                        if (result.size() == 0) {
                            recyclerAction.setVisibility(View.INVISIBLE);
                        } else {
                            recyclerAction.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onError(String s) {
                    if(page > 1){
                        page--;
                    }
                }

                @Override
                public void onComplete() {
                    loading = false;
                    activity.hideLoadingDialog();
                }
            });
        }
    }

    private void loadCategory(){
        MovieApi.getInstance().getCategoryV2(new ApiCallbackV2<ArrayList<Category>>() {
            @Override
            public void onSuccess(String msg, ArrayList<Category> result) {
                if(result != null){
                    for (Category category : result){
                        if(category.getCategoryId().equals(mCategoryID)){
                            titleLayout.setText(category.getCategoryName());
                            break;
                        }
                    }
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void listener() {
        if (getArguments() != null) {
            mCategoryID = getArguments().getString(ID_KEY);
            mCategory = getArguments().getString(NAME_KEY);
            type = getArguments().getInt(TYPE_KEY);
        }
        if(!StringUtils.isEmpty(mCategory)){
            titleLayout.setText(mCategory);
        }else{
            titleLayout.setText(R.string.category_movie);
            loadCategory();
        }
        icBack.setOnClickListener(v -> {
            if (getActivity() instanceof HomeActivity) {
                popBackStackFragment();
            } else {
                getActivity().finish();
            }
        });
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void clickItemAction(Object item, int position) {
        if(type == CATEGORY){
            ApplicationController.self().getFirebaseEventBusiness().logOpenFilmCountry(mCategory);
        }else if(type == COUNTRY){
            ApplicationController.self().getFirebaseEventBusiness().logOpenFilmCategory(mCategoryID,mCategory);
        }
        HomeData homeData = (HomeData) item;
        activity.playMovies(new Movie(homeData));
    }

}

package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.DataHistoryCharge;
import com.metfone.selfcare.model.camid.DataHistoryChargeDetail;
import com.metfone.selfcare.model.camid.EBPHistoryCharge;
import com.metfone.selfcare.model.camid.HistoryCharge;
import com.metfone.selfcare.model.camid.HistoryChargeType;
import com.metfone.selfcare.model.camid.HistoryDataInfo;
import com.metfone.selfcare.module.metfoneplus.holder.DataChargeChildrenViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.DataChargeParentViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.MyServiceViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.OtherChargeViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.TotalChargeViewHolder;

import java.util.List;

public class HistoryChargeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<HistoryCharge> mHistoryCharges;
    private final View.OnClickListener mMyServiceClickListener;
    private final View.OnClickListener mOtherChargeClickListener;

    public HistoryChargeAdapter(Context context, List<HistoryCharge> historyCharges,
                                View.OnClickListener otherChargeClickListener,
                                View.OnClickListener myServiceClickListener) {
        this.mContext = context;
        this.mHistoryCharges = historyCharges;
        this.mOtherChargeClickListener = otherChargeClickListener;
        this.mMyServiceClickListener = myServiceClickListener;
    }

    private void setList(List<HistoryCharge> historyCharges) {
        this.mHistoryCharges = historyCharges;
    }

    public void replaceData(List<HistoryCharge> objectList) {
        setList(objectList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == HistoryChargeType.DATA_CHARGE_PARENT.typeGroup) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_4, parent, false);
            return new DataChargeParentViewHolder(view);
        } else if (viewType == HistoryChargeType.DATA_CHARGE_CHILDREN.typeGroup) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_5, parent, false);
            return new DataChargeChildrenViewHolder(view);
        } else if (viewType == HistoryChargeType.TOTAL_CHARGE.typeGroup) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_1, parent, false);
            return new TotalChargeViewHolder(view);
        } else if (viewType == HistoryChargeType.MY_SERVICES.typeGroup) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_3, parent, false);
            return new MyServiceViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_2, parent, false);
            return new OtherChargeViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == HistoryChargeType.DATA_CHARGE_PARENT.typeGroup) {
            DataHistoryCharge dataHistoryCharge = (DataHistoryCharge) mHistoryCharges.get(position);
            DataChargeParentViewHolder dcpHolder = (DataChargeParentViewHolder) holder;
            dcpHolder.mQuantity.setText(String.valueOf(dataHistoryCharge.getQuantity()));
            dcpHolder.mTotal.setText(String.format(mContext.getString(R.string.m_p_detail_data_charge_total),
                    String.valueOf(dataHistoryCharge.getTotal())));
            dcpHolder.mContentValue.setText(mContext.getString(R.string.m_p_detail_data_charge_mb));

        } else if (holder.getItemViewType() == HistoryChargeType.DATA_CHARGE_CHILDREN.typeGroup) {
            DataHistoryChargeDetail dataHistoryChargeDetail = (DataHistoryChargeDetail) mHistoryCharges.get(position);
            HistoryDataInfo historyDataInfo = dataHistoryChargeDetail.getHistoryDataInfo();
            DataChargeChildrenViewHolder dccHolder = (DataChargeChildrenViewHolder) holder;
            dccHolder.mDate.setText(historyDataInfo.getDate());
            dccHolder.mData.setText(String.format(mContext.getString(R.string.m_p_detail_data_charge_data), String.valueOf(historyDataInfo.getTotal().intValue()), historyDataInfo.getUnit()));
            dccHolder.mMoney.setText(String.format(mContext.getString(R.string.m_p_detail_data_charge_money), String.valueOf(historyDataInfo.getMoney())));

        } else if (holder.getItemViewType() == HistoryChargeType.TOTAL_CHARGE.typeGroup) {
            EBPHistoryCharge ebpHistoryCharge = (EBPHistoryCharge) mHistoryCharges.get(position);
            TotalChargeViewHolder tcHolder = (TotalChargeViewHolder) holder;
            tcHolder.mIcon.setImageDrawable(ContextCompat.getDrawable(mContext, ebpHistoryCharge.getIdResDrawableIcon()));
            tcHolder.mTitle.setText(ebpHistoryCharge.getTitle());
            if (ebpHistoryCharge.isBasicTab()) {
                tcHolder.mIconMoneyBag.setVisibility(View.INVISIBLE);
                tcHolder.mValue.setText(String.format(mContext.getString(R.string.m_p_detail_basic_tab_total_charge_money), String.valueOf(ebpHistoryCharge.getValue())));
            } else {
                tcHolder.mIconMoneyBag.setVisibility(View.VISIBLE);
                tcHolder.mValue.setText(String.valueOf(ebpHistoryCharge.getValue()));
            }

        } else if (holder.getItemViewType() == HistoryChargeType.MY_SERVICES.typeGroup) {
            EBPHistoryCharge ebpHistoryCharge = (EBPHistoryCharge) mHistoryCharges.get(position);
            MyServiceViewHolder msHolder = (MyServiceViewHolder) holder;
            msHolder.mIcon.setImageDrawable(ContextCompat.getDrawable(mContext, ebpHistoryCharge.getIdResDrawableIcon()));
            msHolder.mTitle.setText(ebpHistoryCharge.getTitle());

            msHolder.itemView.setTag(msHolder);
            msHolder.itemView.setOnClickListener(mMyServiceClickListener);
        } else {
            EBPHistoryCharge ebpHistoryCharge = (EBPHistoryCharge) mHistoryCharges.get(position);
            OtherChargeViewHolder ocHolder = (OtherChargeViewHolder) holder;
            ocHolder.mIcon.setImageDrawable(ContextCompat.getDrawable(mContext, ebpHistoryCharge.getIdResDrawableIcon()));
            ocHolder.mTitle.setText(ebpHistoryCharge.getTitle());
            ocHolder.mValue.setText(String.format(mContext.getString(R.string.m_p_detail_other_charge_value), String.valueOf(ebpHistoryCharge.getValue())));

            ocHolder.mHistoryChargeType = ebpHistoryCharge.getType();
            ocHolder.itemView.setTag(ocHolder);
            ocHolder.itemView.setOnClickListener(mOtherChargeClickListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mHistoryCharges != null) {
            if (mHistoryCharges.get(position).getType() == HistoryChargeType.DATA_CHARGE_PARENT) {
                return HistoryChargeType.DATA_CHARGE_PARENT.typeGroup;
            } else if (mHistoryCharges.get(position).getType() == HistoryChargeType.DATA_CHARGE_CHILDREN) {
                return HistoryChargeType.DATA_CHARGE_CHILDREN.typeGroup;
            } else if (mHistoryCharges.get(position).getType() == HistoryChargeType.TOTAL_CHARGE) {
                return HistoryChargeType.TOTAL_CHARGE.typeGroup;
            } else if (mHistoryCharges.get(position).getType() == HistoryChargeType.MY_SERVICES) {
                return HistoryChargeType.MY_SERVICES.typeGroup;
            } else {
                // other
                return HistoryChargeType.CALLING_CHARGE.typeGroup;
            }
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        if (mHistoryCharges == null) {
            return 0;
        }
        return mHistoryCharges.size();
    }
}

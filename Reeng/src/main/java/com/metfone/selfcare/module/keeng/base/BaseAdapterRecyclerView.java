package com.metfone.selfcare.module.keeng.base;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.holder.ChildCategoryHomeHolder;
import com.metfone.selfcare.module.keeng.holder.HeaderTitleHolder;
import com.metfone.selfcare.module.keeng.holder.LoadMoreHolder;
import com.metfone.selfcare.module.keeng.holder.MediaHolder;
import com.metfone.selfcare.module.keeng.holder.PlaylistNewHolder;
import com.metfone.selfcare.module.keeng.holder.RankHomeHolder;
import com.metfone.selfcare.module.keeng.holder.SingerHolder;
import com.metfone.selfcare.util.Log;

public abstract class BaseAdapterRecyclerView extends BaseAdapter<BaseHolder> {
    protected BaseListener.OnClickMedia listener;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount, previousTotal;
    private boolean loading;
    private boolean isExpand = true;
    private boolean hasPosition = false;
    private boolean hasListenNo = true;

    public BaseAdapterRecyclerView(Context context, String ga_source) {
        super(context, ga_source);
    }

    public void setRecyclerView(RecyclerView recyclerView, final BaseListener.OnLoadMoreListener onLoadMoreListener) {
        if (recyclerView != null && recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        Log.d(TAG, "loadmore nao ....");
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                            loading = true;
                        }
                    }
                }
            });
        }
    }

    public void setOnCheckEnd(RecyclerView recyclerView, final BaseListener.OnClickMedia listener) {
        if (recyclerView != null && recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    // Log.d(TAG, "totalItemCount: " + totalItemCount + ";
                    // lastVisibleItem: " + lastVisibleItem);
                    if (totalItemCount == lastVisibleItem + 1) {
                        if (listener != null) {
                            listener.onScrollEnd(true);
                        }
                    } else {
                        if (listener != null) {
                            listener.onScrollEnd(false);
                        }
                    }
                }
            });
        }
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean isExpand) {
        this.isExpand = isExpand;
    }

    public boolean isHasPosition() {
        return hasPosition;
    }

    public void setHasPosition(boolean hasPosition) {
        this.hasPosition = hasPosition;
    }

    public boolean isHasListenNo() {
        return hasListenNo;
    }

    public void setHasListenNo(boolean hasListenNo) {
        this.hasListenNo = hasListenNo;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view;
        switch (type) {
            case ITEM_HEADER_TOPIC:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_header, parent, false);
                return new HeaderTitleHolder(view);

            case ITEM_TOPIC:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_child_topic_home, parent, false);
                return new ChildCategoryHomeHolder(view, listener);

            case ITEM_SINGER:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_singer, parent, false);
                return new SingerHolder(view, listener);

            case ITEM_LOAD_MORE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_load_more, parent, false);
                return new LoadMoreHolder(view);

            case ITEM_PLAYLIST:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_playlist, parent, false);
                return new PlaylistNewHolder(view);

            case ITEM_EMPTY:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_empty, parent, false);
                return new BaseHolder(view, listener);

            case ITEM_MEDIA_SONG:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song, parent, false);
                return new MediaHolder(view, listener).setHasOption(isExpand()).setHasPosition(isHasPosition())
                        .setHasListenNo(isHasListenNo());

            case ITEM_MEDIA_ALBUM:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_album, parent, false);
                return new MediaHolder(view, listener).setHasOption(isExpand()).setHasPosition(isHasPosition())
                        .setHasListenNo(isHasListenNo());

            case ITEM_MEDIA_VIDEO:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_video, parent, false);
                return new MediaHolder(view, listener).setHasOption(isExpand()).setHasPosition(isHasPosition())
                        .setHasListenNo(isHasListenNo());

            case ITEM_MEDIA_SONG_SINGER:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song_singer, parent, false);
                return new MediaHolder(view, listener).setHasSinger(true).setHasOption(true);

            case ITEM_MEDIA_ALBUM_HOME_HOT:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_album_home_hot, parent, false);
                return new MediaHolder(view, listener).setHasListenNo(false);

            case ITEM_MEDIA_VIDEO_HOME_HOT:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_video_home_hot, parent, false);
                return new MediaHolder(view, listener).setHasListenNo(false);

            case ITEM_MEDIA_SONG_HOME_HOT:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song_hot, parent, false);
                return new MediaHolder(view, listener).setHasListenNo(false).setHasOption(true);

            case ITEM_MEDIA_SONG_PLAYER:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song_player, parent, false);
                return new MediaHolder(view, listener).setHasOption(true);

            case ITEM_MEDIA_SONG_PLAYER_LOSSLESS:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song_player, parent, false);
                return new MediaHolder(view, listener).setHasOption(true).setHasLossless(true);

            case ITEM_SUGGEST_PLAYLIST:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_suggest_playlist, parent, false);
                return new PlaylistNewHolder(view);

            case ITEM_RANK_HOME:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_rank_home, parent, false);
                return new RankHomeHolder(view);

            case ITEM_RANK_HOME_TOP:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_rank_home_top, parent, false);
                return new RankHomeHolder(view);

            case ITEM_MEDIA_SONG_RANK:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song_hot, parent, false);
                return new MediaHolder(view, listener).setHasOption(isExpand()).setHasPosition(isHasPosition())
                        .setHasListenNo(isHasListenNo()).setHasDocquyen(false);

            case ITEM_MEDIA_SONG_HOT:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song_hot, parent, false);
                return new MediaHolder(view, listener).setHasListenNo(true).setHasOption(true);

            case ITEM_MEDIA_VIDEO_RANK:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_video_rank, parent, false);
                return new MediaHolder(view, listener).setHasOption(isExpand()).setHasPosition(isHasPosition())
                        .setHasListenNo(true);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_empty, parent, false);
                return new BaseHolder(view, listener);
        }
    }

    public void setLoaded() {
        this.loading = false;
    }

    public void setOnclickListener(BaseListener.OnClickMedia l) {
        this.listener = l;
    }

    public abstract int getItemViewType(int position);

    public Object getItem(int position) {
        return null;
    }

}

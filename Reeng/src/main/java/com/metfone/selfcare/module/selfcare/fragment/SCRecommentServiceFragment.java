package com.metfone.selfcare.module.selfcare.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.adapter.SCRecommentPackageAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCBundle;
import com.metfone.selfcare.module.selfcare.model.SCPackage;
import com.metfone.selfcare.module.selfcare.model.SCRecommentPackage;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCRecommentPackage;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class SCRecommentServiceFragment extends BaseFragment implements AbsInterface.OnPackageHeaderListener, SwipeRefreshLayout.OnRefreshListener {

    private LoadingViewSC loadingView;
    private RecyclerView recyclerView;
    private SCRecommentPackageAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int type;

    private ArrayList<SCRecommentPackage> data = new ArrayList<>();

    public static SCRecommentServiceFragment newInstance(Bundle bundle) {
        SCRecommentServiceFragment fragment = new SCRecommentServiceFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCRecommentServiceFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_recomment_package;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

        loadData();

        return view;
    }

    private void initView(View view) {
        Bundle bundle = getArguments();
        SCBundle data = (SCBundle) bundle.getSerializable(Constants.KEY_DATA);
        type = data.getType();

        layout_refresh = view.findViewById(R.id.refresh);
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.sc_primary));
            layout_refresh.setOnRefreshListener(this);
        }
        loadingView = view.findViewById(R.id.loading_view);
        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCRecommentPackageAdapter(mActivity, this);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);

        loadingView.setLoadingErrorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login lai
//                Intent intent = new Intent(getActivity(), SCAccountActivity.class);
//                mActivity.startActivity(intent);
            }
        });
    }

    private void loadData() {
        if (!isRefresh)
            loadingView.loadBegin();
        //Load account detail info
        final WSSCRestful restful = new WSSCRestful(mActivity);

        if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
            restful.getRecommentServices(new Response.Listener<RestSCRecommentPackage>() {
                                             @Override
                                             public void onResponse(RestSCRecommentPackage result) {
//                                                 super.onResponse(result);
                                                 hideRefresh();
                                                 if (result != null) {
                                                     if (result.getStatus() == 200 && result.getData() != null) {
                                                         if (Utilities.notEmpty(result.getData().getData())) {
                                                             loadingView.loadFinish();
                                                             data.clear();
                                                             data.addAll(result.getData().getData());
                                                             adapter.setItemsList(data);
                                                             adapter.notifyDataSetChanged();
                                                         } else {
                                                             loadingView.loadEmpty();
                                                         }
                                                     } else if (result.getStatus() == 401 || result.getStatus() == 403) {
                                                         //Login lai
                                                         loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                                                     } else {
                                                         //Fail
                                                         loadingView.loadError();
                                                     }
                                                 }
                                             }
                                         },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            hideRefresh();
                            if (volleyError != null && volleyError.networkResponse != null) {
                                int errorCode = volleyError.networkResponse.statusCode;
                                if (errorCode == 401 || errorCode == 403) {
                                    //Login lai
                                    loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                                } else {
                                    //Fail
                                    loadingView.loadError();
                                }
                            } else {
                                //Fail
                                loadingView.loadError();
                            }
                        }
                    });
        }
    }

    @Override
    public void onPackageClick(SCPackage item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        bundle.putInt(SCConstants.PREFERENCE.KEY_FROM_SOURCE, type);
        ((TabSelfCareActivity) mActivity).gotoPackageDetail(bundle);
    }

    @Override
    public void onHeaderClick(SCRecommentPackage item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, new SCBundle(type, item));
        ((TabSelfCareActivity) mActivity).gotoPackageList(bundle);
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        loadData();
    }
}

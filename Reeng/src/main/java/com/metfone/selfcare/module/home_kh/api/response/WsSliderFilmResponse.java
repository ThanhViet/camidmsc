package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.ResultResponse;

import java.util.ArrayList;

import lombok.Data;

@Data
public class WsSliderFilmResponse extends BaseResponse<WsSliderFilmResponse.Response> {
    @Data
    public class Response {
        @SerializedName("HOME_CINEMA")
        private String homeCinema;
        @SerializedName("HOME_SLIDER")
        private ArrayList<String> homeSlider;
    }
}


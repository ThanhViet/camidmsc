package com.metfone.selfcare.module.metfoneplus.topup.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopUpQrCodeRequest {

    String isdn;
    String desIsdn;
    String language = "en";
    String serial;
    String programCode = "TOPUP";

    public TopUpQrCodeRequest() {
    }

    @Override
    public String toString() {
        return "TopUpQrCodeRequest{" +
                "isdn='" + isdn + '\'' +
                ", desIsdn='" + desIsdn + '\'' +
                ", language='" + language + '\'' +
                ", serial='" + serial + '\'' +
                ", programCode='" + programCode + '\'' +
                '}';
    }
}

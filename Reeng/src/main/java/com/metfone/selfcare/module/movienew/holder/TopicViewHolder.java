package com.metfone.selfcare.module.movienew.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.model.CategoryModel;
import com.metfone.selfcare.module.movienew.model.Country;
import com.metfone.selfcare.module.movienew.model.CountryModel;

import java.util.List;
import java.util.Random;

public class TopicViewHolder extends BaseViewHolder {
    private String colors[] = {"#D80F2C","#E39207","#44CA83","#47A3E2","#AA87CC"};
    @Nullable
    Country countryModel;
    @Nullable
    CategoryModel categoryModel;
    int categorySize;


    public TopicViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
    }

    private TextView tvTopic;
    private CardView viewRoot;

    @Override
    public void initViewHolder(View v) {
        if (mData == null || mData.size() == 0) return;
        viewRoot = v.findViewById(R.id.viewRoot);
        tvTopic = v.findViewById(R.id.tvTopic);
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        int color = pos % colors.length;
        if (mData.get(pos) instanceof Country) {
            if (pos > 0 && mData.get(pos-1) instanceof CategoryModel){
                categorySize = pos;
            }
            color = (pos-categorySize) % colors.length;
            countryModel = (Country) mData.get(pos);
            tvTopic.setText(countryModel.getCategoryName());
        } else if (mData.get(pos) instanceof CategoryModel) {
            categoryModel = (CategoryModel) mData.get(pos);
            tvTopic.setText(categoryModel.getCategoryname());
        }
        viewRoot.setCardBackgroundColor(Color.parseColor(colors[color]));
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null) {
            itemViewClickListener.onItemViewClickListener(pos, mData);
        }
    }
}

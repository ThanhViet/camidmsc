package com.metfone.selfcare.module.keeng.widget.lyric;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.view.View;

import com.metfone.selfcare.module.keeng.fragment.player.LyricPlayerFragment;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

public class LoadLyric extends AsyncTask<String, Integer, String> {
    private WeakReference<LyricPlayerFragment> mFragment;
    private String TAG = "LoadLyric";

    public LoadLyric(LyricPlayerFragment fragment) {
        this.mFragment = new WeakReference<>(fragment);
    }

    @SuppressLint("NewApi")
    public <P, T extends AsyncTask<P, ?, ?>> void execute(T task, P... params) {
        if (task.getStatus() != Status.RUNNING) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        } else {
            Log.e(TAG, "task is running");
        }
    }

    @Override
    protected void onPreExecute() {
        if (mFragment != null && mFragment.get() != null) mFragment.get().normalAndSetLyric("");
    }

    @Override
    protected String doInBackground(String... strings) {
        StringBuilder sb = new StringBuilder();
        try {
            InputStream inputStream = new URL(strings[0]).openStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            bufferedReader.close();
            inputStream.close();
        } catch (MalformedURLException e) {
            Log.e(TAG, e);
        } catch (FileNotFoundException e) {
            Log.e(TAG, e);
        } catch (IOException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        if (s == null) s = "";
        if (mFragment != null && mFragment.get() != null) {
//            mFragment.get().setLyricStr(s);
            LrcView mLyricView = mFragment.get().getLyricView();
            if (mLyricView != null) {
                mLyricView.setVisibility(View.VISIBLE);
                mLyricView.loadLrc(s);
            }
            View txtContent = mFragment.get().getTvContent();
            if (txtContent != null)
                txtContent.setVisibility(View.GONE);
        }
    }
}

package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.RecyclingPagerAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCBanner;
import com.metfone.selfcare.module.selfcare.utils.SCImageLoader;

import java.util.ArrayList;
import java.util.List;

public class SCBannerAdapter extends RecyclingPagerAdapter {
    static final String TAG = "SCBannerAdapter";
    private List<SCBanner> datas = new ArrayList<>();
    private LayoutInflater inflater;
    private boolean isInfiniteLoop;
    private AbsInterface.OnSCHomeListener onItemListener;

    public SCBannerAdapter(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isInfiniteLoop = false;
    }

    public void setDatas(List<SCBanner> datas) {
        this.datas = datas;
    }

    private int getPosition(int position) {
        return isInfiniteLoop ? position % datas.size() : position;
    }

    public SCBanner getItem(int position) {
        try {
            return datas.get(position);
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public int getCount() {
        return isInfiniteLoop ? Integer.MAX_VALUE : datas.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup container) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.holder_sc_home_banner, container, false);
            vh = new ViewHolder();
            vh.imvThumbnail = (ImageView) convertView.findViewById(R.id.imvImage);
            vh.tvName = (TextView) convertView.findViewById(R.id.tvName);
            vh.iconPlay = (ImageView) convertView.findViewById(R.id.icon_play);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final SCBanner item = getItem(position);
        if (item != null) {

            vh.tvName.setText(item.getName());
            SCImageLoader.setBannerImage(vh.imvThumbnail.getContext(), vh.imvThumbnail, item.getIconUrl(), position);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onBannerClick(item);
                    }
                }
            });
        }

        return convertView;
    }

    public void setOnClickListener(AbsInterface.OnSCHomeListener listener) {
        this.onItemListener = listener;
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public SCBannerAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }

    static class ViewHolder {
        public ImageView imvThumbnail;
        public TextView tvName;
        public ImageView iconPlay;
    }
}

package com.metfone.selfcare.module.netnews.request;

/**
 * Created by HaiKE on 8/19/17.
 */

public class SearchRequest {

    int page;
    int num;
    String text;

    public SearchRequest(int page, int num, String text) {
        this.page = page;
        this.num = num;
        this.text = text;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

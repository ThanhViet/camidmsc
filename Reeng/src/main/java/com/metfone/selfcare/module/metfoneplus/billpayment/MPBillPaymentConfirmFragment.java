package com.metfone.selfcare.module.metfoneplus.billpayment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.databinding.ServiceBillPaymentFragmentBinding;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseBindingFragment;
import com.metfone.selfcare.module.metfoneplus.topup.WebViewActivity;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.ABAMobilePaymentBillPaymentRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsCreateBillPaymenTransRequest;
import com.metfone.selfcare.network.metfoneplus.response.ABAMobilePaymentBillPaymentResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCreateBillPaymentTransResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountPaymentInfoResponse;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

public class MPBillPaymentConfirmFragment extends MPBaseBindingFragment<ServiceBillPaymentFragmentBinding> implements View.OnClickListener {

    private final int LAUNCH_SECOND_ACTIVITY = 97;
    private static final String ARGS = "ARGS";
    private static final int LAUNCH_ABA_APP = 1000;
    private BaseAdapter mAmountAdapter;
    private MetfonePlusClient client = new MetfonePlusClient();
    private WsCreateBillPaymenTransRequest billPaymentRequest = new WsCreateBillPaymenTransRequest();
    private CamIdUserBusiness camIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
    private String mPaymentMethod;
    private WsGetAccountPaymentInfoResponse.Response data;
    private Boolean paymentIsProcessing = false;


    public static MPBillPaymentConfirmFragment newInstance(WsGetAccountPaymentInfoResponse.Response response) {
        Bundle args = new Bundle();
        args.putSerializable(ARGS, response);
        MPBillPaymentConfirmFragment fragment = new MPBillPaymentConfirmFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.service_bill_payment_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mBinding.viewRootlayout);
        Utilities.adaptViewForInsertBottom(mBinding.constraintLayout);
        initData();
        initView();
        iniListener();
    }

    private void initData() {
        data = (WsGetAccountPaymentInfoResponse.Response)getArguments().getSerializable(ARGS);

    }

    private void initDataRequest() {
        billPaymentRequest.accountEmoney = camIdUserBusiness.getMetfoneUsernameIsdn();
        billPaymentRequest.paymentAmount = data.paymentAmount;
        billPaymentRequest.ftthName = data.customerName;
        billPaymentRequest.totalAmount = data.totalAmount;
        billPaymentRequest.ftthAccount = data.phoneNumber;
        billPaymentRequest.contractIdInfo = data.contractIdInfo;
        billPaymentRequest.ftthType = "internet";
        billPaymentRequest.paymentMethod = mPaymentMethod;
        billPaymentRequest.phoneNumber = camIdUserBusiness.getMetfoneUsernameIsdn();
    }

    private void initView() {
        ServicePaymentAdapter servicePaymentAdapter = new ServicePaymentAdapter(ServicePaymentGroupModel.createServices(), new ServicePaymentAdapter.EventListener() {
            @Override
            public void onServicePaymentClicked(ServicePaymentModel servicePaymentModel) {
                mPaymentMethod = servicePaymentModel.getPaymentMethod();
                mBinding.btnNext.setEnabled(true);
            }
        });
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerView.setAdapter(servicePaymentAdapter);
        mBinding.btnBack.setOnClickListener(v -> popBackStackFragment());
        if(data != null){
            mBinding.tvAccount.setText(data.phoneNumber);
            mBinding.tvAmount.setText(NumberFormat.getCurrencyInstance(Locale.US).format(Double.parseDouble(data.paymentAmount)));
        }
    }

    private void iniListener() {
        mBinding.btnNext.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                if(!paymentIsProcessing){
                    initDataRequest();
                    requestTrans();
                }
                break;
        }
    }

    private void requestTrans() {
        paymentIsProcessing = true;
        if(mPaymentMethod.equals("AbaPay")){
            ABAMobilePaymentBillPaymentRequest.Request request = new ABAMobilePaymentBillPaymentRequest.Request();
            request.accountEmoney = camIdUserBusiness.getMetfoneUsernameIsdn();
            request.paymentAmount = data.paymentAmount;
            request.ftthName = data.customerName;
            request.totalAmount = data.totalAmount;
            request.ftthAccount = data.phoneNumber;
            request.contractIdInfo = data.contractIdInfo;
            request.ftthType = "internet";
            request.paymentMethod = mPaymentMethod;
            request.isdn = camIdUserBusiness.getMetfoneUsernameIsdn();
            client.wsABAMobileBillPayment(request, new MPApiCallback<ABAMobilePaymentBillPaymentResponse>() {
                            @Override
                            public void onResponse(Response<ABAMobilePaymentBillPaymentResponse> response) {
                                paymentIsProcessing = false;
                                if(response.body() != null){
                                    try {
                                        Intent abaIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().abapayDeeplink));
                                        startActivityForResult(abaIntent, LAUNCH_ABA_APP);
                                    }
                                    catch (Exception e){
                                        if(response.body().playStore!=null && !response.body().playStore.isEmpty()){
                                            Intent abaIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().playStore));
                                            startActivity(abaIntent);
                                            getActivity().finish();
                                        }
                                        else
                                        {
                                            paymentIsProcessing = false;
                                            ToastUtils.showToast(getContext(), getActivity().getString(R.string.m_p_notify_error_in_processing));
                                        }
                                    }

                                }
                                else {
                                    onError(null);
                                }
                            }

                            @Override
                            public void onError(Throwable error) {
                                paymentIsProcessing = false;
                                ToastUtils.showToast(getContext(), getActivity().getString(R.string.m_p_notify_error_in_processing));
                            }
                        });
        }
        else {
            client.createBillPaymentTrans(billPaymentRequest, new ApiCallback<WsCreateBillPaymentTransResponse>() {
                @Override
                public void onResponse(Response<WsCreateBillPaymentTransResponse> response) {
                    paymentIsProcessing = false;
                    if (response.body() != null && response.body().status && "00".equals(response.body().responseCode)) {
                        String url = response.body().redirectUrl;
                        Intent intent = new Intent(getActivity(), WebViewActivity.class);
                        intent.putExtra("url", url);
                        startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
                    } else {
                        ToastUtils.showToast(getContext(), getContext().getString(R.string.m_p_notify_error_in_processing));
                    }
                }

                @Override
                public void onError(Throwable error) {
                    paymentIsProcessing = false;
                    ToastUtils.showToast(getContext(), getActivity().getString(R.string.m_p_notify_error_in_processing));
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                for(int i = 0; i< 2; i++){
                    popBackStackFragment();
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}


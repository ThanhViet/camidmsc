package com.metfone.selfcare.module.keeng.holder;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.ui.imageview.AspectImageView;

public class SingerHolder extends BaseHolder {
    public AspectImageView image;
    public TextView name;

    public SingerHolder(View convertView, BaseListener.OnClickMedia h) {
        super(convertView, h);
        image = (AspectImageView) convertView.findViewById(R.id.image);
        name = (TextView) convertView.findViewById(R.id.name);
        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onMediaClick(v, getAdapterPosition());
                }

            }
        });
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.listener;

import com.metfone.selfcare.database.model.PhoneNumber;

import java.util.ArrayList;

public interface GetAllContactsListener {
    void onPrepareGetAllContacts();

    void onFinishedGetAllContacts(ArrayList<PhoneNumber> list, String phoneNumbers);
}

package com.metfone.selfcare.module.home_kh.api;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_PHONE_SERVICE;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.annotation.NonNull;

import com.metfone.esport.helper.AccountBusiness;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.request.CheckUserGameRequest;
import com.metfone.selfcare.module.home_kh.api.request.GetUserAuthKeyRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsCheckForceUpdateAppRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetAllPartnerGiftSearchRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetAllTeLeComGiftRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetAllTelecomGiftForFriendRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetHomeMovieApp2Request;
import com.metfone.selfcare.module.home_kh.api.request.WsGetHotMovieRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetListFilmForUserRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetListGiftReceivedRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetListLogWatchedRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetMovieCategoryV2Request;
import com.metfone.selfcare.module.home_kh.api.request.WsGetMovieCountryRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetMovieDetailRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetMovieRelatedRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetPartnerGiftDetailRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetPartnerGiftRedeemHistoryRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetPointTransferHistoryRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetRankDefineInfoRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetTopMovieCategoryRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsLogAppRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsMovieGetCategoryByIdsRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsMovieGetListFilmOfCategoryRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsMovieGetListGroupDetailRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsRedeemGiftAndReturnExchangeCodeRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsRedeemPointForFriendRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsRedeemPointRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsSliderFilmRequest;
import com.metfone.selfcare.module.home_kh.api.response.AccountPointRankResponse;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.CheckUserGamePermissionResponse;
import com.metfone.selfcare.module.home_kh.api.response.GetAuthKeyReponse;
import com.metfone.selfcare.module.home_kh.api.response.PartnerGiftRedeemHistoryResponse;
import com.metfone.selfcare.module.home_kh.api.response.PointTransferHistoryResponse;
import com.metfone.selfcare.module.home_kh.api.response.RankDefineResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsCheckForceUpdateAppResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetAllPartnerGiftSearchResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetAllTeLeComGiftResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetGiftResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetHomeMovieApp2Response;
import com.metfone.selfcare.module.home_kh.api.response.WsGetListFilmForUserResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetListLogWatchedResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieCategoryV2Response;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieCountryResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetPartnerGiftDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetCategoryByIdsResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetListFilmOfCategoryResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetListGroupDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetRelatedResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsPopupTetResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemGiftAndReturnExchangeCodeResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemGiftTetResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemPointResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsSliderFilmResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.api.rx.KhHomeServiceRx;
import com.metfone.selfcare.module.home_kh.api.rx.KhHomeServiceRxDev;
import com.metfone.selfcare.module.home_kh.fragment.rewards.DetailType;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationClearResponse;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationReadResponse;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationResponse;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.request.AutoLoginRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsAccountInfoRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsCheckShowPopupRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsRedeemGiftTetRequest;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsAccountInfoResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

// 3 7 11 15 -> api test
public class KhHomeClient {
    private static final String TAG = KhHomeClient.class.getSimpleName();
    private static Retrofit mRetrofit = null;
    private static Retrofit mRetrofitDev = null;
    private static Retrofit mRetrofitMovie = null;
    private static KhHomeService khHomeService = null;
    private static KhHomeService khHomeServiceMovie = null;
    private static KhHomeServiceDev khHomeServiceDev = null;
    private static KhHomeServiceRx khHomeServiceRx = null;
    private static KhHomeServiceRx khHomeServiceRxMovie = null;
    private static KhHomeServiceRxDev khHomeServiceRxDev = null;
    public static CamIdUserBusiness mCamIdUserBusiness = null;


    //Dev
  //  private static final String ENDPOINT = "http://36.37.242.104:8123/ApiGateway/CoreService/";
  //  private static final String ENDPOINT_DEV = "http://36.37.242.104:8123/ApiGateway/CoreService/";
    // production
    private static final String ENDPOINT = "https://apigw.camid.app/CoreService/";
    private static final String ENDPOINT_DEV = "https://apigw.camid.app/CoreService/";
    private static final String ENDPOINT_MOVIE = "https://moviegw.camid.app/CoreService/";

    public static final String APP_CODE = "MyVTG";
    public static final String PREFIX = "855";
    public static final String DEVICE = "000229163ad3286e";
    public static String LANGUAGE = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
    public static final String PROGRAM = "CAM_ID";
    public static final String ERROR_CODE_SUCCESS = "S200";
    private static final String API_KEY = "5E648E0585B500A5CB8F0B392D4965A8176E352E1DC3A4FE31186CEB0EA5BE46";
    // Hien tai chi lam cho thue bao tra truoc nen mac dinh subType = 1
    private static final int SUB_TYPE_PREPAID = 1;
    private static String WS_SESSION_ID = "";
    private static String WS_TOKEN = "";
    public static final int TOTAL_ITEM_EACH_PAGE = 100;
    private static KhHomeClient instance = null;
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(ApplicationController.self().getApplicationContext());
    private ReengAccountBusiness reengAccountBusiness = ApplicationController.self().getReengAccountBusiness();
    private String metfoneGiftCode = "PCHBEN";

    //    private String metfoneGiftCode = "KHNY68";
//    private String metfoneGiftCode = "MG68LD10";
//    private String metfoneGiftCode = "MG68LD120";
    private static String getLanguageCode() {
        return LocaleManager.getLanguage(ApplicationController.self());
    }

    private static Retrofit getRetrofit() {
        if (mRetrofit == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }

            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();

            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.DEBUG ? ENDPOINT_DEV : ENDPOINT)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofit;
    }

    private static Retrofit getRetrofitForMovie() {
        if (mRetrofitMovie == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }

            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();

            mRetrofitMovie = new Retrofit.Builder()
                    .baseUrl(ENDPOINT_MOVIE)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofitMovie;
    }

    public KhHomeClient() {
//        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_WS_ISDN, "972550101");
    }

    public static KhHomeClient getInstance() {
        if (instance == null) {
            instance = new KhHomeClient();
        }
        return instance;
    }

    private static Retrofit getRetrofitDev() {
        if (mRetrofitDev == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }

            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();

            mRetrofitDev = new Retrofit.Builder()
                    .baseUrl(ENDPOINT_DEV)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofitDev;
    }

    public static KhHomeService getKhHomeService() {
        if (khHomeService == null) {
            khHomeService = getRetrofit().create(KhHomeService.class);
        }
        return khHomeService;
    }

    public static KhHomeService getKhHomeServiceMovie() {
        if (khHomeServiceMovie == null) {
            khHomeServiceMovie = getRetrofitForMovie().create(KhHomeService.class);
        }
        return khHomeServiceMovie;
    }

    public static KhHomeServiceRx getKhHomeServiceRx() {
        if (khHomeServiceRx == null) {
            khHomeServiceRx = getRetrofit().create(KhHomeServiceRx.class);
        }
        return khHomeServiceRx;
    }

    public static KhHomeServiceRx getKhHomeServiceRxMovie() {
        if (khHomeServiceRxMovie == null) {
            khHomeServiceRxMovie = getRetrofitForMovie().create(KhHomeServiceRx.class);
        }
        return khHomeServiceRxMovie;
    }

    public static KhHomeServiceRxDev getKhHomeServiceRxDev() {
        if (khHomeServiceRxDev == null) {
            khHomeServiceRxDev = getRetrofitDev().create(KhHomeServiceRxDev.class);
        }
        return khHomeServiceRxDev;
    }

    public static KhHomeServiceDev getKhHomeServiceDev() {
        if (khHomeServiceDev == null) {
            khHomeServiceDev = getRetrofitDev().create(KhHomeServiceDev.class);
        }
        return khHomeServiceDev;
    }

    /**
     * Create base request default
     *
     * @param t      object request extend BaseRequest
     * @param wsCode wsCode of request {@link Constants.WSCODE}
     * @param <T>    object type
     * @return BaseRequest has sessionId, wsCode, token, language
     */
    public static <T extends BaseRequest<?>> T createBaseRequest(T t, String wsCode) {
        if (mCamIdUserBusiness == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        }
        WS_SESSION_ID = mCamIdUserBusiness.getMetfoneSessionId();
        WS_TOKEN = mCamIdUserBusiness.getMetfoneToken();
        LANGUAGE = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
//        WS_SESSION_ID = "6f19b086-cd85-46f2-9925-bce0b0179dde"; // TODO
        t.setSessionId(WS_SESSION_ID);
        t.setToken(WS_TOKEN);
        // set token for test reward id screen
//        t.setToken("DCMMy2jw+1+ZGww8PAL52w==");
        t.setLanguage(LANGUAGE);
        t.setWsCode(wsCode);
        return t;
    }

    private void showResponseError(int code) {
        if (code == 401) {
            if (ApplicationController.self().getCurrentActivity() != null) {
                ApplicationController.self().getCurrentActivity().restartApp();
            }
            Log.e(TAG, "onResponse - unauthenticated: " + code);
        } else if (code >= 400 && code < 500) {
            Log.e(TAG, "onResponse - clientError:" + code);
        } else if (code >= 500 && code < 600) {
            Log.e(TAG, "onResponse - serverError:" + code);
        } else {
            Log.e(TAG, "onResponse - RuntimeException - unexpectedError:" + code);
        }
    }

    private void showFailure(Throwable t) {
        if (t instanceof IOException) {
            Log.e(TAG, "showFailure: Network error", t);
        } else {
            Log.e(TAG, "onFailure: UnexpectedError", t);
        }
    }

    public void autoLogin(String username, MPApiCallback<LoginResponse> callback) {
        AutoLoginRequest request = new AutoLoginRequest();
        request.setAppCode(APP_CODE);
        request.setPrefix(PREFIX);
        request.setDevice(DEVICE);
        request.setUserName(username);

        KhHomeService service = getKhHomeService();
        service.autoLogin(request.createRequestBody()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.code() == 200 && response.body() != null && response.body().getUsername() != null) {
                    mCamIdUserBusiness.setMetfoneSessionId(response.body().getSessionId());
                    mCamIdUserBusiness.setMetfoneToken(response.body().getToken());
                    mCamIdUserBusiness.setMetfoneUsernameIsdn(response.body().getUsername());
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsAccountInfo(MPApiCallback<WsAccountInfoResponse> callback) {
        WsAccountInfoRequest request = createBaseRequest(new WsAccountInfoRequest(), Constants.WSCODE.WS_ACCOUNT_INFO);
        WsAccountInfoRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(LANGUAGE);
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        request.setWsRequest(subRequest);
        KhHomeService service = getKhHomeService();
        service.wsAccountInfo(request.createRequestBody()).enqueue(new Callback<WsAccountInfoResponse>() {
            @Override
            public void onResponse(Call<WsAccountInfoResponse> call, Response<WsAccountInfoResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsAccountInfoResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    // 1
    public void wsGetAccountRankInfo(MPApiCallback<WsGetAccountRankInfoResponse> callback) {
        WsGetAccountRankInfoRequest request = createBaseRequest(new WsGetAccountRankInfoRequest(), Constants.WSCODE.WS_GET_ACCOUNT_RANK_INFO);
        WsGetAccountRankInfoRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        subRequest.custId = String.valueOf(userInfoBusiness.getUser().getUser_id());
        request.setWsRequest(subRequest);

        KhHomeService service = getKhHomeService();
        service.wsAccountRank(request.createRequestBody()).enqueue(new Callback<WsGetAccountRankInfoResponse>() {
            @Override
            public void onResponse(Call<WsGetAccountRankInfoResponse> call, Response<WsGetAccountRankInfoResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getResult() != null) {
                        AccountRankDTO accountRankDTO = response.body().getResult().accountRankDTO;
                        if (!response.body().getErrorCode().equals("000")) {
                            if (accountRankDTO == null) {
                                accountRankDTO = new AccountRankDTO();
                                accountRankDTO.rankId = 1;
                            }
                        }
                        response.body().getResult().accountRankDTO = accountRankDTO;
                        ApplicationController.self().setAccountRankDTO(accountRankDTO);
                    } else {
                        ApplicationController.self().setAccountRankDTO(null);
                    }
                    if (callback != null) callback.onResponse(response);
                } else {
                    ApplicationController.self().setAccountRankDTO(null);
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetAccountRankInfoResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                ApplicationController.self().setAccountRankDTO(null);
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    // 2
    public void wsGtAllPartnerGiftSearch(MPApiCallback<WsGetGiftSearchResponse> callback) {
        WsGtAllPartnerGiftSearchRequest request = createBaseRequest(new WsGtAllPartnerGiftSearchRequest(), Constants.WSCODE.WS_GET_ALL_PARTNER_GIFT_SEARCH);
        WsGtAllPartnerGiftSearchRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        ApplicationController app = ApplicationController.self();
        AccountRankDTO rank = app.getAccountRankDTO();
        if (rank == null) {
            subRequest.rankId = "0";
            subRequest.giftPoint = GiftPoint.ALL.value;
        } else {
            subRequest.rankId = String.valueOf(rank.rankId);
            subRequest.giftPoint = GiftPoint.ALL.value;
        }
        request.setWsRequest(subRequest);

        KhHomeService service = getKhHomeService();
        service.wsGtAllPartnerGiftSearch(request.createRequestBody()).enqueue(new Callback<WsGetGiftSearchResponse>() {
            @Override
            public void onResponse(Call<WsGetGiftSearchResponse> call, Response<WsGetGiftSearchResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;

                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetGiftSearchResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    // 11
    public void wsGetListGiftBuyMost(MPApiCallback<WsGetGiftListResponse> callback) {
        WsGtAllPartnerGiftSearchRequest request = createBaseRequest(new WsGtAllPartnerGiftSearchRequest(), "wsGetListGiftBuyMost");
        WsGtAllPartnerGiftSearchRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(getLanguageCode());
        request.setWsRequest(subRequest);

        KhHomeService service = getKhHomeService();
        service.wsGtAllPartnerGiftDetail(request.createRequestBody()).enqueue(new Callback<WsGetGiftListResponse>() {
            @Override
            public void onResponse(Call<WsGetGiftListResponse> call, Response<WsGetGiftListResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;

                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetGiftListResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    // #15
    public void wsGetListGiftBuyMostForUser(MPApiCallback<WsGetGiftListResponse> callback) {
        WsGetListGiftBuyMostForUserRequest request = createBaseRequest(new WsGetListGiftBuyMostForUserRequest(), "wsGetListGiftBuyMostForUser");
        WsGetListGiftBuyMostForUserRequest.Request subRequest = request.new Request();
        subRequest.language = getLanguageCode();
        subRequest.isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        subRequest.custId = String.valueOf(userInfoBusiness.getUser().getUser_id());
        request.setWsRequest(subRequest);

        getKhHomeService().wsGtAllPartnerGiftDetail(request.createRequestBody()).enqueue(new Callback<WsGetGiftListResponse>() {
            @Override
            public void onResponse(Call<WsGetGiftListResponse> call, Response<WsGetGiftListResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;

                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetGiftListResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    @SuppressLint("CheckResult")
    public void wsGetRankDefineInfo(KhApiCallback<KHBaseResponse<RankDefineResponse>> callback) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsGetRankDefineInfoRequest.WsRequest wsRequest = new WsGetRankDefineInfoRequest.WsRequest(isdn, String.valueOf(userInfoBusiness.getUser().getUser_id()));
        WsGetRankDefineInfoRequest request =
                createBaseRequest(new WsGetRankDefineInfoRequest(), Constants.WSCODE.WS_GET_RANK_DEFINE_INFO);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<RankDefineResponse>> cryptoObservable = getKhHomeServiceRx().wsGetRankDefineInfo(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    @SuppressLint("CheckResult")
    public void wsGetAccountRankInfo(KhApiCallback<KHBaseResponse<AccountRankInfoResponse>> callback) {
        WsGetAccountRankInfoRequest request = createBaseRequest(new WsGetAccountRankInfoRequest(), Constants.WSCODE.WS_GET_ACCOUNT_RANK_INFO);
        WsGetAccountRankInfoRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(LANGUAGE);
        subRequest.custId = String.valueOf(userInfoBusiness.getUser().getUser_id());
//        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
//        subRequest.isdn = isdn;
        request.setWsRequest(subRequest);
        Observable<KHBaseResponse<AccountRankInfoResponse>> cryptoObservable = getKhHomeServiceRx().wsGetAccountRankInfo(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        KHBaseResponse<AccountRankInfoResponse> khResponse = data;
                        if (khResponse != null) {
                            if (!khResponse.getErrorCode().equals("000")) {
                                AccountRankInfoResponse infoResponse = khResponse.getData();
                                if (infoResponse != null) {
                                    AccountRankDTO accountRankDTO = infoResponse.getAccountRankDTO();
                                    if (accountRankDTO == null) {
                                        accountRankDTO = new AccountRankDTO();
                                        accountRankDTO.rankId = 1;
                                    }
                                    infoResponse.setAccountRankDTO(accountRankDTO);
                                    khResponse.setData(infoResponse);
                                }
                            }
                        }
                        callback.onSuccess(khResponse);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    @SuppressLint("CheckResult")
    public void wsGetAccountPointInfo(KhApiCallback<KHBaseResponse<AccountPointRankResponse>> callback) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsGetRankDefineInfoRequest.WsRequest wsRequest = new WsGetRankDefineInfoRequest.WsRequest(isdn, String.valueOf(userInfoBusiness.getUser().getUser_id()));
        WsGetRankDefineInfoRequest request =
                createBaseRequest(new WsGetRankDefineInfoRequest(), Constants.WSCODE.WS_GET_ACCOUNT_POINT_INFO);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<AccountPointRankResponse>> cryptoObservable = getKhHomeServiceRx().wsGetAccountPointInfo(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    @SuppressLint("CheckResult")
    public void wsGetPartnerGiftRedeemHistory(KhApiCallback<KHBaseResponse<PartnerGiftRedeemHistoryResponse>> callback, int page) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsGetPartnerGiftRedeemHistoryRequest.WsRequest wsRequest =
                new WsGetPartnerGiftRedeemHistoryRequest.WsRequest(isdn, TOTAL_ITEM_EACH_PAGE, "en", page);
        wsRequest.custId = String.valueOf(userInfoBusiness.getUser().getUser_id());
        WsGetPartnerGiftRedeemHistoryRequest request =
                createBaseRequest(new WsGetPartnerGiftRedeemHistoryRequest(), Constants.WSCODE.WS_GET_PARTNER_GIFT_REDEEM_HISTORY);
        request.setWsRequest(wsRequest);
        getGift(request, callback);
    }

    public void wsGetListGiftReceived(KhApiCallback<KHBaseResponse<PartnerGiftRedeemHistoryResponse>> callback) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsGetListGiftReceivedRequest.WsRequest wsRequest =
                new WsGetListGiftReceivedRequest.WsRequest(isdn, "en");
        wsRequest.custId = String.valueOf(userInfoBusiness.getUser().getUser_id());
        WsGetListGiftReceivedRequest request =
                createBaseRequest(new WsGetListGiftReceivedRequest(), Constants.WSCODE.WS_GET_LIST_GIFT_RECEIVED);
        request.setWsRequest(wsRequest);
        getGift(request, callback);
    }

    @SuppressLint("CheckResult")

    public void getGift(BaseRequest request, KhApiCallback callback) {
        Observable<KHBaseResponse<PartnerGiftRedeemHistoryResponse>> cryptoObservable = getKhHomeServiceRx().wsGetPartnerGiftRedeemHistory(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    @SuppressLint("CheckResult")
    public void wsGetPointTransferHistory(KhApiCallback<KHBaseResponse<PointTransferHistoryResponse>> callback, String type) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsGetPointTransferHistoryRequest.WsRequest wsRequest =
                new WsGetPointTransferHistoryRequest.WsRequest(isdn, type);
        wsRequest.custId = String.valueOf(userInfoBusiness.getUser().getUser_id());
        WsGetPointTransferHistoryRequest request =
                createBaseRequest(new WsGetPointTransferHistoryRequest(), Constants.WSCODE.WS_GET_POINT_TRANSFER_HISTORY);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<PointTransferHistoryResponse>> cryptoObservable =
                getKhHomeServiceRx().wsGetPointTransferHistory(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    @SuppressLint("CheckResult")
    public void wsGetAllPartnerGiftSearch(KhApiCallback<KHBaseResponse<WsGetAllPartnerGiftSearchResponse>> callback, String rankId, int id) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsGetAllPartnerGiftSearchRequest.WsRequest wsRequest =
                new WsGetAllPartnerGiftSearchRequest.WsRequest(isdn, rankId, String.valueOf(id));
        WsGetAllPartnerGiftSearchRequest request =
                createBaseRequest(new WsGetAllPartnerGiftSearchRequest(), Constants.WSCODE.WS_GET_ALL_PARTNER_GIFT_SEARCH);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<WsGetAllPartnerGiftSearchResponse>> cryptoObservable =
                getKhHomeServiceRx().wsGtAllPartnerGiftSearch(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    //#10
    @SuppressLint("CheckResult")
    public void wsGetAllTeLeComGift(KhApiCallback<KHBaseResponse<WsGetAllTeLeComGiftResponse>> callback) {
        String isdn = SharedPrefs.getInstance().get(PREF_PHONE_SERVICE, String.class);
        WsGetAllTeLeComGiftRequest.WsRequest wsRequest = new WsGetAllTeLeComGiftRequest.WsRequest(isdn,
                LocaleManager.getLanguage(ApplicationController.self().getApplicationContext()));
        WsGetAllTeLeComGiftRequest request =
                createBaseRequest(new WsGetAllTeLeComGiftRequest(), Constants.WSCODE.WS_GET_ALL_TE_LE_COM_GIFT);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<WsGetAllTeLeComGiftResponse>> cryptoObservable =
                getKhHomeServiceRx().wsGetAllTeLeComGiftResponse(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    //#3
    @SuppressLint("CheckResult")
    public void wsGetPartnerGiftDetail(KhApiCallback<KHBaseResponse<WsGetPartnerGiftDetailResponse>> callback, String gifId) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsGetPartnerGiftDetailRequest.WsRequest wsRequest =
                new WsGetPartnerGiftDetailRequest.WsRequest(isdn, gifId);
        wsRequest.custId = String.valueOf(userInfoBusiness.getUser().getUser_id());
        WsGetAllTeLeComGiftRequest request =
                createBaseRequest(new WsGetAllTeLeComGiftRequest(), Constants.WSCODE.WS_GET_PARTNER_GIFT_DETAIL);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<WsGetPartnerGiftDetailResponse>> cryptoObservable =
                getKhHomeServiceRx().wsGetPartnerGiftDetail(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    //#14
    @SuppressLint("CheckResult")
    public void wsRedeemPoint(KhApiCallback<KHBaseResponse<WsRedeemPointResponse>> callback,
                              int point, @NonNull DetailType type) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsRedeemPointRequest.WsRequest wsRequest =
                new WsRedeemPointRequest.WsRequest(isdn, type, point);
        WsRedeemPointRequest request =
                createBaseRequest(new WsRedeemPointRequest(), Constants.WSCODE.WS_REDEEM_POINT);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<WsRedeemPointResponse>> cryptoObservable =
                getKhHomeServiceRx().wsRedeemPoint(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    //#7
    @SuppressLint("CheckResult")
    public void wsRedeemGiftAndReturnExchangeCode(KhApiCallback<KHBaseResponse<WsRedeemGiftAndReturnExchangeCodeResponse>> callback,
                                                  String giftId, String rankId, String phone) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsRedeemGiftAndReturnExchangeCodeRequest.WsRequest wsRequest =
                new WsRedeemGiftAndReturnExchangeCodeRequest.WsRequest(giftId, phone, isdn, rankId);
        wsRequest.custId = String.valueOf(userInfoBusiness.getUser().getUser_id());
        WsRedeemGiftAndReturnExchangeCodeRequest request =
                createBaseRequest(new WsRedeemGiftAndReturnExchangeCodeRequest(), Constants.WSCODE.WS_REDEEM_GIFT_AND_RETURN_EXCHANGE_CODE);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<WsRedeemGiftAndReturnExchangeCodeResponse>> cryptoObservable =
                getKhHomeServiceRx().wsRedeemGiftAndReturnExchangeCode(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    // Notifications
    public void wsGetListCamIDNotification(String userId, int page, String deviceId, MPApiCallback<KhNotificationResponse> callback) {
        WsGetNotificationRequest request = createBaseRequest(new WsGetNotificationRequest(), "wsGetListCamIDNotification");
        WsGetNotificationRequest.Request subRequest = request.new Request();
        subRequest.language = LANGUAGE;
        if (userId.equals("0"))
            subRequest.camId = "";
        else
            subRequest.camId = userId;
        subRequest.page = page;
        subRequest.deviceId = deviceId;
        request.setWsRequest(subRequest);

        KhHomeService service = getKhHomeService();
        service.wsGetListCamIDNotification(request.createRequestBody()).enqueue(new Callback<KhNotificationResponse>() {
            @Override
            public void onResponse(Call<KhNotificationResponse> call, Response<KhNotificationResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;

                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<KhNotificationResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    // Notifications-read
    public void wsUpdateIsReadCamIDNotification(String userId, int notificationId, String deviceId, MPApiCallback<KhNotificationReadResponse> callback) {
        WsUpdateIsReadCamIDNotification request = createBaseRequest(new WsUpdateIsReadCamIDNotification(), "wsUpdateIsReadCamIDNotification");
        WsUpdateIsReadCamIDNotification.Request subRequest = request.new Request();
        subRequest.language = LANGUAGE;
        if (userId.equals("0"))
            subRequest.camid = "";
        else
            subRequest.camid = userId;
        subRequest.notifyId = notificationId;
        subRequest.deviceId = deviceId;
        request.setWsRequest(subRequest);

        KhHomeService service = getKhHomeService();
        service.wsUpdateIsReadCamIDNotification(request.createRequestBody()).enqueue(new Callback<KhNotificationReadResponse>() {
            @Override
            public void onResponse(Call<KhNotificationReadResponse> call, Response<KhNotificationReadResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<KhNotificationReadResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsClearAllCamIdNotification(String userId, String deviceId, MPApiCallback<KhNotificationClearResponse> callback) {
        WsClearAllCamIdNotification request = createBaseRequest(new WsClearAllCamIdNotification(), "wsClearAllCamIdNotification");
        WsClearAllCamIdNotification.Request subRequest = request.new Request();
        subRequest.language = LANGUAGE;
        if (userId.equals("0"))
            subRequest.camid = "";
        else
            subRequest.camid = userId;
        subRequest.deviceId = deviceId;
        request.setWsRequest(subRequest);

        KhHomeService service = getKhHomeService();
        service.wsClearAllCamIdNotification(request.createRequestBody()).enqueue(new Callback<KhNotificationClearResponse>() {
            @Override
            public void onResponse(Call<KhNotificationClearResponse> call, Response<KhNotificationClearResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<KhNotificationClearResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });

    }

    @SuppressLint("CheckResult")
    public void wsCheckForceUpdateApp(KhApiCallback<KHBaseResponse<WsCheckForceUpdateAppResponse>> callback, String currentVersion) {
        WsCheckForceUpdateAppRequest.WsRequest wsRequest =
                new WsCheckForceUpdateAppRequest.WsRequest(currentVersion);
        WsCheckForceUpdateAppRequest request =
                createBaseRequest(new WsCheckForceUpdateAppRequest(), Constants.WSCODE.WS_CHECK_FORCE_UPDATE_APP);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<WsCheckForceUpdateAppResponse>> cryptoObservable =
                getKhHomeServiceRx().wsCheckForceUpdateApp(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    public void wsGetAllApps(MPApiCallback<WsGetAllAppsResponse> callback) {
        WsGetAllAppsRequest request = createBaseRequest(new WsGetAllAppsRequest(), Constants.WSCODE.WS_GET_ALL_APPS);
        WsGetAllAppsRequest.Request subRequest = request.new Request();
        subRequest.language = "en";
        request.setUsername(AccountBusiness.getInstance().getPhoneNumber());
        request.setApiKey("6CB8FC45D491D87CECB53428D79423BD");
        request.setWsRequest(subRequest);

        KhHomeService service = getKhHomeService();
        service.wsGetAllApps(request.createRequestBody()).enqueue(new Callback<WsGetAllAppsResponse>() {
            @Override
            public void onResponse(Call<WsGetAllAppsResponse> call, Response<WsGetAllAppsResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetAllAppsResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetGiftInfo(MPApiCallback<WsGiftInfoResponse> callback) {
        WsGiftRequest request = createBaseRequest(new WsGiftRequest(), Constants.WSCODE.WS_GET_GIFT_INFO);
        WsGiftRequest.Request subRequest = request.new Request();
        subRequest.msisdn = getJidNumberFromSharedPrefs();
        subRequest.programeCode = metfoneGiftCode;
        subRequest.language = LANGUAGE;
        request.setWsRequest(subRequest);

        getKhHomeService().wsGetGiftInfo(request.createRequestBody()).enqueue(new Callback<WsGiftInfoResponse>() {
            @Override
            public void onResponse(Call<WsGiftInfoResponse> call, Response<WsGiftInfoResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGiftInfoResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGiftSpin(MPApiCallback<WsGiftSpinResponse> callback, String programeCode) {
        WsGiftRequest request = createBaseRequest(new WsGiftRequest(), Constants.WSCODE.WS_GIFT_SPIN);
        WsGiftRequest.Request subRequest = request.new Request();
        subRequest.msisdn = getJidNumberFromSharedPrefs();
        subRequest.programeCode = programeCode;
        subRequest.language = LANGUAGE;

        request.setWsRequest(subRequest);

        getKhHomeService().wsGiftSpin(request.createRequestBody()).enqueue(new Callback<WsGiftSpinResponse>() {
            @Override
            public void onResponse(Call<WsGiftSpinResponse> call, Response<WsGiftSpinResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGiftSpinResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetGiftBoxList(MPApiCallback<WsGiftBoxResponse> callback) {
        WsGiftRequest request = createBaseRequest(new WsGiftRequest(), Constants.WSCODE.WS_GIFT_BOX_LIST);
        WsGiftRequest.Request subRequest = request.new Request();
        subRequest.msisdn = getJidNumberFromSharedPrefs();
        subRequest.programeCode = metfoneGiftCode;
        subRequest.language = LANGUAGE;

        request.setWsRequest(subRequest);

        getKhHomeService().wsGiftBoxList(request.createRequestBody()).enqueue(new Callback<WsGiftBoxResponse>() {
            @Override
            public void onResponse(Call<WsGiftBoxResponse> call, Response<WsGiftBoxResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGiftBoxResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetRedeemHistoryList(MPApiCallback<WsGiftBoxResponse> callback) {
        WsGiftRequest request = createBaseRequest(new WsGiftRequest(), Constants.WSCODE.WS_REDEEM_HISTORY_LIST);
        WsGiftRequest.Request subRequest = request.new Request();
        subRequest.msisdn = getJidNumberFromSharedPrefs();
        subRequest.programeCode = metfoneGiftCode;
        subRequest.language = LANGUAGE;

        request.setWsRequest(subRequest);

        getKhHomeService().wsGiftBoxList(request.createRequestBody()).enqueue(new Callback<WsGiftBoxResponse>() {
            @Override
            public void onResponse(Call<WsGiftBoxResponse> call, Response<WsGiftBoxResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGiftBoxResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGiftRedeem(MPApiCallback<WsGiftSpinResponse> callback, String rewardId) {
        WsGiftRequest request = createBaseRequest(new WsGiftRequest(), Constants.WSCODE.WS_REDEEM_GIFT);
        WsGiftRequest.Request subRequest = request.new Request();
        subRequest.msisdn = getJidNumberFromSharedPrefs();
        subRequest.programeCode = metfoneGiftCode;
        subRequest.language = LANGUAGE;
        subRequest.giftRewardId = rewardId;

        request.setWsRequest(subRequest);

        getKhHomeService().wsGiftRedeem(request.createRequestBody()).enqueue(new Callback<WsGiftSpinResponse>() {
            @Override
            public void onResponse(Call<WsGiftSpinResponse> call, Response<WsGiftSpinResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGiftSpinResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    private String getJIdNumner() {
        if (reengAccountBusiness == null) {
            reengAccountBusiness = ApplicationController.self().getReengAccountBusiness();
        }
        if (reengAccountBusiness != null && reengAccountBusiness.getJidNumber() != null) {
            return reengAccountBusiness.getJidNumber().replace("+", "");
        }
        return "";
    }

    private String getJidNumberFromSharedPrefs() {
        return SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class).replace("+", "");
    }

    @SuppressLint("CheckResult")
    public void checkUserGamePermission(KhApiCallback<CheckUserGamePermissionResponse> callback) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        CheckUserGameRequest.WsRequest wsRequest = new CheckUserGameRequest.WsRequest(isdn, "LUCKY_LOYALTY");
        CheckUserGameRequest request =
                createBaseRequest(new CheckUserGameRequest(), Constants.WSCODE.WS_CHECK_USER_GAME);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<CheckUserGamePermissionResponse>> cryptoObservable = getKhHomeServiceRx().wsCheckUserGamePermission(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data.getData());
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    @SuppressLint("CheckResult")
    public void getGameAuthKey(KhApiCallback<GetAuthKeyReponse> callback) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        GetUserAuthKeyRequest.WsRequest wsRequest = new GetUserAuthKeyRequest.WsRequest(isdn, "LUCKY_LOYALTY");
        GetUserAuthKeyRequest request =
                createBaseRequest(new GetUserAuthKeyRequest(), Constants.WSCODE.WS_GET_AUTH_KEY_GAME);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<GetAuthKeyReponse>> cryptoObservable = getKhHomeServiceRx().wsGetAuthKey(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data.getData());
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    public void wsGetAllTeLeComGiftForFriend(KhApiCallback<KHBaseResponse<WsGetAllTeLeComGiftResponse>> callback) {
        WsGetAllTelecomGiftForFriendRequest.WsRequest wsRequest = new WsGetAllTelecomGiftForFriendRequest.WsRequest(
                LocaleManager.getLanguage(ApplicationController.self().getApplicationContext()));
        WsGetAllTeLeComGiftRequest request =
                createBaseRequest(new WsGetAllTeLeComGiftRequest(), Constants.WSCODE.WS_GET_ALL_TE_LE_COM_GIFT_FOR_FRIEND);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<WsGetAllTeLeComGiftResponse>> cryptoObservable =
                getKhHomeServiceRx().wsGetAllTeLeComGiftForFriendResponse(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    public void wsRedeemPointForFriend(KhApiCallback<KHBaseResponse<WsRedeemPointResponse>> callback,
                                       String desIsdn, int point, @NonNull DetailType type, String giftId) {
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        WsRedeemPointForFriendRequest.WsRequest wsRequest =
                new WsRedeemPointForFriendRequest.WsRequest(isdn, desIsdn, type, point, giftId);
        WsRedeemPointRequest request =
                createBaseRequest(new WsRedeemPointRequest(), Constants.WSCODE.WS_REDEEM_POINT_FOR_FRIEND);
        request.setWsRequest(wsRequest);
        Observable<KHBaseResponse<WsRedeemPointResponse>> cryptoObservable =
                getKhHomeServiceRx().wsRedeemPointForFriend(request.createRequestBody());
        cryptoObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data.isSuccessful()) {
                        callback.onSuccess(data);
                    } else {
                        callback.onFailed(data.getErrorCode(), data.getErrorMessage());
                    }
                }, error -> callback.onFailed(null, error.getMessage()));
    }

    public void wsGetHotMovie(MPApiCallback<WsGetMovieResponse> callback, WsGetHotMovieRequest.Request requestBody) {
        WsGetHotMovieRequest request = createBaseRequest(new WsGetHotMovieRequest(), Constants.WSCODE.WS_GET_HOT_MOVIE);
        requestBody.setLanguageCode(LANGUAGE);
        WsGetHotMovieRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetHotMovie(request.createRequestBody()).enqueue(new Callback<WsGetMovieResponse>() {
            @Override
            public void onResponse(Call<WsGetMovieResponse> call, Response<WsGetMovieResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetMovieResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetHomeMovieApp2(MPApiCallback<WsGetHomeMovieApp2Response> callback, WsGetHomeMovieApp2Request.Request requestBody) {
        WsGetHomeMovieApp2Request request = createBaseRequest(new WsGetHomeMovieApp2Request(), Constants.WSCODE.WS_GET_MOVIE_HOME_APP2);
        requestBody.setLanguage(LANGUAGE);
        WsGetHomeMovieApp2Request.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetHomeMovieApp2(request.createRequestBody()).enqueue(new Callback<WsGetHomeMovieApp2Response>() {
            @Override
            public void onResponse(Call<WsGetHomeMovieApp2Response> call, Response<WsGetHomeMovieApp2Response> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetHomeMovieApp2Response> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetListLogWatched(MPApiCallback<WsGetListLogWatchedResponse> callback, WsGetListLogWatchedRequest.Request requestBody) {
        WsGetListLogWatchedRequest request = createBaseRequest(new WsGetListLogWatchedRequest(), Constants.WSCODE.WS_GET_MOVIES_LIST_LOG_WATCHED);
        requestBody.setLanguage(LANGUAGE);
        WsGetListLogWatchedRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetListLogWatched(request.createRequestBody()).enqueue(new Callback<WsGetListLogWatchedResponse>() {
            @Override
            public void onResponse(Call<WsGetListLogWatchedResponse> call, Response<WsGetListLogWatchedResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetListLogWatchedResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetListMovieForUser(MPApiCallback<WsGetListFilmForUserResponse> callback, WsGetListFilmForUserRequest.Request requestBody) {
        WsGetListFilmForUserRequest request = createBaseRequest(new WsGetListFilmForUserRequest(), Constants.WSCODE.WS_GET_LIST_FILM_FOR_USER);
        requestBody.setLanguage(LANGUAGE);
        WsGetListFilmForUserRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetListMovieForUser(request.createRequestBody()).enqueue(new Callback<WsGetListFilmForUserResponse>() {
            @Override
            public void onResponse(Call<WsGetListFilmForUserResponse> call, Response<WsGetListFilmForUserResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetListFilmForUserResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetTopMoviesCategory(MPApiCallback<WsGetMovieResponse> callback, WsGetTopMovieCategoryRequest.Request requestBody) {
        WsGetTopMovieCategoryRequest request = createBaseRequest(new WsGetTopMovieCategoryRequest(), Constants.WSCODE.WS_GET_TOP_MOVIE_CATEGORY);
        requestBody.language = LANGUAGE;
        WsGetTopMovieCategoryRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetTopMovieCategory(request.createRequestBody()).enqueue(new Callback<WsGetMovieResponse>() {
            @Override
            public void onResponse(Call<WsGetMovieResponse> call, Response<WsGetMovieResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetMovieResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetMovieDetail(MPApiCallback<WsGetMovieDetailResponse> callback, WsGetMovieDetailRequest.Request requestBody) {
        WsGetMovieDetailRequest request = createBaseRequest(new WsGetMovieDetailRequest(), Constants.WSCODE.WS_GET_MOVIE_DETAIL);
        requestBody.setLanguage(LANGUAGE);
        WsGetMovieDetailRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetMovieDetail(request.createRequestBody()).enqueue(new Callback<WsGetMovieDetailResponse>() {
            @Override
            public void onResponse(Call<WsGetMovieDetailResponse> call, Response<WsGetMovieDetailResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetMovieDetailResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetMovieCategoryV2(MPApiCallback<WsGetMovieCategoryV2Response> callback, WsGetMovieCategoryV2Request.Request requestBody) {
        WsGetMovieCategoryV2Request request = createBaseRequest(new WsGetMovieCategoryV2Request(), Constants.WSCODE.WS_GET_MOVIE_CATEGORY_V2);
        requestBody.language = LANGUAGE;
        WsGetMovieCategoryV2Request.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetMovieCategoryV2(request.createRequestBody()).enqueue(new Callback<WsGetMovieCategoryV2Response>() {
            @Override
            public void onResponse(Call<WsGetMovieCategoryV2Response> call, Response<WsGetMovieCategoryV2Response> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetMovieCategoryV2Response> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetMovieCountry(MPApiCallback<WsGetMovieCountryResponse> callback, WsGetMovieCountryRequest.Request requestBody) {
        WsGetMovieCountryRequest request = createBaseRequest(new WsGetMovieCountryRequest(), Constants.WSCODE.WS_GET_MOVIE_COUNTRY);
        requestBody.language = LANGUAGE;
        WsGetMovieCountryRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetMovieCountry(request.createRequestBody()).enqueue(new Callback<WsGetMovieCountryResponse>() {
            @Override
            public void onResponse(Call<WsGetMovieCountryResponse> call, Response<WsGetMovieCountryResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetMovieCountryResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetMovieRelated(MPApiCallback<WsMovieGetRelatedResponse> callback, WsGetMovieRelatedRequest.Request requestBody) {
        WsGetMovieRelatedRequest request = createBaseRequest(new WsGetMovieRelatedRequest(), Constants.WSCODE.WS_GET_MOVIE_RELATED);
        requestBody.setLanguage(LANGUAGE);
        WsGetMovieRelatedRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetMovieRelated(request.createRequestBody()).enqueue(new Callback<WsMovieGetRelatedResponse>() {
            @Override
            public void onResponse(Call<WsMovieGetRelatedResponse> call, Response<WsMovieGetRelatedResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsMovieGetRelatedResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetMovieListFilmOfCategory(MPApiCallback<WsMovieGetListFilmOfCategoryResponse> callback, WsMovieGetListFilmOfCategoryRequest.Request requestBody) {
        WsMovieGetListFilmOfCategoryRequest request = createBaseRequest(new WsMovieGetListFilmOfCategoryRequest(), Constants.WSCODE.WS_GET_MOVIE_LIST_FILM_OF_CATEGORY);
        requestBody.setLanguage(LANGUAGE);
        WsMovieGetListFilmOfCategoryRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetMovieListOfFilmCategory(request.createRequestBody()).enqueue(new Callback<WsMovieGetListFilmOfCategoryResponse>() {
            @Override
            public void onResponse(Call<WsMovieGetListFilmOfCategoryResponse> call, Response<WsMovieGetListFilmOfCategoryResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsMovieGetListFilmOfCategoryResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetMovieListGroupDetail(MPApiCallback<WsMovieGetListGroupDetailResponse> callback, WsMovieGetListGroupDetailRequest.Request requestBody) {
        WsMovieGetListGroupDetailRequest request = createBaseRequest(new WsMovieGetListGroupDetailRequest(), Constants.WSCODE.WS_GET_MOVIE_LIST_GROUP_DETAIL);
        requestBody.setLanguage(LANGUAGE);
        WsMovieGetListGroupDetailRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetMovieListGroupDetail(request.createRequestBody()).enqueue(new Callback<WsMovieGetListGroupDetailResponse>() {
            @Override
            public void onResponse(Call<WsMovieGetListGroupDetailResponse> call, Response<WsMovieGetListGroupDetailResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsMovieGetListGroupDetailResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetMovieCategoryByIds(MPApiCallback<WsMovieGetCategoryByIdsResponse> callback, WsMovieGetCategoryByIdsRequest.Request requestBody) {
        WsMovieGetCategoryByIdsRequest request = createBaseRequest(new WsMovieGetCategoryByIdsRequest(), Constants.WSCODE.WS_GET_MOVIE_CATEGORY_BY_IDS);
        requestBody.setLanguage(LANGUAGE);
        WsMovieGetCategoryByIdsRequest.Request subRequest = requestBody;
        request.setWsRequest(subRequest);
        getKhHomeServiceMovie().wsGetMovieCategoryByIds(request.createRequestBody()).enqueue(new Callback<WsMovieGetCategoryByIdsResponse>() {
            @Override
            public void onResponse(Call<WsMovieGetCategoryByIdsResponse> call, Response<WsMovieGetCategoryByIdsResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsMovieGetCategoryByIdsResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsAppLog(MPApiCallback<BaseResponse> callback, String wsCode) {
        WsLogAppRequest request = createBaseRequest(new WsLogAppRequest(), Constants.WSCODE.WS_LOG_APP);
        String isdn = "";
        if (mCamIdUserBusiness.getUser() != null) {
            isdn = String.valueOf(mCamIdUserBusiness.getUser().getUserId());
        }
        WsLogAppRequest.WsRequest subRequest = new WsLogAppRequest.WsRequest(wsCode, isdn);
        request.setWsRequest(subRequest);

        getKhHomeService().wsLogApp(request.createRequestBody()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void logApp(String wsCode) {
        wsAppLog(new MPApiCallback<BaseResponse>() {
            @Override
            public void onResponse(retrofit2.Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    Log.e("WS_LOG_APP", String.format("wsLogApp => succeed [%s]", wsCode));
                }
            }

            @Override
            public void onError(Throwable error) {
                Log.e("WS_LOG_APP", "wsLogApp => failed");
            }
        }, wsCode);
    }

    public void getSliderFilm(MPApiCallback<WsSliderFilmResponse> callback) {
        WsSliderFilmRequest request = createBaseRequest(new WsSliderFilmRequest(), Constants.WSCODE.WS_SLIDER_FILM);
        WsSliderFilmRequest.WsRequest subRequest = new WsSliderFilmRequest.WsRequest();
        subRequest.language = LANGUAGE;
        request.setWsRequest(subRequest);
        getKhHomeService().wsGetSliderFilm(request.createRequestBody()).enqueue(new Callback<WsSliderFilmResponse>() {
            @Override
            public void onResponse(Call<WsSliderFilmResponse> call, Response<WsSliderFilmResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<WsSliderFilmResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void checkShowPopup(String phone, String deviceId, MPApiCallback<WsPopupTetResponse> callback) {
        WsCheckShowPopupRequest request = createBaseRequest(new WsCheckShowPopupRequest(),
                Constants.WSCODE.WS_CHECK_SHOW_POPUP_TET);
        UserInfo user = userInfoBusiness.getUser();
        String camId = "";
        if (user != null) {
            camId = String.valueOf(user.getUser_id());
        }
        WsCheckShowPopupRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(phone);
        subRequest.setCamId(camId);
        subRequest.setLanguage(LANGUAGE);
        subRequest.setDeviceId(deviceId);
        request.setWsRequest(subRequest);
        getKhHomeService().wsShowPopupTet(request.createRequestBody()).enqueue(new Callback<WsPopupTetResponse>() {
            @Override
            public void onResponse(Call<WsPopupTetResponse> call, Response<WsPopupTetResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsPopupTetResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void checkGetGiftTet(String phone, String deviceId, MPApiCallback<WsGetGiftResponse> callback) {
        WsCheckShowPopupRequest request = createBaseRequest(new WsCheckShowPopupRequest(),
                Constants.WSCODE.WS_CHECK_GET_GIFT_TET);
        UserInfo user = userInfoBusiness.getUser();
        String camId = "";
        if (user != null) {
            camId = String.valueOf(user.getUser_id());
        }
        WsCheckShowPopupRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(phone);
        subRequest.setLanguage(LANGUAGE);
        subRequest.setCamId(camId);
        subRequest.setDeviceId(deviceId);
        request.setWsRequest(subRequest);
        getKhHomeService().checkGetGiftTet(request.createRequestBody()).enqueue(new Callback<WsGetGiftResponse>() {
            @Override
            public void onResponse(Call<WsGetGiftResponse> call, Response<WsGetGiftResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetGiftResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void redeemLunarGift(String isdn, MPApiCallback<WsRedeemGiftTetResponse> callback) {
        WsRedeemGiftTetRequest request = createBaseRequest(new WsRedeemGiftTetRequest(),
                Constants.WSCODE.WS_REDEEM_GIFT_TET);
        WsRedeemGiftTetRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(isdn);
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getKhHomeService().redeemGiftTet(request.createRequestBody()).enqueue(new Callback<WsRedeemGiftTetResponse>() {
            @Override
            public void onResponse(Call<WsRedeemGiftTetResponse> call, Response<WsRedeemGiftTetResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsRedeemGiftTetResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }
}

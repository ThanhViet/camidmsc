package com.metfone.selfcare.module.keeng.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.PlayListModel;

import java.io.Serializable;

public class RestPlaylist extends AbsResultData implements Serializable {

    private static final long serialVersionUID = -882945095338822569L;

    @SerializedName("data")
    private PlayListModel data;

    public PlayListModel getData() {
        return data;
    }

    public void setData(PlayListModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "[data=" + data + "] error " + getError();
    }

}
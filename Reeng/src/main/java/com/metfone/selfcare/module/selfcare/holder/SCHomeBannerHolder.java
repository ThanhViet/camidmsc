package com.metfone.selfcare.module.selfcare.holder;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.rd.PageIndicatorView;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.adapter.SCBannerAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCBanner;

import java.util.ArrayList;

public class SCHomeBannerHolder extends BaseViewHolder {
    SCBannerAdapter bannerAdapter;
    PageIndicatorView pageIndicatorBannerView;
    ViewPager bannerViewPager;
    View layout_banner;
    ArrayList<SCBanner> bannerList = new ArrayList<>();

    public SCHomeBannerHolder(Context mContext, View view, final AbsInterface.OnSCHomeListener listener) {
        super(view);

        layout_banner = view.findViewById(R.id.layout_banner);
        pageIndicatorBannerView = view.findViewById(R.id.pageIndicatorBannerView);
        bannerViewPager = view.findViewById(R.id.viewpagerSliding);

        bannerAdapter = new SCBannerAdapter(mContext);
        bannerAdapter.setOnClickListener(listener);
        bannerViewPager.setAdapter(bannerAdapter);
        bannerViewPager.setOffscreenPageLimit(3);

        pageIndicatorBannerView.setViewPager(bannerViewPager);
    }

    public void setData(ArrayList<SCBanner> bannerList) {
        this.bannerList = bannerList;
        //Load data
        if (bannerList.size() > 0) {
            layout_banner.setVisibility(View.VISIBLE);
            bannerAdapter.setDatas(bannerList);
            bannerAdapter.notifyDataSetChanged();

            pageIndicatorBannerView.setCount(bannerList.size());
            pageIndicatorBannerView.setViewPager(bannerViewPager);
        } else {
            layout_banner.setVisibility(View.GONE);

//            //fake
//            layout_banner.setVisibility(View.VISIBLE);
//
//            bannerList.add(new SCBanner("1", "banner1", "", ""));
//            bannerList.add(new SCBanner("2", "banner2", "", ""));
//            bannerList.add(new SCBanner("3", "banner3", "", ""));
//            bannerAdapter.setDatas(bannerList);
//            bannerAdapter.notifyDataSetChanged();
//
//            pageIndicatorBannerView.setCount(bannerList.size());
//            pageIndicatorBannerView.setViewPager(bannerViewPager);
        }
    }
}

package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.module.home_kh.service.LoadCinemaService;
import com.metfone.selfcare.module.movie.fragment.MoviePagerFragment;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_SEND_TOKEN_SUCCESS;

public class ChangeLanguageKhDialogFragment extends DialogFragment {

//    @BindView(R.id.list_group)
//    LinearLayout listGroup;

    @BindView(R.id.enCheck)
    View enCheck;

    @BindView(R.id.khCheck)
    View khCheck;

    @BindView(R.id.tvKhmer)
    AppCompatTextView tvKhmer;

    @BindView(R.id.tvEng)
    AppCompatTextView tvEng;

    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView tvTitle;

    private ArrayList<String> keys;
    private ArrayList<String> values;

    private ApplicationController applicationController;
    private Unbinder unbinder;

    private String[] languageEng;
    private String[] languageKhmer;

    public static ChangeLanguageKhDialogFragment newInstance() {
        Bundle args = new Bundle();
        ChangeLanguageKhDialogFragment fragment = new ChangeLanguageKhDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFullscreenV5);
        applicationController = ApplicationController.self();
        if (getArguments() != null) {
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_change_language_kh, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.KH_DialogAnimation;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getDialog().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getDialog().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.kh_background));
        }
        drawDetail();
    }


    private void drawDetail() {
        tvTitle.setText(R.string.kh_language_title);
        String currentLanguage = LocaleManager.getLanguage(getContext());

        languageEng = getResources().getString(R.string.kh_language_english).split(";", 2);
        languageKhmer = getResources().getString(R.string.kh_language_cambodia).split(";", 2);
        tvEng.setText(languageEng[0]);
        tvKhmer.setText(languageKhmer[0]);

        if (currentLanguage.equals(languageKhmer[1])) {
            enCheck.setVisibility(View.INVISIBLE);
            khCheck.setVisibility(View.VISIBLE);
        } else {
            khCheck.setVisibility(View.INVISIBLE);
            enCheck.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.icBackToolbar, R.id.layout_cambodia, R.id.layout_english})
    public void onClickView(View view) {
        String currentLanguage = LocaleManager.getLanguage(getContext());
        switch (view.getId()) {
            case R.id.icBackToolbar:
                dismiss();
                break;
            case R.id.layout_cambodia:
                if (languageKhmer != null && !currentLanguage.equals(languageKhmer[1])) {
                    changeLanguage(languageKhmer[1]);
                }
                break;
            case R.id.layout_english:
                if (languageEng != null && !currentLanguage.equals(languageEng[1])) {
                    changeLanguage(languageEng[1]);

                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        unbinder = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void changeLanguage(String language) {
        LocaleManager.setNewLocale(getContext(), language);
        SharedPrefs.getInstance().put(PREF_SEND_TOKEN_SUCCESS, false);
        DeepLinkHelper.uriDeepLink = null;
        Intent intent = new Intent(getContext(), HomeActivity.class);
        intent.putExtra(MoviePagerFragment.FROM_CHANGE_LANGUAGE,true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
//        .finishAffinity();

//        Intent intent = new Intent(getContext(), HomeActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        int mPendingIntentId = 123456;
//        PendingIntent mPendingIntent = PendingIntent.getActivity(getContext(), mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
//        AlarmManager mgr = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
//        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
//        System.exit(0);
    }
}

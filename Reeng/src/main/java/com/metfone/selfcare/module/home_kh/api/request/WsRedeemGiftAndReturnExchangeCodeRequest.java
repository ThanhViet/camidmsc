package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Data;

public class WsRedeemGiftAndReturnExchangeCodeRequest extends BaseRequest<WsRedeemGiftAndReturnExchangeCodeRequest.Request> {
    public static class Request {
    }
    public static class WsRequest extends Request {
        @SerializedName("giftId")
        String giftId;
        @SerializedName("receivePhone")
        String receivePhone;
        @SerializedName("districtId")
        String districtId;
        @SerializedName("isdn")
        String isdn;
        @SerializedName("receiveAddress")
        String receiveAddress;
        @SerializedName("rankId")
        String rankId;
        @SerializedName("receiveDate")
        String receiveDate;
        @SerializedName("language")
        String language;
        @SerializedName("cityId")
        String cityId;
        @SerializedName("villageId")
        String villageId;
        @SerializedName("custId")
        public String custId;

        public WsRequest(String giftId, String receivePhone, String districtId, String isdn,
                         String receiveAddress, String rankId, String receiveDate, String language,
                         String cityId, String villageId) {
            this.giftId = giftId;
            this.receivePhone = receivePhone;
            this.districtId = districtId;
            this.isdn = isdn;
            this.receiveAddress = receiveAddress;
            this.rankId = rankId;
            this.receiveDate = receiveDate;
            this.language = language;
            this.cityId = cityId;
            this.villageId = villageId;
        }

        public WsRequest(String giftId, String receivePhone, String isdn, String rankId) {
            this.giftId = giftId;
            this.receivePhone = receivePhone;
            this.isdn = isdn;
            this.rankId = rankId;
            this.districtId = "";
            this.receiveAddress = "";
            this.receiveDate = "";
            this.language = "en";
            this.cityId = "";
            this.villageId = "";
        }
    }
}

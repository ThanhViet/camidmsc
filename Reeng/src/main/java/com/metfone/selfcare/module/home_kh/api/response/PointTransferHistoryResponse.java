package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.PointTransferHistory;

import java.util.List;

public class PointTransferHistoryResponse {
    @SerializedName("code")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("listPointTransferHistory")
    @Expose
    List<PointTransferHistory> listPointTransferHistory;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PointTransferHistory> getListPointTransferHistory() {
        return listPointTransferHistory;
    }

    public void setListPointTransferHistory(List<PointTransferHistory> listPointTransferHistory) {
        this.listPointTransferHistory = listPointTransferHistory;
    }
}

package com.metfone.selfcare.module.selfcare.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.adapter.SCAccountDetailAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCAccountDetail;
import com.metfone.selfcare.module.selfcare.model.SCPackage;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCAccountData;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCPackage;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;

public class SCAccountDetailInfoFragment extends BaseFragment implements AbsInterface.OnPackageListener, SwipeRefreshLayout.OnRefreshListener {

    private LoadingViewSC loadingView;
    private RecyclerView recyclerView;
    private SCAccountDetailAdapter adapter;
    private LinearLayoutManager layoutManager;

    private SCAccountDetail data;

    public static SCAccountDetailInfoFragment newInstance() {
        SCAccountDetailInfoFragment fragment = new SCAccountDetailInfoFragment();
        return fragment;
    }

    @Override
    public String getName() {
        return "SCAccountDetailInfoFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_account_info;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

        loadData();

        return view;
    }

    private void initView(View view) {
        layout_refresh = view.findViewById(R.id.refresh);
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.sc_primary));
            layout_refresh.setOnRefreshListener(this);
        }

        loadingView = view.findViewById(R.id.loading_view);
        loadingView.setLoadingErrorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login lai
                /*Intent intent = new Intent(getActivity(), SCAccountActivity.class);
                mActivity.startActivity(intent);*/
//                LoginMyIDActivity.startActivity((BaseSlidingFragmentActivity) getActivity());
                mActivity.loginFromAnonymous();
            }
        });

        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCAccountDetailAdapter(mActivity, this);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);
    }

    private void loadData() {
        if (!isRefresh)
            loadingView.loadBegin();

        data = new SCAccountDetail();
        //Load account detail info
        final WSSCRestful restful = new WSSCRestful(mActivity);

        restful.getAccountInfo(new Response.Listener<RestSCAccountData>() {
                                   @Override
                                   public void onResponse(RestSCAccountData result) {
//                                       super.onResponse(result);
                                       hideRefresh();
                                       if (result != null) {
                                           if (result.getStatus() == 200 && result.getData() != null) {
                                               loadingView.loadFinish();
                                               if (result.getData().size() > 0) {
                                                   data.getAccountDataInfo().clear();
                                                   data.setAccountDataInfo(result.getData());

                                                   adapter.setItemsList(data);
                                                   adapter.notifyDataSetChanged();
                                               } else {
                                                   loadingView.loadEmpty();
                                               }
                                           } else if (result.getStatus() == 401 || result.getStatus() == 403) {
                                               //Login lai
                                               loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                                           } else {
                                               //Fail
                                               loadingView.loadError();
                                           }
                                       }
                                   }
                               },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        hideRefresh();
                        if (volleyError != null && volleyError.networkResponse != null) {
                            int errorCode = volleyError.networkResponse.statusCode;
                            if (errorCode == 401 || errorCode == 403) {
                                //Login lai
                                loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                            } else {
                                //Fail
                                loadingView.loadError();
                            }
                        } else {
                            //Fail
                            loadingView.loadError();
                        }
                    }
                });

        restful.getRegisterService(new Response.Listener<RestSCPackage>() {
                                       @Override
                                       public void onResponse(RestSCPackage result) {
//                                           super.onResponse(result);

                                           if (result != null && result.getData() != null && result.getData().getData() != null && result.getData().getData().size() > 0) {
                                               data.getSevicesList().clear();
                                               data.setSevicesList(result.getData().getData());

                                               adapter.setItemsList(data);
                                               adapter.notifyDataSetChanged();
                                           }
                                       }
                                   },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });

        restful.getRegisterPackage(new Response.Listener<RestSCPackage>() {
                                       @Override
                                       public void onResponse(RestSCPackage result) {
//                                           super.onResponse(result);

                                           if (result != null && result.getData() != null && result.getData().getData() != null && result.getData().getData().size() > 0) {
                                               data.getPackageList().clear();
                                               data.setPackageList(result.getData().getData());

                                               adapter.setItemsList(data);
                                               adapter.notifyDataSetChanged();
                                           }
                                       }
                                   },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
    }

    @Override
    public void onPackageClick(SCPackage item) {
        item.setRegister(true);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        bundle.putInt(SCConstants.PREFERENCE.KEY_FROM_SOURCE, SCConstants.SCREEN_TYPE.TYPE_SERVICES);
        ((TabSelfCareActivity) mActivity).gotoPackageDetail(bundle);
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        loadData();
    }
}

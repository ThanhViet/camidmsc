package com.metfone.selfcare.module.movienew.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.holder.CategoryHolder;
import com.metfone.selfcare.module.movienew.model.Category;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CategoryFragmentNew extends BaseFragment implements  OnInternetChangedListener, ItemViewClickListener {

    private Unbinder unbinder;

    @BindView(R.id.recycler_genres)
    RecyclerView genresList;
    @BindView(R.id.titleLayout)
    AppCompatTextView titleLayout;
    @BindView(R.id.icBack)
    AppCompatImageView btnBack;
    private MovieApi movieApi;
    ArrayList<Category> mCategories = new ArrayList<>();
    private com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter mCategoryAdapter;

    public MovieApi getMovieApi() {
        if (movieApi == null) movieApi = new MovieApi();
        return movieApi;
    }

    public CategoryFragmentNew() {
        // Required empty public constructor
    }

    public static CategoryFragmentNew newInstance() {

        return new CategoryFragmentNew();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.category_fragment_new, container, false);
        unbinder = ButterKnife.bind(this, view);
        CategoryHolder categoryHolder = new CategoryHolder(getActivity().getWindow().getDecorView().getRootView(), this);
        mCategoryAdapter = new com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter(mCategories,getContext(),R.layout.item_category_new,categoryHolder);
        genresList.setLayoutManager(new GridLayoutManager(getContext(),2));
        genresList.setAdapter(mCategoryAdapter);
//        CustomGridLayoutManager layoutManager = new CustomGridLayoutManager(activity, 3);
//        BaseAdapter.setupGridRecycler(activity, genresList, layoutManager, genresAdapter, 2, R.dimen.cinema_card_spacing, true);

        listener();
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (canLazyLoad()) {
            new Handler().postDelayed(() -> loadData(), Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData();
        }
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad()) {
            loadData();
        }
    }

    private void loadData() {
        getMovieApi().getCategoryV2(new ApiCallbackV2<ArrayList<Category>>() {
            @Override
            public void onSuccess(String msg, ArrayList<Category> result) {
                if (mCategories == null) mCategories = new ArrayList<>();
                if (Utilities.notEmpty(result)) {
                    mCategories.addAll(result);
                }
                if (mCategoryAdapter != null) mCategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String s) {
                if (mCategories == null) mCategories = new ArrayList<>();
                if (mCategoryAdapter != null) mCategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onComplete() {
                isDataInitiated = true;
            }
        });
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

//    @Override
//    public void onClick(Object item, int position, boolean isChecked) {
//
//        if (item instanceof Category) {
//                String categoryName = ((Category) genresModels.get(position)).getCategoryName();
//                ActionCategoryFragment actionCategoryFragment = new ActionCategoryFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString("categoryName", categoryName);
//                actionCategoryFragment.setArguments(bundle);
//                addFragment(R.id.frameAction, actionCategoryFragment, true);
//        }
//    }

    private void listener() {
        titleLayout.setText(getString(R.string.category_movie));
        btnBack.setOnClickListener(v -> {
            popBackStackFragment();
        });
    }


    @Override
    public void onItemViewClickListener(int position, List<?> list) {
        if (list.get(position) instanceof Category) {
            String categoryName = ((Category) list.get(position)).getCategoryName();
            String categoryNameId = ((Category) list.get(position)).getCategoryId();
            ActionCategoryFragment actionCategoryFragment = ActionCategoryFragment.newInstance(categoryNameId,categoryName,ActionCategoryFragment.CATEGORY);
            addFragment(R.id.frameAction, actionCategoryFragment, true);
        }
    }
}
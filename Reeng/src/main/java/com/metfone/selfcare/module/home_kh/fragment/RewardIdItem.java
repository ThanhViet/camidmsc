package com.metfone.selfcare.module.home_kh.fragment;

public class RewardIdItem {
    int id;
    String url;
    int discount;
    long time;
    long point;

    public RewardIdItem(int id, String url, int discount, long time, long point) {
        this.id = id;
        this.url = url;
        this.discount = discount;
        this.time = time;
        this.point = point;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }
}

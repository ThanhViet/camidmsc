package com.metfone.selfcare.module.keeng.base;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.CategoryModel;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.keeng.widget.GridSpacingItemDecoration;
import com.metfone.selfcare.module.keeng.widget.LoadingView;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomDialog;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomSheetAdapter;

import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerFragment<T> extends BaseFragment implements OnRefreshListener,
        BaseListener.OnClickMedia, BaseListener.OnClickBottomSheet {

    //    public static final int STATE_LOADING = 1;
//    public static final int STATE_ERROR = 2;
//    public static final int STATE_NO_RESULT = 3;
//    public static final int STATE_END_LIST = 4;
//    public static final int STATE_COMPLETED_PAGE = 5;
    public static final int MAX_ERROR_RETRY = 1;
    // ---------------------------------------------------------//
    public static final int FOLLOW = 1;
    public static final int UNFOLLOW = 0;
    protected int errorCount = 0;
    // phuc vu dong bo hien thi danh ba
    protected SwipeRefreshLayout refreshLayout;
    protected RecyclerView recyclerView;
    protected LoadingView loadingView;
    // cac bien nay tuyet doi khong dc sua.
    protected int numPerPage = KeengApi.NUM_PER_PAGE;
    protected int currentPage = 1;
    protected boolean canLoadMore = true;
    protected boolean isRefresh = true;
    protected boolean isLoadingMore = false;
    protected boolean isLoading;
    protected LinearLayoutManager mLayoutManager;
    //-----------------------------
    BottomDialog mBottomSheetFirst;
    private List<T> datas;
    private BaseAdapterRecyclerView mAdapter;

    public boolean recyclerScrollToPosition(int position) {
        if (mLayoutManager != null)
            try {
                mLayoutManager.scrollToPositionWithOffset(position, 0);
                return true;
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        return false;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof BaseActivity)
            mActivity = (BaseActivity) activity;
        if (activity instanceof TabKeengActivity) {
            mActivity = (TabKeengActivity) activity;
        }
        if (datas == null)
            datas = new ArrayList<>();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_base;
    }

    @SuppressWarnings("NullableProblems")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        refreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        loadingView = view.findViewById(R.id.loadingView);
        recyclerView = view.findViewById(R.id.recycler_view);
        toolbar = view.findViewById(R.id.toolbar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (refreshLayout != null && getActivity() != null) {
            refreshLayout.setOnRefreshListener(this);
            refreshLayout.setColorSchemeColors(getActivity().getResources().getColor(R.color.keeng_primary));
        }
    }

    public List<T> getDatas() {
        if (datas == null) {
            datas = new ArrayList<>();
        }
        return datas;
    }

    public void setDatas(List<T> data) {
        if (this.datas == null) {
            this.datas = new ArrayList<>();
        }
        this.datas.addAll(data);
    }

    public void clearData() {
        if (datas != null && datas.size() > 0) {
            datas.clear();
        } else if (datas == null) {
            datas = new ArrayList<>();
        }
    }

    public void setAdapter(BaseAdapterRecyclerView adapter) {
        this.mAdapter = adapter;
    }

    public void setupRecycler(BaseAdapterRecyclerView adapter) {
        this.mAdapter = adapter;
        mLayoutManager = new CustomLinearLayoutManager(mActivity);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(this.mAdapter);
        this.mAdapter.setOnclickListener(this);
        mAdapter.setOnCheckEnd(recyclerView, this);
//        mAdapter.notifyDataSetChanged();
    }

    public void setupGridRecycler(BaseAdapterRecyclerView adapter, int spanCount, int resIdSpacing, boolean includeEdge) {
        this.mAdapter = adapter;
        mLayoutManager = new CustomGridLayoutManager(mActivity, spanCount);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, getResources().getDimensionPixelOffset(resIdSpacing), includeEdge));
        recyclerView.setAdapter(this.mAdapter);
        this.mAdapter.setOnclickListener(this);
        this.mAdapter.setOnCheckEnd(recyclerView, this);
//        mAdapter.notifyDataSetChanged();
    }

    public void setupHorizontalRecycler(BaseAdapterRecyclerView adapter, int divider) {
        this.mAdapter = adapter;
        mLayoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        if (divider > 0)
            recyclerView.addItemDecoration(new DividerItemDecoration(mActivity, divider));
        recyclerView.setAdapter(this.mAdapter);
        this.mAdapter.setOnclickListener(this);
        this.mAdapter.setOnCheckEnd(recyclerView, this);
//        mAdapter.notifyDataSetChanged();
    }

    public void setupRecycler(BaseAdapterRecyclerView adapter, int divider) {
        if (divider > 0)
            recyclerView.addItemDecoration(new DividerItemDecoration(mActivity, divider));
        setupRecycler(adapter);
    }

    // ------------------Show/Hide Loading----------------------------------//
    public void refreshed() {
        if (isRefresh) {
            clearData();
            currentPage = 1;
            isRefresh = false;
        }
    }

    public void loadMore() {
        try {
            if (isLoadingMore || isLoading) {
                refreshLayout.setRefreshing(false);
            } else {
                isLoadingMore = true;
//                getDatas().add(null);
//                if (mAdapter != null)
//                    mAdapter.notifyItemInserted(mAdapter.getItemCount() - 1);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public void loadMored() {
        try {
            if (isLoadingMore) {
                isLoadingMore = false;
//                if (getDatas().size() > 0) {
//                    getDatas().remove(getDatas().size() - 1);
//                    if (mAdapter != null)
//                        mAdapter.notifyItemRemoved(mAdapter.getItemCount() - 1);
//                }
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public void loadingBegin() {
        if (loadingView != null) {
            loadingView.loadBegin();
        }
    }

    public void loadingFinish() {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (loadingView != null) {
            loadingView.loadFinish();
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void loadingError(View.OnClickListener paramOnClickListener) {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getDatas().size() > 0) {
            if (loadingView != null)
                loadingView.loadFinish();
            return;
        }
        if (loadingView != null) {
            loadingView.loadError();
            loadingView.setLoadingErrorListener(paramOnClickListener);
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void loadingEmpty() {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getDatas().size() > 0) {
            if (loadingView != null)
                loadingView.loadFinish();
            return;
        }
        if (loadingView != null) {
            loadingView.loadEmpty();
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void loadingListSingerEmpty(int songType) {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getDatas().size() > 0) {
            if (loadingView != null)
                loadingView.loadFinish();
            return;
        }
        if (loadingView != null) {
            loadingView.loadListSingerEmpty(songType);
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void loadingEmptySearch() {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getDatas().size() > 0) {
            if (loadingView != null)
                loadingView.loadFinish();
            return;
        }
        if (loadingView != null) {
            loadingView.loadEmptySearch();
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void loadingEmpty(String str) {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getDatas().size() > 0) {
            if (loadingView != null)
                loadingView.loadFinish();
            return;
        }
        if (loadingView != null) {
            loadingView.loadEmpty(str);
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void loadingError(String error, View.OnClickListener paramOnClickListener) {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getDatas().size() > 0) {
            if (loadingView != null)
                loadingView.loadFinish();
            return;
        }
        if (loadingView != null) {
            loadingView.loadError(error);
            loadingView.setLoadingErrorListener(paramOnClickListener);
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void loadingLogin(String str, View.OnClickListener paramOnClickListener) {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (loadingView != null) {
            loadingView.loadLogin(str);
            loadingView.setBtnRetryListener(paramOnClickListener);
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void loadingNotice(String notice) {
        if (refreshLayout != null && refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        if (getDatas().size() > 0) {
            if (loadingView != null)
                loadingView.loadFinish();
            return;
        }
        if (loadingView != null) {
            loadingView.setBackgroundColor(mActivity.getResources().getColor(R.color.transparent));
            loadingView.notice(notice);
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    // ----------------Xu ly click----------------
    @Override
    public void onMediaClick(View v, int position) {
        //
    }

    @Override
    public void onScrollEnd(boolean flag) {
        //
    }

    @Override
    public void onMediaExpandClick(View v, int position) {
        //
    }

    @Override
    public void onMediaClick(View v, int parent, int position) {
        //
    }

    @Override
    public void onMediaExpandClick(View v, int parent, int position) {
        //
    }

    @Override
    public void onTopicClick(View v, int position) {
        //
    }

    @Override
    public void onRefresh() {
        //
        isRefresh = true;
        canLoadMore = true;
        currentPage = 1;
    }

    @Override
    public void onLongClick(View v, int position) {
        //
    }

    protected void checkLoadMore(List<T> result) {
        canLoadMore = result != null && !result.isEmpty();
    }

    protected void checkLoadMoreAbsolute(List<T> result) {
        canLoadMore = result != null && result.size() >= numPerPage;
    }

    protected void dismissBottomSheetFirst() {
        if (mBottomSheetFirst != null)
            mBottomSheetFirst.dismiss();
    }

    public void showPopupMore(List<CategoryModel> datas, final AllModel item) {
        dismissBottomSheetFirst();
        if (getActivity() == null || item == null || datas == null || datas.isEmpty())
            return;
        mBottomSheetFirst = new BottomDialog(getActivity());
        View sheetView = getActivity().getLayoutInflater().inflate(R.layout.layout_bottom_sheet_category, null);
        TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
        TextView tvSinger = sheetView.findViewById(R.id.tvSinger);
        ImageView image = sheetView.findViewById(R.id.image);
        RecyclerView mRecycler = sheetView.findViewById(R.id.recycler);

        tvTitle.setText(item.getName());
        tvSinger.setText(item.getSinger());
        ImageBusiness.setAlbum(image, item.getImage());
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new CustomLinearLayoutManager(getActivity()));
        BottomSheetAdapter adapter = new BottomSheetAdapter(getActivity(), datas, TAG, this);
        mRecycler.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
        mBottomSheetFirst.setContentView(sheetView);
        mBottomSheetFirst.show();
    }

    @Override
    public void onClickSheet(CategoryModel item) {

    }

    @Override
    public void onDestroy() {
        if (mAdapter != null) {
            mAdapter.setOnclickListener(null);
        }
        super.onDestroy();
    }
}
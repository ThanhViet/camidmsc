/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/12
 *
 */

package com.metfone.selfcare.module.video.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.video.fragment.CategoryDetailFragment;
import com.metfone.selfcare.module.video.model.TabModel;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.line)
    View line;

    TabModel tabInfo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_category);
        ButterKnife.bind(this);
        Serializable serializable = getIntent().getSerializableExtra(Constants.KEY_DATA);
        if (serializable instanceof TabModel) {
            tabInfo = (TabModel) serializable;
        }
        if (tabInfo == null) {
            finish();
            return;
        }
        tvTitle.setText(tabInfo.getTitle());
        CategoryDetailFragment fragment = CategoryDetailFragment.newInstance(tabInfo);
        executeFragmentTransaction(fragment, R.id.tab_content, false, false);
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked() {
        finish();
    }

}

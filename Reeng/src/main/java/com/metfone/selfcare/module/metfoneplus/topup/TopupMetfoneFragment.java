package com.metfone.selfcare.module.metfoneplus.topup;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.databinding.TopupMetfoneFragmentBinding;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.ContactTopup;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.notification.detail.RedirectNotificationActivity;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseBindingFragment;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.metfoneplus.topup.model.AmountMetfoneModel;
import com.metfone.selfcare.module.metfoneplus.topup.model.Constants;
import com.metfone.selfcare.module.metfoneplus.topup.viewholder.AmountViewHolder;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

public class TopupMetfoneFragment extends MPBaseBindingFragment<TopupMetfoneFragmentBinding> implements ItemViewClickListener, View.OnClickListener {

    private static final int ZXING_CAMERA_PERMISSION = 1;
    private BaseAdapter amountAdapter;
    private ArrayList<AmountMetfoneModel> amountMetfoneModels = new ArrayList<>();
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private CamIdUserBusiness camIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
    private UserInfo currentUser;
    private String mAmount;
    int position ;
    private List<PhoneNumber> listData = new ArrayList<>();
    private int PICK_CONTACT = 97;
    private ContactTopup mContactTopup;
    private Class<?> mClss;
    private String contactID;

    public static TopupMetfoneFragment newInstance() {
        return new TopupMetfoneFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.topup_metfone_fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mBinding.viewRoot);
        initData();
        initView();
        initListener();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        amountMetfoneModels = AmountMetfoneModel.createAmountValue();
        currentUser = userInfoBusiness.getUser();
        mContactTopup = mApplication.getCamIdUserBusiness().getContactTopup();
    }

    private void initView() {
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.GONE);
        }
        AmountViewHolder amountViewHolder = new AmountViewHolder(getActivity().getWindow().getDecorView().getRootView(), this);
        amountAdapter = new BaseAdapter(amountMetfoneModels, getContext(), R.layout.item_amount, amountViewHolder);
        mBinding.recyclerAmount.setLayoutManager(new GridLayoutManager(getContext(), 5));
        mBinding.recyclerAmount.setAdapter(amountAdapter);
        if (mContactTopup.getPhoneNumber() == null) {
            mBinding.tvPhone.setText("0" + camIdUserBusiness.getPhoneService().substring(0, 2) + " " + camIdUserBusiness.getPhoneService().substring(2));
        } else {
            mBinding.tvPhone.setText("0" + mContactTopup.getPhoneNumber().substring(0, 2) + " " + mContactTopup.getPhoneNumber().substring(2));
        }
        Constants.setupUI(mBinding.viewRoot, getActivity());
    }

    private void initListener() {
        mBinding.btnNext.setOnClickListener(this);
        mBinding.btnBack.setOnClickListener(this);
        mBinding.btnQrCode.setOnClickListener(this);
        mBinding.btnHistory.setOnClickListener(this);
        mBinding.btnContacts.setOnClickListener(this);
        //------------------------------- EVENT EditTExt Change-------------------------------
        mBinding.edtInputAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mBinding.edtInputAmount.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        String valueChange = charSequence.toString().trim();
                        mBinding.edtInputAmount.setSelection(valueChange.length());
                        if (!valueChange.isEmpty()) {
                            mBinding.btnNext.setEnabled(true);
                            // kiểm tra input vs  list amout
                            for (int j = 0; j < amountMetfoneModels.size(); j++) {
                                if (valueChange.equals(amountMetfoneModels.get(j).getAmount() + "")) {
                                    setSateViewSelect(j);
                                    amountAdapter.updateAdapter(amountMetfoneModels);
                                    break;
                                } else {
                                    setSateViewSelect(-1);
                                    amountAdapter.updateAdapter(amountMetfoneModels);
                                }
                            }
                        } else {
                            mBinding.btnNext.setEnabled(false);
                            setSateViewSelect(-1);
                            amountAdapter.updateAdapter(amountMetfoneModels);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                }); String valueChange = charSequence.toString().trim();
                mBinding.edtInputAmount.setSelection(valueChange.length());
                if (!valueChange.isEmpty()) {
                    mBinding.btnNext.setEnabled(true);
                    // kiểm tra input vs  list amout
                    for (int j = 0; j < amountMetfoneModels.size(); j++) {
                        if (valueChange.equals(amountMetfoneModels.get(j).getAmount() + "")) {
                            setSateViewSelect(j);
                            amountAdapter.updateAdapter(amountMetfoneModels);
                            break;
                        } else {
                            setSateViewSelect(-1);
                            amountAdapter.updateAdapter(amountMetfoneModels);
                        }
                    }
                } else {
                    mBinding.btnNext.setEnabled(false);
                    setSateViewSelect(-1);
                    amountAdapter.updateAdapter(amountMetfoneModels);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setSateViewSelect(int position) {
        for (AmountMetfoneModel amountMetfoneModel : amountMetfoneModels) {
            amountMetfoneModel.setCheckSelect(false);
        }
        if (position == -1) return;
        amountMetfoneModels.get(position).setCheckSelect(true);
    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {
        if (list.get(position) instanceof AmountMetfoneModel) {
            mAmount = ((AmountMetfoneModel) list.get(position)).getAmount() + "";
            mBinding.edtInputAmount.setText(mAmount);
            setSateViewSelect(position);
            amountAdapter.updateAdapter(amountMetfoneModels);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                if (validate()) {
                    if (mContactTopup.getPhoneNumber() == null) {
                        gotoMPServiceTopUpFragment(camIdUserBusiness.getPhoneService(), mAmount);
                    } else {
                        gotoMPServiceTopUpFragment(mContactTopup.getPhoneNumber(), mAmount);
                    }
                }
                break;
            case R.id.btnBack:
                onBack();
                break;
            case R.id.btnQrCode:
                gotoMPQrCodeFragment(mBinding.tvPhone.getText().toString());
                break;
            case R.id.btnHistory:
                gotoHistoryTopUpFragment();
                break;
            case R.id.btnContacts:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
                break;
        }
    }

    /**
     * check quyển camera
     *
     * @param clss
     */
    public void launchActivity(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClss = clss;
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            gotoMPQrCodeFragment(mBinding.tvPhone.getText().toString());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mClss != null) {
                        gotoMPQrCodeFragment(mBinding.tvPhone.getText().toString());
                    }
                } else {
                    Toast.makeText(getActivity(), "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor cur = getActivity().managedQuery(contactData, null, null, null, null);
                ContentResolver contect_resolver = getActivity().getContentResolver();
                if (cur.moveToFirst()) {
                    String id = cur.getString(cur.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                    contactID = id;
                    String name = "";
                    String no = "";
                    Cursor phoneCur = contect_resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (phoneCur.moveToFirst()) {
                        name = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        no = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    }
                    Log.e("Phone no & name :***: ", name + " : " + no);
                    if (UserInfoBusiness.isPhoneNumberValid(no, "KH")) {
                        String nationalPhone = Utilities.getNationalPhoneNumber(no, "KH");
                        mBinding.tvPhone.setText("0" + nationalPhone.substring(0, 2) + " " + nationalPhone.substring(2));
                        mContactTopup.setName(name);
                        mContactTopup.setPhoneNumber(nationalPhone);
                        mApplication.getCamIdUserBusiness().setContactTopup(mContactTopup);
                        retrieveContactPhoto();
                    } else {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                    }


                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error :: ", e.toString());
        }
    }

    private boolean validate() {
        String amount = mBinding.edtInputAmount.getText().toString();

        try {
            int num = Integer.parseInt(amount);

            mAmount = String.valueOf(num);
            if (num < 1 || num > 100) {
                Toast.makeText(mApplication, getString(R.string.m_p_top_up_invalid_value_input_amount), Toast.LENGTH_SHORT).show();
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            mAmount = String.valueOf(0);
            mBinding.edtInputAmount.setText(mAmount);
            Toast.makeText(mApplication, getString(R.string.m_p_top_up_invalid_value_input_amount), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

//     lấy avatar khi được chọn sđt từ list danh bạ
    private void retrieveContactPhoto() {
        mBinding.circleImageView.setImageResource(R.drawable.ic_avatar_member);
     //   mApplication.getAvatarBusiness().setPhoneNumberAvatar( mBinding.circleImageView, mBinding.contactAvatarText, entry, size);

        Bitmap photo = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                Glide.with(getContext())
                        .load(photo)
                        .error(R.drawable.ic_avatar_member)
                        .into(mBinding.circleImageView);
                mContactTopup.setAvatar(photo);
            } else {
                Glide.with(getContext())
                        .load(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(mBinding.circleImageView);
            }

            assert inputStream != null;
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onBack() {
        mApplication.getCamIdUserBusiness().setContactTopup(new ContactTopup());
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarColor(R.color.m_home_tab_background_2, true);
            ((HomeActivity)mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }else{
            mParentActivity.finish();
            return;
        }
        popBackStackFragment();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void loadInfoUser(final LoadInfoUser event) {
        if (!StringUtils.isEmpty(mBinding.tvPhone.getText().toString()) && !StringUtils.isEmpty(currentUser.getPhone_number()) && currentUser.getPhone_number().contains(mBinding.tvPhone.getText())) {
            if (event.loadType == LoadInfoUser.LoadType.USER_INFO) {
                if (!StringUtils.isEmpty(event.userInfo.getAvatar())) {
                    Glide.with(getContext())
                            .load(ImageUtils.convertBase64ToBitmap(event.userInfo.getAvatar()))
                            .error(R.drawable.ic_avatar_member)
                            .into(mBinding.circleImageView);
                } else {
                    KhUserRank myRank = KhUserRank.getById(event.rankDTO.rankId);
                    Glide.with(getContext())
                            .load(myRank.resAvatar)
                            .error(R.drawable.ic_avatar_member)
                            .into(mBinding.circleImageView);
                }
            }
        }
    }
}

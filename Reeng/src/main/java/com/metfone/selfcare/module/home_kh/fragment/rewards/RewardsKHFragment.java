package com.metfone.selfcare.module.home_kh.fragment.rewards;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountPointRankResponse;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetAllTeLeComGiftResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemPointResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.RewardBaseFragment;
import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointContainerFragment;
import com.metfone.selfcare.module.home_kh.fragment.reward.RewardCamIdKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.reward.adapter.RewardPageAdapter;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.event.UpdatePoint;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.KHAccountPoint;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.model.RewardChange;
import com.metfone.selfcare.module.home_kh.model.Rewards;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.selfcare.widget.EnhancedWrapContentViewPager;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;

public class RewardsKHFragment extends RewardBaseFragment implements ViewPager.OnPageChangeListener {
    private final String BALANCE = "Balance";
    private final String DATA = "Data";
    private final String VALIDITY = "Validity";
    private KhHomeClient homeKhClient;

    private Unbinder unbinder;
    private AccountRankDTO accountRankDTO;

    public static RewardsKHFragment newInstance() {
        RewardsKHFragment fragment = new RewardsKHFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static final String TAG = RewardsKHFragment.class.getSimpleName();

    @BindView(R.id.pg_contents)
    EnhancedWrapContentViewPager vpReward;

    @BindView(R.id.tab_data)
    AppCompatTextView tabData;

    @BindView(R.id.tab_balance)
    AppCompatTextView tabBalance;

    @BindView(R.id.tab_validity)
    AppCompatTextView tabValidity;

    @BindView(R.id.img_rank)
    AppCompatImageView imgRank;

    @BindView(R.id.tv_name)
    AppCompatTextView tvName;


    @BindView(R.id.tv_points)
    AppCompatTextView pointValue;

    @BindView(R.id.txtTitleToolbar)
    protected AppCompatTextView txtTitle;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.btn_confirm)
    RoundTextView btn_confirm;

    @BindView(R.id.friend_sharing_button)
    CardView friendSharingButton;

    @BindView(R.id.friend_sharing_input)
    CardView friendSharingInput;

    @BindView(R.id.friend_number)
    EditText edtFriendNumber;

    @BindView(R.id.share_gift_layout)
    FrameLayout shareGiftLayout;

    private RewardPageAdapter rewardPageAdapter;
    private double totalAvailability = 0;
    private UserInfoBusiness userInfoBusiness;
    private int selectedTabIndex = 0;
    private RedeemType redeemType = RedeemType.REDEEM_GIFT;


    @Override
    protected boolean needShowBottomMenu() {
        return false;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
        userInfoBusiness = new UserInfoBusiness(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        if (!fromFilter()) {
            Utilities.adaptViewForInsertBottom(view);
        }
        return view;
    }

    private boolean fromFilter(){
        View v = getActivity().findViewById(R.id.spaceOfFilter);
        if(v != null && v.getHeight() > 0){
            return true;
        }
        return false;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSeekBarChange(RewardChange rewardChange){
        if(rewardChange.isDisableButton()){
            setEnableButton(false);
            return;
        }
        setEnableButton(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
//        if(StringUtils.isEmpty(userInfoBusiness.getUser().getPhone_number())){
//            wsGetAllTeLeComGift();
//        }else{
//            wsGetAccountPointInfo(false);
//        }
        wsGetAllTeLeComGift();
        wsGetAllTeLeComGiftForFriend();
        wsGetAccountPointInfo(false);
        getRankAccount();

    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_redeem_reward;
    }

    private void initView() {
        String ss = ResourceUtils.getString(R.string.rewards_title);
        if (ss.equals("")) ss = "Rewards";
        txtTitle.setText(ss);
        pointValue.setText(String.format(getString(R.string.reward_item_point), "0"));
        UserInfo currentUser = userInfoBusiness.getUser();
        if (currentUser != null) {
            String name = currentUser.getFull_name();
            if (name != null) {
                tvName.setText(name);
            }
        }
    }


    private void initPage() {
        List<Fragment> list = new ArrayList<>();
        list.add(DataKHFragment.newInstance());
        list.add(BalanceKHFragment.newInstance());
        list.add(ValidityKHFragment.newInstance());
        rewardPageAdapter = new RewardPageAdapter(getChildFragmentManager(),
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, list);
        vpReward.setAdapter(rewardPageAdapter);
        vpReward.addOnPageChangeListener(this);
        onPageSelected(0);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        if(position != 2){
            showShareToFriend();
        }
        else {
            hideShareToFriend();
        }
        if (position == 0) {
            tabData.setSelected(true);
            tabBalance.setSelected(false);
            tabValidity.setSelected(false);
        } else if (position == 1) {
            tabData.setSelected(false);
            tabBalance.setSelected(true);
            tabValidity.setSelected(false);
        } else if (position == 2) {
            tabData.setSelected(false);
            tabBalance.setSelected(false);
            tabValidity.setSelected(true);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick(R.id.tab_data)
    void clickData() {
        selectedTabIndex = 0;
        if (vpReward != null)
            vpReward.setCurrentItem(0);
    }

    @OnClick(R.id.tab_balance)
    void clickBalance() {
        selectedTabIndex = 1;
        if (vpReward != null)
            vpReward.setCurrentItem(1);
    }

    @OnClick(R.id.tab_validity)
    void clickValidity() {
        selectedTabIndex = 2;
        if (vpReward != null){
            if(friendSharingInput.getVisibility() == View.VISIBLE){
                onCancelFriendSharingClicked();
            }
            vpReward.setCurrentItem(2);
        }
    }

    @OnClick(R.id.img_rank)
    void showRank() {
        replaceFragment(R.id.reward_container, RewardCamIdKHFragment.newInstance(), HistoryPointContainerFragment.TAG);
    }

    private void logH(String m) {
        Log.e("H-Reward", m);
    }


    // 01-07-22 Fix crash bug when click Confirm button
    private void wsGetAllTeLeComGift() {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        progress.setVisibility(View.VISIBLE);
//        btn_confirm.setVisibility(View.GONE);
        btn_confirm.setEnabled(false);
        homeKhClient.wsGetAllTeLeComGift(new KhApiCallback<KHBaseResponse<WsGetAllTeLeComGiftResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<WsGetAllTeLeComGiftResponse> body) {
                progress.setVisibility(View.GONE);
//                btn_confirm.setVisibility(View.VISIBLE);
                logH("wsGetAllTeLeComGift > onSuccess");
                updatePage(body.getData().getWsResponse().getObject());
            }

            @Override
            public void onFailed(String status, String message) {
//                if (progress == null) {
//                    System.out.println("TuanHM bug progress null");
//                }
                progress.setVisibility(View.GONE);
//                btn_confirm.setVisibility(View.VISIBLE);
                btn_confirm.setEnabled(true);
                logH("wsGetAllTeLeComGift > onFailed > " + "status: " + status + "message: " + message);
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsGetAllTeLeComGiftResponse>>> call, Throwable t) {
                progress.setVisibility(View.GONE);
//                btn_confirm.setVisibility(View.VISIBLE);
                t.printStackTrace();
                logH("wsGetAllTeLeComGift > onFailure");
            }
        });
    }

    private void wsGetAllTeLeComGiftForFriend() {
        progress.setVisibility(View.VISIBLE);
//        btn_confirm.setVisibility(View.GONE);
        btn_confirm.setEnabled(false);
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAllTeLeComGiftForFriend(new KhApiCallback<KHBaseResponse<WsGetAllTeLeComGiftResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<WsGetAllTeLeComGiftResponse> body) {
                progress.setVisibility(View.GONE);
//                btn_confirm.setVisibility(View.VISIBLE);
                btn_confirm.setEnabled(true);
                logH("wsGetAllTeLeComGiftForFriend > onSuccess");
                List<Rewards> items = body.getData().getWsResponse().getObject();
                if(items == null || items.isEmpty()){
                    shareGiftLayout.setVisibility(View.GONE);
                    return;
                }
                shareGiftLayout.setVisibility(View.VISIBLE);
                updatePage(items);
            }

            @Override
            public void onFailed(String status, String message) {
                progress.setVisibility(View.GONE);
//                btn_confirm.setVisibility(View.VISIBLE);
                btn_confirm.setEnabled(true);
                logH("wsGetAllTeLeComGiftForFriend > onFailed > " + "status: " + status + "message: " + message);
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsGetAllTeLeComGiftResponse>>> call, Throwable t) {
                progress.setVisibility(View.GONE);
//                btn_confirm.setVisibility(View.VISIBLE);
                btn_confirm.setEnabled(true);
                t.printStackTrace();
                logH("wsGetAllTeLeComGiftForFriend > onFailure");
            }
        });
    }

    private void updatePage(List<Rewards> data) {
        Rewards rewardData = new Rewards();
        Rewards rewardBalance = new Rewards();
        Rewards rewardValidity = new Rewards();
        List<Fragment> list = new ArrayList<>();
        for (Rewards item : data) {
//            if (getString(R.string.check_gift_type_data).equals(item.getGiftTypeName())) {
//                rewardData = item;
//                list.add(DataKHFragment.newInstance(rewardData, (int) totalAvailability));
//            } else if (getString(R.string.check_gift_type_balance).equals(item.getGiftTypeName())) {
//                rewardBalance = item;
//                list.add(BalanceKHFragment.newInstance(rewardBalance, (int) totalAvailability));
//            } else if (getString(R.string.check_gift_type_validity).equals(item.getGiftTypeName())) {
//                rewardValidity = item;
//                list.add(ValidityKHFragment.newInstance(rewardValidity, (int) totalAvailability));
//            }
            if (item.getGiftCode().equals("CHANGE_BALANCE")) {
                rewardBalance = item;
                list.add(BalanceKHFragment.newInstance(rewardBalance, (int) totalAvailability));
            }
            else if (item.getGiftCode().equals("CHANGE_DATA")) {
                rewardData = item;
                list.add(0,DataKHFragment.newInstance(rewardData, (int) totalAvailability));
            }
            else if (item.getGiftCode().equals("CHANGE_DATE")) {
                rewardValidity = item;
                list.add(ValidityKHFragment.newInstance(rewardValidity, (int) totalAvailability));
            }
        }
        for(Fragment fragment: getChildFragmentManager().getFragments()){
            getChildFragmentManager().beginTransaction().remove(fragment).commit();
        }
        rewardPageAdapter = new RewardPageAdapter(getChildFragmentManager(),
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, list);
        vpReward.setAdapter(rewardPageAdapter);
        vpReward.addOnPageChangeListener(this);
        vpReward.setOffscreenPageLimit(2);
        onPageSelected(selectedTabIndex);
        vpReward.setCurrentItem(selectedTabIndex);
        progress.setVisibility(View.INVISIBLE);
//        setEnableButton(false);
    }

    @Subscribe
    public void onEvent(RewardsKHFragment fragment){
        setEnableButton(false);
    }

    @OnClick(R.id.icBackToolbar)
    void onBack() {
        if(getActivity() instanceof DeepLinkActivity){
            getActivity().finish();
            return;
        }
        popBackStackFragment();
    }

    /**
     * get info list point of acc
     */
    private void wsGetAccountPointInfo(boolean updatePoint) {

        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountPointInfo(new KhApiCallback<KHBaseResponse<AccountPointRankResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountPointRankResponse> body) {
                logH("wsGetAccountPointInfo > onSuccess");
                subPoint(body.getData().getListPoint(), updatePoint);
            }

            @Override
            public void onFailed(String status, String message) {
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logH("wsGetAccountPointInfo > onFailed");
                logH("wsGetAccountPointInfo > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountPointRankResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetAccountPointInfo > onFailure");
            }
        });
    }

    private void subPoint(List<KHAccountPoint> list, boolean updatePoint) {
        if (list == null || list.isEmpty()) {
            wsGetAllTeLeComGift();
            return;
        }
        int totalAccumulate = 0;
        totalAvailability = 0;
        for (KHAccountPoint item : list) {
            if (item.getPointType() == PointType.ACTIVE.id) {
                totalAccumulate = totalAccumulate + item.getPointValue();
            } else if (item.getPointType() == PointType.PAST.id) {
                totalAvailability = totalAvailability + item.getPointValue();
            }
        }
        if(totalAvailability > 1){
            pointValue.setText(String.format(getString(R.string.reward_shop_points), formatPoint(totalAvailability)));
        }else{
            pointValue.setText(String.format(getString(R.string.reward_item_point), formatPoint(totalAvailability)));
        }
        if (rewardPageAdapter != null && vpReward != null && updatePoint) {
            for (Fragment fragment : rewardPageAdapter.getData()) {
                if (fragment instanceof DataKHFragment) {
                    ((DataKHFragment) fragment).setMaxPoint((int) totalAvailability);
                } else if (fragment instanceof BalanceKHFragment) {
                    ((BalanceKHFragment) fragment).setMaxPoint((int) totalAvailability);
                } else if (fragment instanceof ValidityKHFragment) {
                    ((ValidityKHFragment) fragment).setMaxPoint((int) totalAvailability);
                }
            }
        }
        if (!updatePoint)
            wsGetAllTeLeComGift();
    }


    /**
     * get info rank of acc
     */
    private void getRankAccount() {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                logH("wsGetAccountRankInfo > onSuccess");
                initRankInfo(body.getData().getAccountRankDTO());
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsGetAccountRankInfo > onFailed");
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logH("wsGetAccountRankInfo > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetAccountRankInfo > onFailure");
            }
        });
    }

    private void initRankInfo(AccountRankDTO account) {
        accountRankDTO = account;
        if (account != null) {
            int rankID = account.rankId;
            KhUserRank.RewardRank myRank = KhUserRank.RewardRank.getById(rankID);
            if (myRank != null) {
                imgRank.setImageDrawable(myRank.drawable);
            }
        }
    }

    @Nullable
    @OnClick(R.id.btn_confirm)
    public void confirm() {
        if (rewardPageAdapter.getItem(vpReward.getCurrentItem()) == null) return;
        Fragment fragment = rewardPageAdapter.getItem(vpReward.getCurrentItem());
        if (fragment instanceof BasePageRewardsKhFragment) {
            BasePageRewardsKhFragment basePageRewardsKhFragment = (BasePageRewardsKhFragment) fragment;
            if(!basePageRewardsKhFragment.isEnoughPoint()){
                ToastUtils.showShort(R.string.not_enough_point);
            }
            confirm(basePageRewardsKhFragment.pointValue, basePageRewardsKhFragment.detailType, basePageRewardsKhFragment.getGiftId());
        }
    }
    public void confirm(int pointValue, DetailType type, String giftId) {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        if(redeemType == RedeemType.REDEEM_GIFT){
            progress.setVisibility(View.VISIBLE);
//            btn_confirm.setVisibility(View.GONE);
            btn_confirm.setEnabled(false);
            homeKhClient.wsRedeemPoint(new KhApiCallback<KHBaseResponse<WsRedeemPointResponse>>() {
                @Override
                public void onSuccess(KHBaseResponse<WsRedeemPointResponse> body) {
                    // 01-14-22 Fix crash bug when Activity is get data or not
                    if (getActivity() != null) {
                        progress.setVisibility(View.GONE);
//                        btn_confirm.setVisibility(View.VISIBLE);
                        logH("wsGetAccountRankInfo > onSuccess");
                        if (getActivity() != null)
                            com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), body.getData().getMessage());
                        getRankAccount();
                        wsGetAccountPointInfo(true);
                        EventBus.getDefault().post(new UpdatePoint());
                    }
                }

                @Override
                public void onFailed(String status, String message) {
                    if (getActivity() != null) {
                        progress.setVisibility(View.GONE);
//                        btn_confirm.setVisibility(View.VISIBLE);
                        logH("wsGetAccountRankInfo > onFailed");
                        if (getActivity() != null)
                            com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                        logH("wsGetAccountRankInfo > " + status + " " + message);
                    }
                }

                @Override
                public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsRedeemPointResponse>>> call, Throwable t) {
                    if (getActivity() != null) {
                        progress.setVisibility(View.GONE);
//                        btn_confirm.setVisibility(View.VISIBLE);
                        t.printStackTrace();
                        logH("wsGetAccountRankInfo > onFailure");
                    }
                }
            }, pointValue, type);
        }
        else{
            String friendNumber = edtFriendNumber.getText().toString();
            if(friendNumber.isEmpty()){
                com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), getString(R.string.phone_number_must_be_not_empty));
            }
            else {
                if(!Utilities.isMetfoneNumber(friendNumber)){
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(),getString(R.string.receiver_number_must_be_metfone));
                    return;
                }
                if( Utilities.getNationalPhoneNumber(friendNumber, "KH").equals( Utilities.getNationalPhoneNumber(userInfoBusiness.getUser().getPhone_number(), "KH"))){
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(),getString(R.string.cannot_share_gift_to_yourseft));
                    return;
                }
                progress.setVisibility(View.VISIBLE);
//                btn_confirm.setVisibility(View.GONE);
                btn_confirm.setEnabled(false);
                homeKhClient.wsRedeemPointForFriend(new KhApiCallback<KHBaseResponse<WsRedeemPointResponse>>() {
                @Override
                public void onSuccess(KHBaseResponse<WsRedeemPointResponse> body) {
                    progress.setVisibility(View.GONE);
//                    btn_confirm.setVisibility(View.VISIBLE);
                    btn_confirm.setEnabled(true);
                    logH("wsGetAccountRankInfo > onSuccess");
                    if (getActivity() != null)
                        com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), body.getData().getMessage());
                    getRankAccount();
                    wsGetAccountPointInfo(true);
                    EventBus.getDefault().post(new UpdatePoint());
                }

                @Override
                public void onFailed(String status, String message) {
                    progress.setVisibility(View.GONE);
                    btn_confirm.setVisibility(View.VISIBLE);
                    logH("wsGetAccountRankInfo > onFailed");
                    if (getActivity() != null)
                        com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                    logH("wsGetAccountRankInfo > " + status + " " + message);
                }

                @Override
                public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsRedeemPointResponse>>> call, Throwable t) {
                    progress.setVisibility(View.GONE);
//                    btn_confirm.setVisibility(View.VISIBLE);
                    btn_confirm.setEnabled(true);
                    t.printStackTrace();
                    logH("wsGetAccountRankInfo > onFailure");
                }
            },edtFriendNumber.getText().toString(), pointValue, type, giftId);
            }
        }
    }
    public void setEnableButton(boolean enableButton) {
        if (getActivity() == null) return;
        if (btn_confirm != null) {
            btn_confirm.setClickable(enableButton);
            if (enableButton) {
                btn_confirm.setTextColor(ContextCompat.getColor(getActivity(), R.color.v5_text_7));
                btn_confirm.setBackgroundColorAndPress(ContextCompat.getColor(getActivity(), R.color.button_continue),
                        ContextCompat.getColor(getActivity(), R.color.button_continue));
            } else {
                btn_confirm.setTextColor(ContextCompat.getColor(getActivity(), R.color.v5_text_3));
                btn_confirm.setBackgroundColorAndPress(ContextCompat.getColor(getActivity(), R.color.v5_cancel),
                        ContextCompat.getColor(getActivity(), R.color.v5_cancel));
            }
        }
    }

    @OnClick(R.id.friend_sharing_button)
    public void onFriendSharingClicked(){
        if(homeKhClient == null){
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.logApp(Constants.LOG_APP.HOME_SHARE_GIFT);
        redeemType = RedeemType.REDEEM_GIFT_FOR_FRIEND;
        friendSharingButton.setVisibility(View.GONE);
        friendSharingInput.setVisibility(View.VISIBLE);
        wsGetAllTeLeComGiftForFriend();
    }

    @OnClick(R.id.friend_sharing_cancel)
    public void onCancelFriendSharingClicked(){
        redeemType = RedeemType.REDEEM_GIFT;
        edtFriendNumber.setText("");
        friendSharingInput.setVisibility(View.GONE);
        friendSharingButton.setVisibility(View.VISIBLE);
        wsGetAllTeLeComGift();
    }

    @OnClick(R.id.btnContact)
    public void onChooseContact(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        startActivityForResult(intent, 100);
    }

    private void showShareToFriend(){
        friendSharingButton.setVisibility(View.VISIBLE);
    }

    private void hideShareToFriend(){
        friendSharingButton.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor cur = getActivity().managedQuery(contactData, null, null, null, null);
                ContentResolver contect_resolver = getActivity().getContentResolver();
                if (cur.moveToFirst()) {
                    String id = cur.getString(cur.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
//                    contactID = id;
                    String name = "";
                    String no = "";
                    Cursor phoneCur = contect_resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (phoneCur.moveToFirst()) {
                        name = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        no = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    }
                    android.util.Log.e("Phone no & name :***: ", name + " : " + no);
                    if (UserInfoBusiness.isPhoneNumberValid(no, "KH")) {
                        String nationalPhone = Utilities.getNationalPhoneNumber(no, "KH");
                        edtFriendNumber.setText(nationalPhone);
//                        mBinding.tvPhone.setText("0" + nationalPhone.substring(0, 2) + " " + nationalPhone.substring(2));
//                        mContactTopup.setName(name);
//                        mContactTopup.setPhoneNumber(nationalPhone);
//                        mApplication.getCamIdUserBusiness().setContactTopup(mContactTopup);
//                        retrieveContactPhoto();
                    } else {
                        com.metfone.selfcare.v5.utils.ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                    }


                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            android.util.Log.e("Error :: ", e.toString());
        }
    }

    enum RedeemType{
        REDEEM_GIFT,
        REDEEM_GIFT_FOR_FRIEND
    }
}
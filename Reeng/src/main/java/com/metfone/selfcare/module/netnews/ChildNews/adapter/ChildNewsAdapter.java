package com.metfone.selfcare.module.netnews.ChildNews.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.newdetails.view.CustomImageRatio;
import com.metfone.selfcare.module.newdetails.view.TextViewWithImages;
import com.metfone.selfcare.module.tiin.DateUtilitis;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HaiKE on 8/19/17.
 */

public class ChildNewsAdapter extends BaseQuickAdapter<NewsModel, BaseViewHolder> {

    private Context context;
    private int categoryId = -1;
    private AbsInterface.OnNewsListener listener;
    private List<NewsModel> datas;

    public ChildNewsAdapter(Context context, int id, List<NewsModel> datas, int categoryId, AbsInterface.OnNewsListener listener) {
        super(id, datas);
        this.context = context;
        this.categoryId = categoryId;
        this.listener = listener;
        this.datas = datas;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void convert(final BaseViewHolder holder, final NewsModel model) {

        try {
            if (model != null) {
                if (categoryId == 7) {
                    if (holder.getView(R.id.tv_title) != null) {
                        holder.setText(R.id.tv_title, model.getTitle());
                        if (checkMarkRead(model.getID())) {
                            TextView textView = holder.getView(R.id.tv_title);
                            textView.setTextColor(textView.getTextColors().withAlpha(120));
                        }
                    }
                    if (holder.getView(R.id.iv_cover) != null) {
                        CustomImageRatio imageView = holder.getView(R.id.iv_cover);
                        if (holder.getAdapterPosition() == 0)
                            imageView.setRatio(9f / 16.0f);
                        else if (holder.getAdapterPosition() == 1 || holder.getAdapterPosition() == 2)
                            imageView.setRatio(10.5f / 16.0f);
                        else
                            imageView.setRatio(9f / 16.0f);
                        ImageBusiness.setImageNewPoster(model.getImage169(), imageView);
                    }
                    if (holder.getView(R.id.tv_desc) != null ) {
                        holder.setVisible(R.id.tv_desc,false);
                    }
                } else if (categoryId == 3 || categoryId == 135) {
                    if (holder.getView(R.id.tv_title) != null) {
                        if (categoryId == 135) {
                            holder.setText(R.id.tv_title, model.getTitle());
                        } else {
//                            setText(holder.getView(R.id.tv_title), model.getTitle(), model.getTypeIcon());
                            holder.setText(R.id.tv_title, model.getTitle());
                        }
                        if (checkMarkRead(model.getID())) {
                            TextView textView = holder.getView(R.id.tv_title);
                            textView.setTextColor(textView.getTextColors().withAlpha(120));
                        }
                    }

                    if (holder.getView(R.id.iv_cover) != null)
                        ImageBusiness.setImageNew(model.getImage(), (ImageView) holder.getView(R.id.iv_cover));

                    if (holder.getView(R.id.tv_duration) != null) {
                        holder.setVisible(R.id.tv_duration, false);
                    }
                    if(categoryId == 135){ //video
                        if (holder.getView(R.id.tv_desc) != null) {
                            holder.setVisible(R.id.tv_desc, false);
                        }
                        if(holder.getView(R.id.iv_icon_news) != null){
                            holder.setVisible(R.id.iv_icon_news,true);
                        }
                    }else {
//                        if (holder.getView(R.id.tv_desc) != null && !TextUtils.isEmpty(model.getShapo())) {
//                            holder.setVisible(R.id.tv_desc, true);
//                            holder.setText(R.id.tv_desc, model.getShapo());
//                        } else {
//                            holder.setVisible(R.id.tv_desc, false);
//                        }
                    }

                } else {
                    if (holder.getView(R.id.tv_title) != null) {
                        TiinUtilities.setText(holder.getView(R.id.tv_title), model.getTitle(), model.getTypeIcon());
                        if (checkMarkRead(model.getID())) {
                            TextView textView = holder.getView(R.id.tv_title);
                            textView.setTextColor(textView.getTextColors().withAlpha(120));
                        }
                    }

                    if (holder.getView(R.id.iv_cover) != null)
                        ImageBusiness.setImageNew(model.getImage(), holder.getView(R.id.iv_cover));
                }
                if (holder.getView(R.id.tv_category) != null) {
                    if (!TextUtils.isEmpty(model.getSourceName())) {
                        holder.setText(R.id.tv_category, model.getSourceName());
                    } else if (!TextUtils.isEmpty(model.getCategory())) {
                        holder.setText(R.id.tv_category, model.getCategory());
                    }
                }

                if (holder.getView(R.id.tv_datetime) != null) {
                    holder.setVisible(R.id.tv_datetime, true);
                    if(model.getTimeStamp() > 0){
                        holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, model.getTimeStamp()));
                    }else {
                        holder.setText(R.id.tv_datetime, Html.fromHtml(model.getDatePub()));
                    }

                } else {
                    if (holder.getView(R.id.tv_datetime) != null)
                        holder.setVisible(R.id.tv_datetime, false);
                }

                if (holder.itemView != null) {
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (listener != null)
                                listener.onItemClick(holder.getAdapterPosition());

                            if (checkMarkRead(model.getID())) {
                                TextView textView = holder.getView(R.id.tv_title);
                                textView.setTextColor(textView.getTextColors().withAlpha(120));
                            }
                        }
                    });
                }
                if (holder.getView(R.id.button_option) != null) {
                    holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(listener != null){
                                listener.onItemClickMore(model);
                            }
                        }
                    });
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    public boolean checkMarkRead(int id) {
//        SharedPref pref = new SharedPref(context);
//        String list = pref.getString(AppConstants.KEY_MARK_READ, "");
//        if(!android.text.TextUtils.isEmpty(list) && !list.equals(","))
//        {
//            String[] listId = list.split(",");
//            for(int i = 0; i < listId.length; i++)
//            {
//                if(listId[i].equals(id + ""))
//                {
//                    return true;
//                }
//            }
//        }
        return false;
    }
    public void setData(List<NewsModel> data){
        datas = data;
        notifyDataSetChanged();
    }
}

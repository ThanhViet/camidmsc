package com.metfone.selfcare.module.newdetails.MainDetailNews.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.newdetails.ChildDetailNews.adapter.NewsDetailAdapter;
import com.metfone.selfcare.module.newdetails.ChildDetailNews.fragment.NewsDetailFragment;
import com.metfone.selfcare.module.newdetails.MainDetailNews.adapter.MainNewsDetailAdapter;
import com.metfone.selfcare.module.newdetails.MainDetailNews.presenter.IMainNewsDetailPresenter;
import com.metfone.selfcare.module.newdetails.MainDetailNews.presenter.MainNewsDetailPresenter;
import com.metfone.selfcare.module.newdetails.MainDetailNews.view.IMainNewsDetailView;
import com.metfone.selfcare.module.newdetails.activity.NewsDetailActivity;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Huongnd38 on 9/26/18.
 */

public class MainNewsDetailFragment extends BaseFragment implements IMainNewsDetailView, AbsInterface.OnNewsHotListener {

    @BindView(R.id.layout_back)
    RelativeLayout rlHeader;
    @BindView(R.id.tv_category_new)
    TextView tvCategoryNew;

    @BindView(R.id.btnClose)
    ImageView btnClose;
    @BindView(R.id.button_option)
    ImageView ivMore;

    @BindView(R.id.tab_view_pager)
    ViewPager viewPager;

    @BindView(R.id.line1)
    View line1;
    @BindView(R.id.line2)
    View line2;
    @BindView(R.id.line3)
    View line3;
    @BindView(R.id.line4)
    View line4;
    @BindView(R.id.line5)
    View line5;

//    @BindView(R.id.pageIndicatorView)
//    PageIndicatorViewCustom pageIndicatorView;

    MainNewsDetailAdapter pagerAdapter;

    NewsModel newsModel;
    String mBaseUrl = "";
    ArrayList<NewsModel> datas = new ArrayList<>();
    int currentPage = 1;
    int numSize = 0;
    int logCurrent = 0;
    boolean isLoadSuccess = false;
    NewsDetailFragment homeNewsDetailFragment = null;

    IMainNewsDetailPresenter mPresenter;
    boolean fromNetNews = false;
    boolean fullData = false;
    long unixTime;

    public static MainNewsDetailFragment newInstance() {
        MainNewsDetailFragment fragment = new MainNewsDetailFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_news_detail, container, false);
        mPresenter = new MainNewsDetailPresenter();
        setUnBinder(ButterKnife.bind(this, view));
        mPresenter.onAttach(this);
        setUp(view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        if (getArguments() != null) {
            newsModel = (NewsModel) getArguments().getSerializable(CommonUtils.KEY_NEWS_ITEM_SELECT);
            fullData = getArguments().getBoolean(CommonUtils.FULL_DATA_DETAIL, false);
        }
        if (newsModel != null) {
            mPresenter.addNewModelSeen(newsModel.getIdFilter());
            Utilities.logClickNews(newsModel);
        }
        if (datas.size() == 0) {
            setupViewPager();
            setUpIndicator(0);
//            pageIndicatorView.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(newsModel.getCategory())) {
            tvCategoryNew.setVisibility(View.VISIBLE);
            tvCategoryNew.setText(newsModel.getCategory());
        }else{
            tvCategoryNew.setVisibility(View.INVISIBLE);
        }
        setVisibilityClose(AppStateProvider.CLICK_CLOSE);
        ivMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()){
//                    getBaseActivity().showDialogLogin();}
//                else{
//                    ShareUtils.openShareMenu(getBaseActivity(), newsModel);
////                    ShareUtils.sendToFriend(getNewsDetailActivity(), ShareUtils.getLinkShare(newsModel));
////                    Log.e("Duong show","show o dau");
//                }
                DialogUtils.showOptionNewsItem(getBaseActivity(), newsModel, new OnClickMoreItemListener() {
                    @Override
                    public void onClickMoreItem(Object object, int menuId) {

                    }
                });
            }
        });
    }

    public void loadData() {
        if (newsModel == null) return;
        if (unixTime == 0) {
            unixTime = newsModel.getUnixTime();
        }
        if (newsModel.getReadFromSource() == CommonUtils.TYPE_NEWS_DETAIL_FROM_CATEGORY) {
            if (newsModel.isPositionFirst())
                mPresenter.loadDataRelateFromCategoryPosition0(newsModel, currentPage, unixTime);
            else
                mPresenter.loadDataRelateFromCategory(newsModel, currentPage, unixTime);
        } else if (newsModel.getReadFromSource() == CommonUtils.TYPE_NEWS_DETAIL_FROM_EVENT) {
            mPresenter.loadDataRelateFromEvent(newsModel, currentPage, unixTime);
        } else
            mPresenter.loadDataRelate(newsModel, currentPage, unixTime);

    }
    public void loadCatagory(String category){
        if(tvCategoryNew != null){
            if(datas != null && datas.size() > 0) {
                datas.get(0).setCategory(category);
            }
            tvCategoryNew.setVisibility(View.VISIBLE);
            tvCategoryNew.setText(category);
        }
    }

    @SuppressLint("ResourceType")
    private void setUpIndicator(int position) {
        if (position % 5 == 0) {
            line1.setBackgroundResource(R.color.v5_main_color);
            line2.setBackgroundResource(R.color.v5_cancel);
            line3.setBackgroundResource(R.color.v5_cancel);
            line4.setBackgroundResource(R.color.v5_cancel);
            line5.setBackgroundResource(R.color.v5_cancel);
        } else if (position % 5 == 1) {
            line1.setBackgroundResource(R.color.v5_cancel);
            line2.setBackgroundResource(R.color.v5_main_color);
            line3.setBackgroundResource(R.color.v5_cancel);
            line4.setBackgroundResource(R.color.v5_cancel);
            line5.setBackgroundResource(R.color.v5_cancel);
        } else if (position % 5 == 2) {
            line1.setBackgroundResource(R.color.v5_cancel);
            line2.setBackgroundResource(R.color.v5_cancel);
            line3.setBackgroundResource(R.color.v5_main_color);
            line4.setBackgroundResource(R.color.v5_cancel);
            line5.setBackgroundResource(R.color.v5_cancel);
        } else if (position % 5 == 3) {
            line1.setBackgroundResource(R.color.v5_cancel);
            line2.setBackgroundResource(R.color.v5_cancel);
            line3.setBackgroundResource(R.color.v5_cancel);
            line4.setBackgroundResource(R.color.v5_main_color);
            line5.setBackgroundResource(R.color.v5_cancel);
        } else if (position % 5 == 4) {
            line1.setBackgroundResource(R.color.v5_cancel);
            line2.setBackgroundResource(R.color.v5_cancel);
            line3.setBackgroundResource(R.color.v5_cancel);
            line4.setBackgroundResource(R.color.v5_cancel);
            line5.setBackgroundResource(R.color.v5_main_color);
        }
    }

    private void setupViewPager() {
        if (pagerAdapter == null) {
            pagerAdapter = new MainNewsDetailAdapter(getChildFragmentManager());
            Bundle bundle = new Bundle();
            bundle.putSerializable(CommonUtils.KEY_NEWS_ITEM_SELECT, newsModel);
            bundle.putBoolean(CommonUtils.FULL_DATA_DETAIL, fullData);
            homeNewsDetailFragment = NewsDetailFragment.newInstance(bundle, true);
            pagerAdapter.addFragment(homeNewsDetailFragment, "Home News Detail");
            datas.clear();
            datas.add(newsModel);
        }
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.e("----Duong----", "onPageScrolled: " + position);
            }

            @Override
            public void onPageSelected(int position) {
                Log.e("----Duong----", "onPageSelected: " + position);
                try {
                    setUpIndicator(position);
                    if (position == (numSize - 1) && position > 0) {
                        currentPage++;
                        unixTime = datas.get(datas.size() - 1).getUnixTime();
                        loadData();
                    }
                    if(tvCategoryNew != null){
                        tvCategoryNew.setVisibility(View.VISIBLE);
                        tvCategoryNew.setText(datas.get(position).getCategory());
                    }
                    if (position > logCurrent) {
                        //todo show log ga
//                        getBaseActivity().trackingEvent("Đọc bài NetNews", datas.get(position).getTitle(), "" + datas.get(position).getID());
                        //todo show log server
                        Utilities.logClickNews(datas.get(position));
                        logCurrent = position;
                        // set seen
                        mPresenter.addNewModelSeen(datas.get(position).getIdFilter());
                    }
                    if (getNewsDetailActivity() != null) {
                        getNewsDetailActivity().stopVideoDetail();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void bindDataRelate(NewsResponse response) {
        if (response != null) {
            if (response.getData() != null && response.getData().size() > 0) {
                for (int i = 0; i < response.getData().size(); i++) {
                    NewsModel model = response.getData().get(i);
                    if (!TextUtils.isEmpty(model.getUrl()) && !model.getUrl().equals(newsModel.getUrl())) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(CommonUtils.KEY_NEWS_ITEM_SELECT, model);
                        NewsDetailFragment newsDetailFragment = NewsDetailFragment.newInstance(bundle, false);
                        pagerAdapter.addFragment(newsDetailFragment, "");
                        datas.add(model);
                        setVisiableLine(i);
                    }
                }
                numSize = datas.size();
                pagerAdapter.notifyDataSetChanged();
//                pageIndicatorView.setVisibility(View.VISIBLE);
//                pageIndicatorView.setViewPager(viewPager);
                viewPager.setOffscreenPageLimit(1);
            }
        }
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        if (flag) {
            isLoadSuccess = true;
        } else if (!isNetworkConnected()) {
            CommonUtils.showNetworkDisconnect(getActivity());
        }
    }

    @Override
    public void onClickImageItem(NewsDetailModel entry) {
    }

    @Override
    public void onClickVideoItem(NewsDetailModel entry, FrameLayout view, int pos) {
    }

    @Override
    public void onClickRelateItem(NewsModel model, int type) {
//        readNews(model);
    }

    @Override
    public void onClickListenItem(NewsModel model) {
    }

    @Override
    public void onClickSourceNewsItem(String cateName, String cateId) {
        if (getBaseActivity() != null && getBaseActivity() instanceof NewsDetailActivity) {
            Bundle bundle = new Bundle();
            bundle.putInt(CommonUtils.KEY_CATEGORY_ID, Integer.parseInt(cateId));
            bundle.putString(CommonUtils.KEY_CATEGORY_NAME, cateName);
            NewsDetailActivity mainActivity = (NewsDetailActivity) getBaseActivity();
            mainActivity.showFragment(CommonUtils.TAB_SOURCE_TOP_NOW, bundle);
        }
    }

    @Override
    public void onClickLinkItem(int type, NewsModel entry) {
    }

    @Override
    public void onPlayVideoDetail(NewsDetailModel model, int position, NewsDetailAdapter.NewsHotDetailVideoHolder holder) {

    }

    @Override
    public void onDetachVideoDetail() {

    }

    @Override
    public void onItemClickBtnMore(NewsModel model) {

    }

    @OnClick(R.id.btnBack)
    public void onNavBackClick() {
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().onBackPressed();
        }
    }

    @OnClick(R.id.btnClose)
    public void onNavCloseClick() {
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().onBackPressed();
        }
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().clearMainNewsDetailStacks();
            getNewsDetailActivity().onBackPressed();
        }
        AppStateProvider.CLICK_CLOSE = 1;
    }

    public void setVisibilityClose(int count) {
        if (count > 1) {
            btnClose.setVisibility(View.VISIBLE);
        } else {
            btnClose.setVisibility(View.GONE);
        }
    }

    public void goPrevious() {
        if (getBaseActivity() != null) {
            getBaseActivity().onBackPressed();
        }
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        homeNewsDetailFragment = null;
        super.onDestroyView();
    }

    @Override
    public void paddingView(boolean flag) {
        viewPager.setPadding(viewPager.getPaddingLeft(), viewPager.getPaddingTop(), viewPager.getPaddingRight(), 0);
    }

    public void loadDataAfterReconnect() {
        if (isLoadSuccess) return;
        if (homeNewsDetailFragment != null) {
            homeNewsDetailFragment.loadData();
        }
        loadData();
    }

    private void setVisiableLine(int i){
        if( i == 0){
            line2.setVisibility(View.VISIBLE);
        }else if( i == 1){
            line3.setVisibility(View.VISIBLE);
        }else if( i == 2){
            line4.setVisibility(View.VISIBLE);
        }else if( i == 5){
            line5.setVisibility(View.VISIBLE);
        }
    }

}

package com.metfone.selfcare.module.tiin.maintiin.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.tiin.AppProvider;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.module.tiin.activitytiin.TiinActivity;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.event.TabTiinEvent;
import com.metfone.selfcare.module.tiin.childtiin.fragment.ChildTiinFragment;
import com.metfone.selfcare.module.tiin.hometiin.fragment.HomeTiinFragment;
import com.metfone.selfcare.module.tiin.maintiin.adapter.ViewpagerTiinAdapter;
import com.metfone.selfcare.module.tiin.maintiin.presenter.IMainTiinMvpPresenter;
import com.metfone.selfcare.module.tiin.maintiin.presenter.MainTiinPresenter;
import com.metfone.selfcare.module.tiin.maintiin.view.MainTiinMvpView;
import com.metfone.selfcare.module.tiin.network.model.Category;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

@SuppressWarnings("StringConcatenationInLoop")
public class TabTiinFragment extends BaseFragment implements MainTiinMvpView {
    Unbinder unbinder;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tv_tab_name)
    TextView tvTabName;
    @BindView(R.id.headerController)
    RelativeLayout headerController;
    @BindView(R.id.vStub)
    ViewStub vStub;

    private ArrayList<Category> listCategory;
    private String listId = "";
    private IMainTiinMvpPresenter mPresenter;
    private ViewpagerTiinAdapter adapter;
    private int currentTabIndex = 0;
    private AppBarLayout appBarLayout;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int currentPosition = -1;

    public static TabTiinFragment newInstance() {
        return new TabTiinFragment();
    }

    private boolean needLoadOnCreate = false;           //dung cho deeplink

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && getView() == null && getActivity() == null) {
            needLoadOnCreate = true;
            return;
        }

        if (isVisibleToUser && getActivity() instanceof HomeActivity) {

            if (getView() == null)
                needLoadOnCreate = true;
            else {
                initViewStub();
            }
        }

    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        long time = System.currentTimeMillis();
        View view = inflater.inflate(R.layout.fragment_main_tiin, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (mPresenter == null) {
            mPresenter = new MainTiinPresenter();
        }
        mPresenter.onAttach(this);
        if (needLoadOnCreate) {
            Log.i(TAG, "load on create");
            setUserVisibleHint(true);
            initViewStub();
        }

        if (getBaseActivity() instanceof HomeActivity) {
            ivBack.setVisibility(View.GONE);
            Utilities.setMargins(tvTabName, Utilities.dpToPixel(R.dimen.v5_spacing_normal, getResources()), tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
//            Utilities.setMargins(headerController, headerController.getLeft(), Utilities.dpToPixel(R.dimen.v5_margin_top_action_bar_home, getResources()), headerController.getRight(), headerController.getBottom());
            Utilities.setMargins(headerController, headerController.getLeft(), 0, headerController.getRight(), headerController.getBottom());
        } else {
            ivBack.setVisibility(View.VISIBLE);
            Utilities.setMargins(tvTabName, 0, tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
            Utilities.setMargins(headerController, headerController.getLeft(), 0, headerController.getRight(), headerController.getBottom());
        }
        Log.d(TAG, "[perform] onCreateView: " + (System.currentTimeMillis() - time));
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof ModuleActivity)
            needLoadOnCreate = true;
    }

    boolean initViewStubDone = false;

    private void initViewStub() {
        if (initViewStubDone) return;
        Log.i(TAG, "initViewStub");
        initViewStubDone = true;
        View view = vStub.inflate();
        appBarLayout = view.findViewById(R.id.id_appbar);
        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.tab_view_pager);
        setupViewpager();
    }

    private void setupViewpager() {
        if (adapter == null) {
            adapter = new ViewpagerTiinAdapter(getChildFragmentManager());
            adapter.addFragment(HomeTiinFragment.newInstance(), getBaseActivity().getResources().getString(R.string.home_tiin));
            adapter.addFragment(ChildTiinFragment.newInstance(0), getBaseActivity().getResources().getString(R.string.new_tiin));
            listCategory = AppProvider.getInstance().getDatas();
            if (listCategory == null || listCategory.size() == 0) {
                listCategory = TiinUtilities.getListCategoryTiin(getBaseActivity());
                AppProvider.getInstance().setDatas(listCategory);
                mPresenter.loadCategory(mPref);
//                bindData(listCategory);
//            } else if (Utilities.notEmpty(listCategory)) {
            } else {
                if (adapter.getCount() <= 2) {
                    mPresenter.loadCategory(mPref);
                }
            }
        }
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        adapter.notifyDataSetChanged();
//        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentTabIndex = position;
                if (appBarLayout != null) {
                    appBarLayout.setExpanded(true, true);
                }
                //Vuot sang tab nao tab day refresh du lieu
                if (position == 0) {
                    if (getPagerAdapter() != null && getPagerAdapter().getItem(0) instanceof HomeTiinFragment) {
                        HomeTiinFragment homeNewsFragment = (HomeTiinFragment) getPagerAdapter().getItem(0);
                        homeNewsFragment.onRefresh();
                    }
                }
                if (currentPosition != position) {
                    String tabName = "";
                    if (position == 0) {
                        tabName = getString(R.string.home_tiin);
                    } else if (position == 1) {
                        tabName = getString(R.string.new_tiin);
                    } else {
                        int pos = position - 2;
                        if (listCategory != null && listCategory.size() > pos && pos >= 0) {
                            Category model = listCategory.get(pos);
                            if (model != null) {
                                tabName = model.getName();
                            }
                        }
                    }
                }
                currentPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                Toast.makeText(getContext(), "Click + "+tab.getPosition(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                EventBus.getDefault().post(new TabTiinEvent(tab.getPosition()));
            }
        });
        AppProvider.CLICK_CLOSE = 1;
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        if (!flag) {
            listCategory = TiinUtilities.getListCategoryTiin(getBaseActivity());
            AppProvider.getInstance().setDatas(listCategory);
            bindData(listCategory);
        }
    }

    @Override
    public void bindData(ArrayList<Category> categoryResponse) {
        listCategory = categoryResponse;
        String str = "";
        for (Category model : listCategory) {
            if (model.isOn()) {
                adapter.addFragment(ChildTiinFragment.newInstance(model.getiD()), model.getName());
                str += model.getiD() + ",";
            }
        }
        listId = str;
        AppProvider.getInstance().setIdCategory(str);
        AppProvider.getInstance().setDatas(listCategory);
        mPref.put(SharedPrefs.LIST_ID_CATAGORY, listId);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void bindDataCategory(String list) {
        listId = list;
        AppProvider.getInstance().setIdCategory(list);
        if (TextUtils.isEmpty(list))//Lan dau vao app
        {
            String str = "";
            for (Category model : listCategory) {
                if (model.isOn()) {
                    adapter.addFragment(ChildTiinFragment.newInstance(model.getiD()), model.getName());
                    str += model.getiD() + ",";
                }
            }
            listId = str;
            AppProvider.getInstance().setIdCategory(listId);
            adapter.notifyDataSetChanged();

        } else {
            if (adapter.getCount() == 2) {
                String[] listCategoryId = AppProvider.getInstance().getIdCategory().split(",");
                if (listCategoryId.length > 0) {
                    for (String categoryId : listCategoryId) {
                        Category model = getCategoryModelById(categoryId);
                        if (model != null) {
                            adapter.addFragment(ChildTiinFragment.newInstance(model.getiD()), model.getName());
                        }
                    }
                }
                AppProvider.getInstance().setIdCategory(listId);
            }
            adapter.notifyDataSetChanged();
        }
        mPref.put(SharedPrefs.LIST_ID_CATAGORY, listId);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (listId == null) {
            listId = "";
        }
        if (AppProvider.getInstance().isCheckConfigCategory() && !listId.equals(AppProvider.getInstance().getIdCategory())) {
            processSettingCategory();
            AppProvider.getInstance().setCheckConfigCategory(false);
        }
    }

    /**
     * click title tu home mo ra tab cua no
     */
    public void selectTab(final int position) {
        if (position == currentTabIndex) return;
        if (viewPager != null) {
            tabLayout.setScrollPosition(position, 0f, true);
            viewPager.setCurrentItem(position);
        }
        scrollToPosition(position);
        currentTabIndex = position;
    }

    @OnClick(R.id.iv_search)
    public void onClickSearch() {
//        HomeTiinModel model = getEventData();
//        Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
//        intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_SEARCH);
//        if (model != null) {
//            intent.putExtra(ConstantTiin.KEY_EVENT_DATA, model);
//        }
//        getBaseActivity().startActivity(intent);
        Utilities.openSearch(getBaseActivity(), Constants.TAB_TIIN_HOME);
    }

    @OnClick(R.id.btnConfig)
    public void onClickConfig() {
        Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
        intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CONFIG);
        getBaseActivity().startActivity(intent);
    }

    public ViewpagerTiinAdapter getPagerAdapter() {
        return adapter;
    }

    /**
     * truyen du lieu su kien vao search
     */
    private HomeTiinModel getEventData() {
        if (getPagerAdapter() != null && getPagerAdapter().getItem(0) != null && getPagerAdapter().getItem(0) instanceof HomeTiinFragment) {
            HomeTiinFragment homeNewsFragment = (HomeTiinFragment) getPagerAdapter().getItem(0);
            return homeNewsFragment.findEventData();
        }
        return null;
    }

    /**
     * cai dat lai cac tab trong tiin
     */
    private void processSettingCategory() {
        try {
            listCategory = AppProvider.getInstance().getDatas();
            listId = AppProvider.getInstance().getIdCategory();
            String[] listCategoryId = AppProvider.getInstance().getIdCategory().split(",");
            if (listCategoryId.length > 0) {
                FragmentManager manager = getChildFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                //Clear all
                for (int i = tabLayout.getTabCount() - 1; i >= 2; i--) {
                    if (tabLayout.getChildCount() > 0) {
                        tabLayout.removeTabAt(i);
                    }
                }

                for (int i = adapter.getCount() - 1; i >= 2; i--) {
                    Fragment fragment = adapter.getItem(i);
                    trans.remove(fragment);
                    adapter.removeFrag(i);
                }
                trans.commit();

                for (String categoryId : listCategoryId) {
                    Category model = getCategoryModelById(categoryId);
                    if (model != null) {
                        adapter.addFragment(ChildTiinFragment.newInstance(model.getiD()), model.getName());
                    }
                }

                adapter.notifyDataSetChanged();
                viewPager.setCurrentItem(0);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public Category getCategoryModelById(String categoryId) {
        for (int i = 0; i < listCategory.size(); i++) {
            if (categoryId.equals(listCategory.get(i).getiD() + ""))
                return listCategory.get(i);
        }
        return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void scrollToPosition(int position) {
        Log.e("---Duong----", "TabTiinFragment");
        try {
            Fragment fragment = adapter.getItem(viewPager.getCurrentItem());
            if (fragment instanceof HomeTiinFragment) {
                ((HomeTiinFragment) fragment).scrollToPosition(position);
            } else if (fragment instanceof ChildTiinFragment) {
                ((ChildTiinFragment) fragment).scrollToPosition(position);
            }
            if (appBarLayout != null) {
                appBarLayout.setExpanded(true, true);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        getBaseActivity().onBackPressed();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final TabTiinEvent event) {
        if (event.isReSelected()) {
            scrollToPosition(0);
        }
    }
}

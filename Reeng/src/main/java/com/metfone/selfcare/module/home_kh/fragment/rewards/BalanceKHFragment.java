package com.metfone.selfcare.module.home_kh.fragment.rewards;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.model.Rewards;

public class BalanceKHFragment extends BasePageRewardsKhFragment {
    public static BalanceKHFragment newInstance() {
        BalanceKHFragment fragment = new BalanceKHFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static BalanceKHFragment newInstance(Rewards item, int max) {
        BalanceKHFragment fragment = new BalanceKHFragment();
        Bundle args = new Bundle();
        args.putSerializable(DATA, item);
        args.putInt(MAX, max);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detailType = DetailType.BALANCE;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
    @Override
    public int getResIdView() {
        return R.layout.fragment_balance_page_kh;
    }
}
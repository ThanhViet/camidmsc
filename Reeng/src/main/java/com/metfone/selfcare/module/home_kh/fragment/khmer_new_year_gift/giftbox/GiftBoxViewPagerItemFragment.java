package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.metfone.selfcare.databinding.FragmentGiftBoxPageBinding;
import com.metfone.selfcare.module.home_kh.api.WsGiftBoxResponse;

import java.util.ArrayList;

public class GiftBoxViewPagerItemFragment extends Fragment {

    private FragmentGiftBoxPageBinding bindng;
    private GiftBoxAdapter adapter;
    public GiftBoxAdapter.EventListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        bindng = FragmentGiftBoxPageBinding.inflate(getLayoutInflater());
        bindng.setHandler(this);
        return bindng.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new GiftBoxAdapter(getActivity(), listener);
        bindng.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        bindng.recyclerView.setAdapter(adapter);
    }

    public void updateData(ArrayList<WsGiftBoxResponse.PrizeInfo> items){
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }
}

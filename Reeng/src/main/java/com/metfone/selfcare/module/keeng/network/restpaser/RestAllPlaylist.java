package com.metfone.selfcare.module.keeng.network.restpaser;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.PlayListModel;

import java.io.Serializable;
import java.util.ArrayList;

public class RestAllPlaylist extends AbsResultData implements Serializable {

	private static final long serialVersionUID = 3161892841746591273L;

	@SerializedName("data")
	private ArrayList<PlayListModel> data;

	public ArrayList<PlayListModel> getData() {
		return data;
	}

	public void setData(ArrayList<PlayListModel> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "RestAllPlaylist [data=" + data + "] error " + getError();
	}

	/*
	 * Kiem tra xem co sai token ko
	 *
	 * @param context
	 *
	 * @return
	 */
	public ArrayList<PlayListModel> getData(Context context) {
		if (isWrongToken()) {
//			AutoLoginAsync.autoLoginManual(context);
		}
		return data;
	}
}
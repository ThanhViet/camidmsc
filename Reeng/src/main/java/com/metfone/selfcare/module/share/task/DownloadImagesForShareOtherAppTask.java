/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/13
 *
 */

package com.metfone.selfcare.module.share.task;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.core.content.FileProvider;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.share.listener.ShareImagesOnOtherAppListener;
import com.metfone.selfcare.util.Utilities;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class DownloadImagesForShareOtherAppTask extends AsyncTask<Void, Integer, ArrayList<Uri>> {
    private final String TAG = "DownloadImagesForShareOtherAppTask";
    private WeakReference<ApplicationController> application;
    private ShareImagesOnOtherAppListener listener;
    private ArrayList<FeedContent.ImageContent> list;

    public DownloadImagesForShareOtherAppTask(ApplicationController application, ArrayList<FeedContent.ImageContent> list) {
        this.application = new WeakReference<>(application);
        this.list = list;
    }

    public void setListener(ShareImagesOnOtherAppListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) listener.onPrepareDownload();
    }

    @Override
    protected ArrayList<Uri> doInBackground(Void... voids) {
        if (Utilities.notNull(application) && Utilities.notEmpty(list)) {
            if (application.get().isDataReady()) {
                ArrayList<Uri> results = new ArrayList<>();
                for (FeedContent.ImageContent item : list) {
                    if (item != null) {
                        Bitmap bitmap;
                        if (!TextUtils.isEmpty(item.getFilePath())) {
                            bitmap = ImageBusiness.getImageBitmapFromFile(item.getFilePath());
                        } else {
                            bitmap = ImageBusiness.downloadImageBitmap(item.getImageUrl(application.get()));
                        }
                        if (bitmap != null) {
                            String imagePath = application.get().getExternalCacheDir() + "/cache_img_" + System.currentTimeMillis() + ".jpg";
                            File file = ImageHelper.saveBitmapToFile(bitmap, imagePath, Bitmap.CompressFormat.JPEG);
                            if (file != null && file.isFile()) {
                                Uri uri = FileProvider.getUriForFile(application.get(), BuildConfig.APPLICATION_ID + ".provider", file);
                                results.add(uri);
                            }
                        }
                    }
                }
                return results;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(@Nullable ArrayList<Uri> results) {
        if (listener != null) listener.onCompletedDownload(results);
    }

}

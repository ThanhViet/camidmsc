package com.metfone.selfcare.module.home_kh.fragment.history;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.PointTransferHistoryResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.history.adapter.HistoryPointItemAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;

public class HistoryPointPageFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private HistoryPointItemAdapter historyPointItemAdapter;
    private CompositeDisposable disposable;
    private KhHomeClient homeKhClient;
    private int type;

    @Override
    public String getName() {
        return HistoryPointPageFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_history_point_page;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        disposable = new CompositeDisposable();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(KEY_TYPE);
        }
        recyclerView = view.findViewById(R.id.recycler);
        historyPointItemAdapter = new HistoryPointItemAdapter(type);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(historyPointItemAdapter);
        loadHistoryPointLog(String.valueOf(type));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recyclerView = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        disposable = null;
    }

    public static final String KEY_TYPE = "type";

    private void loadHistoryPointLog(String type) {

        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetPointTransferHistory(new KhApiCallback<KHBaseResponse<PointTransferHistoryResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<PointTransferHistoryResponse> body) {
                historyPointItemAdapter.addData(body.getData().getListPointTransferHistory());
                historyPointItemAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(String status, String message) {
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<PointTransferHistoryResponse>>> call, Throwable t) {

            }
        }, type);
    }

    public static HistoryPointPageFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt(KEY_TYPE, type);
        HistoryPointPageFragment fragment = new HistoryPointPageFragment();
        fragment.setArguments(args);
        return fragment;
    }
}

package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.RecyclingPagerAdapter;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class SlideAdapter extends RecyclingPagerAdapter {
    static final String TAG = "SlideAdapter";
    private List<AllModel> datas;
    private LayoutInflater inflater;
    private boolean isInfiniteLoop;
    private AbsInterface.OnItemListener onItemListener;

    public SlideAdapter(Context context, List<AllModel> datas) {
        this.datas = datas;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isInfiniteLoop = false;
    }

    private int getPosition(int position) {
        return isInfiniteLoop ? position % datas.size() : position;
    }

    public AllModel getItem(int position) {
        try {
            return datas.get(position);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return null;
    }

    @Override
    public int getCount() {
        return isInfiniteLoop ? Integer.MAX_VALUE : datas.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup container) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.holder_flash_hot_detail, container, false);
            vh = new ViewHolder();
            vh.imvThumbnail = (ImageView) convertView.findViewById(R.id.imvThumbnail);
            vh.tvName = (TextView) convertView.findViewById(R.id.tvName);
            vh.iconPlay = (ImageView) convertView.findViewById(R.id.icon_play);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final AllModel item = getItem(position);
        if (item != null) {

            vh.tvName.setText(item.getName());
            if (item.getType() == Constants.TYPE_VIDEO) {
                vh.iconPlay.setVisibility(View.VISIBLE);
            } else
                vh.iconPlay.setVisibility(View.GONE);

            ImageBusiness.setFlashHot(item.getImage(), vh.imvThumbnail, position);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        item.setSource(MediaLogModel.SRC_FLASH_HOT);
                        onItemListener.onItemClick(item);
                    }
                }
            });
        }

        return convertView;
    }

    public void setOnClickListener(AbsInterface.OnItemListener listener) {
        this.onItemListener = listener;
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public SlideAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }

    static class ViewHolder {
        public ImageView imvThumbnail;
        public TextView tvName;
        public ImageView iconPlay;
    }
}

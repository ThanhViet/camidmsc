package com.metfone.selfcare.module.keeng.widget.buttonSheet.player;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class BottomSheetPlayerAdapter extends BaseAdapterRecyclerView {

    private List<MediaModel> datas;
    private int currentIndex = 0;
    private BaseListener.OnClickBottomPlayerSheet listener;

    public BottomSheetPlayerAdapter(Context context, List<MediaModel> datas, BaseListener.OnClickBottomPlayerSheet listener) {
        super(context, "");
        this.datas = datas;
        this.listener = listener;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    @Override
    public int getItemCount() {
        if (datas != null)
            return datas.size();
        return 0;
    }

    @Override
    public MediaModel getItem(int position) {
        try {
            return datas.get(position);
        } catch (IndexOutOfBoundsException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return null;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int type) {
        if (type == ITEM_BOTTOM_SHEET) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.holder_bottom_sheet_player, parent, false);
            return new BottomSheetPlayerHolder(view, listener);
        }
        return super.onCreateViewHolder(parent, type);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseHolder holder, int position) {
        if (holder instanceof BottomSheetPlayerHolder) {
            BottomSheetPlayerHolder itemHolder = (BottomSheetPlayerHolder) holder;
            if (position == currentIndex) {
                itemHolder.bind(getItem(position), true);
            } else {
                itemHolder.bind(getItem(position), false);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        MediaModel item = getItem(position);
        if (item == null) {
            return ITEM_EMPTY;
        }
        return ITEM_BOTTOM_SHEET;
    }
}

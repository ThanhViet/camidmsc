package com.metfone.selfcare.module.netnews.event;

import com.metfone.selfcare.module.newdetails.model.CategoryModel;

/**
 * Created by HaiKE on 11/3/17.
 */

public class EditCategoryEvent {
    public static final int MODE_NONE = 0;
    public static final int MODE_FOLLOW = 1;
    public static final int MODE_SWIPE = 2;
    int type;
    int mode;
    CategoryModel model;
    String listSource;
    int moveStart;
    int moveEnd;
    boolean isSettingEntry = false;

    public EditCategoryEvent(boolean isSettingEntry) {
        this.isSettingEntry = isSettingEntry;
    }

    public EditCategoryEvent(String listSource, int type, int mode, CategoryModel model1) {
        this.type = type;
        this.mode = mode;
        this.model = model1;
        this.listSource = listSource;
    }

    public EditCategoryEvent(String listSource, int type, boolean isSettingEntry) {
        this.type = type;
        this.isSettingEntry = isSettingEntry;
        this.listSource = listSource;
    }

    public EditCategoryEvent(String listSource, int type, int mode, int moveStart, int moveEnd) {
        this.type = type;
        this.mode = mode;
        this.moveEnd = moveEnd;
        this.moveStart = moveStart;
        this.listSource = listSource;
    }

    public boolean isSettingEntry() {
        return isSettingEntry;
    }

    public void setSettingEntry(boolean settingEntry) {
        isSettingEntry = settingEntry;
    }

    public String getListSource() {
        return listSource;
    }

    public void setListSource(String listSource) {
        this.listSource = listSource;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public CategoryModel getModel() {
        return model;
    }

    public void setModel(CategoryModel model) {
        this.model = model;
    }

    public int getMoveStart() {
        return moveStart;
    }

    public void setMoveStart(int moveStart) {
        this.moveStart = moveStart;
    }

    public int getMoveEnd() {
        return moveEnd;
    }

    public void setMoveEnd(int moveEnd) {
        this.moveEnd = moveEnd;
    }
}

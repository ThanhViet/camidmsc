/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/7/3
 *
 */

package com.metfone.selfcare.module.search.model;

import java.io.Serializable;

public class NotFound implements Serializable {
    private String title;
    private String name;
    private int searchType;

    public NotFound() {
    }

    public NotFound(String title, String name, int searchType) {
        this.title = title;
        this.name = name;
        this.searchType = searchType;
    }

    public NotFound(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSearchType() {
        return searchType;
    }

    public void setSearchType(int searchType) {
        this.searchType = searchType;
    }
}

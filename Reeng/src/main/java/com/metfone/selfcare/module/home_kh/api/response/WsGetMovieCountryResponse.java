package com.metfone.selfcare.module.home_kh.api.response;

import com.metfone.selfcare.module.movienew.model.Country;

import java.util.List;

public class WsGetMovieCountryResponse {
    public String errorCode;
    public String errorMessage;
    public List<Country> result;
}

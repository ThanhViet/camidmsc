package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCSubListModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("individualId")
    @Expose
    private String individualId;
    @SerializedName("subType")
    @Expose
    private String subType;
    @SerializedName("isdn")
    @Expose
    private String isdn;
    @SerializedName("verify")
    @Expose
    private Boolean verify;
    @SerializedName("createdDateTime")
    @Expose
    private String createdDateTime;
    @SerializedName("updatedDateTime")
    @Expose
    private String updatedDateTime;
    @SerializedName("ocsAccount")
    @Expose
    private OcsAccount ocsAccount;

    private SCLoyaltyModel loyaltyModel;

    public SCLoyaltyModel getLoyaltyModel() {
        return loyaltyModel;
    }

    public void setLoyaltyModel(SCLoyaltyModel loyaltyModel) {
        this.loyaltyModel = loyaltyModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIndividualId() {
        return individualId;
    }

    public void setIndividualId(String individualId) {
        this.individualId = individualId;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Boolean getVerify() {
        return verify;
    }

    public void setVerify(Boolean verify) {
        this.verify = verify;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(String updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public OcsAccount getOcsAccount() {
        return ocsAccount;
    }

    public void setOcsAccount(OcsAccount ocsAccount) {
        this.ocsAccount = ocsAccount;
    }

    @Override
    public String toString() {
        return "SCSubListModel{" +
                "id='" + id + '\'' +
                ", individualId='" + individualId + '\'' +
                ", subType='" + subType + '\'' +
                ", isdn='" + isdn + '\'' +
                ", verify=" + verify +
                ", createdDateTime='" + createdDateTime + '\'' +
                ", updatedDateTime='" + updatedDateTime + '\'' +
                ", ocsAccount=" + ocsAccount +
                '}';
    }

    public class OcsAccount {

        @SerializedName("voice")
        @Expose
        private double voice;
        @SerializedName("productCode")
        @Expose
        private String productCode;
        @SerializedName("balance")
        @Expose
        private double balance;
        @SerializedName("data")
        @Expose
        private double data;
        @SerializedName("promotion")
        @Expose
        private double promotion;

        public double getVoice() {
            return voice;
        }

        public void setVoice(double voice) {
            this.voice = voice;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public double getBalance() {
            return balance;
        }

        public void setBalance(double balance) {
            this.balance = balance;
        }

        public double getData() {
            return data;
        }

        public void setData(double data) {
            this.data = data;
        }

        public double getPromotion() {
            return promotion;
        }

        public void setPromotion(double promotion) {
            this.promotion = promotion;
        }

    }
}

package com.metfone.selfcare.module.newdetails.interfaces;

import android.widget.FrameLayout;

import com.metfone.selfcare.module.newdetails.ChildDetailNews.adapter.NewsDetailAdapter;
import com.metfone.selfcare.module.newdetails.model.BottomSheetModel;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;

public interface AbsInterface {

    public interface OnItemListener {
        void onItemClick(int pos);
        void onItemMoreClick(int pos);
    }

    public interface OnItemSettingCategoryListener {
        void onItemRemoveClick(int pos);
        void onItemAddClick(int pos);
        void onItemClickFavourite(CategoryModel categoryModel, int pos);
    }

    public interface OnClickBottomSheet {
        void onClickSheet(BottomSheetModel item);
    }

    public interface OnHomeNewsItemListener {
        void onItemClick(NewsModel model);
        void onItemCategoryHeaderClick(int type, int categoryId, String categoryName);
        void onItemListenClick(NewsModel model);
        void onItemEventClick(NewsModel model);
        void onItemEventHeaderClick();
        void onItemCategoryClick(String cateName, String cateId);
        void onItemClickVideo(NewsModel model);
        void onItemClickBtnMore(NewsModel model);
    }

    public interface OnRadioItemListener {
        void onItemClick(NewsModel model);
        void onItemHeaderListenClick(int pos);
        void onItemMoreClick(NewsModel model);
    }

    public interface OnNewsHotListener {
        void onClickImageItem(NewsDetailModel entry);
        void onClickVideoItem(NewsDetailModel entry, FrameLayout view, int pos);
        void onClickRelateItem(NewsModel entry, int type);
        void onClickListenItem(NewsModel entry);
        void onClickSourceNewsItem(String name, String id);
        void onClickLinkItem(int type, NewsModel entry);
        void onPlayVideoDetail(NewsDetailModel model, int position, NewsDetailAdapter.NewsHotDetailVideoHolder holder);
        void onDetachVideoDetail();
        void onItemClickBtnMore(NewsModel model);
//        void onSendComment(TagsCompletionView input);
    }

    public interface OnTopNowListener {
        void onItemClick(NewsModel model);
        void onItemHeaderClick(int pos);
        void onItemHeaderToNativeClick(int pos);
    }

    public interface OnEventListener {
        void onItemClick(NewsModel pos);
        void onItemEventClick(int pos);
        void onItemClickMore(NewsModel model);
    }

    public interface OnNewsListener {
        void onItemClick(int pos);
        void onItemRadioClick(NewsModel model);
        void onItemClickMore(NewsModel model);
    }

    public interface OnSettingTopNowListener {
        void onItemClick(CategoryModel model);
        void onItemFollowClick(CategoryModel model);
    }
    public interface OnTabNewItemListener{
        void onItemClickFavourite(CategoryModel model);
    }
    interface  OnTabNewChangeRefresh{
        void onItemRefresh(int position);
    }
    interface OnEventDetailListener{
        void onItemClickMore(NewsModel model);
        void onItemClick(NewsModel pos);
    }
}

package com.metfone.selfcare.module.home_kh.api;

import java.io.Serializable;

public class WsGiftInfoResponse {
    private String errorCode;
    private Object errorMessage;
    private Result result;

    public String getErrorCode() { return errorCode; }
    public void setErrorCode(String value) { this.errorCode = value; }

    public Object getErrorMessage() { return errorMessage; }
    public void setErrorMessage(Object value) { this.errorMessage = value; }

    public Result getResult() { return result; }
    public void setResult(Result value) { this.result = value; }

    public class Result implements Serializable {
        private Long status;
        private String code;
        private String message;
        private Object reward;
        private Object prize;
        private Long countSpin;
        private Long countTotal;
        private Double totalAmount;
        private Object prizeInfoList;

        public Long getStatus() { return status; }
        public void setStatus(Long value) { this.status = value; }

        public String getCode() { return code; }
        public void setCode(String value) { this.code = value; }

        public String getMessage() { return message; }
        public void setMessage(String value) { this.message = value; }

        public Object getReward() { return reward; }
        public void setReward(Object value) { this.reward = value; }

        public Object getPrize() { return prize; }
        public void setPrize(Object value) { this.prize = value; }

        public Long getCountSpin() { return countSpin; }
        public void setCountSpin(Long value) { this.countSpin = value; }

        public Long getCountTotal() { return countTotal; }
        public void setCountTotal(Long value) { this.countTotal = value; }

        public Double getTotalAmount() {
            return totalAmount;
        }
        public void setTotalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Object getPrizeInfoList() { return prizeInfoList; }
        public void setPrizeInfoList(Object value) { this.prizeInfoList = value; }
    }
}

package com.metfone.selfcare.module.home_kh.account;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.CallBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.contact.StrangerDetailFragmentNew;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.home_kh.activity.SettingKhActivity;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

// ContactDetailActivity
public class AccountDetailActivity extends BaseSlidingFragmentActivity implements StrangerDetailFragmentNew.OnStrangerFragmentInteractionListener {

    private Unbinder unbinder;
    private ApplicationController mApplication;
    private StrangerBusiness mStrangerBusiness;
    private ContactBusiness mContactBusiness;
    private ImageView mImgAvatar;
    private TextView mTvwAvatar;
    private String urlAvatarTmp = "";

    private StrangerPhoneNumber mStrangerPhoneNumber;
    private NonContact mNonContact;
    private String mFriendJidNumber;
    private String mFriendName;// dung cho chat nguoi la tu mocha
    private String mFriendChangeAvatar;
    private String mFriendStatus;
    private String mFriendBirthDay;
    private int mFriendGender;
    private boolean isStrangerOtherApp;
    protected int type = NumberConstant.TYPE_STRANGER_EXIST;
    public CallBusiness callBusiness;

    private AppCompatTextView tvName;
    private AppCompatTextView tvPhone;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setColorStatusBar(R.color.KH_account_detail_header);
        changeLightStatusBar();
        setContentView(R.layout.activity_account_detail_kh);
        unbinder = ButterKnife.bind(this);
        mApplication = (ApplicationController) getApplication();
        mContactBusiness = mApplication.getContactBusiness();
        mStrangerBusiness = mApplication.getStrangerBusiness();
        callBusiness = ApplicationController.self().getCallBusiness();
        getData();
        initView();
    }

    private void initView(){
        mImgAvatar = findViewById(R.id.ivAvatar);
        mTvwAvatar = findViewById(R.id.tvAvatar);

        tvName = findViewById(R.id.tvName);
        tvPhone = findViewById(R.id.tvPhone);

        drawStrangerMochaDetail();
    }

    @Override
    protected void onPause() {
//        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//        if (inputMethodManager != null) {
//            inputMethodManager.hideSoftInputFromWindow(etContent.getWindowToken(), 0);
//        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @OnClick(R.id.icBackToolbar)
    public void onIvBackClicked() {
        finish();
    }

    @OnClick({R.id.btnCall, R.id.btnSms, R.id.btnAddFriend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCall:
                processCall();
                break;
            case R.id.btnSms:
                processSms();
                break;
            case R.id.btnAddFriend:
                processAddFriend();
                break;
        }
    }

    private void processAddFriend() {
    }

    private void processSms() {
        if (mApplication != null && mApplication.getStrangerBusiness() != null) {
            ThreadMessage threadMessage = mApplication.getStrangerBusiness().createMochaStrangerAndThread(mFriendJidNumber,
                    mFriendName, mFriendChangeAvatar, Constants.CONTACT.STRANGER_MOCHA_ID, true);
            navigateToThreadDetail(threadMessage);
        }
    }

    private void processCall() {
        callBusiness.checkAndStartCall(this, mFriendJidNumber);
    }

    private void drawStrangerMochaDetail() {
        Log.i(TAG, "drawStrangerMochaDetail");
        int size = (int) getResources().getDimension(R.dimen.avatar_small_size);
//        setTextButtonSticky();
        tvName.setText(mFriendName);
//        mTvwProfileNameToolbar.setText(mFriendName);
        if (mNonContact != null) {
            mApplication.getAvatarBusiness().setStrangerAvatar(mImgAvatar, mTvwAvatar, null,
                    mFriendJidNumber, mFriendName, mNonContact.getLAvatar(), size);
//            mApplication.getAvatarBusiness().setStrangerAvatar(mImgToolBarAvatar, mTvwToolBarAvatar, null,
//                    mFriendJidNumber, mFriendName, mNonContact.getLAvatar(), size);
            urlAvatarTmp = mApplication.getAvatarBusiness().getAvatarUrl(mNonContact.getLAvatar(),
                    mFriendJidNumber, size);
        } else {
            mApplication.getAvatarBusiness().setStrangerAvatar(mImgAvatar, mTvwAvatar,
                    mStrangerPhoneNumber, mFriendJidNumber, mFriendName, mFriendChangeAvatar, size);
//            mApplication.getAvatarBusiness().setStrangerAvatar(mImgToolBarAvatar, mTvwToolBarAvatar,
//                    mStrangerPhoneNumber, mFriendJidNumber, mFriendName, mFriendChangeAvatar, size);
            urlAvatarTmp = mApplication.getAvatarBusiness().getAvatarUrl(mFriendChangeAvatar,
                    mFriendJidNumber, size);
        }
//        drawStatusAndGender();
//        drawCoverAndAlbums();
//        if (!getContact && !isRequesting) {
//            getInfoContactFromNumber(mFriendJidNumber);
//        }
        tvPhone.setText("" + mFriendJidNumber);
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //param1 - contactId
            mFriendJidNumber = bundle.getString(StrangerConstant.STRANGER_JID_NUMBER);
            mFriendName = bundle.getString(NumberConstant.NAME);
            mFriendChangeAvatar = bundle.getString(NumberConstant.LAST_CHANGE_AVATAR);
            mFriendStatus = bundle.getString(NumberConstant.STATUS);
            mFriendBirthDay = bundle.getString(NumberConstant.BIRTHDAY_STRING);
            //get gender default tra ve -1 de ko tu set gioi tinh nu
            mFriendGender = bundle.getInt(NumberConstant.GENDER, -1);
            Log.i(TAG, "getData mFriendGender =" + mFriendGender);
            //stranger detail
            mStrangerPhoneNumber = mStrangerBusiness.getExistStrangerPhoneNumberFromNumber(mFriendJidNumber);
            mNonContact = mContactBusiness.getExistNonContact(mFriendJidNumber);
            /*if (type == NumberConstant.TYPE_STRANGER_MOCHA || mNonContact == null) {// chua co noncontact thi goi
            request len sv de lay thong tin
                mContactBusiness.getInfoContactFromNumber(mFriendJidNumber);
            }*/
            /* if (mNonContact == null)
                mContactBusiness.getInfoNumber(mFriendJidNumber);*/
            if (TextUtils.isEmpty(mFriendName)) {
                mFriendName = Utilities.hidenPhoneNumber(mFriendJidNumber);
            }
            isStrangerOtherApp = mStrangerPhoneNumber != null && type == NumberConstant.TYPE_STRANGER_EXIST &&
                    mStrangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger;
        }
    }

    @Override
    public void navigateToReengChatActivity(String number) {

    }

    @Override
    public void navigateToThreadDetail(ThreadMessage threadMessage) {
        NavigateActivityHelper.navigateToChatDetail(this, threadMessage);
    }

    @Override
    public void saveContact(String number, String name) {

    }

    @Override
    public void openListImage(ArrayList<ImageProfile> imageProfiles, String name, String jIdNumber) {

    }
}

package com.metfone.selfcare.module.tiin.childtiin.presenter;

import com.metfone.selfcare.module.tiin.base.MvpPresenter;
import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.module.tiin.network.request.TiinRequest;

import java.util.List;

public interface IChildTiinMvpPresenter  extends MvpPresenter{
    void getCategoryNew(int categoryId, int page, int num, long unixTime);
}

package com.metfone.selfcare.module.selfcare.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.selfcare.model.SCBundle;

public class SCAccountDetailFragment extends BaseFragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView btnBack;
    private ViewPagerDetailAdapter adapter;
    private int selectTab = 0;

    public static SCAccountDetailFragment newInstance(Bundle bundle) {
        SCAccountDetailFragment fragment = new SCAccountDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCAccountDetailFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_account_detail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        
        initView(view);

        loadData();

        return view;
    }

    private void initView(View view) {
        btnBack = view.findViewById(R.id.btnBack);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);

        adapter = new ViewPagerDetailAdapter(getChildFragmentManager());
        adapter.addFragment(SCAccountDetailInfoFragment.newInstance(), mActivity.getString(R.string.sc_account));
        adapter.addFragment(SCAccountDetailChargeFragment.newInstance(), mActivity.getString(R.string.sc_account_search));

        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
    }

    private void loadData()
    {
        Bundle bundle = getArguments();
        Object scBundle = bundle.getSerializable(Constants.KEY_DATA);
        if(scBundle != null && scBundle instanceof SCBundle)
        {
            selectTab = ((SCBundle)scBundle).getType();
            viewPager.setCurrentItem(selectTab);
        }
    }
}

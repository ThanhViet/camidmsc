package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

/**
 * ViewHolder for History charge Exchange, Basic, Promotion: My services
 * layout: item_history_charge_3
 */
public class AccountDetailViewHolder extends RecyclerView.ViewHolder {
    public RelativeLayout mLayout;
    public AppCompatTextView mTitle;
    public AppCompatTextView mValue;
    public AppCompatTextView mExp;

    public AccountDetailViewHolder(View view) {
        super(view);
        mLayout = view.findViewById(R.id.layout_account_detail_item);
        mTitle = view.findViewById(R.id.txt_detail_account_title);
        mExp = view.findViewById(R.id.txt_detail_account_exp);
        mValue = view.findViewById(R.id.txt_detail_account_value);
    }
}
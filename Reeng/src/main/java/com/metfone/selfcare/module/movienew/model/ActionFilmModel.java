package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ActionFilmModel {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("result")
    @Expose
    private ArrayList<HomeData> homeResult;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<HomeData> getHomeResult() {
        return homeResult;
    }

    public void setHomeResult(ArrayList<HomeData> homeResult) {
        this.homeResult = homeResult;
    }
}

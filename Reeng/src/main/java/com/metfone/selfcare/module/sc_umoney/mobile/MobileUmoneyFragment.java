package com.metfone.selfcare.module.sc_umoney.mobile;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.sc_umoney.UMoneyStateProvider;
import com.metfone.selfcare.module.sc_umoney.main_umoney.TabUmoneyActivity;
import com.metfone.selfcare.module.sc_umoney.network.UMoneyApi;
import com.metfone.selfcare.module.sc_umoney.network.model.FieldMap;
import com.metfone.selfcare.module.sc_umoney.network.model.Info;
import com.metfone.selfcare.module.sc_umoney.network.request.TopUpRequest;
import com.metfone.selfcare.module.sc_umoney.network.response.FieldMapResponse;
import com.metfone.selfcare.module.selfcare.event.SCRechargeEvent;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MobileUmoneyFragment extends Fragment implements View.OnClickListener {
    TextView tvTopUp;
    TabUmoneyActivity activity;
    EditText edtSdt, edtDes;
    AutoCompleteTextView edtAmount;
    TextView tvAmount1, tvAmount2, tvAmount3, tvAmount4, tvAmount5, tvAmount6;
    LinearLayout llFrame;
    ImageView ivContact;

    UMoneyApi uMoneyApi;
    List<FieldMap> dataField = new ArrayList<>();
    String roleId = "", panCode = "", accountId = "", serviceType = "", subType = "";
    List<String> data = new ArrayList<>();
    ArrayAdapter adapterAmount;
    private String mCurrentRegionCode = "LA";// mac dinh la
    private boolean checkSdt = false, checkAmount = false;
    private Info mInfo;

    public static MobileUmoneyFragment newInstance() {

        Bundle args = new Bundle();

        MobileUmoneyFragment fragment = new MobileUmoneyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sc_umoney_mobile, container, false);
        activity = (TabUmoneyActivity) getActivity();
        initView(view);
        loadData();
        initEvent();
        return view;
    }

    private void loadData() {
        edtSdt.setText(ApplicationController.self().getReengAccountBusiness().getCurrentAccount().getJiNumberLao());
        uMoneyApi = new UMoneyApi(ApplicationController.self());
        dataField = UMoneyStateProvider.getInstance().getData();
        mInfo = UMoneyStateProvider.getInstance().getInfo();
        if (dataField.size() > 0) {
            for (FieldMap model : dataField) {
                if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.ROLE_ID)) {
                    roleId = model.getValue();
                    continue;
                }
                if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.PAN)) {
                    panCode = model.getValue();
                    continue;
                }
                if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.ACCOUNT_ID)) {
                    accountId = model.getValue();
                }
            }
        }
        if (edtSdt.getText().toString().trim().length() > 8 && edtSdt.getText().toString().trim().length() < 14) {
            edtSdt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sc_complete, 0);
        }
    }

    private void initView(View view) {
        tvTopUp = view.findViewById(R.id.tv_top_up);
        edtAmount = view.findViewById(R.id.edt_amount);
        edtSdt = view.findViewById(R.id.edt_sdt);
        edtDes = view.findViewById(R.id.edt_des);
        tvAmount1 = view.findViewById(R.id.tv_amount_1);
        tvAmount2 = view.findViewById(R.id.tv_amount_2);
        tvAmount3 = view.findViewById(R.id.tv_amount_3);
        tvAmount4 = view.findViewById(R.id.tv_amount_4);
        tvAmount5 = view.findViewById(R.id.tv_amount_5);
        tvAmount6 = view.findViewById(R.id.tv_amount_6);
        llFrame = view.findViewById(R.id.ll_frame);
        ivContact = view.findViewById(R.id.iv_contact);
    }

    private void initEvent() {
        tvAmount1.setOnClickListener(this);
        tvAmount2.setOnClickListener(this);
        tvAmount3.setOnClickListener(this);
        tvAmount4.setOnClickListener(this);
        tvAmount5.setOnClickListener(this);
        tvAmount6.setOnClickListener(this);
        tvTopUp.setOnClickListener(this);
        ivContact.setOnClickListener(this);
        InputMethodUtils.hideKeyboardWhenTouch(llFrame, activity);
        edtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edtAmount.setSelected(false);
                if (s.toString().trim().length() >= 4) {
                    edtAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sc_complete, 0);
//                    checkAmount = true;
//                    if(checkSdt){
//                        tvTopUp.setEnabled(true);
//                    }else{
//                        tvTopUp.setEnabled(false);
//                    }
                } else {
                    edtAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//                    checkAmount = false;
//                    tvTopUp.setEnabled(false);
                }
//                long amount = TextHelper.convertTextDecemberToLongUMoney(s.toString().trim(), 0);

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1 && s.length() < 4) {
                    data = getSuggestAmount(Integer.parseInt(s.toString()));
                    if (data != null) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                adapterAmount = new ArrayAdapter(activity, android.R.layout.simple_list_item_1, data);
                                edtAmount.setAdapter(adapterAmount);
                                adapterAmount.notifyDataSetChanged();
                            }
                        });
                    }
                }
                edtAmount.removeTextChangedListener(this);
                try {
                    String input = s.toString().trim();
                    if (TextUtils.isEmpty(input)) {
                        edtAmount.addTextChangedListener(this);
                        return;
                    }
                    int cp = edtAmount.getSelectionStart();
                    int inLen = input.length();
                    String output = TextHelper.formatTextDecemberUMoney(input);
                    edtAmount.setText(output);
                    int outLen = edtAmount.getText().length();
                    int sel = (cp + (outLen - inLen));
                    if (sel > 0 && sel <= outLen) {
                        edtAmount.setSelection(sel);
                    } else {
                        edtAmount.setSelection(outLen - 1);
                    }
                } catch (Exception e) {
//                    Log.e(TAG, "Exception", e);
                }
                edtAmount.addTextChangedListener(this);
            }
        });
        edtSdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() >= 8 && s.length() < 14) {
                    edtSdt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sc_complete, 0);
//                    checkSdt = true;
//                    if(checkAmount){
//                        tvTopUp.setEnabled(true);
//                    }else{
//                        tvTopUp.setEnabled(false);
//                    }
                } else {
                    edtSdt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//                    checkSdt = false;
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtSdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtSdt.setSelected(false);
            }
        });
        edtAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtAmount.setSelected(false);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_amount_1:
                edtAmount.setText("5000");
                tvAmount1.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                tvAmount2.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount3.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount4.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount5.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount6.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount1.setSelected(true);
                tvAmount2.setSelected(false);
                tvAmount3.setSelected(false);
                tvAmount4.setSelected(false);
                tvAmount5.setSelected(false);
                tvAmount6.setSelected(false);
                break;
            case R.id.tv_amount_2:
                edtAmount.setText("10000");
                tvAmount2.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                tvAmount1.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount3.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount4.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount5.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount6.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount1.setSelected(false);
                tvAmount2.setSelected(true);
                tvAmount3.setSelected(false);
                tvAmount4.setSelected(false);
                tvAmount5.setSelected(false);
                tvAmount6.setSelected(false);
                break;
            case R.id.tv_amount_3:
                edtAmount.setText("20000");
                tvAmount3.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                tvAmount1.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount2.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount4.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount5.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount6.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount1.setSelected(false);
                tvAmount2.setSelected(false);
                tvAmount3.setSelected(true);
                tvAmount4.setSelected(false);
                tvAmount5.setSelected(false);
                tvAmount6.setSelected(false);
                break;
            case R.id.tv_amount_4:
                edtAmount.setText("50000");
                tvAmount4.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                tvAmount1.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount2.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount3.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount5.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount6.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount1.setSelected(false);
                tvAmount2.setSelected(false);
                tvAmount3.setSelected(false);
                tvAmount4.setSelected(true);
                tvAmount5.setSelected(false);
                tvAmount6.setSelected(false);
                break;
            case R.id.tv_amount_5:
                edtAmount.setText("100000");
                tvAmount5.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                tvAmount1.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount2.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount3.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount4.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount6.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount1.setSelected(false);
                tvAmount2.setSelected(false);
                tvAmount3.setSelected(false);
                tvAmount4.setSelected(false);
                tvAmount5.setSelected(true);
                tvAmount6.setSelected(false);
                break;
            case R.id.tv_amount_6:
                edtAmount.setText("500000");
                tvAmount6.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                tvAmount1.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount2.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount3.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount4.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount5.setTextColor(activity.getResources().getColor(R.color.sc_bg_umoney));
                tvAmount1.setSelected(false);
                tvAmount2.setSelected(false);
                tvAmount3.setSelected(false);
                tvAmount4.setSelected(false);
                tvAmount5.setSelected(false);
                tvAmount6.setSelected(true);
                break;
            case R.id.tv_top_up:
                try {
                    long amount = TextHelper.convertTextDecemberToLongUMoney(edtAmount.getText().toString().trim(), 0);
                    if (checkInfo() && checkMoney(amount)) {
                        showDialogAccept();
                    }
                } catch (Exception e) {
                    Log.e("--Duong", "Nhap sai dinh dang money");
                }
                break;
            case R.id.iv_contact:
                Intent chooseFriend = new Intent(activity, ChooseContactActivity.class);
                chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_SELFCARE_RECHARGE);
                activity.startActivity(chooseFriend, true);
                break;
        }
    }

    private boolean checkMoney(long money) {
        if (money >= 3000 && money <= 5000000) {
            return true;
        } else {
            edtAmount.requestFocus();
            edtAmount.setSelected(true);
            edtAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_validate, 0);
            Toast.makeText(ApplicationController.self(), activity.getResources().getString(R.string.u_The_minimum_topup_is_3_5), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void showDialogPin() {
        Dialog dialogPin = new Dialog(activity, R.style.SCDialogFullScreen);
        dialogPin.setContentView(R.layout.dialog_sc_umoney_pin);
        TextView tvCancel, tvNext, tvForgetPin;
        EditText edtPin;
        tvCancel = dialogPin.findViewById(R.id.tv_cancel);
        tvNext = dialogPin.findViewById(R.id.tv_next);
        edtPin = dialogPin.findViewById(R.id.edt_pin_code);
        tvForgetPin = dialogPin.findViewById(R.id.tv_forget_pin);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPin.dismiss();
            }
        });
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin != null && !TextUtils.isEmpty(edtPin.getText())) {
                    loadTopUpUmoney(edtPin.getText().toString().trim());
                    dialogPin.dismiss();
                } else {
                    edtPin.requestFocus();
                    Toast.makeText(ApplicationController.self(), activity.getResources().getString(R.string.u_Please_input_your_password), Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvForgetPin.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                showDialogForgetPin();
            }
        });
        dialogPin.setCancelable(false);
        dialogPin.setCanceledOnTouchOutside(false);
        dialogPin.show();
    }

    private void showDialogAccept() {
        Dialog dialogAccept = new Dialog(activity, R.style.SCDialogFullScreen);
        dialogAccept.setContentView(R.layout.dialog_sc_umoney_accept);
        TextView tvCancel, tvAccept;
        TextView tvSdt, tvMoney, tvDes;
        tvCancel = dialogAccept.findViewById(R.id.tv_cancel);
        tvAccept = dialogAccept.findViewById(R.id.tv_accept);
        tvSdt = dialogAccept.findViewById(R.id.tv_sdt);
        tvMoney = dialogAccept.findViewById(R.id.tv_amount);
        tvDes = dialogAccept.findViewById(R.id.tv_des);
        tvSdt.setText(": " + edtSdt.getText().toString().trim());
        tvMoney.setText(": " + edtAmount.getText().toString().trim() + " LAK");
        tvDes.setText(": " + edtDes.getText());
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAccept.dismiss();
            }
        });
        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogPin();
                dialogAccept.dismiss();
            }
        });
        dialogAccept.setCancelable(false);
        dialogAccept.setCanceledOnTouchOutside(false);
        dialogAccept.show();
    }

    private void loadTopUpUmoney(String pinCode) {
        TopUpRequest request = new TopUpRequest();
        long amount = TextHelper.convertTextDecemberToLongUMoney(edtAmount.getText().toString().trim(), 0);
        request.setAmount(amount + "");
        request.setRoleId(roleId);
        request.setActionNote("1");
        request.setPanCode(panCode);
        request.setPinCode(pinCode);
        request.setMsisdnReceiv(edtSdt.getText().toString().trim());
        request.setAccountId(accountId);
        if(mInfo != null){
            request.setServiceType(mInfo.getServiceType()+"");
            request.setSubType(mInfo.getSubType()+"");
        }
        uMoneyApi.postTopup(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                Gson gson = new Gson();
                FieldMapResponse response = gson.fromJson(data, FieldMapResponse.class);
                if (response != null) {
                    if (response.getCode() == 200) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Constants.UMONEY.UMONEY_COMPLETE, response.getData());
                        activity.showFragment(SCConstants.UMONEY.TAB_COMPLETE, bundle, false);
//                    } else if (response.getCode() == 10118) {
//                        Toast.makeText(activity, activity.getString(R.string.u_10118), Toast.LENGTH_LONG).show();
//                    } else if (response.getCode() == 10155) {
//                        Toast.makeText(activity, activity.getString(R.string.u_10155), Toast.LENGTH_LONG).show();
//                    } else if (response.getCode() == 10156) {
//                        Toast.makeText(activity, activity.getString(R.string.u_10156), Toast.LENGTH_LONG).show();
//                    } else if (response.getCode() == 10113) {
//                        Toast.makeText(activity, activity.getString(R.string.u_10113), Toast.LENGTH_LONG).show();
//                    } else if (response.getCode() == 10532) {
//                        Toast.makeText(activity, activity.getString(R.string.u_10532), Toast.LENGTH_LONG).show();
//                    } else if (response.getCode() == 405) {
//                        Toast.makeText(activity, activity.getString(R.string.u_error_phone_405), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ApplicationController.self(), response.getDesc(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                Toast.makeText(ApplicationController.self(), activity.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
            }
        }, request);
    }

    private boolean checkInfo() {
        if (TextUtils.isEmpty(edtSdt.getText().toString().trim())) {
            edtSdt.requestFocus();
            edtSdt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_validate, 0);
            edtSdt.setSelected(true);
            Toast.makeText(ApplicationController.self(), activity.getString(R.string.u_error_phone_validate), Toast.LENGTH_LONG).show();
            return false;
        } else {
            if (edtSdt.getText().toString().trim().length() < 8 || edtSdt.getText().toString().trim().length() >= 14) {
                edtSdt.requestFocus();
                edtSdt.setSelected(true);
                edtSdt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_validate, 0);
                Toast.makeText(ApplicationController.self(), activity.getString(R.string.u_error_phone), Toast.LENGTH_LONG).show();
                return false;
            }
            //todo +856209, +856309, +856304
            String number = edtSdt.getText().toString().trim();
            String s = number.substring(0, 1);
            if (!s.equals("0")) {
                number = "0" + number;
            }
            number = number.replaceFirst("0", "+856");
            if (!PhoneNumberHelper.getInstant().isViettelNumber(number)) {
                if (!number.contains("+856209") || !number.contains("+856309") || !number.contains("+856304")) {
                    Toast.makeText(ApplicationController.self(), activity.getString(R.string.u_error_not_unitel), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }


//            String numberInput = edtSdt.getText().toString().trim();
            PhoneNumberUtil phoneUtil = ApplicationController.self().getPhoneUtil();
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                    getPhoneNumberProtocol(phoneUtil,
                            number, mCurrentRegionCode);
            if (!PhoneNumberHelper.getInstant().isValidPhoneNumber(phoneUtil, phoneNumberProtocol)) {
                Toast.makeText(ApplicationController.self(), activity.getString(R.string.u_error_not_unitel), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if (TextUtils.isEmpty(edtAmount.getText().toString().trim())) {
            edtAmount.requestFocus();
            edtAmount.setSelected(true);
            edtAmount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_validate, 0);
            Toast.makeText(ApplicationController.self(), activity.getString(R.string.u_error_amount_validate), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(final SCRechargeEvent event) {
        if (event != null) {
            if (!TextUtils.isEmpty(event.getPhoneNumber()))
                edtSdt.setText(SCUtils.formatPhoneNumber(event.getPhoneNumber()));

            EventBus.getDefault().removeStickyEvent(event);
        }
    }

    private List<String> getSuggestAmount(int amount) {
        List<String> data = new ArrayList<>();
        int a = amount * 1000;
        int b = amount * 10000;
        data.add(a + "");
        data.add(b + "");
        return data;
    }

    private void showDialogForgetPin() {
        DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
        dialogConfirm.setLabel(activity.getString(R.string.Information_Umoney));
        dialogConfirm.setMessage(activity.getString(R.string.Please_dial_168_or_0309168168_to_get_support));
        dialogConfirm.setNegativeLabel("OK");
        dialogConfirm.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {

            }
        });
        dialogConfirm.show();
    }
}

package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.HomeResult;

import java.util.List;

public class WsGetListFilmForUserResponse {
    public String errorCode;
    public String errorMessage;
    @SerializedName("result")
    public Response response;

    public static class Response{
        @SerializedName("result")
        public List<HomeData> result;
    }
}

package com.metfone.selfcare.module.backup_restore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class BackupThreadMessageModel {

    /*
    * {
   "name":"thread_name",
   "state":1,												// -1: get lại info (dùng trong tab group), 0- hiển thị ngoài inbox, 1- không hiển thị
   "type":1,												// 0-chat 11, 1-chat group, 2-official, 3-room, 4-broastcast
   "svId":"server id",
   "numbers":"danh sách sdt cách nhau dấu phảy, chat đơn thì chỉ có 1 sdt",
   "unread":5,												// số tin chưa đọc
   "tChange":"last time change",
   "draft":"tn nháp",
   "tDraft":"time save draft",
   "avt":"avatar group",
   "lastMsgId":"last message id",
   "admins":"danh sach admin cách nhau dấu phảy",
   "pin":{													//json thông tin pin
      "title":"0326695346",
      "type":1,
      "target":"0202001_AA649C670D794D57914FCF7CF5137EBC",
      "content":"Lại có bug rồi anh"
   },
   "cls":0												//dùng trong kịch bản class, 1 là class, 0 là group thường
}*/

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("state")
    @Expose
    int state;

    @SerializedName("type")
    @Expose
    int type;

    @SerializedName("svId")
    @Expose
    String svId;

    @SerializedName("numbers")
    @Expose
    String numbers;

    @SerializedName("unread")
    @Expose
    int unread;

    @SerializedName("tChange")
    @Expose
    long tChange;

    @SerializedName("draft")
    @Expose
    String draft;

    @SerializedName("tDraft")
    @Expose
    long tDraft;

    @SerializedName("avt")
    @Expose
    String avt;

    @SerializedName("admins")
    @Expose
    String admins;

    @SerializedName("pin")
    @Expose
    String pin;

    @SerializedName("cls")
    @Expose
    int cls;


    @SerializedName("isStranger")
    @Expose
    int isStranger;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSvId() {
        return svId;
    }

    public void setSvId(String svId) {
        this.svId = svId;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public int getUnread() {
        return unread;
    }

    public void setUnread(int unread) {
        this.unread = unread;
    }

    public long gettChange() {
        return tChange;
    }

    public void settChange(long tChange) {
        this.tChange = tChange;
    }

    public String getDraft() {
        return draft;
    }

    public void setDraft(String draft) {
        this.draft = draft;
    }

    public long gettDraft() {
        return tDraft;
    }

    public void settDraft(long tDraft) {
        this.tDraft = tDraft;
    }

    public String getAvt() {
        return avt;
    }

    public void setAvt(String avt) {
        this.avt = avt;
    }

    public String getAdmins() {
        return admins;
    }

    public void setAdmins(String admins) {
        this.admins = admins;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getCls() {
        return cls;
    }

    public void setCls(int cls) {
        this.cls = cls;
    }

    public int getIsStranger() {
        return isStranger;
    }

    public void setIsStranger(int isStranger) {
        this.isStranger = isStranger;
    }
}

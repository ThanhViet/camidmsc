package com.metfone.selfcare.module.selfcare.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.adapter.SCPackageAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.event.SCAccountEvent;
import com.metfone.selfcare.module.selfcare.model.SCBundle;
import com.metfone.selfcare.module.selfcare.model.SCDeeplink;
import com.metfone.selfcare.module.selfcare.model.SCPackage;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCPackage;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.HyperlinkListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;

public class SCRecommentPackageFragment extends BaseFragment implements AbsInterface.OnPackageRegisterListener, SwipeRefreshLayout.OnRefreshListener {

    private LoadingViewSC loadingView;
    private RecyclerView recyclerView;
    private SCPackageAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int type;

    private ArrayList<SCPackage> data = new ArrayList<>();

    public static SCRecommentPackageFragment newInstance(Bundle bundle) {
        SCRecommentPackageFragment fragment = new SCRecommentPackageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCRecommentPackageFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_recomment_package;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

        loadData();

        return view;
    }

    private void initView(View view) {
        Bundle bundle = getArguments();
        SCBundle data = (SCBundle) bundle.getSerializable(Constants.KEY_DATA);
        type = data.getType();

        layout_refresh = view.findViewById(R.id.refresh);
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.sc_primary));
            layout_refresh.setOnRefreshListener(this);
        }
        loadingView = view.findViewById(R.id.loading_view);
        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCPackageAdapter(mActivity, this);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);

        loadingView.setLoadingErrorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login lai
//                Intent intent = new Intent(getActivity(), SCAccountActivity.class);
//                mActivity.startActivity(intent);
            }
        });
    }

    private void loadData() {
        if (!isRefresh)
            loadingView.loadBegin();
        //Load account detail info
        final WSSCRestful restful = new WSSCRestful(mActivity);

        if (type == SCConstants.SCREEN_TYPE.TYPE_PACKAGE) {
            restful.getRecommentPackage(new Response.Listener<RestSCPackage>() {
                                            @Override
                                            public void onResponse(RestSCPackage result) {
//                                                super.onResponse(result);
                                                hideRefresh();
                                                if (result != null) {
                                                    if (result.getStatus() == 200 && result.getData() != null) {
                                                        if (result.getData().getData().size() > 0) {
                                                            loadingView.loadFinish();

                                                            data.clear();
                                                            data.addAll(result.getData().getData());

                                                            adapter.setItemsList(data);
                                                            adapter.notifyDataSetChanged();
                                                        } else {
                                                            loadingView.loadEmpty();
                                                        }
                                                    } else if (result.getStatus() == 401 || result.getStatus() == 403) {
                                                        //Login lai
                                                        loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                                                    } else {
                                                        //Fail
                                                        loadingView.loadError();
                                                    }
                                                }
                                            }
                                        },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            hideRefresh();
                            if (volleyError != null && volleyError.networkResponse != null) {
                                int errorCode = volleyError.networkResponse.statusCode;
                                if (errorCode == 401 || errorCode == 403) {
                                    //Login lai
                                    loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                                } else {
                                    //Fail
                                    loadingView.loadError();
                                }
                            } else {
                                //Fail
                                loadingView.loadError();
                            }
                        }
                    });
        }
    }

    @Override
    public void onPackageClick(SCPackage item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        bundle.putInt(SCConstants.PREFERENCE.KEY_FROM_SOURCE, type);
        ((TabSelfCareActivity) mActivity).gotoPackageDetail(bundle);
    }

    @Override
    public void onRegisterClick(final SCPackage packageData, final View btnRegister) {
        if (packageData.getVasDescriptions() != null && packageData.getVasDescriptions().size() > 0 && !TextUtils.isEmpty(packageData.getVasDescriptions().get(0).getPopup())) {
            DialogConfirm dialogConfirm = new DialogConfirm(mActivity, false);
            dialogConfirm.setLabel(mActivity.getString(R.string.confirm));
            dialogConfirm.setMessage(packageData.isRegister() ? packageData.getVasDescriptions().get(0).getUnsPopup() : packageData.getVasDescriptions().get(0).getPopup());
            dialogConfirm.setUseHtml(true);
            dialogConfirm.setNegativeLabel(mActivity.getString(R.string.cancel));
            dialogConfirm.setPositiveLabel(mActivity.getString(R.string.ok));
            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    doRegisterPackage(packageData, btnRegister);
                }
            });
            dialogConfirm.setHyperLinkListener(new HyperlinkListener() {
                @Override
                public void onClickHyperLink(String link) {
                    UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(ApplicationController.self(), mActivity, link);
                }
            });
            dialogConfirm.setButtonTextColor(mActivity.getResources().getColor(R.color.sc_primary));
            if (!dialogConfirm.isShowing())
                dialogConfirm.show();
        } else {
            doRegisterPackage(packageData, btnRegister);
        }
    }

    @Override
    public void onDeeplinkClick(SCDeeplink item) {

    }

    private void doRegisterPackage(final SCPackage packageData, final View btnRegister) {
        if (packageData != null) {
            int actionType = 0;
            if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                actionType = packageData.isRegister() ? 1 : 0;
//                if(actionType == 1)
//                {
//                    ToastUtils.makeText(mActivity, mActivity.getString(R.string.e666_not_support_function));
//                    return;
//                }
            }
            if (btnRegister instanceof TextView) {
                ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_processcing));
                btnRegister.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_gray));
            }

            WSSCRestful restful = new WSSCRestful(mActivity);
            restful.registerPackage(packageData.getCode(), actionType, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int code = jsonObject.optInt("errorCode", -1);
                        String message = jsonObject.optString("message");
                        mActivity.showToast(message);

                        if (code == 200) {
                            EventBus.getDefault().postSticky(new SCAccountEvent(SCAccountEvent.UPDATE_INFO));
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnRegister.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
                                    if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                                        packageData.setRegister(!packageData.isRegister());
                                        if (packageData.isRegister())
                                            ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_unsubcribe));
                                        else
                                            ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_subcribe));
                                    } else {
                                        ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_buy_again));
                                    }
                                }
                            });
                        } else {
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnRegister.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
                                    if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                                        if (packageData.isRegister())
                                            ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_unsubcribe));
                                        else
                                            ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_subcribe));
                                    } else {
                                        ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_register));
                                    }
                                }
                            });
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnRegister.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
                                if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                                    if (packageData.isRegister())
                                        ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_unsubcribe));
                                    else
                                        ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_subcribe));
                                } else {
                                    ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_register));
                                }
                            }
                        });
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mActivity.showToast(volleyError.getMessage());
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnRegister.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sc_button));
                            if (type == SCConstants.SCREEN_TYPE.TYPE_SERVICES) {
                                ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_subcribe));
                            } else {
                                ((TextView) btnRegister).setText(mActivity.getString(R.string.sc_register));
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        loadData();
    }
}

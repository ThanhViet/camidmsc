package com.metfone.selfcare.module.home_kh.fragment.rewardshop;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.WsGetAllPartnerGiftSearchResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.RewardBaseFragment;
import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointContainerFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewards.RewardsKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.adapter.FilterAdapter;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.event.SelectedFilter;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.event.ReloadBottomSpace;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.model.CategoryFilter;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.model.FilterItems;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.home_kh.model.PartnerGiftSearch;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;

//0882989999 / 123456
public class FilterShopFragment extends RewardBaseFragment {
    private Unbinder unbinder;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.container_reward_filter)
    FrameLayout container_reward_filter;
    @BindView(R.id.txtTitleToolbar)
    TextView txtTitleToolbar;
    @BindView(R.id.spaceOfFilter)
    View spaceOfFilter;
    View space_bottom;

    private static final String DATA = "DATA";
    private static final String TYPE = "TYPE";
    private static final String LIST_CATEGORY = "LIST_CATEGORY";
    private static final String RANK_ID = "RANK_ID";
    private static final String SHOW_SPACE = "show_space";
    int positionSelected;
    private List<FilterItems> listFilter = new ArrayList<>();
    private List<CategoryFilter> listCategory = new ArrayList<>();
    private String rankId;
    private int type;
    private FilterAdapter filterAdapter;
    private KhHomeClient homeKhClient;
    private boolean isFromTabHome = false;
    private boolean showSpace = false;

    public static FilterShopFragment newInstance(int type, int position, ArrayList<CategoryFilter> listCategory, String rankId) {
        FilterShopFragment fragment = new FilterShopFragment();
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        args.putInt(DATA, position);
        args.putSerializable(LIST_CATEGORY, listCategory);
        args.putString(RANK_ID, rankId);
        fragment.setArguments(args);
        return fragment;
    }

    public static FilterShopFragment newInstance(int type, int position, ArrayList<CategoryFilter> listCategory, String rankId, boolean showSpace) {
        FilterShopFragment fragment = new FilterShopFragment();
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        args.putInt(DATA, position);
        args.putSerializable(LIST_CATEGORY, listCategory);
        args.putString(RANK_ID, rankId);
        args.putBoolean(SHOW_SPACE, showSpace);
        fragment.setArguments(args);
        return fragment;
    }


    public static final String TAG = FilterShopFragment.class.getSimpleName();

    @Override
    protected boolean needShowBottomMenu() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
        space_bottom = getActivity().findViewById(R.id.space_bottom);
        if (getArguments() != null) {
            showSpace = getArguments().getBoolean(SHOW_SPACE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        if (space_bottom != null && space_bottom.getHeight() > 0) {
            space_bottom.setVisibility(View.GONE);
        } else if (showSpace) {
            ViewGroup.LayoutParams params = spaceOfFilter.getLayoutParams();
            params.height = getNavigationBarHeight();
            spaceOfFilter.setLayoutParams(params);
        }
        return view;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (space_bottom != null) {
            EventBus.getDefault()
                    .post(new ReloadBottomSpace(space_bottom.getHeight()));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(homeKhClient == null){
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.logApp(Constants.LOG_APP.HOME_CATEGORY);
        initData();
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_filter_kh;
    }


    @OnClick(R.id.icBackToolbar)
    void onBack() {
        popBackStackFragment();
    }

    private void initData() {
        initRv();
        Bundle bundle = getArguments();
        if (bundle != null) {
            positionSelected = bundle.getInt(DATA);
            type = bundle.getInt(TYPE);
            if (type == RewardsShopKHFragment.CATEGORY) {
                txtTitleToolbar.setText(getActivity().getString(R.string.kh_category));
                listCategory = (List<CategoryFilter>) bundle.getSerializable(LIST_CATEGORY);
                rankId = bundle.getString(RANK_ID);
                if (listCategory == null || listCategory.isEmpty()) {
                    getDataCategory();
                    isFromTabHome = true;
                } else {
                    initDataCategory();
                    isFromTabHome = false;
                }
            } else {
                txtTitleToolbar.setText(ResourceUtils.getString(R.string.kh_filter));
                initDataFilter();
            }
        }

    }

    private void initDataFilter() {
        listFilter.clear();
        FilterItems all = new FilterItems("1", ResourceUtils.getString(R.string.filter_all), R.drawable.ic_filter_all);
        FilterItems popular = new FilterItems("2", ResourceUtils.getString(R.string.filter_popular), R.drawable.ic_filter_popular);
        FilterItems nearbyYou = new FilterItems("3", ResourceUtils.getString(R.string.filter_nearby_you), R.drawable.ic_filter_nearby_you);
        listFilter.add(all);
        listFilter.add(popular);
        listFilter.add(nearbyYou);
        filterAdapter.setSelected(positionSelected);
        filterAdapter.setType(type);
        filterAdapter.notifyDataSetChanged();
    }

    private void initDataCategory() {
        listFilter.clear();
        logH("initDataCategory > start");
        FilterItems all = new FilterItems("0", ResourceUtils.getString(R.string.filter_all), R.drawable.ic_filter_all);
        FilterItems metfone = new FilterItems("1", ResourceUtils.getString(R.string.metfone), R.drawable.ic_metfone);
        listFilter.add(all);
        listFilter.add(metfone);
        for (int i = 0; i < listCategory.size(); i++) {
            FilterItems filterItem = new FilterItems(String.valueOf(i), listCategory.get(i).getName(),
                    0, listCategory.get(i).getUrl());
            listFilter.add(filterItem);
        }
        filterAdapter.setType(type);
        filterAdapter.setSelected(positionSelected);
        filterAdapter.notifyDataSetChanged();
        logH("initDataCategory > end");
    }

    private void showRewardShops() {
//        FragmentManager mFragmentManager = getParentFragmentManager();
//        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
//        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
//                R.anim.decelerate_slide_out_left_fragment,
//                R.anim.decelerate_slide_in_left_fragment,
//                R.anim.decelerate_slide_out_right
//        );
//        mFragmentTransaction.replace(R.id.tab_home_parent, RewardsShopKHFragment.newInstance(), RewardsShopKHFragment.TAG);
//        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        mFragmentTransaction.addToBackStack(HistoryPointContainerFragment.TAG);
//        mFragmentTransaction.commitAllowingStateLoss();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                R.anim.decelerate_slide_out_left_fragment,
                R.anim.decelerate_slide_in_left_fragment,
                R.anim.decelerate_slide_out_right
        );
//        mFragmentTransaction.replace(R.id.container_reward_filter, RewardsKHFragment.newInstance());
//        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        mFragmentTransaction.addToBackStack(RewardsKHFragment.TAG);
//        mFragmentTransaction.commitAllowingStateLoss();
        mFragmentTransaction.replace(R.id.container_reward_filter, RewardsShopKHFragment.newInstance(null, false), RewardsShopKHFragment.TAG);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(HistoryPointContainerFragment.TAG);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    private void initRv() {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }
        filterAdapter = new FilterAdapter(listFilter, position -> {
            // click metro
            if (position == 1 && type == RewardsShopKHFragment.CATEGORY) {
                showRewards();
            } else {
                if (isFromTabHome) {
                    showRewardShops();
                    rv.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(new SelectedFilter(position, type, filterAdapter.getData().get(position)));
                        }
                    }, 500);

                } else {
                    EventBus.getDefault()
                            .post(new SelectedFilter(position, type, filterAdapter.getData().get(position)));
                    popBackStackFragment();
                }
            }
        }, type);
        LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(activity,
                LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration divider = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
        Drawable mDivider = ContextCompat.getDrawable(activity, R.drawable.reward_divider);
        divider.setDrawable(mDivider);
        rv.setLayoutManager(verticalLayoutManager);
        rv.addItemDecoration(divider);
        rv.setAdapter(filterAdapter);
    }

    private void showRewards() {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                R.anim.decelerate_slide_out_left_fragment,
                R.anim.decelerate_slide_in_left_fragment,
                R.anim.decelerate_slide_out_right
        );
        mFragmentTransaction.replace(R.id.container_reward_filter, RewardsKHFragment.newInstance());
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(RewardsKHFragment.TAG);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    private void getDataCategory() {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        if (rankId != null) {
            homeKhClient.wsGetAllPartnerGiftSearch(new KhApiCallback<KHBaseResponse<WsGetAllPartnerGiftSearchResponse>>() {
                @Override
                public void onSuccess(KHBaseResponse<WsGetAllPartnerGiftSearchResponse> body) {
//                progressBar.setVisibility(View.GONE);
                    logH("wsGetAllPartnerGiftSearch > onSuccess");
                    List<PartnerGiftSearch> list = body.getData().getWsResponse().getObject();
                    listCategory = getCategoryFromGift(list);
                    initDataCategory();
                }

                @Override
                public void onFailed(String status, String message) {
//                progressBar.setVisibility(View.GONE);
                    logH("wsGetAllPartnerGiftSearch > onFailed");
                    logH("wsGetAllPartnerGiftSearch > " + status + " " + message);
                    if (getActivity() != null)
                        com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                }

                @Override
                public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsGetAllPartnerGiftSearchResponse>>> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
                    t.printStackTrace();
                    logH("wsGetAllPartnerGiftSearch > onFailure");
                }
            }, String.valueOf(0), Integer.parseInt(rankId));
        } else {
            logH("getDataCategory > rankId is null");
        }
    }

    private void logH(String m) {
        Log.e("H-FilterShopFragment", m);
    }


    private ArrayList<CategoryFilter> getCategoryFromGift(List<PartnerGiftSearch> items) {
        List<PartnerGiftSearch> listPartnerGift = new ArrayList<>();
        ArrayList<CategoryFilter> categoriesFilter = new ArrayList<>();
        for (PartnerGiftSearch item : items) {
            if (item.getListGiftItems() != null && !item.getListGiftItems().isEmpty()) {
                listPartnerGift.add(item);
            }
        }

        for (PartnerGiftSearch item : listPartnerGift) {
            List<GiftItems> gifts = item.getListGiftItems();
            if (gifts != null && !gifts.isEmpty()) {
                categoriesFilter.add(new CategoryFilter(item.getGiftTypeName(), item.getIconUrl()));
            }
        }
        return categoriesFilter;
    }
}
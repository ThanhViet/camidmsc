package com.metfone.selfcare.module.netnews.CategoryNews.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.appcompat.widget.SwitchCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.netnews.CategoryNews.adapter.CategoryNewsAdapter;
import com.metfone.selfcare.module.netnews.CategoryNews.presenter.CategoryNewsPresenter;
import com.metfone.selfcare.module.netnews.CategoryNews.presenter.ICategoryNewsPresenter;
import com.metfone.selfcare.module.netnews.CategoryNews.view.ICategoryNewsView;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.listener.OnItemClickListener;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.CustomLoadMoreView;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by HaiKE on 8/19/17.
 */

public class CategoryNewsFragment extends BaseFragment implements ICategoryNewsView, BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.loadingView)
    View loadingView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ll_check_view)
    LinearLayout llCheckView;
    @BindView(R.id.sc_new)
    SwitchCompat scNew;

    int currentPage = 1;
    int currentCategory = 0;
    String categoryName = "NetNews";
    CategoryNewsAdapter adapter;
    //    LinearLayoutManager layoutManager;
    ArrayList<NewsModel> datas = new ArrayList<>();
    View notDataView, errorView;
    boolean isRefresh;
    long unixTime;

    ICategoryNewsPresenter mPresenter;

    public static CategoryNewsFragment newInstance(Bundle bundle) {
        CategoryNewsFragment fragment = new CategoryNewsFragment();
        fragment.setArguments(bundle);
        fragment.mPresenter = new CategoryNewsPresenter();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_toolbar, container, false);
        if (mPresenter == null) {
            mPresenter = new CategoryNewsPresenter();
        }
        setUnBinder(ButterKnife.bind(this, view));
        mPresenter.onAttach(this);
        setUp(view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        Bundle bundle = getArguments();
        currentCategory = bundle.getInt(CommonUtils.KEY_CATEGORY_ID, 0);
        categoryName = bundle.getString(CommonUtils.KEY_CATEGORY_NAME);

        tvTitle.setText(categoryName);
        isRefresh = true;
        if (datas == null || (datas.size() == 0)) {
            unixTime = 0;
            loadingView.setVisibility(View.VISIBLE);
            mPresenter.loadData(currentCategory, currentPage, unixTime);
        }

        layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        layout_refresh.setOnRefreshListener(this);
        boolean checkView = SharedPrefs.getInstance().get("CHECK_VIEW", Boolean.class);
        setUpRecyclerView(checkView);
    }

    private void setUpRecyclerView(boolean checkView) {
        if (currentCategory == 3 || currentCategory == 135)//Giai tri
        {
            adapter = new CategoryNewsAdapter(getBaseActivity(), R.layout.holder_large_news, datas, currentCategory);
            if (recyclerView.getItemDecorationCount() <= 0) {
                CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getBaseActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
//            LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity());
//            recyclerView.setLayoutManager(layoutManager);

//            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getBaseActivity(), layoutManager.getOrientation());
//            dividerItemDecoration.setDrawable(getBaseActivity().getResources().getDrawable(R.drawable.divider_vertical));
//            recyclerView.addItemDecoration(dividerItemDecoration);
        } else if (currentCategory == 7)//Nguoi dep
        {
            llCheckView.setVisibility(View.GONE);
            scNew.setChecked(checkView);
            if (!checkView) {
                StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(staggeredGridLayoutManager);
//                adapter.setCheckView(false);
                recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getTop(), getResources().getDimensionPixelOffset(R.dimen.v5_spacing_normal), recyclerView.getPaddingBottom());
                adapter = new CategoryNewsAdapter(getBaseActivity(), R.layout.holder_lady_new, datas, currentCategory);
            } else {
                adapter = new CategoryNewsAdapter(getBaseActivity(), R.layout.holder_normal_news, datas, currentCategory);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity());
                recyclerView.setLayoutManager(layoutManager);
//                adapter.setCheckView(true);
            }
            scNew.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    SharedPrefs.getInstance().put("CHECK_VIEW", isChecked);
                    setUpRecyclerView(isChecked);
                }
            });
        } else {
            if (recyclerView.getItemDecorationCount() <= 0) {
                CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getBaseActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
            adapter = new CategoryNewsAdapter(getBaseActivity(), R.layout.holder_large_news, datas, currentCategory);
        }

        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(this);
        adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                readNews(datas.get(position));
            }
        });

        notDataView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_nodata, (ViewGroup) recyclerView.getParent(), false);
        notDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
        errorView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_failed, (ViewGroup) recyclerView.getParent(), false);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
    }


    @Override
    public void loadDataSuccess(boolean flag) {
        loadingView.setVisibility(View.GONE);
        hideRefresh();
        if (!flag) {
            loadingFail();
        }
    }

    @Override
    public void bindData(NewsResponse response) {
        if (datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if (response != null) {
            if (response.getData() != null) {
                loadingComplete(response.getData());
            } else {
                loadingFail();
            }
        }
    }

    public void loadingComplete(ArrayList<NewsModel> response) {
        int mCurrentCounter = response.size();
        if (mCurrentCounter > 0)
            unixTime = response.get(response.size() - 1).getUnixTime();
        if (isRefresh) {
            if (mCurrentCounter == 0) {
                adapter.setEmptyView(notDataView);
            } else {
                datas.clear();
                adapter.setNewData(response);
                datas.addAll(response);
            }
        } else {
            if (mCurrentCounter == 0) {
                adapter.loadMoreEnd();
            } else {
                adapter.addData(response);
                datas.addAll(response);
                adapter.loadMoreComplete();
            }
        }
    }

    public void loadingFail() {
        if (isRefresh) {
            adapter.setEmptyView(errorView);
        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        unixTime = 0;
        mPresenter.loadData(currentCategory, currentPage, unixTime);
    }

    @Override
    public void onLoadMoreRequested() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                currentPage++;
                isRefresh = false;
                mPresenter.loadData(currentCategory, currentPage, unixTime);
            }
        },1000);

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.btnBack)
    public void onBackClick() {
        getBaseActivity().onBackPressed();
    }

    @Override
    public void paddingView(boolean flag) {
        if (flag)
            recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getPaddingTop(), recyclerView.getPaddingRight(), getResources().getDimensionPixelOffset(R.dimen.bottom_bar_height));
        else
            recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getPaddingTop(), recyclerView.getPaddingRight(), 0);
    }

    @OnClick(R.id.ivSearch)
    public void clickSearch() {
        Utilities.openSearch(getBaseActivity(), Constants.TAB_NEWS_HOME);
    }
}

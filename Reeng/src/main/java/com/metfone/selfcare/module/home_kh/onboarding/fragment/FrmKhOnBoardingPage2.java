package com.metfone.selfcare.module.home_kh.onboarding.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.airbnb.lottie.LottieAnimationView;
import com.metfone.selfcare.R;

public class FrmKhOnBoardingPage2 extends FrmKhOnBoarding {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_onboarding_page2;
    }

    @Override
    protected int getPageIndex() {
        return 1;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LottieAnimationView lt = view.findViewById(R.id.obd_img_camid);
        lt.setProgress(0);
        lt.playAnimation();
    }
}

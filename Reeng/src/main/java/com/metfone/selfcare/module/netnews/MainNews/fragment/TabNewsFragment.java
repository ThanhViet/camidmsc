package com.metfone.selfcare.module.netnews.MainNews.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.netnews.ChildNews.fragment.ChildNewsFragment;
import com.metfone.selfcare.module.netnews.HomeNews.fragment.HomeNewsFragment;
import com.metfone.selfcare.module.netnews.MainNews.adapter.MenuCategoryAdapterV5;
import com.metfone.selfcare.module.netnews.MainNews.adapter.TabNewsAdapter;
import com.metfone.selfcare.module.netnews.MainNews.presenter.ITabNewsPresenter;
import com.metfone.selfcare.module.netnews.MainNews.presenter.TabNewsPresenter;
import com.metfone.selfcare.module.netnews.MainNews.view.ITabNewsView;
import com.metfone.selfcare.module.netnews.activity.NetNewsActivity;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.model.FavouriteModel;
import com.metfone.selfcare.module.newdetails.model.SettingCategoryModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.response.CategoryResponse;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.NET_NEWS;

/**
 * Created by HaiKE on 8/18/17.
 */

public class TabNewsFragment extends BaseFragment implements ITabNewsView, AbsInterface.OnTabNewItemListener {

    public static int DEFAULT_TAB_COUNT = 0; //"Home", "Mới nhất", "World cup", //"Tin nóng"
    @BindView(R.id.btnSwitch)
    Switch btnSwitch;
    //    @BindView(R.id.iv_logo)
//    ImageView ivLogo;
    @BindView(R.id.iv_search)
    ImageView btnSearch;
    @BindView(R.id.btnConfig)
    ImageView btnConfig;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.headerController)
    LinearLayout headerController;
    @BindView(R.id.ll_recycler_category)
    LinearLayout llRecyclerCategory;
    @BindView(R.id.ic_menu_category)
    ImageView ivMenuCategory;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_new_home)
    TextView tvNewHome;
    @BindView(R.id.tv_new_hot)
    TextView tvNewHot;
    MenuCategoryAdapterV5 adapterMenu;
    boolean checkMenuCategory = false;

    ITabNewsPresenter mPresenter;
    ArrayList<CategoryModel> categoryList = new ArrayList<>();
    SettingCategoryModel homeNewsFragment;
    SettingCategoryModel latestNewsFragment;
    String sortCategory = null;
    //=====view stub
    @BindView(R.id.vStub)
    ViewStub vStub;
    AppBarLayout appBarLayout;
    //    TabLayout tabLayout;
    ViewPager viewPager;
    boolean initViewStubDone = false;
    private TabNewsAdapter pagerAdapter;
    //=============
    private int currentTabIndex = 0;
    private boolean needLoadOnCreate = false;           //dung cho deeplink

    public static TabNewsFragment newInstance() {
        TabNewsFragment fragment = new TabNewsFragment();
        fragment.mPresenter = new TabNewsPresenter();
        DEFAULT_TAB_COUNT = 0;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_news, container, false);
        DEFAULT_TAB_COUNT = 0;
        if (mPresenter == null) {
            mPresenter = new TabNewsPresenter();
        }
        setUnBinder(ButterKnife.bind(this, view));
        mPresenter.onAttach(this);
        setUp(view);
        if (needLoadOnCreate) {
            Log.i(TAG, "load on create");
            initViewStub();
            setUserVisibleHint(true);
        }
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && getView() == null && getActivity() == null) {
            needLoadOnCreate = true;
            return;
        }

        if (isVisibleToUser && getActivity() instanceof HomeActivity) {
            if (getView() == null)
                needLoadOnCreate = true;
            else {
                initViewStub();

                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin() && NetworkHelper.isConnectInternet(ApplicationController.self())) {
                    long lastTimeShowDialog = ApplicationController.self().getPref().getLong(Constants.PREFERENCE.PREF_LAST_SHOW_DIALOG_FREE_DATA_NETNEWS, 0);
                    int countShowDialog = ApplicationController.self().getPref().getInt(Constants.PREFERENCE.PREF_COUNT_SHOW_DIALOG_FREE_DATA_NETNEWS, 0);
                    if (!TimeHelper.checkTimeInDay(lastTimeShowDialog)) {
                        if (countShowDialog <= 3) {
                            DeepLinkHelper.getInstance().openSchemaLink(getBaseActivity(), NET_NEWS);
                            SharedPreferences.Editor editor = ApplicationController.self().getPref().edit();
                            editor.putLong(Constants.PREFERENCE.PREF_LAST_SHOW_DIALOG_FREE_DATA_NETNEWS, System.currentTimeMillis());
                            editor.putInt(Constants.PREFERENCE.PREF_COUNT_SHOW_DIALOG_FREE_DATA_NETNEWS, countShowDialog + 1);
                            editor.apply();
                        }
                    }
                }
            }
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (mPresenter == null) {
            mPresenter = new TabNewsPresenter();
        }
        if (getActivity() instanceof ModuleActivity)
            needLoadOnCreate = true;
    }

    @Override
    protected void setUp(View view) {
        DEFAULT_TAB_COUNT = 0;
        if (getBaseActivity() instanceof HomeActivity) {
        } else {
            ivBack.setVisibility(View.VISIBLE);
            headerController.setBackgroundColor(ContextCompat.getColor(getBaseActivity(), R.color.white));
            ImageViewCompat.setImageTintList(btnSearch, ColorStateList.valueOf(ContextCompat.getColor(getBaseActivity(), R.color.bg_ab_icon)));
        }
        AppStateProvider.CLICK_CLOSE = 1;
        //todo menu category, goi api getCategory 1 ngay 1 lan
        if (checkDate()) {
            long currentTime = System.currentTimeMillis();
            SharedPref.newInstance(getBaseActivity()).putLong("TIME_CATEGORY", currentTime);
            mPresenter.loadCategory(getBaseActivity());
        } else {
            CategoryResponse data = SharedPrefs.getInstance().get(SharedPrefs.LIST_CATEGORY_NEW, CategoryResponse.class);

            if (data == null || (data.getData() == null || data.getData().size() == 0)) {
                mPresenter.loadCategory(getBaseActivity());
            } else {
                categoryList = data.getData();
            }
        }
        adapterMenu = new MenuCategoryAdapterV5(categoryList, getBaseActivity(), this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getTop(), getResources().getDimensionPixelOffset(R.dimen.v5_spacing_normal), recyclerView.getPaddingBottom());
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(adapterMenu);

        ivMenuCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkMenuCategory) {
                    llRecyclerCategory.setVisibility(View.GONE);
                    checkMenuCategory = false;
                    ivMenuCategory.setImageResource(R.drawable.ic_menu_category);
                } else {
                    llRecyclerCategory.setVisibility(View.VISIBLE);
                    checkMenuCategory = true;
                    ivMenuCategory.setImageResource(R.drawable.ic_close_black_24dp);
                }
            }
        });
    }

    private void initViewStub() {
        if (initViewStubDone) return;
        Log.i(TAG, "initViewStub");
        initViewStubDone = true;
        View view = vStub.inflate();
        appBarLayout = view.findViewById(R.id.id_appbar);
        viewPager = view.findViewById(R.id.tab_view_pager);
        setupViewPager();
    }

    private boolean checkDate() {
        long prevTime = SharedPref.newInstance(getBaseActivity()).getLong("TIME_CATEGORY", 0l);
        long currentTime = System.currentTimeMillis();

        Calendar prevCalendar = Calendar.getInstance();
        prevCalendar.setTimeInMillis(prevTime);

        int prevDate = prevCalendar.get(Calendar.DAY_OF_YEAR);
        int prevYear = prevCalendar.get(Calendar.YEAR);

        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTimeInMillis(currentTime);

        int currentDate = currentCalendar.get(Calendar.DAY_OF_YEAR);
        int currentYear = currentCalendar.get(Calendar.YEAR);

        if (currentYear > prevYear) {
            return true;
        }

        if (currentYear < prevYear) {
            return false;
        }

        return currentDate > prevDate;
    }

    private void setupViewPager() {
        if (pagerAdapter == null) {
            pagerAdapter = new TabNewsAdapter(getChildFragmentManager(), viewPager, null);

            //Tab home
            homeNewsFragment = new SettingCategoryModel(HomeNewsFragment.newInstance(), getString(R.string.home_news), -1);
            pagerAdapter.addFrag(homeNewsFragment);
            DEFAULT_TAB_COUNT++;

            //Tab moi nhat
            Bundle bundle = new Bundle();
            bundle.putInt(CommonUtils.KEY_CATEGORY_ID, 0);
            bundle.putString(CommonUtils.KEY_CATEGORY_NAME, getString(R.string.latest_news));
            latestNewsFragment = new SettingCategoryModel(ChildNewsFragment.newInstance(bundle), getString(R.string.latest_news), 0);
            pagerAdapter.addFrag(latestNewsFragment);
            DEFAULT_TAB_COUNT++;
        }
        setUpSelectText(0);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentTabIndex = position;
                setUpSelectText(position);
                if (appBarLayout != null) {
                    appBarLayout.setExpanded(true, true);
                }
                try {
                    if (position == 1)//Tab moi nhat
                    {
                        //An news count di
                        if (getPagerAdapter() != null && getPagerAdapter().getItem(0) instanceof HomeNewsFragment) {
                            HomeNewsFragment homeNewsFragment = (HomeNewsFragment) getPagerAdapter().getItem(0);
                            AppStateProvider.getInstance().setNewsCount(0);
                        }

                        if (getPagerAdapter() != null && getPagerAdapter().getItem(1) instanceof ChildNewsFragment) {
                            ChildNewsFragment childNewsFragment = (ChildNewsFragment) getPagerAdapter().getItem(1);
                            childNewsFragment.updateLastNews();
                        }
                    }

                    //Vuot sang tab nao tab day refresh du lieu
                    if (position == 0) {
                        if (getPagerAdapter() != null && getPagerAdapter().getItem(0) instanceof HomeNewsFragment) {
                            HomeNewsFragment homeNewsFragment = (HomeNewsFragment) getPagerAdapter().getItem(0);
                            homeNewsFragment.onRefresh();
                        }
                    } else {
                        if (getPagerAdapter() != null && getPagerAdapter().getItem(position) instanceof ChildNewsFragment) {
                            ChildNewsFragment childNewsFragment = (ChildNewsFragment) getPagerAdapter().getItem(position);
                            childNewsFragment.onTabRefresh();
                        }
                    }

                } catch (Exception ex) {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void setUpSelectText(int position) {
        if (position == 0) {
            tvNewHome.setSelected(true);
            tvNewHot.setSelected(false);
            tvNewHome.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            tvNewHot.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            tvNewHome.animate().scaleX(1.4f).scaleY(1.4f).setDuration(100).start();
            tvNewHot.animate().scaleX(1.0f).scaleY(1.0f).setDuration(100).start();
        } else {
            tvNewHome.setSelected(false);
            tvNewHot.setSelected(true);
            tvNewHome.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            tvNewHot.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            tvNewHome.animate().scaleX(1.0f).scaleY(1.0f).setDuration(100).start();
            tvNewHot.animate().scaleX(1.4f).scaleY(1.4f).setDuration(100).start();
        }
        tvNewHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpSelectText(0);
                viewPager.setCurrentItem(0);
            }
        });
        tvNewHot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpSelectText(1);
                viewPager.setCurrentItem(1);
            }
        });
    }

    public void selectTab(final int position) {
        if (position == currentTabIndex) return;
        if (viewPager != null) {
//            tabLayout.setScrollPosition(position, 0f, true);
            viewPager.setCurrentItem(position);
        }
        currentTabIndex = position;
    }

    @Override
    public void loadDataSuccess(boolean flag) {

    }

    @Override
    public void bindData(ArrayList<CategoryModel> categoryModels) {
        adapterMenu.setDataCategory(categoryModels);
        CategoryResponse response = new CategoryResponse();
        response.setData(categoryModels);
        SharedPrefs.getInstance().put(SharedPrefs.LIST_CATEGORY_NEW, response);
    }

    @Override
    public void bindSettingData(String str) {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sortCategory != null && !sortCategory.equals(AppStateProvider.getInstance().getSortNewsCategory())) {
            processSettingCategory();
            if (pref != null) {
                pref.putBoolean(SharedPref.PREF_KEY_NEWS_TAB_CUSTOMIZED, true);
            }
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.iv_search)
    public void onSearchClick() {
        Utilities.openSearch(getBaseActivity(), Constants.TAB_NEWS_HOME);
    }

    @OnClick(R.id.btnConfig)
    public void onConfigClick() {
        if (getBaseActivity() instanceof NetNewsActivity) {
            ((NetNewsActivity) getBaseActivity()).showFragment(CommonUtils.TAB_SETTING_CATEGORY, null, true);
        } else {
            //start NetNewsActivity
            Intent intent = new Intent(getBaseActivity(), NetNewsActivity.class);
            intent.putExtra(CommonUtils.KEY_TAB, CommonUtils.TAB_SETTING_CATEGORY);
            getBaseActivity().startActivity(intent);
        }
    }

//    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(EditCategoryEvent event) {
//        try {
//            if (event != null) {
//                if (event.getType() == CommonUtils.TYPE_NEWS) {
//                    sortCategory = event.getListSource();
//                    AppStateProvider.getInstance().setSortNewsCategory(event.getListSource());
//                    if (event.getMode() == EditCategoryEvent.MODE_FOLLOW) {
//                        CategoryModel categoryModel = event.getModel();
//                        if (categoryModel != null) {
//                            if (event.getModel().isFollow()) {
//                                processSettingCategory();
//                            } else {
//                                final int index = pagerAdapter.getTabIndex(categoryModel.getId() + "");
//                                if (index > -1) {
//                                    Fragment fragment = pagerAdapter.getItem(index);
//                                    FragmentManager manager = getChildFragmentManager();
//                                    FragmentTransaction trans = manager.beginTransaction();
//                                    trans.remove(fragment);
//                                    trans.commit();
//
//                                    pagerAdapter.removeFrag(index);
//                                    pagerAdapter.notifyDataSetChanged();
//                                }
//                            }
//                        }
//                    } else if (event.getMode() == EditCategoryEvent.MODE_SWIPE) {
//                        processSettingCategory();
//                    }
//                }
//            }
//        } catch (Exception ex) {
//
//        }
//    }

    private void processSettingCategory() {
        try {
            sortCategory = AppStateProvider.getInstance().getSortNewsCategory();
            String[] listCategoryId = AppStateProvider.getInstance().getSortNewsCategory().split(",");
            if (listCategoryId.length > 0) {

                FragmentManager manager = getChildFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                //Clear all
                for (int i = pagerAdapter.getCount() - 1; i >= DEFAULT_TAB_COUNT; i--) {
                    Fragment fragment = pagerAdapter.getItem(i);
                    trans.remove(fragment);

                    pagerAdapter.removeFrag(i);
                }
                trans.commit();

                for (int i = 0; i < listCategoryId.length; i++) {
                    String categoryId = listCategoryId[i];
                    CategoryModel categoryModel = getCategoryModelById(categoryId);
                    if (categoryModel != null) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(CommonUtils.KEY_CATEGORY_ID, categoryModel.getId());
                        bundle.putString(CommonUtils.KEY_CATEGORY_NAME, categoryModel.getName());
                        SettingCategoryModel settingCategoryModel = new SettingCategoryModel(ChildNewsFragment.newInstance(bundle), categoryModel.getName(), categoryModel.getId());
                        pagerAdapter.addFrag(settingCategoryModel);
                    }
                }

                pagerAdapter.notifyDataSetChanged();
                viewPager.setCurrentItem(0);
            }
        } catch (Exception e) {
        }
    }

    public CategoryModel getCategoryModelById(String categoryId) {
        for (int i = 0; i < categoryList.size(); i++) {
            if (categoryId.equals(categoryList.get(i).getId() + ""))
                return categoryList.get(i);
        }
        return null;
    }

    public TabNewsAdapter getPagerAdapter() {
        return pagerAdapter;
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        getBaseActivity().onBackPressed();
    }

    public void scrollToPosition(int position) {
//        try {
//            BaseFragment currentFragment = (BaseFragment) pagerAdapter.getItem(viewPager.getCurrentItem());
//            currentFragment.scrollToPosition(position);
//            if (appBarLayout != null) {
//                appBarLayout.setExpanded(true, true);
//            }
//        } catch (Exception e) {
//        }
    }

    @Override
    public void onItemClickFavourite(CategoryModel model) {
//        if(BuildConfig.DEBUG){
//            Toast.makeText(getBaseActivity(), ""+model.getName(), Toast.LENGTH_SHORT).show();
//        }
        //todo kiem tra xem co id trong chuoi chua, neu co thi xoa còn không thì add
        FavouriteModel data = SharedPrefs.getInstance().get("FAVOURITE", FavouriteModel.class);
        if (data == null || data.getData().size() == 0) {
            //todo add luon
            data = new FavouriteModel();
            data.getData().add(String.valueOf(model.getId()));
        } else {
            //todo kiem tra r add
            for (String i : data.getData()) {
                if (i.equals(model.getId() + "")) {
                    data.getData().remove(i);
                    break;
                }
            }
            if (model.isFollow()) {
                data.getData().add(String.valueOf(model.getId()));
            }
        }
        SharedPrefs.getInstance().put("FAVOURITE", data);
    }

}
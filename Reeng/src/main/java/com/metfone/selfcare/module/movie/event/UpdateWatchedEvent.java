/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/2
 *
 */

package com.metfone.selfcare.module.movie.event;

public class UpdateWatchedEvent {
    boolean isUpdate;

    public UpdateWatchedEvent(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean update) {
        isUpdate = update;
    }

    @Override
    public String toString() {
        return "{" +
                "isUpdate=" + isUpdate +
                '}';
    }
}

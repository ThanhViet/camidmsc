package com.metfone.selfcare.module.netnews.MainNews.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.module.netnews.MainNews.fragment.TabNewsFragment;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.response.CategoryResponse;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/18/17.
 */

public class TabNewsPresenter extends BasePresenter implements ITabNewsPresenter {
    public static final String TAG = TabNewsPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;
    long startTime;

    public TabNewsPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadCategory(final Context context) {
        startTime = System.currentTimeMillis();
        mNewsApi.getNewsCategory(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                final long endTime = System.currentTimeMillis();
                if (!isViewAttached() || !(getMvpView() instanceof TabNewsFragment)) {
                    return;
                }
                Gson gson = new Gson();
                CategoryResponse categoryResponse = gson.fromJson(data, CategoryResponse.class);
                if (categoryResponse != null) {
                    ((TabNewsFragment) getMvpView()).bindData(categoryResponse.getData());
                    ((TabNewsFragment) getMvpView()).loadDataSuccess(true);

                } else {
                    ((TabNewsFragment) getMvpView()).loadDataSuccess(false);
                    ArrayList<CategoryModel> categoryList = CommonUtils.getListCategoryNews(context);
                    ((TabNewsFragment) getMvpView()).bindData(categoryList);
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());

            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                final long endTime = System.currentTimeMillis();
                com.metfone.selfcare.util.Log.d(TAG, "loadData: onFailure - " + message);
                if (!isViewAttached() || !(getMvpView() instanceof TabNewsFragment)) {
                    return;
                }
                ((TabNewsFragment) getMvpView()).loadDataSuccess(false);
                ArrayList<CategoryModel> categoryList = CommonUtils.getListCategoryNews(context);
                ((TabNewsFragment) getMvpView()).bindData(categoryList);

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());

            }
        });
    }

    @Override
    public void loadSettingCategory(SharedPref pref) {
        if (pref != null) {
            String str = pref.getString(SharedPref.PREF_KEY_NEWS_CATEGORY, "");
            if (getMvpView() != null && getMvpView() instanceof TabNewsFragment) {
                ((TabNewsFragment) getMvpView()).bindSettingData(str);
            }
        }
    }

    @Override
    public void saveCategoryToDB(SharedPref pref, CategoryResponse categoryResponse) {
        if (categoryResponse != null && categoryResponse.getData().size() > 0) {
            Log.i(TAG, "saveCategoryToDB");
            ArrayList<CategoryModel> datas = categoryResponse.getData();

            String str = "";
            for (int i = 0; i < datas.size(); i++) {
                CategoryModel categoryModel = datas.get(i);
                str += categoryModel.getId() + ",";
            }

            if (pref != null) {
                pref.putString(SharedPref.PREF_KEY_NEWS_CATEGORY, str);
            }
        }
    }

//    HttpCallBack callback = new HttpCallBack() {
//        @Override
//        public void onSuccess(String data) throws Exception {
//            final long endTime = System.currentTimeMillis();
//            if (!isViewAttached() || !(getMvpView() instanceof TabNewsFragment)) {
//                return;
//            }
//            Gson gson = new Gson();
//            CategoryResponse categoryResponse = gson.fromJson(data, CategoryResponse.class);
//            if (categoryResponse != null) {
//                ((TabNewsFragment) getMvpView()).bindData(categoryResponse.getData());
//                ((TabNewsFragment) getMvpView()).loadDataSuccess(true);
//                SharedPrefs.getInstance().put(SharedPrefs.LIST_CATEGORY_NEW,data);
//            } else {
//                ((TabNewsFragment) getMvpView()).loadDataSuccess(false);
//                List<CategoryModel> categoryList = CommonUtils.getListCategoryNews(context);
//                ((HomeNewsFragment) getMvpView()).bindCategoryV5(categoryList);
//            }
//
//            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
//        }
//
//        @Override
//        public void onFailure(String message) {
//            super.onFailure(message);
//            final long endTime = System.currentTimeMillis();
//            com.metfone.selfcare.util.Log.d(TAG, "loadData: onFailure - " + message);
//            if (!isViewAttached() || !(getMvpView() instanceof TabNewsFragment)) {
//                return;
//            }
//            ((TabNewsFragment) getMvpView()).loadDataSuccess(false);
//            List<CategoryModel> categoryList = CommonUtils.getListCategoryNews(context);
//            ((HomeNewsFragment) getMvpView()).bindCategoryV5(categoryList);
//
//            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
//        }
//
//    };
}

package com.metfone.selfcare.module.home_kh.api;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetAllAppsRequest extends BaseRequest<WsGetAllAppsRequest.Request> {
    public class Request{
        public String language;
    }
}

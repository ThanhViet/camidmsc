/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.tab_home.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.tab_home.adapter.TabHomeDetailAdapter;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;

import java.util.ArrayList;

import butterknife.BindView;

public class HorizontalContentHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;

    private ArrayList<Object> list;
    private TabHomeDetailAdapter adapter;
    private TabHomeModel data;
    private int position;

    public HorizontalContentHolder(View view, Activity activity, final TabHomeListener.OnAdapterClick listener, int viewType) {
        super(view);
        list = new ArrayList<>();
        adapter = new TabHomeDetailAdapter(activity);
        adapter.setListener(listener);
        adapter.setParentViewType(viewType);
        adapter.setItems(list);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, R.drawable.divider_default);
    }

    @Override
    public void bindData(Object item, int position) {
        this.position = position;
        if (item instanceof TabHomeModel) {
            data = (TabHomeModel) item;
            if (list == null) list = new ArrayList<>();
            else list.clear();
            list.addAll(data.getList());
            if (adapter != null) adapter.notifyDataSetChanged();
        } else {
            data = null;
        }
    }

}

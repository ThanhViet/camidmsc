package com.metfone.selfcare.module.netnews.ChildNews.view;

import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.NewsResponse;

/**
 * Created by HaiKE on 8/18/17.
 */

public interface IChildNewsView extends MvpView {

    void loadDataSuccess(boolean flag);

    void bindData(NewsResponse childNewsResponse);
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/3/26
 *
 */

package com.metfone.selfcare.module.security.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseViewHolder;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.security.model.NewsModel;
import com.metfone.selfcare.module.security.model.VulnerabilityModel;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

public class SecurityCenterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = SecurityCenterAdapter.class.getSimpleName();
    private final int TYPE_VULNERABILITY = 1;
    private final int TYPE_NEWS = 2;
    private final int TYPE_NO_VULNERABILITY = 3;
    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private ArrayList<Object> vulnerabilities;
    private ArrayList<Object> newsModels;
    private RecyclerClickListener listener;
    private boolean isScanVulnerability;

    public SecurityCenterAdapter(BaseSlidingFragmentActivity activity) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplication();
    }

    public void setVulnerabilities(ArrayList<VulnerabilityModel> list) {
        if (vulnerabilities == null) vulnerabilities = new ArrayList<>();
        else vulnerabilities.clear();
        vulnerabilities.addAll(list);
    }

    public void setNewsModels(ArrayList<NewsModel> list) {
        if (newsModels == null) newsModels = new ArrayList<>();
        else newsModels.clear();
        newsModels.addAll(list);
    }

    public void setScanVulnerability(boolean scanVulnerability) {
        isScanVulnerability = scanVulnerability;
    }

    public void setListener(RecyclerClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if (isScanVulnerability) {
            if (position == 0) {
                if (vulnerabilities == null || vulnerabilities.isEmpty())
                    return TYPE_NO_VULNERABILITY;
                return TYPE_VULNERABILITY;
            }
            if (position == 1) return TYPE_NEWS;
        } else {
            if (position == 0) return TYPE_NEWS;
        }
        return -1;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_VULNERABILITY || viewType == TYPE_NEWS) {
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_security_center, parent, false);
        } else if (viewType == TYPE_NO_VULNERABILITY) {
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_security_no_vulnerability, parent, false);
        } else
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_empty, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemHolder, int position) {
        int viewType = getItemViewType(position);
        if (itemHolder instanceof BaseViewHolder) {
            BaseViewHolder holder = (BaseViewHolder) itemHolder;
            if (viewType == TYPE_VULNERABILITY) {
                holder.setVisible(R.id.line, position != 0);
                holder.setText(R.id.button_more, R.string.security_threat);
                RecyclerView recyclerView = holder.getView(R.id.recycler_view);
                if (recyclerView.getItemDecorationCount() <= 0) {
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.setLayoutManager(new CustomLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
                }
                SecurityDetailAdapter adapter = new SecurityDetailAdapter(activity, Constants.TAB_SECURITY_VULNERABILITY, vulnerabilities);
                adapter.setRecyclerClickListener(listener);
                recyclerView.setAdapter(adapter);
            } else if (viewType == TYPE_NEWS) {
                holder.setVisible(R.id.line, position != 0);
                holder.setText(R.id.button_more, R.string.security_news);
                RecyclerView recyclerView = holder.getView(R.id.recycler_view);
                if (recyclerView.getItemDecorationCount() <= 0) {
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.setLayoutManager(new CustomLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
                }
                SecurityDetailAdapter adapter = new SecurityDetailAdapter(activity, Constants.TAB_SECURITY_CENTER_DETAIL, newsModels);
                adapter.setRecyclerClickListener(listener);
                recyclerView.setAdapter(adapter);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (isScanVulnerability) {
            if (newsModels == null || newsModels.isEmpty())
                return 1;
            return 2;
        } else {
            if (newsModels == null || newsModels.isEmpty())
                return 0;
            return 1;
        }
    }
}

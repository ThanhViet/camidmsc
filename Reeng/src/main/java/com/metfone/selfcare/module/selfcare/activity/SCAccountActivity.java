/*
package com.metfone.selfcare.module.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.CropImageActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.selfcare.fragment.account.SCAccountConnectFragment;
import com.metfone.selfcare.module.selfcare.fragment.account.SCAccountForgetPassFragment;
import com.metfone.selfcare.module.selfcare.fragment.account.SCAccountInfoFragment;
import com.metfone.selfcare.module.selfcare.fragment.account.SCAccountResetPassFragment;
import com.metfone.selfcare.module.selfcare.fragment.account.SCCheckAccountFragment;
import com.metfone.selfcare.module.selfcare.fragment.account.SCMyAccountInfoFragment;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_AVATAR_FILE_CROP;

*/
/**
 * Created by thanhnt72 on 2/13/2019.
 *//*


public class SCAccountActivity extends BaseSlidingFragmentActivity {
    private static final String TAG = SCAccountActivity.class.getSimpleName();

    public static void startMyIDAccountFragment(BaseSlidingFragmentActivity activity, boolean gotohome){
        Intent intent = new Intent(activity, SCAccountActivity.class);
        intent.putExtra(SCAccountActivity.FRAGMENT, SCAccountActivity.FRAGMENT_ACC_INFO);
        intent.putExtra("gotohome", gotohome);
        activity.startActivity(intent);
    }

    public static String FRAGMENT = "fragment";
    public static String USER_NAME = "username";
    public static String SHOW_SKIP = "show_skip";
    public static int FRAGMENT_LOGIN = 1;
    public static int FRAGMENT_ACC_INFO = 2;
    public static int FRAGMENT_CHANGE_PASS = 3;
    private int fragment = 0;

    private ApplicationController mApp;
    private String userName;
    private boolean showSkip;

    private SCMyAccountInfoFragment infoFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.activity_detail);
        mApp = (ApplicationController) getApplication();
        changeStatusBar(true);
        View toolbar = findViewById(R.id.tool_bar);
        toolbar.setVisibility(View.GONE);
//        setToolBar(findViewById(R.id.tool_bar));


        Bundle bundle = getIntent().getExtras();
        boolean gotohome = false;
        if (bundle != null) {
            fragment = bundle.getInt(FRAGMENT);
            userName = bundle.getString(USER_NAME);
            showSkip = bundle.getBoolean(SHOW_SKIP);
            gotohome = bundle.getBoolean("gotohome");
        }
        if (fragment == FRAGMENT_LOGIN) {
            openLoginMyID(userName, false);
        } else if (fragment == FRAGMENT_ACC_INFO) {
            openMyAccountInfo(gotohome);
        } else if (fragment == FRAGMENT_CHANGE_PASS) {
            openChangePass();
        } else {
            executeFragmentTransaction(SCCheckAccountFragment.newInstance(showSkip), R.id.fragment_container, false, false);
        }
    }

    private void openMyAccountInfo(boolean gotohome) {
        infoFragment = SCMyAccountInfoFragment.newInstance(gotohome);
        executeFragmentTransaction(infoFragment, R.id.fragment_container, false, false);
    }

    public void openEnterInfoFragment(String userName, String password, boolean addToBackStack) {
        executeFragmentTransaction(SCAccountInfoFragment.newInstance(userName, password), R.id.fragment_container, addToBackStack, false);
    }


    public void openLoginMyID(String userName, boolean addToBackStack) {
        executeAddFragmentTransaction(SCAccountConnectFragment.newInstance(userName), R.id.fragment_container, addToBackStack, false);
    }

    public void openForgetPass(String userName) {
        executeAddFragmentTransaction(SCAccountForgetPassFragment.newInstance(false), R.id.fragment_container, true, false);
    }

    public void openChangePass() {
        executeFragmentTransaction(SCAccountForgetPassFragment.newInstance(true), R.id.fragment_container, false, false);
    }

    public void resetPass() {
        executeAddFragmentTransaction(SCAccountResetPassFragment.newInstance(), R.id.fragment_container, true, false);
    }


    public void goToHome() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.putExtra("home_action", Constants.ACTION.VIEW_CREATE_CHAT);
        startActivity(intent, false);
        clearBackStack();
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean isHD = SettingBusiness.getInstance(getApplicationContext()).getPrefEnableHDImage();
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                // get information from facebook
                case Constants.ACTION.ACTION_PICK_PICTURE:
                    if (data != null) {
                        Log.d(TAG, "onActivityResult ACTION_PICK_PICTURE");
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (picturePath != null && picturePath.size() > 0) {
                            String pathImage = picturePath.get(0);
                            cropAvatarImage(pathImage);
                        }
                    }
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_TAKE_PHOTO:
                    //Uri takeImageUri;
                    File imageFile = new File(mApp.getPref().getString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, ""));
                    cropAvatarImage(imageFile.getPath());
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_CROP_IMAGE:
                    if (data != null) {
                        String fileCrop = mApp.getPref().getString(PREF_AVATAR_FILE_CROP, "");
                        if (infoFragment != null && infoFragment.isVisible())
                            infoFragment.drawAvatar(fileCrop, true);
                    }
                    break;
                case Constants.ACTION.ADD_NUMBER_SC:
                    if (infoFragment != null && infoFragment.isVisible())
                        infoFragment.drawViewDetail();
                    break;
                case Constants.ACTION.VERIFY_NUMBER:
                    if (infoFragment != null && infoFragment.isVisible())
                        infoFragment.drawViewDetail();
                    break;
                default:
                    break;
            }
        } else {
            setActivityForResult(false);
            setTakePhotoAndCrop(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void cropAvatarImage(String inputFilePath) {
        try {
            String time = String.valueOf(System.currentTimeMillis());
            File cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "/avatar" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            mApp.getPref().edit().putString(PREF_AVATAR_FILE_CROP, cropImageFile.toString()).apply();
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, inputFilePath);
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.RETURN_DATA, false);
            intent.putExtra(CropView.MASK_OVAL, true);
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);

        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

    public static class SCAccountEvent {
        public static final int LOGOUT = 1;
        public static final int CHANGE_AVATAR = 2;
        public static final int CHANGE_NAME = 3;
        public static final int UPDATE_INFO = 4;

        int event;

        public SCAccountEvent(int event) {
            this.event = event;
        }

        public int getEvent() {
            return event;
        }

        public void setEvent(int event) {
            this.event = event;
        }
    }

}
*/

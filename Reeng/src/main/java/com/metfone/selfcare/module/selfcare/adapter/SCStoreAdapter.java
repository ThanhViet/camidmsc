package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCStore;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;

import java.util.List;

public class SCStoreAdapter extends BaseAdapter<BaseViewHolder> {

    private List<SCStore> itemsList;
    private AbsInterface.OnStoreListener listener;

    public SCStoreAdapter(Context context, AbsInterface.OnStoreListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(List<SCStore> itemsList) {
        this.itemsList = itemsList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_store, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final SCStore data = itemsList.get(position);
        if(data != null)
        {
            holder.setText(R.id.tvName, data.getName());
            holder.setText(R.id.tvAddress, data.getAddr());
            if(data.getDistance() != null)
            {
                holder.getView(R.id.tvDistance).setVisibility(View.VISIBLE);
                holder.setText(R.id.tvDistance, SCUtils.numberDistanceFormat(data.getDistance()) + " km");
            }
            else
                holder.getView(R.id.tvDistance).setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                {
                    listener.onStoreClick(data);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }
}

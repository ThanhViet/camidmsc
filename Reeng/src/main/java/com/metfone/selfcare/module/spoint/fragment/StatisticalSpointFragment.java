package com.metfone.selfcare.module.spoint.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.spoint.SpointActivity;
import com.metfone.selfcare.module.spoint.adapter.StatisticalSpointAdapter;
import com.metfone.selfcare.module.spoint.network.SpointApi;
import com.metfone.selfcare.module.spoint.network.model.FilterSpoint;
import com.metfone.selfcare.module.spoint.network.model.StatisticalModel;
import com.metfone.selfcare.module.spoint.network.response.StatisticalResponse;
import com.metfone.selfcare.util.Utilities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class StatisticalSpointFragment extends Fragment implements View.OnClickListener {
    private PieChart pieChart;
    private SpointActivity activity;
    private AppCompatTextView tvSpoint, tvSpointPlus, tvSumSpoint;
    private StatisticalModel currentModel;
    private RecyclerView recyclerView;
    private StatisticalSpointAdapter adapter;
    private AppCompatImageView ivFilter;
    private FilterSpoint currentFilter;

    public static StatisticalSpointFragment newInstance() {
        Bundle args = new Bundle();
        StatisticalSpointFragment fragment = new StatisticalSpointFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistic_spoint, container, false);
        activity = (SpointActivity) getActivity();
        initView(view);
        loadData();
        initEvent();
        return view;
    }

    private void initView(View view) {
        pieChart = view.findViewById(R.id.chart_saving);
        tvSpoint = view.findViewById(R.id.tv_spoint);
        tvSpointPlus = view.findViewById(R.id.tv_spoint_plus);
        tvSumSpoint = view.findViewById(R.id.tv_sum_money);
        recyclerView = view.findViewById(R.id.recycler_view);
        ivFilter = view.findViewById(R.id.iv_filter);

        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        currentFilter = new FilterSpoint(2,0);

    }

    public void loadData() {
        new SpointApi(ApplicationController.self()).historySpoint(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                Gson gson = new Gson();
                StatisticalResponse statisticalResponse = gson.fromJson(data, StatisticalResponse.class);
                if (statisticalResponse != null && statisticalResponse.getCode().equals("200")) {
                    bindData(statisticalResponse);
                }
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
            }
        }, currentFilter.getTypePoint(), currentFilter.getDealPoint());


    }

    private void bindData(StatisticalResponse statisticalResponse) {
        currentModel = statisticalResponse.getData();
        if (currentModel == null) return;
        String point = TextHelper.formatTextDecember(String.valueOf(currentModel.getSpoint()));
        tvSpoint.setText(point + activity.getString(R.string.spoint));
        String pointPlus = TextHelper.formatTextDecember(String.valueOf(currentModel.getSpointPromotion()));
        tvSpointPlus.setText(pointPlus + activity.getString(R.string.spoint));
        tvSumSpoint.setText(TextHelper.formatTextDecemberUMoney(currentModel.getTotalSpointString()));
        Utilities.drawChartSpoint(activity, currentModel, pieChart, false);

        adapter = new StatisticalSpointAdapter(activity, currentModel.getList());
        recyclerView.setAdapter(adapter);
    }

    private void initEvent() {
        ivFilter.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_filter:
                FillterSpointFragment filterFragment = FillterSpointFragment.newInstance();
                filterFragment.setModel(currentFilter);
                filterFragment.setListener(result -> {
//                    filterRecentChanged = false;
//                    drawTabFilter();
//                    refreshAfterChooseFilter();
                    if(result != null) {
                        currentFilter = result;
                        loadData();
                    }
                });
                filterFragment.show(getChildFragmentManager(), "");
                break;
        }
    }

}

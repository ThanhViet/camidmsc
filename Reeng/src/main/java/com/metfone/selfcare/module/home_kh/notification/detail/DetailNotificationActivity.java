package com.metfone.selfcare.module.home_kh.notification.detail;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.service.APIService;
import com.metfone.esport.service.ServiceRetrofit;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.games.tournamentdetails.TournamentDetailFragment;
import com.metfone.esport.ui.live.LiveVideoFragment;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.activity.RegisterScreenActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.encrypt.XXTEACrypt;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationItem;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.movie.event.RequestPermissionBright;
import com.metfone.selfcare.module.movie.event.RequestPermissionVolume;
import com.metfone.selfcare.module.movie.fragment.HorizontalProgressView;
import com.metfone.selfcare.module.movienew.dialog.TrailerFullScreenPlayerDialog;
import com.metfone.selfcare.module.movienew.listener.TrailerPlayerListener;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsGetExchangeServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.ui.view.tab_video.VideoPlayerView;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

public class DetailNotificationActivity extends BaseSlidingFragmentActivity {
    private static final String TAG = "DetailNotification";
    public static final String NOTIFICATION_KEY = "notification";
    public static final String HIDE_BUTTON_KEY = "HIDE_BUTTON_KEY";
    public static final String TRAILER_KEY = "trailer";
    KhNotificationItem notification;
    String trailer = "";
    boolean isHideButton = false;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.rootViewNotification)
    RelativeLayout rootViewNotification;
    @BindView(R.id.icBackToolbar)
    ImageView icBackToolbar;
    @BindView(R.id.image_view_feature)
    ImageView imageViewFeature;
    @BindView(R.id.image_view_action)
    ImageView image_view_action;
    @BindView(R.id.text_view_title)
    TextView textViewTitle;
    @BindView(R.id.text_view_time)
    TextView textViewTime;
    @BindView(R.id.button_get_now)
    RoundTextView buttonGetNow;
    @BindView(R.id.web_view)
    SmallWebView webView;
    @BindView(R.id.placeHolderThumbnail)
    HorizontalProgressView placeHolderThumbnail;
    @BindView(R.id.placeHolderWebView)
    HorizontalProgressView placeHolderWebView;

    //Player
    private boolean isPlaying = false;
    private long currentTime = 0;
    private SimpleExoPlayer playerTrailer;
    private VideoPlayerView playerViewTrailer;
    private DialogConfirm dialogConfirm = null;
    private DialogConfirm dialogVolume = null;

    private CompositeDisposable compositeDisposable;
    private APIService apiService;

    @BindView(R.id.layoutVideoTrailer)
    FrameLayout layoutVideoTrailer;
    @BindView(R.id.layoutPlayerTrailer)
    FrameLayout layoutPlayerTrailer;
    @BindView(R.id.controlViewTrailer)
    RelativeLayout controlViewTrailer;
    @BindView(R.id.trailer_play)
    ImageView btnPlayTrailer;
    @BindView(R.id.trailer_mute)
    ImageView btnMuteTrailer;
    @BindView(R.id.trailer_full_screen)
    ImageView btnFullTrailer;
    @BindView(R.id.trailer_progress_bar)
    ProgressBar progress_bar;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (btnPlayTrailer != null) {
                btnPlayTrailer.setVisibility(View.GONE);
            }
        }
    };
    private Handler handler = new Handler();
    private Runnable runnableTime = new Runnable() {
        @Override
        public void run() {
            if (progress_bar != null) {
                progress_bar.setProgress((int) ((playerTrailer.getCurrentPosition() * 100) / playerTrailer.getDuration()));
                if (isPlaying && playerTrailer != null) {
                    currentTime = playerTrailer.getCurrentPosition();
                }
            }
            handler.postDelayed(this, 1000);
        }
    };

    private TrailerFullScreenPlayerDialog trailerFullScreenPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_notification);
        ButterKnife.bind(this);
        notification = (KhNotificationItem) getIntent().getSerializableExtra(NOTIFICATION_KEY);
        trailer = getIntent().getStringExtra(TRAILER_KEY);
        isHideButton = getIntent().getBooleanExtra(HIDE_BUTTON_KEY, false);
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        Utilities.adaptViewForInsertBottom(rootViewNotification);
        initViews();
        EventBus.getDefault().register(this);
        compositeDisposable = new CompositeDisposable();
        apiService = ServiceRetrofit.getInstance();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTrailer();
        if (trailerFullScreenPlayer != null) {
            trailerFullScreenPlayer.stopTrailer();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        if (btnPlayTrailer != null) {
            btnPlayTrailer.removeCallbacks(runnable);
        }
        if (handler != null) {
            handler.removeCallbacks(runnableTime);
        }
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
    }

    private void initViews() {
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.getSettings().setDefaultFontSize(12);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        webView.getSettings().setNeedInitialFocus(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        webView.requestFocus();
        if (notification != null) {
            textViewTitle.setText(notification.getTitle(this));
            String time = (DateConvert.formatDateTimeNotification(this, notification.time));
            textViewTime.setText(time);
            if (notification.featureImage != null) {
                Glide.with(this).load(notification.featureImage).addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        imageViewFeature.setVisibility(View.VISIBLE);
                        placeHolderThumbnail.setVisibility(View.GONE);
                        return false;
                    }
                }).into(imageViewFeature);
            }
            if (notification.buttonTitle != null) {
                buttonGetNow.setText(notification.buttonTitle);
            }
            if (notification.type == null || notification.type.equals(KhNotificationItem.Type.NONE)) {
                buttonGetNow.setVisibility(View.GONE);
            } else {
                if (notification.buttonTitle != null) {
                    if (notification.buttonTitle.equals("play_now")) {
                        buttonGetNow.setText(R.string.btn_play_now);
                    } else if (notification.buttonTitle.equals("sign_up_now")) {
                        buttonGetNow.setText(R.string.btn_sign_up_now);
                    } else if (notification.buttonTitle.equals("login_now")) {
                        buttonGetNow.setText(R.string.btn_login_now);
                    } else {
                        buttonGetNow.setText(R.string.btn_get_now);
                    }
                } else {
                    buttonGetNow.setText(R.string.btn_play_now);
                }
            }
            if (notification.getValue(this) != null) {
                String content = "<body style=\"margin: 0; padding: 0\">" + notification.getValue(this) + "</body>";
                webView.loadData(content, "text/html; charset=utf-8", "UTF-8");
                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        placeHolderWebView.setVisibility(View.GONE);
                        webView.setVisibility(View.VISIBLE);
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) webView.getLayoutParams();
                        params.height = webView.getRealHeight();
                        webView.setLayoutParams(params);
                        view.loadUrl(
                                "javascript:document.body.style.setProperty(\"color\", \"white\");"
                        );
                        webView.loadUrl(
                                "javascript:( " +
                                        "function () {" +
                                        "window.HTMLOUT.resize(document.body.getBoundingClientRect().height); } });" +
                                        "} ) ()"
                        );
                    }

                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        Intent intent = new Intent(DetailNotificationActivity.this, WebViewActivity.class);
                        intent.putExtra(WebViewActivity.URL_KEY, url);
                        startActivity(intent);
                        return true;
                    }

                    @Override
                    public void onPageCommitVisible(WebView view, String url) {
                        super.onPageCommitVisible(view, url);
                        Log.d(TAG, "onPageCommitVisible(),Real height : " + webView.getRealHeight());
                        Log.d(TAG, "onPageCommitVisible(),Content height : " + view.getContentHeight());
                    }
                });
            } else {
                placeHolderWebView.setVisibility(View.GONE);
            }
            if (!StringUtils.isEmpty(notification.videoUrl)) {
                layoutVideoTrailer.setVisibility(View.VISIBLE);
                initTrailer(notification.videoUrl);
            } else if (KhNotificationItem.ActionType.MOVIE_PROFILE.equals(notification.actionType) || KhNotificationItem.ActionType.MOVIE_PLAY_DIRECTLY.equals(notification.actionType)) {
                if (!StringUtils.isEmpty(trailer)) {
                    layoutVideoTrailer.setVisibility(View.VISIBLE);
                    initTrailer(trailer);
                }
            }
            if (notification.type != null && notification.type.equals(KhNotificationItem.Type.ACTION) && notification.actionType != null) {
                int resIcon = KhNotificationItem.getTypeIcon(notification.actionType);
                Glide.with(this).load(resIcon).into(image_view_action);
            }
            buttonGetNow.setOnClickListener(v -> {
                handleRedirect();
            });
            icBackToolbar.setOnClickListener(v -> {
                finish();
            });
        }
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                Log.d(TAG, "scrollY : " + scrollY);
            }
        });

        if (isHideButton) {
            buttonGetNow.setVisibility(View.GONE);
        } else {
            buttonGetNow.setVisibility(View.VISIBLE);
        }
    }

    private void handleRedirect() {
        if (notification == null) {
            return;
        }
        String type = notification.type;
        String actionType = notification.actionType;
        if (type == null) {
            return;
        } else if (type.equals(KhNotificationItem.Type.ACTION) && actionType != null) {// dieu huong notification
            if (actionType.contains("movie")) {
                handleNotificationMovie(notification);
            } else if (actionType.contains("esport")) {
                handleNotificationEsport(notification);
            } else if (actionType.contains("metfone")) {
                handleNotificationMetfone(notification);
            } else if (actionType.contains("reward_detail")) {
                handleRewardDetail(notification);
            } else if (actionType.contains("sign_up")) {
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
                Intent intent = new Intent(this, RegisterScreenActivity.class);
                startActivity(intent, false);
            } else if (actionType.contains("login")) {
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, false);
                handleLoginNow();
            } else if (actionType.contains(KhNotificationItem.ActionType.GIFT_CATALOG)) {
                handleGiftOpen(notification);
            }
        } else if (type.equals(KhNotificationItem.Type.LINK)) {// open webview
            Intent intent = new Intent(DetailNotificationActivity.this, WebViewActivity.class);
            intent.putExtra(WebViewActivity.URL_KEY, notification.link);
            startActivity(intent);
        }
    }

    private void getMovie(String id, String notificationType) {
        if (id == null) return;
        showLoadingDialog(null, null);
        MovieApi.getInstance().getMovieDetail(id, new ApiCallbackV2<Movie>() {

            @Override
            public void onError(String s) {
                hideLoadingDialog();
            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onSuccess(String msg, Movie result) throws JSONException {
                hideLoadingDialog();
                if (result != null) {
                    if (!StringUtils.isEmpty(result.getIdGroup()) && !result.getIdGroup().equals("0")) {
                        VideoPlayerActivity.start(DetailNotificationActivity.this, result, "", true);
                    } else {
                        Video video = Movie.movie2Video(result);
                        VideoPlayerActivity.playNow(DetailNotificationActivity.this, result, video, "");
                    }
                }
            }
        });
    }

    private void getServiceDetail(KhNotificationItem notification) {
        showLoadingDialog("", "");
        if (notification.isExchange == 0) {
            new MetfonePlusClient().wsGetServiceDetail(notification.actionObjectId, new MPApiCallback<WsGetServiceDetailResponse>() {
                @Override
                public void onResponse(Response<WsGetServiceDetailResponse> response) {
                    hideLoadingDialog();
                    if (response.body() != null && response.body().getResult().getWsResponse() != null) {
                        openRedirectActivity(notification, response.body().getResult().getWsResponse());
                    }
                }

                @Override
                public void onError(Throwable error) {
                    hideLoadingDialog();
                }
            });
        } else if (notification.isExchange == 1) {
            new MetfonePlusClient().wsGetExchangeService(new MPApiCallback<WsGetExchangeServiceResponse>() {
                @Override
                public void onResponse(Response<WsGetExchangeServiceResponse> response) {
                    if (response.body().getResult() != null) {
                        ExchangeItem exchangeItem = null;
                        List<WsGetExchangeServiceResponse.Response> services = response.body().getResult().getData();
                        if (services != null && services.size() > 0) {
                            for (WsGetExchangeServiceResponse.Response service : services) {
                                List<ExchangeItem> items = service.getListItem();
                                for (ExchangeItem exchange : items) {
                                    if (exchange.getGroupCode().equals(notification.actionObjectId)) {
                                        exchangeItem = exchange;
                                        break;
                                    }
                                }
                            }
                        }
                        if (exchangeItem != null) {
                            openRedirectActivity(notification, exchangeItem);
                        }
                    }
                    hideLoadingDialog();
                }

                @Override
                public void onError(Throwable error) {
                    hideLoadingDialog();
                }
            });
        }
    }

    public void getVideoEsport(String videoId) {
        if (compositeDisposable != null)
            compositeDisposable.clear();
        showLoadingDialog("", "");
        RxUtils.async(compositeDisposable, apiService.getVideoById(videoId, getDefaultParam())
                , new ICallBackResponse<DetailVideoResponse>() {
                    @Override
                    public void onSuccess(DetailVideoResponse response) {
                        hideLoadingDialog();
                        addFragment(R.id.fragment_container, LiveVideoFragment.newInstance(response, true), false);
                    }

                    @Override
                    public void onFail(String error) {
                        hideLoadingDialog();
                    }
                });
    }

    public String getDomain() {
        String baseUrl = Constant.BASE_URL_ESPORT;
        try {
            URL url = new URL(baseUrl);
            return url.getHost();
        } catch (Exception e) {
        }
        return baseUrl.replace("https://", "").replace("http://", "");
    }

    public Map<String, Object> getDefaultParam() {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", AccountBusiness.getInstance().getUserId());
        hashMap.put("domain", getDomain());
        hashMap.put("clientType", Constant.CLIENT_TYPE);
        hashMap.put("revision", Constant.REVISION);
        hashMap.put("deviceId", DeviceUtils.getUniqueDeviceId());
        hashMap.put("languageCode", AccountBusiness.getInstance().getLanguageCode());
        return hashMap;
    }

    private void handleNotificationMovie(KhNotificationItem notification) {
        if (notification.actionType.equals(KhNotificationItem.ActionType.MOVIE_PROFILE) || notification.actionType.equals(KhNotificationItem.ActionType.MOVIE_PLAY_DIRECTLY)) {
            getMovie(notification.actionObjectId, notification.actionType);
        } else if (notification.actionType.equals(KhNotificationItem.ActionType.MOVIE_ACTOR) || notification.actionType.equals(KhNotificationItem.ActionType.MOVIE_DIRECTOR) || notification.actionType.equals(KhNotificationItem.ActionType.MOVIE_CATEGORY)) {
            openRedirectActivity(notification);
        }
    }

    private void handleNotificationEsport(KhNotificationItem notification) {
        if (notification.actionType.equals(KhNotificationItem.ActionType.ESPORT_CHANNEL_PROFILE)) {
            long channelId = 0;
            try {
                channelId = Long.parseLong(notification.actionObjectId);
            } catch (Exception e) {
                channelId = -1;
            }
            if (channelId != -1) {
                AloneFragmentActivity.with(this)
                        .parameters(ChannelContainerFragment.newBundle(channelId))
                        .start(ChannelContainerFragment.class);
            }
        } else if (notification.actionType.equals(KhNotificationItem.ActionType.ESPORT_PLAY_DIRECTLY)) {
            getVideoEsport(notification.actionObjectId);
        } else if (notification.actionType.equals(KhNotificationItem.ActionType.ESPORT_TOURNAMENT_PROFILE)) {
            long tournamentId = 0;
            try {
                tournamentId = Long.parseLong(notification.actionObjectId);
            } catch (Exception e) {
                tournamentId = -1;
            }
            if (tournamentId != -1) {
                AloneFragmentActivity.with(this).parameters(TournamentDetailFragment.newBundle(tournamentId))
                        .start(TournamentDetailFragment.class);
            }
        }
    }

    private void handleNotificationMetfone(KhNotificationItem notification) {
        int type = notification.actionType.equals(KhNotificationItem.ActionType.METFONE_SERVICE) ? DeepLinkActivity.TYPE_SERVICES : DeepLinkActivity.TYPE_TOPUP;
        CamIdUserBusiness mCamIdUserBusiness = ((ApplicationController) getApplication()).getCamIdUserBusiness();
        if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
            displayRegisterScreenActivity(true);
            return;
        } else if (StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getToken())) {
            mCamIdUserBusiness.setProcessingLoginSignUpMetfone(true);
            NavigateActivityHelper.navigateToEnterAddPhoneNumber(this);
            return;
        } else if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getPhoneService())) {
            mCamIdUserBusiness.setProcessingLoginSignUpMetfone(true);
            handleServiceList(this, mCamIdUserBusiness, type);
            return;
        }
        if (notification.actionType.equals(KhNotificationItem.ActionType.METFONE_SERVICE)) {
            getServiceDetail(notification);
        } else if (notification.actionType.equals(KhNotificationItem.ActionType.METFONE_TOPUP)) {
            openRedirectActivity(notification);
        }
    }

    private void handleGiftOpen(KhNotificationItem notification) {
        SharedPrefs.getInstance().put("SHOW_NOTIFY", true);
        finish();
    }

    private void handleRewardDetail(KhNotificationItem notification) {
        openRedirectActivity(notification);
    }

    private void handleLoginNow() {
        Intent intent = new Intent(this, RegisterScreenActivity.class);
        startActivity(intent, false);
    }

    private void openRedirectActivity(KhNotificationItem notification) {
        Intent intent = new Intent(DetailNotificationActivity.this, RedirectNotificationActivity.class);
        intent.putExtra(RedirectNotificationActivity.NOTIFICATION_KEY, notification);
        startActivity(intent);
    }

    private void openRedirectActivity(KhNotificationItem notification, WsGetServiceDetailResponse.Response serviceDetail) {
        Intent intent = new Intent(DetailNotificationActivity.this, RedirectNotificationActivity.class);
        intent.putExtra(RedirectNotificationActivity.NOTIFICATION_KEY, notification);
        intent.putExtra(RedirectNotificationActivity.SERVICE_KEY, serviceDetail);
        startActivity(intent);
    }

    private void openRedirectActivity(KhNotificationItem notification, ExchangeItem exchangeItem) {
        Intent intent = new Intent(DetailNotificationActivity.this, RedirectNotificationActivity.class);
        intent.putExtra(RedirectNotificationActivity.NOTIFICATION_KEY, notification);
        intent.putExtra(RedirectNotificationActivity.EXCHANGE_KEY, exchangeItem);
        startActivity(intent);
    }

    private MediaSource buildMediaSource(DefaultDataSourceFactory dataSourceFactory, String url) {
        Uri uri = Uri.parse(url);
        @C.ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultDashChunkSource.Factory(dataSourceFactory), dataSourceFactory).createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultSsChunkSource.Factory(dataSourceFactory), dataSourceFactory).createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    private void initTrailer(String url) {

        playerViewTrailer = new VideoPlayerView(this);
        playerViewTrailer.setUseController(false);
        playerViewTrailer.enableFast(true);
        layoutPlayerTrailer.addView(playerViewTrailer);
        String urlOriginal = url;
        url = XXTEACrypt.decryptBase64StringToString(urlOriginal, BuildConfig.KEY_XXTEA_5DMAX_MOVIES);
        url = url == null ? urlOriginal : url;

        if (playerTrailer == null) {
            TrackSelector trackSelectorDef = new DefaultTrackSelector();
            playerTrailer = ExoPlayerFactory.newSimpleInstance(this, trackSelectorDef);
        }

        String userAgent = Util.getUserAgent(this, getString(R.string.app_name));
        DefaultDataSourceFactory sourceFactory = new DefaultDataSourceFactory(this, userAgent);

        playerTrailer.prepare(buildMediaSource(sourceFactory, url));

        playerViewTrailer.setPlayer(playerTrailer);
        playerTrailer.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_BUFFERING || playbackState == Player.STATE_READY) {
                    if (!isPlaying && playerTrailer.getPlayWhenReady()) {
                        playerTrailer.setPlayWhenReady(false);
                        updateButtonPlay(false);
                    }
                } else if (playbackState == Player.STATE_ENDED) {
                    isPlaying = true;
                    updateButtonPlay(true);
                    currentTime = 0;
                    playerTrailer.seekTo(currentTime);
                } else if (playbackState == Player.STATE_IDLE) {
                    isPlaying = false;
                    updateButtonPlay(false);
                }
                com.metfone.selfcare.util.Log.d(TAG, "isPlaying : " + isPlaying + ". playWhenReady : " + playWhenReady + ", playbackState : " + playbackState);
            }
        });
        btnPlayTrailer.setVisibility(View.VISIBLE);
        btnPlayTrailer.setOnClickListener(view -> {
            if (playerTrailer != null) {
                btnPlayTrailer.removeCallbacks(runnable);
                boolean oldStatus = isPlaying;
                isPlaying = !isPlaying;
                if (oldStatus) {
                    com.metfone.selfcare.util.Log.d(TAG, "Pause");
                    updateButtonPlay(false);
                    currentTime = playerTrailer.getCurrentPosition();
                    playerTrailer.setPlayWhenReady(false);
                    handler.removeCallbacks(runnableTime);
                } else {
                    com.metfone.selfcare.util.Log.d(TAG, "Play");
                    updateButtonPlay(true);
                    playerTrailer.setPlayWhenReady(true);
                    if (currentTime == playerTrailer.getDuration()) {
                        currentTime = 0;
                    }
                    playerTrailer.seekTo(currentTime);
                    btnPlayTrailer.postDelayed(runnable, 2000);
                    handler.removeCallbacks(runnableTime);
                    handler.postDelayed(runnableTime, 0);
                }
            }
        });
        btnMuteTrailer.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_up_new));
        btnMuteTrailer.setOnClickListener(v -> {
            if (playerTrailer != null) {
                if (playerTrailer.getVolume() == 0) {
                    playerTrailer.setVolume(1);
                    btnMuteTrailer.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_up_new));
                } else {
                    playerTrailer.setVolume(0);
                    btnMuteTrailer.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_off_new));
                }
            }
        });
        controlViewTrailer.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                btnPlayTrailer.removeCallbacks(runnable);
                btnPlayTrailer.setVisibility(View.VISIBLE);
                if (playerTrailer != null && playerTrailer.isPlaying()) {
                    btnPlayTrailer.postDelayed(runnable, 2000);
                }
            }
            return false;
        });
        String finalUrl = url;
        btnFullTrailer.setOnClickListener(v -> {
            if (playerTrailer != null) {
                playerTrailer.setPlayWhenReady(false);
                updateButtonPlay(false);
                currentTime = playerTrailer.getCurrentPosition();
            }
            progress_bar.removeCallbacks(runnableTime);
            trailerFullScreenPlayer = new TrailerFullScreenPlayerDialog(this);
            trailerFullScreenPlayer.setData("", finalUrl, currentTime);
            trailerFullScreenPlayer.setTrailerPlayerListener(new TrailerPlayerListener() {
                @Override
                public void dismissTrailerDialog(boolean continuePlay, boolean isEnd, long time) {
                    currentTime = time;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    if (isEnd) {
                        isPlaying = false;
                        updateButtonPlay(false);
                        currentTime = 0;
                        playerTrailer.seekTo(currentTime);
                    } else {
                        progress_bar.setProgress((int) (currentTime * 100 / playerTrailer.getDuration()));
                        playerTrailer.seekTo(currentTime);
                        playerTrailer.setPlayWhenReady(continuePlay);
                        updateButtonPlay(continuePlay);
                    }
                }
            });
            trailerFullScreenPlayer.show();
        });
        // auto play when open detail
        isPlaying = true;
        playerTrailer.setPlayWhenReady(true);
        updateButtonPlay(true);
        btnPlayTrailer.setVisibility(View.INVISIBLE);
        com.metfone.selfcare.util.Log.d(TAG, "Init available now");
    }

    private void updateButtonPlay(boolean play) {
        if (play) {
            btnPlayTrailer.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_available));
            handler.removeCallbacks(runnableTime);
            handler.postDelayed(runnableTime, 0);
        } else {
            btnPlayTrailer.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_available));
            handler.removeCallbacks(runnableTime);
            btnPlayTrailer.setVisibility(View.VISIBLE);
        }
    }

    public void stopTrailer() {
        if (playerTrailer == null) {
            return;
        }
        if (!isPlaying) {
            if (playerTrailer.getPlayWhenReady()) {
                playerTrailer.setPlayWhenReady(false);
                updateButtonPlay(false);
                currentTime = playerTrailer.getCurrentPosition();
                progress_bar.removeCallbacks(runnableTime);
                btnPlayTrailer.setVisibility(View.VISIBLE);
            }
            return;
        }
        isPlaying = false;
        playerTrailer.setPlayWhenReady(false);
        updateButtonPlay(false);
        currentTime = playerTrailer.getCurrentPosition();
        progress_bar.removeCallbacks(runnableTime);
        btnPlayTrailer.setVisibility(View.VISIBLE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void requestPermissionBright(RequestPermissionBright requestPermissionBright) {
        if (dialogConfirm == null) {
            dialogConfirm = new DialogConfirm(this, true);
            dialogConfirm.setLabel(getString(R.string.permission_allow_floating_view));
            dialogConfirm.setMessage(getString(R.string.permission_allow_change_brightness));
            dialogConfirm.setUseHtml(true);
            dialogConfirm.setCancelable(false);
            dialogConfirm.setNegativeLabel(getString(R.string.cancel));
            dialogConfirm.setPositiveLabel(getString(R.string.ok));
            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
            dialogConfirm.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    dialogConfirm.dismiss();
                }
            });
        }
        if (!dialogConfirm.isShowing()) {
            dialogConfirm.show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void requestPermissionVolume(RequestPermissionVolume requestPermissionVolume) {
        if (dialogVolume == null) {
            dialogVolume = new DialogConfirm(this, true);
            dialogVolume.setLabel(getString(R.string.permission_allow_floating_view));
            dialogVolume.setMessage(getString(R.string.permission_allow_change_volume));
            dialogVolume.setUseHtml(true);
            dialogVolume.setCancelable(false);
            dialogVolume.setNegativeLabel(getString(R.string.cancel));
            dialogVolume.setPositiveLabel(getString(R.string.ok));
            dialogVolume.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                    startActivity(intent);
                }
            });
            dialogVolume.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    dialogVolume.dismiss();
                }
            });
        }
        if (!dialogVolume.isShowing()) {
            dialogVolume.show();
        }
    }

    class MyJavaScriptInterface {
        @JavascriptInterface
        public void clickUrl(String url) {
            Intent intent = new Intent(DetailNotificationActivity.this, WebViewActivity.class);
            intent.putExtra(WebViewActivity.URL_KEY, url);
            startActivity(intent);
        }

        @JavascriptInterface
        public void resize(final float height) {
            DetailNotificationActivity.this.runOnUiThread(() -> {
                int heightPx = (int) (height * getResources().getDisplayMetrics().density);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) webView.getLayoutParams();
                params.height = heightPx;
                webView.setLayoutParams(params);
            });
        }
    }
}

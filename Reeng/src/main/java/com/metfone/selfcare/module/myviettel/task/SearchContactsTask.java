/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.task;

import android.os.AsyncTask;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.myviettel.listener.SearchContactsListener;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class SearchContactsTask extends AsyncTask<String, Void, ArrayList<Object>> {
    private final String TAG = "SearchContactsTask";
    private WeakReference<ApplicationController> mApplication;
    private String keySearch;
    private SearchContactsListener listener;
    private CopyOnWriteArrayList<PhoneNumber> data;
    private long startTime;
    private int totalContacts;
    private Comparator comparatorName;
    private Comparator comparatorKeySearch;

    public SearchContactsTask(ApplicationController application) {
        this.mApplication = new WeakReference<>(application);
    }

    public void setData(ArrayList<PhoneNumber> data) {
        this.data = new CopyOnWriteArrayList<>(data);
    }

    public void setListener(SearchContactsListener listener) {
        this.listener = listener;
    }

    public void setComparatorName(Comparator comparatorName) {
        this.comparatorName = comparatorName;
    }

    public void setComparatorKeySearch(Comparator comparatorKeySearch) {
        this.comparatorKeySearch = comparatorKeySearch;
    }

    @Override
    protected void onPreExecute() {
        startTime = System.currentTimeMillis();
        if (listener != null) listener.onPrepareSearchContacts();
    }

    @Override
    protected void onPostExecute(@NonNull ArrayList<Object> result) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, "onPostExecute search: '" + keySearch + "' result: " + result.size() + "/" + totalContacts + " done at " + (System.currentTimeMillis() - startTime) + " ms.");
        if (listener != null) listener.onFinishedSearchContacts(keySearch, result);
    }

    @Override
    protected ArrayList<Object> doInBackground(String... params) {
        ArrayList<Object> list = new ArrayList<>();
        keySearch = params[0];
        if (Utilities.notNull(mApplication) && data != null) {
            totalContacts = data.size();
            if (keySearch == null) keySearch = "";
            else keySearch = keySearch.trim().toLowerCase(Locale.US);
            String keySearchTmp = TextHelper.convertUnicodeForSearch(keySearch).replaceAll(Constants.PATTERN.KEY_SEARCH_REPLACE, "");
            Log.d(TAG, "search keySearch: " + keySearch + ", keySearchTmp: " + keySearchTmp);
            if (TextUtils.isEmpty(keySearch) || TextUtils.isEmpty(keySearchTmp)) {
                list.addAll(data);
            } else {
                String keySearchStartWithPlus = "";
                if (keySearch.startsWith("+")) keySearchStartWithPlus = keySearch.substring(1);
                Log.d(TAG, "keySearchStartWithPlus: " + keySearchStartWithPlus);
                if (TextUtils.isDigitsOnly(keySearch)) {
                    searchWithDigits(list, keySearch, data);
                } else if (!TextUtils.isEmpty(keySearchStartWithPlus) && TextUtils.isDigitsOnly(keySearchStartWithPlus)) {
                    searchWithDigits(list, keySearch, data);
                } else if (TextUtils.isDigitsOnly(keySearchTmp)) {
                    searchWithDigits(list, keySearchTmp, data);
                } else {
                    searchWithText(list, keySearch, data);
                }
            }
        }
        return list;
    }

    private void searchWithDigits(ArrayList<Object> list, String keySearch, CopyOnWriteArrayList<PhoneNumber> data) {
        //todo keySearch là số
        String oldKeySearch = keySearch;
        keySearch = SearchUtils.getKeySearchChat(keySearch);
        String tmpNumber = PhoneNumberHelper.getInstant().getPhoneNumberFromText(mApplication.get(), keySearch);
        if (Utilities.notEmpty(tmpNumber)) {
            keySearch = tmpNumber;
        }
        Log.d(TAG, "searchWithDigits oldKeySearch: " + oldKeySearch + ", keySearch: " + keySearch);
        ArrayList<PhoneNumber> listResults01 = new ArrayList<>(); //todo tên giống hệt keySearch
        ArrayList<PhoneNumber> listResults02 = new ArrayList<>(); //todo SĐT giống hệt keySearch
        ArrayList<PhoneNumber> listResults03 = new ArrayList<>(); //todo tên bắt đầu bằng keySearch
        ArrayList<PhoneNumber> listResults04 = new ArrayList<>(); //todo SĐT bắt đầu bằng keySearch
        ArrayList<PhoneNumber> listResults05 = new ArrayList<>(); //todo SĐT chứa bằng keySearch
        Map<String, Integer> mapContacts = new HashMap<>();
        if (Utilities.notEmpty(data)) {
            ArrayList<PhoneNumber> listTmp = new ArrayList<>();
            for (PhoneNumber item : data) {
                String name = item.getName();
                if (name == null) name = "";
                else name = name.trim();
                String phoneNumber = item.getJidNumber();
                if (phoneNumber == null) phoneNumber = "";
                if (Utilities.notEmpty(phoneNumber)
                        && (name.contains(keySearch) || phoneNumber.contains(keySearch))) {
                    item.setName(name);
                    listTmp.add(item);
                } else {
                    if (keySearch.startsWith("+") && keySearch.length() > 3) {
                        String rawNumber;
                        if (phoneNumber.startsWith("0")) {// so vn
                            rawNumber = "+84" + phoneNumber.substring(1);
                        } else {
                            rawNumber = item.getRawNumber();
                        }
                        if (Utilities.notEmpty(rawNumber) && rawNumber.contains(keySearch)) {
                            item.setName(name);
                            listTmp.add(item);
                        }
                    }
                }
            }
            for (PhoneNumber item : listTmp) {
                String phoneNumber = item.getJidNumber();
                if (Utilities.notEmpty(phoneNumber) && !mapContacts.containsKey(phoneNumber)) {
                    String name = item.getName();
                    if (name == null) name = "";
                    else name = name.trim();
                    if (name.equals(keySearch)) {
                        listResults01.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.equals(keySearch)) {
                        listResults02.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (name.startsWith(keySearch)) {
                        listResults03.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.startsWith(keySearch)) {
                        listResults04.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.contains(keySearch)) {
                        listResults05.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (keySearch.startsWith("+") && keySearch.length() > 3) {
                        String rawNumber;
                        if (phoneNumber.startsWith("0")) {// so vn
                            rawNumber = "+84" + phoneNumber.substring(1);
                        } else {
                            rawNumber = item.getRawNumber();
                        }
                        Log.d(TAG, "phoneNumber: " + phoneNumber + ", rawNumber: " + rawNumber);
                        if (Utilities.notEmpty(rawNumber) && rawNumber.contains(keySearch)) {
                            listResults05.add(item);
                            mapContacts.put(phoneNumber, 1);
                        }
                    }
                }
            }
        }
        if (Utilities.notEmpty(listResults01)) {
            list.addAll(listResults01);
            listResults01.clear();
        }
        if (Utilities.notEmpty(listResults02)) {
            list.addAll(listResults02);
            listResults02.clear();
        }
        if (Utilities.notEmpty(listResults03)) {
            list.addAll(listResults03);
            listResults03.clear();
        }
        if (Utilities.notEmpty(listResults04)) {
            list.addAll(listResults04);
            listResults04.clear();
        }
        if (Utilities.notEmpty(listResults05)) {
            list.addAll(listResults05);
            listResults05.clear();
        }
        if (!mapContacts.containsKey(keySearch) && PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.get(), keySearch)) {
            list.add(keySearch);
        }
        mapContacts.clear();
    }

    private void searchWithText(ArrayList<Object> list, String keySearch, CopyOnWriteArrayList<PhoneNumber> data) {
        //todo keySearch là không phải là chữ
        Log.d(TAG, "searchWithText: " + keySearch);
        ArrayList<PhoneNumber> listResults01 = new ArrayList<>(); //todo tên giống hệt keySearch
        ArrayList<PhoneNumber> listResults02 = new ArrayList<>(); //todo tên bỏ dấu giống hệt keySearch bỏ dấu
        ArrayList<PhoneNumber> listResults03 = new ArrayList<>(); //todo danh bạ có: tên bắt đầu bằng keySearch hoặc tên bỏ dấu bắt đầu bằng keySearch bỏ dấu hoặc tên đã bỏ dấu bắt đầu phân tách thành các từ đơn, keySearch đã bỏ dấu phân tách thành các từ đơn, tất cả các từ đơn của kết quả phân tách của keySearch đều là ký tự bắt đầu trong danh sách tất cả các từ đơn của kết qủa phân tách tên. Sắp xếp theo tên abc.
        Map<String, Integer> mapContacts = new HashMap<>();
        String keySearchUnmark = TextHelper.convertUnicodeForSearch(keySearch).toLowerCase(Locale.US).trim();
        List<String> listKeySearch = new ArrayList<>(Arrays.asList(keySearchUnmark.split(Constants.PATTERN.KEY_SEARCH_REPLACE)));
        Log.d(TAG, "listKeySearch: " + listKeySearch);
        int sizeKeySearch = listKeySearch.size();
        if (sizeKeySearch > 1 && comparatorKeySearch != null) {
            Collections.sort(listKeySearch, comparatorKeySearch);
            Log.d(TAG, "listKeySearch: " + listKeySearch);
        }
        ArrayList<PhoneNumber> listTmp = new ArrayList<>();
        for (PhoneNumber item : data) {
            if (Utilities.notEmpty(item.getJidNumber())) {
                String nameForSearch = TextHelper.convertUnicodeForSearch(item.getName()).toLowerCase(Locale.US).trim();
                if (nameForSearch.contains(keySearchUnmark)) {
                    listTmp.add(item);
                } else {
                    boolean check = true;
                    for (String key : listKeySearch) {
                        if (key != null && !nameForSearch.contains(key)) {
                            check = false;
                            break;
                        }
                    }
                    if (check) listTmp.add(item);
                }
            }
        }
        for (PhoneNumber item : listTmp) {
            String phoneNumber = item.getJidNumber();
            if (Utilities.notEmpty(phoneNumber) && !mapContacts.containsKey(phoneNumber)) {
                String title = item.getName();
                if (title == null) {
                    title = "";
                } else {
                    title = title.trim();
                }
                String name = title.toLowerCase(Locale.US);
                if (Utilities.notEmpty(name)) {
                    String nameForSearch = TextHelper.convertUnicodeForSearch(title);
                    String nameUnmark = nameForSearch.toLowerCase(Locale.US);
                    if (name.equalsIgnoreCase(keySearch)) {
                        listResults01.add(item);
                    } else if (nameUnmark.equalsIgnoreCase(keySearchUnmark)) {
                        listResults02.add(item);
                    } else if (name.startsWith(keySearch)) {
                        listResults03.add(item);
                    } else if (nameUnmark.startsWith(keySearchUnmark)) {
                        listResults03.add(item);
                    } else {
                        //old filter
                        boolean check = false;
                        List<String> listNameUnmark = SearchUtils.getListSplitName(nameForSearch);
                        int sizeNameUnmark = listNameUnmark.size();
                        if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                            if (comparatorName != null && sizeNameUnmark > 1)
                                Collections.sort(listNameUnmark, comparatorName);
                            for (String tmp : listKeySearch) {
                                if (Utilities.notEmpty(tmp)) {
                                    boolean checkTmp = false;
                                    for (String nameTmp : listNameUnmark) {
                                        if (nameTmp.startsWith(tmp)) {
                                            checkTmp = true;
                                            listNameUnmark.remove(nameTmp);
                                            break;
                                        }
                                    }
                                    check = checkTmp;
                                    if (!check) break;
                                }
                            }
                            if (check) listResults03.add(item);
                        }
                        if (!check) {
                            //new filter
                            listNameUnmark = SearchUtils.getListSplitKey(nameForSearch);
                            sizeNameUnmark = listNameUnmark.size();
                            if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                                if (comparatorName != null && sizeNameUnmark > 1)
                                    Collections.sort(listNameUnmark, comparatorName);
                                for (String tmp : listKeySearch) {
                                    if (Utilities.notEmpty(tmp)) {
                                        boolean checkTmp = false;
                                        for (String nameTmp : listNameUnmark) {
                                            if (nameTmp.startsWith(tmp)) {
                                                checkTmp = true;
                                                listNameUnmark.remove(nameTmp);
                                                break;
                                            }
                                        }
                                        check = checkTmp;
                                        if (!check) break;
                                    }
                                }
                                if (check) listResults03.add(item);
                            }
                        }
                    }
                }
            }
        }

        if (Utilities.notEmpty(listResults01)) {
            list.addAll(listResults01);
            listResults01.clear();
        }
        if (Utilities.notEmpty(listResults02)) {
            list.addAll(listResults02);
            listResults02.clear();
        }
        if (Utilities.notEmpty(listResults03)) {
            list.addAll(listResults03);
            listResults03.clear();
        }
        mapContacts.clear();
    }

}
package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsGetAccountRankInfoResponse extends BaseResponse<WsGetAccountRankInfoResponse.Response> {
    public class Response {
        @SerializedName("accountRankDTO")
        public AccountRankDTO accountRankDTO;
    }
}

package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GiftItems implements Serializable {
    @SerializedName("giftTypeId")
    @Expose
    private String giftTypeId;
    @SerializedName("giftName")
    @Expose
    private String giftName;
    @SerializedName("giftCode")
    @Expose
    private String giftCode;
    @SerializedName("giftPoint")
    @Expose
    private int giftPoint;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("giftId")
    @Expose
    private String giftId;
    @SerializedName("partnerName")
    @Expose
    private String partnerName;
    @SerializedName("imageList")
    @Expose
    private List<String> imageList = null;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("score")
    @Expose
    private String score;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("expireDate")
    @Expose
    private String expireDate;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("isHot")
    @Expose
    private int isHot;
    @SerializedName("isRecommentGift")
    @Expose
    private int isRecommentGift;
    @SerializedName("discountRate")
    @Expose
    private String discountRate;
    @SerializedName("rankingList")
    @Expose
    private List<String> rankingList = null;
    @SerializedName("partnerId")
    @Expose
    private String partnerId;
    @SerializedName("distance")
    @Expose
    private String distance;

    public String getGiftTypeId() {
        return giftTypeId;
    }

    public void setGiftTypeId(String giftTypeId) {
        this.giftTypeId = giftTypeId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftCode() {
        return giftCode;
    }

    public void setGiftCode(String giftCode) {
        this.giftCode = giftCode;
    }

    public int getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(int giftPoint) {
        this.giftPoint = giftPoint;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getIsHot() {
        return isHot;
    }

    public void setIsHot(int isHot) {
        this.isHot = isHot;
    }

    public int getIsRecommentGift() {
        return isRecommentGift;
    }

    public void setIsRecommentGift(int isRecommentGift) {
        this.isRecommentGift = isRecommentGift;
    }

    public String getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate;
    }

    public List<String> getRankingList() {
        return rankingList;
    }

    public void setRankingList(List<String> rankingList) {
        this.rankingList = rankingList;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

}
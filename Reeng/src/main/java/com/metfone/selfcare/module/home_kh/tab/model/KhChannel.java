package com.metfone.selfcare.module.home_kh.tab.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.R;

public class KhChannel implements IHomeModelType{
    public static final int VIDEO = 0;
    public static final int LIVE_STREAM = 1;
    @SerializedName("id")
    public int id;
    @SerializedName("username")
    public String username;
    @SerializedName("title")
    public String title;
    @SerializedName("description")
    public String description;
    @SerializedName("original_path")
    public String original_path;
    @SerializedName("image_path_thumb")
    public String image_path_thumb;
    @SerializedName("image_path_small")
    public String image_path_small;
    @SerializedName("total_view")
    public int total_view;
    @SerializedName("link")
    public String link;
    @SerializedName("gameInfo")
    public GameInfo gameInfo;
    @SerializedName("channelInfo")
    public ChannelInfo channelInfo;
    @SerializedName("isLive")
    public int isLive;

    @Override
    public int getItemType() {
        return IHomeModelType.POPULAR_LIVE_CHANNEL;
    }

    public static class GameInfo {
        @SerializedName("id")
        public int id;
        @SerializedName("gameName")
        public String gameName;
        @SerializedName("gameDesc")
        public String gameDesc;
        @SerializedName("gameImage")
        public String gameImage;
    }

    public class ChannelInfo {
        @SerializedName("id")
        public int id;
        @SerializedName("name")
        public String name;
        @SerializedName("urlImagesCover")
        public String urlImagesCover;
    }
}

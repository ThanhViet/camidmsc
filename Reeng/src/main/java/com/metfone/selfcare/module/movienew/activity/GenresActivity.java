package com.metfone.selfcare.module.movienew.activity;

import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.module.movienew.fragment.GenresFragment;
import com.metfone.selfcare.util.Utilities;

public class GenresActivity extends BaseSlidingFragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set SystemUI & hide navigation
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        setContentView(R.layout.activity_genres);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, GenresFragment.newInstance()).commitAllowingStateLoss();
    }
}

package com.metfone.selfcare.module.tiin.childtiin.view;

import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.util.List;

public interface ChildTiinMvpView extends MvpView {
    void loadDataSuccess(boolean flag);

    void bindData(List<TiinModel> response);
}

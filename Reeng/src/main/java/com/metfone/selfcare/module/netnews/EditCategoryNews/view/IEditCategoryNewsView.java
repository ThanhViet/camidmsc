package com.metfone.selfcare.module.netnews.EditCategoryNews.view;


import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.CategoryResponse;

/**
 * Created by HaiKE on 9/19/17.
 */

public interface IEditCategoryNewsView extends MvpView {

    void updateDataSuccess(String str, int type);
    void bindData(CategoryResponse categoryResponse);
    void loadDataSuccess(boolean flag);
}

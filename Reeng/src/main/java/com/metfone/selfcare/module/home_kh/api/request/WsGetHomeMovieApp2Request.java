package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetHomeMovieApp2Request extends BaseRequest<WsGetHomeMovieApp2Request.Request> {

    public static class Request {
        @SerializedName("ref_id")
        private String refID;
        @SerializedName("device_id")
        private String deviceID;
        private String platform;
        private String offset;
        private String limit;
        private String revision;
        private String requestClientType;
        private String clientType;
        private String version;
        private String language;
        private String msisdn;
        @SerializedName("recommend_categoryid")
        private String recommendCategoryid;
        @SerializedName("avaiable_categoryid")
        private String avaiableCategoryid;
        @SerializedName("award_categoryid")
        private String awardCategoryid;
        private String password;
        private String username;

        public String getRefID() { return refID; }
        public void setRefID(String value) { this.refID = value; }

        public String getDeviceID() { return deviceID; }
        public void setDeviceID(String value) { this.deviceID = value; }

        public String getPlatform() { return platform; }
        public void setPlatform(String value) { this.platform = value; }

        public String getOffset() { return offset; }
        public void setOffset(String value) { this.offset = value; }

        public String getLimit() { return limit; }
        public void setLimit(String value) { this.limit = value; }

        public String getRevision() { return revision; }
        public void setRevision(String value) { this.revision = value; }

        public String getRequestClientType() { return requestClientType; }
        public void setRequestClientType(String value) { this.requestClientType = value; }

        public String getClientType() { return clientType; }
        public void setClientType(String value) { this.clientType = value; }

        public String getVersion() { return version; }
        public void setVersion(String value) { this.version = value; }

        public String getLanguage() { return language; }
        public void setLanguage(String value) { this.language = value; }

        public String getMsisdn() { return msisdn; }
        public void setMsisdn(String value) { this.msisdn = value; }

        public String getRecommendCategoryid() { return recommendCategoryid; }
        public void setRecommendCategoryid(String value) { this.recommendCategoryid = value; }

        public String getAvaiableCategoryid() { return avaiableCategoryid; }
        public void setAvaiableCategoryid(String value) { this.avaiableCategoryid = value; }

        public String getAwardCategoryid() { return awardCategoryid; }
        public void setAwardCategoryid(String value) { this.awardCategoryid = value; }

        public String getPassword() { return password; }
        public void setPassword(String value) { this.password = value; }

        public String getUsername() { return username; }
        public void setUsername(String value) { this.username = value; }
    }

}

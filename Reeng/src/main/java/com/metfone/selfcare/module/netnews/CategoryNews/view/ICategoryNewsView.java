package com.metfone.selfcare.module.netnews.CategoryNews.view;

import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.NewsResponse;

/**
 * Created by HaiKE on 8/18/17.
 */

public interface ICategoryNewsView extends MvpView {

    void loadDataSuccess(boolean flag);

    void bindData(NewsResponse childNewsResponse);
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/7/3
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.search.adapter.SearchAllAdapter;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.NotFound;

import butterknife.BindView;
import butterknife.OnClick;

public class NotFoundHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.line)
    View line;
    SearchAllListener.OnAdapterClick listener;
    private NotFound data;

    public NotFoundHolder(View view, final SearchAllListener.OnAdapterClick listener) {
        super(view);
        this.listener = listener;
    }

    @Override
    public void bindData(Object item, int position) {
        if (line != null) {
            line.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
        }
        if (item instanceof NotFound) {
            data = (NotFound) item;
            if (tvTitle != null) {
                tvTitle.setText(data.getTitle());
                //tvTitle.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
            }
            if (tvName != null) tvName.setText(data.getName());
        }
    }

    @OnClick(R.id.layout_title)
    public void onClickItem() {
        if (listener != null && data != null) {
            switch (data.getSearchType()) {
                case SearchAllAdapter.TYPE_CHAT:
                    if (listener instanceof SearchAllListener.OnClickBoxThreadChat)
                        ((SearchAllListener.OnClickBoxThreadChat) listener).onClickThreadChatMore();
                    break;
                case SearchAllAdapter.TYPE_VIDEO:
                    if (listener instanceof SearchAllListener.OnClickBoxVideo)
                        ((SearchAllListener.OnClickBoxVideo) listener).onClickVideoMore();
                    break;
                case SearchAllAdapter.TYPE_CHANNEL:
                    if (listener instanceof SearchAllListener.OnClickBoxVideo)
                        ((SearchAllListener.OnClickBoxVideo) listener).onClickChannelMore();
                    break;
                case SearchAllAdapter.TYPE_NEWS:
                    if (listener instanceof SearchAllListener.OnClickBoxNews)
                        ((SearchAllListener.OnClickBoxNews) listener).onClickNewsMore();
                    break;
                case SearchAllAdapter.TYPE_MOVIES:
                    if (listener instanceof SearchAllListener.OnClickBoxMovies)
                        ((SearchAllListener.OnClickBoxMovies) listener).onClickMoviesMore();
                    break;
                case SearchAllAdapter.TYPE_MUSIC:
                    if (listener instanceof SearchAllListener.OnClickBoxMusic)
                        ((SearchAllListener.OnClickBoxMusic) listener).onClickMusicMore();
                    break;
                case SearchAllAdapter.TYPE_TIIN:
                    if (listener instanceof SearchAllListener.OnClickBoxTiin)
                        ((SearchAllListener.OnClickBoxTiin) listener).onClickTiinMore();
                    break;
                case SearchAllAdapter.TYPE_REWARD:
                    if (listener instanceof SearchAllListener.OnClickReward) {
                        ((SearchAllListener.OnClickReward) listener).onClickReward(null);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
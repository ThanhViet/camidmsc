package com.metfone.selfcare.module.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.TopNowModel;

import java.util.ArrayList;

/**
 * Created by HaiKE on 9/11/17.
 */

public class TopNowResponse extends ErrorResponse{
    @SerializedName("data")
    @Expose
    private ArrayList<TopNowModel> data = new ArrayList<>();

    public ArrayList<TopNowModel> getData() {
        return data;
    }

    public void setData(ArrayList<TopNowModel> data) {
        this.data = data;
    }
}

package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetAccountRankInfoRequest extends BaseRequest<WsGetAccountRankInfoRequest.Request> {
    public class Request {
        @SerializedName("language")
        private String language;
        @SerializedName("custId")
        public String custId;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

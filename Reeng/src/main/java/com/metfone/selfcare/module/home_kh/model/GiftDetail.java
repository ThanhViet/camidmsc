package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GiftDetail implements Serializable {
    @SerializedName("giftTypeId")
    @Expose
    private String giftTypeId;
    @SerializedName("giftName")
    @Expose
    private String giftName;
    @SerializedName("giftCode")
    @Expose
    private String giftCode;
    @SerializedName("giftPoint")
    @Expose
    private int giftPoint;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("giftId")
    @Expose
    private String giftId;
    @SerializedName("partnerName")
    @Expose
    private String partnerName;
    @SerializedName("imageList")
    @Expose
    private List<String> imageList = null;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("expireDate")
    @Expose
    private String expireDate;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("isHot")
    @Expose
    private int isHot;
    @SerializedName("isRecommentGift")
    @Expose
    private int isRecommentGift;
    @SerializedName("discountRate")
    @Expose
    private String discountRate;
    @SerializedName("storeList")
    @Expose
    private List<String> storeList = null;
    @SerializedName("storeListDetail")
    @Expose
    private List<StoreListDetail> storeListDetail = null;
    @SerializedName("webSite")
    @Expose
    private String webSite;
    @SerializedName("rankingList")
    @Expose
    private List<String> rankingList = new ArrayList<>();
    @SerializedName("partnerId")
    @Expose
    private String partnerId;
    @SerializedName("exchangeCode")
    @Expose
    private String exchangeCode;
    @SerializedName("giftQrCode")
    @Expose
    private String giftQrCode;
    @SerializedName("isRedeemed")
    @Expose
    private int isRedeemed;
    @SerializedName("isUsed")
    @Expose
    private int isUsed;
    @SerializedName("validUntil")
    @Expose
    private String validUntil;
    @SerializedName("isCoupon")
    @Expose
    private int isCoupon;
    @SerializedName("linkCoupon")
    @Expose
    private String linkCoupon;
    @SerializedName("imageCoupon")
    @Expose
    private String imageCoupon;
    @SerializedName("redeemCode")
    @Expose
    private String redeemCode;

    public String getGiftTypeId() {
        return giftTypeId;
    }

    public void setGiftTypeId(String giftTypeId) {
        this.giftTypeId = giftTypeId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftCode() {
        return giftCode;
    }

    public void setGiftCode(String giftCode) {
        this.giftCode = giftCode;
    }

    public int getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(int giftPoint) {
        this.giftPoint = giftPoint;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getIsHot() {
        return isHot;
    }

    public void setIsHot(int isHot) {
        this.isHot = isHot;
    }

    public int getIsRecommentGift() {
        return isRecommentGift;
    }

    public void setIsRecommentGift(int isRecommentGift) {
        this.isRecommentGift = isRecommentGift;
    }

    public String getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate;
    }

    public List<String> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<String> storeList) {
        this.storeList = storeList;
    }

    public List<StoreListDetail> getStoreListDetail() {
        return storeListDetail;
    }

    public void setStoreListDetail(List<StoreListDetail> storeListDetail) {
        this.storeListDetail = storeListDetail;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public List<String> getRankingList() {
        return rankingList;
    }

    public void setRankingList(List<String> rankingList) {
        this.rankingList = rankingList;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public String getGiftQrCode() {
        return giftQrCode;
    }

    public void setGiftQrCode(String giftQrCode) {
        this.giftQrCode = giftQrCode;
    }

    public int getIsRedeemed() {
        return isRedeemed;
    }

    public void setIsRedeemed(int isRedeemed) {
        this.isRedeemed = isRedeemed;
    }

    public int getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }

    public String getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }

    public int getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(int isCoupon) {
        this.isCoupon = isCoupon;
    }

    public String getLinkCoupon() {
        return linkCoupon;
    }

    public void setLinkCoupon(String linkCoupon) {
        this.linkCoupon = linkCoupon;
    }

    public String getImageCoupon() {
        return imageCoupon;
    }

    public void setImageCoupon(String imageCoupon) {
        this.imageCoupon = imageCoupon;
    }

    public String getRedeemCode() {
        return redeemCode;
    }

    public void setRedeemCode(String redeemCode) {
        this.redeemCode = redeemCode;
    }
}

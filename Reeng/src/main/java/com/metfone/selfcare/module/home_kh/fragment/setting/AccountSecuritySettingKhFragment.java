package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.view.View;

import androidx.appcompat.widget.LinearLayoutCompat;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.util.Log;

import butterknife.OnClick;

public class AccountSecuritySettingKhFragment extends BaseSettingKhFragment {
    private static final String TAG = AccountSecuritySettingKhFragment.class.getSimpleName();
    private SettingKhFragment.OnFragmentSettingListener mListener;
    public static final String ARG_FUNCTION_ID = "arg_func_id";
    private int mFuncId = -1;
    protected ApplicationController app;
    private LinearLayoutCompat viewAccountDetail;
    private LinearLayoutCompat viewHideThreadChat;
    private LinearLayoutCompat viewPrivacy;
    private LinearLayoutCompat viewCustomizeTabs;
    private LinearLayoutCompat viewBackup;

    public static AccountSecuritySettingKhFragment newInstance() {
        return new AccountSecuritySettingKhFragment();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        app = (ApplicationController) mParentActivity.getApplication();
        try {
            mListener = (SettingKhFragment.OnFragmentSettingListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mFuncId = getArguments().getInt(ARG_FUNCTION_ID);
        }
        findComponentViews(view);
        setTitle(R.string.title_account);
        onShowViewLogged();
    }

    @Override
    public String getName() {
        return "AccountSecuritySettingFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_account_security_setting_kh;
    }

    public LoginVia isLogin() {
        String mochaToken = ApplicationController.self().getReengAccountBusiness().getToken();
        String openIDToken = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_ACCESS_TOKEN, String.class);
        if (mochaToken == null) mochaToken ="";
        if (openIDToken == null) openIDToken ="";

        if (mochaToken.isEmpty() && openIDToken.isEmpty()) {
            return LoginVia.NOT;
        } else if (mochaToken.isEmpty()) {
            return LoginVia.OPEN_ID;
        } else {
            return LoginVia.MOCHA_OPEN_ID;
        }
    }

    public enum LoginVia{
        MOCHA, OPEN_ID, MOCHA_OPEN_ID, NOT
    }

    private void onShowViewLogged() {
        if (isLogin() != LoginVia.NOT) {
            viewAccountDetail.setVisibility(View.VISIBLE);
            viewHideThreadChat.setVisibility(View.VISIBLE);
            viewPrivacy.setVisibility(View.VISIBLE);
            viewCustomizeTabs.setVisibility(View.VISIBLE);
            viewBackup.setVisibility(View.VISIBLE);
        } else {
            viewAccountDetail.setVisibility(View.GONE);
            viewHideThreadChat.setVisibility(View.GONE);
            viewPrivacy.setVisibility(View.GONE);
            viewCustomizeTabs.setVisibility(View.VISIBLE);
            viewBackup.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.setting_account_detail, R.id.setting_privacy,
            R.id.setting_customize_tabs, R.id.setting_backup, R.id.setting_hide_thread_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.setting_account_detail:
//                if (mListener != null)
//                mListener.navigateToBackup();
                NavigateActivityHelper.navigateToMyProfileNew(mParentActivity);
                break;
//            case R.id.setting_change_number:
//                DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, "mocha://changenumber");
//                break;
            case R.id.setting_privacy:
//                showDeactiveAccountPopup(mParentActivity);
                if (mListener != null) {
                    mListener.navigateToSettingDetail(Constants.SETTINGS.SETTING_PRIVATE);
                }
                break;
            case R.id.setting_customize_tabs:
                NavigateActivityHelper.navigateToSettingKhActivity(mParentActivity, Constants.SETTINGS.SETTING_CONFIG_TAG);
//                if (mListener != null)
//                    mListener.navigateToHideThread();
                break;
            case R.id.setting_backup:
                if (mListener != null) {
                    mListener.navigateToBackup();
                }
                break;
            case R.id.setting_hide_thread_chat:
                if (mListener != null) {
                    mListener.navigateToHideThread();
                }
                break;
        }
    }

    private void findComponentViews(View rootView) {
        viewAccountDetail = rootView.findViewById(R.id.setting_account_detail);
        viewHideThreadChat = rootView.findViewById(R.id.setting_hide_thread_chat);
        viewPrivacy = rootView.findViewById(R.id.setting_privacy);
        viewCustomizeTabs = rootView.findViewById(R.id.setting_customize_tabs);
        viewBackup = rootView.findViewById(R.id.setting_backup);
    }
}

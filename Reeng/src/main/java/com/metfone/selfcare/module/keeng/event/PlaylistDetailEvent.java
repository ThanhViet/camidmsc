package com.metfone.selfcare.module.keeng.event;


import com.metfone.selfcare.module.keeng.model.AllModel;

public class PlaylistDetailEvent {

    AllModel media;
    boolean isRemoveSongInPlaylist;

    public AllModel getMedia() {
        return media;
    }

    public void setMedia(AllModel media) {
        this.media = media;
    }

    public boolean isRemoveSongInPlaylist() {
        return isRemoveSongInPlaylist;
    }

    public void setRemoveSongInPlaylist(boolean removeSongInPlaylist) {
        isRemoveSongInPlaylist = removeSongInPlaylist;
    }
}

package com.metfone.selfcare.module.metfoneplus.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.module.metfoneplus.dialog.MPCombinePhoneDialog;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddPhoneFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPVerifyPhoneFragment;
import com.metfone.selfcare.util.Utilities;

import java.util.List;

public class MPAddPhoneActivity extends BaseSlidingFragmentActivity
        implements MPAddPhoneFragment.OnFragmentInteractionListener,
        MPCombinePhoneDialog.OnCombinePhoneListener,
        ClickListener.IconListener {
    private static final String TAG = "MfAddNumberActivity";

    private MPAddPhoneFragment mMPAddPhoneFragment;
    private MPVerifyPhoneFragment mMPVerifyPhoneFragment;
    private static final String TAG_MP_ADD_PHONE_NUMBER = "MF_ADD_PHONE";
    private static final String TAG_MP_VERIFY = "MF_VERIFY";
    private CamIdUserBusiness mCamIdUserBusiness;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp_add_phone);
        mCamIdUserBusiness = ((ApplicationController) getApplicationContext()).getCamIdUserBusiness();
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);

        displayAddPhoneFragment();
    }

    private void displayAddPhoneFragment() {
        mMPAddPhoneFragment = MPAddPhoneFragment.newInstance();
        executeFragmentTransactionAllowLoss(mMPAddPhoneFragment, R.id.mf_add_phone_container, false, false, TAG_MP_ADD_PHONE_NUMBER);
    }

    @Override
    public void skipToHome() {
        mCamIdUserBusiness.setProcessingLoginSignUpMetfone(false);
        ApplicationController.self().getCamIdUserBusiness().setAddOrSelectPhoneCalled(false);
        finish();
    }

    @Override
    public void goToVerifyScreen(String phoneNumber, String codeCheckPhone) {
        mMPVerifyPhoneFragment = MPVerifyPhoneFragment.newInstance(phoneNumber, codeCheckPhone);
        executeFragmentTransactionAllowLossAnimation(mMPVerifyPhoneFragment, R.id.mf_add_phone_container, false, TAG_MP_VERIFY);
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        super.onIconClickListener(view, entry, menuId);
    }

    @Override
    public void onUpdateMetfoneSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onBack() {
        displayAddPhoneFragment();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ApplicationController.self().getCamIdUserBusiness().setAddOrSelectPhoneCalled(false);
    }

    @Override
    public void onBackPressed() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        int fragSize = fragments.size();
        Fragment f = fragments.get(fragSize - 1);
        if (MPVerifyPhoneFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            ((MPVerifyPhoneFragment) f).onBack();
        } else {
            super.onBackPressed();
        }
        ApplicationController.self().getCamIdUserBusiness().setAddOrSelectPhoneCalled(false);
    }
}
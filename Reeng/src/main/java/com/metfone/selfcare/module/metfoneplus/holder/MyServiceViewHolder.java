package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

/**
 * ViewHolder for History charge Exchange, Basic, Promotion: My services
 * layout: item_history_charge_3
 */
public class MyServiceViewHolder extends RecyclerView.ViewHolder {
    public AppCompatImageView mIcon;
    public AppCompatTextView mTitle;

    public MyServiceViewHolder(View view) {
        super(view);
        mIcon = view.findViewById(R.id.icon_history_charge_3);
        mTitle = view.findViewById(R.id.title_history_charge_3);
    }
}
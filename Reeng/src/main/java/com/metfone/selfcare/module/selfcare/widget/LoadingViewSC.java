package com.metfone.selfcare.module.selfcare.widget;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;

public class LoadingViewSC extends LinearLayout {
    private TextView btnRetry;
    private View emptyView;
    private ImageView icon;
    private TextView loadingError;
    private TextView loadingErrorSubtext;
    private TextView loaddingEmpty;
    private View loadingErrorView;
    private View progressBarLayout;
    private LinearLayout layoutMain;

    public LoadingViewSC(Context paramContext) {
        super(paramContext);
        initView(paramContext);
    }

    public LoadingViewSC(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        initView(paramContext);
    }

    public LoadingViewSC(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
        initView(paramContext);
    }

    private void initView(Context paramContext) {
        if (!isInEditMode()) {
            ((LayoutInflater) paramContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_loading_sc, this, true);
            this.progressBarLayout = getRootView().findViewById(R.id.progress_bar_layout);
            this.loadingError = ((TextView) getRootView().findViewById(R.id.loadingerror));
            this.loadingErrorSubtext = ((TextView) getRootView().findViewById(R.id.loadingerror_subtext));
            this.loaddingEmpty = ((TextView) getRootView().findViewById(R.id.loading_empty));
            this.emptyView = getRootView().findViewById(R.id.empty_layout);
            this.btnRetry = ((TextView) getRootView().findViewById(R.id.btn_retry));
            this.loadingErrorView = getRootView().findViewById(R.id.loading_error_layout);
            this.layoutMain = (LinearLayout) getRootView().findViewById(R.id.layout_main);
            this.icon = ((ImageView) getRootView().findViewById(R.id.icon));
        }
    }

    public void loadBegin() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(VISIBLE);
        this.loadingError.setText(getContext().getResources().getString(R.string.e601_error_but_undefined));
        this.btnRetry.setText(getContext().getResources().getString(R.string.retry));
        this.loadingError.setVisibility(GONE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(GONE);
        this.emptyView.setVisibility(GONE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(GONE);
    }

    public void loadEmpty() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_logo_mytel);
        this.loadingError.setText(Html.fromHtml(getResources().getString(R.string.pull_to_load_more_no_result)));
    }

    public void loadEmptySearch() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_logo_mytel);
        this.loadingError.setText(Html.fromHtml(getResources().getString(R.string.pull_to_load_more_no_result_search)));
    }

    public void loadEmpty(String str) {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_logo_mytel);
        if (!TextUtils.isEmpty(str))
            this.loadingError.setText(Html.fromHtml(str));
    }

    public void loadError() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_logo_mytel);
    }

    public void loadError(String paramString) {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_logo_mytel);
        if (!TextUtils.isEmpty(paramString))
            this.loadingError.setText(Html.fromHtml(paramString));
    }

    public void loadFinish() {
        setVisibility(GONE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(GONE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(GONE);
        this.btnRetry.setVisibility(GONE);
    }

    @SuppressWarnings("deprecation")
    public void notice(String paramString) {
        setVisibility(VISIBLE);
        layoutMain.setBackgroundColor(getResources().getColor(R.color.transparent));
        emptyView.setBackgroundColor(getResources().getColor(R.color.transparent));
        loaddingEmpty.setBackgroundColor(getResources().getColor(R.color.transparent));
        this.progressBarLayout.setVisibility(GONE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(GONE);
        this.emptyView.setVisibility(View.VISIBLE);
        this.loadingError.setVisibility(GONE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(GONE);
        this.loadingError.setVisibility(View.GONE);
        this.loaddingEmpty.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(paramString)) {
            this.loaddingEmpty.setText(paramString);
        }

    }

    @SuppressWarnings("deprecation")
    public void loadLogin(String str) {
        setVisibility(VISIBLE);
        layoutMain.setBackgroundColor(getResources().getColor(R.color.white));
        this.progressBarLayout.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_logo_mytel);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(VISIBLE);
        if (!TextUtils.isEmpty(str))
            this.loadingError.setText(str);
        this.btnRetry.setText(Html.fromHtml(getResources().getString(R.string.sign_in)));
    }

    public void loadVerify(String str, String button) {
        setVisibility(VISIBLE);
        layoutMain.setBackgroundColor(getResources().getColor(R.color.white));
        this.progressBarLayout.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_logo_mytel);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(VISIBLE);
        if (!TextUtils.isEmpty(str))
            this.loadingError.setText(str);
        this.btnRetry.setText(Html.fromHtml(button));
    }

    public void showRetry() {
        setVisibility(VISIBLE);
        layoutMain.setBackgroundColor(getResources().getColor(R.color.white));
        this.progressBarLayout.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_logo_mytel);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(VISIBLE);
        this.loadingError.setText(getContext().getResources().getString(R.string.e601_error_but_undefined));
        this.btnRetry.setText(getContext().getResources().getString(R.string.retry));
    }

    public void setLoadingErrorListener(OnClickListener paramOnClickListener) {
        if (paramOnClickListener != null)
            this.loadingError.setOnClickListener(paramOnClickListener);
    }

    public void setBtnRetryListener(OnClickListener paramOnClickListener) {
        if (paramOnClickListener != null)
            this.btnRetry.setOnClickListener(paramOnClickListener);
    }
}
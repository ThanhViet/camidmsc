package com.metfone.selfcare.module.metfoneplus.search.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.KeyboardUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.fragment.HomePagerFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogOtpFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.NumberPickerBottomSheetDialogFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPApplyingSimFragment;
import com.metfone.selfcare.module.metfoneplus.search.dialog.SelectPrefixNumberDialog;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.WsSearchNumberToBuyRequest;
import com.metfone.selfcare.network.metfoneplus.response.WsGetListPrefixResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSearchNumberToBuyResponse;
import com.metfone.selfcare.ui.RangeSeekBar;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class BuyPhoneNumberFragment extends MPBaseFragment implements TextWatcher, MPApplyingSimFragment.ButtonOnClickListener {

    public static int PAGE_SIZE = 30;

    @BindView(R.id.btn_search)
    RoundTextView btn_search;
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.filter_stranger_age_seek_bar)
    RangeSeekBar<Integer> priceRangeSeekBar;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.price_min)
    TextView price_min;
    @BindView(R.id.price_max)
    TextView price_max;
    @BindView(R.id.edit_number_1)
    EditText edit_number_1;
    @BindView(R.id.edit_number_2)
    EditText edit_number_2;
    @BindView(R.id.tv_filter)
    TextView tv_prefix;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private boolean mIsBottomSheetShowing = false;
    private String prefixSelected = "";
    private int positionSelected = 0;
    private ArrayList<String> prefixList = new ArrayList<>();
    private ArrayList<String> prefixListDisplay = new ArrayList<>();
    private int commitmentDuration = 0;
    private int fee = 0;
    private static final String SUCCESS = "success";
    private Boolean checkSuccess = false;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_search_buy_phone_number;
    }

    public static BuyPhoneNumberFragment newInstance(Boolean success) {
        BuyPhoneNumberFragment fragment = new BuyPhoneNumberFragment();
        Bundle args = new Bundle();
        args.putBoolean(SUCCESS, success);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(view);
        initViews();
        if (checkSuccess) {
            onShowSuccess();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            checkSuccess = (Boolean) getArguments().getBoolean(SUCCESS);
        }
        loadPrefixNumber();
        updateBottomMenuColor(R.color.m_home_tab_background);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        edit_number_1.requestFocus();
        edit_number_1.setText("");
        edit_number_2.setText("");
        if (checkSuccess) {
            checkSuccess = false;
        }
        priceRangeSeekBar.setRangeValues(0, 1000, 10);
        priceRangeSeekBar.setSelectedMinValue(0);
        priceRangeSeekBar.setSelectedMaxValue(1000);
        price_min.setText("$0 ");
        price_max.setText(" $1000");
//        prefixSelected = "All";
//        tv_prefix.setText("All");

    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }else{
            mParentActivity.finish();
            return;
        }
        gotoMPRootFragment();
    }

    @OnClick({R.id.btn_search, R.id.btn_order, R.id.layout_filter})
    public void onClick(View view) {
        if (view.getId() == R.id.btn_search) {
//            if (edit_number_1.getText().length() > 0 || edit_number_2.getText().length() > 0) {
            KeyboardUtils.hideSoftInput(getActivity());
            openResultSearch();
            //}
        } else if (view.getId() == R.id.btn_order) {
            gotoMPHistoryNewSimFragment();
        } else if (view.getId() == R.id.layout_filter) {
            showDialogPrefix();
        }
    }

    private void initViews() {
        action_bar_title.setText(R.string.buy_phone_number);
        priceRangeSeekBar.setRangeValues(0, 1000, 10);
        priceRangeSeekBar.setSelectedMinValue(0);
        priceRangeSeekBar.setSelectedMaxValue(1000);
        price_min.setText("$0 ");
        price_max.setText(" $1000");
        priceRangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<Integer> bar, Integer minValue, Integer maxValue) {
                price_min.setText("$" + bar.getSelectedMinValue() + " ");
                price_max.setText(" $" + bar.getSelectedMaxValue());
            }
        });
        priceRangeSeekBar.setNotifyWhileDragging(true);
        edit_number_1.addTextChangedListener(this);
        edit_number_2.addTextChangedListener(this);
        if (prefixListDisplay.size() > positionSelected) {
            tv_prefix.setText(prefixListDisplay.get(positionSelected));
        }
    }

    private void updateStateButtonSearch() {
        if (edit_number_1.getText().length() == 3) {
            edit_number_2.requestFocus();
        }
//        if (edit_number_1.getText().length() > 0 || edit_number_2.getText().length() > 0) {
//            btn_search.setEnabled(true);
//            btn_search.setBackgroundColorRound(Color.parseColor("#D80F2C"));
//        } else {
//            btn_search.setEnabled(false);
//            btn_search.setBackgroundColorRound(Color.parseColor("#3C3E3F"));
//        }
    }

    private void loadPrefixNumber() {
//        if (pbLoading != null) {
//            pbLoading.setVisibility(View.VISIBLE);
//        }
        MetfonePlusClient.getInstance().getListPrefixNumber(new ApiCallback<WsGetListPrefixResponse>() {
            @Override
            public void onResponse(Response<WsGetListPrefixResponse> response) {
                if (pbLoading != null) {
                    pbLoading.setVisibility(View.GONE);
                }
                if (response != null
                        && response.body() != null
                        && response.body().getResult() != null
                        && response.body().getResult().getWsResponse() != null
                        && response.body().getResult().getWsResponse().getPrefix() != null
                        && response.body().getResult().getWsResponse().getPrefix() != null) {
                    String[] result = response.body().getResult().getWsResponse().getPrefix().split(",");
                    prefixList.add(mParentActivity.getString(R.string.all));
                    prefixListDisplay.add(mParentActivity.getString(R.string.all));
                    if (result != null) {
                        for (String str : result) {
                            prefixList.add(str);
                            prefixListDisplay.add("0" + str);
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                if (pbLoading != null) {
                    pbLoading.setVisibility(View.GONE);
                }
                ToastUtils.showToast(getContext(), getString(R.string.message_error_connect_network));
            }
        });
    }

    private void showDialogPrefix() {
        if (!isAdded() || prefixListDisplay == null || prefixListDisplay.size() == 0 || mIsBottomSheetShowing) {
            return;
        }
        SelectPrefixNumberDialog selectPrefixNumberDialog = SelectPrefixNumberDialog.newInstance(getString(R.string.please_choose_a_prefix), (String[]) prefixListDisplay.toArray(new String[prefixListDisplay.size()]), 0);
        selectPrefixNumberDialog.setOnNumberPickerBottomSheetOnClick(new SelectPrefixNumberDialog.OnNumberPickerBottomSheetOnClick() {
            @Override
            public void onDone(String valueSelected, int position) {
                prefixSelected = valueSelected;
                tv_prefix.setText(valueSelected);
                positionSelected = position;
            }

            @Override
            public void onDismiss() {
                mIsBottomSheetShowing = false;
            }
        });
        mIsBottomSheetShowing = true;
        selectPrefixNumberDialog.show(mParentActivity.getSupportFragmentManager(), SelectPrefixNumberDialog.TAG);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        updateStateButtonSearch();
    }

    private void openResultSearch() {
        WsSearchNumberToBuyRequest request = MetfonePlusClient.createBaseRequest(new WsSearchNumberToBuyRequest(), Constants.WSCODE.WS_SEARCH_NUMBER_TO_BUY);
        WsSearchNumberToBuyRequest.Request subRequest = request.new Request();
        subRequest.setPageNo(0);
        subRequest.setPageSize(PAGE_SIZE);
        subRequest.setFromPrice(priceRangeSeekBar.getSelectedMinValue());
        subRequest.setToPrice(priceRangeSeekBar.getSelectedMaxValue());
        subRequest.setLanguage(LocaleManager.getLanguage(getActivity()));
        if (positionSelected == 0) {
            subRequest.setPrefix("");
        } else {
            subRequest.setPrefix(prefixList.get(positionSelected));
        }

        subRequest.setTypeNumber(edit_number_1.getText().toString() + edit_number_2.getText().toString());
        request.setWsRequest(subRequest);
        searchNumber(request); // search truoc roi moi chuyen man hinh
    }

    private void searchNumber(WsSearchNumberToBuyRequest request) {
        if (pbLoading != null) {
            pbLoading.setVisibility(View.VISIBLE);
        }
        MetfonePlusClient.getInstance().searchNumberToBuy(request, new ApiCallback<WsSearchNumberToBuyResponse>() {
            @Override
            public void onResponse(Response<WsSearchNumberToBuyResponse> response) {
                if (pbLoading != null) {
                    pbLoading.setVisibility(View.GONE);
                }
                if (response != null
                        && response.body() != null
                        && response.body().getResult() != null) {
                    if (response.body().getResult().getErrorCode().equals("0")) {
                        List<AvailableNumber> result = response.body().getResult().getWsResponse().getLstNumberToBuy();
                        commitmentDuration = (int) Float.parseFloat(response.body().getResult().getWsResponse().getCommitDuration());
                        fee = (int) Float.parseFloat(response.body().getResult().getWsResponse().getFee());
                        gotoSearchNumberToBuyFragment((ArrayList<AvailableNumber>) result, request, commitmentDuration, fee);
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getResult().getMessage());
                    }
                } else if (response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() == null) {
                    ToastUtils.showToast(getContext(), response.body().getResult().getMessage());
                }
            }

            @Override
            public void onError(Throwable error) {
                if (pbLoading != null) {
                    pbLoading.setVisibility(View.GONE);
                }
            }
        });
    }

    public void onShowSuccess() {
        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.img_dialog_1, getString(R.string.successfully), getString(R.string.buy_sim_success_1), false);
        baseMPSuccessDialog.show();
    }

}

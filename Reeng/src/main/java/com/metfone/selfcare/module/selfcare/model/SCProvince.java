package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCProvince implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    public SCProvince(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SCProvince{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

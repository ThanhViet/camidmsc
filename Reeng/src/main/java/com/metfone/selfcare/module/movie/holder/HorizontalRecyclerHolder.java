/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.holder;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.movie.adapter.CategoryDetailAdapter;
import com.metfone.selfcare.module.movie.listener.LoadmoreListener;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;

import java.util.ArrayList;

import butterknife.BindView;

public class HorizontalRecyclerHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;

    private ArrayList<Object> list;
    private CategoryDetailAdapter adapter;
    private MoviePagerModel data;
    private String mBlockName;

    public HorizontalRecyclerHolder(View view, Activity activity, final TabMovieListener.OnAdapterClick listener,
                                    int viewType, LoadmoreListener.IStartLoadmoreListener startLoadmoreListener) {
        super(view);
        list = new ArrayList<>();
        adapter = new CategoryDetailAdapter(activity, viewType);
        adapter.setWidthItem(TabHomeUtils.getWidthPosterMovie());
        adapter.setListener(listener);
        adapter.setItems(list);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition() == list.size() - 1 &&
                        !mBlockName.equals(activity.getString(R.string.cinema_continue_watching)) &&
                        !mBlockName.equals(activity.getString(R.string.cinema_movie_for_you))) {
                    startLoadmoreListener.startLoadmore();
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_DRAGGING){
                    listener.onScrollData();
                }
            }
        });
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, R.drawable.divider_movie);
    }

    @Override
    public void bindData(Object item, int position) {
        if (list == null) list = new ArrayList<>();
        else list.clear();
        if (item instanceof MoviePagerModel) {
            data = (MoviePagerModel) item;
            list.addAll(data.getList());
            mBlockName = data.getTitle();
        } else {
            data = null;
        }
        if (adapter != null) {
            adapter.setBlockName(mBlockName);
            adapter.notifyDataSetChanged();
        }
    }

}

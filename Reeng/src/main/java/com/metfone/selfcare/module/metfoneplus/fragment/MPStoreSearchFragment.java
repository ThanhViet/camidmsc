package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.camid.ProvinceDistrict;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.module.metfoneplus.adapter.StoreAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPSupportStoreDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.NumberPickerBottomSheetDialogFragment;
import com.metfone.selfcare.module.metfoneplus.holder.StoreViewHolder;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsFindStoreByAddrResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProvinceDistrictResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPStoreSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPStoreSearchFragment extends MPBaseFragment implements View.OnClickListener {
    public static final String TAG = MPStoreSearchFragment.class.getSimpleName();

    @BindView(R.id.store_search_container)
    CoordinatorLayout mStoreSearchContainer;
    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.m_p_store_search_list_store)
    RecyclerView mListProvinceDistrict;
    @BindView(R.id.m_p_store_search_province)
    AppCompatTextView mStoreSearchProvince;
    @BindView(R.id.m_p_store_search_district)
    AppCompatTextView mStoreSearchDistrict;
    @BindView(R.id.m_p_store_search_no_store)
    LinearLayout mLayoutNoStore;

    private StoreAdapter mStoreAdapter;
    private List<ProvinceDistrict> mProvinces;
    private List<ProvinceDistrict> mDistricts;
    private String mProvinceIdSelected = "";
    private String mCurrentProvinceIdSelected = "";
    private String mDistrictIdSelected = "";
    private String mCurrentDistrictIdSelected = "";
    private boolean mIsBottomSheetShowing = false;
    private NumberPickerBottomSheetDialogFragment mProvinceBottomSheet;
    private NumberPickerBottomSheetDialogFragment mDistrictBottomSheet;
    private int mPositionProvinceSelection = 0;
    private int mPositionDistrictSelection = 0;

    private View.OnClickListener mOnItemStoreTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            StoreViewHolder storeViewHolder = (StoreViewHolder) view.getTag();
            MPSupportStoreDialog supportStoreDialog = MPSupportStoreDialog.newInstance(storeViewHolder.mStore);
            supportStoreDialog.show(getParentFragmentManager(), MPSupportStoreDialog.TAG);
        }
    };

    public MPStoreSearchFragment() {
        // Required empty public constructor
    }


    public static MPStoreSearchFragment newInstance() {
        MPStoreSearchFragment fragment = new MPStoreSearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_store_search;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActionBarTitle.setText(getString(R.string.m_p_store_search_title));
        Utilities.adaptViewForInserts(mLayoutActionBar);
        Utilities.adaptViewForInsertBottom(mStoreSearchContainer);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mParentActivity, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mParentActivity, R.drawable.divider_list_store)));

        mStoreAdapter = new StoreAdapter(mParentActivity, new ArrayList<Store>(), mOnItemStoreTapped);

        mListProvinceDistrict.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListProvinceDistrict.setHasFixedSize(true);
        mListProvinceDistrict.addItemDecoration(dividerItemDecoration);
        mListProvinceDistrict.setAdapter(mStoreAdapter);

        mStoreSearchProvince.setOnClickListener(this);
        mStoreSearchDistrict.setOnClickListener(null);
        mStoreSearchDistrict.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getProvinces();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.action_bar_back)
    void onBack() {
        popBackStackFragment();
    }

    /**
     * BottomSheetDialog for change province/district.
     */
    private void showBottomSheetProvince(List<ProvinceDistrict> list, int position) {
        if (!isAdded() || list == null || list.size() == 0 || mIsBottomSheetShowing) {
            return;
        }

        List<String> strings = new ArrayList<>();
        for (ProvinceDistrict p : list) {
            strings.add(p.getName());
        }

        final String[] value = strings.toArray(new String[0]);
        String title = getString(R.string.m_p_store_select_province);
        mProvinceBottomSheet = NumberPickerBottomSheetDialogFragment.newInstance(title, value, position);
        mProvinceBottomSheet.setOnNumberPickerBottomSheetOnClick(new NumberPickerBottomSheetDialogFragment.OnNumberPickerBottomSheetOnClick() {
            @Override
            public void onDone(String valueSelected, int position) {
                mProvinceIdSelected = list.get(position).getId();
                if (!mCurrentProvinceIdSelected.equals(mProvinceIdSelected)) {
                    getDistrict(mProvinceIdSelected);
                    findStoreByAddress(null, Constants.INVALID_LAT, Constants.INVALID_LNG, mProvinceIdSelected);

                    mStoreSearchDistrict.setEnabled(true);
                    mStoreSearchDistrict.setOnClickListener(MPStoreSearchFragment.this);
                    mCurrentProvinceIdSelected = mProvinceIdSelected;

                    mDistrictIdSelected = null;
                    mPositionProvinceSelection = position;
                    mPositionDistrictSelection = 0;
                }
            }

            @Override
            public void onDismiss() {
                mIsBottomSheetShowing = false;
            }
        });
        mIsBottomSheetShowing = true;
        mProvinceBottomSheet.show(mParentActivity.getSupportFragmentManager(), getString(R.string.m_p_store_select_province));
    }

    private void showBottomSheetDistrict(List<ProvinceDistrict> list, int position) {
        if (!isAdded() || list == null || list.size() == 0 || mIsBottomSheetShowing) {
            return;
        }

        List<String> strings = new ArrayList<>();
        for (ProvinceDistrict p : list) {
            strings.add(p.getName());
        }

        final String[] value = strings.toArray(new String[0]);
        String title = getString(R.string.m_p_store_select_district);
        mDistrictBottomSheet = NumberPickerBottomSheetDialogFragment.newInstance(title, value, position);
        mDistrictBottomSheet.setOnNumberPickerBottomSheetOnClick(new NumberPickerBottomSheetDialogFragment.OnNumberPickerBottomSheetOnClick() {
            @Override
            public void onDone(String valueSelected, int position) {
                mDistrictIdSelected = list.get(position).getId();
                if (!mCurrentDistrictIdSelected.equals(mDistrictIdSelected)) {
                    findStoreByAddress(mDistrictIdSelected, Constants.INVALID_LAT, Constants.INVALID_LNG, mProvinceIdSelected);
                    mCurrentDistrictIdSelected = mDistrictIdSelected;
                    mPositionDistrictSelection = position;
                }
            }

            @Override
            public void onDismiss() {
                mIsBottomSheetShowing = false;
            }
        });
        mIsBottomSheetShowing = true;
        mDistrictBottomSheet.show(mParentActivity.getSupportFragmentManager(), getString(R.string.m_p_store_select_district));
    }

    private void getProvinces() {
        Log.e(TAG, "getProvinces: " + mProvinceIdSelected);
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetProvinces(new MPApiCallback<WsGetProvinceDistrictResponse>() {
            @Override
            public void onResponse(Response<WsGetProvinceDistrictResponse> response) {
                if (response.body() != null && response.body().getResult() != null
                        && response.body().getResult().getWsResponse() != null) {
                    mProvinces = response.body().getResult().getWsResponse();
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void getDistrict(String provinceId) {
        Log.e(TAG, "getDistrict: " + provinceId);
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetDistricts(provinceId, new MPApiCallback<WsGetProvinceDistrictResponse>() {
            @Override
            public void onResponse(Response<WsGetProvinceDistrictResponse> response) {
                if (response.body() != null && response.body().getResult().getWsResponse() != null) {
                    mDistricts = response.body().getResult().getWsResponse();
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void findStoreByAddress(String districtId, double latitude, double longitude, String provinceId) {
        Log.e(TAG, "findStoreByAddress: provinceId = " + provinceId + " - districtId = " + districtId);
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsFindStoreByAddr(districtId, latitude, longitude, provinceId, new MPApiCallback<WsFindStoreByAddrResponse>() {
            @Override
            public void onResponse(Response<WsFindStoreByAddrResponse> response) {
                if (response.body() != null) {
                    updateListStore(response.body().getResult().getWsResponse());
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void updateListStore(List<Store> stores) {
        if (stores == null || stores.size() == 0) {
            mStoreAdapter.replaceData(new ArrayList<Store>());
            mLayoutNoStore.setVisibility(View.VISIBLE);
            mListProvinceDistrict.setVisibility(View.GONE);
            getCamIdUserBusiness().setStores(null);
        } else {
            mStoreAdapter.replaceData(stores);
            mLayoutNoStore.setVisibility(View.GONE);
            mListProvinceDistrict.setVisibility(View.VISIBLE);
            getCamIdUserBusiness().setStores(stores);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.m_p_store_search_province:
                if (mProvinces != null) {
                    showBottomSheetProvince(mProvinces, mPositionProvinceSelection);
                }
                break;
            case R.id.m_p_store_search_district:
                if (mDistricts != null) {
                    showBottomSheetDistrict(mDistricts, mPositionDistrictSelection);
                }
                break;
        }
    }
}
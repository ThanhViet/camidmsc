package com.metfone.selfcare.module.selfcare.fragment.loyalty;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.selfcare.event.SCAccountEvent;
import com.metfone.selfcare.module.selfcare.event.SCRechargeEvent;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

public class SCMyShareFragment extends SCBaseFragment implements View.OnClickListener {

    TextView btnSubmit;
    TextView tvBalance;
    TextView tv5000;
    TextView tv10000;
    TextView tv20000;
    TextView tv50000;
    TextView tvDes;
    TextInputLayout tilValue;
    TextInputLayout tilPhone;
    TextInputLayout tilPass;
    private EditText edtValue;
    private EditText edtPhone;
    private EditText edtPass;
    View btnBack;
    TextView btnChangePass;
    private ImageView imvBanner;

    public static SCMyShareFragment newInstance(Bundle args) {

        SCMyShareFragment fragment = new SCMyShareFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCMyShareFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_my_share;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        btnBack = view.findViewById(R.id.btnBack);
        btnChangePass = view.findViewById(R.id.tvChangePass);
        imvBanner = view.findViewById(R.id.imvBanner);
        btnSubmit = view.findViewById(R.id.btnSubmit);
        tvBalance = view.findViewById(R.id.tvBalance);
        tv5000 = view.findViewById(R.id.tv5000);
        tv10000 = view.findViewById(R.id.tv10000);
        tv20000 = view.findViewById(R.id.tv20000);
        tv50000 = view.findViewById(R.id.tv50000);
        tvDes = view.findViewById(R.id.tvDescription);

        btnBack.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        tv5000.setOnClickListener(this);
        tv10000.setOnClickListener(this);
        tv20000.setOnClickListener(this);
        tv50000.setOnClickListener(this);
        btnChangePass.setOnClickListener(this);
        btnChangePass.setPaintFlags(btnChangePass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tilPhone = view.findViewById(R.id.tilPhone);
        tilValue = view.findViewById(R.id.tilValue);
        tilPass = view.findViewById(R.id.tilPass);
        edtValue = tilValue.getEditText();
        edtPhone = tilPhone.getEditText();
        edtPass = tilPass.getEditText();

        loadData();

        edtPhone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (edtPhone.getRight() - edtPhone.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        Intent chooseFriend = new Intent(mActivity, ChooseContactActivity.class);
                        chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_SELFCARE_RECHARGE);
                        mActivity.startActivity(chooseFriend, true);
                        return true;
                    }
                }
                return false;
            }
        });

        return view;
    }

    private void loadData() {
        if (SCUtils.getCurrentAccount() != null && SCUtils.getCurrentAccount().getOcsAccount() != null)
            tvBalance.setText(mActivity.getString(R.string.sc_available_balance, SCUtils.numberFormat(SCUtils.getCurrentAccount().getOcsAccount().getBalance()) + SCConstants.SC_CURRENTCY));

    }

    private void onSubmit() {
        mActivity.hideKeyboard();
        if (TextUtils.isEmpty(edtValue.getText())) {
            ToastUtils.makeText(mActivity, mActivity.getString(R.string.sc_please_select_an_amount));
            return;
        }
        if (TextUtils.isEmpty(edtPhone.getText())) {
            ToastUtils.makeText(mActivity, mActivity.getString(R.string.sc_my_share_phone_hint));
            return;
        }

        if (TextUtils.isEmpty(edtPass.getText())) {
            ToastUtils.makeText(mActivity, mActivity.getString(R.string.sc_ishare_pass_validate));
            return;
        }

        final String amount = edtValue.getText().toString();
        final String receiveIsdn = edtPhone.getText().toString();
        final String pass = edtPass.getText().toString();

        WSSCRestful restful = new WSSCRestful(mActivity);
        restful.transferIshare(receiveIsdn, amount, pass, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.optInt("errorCode", -1);
                    String message = jsonObject.optString("message");
                    mActivity.showToast(message);
                    if (code == 200)
                        EventBus.getDefault().postSticky(new SCAccountEvent(SCAccountEvent.UPDATE_INFO));
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }

    private void doChangePassword() {
        WSSCRestful restful = new WSSCRestful(mActivity);
        restful.resetPasswordIShare(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.optInt("errorCode", -1);
                    String message = jsonObject.optString("message");
                    mActivity.showToast(message);
                    if (code == 200)
                        EventBus.getDefault().postSticky(new SCAccountEvent(SCAccountEvent.UPDATE_INFO));
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv5000:
                edtValue.setText("5000");
                break;
            case R.id.tv10000:
                edtValue.setText("10000");
                break;
            case R.id.tv20000:
                edtValue.setText("20000");
                break;
            case R.id.tv50000:
                edtValue.setText("50000");
                break;
            case R.id.btnBack:
                if (mActivity != null)
                    mActivity.onBackPressed();
                break;
            case R.id.btnSubmit:
                onSubmit();
                break;
            case R.id.tvChangePass:
                doChangePassword();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(final SCRechargeEvent event) {
        if (event != null) {
            if (!TextUtils.isEmpty(event.getPhoneNumber()))
                edtPhone.setText(event.getPhoneNumber());

            EventBus.getDefault().removeStickyEvent(event);
        }
    }
}

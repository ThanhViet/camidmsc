/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2018/12/19
 *
 */

package com.metfone.selfcare.module.keeng.adapter.detail;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.util.Utilities;

import java.util.List;


public class AlbumDetailAdapter extends BaseAdapter<BaseViewHolder> {
    private BaseListener.AlbumDetailListener mListener;
    private List<AllModel> itemsList;

    public AlbumDetailAdapter(Context context, List<AllModel> itemsList) {
        super(context);
        this.itemsList = itemsList;
    }

    public AllModel getItem(int position) {
        return (position >= 0 && null != itemsList && itemsList.size() > position ? itemsList.get(position) : null);
    }

    public List<AllModel> getItemsList() {
        return itemsList;
    }

    public void setListener(BaseListener.AlbumDetailListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public int getItemViewType(int position) {
        AllModel item = getItem(position);
        if (item != null) {
            switch (item.getType()) {
                case Constants.TYPE_SONG:
                    return BaseAdapterRecyclerView.ITEM_MEDIA_SONG;
                case Constants.TYPE_VIDEO:
                    return BaseAdapterRecyclerView.ITEM_MEDIA_VIDEO;
                default:
                    break;
            }
        }
        return BaseAdapterRecyclerView.ITEM_EMPTY;
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        int resLayout;
        switch (viewType) {
            case BaseAdapterRecyclerView.ITEM_MEDIA_SONG:
                resLayout = R.layout.holder_detail_album_type_song;
                break;
            case BaseAdapterRecyclerView.ITEM_MEDIA_VIDEO:
                resLayout = R.layout.holder_detail_album_type_video;
                break;
            default:
                resLayout = R.layout.holder_empty;
                break;
        }
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(resLayout, null);
        return new BaseViewHolder(view);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final AllModel item = getItem(position);
        if (item != null) {
            switch (item.getType()) {
                case Constants.TYPE_SONG: {
                    holder.setText(R.id.tv_title, item.getName());
                    String str = item.getSinger();
                    if (TextUtils.isEmpty(str)) {
                        holder.setVisible(R.id.tv_description, false);
                    } else {
                        if (holder.getView(R.id.tv_description) instanceof TextView) {
                            TextView tvSinger = holder.getView(R.id.tv_description);
                            tvSinger.setVisibility(View.VISIBLE);
                            tvSinger.setText(str);
                            if (item.isDocQuyen()) {
                                tvSinger.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources()
                                        .getDrawable(R.drawable.ic_docquyen), null, null, null);
                            } else {
                                tvSinger.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
                                        null);
                            }
                        }
                    }
                    if (item.getListened() < 1) {
                        holder.setVisible(R.id.tv_listen_no, false);
                    } else if (item.getListened() == 1) {
                        holder.setVisible(R.id.tv_listen_no, true);
                        holder.setText(R.id.tv_listen_no, mContext.getResources().getString(R.string.m_listen_no, "1"));
                    } else {
                        holder.setVisible(R.id.tv_listen_no, true);
                        holder.setText(R.id.tv_listen_no, mContext.getResources().getString(R.string.m_listens_no, item.getListenNo()));
                    }
                    if (holder.getView(R.id.iv_cover) instanceof ImageView)
                        ImageBusiness.setSong(item.getImage(), holder.getView(R.id.iv_cover), position,
                                Utilities.dpToPx(19));

                    holder.setOnClickListener(R.id.button_option, view -> {
                        if (mListener != null) {
                            mListener.onClickOptionMedia(view, item);
                        }
                    });
                }
                break;

                case Constants.TYPE_VIDEO: {
                    holder.setText(R.id.tv_title, item.getName());
                    String str = item.getSinger();
                    if (TextUtils.isEmpty(str)) {
                        holder.setVisible(R.id.tv_description, false);
                    } else {
                        if (holder.getView(R.id.tv_description) instanceof TextView) {
                            TextView tvSinger = holder.getView(R.id.tv_description);
                            tvSinger.setVisibility(View.VISIBLE);
                            tvSinger.setText(str);
                            if (item.isDocQuyen()) {
                                tvSinger.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources()
                                        .getDrawable(R.drawable.ic_docquyen), null, null, null);
                            } else {
                                tvSinger.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
                                        null);
                            }
                        }
                    }
//                    if (item.getListened() < 1) {
//                        holder.setVisible(R.id.tv_listen_no, false);
//                    } else if (item.getListened() == 1) {
//                        holder.setVisible(R.id.tv_listen_no, true);
//                        holder.setText(R.id.tv_listen_no, mContext.getResources().getString(R.string.m_view_no, "1"));
//                    } else {
//                        holder.setVisible(R.id.tv_listen_no, true);
//                        holder.setText(R.id.tv_listen_no, mContext.getResources().getString(R.string.m_views_no, item.getListenNo()));
//                    }
                    if (holder.getView(R.id.iv_cover) instanceof ImageView)
                        ImageBusiness.setVideo(item.getImage(), holder.getView(R.id
                                .iv_cover), position);
                }
                break;
            }

            holder.convertView.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onClickMedia(view, position);
                }
            });
        }
    }
}
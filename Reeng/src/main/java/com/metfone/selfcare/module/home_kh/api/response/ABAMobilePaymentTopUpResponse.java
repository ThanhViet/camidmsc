package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;

public class ABAMobilePaymentTopUpResponse {
    public String errorCode;
    public String errorMessage;
    public Response result;
    public static class Response{
        public String responseCode;
        public String responseMessage;
        public String qrString;
        @SerializedName("abapay_deeplink")
        public String abapayDeeplink;
        @SerializedName("app_store")
        public String appStore;
        @SerializedName("play_store")
        public String playStore;
        public String txnid;

        public boolean isSuccess() {
            return "00".equals(responseCode);
        }
    }
}

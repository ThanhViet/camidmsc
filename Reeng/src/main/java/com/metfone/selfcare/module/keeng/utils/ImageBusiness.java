package com.metfone.selfcare.module.keeng.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Random;

import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

public class ImageBusiness {
    /*
    TODO thông tin chi tiết ảnh các màn hình
    1	Flash hot	-	640x360
    2	MV trang chủ	-	390x220
    3	MV trang danh sách	-	390x220
    4	MV trang chi tiết	-	640x360
    5	Album trang chủ	-	310x310
    6	Album trang danh sách	-	310x310
    7	Top hit trang chủ	-	640x360
    8	Top hit trang danh sách	-	640x360
    9	Top hit trang chi tiết	-	640x360
    10	Bài hát trang chủ	-	103x103
    11	Bài hát trang player	-	310x310
    12	Chủ đề trang chủ	-	390x220
    13	Chủ đề trang danh sách	-	640x360
    14	Chủ đề trang chi tiết	-	640x360
    15	Ca sĩ trang chủ	-	310x310
    16	Ca sĩ trang chi tiết (ảnh ngang)	-	640x360

    image310: ảnh lớn
    image: ảnh nhỎ

    Song:
    image310 <=> image 310
    image <=> image 103
    list_image [
        //ảnh 310 ca sĩ
    ]

    MV:
    image310 <=> image 640x360
    image <=> image 390x220

    Album:
    image310 <=> image gốc
    image <=> image 300x300

    Flash_hot:
    image310 <=> image 640x360
    image <=> image 640x360

    Tophit:
    cover <=> image 640x360

    Topic:
    cover <=> image 640x360

    Category:
    cover <=> image 640x360

    Singer:
    avatar <=> image 310
    cover <=> image 640x360
    **/

    protected static final String TAG = "ImageBusiness";
    //protected static int SIZE_AVATAR = com.metfone.selfcare.util.Utilities.dpToPx(120);

    public static void setResourceTransform(ImageView image, int resource) {
        if (image == null)
            return;
        setResourceTransform(image, resource, ApplicationController.self().getRoundedCornersTransformation());
    }

    protected static void setResourceTransform(ImageView image, int resource, Transformation transformation) {
        if (image == null)
            return;
        try {
            int width = image.getWidth();
            int height = image.getHeight();
            Context context = image.getContext();
            if (width > 0 && height > 0) {
                RequestOptions requestOptions = RequestOptions.bitmapTransform(transformation)
                        .override(width, height)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .clone();
                Glide.with(context)
                        .load(resource)
                        .apply(requestOptions)
                        .into(image);
            } else {
                RequestOptions requestOptions = RequestOptions.bitmapTransform(transformation)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .clone();
                Glide.with(context)
                        .load(resource)
                        .apply(requestOptions)
                        .into(image);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setImage(ImageView image, String url, int placeholder, int error, Transformation transformation) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            if (transformation == null) {
                if (width > 0 && height > 0) {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(placeholder)
                            .error(error)
                            .override(width, height)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
//                            .transition(DrawableTransitionOptions.withCrossFade(500))
                            .into(image);
                } else {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(placeholder)
                            .error(error)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
//                            .transition(DrawableTransitionOptions.withCrossFade(500))
                            .into(image);
                }
            } else {
                if (width > 0 && height > 0) {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(placeholder)
                            .override(width, height)
                            .error(error)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .centerCrop()
                            .dontAnimate()
                            .transform(transformation);
                    DrawableTransitionOptions transitionOptions = new DrawableTransitionOptions();
                    transitionOptions.crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)).withCrossFade(500);
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
//                            .transition(transitionOptions)
                            .into(image);
                } else {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(placeholder)
                            .error(error)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .centerCrop()
                            .dontAnimate()
                            .transform(transformation);
                    DrawableTransitionOptions transitionOptions = new DrawableTransitionOptions();
                    transitionOptions.crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)).withCrossFade(500);
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .transition(transitionOptions)
                            .into(image);
                }
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @SuppressLint("CheckResult")
    private static void setCoverItem(ImageView image, String url, int placeholder, int error) {
        if (image == null) return;
        if (TextUtils.isEmpty(url)) {
            if (placeholder > 0) setResource(image, placeholder);
            else if (error > 0) setResource(image, error);
        } else {
            RequestOptions requestOptions = new RequestOptions();
            if (placeholder > 0) requestOptions.placeholder(placeholder);
            if (error > 0) requestOptions.error(error);
            try {
                int width = image.getWidth();
                int height = image.getHeight();
                //todo tam thoi resize theo placeholder de trang cover co resolution > 720
                if (placeholder == R.drawable.df_image_home_21_9) {
                    requestOptions.override(640, 274);
                } else if (placeholder == R.drawable.df_image_home_16_9) {
                    requestOptions.override(640, 360);
                } else if (placeholder == R.drawable.df_image_home) {
                    requestOptions.override(300, 300);
                } else if (width > 0 && height > 0) {
                    requestOptions.override(width, height);
                }
                requestOptions.priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .dontAnimate()
                        .dontTransform()
                        .centerCrop()
                        .clone();
                Glide.with(image.getContext())
                        .asBitmap()
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    protected static void setImageTransform(ImageView image, String url, int placeholder, int error) {
        if (image == null)
            return;
        setImage(image, url, placeholder, error, ApplicationController.self().getRoundedCornersTransformation());
    }

    protected static void setImageTransform(ImageView image, String url, int placeholder, int error, Transformation transformation) {
        if (image == null)
            return;
        setImage(image, url, placeholder, error, transformation);
    }

    private static int getResIdCover() {
        Random ran = new Random();
        int index = ran.nextInt(50);
        return getResIdCover(index);
    }

    private static int getResIdFlashHot(int position) {
        return R.drawable.df_slide;
    }

    private static int getResIdCover(int position) {
        return R.drawable.df_profile;
    }

    private static int getResIdImage() {
        Random ran = new Random();
        int index = ran.nextInt(10);
        return getResIdImage(index);
    }

    private static int getResIdAlbum() {
        Random ran = new Random();
        int index = ran.nextInt(10);
        return getResIdAlbum(index);
    }

    private static int getResIdVideo() {
        Random ran = new Random();
        int index = ran.nextInt(10);
        return getResIdVideo(index);
    }

    private static int getResIdNew() {
        Random ran = new Random();
        int index = ran.nextInt(10);
        return getResIdNew(index);
    }

    private static int getResIdImage(long position) {
        return R.drawable.df_album;
    }

    private static int getResIdAlbum(long position) {
        return R.drawable.df_album_r;
    }

    private static int getResIdVideo(long position) {
        return R.drawable.df_video;
    }

    private static int getResIdNew(long position) {
        return R.drawable.df_image_home_16_9;
    }

    private static int getResIdVideoCurved(long position) {
        return R.drawable.df_video_r;
    }

    public static void setAvatarProfile(String url, final ImageView image) {
        if (image == null)
            return;
        if (TextUtils.isEmpty(url)) {
            setResource(image, R.drawable.df_avatar_profile);
        } else {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            if (width > 0 && height > 0) {
                RequestOptions requestOptions = new RequestOptions()
                        .placeholder(R.drawable.df_avatar_profile)
                        .error(R.drawable.df_avatar_profile)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .override(width, height)
                        .dontAnimate()
                        .dontTransform()
                        .fitCenter()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            } else {
                int size = Utilities.dpToPx(context, 48);
                RequestOptions requestOptions = new RequestOptions()
                        .placeholder(R.drawable.df_avatar_profile)
                        .error(R.drawable.df_avatar_profile)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .override(size, size)
                        .dontAnimate()
                        .dontTransform()
                        .fitCenter()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            }
        }
    }

//    public static void setImagePlayerBlur(final ImageView image, String url, final int radius) {
//        if (image == null)
//            return;
//        try {
//            Context context = image.getContext();
//            int width = image.getWidth();
//            int height = image.getHeight();
//            if (width > 0 && height > 0) {
//                RequestOptions requestOptions = new RequestOptions()
//                        .override(width, height)
//                        .placeholder(R.drawable.ic_call_blur)
//                        .error(R.drawable.ic_call_blur)
//                        .priority(Priority.HIGH)
//                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                        .transform(new BlurTransformation(radius));
//                Glide.with()(context)
//                        .asBitmap()
//                        .load(url)
//                        .apply(requestOptions)
//                        .transition(withCrossFade())
//                        .into(image);
//            } else {
//                RequestOptions requestOptions = new RequestOptions()
//                        .priority(Priority.HIGH)
//                        .placeholder(R.drawable.ic_call_blur)
//                        .error(R.drawable.ic_call_blur)
//                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                        .transform(new BlurTransformation(radius));
//
//                Glide.with()(context)
//                        .asBitmap()
//                        .load(url)
//                        .apply(requestOptions)
//                        .transition(withCrossFade())
//                        .into(image);
//
//            }
//        } catch (OutOfMemoryError e) {
//            Log.e(TAG, e);
//        } catch (IllegalArgumentException e) {
//            Log.e(TAG, e);
//        } catch (Exception e) {
//            Log.e(TAG, e);
//        }
//    }

    public static void setImageBlur(final ImageView image, String url) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            RequestOptions requestOptions;
            if (width > 0 && height > 0) {
                requestOptions = new RequestOptions()
                        .override(width, height)
                        .placeholder(R.drawable.ic_call_blur)
                        .error(R.drawable.ic_call_blur)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .transform(new BlurTransformation(150));
            } else {
                requestOptions = new RequestOptions()
                        .priority(Priority.HIGH)
                        .placeholder(R.drawable.ic_call_blur)
                        .error(R.drawable.ic_call_blur)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .transform(new BlurTransformation(150));
            }
            Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .apply(requestOptions)
                    .transition(withCrossFade())
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setNotification(Context context, String url, int error, Target target) {
        if (context == null || target == null)
            return;
        try {
            if (TextUtils.isEmpty(url)) {
                RequestOptions requestOptions = new RequestOptions()
                        .priority(Priority.IMMEDIATE)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                Glide.with(context)
                        .asBitmap()
                        .load(error)
                        .apply(requestOptions)
                        .into(target);
            } else {
                RequestOptions requestOptions = new RequestOptions()
                        .priority(Priority.IMMEDIATE)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .placeholder(error)
                        .fitCenter()
                        .clone()
                        .dontAnimate()
                        .dontTransform();
                Glide.with(context)
                        .asBitmap()
                        .load(url)
                        .apply(requestOptions)
                        .into(target);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

//    public static void setBackgroundVideoPlayer(ImageView image, String url) {
//        if (TextUtils.isEmpty(url)) {
//            setResource(image, R.drawable.gradient_bg_video);
//        } else {
//            setImage(image, url, 0, 0, null);
//        }
//    }

//    public static void setCover(String url, ImageView image) {
//        if (TextUtils.isEmpty(url))
//            setResource(image, getResIdCover());
//        else {
//            setImage(image, url, 0, getResIdCover(), null);
//        }
//    }

    public static void setCover(String url, ImageView image, int position) {
        if (TextUtils.isEmpty(url)) {
            setResource(image, getResIdCover(position));
        } else {
            setImage(image, url, R.drawable.df_slide, getResIdCover(position), null);
        }
    }

//    public static void setCover(ImageView image, String url) {
//        if (TextUtils.isEmpty(url))
//            setResource(image, getResIdCover());
//        else {
//            setImage(image, url, 0, getResIdCover(), null);
//        }
//    }

    public static void setCover(ImageView image, String url, int position) {
        if (TextUtils.isEmpty(url)) {
            setResource(image, getResIdCover(position));
        } else {
            setImage(image, url, R.drawable.df_slide, getResIdCover(position), null);
        }
    }

//    public static void setImage(String url, ImageView image) {
//        if (TextUtils.isEmpty(url)) {
//            setResource(image, getResIdImage());
//        } else {
//            setImage(image, url, R.drawable.df_image, getResIdImage(), null);
//        }
//    }

    public static void setImage(String url, ImageView image, long position) {
        if (TextUtils.isEmpty(url)) {
            setResource(image, getResIdImage(position));
        } else {
            setImage(image, url, R.drawable.df_image, getResIdImage(position), null);
        }
    }

    public static void setAlbum(ImageView image, String url) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdAlbum());
        } else {
            setImageTransform(image, url, R.drawable.df_album, getResIdAlbum());
        }
    }

    public static void setAlbum(ImageView image, String url, long position, int round) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdAlbum(position));
        } else {
            setImageTransform(image, url, R.drawable.df_album, getResIdAlbum(position),
                    new RoundedCornersTransformation(round, 0));
        }
    }

//    public static void setAlbum(String url, ImageView image) {
//        if (TextUtils.isEmpty(url)) {
//            setResourceTransform(image, getResIdAlbum());
//        } else {
//            setImageTransform(image, url, R.drawable.df_album, getResIdAlbum());
//        }
//    }

    public static void setAlbum(String url, ImageView image, int position) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdAlbum(position));
        } else {
            setImageTransform(image, url, R.drawable.df_album, getResIdAlbum(position));
        }
    }

    public static void setSong(String url, ImageView image, int position, int round) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdAlbum(position));
        } else {
            setImageTransform(image, url, R.drawable.df_song, getResIdAlbum(position),
                    new RoundedCornersTransformation(round, 0));
        }
    }

    public static void setSong(ImageView image, String url) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdAlbum());
        } else {
            setImageTransform(image, url, R.drawable.df_song, getResIdAlbum());
        }
    }

    public static void setSongTwoPlayer(ImageView image, String url, int round) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdAlbum());
        } else {
            setImageTransform(image, url, R.drawable.df_song_two_player, getResIdAlbum(), new RoundedCornersTransformation(round, 0));
        }
    }

    public static void setSong(ImageView image, String url, long position, int round) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdAlbum(position));
        } else {
            setImageTransform(image, url, R.drawable.df_song, getResIdAlbum(),
                    new RoundedCornersTransformation(round, 0));
        }
    }

    public static void setVideoLarge(String url, ImageView image, int position) {
        if (TextUtils.isEmpty(url)) {
            setResource(image, getResIdVideo(position));
        } else {
            setImage(image, url, R.drawable.df_slide, getResIdVideo(position), null);
        }
    }

    public static void setVideo(ImageView image, String url, long position) {

        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdVideoCurved(position));
        } else {
            setImageTransform(image, url, R.drawable.df_video, getResIdVideoCurved(position));
        }
    }

    public static void setVideo(String url, ImageView image) {

        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdVideo());
        } else {
            setImageTransform(image, url, R.drawable.df_video, getResIdVideo());
        }
    }

    public static void setImageNew(String url, ImageView image) {

        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdVideo());
        } else {
            setImageTransform(image, url, R.drawable.df_image_home_16_9, getResIdNew());
        }
    }

    public static void setImageNew219(String url, ImageView image) {

        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdVideo());
        } else {
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.drawable.df_image_home_21_9)
                    .priority(Priority.HIGH)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .dontAnimate()
                    .dontTransform()
                    .centerCrop();
            try {
                int width = image.getWidth();
                int height = image.getHeight();
                if (width > 0 && height > 0) {
                    requestOptions.override(width, height);
                }
                Glide.with(image.getContext())
                        .load(url)
                        .apply(requestOptions)
                        .into(image);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public static void setImageNewPoster(String url, ImageView image) {

        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdVideo());
        } else {
            setImageTransform(image, url, R.drawable.df_image_home_poster, getResIdNew());
        }
    }

    public static void setVideo(String url, ImageView image, long position) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdVideoCurved(position));
        } else {
            setImageTransform(image, url, R.drawable.df_video, getResIdVideoCurved(position));
        }
    }

    public static void setCoverTopic(String url, ImageView image, int position) {
        if (TextUtils.isEmpty(url)) {
            setResource(image, getResIdCover(position));
        } else {
            setImage(image, url, R.drawable.tab_shadow, getResIdCover(position), null);
        }
    }

    public static void setFlashHot(String url, ImageView image, int position) {
        if (TextUtils.isEmpty(url)) {
            setResource(image, getResIdFlashHot(position));
        } else {
            setImage(image, url, R.drawable.df_slide, getResIdFlashHot(position), null);
        }
    }

    public static void setAvatarSinger(String url, ImageView image, int position) {
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, getResIdAlbum(position));
        } else {
            setImageTransform(image, url, R.drawable.df_album, getResIdAlbum(position));
        }
    }

    public static void setAvatarSingerHome(String url, ImageView image, int position) {
        if (image == null)
            return;
        if (TextUtils.isEmpty(url)) {
            setResource(image, getResIdAlbum(position));
        } else {
            try {
                Context context = image.getContext();
                int width, height;
                width = image.getWidth();
                height = image.getHeight();
                if (width > 0 && height > 0) {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(R.drawable.df_image)
                            .error(getResIdAlbum(position))
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .override(width, height)
                            .dontAnimate()
                            .dontTransform()
                            .fitCenter()
                            .clone();
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .into(image);

                } else {
                    width = height = Utilities.dpToPx(context, context.getResources().getDimension(R.dimen.width_singer_home));
                    RequestOptions requestOptions = new RequestOptions()
                            .override(width, height)
                            .placeholder(R.drawable.df_image)
                            .error(getResIdAlbum(position))
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .dontAnimate()
                            .dontTransform()
                            .fitCenter()
                            .clone();
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .into(image);
                }
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

//    public static void setLabel(ImageView image, String url) {
//        if (TextUtils.isEmpty(url)) {
//            setResource(image, 0);
//        } else {
//            setImage(image, url, 0, 0, null);
//        }
//    }

//    public static void setAvatar(String url, final ImageView image) {
//        if (TextUtils.isEmpty(url)) {
//            setResource(image, R.drawable.df_avatar_profile);
//        } else {
//            setImageTransform(image, url, R.drawable.df_avatar_profile, R.drawable.df_avatar_profile);
//        }
//    }

    public static void setImageNoAnime(String url, int place, final ImageView image) {
        try {
            Context context = image.getContext();
            RequestOptions requestOptions = new RequestOptions()
                    .priority(Priority.IMMEDIATE)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(place)
                    .fitCenter()
                    .clone()
                    .dontAnimate()
                    .dontTransform();
            Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .apply(requestOptions)
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setDefaultAvatarMedia(ImageView image) {
        setResourceTransform(image, R.drawable.df_avatar_media);
    }

    public static void setDefaultMediaBlur(final ImageView image, int radius) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            if (width > 0 && height > 0) {
                RequestOptions requestOptions = new RequestOptions()
                        .override(width, height)
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .transform(new BlurTransformation(radius));
                Glide.with(context)
                        .load(R.drawable.df_cover_media)
                        .apply(requestOptions)
                        .transition(new DrawableTransitionOptions().crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)))
                        .into(image);
            } else {
                RequestOptions requestOptions = new RequestOptions()
                        .priority(Priority.HIGH)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .transform(new BlurTransformation(radius));
                Glide.with(context)
                        .load(R.drawable.df_cover_media)
                        .apply(requestOptions)
                        .transition(new DrawableTransitionOptions().crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)))
                        .into(image);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

//    private static int getResIdPoster(long position) {
//        return 0;
//    }

//    public static void setPoster(ImageView image, String url, int position) {
//        if (TextUtils.isEmpty(url)) {
//            setResourceTransform(image, getResIdPoster(position));
//        } else {
//            setImageTransform(image, url, 0, getResIdPoster(position));
//        }
//    }

    public static void setImagePlaylist(List<String> listImages, ImageView ivTopLeft, ImageView ivTopRight
            , ImageView ivBottomRight, ImageView ivBottomLeft, View viewRight) {
        if (ivTopLeft == null || ivBottomLeft == null || ivTopRight == null || ivBottomRight == null || viewRight == null)
            return;
        if (com.metfone.selfcare.util.Utilities.isEmpty(listImages)) {
            viewRight.setVisibility(View.GONE);
            ivTopLeft.setVisibility(View.VISIBLE);
            ivTopRight.setVisibility(View.GONE);
            ivBottomLeft.setVisibility(View.GONE);
            ivBottomRight.setVisibility(View.GONE);
            setAlbum(ivTopLeft, "");
            return;
        }

        int size = listImages.size();
        if (size >= 4) {
            viewRight.setVisibility(View.VISIBLE);
            ivTopLeft.setVisibility(View.VISIBLE);
            ivTopRight.setVisibility(View.VISIBLE);
            ivBottomLeft.setVisibility(View.VISIBLE);
            ivBottomRight.setVisibility(View.VISIBLE);
            setTopLeftPlaylist(ivTopLeft, listImages.get(0));
            setTopRightPlaylist(ivTopRight, listImages.get(1));
            setBottomRightPlaylist(ivBottomRight, listImages.get(2));
            setBottomLeftPlaylist(ivBottomLeft, listImages.get(3));
        } else {
            viewRight.setVisibility(View.GONE);
            ivTopLeft.setVisibility(View.VISIBLE);
            ivTopRight.setVisibility(View.GONE);
            ivBottomLeft.setVisibility(View.GONE);
            ivBottomRight.setVisibility(View.GONE);
            setAlbum(ivTopLeft, listImages.get(0));
        }
    }

    public static void setPosterMovie(String url, ImageView image) {
        setCoverItem(image, url, R.drawable.df_image_home_poster, R.drawable.df_image_home_poster);
    }

    public static void setCoverMovie(String url, ImageView image) {
        setCoverItem(image, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9);
    }

    public static void setCoverChannelOnFeed(ImageView imageView, String url) {
        if (imageView == null) return;
        if (url == null) url = "";
        try {
            //new FitCenter(),
            Glide.with(imageView.getContext())
                    .asBitmap()
                    .load(url)
                    .transition(withCrossFade(500))
                    .apply(new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                    .transforms(new RoundedCornersTransformation(ApplicationController.self().getRound(), 0, RoundedCornersTransformation.CornerType.TOP))
//                        .placeholder(R.drawable.df_profile)
                                    .error(R.drawable.df_profile)
                    )
                    .into(imageView);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setAvatarChannelOnFeed(final ImageView imageView, String url) {
        if (imageView == null)
            return;
        if (TextUtils.isEmpty(url)) {
            setResource(imageView, R.drawable.df_avatar_profile);
        } else {
            try {
                Context context = imageView.getContext();
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                if (width > 0 && height > 0) {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(R.drawable.df_avatar_profile)
                            .error(R.drawable.df_avatar_profile)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .override(width, height)
                            .dontAnimate()
                            .dontTransform()
                            .fitCenter()
                            .clone();
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .into(imageView);
                } else {
                    int size = Utilities.dpToPx(context, 72);
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(R.drawable.df_avatar_profile)
                            .error(R.drawable.df_avatar_profile)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .override(size, size)
                            .dontAnimate()
                            .dontTransform()
                            .fitCenter()
                            .clone();
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .into(imageView);
                }
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public static void setAvatarChannel(final ImageView imageView, String url) {
        if (imageView == null)
            return;
        if (TextUtils.isEmpty(url)) {
            setResource(imageView, R.drawable.df_channel_avatar);
        } else {
            try {
                Context context = imageView.getContext();
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                if (width > 0 && height > 0) {
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(R.drawable.df_channel_avatar)
                            .error(R.drawable.df_channel_avatar)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .override(width, height)
                            .dontAnimate()
                            .dontTransform()
                            .fitCenter()
                            .clone();
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .into(imageView);
                } else {
                    int size = Utilities.dpToPx(context, 120);
                    RequestOptions requestOptions = new RequestOptions()
                            .placeholder(R.drawable.df_channel_avatar)
                            .error(R.drawable.df_channel_avatar)
                            .priority(Priority.HIGH)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .override(size, size)
                            .dontAnimate()
                            .dontTransform()
                            .fitCenter()
                            .clone();
                    Glide.with(context)
                            .load(url)
                            .apply(requestOptions)
                            .into(imageView);
                }
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public static void setLogo(ImageView image, String url) {
        if (image == null) return;
        if (TextUtils.isEmpty(url)) {
            image.setVisibility(View.GONE);
        } else {
            image.setVisibility(View.VISIBLE);
            setImage(image, url, 0, 0, null);
        }
    }

    @SuppressLint("CheckResult")
    public static void setResource(ImageView image, int resource) {
        if (image == null) return;
        try {
            int width = image.getWidth();
            int height = image.getHeight();
            Context context = image.getContext();
            RequestOptions requestOptions = new RequestOptions()
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            if (width > 0 && height > 0) {
                requestOptions.override(width, height);
            }
            Glide.with(context)
                    .load(resource)
                    .apply(requestOptions.clone())
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setChannelVideo(String url, ImageView imageView) {
        if (imageView == null) return;
        if (url == null) url = "";
        try {
            Context context = imageView.getContext();
            int width = imageView.getWidth();
            int height = imageView.getHeight();
            if (width > 0 && height > 0) {
                RequestOptions requestOptions = new RequestOptions()
                        .placeholder(R.drawable.df_channel_avatar)
                        .error(R.drawable.df_channel_avatar)
                        .priority(Priority.HIGH)
                        .override(width, height)
                        .dontAnimate()
                        .dontTransform()
                        .centerCrop()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(imageView);
            } else {
                int size = Utilities.dpToPx(context, 120);
                RequestOptions requestOptions = new RequestOptions()
                        .placeholder(R.drawable.df_channel_avatar)
                        .error(R.drawable.df_channel_avatar)
                        .priority(Priority.HIGH)
                        .override(size, size)
                        .dontAnimate()
                        .dontTransform()
                        .centerCrop()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(imageView);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setTopLeftPlaylist(ImageView image, String url) {
        if (image == null) return;
        Transformation transformation = new RoundedCornersTransformation(ApplicationController.self().getRound()
                , 0, RoundedCornersTransformation.CornerType.TOP_LEFT);
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, R.drawable.df_playlist_top_left, transformation);
        } else {
            setImage(image, url, R.drawable.df_playlist_top_left, R.drawable.df_playlist_top_left, transformation);
        }
    }

    public static void setTopRightPlaylist(ImageView image, String url) {
        if (image == null) return;
        Transformation transformation = new RoundedCornersTransformation(ApplicationController.self().getRound()
                , 0, RoundedCornersTransformation.CornerType.TOP_RIGHT);
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, R.drawable.df_playlist_top_right, transformation);
        } else {
            setImage(image, url, R.drawable.df_playlist_top_right, R.drawable.df_playlist_top_right, transformation);
        }
    }

    public static void setBottomLeftPlaylist(ImageView image, String url) {
        if (image == null) return;
        Transformation transformation = new RoundedCornersTransformation(ApplicationController.self().getRound()
                , 0, RoundedCornersTransformation.CornerType.BOTTOM_LEFT);
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, R.drawable.df_playlist_bottom_left, transformation);
        } else {
            setImage(image, url, R.drawable.df_playlist_bottom_left, R.drawable.df_playlist_bottom_left, transformation);
        }
    }

    public static void setBottomRightPlaylist(ImageView image, String url) {
        if (image == null) return;
        Transformation transformation = new RoundedCornersTransformation(ApplicationController.self().getRound()
                , 0, RoundedCornersTransformation.CornerType.BOTTOM_RIGHT);
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, R.drawable.df_playlist_bottom_right, transformation);
        } else {
            setImage(image, url, R.drawable.df_playlist_bottom_right, R.drawable.df_playlist_bottom_right, transformation);
        }
    }

    public static void setNews(String url, ImageView imageView) {
//        setImageTransform(imageView, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9);
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.df_image_home_16_9)
                .error(R.drawable.df_image_home_16_9)
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(ApplicationController.self())
                .load(url)
                .apply(requestOptions)
                .centerCrop()
//                .transition(DrawableTransitionOptions.withCrossFade(500))
                .transform(ApplicationController.self().getRoundedCornersTransformation())
                .into(imageView);
    }

    public static void setVideoEpisode(ImageView imageView, String url) {
        //setImageTransform(imageView, url, 0, R.drawable.df_image_home_16_9);

        if (imageView == null || url == null) return;
        try {
            Glide.with(ApplicationController.self())
                    .asBitmap()
                    .load(url)
                    .transition(withCrossFade(500))
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .transforms(new CenterCrop()
                                    , ApplicationController.self().getRoundedCornersTransformation())
                            .error(R.drawable.df_image_home_16_9)
                    )
                    .into(imageView);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setImageShareContent(ImageView image, String url) {
        if (!TextUtils.isEmpty(url)) {
            setImage(image, url, R.drawable.df_image_home, R.drawable.df_image_home, null);
        }
    }

    public static void setImageFileShareContent(ImageView image, String filePath) {
        if (!TextUtils.isEmpty(filePath)) {
            setImageFile(image, filePath, R.drawable.df_image_home, R.drawable.df_image_home, 0, 0, null);
        }
    }

    public static void setImagePromotionMyViettel(CircleImageView imageView, TextView textView, String url, String shortName, int id) {
        try {
            if (TextUtils.isEmpty(url)) {
                if (shortName == null) shortName = "";
                else shortName = shortName.trim();
                if (shortName.length() > 5) shortName = shortName.substring(0, 5);
                if (textView != null) {
                    textView.setText(shortName);
                    textView.setVisibility(View.VISIBLE);
                }
                int index = Math.abs(id % Constants.CONTACT.NUM_COLORS_AVATAR_DEFAULT);
                imageView.setImageBitmap(ApplicationController.self().getAvatarBusiness()
                        .getAvatarBitmaps()[index]);
            } else {
                if (textView != null) {
                    textView.setText("");
                    textView.setVisibility(View.GONE);
                }
                setImage(imageView, url, R.drawable.df_image_home, R.drawable.df_image_home, null);
                Context context = imageView.getContext();
                int size = Utilities.dpToPx(context, 120);
                RequestOptions requestOptions = new RequestOptions()
                        .placeholder(R.drawable.df_image_home_circle)
                        .error(R.drawable.df_image_home_circle)
                        .priority(Priority.HIGH)
                        .override(size, size)
                        .dontAnimate()
                        .dontTransform()
                        .centerCrop()
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(imageView);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setResourceCaptcha(ImageView image, int resource) {
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            RequestOptions requestOptions = new RequestOptions()
                    .priority(Priority.HIGH)
                    .dontTransform()
                    .dontAnimate()
                    .optionalCenterInside()
                    .clone();
            if (width > 0 && height > 0) {
                requestOptions = requestOptions.override(width, height);
            }
            Glide.with(context)
                    .load(resource)
                    .apply(requestOptions)
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void setImageCaptcha(ImageView image, String url, RequestListener<Drawable> listener) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.color.white)
                    .error(R.color.white)
                    .priority(Priority.HIGH)
                    .dontTransform()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.NONE);
            if (width > 0 && height > 0) {
                requestOptions = requestOptions.override(width, height);
            }
            Glide.with(context)
                    .load(url)
                    .apply(requestOptions)
                    .listener(listener)
                    .into(image);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @SuppressLint("CheckResult")
    public static void setImageFile(ImageView image, String filePath, int placeholder, int error, int width, int height, Transformation transformation) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            File file = new File(filePath);
            Uri imageUri = Uri.fromFile(file);
            if (width == 0 && height == 0) {
                width = image.getMeasuredWidth();
                height = image.getMeasuredHeight();
            }
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(placeholder)
                    .error(error)
                    .priority(Priority.HIGH)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            if (width > 0 && height > 0)
                requestOptions.override(width, height);

            if (transformation == null) {
                Glide.with(context)
                        .load(imageUri)
                        .apply(requestOptions)
                        .transition(DrawableTransitionOptions.withCrossFade(500))
                        .into(image);
            } else {
                requestOptions.centerCrop()
                        .dontAnimate()
                        .transform(transformation);
                DrawableTransitionOptions transitionOptions = new DrawableTransitionOptions();
                transitionOptions.crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)).withCrossFade(500);
                Glide.with(context)
                        .load(imageUri)
                        .apply(requestOptions)
                        .transition(transitionOptions)
                        .into(image);
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static Bitmap downloadImageBitmap(String linkImage) {
        Log.d(TAG, "downloadImageBitmap: " + linkImage);
        Bitmap bitmap = null;
        try {
            InputStream inputStream = new URL(linkImage).openStream();   // Download Image from URL
            bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
            inputStream.close();
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return bitmap;
    }

    public static Bitmap getImageBitmapFromFile(String filePath) {
        Log.d(TAG, "getImageBitmapFromFile: " + filePath);
        Bitmap bitmap = null;
        try {
            File file = new File(filePath);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return bitmap;
    }

    public static void setCoverVideo(ImageView image, String url) {
        setCoverItem(image, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9);
    }

    public static void setBannerHome(String url, ImageView image) {
        setCoverItem(image, url, R.drawable.df_image_home_21_9, R.drawable.df_image_home_21_9);
    }

    public static void setCoverLargeComic(ImageView image, String url) {
        setCoverItem(image, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9);
    }

    public static void setCoverGridComic(ImageView image, String url) {
        setCoverItem(image, url, R.drawable.df_image_home, R.drawable.df_image_home);
    }

    public static void setCoverNotification(ImageView image, String url, int defaultRes) {
        setCoverItem(image, url, defaultRes, defaultRes);
    }

    public static void setCoverLargeNews(ImageView image, String url) {
        setCoverItem(image, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9);
    }

    public static void setCoverGridNews(ImageView image, String url) {
        setCoverItem(image, url, R.drawable.df_image_home, R.drawable.df_image_home);
    }

    public static void setCoverLargeTiin(ImageView image, String url) {
        setCoverItem(image, url, R.drawable.df_image_home_21_9, R.drawable.df_image_home_21_9);
    }

    public static void setCoverGridTiin(ImageView image, String url) {
        setCoverItem(image, url, R.drawable.df_image_home, R.drawable.df_image_home);
    }

    public static void setVideoPlayer(ImageView image, String url) {
        if (image == null) return;
        if (TextUtils.isEmpty(url)) {
            image.setImageResource(R.drawable.df_video_player);
        } else {
            setCoverItem(image, url, R.drawable.df_video_player, R.drawable.df_video_player);
        }
    }
}
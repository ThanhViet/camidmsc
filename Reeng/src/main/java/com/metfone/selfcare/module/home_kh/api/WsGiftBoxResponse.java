package com.metfone.selfcare.module.home_kh.api;

import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.tiin.DateUtilitis;

import java.util.Date;
import java.util.List;

public class WsGiftBoxResponse {

    private String errorCode;
    private String errorMessage;
    private Result result;

    public String getErrorCode() { return errorCode; }
    public void setErrorCode(String value) { this.errorCode = value; }

    public String getErrorMessage() { return errorMessage; }
    public void setErrorMessage(String value) { this.errorMessage = value; }

    public Result getResult() { return result; }
    public void setResult(Result value) { this.result = value; }
    public class Result {
        private Long status;
        private String code;
        private String message;
        private Object reward;
        private Object prize;
        private Long countSpin;
        private Long countTotal;
        private List<PrizeInfo> prizeInfoList;

        public Long getStatus() { return status; }
        public void setStatus(Long value) { this.status = value; }

        public String getCode() { return code; }
        public void setCode(String value) { this.code = value; }

        public String getMessage() { return message; }
        public void setMessage(String value) { this.message = value; }

        public Object getReward() { return reward; }
        public void setReward(Object value) { this.reward = value; }

        public Object getPrize() { return prize; }
        public void setPrize(Object value) { this.prize = value; }

        public Long getCountSpin() { return countSpin; }
        public void setCountSpin(Long value) { this.countSpin = value; }

        public Long getCountTotal() { return countTotal; }
        public void setCountTotal(Long value) { this.countTotal = value; }

        public List<PrizeInfo> getPrizeInfoList() { return prizeInfoList; }
        public void setPrizeInfoList(List<PrizeInfo> value) { this.prizeInfoList = value; }
    }

    public static class PrizeInfo {
        private Long giftPrizeID;
        private Long giftProgrameID;
        private Long spinType;
        private String prizeName;
        private Long status;
        private String description;
        private String prizeType;
        private Long prizeValue;
        private String prizeUnit;
        private String image;
        private Long expiredTime;
        private Long redeemTime;
        private String qrCode;
        private String giftRewardID;
        private String objectData1;

        public Long getGiftPrizeID() {
            return giftPrizeID;
        }

        public void setGiftPrizeID(Long value) {
            this.giftPrizeID = value;
        }

        public Long getGiftProgrameID() {
            return giftProgrameID;
        }

        public void setGiftProgrameID(Long value) {
            this.giftProgrameID = value;
        }

        public Long getSpinType() {
            return spinType;
        }

        public void setSpinType(Long value) {
            this.spinType = value;
        }

        public String getPrizeName() {
            return prizeName;
        }

        public void setPrizeName(String value) {
            this.prizeName = value;
        }

        public Long getStatus() {
            return status;
        }

        public void setStatus(Long value) {
            this.status = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String value) {
            this.description = value;
        }

        public String getPrizeType() {
            return prizeType;
        }

        public void setPrizeType(String value) {
            this.prizeType = value;
        }

        public Long getPrizeValue() {
            return prizeValue;
        }

        public void setPrizeValue(Long value) {
            this.prizeValue = value;
        }

        public String getPrizeUnit() {
            return prizeUnit;
        }

        public void setPrizeUnit(String value) {
            this.prizeUnit = value;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String value) {
            this.image = value;
        }

        public Long getExpiredTime() {
            return expiredTime;
        }

        public void setExpiredTime(Long value) {
            this.expiredTime = value;
        }

        public Long getRedeemTime() {
            return redeemTime;
        }

        public void setRedeemTime(Long value) {
            this.redeemTime = value;
        }

        public String getGiftRewardID() {
            return giftRewardID;
        }

        public void setGiftRewardID(String giftRewardID) {
            this.giftRewardID = giftRewardID;
        }

        public String getQrCode() {
            return qrCode;
        }

        public void setQrCode(String qrCode) {
            this.qrCode = qrCode;
        }

        public String getExpiredTimeFormat(){
            if(expiredTime == null){
                expiredTime = new Date().getTime();
            }
            return DateTimeUtils.dateToString(new Date(getExpiredTime()), DateConvert.DATE_REWARD_OUTPUT_EXPIRED_KM);
        }

        public String getRedeemTimeFormat(){
            if(redeemTime == null){
                redeemTime = new Date().getTime();
            }
            return DateTimeUtils.dateToString(new Date(getRedeemTime()), DateConvert.DATE_REWARD_OUTPUT_EXPIRED_KM);
        }

        public String getObjectData1() {
            return objectData1;
        }

        public void setObjectData1(String objectData1) {
            this.objectData1 = objectData1;
        }
    }
}

package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.metfone.selfcare.module.keeng.holder.SlidingBannerDetailHolder;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;

public class SlidingBannerAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> {

    private AbsInterface.OnItemListener listener;

    public SlidingBannerAdapter(Context context, AbsInterface.OnItemListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setListener(AbsInterface.OnItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SlidingBannerDetailHolder(listener, layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(items, position);
        }
    }
}
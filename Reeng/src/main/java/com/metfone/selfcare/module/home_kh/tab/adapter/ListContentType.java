package com.metfone.selfcare.module.home_kh.tab.adapter;

public class ListContentType {
    public static int HOZ_TOPIC_TYPE = 22;
    public static int HOZ_REWARD_CATE_TYPE = 23;
    public static int HOZ_REWARD_SHOP_TYPE = 24;
    public static int HOZ_EMPTY_TYPE = 25;
}

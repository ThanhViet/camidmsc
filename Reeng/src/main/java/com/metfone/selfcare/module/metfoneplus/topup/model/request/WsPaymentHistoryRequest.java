package com.metfone.selfcare.module.metfoneplus.topup.model.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsPaymentHistoryRequest extends BaseRequest<WsPaymentHistoryRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends Request {
        @SerializedName("language")
        private String language;
        @SerializedName("isdn")
        private String isdn;
        @SerializedName("pageSize")
        private int pageSize;
        @SerializedName("pageNum")
        private int pageNume;
        @SerializedName("fromDate")
        private String fromDate;
        @SerializedName("toDate")
        private String toDate;


        public WsRequest(String language, String isdn, int pageSize, int pageNume, String fromDate, String toDate) {
            this.language = language;
            this.isdn = isdn;
            this.pageSize = pageSize;
            this.pageNume = pageNume;
            this.fromDate = fromDate;
            this.toDate = toDate;
        }
    }
}

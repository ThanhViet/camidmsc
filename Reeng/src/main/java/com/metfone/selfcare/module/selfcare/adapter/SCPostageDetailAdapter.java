package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.holder.SCPostageDetailHolder;
import com.metfone.selfcare.module.selfcare.model.SCPostageDetailInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class SCPostageDetailAdapter extends BaseAdapter<BaseViewHolder> {
    private final static int TYPE_INFO = 0;
    private final static int TYPE_HEADER = 1;
    private final static int TYPE_POSTAGE = 2;
    private final static int TYPE_EMPTY = 3;

    private ArrayList<SCPostageDetailInfo> data = new ArrayList<>();
    private long fromDate = 0;
    private long toDate = 0;
    private double fee = 0;
    private int postType = 0;

    private SimpleDateFormat sdf = new SimpleDateFormat(TimeHelper.SDF_TIME_MYTEL, Locale.US);

    public SCPostageDetailAdapter(Context context) {
        super(context);
    }

    public void setItemsList(ArrayList<SCPostageDetailInfo> data) {
        this.data = data;
    }

    public void setInfo(int postType, long fromDate, long toDate, double fee)
    {
        this.postType = postType;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fee = fee;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return TYPE_INFO;
        else if(position == 1)
            return TYPE_HEADER;
        else
            return TYPE_POSTAGE;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(viewType == TYPE_INFO)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_postage_detail_info, null);
            return new BaseViewHolder(view);
        }
        else if(viewType == TYPE_HEADER)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_header, null);
            return new BaseViewHolder(view);
        }
        else
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_postage_detail_title, null);
            return new SCPostageDetailHolder(mContext, view, postType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        if(holder instanceof SCPostageDetailHolder)
        {
            SCPostageDetailHolder viewHolder = (SCPostageDetailHolder) holder;
            viewHolder.setData(data.get(position - 2));
        }
        else if(position == 0)
        {
            holder.setText(R.id.tvDate, sdf.format(fromDate) + " - " + sdf.format(toDate));
//            holder.setText(R.id.tvFeeAll, SCUtils.numberFormat(fee) +  SCConstants.SC_CURRENTCY);
        }
    }

    @Override
    public int getItemCount() {
        if(data == null || data.size() == 0) return 0;
        return data.size() + 2;
    }
}

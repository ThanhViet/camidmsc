package com.metfone.selfcare.module.search.adapter;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.CustomFragmentPagerAdapter;
import com.metfone.selfcare.module.search.event.KeySearchChangeEvent;
import com.metfone.selfcare.module.search.fragment.SearchFragment;
import com.metfone.selfcare.v5.widget.DynamicHeightViewPager;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends CustomFragmentPagerAdapter {
    static final String TAG = "ViewPagerDetailAdapter";
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private final List<Integer> mTypeList = new ArrayList<>();
    private int mCurrentPosition = -1;

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);

        if (position != mCurrentPosition && container instanceof DynamicHeightViewPager) {
            Fragment fragment = (Fragment) object;
            DynamicHeightViewPager pager = (DynamicHeightViewPager) container;

            if (fragment != null && fragment.getView() != null) {
                mCurrentPosition = position;
                pager.measureCurrentView(fragment.getView());
            }
        }
    }

    @Override
    public Fragment getItem(int position) {
        try {
            return mFragmentList.get(position);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return null;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }
    public void addFragment(int position,Fragment fragment, String title) {
        mFragmentList.add(position,fragment);
        mFragmentTitleList.add(position,title);
    }
    public void addFragment(Fragment fragment, String title, int type) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        mTypeList.add(type);
    }

    public int getTotalFragment() {
        return mFragmentList.size();
    }

    public int getPositionByType(int type) {
        return mTypeList.indexOf(type);
    }

    public int getTypeByPosition(int position) {
        return mTypeList.get(position);
    }

    @Override
    public String getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public synchronized void updateKeySearch(KeySearchChangeEvent event) {
        for (Fragment fragment : mFragmentList) {
            if (fragment instanceof SearchFragment) {
                ((SearchFragment) fragment).updateKeySearch(event);
            }
        }
    }
}
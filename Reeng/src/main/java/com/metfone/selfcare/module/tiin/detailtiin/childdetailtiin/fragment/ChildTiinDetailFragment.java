package com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.adapter.TagOnMediaAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.newdetails.SlideImage.SlideImageActivity;
import com.metfone.selfcare.module.newdetails.model.ListImageModel;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.tiin.AppProvider;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.module.tiin.activitytiin.TiinActivity;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.adapter.ChildDetailTiinAdapter;
import com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.presenter.ChildTiinDetailPresenter;
import com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.presenter.IChildTiinDetailMvpPresenter;
import com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.view.ChildTiinDetailMvpView;
import com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.fragment.MainTiinDetailFragment;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.tokenautocomplete.FilteredArrayAdapter;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChildTiinDetailFragment extends BaseFragment implements ChildTiinDetailMvpView, TiinListener.onDetailTiinItemListener {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerViewDetail;
    Unbinder unbinder;
    LinearLayout mBottomLikeCommentLayout;
    @BindView(R.id.retry_layout)
    LinearLayout retryLayout;
    private TagsCompletionView mEditText;
    private ImageButton mSend;
    private AppCompatImageView imgBtnLike, imgBtnComment, imgBtnShare;
    private TextView tvNumberComment;
    private RelativeLayout rlComment;
    private WSOnMedia rest;
    private ArrayList<UserInfo> userLikesInComment = new ArrayList<>();
    private ReengAccount mAccount;
    private FeedModelOnMedia mFeed;
    private FeedBusiness mFeedBusiness;
    private String urlAction;
    private int gaCategoryId, gaActionId;
    private String currentUrl = "";
    private boolean isLikeUrl = false;
    String mNewsUrl;
    boolean isRunningAnimation;

    private TiinModel tiinModel;
    private IChildTiinDetailMvpPresenter mPresenter;
    private ChildDetailTiinAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private int reTryModel = 1;
    private int reTryRelated = 1;
    private int reTrySibling = 1;
    private int currentPage = 1;
    private static final int COUNT = 3;
    private boolean loading;
    private List<TiinModel> dataSibling = new ArrayList<>();
    private boolean isStatusLoaded = false;
    public boolean isHomeNewsDetailFragment = false;
    private int numberComment = 0;
    private boolean fromTiin = false; //todo check full content hay chua
    private TagOnMediaAdapter adapterUserTag;

    public static ChildTiinDetailFragment newInstance(Bundle bundle, boolean isHome) {
        ChildTiinDetailFragment fragment = new ChildTiinDetailFragment();
        fragment.setArguments(bundle);
        fragment.isHomeNewsDetailFragment = isHome;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_child_tiin, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (mPresenter == null) {
            mPresenter = new ChildTiinDetailPresenter();
        }
        mPresenter.onAttach(this);
        setUp(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    private void setUp(View view) {
        if (getArguments() == null) {
            return;
        }
        tiinModel = (TiinModel) getArguments().getSerializable(ConstantTiin.KEY_ITEM_TIIN);
        fromTiin = getArguments().getBoolean(ConstantTiin.FULL_CONTENT_TIIN);
        if (tiinModel != null) {
            mNewsUrl = tiinModel.getUrl();
        }
        if (!TextUtils.isEmpty(mNewsUrl) && (mNewsUrl.contains("tiin.vn") || mNewsUrl.contains("http:") || mNewsUrl.contains("https:"))) {
            urlAction = mNewsUrl;
        } else {
            urlAction = ConstantTiin.DOMAIN + mNewsUrl;
        }
        if (TextUtils.isEmpty(urlAction)) {
            return;
        }
        if (tiinModel != null) {
            linearLayoutManager = new LinearLayoutManager(getTiinDetailActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewDetail.setHasFixedSize(true);
            recyclerViewDetail.setLayoutManager(linearLayoutManager);
            adapter = new ChildDetailTiinAdapter(getTiinDetailActivity(), tiinModel, this, fromTiin);
//            recyclerViewDetail.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (adapter != null) {
//                        adapter.loadAdView();
//                    }
//                }
//            }, 1000);
            recyclerViewDetail.setAdapter(adapter);
        }

        ApplicationController application = (ApplicationController) getBaseActivity().getApplication();
        mFeedBusiness = application.getFeedBusiness();
        int feedType = getArguments().getInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, 0);
        if (feedType == Constants.ONMEDIA.FEED_CONTACT_DETAIL) {
            mFeed = mFeedBusiness.getFeedProfileProcess();
        } else if (feedType == Constants.ONMEDIA.FEED_PROFILE) {
            mFeed = mFeedBusiness.getFeedProfileFromUrl(urlAction);
        } else {
            mFeed = mFeedBusiness.getFeedModelFromUrl(urlAction);
        }

        rest = new WSOnMedia(application);
        if (mFeed == null && tiinModel != null) {
            mFeed = new FeedModelOnMedia();
            FeedContent feedContent = new FeedContent();
            feedContent.setItemType(FeedContent.ITEM_TYPE_NEWS);
            feedContent.setUrl(tiinModel.getUrl());
            feedContent.setItemName(tiinModel.getTitle());
            feedContent.setImageUrl(tiinModel.getImage());
            mFeed.setFeedContent(feedContent);
        }
        currentUrl = urlAction;
        mAccount = application.getReengAccountBusiness().getCurrentAccount();

        if (currentUrl == null) {
            currentUrl = "";
        }
        mBottomLikeCommentLayout = view.findViewById(R.id.person_chat_detail_footer);
        mEditText = view.findViewById(R.id.person_chat_detail_input_text);
        mSend = view.findViewById(R.id.person_chat_detail_send_reeng_text);
        imgBtnLike = view.findViewById(R.id.img_like_content);
        imgBtnComment = view.findViewById(R.id.img_comment_new);
        imgBtnShare = view.findViewById(R.id.img_share_new);
        tvNumberComment = view.findViewById(R.id.tv_number_comment_new);
        rlComment = view.findViewById(R.id.rl_comment);
        if (DeviceHelper.isTablet(getBaseActivity())) {
            mEditText.setImeOptions(EditorInfo.IME_FLAG_NO_FULLSCREEN);
        } else {
            mEditText.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        setupCommentLike();
        if (mFeed != null) {
            isLikeUrl = mFeed.getIsLike() == 1;
            if (mFeed.getIsLike() == 1) {
                imgBtnLike.setImageResource(R.drawable.ic_v5_heart_active);
            } else {
                imgBtnLike.setImageResource(R.drawable.ic_v5_heart_normal);
            }
        }

        recyclerViewDetail.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    int posVisible = linearLayoutManager.findFirstVisibleItemPosition();
                    int lastVisible = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                    //Load more
                    int totalItemCount = linearLayoutManager.getItemCount();
                    if (!loading && totalItemCount <= (lastVisible + 5) && dataSibling.size() > 0) {
                        onLoadMore();
                        loading = true;
                    }
                } catch (Exception ex) {
                    Log.e(TAG, "onScrolled Exception:" + ex.toString());
                }
            }
        });
        InputMethodUtils.hideKeyboardWhenTouch(recyclerViewDetail, getTiinDetailActivity());
    }

    private void onLoadMore() {
//        isRefresh = false;
        currentPage++;
        if (mPresenter != null) {
            mPresenter.getSiblingNew(tiinModel.getId(), currentPage, 10);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (adapter != null) {
            adapter = null;
        }
        Log.e("--------cancel Tag Tiin: ", getTagRequest() + " retry: " + reTryModel);
        Http.cancel(getTagRequest());
        if (mPresenter != null) {
            mPresenter.onDetach();
            mPresenter = null;
        }
        if (mSend != null) {
            mSend.setOnClickListener(null);
        }

        if (imgBtnLike != null) {
            imgBtnLike.setOnClickListener(null);
        }
        if (getTiinDetailActivity() != null) {
            getTiinDetailActivity().stopVideoDetail();
        }
        if(adapterUserTag != null){
            adapterUserTag = null;
        }
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFeedBusiness = null;
        if (userLikesInComment != null) {
            userLikesInComment.clear();
        }
        rest = null;
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        if (!flag) {
            if(retryLayout != null) retryLayout.setVisibility(View.VISIBLE);
        } else {
            if(retryLayout != null) retryLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void loadDataStatus(boolean flag) {
        isStatusLoaded = flag;
    }

    public void loadData() {
        if (mPresenter != null && tiinModel != null) {
            if(!fromTiin) {
                reTryModel = 1;
                Log.e("--------add Tag Tiin", getTagRequest());
                mPresenter.getDetailNew(tiinModel.getId(), getTagRequest());
            }else{
                if (isHomeNewsDetailFragment) {
                    loadNewsStatus();
                }
                if (isHomeNewsDetailFragment && getParentFragment() != null && getParentFragment() instanceof MainTiinDetailFragment) {
                    ((MainTiinDetailFragment) getParentFragment()).loadData();
                    isHomeNewsDetailFragment = false;
                }
                //setdata
                if (mBottomLikeCommentLayout != null) {
                    mBottomLikeCommentLayout.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void loadNewsStatus() {
        if (mPresenter != null && !isStatusLoaded) {
            mPresenter.getNewStatus(urlAction);
            loadDataRelate();
        }
    }

    @Override
    public void bindData(TiinModel response) {

        if (response == null || response.getBody() == null || response.getBody().size() == 0) {
            if (reTryModel <= COUNT) {
                Log.e("--------cancel Tag Tiin: ", getTagRequest() + " retry: " + reTryModel);
                Http.cancel(getTagRequest());
                reTryModel++;
                Log.e("--------add Tag Tiin", getTagRequest());
                mPresenter.getDetailNew(tiinModel.getId(),getTagRequest());
            }
        } else {
            tiinModel = response;
            if (isHomeNewsDetailFragment) {
                loadNewsStatus();
            }
            if (isHomeNewsDetailFragment && getParentFragment() != null && getParentFragment() instanceof MainTiinDetailFragment) {
                ((MainTiinDetailFragment) getParentFragment()).loadData();
                ((MainTiinDetailFragment) getParentFragment()).loadCatagory(response.getCategory());
                isHomeNewsDetailFragment = false;
            }
            //setdata
            adapter.addBlock(response);
            if (mBottomLikeCommentLayout != null) {
                mBottomLikeCommentLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private String getTagRequest(){
        return this.toString() + String.valueOf(reTryModel);
    }

    @Override
    public void bindDataRelate(List<TiinModel> response) {
        if (response == null) {
            if (reTryRelated <= COUNT) {
                reTryRelated++;
                loadDataRelate();
            }
        } else {
            adapter.addRelated(response);
            if (mPresenter != null && tiinModel != null) {
                mPresenter.getSiblingNew(tiinModel.getId(), currentPage, 10);
            }
        }
    }

    @Override
    public void binDataSibling(List<TiinModel> response) {
        if (response == null) {
            if (reTrySibling <= COUNT) {
                reTrySibling++;
                mPresenter.getSiblingNew(tiinModel.getId(), currentPage, 10);
            }
        } else {
            dataSibling.addAll(response);
            adapter.addSibling(dataSibling);
            loading = false;
        }
    }

    @Override
    public void loadDataFeed(FeedModelOnMedia model) {
        if (model == null) {
            return;
        }
        mFeed = model;
        if (TextUtils.isEmpty(mFeed.getFeedContent().getUrl())) {
            mFeed.getFeedContent().setUrl(currentUrl);
        }
        if (TextUtils.isEmpty(mFeed.getFeedContent().getItemType()) || !mFeed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_NEWS)) {
            mFeed.getFeedContent().setItemType(FeedContent.ITEM_TYPE_NEWS);
        }
        if (TextUtils.isEmpty(mFeed.getFeedContent().getItemName()) || !mFeed.getFeedContent().getItemName().equals(tiinModel.getTitle())) {
            mFeed.getFeedContent().setItemName(tiinModel.getTitle());
        }
        if (TextUtils.isEmpty(mFeed.getFeedContent().getImageUrl()) || !mFeed.getFeedContent().getImageUrl().equals(tiinModel.getImage())) {
            mFeed.getFeedContent().setImageUrl(tiinModel.getImage());
        }
        setLikeFeed(mFeed.getIsLike() == 1);
        setNumberComment((int) mFeed.getFeedContent().getCountComment());
    }

    @Override
    public void onItemClickRelate(TiinModel model) {
        readTiin(model);
        AppProvider.CLICK_CLOSE = AppProvider.CLICK_CLOSE + 1;
        if (getTiinDetailActivity() != null) {
            getTiinDetailActivity().stopVideoDetail();
        }
        scrollFirst();
    }

    @Override
    public void onClickLinkText(String text, String url) {
        if (getTiinDetailActivity() != null) {
            getTiinDetailActivity().stopVideoDetail();
        }
        TiinModel model = TiinUtilities.parserLinkURLNews(url);
        model.setUrl(url);
        if (model.getId() > 0) {
            if (model.getType() == 5) {
                Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
                intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY);
                intent.putExtra(ConstantTiin.KEY_CATEGORY, model.getId());
                intent.putExtra(ConstantTiin.KEY_TITLE, text);
                getBaseActivity().startActivity(intent);
            } else if (model.getType() == 3) {
                getTiinDetailActivity().playVideoTiin(model);
            } else {
                readTiin(model);
                AppProvider.CLICK_CLOSE = AppProvider.CLICK_CLOSE + 1;
            }
        } else {
            //todo ra webview
            UrlConfigHelper.gotoWebViewOnMedia(ApplicationController.self(), getBaseActivity(), url);
        }
    }

    @Override
    public void onClickImage(NewsDetailModel entry) {
        if (getTiinDetailActivity() != null) {
            getTiinDetailActivity().stopVideoDetail();
        }
        try {
            int position = 0;
            ArrayList<String> listImage = new ArrayList<>();
            for (int i = 0; i < tiinModel.getBody().size(); i++) {
                NewsDetailModel modelTiin = tiinModel.getBody().get(i);
                if (modelTiin.getType() == 2) {
                    listImage.add(modelTiin.getContent());
                    if (modelTiin.getContent().equals(entry.getContent())) {
                        position = listImage.size() - 1;
                    }
                }
            }

            ListImageModel listImageModel = new ListImageModel(listImage);
            Intent intent = new Intent(getBaseActivity(), SlideImageActivity.class);
            intent.putExtra("position", position);
            intent.putExtra("listImage", listImageModel);
            getBaseActivity().startActivity(intent);
        } catch (Exception ex) {
            Log.e(TAG, "onClickImageItem : " + ex.toString());
        }
    }

    public void scrollFirst() {
        if (recyclerViewDetail != null) {
            if (getTiinDetailActivity() != null && getTiinDetailActivity().isPlaying) {
                recyclerViewDetail.scrollToPosition(0);
            }
        }
    }

    private void setupCommentLike() {
        ArrayList<PhoneNumber> listPhone = ApplicationController.self().getContactBusiness().getListNumberUseMocha();
        if (listPhone == null) listPhone = new ArrayList<>();
        adapterUserTag = new TagOnMediaAdapter(ApplicationController.self(), listPhone, mEditText);
        mEditText.setAdapter(adapterUserTag);

        mEditText.setThreshold(0);
        adapterUserTag.setListener(new FilteredArrayAdapter.OnChangeItem() {
            @Override
            public void onChangeItem(int count) {
                Log.i(TAG, "onChangeItem: " + count);
                if (count > 2) {
                    int height = getResources().getDimensionPixelOffset(R.dimen.max_height_drop_down_tag);
                    mEditText.setDropDownHeight(height);
                } else
                    mEditText.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });
        TiinUtilities.setVisibilityView(rlComment, true);
        TiinUtilities.setVisibilityView(imgBtnShare, true);
        mSend.setClickable(true);
        mEditText.setHint(getResources().getString(R.string.hint_comment_tiin));
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (getBaseActivity() == null) {
                    return;
                }
                Log.i(TAG, "link: " + charSequence.toString());
                String text = charSequence.toString();
                if (!text.isEmpty()) {
                    getBaseActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSend.setVisibility(View.VISIBLE);
//                            imgBtnLike.setVisibility(View.GONE);
                            TiinUtilities.setVisibilityView(rlComment, false);
                            TiinUtilities.setVisibilityView(imgBtnShare, false);
//                            TiinUtilities.setVisibilityView(tvNumberComment, false);
                            mSend.setClickable(true);
                        }
                    });
                } else {
                    getBaseActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSend.setVisibility(View.GONE);
//                            imgBtnLike.setVisibility(View.VISIBLE);
                            TiinUtilities.setVisibilityView(rlComment, true);
                            TiinUtilities.setVisibilityView(imgBtnShare, true);
//                            TiinUtilities.setVisibilityView(tvNumberComment, true);
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                    getBaseActivity().showDialogLogin();
                else
                    sendComment();
            }
        });

        imgBtnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                    getBaseActivity().showDialogLogin();
                else {
//                    getBaseActivity().trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_like);
                    if (mFeed != null && currentUrl.equals(urlAction)
                            && !mFeed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_TOTAL)) {
                        onClickLikeFeed(mFeed);
                    } else {
                        onClickLike(isLikeUrl);
                    }
                }
            }
        });
        rlComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFeed != null && mFeedBusiness != null) {
                    mFeed.getFeedContent().setUrl(urlAction);
                    Intent intent = new Intent(getTiinDetailActivity(), OnMediaActivityNew.class);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, mFeed.getFeedContent().getUrl());
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, mFeed);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, true);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_GET_DETAIL_URL, false);
                    //intent.putExtra(Constants.ONMEDIA.EXTRAS_CONTENT_DATA, mFeed.getFeedContent());
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_TAB_NEWS);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_ROW_ID, mFeed.getBase64RowId());
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, false);
                    startActivity(intent);
                }
            }
        });
        imgBtnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    getBaseActivity().showDialogLogin();
                } else {
                    if (tiinModel != null) {
                        ShareUtils.openShareMenu(getBaseActivity(), tiinModel);
                    }
                }
            }
        });
    }

    public void onClickLikeFeed(final FeedModelOnMedia feed) {
        if (getTiinDetailActivity() == null || feed == null || mAccount == null || rest == null) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(getBaseActivity())) {
            getTiinDetailActivity().showToast(R.string.no_connectivity_check_again);
            return;
        }

        final boolean isLiked = (feed.getIsLike() == 1);
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(feed, !isLiked);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), action, "", feed.getBase64RowId(), "", null,
                new ResponseListener(ChildTiinDetailFragment.this, 1, isLiked), new ResponseErrorListener(ChildTiinDetailFragment.this, 1, isLiked));
    }

    public void setLikeFeed(boolean isLike) {
        if (isLike) {
            imgBtnLike.setImageResource(R.drawable.ic_v5_heart_active);
            isLikeUrl = true;
        } else {
            isLikeUrl = false;
            imgBtnLike.setImageResource(R.drawable.ic_v5_heart_normal);
        }
    }

    private void setLikeFeed(FeedModelOnMedia feed, boolean isLike) {
        //neu la like thi tang like them 1, neu la unlike thi tru 1.

        int delta;
        if (isLike) {
            delta = 1;
            feed.setIsLike(1);
//            mImgLike.setBackgroundResource(R.drawable.ic_onmedia_like_press);
            imgBtnLike.setImageResource(R.drawable.ic_v5_heart_active);
        } else {
            delta = -1;
            feed.setIsLike(0);
//            mImgLike.setBackgroundResource(R.drawable.ic_onmedia_like);
            imgBtnLike.setImageResource(R.drawable.ic_v5_heart_normal);
        }
        long countLike = feed.getFeedContent().getCountLike();
        feed.getFeedContent().setCountLike(countLike + delta);
        if (mFeed != null) {
            mFeed.getFeedContent().setCountLike(countLike + delta);
        }
    }

    public void onClickLike(final boolean isLiked) {
        if (getTiinDetailActivity() == null || mAccount == null || rest == null) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(getBaseActivity())) {
            getBaseActivity().showToast(R.string.no_connectivity_check_again);
            return;
        }

        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(!isLiked);
        rest.logAppV6(currentUrl, "", null, action, "", "", "", null,
                new ResponseListener(ChildTiinDetailFragment.this, 2, isLiked), new ResponseErrorListener(ChildTiinDetailFragment.this, 2, isLiked));
    }

    private void sendComment() {
        if (getTiinDetailActivity() == null || mFeedBusiness == null || rest == null || mEditText.getText().toString().length() == 0) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(getBaseActivity())) {
            getBaseActivity().showToast(R.string.no_connectivity_check_again);
            return;
        }
        Log.i(TAG, "send text: " + mEditText.getText().toString());

        ArrayList<TagMocha> mListTagFake = mFeedBusiness.getListTagFromListPhoneNumber(mEditText.getUserInfo());
        String textTag = mFeedBusiness.getTextTag(mListTagFake);

        String messageContent = TextHelper.trimTextOnMedia(mEditText.getTextTag());
//        messageContent = TextHelper.getInstant().badwordFilter(messageContent);
        mEditText.resetObject();
        Log.i(TAG, "text after getRaw: " + messageContent);
        String rowId = "";
        FeedContent feedContent = null;
        if (mFeed != null && currentUrl.equals(urlAction)) {
            rowId = mFeed.getBase64RowId();
            feedContent = mFeed.getFeedContent();
        }
        rest.logAppV6(currentUrl, "", feedContent, FeedModelOnMedia.ActionLogApp.COMMENT, messageContent,
                rowId, textTag, null,
                new ResponseListener(ChildTiinDetailFragment.this, 3), new ResponseErrorListener(ChildTiinDetailFragment.this, 3));

//        if (getBaseActivity() != null) {
//            getBaseActivity().trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_comment);
//        }
    }

    private static class ResponseListener implements Response.Listener<String> {
        WeakReference<ChildTiinDetailFragment> weakReference;
        int type;
        boolean isLiked;

        ResponseListener(ChildTiinDetailFragment fragment, int type, boolean isLiked) {
            weakReference = new WeakReference<>(fragment);
            this.type = type;
            this.isLiked = isLiked;
        }

        ResponseListener(ChildTiinDetailFragment fragment, int type) {
            weakReference = new WeakReference<>(fragment);
            this.type = type;
        }

        @Override
        public void onResponse(String response) {
            if (weakReference == null) return;
            ChildTiinDetailFragment fragment = weakReference.get();
            if (fragment == null) {
                return;
            }
            switch (type) {
                case 1: //like feed response
                    fragment.onLikeFeedResponse(response, fragment.mFeed, isLiked);
                    break;

                case 2: // like response
                    fragment.onLikeResponse(response, isLiked);
                    break;

                case 3: // comment response
                    fragment.onSendCommentResponse(response);
                    break;
            }
        }
    }

    private static class ResponseErrorListener implements Response.ErrorListener {
        WeakReference<ChildTiinDetailFragment> weakReference;
        int type;
        boolean isLiked;

        ResponseErrorListener(ChildTiinDetailFragment fragment, int type, boolean isLiked) {
            weakReference = new WeakReference<>(fragment);
            this.type = type;
            this.isLiked = isLiked;
        }

        ResponseErrorListener(ChildTiinDetailFragment fragment, int type) {
            weakReference = new WeakReference<>(fragment);
            this.type = type;
        }

        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if (weakReference == null) return;
            ChildTiinDetailFragment fragment = weakReference.get();
            if (fragment == null) {
                return;
            }
            switch (type) {
                case 1: //like feed error
                    fragment.setLikeFeed(fragment.mFeed, isLiked);
                    if (fragment.getBaseActivity() != null) {
                        fragment.getBaseActivity().showToast(R.string.e601_error_but_undefined);
                    }
                    break;

                case 2: // like error
                    fragment.setLikeFeed(isLiked);
                    if (fragment.getBaseActivity() != null) {
                        fragment.getBaseActivity().showToast(R.string.e601_error_but_undefined);
                    }
                    break;

                case 3: // comment error
                    Log.i("NewsDetailFragment", "Response error" + volleyError.getMessage());
                    if (fragment.getBaseActivity() != null) {
                        fragment.getBaseActivity().showToast(R.string.e601_error_but_undefined);
                    }
                    break;
            }
        }
    }

    public void onLikeFeedResponse(String response, FeedModelOnMedia feed, boolean isLiked) {
        Log.i(TAG, "actionLike: onresponse: " + response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                if (code != HTTPCode.E200_OK) {
                    setLikeFeed(feed, isLiked);
                    if (getBaseActivity() != null)
                        getBaseActivity().showToast(R.string.e601_error_but_undefined);
                } else {
                    //islike----unlike, !islike----like
                    if (!isLiked) {
                        userLikesInComment.add(0, new UserInfo(mAccount.getJidNumber(), mAccount.getName()));
                        if (userLikesInComment.size() > 2) {
                            ArrayList<UserInfo> listTmp = new ArrayList<>();
                            listTmp.add(0, userLikesInComment.get(0));
                            listTmp.add(1, userLikesInComment.get(1));
                            userLikesInComment = listTmp;
                        }
                    } else {
                        if (userLikesInComment.size() == 0) {
                            Log.i(TAG, "Loi roi, size phai khac 0");
                        } else if (userLikesInComment.size() == 1) {
                            userLikesInComment.clear();
                        } else if (userLikesInComment.size() == 2) {
                            int indexToDelete = -1;
                            for (int i = 0; i < userLikesInComment.size(); i++) {
                                if (userLikesInComment.get(i).getMsisdn().equals(mAccount.getJidNumber())) {
                                    indexToDelete = i;
                                    break;
                                }
                            }
                            if (indexToDelete != -1) {
                                userLikesInComment.remove(indexToDelete);
                            }
                        } else {
                            Log.i(TAG, "Loi roi, size phai < 3");
                        }
                    }
                }
            } else {
                setLikeFeed(feed, isLiked);
                if (getBaseActivity() != null)
                    getBaseActivity().showToast(R.string.e601_error_but_undefined);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            setLikeFeed(feed, isLiked);
            if (getBaseActivity() != null)
                getBaseActivity().showToast(R.string.e601_error_but_undefined);
        }
    }

    public void onLikeResponse(String response, boolean isLiked) {
        Log.i(TAG, "actionLike: onresponse: " + response);
        try {
            if (isDetached()) {
                return;
            }
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                if (code != HTTPCode.E200_OK) {
                    setLikeFeed(isLiked);
                    if (getBaseActivity() != null)
                        getBaseActivity().showToast(R.string.e601_error_but_undefined);
                } else {
                    //islike----unlike, !islike----like
                    if (mAccount == null) {
                        return;
                    }
                    if (!isLiked) {
                        userLikesInComment.add(0, new UserInfo(mAccount.getJidNumber(), mAccount.getName()));
                        if (userLikesInComment.size() > 2) {
                            ArrayList<UserInfo> listTmp = new ArrayList<>();
                            listTmp.add(0, userLikesInComment.get(0));
                            listTmp.add(1, userLikesInComment.get(1));
                            userLikesInComment = listTmp;
                        }
                    } else {
                        if (userLikesInComment.size() == 0) {
                            Log.i(TAG, "Loi roi, size phai khac 0");
                        } else if (userLikesInComment.size() == 1) {
                            userLikesInComment.clear();
                        } else if (userLikesInComment.size() == 2) {
                            int indexToDelete = -1;
                            for (int i = 0; i < userLikesInComment.size(); i++) {
                                if (userLikesInComment.get(i).getMsisdn().equals(mAccount.getJidNumber())) {
                                    indexToDelete = i;
                                    break;
                                }
                            }
                            if (indexToDelete != -1) {
                                userLikesInComment.remove(indexToDelete);
                            }
                        } else {
                            Log.i(TAG, "Loi roi, size phai < 3");
                        }
                    }
                }
            } else {
                setLikeFeed(isLiked);
                if (getBaseActivity() != null) {
                    getBaseActivity().showToast(R.string.e601_error_but_undefined);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "onClickLikeFeed Exception", e);
            setLikeFeed(isLiked);
            if (getBaseActivity() != null) {
                getBaseActivity().showToast(R.string.e601_error_but_undefined);
            }
        }
    }

    public void onSendCommentResponse(String response) {
        Log.i(TAG, "actionComment: onSendCommentResponse: " + response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                if (getBaseActivity() != null) {
                    if (code == HTTPCode.E200_OK) {
                        getBaseActivity().showToast(R.string.comment_success);
                        numberComment++;
                        mFeed.getFeedContent().setCountComment(numberComment);
                        setNumberComment(numberComment);
                    } else {
                        getBaseActivity().showToast(R.string.e601_error_but_undefined);
                    }
                }
            } else {
                if (getBaseActivity() != null) {
                    getBaseActivity().showToast(R.string.e601_error_but_undefined);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            if (getBaseActivity() != null) {
                getBaseActivity().showToast(R.string.e601_error_but_undefined);
            }
        }
    }

    @Override
    public void onPlayVideo(NewsDetailModel model, int position, ChildDetailTiinAdapter.NewsDetailVideoHolder holder) {
        if (getTiinDetailActivity() != null) {
            getTiinDetailActivity().playVideoDetailTiin(model, holder);
        }
    }

    @Override
    public void onDetachVideoDetail() {
        if (getTiinDetailActivity() != null) {
            getTiinDetailActivity().stopVideoDetail();
        }
    }

    public void setNumberComment(int number) {
        if (number > 0) {
            numberComment = number;
            TiinUtilities.setVisibilityView(tvNumberComment, true);
            if(number > 99){
                tvNumberComment.setText("99+");
            }else {
                tvNumberComment.setText(Utilities.shortenLongNumber(number));
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(FeedModelOnMedia noteEvent) {
        if (urlAction.equals(noteEvent.getFeedContent().getUrl())) {
            mFeed = noteEvent;
            setLikeFeed(mFeed.getIsLike() == 1);
            setNumberComment((int) mFeed.getFeedContent().getCountComment());
        }

    }

    private void loadDataRelate() {
        if (tiinModel != null) {
            if (tiinModel.getThematicId() > 0) {
                mPresenter.getRelateEventNew(tiinModel.getId(), 1, 10);
            } else {
                mPresenter.getRelateNew(tiinModel.getId());
            }
        }
    }
    @Override
    public void onItemClickMore(TiinModel model) {
        DialogUtils.showOptionTiinItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionTiin(object, menuId);
            }
        });
    }
}

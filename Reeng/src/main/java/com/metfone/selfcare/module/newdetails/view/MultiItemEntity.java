package com.metfone.selfcare.module.newdetails.view;

public interface MultiItemEntity {
    int getItemType();
}

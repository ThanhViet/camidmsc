/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/13
 *
 */

package com.metfone.selfcare.module.share.listener;

import android.net.Uri;

import java.util.ArrayList;

public interface ShareImagesOnOtherAppListener {
    void onPrepareDownload();

    void onCompletedDownload(ArrayList<Uri> list);
}

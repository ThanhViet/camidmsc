/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.tab_home.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.tab_home.holder.HorizontalContentHolder;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.util.Log;

public class BoxContentAdapter extends TabHomeDetailAdapter {

    public BoxContentAdapter(Activity act) {
        super(act);
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof TabHomeModel) {
            return ((TabHomeModel) item).getType();
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public BaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TabHomeModel.TYPE_BOX_GRID_VIDEO
                || viewType == TabHomeModel.TYPE_BOX_GRID_MUSIC
                || viewType == TabHomeModel.TYPE_BOX_GRID_MOVIE
                || viewType == TabHomeModel.TYPE_BOX_GRID_NEWS
                || viewType == TabHomeModel.TYPE_BOX_GRID_COMIC
                || viewType == TabHomeModel.TYPE_BOX_GRID_CHANNEL
                || viewType == TabHomeModel.TYPE_BOX_GRID_TIIN
        ) {
            return new HorizontalContentHolder(layoutInflater.inflate(R.layout.holder_recycler_view, parent, false), activity, listener, viewType);
        }
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseAdapter.ViewHolder holder, int position) {
        Object item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

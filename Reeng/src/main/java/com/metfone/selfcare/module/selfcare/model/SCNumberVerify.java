package com.metfone.selfcare.module.selfcare.model;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 5/10/2019.
 */

public class SCNumberVerify implements Serializable{
    private String msisdn;
    private boolean isVerify;
    private boolean selected;
    private String subId;
    private String otp;
    private boolean isMyAccount;


    public SCNumberVerify(String msisdn, boolean isVerify, String subId) {
        this.msisdn = msisdn;
        this.isVerify = isVerify;
        this.subId = subId;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public void setVerify(boolean verify) {
        isVerify = verify;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public boolean isVerify() {
        return isVerify;
    }

    public String getSubId() {
        return subId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public boolean isMyAccount() {
        return isMyAccount;
    }

    public void setMyAccount(boolean myAccount) {
        isMyAccount = myAccount;
    }

    @Override
    public String toString() {
        return "SCNumberVerify{" +
                "msisdn='" + msisdn + '\'' +
                ", isVerify=" + isVerify +
                ", selected=" + selected +
                ", subId='" + subId + '\'' +
                ", otp='" + otp + '\'' +
                '}';
    }
}

package com.metfone.selfcare.module.gameLive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.player.MochaPlayerUtil;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.gameLive.model.LiveGameModel;
import com.metfone.selfcare.module.gameLive.network.APICallBack;
import com.metfone.selfcare.module.gameLive.network.RetrofitClientInstance;
import com.metfone.selfcare.module.gameLive.network.parse.RestGameInfo;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.ui.view.tab_video.VideoPlaybackControlView;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class GameLiveActivity extends BaseSlidingFragmentActivity {

    private static final String TAG = GameLiveActivity.class.getSimpleName();

    public static void startActivity(BaseSlidingFragmentActivity activity, Video video) {
        Intent intent = new Intent(activity, GameLiveActivity.class);
        intent.putExtra(Constants.TabVideo.VIDEO, video);
        activity.startActivity(intent);
        if (activity instanceof VideoPlayerActivity) activity.finish();
    }

    public static void start(Context context, String idGame) {
        Intent starter = new Intent(context, GameLiveActivity.class);
        starter.putExtra("GameId", idGame);
        context.startActivity(starter);
    }

    @BindView(R.id.ivVideo)
    ImageView ivVideo;
    @BindView(R.id.frVideo)
    FrameLayout frVideo;
    @BindView(R.id.frController)
    FrameLayout frController;
    @BindView(R.id.container)
    RelativeLayout container;
    @BindView(R.id.bg)
    ImageView bg;

    private ApplicationController app;
    //    private Video currentVideo;
    private LiveGameModel currentGameInfo;
    private String playerName;
    private int widthScreen, heightScreen;
    private RetrofitClientInstance retrofitClientInstance = new RetrofitClientInstance();

    private synchronized RetrofitClientInstance getRetrofitClient() {
        if (retrofitClientInstance == null) retrofitClientInstance = new RetrofitClientInstance();
        return retrofitClientInstance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game_live);
        ButterKnife.bind(this);
//        if (getIntent() != null)
//            currentVideo = (Video) getIntent().getSerializableExtra(Constants.TabVideo.VIDEO);
//        if (currentVideo == null) {
//            showToast(R.string.e601_error_but_undefined);
//            finish();
//        }

        app = (ApplicationController) getApplication();

        initView();
        loadData();
    }

    private void initView() {

        widthScreen = ScreenManager.getWidth(this);
        heightScreen = ScreenManager.getHeight(this);

        ImageBusiness.setResource(bg, R.drawable.bg_game_live);
    }

    private void loadData() {
        ReengAccount reengAccount = app.getReengAccountBusiness().getCurrentAccount();
        getRetrofitClient().getGameInfo(reengAccount.getJidNumber(), new APICallBack<RestGameInfo>() {
            @Override
            public void onResponse(Response<RestGameInfo> response) {
                if (response != null && response.body() != null && response.body().getResult() != null) {
                    currentGameInfo = response.body().getResult();
                } else {
                    currentGameInfo = new LiveGameModel();
                    currentGameInfo.setActive(-1);
                    currentGameInfo.setLinkGame("");
                }

                //Add player and set du lieu video
                executeFragmentTransaction(GameLiveFragment.newInstance(currentGameInfo), R.id.container, false, false);

                Video currentVideo = new Video();
                currentVideo.setOriginalPath(currentGameInfo.getLinkGame());
                currentVideo.setAspectRatio((float) 16 / (float) 9);
                currentVideo.setLive(true);
                playerName = String.valueOf(System.nanoTime());
                setSizeFrameVideo(currentVideo.getAspectRatio());
                MochaPlayerUtil.getPlayer(playerName).addPlayerViewTo(frVideo);
                MochaPlayerUtil.getPlayer(playerName).addControllerListener(callBackListener);
                MochaPlayerUtil.getPlayer(playerName).setupGameStreaming();
                MochaPlayerUtil.getPlayer(playerName).setPlayWhenReady(true);
                ivVideo.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MochaPlayerUtil.getPlayer(playerName).prepare(currentVideo);
                    }
                }, 200);
            }

            @Override
            public void onError(Throwable error) {
                if(GameLiveActivity.this.isFinishing()) return;

                DialogMessage dialogMessage = new DialogMessage(GameLiveActivity.this, true);
                dialogMessage.setMessage(getString(R.string.e601_error_but_undefined));
                dialogMessage.setNegativeListener(new NegativeListener() {
                    @Override
                    public void onNegative(Object result) {
                        finish();
                    }
                });
                if (dialogMessage != null && !dialogMessage.isShowing())
                    dialogMessage.show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MochaPlayerUtil.getPlayer(playerName).setPlayWhenReady(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MochaPlayerUtil.getPlayer(playerName).setPlayWhenReady(false);
    }

    @Override
    protected void onDestroy() {
        MochaPlayerUtil.getPlayer(playerName).removeControllerListener(callBackListener);
        MochaPlayerUtil.getInstance().removerPlayerBy(playerName);
        super.onDestroy();
    }

    public void setSizeFrameVideo(double aspectRatio) {
        ViewGroup.LayoutParams layoutParams = frController.getLayoutParams();
        layoutParams.width = widthScreen;
        layoutParams.height = (int) (layoutParams.width / aspectRatio);
        frController.setLayoutParams(layoutParams);
    }

    private VideoPlaybackControlView.CallBackListener callBackListener = new VideoPlaybackControlView.CallBackListener() {
        @Override
        public void onPlayerStateChanged(int stage) {

        }

        @Override
        public void onPlayerError(String error) {

        }

        @Override
        public void onVisibilityChange(int visibility) {

        }

        @Override
        public void onFullScreen() {

        }

        @Override
        public void onMute(boolean flag) {

        }

        @Override
        public void onTimeChange(long time) {

        }

        @Override
        public void onHaveSeek(boolean flag) {

        }

        @Override
        public void onPlayNextVideo() {

        }

        @Override
        public void onPlayPreviousVideo() {

        }

        @Override
        public void onPlayPause(boolean state) {

        }

        @Override
        public void onSmallScreen() {

        }

        @Override
        public void onMoreClick() {

        }

        @Override
        public void onReplay() {

        }

        @Override
        public void onQuality() {

        }

        @Override
        public void onRequestAds(String position) {

        }

        @Override
        public void onShowAd() {

        }

        @Override
        public void onHideAd() {

        }

        @Override
        public void onHideController() {

        }

        @Override
        public void onShowController() {

        }

        @Override
        public void autoSwitchPlayer() {
            MochaPlayerUtil.getPlayer(playerName).setupGameStreaming();
        }

        @Override
        public void onClickViewFrame() {

        }
    };
}

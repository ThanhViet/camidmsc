package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.metfone.selfcare.activity.FeedbackActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.module.home_kh.activity.FeedbackKhActivity;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsCheckForceUpdateAppResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.ui.dialog.DialogEditText;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.viettel.util.LogDebugHelper;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

public class AboutKhFragment extends BaseSettingKhFragment {
    private static final String TAG = AboutKhFragment.class.getSimpleName();
    //    private OnFragmentAppInfoListener mListener;
    @BindView(R.id.setting_help_view)
    LinearLayoutCompat mViewHelp;
    @BindView(R.id.setting_privacy_view)
    LinearLayoutCompat mViewPrivacy;
    //    @BindView(R.id.viewGroupAppInfor)
//    ConstraintLayout viewAppVersion;
    //    private Button mBtnUpdate;
    private ImageView ivIcon;

    private int countClick;
    private static final int COUNT_OPEN_DIALOG = 7;
    private static final String ENABLE_LOG_DEBUG = "*098#";
    private static final String EXPORT_AND_UPLOAD_LOG_DEBUG = "*097#";
    private static final String DISABLE_LOG_DEBUG = "*096#";
    private static final String SHOW_REVISION = "*101#";
    private static final String SHOW_ALL_INFO = "#MC#0*";
    private static final String SWITCH_SV_TEST = "#MC#dev*";
    private static final String SWITCH_SV_PROD = "#MC#prod*";
    @BindView(R.id.tvVersionApp)
    AppCompatTextView tvVersion;
    @BindView(R.id.tvVersionDes)
    AppCompatTextView tvVersionDes;
    @BindView(R.id.btnUpdate)
    RoundTextView btnUpdate;
    private KhHomeClient homeKhClient;
//    @BindView(R.id.ivCircleRed)
//    View ivCircleRed;

    public static AboutKhFragment newInstance() {
        AboutKhFragment fragment = new AboutKhFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
//            mListener = (OnFragmentAppInfoListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_about_app_kh;
    }

    @Override
    protected void initView(View view) {
        findComponentViews(view);
        setListener();
        setTitle(R.string.setting_about);
    }

    private void findComponentViews(View view) {
        ivIcon = view.findViewById(R.id.imgIconApp);

//        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
//        View mActionBarView = mParentActivity.getToolBarView();
//
//        EllipsisTextView mTvwTitle = mActionBarView.findViewById(R.id.ab_title);
//        mImgBack = mActionBarView.findViewById(R.id.ab_back_btn);
//        ImageView mImgOption = mActionBarView.findViewById(R.id.ab_more_btn);
        //
//        TextView mTvwRevision = view.findViewById(R.id.about_reversion);
//        mImgOption.setVisibility(View.GONE);
//        mTvwTitle.setText(mRes.getString(R.string.setting_about));
        try {
            tvVersion.setText(String.format(getString(R.string.version_name), BuildConfig.VERSION_NAME));
//            String revision = mParentActivity.getResources().getString(R.string.build) + ": " + Config.REVISION
//                    + (BuildConfig.DEBUG ? ("; Version code: " + BuildConfig.VERSION_CODE) : "");
//            mTvwRevision.setText(revision);
//            mBtnUpdate = view.findViewById(R.id.btn_update_version);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                if (mApplication.getConfigBusiness().isUpdate()) {
//                    mBtnUpdate.setVisibility(View.VISIBLE);
//                    tvUpdateStatus.setVisibility(View.GONE);
//                    tvVersionDes.setText(R.string.has_new_version);
                    getVersionChPlay();
                    btnUpdate.setVisibility(View.VISIBLE);
//                    ivCircleRed.setVisibility(View.VISIBLE);
                } else {
//                    mBtnUpdate.setVisibility(View.GONE);
//                    tvVersionDes.setVisibility(View.VISIBLE);
                    tvVersionDes.setText(R.string.update_status);
                }
            } else {
//                mBtnUpdate.setVisibility(View.GONE);
//                tvVersionDes.setVisibility(View.VISIBLE);
                tvVersionDes.setText(R.string.update_status_sdk_less_than_19);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }

//        if (BuildConfig.DEBUG) {
//            View viewServerInfo = view.findViewById(R.id.layout_server_test_info);
//            if (viewServerInfo != null) viewServerInfo.setVisibility(View.VISIBLE);
//            TextView tvServerInfo = view.findViewById(R.id.tv_server_info);
//            StringBuilder sb = new StringBuilder();
//            sb.append("<b>isDev: </b>").append(mApplication.getReengAccountBusiness().isDev());
//            sb.append("<br/><b>domainOTP: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainOTP());
//            sb.append("<br/><b>domainFile: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainFile());
//            sb.append("<br/><b>domainMessage: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainMessage());
//            sb.append("<br/><b>domainImage: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainImage());
//            sb.append("<br/><b>domainOnMedia: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainOnMedia());
//            sb.append("<br/><b>domainMochaVideo: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainMochaVideo());
//            sb.append("<br/><b>domainKMusic: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainKeengMusic());
//            sb.append("<br/><b>domainKMusicSearch: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainKeengMusicSearch());
//            sb.append("<br/><b>domainKMovies: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainKeengMovies());
//            sb.append("<br/><b>domainNetNews: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainNetnews());
//            sb.append("<br/><b>domainTiin: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainTiin());
//            sb.append("<br/><b>domainServiceKeeng: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainServiceKeeng());
//            sb.append("<br/><b>domainMediaKeeng: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainMediaKeeng());
//            sb.append("<br/><b>domainImageKeeng: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainImageKeeng());
//            sb.append("<br/><b>domainGameIQ: </b>").append(mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_GAME_IQ));
//            sb.append("<br/>--------------------");
//            sb.append("<br/><b>domainFileOld: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainFileOld());
//            sb.append("<br/><b>domainImageOld: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainImageOld());
//            sb.append("<br/><b>domainOnMediaOld: </b>").append(UrlConfigHelper.getInstance(mApplication).getDomainOnMediaOld());
//            sb.append("<br/><br/>--------------------");
//            if (tvServerInfo != null) tvServerInfo.setText(Html.fromHtml(sb.toString()));
//        }
    }

    private void setListener() {
//        setBackListener();
//        setHelpListener();
//        setPrivacyListener();
        setUpdateListener();
//        setLogoListener();
    }

    private void setLogoListener() {
        ivIcon.setOnClickListener(v -> {
            countClick++;
            if (countClick == COUNT_OPEN_DIALOG) {
                countClick = 0;
                DialogEditText dialogEditText = new DialogEditText(mParentActivity, false);
                dialogEditText.setLabel("Enter code");
                dialogEditText.setNegativeLabel(getString(R.string.exit));
                dialogEditText.setPositiveLabel(getString(R.string.ok));
                dialogEditText.setPositiveListener(result -> {
                    if (ENABLE_LOG_DEBUG.equals(result)) {
                        Toast.makeText(mApplication, "Enable log debug", Toast.LENGTH_SHORT).show();
                        LogDebugHelper.getInstance(mApplication).setStateEnableLog(true);
                    } else if (EXPORT_AND_UPLOAD_LOG_DEBUG.equals(result)) {
                        mApplication.getTransferFileBusiness().startUploadLog(true);
                    } else if (DISABLE_LOG_DEBUG.equals(result)) {
                        Toast.makeText(mApplication, "Disable log debug", Toast.LENGTH_SHORT).show();
                        LogDebugHelper.getInstance(mApplication).setStateEnableLog(false);
                    } else if (SHOW_REVISION.equals(result)) {
                        Toast.makeText(mApplication, "REVISION: " + Config.REVISION, Toast.LENGTH_SHORT).show();
                    } else if (SHOW_ALL_INFO.equals(result)) {
                        showAllInfo();
                    } else if (SWITCH_SV_TEST.equals(result)) {
                        switchToServerTest(true);
                    } else if (SWITCH_SV_PROD.equals(result)) {
                        switchToServerTest(false);
                    }
                });
                dialogEditText.show();
            }
        });
    }

    private void switchToServerTest(boolean serverTest) {
        mApplication.getReengAccountBusiness().setDev(serverTest);
        String content = "Switch to server " + (serverTest ? "dev" : "product") + " success";
        Toast.makeText(mApplication, content, Toast.LENGTH_SHORT).show();

        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            mParentActivity.showLoadingDialog("", R.string.loading);
            mApplication.getApplicationComponent().provideUserApi().getInfoAnonymous(BuildConfig.KEY_XXTEA, new ApiCallbackV2<String>() {
                @Override
                public void onSuccess(String lastId, String s) {
                    mApplication.getConfigBusiness().getConfigFromServer(true, true);
//                    processPermission();
                }

                @Override
                public void onError(String s) {
//                    showToast(s);
                }

                @Override
                public void onComplete() {
                    mParentActivity.hideLoadingDialog();
                }
            });
        }
    }

    private void showAllInfo() {
        DialogMessage dialogMessage = new DialogMessage(mParentActivity, true);
        String msisdn = mApplication.getReengAccountBusiness().getJidNumber();
        String token = mApplication.getReengAccountBusiness().getToken();
        token = token.substring(0, token.length() - 4);
        int ssl = mApplication.getPref().getInt(Constants.PREFERENCE.PREF_MOCHA_ENABLE_SSL, -1);
        String uuid = Utilities.getUuidApp();
        StringBuilder sb = new StringBuilder();
        sb.append("Msisdn: ").append(msisdn)
                .append("\nuuid: ").append(uuid)
                .append("\nToken: ").append(token)
                .append("\nSSL: ").append(ssl);

        sb.append("\nisDev: ").append(mApplication.getReengAccountBusiness().isDev());
        sb.append("\nOTP: ").append(UrlConfigHelper.getInstance(mApplication).getDomainOTP());
        sb.append("\nFileV1: ").append(UrlConfigHelper.getInstance(mApplication).getDomainFile());
        sb.append("\nMessage: ").append(UrlConfigHelper.getInstance(mApplication).getDomainMessage());
        sb.append("\nImageV1: ").append(UrlConfigHelper.getInstance(mApplication).getDomainImage());
        sb.append("\nOnMediaV1: ").append(UrlConfigHelper.getInstance(mApplication).getDomainOnMedia());
        sb.append("\nMochaVideo: ").append(UrlConfigHelper.getInstance(mApplication).getDomainMochaVideo());
        sb.append("\nKeengMusic: ").append(UrlConfigHelper.getInstance(mApplication).getDomainKeengMusic());
        sb.append("\nKeengMusicSearch: ").append(UrlConfigHelper.getInstance(mApplication).getDomainKeengMusicSearch());
        sb.append("\nKeengMovies: ").append(UrlConfigHelper.getInstance(mApplication).getDomainKeengMovies());
        sb.append("\nNetNews: ").append(UrlConfigHelper.getInstance(mApplication).getDomainNetnews());
        sb.append("\nTiin: ").append(UrlConfigHelper.getInstance(mApplication).getDomainTiin());
        sb.append("\nServiceKeeng: ").append(UrlConfigHelper.getInstance(mApplication).getDomainServiceKeeng());
        sb.append("\nMediaKeeng: ").append(UrlConfigHelper.getInstance(mApplication).getDomainMediaKeeng());
        sb.append("\nImageKeeng: ").append(UrlConfigHelper.getInstance(mApplication).getDomainImageKeeng());
        sb.append("\ndomainGameIQ: ").append(mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_GAME_IQ));
        sb.append("\n--------------------");
        sb.append("\n\nFileOld: ").append(UrlConfigHelper.getInstance(mApplication).getDomainFileOld());
        sb.append("\nImageOld: ").append(UrlConfigHelper.getInstance(mApplication).getDomainImageOld());
        sb.append("\nOnMediaOld: ").append(UrlConfigHelper.getInstance(mApplication).getDomainOnMediaOld());

        dialogMessage.setMessage(sb.toString());
        dialogMessage.setLabelButton("OK");
        dialogMessage.show();
    }

    private void setUpdateListener() {
        if (mApplication.getConfigBusiness().isUpdate()) {
            btnUpdate.setOnClickListener(v -> NavigateActivityHelper.navigateToPlayStore(mParentActivity, BuildConfig.APPLICATION_ID));
        }
    }

//    private void setBackListener() {
//        mImgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mParentActivity.onBackPressed();
//            }
//        });
//    }

//    private void setHelpListener() {
//        mViewHelp.setOnClickListener(view -> mListener.navigateWebViewInApp(getString(R.string.help_link), R.string.setting_help));
//    }

//    private void setPrivacyListener() {
//        mViewPrivacy.setOnClickListener(view -> mListener.navigateWebViewInApp(getString(R.string.privacy_link), R.string.setting_privacy));
//    }

    @OnClick({R.id.setting_feedback, R.id.setting_rate_app, R.id.btnUpdate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.setting_feedback:
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    mParentActivity.showDialogLogin();
                } else {
                    FeedbackKhActivity.start(mParentActivity);
//                    FeedbackActivity.start(mParentActivity);
                }
                break;
            case R.id.setting_rate_app:
                NavigateActivityHelper.navigateToPlayStore(mParentActivity, BuildConfig.APPLICATION_ID);
                break;
            case R.id.btnUpdate:
                NavigateActivityHelper.navigateToPlayStore(mParentActivity, BuildConfig.APPLICATION_ID);
                break;
        }
    }

    private void getVersionChPlay() {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsCheckForceUpdateApp(new KhApiCallback<KHBaseResponse<WsCheckForceUpdateAppResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<WsCheckForceUpdateAppResponse> body) {
                if (body.getData() != null && body.getData().getWsResponse() != null) {
                    tvVersionDes.setText(String.format(ResourceUtils.getString(R.string.please_update_version), body.getData().getWsResponse().getNewVersion()));
                }
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsCheckForceUpdateApp > onFailed");
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logH("wsCheckForceUpdateApp > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsCheckForceUpdateAppResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsCheckForceUpdateApp > onFailure");
            }
        }, BuildConfig.VERSION_NAME);
    }

    private void logH(String m) {
        Log.e("H-About", m);
    }
}
package com.metfone.selfcare.module.selfcare.network.request;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterMyCreditRequest {
    private String isdn;

    public RegisterMyCreditRequest(String isdn) {
        this.isdn = isdn;
    }

    public String getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msisdn", isdn);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

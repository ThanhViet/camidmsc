package com.metfone.selfcare.module.metfoneplus.dialog;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;

public class MPDatePickerFragment extends DialogFragment {
    public static final String TAG = "DatePickerFragment";
    private AppCompatTextView mTxtDateFrom;
    private AppCompatTextView mTxtDateTo;
    private CalendarView mCalendarView;

    public MPDatePickerFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_mp_datepicker_dialog, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTxtDateFrom = view.findViewById(R.id.txt_date_picker_from);
        mTxtDateTo = view.findViewById(R.id.txt_date_picker_to);
        mCalendarView = view.findViewById(R.id.calendar_view);

        mTxtDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mTxtDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                Log.e(TAG, "onSelectedDayChange: day = " + dayOfMonth);
            }
        });
    }
}

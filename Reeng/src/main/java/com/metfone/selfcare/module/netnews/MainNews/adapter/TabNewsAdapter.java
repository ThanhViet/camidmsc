package com.metfone.selfcare.module.netnews.MainNews.adapter;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.ViewGroup;

import com.metfone.selfcare.module.newdetails.model.SettingCategoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HaiKE on 8/18/17.
 */

public class TabNewsAdapter extends FragmentStatePagerAdapter {
    ViewPager viewPager;
    TabLayout tabLayout;
    private List<SettingCategoryModel> mFragments = new ArrayList<>();
    public TabNewsAdapter(FragmentManager fm, List<SettingCategoryModel> mFragments) {
        super(fm);
        this.mFragments = mFragments;
    }

    public TabNewsAdapter(FragmentManager manager, List<SettingCategoryModel> mFragments, ViewPager viewPager, TabLayout tabLayout) {
        super(manager);
        this.mFragments = mFragments;
        this.viewPager = viewPager;
        this.tabLayout = tabLayout;
    }

    public TabNewsAdapter(FragmentManager manager, ViewPager viewPager, TabLayout tabLayout) {
        super(manager);
        this.viewPager = viewPager;
        this.tabLayout = tabLayout;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragments.get(position).getTitle();
    }

    @Override
    public int getItemPosition(Object object) {
//        return super.getItemPosition(object);
        return POSITION_NONE;
    }


    public SettingCategoryModel getSettingCategoryModel(String categoryId) {
        for (int i = 0; i < mFragments.size(); i++) {
            if (categoryId.equals(mFragments.get(i).getCategoryId() + ""))
                return mFragments.get(i);
        }
        return null;
    }

    public int getTabIndex(String categoryId) {
        for (int i = 0; i < mFragments.size(); i++) {
            if (categoryId.equals(mFragments.get(i).getCategoryId() + ""))
                return i;
        }
        return -1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        try {
            return super.instantiateItem(container, position);
        } catch (Exception e) {
            return getItem(position);
        }
    }

    public void addFrag(SettingCategoryModel fragment, int index) {
        mFragments.add(index, fragment);
    }

    public void addFrag(SettingCategoryModel fragment) {
        mFragments.add(fragment);
    }

    public void removeFrag(int position) {
        mFragments.remove(position);
    }

}
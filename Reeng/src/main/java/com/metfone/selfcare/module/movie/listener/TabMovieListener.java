/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.listener;

import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnClickSliderBanner;
import com.metfone.selfcare.model.tabMovie.Movie;

public class TabMovieListener {

    public interface OnAdapterClick extends OnClickContentMovie, OnClickSliderBanner {

        void onClickTitleBox(Object item, int position);
        void onClickRefresh();
        void playFilm(Object item, int position,boolean isPlayNow);
        void onScrollData();
    }

    public interface IPlayVideoNow {
        void onPlayNow(Movie movie);
        void playTrailer(String title,String urlTrailer,long currentTime);
    }
}

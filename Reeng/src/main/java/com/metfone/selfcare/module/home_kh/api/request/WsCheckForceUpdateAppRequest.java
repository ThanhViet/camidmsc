package com.metfone.selfcare.module.home_kh.api.request;

import com.blankj.utilcode.util.LanguageUtils;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsCheckForceUpdateAppRequest extends BaseRequest<WsCheckForceUpdateAppRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends Request {
        @SerializedName("version")
        String version;
        @SerializedName("isdn")
        String isdn;
        @SerializedName("language")
        String language;
        @SerializedName("versionApp")
        String versionApp;

        public WsRequest(String version) {
            this.version = version;
            this.isdn = "";
            this.language = LanguageUtils.getCurrentLocale().getLanguage();
            this.versionApp = "Android";
        }
    }
}

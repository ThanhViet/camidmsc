/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2018/12/20
 *
 */

package com.metfone.selfcare.module.keeng.fragment.category;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.adapter.detail.PlaylistDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.CategoryModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.AlbumDetailGridItemDecoration;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.keeng.widget.LoadingView;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomDialog;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomSheetAdapter;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomSheetData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class PlaylistDetailFragment extends BaseFragment implements BaseListener.PlaylistDetailListener,
        SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, AppBarLayout
                .OnOffsetChangedListener, BaseListener.OnClickBottomSheet {
    private final int MAX_NUMBER_TO_RETRY = KeengApi.MAX_NUMBER_TO_RETRY;
    private int currentPage = 1;
    //    private int numPerPage = 20;
    private int countError = 0;
    private boolean canLoadMore = false;
    private boolean isRefresh = true;
    private boolean isLoading = false;
    private SwipeRefreshLayout refreshView;
    private LoadingView loadingView;
    private RecyclerView recyclerView;
    private PlaylistDetailAdapter adapter;
    private List<AllModel> data = new ArrayList<>();
    private PlayListModel mediaInfo = null;
    private ImageView ivCover, ivTopLeft, ivBottomLeft, ivTopRight, ivBottomRight;
    private View ivAvatar, viewRight;
    private TextView tvTitleToolbar, tvTitle, tvDescription, tvListenNo, tvCount;
    private View btnOption, btnRandom, btnPlay;
    private AppBarLayout appBarLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    private Constants.State currentState = Constants.State.IDLE;
    private BottomDialog dialogOption;
    private LinearLayoutManager layoutManager;
    private ListenerUtils listenerUtils;

    public static PlaylistDetailFragment newInstance() {
        Bundle args = new Bundle();
        PlaylistDetailFragment fragment = new PlaylistDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "PlaylistDetailFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_playlist_detail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        refreshView = view.findViewById(R.id.refresh_view);
        loadingView = view.findViewById(R.id.loading_view);
        recyclerView = view.findViewById(R.id.recycler_view);
        ivCover = view.findViewById(R.id.iv_cover);
        ivTopLeft = view.findViewById(R.id.iv_top_left);
        ivTopRight = view.findViewById(R.id.iv_top_right);
        ivBottomLeft = view.findViewById(R.id.iv_bottom_left);
        ivBottomRight = view.findViewById(R.id.iv_bottom_right);
        ivAvatar = view.findViewById(R.id.layout_image);
        viewRight = view.findViewById(R.id.right_layout);
        tvTitleToolbar = view.findViewById(R.id.tv_title_toolbar);
        tvTitle = view.findViewById(R.id.tv_title);
        tvDescription = view.findViewById(R.id.tv_description);
        tvListenNo = view.findViewById(R.id.tv_listen_no);
        tvCount = view.findViewById(R.id.tv_count);
        btnOption = view.findViewById(R.id.button_option);
        btnPlay = view.findViewById(R.id.button_play);
        btnRandom = view.findViewById(R.id.button_shuffle);
        appBarLayout = view.findViewById(R.id.app_bar_layout);
        collapsingToolbar = view.findViewById(R.id.collapsing_toolbar);
        toolbar = view.findViewById(R.id.toolbar);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            assert getArguments() != null;
            mediaInfo = (PlayListModel) getArguments().getSerializable(Constants.KEY_DATA);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        if (mediaInfo == null) {
            onBackPressed();
            return;
        }
        if (refreshView != null) {
            refreshView.setEnabled(false);
        }
        initListener();
        adapter = new PlaylistDetailAdapter(mActivity, getData());
        adapter.setListener(this);
        new Handler().postDelayed(() -> {
            setupMediaInfo();
            setupAvatar(false);
            setupCountInfo();
            if (getData().isEmpty())
                doLoadData(true);
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    private void initListener() {
//        btnOption.setOnClickListener(this);
        btnRandom.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        if (collapsingToolbar != null) {
            collapsingToolbar.setTitle(" ");
            collapsingToolbar.setTitleEnabled(false);
        }
        if (toolbar != null) {
            toolbar.setNavigationIcon(mActivity.getResources().getDrawable(R.drawable.ic_v5_back_white));
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        if (appBarLayout != null) {
            appBarLayout.setExpanded(true);
            appBarLayout.addOnOffsetChangedListener(this);
        }
    }

    private void setupMediaInfo() {
        if (mediaInfo == null || mActivity == null)
            return;
        tvTitle.setText(mediaInfo.getName());
        tvTitleToolbar.setText(mediaInfo.getName());
        String str = mediaInfo.getNameUser();
        if (TextUtils.isEmpty(str)) {
            tvDescription.setVisibility(View.GONE);
        } else {
            tvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(mActivity.getString(R.string.create_by, str));
        }
        if (mediaInfo.getListened() < 1) {
            tvListenNo.setVisibility(View.GONE);
            tvListenNo.setText("");
        } else if (mediaInfo.getListened() == 1) {
            if (mediaInfo.isVideoList()) {
                tvListenNo.setVisibility(View.VISIBLE);
                tvListenNo.setText(getString(R.string.m_view_no, "1"));
            } else {
                tvListenNo.setVisibility(View.VISIBLE);
                tvListenNo.setText(getString(R.string.m_listen_no, "1"));
            }
        } else {
            if (mediaInfo.isVideoList()) {
                tvListenNo.setVisibility(View.VISIBLE);
                tvListenNo.setText(getString(R.string.m_views_no, mediaInfo.getListenNo()));
            } else {
                tvListenNo.setVisibility(View.VISIBLE);
                tvListenNo.setText(getString(R.string.m_listens_no, mediaInfo.getListenNo()));
            }
        }
    }

    private void setupAvatar(boolean isRefresh) {
        if (mediaInfo != null) {
            List<String> listImages = mediaInfo.getListAvatar(isRefresh);
            ImageBusiness.setImagePlaylist(listImages, ivTopLeft, ivTopRight, ivBottomRight, ivBottomLeft, viewRight);
            ImageBusiness.setCover(ivCover, mediaInfo.getCoverUrl(), (int) mediaInfo.getId());
//            ImageBusiness.setImagePlayerBlur(ivCover, listImages.get(0), 200);
        } else {
            viewRight.setVisibility(View.GONE);
            ivTopLeft.setVisibility(View.VISIBLE);
            ivTopRight.setVisibility(View.GONE);
            ivBottomLeft.setVisibility(View.GONE);
            ivBottomRight.setVisibility(View.GONE);
            ImageBusiness.setDefaultAvatarMedia(ivTopLeft);
            ImageBusiness.setDefaultMediaBlur(ivCover, 200);
        }
    }

    private void setupCountInfo() {
        int size = getData().size();
        if (size == 0) {
            tvCount.setVisibility(View.GONE);
            tvCount.setText("");
        } else {
            if (mediaInfo.isVideoList()) {
                tvCount.setVisibility(View.VISIBLE);
                if (size == 1)
                    tvCount.setText(getString(R.string.total_video, size));
                else
                    tvCount.setText(getString(R.string.total_videos, size));
            } else {
                tvCount.setVisibility(View.VISIBLE);
                if (size == 1)
                    tvCount.setText(getString(R.string.music_total_song, size));
                else
                    tvCount.setText(getString(R.string.music_total_songs, size));
            }
        }
    }

    private void setupGridRecycler(int spanCount, int resIdSpacing, boolean includeEdge) {
        if (recyclerView != null) {
            if (recyclerView.getItemDecorationCount() <= 0) {
                layoutManager = new CustomGridLayoutManager(mActivity, spanCount);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(new AlbumDetailGridItemDecoration(spanCount, mActivity
                        .getResources()
                        .getDimensionPixelOffset(resIdSpacing), includeEdge));
            }
            recyclerView.setAdapter(adapter);
        }
    }

    private void setupRecycler() {
        if (recyclerView != null) {
            if (recyclerView.getItemDecorationCount() <= 0) {
                layoutManager = new CustomLinearLayoutManager(mActivity);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(mActivity, R.drawable
                        .divider_default));
            }
            recyclerView.setAdapter(adapter);
        }
    }

    private List<AllModel> getData() {
        if (data == null) data = new ArrayList<>();
        return data;
    }

    private void clearData() {
        if (data == null) data = new ArrayList<>();
        else data.clear();
    }

    private void checkLoadMore(List<AllModel> result) {
        canLoadMore = result != null && !result.isEmpty();
    }

    private void refreshed() {
        isLoading = false;
        countError = 0;
        if (isRefresh) {
            isRefresh = false;
            currentPage = 1;
            clearData();
        }
    }

    private void loadingNoInfo() {
        if (refreshView != null && refreshView.isRefreshing()) {
            refreshView.setRefreshing(false);
        }
        if (loadingView != null) {
            loadingView.loadEmpty(mActivity.getString(R.string.playlist_was_deleted));
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
//        if (btnOption != null) btnOption.setVisibility(View.GONE);
        if (btnRandom != null) btnRandom.setVisibility(View.GONE);
        if (btnPlay != null) btnPlay.setVisibility(View.GONE);
    }

    private void loadingFinish() {
        if (refreshView != null && refreshView.isRefreshing()) {
            refreshView.setRefreshing(false);
        }
        if (getData().isEmpty()) {
            if (loadingView != null) {
                loadingView.loadEmpty();
            }
            if (recyclerView != null) {
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            if (loadingView != null) {
                loadingView.loadFinish();
            }
            if (recyclerView != null) {
                recyclerView.setVisibility(View.VISIBLE);
            }
        }
//        if (btnOption != null) btnOption.setVisibility(View.VISIBLE);
        if (btnRandom != null) btnRandom.setVisibility(View.VISIBLE);
        if (btnPlay != null) btnPlay.setVisibility(View.VISIBLE);
    }

    private void loadError() {
        refreshed();
        if (getData().isEmpty()) {
            if (refreshView != null && refreshView.isRefreshing()) {
                refreshView.setRefreshing(false);
            }
            if (loadingView != null) {
                loadingView.loadError();
                loadingView.setLoadingErrorListener(view -> doLoadData(true));
            }
            if (recyclerView != null) {
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            loadingFinish();
        }
    }

    private void doLoadData(boolean showLoading) {
        if (!isLoading) {
            if (showLoading && loadingView != null) {
                loadingView.loadBegin();
            }
            isLoading = true;
            if (isRefresh)
                currentPage = 1;
            if (mediaInfo != null && (mediaInfo.getId() > 0 || !TextUtils.isEmpty(mediaInfo.getIdentify()))) {
                new KeengApi().getPlaylistInfo(mediaInfo.getId(), mediaInfo.getIdentify(), mediaInfo.getType()
                        , response -> {
                            if (response != null && response.getData() != null) {
                                boolean isFavorite = mediaInfo.isFavorite();
                                int type = mediaInfo.getType();
                                mediaInfo = response.getData();
                                mediaInfo.setType(type);
                                mediaInfo.setFavorite(isFavorite);
                                if (mediaInfo.isVideoList()) {
                                    setupGridRecycler(2, R.dimen.padding_16, true);
                                } else {
                                    setupRecycler();
                                }
                                setupMediaInfo();
                                doAddResult(mediaInfo.getMediaList());
                                setupAvatar(true);
                                setupCountInfo();
                                mediaInfo.setMediaList(null);
                            } else {
                                loadingNoInfo();
                            }
                        }, error -> {
                            Log.e(TAG, error);
                            countError++;
                            if (countError < MAX_NUMBER_TO_RETRY) {
                                isLoading = false;
                                doLoadData(false);
                                return;
                            }
                            loadError();
                        }
                );
            } else {
                loadingNoInfo();
            }
        }
    }

    private void doAddResult(List<AllModel> result) {
        checkLoadMore(result);
        refreshed();
        if (result != null && !result.isEmpty()) {
            if (mediaInfo.isVideoList()) {
                ConvertHelper.convertData(result, MediaLogModel.SRC_PLAYLIST, Constants.TYPE_VIDEO);
            } else {
                ConvertHelper.convertData(result, MediaLogModel.SRC_PLAYLIST, Constants.TYPE_SONG);
            }
            getData().addAll(result);
            currentPage++;
        }
        if (adapter != null) adapter.notifyDataSetChanged();
        loadingFinish();
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        doLoadData(false);
    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            doLoadData(false);
        }
    }

    @Override
    public void onClickMedia(View view, int position) {
        if (mActivity == null || mActivity.isFinishing() || mediaInfo == null)
            return;
//        ViewUtil.lockClickView(view);
        onClickPlay(false, position);
    }

    @Override
    public void onClickOptionMedia(View view, AllModel item) {
        if (mActivity == null || mActivity.isFinishing() || item == null || mediaInfo == null)
            return;
//        ViewUtil.lockClickView(view);
        boolean canRemove = false;
        boolean isAddPlaylist = false;
        if (mediaInfo.getUser().isMyUser(mActivity)) {
            canRemove = true;
        } else {
            isAddPlaylist = true;
        }
        if (canRemove && mediaInfo.getListened() > Constants.MAX_LISTEN_NO_DELETE_PLAYLIST) {
            canRemove = false;
        }
        if (mediaInfo.isVideoList()) {
            mActivity.showPopupMore(BottomSheetData.getPopupOfVideo(item), item);
        } else {
            mActivity.showPopupMore(BottomSheetData.getPopupOfSong(item, mActivity.isPlayingMedia(), isAddPlaylist, false, canRemove, false), item);
        }
    }

    private void onClickPlay(boolean isShuffle, int position) {
        if (mActivity == null || mActivity.isFinishing() || mediaInfo == null || getData().isEmpty())
            return;
        if (mediaInfo.isVideoList()) {
            mActivity.setMediaToPlayVideo(getData().get(position));
        } else {
            PlayingList playingList = new PlayingList(getData(), mediaInfo.getType(), MediaLogModel.SRC_PLAYLIST);
            playingList.setName(mediaInfo.getName());
            playingList.setId(mediaInfo.getId());
            playingList.setSinger(mediaInfo.getSinger());
            if (isShuffle) {
                int size = getData().size();
                if (size > 0)
                    position = new Random().nextInt(size);
            }
            mActivity.setMediaPlayingAudioWithState(playingList, position, Constants.PLAY_MUSIC.REPEAT_All,
                    isShuffle ? Constants.PLAY_MUSIC.REPEAT_SUFF : Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
        }
    }

    @Override
    public void onDetach() {
        App.getInstance().cancelPendingRequests(KeengApi.GET_PLAYLIST_INFO);
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        if (mActivity == null || mActivity.isFinishing() || mediaInfo == null)
            return;
//        ViewUtil.lockClickView(view);
        switch (view.getId()) {
            case R.id.button_option: {
                showDialogOption(mediaInfo);
            }
            break;
            case R.id.button_shuffle: {
                onClickPlay(true, 0);
            }
            break;
            case R.id.button_play: {
                onClickPlay(false, 0);
            }
            break;
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0) {
            if (currentState != Constants.State.EXPANDED) {
                ivAvatar.setAlpha(1f);
                tvTitle.setAlpha(1f);
                tvDescription.setAlpha(1f);
                tvListenNo.setAlpha(1f);
                btnPlay.setVisibility(View.VISIBLE);
                tvTitleToolbar.setAlpha(0f);
            }
            currentState = Constants.State.EXPANDED;
        } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
            if (currentState != Constants.State.COLLAPSED) {
                if (mediaInfo != null) {
                    ivAvatar.setAlpha(0f);
                    tvTitle.setAlpha(0f);
                    tvDescription.setAlpha(0f);
                    tvListenNo.setAlpha(0f);
                    btnPlay.setVisibility(View.GONE);
                    tvTitleToolbar.setAlpha(1f);
                }
            }
            currentState = Constants.State.COLLAPSED;
        } else {
            if (currentState != Constants.State.IDLE) {
                ivAvatar.setAlpha(1f);
                tvTitle.setAlpha(1f);
                tvDescription.setAlpha(1f);
                tvListenNo.setAlpha(1f);
                btnPlay.setVisibility(View.GONE);
                tvTitleToolbar.setAlpha(0f);
            }
            currentState = Constants.State.IDLE;
        }

        if (appBarLayout != null) {
            float alpha = 1 - Math.abs(verticalOffset / (float) appBarLayout.getTotalScrollRange());
            ivAvatar.setAlpha(alpha);
            tvTitle.setAlpha(alpha);
            tvDescription.setAlpha(alpha);
            tvListenNo.setAlpha(alpha);
        }
    }

    @Override
    public void onClickSheet(CategoryModel item) {
        if (mActivity == null || mActivity.isFinishing() || item == null || mediaInfo == null)
            return;
        if (dialogOption != null)
            dialogOption.dismiss();
        if (item.getType() == BottomSheetData.SHARE) {
            if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                mActivity.showDialogLogin();
            else
                ShareUtils.openShareMenu(mActivity, item.getPlaylist());
        }
    }

    private void showDialogOption(PlayListModel item) {
        if (item == null)
            return;
        if (dialogOption != null)
            dialogOption.dismiss();
        dialogOption = new BottomDialog(mActivity);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_playlist, null);
        View layoutTitle = sheetView.findViewById(R.id.layout_title);
        layoutTitle.setVisibility(View.GONE);
        TextView tvTitle = sheetView.findViewById(R.id.tv_title);
        TextView tvSinger = sheetView.findViewById(R.id.tv_description);
        ImageView ivTopLeft = sheetView.findViewById(R.id.iv_top_left);
        ImageView ivTopRight = sheetView.findViewById(R.id.iv_top_right);
        ImageView ivBottomLeft = sheetView.findViewById(R.id.iv_bottom_left);
        ImageView ivBottomRight = sheetView.findViewById(R.id.iv_bottom_right);
        View viewRight = sheetView.findViewById(R.id.right_layout);
        ImageBusiness.setImagePlaylist(item.getListAvatar(false), ivTopLeft, ivTopRight, ivBottomRight, ivBottomLeft, viewRight);
        tvTitle.setText(item.getName());
        tvSinger.setText(item.getSinger());
        RecyclerView mRecycler = sheetView.findViewById(R.id.recycler);
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new CustomLinearLayoutManager(mActivity));
        BottomSheetAdapter adapter = new BottomSheetAdapter(mActivity, BottomSheetData.getPopupOfPlaylist(item), TAG, this);
        mRecycler.setAdapter(adapter);
        dialogOption.setContentView(sheetView);
        dialogOption.show();
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && mediaInfo == null)
            onRefresh();
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }
}

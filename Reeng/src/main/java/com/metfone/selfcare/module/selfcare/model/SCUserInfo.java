package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCUserInfo implements Serializable {
//        "name": "MYTEL",
//                "mainAcc": 805.43,
//                "proAcc": 0,
//                "dataPkgName": "SH70 ,MITE_TAL_10GB ,D10000 ,D5000 ,SH8 ,SH35 ,D3000",
//                "dataVolume": 9070

    @SerializedName("name")
    private String name;

    @SerializedName("mainAcc")
    private String mainAcc;

    @SerializedName("proAcc")
    private String proAcc;

    @SerializedName("dataPkgName")
    private String dataPkgName;

    @SerializedName("dataVolume")
    private String dataVolume;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainAcc() {
        return mainAcc;
    }

    public void setMainAcc(String mainAcc) {
        this.mainAcc = mainAcc;
    }

    public String getProAcc() {
        return proAcc;
    }

    public void setProAcc(String proAcc) {
        this.proAcc = proAcc;
    }

    public String getDataPkgName() {
        return dataPkgName;
    }

    public void setDataPkgName(String dataPkgName) {
        this.dataPkgName = dataPkgName;
    }

    public String getDataVolume() {
        return dataVolume;
    }

    public void setDataVolume(String dataVolume) {
        this.dataVolume = dataVolume;
    }

    @Override
    public String toString() {
        return "SCUserInfo{" +
                "name='" + name + '\'' +
                ", mainAcc='" + mainAcc + '\'' +
                ", proAcc='" + proAcc + '\'' +
                ", dataPkgName='" + dataPkgName + '\'' +
                ", dataVolume='" + dataVolume + '\'' +
                '}';
    }
}

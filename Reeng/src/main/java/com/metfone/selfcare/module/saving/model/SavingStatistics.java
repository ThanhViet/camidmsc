package com.metfone.selfcare.module.saving.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SavingStatistics implements Serializable {
    @SerializedName("callout_saving")
    private String callOutSaving = "";
    @SerializedName("smsout_saving")
    private String smsOutSaving = "";
    @SerializedName("total_saving")
    private String totalSaving = "";
    @SerializedName("range_date")
    private String rangeDate = "";
    @SerializedName("total_saving_int")
    private long totalSavingInt = 0;
    @SerializedName("smsout_saving_int")
    private long smsOutSavingInt = 0;
    @SerializedName("callout_saving_int")
    private long callOutSavingInt = 0;
    //@SerializedName("data_saving")
    private String dataSaving = "";
    //@SerializedName("data_saving_int")
    private long dataSavingInt = 0;
    private boolean showWithAnimation;

    public String getCallOutSaving() {
        return callOutSaving;
    }

    public void setCallOutSaving(String callOutSaving) {
        this.callOutSaving = callOutSaving;
    }

    public String getSmsOutSaving() {
        return smsOutSaving;
    }

    public void setSmsOutSaving(String smsOutSaving) {
        this.smsOutSaving = smsOutSaving;
    }

    public String getTotalSaving() {
        return totalSaving;
    }

    public void setTotalSaving(String totalSaving) {
        this.totalSaving = totalSaving;
    }

    public String getRangeDate() {
        return rangeDate;
    }

    public void setRangeDate(String rangeDate) {
        this.rangeDate = rangeDate;
    }

    public long getTotalSavingInt() {
        return totalSavingInt;
    }

    public void setTotalSavingInt(long totalSavingInt) {
        this.totalSavingInt = totalSavingInt;
    }

    public long getSmsOutSavingInt() {
        return smsOutSavingInt;
    }

    public void setSmsOutSavingInt(long smsOutSavingInt) {
        this.smsOutSavingInt = smsOutSavingInt;
    }

    public long getCallOutSavingInt() {
        return callOutSavingInt;
    }

    public void setCallOutSavingInt(long callOutSavingInt) {
        this.callOutSavingInt = callOutSavingInt;
    }

    public boolean isShowWithAnimation() {
        return showWithAnimation;
    }

    public void setShowWithAnimation(boolean showWithAnimation) {
        this.showWithAnimation = showWithAnimation;
    }

    public String getDataSaving() {
        return dataSaving;
    }

    public void setDataSaving(String dataSaving) {
        this.dataSaving = dataSaving;
    }

    public long getDataSavingInt() {
        return dataSavingInt;
    }

    public void setDataSavingInt(long dataSavingInt) {
        this.dataSavingInt = dataSavingInt;
    }

    @Override
    public String toString() {
        return "{" +
                "callOutSaving='" + callOutSaving + '\'' +
                ", smsOutSaving='" + smsOutSaving + '\'' +
                ", totalSaving='" + totalSaving + '\'' +
                ", rangeDate='" + rangeDate + '\'' +
                '}';
    }
}

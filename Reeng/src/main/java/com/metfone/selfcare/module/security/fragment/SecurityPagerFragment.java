/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/25
 */

package com.metfone.selfcare.module.security.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.security.activity.SecurityActivity;
import com.metfone.selfcare.module.security.event.SecurityDetailEvent;
import com.metfone.selfcare.module.security.event.SecurityPageEvent;
import com.metfone.selfcare.module.security.helper.SecurityHelper;
import com.metfone.selfcare.ui.DeativatableViewPager;
import com.metfone.selfcare.ui.dialog.DialogEditText;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SecurityPagerFragment extends BaseFragment implements ViewPager.OnPageChangeListener {
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    DeativatableViewPager viewPager;

    @BindView(R.id.fab_action)
    FloatingActionButton fabAction;

    @BindView(R.id.header_tab_event)
    View headerTabEvent;
    @BindView(R.id.iv_checkbox)
    ImageView ivCheckBox;
    @BindView(R.id.tv_checkbox)
    TextView tvCheckBox;

    private Unbinder unbinder;
    private int type;
    private boolean isSelectMode;
    private int currentTab;
    private boolean isSelectAll;

    public static SecurityPagerFragment newInstance() {
        Bundle args = new Bundle();
        SecurityPagerFragment fragment = new SecurityPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "SecurityPagerFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_security_tab;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            type = bundle.getInt(Constants.KEY_TYPE);
        }
        setTitle();
        initView();
    }

    private void setTitle() {
        if (mActivity instanceof SecurityActivity) {
            switch (type) {
                case Constants.TAB_SECURITY_CENTER:
                    ((SecurityActivity) mActivity).setTitle(R.string.security_center);
                    ((SecurityActivity) mActivity).showLine(true);
                    break;
                case Constants.TAB_SECURITY_SPAM:
                    ((SecurityActivity) mActivity).setTitle(R.string.security_spam_sms);
                    ((SecurityActivity) mActivity).showLine(false);
                    break;
                case Constants.TAB_SECURITY_FIREWALL:
                    ((SecurityActivity) mActivity).setTitle(R.string.security_firewall);
                    ((SecurityActivity) mActivity).showLine(false);
                    break;
                default:
                    break;
            }
        }
    }

    private void initView() {
        viewPager.setAdapter(createPagerAdapter());
        viewPager.setOffscreenPageLimit(3);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(viewPager);
//        tabLayout.addOnTabSelectedListener(this);
    }

    private PagerAdapter createPagerAdapter() {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(mActivity);
        switch (type) {
            case Constants.TAB_SECURITY_CENTER: {

            }
            break;
            case Constants.TAB_SECURITY_SPAM: {
                Bundle bundle;
                bundle = new Bundle();
                bundle.putInt(Constants.KEY_TYPE, Constants.TAB_SECURITY_SPAM_SMS);
                creator.add(FragmentPagerItem.of(getString(R.string.security_spam_sms_2), SecurityDetailFragment.class, bundle));

                bundle = new Bundle();
                bundle.putInt(Constants.KEY_TYPE, Constants.TAB_SECURITY_SPAM_BLOCK);
                creator.add(FragmentPagerItem.of(getString(R.string.security_block), SecurityDetailFragment.class, bundle));

                bundle = new Bundle();
                bundle.putInt(Constants.KEY_TYPE, Constants.TAB_SECURITY_SPAM_IGNORE);
                creator.add(FragmentPagerItem.of(getString(R.string.security_ignore), SecurityDetailFragment.class, bundle));

            }
            break;
            case Constants.TAB_SECURITY_FIREWALL: {
                Bundle bundle;
                bundle = new Bundle();
                bundle.putInt(Constants.KEY_TYPE, Constants.TAB_SECURITY_FIREWALL_SETTING);
                creator.add(FragmentPagerItem.of(getString(R.string.security_setting), FirewallSettingFragment.class, bundle));

                bundle = new Bundle();
                bundle.putInt(Constants.KEY_TYPE, Constants.TAB_SECURITY_FIREWALL_IGNORE);
                creator.add(FragmentPagerItem.of(getString(R.string.security_ignore), SecurityDetailFragment.class, bundle));

                bundle = new Bundle();
                bundle.putInt(Constants.KEY_TYPE, Constants.TAB_SECURITY_FIREWALL_HISTORY);
                creator.add(FragmentPagerItem.of(getString(R.string.security_history), SecurityDetailFragment.class, bundle));
            }
            break;
            default:
                break;
        }
        return new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentTab = position;
        if (appBarLayout != null) {
            appBarLayout.setExpanded(true);
        }
        if (type == Constants.TAB_SECURITY_FIREWALL) {
            if (position == 1)
                fabAction.show();
            else
                fabAction.hide();
        } else if (type == Constants.TAB_SECURITY_SPAM) {
            if (position == 1 || position == 2)
                fabAction.show();
            else
                fabAction.hide();
        } else {
            fabAction.hide();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SecurityPageEvent event) {
        int tabId = event.getTabId();
        if (event.isSelectTab()) {
            try {
                viewPager.setCurrentItem(event.getPositionTab());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ((tabId == Constants.TAB_SECURITY_FIREWALL_IGNORE && type == Constants.TAB_SECURITY_FIREWALL && currentTab == 1)
                || (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY && type == Constants.TAB_SECURITY_FIREWALL && currentTab == 2)
                || (tabId == Constants.TAB_SECURITY_SPAM_SMS && type == Constants.TAB_SECURITY_SPAM && currentTab == 0)
                || (tabId == Constants.TAB_SECURITY_SPAM_BLOCK && type == Constants.TAB_SECURITY_SPAM && currentTab == 1)
                || (tabId == Constants.TAB_SECURITY_SPAM_IGNORE && type == Constants.TAB_SECURITY_SPAM && currentTab == 2)
                ) {
            if (event.isSelectMode()) {
                if (!isSelectMode)
                    setSelectMode(true);
                setStateCheckBoxAll(event.getSizeSelected(), event.isSelectAll());
            } else {
                if (isSelectMode)
                    setSelectMode(false);
                setStateCheckBoxAll(0, false);
            }
        } else {
            setSelectMode(false);
            setStateCheckBoxAll(0, false);
        }
    }

    private void setSelectMode(boolean selectMode) {
        isSelectMode = selectMode;
        if (viewPager != null) {
            viewPager.setPagingEnable(!selectMode);
        }

        if (fabAction != null) {
            fabAction.setVisibility(selectMode ? View.GONE : View.VISIBLE);
        }

        if (headerTabEvent != null) {
            headerTabEvent.setVisibility(selectMode ? View.VISIBLE : View.GONE);
            startHeaderTabAnimation(selectMode);
        }

        enableTabClick(selectMode);
    }

    private void enableTabClick(final boolean selectMode) {
        if (tabLayout == null) return;
        tabLayout.setEnabled(!selectMode);
        tabLayout.setAlpha(selectMode ? 0.6f : 1.0f);
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return selectMode;
                }
            });
        }
    }

    private void setStateCheckBoxAll(int sizeChecked, boolean selectAll) {
        if (ivCheckBox == null || tvCheckBox == null) return;
        isSelectAll = (sizeChecked > 0) && selectAll;
        if (selectAll) {
            ivCheckBox.setImageResource(R.drawable.ic_cb_selected);
        } else {
            ivCheckBox.setImageResource(R.drawable.ic_cb_unselected);
        }
        tvCheckBox.setText(String.format(mActivity.getString(R.string.ab_delete_title), sizeChecked));
    }

    private void startHeaderTabAnimation(boolean isSelectMode) {
        if (mActivity == null || headerTabEvent == null) return;
        if (headerTabEvent.getAnimation() != null) {
            headerTabEvent.getAnimation().cancel();
            headerTabEvent.clearAnimation();
        }
        if (isSelectMode) {
            final int marginLeft = mActivity.getResources().getDimensionPixelSize(R.dimen.margin_more_content_30);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) headerTabEvent.getLayoutParams();
            layoutParams.leftMargin = -marginLeft;
            headerTabEvent.setLayoutParams(layoutParams);
            headerTabEvent.setAlpha(0.0f);
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    super.applyTransformation(interpolatedTime, t);
                    int marginLeftTime = (int) (interpolatedTime * marginLeft - marginLeft);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) headerTabEvent.getLayoutParams();
                    layoutParams.leftMargin = marginLeftTime;
                    headerTabEvent.setLayoutParams(layoutParams);
                    headerTabEvent.setAlpha(interpolatedTime);
                }

                @Override
                public void cancel() {
                    super.cancel();
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) headerTabEvent.getLayoutParams();
                    layoutParams.leftMargin = 0;
                    headerTabEvent.setLayoutParams(layoutParams);
                    headerTabEvent.setAlpha(1.0f);
                }
            };
            animation.setDuration(400);
            headerTabEvent.startAnimation(animation);
        }
    }

    @OnClick(R.id.fab_action)
    public void clickFabAction() {
        if (mActivity != null && !mActivity.isFinishing()) {
            String label = "";
            String hint = "";
            if (type == Constants.TAB_SECURITY_FIREWALL) {
                label = mActivity.getString(R.string.security_add_number_to_ignore_list);
                hint = mActivity.getString(R.string.security_add_number_to_ignore_list_hint);
            } else if (type == Constants.TAB_SECURITY_SPAM) {
                if (currentTab == 1) {
                    label = mActivity.getString(R.string.security_add_number_to_block_list);
                    hint = mActivity.getString(R.string.security_add_number_to_block_list_hint);
                } else if (currentTab == 2) {
                    label = mActivity.getString(R.string.security_add_number_to_ignore_list);
                    hint = mActivity.getString(R.string.security_add_number_to_ignore_list_hint);
                }
            }
            if (!TextUtils.isEmpty(hint)) {
                final DialogEditText dialog = new DialogEditText(mActivity, true);
                dialog.setCheckEnable(false);
                dialog.setLabel(label);
                dialog.setTextHint(hint);
                dialog.setInputType(InputType.TYPE_CLASS_PHONE);
                dialog.setNegativeLabel(mActivity.getString(R.string.cancel));
                dialog.setPositiveLabel(mActivity.getString(R.string.ok));
                dialog.setMaxLength(-1);
                dialog.setAutoDismiss(false);
                dialog.setPositiveListener(new PositiveListener<String>() {
                    @Override
                    public void onPositive(String result) {
                        if (type == Constants.TAB_SECURITY_FIREWALL) {
                            if (TextUtils.isEmpty(result)) {

                            } else if (TextUtils.isDigitsOnly(result) && result.length() <= 16) {
                                SecurityHelper.updateFirewallWhiteList(mActivity, result, true);
                                dialog.dismiss();
                            } else {
                                mActivity.showToast(R.string.security_toast_invalid_number_firewall);
                            }
                        } else if (type == Constants.TAB_SECURITY_SPAM) {
                            if (TextUtils.isEmpty(result)) {

                            } else if (SecurityHelper.isValidPhoneNumber(result)) {
                                if (currentTab == 1) {
                                    SecurityHelper.updateSpamBlackList(mActivity, result, true);
                                } else if (currentTab == 2) {
                                    SecurityHelper.updateSpamWhiteList(mActivity, result, true);
                                }
                                dialog.dismiss();
                            } else {
                                mActivity.showToast(R.string.phone_number_invalid);
                            }
                        }
                    }
                });
                dialog.setNegativeListener(new NegativeListener() {
                    @Override
                    public void onNegative(Object result) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }
    }

    @OnClick(R.id.iv_checkbox)
    public void clickCheckbox() {
        if (mActivity != null && !mActivity.isFinishing()) {
            SecurityDetailEvent event = new SecurityDetailEvent();
            if (type == Constants.TAB_SECURITY_FIREWALL && currentTab == 1)
                event.setTabId(Constants.TAB_SECURITY_FIREWALL_IGNORE);
            else if (type == Constants.TAB_SECURITY_FIREWALL && currentTab == 2)
                event.setTabId(Constants.TAB_SECURITY_FIREWALL_HISTORY);
            else if (type == Constants.TAB_SECURITY_SPAM && currentTab == 0)
                event.setTabId(Constants.TAB_SECURITY_SPAM_SMS);
            else if (type == Constants.TAB_SECURITY_SPAM && currentTab == 1)
                event.setTabId(Constants.TAB_SECURITY_SPAM_BLOCK);
            else if (type == Constants.TAB_SECURITY_SPAM && currentTab == 2)
                event.setTabId(Constants.TAB_SECURITY_SPAM_IGNORE);
            if (isSelectAll) event.setDeSelectAll(true);
            else event.setSelectAll(true);
            EventBus.getDefault().post(event);
        }
    }

    @OnClick(R.id.button_cancel_checkbox)
    public void clickCancelCheckbox() {
        if (mActivity != null && !mActivity.isFinishing()) {
            SecurityDetailEvent event = new SecurityDetailEvent();
            if (type == Constants.TAB_SECURITY_FIREWALL && currentTab == 1)
                event.setTabId(Constants.TAB_SECURITY_FIREWALL_IGNORE);
            else if (type == Constants.TAB_SECURITY_FIREWALL && currentTab == 2)
                event.setTabId(Constants.TAB_SECURITY_FIREWALL_HISTORY);
            else if (type == Constants.TAB_SECURITY_SPAM && currentTab == 0)
                event.setTabId(Constants.TAB_SECURITY_SPAM_SMS);
            else if (type == Constants.TAB_SECURITY_SPAM && currentTab == 1)
                event.setTabId(Constants.TAB_SECURITY_SPAM_BLOCK);
            else if (type == Constants.TAB_SECURITY_SPAM && currentTab == 2)
                event.setTabId(Constants.TAB_SECURITY_SPAM_IGNORE);
            event.setCancelSelect(true);
            EventBus.getDefault().post(event);
        }
    }

}

package com.metfone.selfcare.module.keeng.model;

import com.metfone.selfcare.module.keeng.utils.Utilities;

import java.io.Serializable;

/**
 * Created by namnh40 on 2/10/2017.
 */

public class ContactInfo implements Serializable {
    private static final long serialVersionUID = 8696522447450461026L;
    private long id;
    private long contactId;
    private long userId;
    private String msisdn;
    private String name;
    private String image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getMsisdn() {
        if (msisdn == null)
            msisdn = "";
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        if (name == null)
            name = "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isUserKeeng() {
        return userId > 0;
    }

    public String getNameUser() {
        return Utilities.getUsername(name, msisdn);
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", contactId=" + contactId +
                ", userId=" + userId +
                ", msisdn='" + msisdn + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

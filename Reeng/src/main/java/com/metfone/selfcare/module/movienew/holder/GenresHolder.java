/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movienew.holder;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.movienew.listener.OnClickGenres;
import com.metfone.selfcare.module.movienew.model.Category;

import butterknife.BindView;

public class GenresHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.txt_title)
    @Nullable
    TextView tvTitle;

    @BindView(R.id.img_genres)
    @Nullable
    ImageView imgGenres;

    private OnClickGenres listener;
    private View rootView;
    private Activity activity;


    public GenresHolder(View view, Activity activity, final OnClickGenres listener) {
        super(view);
        this.activity = activity;
        this.rootView = view;
        this.listener = listener;
    }

    @Override
    public void bindData(Object item, int position) {
        Category genres = (Category) item;
        if (tvTitle != null) tvTitle.setText(genres.getCategoryName());
        if (genres.isChecked()) {
            imgGenres.setAlpha(getMaxAlpha(1.0f));
        } else {
            imgGenres.setAlpha(getMaxAlpha(0.2f));
        }
        Glide.with(activity)
                .load(genres.getUrlImages()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                .into(imgGenres);
        imgGenres.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                genres.setChecked(!genres.isChecked());
                listener.onClick(item, position, genres.isChecked());
            }
        });
    }

    private float getMaxAlpha(float alpha) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            return alpha * 255;
        }
        return alpha;
    }
}

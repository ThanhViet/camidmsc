package com.metfone.selfcare.module.metfoneplus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.metfoneplus.request.WsSearchNumberToBuyRequest;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.SneakyThrows;

public class MPFragmentOrderNumber extends MPBaseFragment {
    private static final String NUMBER_KEY = "number";
    private static final String DURATION_KEY = "duration";
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    private AvailableNumber availableNumber;
    UserInfoBusiness userInfoBusiness;
    private int commitmentDuration = 0;
    public MPFragmentOrderNumber() {
        // Required empty public constructor
    }
    public static MPFragmentOrderNumber newInstance(AvailableNumber availableNumber, int commitmentDuration) {
        Bundle args = new Bundle();
        MPFragmentOrderNumber fragment = new MPFragmentOrderNumber();
        args.putSerializable(NUMBER_KEY, availableNumber);
        args.putInt(DURATION_KEY, commitmentDuration);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            availableNumber = (AvailableNumber) getArguments().getSerializable(NUMBER_KEY);
            commitmentDuration = getArguments().getInt(DURATION_KEY);
        }

    }

    @SneakyThrows
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_number, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        Utilities.adaptViewForInserts(view);
        userInfoBusiness = new UserInfoBusiness(getActivity());
        action_bar_title.setText(R.string.register_with_current_sim);
        return view;
    }
    @OnClick({R.id.action_bar_back})
    public void onBack() {
        popBackStackFragment();
    }

    @OnClick({R.id.btnBuyFullPrice})
    public void onBtnBuyFullPrice() {
        gotoMPApplyForYouCurrentSimFragment(availableNumber, 1, commitmentDuration);
    }

    @OnClick({R.id.btnBuyCommitment})
    public void onBtnBuyCommitment() {
        gotoMPApplyForYouCurrentSimFragment(availableNumber, 2, commitmentDuration);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_order_number;
    }


}

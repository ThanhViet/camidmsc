package com.metfone.selfcare.module.metfoneplus.search.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.search.adapter.AvailableNumberAdapter;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.WsSearchNumberToBuyRequest;
import com.metfone.selfcare.network.metfoneplus.response.WsSearchNumberToBuyResponse;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class ListNumberFragment extends MPBaseFragment implements AvailableNumberAdapter.AvailableNumberListener {
    private static final String REQUEST_KEY = "request";
    private static final String RESULT_KEY = "result";
    private static final String DURATION_KEY = "duration";
    private static final String FEE_KEY = "fee";

    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.recycler_view_number)
    RecyclerView recycler_view_number;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    private boolean isLoading = false;
    private boolean hasReached = false;
    private ArrayList<AvailableNumber> availableNumbers = null;
    private AvailableNumberAdapter adapter;
    private WsSearchNumberToBuyRequest request;
    private LinearLayoutManager layoutManager;
    private int commitmentDuration = 0;
    private int fee = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_number;
    }

    public static ListNumberFragment newInstance(ArrayList<AvailableNumber> result, WsSearchNumberToBuyRequest request, int commitmentDuration, int fee) {
        Bundle args = new Bundle();
        args.putSerializable(REQUEST_KEY, request);
        args.putSerializable(RESULT_KEY, result);
        args.putInt(DURATION_KEY, commitmentDuration);
        args.putInt(FEE_KEY, fee);
        ListNumberFragment fragment = new ListNumberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            request = (WsSearchNumberToBuyRequest) getArguments().getSerializable(REQUEST_KEY);
            availableNumbers = (ArrayList<AvailableNumber>) getArguments().getSerializable(RESULT_KEY);
            commitmentDuration = getArguments().getInt(DURATION_KEY);
            fee = getArguments().getInt(FEE_KEY);
        }
        if (availableNumbers != null) {
            loadDataSearch(request);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(view);
        action_bar_title.setText(R.string.list_of_available_number);
        adapter = new AvailableNumberAdapter(getContext(), availableNumbers, this);
        layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recycler_view_number.setLayoutManager(layoutManager);
        recycler_view_number.setAdapter(adapter);
        BaseAdapter.setRecyclerViewLoadMore(recycler_view_number, new BaseAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isLoading && !hasReached) {
                    request.getWsRequest().setPageNo(request.getWsRequest().getPageNo() + 1);
                    ListNumberFragment.this.loadDataSearch(request);
                }
            }
        });
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        popBackStackFragment();
    }

    private void loadDataSearch(WsSearchNumberToBuyRequest request) {
        if (isLoading) {
            return;
        }
        isLoading = true;
        if (pbLoading != null) {
            pbLoading.setVisibility(View.VISIBLE);
        }
        MetfonePlusClient.getInstance().searchNumberToBuy(request, new ApiCallback<WsSearchNumberToBuyResponse>() {
            @Override
            public void onResponse(Response<WsSearchNumberToBuyResponse> response) {
                if (response != null
                        && response.body() != null
                        && response.body().getResult() != null) {
                    if (!response.body().getResult().getErrorCode().equals("0")) {
                        ToastUtils.showToast(getContext(), response.body().getResult().getMessage());
                        hasReached = true;
                    } else if (response.body().getResult().getWsResponse() != null
                            && response.body().getResult().getWsResponse().getLstNumberToBuy() != null) {
                        if (availableNumbers == null) {
                            availableNumbers = new ArrayList<>();
                        }
                        List<AvailableNumber> result = response.body().getResult().getWsResponse().getLstNumberToBuy();
                        for (AvailableNumber availableNumber : result) {
                            availableNumbers.add(availableNumber);
                        }
                        if (result.size() < BuyPhoneNumberFragment.PAGE_SIZE) {
                            hasReached = true;
                        }
                        commitmentDuration = (int) Float.parseFloat(response.body().getResult().getWsResponse().getCommitDuration());
                        adapter.notifyDataSetChanged();
                    }
                }
                isLoading = false;
                if (pbLoading != null) {
                    pbLoading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable error) {
                isLoading = false;
                if (pbLoading != null) {
                    pbLoading.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void registerNumber(AvailableNumber availableNumber) {
        gotoDetailPhoneNumberFragment(availableNumber, commitmentDuration, fee);
    }
}

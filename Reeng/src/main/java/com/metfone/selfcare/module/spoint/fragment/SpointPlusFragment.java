package com.metfone.selfcare.module.spoint.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class SpointPlusFragment extends Fragment {
    public static SpointPlusFragment newInstance() {
        Bundle args = new Bundle();
        SpointPlusFragment fragment = new SpointPlusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spoint, container, false);
        initView(view);
        loadData();
        initEvent();
        return view;
    }

    private void initView(View view) {

    }

    private void loadData() {

    }

    private void initEvent() {

    }

}

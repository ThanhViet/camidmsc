package com.metfone.selfcare.module.metfoneplus.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.addAccount.AddAccountFtthUserResponse;
import com.metfone.selfcare.model.camid.TypeComplaint;
import com.metfone.selfcare.module.metfoneplus.activity.AddFeedbackActivity;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.NumberPickerBottomSheetDialogFragment;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.WsSubmitComplaintMyMetfoneRequest;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComTypeResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubmitComplaintMyMetfoneResponse;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.metfone.selfcare.helper.Constants.PERMISSION.PERMISSION_REQUEST_LOCATION;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPFeedbackInternetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPFeedbackInternetFragment extends MPBaseFragment {
    public static String TAG = MPFeedbackInternetFragment.class.getSimpleName();
    public static String SERVICE_TYPE = "2";
    public static String ADD_INTERNET = "add_internet";

    @BindView(R.id.layout_content)
    ScrollView mLayoutContent;
    @BindView(R.id.m_p_feedback_us_error_type)
    AppCompatTextView mErrorTypeName;
    @BindView(R.id.m_p_feedback_us_error_phone)
    EditText mFeedbackErrorPhone;
    @BindView(R.id.m_p_feedback_us_your_phone)
    EditText mFeedbackYourPhone;
    @BindView(R.id.m_p_feedback_us_your_feedback)
    AppCompatEditText mFeedbackYourFeedback;
    @BindView(R.id.layoutParent)
    CoordinatorLayout layoutParent;

    @BindView(R.id.m_p_feedback_us_txt_title_upload_photo)
    AppCompatTextView mFeedbackTxtTitleUploadPhoto;
    @BindView(R.id.layout_upload_photo)
    RelativeLayout mFeedbackLayoutUploadPhoto;
    @BindView(R.id.layout_upload_photo_with_content)
    RelativeLayout mFeedbackLayoutUploadPhotoWithContent;
    @BindView(R.id.m_p_feedback_us_img_delete_photo)
    AppCompatImageView mFeedbackImgDeletePhoto;

    private AddFeedbackActivity activity;
    private FragmentManager mFragmentManager;
    private String mImageEncodeBase64 = "";
    private List<TypeComplaint> mComplaintList;
    private TypeComplaint mTypeComplaintSelected = null;
    private boolean mIsBottomSheetShowing = false;
    private int mPositionSelected = 0;
    private String mComptemplate;

    public MPFeedbackInternetFragment() {
        // Required empty public constructor
    }

    public static MPFeedbackInternetFragment newInstance() {
        MPFeedbackInternetFragment fragment = new MPFeedbackInternetFragment();
        return fragment;
    }

    private static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(Objects.requireNonNull(drawable))).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(Objects.requireNonNull(drawable).getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_feedback_internet;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (AddFeedbackActivity) context;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (AddFeedbackActivity) getActivity();

        Utilities.adaptViewForInsertBottom(layoutParent);
        setupUI(layoutParent);
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.GONE);
        }
        for (ServicesModel service: activity.servicesModelList) {
            if (service.getService_id() == 2 && service.getPhone_number()!=null){
                activity.account=service.getPhone_number();
                break;
            }
        }
        mFeedbackErrorPhone.setText(activity.account);
        mLayoutContent.setVisibility(View.VISIBLE);


        handleUploadPhoto(false, "");

        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_LOCATION);
        } else {
            getUserInformation();
        }
    }

    @OnClick(R.id.m_p_feedback_us_send_feedback)
    void sendFeedback() {

        wsSubmitComplaintMyMetfone(mTypeComplaintSelected);
    }

    public void getUserInformation() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.getFtthAccount(new ApiCallback<BaseResponse<ArrayList<AddAccountFtthUserResponse>>>() {
            @Override
            public void onResponse(Response<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> response) {
                if (response.body() != null) {
                    if (Integer.valueOf(response.body().getCode()) == 0) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            String account = response.body().getData().get(0).getPhoneNumber();
                            account = response.body().getData().get(0).getPhoneNumber();
                            String contractPoint = response.body().getData().get(0).getContractPoint();
                            activity.contractPoint=contractPoint;
                            mFeedbackYourPhone.setText(activity.contractPoint);
                            getListComType(account);
                        }
                        else
                        {
                            mLayoutContent.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    /**
     * Get content for complaint spinner
     */
    private void getListComType(String account) {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        try {
            new MetfonePlusClient().wsGetListComType(account, new MPApiCallback<WsGetComTypeResponse>() {
                @Override
                public void onResponse(Response<WsGetComTypeResponse> response) {
                    if (response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null) {
                        if ("0".equals(response.body().getResult().getErrorCode())) {
                            mComplaintList = response.body().getResult().getWsResponse().getTypeComplaints();
                            if (mComplaintList != null) {
                                mTypeComplaintSelected = mComplaintList.get(mPositionSelected);
                                mErrorTypeName.setText(mTypeComplaintSelected.getName());
                                mComptemplate = mTypeComplaintSelected.getCompTemplate();
                                mLayoutContent.setVisibility(View.VISIBLE);
                            }
                        } else {
                            handleShowErrorDialog(response);
                        }
                    }
                    mParentActivity.hideLoadingDialog();
                }

                @Override
                public void onError(Throwable error) {
                    mParentActivity.hideLoadingDialog();
                }
            });
        } catch (Exception e) {
            mParentActivity.hideLoadingDialog();
            com.metfone.selfcare.util.Log.e(TAG, "Error: ", e);
        }

    }

    private void wsSubmitComplaintMyMetfone(TypeComplaint typeComplaint) {
        WsSubmitComplaintMyMetfoneRequest request = new WsSubmitComplaintMyMetfoneRequest();

        String yourFeedback = mFeedbackYourFeedback.getText().toString().trim();
        String yourComplainerPhone = mFeedbackYourPhone.getText().toString().trim().replace(" ","");
        String errorComplainerPhone = mFeedbackErrorPhone.getText().toString().trim().replace(" ","");
        if (TextUtils.isEmpty(yourFeedback)){
            ToastUtils.showToast(mParentActivity, getString(R.string.error_feedback_input_invalid));
            return;
        }
        if (TextUtils.isEmpty(yourComplainerPhone)){
            ToastUtils.showToast(mParentActivity, getString(R.string.error_contact_invalid));
            return;
        }
        if (TextUtils.isEmpty(errorComplainerPhone)){
            ToastUtils.showToast(mParentActivity, getString(R.string.error_account_invalid));
            return;
        }
        if (typeComplaint==null){
            ToastUtils.showToast(mParentActivity, getString(R.string.error_account_invalid));
            return;
        }

//        boolean isChecked = false;
//        for (ServicesModel service: activity.servicesModelList) {
//            if (service.getService_id() == 2){
//                if (service.getPhone_number().equals(errorComplainerPhone)){
//                    isChecked = true;
//                } else if (errorComplainerPhone.startsWith("0") && errorComplainerPhone.substring(1).equals(service.getPhone_number())){
//                    isChecked = true;
//                } else if (errorComplainerPhone.startsWith("855") && errorComplainerPhone.substring(3).equals(service.getPhone_number())){
//                    isChecked = true;
//                }
//            }
//        }
//        if (!isChecked){
//            ToastUtils.showToast(mParentActivity, "Error Number Invalid");
//            return;
//        }

        mParentActivity.showLoadingDialog("", R.string.waiting);
        String compContent = request.createCompContent(typeComplaint.getCompTemplate(), "", "", yourFeedback, "");
        try {

            new MetfonePlusClient().wsSubmitComplaintMyMetfone(SERVICE_TYPE, "", mImageEncodeBase64, compContent, String.valueOf(typeComplaint.getCompTypeId()), yourComplainerPhone,errorComplainerPhone,
                    new MPApiCallback<WsSubmitComplaintMyMetfoneResponse>() {
                        @Override
                        public void onResponse(Response<WsSubmitComplaintMyMetfoneResponse> response) {
                            mParentActivity.hideLoadingDialog();
                            if (response != null && response.body() != null && response.code() == 200 && response.body().getResult() != null) {
                                if (response.body().getResult().getErrorCode().equals("0")) {
                                    MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                                            .setTitle(R.string.m_p_add_feedback_us_send_notification_title)
                                            .setContent(R.string.m_p_add_feedback_us_send_notification_content)
                                            .setLottieAssertName("lottie/img_dialog_women_success.json")
                                            .setTimeDelayAutoDismiss(3000)
                                            .build();
                                    setValueAdd(true);
                                    dialogSuccess.show(getChildFragmentManager(), null);
                                    dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
                                        @Override
                                        public void onDialogDismiss() {
                                            if (mParentActivity instanceof AddFeedbackActivity) {
                                                mParentActivity.finish();
                                                return;
                                            }
                                        }
                                    });
                                } else {
                                    ToastUtils.showToast(mParentActivity, response.body().getResult().getMessage());
                                }
                            } else {
                                Toast.makeText(getContext(), R.string.service_error, Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onError(Throwable error) {
                            mParentActivity.hideLoadingDialog();
                            Toast.makeText(getContext(), R.string.service_error, Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {
            mParentActivity.hideLoadingDialog();
            com.metfone.selfcare.util.Log.e(TAG, "Error: ", e);
        }
    }

    private void handleShowErrorDialog(Response<WsGetComTypeResponse> response) {
        String message = response.body().getResult().getUserMsg();
        if (message == null) {
            message = response.body().getResult().getMessage();
            if (message == null) {
                message = getString(R.string.m_p_no_data);
            }
        }
        MPDialogFragment errorDialog = new MPDialogFragment.Builder()
                .setTitle(R.string.m_p_dialog_error_title)
                .setContent(message)
                .setCancelableOnTouchOutside(true)
                .build();
        errorDialog.show(getChildFragmentManager(), null);

        errorDialog.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onDialogDismiss() {
                popBackStackFragment();
            }
        });
    }

    public void setupUI(View view) {

        if (!(view instanceof CoordinatorLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    @OnClick({R.id.m_p_feedback_us_error_type})
    void openSelectionType() {
        showSelectionTypeBottomSheet();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void showSelectionTypeBottomSheet() {
        if (!isAdded() || mComplaintList == null || mComplaintList.size() == 0 || mIsBottomSheetShowing) {
            String yourComplainerPhone = mFeedbackErrorPhone.getText().toString().trim().replace(" ","");
            getListComType(yourComplainerPhone);
            return;
        }

        List<String> selectionTypeStrings = new ArrayList<>();
        for (TypeComplaint t : mComplaintList) {
            selectionTypeStrings.add(t.getName());
        }

        String[] value = selectionTypeStrings.toArray(new String[0]);
        String title = getString(R.string.m_p_add_feedback_us_selection_type);
        NumberPickerBottomSheetDialogFragment changePhoneBS = NumberPickerBottomSheetDialogFragment.newInstance(title, value, mPositionSelected);
        changePhoneBS.setOnNumberPickerBottomSheetOnClick(new NumberPickerBottomSheetDialogFragment.OnNumberPickerBottomSheetOnClick() {
            @Override
            public void onDone(String valueSelected, int position) {
                android.util.Log.e(TAG, "onDone: valueSelected = " + valueSelected + " -- " + mComplaintList.get(position).toString());
                mPositionSelected = position;
                mTypeComplaintSelected = mComplaintList.get(position);
                mErrorTypeName.setText(mTypeComplaintSelected.getName());
            }

            @Override
            public void onDismiss() {
                mIsBottomSheetShowing = false;
            }
        });
        mIsBottomSheetShowing = true;
        changePhoneBS.show(mParentActivity.getSupportFragmentManager(), changePhoneBS.getClass().getSimpleName());
    }

    public void setValueAdd(Boolean value) {
        SharedPreferences sharedPref = getContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(ADD_INTERNET, value);
        editor.commit();
    }

    @OnClick({R.id.m_p_feedback_us_img_upload_photo_1,
            R.id.m_p_feedback_us_img_upload_photo_2,
            R.id.m_p_feedback_us_txt_upload_photo,
            R.id.m_p_feedback_us_txt_title_upload_photo})
    void uploadPhotoImage() {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    Constants.PERMISSION.PERMISSION_READ_EXTERNAL_STORAGE);
        } else {
            openGallery();
        }
    }

    @OnClick(R.id.m_p_feedback_us_img_delete_photo)
    void deletePhoto() {
        if (mImageEncodeBase64 != null) {
            mImageEncodeBase64 = "";
        }
        handleUploadPhoto(false, "");
    }

    private void openGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.setType("image/*");
        startActivityForResult(pickPhoto, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.ACTION.ACTION_PICK_PICTURE) {
                if (data != null) {
                    Uri uri = data.getData();
                    String fileName = "";
                    if (ContentResolver.SCHEME_FILE.equals(Objects.requireNonNull(uri).getScheme())) {
                        File file = new File(Objects.requireNonNull(uri.getPath()));
                        fileName = file.getName();
                    } else if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) { // Content Scheme.
                        Cursor returnCursor =
                                mParentActivity.getContentResolver().query(uri, null, null, null, null);
                        if (returnCursor != null && returnCursor.moveToFirst()) {
                            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            fileName = returnCursor.getString(nameIndex);
                            returnCursor.close();
                        }
                    }
                    handleUploadPhoto(true, fileName);

                    try {
                        Bitmap selectedImage = MediaStore.Images.Media.getBitmap(mParentActivity.getContentResolver(), uri);
                        new MPAddFeedbackFragment.EncodeToBase64Task(selectedImage, (string) -> {
                            mImageEncodeBase64 = string;
                        }).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constants.PERMISSION.PERMISSION_READ_EXTERNAL_STORAGE) {
            if (PermissionHelper.allowedPermission(mParentActivity.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                openGallery();
            } else {
                android.util.Log.e(TAG, "onRequestPermissionsResult: READ_EXTERNAL_STORAGE denied");
            }
        }
    }

    private void handleUploadPhoto(boolean isImageSelected, String titlePhoto) {
        if (isImageSelected) {
            mFeedbackLayoutUploadPhoto.setVisibility(View.GONE);
            mFeedbackTxtTitleUploadPhoto.setText(titlePhoto);
            mFeedbackLayoutUploadPhotoWithContent.setVisibility(View.VISIBLE);
        } else {
            mFeedbackLayoutUploadPhoto.setVisibility(View.VISIBLE);
            mFeedbackTxtTitleUploadPhoto.setText(titlePhoto);
            mFeedbackLayoutUploadPhotoWithContent.setVisibility(View.GONE);
        }
    }

    interface OnBitmapEncodeListener {
        void onSuccess(String string);
    }

    @SuppressLint("StaticFieldLeak")
    static class EncodeToBase64Task extends AsyncTask<Void, Void, String> {
        private Bitmap bitmap;
        private MPAddFeedbackFragment.OnBitmapEncodeListener listener;

        EncodeToBase64Task(Bitmap bitmap, MPAddFeedbackFragment.OnBitmapEncodeListener listener) {
            this.bitmap = bitmap;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(Void... voids) {
            ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayBitmapStream);
            byte[] b = byteArrayBitmapStream.toByteArray();
            return Base64.encodeToString(b, Base64.NO_WRAP);

        }

        @Override
        protected void onPostExecute(String s) {
            listener.onSuccess(s);
        }
    }
}
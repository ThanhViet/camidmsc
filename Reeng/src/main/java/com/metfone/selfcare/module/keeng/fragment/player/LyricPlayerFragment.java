package com.metfone.selfcare.module.keeng.fragment.player;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.lyric.LoadLyric;
import com.metfone.selfcare.module.keeng.widget.lyric.LrcView;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.ui.CusBottomSheetDialog;
import com.metfone.selfcare.util.Utilities;

public class LyricPlayerFragment extends BottomSheetDialogFragment {
    public static final String TAG = LyricPlayerFragment.class.getSimpleName();

    private TextView tvContent, tvLyricSongName, tvLyricSongSinger;
    private LrcView lyricView;
    private View scrollView;
    private SeekBar seekBar;
    private ImageView ivLike;
    private ImageView btnPlay, ivLyricAvatar;
    private RelativeLayout rlLyricLoading;
    //    private int mPlayStatus;
    private PlayMusicController mPlayMusicController;

    private MediaModel mCurrentModel;
    private ClickItemListener mListener;

    public static LyricPlayerFragment newInstance(MediaModel mediaModel, ClickItemListener listener) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommonUtils.KEY_SONG, mediaModel);
        LyricPlayerFragment fragment = new LyricPlayerFragment();
        fragment.mListener = listener;
        fragment.setArguments(bundle);
        return fragment;
    }

    public static LyricPlayerFragment newInstance(MediaModel mediaModel, PlayMusicController playStatus, ClickItemListener listener) {
        LyricPlayerFragment fragment = newInstance(mediaModel, listener);
        fragment.mPlayMusicController = playStatus;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mCurrentModel = (MediaModel) bundle.getSerializable(CommonUtils.KEY_SONG);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lyric, container, false);
        if (view == null) return null;
        tvContent = view.findViewById(R.id.content);
        tvContent.setMovementMethod(new ScrollingMovementMethod());
        tvLyricSongName = view.findViewById(R.id.tvLyricSongName);
        tvLyricSongSinger = view.findViewById(R.id.tvLyricSongSinger);
        lyricView = view.findViewById(R.id.lyric_view);
        scrollView = view.findViewById(R.id.scroll_view);
        ivLike = view.findViewById(R.id.ivLyricLike);
        ivLike.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClickLyricLike();
            }
        });
        ivLyricAvatar = view.findViewById(R.id.ivLyricAvatar);
        btnPlay = view.findViewById(R.id.btnLyricPlay);
        btnPlay.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClickLyricPlay();
            }
        });
        rlLyricLoading = view.findViewById(R.id.rlLyricLoading);
        LinearLayout llCloseOptionDialog = view.findViewById(R.id.llCloseLyricDialog);
        llCloseOptionDialog.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClickLyricExit();
            }
        });
        seekBar = view.findViewById(R.id.seekBarLyric);
        seekBar.setOnTouchListener((v, event) -> true);
        loadData();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() == null) return;
        lyricView.setTimelineTextColor(getActivity().getResources().getColor(R.color.v5_text_description));
        lyricView.setCurrentColor(getActivity().getResources().getColor(R.color.v5_text));
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final CusBottomSheetDialog dialog = new CusBottomSheetDialog(getActivity(), getTheme());
        dialog.setOnShowListener(dialogInterface -> {
            try {
                FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
                if (bottomSheet == null) return;
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                dialog.setLockDragging(true);
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.d("PollDetailFragmentV2", "show: Can not expand", e);
            }
        });

        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            CusBottomSheetDialog dialog = (CusBottomSheetDialog) getDialog();
            if (dialog == null) return;
            FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            BottomSheetBehavior behavior;

            if (bottomSheet != null) {
                ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();
                layoutParams.height = Math.max(Utilities.dpToPx(470), ScreenManager.getHeight(getActivity()) / 2);
                bottomSheet.setLayoutParams(layoutParams);

                behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0);
            }
        });
    }


//    public void updateData(MediaModel model) {
//        mCurrentModel = model;
//        loadData();
//    }

    private void loadData() {
        if (mCurrentModel == null) return;
        if (tvLyricSongName != null) {
            tvLyricSongName.setText(mCurrentModel.getName());
        }
        if (tvLyricSongSinger != null) {
            tvLyricSongSinger.setText(mCurrentModel.getSinger());
        }
        if (ivLyricAvatar != null) {
            ImageBusiness.setSongTwoPlayer(ivLyricAvatar, mCurrentModel.getImage(), Utilities.dpToPx(19));
        }
        if (ivLike != null) {
            ivLike.setSelected(mCurrentModel.isLike());
        }
        if (!TextUtils.isEmpty(mCurrentModel.getLyricUrl())) {
            new LoadLyric(this).execute(mCurrentModel.getLyricUrl());
        } else if (!TextUtils.isEmpty(mCurrentModel.getLyric())) {
            normalAndSetLyric(mCurrentModel.getLyric());
        } else if (!TextUtils.isEmpty(mCurrentModel.getId())) {
            new KeengApi().getLyric(Long.parseLong(mCurrentModel.getId()), response -> {
                String lyricTmp = response.getData();
                if (!TextUtils.isEmpty(lyricTmp)) {
                    try {
                        normalAndSetLyric(lyricTmp);
                        return;
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                }
                normalAndSetLyric("");
            }, error -> normalAndSetLyric(""));
        }
        setStateMediaPlayer();
    }

    public void setStateMediaPlayer() {
        if (mPlayMusicController == null) return;
        seekBar.setProgress(mPlayMusicController.getCurrentProgress());
        switch (mPlayMusicController.getStatePlaying()) {
            case Constants.PLAY_MUSIC.PLAYING_NONE:
            case Constants.PLAY_MUSIC.PLAYING_GET_INFO:
            case Constants.PLAY_MUSIC.PLAYING_PREPARING:
            case Constants.PLAY_MUSIC.PLAYING_RETRIEVING:
                rlLyricLoading.setVisibility(View.VISIBLE);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PLAYING:
                rlLyricLoading.setVisibility(View.INVISIBLE);
                btnPlay.setImageResource(R.drawable.ic_music_stop);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PAUSED:
                rlLyricLoading.setVisibility(View.INVISIBLE);
                btnPlay.setImageResource(R.drawable.ic_music_play);
                break;
            case Constants.PLAY_MUSIC.PLAYING_BUFFERING_START:
                rlLyricLoading.setVisibility(View.VISIBLE);
                break;
            case Constants.PLAY_MUSIC.PLAYING_BUFFERING_END:
                rlLyricLoading.setVisibility(View.INVISIBLE);
                break;
            default:
                rlLyricLoading.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void updateLyricView(int percent, long time) {
        if (seekBar != null) {
            seekBar.setProgress(percent);
        }
        if (lyricView != null) {
            lyricView.updateTime(time);
        }
    }

    public void updateMediaView(PlayMusicController playMusicController) {
        mPlayMusicController = playMusicController;
        setStateMediaPlayer();
    }

    public void updateAllView(MediaModel mediaModel) {
        mCurrentModel = mediaModel;
        loadData();
    }

    public void updateLikeState(MediaModel mediaModel){
        mCurrentModel = mediaModel;
        ivLike.setSelected(mCurrentModel.isLike());
    }

    public void normalAndSetLyric(String lyric) {
        if (lyricView != null)
            lyricView.setVisibility(View.GONE);
        if (tvContent != null) {
            tvContent.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(lyric)) {
                tvContent.setText(Html.fromHtml(lyric));
                try {
                    scrollView.scrollTo(0, 0);
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            } else {
                tvContent.setText(R.string.lyric_default);
            }
        }
    }

    @Override
    public void onDetach() {
        App.getInstance().cancelPendingRequests(KeengApi.GET_LYRIC);
        super.onDetach();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onDestroy() {
        if (lyricView != null) lyricView.onDrag(0);
        super.onDestroy();
    }

    public TextView getTvContent() {
        return tvContent;
    }

    public LrcView getLyricView() {
        return lyricView;
    }

    public interface ClickItemListener {
        void onClickLyricLike();

        void onClickLyricPlay();

        void onClickLyricExit();
    }
}

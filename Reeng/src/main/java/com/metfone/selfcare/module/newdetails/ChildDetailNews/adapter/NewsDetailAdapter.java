package com.metfone.selfcare.module.newdetails.ChildDetailNews.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.activity.NewsDetailActivity;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.newdetails.view.TextViewLinkHandler;
import com.metfone.selfcare.module.tiin.DateUtilitis;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.ui.glide.ImageLoader;
import com.metfone.selfcare.util.Log;
import com.vtm.adslib.AdsNativeHelper;
import com.vtm.adslib.template.TemplateView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NewsDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = NewsDetailAdapter.class.getSimpleName();
    private Context mContext;
    private AbsInterface.OnNewsHotListener mListener;
    private LayoutInflater infalter;
    private List<NewsDetailModel> datas = new ArrayList<>();
    private List<NewsModel> relateDatas = new ArrayList<>();
    private List<NewsModel> newsMostDatas = new ArrayList<>();
    private List<NewsModel> eventDatas = new ArrayList<>();
    private NewsModel newsModel;

    private final int TYPE_HEADER = 0;
    private final int TYPE_TEXT = 1;
    private final int TYPE_IMAGE = 2;
    private final int TYPE_VIDEO = 3;
    private final int TYPE_AUTHOR = 4;
    private final int TYPE_TEXT_NOTE = 5;
    public final static int TYPE_HEADER_NEWS_EVENT = 10;
    public final static int TYPE_NEWS_EVENT = 11;
    private final int TYPE_HEADER_RELATE = 12;
    public final static int TYPE_RELATE = 13;
    private final int TYPE_HEADER_NEWS_MOST = 14;
    public final static int TYPE_NEWS_MOST = 15;
    public final static int TYPE_FB_ADVERTISING = 100;

    private final int TYPE_NONE = -1;
    private int isStopVideo = -1;
    private NewsHotDetailVideoHolder currentVideoHolder;
    private int demVideo = 0;

    private boolean hasAdvertising = false;
//    private View adContainer;
//    AdView adViewInRelate;
//    AdView adViewInDetail;

    NewsDetailModel loadingModel = null;

    public NewsDetailAdapter(Context context, NewsModel model, AbsInterface.OnNewsHotListener listener, boolean fullData) {
        this.mContext = context;
        this.infalter = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mListener = listener;

        if (model != null) {
            if (model.getBody() != null && model.getBody().size() > 0 && !model.getBody().contains(loadingModel)) {
                if (!fullData) {
                    loadingModel = new NewsDetailModel(context.getString(R.string.loadding_data), 0, "", "", TYPE_TEXT, 0);
                } else {
                    loadingModel = new NewsDetailModel("", 0, "", "", -1, 0);
                }
                loadingModel.currentPos = model.getBody().size();
                model.getBody().add(loadingModel);
            }
            this.datas = model.getBody();
            this.newsModel = model;
        }
    }

    public void setModelInfo(NewsModel modelInfo) {
        if (modelInfo == null) return;

        if (TextUtils.isEmpty(newsModel.getTitle()))
            newsModel.setTitle(modelInfo.getTitle());
        if (TextUtils.isEmpty(newsModel.getShapo()))
            newsModel.setShapo(modelInfo.getShapo());
        if (TextUtils.isEmpty(newsModel.getDatePub()))
            newsModel.setDatePub(modelInfo.getDatePub());
        if (TextUtils.isEmpty(newsModel.getUrl()))
            newsModel.setUrl(modelInfo.getUrl());
        if (TextUtils.isEmpty(newsModel.getSourceName()))
            newsModel.setSourceName(modelInfo.getSourceName());
        if (TextUtils.isEmpty(newsModel.getSid()))
            newsModel.setSid(modelInfo.getSid());
        if (TextUtils.isEmpty(newsModel.getSourceIcon()))
            newsModel.setSourceIcon(modelInfo.getSourceIcon());
        if (newsModel.getID() == 0)
            newsModel.setID(modelInfo.getID());
        if (TextUtils.isEmpty(newsModel.getCategory()))
            newsModel.setCategory(modelInfo.getCategory());
        if (newsModel.getTimeStamp() == 0L || newsModel.getTimeStamp() != modelInfo.getTimeStamp())
            newsModel.setTimeStamp(modelInfo.getTimeStamp());
        if (newsModel.getUnixTime() == 0L)
            newsModel.setUnixTime(modelInfo.getUnixTime());
        if (TextUtils.isEmpty(newsModel.getUrlOrigin())) {
            newsModel.setUrlOrigin(modelInfo.getUrlOrigin());
        }
    }

    public void setDatas(List<NewsDetailModel> originalDatas) {
        hasAdvertising = AdsUtils.checkShowAds() && AdsNativeHelper.getInstance().canShowAd();
        if (hasAdvertising && originalDatas != null && originalDatas.size() > 1) {
            //create fb advertising pos
//            NewsDetailModel fbAdvertisingModle = new NewsDetailModel("", 0, "", "", TYPE_FB_ADVERTISING, 0);
//            int adPos = (originalDatas.size() / 2);
//            originalDatas.add(adPos, fbAdvertisingModle);

            for (int i = 1; i < originalDatas.size(); i++) {
                if (i % 10 == 0 && i + 9 < originalDatas.size()) {
                    NewsDetailModel fbAdvertisingModle = new NewsDetailModel("", 0, "", "", TYPE_FB_ADVERTISING, 0);
                    originalDatas.add(i, fbAdvertisingModle);
                }
            }
        }
        this.datas = originalDatas;
        notifyDataSetChanged();
    }

    public void setRelateDatas(ArrayList<NewsModel> relateDatas) {
        this.relateDatas = relateDatas;
    }

    public void setNewsMostDatas(ArrayList<NewsModel> newsMostDatas) {
        this.newsMostDatas = newsMostDatas;
    }

    public void setNewsEventDatas(ArrayList<NewsModel> eventDatas) {
        this.eventDatas = eventDatas;
    }

    public Object getItem(int position) {
        int countEvent = eventDatas.size() == 0 ? 0 : eventDatas.size() + 1;
        int countRelateNews = relateDatas.size() == 0 ? 0 : relateDatas.size() + 1;

        int itemType = getItemViewType(position);
        switch (itemType) {
            case TYPE_HEADER:
                return null;
            case TYPE_TEXT:
            case TYPE_AUTHOR:
            case TYPE_IMAGE:
            case TYPE_VIDEO:
            case TYPE_TEXT_NOTE:
                return datas.get(position - 1);
            case TYPE_HEADER_NEWS_EVENT:
                return eventDatas.get(0);
            case TYPE_NEWS_EVENT:
                return eventDatas.get(position - 1 - (datas.size() + 1));
            case TYPE_HEADER_RELATE:
                return null;
            case TYPE_RELATE:
                return relateDatas.get(position - 1 - (datas.size() + 1) - countEvent);
            case TYPE_HEADER_NEWS_MOST:
                return null;
            case TYPE_NEWS_MOST:
                return newsMostDatas.get(position - 1 - (datas.size() + 1) - countEvent - countRelateNews);

            case TYPE_NONE:
            default:
                return null;
        }
    }

    public void setLoadingText(boolean isLoading) {
        if (loadingModel != null && mContext != null) {
            String content = isLoading ? mContext.getString(R.string.loadding_data) : "";
            loadingModel.setContent(content);
            try {
                notifyItemChanged(loadingModel.currentPos + 1);
            } catch (Exception e) {
                Log.e(TAG, "setLoadingText: Exception ", e);
            }
        }
    }

    public void onDestroy() {
        if (datas != null) {
            datas.remove(loadingModel);
        }
        mListener = null;
//        if (adViewInDetail != null) {
//            adViewInDetail.destroy();
//        }
//        if (adViewInRelate != null) {
//            adViewInRelate.destroy();
//        }
    }

    @Override
    public int getItemCount() {
        if (datas == null) return 0;

        int countEvent = eventDatas.size() == 0 ? 0 : eventDatas.size() + 1;
        int countMostNews = newsMostDatas.size() == 0 ? 0 : newsMostDatas.size() + 1;
        int countRelateNews = relateDatas.size() == 0 ? 0 : relateDatas.size() + 1;

        return 1 + datas.size() + countEvent + countMostNews + countRelateNews + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) //Header
        {
            return TYPE_HEADER;
        } else if (position > 0 && position < datas.size() + 1) {
            NewsDetailModel model = datas.get(position - 1);
            if (model.getType() == 3) {
                demVideo++;
            }
            if (model.getType() == TYPE_TEXT && model.getIsNote() == 1) {
                return TYPE_TEXT_NOTE;
            }
            return model.getType();
        } else if (eventDatas.size() > 0) {
            if (position == datas.size() + 1) {
                return TYPE_HEADER_NEWS_EVENT;
            } else if (position > datas.size() + 1 && position < datas.size() + 1 + eventDatas.size() + 1) {
                return TYPE_NEWS_EVENT;
            } else if (relateDatas.size() > 0) {
                if (position == datas.size() + 1 + eventDatas.size() + 1) {
                    return TYPE_HEADER_RELATE;
                } else if (position > datas.size() + 1 + eventDatas.size() + 1 && position < datas.size() + 1 + eventDatas.size() + 1 + relateDatas.size() + 1) {
                    return TYPE_RELATE;
                } else if (newsMostDatas.size() > 0) {
                    if (position == datas.size() + 1 + eventDatas.size() + 1 + relateDatas.size() + 1) {
                        return TYPE_HEADER_NEWS_MOST;
                    } else if (position > datas.size() + 1 + eventDatas.size() + 1 + relateDatas.size() + 1 &&
                            position < datas.size() + 1 + eventDatas.size() + 1 + relateDatas.size() + 1 + newsMostDatas.size() + 1) {
                        return TYPE_NEWS_MOST;
                    } else {
                        return TYPE_NONE;
                    }
                } else {
                    return TYPE_NONE;
                }
            } else {
                if (newsMostDatas.size() > 0) {
                    if (position == datas.size() + 1 + eventDatas.size() + 1) {
                        return TYPE_HEADER_NEWS_MOST;
                    } else if (position > datas.size() + 1 + eventDatas.size() + 1 && position < datas.size() + 1 + eventDatas.size() + 1 + newsMostDatas.size() + 1) {
                        return TYPE_NEWS_MOST;
                    } else {
                        return TYPE_NONE;
                    }
                } else {
                    return TYPE_NONE;
                }
            }
        } else {
            if (relateDatas.size() > 0) {
                if (position == datas.size() + 1) {
                    return TYPE_HEADER_RELATE;
                } else if (position > datas.size() + 1 && position < datas.size() + 1 + relateDatas.size() + 1) {
                    return TYPE_RELATE;
                } else if (newsMostDatas.size() > 0) {
                    if (position == datas.size() + 1 + relateDatas.size() + 1) {
                        return TYPE_HEADER_NEWS_MOST;
                    } else if (position > datas.size() + 1 + relateDatas.size() + 1 && position < datas.size() + 1 + relateDatas.size() + 1 + newsMostDatas.size() + 1) {
                        return TYPE_NEWS_MOST;
                    } else {
                        return TYPE_NONE;
                    }
                } else {
                    return TYPE_NONE;
                }
            } else {
                if (newsMostDatas.size() > 0) {
                    if (position == datas.size() + 1) {
                        return TYPE_HEADER_NEWS_MOST;
                    } else if (position > datas.size() + 1 && position < datas.size() + 1 + newsMostDatas.size() + 1) {
                        return TYPE_NEWS_MOST;
                    } else {
                        return TYPE_NONE;
                    }
                } else {
                    return TYPE_NONE;
                }
            }
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder instanceof NewsHotDetailVideoHolder) {
            if (isStopVideo == holder.getAdapterPosition() && ((NewsDetailActivity) mContext).isPlaying) {
                mListener.onDetachVideoDetail();
                checkCurrentVideo();
                Log.e("DDuong", "detach");
            }
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (holder instanceof NewsHotDetailVideoHolder) {
            if (isStopVideo == holder.getAdapterPosition()) {
                if (((NewsDetailActivity) mContext).isPlaying) {
                    setVisibility(((NewsHotDetailVideoHolder) holder).ivPlay, View.GONE);
                    setVisibility(((NewsHotDetailVideoHolder) holder).ivImage, View.GONE);
                } else {
                    setVisibility(((NewsHotDetailVideoHolder) holder).ivPlay, View.VISIBLE);
                    setVisibility(((NewsHotDetailVideoHolder) holder).ivImage, View.VISIBLE);
                    checkCurrentVideo();
                    Log.e("DDuong", "attach");
                }

            } else {
                setVisibility(((NewsHotDetailVideoHolder) holder).ivPlay, View.VISIBLE);
                setVisibility(((NewsHotDetailVideoHolder) holder).ivImage, View.VISIBLE);

            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View convertView;
        switch (type) {
            case TYPE_HEADER:
                convertView = infalter.inflate(R.layout.item_news_detail_header, parent, false);
                return new NewsHotDetailHeaderHolder(convertView, mContext, mListener);
            case TYPE_TEXT:
                convertView = infalter.inflate(R.layout.item_news_detail_text, parent, false);
                return new NewsHotDetailTextHolder(convertView, mContext, mListener);
            case TYPE_AUTHOR:
                convertView = infalter.inflate(R.layout.item_news_detail_author, parent, false);
                return new NewsHotDetailTextHolder(convertView, mContext, mListener);
            case TYPE_IMAGE:
                convertView = infalter.inflate(R.layout.item_news_detail_image, parent, false);
                return new NewsHotDetailImageHolder(convertView, mContext, mListener);
            case TYPE_VIDEO:
                convertView = infalter.inflate(R.layout.item_news_detail_video, parent, false);
                return new NewsHotDetailVideoHolder(convertView, mListener);
            case TYPE_HEADER_RELATE:
                convertView = infalter.inflate(R.layout.item_news_detail_relate_header, parent, false);
//                if (convertView != null && convertView instanceof ViewGroup) {
//                    try {
//                        if (hasAdvertising) {
//                            adContainer = infalter.inflate(R.layout.item_advertising_fb, null, false);
//                            ((ViewGroup) convertView).addView(adContainer, 0);
//                        }
//                    } catch (Exception e) {
//                    }
//                }
                return new NewsHotDetailRelateHeaderHolder(convertView, mContext);
            case TYPE_RELATE:
                convertView = infalter.inflate(R.layout.holder_normal_news, parent, false);
                return new NewsHotDetailRelateHolder(convertView, mContext, mListener, TYPE_RELATE);
            case TYPE_HEADER_NEWS_MOST:
                convertView = infalter.inflate(R.layout.item_news_detail_most_header, parent, false);
                return new NewsHotDetailRelateHeaderHolder(convertView, mContext);
            case TYPE_NEWS_MOST:
                convertView = infalter.inflate(R.layout.holder_normal_news, parent, false);
                return new NewsHotDetailRelateHolder(convertView, mContext, mListener, TYPE_NEWS_MOST);
            case TYPE_HEADER_NEWS_EVENT:
                convertView = infalter.inflate(R.layout.item_news_detail_event_header, parent, false);
//                if (convertView != null && convertView instanceof ViewGroup) {
//                    try {
//                        if (hasAdvertising) {
//                            adContainer = infalter.inflate(R.layout.item_advertising_fb, null, false);
//                            ((ViewGroup) convertView).addView(adContainer, 0);
//                        }
//                    } catch (Exception e) {
//                    }
//                }
                return new NewsHotDetailRelateHeaderHolder(convertView, mContext);
            case TYPE_NEWS_EVENT:
                convertView = infalter.inflate(R.layout.holder_normal_news, parent, false);
                return new NewsHotDetailRelateHolder(convertView, mContext, mListener, TYPE_NEWS_MOST);

            case TYPE_FB_ADVERTISING:
                convertView = infalter.inflate(R.layout.ad_unified_medium, parent, false);
                return new NewsFbAdvertisingHolder(mContext, convertView);

            case TYPE_TEXT_NOTE:
                convertView = infalter.inflate(R.layout.item_news_detail_text_note, parent, false);
                return new NewsTextNoteHolder(convertView);
            case TYPE_NONE:
                convertView = infalter.inflate(R.layout.holder_footer_new, parent, false);
                return new BaseViewHolder(convertView);
            default:
                convertView = infalter.inflate(R.layout.item_empty, parent, false);
                return new NewsHotDetailTextHolder(convertView, mContext, mListener);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof NewsHotDetailHeaderHolder) {
                ((NewsHotDetailHeaderHolder) holder).setElement(newsModel);
            } else if (holder instanceof NewsHotDetailTextHolder) {
                ((NewsHotDetailTextHolder) holder).setElement(getItem(position), position);
            } else if (holder instanceof NewsHotDetailImageHolder) {
                ((NewsHotDetailImageHolder) holder).setElement(getItem(position));
            } else if (holder instanceof NewsHotDetailVideoHolder) {
                ((NewsHotDetailVideoHolder) holder).setElement(getItem(position), position, (NewsHotDetailVideoHolder) holder);
            } else if (holder instanceof NewsHotDetailRelateHolder) {
                ((NewsHotDetailRelateHolder) holder).setElement(getItem(position));

//                if (hasAdvertising) {
//                    handleAudienceNetworkAdRelate(adViewInRelate, adContainer);
//                } else {
//                    if (adContainer != null) {
//                        adContainer.setVisibility(View.GONE);
//                    }
//                }
            } else if (holder instanceof NewsFbAdvertisingHolder) {
                // do action for audience network (fb advertising)
//                handleAudienceNetworkAdDetail(adViewInDetail, ((NewsFbAdvertisingHolder) holder).fbAdViewContainer);
                ((NewsFbAdvertisingHolder) holder).bindData();
            } else if (holder instanceof NewsHotDetailRelateHeaderHolder) {
                int viewType = getItemViewType(position);
                if (viewType == TYPE_HEADER_NEWS_EVENT) {
                    ((NewsHotDetailRelateHeaderHolder) holder).setElement(getItem(position));
                    ((NewsHotDetailRelateHeaderHolder) holder).bindAds();
//                    if (hasAdvertising) {
//                        handleAudienceNetworkAdRelate(adViewInRelate, adContainer);
//                    } else {
//                        if (adContainer != null) {
//                            adContainer.setVisibility(View.GONE);
//                        }
//                    }
                } else if (viewType == TYPE_HEADER_RELATE) {
                    ((NewsHotDetailRelateHeaderHolder) holder).bindAds();
                }
            } else if (holder instanceof NewsTextNoteHolder) {
                ((NewsTextNoteHolder) holder).setElement(getItem(position));
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public class NewsHotDetailTextHolder extends BaseViewHolder {
        WeakReference<AbsInterface.OnNewsHotListener> mListener;
        private Context mContext;
        private NewsDetailModel mEntry;
        private TextView tvContent;
        //        float currentFontContent;
        private ImageView ivSource;

        public NewsHotDetailTextHolder(View convertView, Context context, AbsInterface.OnNewsHotListener listener) {
            super(convertView);
            this.mContext = context;
            this.mListener = new WeakReference<>(listener);
            tvContent = convertView.findViewById(R.id.tvContent);
//            currentFontContent = tvContent.getTextSize();
            ivSource = convertView.findViewById(R.id.iv_source);
        }

        int pos;

        public void setElement(Object obj, int pos) {
            mEntry = (NewsDetailModel) obj;
            this.pos = pos;
            initData();
        }

        private void initData() {
            if (mEntry != null) {

                tvContent.setText(Html.fromHtml(mEntry.getContent()));
                tvContent.setLinksClickable(true);
                tvContent.setMovementMethod(new TextViewLinkHandler() {
                    @Override
                    public void onLinkClick(String url) {
                        int type;
                        String baseUrl = "";
                        String originalUrl = url;
                        if (!TextUtils.isEmpty(url)) {
                            baseUrl = url.replaceAll("http://netnews.vn/", "");
                        }
                        try {
//                            url = "http://netnews.vn/Xem.aspx?id=1545065&pid=150&cid=837";
                            if (url.contains(".html")) {
                                url = url.replace(".html", "");
                                String[] list = url.split("-");
                                if (list.length > 1) {
                                    String id = list[list.length - 1];

                                    if (id.length() == 7)//Tin binh thuong
                                    {
                                        type = 1;

                                        String cid = list[list.length - 2];
                                        String pid = list[list.length - 3];
                                        NewsModel model = new NewsModel();
                                        model.setID(Integer.parseInt(id));
                                        model.setPid(Integer.parseInt(pid));
                                        model.setCid(Integer.parseInt(cid));
                                        model.setUrl(baseUrl);
                                        if (mListener != null && mListener.get() != null)
                                            mListener.get().onClickLinkItem(type, model);
                                    } else if (id.length() == 4)//Tin su kien
                                    {
                                        type = 2;

                                        NewsModel model = new NewsModel();
                                        model.setID(Integer.parseInt(id));
                                        model.setTitle("");
                                        model.setUrl(originalUrl);
                                        if (mListener != null && mListener.get() != null)
                                            mListener.get().onClickLinkItem(type, model);
                                    } else {
                                        type = 3;

                                        ToastUtils.makeText(mContext, "Nội dung này có thể mất phí 3G/4G");
                                        NewsModel model = new NewsModel();
                                        model.setUrl(originalUrl);
                                        if (mListener != null && mListener.get() != null)
                                            mListener.get().onClickLinkItem(type, model);
                                    }
                                } else {
                                    type = 3;

                                    ToastUtils.makeText(mContext, "Nội dung này có thể mất phí 3G/4G");
                                    NewsModel model = new NewsModel();
                                    model.setUrl(originalUrl);
                                    if (mListener != null && mListener.get() != null)
                                        mListener.get().onClickLinkItem(type, model);
                                }
                            } else {
                                if (url.contains("id=") && url.contains("cid=") && url.contains("pid=")) {
                                    int index = url.indexOf("?");
                                    url = url.substring(index + 1);
                                    url = url.replace("cid=", "");
                                    url = url.replace("pid=", "");
                                    url = url.replace("id=", "");

                                    String[] list = url.split("&");
                                    if (list.length >= 3) {
                                        String id = list[0];

                                        if (id.length() == 7)//Tin binh thuong
                                        {
                                            type = 1;

                                            String pid = list[1];
                                            String cid = list[2];
                                            NewsModel model = new NewsModel();
                                            model.setID(Integer.parseInt(id));
                                            model.setPid(Integer.parseInt(pid));
                                            model.setCid(Integer.parseInt(cid));
                                            model.setUrl(baseUrl);
                                            if (mListener != null && mListener.get() != null)
                                                mListener.get().onClickLinkItem(type, model);
                                        } else if (id.length() == 4)//Tin su kien
                                        {
                                            type = 2;

                                            NewsModel model = new NewsModel();
                                            model.setID(Integer.parseInt(id));
                                            model.setTitle("");
                                            model.setUrl(originalUrl);
                                            if (mListener != null && mListener.get() != null)
                                                mListener.get().onClickLinkItem(type, model);
                                        } else {
                                            type = 3;

                                            ToastUtils.makeText(mContext, "Nội dung này có thể mất phí 3G/4G");
                                            NewsModel model = new NewsModel();
                                            model.setUrl(originalUrl);
                                            if (mListener != null && mListener.get() != null)
                                                mListener.get().onClickLinkItem(type, model);
                                        }
                                    }
                                } else {
                                    type = 3;

                                    ToastUtils.makeText(mContext, "Nội dung này có thể mất phí 3G/4G");
                                    NewsModel model = new NewsModel();
                                    model.setUrl(originalUrl);
                                    if (mListener != null && mListener.get() != null)
                                        mListener.get().onClickLinkItem(type, model);
                                }
                            }
                        } catch (Exception ex) {
                            type = 3;

                            ToastUtils.makeText(mContext, "Nội dung này có thể mất phí 3G/4G");
                            NewsModel model = new NewsModel();
                            model.setUrl(originalUrl);
                            if (mListener != null && mListener.get() != null)
                                mListener.get().onClickLinkItem(type, model);
                        }
                    }
                });
            }

            if (ivSource != null && newsModel != null) {
                if (!TextUtils.isEmpty(newsModel.getUrlOrigin())) {
                    ivSource.setVisibility(View.VISIBLE);
                    ivSource.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            TiinUtilities.showDialogSource((BaseSlidingFragmentActivity) mContext, newsModel.getUrlOrigin());
                        }
                    });
                } else {
                    ivSource.setVisibility(View.GONE);
                }

            }
        }
    }

    public static class NewsHotDetailImageHolder extends BaseViewHolder {

        private Context mContext;
        private NewsDetailModel mEntry;
        private ImageView imvImage;
        private WeakReference<AbsInterface.OnNewsHotListener> listener;

        public NewsHotDetailImageHolder(View convertView, Context context, AbsInterface.OnNewsHotListener listener) {
            super(convertView);
            this.mContext = context;
            this.listener = new WeakReference<>(listener);
            imvImage = convertView.findViewById(R.id.imvImage);
        }

        public void setElement(Object obj) {
            mEntry = (NewsDetailModel) obj;
            initData();
            setListener();
        }

        private void setListener() {
            if (getConvertView() != null) {
                getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null && listener.get() != null)
                            listener.get().onClickImageItem(mEntry);
                    }
                });
            }
        }

        private void initData() {
            if (mEntry == null) return;
            int widthPixels;
            DisplayMetrics displaymetrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(displaymetrics);
            if (displaymetrics.widthPixels > displaymetrics.heightPixels) {
                // width>height (set nguoc lai)
                widthPixels = displaymetrics.heightPixels;
            } else {
                widthPixels = displaymetrics.widthPixels;
            }
            float ratio = (float) mEntry.getHeight() / (float) mEntry.getWidth();
            ViewGroup.LayoutParams layoutParams = imvImage.getLayoutParams();
            layoutParams.width = widthPixels;
            layoutParams.height = (int) (layoutParams.width * ratio);
            imvImage.setLayoutParams(layoutParams);
            if (mEntry.getContent().contains(".gif")) {
                ImageLoader.setNewImageGif(mContext, mEntry.getContent(), imvImage);
            } else {
                ImageLoader.setNewsImageCache(mContext, mEntry.getContent(), imvImage);
            }
        }
    }

    public class NewsHotDetailVideoHolder extends RecyclerView.ViewHolder {
        public FrameLayout layoutVideo;
        ImageView ivImage, ivPlay;
        private NewsDetailModel mEntry;
        AbsInterface.OnNewsHotListener listener;

        public NewsHotDetailVideoHolder(View itemView, AbsInterface.OnNewsHotListener listener) {
            super(itemView);
            this.listener = listener;
            layoutVideo = itemView.findViewById(R.id.frVideo);
            ivImage = itemView.findViewById(R.id.ivVideo);
            ivPlay = itemView.findViewById(R.id.iv_detail_play);
            TiinUtilities.setWidthHeight((Activity) mContext, layoutVideo);
            TiinUtilities.setWidthHeightImage((Activity) mContext, ivImage);
            if (ivPlay.getVisibility() == View.GONE) {
                setVisibility(ivPlay, View.VISIBLE);
            }
            if (ivImage.getVisibility() == View.GONE) {
                setVisibility(ivImage, View.VISIBLE);
            }
        }

        public void setElement(Object obj, final int position, final NewsHotDetailVideoHolder holder) {
            if (obj == null) {
                return;
            }
            mEntry = (NewsDetailModel) obj;
            ImageLoader.setNewsImage(mContext, mEntry.getContent(), ivImage);
            ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (datas != null && datas.size() > 1) {
                        setVisibility(ivImage, View.GONE);
                        setVisibility(ivPlay, View.GONE);
                        listener.onPlayVideoDetail(mEntry, position, holder);
                        isStopVideo = position;
                        if (currentVideoHolder != null) {
                            setVisibility(currentVideoHolder.ivImage, View.VISIBLE);
                            setVisibility(currentVideoHolder.ivPlay, View.VISIBLE);
                        }
                        if (demVideo >= 2) {
                            currentVideoHolder = holder;
                        }
                    } else {
                        if (BuildConfig.DEBUG) {
                            Toast.makeText(ApplicationController.self(), mContext.getString(R.string.loadding_data), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }

    }

    public void checkCurrentVideo() {
        if (currentVideoHolder != null) {
            currentVideoHolder = null;
        }
    }

    private void setVisibility(View view, int visibility) {
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    public static class NewsHotDetailHeaderHolder extends BaseViewHolder {

        private Context mContext;
        private NewsModel newsModel;
        private TextView tvTitle;
        private TextView tvCategory;
        private TextView tvSapo;
        private TextView tvDate;
        private TextView tvLoading;
        //private TextView btnListen;
        private ImageView icon;
        private ImageView imvDot;
        WeakReference<AbsInterface.OnNewsHotListener> listener;
        float currentFontTitle;
        float currentFontSapo;
        float currentFontCategory;
        float currentFontDate;

        public NewsHotDetailHeaderHolder(View convertView, Context context, AbsInterface.OnNewsHotListener listener) {
            super(convertView);
            this.mContext = context;
            this.listener = new WeakReference<>(listener);
            tvTitle = convertView.findViewById(R.id.tvTitle);
            tvSapo = convertView.findViewById(R.id.tvSapo);
            tvDate = convertView.findViewById(R.id.tvDate);
            tvCategory = convertView.findViewById(R.id.tvCategory);
            tvLoading = convertView.findViewById(R.id.tvLoading);
            //btnListen = (TextView) convertView.findViewById(R.id.btnListen);
            icon = convertView.findViewById(R.id.icon);
            imvDot = convertView.findViewById(R.id.imvDot);

//            currentFontTitle = tvTitle.getTextSize();
//            currentFontSapo = tvSapo.getTextSize();
//            currentFontCategory = tvCategory.getTextSize();
//            currentFontDate = tvDate.getTextSize();
        }

        private void setVisibility(View view, boolean visibility) {
            if (view != null) {
                view.setVisibility(visibility ? View.VISIBLE : View.GONE);
            }
        }

        public void setElement(Object obj) {
            newsModel = (NewsModel) obj;
            initData();
        }

        private void initData() {
            try {
                if (newsModel == null) return;
                if (newsModel.getBody().size() > 0)
                    setVisibility(tvLoading, false);
                else
                    setVisibility(tvLoading, true);
                if (!TextUtils.isEmpty(newsModel.getTitle()))
                    tvTitle.setText(Html.fromHtml(newsModel.getTitle()));
                if (newsModel.isLocalAreaNews()) {
                    tvCategory.setText(newsModel.getLocalAreaName());
                    setVisibility(icon, false);
                } else if (!TextUtils.isEmpty(newsModel.getSourceName())) {
                    tvCategory.setText(Html.fromHtml(newsModel.getSourceName()));
                    if (!TextUtils.isEmpty(newsModel.getSourceIcon())) {
                        setVisibility(icon, true);
                        ImageLoader.setNewsImage(mContext, newsModel.getSourceIcon(), icon);
                    } else {
                        setVisibility(icon, false);
                    }

                    if (!TextUtils.isEmpty(newsModel.getSid())) {
                        tvCategory.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (listener != null && listener.get() != null) {
                                    listener.get().onClickSourceNewsItem(newsModel.getSourceName(), newsModel.getSid());
                                }
                            }
                        });
                    }
                } else {
                    setVisibility(icon, false);
                    if (!TextUtils.isEmpty(newsModel.getCategory()))
                        tvCategory.setText(Html.fromHtml(newsModel.getCategory()));
                }
                if (newsModel.getTimeStamp() != 0) {
                    if (tvDate.getVisibility() == View.GONE) {
                        setVisibility(tvDate, true);
                    }
                    setVisibility(imvDot, true);
                    tvDate.setText(DateUtilitis.calculateDate(mContext, newsModel.getTimeStamp()));
                } else {
                    setVisibility(tvDate, false);
                    setVisibility(imvDot, false);
                }

                if (TextUtils.isEmpty(newsModel.getShapo())) {
                    setVisibility(tvSapo, false);
                } else {
                    setVisibility(tvSapo, true);
                    if (tvSapo != null) {
                        tvSapo.setText(newsModel.getShapo());
                    }
                }
            } catch (Exception ex) {

            }

        }
    }

    public static class NewsHotDetailRelateHeaderHolder extends BaseViewHolder {

        private Context mContext;
        private NewsModel mEntry;
        private TextView tvTitle;
        private TemplateView layout_ads;

        public NewsHotDetailRelateHeaderHolder(View convertView, Context context) {
            super(convertView);
            this.mContext = context;

            tvTitle = convertView.findViewById(R.id.tvTitle);
            layout_ads = convertView.findViewById(R.id.layout_ads);
        }

        public void setElement(Object obj) {
            mEntry = (NewsModel) obj;
            initData();
        }

        private void initData() {
            if (mEntry == null) return;

            tvTitle.setText(mEntry.getLatestTitle());
        }

        public void bindAds()
        {
            if (layout_ads != null)
                AdsManager.getInstance().showAdsNative(layout_ads);
        }
    }

    public static class NewsHotDetailRelateHolder extends BaseViewHolder {
        private WeakReference<AbsInterface.OnNewsHotListener> mListener;
        private Context mContext;
        private NewsModel mEntry;
        private ImageView imvImage;
        private TextView tvTitle;
        private TextView tvCategory;
        private TextView tvDate;
        private TextView tvDes;
        private ImageView ivMore;
        public View line;
        public int type;
        public TemplateView layout_ads;

        public NewsHotDetailRelateHolder(View convertView, Context context, AbsInterface.OnNewsHotListener listener, int type) {
            super(convertView);
            this.mContext = context;
            this.mListener = new WeakReference<>(listener);
            this.type = type;
            imvImage = convertView.findViewById(R.id.iv_cover);
            tvTitle = convertView.findViewById(R.id.tv_title);
            tvCategory = convertView.findViewById(R.id.tv_category);
            tvDate = convertView.findViewById(R.id.tv_datetime);
            tvDes = convertView.findViewById(R.id.tv_desc);
            ivMore = convertView.findViewById(R.id.button_option);
            line = convertView.findViewById(R.id.line);
            layout_ads = convertView.findViewById(R.id.layout_ads);
        }

        public void setElement(Object obj) {
            mEntry = (NewsModel) obj;
            drawHolderDetail();
            setListener();

            if (layout_ads != null)
                AdsManager.getInstance().showAdsNative(layout_ads);
        }

        private void setListener() {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null && mListener.get() != null) {
                        mListener.get().onClickRelateItem(mEntry, type);
                    }
                }
            });
            ivMore.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (mListener != null && mListener.get() != null) {
                        mListener.get().onItemClickBtnMore(mEntry);
                    }
                }
            });
        }

        private void setVisibility(View view, int visibility) {
            if (view != null) {
                view.setVisibility(visibility);
            }
        }

        private void drawHolderDetail() {
            if (mEntry == null) return;

            tvTitle.setText(mEntry.getTitle());
            tvCategory.setText(!TextUtils.isEmpty(mEntry.getSourceName()) ? mEntry.getSourceName() : mEntry.getCategory());

            tvDate.setText(Html.fromHtml(mEntry.getDatePub()));
            setVisibility(tvDate, View.VISIBLE);
//            setVisibility(imvDot, View.GONE);
//            ImageLoader.setNewsImage(mContext, mEntry.getImage169(), imvImage);
            ImageBusiness.setImageNew(mEntry.getImage169(), imvImage);
        }
    }

    private void removeViewParent(View view) {
        if (view != null && view.getParent() != null && view.getParent() instanceof ViewGroup) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }

    public static class NewsTextNoteHolder extends BaseViewHolder {
        TextView tvNote;
        NewsDetailModel mEntry;

        public NewsTextNoteHolder(View view) {
            super(view);
            tvNote = view.findViewById(R.id.tvNote);
        }

        public void setElement(Object obj) {
            mEntry = (NewsDetailModel) obj;
            if (mEntry != null) {
                tvNote.setText(Html.fromHtml(mEntry.getContent()));
            }
        }
    }

    //Ads
    public class NewsFbAdvertisingHolder extends BaseViewHolder {
        TemplateView fbAdViewContainer;

        public NewsFbAdvertisingHolder(Context context, View view) {
            super(view);
            fbAdViewContainer = view.findViewById(R.id.adContainer);
        }

        public void bindData() {
            if (fbAdViewContainer != null)
                AdsManager.getInstance().showAdsNative(fbAdViewContainer);
        }
    }

//    public void loadAdView() {
//        if (!hasAdvertising) return;
//        try {
//            String ad_player_music = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_NEWS);
//            if (TextUtils.isEmpty(ad_player_music)) return;
//            if (adViewInDetail == null) {
//                adViewInDetail = initAdView(ad_player_music, "AD_MIDDLE");
//                if (adViewInDetail != null && !adViewInDetail.isLoading()) {
//                    AdRequest adRequest = new AdRequest.Builder()
//                            .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
//                            .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
//                            .build();
//                    adViewInDetail.loadAd(adRequest);
//                }
//            }
//            if (adViewInRelate == null) {
//                adViewInRelate = initAdView(ad_player_music, "AD_END");
//                if (adViewInRelate != null && !adViewInRelate.isLoading()) {
//                    AdRequest adRequest = new AdRequest.Builder()
//                            .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
//                            .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
//                            .build();
//                    adViewInRelate.loadAd(adRequest);
//                }
//            }
//        } catch (Error | Exception e) {
//
//        }
//    }
//
//    private AdView initAdView(String unitId, String pos) {
//        AdView adView = new AdView(mContext);
//        adView.setAdUnitId(unitId);
//        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
//        adView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                super.onAdLoaded();
//                if (adView != null) {
//                    adView.setTag("Loaded");
//                }
//                if("AD_MIDDLE".equals(pos))
//                {
//                    notifyItemChanged((datas.size() - 1) / 2 + 1);
//                }
//                else if("AD_END".equals(pos))
//                {
//                    notifyItemChanged(1 + datas.size());
//                }
//            }
//
//            @Override
//            public void onAdClosed() {
//                super.onAdClosed();
//            }
//
//            @Override
//            public void onAdFailedToLoad(int i) {
//                super.onAdFailedToLoad(i);
//                if (adView != null) {
//                    adView.setTag("Error");
//                }
//            }
//        });
//        return adView;
//    }
//
//    private boolean handleAudienceNetworkAdDetail(AdView adView, View container) {
//        if (container == null) return false;
//        if (adView == null) {
//            container.setVisibility(View.GONE);
//            return false;
//        }
//        if (adView.getTag() != null) {
//            if (adView.getTag().equals("Loaded")) {
//                container.setVisibility(View.VISIBLE);
//                if (container == adView.getParent()) return true;
//                removeViewParent(adView);
//                ((ViewGroup) container).addView(adView);
//                return true;
//            } else {
//                container.setVisibility(View.GONE);
//                return false;
//            }
//        } else {
//            container.setVisibility(View.GONE);
//        }
//        return false;
//    }
//
//    private boolean handleAudienceNetworkAdRelate(AdView adView, View container) {
//        if (adView == null || container == null) return false;
//        try {
//            if (adView.getTag() != null) {
//                if (adView.getTag().equals("Loaded")) {
//                    container.setVisibility(View.VISIBLE);
//                    if (container.findViewById(R.id.adView) == adView.getParent()) return true;
//                    removeViewParent(adView);
//                    ((ViewGroup) container.findViewById(R.id.adView)).addView(adView);
//                    return true;
//                } else {
//                    container.setVisibility(View.GONE);
//                    return false;
//                }
//            } else {
//                container.setVisibility(View.GONE);
//            }
//            return false;
//        } catch (Exception | Error e) {
//            return false;
//        }
//    }
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/20
 */

package com.metfone.selfcare.module.saving.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.saving.activity.SavingActivity;
import com.metfone.selfcare.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.IllegalFormatConversionException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SavingSearchFragment extends Fragment {
    private static final String TAG = SavingSearchFragment.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.edit_start_date)
    EditText editStartDate;
    @BindView(R.id.edit_end_date)
    EditText editEndDate;

    private SavingActivity mActivity;
    private Resources mRes;
    private SimpleDateFormat sdfToTitle = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    private SimpleDateFormat sdfToRequest = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    public static SavingSearchFragment newInstance() {
        SavingSearchFragment fragment = new SavingSearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public String getDefaultEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date date = calendar.getTime();
        return sdfToTitle.format(date);
    }

    public String getDefaultStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date date = calendar.getTime();
        return sdfToTitle.format(date);
    }

    public long convertStringToTime(String text) {
        if (!TextUtils.isEmpty(text)) {
            try {
                Date date = sdfToTitle.parse(text);
                return date.getTime();
            } catch (Exception e) {
            }
        }
        return -1;
    }

    @OnClick(R.id.edit_start_date)
    public void onStartDateClick() {
        showDateDialog(editStartDate);
    }

    @OnClick(R.id.edit_end_date)
    public void onEndDateClick() {
        showDateDialog(editEndDate);
    }

    @OnClick(R.id.button_7_days)
    public void on7DaysClick() {
        Calendar cal = Calendar.getInstance();
        String endDate = sdfToRequest.format(cal.getTime());
        cal.add(Calendar.DAY_OF_YEAR, -7);
        String startDate = sdfToRequest.format(cal.getTime());
        NavigateActivityHelper.navigateToTabSavingHistoryDetail(mActivity, mRes.getString(R.string.last_7_days), startDate, endDate);
    }

    @OnClick(R.id.button_30_days)
    public void on30DaysClick() {
        Calendar cal = Calendar.getInstance();
        String endDate = sdfToRequest.format(cal.getTime());
        cal.add(Calendar.DAY_OF_YEAR, -30);
        String startDate = sdfToRequest.format(cal.getTime());
        NavigateActivityHelper.navigateToTabSavingHistoryDetail(mActivity, mRes.getString(R.string.last_30_days), startDate, endDate);
    }

    @OnClick(R.id.button_this_week)
    public void onThisWeekClick() {
        Calendar cal = Calendar.getInstance();
        String endDate = sdfToRequest.format(cal.getTime());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        cal.add(Calendar.WEEK_OF_YEAR, 0);
        String startDate = sdfToRequest.format(cal.getTime());
        NavigateActivityHelper.navigateToTabSavingHistoryDetail(mActivity, mRes.getString(R.string.this_week), startDate, endDate);
    }

    @OnClick(R.id.button_this_month)
    public void onThisMonthClick() {
        Calendar cal = Calendar.getInstance();
        String endDate = sdfToRequest.format(cal.getTime());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.MONTH, 0);
        String startDate = sdfToRequest.format(cal.getTime());
        NavigateActivityHelper.navigateToTabSavingHistoryDetail(mActivity, mRes.getString(R.string.this_month), startDate, endDate);
    }

    @SuppressLint("SimpleDateFormat")
    @OnClick(R.id.button_submit)
    public void onSubmitClick() {
        String startDate = editStartDate.getText().toString();
        String endDate = editEndDate.getText().toString();
        if (TextUtils.isEmpty(startDate)) {
            editStartDate.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(endDate)) {
            editEndDate.requestFocus();
            return;
        }

        Date startDte = null;
        try {
            startDte = sdfToTitle.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDte = null;
        try {
            endDte = sdfToTitle.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (startDte == null || endDte == null) return;
        if (startDte.getTime() > endDte.getTime()) {
            mActivity.showToast(R.string.msg_error_pick_date_2);
            editStartDate.requestFocus();
            return;
        }
        if ((endDte.getTime() - startDte.getTime()) > 2592000000L) {
            mActivity.showToast(R.string.msg_error_pick_date);
            editStartDate.requestFocus();
            return;
        }
        NavigateActivityHelper.navigateToTabSavingHistoryDetail(mActivity, startDate + " - " + endDate, sdfToRequest.format(startDte), sdfToRequest.format(endDte));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (SavingActivity) context;
        ApplicationController mApplication = (ApplicationController) mActivity.getApplicationContext();
        mRes = mApplication.getResources();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_saving_search, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        editStartDate.setText(getDefaultStartDate());
        editEndDate.setText(getDefaultEndDate());
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity.setTitle(mRes.getString(R.string.saving_history));
    }

    private void showDateDialog(final EditText editText) {
        if (editText == null) return;
        Calendar c = Calendar.getInstance();
        long dateLong = convertStringToTime(editText.getText().toString());
        if (dateLong > 0) {
            c.setTimeInMillis(dateLong);
        }
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        Calendar maxCalender = Calendar.getInstance();
        Context context = new ContextWrapper(getActivity()) {
            private Resources wrappedResources;

            @Override
            public Resources getResources() {
                Resources r = super.getResources();
                if (wrappedResources == null) {
                    wrappedResources = new Resources(r.getAssets(), r.getDisplayMetrics(), r.getConfiguration()) {
                        @NonNull
                        @Override
                        public String getString(int id, Object... formatArgs) throws NotFoundException {
                            try {
                                return super.getString(id, formatArgs);
                            } catch (IllegalFormatConversionException ifce) {
                                Log.e("DatePickerDialogFix", "IllegalFormatConversionException Fixed!", ifce);
                                String template = super.getString(id);
                                template = template.replaceAll("%" + ifce.getConversion(), "%s");
                                return String.format(getConfiguration().locale, template, formatArgs);
                            }
                        }
                    };
                }
                return wrappedResources;
            }
        };

        if (Build.VERSION.SDK_INT >= 18) {
            com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener listener = new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                @SuppressLint("SimpleDateFormat")
                @Override
                public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                    Calendar cal = Calendar.getInstance();
                    cal.set(selectedYear, selectedMonth, selectedDay);
                    editText.setText(sdfToTitle.format(cal.getTime()));
                }
            };
            new SpinnerDatePickerDialogBuilder()
                    .context(context)
                    .callback(listener)
                    .spinnerTheme(R.style.NumberPickerStyle)
                    .showTitle(true)
                    .showDaySpinner(true)
                    .defaultDate(mYear, mMonth, mDay)
                    .maxDate(maxCalender.get(Calendar.YEAR), maxCalender.get(Calendar.MONTH), maxCalender.get(Calendar.DAY_OF_MONTH))
                    .build()
                    .show();
        } else {
            maxCalender.add(Calendar.YEAR, -12);
            maxCalender.set(Calendar.MONTH, Calendar.DECEMBER);
            maxCalender.set(Calendar.DAY_OF_MONTH, 31);
            DatePickerDialog dialog = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                            Calendar cal = Calendar.getInstance();
                            cal.set(selectedYear, selectedMonth, selectedDay);
                            editText.setText(sdfToTitle.format(cal.getTime()));
                        }
                    }, mYear, mMonth, mDay);
            DatePicker datePicker = dialog.getDatePicker();
            datePicker.setMaxDate(maxCalender.getTimeInMillis());
            dialog.show();
        }

    }
}

package com.metfone.selfcare.module.selfcare.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.selfcare.adapter.SCPostageDetailAdapter;
import com.metfone.selfcare.module.selfcare.model.SCPostageDetail;
import com.metfone.selfcare.module.selfcare.model.SCPostageDetailInfo;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCPostageDetail;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.ui.view.load_more.OnEndlessScrollListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class SCPostageDetailFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private ImageView btnBack;
    private TextView tvTitle;
    private LoadingViewSC loadingView;
    private RecyclerView recyclerView;
    private SCPostageDetailAdapter adapter;
    private LinearLayoutManager layoutManager;

    private long fromDate;
    private long toDate;
    private int postType;
    private double fee;
    private double count;
    private int currentPage = 0;
    private boolean isLoading = false;
    private boolean isLoadMoreEnd = false;
    private ArrayList<SCPostageDetailInfo> postageDetailInfoList = new ArrayList<>();
    private ArrayList<SCPostageDetail> data = new ArrayList<>();
    private String prevStartTime = "";

    private SimpleDateFormat sdf = new SimpleDateFormat(TimeHelper.SDF_TIME_MYTEL, Locale.US);

    public static SCPostageDetailFragment newInstance(Bundle args) {
        SCPostageDetailFragment fragment = new SCPostageDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_postage_detail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);
        loadInfo();

        resetData();
        loadData();

        return view;
    }

    private void initView(View view) {
        layout_refresh = view.findViewById(R.id.refresh);
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.sc_primary));
            layout_refresh.setOnRefreshListener(this);
        }

        btnBack = view.findViewById(R.id.btnBack);
        tvTitle = view.findViewById(R.id.tvTitle);
        loadingView = view.findViewById(R.id.loading_view);
        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCPostageDetailAdapter(mActivity);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(onEndlessScrollListener);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        loadingView.setLoadingErrorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login lai
//                Intent intent = new Intent(getActivity(), SCAccountActivity.class);
//                mActivity.startActivity(intent);
            }
        });
    }

    private void loadInfo() {
        Bundle bundle = getArguments();
        fromDate = bundle.getLong(SCConstants.KEY_DATA.START_DATE, System.currentTimeMillis() - Calendar.getInstance().getTime().getHours() * DateTimeUtils.HOUR);
        toDate = bundle.getLong(SCConstants.KEY_DATA.END_DATE, System.currentTimeMillis());
        postType = bundle.getInt(SCConstants.KEY_DATA.POST_TYPE, 0);
        fee = bundle.getDouble(SCConstants.KEY_DATA.FEE, 0);
        count = bundle.getDouble(SCConstants.KEY_DATA.COUNT, 0);

        if (postType == SCConstants.POST_TYPE.CALL) {
            tvTitle.setText(mActivity.getString(R.string.sc_charge_call));
        } else if (postType == SCConstants.POST_TYPE.SMS) {
            tvTitle.setText(mActivity.getString(R.string.sc_charge_sms));
        } else if (postType == SCConstants.POST_TYPE.DATA) {
            tvTitle.setText(mActivity.getString(R.string.sc_charge_data));
        } else if (postType == SCConstants.POST_TYPE.VAS) {
            tvTitle.setText(mActivity.getString(R.string.sc_charge_services));
        } else if (postType == SCConstants.POST_TYPE.OTHER) {
            tvTitle.setText(mActivity.getString(R.string.sc_charge_other));
        } else {
            tvTitle.setText(mActivity.getString(R.string.sc_charge_search));
        }
    }

    private void loadData() {
        isLoading = true;
        if (currentPage == 0 && !isRefresh)
            loadingView.loadBegin();

        final WSSCRestful restful = new WSSCRestful(mActivity);
        restful.getPostageDetail(fromDate, toDate, postType, currentPage, SCConstants.NUM_SIZE, "desc", new Response.Listener<RestSCPostageDetail>() {
                    @Override
                    public void onResponse(RestSCPostageDetail result) {
//                        super.onResponse(result);
                        hideRefresh();
                        isLoading = false;

                        loadingView.loadFinish();
                        if (result != null) {
                            if (result.getStatus() == 200 && result.getData() != null) {
                                loadingView.loadFinish();

                                if (result.getData().size() > 0) {
                                    if (currentPage == 0)
                                        data.clear();
                                    data.addAll(result.getData());
                                    processData();

                                    adapter.setInfo(postType, fromDate, toDate, fee);
                                    adapter.setItemsList(postageDetailInfoList);
                                    adapter.notifyDataSetChanged();
                                } else {
                                    if (currentPage == 0)
                                        loadingView.loadEmpty();
                                    else
                                        isLoadMoreEnd = true;
                                }
                            } else if (result.getStatus() == 401 || result.getStatus() == 403) {
                                //Login lai
                                loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                            } else {
                                //Fail
                                if (currentPage == 0)
                                    loadingView.loadError();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        hideRefresh();
                        isLoading = false;
                        if (volleyError != null && volleyError.networkResponse != null) {
                            int errorCode = volleyError.networkResponse.statusCode;
                            if (errorCode == 401 || errorCode == 403) {
                                //Login lai
                                loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                            } else {
                                //Fail
                                if (currentPage == 0)
                                    loadingView.loadError();
                            }
                        } else {
                            //Fail
                            if (currentPage == 0)
                                loadingView.loadError();
                        }
                    }
                });
    }

    private OnEndlessScrollListener onEndlessScrollListener = new OnEndlessScrollListener(3) {
        @Override
        public void onLoadNextPage(View view) {
            super.onLoadNextPage(view);
            if (isLoading || isLoadMoreEnd) return;
            currentPage += 1;
            loadData();
        }
    };

    @Override
    public void onDestroyView() {
        App.getInstance().cancelPendingRequests(WSSCRestful.GET_POSTAGE_DETAIL);
        currentPage = 0;
        if (onEndlessScrollListener != null && recyclerView != null)
            recyclerView.removeOnScrollListener(onEndlessScrollListener);
        super.onDestroyView();
    }

    private void resetData() {
        currentPage = 0;
        isLoadMoreEnd = false;
        postageDetailInfoList.clear();
        data.clear();
        prevStartTime = "";
    }

    private void processData() {
        postageDetailInfoList.clear();
        SCPostageDetailInfo postageDetailInfo = new SCPostageDetailInfo();

        for (int i = 0; i < data.size(); i++) {
            SCPostageDetail model = data.get(i);
            if (postType == SCConstants.POST_TYPE.CALL) {
                model.setIcon(R.drawable.ic_sc_call);
            } else if (postType == SCConstants.POST_TYPE.SMS) {
                model.setIcon(R.drawable.ic_sc_sms);
            } else if (postType == SCConstants.POST_TYPE.DATA) {
                model.setIcon(R.drawable.ic_sc_data);
            } else if (postType == SCConstants.POST_TYPE.VAS) {
                model.setIcon(R.drawable.ic_sc_services);
            } else {
                model.setIcon(R.drawable.ic_sc_other);
            }

            String startTime = sdf.format(model.getStart_time());

            if (TextUtils.isEmpty(prevStartTime)) {
                postageDetailInfo.setTitle(startTime);
            }

            if (!startTime.equals(prevStartTime) && !TextUtils.isEmpty(prevStartTime)) {
                postageDetailInfoList.add(postageDetailInfo);

                postageDetailInfo = new SCPostageDetailInfo();
                postageDetailInfo.setTitle(startTime);
                postageDetailInfo.getData().add(model);

                if (i == data.size() - 1) {
                    postageDetailInfoList.add(postageDetailInfo);
                }
            } else {
                postageDetailInfo.getData().add(model);

                if (i == data.size() - 1) {
                    postageDetailInfoList.add(postageDetailInfo);
                }
            }

            prevStartTime = startTime;
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 0;
        isLoadMoreEnd = false;
        prevStartTime = "";
        loadData();
    }
}

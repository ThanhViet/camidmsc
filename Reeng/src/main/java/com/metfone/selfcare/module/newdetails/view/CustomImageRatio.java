package com.metfone.selfcare.module.newdetails.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.metfone.selfcare.R;

/**
 * Created by HaiKE on 10/09/2015.
 */
public class CustomImageRatio extends ImageView {
    private Context mContext;
    private float ratio = DEFAULT_ASPECT;

    public static final float DEFAULT_ASPECT = 1f;
    private static final int VERTICAL = 0;
    private static final int HORIZONTAL = 0;

    public CustomImageRatio(Context context) {
        super(context);
        this.mContext = context;
        init(null);
    }

    public CustomImageRatio(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init(attrs);
    }

    public CustomImageRatio(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomImageRatio(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.customImageView);
            ratio = a.getFloat(R.styleable.customImageView_ratio, 1);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        if (widthMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.AT_MOST) {
            height = calculate(width, ratio, VERTICAL);
        } else if (heightMode == MeasureSpec.EXACTLY || heightMode == MeasureSpec.AT_MOST) {
            width = calculate(height, ratio, HORIZONTAL);
        } else {
            throw new IllegalArgumentException("Either width or height should have exact value");
        }

        int specWidth = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        int specHeight = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(specWidth, specHeight);
    }

    private int calculate(int size, float aspect, int direction) {
        int wp = getPaddingLeft() + getPaddingRight();
        int hp = getPaddingTop() + getPaddingBottom();
        return direction == VERTICAL
                ? Math.round((size - wp) / aspect) + hp
                : Math.round((size - hp) * aspect) + wp;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
        invalidate();
    }
}

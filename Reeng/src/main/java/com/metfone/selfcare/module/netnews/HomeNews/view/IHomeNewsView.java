package com.metfone.selfcare.module.netnews.HomeNews.view;

import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;
import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.CategoryResponse;
import com.metfone.selfcare.module.response.HomeNewsResponse;
import com.metfone.selfcare.module.response.TopNowResponse;

import java.util.List;

/**
 * Created by HaiKE on 8/18/17.
 */

public interface IHomeNewsView extends MvpView {

    void loadDataSuccess(boolean flag);

    void bindData(HomeNewsResponse childNewsResponse, boolean flag);
//    void bindDataCategory(TopNowResponse response);
//
//    void bindDataCache(HomeNewsResponse childNewsResponse);
//    void bindDataCategoryCache(TopNowResponse response);
//
    void saveDataV5Cache(String data);
    void saveDataCareV5Cache(String data);

    void bindCategoryV5(List<CategoryModel> categoryResponse);
    void bindCanCareV5(HomeNewsModel model);
}

package com.metfone.selfcare.module.netnews.EventNews.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.EventModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.DateUtilitis;

public class EventItemAdapterV5 extends RecyclerView.Adapter<BaseViewHolder> {
    private final int SECTIONE_NORMAL = 2;
    private final int SECTIONE_EMPTY = 0;
    private EventModel model;
    private Context mContext;

    private AbsInterface.OnEventListener onItemListener;

    public EventItemAdapterV5(EventModel model, Context mContext, AbsInterface.OnEventListener onItemListener) {
        this.model = model;
        this.mContext = mContext;

        this.onItemListener = onItemListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case SECTIONE_NORMAL:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_special_news, parent, false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final NewsModel tiinModel = getItem(position);

        String[] listImage = tiinModel.getImage169().split(",");
        if (listImage.length > 1) {
            if (holder.getView(R.id.iv_cover) != null) {
                ImageBusiness.setImageNew(listImage[1], (ImageView) holder.getView(R.id.iv_cover));
            }
        } else if (listImage.length > 0) {
            if (holder.getView(R.id.iv_cover) != null) {
                ImageBusiness.setImageNew(listImage[0], (ImageView) holder.getView(R.id.iv_cover));
            }
        }
        if (holder.getView(R.id.tv_title) != null) {
            holder.setText(R.id.tv_title, tiinModel.getTitle());
        }
        if (holder.getView(R.id.tv_desc) != null) {
            holder.setVisible(R.id.tv_desc, true);
            holder.setText(R.id.tv_desc, tiinModel.getShapo());
        }
        if (holder.getView(R.id.tv_category) != null) {
            holder.setText(R.id.tv_category, tiinModel.getSourceName());
        }
        if (holder.getView(R.id.tv_datetime) != null) {
            if (holder.getView(R.id.tv_datetime) != null) {
                holder.setVisible(R.id.tv_datetime, true);
                if(tiinModel.getUnixTime() > 0){
                    holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, tiinModel.getUnixTime()));
                }else {
                    holder.setText(R.id.tv_datetime, Html.fromHtml(tiinModel.getDatePub()));
                }
            }
        }
        if (holder.getView(R.id.button_option) != null) {
            holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onItemClickMore(tiinModel);
                    }
                }
            });
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemListener.onItemClick(tiinModel);
            }
        });


    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null) {
            return SECTIONE_NORMAL;
        }
        return SECTIONE_EMPTY;
    }

    public NewsModel getItem(int position) {
        try {
            return model.getData().get(position);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
        return null;
    }

}

package com.metfone.selfcare.module.keeng.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseHolder;

public class PlaylistNewHolder extends BaseHolder {

    public ImageView mTopLeftImage, mTopRightImage, mBottomLeftImage, mBottomRightImage, mCover;
    public TextView mTvTitle, mTvContent, mTvListened, mTvCreated;
    public View mRightImageLayout;
    public View mBtnOption, viewOption;

    public PlaylistNewHolder(View itemView) {
        super(itemView);
        mTopLeftImage = (ImageView) itemView.findViewById(R.id.top_left_image);
        mBottomLeftImage = (ImageView) itemView.findViewById(R.id.bottom_left_image);
        mTopRightImage = (ImageView) itemView.findViewById(R.id.top_right_image);
        mBottomRightImage = (ImageView) itemView.findViewById(R.id.bottom_right_image);
        mCover = (ImageView) itemView.findViewById(R.id.cover);
        mTvTitle = (TextView) itemView.findViewById(R.id.title);
        mTvContent = (TextView) itemView.findViewById(R.id.content);
        mTvListened = (TextView) itemView.findViewById(R.id.listen);
        mTvCreated = (TextView) itemView.findViewById(R.id.created);
        mRightImageLayout = itemView.findViewById(R.id.right_image_layout);
        mBtnOption = itemView.findViewById(R.id.btn_more);
        viewOption = itemView.findViewById(R.id.layout_option);
    }

    public void setOneImage() {
        mRightImageLayout.setVisibility(View.GONE);
        mTopLeftImage.setVisibility(View.VISIBLE);
        mTopRightImage.setVisibility(View.GONE);
        mBottomLeftImage.setVisibility(View.GONE);
        mBottomRightImage.setVisibility(View.GONE);
    }

    public void setTwoImages() {
        mRightImageLayout.setVisibility(View.VISIBLE);
        mTopLeftImage.setVisibility(View.VISIBLE);
        mTopRightImage.setVisibility(View.VISIBLE);
        mBottomLeftImage.setVisibility(View.GONE);
        mBottomRightImage.setVisibility(View.GONE);
    }

    public void setThreeImages() {
        mRightImageLayout.setVisibility(View.VISIBLE);
        mTopLeftImage.setVisibility(View.VISIBLE);
        mTopRightImage.setVisibility(View.VISIBLE);
        mBottomRightImage.setVisibility(View.VISIBLE);
        mBottomLeftImage.setVisibility(View.GONE);
    }

    public void setFourImages() {
        mRightImageLayout.setVisibility(View.VISIBLE);
        mTopLeftImage.setVisibility(View.VISIBLE);
        mTopRightImage.setVisibility(View.VISIBLE);
        mBottomLeftImage.setVisibility(View.VISIBLE);
        mBottomRightImage.setVisibility(View.VISIBLE);
    }

}

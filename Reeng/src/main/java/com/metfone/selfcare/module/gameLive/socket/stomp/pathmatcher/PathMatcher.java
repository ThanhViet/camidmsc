package com.metfone.selfcare.module.gameLive.socket.stomp.pathmatcher;

import com.metfone.selfcare.module.gameLive.socket.stomp.dto.StompMessage;

public interface PathMatcher {

    boolean matches(String path, StompMessage msg);
}

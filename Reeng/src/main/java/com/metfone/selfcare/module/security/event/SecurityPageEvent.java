/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/27
 */

package com.metfone.selfcare.module.security.event;

public class SecurityPageEvent {
    private boolean isSelectMode;
    private boolean selectAll;
    private boolean delete;
    private int sizeSelected;
    private int tabId;
    private boolean selectTab;
    private int positionTab;

    public SecurityPageEvent() {
    }

    public boolean isSelectMode() {
        return isSelectMode;
    }

    public void setSelectMode(boolean selectMode) {
        isSelectMode = selectMode;
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    public void setSelectAll(boolean selectAll) {
        this.selectAll = selectAll;
    }

    public int getSizeSelected() {
        return sizeSelected;
    }

    public void setSizeSelected(int sizeSelected) {
        this.sizeSelected = sizeSelected;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public int getTabId() {
        return tabId;
    }

    public void setTabId(int tabId) {
        this.tabId = tabId;
    }

    public void setSelectTab(boolean selectTab) {
        this.selectTab = selectTab;
    }

    public boolean isSelectTab() {
        return selectTab;
    }

    public int getPositionTab() {
        return positionTab;
    }

    public void setPositionTab(int positionTab) {
        this.positionTab = positionTab;
    }
}

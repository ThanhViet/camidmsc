/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/11/6
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;

import butterknife.BindView;

public class HistoryDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_phone_number)
    @Nullable
    TextView tvPhoneNumber;
    @BindView(R.id.tv_time_invited)
    @Nullable
    TextView tvTimeInvited;
    @BindView(R.id.tv_data_package)
    @Nullable
    TextView tvDataPackage;
    @BindView(R.id.tv_price)
    @Nullable
    TextView tvPrice;
    @BindView(R.id.tv_title_bonus)
    @Nullable
    TextView tvTitleBonus;
    @BindView(R.id.tv_state)
    @Nullable
    TextView tvState;
    @BindView(R.id.tv_commission)
    @Nullable
    TextView tvCommission;
    @BindView(R.id.tv_title_state)
    @Nullable
    TextView tvTitleState;

    private DataChallenge data;
    private Activity activity;

    public HistoryDetailHolder(View view, final Activity activity) {
        super(view);
        this.activity = activity;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void bindData(Object item, int position) {
        if (activity == null) return;
        if (item instanceof DataChallenge) {
            data = (DataChallenge) item;
            if (tvPhoneNumber != null) {
                tvPhoneNumber.setText(data.getIsdnB());
            }
            if (tvTimeInvited != null) {
                tvTimeInvited.setText(data.getInvitationTime());
            }
            if (tvDataPackage != null) {
                tvDataPackage.setText(data.getDataPackage());
            }
            if (tvPrice != null) {
                tvPrice.setText(TextHelper.formatCurrencyVN(data.getPrice()) + " vnđ");
            }
            //todo 0 – Thành công, 1 – Thất Bại, 2 – Đang chờ, 3 – Tất cả
            if ("0".equals(data.getStatus())) {
                if (tvTitleState != null) tvTitleState.setText(R.string.state);
                if (tvState != null) {
                    tvState.setText(activity.getString(R.string.commission_state_0));
                    tvState.setTextColor(activity.getResources().getColor(R.color.bg_mocha));
                }
                if (tvTitleBonus != null) tvTitleBonus.setText(R.string.bonus_received);
                if (tvCommission != null) {
                    tvCommission.setText(TextHelper.formatCurrencyVN(data.getCommisionLong()) + " vnđ");
                    tvCommission.setTextColor(activity.getResources().getColor(R.color.bg_mocha));
                }
            } else if ("1".equals(data.getStatus())) {
                if (tvTitleState != null) tvTitleState.setText(R.string.state);
                if (tvState != null) {
                    tvState.setText(R.string.commission_state_1);
                    tvState.setTextColor(activity.getResources().getColor(R.color.red));
                }
                if (tvTitleBonus != null) tvTitleBonus.setText(R.string.cause_of_failure);
                if (tvCommission != null) {
                    tvCommission.setText(data.getReason());
                    tvCommission.setTextColor(activity.getResources().getColor(R.color.red));
                }
            } else if (data.getStatus() == null || "2".equals(data.getStatus())) {
                if (tvTitleState != null) tvTitleState.setText(R.string.state);
                if (tvState != null) {
                    tvState.setText(R.string.commission_state_2);
                    tvState.setTextColor(activity.getResources().getColor(R.color.text_title_my_viettel));
                }
                if (tvTitleBonus != null) tvTitleBonus.setText("");
                if (tvCommission != null) tvCommission.setText("");
            } else {
                if (tvTitleState != null) tvTitleState.setText("");
                if (tvState != null) tvState.setText("");
                if (tvTitleBonus != null) tvTitleBonus.setText("");
                if (tvCommission != null) tvCommission.setText("");
            }
        } else {
            data = null;
        }
    }

}
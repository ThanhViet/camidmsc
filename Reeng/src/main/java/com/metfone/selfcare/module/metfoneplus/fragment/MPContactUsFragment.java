package com.metfone.selfcare.module.metfoneplus.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPCallUsDialog;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPContactUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPContactUsFragment extends MPBaseFragment {
    public static final String TAG = "MPContactUsFragment";

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;

    public MPContactUsFragment() {
        // Required empty public constructor
    }


    public static MPContactUsFragment newInstance() {
        MPContactUsFragment fragment = new MPContactUsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_contact_us;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mLayoutActionBar);
        mActionBarTitle.setText(getString(R.string.m_p_support_contact_us_title));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.action_bar_back)
    void onBack() {
        popBackStackFragment();
    }

    @OnClick(R.id.layout_call_us)
    void callUs() {
        MPCallUsDialog callUsDialog = MPCallUsDialog.newInstance(mParentActivity.isFromInternet);
        callUsDialog.show(getParentFragmentManager(), MPCallUsDialog.TAG);
    }

    @OnClick(R.id.layout_feedback_us)
    void feedbackUs() {
        gotoMPFeedbackUs();
    }

    @OnClick(R.id.layout_chat)
    void chat() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.m_p_contact_us_char_url)));
        mParentActivity.startActivity(intent);
    }
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/5
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;

import butterknife.BindView;

public class CommissionDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_phone_number)
    @Nullable
    TextView tvPhoneNumber;
    @BindView(R.id.tv_time_invited)
    @Nullable
    TextView tvTimeInvited;
    @BindView(R.id.tv_data_package)
    @Nullable
    TextView tvDataPackage;
    @BindView(R.id.tv_price)
    @Nullable
    TextView tvPrice;
    @BindView(R.id.tv_title_bonus)
    @Nullable
    TextView tvTitleBonus;
    @BindView(R.id.tv_state)
    @Nullable
    TextView tvState;
    @BindView(R.id.tv_commission)
    @Nullable
    TextView tvCommission;

    private DataChallenge data;
    private Activity activity;

    public CommissionDetailHolder(View view, final Activity activity) {
        super(view);
        this.activity = activity;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void bindData(Object item, int position) {
        if (activity == null) return;
        if (item instanceof DataChallenge) {
            data = (DataChallenge) item;
            if (tvPhoneNumber != null) {
                tvPhoneNumber.setText(data.getIsdnB());
            }
            if (tvTimeInvited != null) {
                tvTimeInvited.setText(data.getInvitationTime());
            }
            if (tvDataPackage != null) {
                tvDataPackage.setText(data.getDataPackage());
            }
            if (tvPrice != null) {
                tvPrice.setText(TextHelper.formatCurrencyVN(data.getPrice()) + " vnđ");
            }
            if (tvState != null)
                tvState.setText(activity.getString(R.string.commission_state_0));
            if (tvCommission != null)
                tvCommission.setText(TextHelper.formatCurrencyVN(data.getCommisionLong()) + " vnđ");
        } else {
            data = null;
        }
    }

}
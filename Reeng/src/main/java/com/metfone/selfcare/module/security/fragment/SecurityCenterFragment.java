/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/25
 */

package com.metfone.selfcare.module.security.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.LoadingView;
import com.metfone.selfcare.module.security.activity.SecurityActivity;
import com.metfone.selfcare.module.security.adapter.SecurityCenterAdapter;
import com.metfone.selfcare.module.security.helper.SecurityApi;
import com.metfone.selfcare.module.security.helper.SecurityHelper;
import com.metfone.selfcare.module.security.listener.ScanVulnerabilityListener;
import com.metfone.selfcare.module.security.model.LogModel;
import com.metfone.selfcare.module.security.model.NewsModel;
import com.metfone.selfcare.module.security.model.PhoneNumberModel;
import com.metfone.selfcare.module.security.model.VulnerabilityModel;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SecurityCenterFragment extends BaseFragment implements RecyclerClickListener, ScanVulnerabilityListener {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingView loadingView;
    private Unbinder unbinder;
    private SecurityCenterAdapter adapter;
    private boolean isLoading;
    private boolean isScanningVulnerability;

    public static SecurityCenterFragment newInstance() {
        Bundle args = new Bundle();
        SecurityCenterFragment fragment = new SecurityCenterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "SecurityCenterFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_security_detail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mActivity instanceof SecurityActivity) {
            ((SecurityActivity) mActivity).setTitle(R.string.security_center);
        }
        initView();
        if (loadingView != null) {
            loadingView.loadBegin(R.drawable.circular_progress_bar_security);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadVulnerability();
                loadData();
            }
        }, 400);
    }

    private void loadVulnerability() {
        long oldTimeScan = SharedPref.newInstance(mActivity).getLong(Constants.PREFERENCE.PREF_SECURITY_TIME_SCAN_VULNERABILITY, 0);
        if (oldTimeScan > 0) {
            ArrayList<VulnerabilityModel> list = new ArrayList<>();
            ArrayList<String> tmp = SharedPref.newInstance(mActivity).getListString(Constants.PREFERENCE.PREF_SECURITY_VULNERABILITY_ON_DEVICE);
            for (int i = 0; i < tmp.size(); i++) {
                VulnerabilityModel item = SecurityHelper.getVulnerability(tmp.get(i));
                if (item != null) list.add(item);
            }
            isScanningVulnerability = false;
            if (adapter != null) {
                adapter.setVulnerabilities(list);
                adapter.setScanVulnerability(true);
                adapter.notifyDataSetChanged();
            }
            loadingFinish();
        } else {
            scanVulnerability();
        }
    }

    private void initView() {
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        }
        adapter = new SecurityCenterAdapter(mActivity);
        adapter.setScanVulnerability(false);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    private void scanVulnerability() {
        Log.i(TAG, "scanVulnerability begin");
        isScanningVulnerability = true;
        SecurityHelper.ScanVulnerabilityTask task = new SecurityHelper.ScanVulnerabilityTask();
        task.setContext(mActivity);
        task.setListener(this);
        task.execute();
    }

    private void loadData() {
        Log.i(TAG, "loadData begin");
        if (isLoading) return;
        isLoading = true;
        String tmp = SharedPref.newInstance(mActivity).getString(Constants.PREFERENCE.PREF_SECURITY_NEWS_LIST, "");
        if (!TextUtils.isEmpty(tmp))
            try {
                ArrayList<NewsModel> data = new Gson().fromJson(tmp, new TypeToken<ArrayList<NewsModel>>() {
                }.getType());
                if (adapter != null) {
                    adapter.setNewsModels(data);
                    adapter.notifyDataSetChanged();
                }
                isLoading = false;
                loadingFinish();
            } catch (Exception e) {
                e.printStackTrace();
            }

        SecurityApi.getInstance().getNews(new ApiCallbackV2<ArrayList<NewsModel>>() {
            @Override
            public void onSuccess(String lastId, ArrayList<NewsModel> newsModels) throws JSONException {
                if (adapter != null) {
                    adapter.setNewsModels(newsModels);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(String s) {
            }

            @Override
            public void onComplete() {
                isLoading = false;
                loadingFinish();
                Log.i(TAG, "loadData finish");
            }
        });
    }

    private void loadingFinish() {
        Log.i(TAG, "loadingFinish isLoading: " + isLoading + ", isScanningVulnerability: " + isScanningVulnerability);
        if (!isLoading && !isScanningVulnerability) {
            if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
            if (loadingView != null) loadingView.loadFinish();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
//                    if (loadingView != null) loadingView.loadFinish();
//                }
//            }, 200);
        }
    }

    @Override
    public void scanCompleted(ArrayList<VulnerabilityModel> list) {
        Log.i(TAG, "scanVulnerability completed");
        ArrayList<String> tmp = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            tmp.add(list.get(i).getKey());
        }
        SharedPref.newInstance(mActivity).putListString(Constants.PREFERENCE.PREF_SECURITY_VULNERABILITY_ON_DEVICE, tmp);
        SharedPref.newInstance(mActivity).putLong(Constants.PREFERENCE.PREF_SECURITY_TIME_SCAN_VULNERABILITY, new Date().getTime());
        isScanningVulnerability = false;
        if (adapter != null) {
            adapter.setVulnerabilities(list);
            adapter.setScanVulnerability(true);
            adapter.notifyDataSetChanged();
        }
        loadingFinish();
    }

    @Override
    public void onClick(View v, int pos, Object object) {
        if (object instanceof LogModel) {

        } else if (object instanceof PhoneNumberModel) {

        } else if (object instanceof VulnerabilityModel) {
            NavigateActivityHelper.navigateToTabSecurityVulnerability(mActivity, (VulnerabilityModel) object);
        } else if (object instanceof NewsModel) {
            NavigateActivityHelper.navigateToTabSecurityNews(mActivity, (NewsModel) object);
        }
    }

    @Override
    public void onLongClick(View v, int pos, Object object) {

    }
}

package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;

import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPMoreServiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPMoreServiceFragment extends MPBaseFragment {
    public static final String TAG = "MPMoreServiceFragment";
    private RelativeLayout layoutBuyService;
    public MPMoreServiceFragment() {
        // Required empty public constructor
    }

    public static MPMoreServiceFragment newInstance() {
        MPMoreServiceFragment fragment = new MPMoreServiceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mp_more_service;
    }

    @OnClick(R.id.ll_close_more_services)
    void closeMoreServices() {
        popBackStackFragment();
    }

    @OnClick(R.id.layout_top_up)
    void gotoTopUpScreen() {
        Toast.makeText(getContext(), "Go to top up", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.layout_buy_services)
    void gotoBuyServicesScreen() {
        Toast.makeText(getContext(), "Go to buy service", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.layout_buy_phone_number)
    void gotoBuyPhoneNumberScreen() {
        Toast.makeText(getContext(), "Go to buy phone number", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.layout_i_share)
    void gotoIShareScreen() {

    }

    @OnClick(R.id.layout_support)
    void gotoSupportScreen() {

    }
}
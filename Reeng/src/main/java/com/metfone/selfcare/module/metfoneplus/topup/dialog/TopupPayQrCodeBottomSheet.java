package com.metfone.selfcare.module.metfoneplus.topup.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.TopupPayQrcodeBottomSheetBinding;
import com.metfone.selfcare.module.metfoneplus.topup.interfacelistener.RunUi;

import static com.metfone.selfcare.module.metfoneplus.topup.model.Constants.CALL_PAY;
import static com.metfone.selfcare.module.metfoneplus.topup.model.Constants.CANCEL;

public class TopupPayQrCodeBottomSheet extends Dialog {
    private TopupPayQrcodeBottomSheetBinding mDialogBinding;
    private RunUi mRunUi;
    private Activity mActivity;
    private String mMess;

    public TopupPayQrCodeBottomSheet(@NonNull Context context, String Mess, Activity activity, RunUi runUi) {
        super(context);
        this.mMess = Mess;
        this.mRunUi = runUi;
        this.mActivity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialogBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.topup_pay_qrcode_bottom_sheet, null, false);
        setContentView(mDialogBinding.getRoot());
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setCancelable(true);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setGravity(Gravity.BOTTOM);
        //set tỉ lệ dialog so với chiều ngang của màn hình - 90% so với chiều ngang của màn
        int windowsScale = this.getContext().getResources().getDisplayMetrics().widthPixels;
        windowsScale = (int) ((float) windowsScale * 1.15f);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, windowsScale);

        listener();
    }

    private void listener() {
        mDialogBinding.tvMessage.setText("Call " + mMess);
        mDialogBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRunUi.run(CANCEL);
                dismiss();
            }
        });
        mDialogBinding.tvMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRunUi.run(CALL_PAY);
                dismiss();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
    }

}

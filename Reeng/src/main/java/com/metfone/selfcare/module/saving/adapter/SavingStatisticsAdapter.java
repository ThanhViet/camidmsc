/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/19
 */

package com.metfone.selfcare.module.saving.adapter;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.base.BaseViewHolder;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.saving.model.SavingStatisticsModel;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.util.Utilities;

import java.util.List;

public class SavingStatisticsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = SavingStatisticsAdapter.class.getSimpleName();
    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private List<SavingStatisticsModel> data;
    private Resources mRes;

    public SavingStatisticsAdapter(BaseSlidingFragmentActivity activity, List<SavingStatisticsModel> data) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplication();
        this.data = data;
        this.mRes = mApplication.getResources();
    }

    @Override
    public int getItemViewType(int position) {
        SavingStatisticsModel item = getItem(position);
        if (item != null) {
            return item.getType().VALUE;
        }
        return -1;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == SavingStatisticsModel.Type.PROMOTION.VALUE) {
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_promotion_home, parent, false);
        } else if (viewType == SavingStatisticsModel.Type.SAVING.VALUE) {
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_saving_home, parent, false);
        } else
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_empty, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemHolder, int position) {
        final SavingStatisticsModel item = getItem(position);
        int viewType = getItemViewType(position);
        if (itemHolder instanceof BaseViewHolder) {
            BaseViewHolder holder = (BaseViewHolder) itemHolder;
            if (item != null) {
                if (viewType == SavingStatisticsModel.Type.PROMOTION.VALUE) {
                    RecyclerView recyclerView = holder.getView(R.id.recycler);
                    if (recyclerView.getItemDecorationCount() <= 0) {
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setNestedScrollingEnabled(false);
                        recyclerView.setLayoutManager(new CustomLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
                    }
                    PromotionAdapter adapter = new PromotionAdapter(activity, item.getPromotion());
                    recyclerView.setAdapter(adapter);
                } else if (viewType == SavingStatisticsModel.Type.SAVING.VALUE) {
                    if (item.getSavingStatistics() != null) {
                        holder.setText(R.id.tv_datetime, item.getSavingStatistics().getRangeDate());
                        boolean hasData = item.getSavingStatistics().getTotalSavingInt() > 0;
                        if (hasData) {
                            holder.setVisible(R.id.layout_data, true);
                            holder.setVisible(R.id.layout_no_data, false);
                            PieChart chart = holder.getView(R.id.chart_saving);
                            Utilities.drawChartSaving(activity, item.getSavingStatistics(), chart, false);
                            holder.setText(R.id.tv_total_saving, item.getSavingStatistics().getTotalSaving());
                            holder.setText(R.id.tv_sms_out_saving_detail, item.getSavingStatistics().getSmsOutSaving());
                            holder.setText(R.id.tv_call_out_saving_detail, item.getSavingStatistics().getCallOutSaving());
                            holder.setText(R.id.tv_data_saving_detail, item.getSavingStatistics().getDataSaving());
                            holder.setVisible(R.id.layout_sms_out_saving_detail, Utilities.notEmpty(item.getSavingStatistics().getSmsOutSaving()));
                            holder.setVisible(R.id.layout_call_out_saving_detail, Utilities.notEmpty(item.getSavingStatistics().getCallOutSaving()));
                            holder.setVisible(R.id.layout_data_saving_detail, Utilities.notEmpty(item.getSavingStatistics().getDataSaving()));
                            holder.setOnClickListener(R.id.button_help_saving, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (activity != null && item != null && item.getDialogInfo() != null) {
                                        DialogMessage dialog = new DialogMessage(activity, true);
                                        dialog.setLabel(item.getDialogInfo().getTitle());
                                        dialog.setMessage(item.getDialogInfo().getBody());
                                        dialog.setLabelButton(item.getDialogInfo().getLabel());
                                        dialog.show();
                                    }
                                }
                            });
                            holder.setOnClickListener(R.id.button_view_detail, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (activity != null)
                                        NavigateActivityHelper.navigateToTabSavingSearch(activity);
                                }
                            });
                            holder.setOnClickListener(R.id.button_invite, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (activity != null)
                                        NavigateActivityHelper.navigateToInviteFriends(activity, Constants.CHOOSE_CONTACT
                                                .TYPE_INVITE_FRIEND);
                                }
                            });
                        } else {
                            holder.setVisible(R.id.layout_data, false);
                            holder.setVisible(R.id.layout_no_data, true);
                        }
                    }
                }
            }
        }
    }

    public SavingStatisticsModel getItem(int position) {
        if (data != null && data.size() > position && position >= 0)
            return data.get(position);
        return null;
    }

    @Override
    public int getItemCount() {
        if (data == null || data.isEmpty())
            return 0;
        return data.size();
    }
}

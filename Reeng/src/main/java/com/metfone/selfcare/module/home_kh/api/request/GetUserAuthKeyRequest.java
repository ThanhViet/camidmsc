package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class GetUserAuthKeyRequest extends BaseRequest<GetUserAuthKeyRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("gameCode")
        String gameCode;

        public WsRequest(String isdn, String gameCode) {
            this.isdn = isdn;
            this.gameCode = gameCode;
        }
    }
}

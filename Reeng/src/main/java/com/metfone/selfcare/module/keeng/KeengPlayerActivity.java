package com.metfone.selfcare.module.keeng;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseActivity;
import com.metfone.selfcare.module.keeng.event.CommentEvent;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.module.keeng.event.LikeEvent;
import com.metfone.selfcare.module.keeng.fragment.player.HomePlayerFragment;
import com.metfone.selfcare.module.keeng.fragment.player.LyricPlayerFragment;
import com.metfone.selfcare.module.keeng.fragment.player.RelatePlayerFragment;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.keeng.widget.floatingView.MusicFloatingView;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.dialog.BottomSheetDialog;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.easyvideoplayer.Util;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.LISTENER_MUSIC;

public class KeengPlayerActivity extends BaseActivity implements PlayMusicController.OnPlayMusicStateChange,
        View.OnClickListener, ViewPager.OnPageChangeListener, LyricPlayerFragment.ClickItemListener {
    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    public static final float RATIO_SCALE = 0.35f;
    private final String TAG = getClass().getSimpleName();

    private ApplicationController mApplication;
    private PlayMusicController playMusicController;

    private ImageView btnPrev;
    private ImageView btnPlay;
    private ImageView btnNext;
    private ImageView btnPlayRepeatMode;
    private ImageView btnPlayShuffleMode;
    private View layoutPlayLoading, btnPlayLoading;
    private TextView tvProcess;
    private TextView tvDuration;
    private TextView tvSongName;
    private TextView tvSongSinger;
    private TextView tvTitle;
    private SeekBar seekBar;
    private BottomSheetDialog optionBottomDialog;

    private ViewPager viewPager;
    private ViewPagerDetailAdapter playerAdapter;
    private LyricPlayerFragment lyricPlayerFragment;

    private MediaModel currentSong;
    private RelatePlayerFragment relatePlayerFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keeng_player);
        mApplication = (ApplicationController) getApplicationContext();
        playMusicController = mApplication.getPlayMusicController();
        playMusicController.setRunningKeengPlayerActivity(true);
        //Stop Floating service
//        KeengFloatingViewService.stop(this);
        MusicFloatingView.stop(this);
        VideoService.stop(this);

        viewPager = findViewById(R.id.view_pager);
        ImageView btnBack = findViewById(R.id.btnBack);
        btnPrev = findViewById(R.id.btnPrev);
        btnPlay = findViewById(R.id.btnPlay);
        layoutPlayLoading = findViewById(R.id.layout_play_loading);
        btnPlayLoading = findViewById(R.id.btnPlayLoading);
        btnNext = findViewById(R.id.btnNext);
        btnPlayRepeatMode = findViewById(R.id.btnPlayRepeatMode);
        btnPlayShuffleMode = findViewById(R.id.btnPlayShuffleMode);
        ImageView btnMoreOption = findViewById(R.id.btnMoreOption);
        tvProcess = findViewById(R.id.tvProcess);
        tvDuration = findViewById(R.id.tvDuration);
        seekBar = findViewById(R.id.seekBar);
        tvSongName = findViewById(R.id.tvSongName);
        tvSongSinger = findViewById(R.id.tvSongSinger);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setOnClickListener(this);
        TextView tvLyric = findViewById(R.id.tvLyric);
        btnBack.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPlayRepeatMode.setOnClickListener(this);
        btnPlayShuffleMode.setOnClickListener(this);
        btnMoreOption.setOnClickListener(this);
        tvLyric.setOnClickListener(this);
        setupViewPager();
        loadData();
        PlayMusicController.addPlayMusicStateChange(this);

        if (playMusicController != null) {
            int stateRepeat = playMusicController.getStateRepeat();
            if (stateRepeat == Constants.PLAY_MUSIC.REPEAT_All
                    || stateRepeat == Constants.PLAY_MUSIC.REPEAT_ONE) {
                btnPlayRepeatMode.setImageLevel(stateRepeat);
            }

            int stateShuffle = playMusicController.getStateShuffle();
            if (stateShuffle == Constants.PLAY_MUSIC.REPEAT_SUFF
                    || stateShuffle == Constants.PLAY_MUSIC.REPEAT_SUFF_OFF) {
                btnPlayShuffleMode.setImageLevel(stateShuffle);
            }
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (playMusicController != null && playMusicController.isPrepared()) {
                    MediaPlayer mediaPlayer = playMusicController.getMediaPlayer();
                    if (mediaPlayer != null) {
                        int currentTotalDuration = mediaPlayer.getDuration();
                        int currentPosition = DateTimeUtils.progressToTimer(seekBar.getProgress(), currentTotalDuration);
                        if (currentTotalDuration >= 0 && currentPosition >= 0) {
                            mediaPlayer.seekTo(currentPosition);
                        }
                    }
                }
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                try {
                    if (playMusicController != null && playMusicController.isPrepared()) {
                        MediaPlayer mediaPlayer = playMusicController.getMediaPlayer();
                        if (mediaPlayer != null) {
                            int currentTotalDuration = mediaPlayer.getDuration();
                            int currentPosition = DateTimeUtils.progressToTimer(seekBar.getProgress(), currentTotalDuration);
                            tvDuration.setText(DateTimeUtils.milliSecondsToTimer(currentTotalDuration));
                            tvProcess.setText(DateTimeUtils.milliSecondsToTimer(currentPosition));
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (playMusicController != null) playMusicController.setRunningKeengPlayerActivity(false);
        super.onDestroy();

    }

    private void loadData() {
        currentSong = playMusicController.getCurrentSong();
        if (currentSong != null) {
            updateSongInfo(currentSong.getUrl());
            if (playMusicController != null) {
                if (playMusicController.isPrepared()) {
                    seekBar.setSecondaryProgress(playMusicController.getCurrentBuffer());
                    setProgress(playMusicController.getCurrentProgress(), playMusicController.getCurrentMediaPosition());
                    tvDuration.setText(playMusicController.getDurationString());
                }
                setStateMediaPlayer(playMusicController.getStatePlaying());
            }
            updateSongName();
            if (lyricPlayerFragment != null && lyricPlayerFragment.isVisible())
                lyricPlayerFragment.updateAllView(currentSong);
            if (relatePlayerFragment != null && relatePlayerFragment.isVisible())
                relatePlayerFragment.updateData(playMusicController);
        }
    }

    private void updateSongName() {
        new Handler().postDelayed(() -> {
            tvSongSinger.setText(currentSong.getSinger());
            tvSongName.setText(currentSong.getName());
            tvTitle.setText(playMusicController.getCurrentPlayListName());
        }, 200);
    }

    private void updateData() {
        currentSong = playMusicController.getCurrentSong();
        if (currentSong != null) {
            tvDuration.setText(playMusicController.getDurationString());
        }
    }

    private boolean isOpenMini() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this);
    }

    @Override
    public void onBackPressed() {
        onBack(true);
    }

    private void onBack(boolean isBackPress) {
        PlayMusicController.removePlayMusicStateChange(this);
        //Neu pause thi exit luon
        if (!playMusicController.isPlaying() && playMusicController.isPrepared()) {
            stopMusic();
        } else {
            if (isOpenMini()) {
                showFloatingMusic();
                finish();
            } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                //Hien popup xin quyen
                DialogConfirm dialogConfirm = new DialogConfirm(this, true);
                dialogConfirm.setLabel(getString(R.string.permission_allow_floating_view));
                dialogConfirm.setMessage(getString(R.string.permission_allow_floating_view_content));
                dialogConfirm.setUseHtml(true);
                dialogConfirm.setNegativeLabel(getString(R.string.cancel));
                dialogConfirm.setPositiveLabel(getString(R.string.ok));
                dialogConfirm.setPositiveListener(result -> {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
                });
                dialogConfirm.setNegativeListener(result -> {
                    //stopMusic();
                    finish();
                });
                dialogConfirm.show();
            } else {
                //stopMusic();
                finish();
            }

        }
    }

    //Implement music
    @Override
    public void onChangeStateRepeat(int state) {

    }

    @Override
    public void onChangeStateNone() {
        seekBar.setProgress(0);
        seekBar.setSecondaryProgress(0);
    }

    @Override
    public void onChangeStateGetData() {
        runOnUiThread(() -> {
//                setStateMediaPlayer(playMusicController.getStatePlaying());
        });
    }

    @Override
    public void onChangeStatePreparing(MediaModel song) {
        runOnUiThread(() -> setStateMediaPlayer(playMusicController.getStatePlaying()));
    }

    @Override
    public void onChangeStatePlaying(MediaModel song) {
        runOnUiThread(() -> {
            updateData();
            setStateMediaPlayer(playMusicController.getStatePlaying());
        });
    }

    @Override
    public void onActionFromUser(MediaModel song) {

    }

    @Override
    public void onCloseMusic() {
        PlayMusicController.removePlayMusicStateChange(this);
        finish();
    }

    @Override
    public void onCallLoadDataFail(String message) {

    }

    @Override
    public void onUpdateSeekBarProgress(final int percent, final int currentPosition) {
        runOnUiThread(() -> {
            setProgress(percent, currentPosition);
            if (lyricPlayerFragment != null && lyricPlayerFragment.isVisible())
                lyricPlayerFragment.updateLyricView(percent, currentPosition);
        });
    }

    @Override
    public void onUpdateSeekBarBuffering(int percent) {
        seekBar.setSecondaryProgress(percent);
    }

    @Override
    public void onChangeStateInfo(final int state) {
        runOnUiThread(() -> {
            if (playMusicController != null && !playMusicController.isBufferFirst())
                setStateMediaPlayer(state);
        });
    }

    @Override
    public void onUpdateData(MediaModel song) {
        if (viewPager.getCurrentItem() != playMusicController.getCurrentSongIndex()) {
            viewPager.setCurrentItem(playMusicController.getCurrentSongIndex());
            return;
        }
        loadData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTitle:
                showRelatePlayerFragment();
                break;
            case R.id.btnBack:
                onBack(false);
                break;
            case R.id.llOptionLike:
                processWhenClickLike();
                closeOptionDialog();
                break;
            case R.id.llOptionComment:
                if (currentSong != null) {
                    NavigateActivityHelper.navigateToOnMediaDetail(this,
                            currentSong.getUrl(),
                            Constants.ONMEDIA.COMMENT,
                            true,
                            FeedModelOnMedia.convertMediaToFeedModelOnMedia(currentSong), Constants.ONMEDIA.FEED_TAB_MUSIC);
                }
                closeOptionDialog();
                break;
            case R.id.llOptionShare:
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    showDialogLogin();
                } else {
                    ShareUtils.openShareMenu(this, currentSong);
                }
                closeOptionDialog();
//                if (currentSong != null)
//                    new FacebookHelper(this).shareContentToFacebook(this, getCallbackManager(), currentSong.getUrl(),
//                            null, null, null, null);
                break;
            case R.id.llOptionListen:
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    DeepLinkHelper.getInstance().openSchemaLink(this, LISTENER_MUSIC);
                } else {
                    NavigateActivityHelper.navigateToChooseContact(this, Constants.CHOOSE_CONTACT.TYPE_TOGETHER_MUSIC,
                            null, currentSong, null, false, true, -1, true);
                }
                closeOptionDialog();
                break;
            case R.id.btnPlay:
                processWhenClickPlay();
                break;
            case R.id.btnPrev:
                playMusicController.playForwardMusic();
                break;
            case R.id.btnNext:
                playMusicController.playNextMusic();
                break;
            case R.id.btnPlayRepeatMode:
                int level = btnPlayRepeatMode.getDrawable().getLevel();
                if (level == Constants.PLAY_MUSIC.REPEAT_All) {
                    btnPlayRepeatMode.setImageLevel(Constants.PLAY_MUSIC.REPEAT_ONE);
                    playMusicController.setStateRepeat(Constants.PLAY_MUSIC.REPEAT_ONE);
                    ToastUtils.makeText(this, getString(R.string.repeat_one), Toast.LENGTH_SHORT);
                } else if (level == Constants.PLAY_MUSIC.REPEAT_ONE) {
                    btnPlayRepeatMode.setImageLevel(Constants.PLAY_MUSIC.REPEAT_All);
                    playMusicController.setStateRepeat(Constants.PLAY_MUSIC.REPEAT_All);
                    ToastUtils.makeText(this, getString(R.string.repeat_all), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.btnPlayShuffleMode:
                int shuffleLevel = btnPlayShuffleMode.getDrawable().getLevel();
                if (shuffleLevel == Constants.PLAY_MUSIC.REPEAT_SUFF_OFF) {
                    btnPlayShuffleMode.setImageLevel(Constants.PLAY_MUSIC.REPEAT_SUFF);
                    playMusicController.setStateShuffle(Constants.PLAY_MUSIC.REPEAT_SUFF);
                    ToastUtils.makeText(this, getString(R.string.repeat_shuffe), Toast.LENGTH_SHORT);
                } else if (shuffleLevel == Constants.PLAY_MUSIC.REPEAT_SUFF) {
                    btnPlayShuffleMode.setImageLevel(Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
                    playMusicController.setStateShuffle(Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
                    ToastUtils.makeText(this, getString(R.string.repeat_shuffe_off), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.btnMoreOption:
                showOptionBottomDialog();
                break;
            case R.id.tvLyric:
                showLyricBottomDialog();
                break;
            case R.id.llCloseOptionDialog:
                closeOptionDialog();
                break;
        }
    }

    private void processWhenClickPlay() {
        playMusicController.toggleMusic();
        playMusicController.setStatePauseChangeSong(false);
    }

    private void processWhenClickLike() {
        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            showDialogLogin();
        } else {
            if (currentSong != null) {
                FeedModelOnMedia feed = FeedModelOnMedia.convertMediaToFeedModelOnMedia(currentSong);
                WSOnMedia rest = new WSOnMedia(mApplication);
                rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(),
                        !currentSong.isLike() ? FeedModelOnMedia.ActionLogApp.LIKE : FeedModelOnMedia.ActionLogApp.UNLIKE,
                        "", feed.getBase64RowId(), "", FeedModelOnMedia.ActionFrom.mochavideo,
                        response -> Log.i(TAG, "onResponse: " + response), volleyError -> Log.e(TAG, "onErrorResponse: "));

                if (currentSong.isLike()) {
                    currentSong.setLike(false);
                    showToast(R.string.music_un_like);
                } else {
                    currentSong.setLike(true);
                    showToast(R.string.music_like);
                }
                if (lyricPlayerFragment != null && lyricPlayerFragment.isVisible())
                    lyricPlayerFragment.updateLikeState(currentSong);
            }
        }
    }

    private void closeOptionDialog() {
        if (optionBottomDialog != null && optionBottomDialog.isShowing()) {
            optionBottomDialog.dismiss();
            optionBottomDialog = null;
        }
    }

    private void showOptionBottomDialog() {
        closeOptionDialog();
        if (currentSong == null) {
            return;
        }
        optionBottomDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_player_option, null, false);
        sheetView.findViewById(R.id.llOptionLike).setOnClickListener(this);
        sheetView.findViewById(R.id.ivOptionLike).setSelected(currentSong.isLike());
        sheetView.findViewById(R.id.llOptionComment).setOnClickListener(this);
        ((TextView) sheetView.findViewById(R.id.tvOptionComment)).setText(R.string.music_option_comment);
        if (currentSong.getTotalComment() > 0) {
            ((TextView) sheetView.findViewById(R.id.tvOptionComment)).setText(getString(R.string.music_option_comment_count,
                    (int) currentSong.getTotalComment()));
        }
        sheetView.findViewById(R.id.llOptionShare).setOnClickListener(this);
        sheetView.findViewById(R.id.llOptionListen).setOnClickListener(this);
        sheetView.findViewById(R.id.llCloseOptionDialog).setOnClickListener(this);
        optionBottomDialog.setContentView(sheetView);
        if (!optionBottomDialog.isShowing())
            optionBottomDialog.show();
    }

    private void closeLyricDialog() {
        if (lyricPlayerFragment != null) {
            lyricPlayerFragment.dismiss();
        }
    }

    private void showLyricBottomDialog() {
        closeLyricDialog();
        if (currentSong == null) {
            return;
        }
        if (playMusicController != null) {
            lyricPlayerFragment = LyricPlayerFragment.newInstance(currentSong, playMusicController, this);
        } else {
            lyricPlayerFragment = LyricPlayerFragment.newInstance(currentSong, this);
        }
        lyricPlayerFragment.show(getSupportFragmentManager(), LyricPlayerFragment.TAG);
    }

    private void showRelatePlayerFragment() {
        relatePlayerFragment = RelatePlayerFragment.newInstance();
        relatePlayerFragment.updateData(playMusicController);
        relatePlayerFragment.show(getSupportFragmentManager(), RelatePlayerFragment.TAG);
    }

    public void removeSongPlaying(int index) {
        try {
            //Remove 1 bai hat trong danh sach phat
            if (index == playMusicController.getCurrentSongIndex()) {
                playerAdapter.removeFragment(index);
                playMusicController.getDataSong().remove(index);
                if (playMusicController.getDataSong().size() > 0) {
                    playMusicController.playSong(false, index);

                    if (relatePlayerFragment != null && relatePlayerFragment.isVisible())
                        relatePlayerFragment.updateData(playMusicController);

                } else {
                    stopMusic();
                }
            } else if (index < playMusicController.getCurrentSongIndex()) {
                int currentSongIndex = Math.max((playMusicController.getCurrentSongIndex() - 1), 0);
                playMusicController.getDataSong().remove(index);
                playMusicController.setCurrentSongIndex(currentSongIndex);
                playerAdapter.removeFragment(index);
                viewPager.setCurrentItem(currentSongIndex);
                if (relatePlayerFragment != null && relatePlayerFragment.isVisible())
                    relatePlayerFragment.updateData(playMusicController);
            } else {
                playerAdapter.removeFragment(index);
                playMusicController.getDataSong().remove(index);
                if (relatePlayerFragment != null && relatePlayerFragment.isVisible())
                    relatePlayerFragment.updateData(playMusicController);
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
        }
    }

    private void showFloatingMusic() {
//        startService(new Intent(this, KeengFloatingViewService.class));
        final Intent intent = new Intent(this, MusicFloatingView.class);
//        intent.putExtra(MusicFloatingView.EXTRA_CUTOUT_SAFE_AREA, FloatingViewManager.findCutoutSafeArea(this));
        startService(intent);
    }

    public void setStateMediaPlayer(int stateMediaPlayer) {
        switch (stateMediaPlayer) {
            case Constants.PLAY_MUSIC.PLAYING_NONE:
            case Constants.PLAY_MUSIC.PLAYING_GET_INFO:
            case Constants.PLAY_MUSIC.PLAYING_PREPARING:
            case Constants.PLAY_MUSIC.PLAYING_RETRIEVING:
                drawStateAction(true);
                setProgress(0, 0);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PLAYING:
                drawStateAction(false);
                btnPlay.setImageResource(R.drawable.ic_music_stop);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PAUSED:
                drawStateAction(false);
                btnPlay.setImageResource(R.drawable.ic_music_play);
                break;
            case Constants.PLAY_MUSIC.PLAYING_BUFFERING_START:
                drawStateAction(true);
                break;
            case Constants.PLAY_MUSIC.PLAYING_BUFFERING_END:
                drawStateAction(false);
                break;
            default:
                drawStateAction(true);
                setProgress(0, 0);
                break;
        }
        if (lyricPlayerFragment != null && lyricPlayerFragment.isVisible()) {
            lyricPlayerFragment.updateMediaView(playMusicController);
        }
    }

    private void drawStateAction(boolean isLoading) {
        if (isLoading)//Loading du lieu
        {
            btnNext.setEnabled(false);
            btnPrev.setEnabled(false);
            btnPlay.setVisibility(View.INVISIBLE);

            layoutPlayLoading.setVisibility(View.VISIBLE);

            if (btnPlayLoading.getAnimation() == null) {
                btnPlayLoading.clearAnimation();
                //Chay animation btnPlayLoading
                RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 1.0f * 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotateAnimation.setDuration(1000);
                rotateAnimation.setInterpolator(new LinearInterpolator());
                rotateAnimation.setRepeatCount(Animation.INFINITE);
                rotateAnimation.setRepeatMode(Animation.INFINITE);
                btnPlayLoading.startAnimation(rotateAnimation);
            }
        } else {
            layoutPlayLoading.setVisibility(View.INVISIBLE);
            btnPlayLoading.clearAnimation();

            btnPlay.setVisibility(View.VISIBLE);
            btnNext.setEnabled(true);
            btnPrev.setEnabled(true);
        }
    }

    public void setProgress(int progress, int currentPosition) {
        seekBar.setProgress(progress);
        tvProcess.setText(Util.getDurationString(currentPosition, false));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    int threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    onBack(false);
                    break;
                default:
                    break;
            }
        }
    }

    private void setupViewPager() {
        playerAdapter = new ViewPagerDetailAdapter(getSupportFragmentManager());
        for (MediaModel mediaModel : playMusicController.getDataSong()) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(CommonUtils.KEY_SONG, mediaModel);
            playerAdapter.addFragment(HomePlayerFragment.newInstance(bundle), getString(R.string.song));
        }
        int padding = (mApplication.getWidthPixels() - Utilities.dpToPx(250)) / 2; //size of cover image + padding 20*2
        viewPager.setPadding(padding, 0, padding, 0);
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(playerAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setClipToPadding(false);
        viewPager.setCurrentItem(playMusicController.getCurrentSongIndex());
        playerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        HomePlayerFragment sampleFragment = (HomePlayerFragment) playerAdapter.getItem(position);
        float scale = 1 - (positionOffset * RATIO_SCALE);
        sampleFragment.scaleImage(scale);
        if (position + 1 < playerAdapter.getCount()) {
            sampleFragment = (HomePlayerFragment) playerAdapter.getItem(position + 1);
            scale = positionOffset * RATIO_SCALE + (1 - RATIO_SCALE);
            sampleFragment.scaleImage(scale);
        }
    }

    @Override
    public void onPageSelected(int position) {
        playSong(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            HomePlayerFragment fragment = (HomePlayerFragment) playerAdapter.getItem(viewPager.getCurrentItem());
            fragment.scaleImage(1);
            if (viewPager.getCurrentItem() > 0) {
                fragment = (HomePlayerFragment) playerAdapter.getItem(viewPager.getCurrentItem() - 1);
                fragment.scaleImage(1 - RATIO_SCALE);
            }

            if (viewPager.getCurrentItem() + 1 < playerAdapter.getCount()) {
                fragment = (HomePlayerFragment) playerAdapter.getItem(viewPager.getCurrentItem() + 1);
                fragment.scaleImage(1 - RATIO_SCALE);
            }
        }
    }

    public void playSong(int index) {
        if (playMusicController.isPlaying() && index == playMusicController.getCurrentSongIndex()) {
            return;
        }
        playMusicController.playSong(false, index);
    }

    private void updateSongInfo(String url) {
        new BaseApi(mApplication).getDetailUrl(url, false, new ApiCallbackV2<RestAllFeedsModel>() {
            @Override
            public void onSuccess(String msg, RestAllFeedsModel result) {
                if (currentSong != null) {
                    if (result != null && Utilities.notEmpty(result.getData())) {
                        FeedModelOnMedia feedModel = result.getData().get(0);
                        if (feedModel != null) {
                            currentSong.setLike(feedModel.isLike());
                            currentSong.setShare(feedModel.isShare());
                            FeedContent feedContent = feedModel.getFeedContent();
                            if (feedContent != null) {
                                currentSong.setTotalLike(feedContent.getCountLike());
                                currentSong.setTotalShare(feedContent.getCountShare());
                                currentSong.setTotalComment(feedContent.getCountComment());
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void stopMusic() {
        //Xoa all, tat music
        mApplication.getPlayMusicController().closeMusic();
        PlayMusicController.removePlayMusicStateChange(this);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventHelper.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventHelper.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onEvent(LikeEvent event) {
        if (event != null) {
            if (event.isLike()) {
                currentSong.setLike(true);
            } else {
                currentSong.setLike(false);
            }
            EventHelper.removeStickyEvent(event);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onEvent(CommentEvent event) {
        if (event != null) {
            if (currentSong != null && !TextUtils.isEmpty(currentSong.getUrl())) {
                updateSongInfo(currentSong.getUrl());
            }
            EventHelper.removeStickyEvent(event);
        }
    }

    @Override
    public void onClickLyricLike() {
        processWhenClickLike();
//        closeLyricDialog();
    }

    @Override
    public void onClickLyricPlay() {
        processWhenClickPlay();
//        closeLyricDialog();
    }

    @Override
    public void onClickLyricExit() {
        closeLyricDialog();
    }
}

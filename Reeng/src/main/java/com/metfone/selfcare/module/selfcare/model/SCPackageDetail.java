package com.metfone.selfcare.module.selfcare.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SCPackageDetail implements Serializable {

//    {
//        "errorCode": 0,
//            "result": {
//        "name": "VOICE DAILY",
//                "code": "V40",
//                "fullDes": "ရက္ 1 သုံး\nခ်ဳိသာဆုံးေစ်းႏႈန္းႏွင့္ Mytel အခ်င္းခ်င္းဖုန္းေခၚဆုိမႈအတြက္\n40 မိနစ္ ႏွင့္ SMS 40 အား 200 က်ပ္ (အခြန္ပါ၀င္ၿပီး)\nျဖင့္၀ယ္ယူႏိုင္ပါည္။ ယခုပဲ၀ယ္ယူလိုက္ေတာ့ေနာ္။ <br><p></p><p></p><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:\nnormal\">\n\nေစ်းႏႈန္း - 200 က်ပ္</p><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:\nnormal\">ပမာဏ - Mytel အခ်င္းခ်င္းဖုန္းေခၚဆိုမႈအတြက္ 40 မိနစ္</p><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:\nnormal\">သက္တမ္း - 1 ရက္&nbsp;</p><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:\nnormal\">ပရိုမိုးရွင္း လက္ေဆာင္ - 40 SMS on-net</p><p></p><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:\nnormal\">ပရုိမိုးရွင္း သက္တမ္း - 1 ရက္</p><p></p><p></p><p>\n\nမွတ္ခ်က္။မၾကာမီကစည္းမ်ဥ္းထုတ္ျပန္ခ်က္မ်ားႏွင့္အညီလုပ္ေဆာင္ႏိုင္ရန္ေဒတာပမာဏမ်ားအေျပာင္းအလဲထားရွိမႈမ်ားအား\nနားလည္ေပးရန္ ေမတၱာရပ္ခံအပ္ပါသည္။</p><p></p><p><br></p>",
//                "imgDesUrl": "http://103.85.107.171:8456/myvtg-frontend/uploads/service/accb8b23-c7af-45da-86f3-edfd462395cd.png",
//                "regState": 0,
//                "enableAct": 3
//    },
//        "message": "Successfully!"
//    }
    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("fullDes")
    private String fullDes;

    @SerializedName("imgDesUrl")
    private String imgDesUrl;

    @SerializedName("regisState")
    private boolean regisState;

    @SerializedName("webLink")
    private String webLink;

    @SerializedName("packages")
    private ArrayList<SCPackageDetail> packages = new ArrayList<>();

    public boolean getRegisState() {
        return regisState;
    }

    public void setRegisState(boolean regisState) {
        this.regisState = regisState;
    }

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    public ArrayList<SCPackageDetail> getPackages() {
        return packages;
    }

    public void setPackages(ArrayList<SCPackageDetail> packages) {
        this.packages = packages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullDes() {
        if(TextUtils.isEmpty(fullDes))
            return "";
        return fullDes;
    }

    public void setFullDes(String fullDes) {
        this.fullDes = fullDes;
    }

    public String getImgDesUrl() {
        return imgDesUrl;
    }

    public void setImgDesUrl(String imgDesUrl) {
        this.imgDesUrl = imgDesUrl;
    }


    @Override
    public String toString() {
        return "SCPackageDetail{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", fullDes='" + fullDes + '\'' +
                ", imgDesUrl='" + imgDesUrl + '\'' +
                '}';
    }
}

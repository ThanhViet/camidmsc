package com.metfone.selfcare.module.keeng.base;

import android.view.View;

import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;


public class BaseHolder extends BaseViewHolder {
    protected BaseListener.OnClickMedia listener;

    public BaseHolder(View view) {
        super(view);
    }

    public BaseHolder(View view, BaseListener.OnClickMedia listener) {
        super(view);
        this.listener = listener;
    }
}
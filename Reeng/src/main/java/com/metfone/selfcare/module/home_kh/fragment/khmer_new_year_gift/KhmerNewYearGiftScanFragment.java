package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.util.Strings;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.barcode.Barcode;
import com.google.mlkit.vision.barcode.BarcodeScanner;
import com.google.mlkit.vision.barcode.BarcodeScannerOptions;
import com.google.mlkit.vision.barcode.BarcodeScanning;
import com.google.mlkit.vision.common.InputImage;
import com.google.zxing.Result;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.databinding.FragmentKhmerNewYearGiftScanBinding;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.home_kh.activity.KhmerNewYearGiftActivity;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.WsGiftInfoResponse;
import com.metfone.selfcare.module.home_kh.api.WsGiftSpinResponse;
import com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox.KhmerNewYearGiftBoxFragment;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.util.IFragmentBackPress;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.io.IOException;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Response;

public class KhmerNewYearGiftScanFragment extends BaseFragment implements ZXingScannerView.ResultHandler, IFragmentBackPress {
    public static final String TAG = KhmerNewYearGiftScanFragment.class.getSimpleName();
    private static final int ZXING_CAMERA_PERMISSION = 1002;
    private static final int ACTION_GOTO_SETTING = 1003;
    private static final int SELECT_QR_CODE = 1004;
    private static final String PERMISSION_CAMERA_KEY = "permission_camera_key";
    private static final String INFO = "INFO";
    private static final String HOST = "metfone.com.kh";

    private FragmentKhmerNewYearGiftScanBinding binding;
    private ZXingScannerView mScannerView;
    private SharedPrefs mSharedPrefs;
    private boolean mIsTurnOnCameraTapped = false;
    private WsGiftInfoResponse.Result giftInfo;
    private KhHomeClient client;
    private KhmerNewYearGiftActivity activity;
    private boolean isGoHome = false;
    private BaseMPConfirmDialog confirmDialogMessenger;

    public ObservableField<SpannableString> scanCountText = new ObservableField<>();
    public ObservableField<String> giftTotalText = new ObservableField<>();
    public ObservableField<String> giftTotalAmount = new ObservableField<>();
    public ObservableBoolean shouldShowWinScreen = new ObservableBoolean(false);
    public ObservableBoolean showShareLayout = new ObservableBoolean(false);
    public ObservableField<WsGiftSpinResponse.Prize> prize = new ObservableField<>();
    public ObservableField<WsGiftSpinResponse.SocialShareInfo> socialShareInfo = new ObservableField<>();

    public ObservableField<String> errorMessage = new ObservableField<>();
    public ObservableBoolean shouldShowError = new ObservableBoolean(false);


    public static KhmerNewYearGiftScanFragment newInstance(WsGiftInfoResponse.Result info) {
        KhmerNewYearGiftScanFragment fragment = new KhmerNewYearGiftScanFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(INFO, info);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentKhmerNewYearGiftScanBinding.inflate(getLayoutInflater());
        binding.setHandler(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Utilities.reCalculateDimensionAnchorView(binding.anchorPointForFrame,
                binding.container,
                binding.imageOriginalSize,
                binding.anchorPointForFrame);

        mScannerView = new ZXingScannerView(getContext());
        client = new KhHomeClient();
        activity = (KhmerNewYearGiftActivity) getActivity();
        binding.camerFrame.addView(mScannerView);
        mSharedPrefs = SharedPrefs.getInstance();
        handleCameraPermission();
        Bundle bundle = getArguments();
        giftInfo = (WsGiftInfoResponse.Result) bundle.getSerializable(INFO);

        if(giftInfo != null){
            updateValueScanCountText(giftInfo.getCountSpin().intValue());
            giftTotalAmount.set(getResources().getString(R.string.usd, giftInfo.getTotalAmount().toString()));
        }

        ObjectAnimator animator = ObjectAnimator.ofFloat(binding.imgShineBackground, "rotation", 0, 360);
        animator.setDuration(2000);
        animator.setRepeatCount(Animation.INFINITE);
        animator.start();
    }

    @Override
    public String getName() {
        return KhmerNewYearGiftScanFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return 0;
    }

    public void handleCameraPermission() {
        mIsTurnOnCameraTapped = true;
        if (PermissionHelper.declinedPermission(mActivity, Manifest.permission.CAMERA)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            resumeQRCamera();
        }
    }

    public void chooseQrCode(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select QR Code"), SELECT_QR_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    resumeQRCamera();
                } else {
                    binding.frameLayoutRequestPermission.setVisibility(View.VISIBLE);
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                        if (mSharedPrefs.get(PERMISSION_CAMERA_KEY, Boolean.class)) {
                            if (mIsTurnOnCameraTapped) {
                                mIsTurnOnCameraTapped = false;
                                showCameraPermissionDialog();
                            }
                        } else {
                            mSharedPrefs.put(PERMISSION_CAMERA_KEY, true);
                        }
                    }
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ACTION_GOTO_SETTING && resultCode == 0
                && ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            mSharedPrefs.put(PERMISSION_CAMERA_KEY, false);
            resumeQRCamera();
        }
        else if(requestCode == SELECT_QR_CODE && resultCode == Activity.RESULT_OK){
            try {
                scanQRImageFromFile(data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void scanQRImageFromFile(Uri uri) throws IOException {
        BarcodeScannerOptions options = new BarcodeScannerOptions.Builder().setBarcodeFormats(Barcode.FORMAT_QR_CODE).build();
        BarcodeScanner scanner = BarcodeScanning.getClient(options);
        InputImage image = InputImage.fromFilePath(getContext(),uri);
        scanner.process(image).addOnSuccessListener(barcodes -> {
            if(barcodes.size() > 0){
                scanFinished(barcodes.get(0).getRawValue());
            }
            else{
                scanFinished(null);
            }
        }).addOnFailureListener(e -> scanFinished(null));
    }

    private void showCameraPermissionDialog() {
        MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                .setContent(R.string.m_p_top_up_request_camera_permission_guide)
                .setNameButtonTop(R.string.m_p_top_up_request_camera_permission_open_setting)
                .build();

        dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onTopButtonClick() {
                dialogSuccess.dismiss();
                openSettings();
            }
        });

        dialogSuccess.show(getChildFragmentManager(), null);
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    public void openGiftBoxScreen(){
        activity.playTabSound();
        shouldShowWinScreen.set(false);
        addFragment(R.id.root_frame, KhmerNewYearGiftBoxFragment.newInstance(), KhmerNewYearGiftBoxFragment.TAG);
    }

    public void shareYourGift(){
        showShareLayout.set(true);
    }

    public void shareWithMessenger() {
        if (!Utilities.isPackageInstalled(getContext(), "com.facebook.orca")) {
            confirmDialogMessenger = new BaseMPConfirmDialog(R.drawable.img_dialog_2, R.string.info,
                    R.string.warning_messenger_was_not_installed, R.string.download_app, R.string.cancel, view -> {
                try {
                    getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.facebook.orca")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.facebook.orca")));
                }
                if (confirmDialogMessenger != null) {
                    confirmDialogMessenger.dismiss();
                }
            }, view -> {
                if (confirmDialogMessenger != null) {
                    confirmDialogMessenger.dismiss();
                }
            });
            confirmDialogMessenger.show(getActivity().getSupportFragmentManager(), null);
            return;
        }
        showShareLayout.set(false);
        if (socialShareInfo.get() == null) {
            return;
        }
        ShareLinkContent contentMessage = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(getString(R.string.khmer_pchum_ben_link_share)))
                .setQuote(socialShareInfo.get().getShareContent())
                .build();
        MessageDialog.show(requireActivity(), contentMessage);
    }

    public void shareWithFacebook() {
        showShareLayout.set(false);
        if (socialShareInfo.get() == null) {
            return;
        }
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(getString(R.string.khmer_pchum_ben_link_share)))
                .setQuote(socialShareInfo.get().getShareContent())
                .build();
        ShareDialog.show(requireActivity(), content);
    }

    public void copyLinkForShare() {
        showShareLayout.set(false);
        if (socialShareInfo.get() == null) {
            return;
        }
        TextHelper.copyToClipboard(requireActivity(), socialShareInfo.get().getShareContent());
        ToastUtils.showToast(requireActivity(), getResources().getString(R.string.copy_to_clipboard));
    }

    public void shareMore() {
        showShareLayout.set(false);
        if (socialShareInfo.get() == null) {
            return;
        }
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, socialShareInfo.get().getShareContent());
        startActivity(Intent.createChooser(intent, getString(R.string.kh_menu_share_more)));
    }

    public void resumeScan(){
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!PermissionHelper.declinedPermission(mActivity, Manifest.permission.CAMERA)) {
            resumeQRCamera();
        }
    }

    private void resumeQRCamera() {
        if (mScannerView != null) {
            binding.frameLayoutRequestPermission.setVisibility(View.GONE);
            mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
            mScannerView.startCamera();// Start camera on resume
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        scanFinished(result.getText());
    }

    private void scanFinished(String qrCodeResult){
        boolean isNotError = false;
        if(!Strings.isEmptyOrWhitespace(qrCodeResult)){
            Uri uri = Uri.parse(qrCodeResult);
            if(uri.getHost() != null && uri.getHost().equals(HOST)){
                String p = uri.getQueryParameter("p");
                String t = uri.getQueryParameter("t");
                if("1".equals(t) && !Strings.isEmptyOrWhitespace(p)){
                    isNotError = true;
                    client.wsGiftSpin(new MPApiCallback<WsGiftSpinResponse>() {
                        @Override
                        public void onResponse(Response<WsGiftSpinResponse> response) {
                            if(response.body() != null && response.body().getResult() != null){
                                if(response.body().getResult().getStatus() == 0){
                                    activity.playWonSound();
                                    prize.set(response.body().getResult().getPrize());
                                    socialShareInfo.set(response.body().getResult().getSocialShareInfo());
                                    Glide.with(binding.imgPrize)
                                            .load(prize.get().getImage())
                                            .into(binding.imgPrize);
                                    shouldShowWinScreen.set(true);
                                    reFreshScanInfo();
                                }
                                else {
                                    if(response.body().getResult().getCode().equals("ERR_NO_MORE_SPIN")){
                                        onBackPressed();
                                        addFragment(R.id.root_frame, KhmerNewYearGiftRequireTopUpFragment.newInstance(), KhmerNewYearGiftRequireTopUpFragment.TAG);
                                    }
                                    else {
                                        showGiftError(response.body().getResult().getMessage(), response.body().getResult().getStatus() == -1);
                                    }
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                        }
                    }, p);
                }
            }
        }

        if(!isNotError){
            showGiftError(getString(R.string.invalid_qr_code), false);
        }

    }

    private void reFreshScanInfo(){
        client.wsGetGiftInfo(new MPApiCallback<WsGiftInfoResponse>() {
            @Override
            public void onResponse(Response<WsGiftInfoResponse> response) {
                if(response.body() != null){
                    if(response.body().getResult().getStatus() != 0){
                        showGiftError(response.body().getResult().getMessage(), response.body().getResult().getStatus() == -1);;
                        return;
                    }
                    WsGiftInfoResponse.Result infoResponse = response.body().getResult();
                    if(infoResponse != null){
                        updateValueScanCountText(infoResponse.getCountSpin().intValue());
                        giftTotalAmount.set(getResources().getString(R.string.usd, infoResponse.getTotalAmount().toString()));
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                activity.connectionError();
            }
        });

    }

    @Override
    public boolean onFragmentBackPressed() {
        if(shouldShowWinScreen.get() || shouldShowError.get()){
            shouldShowWinScreen.set(false);
            shouldShowError.set(false);
            resumeScan();
            return false;
        }
        return true;
    }

    public void forceBack(){
        activity.playTabSound();
        if(isGoHome){
            activity.finish();
        }
        else {
            shouldShowError.set(false);
            resumeScan();
        }
    }

    private void showGiftError(String message, boolean isGoHome){
        activity.playMessageSound();
        if(!isGoHome){
            this.isGoHome = false;
            binding.errorButton.setText(R.string.close);
        }
        else {
            this.isGoHome = true;
            binding.errorButton.setText(R.string.go_home);
        }
        shouldShowError.set(true);
        errorMessage.set(message);
    }

    private void updateValueScanCountText(int count) {
        SpannableString spannable = new SpannableString(getResources().getQuantityString(R.plurals.khmer_new_year_scan_count, count, count));
        int start = getString(R.string.khmer_new_year_scan_count_spannable).length();
        int end = start + String.valueOf(count).length() + 1;
        spannable.setSpan(
                new StyleSpan(Typeface.BOLD),
                start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        spannable.setSpan(
                new RelativeSizeSpan(1.2f),
                start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        scanCountText.set(spannable);
    }
}

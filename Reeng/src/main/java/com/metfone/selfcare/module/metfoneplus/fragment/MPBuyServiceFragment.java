package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.model.metphone.BuyServiceCategoryModel;
import com.metfone.selfcare.module.metfoneplus.adapter.BuyServiceCategoryAdapter;
import com.metfone.selfcare.module.metfoneplus.adapter.BuyServiceGroupAdapter;
import com.metfone.selfcare.module.metfoneplus.adapter.divider.SpacesItemDecoration;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceCategoryViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceGroupViewHolder;
import com.metfone.selfcare.module.metfoneplus.listener.ServiceListener;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsGetExchangeServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServicesByGroupResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import retrofit2.Response;

public class MPBuyServiceFragment extends MPBaseFragment {
    public static final String TAG = MPBuyServiceFragment.class.getSimpleName();
    private static final int TYPE_EXCHANGE = 1;
    private static final int TYPE_SERVICE_GROUP = 2;

    @BindView(R.id.buy_service_list_service_category)
    RecyclerView mListServiceCategory;
    @BindView(R.id.buy_service_list_service_group)
    RecyclerView mListServiceGroup;

    private ArrayList<BuyServiceCategoryModel> mListService;

    private BuyServiceCategoryAdapter mBuyServiceCategoryAdapter;
    private BuyServiceGroupAdapter mBuyServiceGroupAdapter;
    private OnServiceListener mOnServiceListener;

    private String mServiceGroupId;
    private int mIdResTitleName;
    private int typeService = 0;
    private List<ServiceGroup> dataServiceGroups = new ArrayList<>();
    private List<ExchangeItem> dataExchangeItems = new ArrayList<>();

    private View.OnClickListener mOnServiceCategoryListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            BuyServiceCategoryViewHolder holder = (BuyServiceCategoryViewHolder) view.getTag();
            if (R.string.m_p_buy_service_exchange_pack == mListService.get(holder.mPosition).categoryName) {
                mServiceGroupId = "";
                getExchangeService();
            } else {
                mServiceGroupId = mListService.get(holder.mPosition).serviceGroupId;
                getServicesByGroup(mServiceGroupId);
            }
            String logCode = "";
            switch (holder.mPosition){
                case 0:
                    logCode = Constants.LOG_APP.SERVICE_EXCHANGE;
                    break;
                case 1:
                    logCode = Constants.LOG_APP.SERVICE_SOCIAL_MEDIA;
                    break;
                case 2:
                    logCode = Constants.LOG_APP.SERVICE_MOBILE_INTERNET;
                    break;
                case 3:
                    logCode = Constants.LOG_APP.SERVICE_INTERNATIONAL;
                    break;
            }
            logApp(logCode);
            mIdResTitleName = mListService.get(holder.mPosition).categoryName;
            ApplicationController.self().getFirebaseEventBusiness().logServicePack(getString(mIdResTitleName));
        }
    };

    private ServiceListener serviceListener = new ServiceListener() {
        @Override
        public void serviceClick(int position, BuyServiceGroupViewHolder holder) {
            String serviceCode = "";
            //   Mobile, Social, International, Vas
            Object service = null;
            if (typeService == TYPE_SERVICE_GROUP) {
                service = holder.mServiceGroup;
                if (service == null && dataServiceGroups.size() > position) {
                    service = dataServiceGroups.get(position);
                }
                serviceCode = holder.mServiceGroup.getCode();
                Log.e(TAG, "onClick: ServiceGroup = " + holder.mServiceGroup.getName());
            } else if (typeService == TYPE_EXCHANGE) {
                service = holder.mExchangeItem;
                if (service == null && dataExchangeItems.size() > position) {
                    service = dataExchangeItems.get(position);
                }
                serviceCode = holder.mExchangeItem.getGroupCode();
                ApplicationController.self().getFirebaseEventBusiness().logExchangeService(holder.mExchangeItem.getGroupName(),holder.mExchangeItem.getGroupCode());
                Log.e(TAG, "onClick: ExchangeItem = " + holder.mExchangeItem.getGroupName());
            }
            if (service != null) {
                mOnServiceListener.onDetailItemSelected(service, mServiceGroupId, serviceCode);
            }
        }
    };

    public MPBuyServiceFragment() {

    }

    public static MPBuyServiceFragment newInstance() {
        MPBuyServiceFragment fragment = new MPBuyServiceFragment();
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_buy_service;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initListServiceCategory();
        initListServiceGroup();
    }

    private void initListServiceCategory() {
        mListService = new ArrayList<>();
        mListService.add(new BuyServiceCategoryModel(getString(R.string.service_group_exchange_pack_id),
                R.drawable.ic_exchange_packs, R.string.m_p_buy_service_exchange_pack, R.string.m_p_buy_service_exchange_pack_description,
                new int[]{ContextCompat.getColor(mParentActivity, R.color.m_p_dark_blue_gradient_top),
                        ContextCompat.getColor(mParentActivity, R.color.m_p_dark_blue_gradient_bottom)}));
        mListService.add(new BuyServiceCategoryModel(getString(R.string.service_group_social_network_pack_id),
                R.drawable.ic_social_network_packs, R.string.m_p_buy_service_social_network_pack, R.string.m_p_buy_service_social_network_pack_description,
                new int[]{ContextCompat.getColor(mParentActivity, R.color.m_p_blue_gradient_top),
                        ContextCompat.getColor(mParentActivity, R.color.m_p_blue_gradient_bottom)}));
        mListService.add(new BuyServiceCategoryModel(getString(R.string.service_group_mobile_internet_pack_id),
                R.drawable.ic_mobile_internet_packs, R.string.m_p_buy_service_internet_pack, R.string.m_p_buy_service_internet_pack_description,
                new int[]{ContextCompat.getColor(mParentActivity, R.color.m_p_violet_gradient_top),
                        ContextCompat.getColor(mParentActivity, R.color.m_p_violet_gradient_bottom)}));
        mListService.add(new BuyServiceCategoryModel(getString(R.string.service_group_international_pack_id),
                R.drawable.ic_international_packs, R.string.m_p_buy_service_international_pack, R.string.m_p_buy_service_international_pack_description,
                new int[]{ContextCompat.getColor(mParentActivity, R.color.m_p_orange_gradient_top),
                        ContextCompat.getColor(mParentActivity, R.color.m_p_orange_gradient_bottom)}));
//        mListService.add(new BuyServiceCategoryModel(getString(R.string.service_group_vas_services_id),
//                R.drawable.ic_vas_services, R.string.m_p_buy_service_vas_pack, R.string.m_p_buy_service_vas_pack_description,
//                new int[]{ContextCompat.getColor(mParentActivity, R.color.m_p_orange_gradient_top),
//                        ContextCompat.getColor(mParentActivity, R.color.m_p_orange_gradient_bottom)}));

        mBuyServiceCategoryAdapter = new BuyServiceCategoryAdapter(getContext(), mListService, mOnServiceCategoryListener);
        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration(Utilities.convertDpToPixel(16), true, true);
        mListServiceCategory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListServiceCategory.setHasFixedSize(true);
        mListServiceCategory.setAdapter(mBuyServiceCategoryAdapter);
        mListServiceCategory.addItemDecoration(spacesItemDecoration);
    }

    private void initListServiceGroup() {
        mBuyServiceGroupAdapter = new BuyServiceGroupAdapter(mParentActivity,
                new ArrayList<ExchangeItem>(), new ArrayList<ServiceGroup>(),
                serviceListener);

        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration(Utilities.convertDpToPixel(16), true, true);
        mListServiceGroup.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListServiceGroup.setHasFixedSize(true);
        mListServiceGroup.setAdapter(mBuyServiceGroupAdapter);
        mListServiceGroup.addItemDecoration(spacesItemDecoration);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * Get info of Exchange packs
     */
    private void getExchangeService() {
        typeService = TYPE_EXCHANGE;
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetExchangeService(new MPApiCallback<WsGetExchangeServiceResponse>() {
            @Override
            public void onResponse(Response<WsGetExchangeServiceResponse> response) {
                if (response.body() != null && response.body().getResult() != null) {
                    if (response.body().getResult().getData() != null) {
                        List<ExchangeItem> exchangeItems = new ArrayList<>();
                        for (WsGetExchangeServiceResponse.Response e : response.body().getResult().getData()) {
                            exchangeItems.addAll(e.getListItem());
                        }
                        dataExchangeItems.clear();
                        dataExchangeItems.addAll(exchangeItems);
                        mBuyServiceGroupAdapter.replaceExchangeData(exchangeItems);
                        handleShowLisCategoryListGroup(false);
                    } else {
                        String message = response.body().getResult().getMessage();
                        if (message == null) {
                            message = getString(R.string.m_p_dialog_service_register_content_general_error);
                        }
                        MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                                .setTitle(R.string.m_p_dialog_error_title)
                                .setContent(message)
                                .setCancelableOnTouchOutside(true)
                                .build();

                        dialogSuccess.show(getChildFragmentManager(), null);
                    }
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    /**
     * Get info of pack base on serviceGroupId
     * - Mobile Internet pack: Data
     * - Social network packs: Add-on
     * - International packs: International
     * - Vas services: VAS
     *
     * @param serviceGroupId
     */
    private void getServicesByGroup(String serviceGroupId) {
        typeService = TYPE_SERVICE_GROUP;
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetServicesByGroup(serviceGroupId, new MPApiCallback<WsGetServicesByGroupResponse>() {
            @Override
            public void onResponse(Response<WsGetServicesByGroupResponse> response) {
                if (response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null) {
                    dataServiceGroups.clear();
                    dataServiceGroups.addAll(response.body().getResult().getWsResponse());
                    mBuyServiceGroupAdapter.replaceServiceGroupData(response.body().getResult().getWsResponse());
                    handleShowLisCategoryListGroup(false);
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    public void handleShowLisCategoryListGroup(boolean isCategoryShow) {
        if (!isAdded()) {
            return;
        }
        if (isCategoryShow) {
            mListServiceCategory.setVisibility(View.VISIBLE);
            mListServiceGroup.setVisibility(View.GONE);
            mOnServiceListener.onCategorySelected(R.string.m_p_buy_service_title);
        } else {
            mListServiceCategory.setVisibility(View.GONE);
            mListServiceGroup.setVisibility(View.VISIBLE);
            mOnServiceListener.onCategorySelected(mIdResTitleName);
        }
    }

    public void setOnServiceListener(OnServiceListener listener) {
        this.mOnServiceListener = listener;
    }

    interface OnServiceListener {
        void onCategorySelected(int idResName);

        void onDetailItemSelected(Object object, String serviceGroupId, String serviceCode);
    }
}

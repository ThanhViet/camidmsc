package com.metfone.selfcare.module.keeng.widget.buttonSheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.model.CategoryModel;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class BottomSheetAdapter extends BaseAdapterRecyclerView {

    List<CategoryModel> datas;
    BaseListener.OnClickBottomSheet listener;

    public BottomSheetAdapter(Context context, List<CategoryModel> datas, String ga_source, BaseListener.OnClickBottomSheet listener) {
        super(context, ga_source);
        this.datas = datas;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        if (datas != null)
            return datas.size();
        return 0;
    }

    @Override
    public CategoryModel getItem(int position) {
        try {
            return datas.get(position);
        } catch (IndexOutOfBoundsException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return null;
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int type) {
        if (type == ITEM_BOTTOM_SHEET) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.holder_bottom_sheet, parent, false);
            return new BottomSheetHolder(view, listener);
        }
        return super.onCreateViewHolder(parent, type);
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (holder instanceof BottomSheetHolder) {
            BottomSheetHolder itemHolder = (BottomSheetHolder) holder;
            itemHolder.bind(getItem(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        CategoryModel item = getItem(position);
        if (item == null) {
            return ITEM_EMPTY;
        }
        return ITEM_BOTTOM_SHEET;
    }
}

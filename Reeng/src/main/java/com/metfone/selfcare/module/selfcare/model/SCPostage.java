package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCPostage implements Serializable {
//    {
//        "monthlyFee": 0,
//            "basic": 2888,
//            "prom": 40.7554,
//            "callFee": 40.7554,
//            "smsFee": 0,
//            "otherFee": 0,
//            "dataFee": 0,
//            "vasFee": 2888,
//            "callRc": 9,
//            "smsRc": 0,
//            "otherRc": 0,
//            "dataRc": 165,
//            "vasRc": 39
//    }

    @SerializedName("monthlyFee")
    @Expose
    private double monthlyFee;
    @SerializedName("basic")
    @Expose
    private double basic;
    @SerializedName("prom")
    @Expose
    private double prom;
    @SerializedName("callFee")
    @Expose
    private double callFee;
    @SerializedName("smsFee")
    @Expose
    private double smsFee;
    @SerializedName("otherFee")
    @Expose
    private double otherFee;
    @SerializedName("dataFee")
    @Expose
    private double dataFee;
    @SerializedName("vasFee")
    @Expose
    private double vasFee;
    @SerializedName("callRc")
    @Expose
    private double callRc;
    @SerializedName("smsRc")
    @Expose
    private double smsRc;
    @SerializedName("otherRc")
    @Expose
    private double otherRc;
    @SerializedName("dataRc")
    @Expose
    private double dataRc;
    @SerializedName("vasRc")
    @Expose
    private double vasRc;

    public double getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(double monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    public double getBasic() {
        return basic;
    }

    public void setBasic(double basic) {
        this.basic = basic;
    }

    public double getProm() {
        return prom;
    }

    public void setProm(double prom) {
        this.prom = prom;
    }

    public double getCallFee() {
        return callFee;
    }

    public void setCallFee(double callFee) {
        this.callFee = callFee;
    }

    public double getSmsFee() {
        return smsFee;
    }

    public void setSmsFee(double smsFee) {
        this.smsFee = smsFee;
    }

    public double getOtherFee() {
        return otherFee;
    }

    public void setOtherFee(double otherFee) {
        this.otherFee = otherFee;
    }

    public double getDataFee() {
        return dataFee;
    }

    public void setDataFee(double dataFee) {
        this.dataFee = dataFee;
    }

    public double getVasFee() {
        return vasFee;
    }

    public void setVasFee(double vasFee) {
        this.vasFee = vasFee;
    }

    public double getCallRc() {
        return callRc;
    }

    public void setCallRc(double callRc) {
        this.callRc = callRc;
    }

    public double getSmsRc() {
        return smsRc;
    }

    public void setSmsRc(double smsRc) {
        this.smsRc = smsRc;
    }

    public double getOtherRc() {
        return otherRc;
    }

    public void setOtherRc(double otherRc) {
        this.otherRc = otherRc;
    }

    public double getDataRc() {
        return dataRc;
    }

    public void setDataRc(double dataRc) {
        this.dataRc = dataRc;
    }

    public double getVasRc() {
        return vasRc;
    }

    public void setVasRc(double vasRc) {
        this.vasRc = vasRc;
    }

    @Override
    public String toString() {
        return "SCPostage{" +
                "monthlyFee='" + monthlyFee + '\'' +
                ", basic='" + basic + '\'' +
                ", prom='" + prom + '\'' +
                ", callFee='" + callFee + '\'' +
                ", smsFee='" + smsFee + '\'' +
                ", otherFee='" + otherFee + '\'' +
                ", dataFee='" + dataFee + '\'' +
                ", vasFee='" + vasFee + '\'' +
                ", callRc='" + callRc + '\'' +
                ", smsRc='" + smsRc + '\'' +
                ", otherRc='" + otherRc + '\'' +
                ", dataRc='" + dataRc + '\'' +
                ", vasRc='" + vasRc + '\'' +
                '}';
    }
}

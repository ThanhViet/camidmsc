package com.metfone.selfcare.module.metfoneplus.billpayment;

import com.google.gson.annotations.SerializedName;

import java.text.NumberFormat;
import java.util.Locale;

public class MPBillPaymentTemplate {
    @SerializedName("paymentAccount")
    public String paymentAccount= "Toan";
    @SerializedName("ftthAccount")
    public String ftthAccount;
    @SerializedName("amount")
    public double amount;
    @SerializedName("ftthName")
    public String ftthName;
    @SerializedName("paymentDate")
    public String paymentDate;

    public String getAmountAsFormat(){
        return NumberFormat.getCurrencyInstance(Locale.US).format(amount);
    }
}

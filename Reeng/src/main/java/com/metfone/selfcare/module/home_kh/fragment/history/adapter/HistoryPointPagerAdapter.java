package com.metfone.selfcare.module.home_kh.fragment.history.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointPageFragment;
import com.metfone.selfcare.module.home_kh.model.PointType;

public class HistoryPointPagerAdapter extends FragmentStatePagerAdapter {


    public HistoryPointPagerAdapter(@NonNull FragmentManager fm) {
        super(fm, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return HistoryPointPageFragment.newInstance(position == 0 ? PointType.ACTIVE.id : PointType.PAST.id);
    }

    @Override
    public int getCount() {
        return 2;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/7/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.search.adapter.SearchAllAdapter;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.utils.ImageBusiness;
import com.metfone.selfcare.module.search.utils.SearchUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class MVDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_description)
    @Nullable
    TextView tvDescription;
    @BindView(R.id.tv_view_all)
    @Nullable
    TextView tvViewAll;

    private SearchAllListener.OnAdapterClick listener;
    private SearchModel data;
    private String domainImage;
    private String keySearch;

    public MVDetailHolder(View view, Activity activity, SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
        this.domainImage = UrlConfigHelper.getInstance(activity).getDomainKeengMusicImage();
        if (type == SearchAllAdapter.TYPE_MUSIC) {
            int width = SearchUtils.getWidthMV();
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = width;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
    }

    public void bindData(Object item, int position, String keySearch) {
        this.keySearch = keySearch;
        if (item instanceof SearchModel) {
            data = (SearchModel) item;
            if (tvViewAll != null) tvViewAll.setVisibility(View.GONE);
            if (tvTitle != null) {
                tvTitle.setText(data.getFullName());
                //tvTitle.setText(TextHelper.getTextBoldFromKeySearch(data.getFullName(), keySearch));
            }
            if (tvDescription != null) {
                tvDescription.setText(data.getFullSinger());
                tvDescription.setVisibility(TextUtils.isEmpty(data.getFullSinger()) ? View.GONE : View.VISIBLE);
            }
            ImageBusiness.setVideo(domainImage + data.getImage(), ivCover);
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            ((SearchAllListener.OnClickBoxMusic) listener).onClickMusicItem(data);
        }
    }

    @OnClick(R.id.iv_more)
    public void onClickItemMore() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            listener.onClickMenuMore(data);
        }
    }
}
package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RankDefine implements Serializable {
    @SerializedName("rankId")
    @Expose
    int rankId;
    @SerializedName("rankName")
    @Expose
    String rankName;
    @SerializedName("minPoint")
    @Expose
    int minPoint;
    @SerializedName("maxPoint")
    @Expose
    int maxPoint;
    @SerializedName("userUpdate")
    @Expose
    String userUpdate;
    @SerializedName("updateDate")
    @Expose
    String updateDate;
    @SerializedName("tenant")
    @Expose
    int tenant;
    @SerializedName("description")
    @Expose
    String description;
    @SerializedName("usedTimeDescription")
    @Expose
    String usedTimeDescription;
    @SerializedName("benefitListDetail")
    List<BenefitDetail> benefitDetails;

    public int getRankId() {
        return rankId;
    }

    public void setRankId(int rankId) {
        this.rankId = rankId;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    public int getMinPoint() {
        return minPoint;
    }

    public void setMinPoint(int minPoint) {
        this.minPoint = minPoint;
    }

    public int getMaxPoint() {
        return maxPoint;
    }

    public void setMaxPoint(int maxPoint) {
        this.maxPoint = maxPoint;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getTenant() {
        return tenant;
    }

    public void setTenant(int tenant) {
        this.tenant = tenant;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsedTimeDescription() {
        return usedTimeDescription;
    }

    public void setUsedTimeDescription(String usedTimeDescription) {
        this.usedTimeDescription = usedTimeDescription;
    }

    public List<BenefitDetail> getBenefitDetails() {
        return benefitDetails;
    }

    public void setBenefitDetails(List<BenefitDetail> benefitDetails) {
        this.benefitDetails = benefitDetails;
    }

    public class BenefitDetail {

        @SerializedName("benefitId")
        @Expose
        private String benefitId;
        @SerializedName("benefitName")
        @Expose
        private String benefitName;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("icon")
        @Expose
        private String icon;

        public String getBenefitId() {
            return benefitId;
        }

        public void setBenefitId(String benefitId) {
            this.benefitId = benefitId;
        }

        public String getBenefitName() {
            return benefitName;
        }

        public void setBenefitName(String benefitName) {
            this.benefitName = benefitName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }

}

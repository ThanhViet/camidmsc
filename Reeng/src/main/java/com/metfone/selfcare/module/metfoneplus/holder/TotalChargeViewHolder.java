package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

/**
 * ViewHolder for History charge Exchange, Basic, Promotion: Total charge
 * Layout: item_history_charge_1
 */
public class TotalChargeViewHolder extends RecyclerView.ViewHolder {
    public AppCompatImageView mIcon;
    public AppCompatTextView mTitle;
    public AppCompatTextView mValue;
    public AppCompatImageView mIconMoneyBag;

    public TotalChargeViewHolder(View view) {
        super(view);
        mIcon = view.findViewById(R.id.icon_history_charge_1);
        mTitle = view.findViewById(R.id.title_history_charge_1);
        mValue = view.findViewById(R.id.value_history_charge_1);
        mIconMoneyBag = view.findViewById(R.id.icon_money_bag_history_charge_1);
    }
}
package com.metfone.selfcare.module.selfcare.event;

import java.io.Serializable;

public class SCQRScanEvent implements Serializable {
    private String content;

    public SCQRScanEvent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "SCQRScanEvent{" +
                "content='" + content + '\'' +
                '}';
    }
}

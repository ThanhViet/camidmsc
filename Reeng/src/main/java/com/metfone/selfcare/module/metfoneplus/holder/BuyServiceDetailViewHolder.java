package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.model.camid.ServicePackage;

public class BuyServiceDetailViewHolder extends RecyclerView.ViewHolder {
    public RelativeLayout mLayoutDetail;
    public AppCompatTextView mDetailName;
    public AppCompatTextView mDetailDescription;
    public AppCompatImageView mDetailSelected;
    public ServiceGroup mServiceGroup;
    public ServicePackage mServicePackage;
    public int position;
    public boolean isAutoRenew;

    public BuyServiceDetailViewHolder(@NonNull View itemView) {
        super(itemView);
        mLayoutDetail = itemView.findViewById(R.id.layout_item_buy_service_detail);
        mDetailName = itemView.findViewById(R.id.item_buy_service_detail_title);
        mDetailDescription = itemView.findViewById(R.id.item_buy_service_detail_description);
        mDetailSelected = itemView.findViewById(R.id.item_buy_service_detail_selected);
    }
}

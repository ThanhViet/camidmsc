package com.metfone.selfcare.module.tiin;

public class ConstantTiin {
    public static final String HOME = "Home";
    public static final String CATEGORY = "Category";


    public static final String KEY_TAB = "key";
    public static final String KEY_TITLE = "title";
    public static final String KEY_CATEGORY = "idcategory";
    public static final String KEY_HASHTAG = "hashtag";
    public static final String KEY_ITEM_TIIN="item";
    public static final String KEY_SHOW_TITLE_CONFIG="show_config";
    public static final String KEY_LOG_READ_TIIN="MC_TIIN";

    public static final int TAB_SEARCH = 1;
    public static final int TAB_CONFIG = 2;
    public static final int TAB_CATEGORY_NOW = 3;
    public static final int TAB_CATEGORY = 5;
    public static final int TAB_DETAIL_TIIN = 4;

    public static final String INTENT_MODULE="module";
    public static final String KEY_EVENT_DATA = "eventdata";
    public static final String DOMAIN = "http://tiin.vn/";

    public static final String KEY_CREATE_HOME = "create";
    public static final String KEY_DATA_OLD = "dataold";
    public static final String FULL_CONTENT_TIIN = "fromTiin";

    public static final int CATEOGRY_EVENT = 10000;
    public static final int CATEGOTY_HASHTAG = 10001;
}

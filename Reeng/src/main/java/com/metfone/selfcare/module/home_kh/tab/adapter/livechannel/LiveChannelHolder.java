package com.metfone.selfcare.module.home_kh.tab.adapter.livechannel;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;
import com.metfone.selfcare.module.home_kh.tab.model.KhChannel;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;

public class LiveChannelHolder extends BaseAdapter.ViewHolder {
    @BindView(R.id.rootView)
    @Nullable
    ViewGroup viewRoot;
    @BindView(R.id.is_video_live)
    TextView is_video_live;
    @BindView(R.id.channel_thumbnail)
    ImageView channel_thumbnail;
    @BindView(R.id.channel_avatar)
    ImageView channel_avatar;
    @BindView(R.id.total_viewer)
    TextView total_viewer;
    @BindView(R.id.channel_user)
    TextView channel_user;
    @BindView(R.id.channel_title)
    TextView channel_title;
    @BindView(R.id.channel_game)
    TextView channel_game;
    private TabHomeKhListener.OnAdapterClick listener;
    private Activity activity;

    public LiveChannelHolder(View view, Activity activity, TabHomeKhListener.OnAdapterClick listener) {
        super(view);
        this.listener = listener;
        this.activity = activity;

        ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
        layoutParams.width = TabHomeUtils.getWidthKhHomeLiveChannel();
        viewRoot.setLayoutParams(layoutParams);
        viewRoot.requestLayout();
    }

    @Override
    public void bindData(Object item, int position) {
        try {
            VideoInfoResponse video = (VideoInfoResponse) item;

            Glide.with(activity).load(video.image_path_thumb).into(channel_thumbnail);

            if (video.channelInfo != null && video.channelInfo.urlAvatar != null) {
                Glide.with(activity)
                        .load(video.channelInfo.urlAvatar)
                        .error(R.drawable.ic_avatar_default)
                        .placeholder(R.drawable.ic_avatar_default)
                        .circleCrop()
                        .into(channel_avatar);
            } else {
                channel_avatar.setImageResource(R.drawable.ic_avatar_default);
            }

            total_viewer.setText(Utilities.shortenLongNumber(video.ccu));
            channel_title.setText(video.title);

            channel_user.setText(video.channelInfo != null ? video.channelInfo.name : "");
            channel_game.setText(video.gameInfo != null ? video.gameInfo.gameName : "");

            if (video.isLive == KhChannel.LIVE_STREAM) {
                is_video_live.setVisibility(View.VISIBLE);
            } else {
                is_video_live.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        channel_thumbnail.setOnClickListener(view -> {
            listener.onClickVideoItem(item, position);

            channel_thumbnail.setEnabled(false);

            channel_thumbnail.postDelayed(() -> {
                channel_thumbnail.setEnabled(true);
            }, 300);
        });
        channel_title.setOnClickListener(view -> {
            listener.onClickVideoItem(item, position);

            channel_title.setEnabled(false);

            channel_title.postDelayed(() -> {
                channel_title.setEnabled(true);
            }, 300);
        });
        channel_user.setOnClickListener(view -> {
            listener.onClickChannelItem(item, position);

            channel_user.setEnabled(false);

            channel_user.postDelayed(() -> {
                channel_user.setEnabled(true);
            }, 300);
        });
        channel_avatar.setOnClickListener(view -> {
            listener.onClickChannelItem(item, position);

            channel_avatar.setEnabled(false);

            channel_avatar.postDelayed(() -> {
                channel_avatar.setEnabled(true);
            }, 300);
        });
    }
}

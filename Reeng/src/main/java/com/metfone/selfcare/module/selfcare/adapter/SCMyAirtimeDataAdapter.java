package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCAirTimeInfo;

import java.util.ArrayList;

public class SCMyAirtimeDataAdapter<T> extends BaseAdapter<BaseViewHolder> {

    private ArrayList<T> data;
    private AbsInterface.onAirTimeClickListener listener;

    public SCMyAirtimeDataAdapter(Context context, AbsInterface.onAirTimeClickListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(ArrayList<T> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_airtime_data, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder holder, int position) {
        final SCAirTimeInfo model = (SCAirTimeInfo) data.get(position);
        if (model != null) {
            ((TextView) holder.getView(R.id.tvName)).setText(model.getName());
            if (!TextUtils.isEmpty(model.getDescription())) {
                ((TextView) holder.getView(R.id.tvDescription)).setVisibility(View.VISIBLE);
                ((TextView) holder.getView(R.id.tvDescription)).setText(model.getDescription());
            } else
                ((TextView) holder.getView(R.id.tvDescription)).setVisibility(View.GONE);
            ((TextView) holder.getView(R.id.btnSubmit)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onBorrowClick(model);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }
}
package com.metfone.selfcare.module.metfoneplus.dialog;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.SetUpProfileActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.fragment.addAccount.AddAccountFragment;
import com.metfone.selfcare.model.BaseDataRequest;
import com.metfone.selfcare.model.account.DeleteServiceRequest;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;

public class MPConfirmRemoveDialog extends MPHeroDialog {
    public static final String TAG = "MPConfirmInformationDialog";
    UserInfoBusiness userInfoBusiness;
    DeleteServiceRequest request;

    public static MPConfirmRemoveDialog newInstance() {
        return new MPConfirmRemoveDialog();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        String json = null;
        if (bundle != null) {
            json = bundle.getString(EnumUtils.OBJECT_KEY, "");
        }

        if (!TextUtils.isEmpty(json)) {
            Gson gson = new GsonBuilder().create();
            request = gson.fromJson(json, DeleteServiceRequest.class);
        }
        userInfoBusiness = new UserInfoBusiness(getActivity());
        mImage.setImageResource(R.drawable.img_camid_account);
        flImage.setVisibility(View.VISIBLE);
        mImage.setVisibility(View.VISIBLE);
        mTitle.setText(R.string.title_dialog_remove_number);
        mContent.setText(getString(R.string.txt_remove_this_number)+ request.getServiceName() + getString(R.string.txt_service_remove));

        setTopButton(R.string.delete, R.drawable.bg_red_button_corner_6, buttonTop -> {
            if (request != null) {
                deleteService(request);
            }
        });

        setBottomButton(R.string.cancel, R.drawable.bg_transparent_button_corner_6, buttonBottom -> {
            dismissAllowingStateLoss();
        });
    }

    public void deleteService(DeleteServiceRequest request) {
        ((BaseSlidingFragmentActivity) getActivity()).showLoadingDialog("", R.string.processing);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        BaseDataRequest<DeleteServiceRequest> deleteRequest = new BaseDataRequest<>(request, "", "", "", "");
        apiService.deleteService(token, deleteRequest).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                ((BaseSlidingFragmentActivity) getActivity()).hideLoadingDialog();
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())){
                        if (response.body().getData() != null){
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                            UserInfo userInfo = new UserInfo();
                            ApplicationController.self().getCamIdUserBusiness().setMetfoneUsernameIsdn(response.body().getData().getServices().get(0).getPhoneLinkedList().get(0).getPhoneNumber());
                            EventBus.getDefault().postSticky(userInfo);
                            AddAccountFragment.isNeedRefreshAccount = true;
                        }
                        ((SetUpProfileActivity) getActivity()).drawProfile();
                    }else {
                        ToastUtils.showToast(getActivity(), response.body().getMessage());
                    }
                    dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                ((BaseSlidingFragmentActivity) getActivity()).showLoadingDialog("", R.string.processing);
                ToastUtils.showToast(getActivity(), getString(R.string.service_error));
                dismiss();
            }
        });
    }

}

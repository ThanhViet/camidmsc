package com.metfone.selfcare.module.saving.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DialogInfo implements Serializable {
    @SerializedName("label")
    private String label = "";
    @SerializedName("body")
    private String body = "";
    @SerializedName("title")
    private String title = "";

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "{" +
                "label='" + label + '\'' +
                ", body='" + body + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}

package com.metfone.selfcare.module.metfoneplus.listener;

import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceGroupViewHolder;

public interface ServiceListener {
    void serviceClick(int position, BuyServiceGroupViewHolder holder);
}

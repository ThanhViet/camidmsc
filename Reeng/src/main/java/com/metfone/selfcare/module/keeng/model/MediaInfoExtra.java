package com.metfone.selfcare.module.keeng.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MediaInfoExtra implements Serializable {

    private static final long serialVersionUID = -7313200056096668617L;
    @SerializedName("id")
    private int id;

    @SerializedName("list_image")
    private List<String> listImages;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getListImages() {
        if (listImages == null) listImages = new ArrayList<>();
        return listImages;
    }

    public void setListImages(List<String> listImages) {
        this.listImages = listImages;
    }
}

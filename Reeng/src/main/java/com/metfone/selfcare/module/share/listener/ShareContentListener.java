/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/15
 *
 */

package com.metfone.selfcare.module.share.listener;

import com.metfone.selfcare.module.search.model.ContactProvisional;

public interface ShareContentListener {
    void onShareToContact(ContactProvisional item, int position);
}

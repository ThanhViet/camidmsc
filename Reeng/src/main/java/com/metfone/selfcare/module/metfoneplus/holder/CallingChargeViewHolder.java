package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

public class CallingChargeViewHolder extends RecyclerView.ViewHolder {
    public AppCompatTextView mPhoneNumber;
    public AppCompatTextView mDate;
    public AppCompatTextView mMoney;
    public AppCompatTextView mCallingTime;

    public CallingChargeViewHolder(@NonNull View itemView) {
        super(itemView);
        mPhoneNumber = itemView.findViewById(R.id.phone_number_calling_charge);
        mDate = itemView.findViewById(R.id.date_calling_charge);
        mMoney = itemView.findViewById(R.id.money_calling_charge);
        mCallingTime = itemView.findViewById(R.id.time_calling_charge);
    }
}

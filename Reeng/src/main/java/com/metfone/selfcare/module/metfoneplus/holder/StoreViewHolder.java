package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.Store;

public class StoreViewHolder extends RecyclerView.ViewHolder {
    public AppCompatTextView mStoreName;
    public AppCompatTextView mStoreAddress;
    public AppCompatTextView mStoreDistance;
    public Store mStore;

    public StoreViewHolder(View view) {
        super(view);
        mStoreName = view.findViewById(R.id.item_store_name);
        mStoreAddress = view.findViewById(R.id.item_store_address);
        mStoreDistance = view.findViewById(R.id.item_store_distance);
    }
}

package com.metfone.selfcare.module.home_kh.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.encrypt.XXTEACrypt;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.graphics.Color.WHITE;

public class MyQRCodeKhActivity extends BaseSlidingFragmentActivity implements View.OnClickListener {

    private static final String TAG = MyQRCodeKhActivity.class.getSimpleName();

    private ApplicationController mApplication;
    ImageView mMyQrCode, mShareFb, mBack;
    AppCompatTextView mBtnScanQr;
    Bitmap mQrCode;
    SharedPreferences sharedpreferences;
    String qrcode_file_path = "";
    UserInfo currentUser;
    Bitmap imgAvatar;
    Bitmap output;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) getApplicationContext();
        setContentView(R.layout.activity_my_qrcode_kh_layout);
        setColorIconStatusBar();
        mMyQrCode = (ImageView) findViewById(R.id.my_qr_code);
        mShareFb = (ImageView) findViewById(R.id.share_fb);
        mBack = (ImageView) findViewById(R.id.ab_back_btn);
        mBtnScanQr = (AppCompatTextView) findViewById(R.id.btn_scan_qr);
        mBtnScanQr.setEnabled(false);
        mBtnScanQr.setOnClickListener(this);
        mShareFb.setOnClickListener(this);
        mBack.setOnClickListener(this);
        getAvatar();
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                GenerateClick();
//            }
//        });
//        thread.start();
        sharedpreferences = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);

        qrcode_file_path = sharedpreferences.getString(Constants.PREFERENCE.PREF_QRCODE_URL, "");

        Log.i(TAG, "url--------------------------" + qrcode_file_path);
        if (TextUtils.isEmpty(qrcode_file_path)) {
            GenQrCodeTask genQrCodeTask = new GenQrCodeTask();
            genQrCodeTask.execute();
        } else {
            File avatarFile = new File(qrcode_file_path);

            if (avatarFile.exists()) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    mQrCode = BitmapFactory.decodeFile(qrcode_file_path, options);
                    mMyQrCode.setImageBitmap(mQrCode);
                    mBtnScanQr.setEnabled(true);

                } catch (Exception e) {
                    Log.e(TAG, "Exception save file", e);
                    GenQrCodeTask genQrCodeTask = new GenQrCodeTask();
                    genQrCodeTask.execute();
                }

            } else {
                GenQrCodeTask genQrCodeTask = new GenQrCodeTask();
                genQrCodeTask.execute();
            }
        }

    }

    public Bitmap mergeBitmaps(Bitmap logo, Bitmap qrcode) {

//        logo = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_logo_in_qr);
        Bitmap combined = Bitmap.createBitmap(qrcode.getWidth(), qrcode.getHeight(), qrcode.getConfig());
        Canvas canvas = new Canvas(combined);
        int canvasWidth = canvas.getWidth();
        int canvasHeight = canvas.getHeight();
        canvas.drawBitmap(qrcode, new Matrix(), null);

        Bitmap resizeLogo = Bitmap.createScaledBitmap(logo, canvasWidth / 5, canvasHeight / 5, true);
        int centreX = (canvasWidth - resizeLogo.getWidth()) / 2;
        int centreY = (canvasHeight - resizeLogo.getHeight()) / 2;
        canvas.drawBitmap(resizeLogo, centreX, centreY, null);
        return combined;
    }

    public void GenerateClick() {
        try {
            //setting size of qr code
            int width = 800;
            int height = 800;
            int smallestDimension = width < height ? width : height;

//            EditText editText=(EditText)findViewById(R.id.editText) ;

//            String qrCodeData = "http://mocha.com
// .vn/user?id=QEDjmwbwHaDxu2qKIzuzIrv%2BD7temTKMlzhdR%2FdCNMX1LF0H9HHjI6AWgYlq5J4scv42S2dlcMqhPM1A2tvCxstS
// %2BHxdxkmu8jQpeUldfUfykuZmh5hMHoR5QlcE%2B1YV%2BZz%2BCljzSGsclMDULgejWpQI8g%2BstYwQ7jbQvsjjE6g%3D";
            //setting parameters for qr code
            String qrCodeData = XXTEACrypt.encryptToBase64String(mApplication.getReengAccountBusiness().getJidNumber
                    (), Constants.XXTEA_KEY);
            String a = "http://mocha.com.vn/user?id=" + HttpHelper.EncoderUrl(qrCodeData);
            qrCodeData = a;
            Log.i(TAG, "xxtea-----------" + qrCodeData);
            String charset = "UTF-8";
            Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            CreateQRCode(qrCodeData, charset, hintMap, smallestDimension, smallestDimension);

        } catch (Exception ex) {
            Log.e(TAG, "Exception Generate", ex);
        }
    }

    public void CreateQRCode(String qrCodeData, String charset, Map hintMap, int qrCodeheight, int qrCodewidth) {

        Bitmap bitmap = null;
        try {
            //generating qr code in bitmatrix type
            BitMatrix matrix = new MultiFormatWriter().encode(new String(qrCodeData.getBytes(charset), charset),
                    BarcodeFormat.QR_CODE, qrCodewidth, qrCodeheight, hintMap);
            //converting bitmatrix to bitmap

            int width = matrix.getWidth();
            int height = matrix.getHeight();
            int[] pixels = new int[width * height];
            // All are 0, or black, by default
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    //pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
                    pixels[offset + x] = matrix.get(x, y) ?
                            ResourcesCompat.getColor(getResources(), R.color.black, null) : WHITE;
                }
            }

            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        } catch (Exception er) {
            Log.e(TAG, "Exception", er);
        }

        if (bitmap != null) {
            setImageToQRCode(bitmap);
        }


    }

    private void setImageToQRCode(Bitmap bitmap) {
        //setting bitmap to image view
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        String avatarPath = account.getAvatarPath();
        Bitmap overlay;

        if (TextUtils.isEmpty(avatarPath)) {
            overlay = output;
        } else {
            String userNumber = account.getJidNumber();
            String lastChange = account.getLastChangeAvatar();

            File avatarFile = new File(avatarPath);

            if (avatarFile.exists()) {
                try {
//                    avatarPath = FILE_PATH + avatarPath;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                    overlay = getCircleFromBitmap(BitmapFactory.decodeFile(avatarPath, options));
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    overlay = output;

                }
            } else {
                int size = (int) getResources().getDimension(R.dimen.avatar_home_menu_size);
                String url1 = mApplication.getAvatarBusiness().getAvatarUrl(lastChange, userNumber, size, account
                        .getAvatarVerify());
                try {
                    URL url = new URL(url1);
                    overlay = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (IOException e) {
                    Log.e(TAG, "IOException", e);
                    overlay =output;
                }
            }
        }
        mQrCode = mergeBitmaps(overlay, bitmap);
    }

    private Bitmap getCircleFromBitmap(Bitmap bitmapimg) {
//        Bitmap output = Bitmap.createBitmap(bitmapimg.getWidth(),
//                bitmapimg.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(output);
//
//        final int color = 0xff424242;
//        final Paint paint = new Paint();
//        final Rect rect = new Rect(0, 0, bitmapimg.getWidth(),
//                bitmapimg.getHeight());
//
//        paint.setAntiAlias(true);
//        canvas.drawARGB(0, 0, 0, 0);
//        paint.setColor(color);
//        canvas.drawCircle(bitmapimg.getWidth() / 2,
//                bitmapimg.getHeight() / 2, bitmapimg.getWidth() / 2, paint);
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
//        canvas.drawBitmap(bitmapimg, rect, rect, paint);
//        return output;
        int w = bitmapimg.getWidth();
        int h = bitmapimg.getHeight();

        int radius = Math.min(h / 2, w / 2);
        output = Bitmap.createBitmap(w + 20, h + 20, Bitmap.Config.ARGB_8888);

        Paint p = new Paint();
        p.setAntiAlias(true);

        Canvas c = new Canvas(output);
        c.drawARGB(0, 0, 0, 0);
        p.setStyle(Paint.Style.FILL);

        c.drawCircle((w / 2) + 10, (h / 2) + 10, radius, p);

        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        c.drawBitmap(bitmapimg, 10, 10, p);
        p.setXfermode(null);
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.WHITE);
        p.setStrokeWidth(10);
        c.drawCircle((w / 2) + 10, (h / 2) + 10, radius, p);

        return output;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_scan_qr:
//                onBackPressed();
                shareQrCode();
                break;
            case R.id.share_fb:
//                onBackPressed();
                mShareFb.setEnabled(false);
                shareImageFacebook(mQrCode, getResources().getString(R.string.my_qr_code_title));
                break;
            case R.id.ab_back_btn:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShareFb.setEnabled(true);
    }

    private void shareQrCode() {
        Bitmap icon = mQrCode;
//        Intent share = new Intent(Intent.ACTION_SEND);
//        share.setType("image/jpeg");
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
//        try {
//            f.createNewFile();
//            FileOutputStream fo = new FileOutputStream(f);
//            fo.write(bytes.toByteArray());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        share.putExtra(Intent.EXTRA_STREAM, Uri.parse(qrcode_file_path));
//        startActivity(Intent.createChooser(share, "Share QR code"));

//        File f=new File("full image path");
        try {
            File file = new File(qrcode_file_path);
            if (file.exists()) {
//                Uri uri = Uri.fromFile(file);
                Uri uri = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", file);
//                Uri uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, file);

                Intent share = new Intent(Intent.ACTION_SEND);
                share.putExtra(Intent.EXTRA_STREAM, uri);
                share.setType("image/*");
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(share, getString(R.string.kh_qr_code_action_share_qr)));
            } else {
                showToast(R.string.e601_error_but_undefined);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public class GenQrCodeTask
            extends AsyncTask<Void, Integer, Void> {
        //khai báo Activity để lưu trữ địa chỉ của MainActivity
        Activity contextCha;

        //constructor này được truyền vào là MainActivity


        //hàm này sẽ được thực hiện đầu tiên
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

        }

        //sau đó tới hàm doInBackground
        //tuyệt đối không được cập nhật giao diện trong hàm này
        @Override
        protected Void doInBackground(Void... arg0) {
            GenerateClick();
            return null;
        }

        /**
         * ta cập nhập giao diện trong hàm này
         */
        @Override
        protected void onProgressUpdate(Integer... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        /**
         * sau khi tiến trình thực hiện xong thì hàm này sảy ra
         */
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            long time = System.currentTimeMillis();
            String fname = "qr" + time + Constants.FILE.JPEG_FILE_SUFFIX;
            qrcode_file_path = Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER + "/" + fname;
            ImageHelper.getInstance(mApplication).
                    saveBitmapToPath(mQrCode, qrcode_file_path, Bitmap.CompressFormat.JPEG);

            Log.i(TAG, "path-------------------------" + qrcode_file_path);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(Constants.PREFERENCE.PREF_QRCODE_URL, qrcode_file_path);
            editor.apply();
            mMyQrCode.setImageBitmap(mQrCode);
            mBtnScanQr.setEnabled(true);

        }
    }
    private void getAvatar() {
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(this);
        currentUser = userInfoBusiness.getUser();
        if (!StringUtils.isEmpty(currentUser.getAvatar())) {
            getCircleFromBitmap(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()));
        } else {
            getCircleFromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        }
    }


}

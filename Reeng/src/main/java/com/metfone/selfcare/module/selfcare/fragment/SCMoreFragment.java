package com.metfone.selfcare.module.selfcare.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;

public class SCMoreFragment extends BaseFragment implements View.OnClickListener {

    private View layout_chat;
    private View layout_faq;
    private View layout_store;
    private View layout_fanpage;
    private View layout_web;
    private View btnBack;

    public static SCMoreFragment newInstance() {
        SCMoreFragment fragment = new SCMoreFragment();
        return fragment;
    }

    @Override
    public String getName() {
        return "SCMoreFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_more;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

        return view;
    }

    private void initView(View view) {
        layout_chat = view.findViewById(R.id.layout_chat);
        layout_faq = view.findViewById(R.id.layout_faq);
        layout_store = view.findViewById(R.id.layout_store);
        layout_fanpage = view.findViewById(R.id.layout_fanpage);
        layout_web = view.findViewById(R.id.layout_web);
        btnBack = view.findViewById(R.id.btnBack);

        layout_chat.setOnClickListener(this);
        layout_faq.setOnClickListener(this);
        layout_store.setOnClickListener(this);
        layout_fanpage.setOnClickListener(this);
        layout_web.setOnClickListener(this);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_chat:
                SCUtils.doChatSupport(mActivity);
                break;
            case R.id.layout_faq:
                UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia((ApplicationController) mActivity.getApplication(), mActivity, SCConstants.SC_FAQ);
                break;
            case R.id.layout_store:
                ((TabSelfCareActivity) mActivity).gotoStoreCenter(null);
                break;
            case R.id.layout_fanpage:
                UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia((ApplicationController) mActivity.getApplication(), mActivity, SCConstants.SC_FANPAGE);
                break;
            case R.id.layout_web:
                UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia((ApplicationController) mActivity.getApplication(), mActivity, SCConstants.SC_WEBSITE);
                break;
        }
    }
}

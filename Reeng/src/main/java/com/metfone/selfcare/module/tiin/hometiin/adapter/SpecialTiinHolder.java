package com.metfone.selfcare.module.tiin.hometiin.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

public class SpecialTiinHolder extends BaseViewHolder {
    ImageView ivCover;

    public SpecialTiinHolder(View view, int widthLayout) {
        super(view);
        ivCover = view.findViewById(R.id.iv_cover);
        if (widthLayout > 0 && ivCover != null) {
            ViewGroup.LayoutParams layoutParams = ivCover.getLayoutParams();
            layoutParams.width = widthLayout;
            layoutParams.height = (int) (widthLayout / 1.67);
            ivCover.setLayoutParams(layoutParams);
            ivCover.requestLayout();
        }
    }
}

package com.metfone.selfcare.module.movie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.esport.listener.OnSingleClickListener;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movienew.holder.MovieHolder;
import com.metfone.selfcare.module.movienew.model.CustomTopicFilm;

import java.util.List;

public class FilmTopicAdapter extends RecyclerView.Adapter<FilmTopicAdapter.FilmTopicHolder> {

    private List<CustomTopicFilm> mListCustomTopicFilm;
    private Context mContext;
    private TabMovieListener.OnAdapterClick listener;
    private String mBlockName;
    private int mPageLoaded = 0;

    public FilmTopicAdapter(List<CustomTopicFilm> listCustomTopicFilm, String blockName,
                            Context context, TabMovieListener.OnAdapterClick listener) {
        this.mListCustomTopicFilm = listCustomTopicFilm;
        this.mBlockName = blockName;
        this.mContext = context;
        this.listener = listener;
    }

    public int getPageLoaded() {
        return mPageLoaded;
    }

    public void increasePageLoaded() {
        mPageLoaded++;
    }

    public String getBlockName() {
        return mBlockName;
    }

    @NonNull
    @Override
    public FilmTopicHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilmTopicHolder(LayoutInflater.from(mContext).inflate(R.layout.cinema_movie_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FilmTopicHolder holder, int position) {
        Glide.with(mContext)
                .load(mListCustomTopicFilm.get(position).getPosterPath())
                .error(R.drawable.cinema_loading_bg)
                .placeholder(R.drawable.cinema_loading_bg)
                .into(holder.imgMovie);
        holder.imgMovie.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                ApplicationController.self().getFirebaseEventBusiness().logOpenFilmTopic(mBlockName);
                listener.onClickTitleBox(mListCustomTopicFilm.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListCustomTopicFilm != null ? mListCustomTopicFilm.size() : 0;
    }

    public class FilmTopicHolder extends RecyclerView.ViewHolder {

        ImageView imgMovie;

        public FilmTopicHolder(@NonNull View itemView) {
            super(itemView);
            imgMovie = itemView.findViewById(R.id.img_movie);
        }
    }

    public void addListAndNotify(List<CustomTopicFilm> listCustomTopicFilm) {
        int startPosition = mListCustomTopicFilm.size() + 1;
        mListCustomTopicFilm.addAll(listCustomTopicFilm);
        notifyItemRangeInserted(startPosition, listCustomTopicFilm.size());
    }
}

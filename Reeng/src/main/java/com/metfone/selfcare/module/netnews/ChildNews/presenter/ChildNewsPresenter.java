package com.metfone.selfcare.module.netnews.ChildNews.presenter;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.module.netnews.ChildNews.fragment.ChildNewsFragment;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.netnews.request.NewsRequest;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Log;

/**
 * Created by HaiKE on 8/19/17.
 */

public class ChildNewsPresenter extends BasePresenter implements IChildNewsPresenter {
    public static final String TAG = ChildNewsPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;
    long startTime;

    public ChildNewsPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadData(int cateId, int page, long unixTime) {
        startTime = System.currentTimeMillis();
        mNewsApi.getNewsByCategory(new NewsRequest(cateId, page, CommonUtils.NUM_SIZE, unixTime), callback);
    }

    @Override
    public void updateLastNews(SharedPref sharedPref, int id) {
        //getDataManager().setLastNews(id);
        if (sharedPref != null) {
            sharedPref.putInt(SharedPref.PREF_LAST_NEWS, id);
        }
    }

    @Override
    public int getLastNews(SharedPref sharedPref) {
        if (sharedPref != null) {
            return sharedPref.getInt(SharedPref.PREF_LAST_NEWS, 0);
        }
        return 0;
    }

    HttpCallBack callback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildNewsFragment)) {
                return;
            }
            Gson gson = new Gson();
            NewsResponse childNewsResponse = gson.fromJson(data, NewsResponse.class);

            ((ChildNewsFragment) getMvpView()).bindData(childNewsResponse);
            ((ChildNewsFragment) getMvpView()).loadDataSuccess(true);

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_NEWS_BY_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof ChildNewsFragment)) {
                return;
            }
            ((ChildNewsFragment) getMvpView()).loadDataSuccess(false);

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_NEWS_BY_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
}

package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Data;

public class WsGetPartnerGiftRedeemHistoryRequest extends BaseRequest<WsGetPartnerGiftRedeemHistoryRequest.Request> {
    public static class Request {
    }
    public static class WsRequest extends Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("limit")
        int limit;
        @SerializedName("language")
        String language;
        @SerializedName("page")
        int page;
        @SerializedName("custId")
        public String custId;

        public WsRequest(String isdn, int limit, String language, int page) {
            this.isdn = isdn;
            this.limit = limit;
            this.language = language;
            this.page = page;
        }
    }
}

package com.metfone.selfcare.module.selfcare.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

import jp.wasabeef.glide.transformations.BlurTransformation;

public class SCImageLoader {
    private static final String TAG = SCImageLoader.class.getSimpleName();

    public static void setImage(Context context, ImageView image, String url)
    {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.color.sc_color_place_holder)
                .error(R.color.sc_color_place_holder)
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(image);
    }

    public static void setBannerImage(Context context, ImageView image, String url, int pos)
    {
        if(!TextUtils.isEmpty(url))
        {
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.color.sc_color_place_holder)
                    .error(R.color.sc_color_place_holder)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            Glide.with(context)
                    .load(url)
                    .apply(requestOptions)
                    .into(image);
        }
        else
        {
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.color.sc_color_place_holder)
                    .error(R.color.sc_color_place_holder)
                    .priority(Priority.HIGH)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            Glide.with(context)
                    .load(R.color.sc_color_place_holder)
                    .apply(requestOptions)
                    .into(image);
        }
    }

    public static void setAvatar(Context context, ImageView image, String url)
    {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_avatar_default)
                .error(R.drawable.ic_avatar_default)
                .priority(Priority.HIGH)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(image);
    }

    public static void setImageBlur(final ImageView image, String url, final int radius) {
        if (image == null)
            return;
        try {
            Context context = image.getContext();
            int width = image.getWidth();
            int height = image.getHeight();
            if (width > 0 && height > 0) {
                RequestOptions requestOptions = new RequestOptions()
                        .override(width, height)
                        .priority(Priority.HIGH)
                        .placeholder(R.drawable.ic_call_blur)
                        .error(R.drawable.ic_call_blur)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .transform(new BlurTransformation(radius));
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .transition(new DrawableTransitionOptions().crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)))
                        .into(image);
            } else {
                RequestOptions requestOptions = new RequestOptions()
                        .priority(Priority.HIGH)
                        .placeholder(R.drawable.ic_call_blur)
                        .error(R.drawable.ic_call_blur)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .transform(new BlurTransformation(radius));
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .transition(new DrawableTransitionOptions().crossFade(new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true)))
                        .into(image);

            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e.getMessage());
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
}

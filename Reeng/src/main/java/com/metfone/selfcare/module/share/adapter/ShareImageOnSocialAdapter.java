/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/13
 *
 */

package com.metfone.selfcare.module.share.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.module.share.holder.ShareImageOnSocialHolder;

public class ShareImageOnSocialAdapter extends BaseAdapter<BaseAdapter.ViewHolder, FeedContent.ImageContent> {
    private static final int TYPE_IMAGE = 1;

    public ShareImageOnSocialAdapter(Activity activity) {
        super(activity);
    }

    @Override
    public int getItemViewType(int position) {
        FeedContent.ImageContent item = getItem(position);
        if (item != null) {
            return TYPE_IMAGE;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_IMAGE) {
            return new ShareImageOnSocialHolder(layoutInflater.inflate(R.layout.holder_share_image_on_social, parent, false), activity);
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder instanceof ShareImageOnSocialHolder) {
            holder.bindData(getItem(position), position);
        }
    }

}

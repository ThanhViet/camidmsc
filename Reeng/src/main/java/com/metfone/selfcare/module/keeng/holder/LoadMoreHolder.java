package com.metfone.selfcare.module.keeng.holder;

import android.view.View;
import android.widget.ProgressBar;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseHolder;

public class LoadMoreHolder extends BaseHolder {
    public ProgressBar mProgressWheel;

    public LoadMoreHolder(View itemView) {
        super(itemView, null);
        mProgressWheel = (ProgressBar) itemView.findViewById(R.id.progress_wheel);
    }

}

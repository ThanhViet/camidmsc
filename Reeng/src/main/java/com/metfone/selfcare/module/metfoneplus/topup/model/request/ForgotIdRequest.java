package com.metfone.selfcare.module.metfoneplus.topup.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ForgotIdRequest {

    public String isdn;
    public String language = "en";

    public ForgotIdRequest() {
    }

    @Override
    public String toString() {
        return "forgetRequest{" +
                ", language='" + language + '\'' +
                ", requestId='" + isdn + '\'' +
                '}';
    }
}

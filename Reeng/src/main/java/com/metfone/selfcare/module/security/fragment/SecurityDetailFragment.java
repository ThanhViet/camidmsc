/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/25
 */

package com.metfone.selfcare.module.security.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.security.activity.SecurityActivity;
import com.metfone.selfcare.module.security.adapter.SecurityDetailAdapter;
import com.metfone.selfcare.module.security.event.SecurityDetailEvent;
import com.metfone.selfcare.module.security.event.SecurityPageEvent;
import com.metfone.selfcare.module.security.helper.SecurityHelper;
import com.metfone.selfcare.module.security.listener.SecurityListener;
import com.metfone.selfcare.module.security.model.LogModel;
import com.metfone.selfcare.module.security.model.NewsModel;
import com.metfone.selfcare.module.security.model.PhoneNumberModel;
import com.metfone.selfcare.module.security.model.VulnerabilityModel;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SecurityDetailFragment extends BaseFragment implements SecurityListener, RecyclerClickListener {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.layout_no_data)
    View noDataView;
    @BindView(R.id.icon_no_data)
    ImageView iconNoData;
    @BindView(R.id.tv_title_no_data)
    TextView tvTitleNoData;
    @BindView(R.id.tv_description_no_data)
    TextView tvDescNoData;

    @BindView(R.id.layout_action_footer)
    View viewActionFooter;
    @BindView(R.id.button_action_footer_01)
    View btnActionFooter01;
    @BindView(R.id.icon_action_footer_01)
    ImageView ivIconActionFooter01;
    @BindView(R.id.tv_title_action_footer_01)
    TextView tvTitleActionFooter01;
    @BindView(R.id.button_action_footer_02)
    View btnActionFooter02;
    @BindView(R.id.icon_action_footer_02)
    ImageView ivIconActionFooter02;
    @BindView(R.id.tv_title_action_footer_02)
    TextView tvTitleActionFooter02;
    @BindView(R.id.button_action_footer_03)
    View btnActionFooter03;
    @BindView(R.id.icon_action_footer_03)
    ImageView ivIconActionFooter03;
    @BindView(R.id.tv_title_action_footer_03)
    TextView tvTitleActionFooter03;

    private Unbinder unbinder;
    private int tabId;
    private ArrayList<Object> data;
    private SecurityDetailAdapter adapter;
    private boolean isSelectMode = false;
    private boolean isDisableScrollRecyclerView = false;
    private int sizeChecked;
    private LinearLayoutManager layoutManager;

    public static SecurityDetailFragment newInstance() {
        Bundle args = new Bundle();
        SecurityDetailFragment fragment = new SecurityDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "SecurityDetailFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_security_detail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private String phoneNumber;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            tabId = bundle.getInt(Constants.KEY_TYPE);
            Log.i(TAG, "tabId: " + tabId);
            if (tabId == Constants.TAB_SECURITY_SPAM_SMS_DETAIL) {
                phoneNumber = bundle.getString(Constants.KEY_DATA);
                if (mActivity instanceof SecurityActivity) {
                    ((SecurityActivity) mActivity).setTitle(SecurityHelper.getName(phoneNumber, true));
                }
            } else if (tabId == Constants.TAB_SECURITY_FIREWALL_SMS_DETAIL) {
                phoneNumber = bundle.getString(Constants.KEY_DATA);
                if (mActivity instanceof SecurityActivity) {
                    ((SecurityActivity) mActivity).setTitle(phoneNumber);
                }
            }
        }
        initViewNoData();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initView();
                loadData();
                if (tabId == Constants.TAB_SECURITY_SPAM_SMS_DETAIL || tabId == Constants.TAB_SECURITY_FIREWALL_SMS_DETAIL) {
                    scrollToEnd();
                }
            }
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
    }

    private void loadData() {
        if (data == null) data = new ArrayList<>();
        else data.clear();
        if (mActivity != null) {
            switch (tabId) {
                case Constants.TAB_SECURITY_FIREWALL_SETTING: {
                }
                break;
                case Constants.TAB_SECURITY_FIREWALL_IGNORE: {
                    data.addAll(SecurityHelper.getPhoneNumbersFromCache(mActivity, Constants.PREFERENCE.PREF_SECURITY_FIREWALL_WHITE_LIST));
                }
                break;
                case Constants.TAB_SECURITY_FIREWALL_HISTORY: {
                    data.addAll(SecurityHelper.getShortListFirewallSms(mActivity));
                }
                break;
                case Constants.TAB_SECURITY_SPAM_SMS: {
                    data.addAll(SecurityHelper.getShortListSpamSms(mActivity));
                }
                break;
                case Constants.TAB_SECURITY_SPAM_BLOCK: {
                    data.addAll(SecurityHelper.getPhoneNumbersFromCache(mActivity, Constants.PREFERENCE.PREF_SECURITY_SPAM_BLACK_LIST));
                }
                break;
                case Constants.TAB_SECURITY_SPAM_IGNORE: {
                    data.addAll(SecurityHelper.getPhoneNumbersFromCache(mActivity, Constants.PREFERENCE.PREF_SECURITY_SPAM_WHITE_LIST));
                }
                break;
                case Constants.TAB_SECURITY_SPAM_SMS_DETAIL: {
                    data.addAll(SecurityHelper.getSpamSmsDetail(mActivity, phoneNumber));
                }
                break;
                case Constants.TAB_SECURITY_FIREWALL_SMS_DETAIL: {
                    data.addAll(SecurityHelper.getFirewallSmsDetail(mActivity, phoneNumber));
                }
                break;
            }
        }
        if (adapter != null)
            adapter.notifyDataSetChanged();
        showEmptyView(data.isEmpty());
    }

    private void scrollToEnd() {
        if (data != null && !data.isEmpty()) {
            try {
                if (layoutManager != null)
                    layoutManager.smoothScrollToPosition(recyclerView, null, data.size() - 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initViewNoData() {
        switch (tabId) {
            case Constants.TAB_SECURITY_FIREWALL_SETTING: {
                if (tvTitleNoData != null)
                    tvTitleNoData.setText(R.string.security_firewall_setting_desc);
                if (tvDescNoData != null)
                    tvDescNoData.setText(R.string.security_firewall_setting_desc);
                if (iconNoData != null)
                    iconNoData.setImageResource(R.drawable.ic_security_firewall_setting);
            }
            break;
            case Constants.TAB_SECURITY_FIREWALL_IGNORE: {
                if (tvTitleNoData != null)
                    tvTitleNoData.setText(R.string.security_firewall_ignore_title);
                if (tvDescNoData != null)
                    tvDescNoData.setText(R.string.security_firewall_ignore_desc);
                if (iconNoData != null)
                    iconNoData.setImageResource(R.drawable.ic_security_firewall_non_ignore);
            }
            break;
            case Constants.TAB_SECURITY_FIREWALL_HISTORY: {
                if (tvTitleNoData != null)
                    tvTitleNoData.setText(R.string.security_firewall_history_title);
                if (tvDescNoData != null)
                    tvDescNoData.setText(R.string.security_firewall_history_desc);
                if (iconNoData != null)
                    iconNoData.setImageResource(R.drawable.ic_security_firewall_no_history);
            }
            break;
            case Constants.TAB_SECURITY_SPAM_SMS: {
                if (tvTitleNoData != null)
                    tvTitleNoData.setText(R.string.security_spam_sms_title);
                if (tvDescNoData != null)
                    tvDescNoData.setText(R.string.security_spam_sms_desc);
                if (iconNoData != null)
                    iconNoData.setImageResource(R.drawable.ic_security_spam_no_data);
            }
            break;
            case Constants.TAB_SECURITY_SPAM_BLOCK: {
                if (tvTitleNoData != null)
                    tvTitleNoData.setText(R.string.security_spam_block_title);
                if (tvDescNoData != null)
                    tvDescNoData.setText(R.string.security_spam_block_desc);
                if (iconNoData != null)
                    iconNoData.setImageResource(R.drawable.ic_security_spam_non_block);
            }
            break;
            case Constants.TAB_SECURITY_SPAM_IGNORE: {
                if (tvTitleNoData != null)
                    tvTitleNoData.setText(R.string.security_spam_ignore_title);
                if (tvDescNoData != null)
                    tvDescNoData.setText(R.string.security_spam_ignore_desc);
                if (iconNoData != null)
                    iconNoData.setImageResource(R.drawable.ic_security_spam_non_ignore);
            }
            break;
            default: {
                if (tvTitleNoData != null)
                    tvTitleNoData.setText("");
                if (tvDescNoData != null)
                    tvDescNoData.setText(R.string.no_data);
                if (iconNoData != null) iconNoData.setVisibility(View.INVISIBLE);
            }
            break;
        }
    }

    private void initView() {
        layoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        if (data == null) data = new ArrayList<>();
        adapter = new SecurityDetailAdapter(mActivity, tabId, data);
        adapter.setListener(this);
        adapter.setRecyclerClickListener(this);
        recyclerView.setAdapter(adapter);

        if (tabId == Constants.TAB_SECURITY_FIREWALL_IGNORE) {
            btnActionFooter01.setEnabled(true);
            btnActionFooter02.setEnabled(false);
            btnActionFooter03.setEnabled(false);
            btnActionFooter01.setVisibility(View.VISIBLE);
            btnActionFooter02.setVisibility(View.INVISIBLE);
            btnActionFooter03.setVisibility(View.INVISIBLE);
            ivIconActionFooter01.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_delete));
            tvTitleActionFooter01.setText(R.string.delete);
        } else if (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY) {
            btnActionFooter01.setEnabled(true);
            btnActionFooter02.setEnabled(true);
            btnActionFooter03.setEnabled(true);
            btnActionFooter01.setVisibility(View.VISIBLE);
            btnActionFooter02.setVisibility(View.VISIBLE);
            btnActionFooter03.setVisibility(View.VISIBLE);
            ivIconActionFooter01.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_delete));
            tvTitleActionFooter01.setText(R.string.delete);
            ivIconActionFooter02.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_mark_all_read));
            tvTitleActionFooter02.setText(R.string.menu_mark_read);
            ivIconActionFooter03.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_report_not_spam));
            tvTitleActionFooter03.setText(R.string.security_allow_to_send_message);
        } else if (tabId == Constants.TAB_SECURITY_SPAM_SMS) {
            btnActionFooter01.setEnabled(true);
            btnActionFooter02.setEnabled(true);
            btnActionFooter03.setEnabled(true);
            btnActionFooter01.setVisibility(View.VISIBLE);
            btnActionFooter02.setVisibility(View.VISIBLE);
            btnActionFooter03.setVisibility(View.VISIBLE);
            ivIconActionFooter01.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_delete));
            tvTitleActionFooter01.setText(R.string.delete);
            ivIconActionFooter02.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_mark_all_read));
            tvTitleActionFooter02.setText(R.string.menu_mark_read);
            ivIconActionFooter03.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_report_not_spam));
            tvTitleActionFooter03.setText(R.string.security_report_not_spam);
        } else if (tabId == Constants.TAB_SECURITY_SPAM_BLOCK) {
            btnActionFooter01.setEnabled(true);
            btnActionFooter02.setEnabled(false);
            btnActionFooter03.setEnabled(false);
            btnActionFooter01.setVisibility(View.VISIBLE);
            btnActionFooter02.setVisibility(View.INVISIBLE);
            btnActionFooter03.setVisibility(View.INVISIBLE);
            ivIconActionFooter01.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_unblock));
            tvTitleActionFooter01.setText(R.string.unblock);
        } else if (tabId == Constants.TAB_SECURITY_SPAM_IGNORE) {
            btnActionFooter01.setEnabled(true);
            btnActionFooter02.setEnabled(false);
            btnActionFooter03.setEnabled(false);
            btnActionFooter01.setVisibility(View.VISIBLE);
            btnActionFooter02.setVisibility(View.INVISIBLE);
            btnActionFooter03.setVisibility(View.INVISIBLE);
            ivIconActionFooter01.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_option_delete));
            tvTitleActionFooter01.setText(R.string.delete);
        } else {
            btnActionFooter01.setVisibility(View.INVISIBLE);
            btnActionFooter02.setVisibility(View.INVISIBLE);
            btnActionFooter03.setVisibility(View.INVISIBLE);
            btnActionFooter01.setEnabled(false);
            btnActionFooter02.setEnabled(false);
            btnActionFooter03.setEnabled(false);
        }
    }

    private void showEmptyView(boolean isShow) {
        if (noDataView != null) noDataView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    private void setStateCheckBoxAll() {
        sizeChecked = 0;
        if (data != null) {
            for (Object object : data) {
                if (object instanceof LogModel) {
                    if (((LogModel) object).isSelected()) sizeChecked++;
                } else if (object instanceof PhoneNumberModel) {
                    if (((PhoneNumberModel) object).isSelected()) sizeChecked++;
                }
            }
        }
        if (sizeChecked == 0) {
            isSelectMode = false;
            viewActionFooter.setVisibility(View.GONE);
        } else {
            isSelectMode = true;
            viewActionFooter.setVisibility(View.VISIBLE);
        }
        if (adapter != null) {
            adapter.setSelectMode(isSelectMode);
            adapter.notifyDataSetChanged();
        }
        showEmptyView(data.isEmpty());
    }

    private void startSelectModeAnimation(boolean isSelectMode) {
        if (mActivity == null || recyclerView == null) return;
        if (recyclerView.getAnimation() != null) {
            recyclerView.getAnimation().cancel();
            recyclerView.clearAnimation();
        }
        final int marginLeft = mActivity.getResources().getDimensionPixelSize(R.dimen.margin_more_content_40);
        if (isSelectMode) {
            isDisableScrollRecyclerView = true;
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) recyclerView.getLayoutParams();
            layoutParams.leftMargin = -marginLeft;
            recyclerView.setLayoutParams(layoutParams);
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    super.applyTransformation(interpolatedTime, t);
                    int marginLeftTime = (int) (interpolatedTime * marginLeft - marginLeft);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) recyclerView.getLayoutParams();
                    layoutParams.leftMargin = marginLeftTime;
                    recyclerView.setLayoutParams(layoutParams);
                }

                @Override
                public void cancel() {
                    super.cancel();
                    isDisableScrollRecyclerView = false;
                }
            };
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    isDisableScrollRecyclerView = true;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    isDisableScrollRecyclerView = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animation.setDuration(400);
            recyclerView.startAnimation(animation);
        } else {
            isDisableScrollRecyclerView = false;
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) recyclerView.getLayoutParams();
            layoutParams.leftMargin = marginLeft;
            recyclerView.setLayoutParams(layoutParams);
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    super.applyTransformation(interpolatedTime, t);
                    int marginLeftTime = (int) (marginLeft - interpolatedTime * marginLeft);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) recyclerView.getLayoutParams();
                    layoutParams.leftMargin = marginLeftTime;
                    recyclerView.setLayoutParams(layoutParams);
                }

                @Override
                public void cancel() {
                    super.cancel();
                }
            };
            animation.setDuration(400);
            recyclerView.startAnimation(animation);
        }
    }

    @Override
    public void onSpamWhiteListRemove(PhoneNumberModel item) {
        if (data != null) {
            data.remove(item);
            adapter.notifyDataSetChanged();
            showEmptyView(data.isEmpty());
        }
        SecurityHelper.updateSpamWhiteList(mActivity, item.getPhoneNumber(), false);
    }

    @Override
    public void onSpamBlackListRemove(PhoneNumberModel item) {
        if (data != null) {
            data.remove(item);
            adapter.notifyDataSetChanged();
            showEmptyView(data.isEmpty());
        }
        SecurityHelper.updateSpamBlackList(mActivity, item.getPhoneNumber(), false);
    }

    @Override
    public void onFirewallWhiteListRemove(PhoneNumberModel item) {
        if (data != null) {
            data.remove(item);
            adapter.notifyDataSetChanged();
            showEmptyView(data.isEmpty());
        }
        SecurityHelper.updateFirewallWhiteList(mActivity, item.getPhoneNumber(), false);
    }

    @Override
    public void onDeleteAllSpamSmsClick() {
        showDialogConfirm(R.string.security_title_confirm_delete_sms, R.string.security_body_confirm_delete_sms
                , R.string.delete, new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        if (tabId == Constants.TAB_SECURITY_SPAM_SMS) {
                            SharedPref.newInstance(mActivity).remove(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST);
                            if (data == null) data = new ArrayList<>();
                            else data.clear();
                            if (adapter != null) adapter.notifyDataSetChanged();
                            showEmptyView(true);
                        } else if (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY) {
                            SharedPref.newInstance(mActivity).remove(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST);
                            if (data == null) data = new ArrayList<>();
                            else data.clear();
                            if (adapter != null) adapter.notifyDataSetChanged();
                            showEmptyView(true);
                        }
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SecurityDetailEvent event) {
        if (tabId == event.getTabId()) {
            if (event.isSelectAll()) {
                if (data != null) {
                    for (int i = 0; i < data.size(); i++) {
                        Object object = data.get(i);
                        if (object instanceof LogModel) {
                            ((LogModel) object).setSelected(true);
                        } else if (object instanceof PhoneNumberModel) {
                            ((PhoneNumberModel) object).setSelected(true);
                        }
                    }
                    postEventUpdateCheckBox();
                }
            } else if (event.isCancelSelect() || event.isDeSelectAll()) {
                if (data != null) {
                    for (int i = 0; i < data.size(); i++) {
                        Object object = data.get(i);
                        if (object instanceof LogModel) {
                            ((LogModel) object).setSelected(false);
                        } else if (object instanceof PhoneNumberModel) {
                            ((PhoneNumberModel) object).setSelected(false);
                        }
                    }
                    startSelectModeAnimation(false);
                    postEventUpdateCheckBox();
                }
            } else if (event.isReloadData()) {
                loadData();
                scrollToEnd();
            }
        }
    }

    private void showDialogConfirm(int resLabel, int resMessage, int resPositiveLabel, PositiveListener<Object> listener) {
        DialogConfirm dialog = new DialogConfirm(mActivity, true);
        dialog.setLabel(mActivity.getResources().getString(resLabel));
        dialog.setMessage(mActivity.getResources().getString(resMessage));
        dialog.setPositiveLabel(mActivity.getResources().getString(resPositiveLabel));
        dialog.setNegativeLabel(mActivity.getResources().getString(R.string.cancel));
        dialog.setPositiveListener(listener);
        dialog.show();
    }

    @OnClick({R.id.button_action_footer_01, R.id.button_action_footer_02, R.id.button_action_footer_03})
    public void onClickView(View view) {
        switch (view.getId()) {
            case R.id.button_action_footer_01: {
                if (tabId == Constants.TAB_SECURITY_FIREWALL_IGNORE) {
                    showDialogConfirm(R.string.security_title_remove_number, R.string.security_body_remove_number
                            , R.string.delete, new PositiveListener<Object>() {
                                @Override
                                public void onPositive(Object result) {
                                    if (data != null) {
                                        final ArrayList<String> list = new ArrayList<>();
                                        int size = data.size() - 1;
                                        for (int i = size; i >= 0; i--) {
                                            Object object = data.get(i);
                                            if (object instanceof PhoneNumberModel) {
                                                if (((PhoneNumberModel) object).isSelected()) {
                                                    list.add(((PhoneNumberModel) object).getPhoneNumber());
                                                }
                                            }
                                        }
                                        SecurityHelper.updateFirewallWhiteList(mActivity, list, false, new ApiCallbackV2() {
                                            @Override
                                            public void onSuccess(String lastId, Object o) throws JSONException {
                                                isSelectMode = false;
                                                if (data != null) {
                                                    int size = data.size() - 1;
                                                    for (int i = size; i >= 0; i--) {
                                                        Object object = data.get(i);
                                                        if (object instanceof PhoneNumberModel) {
                                                            if (((PhoneNumberModel) object).isSelected()) {
                                                                data.remove(i);
                                                            } else if (list.contains(((PhoneNumberModel) object).getPhoneNumber())) {
                                                                data.remove(i);
                                                            }
                                                        }
                                                    }
                                                }
                                                postEventUpdateCheckBox();
                                                mActivity.showToast(R.string.security_toast_delete_phone_number_ignore);
                                            }

                                            @Override
                                            public void onError(String s) {
                                                mActivity.showToast(s);
                                            }

                                            @Override
                                            public void onComplete() {
                                                mActivity.hideLoadingDialog();
                                            }
                                        });
                                    }
                                }
                            });
                } else if (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY) {
                    showDialogConfirm(R.string.security_title_confirm_delete_sms, R.string.security_body_confirm_delete_sms
                            , R.string.delete, new PositiveListener<Object>() {
                                @Override
                                public void onPositive(Object result) {
                                    if (data != null) {
                                        ArrayList<LogModel> list = new ArrayList<>();
                                        int size = data.size() - 1;
                                        for (int i = size; i >= 0; i--) {
                                            Object object = data.get(i);
                                            if (object instanceof LogModel) {
                                                if (((LogModel) object).isSelected()) {
                                                    list.add((LogModel) object);
                                                    data.remove(i);
                                                }
                                            }
                                        }
                                        isSelectMode = false;
                                        SecurityHelper.deleteFirewallSms(mActivity, list);
                                        postEventUpdateCheckBox();
                                    }
                                }
                            });
                } else if (tabId == Constants.TAB_SECURITY_SPAM_SMS) {
                    showDialogConfirm(R.string.security_title_confirm_delete_sms, R.string.security_body_confirm_delete_sms
                            , R.string.delete, new PositiveListener<Object>() {
                                @Override
                                public void onPositive(Object result) {
                                    if (data != null) {
                                        ArrayList<LogModel> list = new ArrayList<>();
                                        int size = data.size() - 1;
                                        for (int i = size; i >= 0; i--) {
                                            Object object = data.get(i);
                                            if (object instanceof LogModel) {
                                                if (((LogModel) object).isSelected()) {
                                                    list.add((LogModel) object);
                                                    data.remove(i);
                                                }
                                            }
                                        }
                                        isSelectMode = false;
                                        SecurityHelper.deleteSpamSms(mActivity, list);
                                        postEventUpdateCheckBox();
                                    }
                                }
                            });
                } else if (tabId == Constants.TAB_SECURITY_SPAM_BLOCK) {
                    showDialogConfirm(R.string.security_title_unblock_phone_number, R.string.security_body_unblock_phone_number
                            , R.string.unblock, new PositiveListener<Object>() {
                                @Override
                                public void onPositive(Object result) {
                                    if (data != null) {
                                        final ArrayList<String> list = new ArrayList<>();
                                        int size = data.size() - 1;
                                        for (int i = size; i >= 0; i--) {
                                            Object object = data.get(i);
                                            if (object instanceof PhoneNumberModel) {
                                                if (((PhoneNumberModel) object).isSelected()) {
                                                    list.add(((PhoneNumberModel) object).getPhoneNumber());
                                                }
                                            }
                                        }
                                        SecurityHelper.updateSpamBlackList(mActivity, list, false, new ApiCallbackV2() {
                                            @Override
                                            public void onSuccess(String lastId, Object o) throws JSONException {
                                                isSelectMode = false;
                                                if (data != null) {
                                                    int size = data.size() - 1;
                                                    for (int i = size; i >= 0; i--) {
                                                        Object object = data.get(i);
                                                        if (object instanceof PhoneNumberModel) {
                                                            if (((PhoneNumberModel) object).isSelected()) {
                                                                data.remove(i);
                                                            } else if (list.contains(((PhoneNumberModel) object).getPhoneNumber())) {
                                                                data.remove(i);
                                                            }
                                                        }
                                                    }
                                                }
                                                postEventUpdateCheckBox();
                                                mActivity.showToast(R.string.security_toast_delete_phone_number_block);
                                            }

                                            @Override
                                            public void onError(String s) {
                                                mActivity.showToast(s);
                                            }

                                            @Override
                                            public void onComplete() {
                                                mActivity.hideLoadingDialog();
                                            }
                                        });
                                    }
                                }
                            });
                } else if (tabId == Constants.TAB_SECURITY_SPAM_IGNORE) {
                    showDialogConfirm(R.string.security_title_remove_number_spam, R.string.security_body_remove_number_spam
                            , R.string.delete, new PositiveListener<Object>() {
                                @Override
                                public void onPositive(Object result) {
                                    if (data != null) {
                                        ArrayList<String> list = new ArrayList<>();
                                        int size = data.size() - 1;
                                        for (int i = size; i >= 0; i--) {
                                            Object object = data.get(i);
                                            if (object instanceof PhoneNumberModel) {
                                                if (((PhoneNumberModel) object).isSelected()) {
                                                    list.add(((PhoneNumberModel) object).getPhoneNumber());
                                                }
                                            }
                                        }
                                        SecurityHelper.updateSpamWhiteList(mActivity, list, false, new ApiCallbackV2() {
                                            @Override
                                            public void onSuccess(String lastId, Object o) throws JSONException {
                                                if (data != null) {
                                                    ArrayList<String> list = new ArrayList<>();
                                                    int size = data.size() - 1;
                                                    for (int i = size; i >= 0; i--) {
                                                        Object object = data.get(i);
                                                        if (object instanceof PhoneNumberModel) {
                                                            if (((PhoneNumberModel) object).isSelected()) {
                                                                data.remove(i);
                                                            } else if (list.contains(((PhoneNumberModel) object).getPhoneNumber())) {
                                                                data.remove(i);
                                                            }
                                                        }
                                                    }
                                                    isSelectMode = false;
                                                }
                                                isSelectMode = false;
                                                postEventUpdateCheckBox();
                                                mActivity.showToast(R.string.security_toast_delete_phone_number_ignore);
                                            }

                                            @Override
                                            public void onError(String s) {
                                                mActivity.showToast(s);
                                            }

                                            @Override
                                            public void onComplete() {
                                                mActivity.hideLoadingDialog();
                                            }
                                        });
                                    }
                                }
                            });
                }
            }
            break;
            case R.id.button_action_footer_02: {
                if (tabId == Constants.TAB_SECURITY_FIREWALL_IGNORE) {

                } else if (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY) {
                    if (data != null) {
                        for (int i = 0; i < data.size(); i++) {
                            Object object = data.get(i);
                            if (object instanceof LogModel) {
                                if (((LogModel) object).isSelected()) {
                                    ((LogModel) object).setReaded(true);
                                }
                            }
                        }
                        ArrayList<LogModel> list = new ArrayList<>();
                        int size = data.size() - 1;
                        for (int i = size; i >= 0; i--) {
                            Object object = data.get(i);
                            if (object instanceof LogModel) {
                                if (((LogModel) object).isSelected()) {
                                    ((LogModel) object).setSelected(false);
                                    list.add((LogModel) object);
                                }
                            }
                        }
                        isSelectMode = false;
                        startSelectModeAnimation(false);
                        postEventUpdateCheckBox();
                        SecurityHelper.markAsReadFirewallSms(mActivity, list);
                    }
                } else if (tabId == Constants.TAB_SECURITY_SPAM_SMS) {
                    if (data != null) {
                        for (int i = 0; i < data.size(); i++) {
                            Object object = data.get(i);
                            if (object instanceof LogModel) {
                                if (((LogModel) object).isSelected()) {
                                    ((LogModel) object).setReaded(true);
                                }
                            }
                        }
                        ArrayList<LogModel> list = new ArrayList<>();
                        int size = data.size() - 1;
                        for (int i = size; i >= 0; i--) {
                            Object object = data.get(i);
                            if (object instanceof LogModel) {
                                if (((LogModel) object).isSelected()) {
                                    ((LogModel) object).setSelected(false);
                                    list.add((LogModel) object);
                                }
                            }
                        }
                        isSelectMode = false;
                        startSelectModeAnimation(false);
                        postEventUpdateCheckBox();
                        SecurityHelper.markAsReadSpamSms(mActivity, list);
                    }
                } else if (tabId == Constants.TAB_SECURITY_SPAM_BLOCK) {

                } else if (tabId == Constants.TAB_SECURITY_SPAM_IGNORE) {

                }
            }
            break;
            case R.id.button_action_footer_03: {
                if (tabId == Constants.TAB_SECURITY_FIREWALL_IGNORE) {

                } else if (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY) {
                    final DialogConfirm dialog = new DialogConfirm(mActivity, true);
                    dialog.setLabel(mActivity.getResources().getString(R.string.security_allow_to_send_message));
                    dialog.setMessage(mActivity.getResources().getString(R.string.security_msg_allow_to_send_message));
                    dialog.setCheckBox(mActivity.getResources().getString(R.string.security_check_box_allow_to_send_message));
                    dialog.setPositiveLabel(mActivity.getResources().getString(R.string.confirm));
                    dialog.setNegativeLabel(mActivity.getResources().getString(R.string.cancel));
                    dialog.setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            if (data != null) {
                                ArrayList<LogModel> list = new ArrayList<>();
                                int size = data.size() - 1;
                                for (int i = size; i >= 0; i--) {
                                    Object object = data.get(i);
                                    if (object instanceof LogModel) {
                                        if (((LogModel) object).isSelected()) {
                                            list.add((LogModel) object);
                                            data.remove(i);
                                        }
                                    }
                                }
                                isSelectMode = false;
                                SecurityHelper.allowToSendMessage(mActivity, list, dialog.isChecked());
                                postEventUpdateCheckBox();
                            }
                        }
                    });
                    dialog.show();
                } else if (tabId == Constants.TAB_SECURITY_SPAM_SMS) {
                    final DialogConfirm dialog = new DialogConfirm(mActivity, true);
                    dialog.setLabel(mActivity.getResources().getString(R.string.security_title_report_not_spam));
                    dialog.setMessage(mActivity.getResources().getString(R.string.security_body_report_not_spam));
                    dialog.setCheckBox(mActivity.getResources().getString(R.string.security_check_box_report_not_spam));
                    dialog.setPositiveLabel(mActivity.getResources().getString(R.string.confirm));
                    dialog.setNegativeLabel(mActivity.getResources().getString(R.string.cancel));
                    dialog.setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            if (data != null) {
                                ArrayList<LogModel> list = new ArrayList<>();
                                int size = data.size() - 1;
                                for (int i = size; i >= 0; i--) {
                                    Object object = data.get(i);
                                    if (object instanceof LogModel) {
                                        if (((LogModel) object).isSelected()) {
                                            list.add((LogModel) object);
                                            data.remove(i);
                                        }
                                    }
                                }
                                isSelectMode = false;
                                SecurityHelper.reportNotSpam(mActivity, list, dialog.isChecked());
                                postEventUpdateCheckBox();
                            }
                        }
                    });
                    dialog.show();
                } else if (tabId == Constants.TAB_SECURITY_SPAM_BLOCK) {

                } else if (tabId == Constants.TAB_SECURITY_SPAM_IGNORE) {

                }
            }
            break;
        }
    }

    @Override
    public void onClick(View v, int pos, Object object) {
        if (isSelectMode) {
            if (object instanceof LogModel && (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY || tabId == Constants.TAB_SECURITY_SPAM_SMS)) {
                boolean isSelected = !((LogModel) object).isSelected();
                ((LogModel) object).setSelected(isSelected);
                postEventUpdateCheckBox();
            } else if (object instanceof PhoneNumberModel && (tabId == Constants.TAB_SECURITY_FIREWALL_IGNORE ||
                    tabId == Constants.TAB_SECURITY_SPAM_BLOCK || tabId == Constants.TAB_SECURITY_SPAM_IGNORE)) {
                boolean isSelected = !((PhoneNumberModel) object).isSelected();
                ((PhoneNumberModel) object).setSelected(isSelected);
                postEventUpdateCheckBox();
            }
        } else {
            if (object instanceof LogModel && (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY || tabId == Constants.TAB_SECURITY_SPAM_SMS)) {
                NavigateActivityHelper.navigateToTabSecuritySmsDetail(mActivity, (LogModel) object);
            } else if (object instanceof PhoneNumberModel) {

            } else if (object instanceof VulnerabilityModel) {
                NavigateActivityHelper.navigateToTabSecurityVulnerability(mActivity, (VulnerabilityModel) object);
            } else if (object instanceof NewsModel) {
                NavigateActivityHelper.navigateToTabSecurityNews(mActivity, (NewsModel) object);
            }
        }
    }

    @Override
    public void onLongClick(View v, int pos, Object object) {
        if (!isSelectMode) {
            if (object instanceof LogModel && (tabId == Constants.TAB_SECURITY_FIREWALL_HISTORY || tabId == Constants.TAB_SECURITY_SPAM_SMS)) {
                ((LogModel) object).setSelected(true);
                startSelectModeAnimation(true);
                postEventUpdateCheckBox();
            } else if (object instanceof LogModel && (tabId == Constants.TAB_SECURITY_FIREWALL_SMS_DETAIL || tabId == Constants.TAB_SECURITY_SPAM_SMS_DETAIL)) {
//                ((LogModel) object).setSelected(true);
//                startSelectModeAnimation(true);
//                postEventUpdateCheckBox();
            } else if (object instanceof PhoneNumberModel && (tabId == Constants.TAB_SECURITY_FIREWALL_IGNORE ||
                    tabId == Constants.TAB_SECURITY_SPAM_BLOCK || tabId == Constants.TAB_SECURITY_SPAM_IGNORE)) {
                ((PhoneNumberModel) object).setSelected(true);
                startSelectModeAnimation(true);
                postEventUpdateCheckBox();
            }
        }

    }

    private void postEventUpdateCheckBox() {
        setStateCheckBoxAll();
        SecurityPageEvent securityPageEvent = new SecurityPageEvent();
        securityPageEvent.setTabId(tabId);
        securityPageEvent.setSelectMode(isSelectMode);
        securityPageEvent.setSizeSelected(sizeChecked);
        securityPageEvent.setSelectAll(data != null && data.size() == sizeChecked && sizeChecked > 0);
        EventBus.getDefault().post(securityPageEvent);
    }
}

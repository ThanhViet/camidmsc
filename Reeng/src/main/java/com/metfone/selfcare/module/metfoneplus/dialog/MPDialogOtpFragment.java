package com.metfone.selfcare.module.metfoneplus.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.AddMoreMethodActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.module.home_kh.base.BaseDialogKhFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpResponse;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinEntryEditText;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

public class MPDialogOtpFragment extends DialogFragment implements View.OnClickListener, ClickListener.IconListener {
    public static final String TAG = MPSupportStoreDialog.class.getSimpleName();
    private static final int TIME_DEFAULT = 60000;
    @BindView(R.id.etOtp)
    PinEntryEditText etOtp;
    @BindView(R.id.ivWrongOTP)
    CamIdTextView ivWrongOTP;
    @BindView(R.id.tvResendOtp)
    CamIdTextView tvResendOtp;
    @BindView(R.id.tvTimeResendOtp)
    CamIdTextView tvTimeResendOtp;
    private long countDownTimer;
    private Context mContext;
    Unbinder unbinder;
    private Handler mHandler;
    int remain = 0;
    private String phone;
    private String otp;
    private ClickListener.IconListener mClickHandler;
    private BaseSlidingFragmentActivity baseSlidingFragmentActivity;
    private MPDialogOtpFragment.ButtonOnClickListener mButtonOnClickListener;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private static boolean handlerflag = false;
    private boolean isSaveInstanceState = false;
    private boolean checkResend = false;
    private static final String ARG_DIALOG_ID = "ARG_DIALOG_ID";

    private int mDialogId;
    private boolean isOTPDialogShow = false;

    public void setIsOTPDialogShow(boolean isOTPDialogShow) {
        this.isOTPDialogShow = isOTPDialogShow;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public static MPDialogOtpFragment newInstance(Store store) {
        MPDialogOtpFragment supportStoreDialog = new MPDialogOtpFragment();
        Bundle bundle = new Bundle();
        supportStoreDialog.setArguments(bundle);
        return supportStoreDialog;
    }
//    public static <T extends Fragment & DialogProvider> void show(T invoker, int dialogId) {
//        Bundle args = new Bundle();
//        args.putInt(ARG_DIALOG_ID, dialogId);
//        MPDialogOtpFragment dialogWrapper = new MPDialogOtpFragment();
//        dialogWrapper.setArguments(args);
//        dialogWrapper.setTargetFragment(invoker, 0);
//        dialogWrapper.show(invoker.getActivity().getSupportFragmentManager(), null);
//    }

    @Override
    public void onAttach(@NonNull Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFullScreen);
        mHandler = new Handler();
        handlerflag = true;
    }

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        return getDialogProvider().getDialog(mDialogId);
//    }
//
//    private DialogProvider getDialogProvider() {
//        return (DialogProvider) getTargetFragment();
//    }
//
//    public interface DialogProvider {
//        Dialog getDialog(int dialogId);
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_mp_get_otp, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        setCancelable(false);
        setViewListeners();
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        etOtp.setSingleCharHint("-");
        etOtp.setHintTextColor(Color.WHITE);
        etOtp.requestFocus();
        return rootView;
    }
    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);
        if(!isOTPDialogShow){
            generateOtpByServer();
        }

        etOtp.setOnPinEnteredListener(str -> {
            if (str != null && str.length() == 6) {
                otp = etOtp.getText().toString();
                checkOtp();
            }
        });

        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() < 6){
                    if (ivWrongOTP.getVisibility() == View.VISIBLE){
                        etOtp.setPinBackground(ContextCompat.getDrawable(requireContext(), R.drawable.bg_pin_otp));
                        ivWrongOTP.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etOtp.setOnKeyListener((v, keyCode, event) -> {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                //this is for backspace
                if (ivWrongOTP.getVisibility() == View.VISIBLE) {
                    etOtp.setPinBackground(ContextCompat.getDrawable(requireContext(), R.drawable.bg_buy_phone_pin_otp));
                    ivWrongOTP.setVisibility(View.GONE);
                }
            }
            return false;
        });
    }
    private void startCountDown(int time) {
        tvResendOtp.setOnClickListener(null);
        countDownTimer = time;
        String countDownString = getString(R.string.mc_resend_otp);
        tvResendOtp.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                remain = (int) (countDownTimer / 1000.0);
                if (remain == 0) {
                } else if(remain > 0){
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                }else{remain=0;}
                    String countDownString = getString(R.string.mc_resend_otp);
                    tvResendOtp.setText(countDownString);
                    tvTimeResendOtp.setText(remain +" "+ getString(R.string.text_seconds));
            }
        }, 1000);
    }

    public void generateOtpByServer() {
        new MetfonePlusClient().wsGetOTPMetFonePlus("", new MPApiCallback<WsGetOtpResponse>() {
            @Override
            public void onResponse(Response<WsGetOtpResponse> response) {
                if (checkResend) {
                    checkResend = false;
                    ivWrongOTP.setVisibility(View.VISIBLE);
                    if (ivWrongOTP.getVisibility() == View.VISIBLE){
                        ivWrongOTP.setText(getString(R.string.mc_resend_success));
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ivWrongOTP.setVisibility(View.GONE);
                            }
                        }, 3000);
                    }
                }

                startSmsUserConsent();
                android.util.Log.d("TAG", "onResponse: " + response.body());
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(),etOtp);
                    }
                }, 200);
            }

            @Override
            public void onError(Throwable error) {
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), error.getMessage(), false);
                baseMPSuccessDialog.show();
            }
        });
    }
    public void checkOtp() {
        new MetfonePlusClient().wsCheckOTPMetFonePlus(phone, otp, new MPApiCallback<WsCheckOtpResponse>() {
            @Override
            public void onResponse(Response<WsCheckOtpResponse> response) {
                if (response.body() != null && response.body().getResult().getErrorCode().equals("0")) {
                    mButtonOnClickListener.onCheckOtpSuccess(etOtp.getText().toString());
                    //mButtonOnClickListener.onDialogDismiss(response.body().getResult().getUserMsg());
                } else {
                    ivWrongOTP.setVisibility(View.VISIBLE);
                    if (ivWrongOTP.getVisibility() == View.VISIBLE){
                        ivWrongOTP.setText(response.body().getResult().getUserMsg());
                    }
                }

                handlerflag = false;
            }

            @Override
            public void onError(Throwable error) {
                mButtonOnClickListener.onCheckOtpSuccess(etOtp.getText().toString());
            }
        });
    }
    private void startSmsUserConsent() {

        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

        Task task = client.startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.btnCancle)
                .setOnClickListener(this);
        view.findViewById(R.id.tvResendOtp)
                .setOnClickListener(this);
//        view.findViewById(R.id.layout_root_dialog_mp_support_store)
//                .setOnClickListener((v) -> {
//                    dismissAllowingStateLoss();
//                });
    }
    public void addButtonOnClickListener(MPDialogOtpFragment.ButtonOnClickListener buttonOnClickListener) {
        this.mButtonOnClickListener = buttonOnClickListener;
    }
    @Override
    public void onPause() {
        super.onPause();
        InputMethodUtils.hideSoftKeyboard(etOtp, mContext);
        dismissAllowingStateLoss();
        mClickHandler = null;
        handlerflag = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancle:
                dismissAllowingStateLoss();
                break;
            case R.id.tvResendOtp:
                checkResend = true;
                ivWrongOTP.setVisibility(View.GONE);
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
        }
    }

    public interface ButtonOnClickListener {
        default void onDialogDismiss(String mesError) {}
        default void onCheckOtpSuccess(String otp) {}
        default void onShowTopUp(String otp) {}
    }
    @Override
    public void onResume() {
        etOtp.requestFocus();
        UIUtil.showKeyboard(getContext(),etOtp);
        if (!handlerflag && remain > 0) {
            startCountDown(remain * 1000);
        }
        isSaveInstanceState = false;
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
//        if (countDownTimer != null) countDownTimer.cancel();

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
            default:
                break;
        }
    }


}

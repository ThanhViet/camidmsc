/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/5
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.adapter.CommissionDetailAdapter;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;

import java.util.ArrayList;

import butterknife.BindView;

public class BoxDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;

    private CommissionDetailAdapter adapter;
    private ArrayList<DataChallenge> data;

    public BoxDetailHolder(View view, Activity activity, boolean isHistory) {
        super(view);
        data = new ArrayList<>();
        adapter = new CommissionDetailAdapter(activity, isHistory);
        adapter.setItems(data);
        BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, true);
    }

    public void bindData(ArrayList<DataChallenge> list) {
        if (data == null) data = new ArrayList<>();
        else data.clear();
        if (list != null) data.addAll(list);
        if (adapter != null) adapter.notifyDataSetChanged();
    }

}

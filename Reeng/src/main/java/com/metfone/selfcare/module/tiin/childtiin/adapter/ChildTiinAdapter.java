package com.metfone.selfcare.module.tiin.childtiin.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.newdetails.view.CustomImageRatio;
import com.metfone.selfcare.module.tiin.DateUtilitis;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.ui.view.AspectRatioView;
import com.metfone.selfcare.util.Log;

import java.util.List;

public class ChildTiinAdapter extends BaseQuickAdapter<TiinModel, BaseViewHolder> {

    private Context context;
    private int categoryId = -1;
    private TiinListener.onCategoryItemListener listener;

    public ChildTiinAdapter(Context context, int id, List<TiinModel> datas, int categoryId, TiinListener.onCategoryItemListener listener) {
        super(id, datas);
        this.context = context;
        this.categoryId = categoryId;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void convert(final BaseViewHolder holder, final TiinModel model) {

        try {
            if (model != null) {
                if (categoryId == 101 || categoryId == 6) {
                    if (holder.getView(R.id.tv_title) != null) {
                        TextView tvDesc = holder.getView(R.id.tv_title);
                        tvDesc.setVisibility(View.VISIBLE);
                        tvDesc.setMaxLines(3);
                        tvDesc.setText(model.getTitle());
                    }
                    if (holder.getView(R.id.iv_cover) != null) {
                        CustomImageRatio imageView = holder.getView(R.id.iv_cover);
                        if (holder.getAdapterPosition() == 0)
                            imageView.setRatio(9f / 16.0f);
                        else if (holder.getAdapterPosition() == 1 || holder.getAdapterPosition() == 2)
                            imageView.setRatio(10.5f / 16.0f);
                        else
                            imageView.setRatio(9f / 16.0f);
                        ImageBusiness.setImageNewPoster(model.getImage169(), imageView);
                    }
                    if (holder.getView(R.id.tv_desc) != null) {
                        holder.setVisible(R.id.tv_desc, false);
                    }
                    if (holder.getView(R.id.tv_category) != null) {
                        holder.setVisible(R.id.tv_category, false);
                    }
                    if (holder.getView(R.id.tv_datetime) != null) {
                        holder.setVisible(R.id.tv_datetime, false);
                    }
                } else if (categoryId == 0) {
                    if (holder.getView(R.id.iv_cover) != null) {
                        ImageBusiness.setImageNew219(model.getImage(), (ImageView) holder.getView(R.id.iv_cover));
                    }
                    if (holder.getView(R.id.tv_title) != null) {
                        TiinUtilities.setText(holder.getView(R.id.tv_title), model.getTitle(), model.getTypeIcon());
                    }
                    if (holder.getView(R.id.tv_category) != null) {
                        TextView tvDuration = holder.getView(R.id.tv_category);
                        tvDuration.setTextColor(mContext.getResources().getColor(R.color.v5_text_2));
                        tvDuration.setVisibility(View.VISIBLE);
                        tvDuration.setText(DateUtilitis.calculateDate(mContext, model.getDatePub()));
                    }
                } else if (categoryId == 94) {
                    if (holder.getView(R.id.iv_cover) != null) {
                        ImageBusiness.setImageNew219(model.getImage(), holder.getView(R.id.iv_cover));
                    }
                    if (holder.getView(R.id.tv_title) != null) {
                        holder.setText(R.id.tv_title,model.getTitle());
                    }
                    if (holder.getView(R.id.iv_icon_news) != null) {
                        holder.setVisible(R.id.iv_icon_news, true);
                    }
                } else {
                    if (holder.getView(R.id.iv_cover) != null) {
                        ImageBusiness.setImageNew219(model.getImage(), holder.getView(R.id.iv_cover));
                    }
                    if (holder.getView(R.id.tv_title) != null) {
                        TiinUtilities.setText(holder.getView(R.id.tv_title), model.getTitle(), model.getTypeIcon());
                    }
                    if (holder.getView(R.id.tv_category) != null) {
                        TextView tvDuration = holder.getView(R.id.tv_category);
                        tvDuration.setTextColor(mContext.getResources().getColor(R.color.v5_text_2));
                        tvDuration.setVisibility(View.VISIBLE);
                        tvDuration.setText(DateUtilitis.calculateDate(mContext, model.getDatePub()));
                    }

                }
                if (holder.getView(R.id.button_option) != null) {
                    holder.setOnClickListener(R.id.button_option, new OnSingleClickListener() {
                        @Override
                        public void onSingleClick(View view) {
                            listener.onItemClickMore(model);
                        }
                    });
                }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(model);
                    }
                });
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }
}
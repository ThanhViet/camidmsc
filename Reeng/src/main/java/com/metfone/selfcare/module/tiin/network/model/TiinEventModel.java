package com.metfone.selfcare.module.tiin.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TiinEventModel implements Serializable {
    @SerializedName("ThematicId")
    @Expose
    private int thematicId;
    @SerializedName("ThematicName")
    @Expose
    private String thematicName;
    @SerializedName("ThematicSlug")
    @Expose
    private String ThematicSlug;
    @SerializedName("ThematicThumbPath")
    @Expose
    private String ThematicThumbPath;
    @SerializedName("data")
    @Expose
    private List<TiinModel> data;

    public TiinEventModel() {
    }

    public int getThematicId() {
        return thematicId;
    }

    public void setThematicId(int thematicId) {
        this.thematicId = thematicId;
    }

    public String getThematicName() {
        return thematicName;
    }

    public void setThematicName(String thematicName) {
        this.thematicName = thematicName;
    }

    public String getThematicSlug() {
        return ThematicSlug;
    }

    public void setThematicSlug(String thematicSlug) {
        ThematicSlug = thematicSlug;
    }

    public String getThematicThumbPath() {
        return ThematicThumbPath;
    }

    public void setThematicThumbPath(String thematicThumbPath) {
        ThematicThumbPath = thematicThumbPath;
    }

    public List<TiinModel> getData() {
        return data;
    }

    public void setData(List<TiinModel> data) {
        this.data = data;
    }
}

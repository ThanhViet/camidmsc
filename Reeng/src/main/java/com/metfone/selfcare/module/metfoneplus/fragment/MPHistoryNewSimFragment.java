package com.metfone.selfcare.module.metfoneplus.fragment;

import android.Manifest;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.module.metfoneplus.adapter.HistoryOrderSimAdapter;
import com.metfone.selfcare.module.metfoneplus.adapter.StoreAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPLocationServiceRequestDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPSupportStoreDialog;
import com.metfone.selfcare.module.metfoneplus.holder.StoreViewHolder;
import com.metfone.selfcare.module.metfoneplus.search.fragment.BuyPhoneNumberFragment;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsGetNearestStoreResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryOrderedSimResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsLstOrderedNumberResponse;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PERMISSION.PERMISSION_REQUEST_LOCATION;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPHistoryNewSimFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPHistoryNewSimFragment extends MPBaseFragment implements View.OnClickListener, HistoryOrderSimAdapter.HistoryOrderSimClickListener, MPMapsFragment.OnMapListener {
    public static final String TAG = MPHistoryNewSimFragment.class.getSimpleName();


    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.storeRecyclerView)
    RecyclerView mListProvinceDistrict;
    @BindView(R.id.historyRecyclerView)
    RecyclerView mListHistory;
    @BindView(R.id.txtEmpty)
    CamIdTextView txtEmpty;

    private StoreAdapter mStoreAdapter;
    private HistoryOrderSimAdapter mHistoryOrderSimAdapter;
    private MPMapsFragment mMPMapsFragment;
    private View.OnClickListener mOnItemStoreTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            StoreViewHolder storeViewHolder = (StoreViewHolder) view.getTag();
            MPSupportStoreDialog supportStoreDialog = MPSupportStoreDialog.newInstance(storeViewHolder.mStore);
            supportStoreDialog.show(getParentFragmentManager(), MPSupportStoreDialog.TAG);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        getHistoryOrderedNumber();
        findStoreByAddress(Constants.INVALID_LAT, Constants.INVALID_LNG);
    }

    public MPHistoryNewSimFragment() {
        // Required empty public constructor
    }


    public static MPHistoryNewSimFragment newInstance() {
        MPHistoryNewSimFragment fragment = new MPHistoryNewSimFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getHistoryOrderedNumber();
        findStoreByAddress(Constants.INVALID_LAT, Constants.INVALID_LNG);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_history_order_sim;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActionBarTitle.setText(getString(R.string.ordered_number));
        Utilities.adaptViewForInserts(mLayoutActionBar);
        initMap();
       // DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mParentActivity, DividerItemDecoration.VERTICAL);
       // dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mParentActivity, R.drawable.divider_list_store)));

        mStoreAdapter = new StoreAdapter(mParentActivity, new ArrayList<Store>(), mOnItemStoreTapped);
        mListProvinceDistrict.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListProvinceDistrict.setHasFixedSize(true);
    //    mListProvinceDistrict.addItemDecoration(dividerItemDecoration);
        mListProvinceDistrict.setAdapter(mStoreAdapter);

        mHistoryOrderSimAdapter = new HistoryOrderSimAdapter(mParentActivity, new ArrayList<WsLstOrderedNumberResponse>(), this);
        mListHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListHistory.setHasFixedSize(true);
        //    mListProvinceDistrict.addItemDecoration(dividerItemDecoration);
        mListHistory.setAdapter(mHistoryOrderSimAdapter);

    }
    private void initMap() {
        mMPMapsFragment = MPMapsFragment.newInstance();
        mMPMapsFragment.setOnTaskCompleted(this);
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_LOCATION);
        } else {
            if (!Utilities.isLocationEnabled(mParentActivity)) {
                MPLocationServiceRequestDialog dialog = MPLocationServiceRequestDialog.newInstance();
                dialog.show(getParentFragmentManager(), MPLocationServiceRequestDialog.TAG);
                return;
            }
        }

    }
    @Override
    public void onDestroyView() {
        if (mMPMapsFragment != null) {
            mMPMapsFragment.removeLocationCallback();
            mMPMapsFragment.setOnTaskCompleted(null);
            mMPMapsFragment = null;
        }
        super.onDestroyView();
        getCamIdUserBusiness().setStores(null);
    }

    @OnClick(R.id.action_bar_back)
    void onBack() {
        popBackStackFragment();
    }

    private void getHistoryOrderedNumber() {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetLstHistoryOrderedSim(new MPApiCallback<WsHistoryOrderedSimResponse>() {
            @Override
            public void onResponse(Response<WsHistoryOrderedSimResponse> response) {
                if (response.body() != null) {
                    if(response.body().getResult().getWsResponse() != null){
                        updateListHistory(response.body().getResult().getWsResponse().getLstOrderedNumber());
                    }
                    if (response.body().getResult().getWsResponse() == null) {
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText(response.body().getResult().getMessage());
                    }
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void findStoreByAddress(double latitude, double longitude) {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetNearestStore(latitude, longitude, new MPApiCallback<WsGetNearestStoreResponse>() {
            @Override
            public void onResponse(Response<WsGetNearestStoreResponse> response) {
                if (response.body() != null) {
                    updateListStore(response.body().getResult().getWsResponse());

                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void updateListStore(List<Store> stores) {
        if (stores == null || stores.size() == 0) {
            mStoreAdapter.replaceData(new ArrayList<Store>());
           // mLayoutNoStore.setVisibility(View.VISIBLE);
            getCamIdUserBusiness().setStores(null);
        } else {
//            mLayoutNoStore.setVisibility(View.GONE);
            getCamIdUserBusiness().setStores(stores);
        }
    }

    private void updateListHistory(List<WsLstOrderedNumberResponse> wsHistoryOrderedSimResponses) {
        if (wsHistoryOrderedSimResponses == null || wsHistoryOrderedSimResponses.size() == 0) {
            mHistoryOrderSimAdapter.replaceData(new ArrayList<WsLstOrderedNumberResponse>());
            // mLayoutNoStore.setVisibility(View.VISIBLE);
            txtEmpty.setVisibility(View.VISIBLE);;
        } else {
            mHistoryOrderSimAdapter.replaceData(wsHistoryOrderedSimResponses);
//            mLayoutNoStore.setVisibility(View.GONE);
            txtEmpty.setVisibility(View.GONE);;
        }
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void registerNumber(WsLstOrderedNumberResponse obj) {
        AvailableNumber availableNumber = new AvailableNumber();
        availableNumber.setIsdn(obj.getIsdn());
        availableNumber.setMonthlyFee((int)Math.round(obj.getMonthlyFee()));
        availableNumber.setPrice(String.valueOf(obj.getPrice()));
        availableNumber.setProduct(obj.getProduct());
        availableNumber.setRegisterFee((int)Math.round(obj.getRegisterFee()));
        availableNumber.setType(10);
        if (obj.getMonthlyFee() > 0) {
            gotoOrderNumberFragment(availableNumber, 0);
        } else {
            gotoMPApplyForYouCurrentSimFragment(availableNumber, 0, 0);
        }
    }

    @Override
    public void onGetAddressCompleted(String result) {
        mMPMapsFragment.removeLocationCallback();
    }

    @Override
    public void onGetLocationCompleted(LatLng latLng) {
        findStoreByAddress(latLng.latitude, latLng.longitude);
        getCamIdUserBusiness().setStores(null);
    }

    @Override
    public void onRequestLocationPermission() {

    }
}
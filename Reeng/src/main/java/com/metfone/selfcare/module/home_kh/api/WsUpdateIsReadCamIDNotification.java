package com.metfone.selfcare.module.home_kh.api;

import android.provider.Settings;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsUpdateIsReadCamIDNotification extends BaseRequest<WsUpdateIsReadCamIDNotification.Request> {
    public class Request {
        @SerializedName("language")
        public String language;
        @SerializedName("camid")
        public String camid;
        @SerializedName("notifyId")
        public int  notifyId;
        @SerializedName("deviceId")
        public String  deviceId;

    }
}

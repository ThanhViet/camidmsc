package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.module.metfoneplus.adapter.MyServiceGroupAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsDoActionServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetCurrentUsedServicesResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import butterknife.BindView;
import retrofit2.Response;

public class MPMyServiceFragment extends MPBaseFragment {
    public static final String TAG = MPMyServiceFragment.class.getSimpleName();

    @BindView(R.id.my_service_list_service)
    RecyclerView mListMyService;

    private MyServiceGroupAdapter mMyServiceGroupAdapter;

    public MPMyServiceFragment() {
        logApp(Constants.LOG_APP.TAB_MY_SERVICE);
    }

    public static MPMyServiceFragment newInstance() {
        MPMyServiceFragment fragment = new MPMyServiceFragment();
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_service;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMyServiceGroupAdapter = new MyServiceGroupAdapter(getContext(), new ArrayList<ServiceGroup>());

        mMyServiceGroupAdapter.setOnMyServiceListener(new MyServiceGroupAdapter.OnMyServiceListener() {
            @Override
            public void onAutoRenewButtonClick(String serviceCode) {
                showConfirmDialog(serviceCode, getString(R.string.m_p_dialog_service_stop_auto_renew_content));
            }
        });

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mParentActivity, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mParentActivity, R.drawable.divider_list_service)));
        mListMyService.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListMyService.setHasFixedSize(true);
        mListMyService.setAdapter(mMyServiceGroupAdapter);
        mListMyService.addItemDecoration(dividerItemDecoration);
    }


    @Override
    public void onResume() {
        super.onResume();
        getCurrentUsedServices();
    }

    public void getCurrentUsedServices() {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetCurrentUsedServices(new MPApiCallback<WsGetCurrentUsedServicesResponse>() {
            @Override
            public void onResponse(Response<WsGetCurrentUsedServicesResponse> response) {
                if (response.body() != null && response.body().getResult().getWsResponse() != null) {
                    mMyServiceGroupAdapter.replaceServiceGroupData(response.body().getResult().getWsResponse());
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void showConfirmDialog(String serviceCode, String content) {
        MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
            .setTitle(R.string.m_p_dialog_service_stop_auto_renew_title_confirm)
            .setContent(content)
            .setNameButtonTop(R.string.m_p_dialog_service_stop_auto_renew_i_am_sure)
            .setNameButtonBottom(R.string.m_p_dialog_service_stop_auto_renew_no_later)
            .setCancelableOnTouchOutside(false)
            .build();

        dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onTopButtonClick() {
                dialogSuccess.dismiss();
                stopAutoRenewService(serviceCode);
            }

            @Override
            public void onBottomButtonClick() {
                dialogSuccess.dismiss();
            }
        });

        dialogSuccess.show(getChildFragmentManager(), null);
    }

    private void stopAutoRenewService(String serviceCode) {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().stopAutoRenewService(serviceCode, new MPApiCallback<WsDoActionServiceResponse>() {
            @Override
            public void onResponse(Response<WsDoActionServiceResponse> response) {
                if (response.body() != null && response.body().getResult() != null) {
                    if (Constants.WSCODE.WS_DO_ACTION_SERVICE_RESULT_SUCCESS.equals(response.body().getResult().getErrorCode())) {
                        reloadCurrentUsedServices();
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void reloadCurrentUsedServices() {
        new MetfonePlusClient().wsGetCurrentUsedServices(new MPApiCallback<WsGetCurrentUsedServicesResponse>() {
            @Override
            public void onResponse(Response<WsGetCurrentUsedServicesResponse> response) {
                boolean hasData = response.isSuccessful() && response.body() != null;

                if (hasData && response.body().getResult().getWsResponse() != null) {
                    mMyServiceGroupAdapter.replaceServiceGroupData(response.body().getResult().getWsResponse());
                } else if (hasData && response.body().getResult().getWsResponse() == null) {
                    mMyServiceGroupAdapter.replaceServiceGroupData(Collections.emptyList());
                } else {
                    Toast.makeText(mParentActivity, getString(R.string.error_response_data), Toast.LENGTH_LONG).show();
                }

                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }
}

package com.metfone.selfcare.module.metfoneplus.topup.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.ConfirmationTopupDialogBinding;
import com.metfone.selfcare.module.metfoneplus.topup.interfacelistener.RunUi;

import static com.metfone.selfcare.module.metfoneplus.topup.model.Constants.CONFIRM;

public class ConfirmationTopUpDialog extends DialogFragment {

    private ConfirmationTopupDialogBinding mDialogBinding;
    private RunUi mRunUi;
    private String mMess = "";
    private Activity mActivity;


    public ConfirmationTopUpDialog(String Mess, Activity activity, RunUi runUi) {
        this.mMess = Mess;
        this.mRunUi = runUi;
        this.mActivity = activity;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int maxWidth = displayMetrics.widthPixels;
        int scaledWidth = (int) (maxWidth * 0.90);
        if (dialog != null) {
            dialog.getWindow().setLayout(scaledWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDialogBinding = DataBindingUtil.inflate(inflater, R.layout.confirmation_topup_dialog, container, false);
        return mDialogBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDialogBinding.txtMessTopUp.setText(mMess);
        mDialogBinding.btnConfirm.setOnClickListener(v -> {
            mRunUi.run(CONFIRM);
            dismiss();
        });
        mDialogBinding.btnDissmiss.setOnClickListener(v -> dismiss());
    }

}

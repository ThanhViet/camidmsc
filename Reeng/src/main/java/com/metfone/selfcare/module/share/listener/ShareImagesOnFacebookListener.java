/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/13
 *
 */

package com.metfone.selfcare.module.share.listener;

import android.graphics.Bitmap;

import java.util.ArrayList;

public interface ShareImagesOnFacebookListener {
    void onPrepareDownload();

    void onCompletedDownload(ArrayList<Bitmap> list);

    void onCompletedDownload(Bitmap bitmap);
}

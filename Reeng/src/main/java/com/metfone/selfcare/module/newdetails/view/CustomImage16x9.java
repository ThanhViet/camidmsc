package com.metfone.selfcare.module.newdetails.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.metfone.selfcare.R;

/**
 * Created by HaiKE on 10/09/2015.
 */
public class CustomImage16x9 extends ImageView {
    private Context mContext;
    private boolean is16v9;

    public CustomImage16x9(Context context) {
        super(context);
        this.mContext = context;
        init(null);
    }

    public CustomImage16x9(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init(attrs);
    }

    public CustomImage16x9(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomImage16x9(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.customImageView);
            is16v9 = a.getBoolean(R.styleable.customImageView_isRatio, false);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h;
        if (is16v9) {
            h = (w * 9) / 16;
        } else {
            h = MeasureSpec.getSize(heightMeasureSpec);
        }
        this.setMeasuredDimension(w, h);
    }

    public void setImage16v9(boolean boo) {
        this.is16v9 = boo;
    }
}

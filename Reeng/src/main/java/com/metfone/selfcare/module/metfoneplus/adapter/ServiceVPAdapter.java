package com.metfone.selfcare.module.metfoneplus.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;

import java.util.List;

public class ServiceVPAdapter extends FragmentStatePagerAdapter {
    private final static int SIZE_VIEW_PAGER_SERVICE = 2;

    private String[] mTitleBalance;
    private List<MPBaseFragment> mFragmentList;

    public ServiceVPAdapter(@NonNull FragmentManager fm, int behavior, String[] titleBalance, List<MPBaseFragment> fragmentList) {
        super(fm, behavior);
        this.mTitleBalance = titleBalance;
        this.mFragmentList = fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return SIZE_VIEW_PAGER_SERVICE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleBalance[position];
    }
}

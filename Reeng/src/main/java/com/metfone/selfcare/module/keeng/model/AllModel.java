package com.metfone.selfcare.module.keeng.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.utils.Utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AllModel implements Serializable {
    private static final long serialVersionUID = 3013948182694996063L;

    @SerializedName("item_type")
    public int type;
    @SerializedName("id")
    public long id = 0;
    @SerializedName("name")
    public String name = "";
    @SerializedName("singer")
    public String singer = "";
    @SerializedName("image")
    public String image = "";
    @SerializedName("image310")
    public String image310 = "";

    @SerializedName("is_like")
    public int is_like = 0;
    @SerializedName("price")
    public int price;
    @SerializedName("url")
    public String url;
    @SerializedName("download_url")
    public String downloadUrl;
    @SerializedName("local_url")
    public String local_url;
    @SerializedName("is_download")
    public int is_downloadable = 1;
    @SerializedName("lyric")
    public String lyric = "";
    @SerializedName("album_id")
    public long album_id = -1;
    public int stateDownloading;
    @SerializedName("identify")
    public String identify = null;
    @SerializedName("user")
    public UserInfo userInfo;
    @SerializedName("image_path_cover")
    public String imageCover = "";
    @SerializedName("media_url")
    private String mediaUrl;
    @SerializedName("song_list")
    private List<AllModel> songList;
    @SerializedName("list_item")
    private List<AllModel> mediasList;
    @SerializedName("listen_no")
    private long listened = 0;
    @SerializedName("list_user")
    private List<UserInfo> listUser;
    @SerializedName("is_lossless")
    private int is_lossless = 0;
    @SerializedName("doc_quyen")
    private int docQuyen = 0;
    private boolean isDownloadLossless = false;
    @SerializedName("total_share")
    private long total_share = 0;
    @SerializedName("total_like")
    private long total_like = 0;
    @SerializedName("number_comment")
    private long totalComment = 0;
    @SerializedName("singer_id")
    private long singer_id = 0;
    private String timeDownload = "0";
    @SerializedName("list_image")
    private List<String> listImage;
    @SerializedName("image_path")
    private String imagePath = "";
    private String actionType = "";
    private long localId = 0;
    @SerializedName("download_url_lossless")
    private String downloadUrlLossless = "";
    @SerializedName("lyric_path")
    private String lyricUrl = "";
    @SerializedName("list_resolution")
    private List<StreamingModel> resolutionList;
    @SerializedName("record_name")
    private String recordName = "";
    @SerializedName("linkYoutube")
    private String linkYoutube = "";
    private boolean isPlayLossless = false;
    private boolean isFavorite = false;
    //TODO cac bien phuc vu luu log
    private int source;
    private int action = MediaLogModel.ACT_IGNORE;
    private long timeStartPlay;
    private int percentListen;
    private int duration;
    private int seek;
    private String normalMediaUrl = "";
    private String albumName = "";
    private String idYoutube = "";

    @SerializedName("is_album_video")
    private int isVideoList = 0;

    @SerializedName("info_extra")
    private MediaInfoExtra infoExtra;

    @SerializedName("total_media")
    private int totalMedia = 0;

    public AllModel(int type, long id, String name, String singer, String image, String mediaUrl, String url, String downloadUrl, int listened, ArrayList<AllModel> song_list) {
        super();
        this.type = type;
        this.id = id;
        this.name = name;
        this.singer = singer;
        this.image = image;
        this.mediaUrl = mediaUrl;
        this.url = url;
        this.downloadUrl = downloadUrl;
        this.listened = listened;
        this.songList = song_list;
    }

    public AllModel(String name, List<AllModel> songs, int type) {
        this.name = name;
        this.songList = new ArrayList<>();
        this.songList.addAll(songs);
        this.type = type;
    }

    public AllModel() {
        super();
        songList = new ArrayList<>();
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public long getTimeStartPlay() {
        return timeStartPlay;
    }

    public void setTimeStartPlay(long timeStartPlay) {
        this.timeStartPlay = timeStartPlay;
    }

    public int getPercentListen() {
        return percentListen;
    }

    public void setPercentListen(int percentListen) {
        this.percentListen = percentListen;
    }

    public String getLocalUrl() {
        return local_url;
    }

    public void setLocalUrl(String local_url) {
        this.local_url = local_url;
    }

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public long getListened() {
        return listened;
    }

    public void setListened(long listened) {
        this.listened = listened;
    }

    public String getListenNo() {
        return Utilities.formatNumber(listened);
    }

    public String getSinger() {
        if (singer == null)
            singer = "";
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getName() {
        if (name == null)
            name = "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitleCoverSinger() {
        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(singer))
            return name + " - " + singer;
        return name;
    }

    public String getURLMedia() {
        if (!TextUtils.isEmpty(local_url))
            return local_url;
        return mediaUrl;
    }

    public String getMediaUrl() {
        if (mediaUrl == null)
            mediaUrl = "";
        return mediaUrl;
    }

    public void setMediaUrl(String media_url) {
        this.mediaUrl = media_url;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getMediaLink() {
        String url = mediaUrl;
        if (!TextUtils.isEmpty(url)) {
            if (url.contains("?")) {
                url += "&v=" + BuildConfig.VERSION_CODE;
                if (!url.contains("&rt=")) {
                    url += "&rt=CP";
                } else if (url.contains("&rt=WP")) {
                    url = url.replace("&rt=WP", "&rt=CP");
                } else if (url.contains("&rt=P")) {
                    url = url.replace("&rt=P", "&rt=CP");
                }
            } else {
                url += "?rt=CP";
                url += "&v=" + BuildConfig.VERSION_CODE;
            }
        }
        return url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isLiked() {
        return is_like == 1;
    }

    public String getImage310() {
        //todo ảnh lớn
        if (!TextUtils.isEmpty(image310))
            return image310;
        if (!TextUtils.isEmpty(image))
            return image;
        if (!TextUtils.isEmpty(imagePath))
            return imagePath;
        if (listImage != null && !listImage.isEmpty()) {
            return listImage.get(0);
        }
        return "";
    }

    public void setImage310(String image310) {
        this.image310 = image310;
    }

    public String getImage() {
        //todo ảnh nhỏ
        if (!TextUtils.isEmpty(image))
            return image;
        if (!TextUtils.isEmpty(image310))
            return image310;
        if (!TextUtils.isEmpty(imagePath))
            return imagePath;

        if (listImage != null && !listImage.isEmpty()) {
            return listImage.get(0);
        }
        return "";
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isDownloadLossless() {
        return isDownloadLossless;
    }

    public void setDownloadLossless(boolean isDownloadLossless) {
        this.isDownloadLossless = isDownloadLossless;
    }

    public long getTotal_like() {
        return total_like;
    }

    public void setTotal_like(long total_like) {
        this.total_like = total_like;
    }

    public long getTotalComment() {
        return totalComment;
    }

    public void setTotalComment(long totalComment) {
        this.totalComment = totalComment;
    }

    public long getSinger_id() {
        return singer_id;
    }

    public void setSinger_id(long singer_id) {
        this.singer_id = singer_id;
    }

    public long getTotal_share() {
        return total_share;
    }

    public void setTotal_share(long total_share) {
        this.total_share = total_share;
    }

    public String getTimeDownload() {
        if (TextUtils.isEmpty(timeDownload))
            timeDownload = "0";
        return timeDownload;
    }

    public void setTimeDownload(String timeDownload) {
        this.timeDownload = timeDownload;
    }

    public String getNameUser() {
        String name = "";
        if (userInfo != null) {
            name = userInfo.getNameUser();
        }
        return name;
    }

    public long getUserId() {
        long id = 0;
        if (userInfo != null) {
            id = userInfo.getId();
        }
        return id;
    }

    public void setPlaylist(PlayListModel playlist) {
        if (playlist == null)
            return;
        this.id = playlist.id;
        this.type = Constants.TYPE_PLAYLIST;
        this.name = playlist.name;
        this.userInfo = playlist.getUser();
        this.url = playlist.url;
    }

    public void setPlaylist(PlayingList playlist) {
        if (playlist == null)
            return;
        this.id = playlist.getId();
        this.type = Constants.TYPE_PLAYLIST;
        this.name = playlist.getName();

        if (this.userInfo == null) {
            this.userInfo = new UserInfo();
        }
        this.userInfo.id = playlist.getUserId();
    }

    public String getLikeNo() {
        if (total_like <= 0)
            return "";
        return Utilities.formatNumber(total_like);
    }

    public String getCommentNo() {
        if (totalComment <= 0)
            return "";
        return Utilities.formatNumber(totalComment);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<UserInfo> getListUser() {
        if (listUser == null)
            listUser = new ArrayList<>();
        return listUser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setIsLike(int is_like) {
        this.is_like = is_like;
    }

    public String getLyric() {
        return lyric;
    }

    public void setLyric(String lyric) {
        this.lyric = lyric;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public int getDocQuyen() {
        return docQuyen;
    }

    public boolean isDocQuyen() {
        return (docQuyen == 1) || (docQuyen == 3);
    }

    public void setDocQuyen(int doc_quyen) {
        this.docQuyen = doc_quyen;
    }

    public int getIsLossless() {
        return is_lossless;
    }

    public void setIsLossless(int is_lossless) {
        this.is_lossless = is_lossless;
    }

    public boolean isLossless() {
        return is_lossless == 1;
    }

    public String getLosslessUrl() {
        if (TextUtils.isEmpty(mediaUrl))
            return "";
        String url = mediaUrl;
        if (url.contains("&rt=WP")) {
            url = url.replace("&rt=WP", "&rt=4G");
        }
        return url;
    }

    public String getIdentify() {
        if (identify == null)
            identify = "";
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public List<String> getListImage() {
        if (listImage == null)
            listImage = new ArrayList<>();
        return listImage;
    }

    public void setListImage(List<String> listImage) {
        this.listImage = listImage;
    }

    public String getListImageStr() {
        if (listImage != null && !listImage.isEmpty()) {
            StringBuilder sb = new StringBuilder("");
            for (int i = 0; i < listImage.size(); i++) {
                String img = listImage.get(i);
                if (!TextUtils.isEmpty(img)) {
                    sb.append(img);
                    sb.append("|");
                }
            }
            String tmp = sb.toString();
            if (tmp.length() > 0)
                return tmp.substring(0, tmp.length() - 1);
        }
        return "";
    }

    public String getDownloadUrlLossless() {
        return downloadUrlLossless;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public boolean isPlayLossless() {
        return isPlayLossless;
    }

    public void setPlayLossless(boolean playLossless) {
        isPlayLossless = playLossless;
    }

    public String getNormalMediaUrl() {
        if (TextUtils.isEmpty(normalMediaUrl))
            return mediaUrl;
        return normalMediaUrl;
    }

    public void setNormalMediaUrl(String normalMediaUrl) {
        this.normalMediaUrl = normalMediaUrl;
    }

    @Override
    public String toString() {
        return "{" +
                ", id=" + id +
                ", name=" + name +
                ", localId=" + localId +
                ", identify='" + identify + '\'' +
                ", image310='" + image310 + '\'' +
                ", image='" + image + '\'' +
                ", item_type=" + type +
                ", total_like=" + total_like +
                ", total_share=" + total_share +
                ", url='" + url + '\'' +
                '}';
    }

    public void unlike() {
        if (total_like > 0)
            total_like--;
        is_like = 0;
    }

    public void like() {
        if (total_like < 0)
            total_like = 0;
        total_like++;
        is_like = 1;
    }

    public List<AllModel> getSongList() {
        if (songList == null)
            songList = new ArrayList<>();
        return songList;
    }

    public void setSongList(List<AllModel> songList) {
        this.songList = songList;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public int getSeek() {
        return seek;
    }

    public void setSeek(int seek) {
        this.seek = seek;
    }

    public String getLyricUrl() {
        return lyricUrl;
    }

    public void setLyricUrl(String lyricUrl) {
        this.lyricUrl = lyricUrl;
    }

    public List<StreamingModel> getResolutionList() {
        if (resolutionList == null)
            resolutionList = new ArrayList<>();
        return resolutionList;
    }

    public void setResolutionList(List<StreamingModel> resolutionList) {
        this.resolutionList = resolutionList;
    }

    public boolean isAdaptiveStreaming() {
        return !getResolutionList().isEmpty();
    }

    public long getAlbumId() {
        return album_id;
    }

    public void setAlbumId(long album_id) {
        this.album_id = album_id;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public boolean isLiveVideo() {
        if (!TextUtils.isEmpty(mediaUrl)) {
            return mediaUrl.endsWith(".m3u8");
        }
        return false;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getRecordName() {
        return recordName;
    }

    public void setRecordName(String recordName) {
        this.recordName = recordName;
    }

    public String getLinkYoutube() {
        return linkYoutube;
    }

    public String getIdYoutube() {
        if (TextUtils.isEmpty(idYoutube))
            idYoutube = Utilities.getIdYoutube(linkYoutube);
        return idYoutube;
    }

    public boolean isYoutube() {
        return type == Constants.TYPE_YOUTUBE;
    }

    public String getImageCover() {
        if (!TextUtils.isEmpty(imageCover))
            return imageCover;
        return getImage310();
    }

    public List<AllModel> getMediaList() {
        if (mediasList == null) mediasList = new ArrayList<>();
        return mediasList;
    }

    public void setMediaList(List<AllModel> mediasList) {
        this.mediasList = mediasList;
    }

    public boolean isVideoList() {
        return isVideoList == 1 || type == Constants.TYPE_ALBUM_VIDEO;
    }

    public MediaInfoExtra getInfoExtra() {
        return infoExtra;
    }

    public void setInfoExtra(MediaInfoExtra infoExtra) {
        this.infoExtra = infoExtra;
    }

    public int getTotalMedia() {
        return totalMedia;
    }

    public void setTotalMedia(int totalMedia) {
        this.totalMedia = totalMedia;
    }
}

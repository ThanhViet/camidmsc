package com.metfone.selfcare.module.netnews.HomeNews.fragment;

import android.animation.Animator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.netnews.ChildNews.fragment.ChildNewsFragment;
import com.metfone.selfcare.module.netnews.HomeNews.adapter.HomeNewsAdapterV5;
import com.metfone.selfcare.module.netnews.HomeNews.presenter.HomeNewsPresenter;
import com.metfone.selfcare.module.netnews.HomeNews.presenter.IHomeNewsPresenter;
import com.metfone.selfcare.module.netnews.HomeNews.view.IHomeNewsView;
import com.metfone.selfcare.module.netnews.activity.NetNewsActivity;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.response.HomeNewsResponse;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.home.fragment.TabNewsFragmentV2;
import com.vtm.adslib.AdsHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by HaiKE on 8/19/17.
 */

public class HomeNewsFragment extends BaseFragment implements IHomeNewsView, SwipeRefreshLayout.OnRefreshListener, AbsInterface.OnHomeNewsItemListener {

    @BindView(R.id.loadingView)
    View loadingView;

    @BindView(R.id.loadingFail)
    View loadingFail;

    @BindView(R.id.tvLoadFail)
    TextView tvLoadFail;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.tvNewsCount)
    TextView tvNewsCount;
    HomeNewsAdapterV5 adapterV5;
    LinearLayoutManager layoutManager;
    ArrayList<HomeNewsModel> datas = new ArrayList<>();

    IHomeNewsPresenter mPresenter;
    private ListenerUtils listenerUtils;
    private boolean isLoadSuccess = false;
    private boolean isRunningAnimation = false;
    private AdsHelper adsHelper;

    public static HomeNewsFragment newInstance() {
        HomeNewsFragment fragment = new HomeNewsFragment();
        fragment.mPresenter = new HomeNewsPresenter();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_news, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        if (mPresenter == null) {
            mPresenter = new HomeNewsPresenter();
        }

        mPresenter.onAttach(this);
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
        setUp(view);
//        checkLocation();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (AppStateProvider.getInstance().isLocationFavourite()) {
            if (mPresenter != null) {
                mPresenter.loadDataV5(true);
                AppStateProvider.getInstance().setLocationFavourite(false);
            }
        }
    }

    private void checkLocation() {
        //todo nếu cấp quyền location thì lấy locaiton, nếu không thì gọi api
        processShowTabAround();
    }

    private void processShowTabAround() {
        if (LocationHelper.getInstant(ApplicationController.self()).checkGooglePlayService()) {
            if (ContextCompat.checkSelfPermission(getBaseActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            } else {
                if (!LocationHelper.getInstant(ApplicationController.self()).isLocationServiceEnabled()) {
                } else if (!LocationHelper.getInstant(ApplicationController.self()).isNetworkLocationEnabled()) {
                } else {
                    LocationHelper.getInstant(ApplicationController.self()).getCurrentLatLng(new LocationHelper.GetLocationListener() {
                        @Override
                        public void onResponse(Location location) {
                            Log.d(TAG, "loadListStrangerAround getCurrentLatLng: onResponse: " + location.getLatitude() + "," +
                                    location.getLongitude());
                            LocationHelper.getInstant(ApplicationController.self()).setGetLocationListener(null);
                            LocationHelper.getInstant(ApplicationController.self()).saveLastLocationToPref(location);
                            //todo luu location mới để lần sau gọi request
                        }

                        @Override
                        public void onFail() {
                            Log.d(TAG, "loadListStrangerAround: onFail");
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void setUp(View view) {
//        layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        layout_refresh.setOnRefreshListener(this);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(getBaseActivity());
        layoutManager.setInitialPrefetchItemCount(11);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(11);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(layoutManager);

        adapterV5 = new HomeNewsAdapterV5(getBaseActivity(), datas, this);
        recyclerView.setAdapter(adapterV5);

        adsHelper = new AdsHelper(getBaseActivity());
        if (AdsUtils.checkShowAds()) {
            String adUnit = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_PLAYER_MUSIC);
            adsHelper.init(adUnit, AdSize.MEDIUM_RECTANGLE, new AdsHelper.AdsBannerListener() {
                @Override
                public void onAdShow(AdView adView) {
                    adapterV5.notifyDataSetChanged();
                }
            });
            adapterV5.setAdsHelper(adsHelper);
        }

        setupMoveTop();

        if (datas == null || (datas.size() == 0)) {
            loadingView.setVisibility(View.VISIBLE);
            loadingFail.setVisibility(View.GONE);
            if (mPresenter != null) {
                mPresenter.loadDataCacheV5(null);
                mPresenter.loadDataV5(false);
            }
        }
        tvNewsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getParentFragment() != null && getParentFragment() instanceof TabNewsFragmentV2) {
                    try {
                        TabNewsFragmentV2 tabNewsFragment = (TabNewsFragmentV2) getParentFragment();
                        tabNewsFragment.selectTab(1);
                        hideNewsCount();
                        AppStateProvider.getInstance().setNewsCount(0);
                        if (tabNewsFragment.getCurrentAdapter() != null && tabNewsFragment.getCurrentAdapter().getItem(1) instanceof ChildNewsFragment) {
                            ChildNewsFragment childNewsFragment = (ChildNewsFragment) tabNewsFragment.getCurrentAdapter().getItem(1);
                            childNewsFragment.updateLastNews();
                            isRunningAnimation = false;
                        }
                    } catch (Exception ex) {
                        Log.e(TAG, "Exception", ex);
                    }
                }
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    if (dy <= 0) {
                        showNewsCount();
                    } else {
                        hideNewsCount();
                    }
                } catch (Exception ex) {
                    Log.e(TAG, "Exception", ex);
                }
            }
        });
    }

    public void updateNewsCount(int size) {
        if (size > 0) {
            AppStateProvider.getInstance().setNewsCount(size);
            isRunningAnimation = false;
            showNewsCount();
        }
    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(getBaseActivity()) || isLoadSuccess || recyclerView == null || mPresenter == null)
            return;
        onRefresh();
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        loadingView.setVisibility(View.GONE);
        hideRefresh();
        if (!flag) {
            loadingFail();
        } else {
            isLoadSuccess = flag;
        }
    }

    @Override
    public void bindData(HomeNewsResponse response, boolean flag) {
        if (datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if (loadingView != null) {
            loadingView.setVisibility(View.GONE);
        }
        if (response != null) {
            if (response.getData() != null) {
                loadingComplete(response.getData(), flag);

            } else {
                loadingFail();
            }
        }
    }

    @Override
    public void bindCategoryV5(List<CategoryModel> categoryResponse) {
        if (adapterV5 != null) {
            adapterV5.setDataCatogory(categoryResponse);
        }
    }

    @Override
    public void bindCanCareV5(HomeNewsModel model) {
        if (model == null || model.getData().size() == 0 || datas == null || adapterV5 == null) {
            return;
        }
        if (!datas.get(0).getHeader().equalsIgnoreCase(model.getHeader())) {
            datas.add(0, model);
            adapterV5.notifyItemChanged(0);
        }
    }

    @Override
    public void saveDataV5Cache(String data) {
        if (!TextUtils.isEmpty(data)) {
            SharedPrefs.getInstance().put(SharedPrefs.HOME_NEW_V5, data);
        }
    }

    @Override
    public void saveDataCareV5Cache(String data) {
        if (!TextUtils.isEmpty(data)) {
            SharedPrefs.getInstance().put(SharedPrefs.HOME_NEW_CARE_V5, data);
        }
    }

    public void loadingComplete(ArrayList<HomeNewsModel> response, boolean flag) {
        datas.clear();
        datas.addAll(response);
        adapterV5.notifyDataSetChanged();

        if (mPresenter != null && flag) {
            mPresenter.loadCanCareV5();
        }
        if (datas.size() > 0) {
            if (layoutManager != null)
                layoutManager.scrollToPosition(0);
        }
    }

    public void loadingFail() {
        if (datas.size() == 0) {
            loadingFail.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.tvLoadFail)
    public void loadFailClick() {
        onRefresh();
    }

    @OnClick(R.id.imvRefresh)
    public void loadFailIconClick() {
        onRefresh();
    }

    @Override
    public void onRefresh() {
        if (loadingFail != null) {
            loadingFail.setVisibility(View.GONE);
        }
        if (mPresenter != null) {
            mPresenter.loadDataCacheV5(null);
            mPresenter.loadDataV5(false);
            if (adsHelper != null && AdsUtils.checkShowAds())
                adsHelper.loadAd();
        }
    }

    @Override
    public void onDestroyView() {
        if (adapterV5 != null) {
//            adapterV5.onDestroy();
        }
        mPresenter.onDetach();
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        if (adsHelper != null)
            adsHelper.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void onItemClick(NewsModel model) {
        readNews(model);
    }

    @Override
    public void onItemCategoryHeaderClick(int type, int categoryId, String categoryName) {
        if (type == HomeNewsAdapterV5.TYPE_CATEGORY) {
            if (getBaseActivity() != null && getBaseActivity() instanceof NetNewsActivity) {
                Bundle bundle = new Bundle();
                bundle.putInt(CommonUtils.KEY_CATEGORY_ID, categoryId);
                bundle.putString(CommonUtils.KEY_CATEGORY_NAME, categoryName);
                NetNewsActivity mainActivity = (NetNewsActivity) getBaseActivity();
                mainActivity.showFragment(CommonUtils.TAB_CATEGORY_NEWS, bundle, true);
            } else if (getBaseActivity() != null) {
                //start NetNewsActivity
                Intent intent = new Intent(getBaseActivity(), NetNewsActivity.class);
                intent.putExtra(CommonUtils.KEY_CATEGORY_ID, categoryId);
                intent.putExtra(CommonUtils.KEY_CATEGORY_NAME, categoryName);
                intent.putExtra(CommonUtils.KEY_TAB, CommonUtils.TAB_CATEGORY_NEWS);
                getBaseActivity().startActivity(intent);
            }
        }
    }

    @Override
    public void onItemListenClick(NewsModel model) {

    }

    @Override
    public void onItemEventClick(NewsModel model) {
        if (getBaseActivity() == null) return;
        if (getBaseActivity() instanceof NetNewsActivity) {
            Bundle bundle = new Bundle();
            bundle.putInt(CommonUtils.KEY_CATEGORY_ID, model.getID());
            bundle.putString(CommonUtils.KEY_CATEGORY_NAME, model.getTitle());
            ((NetNewsActivity) getBaseActivity()).showFragment(CommonUtils.TAB_NEWS_BY_EVENT, bundle, true);
            ((NetNewsActivity) getBaseActivity()).saveMarkRead(model.getID());
        } else {
            //start NetNewsActivity
            Intent intent = new Intent(getBaseActivity(), NetNewsActivity.class);
            intent.putExtra(CommonUtils.KEY_CATEGORY_ID, model.getID());
            intent.putExtra(CommonUtils.KEY_CATEGORY_NAME, model.getTitle());
            intent.putExtra(CommonUtils.KEY_TAB, CommonUtils.TAB_NEWS_BY_EVENT);
            getBaseActivity().startActivity(intent);
        }
    }

    @Override
    public void onItemEventHeaderClick() {
        if (getBaseActivity() == null) return;
        if (getBaseActivity() instanceof NetNewsActivity) {
            ((NetNewsActivity) getBaseActivity()).showFragment(CommonUtils.TAB_EVENT, null, true);
        } else {
            //start NetNewsActivity
            Intent intent = new Intent(getBaseActivity(), NetNewsActivity.class);
            intent.putExtra(CommonUtils.KEY_TAB, CommonUtils.TAB_EVENT);
            getBaseActivity().startActivity(intent);
        }
    }

    @Override
    public void onItemClickBtnMore(NewsModel model) {
        DialogUtils.showOptionNewsItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionNew(object, menuId);
            }
        });
    }

    @Override
    public void onItemCategoryClick(String cateName, String cateId) {
        if (getBaseActivity() != null && getBaseActivity() instanceof NetNewsActivity) {
            Bundle bundle = new Bundle();
            bundle.putInt(CommonUtils.KEY_CATEGORY_ID, Integer.parseInt(cateId));
            bundle.putString(CommonUtils.KEY_CATEGORY_NAME, cateName);
            NetNewsActivity mainActivity = (NetNewsActivity) getBaseActivity();
            mainActivity.showFragment(CommonUtils.TAB_SOURCE_TOP_NOW, bundle, true);
        } else {
            //start NetNewsActivity
            Intent intent = new Intent(getBaseActivity(), NetNewsActivity.class);
            intent.putExtra(CommonUtils.KEY_CATEGORY_ID, Integer.parseInt(cateId));
            intent.putExtra(CommonUtils.KEY_CATEGORY_NAME, cateName);
            intent.putExtra(CommonUtils.KEY_TAB, CommonUtils.TAB_SOURCE_TOP_NOW);
            getBaseActivity().startActivity(intent);
        }
    }

    @Override
    public void onItemClickVideo(NewsModel model) {
        readNews(model);
    }

    public void setupMoveTop() {
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    public void scrollToPosition(int position) {
        if (layoutManager != null && recyclerView != null) {
            recyclerView.stopScroll();
            layoutManager.scrollToPosition(0);
        }
    }

    public void showNewsCount() {
        if (tvNewsCount == null) return;
        if (tvNewsCount.getVisibility() == View.VISIBLE) return;
        if (isRunningAnimation) return;
        isRunningAnimation = true;

        int newsCount = AppStateProvider.getInstance().getNewsCount();
        if (newsCount == 0) return;
        String str;
        if (newsCount >= 20)
            str = "20+";
        else
            str = newsCount + "";
        tvNewsCount.setText(getResources().getString(R.string.count_news, str));

        tvNewsCount.setY(-tvNewsCount.getHeight());
        tvNewsCount.setVisibility(View.VISIBLE);
        tvNewsCount.animate().translationY(0)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        if (tvNewsCount != null) {
                            tvNewsCount.animate().setListener(null);
                        }
                        isRunningAnimation = false;
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                }).setDuration(500).start();
    }

    public void hideNewsCount() {
        if (tvNewsCount == null) return;
        if (tvNewsCount.getVisibility() == View.GONE) return;
        if (isRunningAnimation) return;
        isRunningAnimation = true;
        tvNewsCount.animate().translationY(-tvNewsCount.getHeight())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        if (tvNewsCount != null) {
                            tvNewsCount.setVisibility(View.GONE);
                            tvNewsCount.animate().setListener(null);
                        }
                        isRunningAnimation = false;
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                })
                .setDuration(300).start();
    }
}

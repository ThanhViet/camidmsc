package com.metfone.selfcare.module.keeng.base;

import android.view.View;

import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.CategoryModel;

public abstract class BaseListener {

    public interface OnClickMedia {
        void onMediaClick(View view, int position);

        void onMediaExpandClick(View view, int position);

        void onMediaClick(View view, int parent, int position);

        void onMediaExpandClick(View view, int parent, int position);

        void onLongClick(View view, int position);

        void onTopicClick(View view, int position);

        void onScrollEnd(boolean flag);

    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public interface OnClickBottomSheet {
        void onClickSheet(CategoryModel item);
    }

    public interface OnClickBottomPlayerSheet {
        void onClickSheet(int index);

        void onDeleteItem(int index);
    }

    public interface AlbumDetailListener {
        void onLoadMore();

        void onClickMedia(View view, int position);

        void onClickOptionMedia(View view, AllModel item);

    }

    public interface PlaylistDetailListener {
        void onLoadMore();

        void onClickMedia(View view, int position);

        void onClickOptionMedia(View view, AllModel item);
    }

}

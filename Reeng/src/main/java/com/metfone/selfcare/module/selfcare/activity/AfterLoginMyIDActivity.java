/*
package com.metfone.selfcare.module.selfcare.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.listeners.LoginStateListener;
import com.metfone.selfcare.module.selfcare.fragment.account.AddNumberFragment;
import com.metfone.selfcare.module.selfcare.fragment.account.VerifyNumberFragment;
import com.metfone.selfcare.module.selfcare.loginhelper.AuthUtility;
import com.metfone.selfcare.module.selfcare.loginhelper.dialogselectphone.DialogSelectPhone;
import com.metfone.selfcare.module.selfcare.model.SCNumberVerify;
import com.metfone.selfcare.module.selfcare.network.SCAccountCallback;
import com.metfone.selfcare.module.selfcare.network.SelfCareAccountApi;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.ClientSecretBasic;
import net.openid.appauth.TokenResponse;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.metfone.selfcare.module.selfcare.loginhelper.KeycloakConfig.CLIENT_SECRET;

*/
/**
 * Created by thanhnt72 on 4/25/2019.
 *//*


public class AfterLoginMyIDActivity extends BaseSlidingFragmentActivity implements SCAccountCallback.SCAccountApiListener, LoginStateListener {
    @BindView(R.id.rlBackground)
    RelativeLayout rlBackground;
    private String TAG = getClass().getSimpleName();

    private ApplicationController mApp;
    boolean isAddNumber = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = (ApplicationController) getApplication();
        setContentView(R.layout.activity_after_login);
        ButterKnife.bind(this);
        if (getCallingActivity() != null && getCallingActivity().getClassName().equals(SCAccountActivity.class.getCanonicalName())) {
            if (getIntent().getExtras() != null) {
                SCNumberVerify number = (SCNumberVerify) getIntent().getExtras().getSerializable("number");
                if (number != null) {
                    onSelectNumber(number);
                    isAddNumber = true;
                    return;
                } else {
                    showToast(R.string.e601_error_but_undefined);
                    onBackPressed();
                }
            }
            isAddNumber = true;
        }
        if (!isAddNumber)
            handleAuthorizationResponse(getIntent());
        else {
            goToAddNumber();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        InputMethodUtils.hideSoftKeyboard(this);
    }

    private void handleAuthorizationResponse(@NonNull Intent intent) {
        Log.i(TAG, "handleAuthorizationResponse");

        final AuthorizationResponse response = AuthorizationResponse.fromIntent(intent);
        AuthorizationException error = AuthorizationException.fromIntent(intent);

        if (response == null && error == null) {
            String accessToken = SelfCareAccountApi.getInstant(mApp).getAccessToken();
            onLoginMytelSuccess(accessToken);
            return;
        }


        // Log.i(TAG, "handleAuthorizationResponse token =" + response.jsonSerialize());

        AuthUtility.mAuthState = new AuthState(response, error);
        //createTokenExchangeRequest

        if (response != null) {
            showLoadingDialog("", R.string.loading);
            final AuthorizationService service = new AuthorizationService(this);
            service.performTokenRequest(response.createTokenExchangeRequest(), new ClientSecretBasic(CLIENT_SECRET),
                    new AuthorizationService.TokenResponseCallback() {
                        @Override
                        public void onTokenRequestCompleted(@Nullable TokenResponse tokenResponse, @Nullable AuthorizationException exception) {
                            Log.i(TAG, "onTokenRequestCompleted=" + tokenResponse);
                            if (exception != null || tokenResponse == null) {
                                Log.i(TAG, "Token Exchange failed", exception);
                                onLoginFail();
                            } else {
                                if (tokenResponse.accessToken != null) {
                                    AuthUtility.updateAuth(tokenResponse, exception);
                                    AuthUtility.persistAuthState(mApp);
                                    Log.i(TAG, String.format("Token Response [ Access Token: %s, ID Token: %s ]", tokenResponse.accessToken, tokenResponse.idToken));
                                    // Save this refreshToken in sharepreference if it is needed to use later
                                    String refreshToken = tokenResponse.refreshToken;
                                    Log.i(TAG, "refreshToken=" + refreshToken);
                                    mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_REFRESH_TOKEN, refreshToken).apply();
                                    onLoginMytelSuccess(tokenResponse.accessToken);
                                } else {
                                    onLoginFail();
                                }
                            }
                        }
                    });
            service.dispose();
        } else {
            showToast(R.string.e601_error_but_undefined);
            finish();
        }
    }


    private void onLoginMytelSuccess(final String accessToken) {
        SelfCareAccountApi.getInstant(mApp).saveAccessToken(accessToken);

        SelfCareAccountApi.getInstant(mApp).getListPhoneNumberAfterLogin(new ApiCallbackV2<ArrayList<SCNumberVerify>>() {
            @Override
            public void onSuccess(String lastId, ArrayList<SCNumberVerify> strings) throws JSONException {
                SelfCareAccountApi.getInstant(mApp).getAndSaveInfo();
                onGetListNumberDone(strings, accessToken);
            }

            @Override
            public void onError(final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onLoginFail();
                        if ("401".equals(s)) {
                            SelfCareAccountApi.getInstant(mApp).saveAccessToken("");
                            finish();
                            AfterLoginMyIDActivity.this.loginFromAnonymous();
                        }
                    }
                });

            }

            @Override
            public void onComplete() {

            }
        });

    }

    private void onGetListNumberDone(final ArrayList<SCNumberVerify> strings, final String accessToken) {
        this.accessToken = accessToken;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideLoadingDialog();
                if (strings == null || strings.isEmpty()) {     //nếu chưa có sdt nào thì thêm mới
                    goToAddNumber();
                } else if (strings.size() == 1) {
                    SCNumberVerify scNumberVerify = strings.get(0);
                    if (scNumberVerify.isVerify())
                        onSelectNumber(scNumberVerify);
                    else {
                        onShowDialogSelectPhone(strings);
                    }
                } else {
                    onShowDialogSelectPhone(strings);
                }
            }
        });
    }

    private void onShowDialogSelectPhone(ArrayList<SCNumberVerify> list) {
        rlBackground.setVisibility(View.VISIBLE);
        //TODO chon sdt
        new DialogSelectPhone(AfterLoginMyIDActivity.this).setListNumber(list).setPositiveListener(new PositiveListener<SCNumberVerify>() {
            @Override
            public void onPositive(SCNumberVerify result) {
                onSelectNumber(result);
            }
        }).setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                goToAddNumber();
            }
        }).show();
    }

    private ApiCallbackV2<String> apiCallbackAdd = new ApiCallbackV2<String>() {
        @Override
        public void onSuccess(String lastId, final String subId) throws JSONException {
            Log.i(TAG, "addnumber success: " + subId);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideLoadingDialog();
                    SCNumberVerify numberVerify = new SCNumberVerify(msisdn, false, subId);
                    goToVerifyNumber(numberVerify, true);
//                    onAddNumberSuccess(subId);
                }
            });

        }

        @Override
        public void onError(final String s) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideLoadingDialog();
                    if (!TextUtils.isEmpty(s))
                        showToast(s);
                    else
                        showToast(R.string.e601_error_but_undefined);
                }
            });

        }

        @Override
        public void onComplete() {

        }
    };

    */
/*private void onAddNumberSuccess(String subId) {
        SCNumberVerify numberVerify = new SCNumberVerify(msisdn, false, subId);
        goToVerifyNumber(numberVerify);


       *//*
*/
/* SCNumberVerify numberVerify = new SCNumberVerify(msisdn, false, subId);
        DialogAddNumber dialogOtp = new DialogAddNumber(this);
        dialogOtp.setSCNumberVerify(numberVerify);
        dialogOtp.setPositiveListener(new PositiveListener<SCNumberVerify>() {
            @Override
            public void onPositive(SCNumberVerify result) {
                showLoadingDialog("", R.string.loading);
                SelfCareAccountApi.getInstant(mApp).verifyPhoneNumber(result.getOtp(), result.getSubId(), apiCallbackVerify);
            }
        });
        dialogOtp.show();*//*
*/
/*
    }*//*



    private String msisdn;
    private String accessToken;

    private void onSelectNumber(final SCNumberVerify result) {
        Log.i(TAG, "onSelectNumber: result: " + result.toString());
        msisdn = result.getMsisdn();
        if (result.isVerify()) {
            SelfCareAccountApi.getInstant(mApp).loginMocha(result.getMsisdn(), accessToken, AfterLoginMyIDActivity.this);
        } else {
            //goi api gen otp
            showLoadingDialog("", R.string.loading);
            SelfCareAccountApi.getInstant(mApp).genOtpAfterAddNumber(result.getSubId(), new ApiCallbackV2<String>() {
                @Override
                public void onSuccess(String lastId, String s) throws JSONException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoadingDialog();
                            goToVerifyNumber(result, false);
//                            showDialogGenOtp(result);
                        }
                    });

                }

                @Override
                public void onError(String s) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoadingDialog();
                            showToast(R.string.e601_error_but_undefined);
                        }
                    });

                }

                @Override
                public void onComplete() {

                }
            });


        }
    }

    */
/*private void showDialogGenOtp(SCNumberVerify numberVerify) {
        DialogAddNumber dialogOtp = new DialogAddNumber(this);
        dialogOtp.setSCNumberVerify(numberVerify);
        dialogOtp.setPositiveListener(new PositiveListener<SCNumberVerify>() {
            @Override
            public void onPositive(SCNumberVerify result) {
                showLoadingDialog("", R.string.loading);
                SelfCareAccountApi.getInstant(mApp).verifyPhoneNumber(result.getOtp(), result.getSubId(), apiCallbackVerify);
            }
        });
        dialogOtp.show();
    }*//*



    private ApiCallbackV2<String> apiCallbackVerify = new ApiCallbackV2<String>() {
        @Override
        public void onSuccess(String lastId, String s) throws JSONException {
            Log.i(TAG, "verify success");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideLoadingDialog();
                    if (isAddNumber) {
                        EventBus.getDefault().postSticky(new SCAccountActivity.SCAccountEvent(SCAccountActivity.SCAccountEvent.UPDATE_INFO));

                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                }
            });
            if (!isAddNumber)
                SelfCareAccountApi.getInstant(mApp).loginMocha(msisdn, accessToken, AfterLoginMyIDActivity.this);
        }

        @Override
        public void onError(String s) {
            Log.i(TAG, "verify error");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideLoadingDialog();
                    showToast(R.string.e601_error_but_undefined);
                }
            });

        }

        @Override
        public void onComplete() {

        }
    };

    private void onLoginFail() {
        hideLoadingDialog();
        showToast(R.string.e601_error_but_undefined);
    }

    @Override
    public void onSuccess(String response) {
        ArrayList<String> listNumber = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            int code = jsonObject.optInt("errorCode", -1);
            if (code == 0) {
                JSONArray jsonArray = jsonObject.optJSONArray("phoneNo");
                if (jsonArray != null && jsonArray.length() != 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        listNumber.add(jsonArray.getString(i));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (String s : listNumber) {
            Log.i(TAG, "num: " + s);
        }
    }

    @Override
    public void onError(int code, String message) {

    }

    @Override
    public void onLoginSuccess(String accessToken) {

        final String avatar = mApp.getPref().getString(SCConstants.PREFERENCE.SC_AVATAR, "");
        String name = mApp.getPref().getString(SCConstants.PREFERENCE.SC_FULL_NAME, "");
        long newBirthday = mApp.getPref().getLong(SCConstants.PREFERENCE.SC_BIRHDAY_TMP, 0L);
        String gender = mApp.getPref().getString(SCConstants.PREFERENCE.SC_GENDER_TMP, "female");
        if (!TextUtils.isEmpty(avatar))
            mApp.getAvatarBusiness().downloadAndSaveAvatar(mApp, null, avatar);
        ReengAccount reengAccount = mApp.getReengAccountBusiness().getCurrentAccount();
        reengAccount.setName(name);
        String birthDay = String.valueOf(TimeHelper.formatTimeBirthdayString(newBirthday));
        reengAccount.setBirthday(String.valueOf(newBirthday));
        reengAccount.setBirthdayString(birthDay);
        reengAccount.setGender("male".equals(gender) ? Constants.CONTACT.GENDER_MALE : Constants.CONTACT.GENDER_FEMALE);
        mApp.getReengAccountBusiness().updateReengAccount(reengAccount);

        if (TextUtils.isEmpty(name)) {
            hideLoadingDialog();
            SCAccountActivity.startMyIDAccountFragment(AfterLoginMyIDActivity.this, true);
            finish();

        } else {
            goToHome();
        }
        */
/*DeepLinkHelper.getInstance().openSchemaLink(this, "mytel://home/selfcare?clearStack=1");
        clearBackStack();
        finish();*//*

    }

    */
/*private void getUserInfoAndGoToMyIDFragment() {
        ProfileRequestHelper.onResponseUserInfoListener listener = new ProfileRequestHelper
                .onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
                hideLoadingDialog();
                SCAccountActivity.startMyIDAccountFragment(AfterLoginMyIDActivity.this);
                finish();
            }

            @Override
            public void onError(int errorCode) {
                hideLoadingDialog();
                SCAccountActivity.startMyIDAccountFragment(AfterLoginMyIDActivity.this);
                finish();
            }
        };
        ProfileRequestHelper.getInstance(mApp).getUserInfo(mApp.getReengAccountBusiness().getCurrentAccount(), listener);
    }*//*


    */
/*private void onSetUserInfo(String nameMytel) {
        hideLoadingDialog();
        ReengAccount account = mApp.getReengAccountBusiness().getCurrentAccount();
        account.setName(nameMytel);
        if (account.getBirthdayLong() == 0)
            account.setBirthday(String.valueOf(TimeHelper.BIRTHDAY_DEFAULT_PICKER));
        mApp.getReengAccountBusiness().updateReengAccount(account);
        ProfileRequestHelper.getInstance(mApp).setUserInfo(account, null);

        final String avatar = mApp.getPref().getString(SCConstants.PREFERENCE.SC_AVATAR, "");
        if (!TextUtils.isEmpty(avatar)) {
            mApp.getAvatarBusiness().downloadAndSaveAvatar(mApp, null, avatar);
        }

        Intent intent = new Intent(AfterLoginMyIDActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }*//*


    @Override
    public void onLoginError(String s) {
        onLoginFail();
    }

    public void onClickAddNumber(String number) {
        msisdn = number;
        showLoadingDialog("", R.string.loading);
        SelfCareAccountApi.getInstant(mApp).addPhoneNumber(msisdn, apiCallbackAdd);
    }

    public void goToAddNumber() {
        rlBackground.setVisibility(View.GONE);
        executeFragmentTransactionAllowLoss(AddNumberFragment.newInstance(), R.id.fragment_container, false, false, TAG);
    }

    public void goToVerifyNumber(SCNumberVerify scNumberVerify, boolean addBackStack) {
        msisdn = scNumberVerify.getMsisdn();
        rlBackground.setVisibility(View.GONE);
        executeAddFragmentTransaction(VerifyNumberFragment.newInstance(scNumberVerify), R.id.fragment_container, addBackStack, false);
    }

    public void onInputOtp(SCNumberVerify scNumberVerify) {
        showLoadingDialog("", R.string.loading);
        SelfCareAccountApi.getInstant(mApp).verifyPhoneNumber(scNumberVerify.getOtp(), scNumberVerify.getSubId(), apiCallbackVerify);
    }

}
*/

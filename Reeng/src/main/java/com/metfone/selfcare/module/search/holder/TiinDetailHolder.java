package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import butterknife.BindView;
import butterknife.OnClick;

public class TiinDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_description)
    @Nullable
    TextView tvDescription;
    @BindView(R.id.tv_source)
    @Nullable
    TextView tvSource;
    @BindView(R.id.tv_created_date)
    @Nullable
    TextView tvCreatedDate;
    @BindView(R.id.tv_fixed_dot)
    @Nullable
    TextView tvDot;

    private SearchAllListener.OnAdapterClick listener;
    private TiinModel data;

    public TiinDetailHolder(View view, Activity activity, SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
    }

    public void bindData(Object item, boolean isEnd, String keySearch) {
        if (item instanceof TiinModel) {
            data = (TiinModel) item;
            if (tvTitle != null) {
                tvTitle.setText(data.getTitle());
            }
            if (tvSource != null) {
                tvSource.setText(data.getCategory());
                tvSource.setVisibility(TextUtils.isEmpty(data.getCategory()) ? View.GONE : View.VISIBLE);
            }
            if (tvDescription != null) {
                tvDescription.setText(data.getShapo());
                tvDescription.setVisibility(TextUtils.isEmpty(data.getShapo()) ? View.GONE : View.VISIBLE);
            }
            if (tvCreatedDate != null) {
                tvCreatedDate.setText(DateTimeUtils.calculateTime(tvCreatedDate.getResources(), data.getDatePub() * 1000));
                tvCreatedDate.setVisibility(data.getDatePub() > 0 ? View.VISIBLE : View.GONE);
            }
            if (tvDot != null) {
                tvDot.setVisibility(!TextUtils.isEmpty(data.getCategory()) && data.getDatePub() > 0 ? View.VISIBLE : View.GONE);
            }
            if (ivCover != null)
//                ImageLoader.setNewsImage(ivCover.getContext(), data.getImage(), ivCover);
                ImageBusiness.setNews(data.getImage(), ivCover);
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxTiin && data != null) {
            ((SearchAllListener.OnClickBoxTiin) listener).onClickTiinItem(data);
        }
    }

    @OnClick(R.id.iv_more)
    public void onClickItemMore() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            listener.onClickMenuMore(data);
        }
    }
}

package com.metfone.selfcare.module.sc_umoney.network;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.sc_umoney.network.request.RegisterRequest;
import com.metfone.selfcare.module.sc_umoney.network.request.TopUpRequest;

public class UMoneyApi extends BaseApi {
    private static final String GET_UMONEY_INFO = "/ReengBackendBiz/umoney/info";
    private static final String POST_REGISTER = "/ReengBackendBiz/umoney/reg";
    private static final String POST_TOPUP = "/ReengBackendBiz/umoney/topup";
    private static final String GET_TELECOM_INFO = "/ReengBackendBiz/umoney/bccs/info";
    private static final String GET_UMONEY_BALANCE = "/ReengBackendBiz/umoney/balance";
    private String domain;
    private ApplicationController app;

    public UMoneyApi(ApplicationController app) {
        super(app);
        domain = getDomainFile();
        this.app = app;
    }

    @Override
    protected Http.Builder get(String baseUrl, String url) {
        Http.Builder builder = super.get(baseUrl, url);
        builder.putHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        return builder;
    }

    @Override
    protected Http.Builder post(String baseUrl, String url) {
        Http.Builder builder = super.post(baseUrl, url);
        builder.putHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        return builder;
    }

    public void getInfoUmoney(HttpCallBack httpCallBack) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //todo token, timestamp luon co
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(myAccount.getToken())
                .append(timeStamp);
        get(domain, GET_UMONEY_INFO)
                .putParameter("msisdn", myAccount.getJidNumber())
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getTelecomInfo(HttpCallBack httpCallBack) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //todo token, timestamp luon co
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(myAccount.getToken())
                .append(timeStamp);
        get(domain, GET_TELECOM_INFO)
                .putParameter("msisdn", myAccount.getJidNumber())
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void registerUmoney(HttpCallBack httpCallBack, RegisterRequest request) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //msisdn + clientType + revision + serviceCode + birthday + gender + paperType + paperNum
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(request.getServiceCode())
                .append(request.getBirthday())
                .append(request.getGender())
                .append(request.getPaperType())
                .append(request.getPaperNum())
                .append(myAccount.getToken())
                .append(timeStamp);
        post(domain, POST_REGISTER)
                .putParameter("msisdn", myAccount.getJidNumber())
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("serviceCode", request.getServiceCode() + "")
                .putParameter("name", request.getName())
                .putParameter("birthday", request.getBirthday())
                .putParameter("gender", request.getGender() + "")
                .putParameter("paperType", request.getPaperType())
                .putParameter("paperNum", request.getPaperNum())
                .putParameter("transDesc", request.getTransDesc())
                .putParameter("countryCode", "LA")
                .putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void postTopup(HttpCallBack httpCallBack, TopUpRequest request) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //md5= msisdn + clientType + revision +amount + roleId + actionNote + gender + pancode + pincode + msisdnReceiv + accountId
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(request.getAmount())
                .append(request.getRoleId())
                .append(request.getActionNote())
                .append(request.getPanCode())
                .append(request.getPinCode())
                .append(request.getMsisdnReceiv())
                .append(request.getAccountId())
                .append(myAccount.getToken())
                .append(timeStamp);
        post(domain, POST_TOPUP)
                .putParameter("msisdn", myAccount.getJidNumber())
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("amount", request.getAmount())
                .putParameter("roleId", request.getRoleId())
                .putParameter("actionNote", request.getActionNote())
                .putParameter("pancode", request.getPanCode())
                .putParameter("pincode", request.getPinCode())
                .putParameter("msisdnReceiv", request.getMsisdnReceiv())
                .putParameter("accountId", request.getAccountId())
                .putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage())
                .putParameter("serviceType", request.getServiceType())
                .putParameter("subType", request.getSubType())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getUmoneyBalance(HttpCallBack httpCallBack) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        StringBuilder sb = new StringBuilder();
        //todo token, timestamp luon co
        sb.append(myAccount.getJidNumber())
                .append("Android")
                .append(Config.REVISION)
                .append(myAccount.getToken())
                .append(timeStamp);
        get(domain, GET_UMONEY_BALANCE)
                .putParameter("msisdn", myAccount.getJidNumber())
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage())
                .withCallBack(httpCallBack)
                .execute();
    }
}

package com.metfone.selfcare.module.keeng.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.SearchModel;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSearchSuggest implements Serializable {

	private static final long serialVersionUID = 2140162184794294725L;
	@SerializedName("grouped")
	public Grouped grouped;

	public static class Grouped implements Serializable {
		private static final long serialVersionUID = 568989243828465716L;
		@SerializedName("type")
		public ResType type;
	}

	public static class ResType implements Serializable {
		private static final long serialVersionUID = -6841304470618971712L;
		@SerializedName("groups")
		public ArrayList<Group> groups;
	}

	public static class Group implements Serializable {
		private static final long serialVersionUID = 3166565783930022905L;
		@SerializedName("groupValue")
		public String groupValue;

		@SerializedName("doclist")
		public DocList doclist;
	}

	public static class DocList implements Serializable {
		private static final long serialVersionUID = 5678000400286661230L;
		@SerializedName("docs")
		public ArrayList<SearchModel> docs;
	}

}
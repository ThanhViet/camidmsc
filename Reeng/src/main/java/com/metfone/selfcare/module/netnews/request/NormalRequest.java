package com.metfone.selfcare.module.netnews.request;

/**
 * Created by HaiKE on 8/19/17.
 */

public class NormalRequest {

    int num;
    int page;

    public NormalRequest(int page, int num)
    {
        this.page = page;
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}

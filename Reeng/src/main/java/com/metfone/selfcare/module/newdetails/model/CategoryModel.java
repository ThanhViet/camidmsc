package com.metfone.selfcare.module.newdetails.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 8/20/17.
 */

public class CategoryModel implements Serializable {
    @SerializedName("ID")
    @Expose
    private int id;

    @SerializedName("Pid")
    @Expose
    private int Pid;

    @SerializedName("Image")
    @Expose
    private String image;

    @SerializedName("ColorName")
    @Expose
    private String colorName;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Url")
    @Expose
    private String url;

    @SerializedName("IsSelect")
    @Expose
    private int isSelect;

    private boolean isFollow = true;

    private boolean isEdit = false;

    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public CategoryModel(int id, int pid, String name, String image, boolean isFollow, int type) {
        this.id = id;
        Pid = pid;
        this.image = image;
        this.name = name;
        this.isFollow = true;
        this.type = type;
    }

    public CategoryModel(int id, int pid, String name, String image, boolean isFollow, int type, int isSelect) {
        this.id = id;
        Pid = pid;
        this.image = image;
        this.name = name;
        this.isFollow = true;
        this.type = type;
        this.isSelect = isSelect;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    public int getId() {
        return id;
    }

    public int getPid() {
        return Pid;
    }

    public void setPid(int pid) {
        Pid = pid;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        if (name.equalsIgnoreCase("nettv")) {
            name = "Video";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(int isSelect) {
        this.isSelect = isSelect;
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", name='" + name + '\'' +
                ", isFollow=" + isFollow +
                ", type=" + type +
                '}';
    }
}

package com.metfone.selfcare.module.home_kh.nearfriend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.holder.NearYouHolder;

import java.util.ArrayList;

public class NearYouKhAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = NearYouKhAdapter.class.getSimpleName();
    private Context mContext;
    private NearYouInterface mListener;
    private LayoutInflater infalter;
    private ArrayList<StrangerMusic> mStrangerMusics;

    public NearYouKhAdapter(Context context, ArrayList<StrangerMusic> strangerMusics, NearYouInterface listener) {
        this.mContext = context;
        this.infalter = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mStrangerMusics = strangerMusics;
        this.mListener = listener;
    }

    public void setDatas(ArrayList<StrangerMusic> strangerMusics) {
        this.mStrangerMusics = strangerMusics;
    }

    public Object getItem(int position) {
        return mStrangerMusics.get(position);
    }

    @Override
    public int getItemCount() {
        if (mStrangerMusics == null) return 0;
        return mStrangerMusics.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View convertView = infalter.inflate(R.layout.item_stranger_around_kh, parent, false);
        return new NearYouKhHolder(convertView, mContext, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((NearYouKhHolder) holder).setElement(getItem(position));
    }

    public interface NearYouInterface {
        void onClickListen(StrangerMusic entry);

        void onClickPoster(StrangerMusic entry);

        void onClickMessage(StrangerMusic entry);
    }
}

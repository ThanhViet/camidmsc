package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCChargeInfo;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCCharge extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCChargeInfo> data;

    public ArrayList<SCChargeInfo> getData() {
        return data;
    }

    public void setData(ArrayList<SCChargeInfo> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCCharge [data=" + data + "] errror " + getErrorCode();
    }
}

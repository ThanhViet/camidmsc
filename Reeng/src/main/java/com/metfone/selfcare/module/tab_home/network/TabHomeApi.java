package com.metfone.selfcare.module.tab_home.network;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.HomeContent;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;

public class TabHomeApi extends BaseApi {

    private static final String TAG = TabHomeApi.class.getSimpleName();

    public TabHomeApi() {
        super(ApplicationController.self());
    }

    public void getHome(@Nullable final ApiCallbackV2<TabHomeResponse> listener) {
        long currentTime = TimeHelper.getCurrentTime();
        String domain = getDomainOnMedia();
        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LAST_TIME_DATA_TAB_HOME, System.currentTimeMillis());
        ReengAccount reengAccount = getReengAccount();
        String accountToken = reengAccount != null ? reengAccount.getToken() : "";
        String msisdn = reengAccount != null ? reengAccount.getJidNumber() : "";
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String countryCode = getReengAccountBusiness().getRegionCode();
        String languageCode = getReengAccountBusiness().getCurrentLanguage();
        String vip = getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP;
        String timestamp = String.valueOf(currentTime);
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(domain);
        sb.append(clientType);
        sb.append(revision);
        sb.append(countryCode);
        sb.append(languageCode);
        sb.append(vip);
        sb.append(accountToken);
        sb.append(currentTime);
        String security = HttpHelper.encryptDataV2(application, sb.toString(), accountToken);

        Http.Builder builder;
        builder = get(domain, Url.OnMedia.API_GET_TAB_HOME);
        builder.putParameter("msisdn", msisdn);
        builder.putParameter("domain", domain);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("countryCode", countryCode);
        builder.putParameter("languageCode", languageCode);
        builder.putParameter("vip", vip);
        builder.putParameter("uuid", Utilities.getUuidApp());
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("security", security);
        builder.putParameter("loginStatus", getReengAccountBusiness().isAnonymousLogin() ? "0" : "1");
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                JSONObject jsonObject = new JSONObject(response);
                TabHomeResponse tmp = gson.fromJson(jsonObject.optString("data"), new TypeToken<TabHomeResponse>() {
                }.getType());
                if (tmp == null)
                    SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW);
                else
                    SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW, tmp);
                if (listener != null) {
                    if (tmp != null)
                        listener.onSuccess("", tmp);
                    else
                        listener.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onFailure(String message) {
                Log.d(TAG, "getHome onFailure: " + message);
                if (listener != null)
                    listener.onError(message);
            }

            @Override
            public void onCompleted() {
                Log.d(TAG, "getHome onCompleted");
            }
        });
        builder.setTimeOut(30);
        builder.execute();
    }

    public static class TabHomeResponse {
        @SerializedName("lst_banner")
        private ArrayList<Content> listBanners;

        @SerializedName("lst_data")
        private ArrayList<HomeContent> listContents;

        public ArrayList<Content> getListBanners() {
            return listBanners;
        }

        public void setListBanners(ArrayList<Content> listBanners) {
            this.listBanners = listBanners;
        }

        public ArrayList<HomeContent> getListContents() {
            return listContents;
        }

        public void setListContents(ArrayList<HomeContent> listContents) {
            this.listContents = listContents;
        }
    }
}

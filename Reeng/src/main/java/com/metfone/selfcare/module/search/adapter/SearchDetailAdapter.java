/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.adapter;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.adapter.rewardtopic.RewardTopicListHolder;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.search.holder.ChannelVideoDetailHolder;
import com.metfone.selfcare.module.search.holder.CreateThreadChatHolder;
import com.metfone.selfcare.module.search.holder.HistorySearchHolder;
import com.metfone.selfcare.module.search.holder.MVDetailHolder;
import com.metfone.selfcare.module.search.holder.MoviesDetailHolder;
import com.metfone.selfcare.module.search.holder.MusicSongHolder;
import com.metfone.selfcare.module.search.holder.NewsDetailHolder;
import com.metfone.selfcare.module.search.holder.RewardDetailHolder;
import com.metfone.selfcare.module.search.holder.ThreadMessageDetailHolder;
import com.metfone.selfcare.module.search.holder.TiinDetailHolder;
import com.metfone.selfcare.module.search.holder.VideoDetailHolder;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.search.model.CreateThreadChat;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.util.ArrayList;

public class SearchDetailAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {

    private static final int TYPE_HISTORY_SEARCH = 1;
    private static final int TYPE_THREAD_CHAT_DETAIL = 2;
    private static final int TYPE_VIDEO_DETAIL = 3;
    private static final int TYPE_NEWS_DETAIL = 4;
    private static final int TYPE_MOVIES_DETAIL = 5;
    private static final int TYPE_MUSIC_DETAIL = 6;
    private static final int TYPE_CHANNEL_DETAIL = 7;
    private static final int TYPE_MV_DETAIL = 8;
    private static final int TYPE_CREATE_CHAT = 9;
    private static final int TYPE_TIIN_DETAIL = 20;
    private static final int TYPE_MOVIE_ALL = 22;
    private static final int TYPE_REWARD_DETAIL = 23;

    private static final int TYPE_CHAT_SUBMIT = 10;
    private static final int TYPE_CONTACT_SUBMIT = 11;
    private static final int TYPE_VIDEO_SUBMIT = 12;
    private static final int TYPE_NEWS_SUBMIT = 13;
    private static final int TYPE_MOVIES_SUBMIT = 14;
    private static final int TYPE_MUSIC_SUBMIT = 15;
    private static final int TYPE_CHANNEL_SUBMIT = 16;
    private static final int TYPE_MV_SUBMIT = 17;
    private static final int TYPE_HISTORY_CONTACT = 18;
    private static final int TYPE_HISTORY_CHANNEL = 19;
    private static final int TYPE_TIIN_SUBMIT = 21;

    private SearchAllListener.OnAdapterClick listener;
    private int parentType;
    private String keySearch;

    public SearchDetailAdapter(Activity activity) {
        super(activity);
    }

    public SearchDetailAdapter(Activity activity, ArrayList<Object> list) {
        super(activity, list);
    }

    public void setParentType(int parentType) {
        this.parentType = parentType;
    }

    public void setListener(SearchAllListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    public void setKeySearch(String keySearch) {
        this.keySearch = keySearch;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);

        switch (parentType) {
            case SearchAllAdapter.TYPE_CHAT:
                if (item instanceof ThreadMessage) return TYPE_THREAD_CHAT_DETAIL;
                if (item instanceof PhoneNumber) return TYPE_THREAD_CHAT_DETAIL;
                if (item instanceof ContactProvisional) return TYPE_THREAD_CHAT_DETAIL;
                if (item instanceof CreateThreadChat) return TYPE_CREATE_CHAT;
                break;
            case SearchAllAdapter.TYPE_VIDEO:
                if (item instanceof Video) return TYPE_VIDEO_DETAIL;
                break;
            case SearchAllAdapter.TYPE_CHANNEL:
                if (item instanceof Channel) return TYPE_CHANNEL_DETAIL;
                break;
            case SearchAllAdapter.TYPE_NEWS:
                if (item != null) return TYPE_NEWS_DETAIL;
                break;
            case SearchAllAdapter.TYPE_MOVIES:
                if (item != null) return TYPE_MOVIES_DETAIL;
                break;
            case SearchAllAdapter.TYPE_REWARD:
                if (item != null) return TYPE_REWARD_DETAIL;
                break;
            case SearchAllAdapter.TYPE_MUSIC:
                if (item instanceof SearchModel) {
                    if (((SearchModel) item).getType() == Constants.TYPE_SONG)
                        return TYPE_MUSIC_DETAIL;
                    if (((SearchModel) item).getType() == Constants.TYPE_VIDEO)
                        return TYPE_MV_DETAIL;
                }
                break;
            case SearchAllAdapter.TYPE_HISTORY:
                if (item != null) {
//                    if (item instanceof PhoneNumber)
//                        return TYPE_HISTORY_CONTACT;
//                    else if (item instanceof Channel)
//                        return TYPE_HISTORY_CHANNEL;
                    return TYPE_HISTORY_SEARCH;
                }
                break;
            case Constants.TAB_SEARCH_ALL: {
                if (item instanceof ThreadMessage) return TYPE_CHAT_SUBMIT;
                if (item instanceof PhoneNumber) return TYPE_CONTACT_SUBMIT;
                if (item instanceof ContactProvisional) return TYPE_CONTACT_SUBMIT;
                if (item instanceof CreateThreadChat) return TYPE_CREATE_CHAT;
                if (item instanceof Video) return TYPE_VIDEO_SUBMIT;
                if (item instanceof Channel) return TYPE_CHANNEL_SUBMIT;
                if (item instanceof Movie) return TYPE_MOVIES_SUBMIT;
                if (item instanceof SearchModel) {
                    if (((SearchModel) item).getType() == Constants.TYPE_SONG)
                        return TYPE_MUSIC_SUBMIT;
                    if (((SearchModel) item).getType() == Constants.TYPE_VIDEO)
                        return TYPE_MV_SUBMIT;
                }
                if (item instanceof NewsModel) return TYPE_NEWS_SUBMIT;
                if (item instanceof TiinModel) return TYPE_TIIN_SUBMIT;
                if (item instanceof SearchRewardResponse.Result.ItemReward) return TYPE_REWARD_DETAIL;
            }
            break;
            case SearchAllAdapter.TYPE_TIIN:
                if (item != null) return TYPE_TIIN_DETAIL;
                break;
            case SearchAllAdapter.TYPE_MOVIES_ALL:
                if(item != null) return TYPE_MOVIE_ALL;
                break;
            default:
                break;
        }

        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public BaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_THREAD_CHAT_DETAIL:
            case TYPE_CHAT_SUBMIT:
            case TYPE_CONTACT_SUBMIT:
                return new ThreadMessageDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_thread_message_detail, parent, false)
                        , activity, listener);
            case TYPE_VIDEO_DETAIL:
                return new VideoDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_video_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_CHANNEL_DETAIL:
                return new ChannelVideoDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_channel_video_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_NEWS_DETAIL:
            case TYPE_NEWS_SUBMIT:
                return new NewsDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_news_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_MOVIES_DETAIL:
            case TYPE_MOVIES_SUBMIT:
            case TYPE_MOVIE_ALL:
                return new MoviesDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_movies_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_MUSIC_DETAIL:
            case TYPE_MUSIC_SUBMIT:
                return new MusicSongHolder(layoutInflater.inflate(R.layout.holder_search_all_music_song, parent, false)
                        , activity, listener, viewType);
            case TYPE_HISTORY_SEARCH:
                return new HistorySearchHolder(layoutInflater.inflate(R.layout.holder_search_all_history_search, parent, false)
                        , activity, listener);
            case TYPE_VIDEO_SUBMIT:
                return new VideoDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_video_submit_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_CHANNEL_SUBMIT:
                return new ChannelVideoDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_channel_submit_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_CREATE_CHAT:
                return new CreateThreadChatHolder(layoutInflater.inflate(R.layout.holder_search_all_create_thread_chat, parent, false)
                        , listener);
            case TYPE_MV_DETAIL:
                return new MVDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_mv_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_MV_SUBMIT:
                return new MVDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_mv_submit_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_TIIN_DETAIL:
            case TYPE_TIIN_SUBMIT:
                return new TiinDetailHolder(layoutInflater.inflate(R.layout.holder_search_all_news_detail, parent, false)
                        , activity, listener, parentType);
            case TYPE_REWARD_DETAIL:
                return new RewardDetailHolder(layoutInflater.inflate(R.layout.item_reward_shop_kh, parent, false), activity, (SearchAllListener.OnClickReward) listener, parentType);
            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseAdapter.ViewHolder holder, int position) {
        if (holder instanceof MusicSongHolder) {
            ((MusicSongHolder) holder).bindData(getItem(position), position, (getItemCount() == position + 1), keySearch);
        } else if (holder instanceof NewsDetailHolder) {
            ((NewsDetailHolder) holder).bindData(getItem(position), (getItemCount() == position + 1), keySearch);
        } else if (holder instanceof ThreadMessageDetailHolder) {
            ((ThreadMessageDetailHolder) holder).bindData(getItem(position), position, keySearch);
        } else if (holder instanceof VideoDetailHolder) {
            ((VideoDetailHolder) holder).bindData(getItem(position), position, keySearch);
        } else if (holder instanceof ChannelVideoDetailHolder) {
            ((ChannelVideoDetailHolder) holder).bindData(getItem(position), position, keySearch);
        } else if (holder instanceof MoviesDetailHolder) {
            ((MoviesDetailHolder) holder).bindData(getItem(position), position, keySearch);
        } else if (holder instanceof MVDetailHolder) {
            ((MVDetailHolder) holder).bindData(getItem(position), position, keySearch);
        } else if (holder instanceof TiinDetailHolder) {
            ((TiinDetailHolder) holder).bindData(getItem(position), (getItemCount() == position + 1), keySearch);
        }else if(holder instanceof RewardDetailHolder){
            ((RewardDetailHolder) holder).bindData(getItem(position), position);
        }
        else {
            holder.bindData(getItem(position), position);
        }
    }
}

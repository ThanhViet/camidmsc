package com.metfone.selfcare.module.metfoneplus.adapter.divider;

import android.graphics.Rect;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


/*

Decorator which adds spacing around the tiles in a Grid layout RecyclerView. Apply to a RecyclerView with:
    SpacesItemDecoration decoration = new SpacesItemDecoration(16);
    mRecyclerView.addItemDecoration(decoration);

Feel free to add any value you wish for SpacesItemDecoration. That value determines the amount of spacing.
Source: http://blog.grafixartist.com/pinterest-masonry-layout-staggered-grid/
*/
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private final int mSpace;
    private final boolean mAddSpaceTopForFirstItem;
    private final boolean mAddSpaceBottomForLastItem;

    public SpacesItemDecoration(int space, boolean addSpaceTopForFirstIte, boolean addSpaceBottomForLastItem) {
        this.mSpace = space;
        this.mAddSpaceTopForFirstItem = addSpaceTopForFirstIte;
        this.mAddSpaceBottomForLastItem = addSpaceBottomForLastItem;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.bottom = mSpace;

        Log.e("SpacesItemDecoration", "getItemOffsets: mSpace = " + mSpace);
        // add top margin only for the first item to avoid double space between items
        if (parent.getChildAdapterPosition(view) == 0 && mAddSpaceTopForFirstItem) {
            outRect.top = mSpace;
        }

        // add bottom margin only for the last item to avoid double space between items
        if (parent.getAdapter() != null) {
            if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
                if (mAddSpaceBottomForLastItem) {
                    outRect.bottom = mSpace;
                } else {
                    outRect.bottom = 0;
                }
            }
        }
    }
}

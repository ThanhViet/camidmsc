package com.metfone.selfcare.module.home_kh.util;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.ArrayRes;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.PluralsRes;
import androidx.annotation.StringRes;
import androidx.annotation.StyleableRes;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;

import com.metfone.selfcare.app.dev.ApplicationController;

public final class ResourceUtils {

    private ResourceUtils() {
    }

    @NonNull
    public static Resources getResources() {
        return ApplicationController.getApp().getResources();
    }

    @NonNull
    public static String getString(@StringRes int stringResId) {
        /*
         * fix bug firebase: 'java.lang.String android.app.Activity.getString(int)' on a null object reference
         * */
        if (ApplicationController.self().getCurrentActivity() == null) return "";
        return ApplicationController.self().getCurrentActivity().getString(stringResId);
    }

    @NonNull
    public static String getString(@StringRes int stringResId, Object... formatArgs) {
        /*
         * fix bug firebase: 'java.lang.String android.app.Activity.getString(int)' on a null object reference
         * */
        if (ApplicationController.self().getCurrentActivity() == null) return "";
        return ApplicationController.self().getCurrentActivity().getString(stringResId, formatArgs);
    }

    @NonNull
    public static String getQuantityString(@PluralsRes int stringResId, int quantity) {
        return getResources().getQuantityString(stringResId, quantity);
    }

    @NonNull
    public static String getQuantityString(@PluralsRes int stringResId, int quantity, Object... formatArgs) {
        return getResources().getQuantityString(stringResId, quantity, formatArgs);
    }

    @ColorInt
    public static int getColor(@ColorRes int colorResId) {
        return ResourcesCompat.getColor(getResources(), colorResId, null);
    }

    @NonNull
    public static String getColorAsHexString(@ColorRes int colorResId) {
        int color = getColor(colorResId);
        return String.format("#%06X", 0xFFFFFF & color);
    }

    public static int getDimensionPixelSize(@DimenRes int dimesResId) {
        return getResources().getDimensionPixelSize(dimesResId);
    }

    @NonNull
    public static String[] getStringArray(@ArrayRes int arrayResId) {
        return getResources().getStringArray(arrayResId);
    }

    public static Drawable getDrawable(@DrawableRes int drawableResId) {
        return ResourcesCompat.getDrawable(getResources(),
                drawableResId,
                ApplicationController.getApp().getTheme());
    }

    public static void setBackgroundTint(@NonNull View view, @ColorRes int color) {
        ViewCompat.setBackgroundTintList(view, ContextCompat.getColorStateList(view.getContext(), color));
    }

    public static int getDimensionPixelSize(@NonNull TypedArray typedArray, @StyleableRes int attrResId, @DimenRes int defaultDimenResId) {
        return typedArray.getDimensionPixelSize(attrResId, getResources().getDimensionPixelSize(defaultDimenResId));
    }
}

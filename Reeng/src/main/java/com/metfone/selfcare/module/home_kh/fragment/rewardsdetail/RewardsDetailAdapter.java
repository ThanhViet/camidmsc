package com.metfone.selfcare.module.home_kh.fragment.rewardsdetail;

import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.model.StoreListDetail;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class RewardsDetailAdapter extends RecyclerView.Adapter<RewardsDetailAdapter.FilterVH> {

    private List<StoreListDetail> data = new ArrayList<>();
    private IGiftsItemClick iGiftsItemClick;

    public RewardsDetailAdapter(List<StoreListDetail> data, IGiftsItemClick iFilterClick) {
        this.data = data;
        this.iGiftsItemClick = iFilterClick;
    }

    @NonNull
    @Override
    public FilterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_available_store, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FilterVH holder, int position) {
        holder.bindData(position);
    }


    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public List<StoreListDetail> getData() {
        return data;
    }

    class FilterVH extends RecyclerView.ViewHolder {
        private ConstraintLayout layoutParent;
        private AppCompatImageView icon;

        private AppCompatTextView tvTitle;
        private AppCompatTextView tvContent;
        private AppCompatTextView tvDistance;
        private View line;

        public FilterVH(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvContent = itemView.findViewById(R.id.tv_content);
            tvDistance = itemView.findViewById(R.id.tv_distance);
            line = itemView.findViewById(R.id.line);
            layoutParent = itemView.findViewById(R.id.parent);
        }

        public void bindData(int position) {
            line.setVisibility(position == data.size() - 1 ? View.GONE : View.VISIBLE);
            StoreListDetail item = data.get(position);
            tvTitle.setText(item.getStoreName());
            tvContent.setText(item.getAddress());
            tvDistance.setText(String.format(ResourceUtils.getString(R.string.distance_detail), new DecimalFormat("##.##").format(item.getDistance())));
            layoutParent.setOnClickListener(v -> {
                if (iGiftsItemClick != null) {
                    iGiftsItemClick.itemClick(position);
                }
            });
        }
    }

    public interface IGiftsItemClick {
        void itemClick(int position);
    }
}

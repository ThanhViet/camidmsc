/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/18
 */

package com.metfone.selfcare.module.security.fragment;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.LoadingView;
import com.metfone.selfcare.module.security.adapter.SecurityDetailAdapter;
import com.metfone.selfcare.module.security.helper.SecurityApi;
import com.metfone.selfcare.module.security.helper.SecurityHelper;
import com.metfone.selfcare.module.security.listener.ScanVulnerabilityListener;
import com.metfone.selfcare.module.security.model.LogModel;
import com.metfone.selfcare.module.security.model.NewsModel;
import com.metfone.selfcare.module.security.model.PhoneNumberModel;
import com.metfone.selfcare.module.security.model.SecurityModel;
import com.metfone.selfcare.module.security.model.VulnerabilityModel;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.view.HeaderFrameLayout;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SecurityFragment extends BaseFragment implements RecyclerClickListener, ScanVulnerabilityListener {
    @BindView(R.id.headerController)
    LinearLayout headerController;
    @BindView(R.id.headerTop)
    HeaderFrameLayout headerTop;
    @BindView(R.id.bgHeader)
    View bgHeader;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.statusBarHeight)
    View statusBarHeight;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tv_title)
    TextView tvTitle2;
    @BindView(R.id.tv_description)
    TextView tvDesc;
    @BindView(R.id.button_scan)
    TextView btnScan;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.icon)
    ImageView ivIcon;
    @BindView(R.id.loading_view)
    LoadingView loadingView;
    @BindView(R.id.loading_vulnerability)
    View loadingVulnerability;
    private Unbinder unbinder;
    private ArrayList<Object> data;
    private SecurityDetailAdapter adapter;
    private boolean isLoading;

    public static SecurityFragment newInstance() {
        Bundle args = new Bundle();
        SecurityFragment fragment = new SecurityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.ivBack, R.id.tab_security_center, R.id.tab_spam, R.id.tab_firewall, R.id.button_scan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack: {
                onBackPressed();
            }
            break;

            case R.id.tab_security_center: {
                NavigateActivityHelper.navigateToTabSecurityCenter(mActivity);
            }
            break;

            case R.id.tab_spam: {
                NavigateActivityHelper.navigateToTabSecuritySpamSms(mActivity);
            }
            break;

            case R.id.tab_firewall: {
                NavigateActivityHelper.navigateToTabSecurityFirewallSms(mActivity);
            }
            break;

            case R.id.button_scan: {
                scanVulnerability();
                loadData(false);
            }
            break;
        }
    }

    @Override
    public String getName() {
        return "SecurityFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_security_home;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!(mActivity instanceof HomeActivity)) {
            ivBack.setVisibility(View.VISIBLE);
            headerTop.setVisibility(View.GONE);
//            statusBarHeight.setVisibility(View.GONE);
            tvTitle.setPadding(0, 0, 0, 0);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setTextColor(ContextCompat.getColor(mActivity, R.color.text_ab_title));
            headerController.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.white));
        }
        initView();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                long oldTimeScan = SharedPref.newInstance(mActivity).getLong(Constants.PREFERENCE.PREF_SECURITY_TIME_SCAN_VULNERABILITY, 0);
                if (oldTimeScan > 0) {
                    if (data == null) data = new ArrayList<>();
                    else data.clear();
                    if (loadingVulnerability != null) loadingVulnerability.setVisibility(View.GONE);
                    Date date = new Date(oldTimeScan);
                    @SuppressLint("SimpleDateFormat")
                    String lastScanDate = new SimpleDateFormat(DATE_FORMAT).format(date);
                    if (tvDesc != null)
                        tvDesc.setText(mActivity.getResources().getString(R.string.last_scan, lastScanDate));
                    ArrayList<String> tmp = SharedPref.newInstance(mActivity).getListString(Constants.PREFERENCE.PREF_SECURITY_VULNERABILITY_ON_DEVICE);
                    for (int i = 0; i < tmp.size(); i++) {
                        VulnerabilityModel item = SecurityHelper.getVulnerability(tmp.get(i));
                        if (item != null) data.add(item);
                    }
                    if (data.isEmpty()) {
                        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                        if (tvTitle2 != null) tvTitle2.setText(R.string.device_safe);
                        if (ivIcon != null) ivIcon.setImageResource(R.drawable.ic_security_safe);
                    } else {
                        if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
                        if (tvTitle2 != null) tvTitle2.setText(R.string.device_unsafe);
                        if (ivIcon != null) ivIcon.setImageResource(R.drawable.ic_security_unsafe);
                    }
                    if (btnScan != null) {
                        btnScan.setVisibility(View.VISIBLE);
                        btnScan.setText(R.string.btn_security_scan_again);
                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                } else {
                    if (btnScan != null) {
                        btnScan.setVisibility(View.VISIBLE);
                        btnScan.setText(R.string.btn_security_scan);
                    }
                    scanVulnerability();
                }

                long lastTime = SharedPref.newInstance(mActivity).getLong(Constants.PREFERENCE.PREF_LAST_TIME_GET_SECURITY_DATA, 0L);
                if (TimeHelper.checkTimeInDay(lastTime)) {
                    loadData(false);
                } else {
                    loadData(true);
                }
            }
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
    }

    private void initView() {
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        }
        if (data == null) data = new ArrayList<>();
        adapter = new SecurityDetailAdapter(mActivity, Constants.TAB_SECURITY_VULNERABILITY, data);
        adapter.setRecyclerClickListener(this);
        recyclerView.setAdapter(adapter);

    }

    private void scanVulnerability() {
        if (btnScan != null) {
            btnScan.setText(R.string.scanning);
            btnScan.setEnabled(false);
        }
        if (loadingVulnerability != null) loadingVulnerability.setVisibility(View.VISIBLE);
        SecurityHelper.ScanVulnerabilityTask task = new SecurityHelper.ScanVulnerabilityTask();
        task.setContext(mActivity);
        task.setListener(this);
        task.execute();
    }

    private void loadData(final boolean showLoading) {
        if (isLoading) return;
        if (showLoading && loadingView != null) {
            loadingView.loadBegin(R.drawable.circular_progress_bar_security);
        }
        isLoading = true;
        SecurityApi.getInstance().getConfig(new ApiCallbackV2<SecurityModel>() {
            @Override
            public void onSuccess(String lastId, SecurityModel model) throws JSONException {
                loadingFinish();
            }

            @Override
            public void onError(String s) {
                if (showLoading) {
                    loadError(s);
                } else {
                    loadingFinish();
                }
            }

            @Override
            public void onComplete() {
                isLoading = false;
                isDataInitiated = true;
            }
        });
        SecurityApi.getInstance().getLog(null);
    }

    private void loadingFinish() {
        if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
        if (loadingView != null) loadingView.loadFinish();
    }

    private void loadError(String error) {
        if (loadingView != null) {
            loadingView.loadError(error);
            loadingView.setLoadingErrorListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadData(true);
                }
            });
        }
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
    }

    private String DATE_FORMAT = "dd-MM-yyyy hh:mm aaa";

    @Override
    public void scanCompleted(ArrayList<VulnerabilityModel> list) {
        if (loadingVulnerability != null) loadingVulnerability.setVisibility(View.GONE);
        if (mActivity != null && !mActivity.isFinishing()) {
            if (data == null) data = new ArrayList<>();
            else data.clear();
            data.addAll(list);
            if (data.isEmpty()) {
                if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                if (tvTitle2 != null) tvTitle2.setText(R.string.device_safe);
                if (ivIcon != null) ivIcon.setImageResource(R.drawable.ic_security_safe);
            } else {
                if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
                if (tvTitle2 != null) tvTitle2.setText(R.string.device_unsafe);
                if (ivIcon != null) ivIcon.setImageResource(R.drawable.ic_security_unsafe);
            }
            ArrayList<String> tmp = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                tmp.add(list.get(i).getKey());
            }
            SharedPref.newInstance(mActivity).putListString(Constants.PREFERENCE.PREF_SECURITY_VULNERABILITY_ON_DEVICE, tmp);
            Date date = new Date();
            SharedPref.newInstance(mActivity).putLong(Constants.PREFERENCE.PREF_SECURITY_TIME_SCAN_VULNERABILITY, date.getTime());
            if (tvDesc != null) {
                @SuppressLint("SimpleDateFormat")
                String lastScanDate = new SimpleDateFormat(DATE_FORMAT).format(date);
                tvDesc.setText(mActivity.getResources().getString(R.string.last_scan, lastScanDate));
            }
            if (adapter != null) adapter.notifyDataSetChanged();
            if (btnScan != null) {
                btnScan.setEnabled(true);
                btnScan.setText(R.string.btn_security_scan_again);
            }
        }
    }

    @Override
    public void onClick(View v, int pos, Object object) {
        if (object instanceof LogModel) {

        } else if (object instanceof PhoneNumberModel) {

        } else if (object instanceof VulnerabilityModel) {
            NavigateActivityHelper.navigateToTabSecurityVulnerability(mActivity, (VulnerabilityModel) object);
        } else if (object instanceof NewsModel) {
            NavigateActivityHelper.navigateToTabSecurityNews(mActivity, (NewsModel) object);
        }
    }

    @Override
    public void onLongClick(View v, int pos, Object object) {

    }
}

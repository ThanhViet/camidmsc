/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.network;

public interface ApiCallback<T> extends com.metfone.selfcare.common.api.ApiCallback {
    void onSuccess(String msg, T result) throws Exception;
}

package com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.module.tiin.base.BasePresenter;
import com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.fragment.MainTiinDetailFragment;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.module.tiin.network.request.TiinRequest;
import com.metfone.selfcare.util.FilterContentSeenUtil;

import org.json.JSONObject;

import java.util.ArrayList;

public class MainDetailTiinPresenter extends BasePresenter implements IMainDetailTiinMvpPresenter {
    private TiinApi mTiinApi;
    long startTime;
    private FilterContentSeenUtil filterContentSeenUtil;

    public MainDetailTiinPresenter() {
        mTiinApi = new TiinApi(ApplicationController.self());
    }

    @Override
    public void getDataSibling(int id, int page, int num) {
        startTime = System.currentTimeMillis();
        mTiinApi.getNewSibling(httpCallBack, new TiinRequest(page, num, id));
    }

    @Override
    public void onDestroyView() {
        filterContentSeenUtil = null;
    }

    @Override
    public void addItemSeen(String id) {
        getFilterContentSeenUtil().addContentSeen(id);
    }

    HttpCallBack httpCallBack = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof MainTiinDetailFragment)) {
                return;
            }
            Gson gson = new Gson();
            ArrayList<TiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<ArrayList<TiinModel>>() {
            }.getType());
            //filter
            response = getFilterContentSeenUtil().filterList(response);
            ((MainTiinDetailFragment) getMvpView()).bindDataSibling(response);
            ((MainTiinDetailFragment) getMvpView()).loadDataSuccess(false);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_SIBLING_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof MainTiinDetailFragment)) {
                return;
            }
            ((MainTiinDetailFragment) getMvpView()).loadDataSuccess(false);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_SIBLING_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    public FilterContentSeenUtil getFilterContentSeenUtil() {
        if (filterContentSeenUtil == null) {
            filterContentSeenUtil = new FilterContentSeenUtil(Constants.CacheSeen.CACHE_IDS_TIIN);
        }
        return filterContentSeenUtil;
    }
}

package com.metfone.selfcare.module.netnews.bottomsheet;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.model.BottomSheetModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HaiKE on 9/15/17.
 */

public class BottomSheetData {
    public static final int BOOK_MARKS_NEWS = 1;
    public static final int ADD_FAVORITE = 2;
    public static final int SHARE = 3;
    public static final int COPY_LINK = 4;
    public static final int READ_ORIGINAL_NEWS = 5;
    public static final int ADD_QUEUE_LIST = 6;
    public static final int REMOVE_QUEUE_LIST = 7;
    public static final int READ_NEWS = 8;
    public static final int DOWNLOAD_OFFLINE = 9;
    public static final int FONT_SIZE = 10;
    public static final int SHARE_FACEBOOK = 11;

    public static List<BottomSheetModel> getPopupMoreNews(NewsModel item) {
        List<BottomSheetModel> list = new ArrayList<>();
        list.add(new BottomSheetModel(FONT_SIZE, R.drawable.ic_font_size_normal, R.string.font_size, true).setNewsModel(item));
        list.add(new BottomSheetModel(SHARE, R.drawable.ic_share_black, R.string.share).setNewsModel(item));
//        list.add(new BottomSheetModel(READ_ORIGINAL_NEWS, R.drawable.ic_original_news, R.string.read_original_news).setNewsModel(item));
        list.add(new BottomSheetModel(COPY_LINK, R.drawable.ic_bottom_copy_link, R.string.web_pop_copylink).setNewsModel(item));
//        list.add(new BottomSheetModel(SHARE_FACEBOOK, R.drawable.ic_share_fb, R.string.share_fb).setNewsModel(item));
        return list;
    }
}

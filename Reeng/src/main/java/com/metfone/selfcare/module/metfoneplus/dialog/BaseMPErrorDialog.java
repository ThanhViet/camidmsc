package com.metfone.selfcare.module.metfoneplus.dialog;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;

public class BaseMPErrorDialog extends BaseCustomDialog {
    private int drawableId;
    private int titleId;
    private int contentId;
    private String title;
    private String content;
    private int topStringId;
    private boolean visibilityTop;
    private boolean bgWhite;
    private View.OnClickListener topButtonClickListener;

    public void setBgWhite(boolean bgWhite) {
        this.bgWhite = bgWhite;
    }

    public BaseMPErrorDialog(Activity activity, int drawableId, int titleId, int contentId) {
        super(activity);
        this.drawableId = drawableId;
        this.titleId = titleId;
        this.contentId = contentId;
    }

    public BaseMPErrorDialog(Activity activity, int drawableId, String title, String content) {
        super(activity);
        this.drawableId = drawableId;
        this.title = title;
        this.content = content;
    }

    public BaseMPErrorDialog(Activity activity, int drawableId, int titleId, String content) {
        super(activity);
        this.drawableId = drawableId;
        this.titleId = titleId;
        try {
            this.title = activity.getString(titleId);
        } catch (Exception e) {

        }
        this.content = content;
    }

    public BaseMPErrorDialog(Activity activity, int drawableId, String title, String content, boolean visibilityTop, int topStringId,
                             View.OnClickListener topButtonClickListener) {
        super(activity);
        this.drawableId = drawableId;
        this.title = title;
        this.content = content;
        this.visibilityTop = visibilityTop;
        this.topStringId = topStringId;
        this.topButtonClickListener = topButtonClickListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getWindow() != null) {
            getWindow().setLayout(UserInfoBusiness.getWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
//            getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.transparent)));

        }
        setCancelable(true);
        if (visibilityTop) {
            updateUiDialog(true);
        } else {
            updateUiDialog();
        }
    }

    private void updateUiDialog() {
        if (title == null) {
            this.title = getActivity().getString(titleId);
        }
        if (bgWhite) {
            lnlContent.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_white_corner_6));
            mTitle.setTextColor(Color.parseColor("#CC212121"));
            mContent.setTextColor(Color.parseColor("#80212121"));
        }
        flImage.setVisibility(View.VISIBLE);
        mImage.setVisibility(View.VISIBLE);
        mImage.setImageResource(drawableId);

        mTitle.setText(title);
        mContent.setText(content);
        setButtonTopVisibility(View.GONE);
        setButtonBottomVisibility(View.GONE);
    }

    public void updateUiDialog(boolean visibilityTop) {
        if (bgWhite) {
            lnlContent.setBackgroundColor(Color.WHITE);
            mTitle.setTextColor(Color.parseColor("#CC212121"));
            mContent.setTextColor(Color.parseColor("#80212121"));
            if (getWindow() != null)
                getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.transparent)));
        }
        flImage.setVisibility(View.VISIBLE);
        mImage.setVisibility(View.VISIBLE);
        mImage.setImageResource(drawableId == 0 ? R.drawable.image_error_dialog : drawableId);

        if (title == null) {
            this.title = getActivity().getString(titleId);
        }
        mTitle.setText(title);
        mContent.setText(content);
        if (visibilityTop) {
            setButtonTopVisibility(View.VISIBLE);
            setTopButton(topStringId, R.drawable.bg_red_button_corner_6, topButtonClickListener);
        } else {
            setButtonTopVisibility(View.GONE);
        }
        setButtonBottomVisibility(View.GONE);
    }

    private void updateUiDialog(int drawableId, int titleId, int contentId) {
        mImage.setImageResource(drawableId);
        mTitle.setText(titleId);
        mContent.setText(contentId);
        setButtonTopVisibility(View.GONE);
        setButtonBottomVisibility(View.GONE);
    }
}
package com.metfone.selfcare.module.keeng.fragment.category;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.adapter.category.SongRankAdapter;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.model.RankModel;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class ChildRankFragment extends RecyclerFragment<AllModel> implements BaseListener.OnLoadMoreListener {

    private int currentType = Constants.TYPE_SONG;
    private RankModel item;
    private SongRankAdapter adapter;
    private TabKeengActivity mActivity;
    private ListenerUtils listenerUtils;
    private OnLoadDataListener onLoadDataListener;

    public static ChildRankFragment newInstances(Bundle argument, OnLoadDataListener onLoadDataListener) {
        ChildRankFragment mInstance = new ChildRankFragment();
        mInstance.onLoadDataListener = onLoadDataListener;
        mInstance.setArguments(argument);
        return mInstance;
    }

    @Override
    public String getName() {
        return "ChildRankFragment";
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TabKeengActivity) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            item = (RankModel) getArguments().getSerializable(Constants.KEY_DATA);
            currentType = getArguments().getInt("type");
        }
        adapter = new SongRankAdapter(mActivity, getDatas(), TAG);
        adapter.setHasPosition(true);
        adapter.setHasListenNo(false);
        refreshLayout.setEnabled(false);
        setupRecycler(adapter);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDatas().size() == 0) {
            doLoadData(true);
        }
    }

    private void doLoadData(boolean type) {
        if (!isLoading) {
            if (type)
                loadingBegin();
            isLoading = true;
            loadData();
        }
    }

    private void loadData() {
        if (mActivity == null || item == null)
            return;
        int type = item.getType();
        if (currentType == Constants.TYPE_VIDEO && item.getType() == Constants.TYPE_SONG_BXH_VN) {
            type = Constants.TYPE_VIDEO_BXH_VN;
        } else if (currentType == Constants.TYPE_VIDEO && item.getType() == Constants.TYPE_SONG_BXH_CA) {
            type = Constants.TYPE_VIDEO_BXH_CA;
        } else if (currentType == Constants.TYPE_VIDEO && item.getType() == Constants.TYPE_SONG_BXH_AM) {
            type = Constants.TYPE_VIDEO_BXH_AM;
        }
        new KeengApi().getRankByType(currentType, type, currentPage, 100,
                result -> loadMediaComplete(result.getData()),
                error -> {
                    Log.e(TAG, error);
                    errorCount++;
                    if (errorCount < MAX_ERROR_RETRY) {
                        loadData();
                        return;
                    }
                    loadMediaComplete(null);
                });
    }

    protected void loadMediaComplete(List<AllModel> result) {
        isLoading = false;
        errorCount = 0;
        try {
            checkLoadMoreAbsolute(result);
            if (result == null) {
                if (isRefresh) {
                    isRefresh = false;
                }
                loadMored();
                adapter.setLoaded();
                loadingError(v -> doLoadData(true));
                return;
            }

            if (getDatas().isEmpty() && result.isEmpty()) {
                refreshed();
                loadMored();
                loadingEmpty();
            } else {
                refreshed();
                loadMored();
                loadingFinish();
                if (currentType == Constants.TYPE_SONG)
                    ConvertHelper.convertData(result, MediaLogModel.SRC_RANK);
                setDatas(result);
                adapter.setLoaded();
                adapter.notifyDataSetChanged();
                currentPage++;
            }
            if (onLoadDataListener != null) {
                onLoadDataListener.onLoadFinish();
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        canLoadMore = true;
        currentPage = 1;
        doLoadData(false);
    }

    @Override
    public void onLoadMore() {
//        if (!isLoading && !isRefresh && canLoadMore) {
//            loadMore();
//            doLoadData(false);
//        }
    }

    @Override
    public void onMediaClick(View v, int position) {
        if (mActivity == null || adapter == null)
            return;
        AllModel media = adapter.getItem(position);
        if (media != null) {
            media.setSource(MediaLogModel.SRC_RANK);
            switch (media.type) {
                case Constants.TYPE_ALBUM:
                case Constants.TYPE_ALBUM_VIDEO:
                    mActivity.gotoAlbumDetail(media);
                    break;
                case Constants.TYPE_SONG:
                    PlayingList playinglist = new PlayingList(getDatas(), PlayingList.TYPE_FULL_DATA, MediaLogModel.SRC_RANK);
                    playinglist.setName(media.getName());
                    mActivity.setMediaPlayingAudio(playinglist, position);
                    break;
                case Constants.TYPE_VIDEO:
                    mActivity.setMediaToPlayVideo(media);
                    break;
            }
        }
    }

    @Override
    public void onMediaExpandClick(View v, int position) {
        if (mActivity == null || adapter == null)
            return;
        AllModel item = adapter.getItem(position);
        mActivity.showPopupMore(item);
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && getDatas().size() == 0)
            onRefresh();
    }

    interface OnLoadDataListener {
        void onLoadFinish();
    }
}

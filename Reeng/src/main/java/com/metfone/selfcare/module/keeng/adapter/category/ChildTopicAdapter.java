package com.metfone.selfcare.module.keeng.adapter.category;

import android.content.Context;

import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.holder.MediaHolder;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.util.Log;

import java.util.List;

public class ChildTopicAdapter extends BaseAdapterRecyclerView {
    protected List<AllModel> datas;

    public ChildTopicAdapter(Context context, List<AllModel> datas, String ga_source) {
        super(context, ga_source);
        this.datas = datas;
    }

    @Override
    public int getItemCount() {
        if (datas != null && datas.size() > 0)
            return datas.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        AllModel item = getItem(position);
        if (item == null) {
            if (position == getItemCount() - 1)
                return ITEM_LOAD_MORE;
            else
                return ITEM_EMPTY;
        }
        switch (item.getType()) {
            case Constants.TYPE_SONG:
                return ITEM_MEDIA_SONG;
            case Constants.TYPE_ALBUM:
                return ITEM_MEDIA_ALBUM_HOME_HOT;
            case Constants.TYPE_VIDEO:
                return ITEM_MEDIA_VIDEO_HOME_HOT;
            case Constants.TYPE_PLAYLIST:
                return ITEM_MEDIA_ALBUM;
            default:
                return ITEM_MEDIA_ALBUM;
        }
    }

    @Override
    public AllModel getItem(int position) {
        if (datas != null)
            try {
                return datas.get(position);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.e(TAG, "Error:", e);
            } catch (Exception e) {
                Log.e(TAG, "Error:", e);
            }
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
//        if (holder instanceof MediaSocialHolder) {
//            AllModel item = getItem(position);
//            MediaSocialHolder itemHolder = (MediaSocialHolder) holder;
//            itemHolder.bind(mContext, item);
//        } else
         if (holder instanceof MediaHolder) {
            AllModel item = getItem(position);
            MediaHolder itemHolder = (MediaHolder) holder;
            itemHolder.bind(mContext, item); }
    }

}

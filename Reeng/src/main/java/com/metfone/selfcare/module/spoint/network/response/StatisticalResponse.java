package com.metfone.selfcare.module.spoint.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.spoint.network.model.StatisticalModel;

import java.io.Serializable;

public class StatisticalResponse implements Serializable {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("data")
    @Expose
    private StatisticalModel data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public StatisticalModel getData() {
        return data;
    }

    public void setData(StatisticalModel data) {
        this.data = data;
    }
}

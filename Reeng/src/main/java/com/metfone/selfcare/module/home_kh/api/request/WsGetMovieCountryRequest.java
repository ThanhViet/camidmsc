package com.metfone.selfcare.module.home_kh.api.request;

import com.blankj.utilcode.util.LanguageUtils;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetMovieCountryRequest extends BaseRequest<WsGetMovieCountryRequest.Request> {
    public static class Request{
        public String offset;
        public String limit;
        public String language = LanguageUtils.getCurrentLocale().getLanguage();
    }
}

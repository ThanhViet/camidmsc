package com.metfone.selfcare.module.movienew.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.movienew.listener.OnClickGenres;
import com.metfone.selfcare.module.movienew.model.HomeData;

import java.util.ArrayList;

public class ItemHolder extends BaseAdapter.ViewHolder {
    private ImageView imgAction;
    private OnClickGenres listener;
    private Activity activity;
    private ArrayList<?> mArrayList;

    public ItemHolder(View view, OnClickGenres listener, Activity activity, ArrayList<?> arrayList) {
        super(view);
        this.listener = listener;
        this.activity = activity;
        imgAction = view.findViewById(R.id.itemAction);
        this.mArrayList = arrayList;
    }


    @Override
    public void bindData(Object item, int position) {
        super.bindData(item, position);
        if (mArrayList.get(position) instanceof HomeData) {
            ArrayList<HomeData> results = (ArrayList<HomeData>) mArrayList;
            Glide.with(activity)
                    .load(results.get(position).getPosterPath()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                    .into(imgAction);
            itemView.setOnClickListener(v -> listener.onClick(results.get(position), position, true));
        }
    }

    public interface OnclickItemAction {
        void clickItemAction(Object item, int position);
    }
}

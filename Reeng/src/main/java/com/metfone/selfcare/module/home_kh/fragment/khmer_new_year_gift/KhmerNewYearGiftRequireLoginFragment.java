package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.databinding.FragmentKhmerNewYearRequireLoginBinding;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.home_kh.activity.KhmerNewYearGiftActivity;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.util.Utilities;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;

public class KhmerNewYearGiftRequireLoginFragment extends BaseFragment {
    public static final String TAG = KhmerNewYearGiftRequireLoginFragment.class.getSimpleName();

    private FragmentKhmerNewYearRequireLoginBinding binding;

    public ObservableBoolean isAlreadyLogin = new ObservableBoolean(false);

    public static KhmerNewYearGiftRequireLoginFragment newInstance() {
        KhmerNewYearGiftRequireLoginFragment fragment = new KhmerNewYearGiftRequireLoginFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentKhmerNewYearRequireLoginBinding.inflate(getLayoutInflater());
        binding.setHandler(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isAlreadyLogin.set(ApplicationController.self().isLogin() != FragmentTabHomeKh.LoginVia.NOT);

        Utilities.reCalculateDimensionAnchorView(binding.anchorPointForFrame,
                binding.container,
                binding.imageOriginalSize,
                binding.anchorPointForFrame);
    }

    @Override
    public String getName() {
        return TAG;
    }

    @Override
    public int getResIdView() {
        return 0;
    }

    public void openLoginScreen(){
        ((KhmerNewYearGiftActivity)getActivity()).playTabSound();
        SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
        NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true);
    }

    public void close(){
        ((KhmerNewYearGiftActivity)getActivity()).playTabSound();
        onBackPressed();
    }
}

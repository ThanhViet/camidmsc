package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.databinding.FragmentKhmerNewYearGiftBoxBinding;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.activity.KhmerNewYearGiftActivity;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.WsGiftBoxResponse;
import com.metfone.selfcare.module.home_kh.api.WsGiftSpinResponse;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.util.IFragmentBackPress;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import retrofit2.Response;

public class KhmerNewYearGiftBoxFragment extends BaseFragment implements GiftBoxAdapter.EventListener, GiftHistoryAdapter.EventListener, IFragmentBackPress {
    public static final String TAG = KhmerNewYearGiftBoxFragment.class.getSimpleName();

    private FragmentKhmerNewYearGiftBoxBinding binding;
    private KhHomeClient client;
    private KhmerNewYearGiftActivity activity;
    private boolean isGoHome = false;

    public ObservableField<TabMenu> tabMenu = new ObservableField<>(TabMenu.GIFT_BOX);
    public ObservableBoolean shouldShowRedeemScreen = new ObservableBoolean(false);
    public ObservableField<WsGiftBoxResponse.PrizeInfo> redeemInfo = new ObservableField<>();
    public ObservableBoolean shouldShowLoading = new ObservableBoolean(false);
    public ObservableBoolean shouldShowItemInfo = new ObservableBoolean(false);
    public ObservableField<WsGiftBoxResponse.PrizeInfo> itemInfo = new ObservableField<>();
    public ObservableField<String> phoneNumber = new ObservableField<>();

    public ObservableField<String> errorMessage = new ObservableField<>();
    public ObservableBoolean shouldShowError = new ObservableBoolean(false);

    public GiftBoxFragmentStateAdapter adapter;

    public static KhmerNewYearGiftBoxFragment newInstance() {
        KhmerNewYearGiftBoxFragment fragment = new KhmerNewYearGiftBoxFragment();
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentKhmerNewYearGiftBoxBinding.inflate(getLayoutInflater());
        binding.setHandler(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Utilities.reCalculateDimensionAnchorView(binding.anchorPointForFrame,
                binding.container,
                binding.imageOriginalSize,
                binding.anchorPointForFrame);

        activity = (KhmerNewYearGiftActivity) getActivity();
        client = new KhHomeClient();
        adapter = new GiftBoxFragmentStateAdapter(activity, this, this);
        binding.viewPager.setUserInputEnabled(false);
        binding.viewPager.setAdapter(adapter);
        ReengAccountBusiness reengAccountBusiness = ((ApplicationController)(getActivity().getApplicationContext())).getReengAccountBusiness();
//        if(reengAccountBusiness != null){
//            phoneNumber.set(reengAccountBusiness.getJidNumber().replace("+",""));
//        }

        String phone = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class).replace("+","");
        if(phone.length()>=3){
            if (phone.charAt(0) == 0) {
                phone = phone.replaceFirst("0", "");
            }
            if (!phone.substring(0, 3).equals("855")) {
                phone = phone.replaceFirst("", "855");
            }
            phoneNumber.set(phone);
        }

        loadGiftBoxList();
    }

    @Override
    public void onRedeemClicked(WsGiftBoxResponse.PrizeInfo info) {
        activity.playTabSound();
        showRedeemScreen(info);
    }

    @Override
    public void onShowQrClicked(WsGiftBoxResponse.PrizeInfo info) {
        activity.playTabSound();
        WsGiftSpinResponse.Prize prize = new WsGiftSpinResponse().new Prize();
        prize.setQrCode(info.getQrCode());
        prize.setPrizeName(info.getPrizeName());
        prize.setDescription(info.getDescription());
        addFragment(R.id.root_frame, GiftRedeemSucceedFragment.newInstance(prize), GiftRedeemSucceedFragment.TAG);
    }

    private void loadGiftBoxList(){
        shouldShowLoading.set(true);
        client.wsGetGiftBoxList(new MPApiCallback<WsGiftBoxResponse>() {
            @Override
            public void onResponse(Response<WsGiftBoxResponse> response) {
                shouldShowLoading.set(false);
                if(response.body() != null && response.body().getResult() != null && response.body().getResult().getPrizeInfoList() != null){
                    adapter.updateGiftBox(new ArrayList<>(response.body().getResult().getPrizeInfoList()));
                }
            }

            @Override
            public void onError(Throwable error) {
                shouldShowLoading.set(false);
            }
        });
    }

    private void loadRedeemHistoryList(){
        shouldShowLoading.set(true);
        client.wsGetRedeemHistoryList(new MPApiCallback<WsGiftBoxResponse>() {
            @Override
            public void onResponse(Response<WsGiftBoxResponse> response) {
                shouldShowLoading.set(false);
                if(response.body() != null && response.body().getResult() != null && response.body().getResult().getPrizeInfoList() != null){
                    adapter.updateHistory(new ArrayList<>(response.body().getResult().getPrizeInfoList()));
                }
            }

            @Override
            public void onError(Throwable error) {
                shouldShowLoading.set(false);
            }
        });
    }

    public void onTabChange(TabMenu tabMenu){
        activity.playTabSound();
        if(this.tabMenu.get() != tabMenu){
            this.tabMenu.set(tabMenu);
            switch (tabMenu){
                case HISTORY:
                    binding.viewPager.setCurrentItem(1);
                    loadRedeemHistoryList();
                    break;
                case GIFT_BOX:binding.viewPager.setCurrentItem(0);
                    loadGiftBoxList();
                    break;
            }
        }
    }


    public void hideRedeemScreen(){
        shouldShowRedeemScreen.set(false);
    }

    public void hideItemInfo(){
        shouldShowItemInfo.set(false);
    }

    public void showRedeemScreen(WsGiftBoxResponse.PrizeInfo info){
        shouldShowRedeemScreen.set(true);
        Glide.with(binding.imgPrize).load(info.getImage()).into(binding.imgPrize);
        redeemInfo.set(info);
    }

    public void redeem(){
        activity.playTabSound();
        shouldShowLoading.set(true);
        client.wsGiftRedeem(new MPApiCallback<WsGiftSpinResponse>() {
            @Override
            public void onResponse(Response<WsGiftSpinResponse> response) {
                shouldShowLoading.set(false);
                shouldShowRedeemScreen.set(false);
                if(response.body() != null && response.body().getResult() != null){
                    if(response.body().getResult().getStatus() == 0){
                        WsGiftSpinResponse.Prize prize = response.body().getResult().getPrize();
                        if(prize != null){
                            prize.setDescription(response.body().getResult().getMessage());
                            if(prize.getPrizeType().equalsIgnoreCase("special")){
                                prize.setPrizeName(prize.getDescription());
                                addFragment(R.id.root_frame, GiftRedeemSucceedFragment.newInstance(prize), GiftRedeemSucceedFragment.TAG);
                            }
                            else {
                                WsGiftBoxResponse.PrizeInfo info = new WsGiftBoxResponse.PrizeInfo();
                                info.setObjectData1(getString(R.string.redeemed_successfully));
                                info.setDescription(prize.getDescription());
                                showItemInfo(info);
                            }

                            loadGiftBoxList();
                        }
                    }
                    else {
                        showGiftError(response.body().getResult().getMessage(), response.body().getResult().getStatus() == -1);
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                shouldShowLoading.set(false);
            }
        }, redeemInfo.get().getGiftRewardID());
    }

    @Override
    public void onGiftItemHistoryClicked(WsGiftBoxResponse.PrizeInfo info) {
        showItemInfo(info);
    }

    public void showItemInfo(WsGiftBoxResponse.PrizeInfo info){
        activity.playMessageSound();
        itemInfo.set(info);
        shouldShowItemInfo.set(true);
    }

    public void forceBack(){
        activity.playTabSound();
        if(isGoHome){
            activity.finish();
        }
        else {
            shouldShowError.set(false);
        }
    }

    private void showGiftError(String message, boolean isGoHome){
        activity.playMessageSound();
        if(!isGoHome){
            this.isGoHome = false;
            binding.errorButton.setText(R.string.close);
        }
        else {
            this.isGoHome = true;
            binding.errorButton.setText(R.string.go_home);
        }
        shouldShowError.set(true);
        errorMessage.set(message);
    }

    @Override
    public String getName() {
        return KhmerNewYearGiftBoxFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return 0;
    }

    @Override
    public boolean onFragmentBackPressed() {
        if(shouldShowRedeemScreen.get() || shouldShowItemInfo.get() || shouldShowError.get()){
            shouldShowError.set(false);
            hideRedeemScreen();
            hideItemInfo();
            return false;
        }
        return true;
    }

    public void back(){
        activity.playTabSound();
        onBackPressed();
    }

    public enum TabMenu{
        GIFT_BOX,
        HISTORY
    }

    public void exit(){
        activity.finish();
    }

}

//package com.metfone.selfcare.module.selfcare.network;
//
//import android.content.Context;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//
//import com.metfone.selfcare.app.dev.ApplicationController;
//
//import java.io.IOException;
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.security.SecureRandom;
//import java.security.cert.CertificateException;
//import java.security.cert.X509Certificate;
//import java.util.concurrent.TimeUnit;
//
//import javax.net.ssl.HostnameVerifier;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.SSLSession;
//import javax.net.ssl.SSLSocketFactory;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;
//
//import okhttp3.Interceptor;
//import okhttp3.OkHttpClient;
//
//public class HttpsTrustManager implements X509TrustManager {
//
//    private static final X509Certificate[] _AcceptedIssuers = new X509Certificate[]{};
//    private static TrustManager[] trustManagers;
//
//    public static void allowAllSSL() {
//        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
//
//            @Override
//            public boolean verify(String url, SSLSession arg1) {
//                if(url.contains("https"))
//                    return true;
//                else
//                    return false;
//            }
//
//        });
//
//        SSLContext context = null;
//        if (trustManagers == null) {
//            trustManagers = new TrustManager[]{new HttpsTrustManager()};
//        }
//
//        try {
//            context = SSLContext.getInstance("TLS");
//            context.init(null, trustManagers, new SecureRandom());
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        }
//
//        HttpsURLConnection.setDefaultSSLSocketFactory(context
//                .getSocketFactory());
//    }
//
//    @Override
//    public void checkClientTrusted(
//            X509Certificate[] x509Certificates, String s)
//            throws CertificateException {
//
//    }
//
//    @Override
//    public void checkServerTrusted(
//            X509Certificate[] x509Certificates, String s)
//            throws CertificateException {
//
//    }
//
//    public boolean isClientTrusted(X509Certificate[] chain) {
//        return true;
//    }
//
//    public boolean isServerTrusted(X509Certificate[] chain) {
//        return true;
//    }
//
//    @Override
//    public X509Certificate[] getAcceptedIssuers() {
//        return _AcceptedIssuers;
//    }
//
//
//    public static OkHttpClient getUnsafeOkHttpClient(long timeOut) {
//        try {
//            // Create a trust manager that does not validate certificate chains
//            final TrustManager[] trustAllCerts = new TrustManager[]{
//                    new X509TrustManager() {
//                        @Override
//                        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//                        }
//
//                        @Override
//                        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//                        }
//
//                        @Override
//                        public X509Certificate[] getAcceptedIssuers() {
//                            return new X509Certificate[]{};
//                        }
//                    }
//            };
//
//            // Install the all-trusting trust manager
//            final SSLContext sslContext = SSLContext.getInstance("SSL");
//            sslContext.init(null, trustAllCerts, new SecureRandom());
//
//            // Create an ssl socket factory with our all-trusting manager
//            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
//
//            OkHttpClient.Builder builder = new OkHttpClient.Builder();
//            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
//            builder.hostnameVerifier(new HostnameVerifier() {
//                @Override
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            });
//            builder.addInterceptor(provideInterceptorCache());
//
//            OkHttpClient okHttpClient = builder
//                    .connectTimeout(timeOut, TimeUnit.SECONDS)
//                    .readTimeout(timeOut, TimeUnit.SECONDS)
//                    .writeTimeout(timeOut, TimeUnit.SECONDS)
//                    .build();
//            return okHttpClient;
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    private static final int TIME_CACHE_DEFAULT = 60;
//    private static Interceptor provideInterceptorCache() {
//        return new Interceptor() {
//            @Override
//            public okhttp3.Response intercept(Chain chain) throws IOException {
//                okhttp3.Response originalResponse = chain.proceed(chain.request());
//                if (isNetworkAvailable(ApplicationController.self())) {
//                    int maxAge = TIME_CACHE_DEFAULT;// lưu request trong vòng 1 phut
//                    return originalResponse.newBuilder()
//                            .header("Cache-Control", "public, max-age=" + maxAge)
//                            .build();
//                } else {
//                    int maxStale = 60 * 60 * 24 * 28; // lưu request trong vòng 1 tuần
//                    return originalResponse.newBuilder()
//                            .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
//                            .build();
//                }
//            }
//        };
//    }
//
//    static boolean isNetworkAvailable(Context context) {
//        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        if (connectivityManager == null) return false;
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }
//
//}

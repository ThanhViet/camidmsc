package com.metfone.selfcare.module.home_kh.fragment.reward;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.PartnerGiftRedeemHistoryResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.history.adapter.ScrollLoadMoreHandler;
import com.metfone.selfcare.module.home_kh.fragment.reward.adapter.RewardPagerRvAdapter;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.RewardsShopKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.setting.BaseSettingKhFragment;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.model.RedeemItem;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import retrofit2.Call;

public class GiftActiveKHFragment extends BaseSettingKhFragment implements RewardPagerRvAdapter.IActiveClick {

    private static final String TAG = GiftActiveKHFragment.class.getSimpleName();
    List<RedeemItem> listData = new ArrayList<>();
    Map<String, RedeemItem> listPast = new HashMap<>();
    @NonNull
    @BindView(R.id.rv)
    RecyclerView rv;
    @NonNull
    @BindView(R.id.none)
    LinearLayout lnNone;
    AppCompatTextView tvShopforReward;
    private KhHomeClient homeKhClient;
    private int currentPage = 1;
    private RewardPagerRvAdapter rewardPagerRvAdapter;

    public static GiftActiveKHFragment newInstance() {
        GiftActiveKHFragment fragment = new GiftActiveKHFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView(View view) {
        tvShopforReward = view.findViewById(R.id.tv_shop_for_reward);
        tvShopforReward.setOnClickListener(v -> {
            addFragment(R.id.reward_container, RewardsShopKHFragment.newInstance(null,false), RewardsShopKHFragment.TAG);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findComponentViews(view);
        loadGiftReceived();
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_reward_redeem_kh;
    }

    private void findComponentViews(final View view) {
        if (view == null) return;
        Activity activity = getActivity();
        if (activity != null) {
            rewardPagerRvAdapter = new RewardPagerRvAdapter(listData, this);
            rewardPagerRvAdapter.setType(PointType.ACTIVE.id);
            LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(activity,
                LinearLayoutManager.VERTICAL, false);
            DividerItemDecoration divider = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
            Drawable mDivider = ContextCompat.getDrawable(activity, R.drawable.reward_divider);
            divider.setDrawable(mDivider);
            rv.setLayoutManager(verticalLayoutManager);
            rv.addItemDecoration(divider);
            rv.setAdapter(rewardPagerRvAdapter);
            final ScrollLoadMoreHandler handler = new ScrollLoadMoreHandler(verticalLayoutManager, (page, totalItemsCount, recyclerView) -> {
                if (listData.size() >= currentPage * KhHomeClient.TOTAL_ITEM_EACH_PAGE)
                    loadGiftActive(true);
            });
            rv.addOnScrollListener(handler);
        }
    }

    /**
     * get list gift active
     *
     * @param loadMore
     */
    private void loadGiftActive(boolean loadMore) {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        rewardPagerRvAdapter.setLoading(true);
        if (loadMore) {
            currentPage++;
        } else {
            rewardPagerRvAdapter.removeData();
        }
        homeKhClient.wsGetPartnerGiftRedeemHistory(new KhApiCallback<KHBaseResponse<PartnerGiftRedeemHistoryResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<PartnerGiftRedeemHistoryResponse> body) {
                logH("wsGetPartnerGiftRedeemHistory > onSuccess");
                rewardPagerRvAdapter.setLoading(false);
                List<RedeemItem> data = body.getData().getWsResponse().getObject();
                if (data.size() > 0) {
                    removeExpired(data);
                }
                lnNone.setVisibility(listData.size() == 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsGetPartnerGiftRedeemHistory > " + status + " " + message);
                rewardPagerRvAdapter.setLoading(false);
                currentPage--;
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);

            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<PartnerGiftRedeemHistoryResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetPartnerGiftRedeemHistory > onFailure");
                rewardPagerRvAdapter.setLoading(false);
                currentPage--;
            }
        }, currentPage);

    }

    /**
     * remove item has expired date small than now time and have gifId in list past
     *
     * @param data list gift active
     */
    private void removeExpired(List<RedeemItem> data) {
        List<RedeemItem> expired = new ArrayList<>();
        long now = Calendar.getInstance().getTimeInMillis();
        Iterator<RedeemItem> i = data.iterator();
        while (i.hasNext()) {
            RedeemItem item = i.next();
            if (listPast.containsKey(item.getGiftId())) {
                i.remove();
            } else if (item.getExpireDateLong() <= now) {
                listPast.put(item.getGiftId(), item);
                expired.add(item);
                i.remove();
            }
        }
        rewardPagerRvAdapter.add(data);
        if (expired.size() > 0) {
            addExpired(expired);
        }
    }

    /**
     * get list gift past
     */
    private void loadGiftReceived() {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetListGiftReceived(new KhApiCallback<KHBaseResponse<PartnerGiftRedeemHistoryResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<PartnerGiftRedeemHistoryResponse> body) {
                logH("wsGetListGiftReceived > onSuccess");
                List<RedeemItem> data = body.getData().getWsResponse().getObject();
                if (data != null && data.size() > 0) {
                    for (RedeemItem item : data) {
                        listPast.put(item.getGiftId(), item);
                    }
                    addExpired(data);
                }
                loadGiftActive(false);
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsGetListGiftReceived > " + status + " " + message);
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<PartnerGiftRedeemHistoryResponse>>> call, Throwable t) {
                logH("wsGetListGiftReceived > onFailure");
                t.printStackTrace();
            }
        });

    }

    private void logH(String m) {
        Log.e("H-Reward", m);
    }

    private void addExpired(List<RedeemItem> data) {
        RewardCamIdKHFragment parentFrag = ((RewardCamIdKHFragment) GiftActiveKHFragment.this.getParentFragment());
        if (parentFrag != null)
            parentFrag.addExpired(data);
    }

    @Override
    public void clickItem(RedeemItem item) {
        RewardCamIdKHFragment parentFragment = (RewardCamIdKHFragment) getParentFragment();
        if (parentFragment != null) {
            parentFragment.showDetailActive(item);
        }
    }
}
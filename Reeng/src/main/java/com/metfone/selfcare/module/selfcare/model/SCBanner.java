package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCBanner implements Serializable {
//      "id": "8a49840c68700ef20168e5f4d9140007",
//              "name": "Adaptive Video Feature- “HLOOT THONE”!",
//              "iconUrl": "https://imagemytel.viettelglobal.net/1550045613707.jpeg",
//              "publishDate": 1549992600000

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("imgUrl")
    private String iconUrl;

    @SerializedName("deepLink")
    private String deepLink;

    @SerializedName("href")
    private String href;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public SCBanner(String id, String name, String iconUrl, String deepLink) {
        this.id = id;
        this.name = name;
        this.iconUrl = iconUrl;
        this.deepLink = deepLink;
    }

    @Override
    public String toString() {
        return "SCBanner{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", deepLink='" + deepLink + '\'' +
                '}';
    }
}

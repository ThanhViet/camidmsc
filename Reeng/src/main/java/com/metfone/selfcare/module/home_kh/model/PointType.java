package com.metfone.selfcare.module.home_kh.model;

import android.graphics.drawable.Drawable;

import androidx.annotation.DrawableRes;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

public enum PointType {
    ACTIVE(1, R.drawable.ic_reward_active),
    PAST(2, R.drawable.ic_reward_past);
    public final int id;
    public final Drawable drawable;

    PointType(int id, @DrawableRes int color) {
        this.id = id;
        this.drawable = ResourceUtils.getDrawable(color);
    }
}
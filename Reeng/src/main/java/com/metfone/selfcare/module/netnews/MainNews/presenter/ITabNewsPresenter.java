package com.metfone.selfcare.module.netnews.MainNews.presenter;


import android.content.Context;

import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.response.CategoryResponse;

/**
 * Created by HaiKE on 8/18/17.
 */

public interface ITabNewsPresenter extends MvpPresenter {

    void loadCategory(Context context);
    void loadSettingCategory(SharedPref pref);
    void saveCategoryToDB(SharedPref pref, CategoryResponse categoryResponse);
}
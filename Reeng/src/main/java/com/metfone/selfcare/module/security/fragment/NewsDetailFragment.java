/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/27
 */

package com.metfone.selfcare.module.security.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.security.activity.SecurityActivity;
import com.metfone.selfcare.module.security.model.NewsModel;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NewsDetailFragment extends BaseFragment {
    @BindView(R.id.web_view)
    WebView webView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private Unbinder unbinder;
    private NewsModel data;

    public static NewsDetailFragment newInstance() {
        Bundle args = new Bundle();
        NewsDetailFragment fragment = new NewsDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "NewsDetailFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_security_news;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadData();
            }
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

    }

    private void loadData() {
        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                Serializable serializable = bundle.getSerializable(Constants.KEY_DATA);
                if (serializable instanceof NewsModel) {
                    data = (NewsModel) serializable;
                }
            }
            if (data != null) {
                if (mActivity instanceof SecurityActivity) {
                    ((SecurityActivity) mActivity).setTitle(data.getTitle());
                }
                WebSettings webSettings = webView.getSettings();
                webSettings.setDefaultTextEncodingName("utf-8");
                webView.loadData(data.getContent(), "text/html; charset=utf-8", "utf-8");
                webView.setWebChromeClient(new MyWebChromeClient());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MyWebChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (newProgress >= 90) {
                progressBar.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.VISIBLE);
            }
        }

    }
}

package com.metfone.selfcare.module.tab_home.holder;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.HomeContact;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class QuickContactDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_view_all)
    TextView tvViewAll;
    @BindView(R.id.layout_thread_avatar)
    View viewAvatar;
    TabHomeListener.OnAdapterClick listener;
    HomeContact data;
    private ImageView ivThreadAvatar;
    private View viewAvatarGroup;
    private TextView tvAvatar;
    private TextView tvUnreadMsg;

    public QuickContactDetailHolder(View view, TabHomeListener.OnAdapterClick listener) {
        super(view);
        this.listener = listener;
        ivThreadAvatar = view.findViewById(R.id.thread_avatar);
        viewAvatarGroup = view.findViewById(R.id.rlAvatarGroup);
        tvAvatar = view.findViewById(R.id.contact_avatar_text);
        tvUnreadMsg = view.findViewById(R.id.thread_number_unread);

        int width = TabHomeUtils.getWidthQuickContact();
        ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
        layoutParams.width = width;
        viewRoot.setLayoutParams(layoutParams);
        viewRoot.requestLayout();
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof HomeContact) {
            data = (HomeContact) item;

            if (data.isViewAll()) {
                if (tvViewAll != null) tvViewAll.setVisibility(View.VISIBLE);
                if (tvTitle != null) tvTitle.setVisibility(View.GONE);
                if (ivCover != null) ivCover.setVisibility(View.VISIBLE);
                ImageBusiness.setResource(ivCover, R.drawable.ic_view_all_contact);
                if (viewAvatar != null) viewAvatar.setVisibility(View.GONE);
            } else {
                if (tvViewAll != null) tvViewAll.setVisibility(View.GONE);
                if (tvTitle != null) tvTitle.setVisibility(View.VISIBLE);
                if (ivCover != null) ivCover.setVisibility(View.GONE);
                if (viewAvatar != null) viewAvatar.setVisibility(View.VISIBLE);
                setPhoneNumberView(data.getThreadMessage());
                setMessageView(data.getThreadMessage());
            }
        }
    }

    private void setPhoneNumberView(ThreadMessage threadMessage) {
        int threadType = threadMessage.getThreadType();
        ApplicationController mApplication = ApplicationController.self();
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        tvTitle.setText(mApplication.getMessageBusiness().getThreadName(threadMessage));
        ivThreadAvatar.setVisibility(View.VISIBLE);
        tvAvatar.setVisibility(View.GONE);
        int sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String numberFriend = threadMessage.getSoloNumber();
            PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(numberFriend);
            StrangerPhoneNumber stranger = threadMessage.getStrangerPhoneNumber();
            if (phoneNumber != null) {
                avatarBusiness.setPhoneNumberAvatar(ivThreadAvatar, tvAvatar, phoneNumber, sizeAvatar);
            } else {
                if (threadMessage.isStranger()) {
                    if (stranger != null) {
                        avatarBusiness.setStrangerAvatar(ivThreadAvatar, tvAvatar,
                                stranger, stranger.getPhoneNumber(), null, null, sizeAvatar);
                    } else {
                        avatarBusiness.setUnknownNumberAvatar(ivThreadAvatar, tvAvatar, numberFriend, sizeAvatar);
                    }
                } else {
                    avatarBusiness.setUnknownNumberAvatar(ivThreadAvatar, tvAvatar, numberFriend, sizeAvatar);
                }
            }
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
            avatarBusiness.setGroupThreadAvatar(ivThreadAvatar, viewAvatarGroup, threadMessage);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {//chat room
            avatarBusiness.setOfficialThreadAvatar(ivThreadAvatar, sizeAvatar,
                    threadMessage.getServerId(), null, false);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            ivThreadAvatar.setImageResource(R.drawable.ic_broadcast_group);
        } else {
            OfficerAccount official = mApplication.getOfficerBusiness().getOfficerAccountByServerId(threadMessage.getServerId());
            avatarBusiness.setOfficialThreadAvatar(ivThreadAvatar, sizeAvatar,
                    threadMessage.getServerId(), official, false);
        }
    }

    private void setMessageView(ThreadMessage threadMessage) {
        CopyOnWriteArrayList<ReengMessage> allMessages = threadMessage.getAllMessages();
        tvUnreadMsg.setVisibility(View.GONE);
        if (Utilities.notEmpty(allMessages)) {
            int numOfUnreadMessage = threadMessage.getNumOfUnreadMessage();
            if (numOfUnreadMessage > 0) {
                tvTitle.setTypeface(null, Typeface.BOLD);
                tvUnreadMsg.setVisibility(View.VISIBLE);
                if ((threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT)) {
                    tvUnreadMsg.setText("N");
                } else {
                    if (numOfUnreadMessage <= 9) {
                        tvUnreadMsg.setText(String.valueOf(numOfUnreadMessage));
                    } else {
                        tvUnreadMsg.setText("9+");
                    }
                }
            } else {
                tvTitle.setTypeface(null, Typeface.NORMAL);
            }
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener != null && data != null) {
            if (data.isViewAll()) {
                listener.onClickTitleBoxContact();
            } else if (data.getThreadMessage() != null)
                listener.onClickContact(data.getThreadMessage());
        }
    }
}

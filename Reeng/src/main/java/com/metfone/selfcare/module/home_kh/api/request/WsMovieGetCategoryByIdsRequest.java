package com.metfone.selfcare.module.home_kh.api.request;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsMovieGetCategoryByIdsRequest extends BaseRequest<WsMovieGetCategoryByIdsRequest.Request> {
    public static class Request {
        private String platform;
        private String language;
        private String revision;
        private String clientType;
        private String msisdn;
        private String ids;

        public String getPlatform() { return platform; }
        public void setPlatform(String value) { this.platform = value; }

        public String getLanguage() { return language; }
        public void setLanguage(String value) { this.language = value; }

        public String getRevision() { return revision; }
        public void setRevision(String value) { this.revision = value; }

        public String getClientType() { return clientType; }
        public void setClientType(String value) { this.clientType = value; }

        public String getMsisdn() { return msisdn; }
        public void setMsisdn(String value) { this.msisdn = value; }

        public String getIDS() { return ids; }
        public void setIDS(String value) { this.ids = value; }
    }

}

package com.metfone.selfcare.module.home_kh.fragment.reward;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.fragment.reward.adapter.RewardPagerRvAdapter;
import com.metfone.selfcare.module.home_kh.fragment.setting.BaseSettingKhFragment;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.model.RedeemItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class GiftPastKHFragment extends BaseSettingKhFragment {
    public static GiftPastKHFragment newInstance() {
        GiftPastKHFragment fragment = new GiftPastKHFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private static final String TAG = GiftPastKHFragment.class.getSimpleName();
    private RewardPagerRvAdapter rewardPagerRvAdapter;
    List<RedeemItem> list = new ArrayList<>();
    @NonNull
    @BindView(R.id.rv)
    RecyclerView rv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    protected void initView(View view) {
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findComponentViews(view);
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_reward_received_kh;
    }

    private void findComponentViews(final View view) {
        if (view == null) return;
        initRv(list);
    }

    public void addExpired(List<RedeemItem> list) {
       if(rewardPagerRvAdapter!=null){
           rewardPagerRvAdapter.add(list);
       }
    }

    private void initRv(List<RedeemItem> list) {
        Activity activity = getActivity();
        if (activity != null) {
            rewardPagerRvAdapter = new RewardPagerRvAdapter(list);
            rewardPagerRvAdapter.setType(PointType.PAST.id);
            LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(activity,
                    LinearLayoutManager.VERTICAL, false);
            DividerItemDecoration divider = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
            Drawable mDivider = ContextCompat.getDrawable(activity, R.drawable.reward_divider);
            divider.setDrawable(mDivider);
            rv.setLayoutManager(verticalLayoutManager);
            rv.addItemDecoration(divider);
            rv.setAdapter(rewardPagerRvAdapter);
        }
    }
}
package com.metfone.selfcare.module.selfcare.widget;

import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomDialog;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

public class SCBottomSheetCSKHDialog implements View.OnClickListener {

    BaseSlidingFragmentActivity activity;
    BottomDialog bottomSheetDialog;

    private View btnChat;
    private View btnCall;
    private View btnCallVideo;

    private String accountId;
    private String token;

    public SCBottomSheetCSKHDialog(@NonNull BaseSlidingFragmentActivity activity) {
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_sc_bottom_sheet_cskh, null);
        bottomSheetDialog = new BottomDialog(activity);
        bottomSheetDialog.setContentView(view);
        this.activity = activity;
        init(view);
    }

    protected void init(View view) {
        btnChat = view.findViewById(R.id.btnChat);
        btnCall = view.findViewById(R.id.btnCall);
        btnCallVideo = view.findViewById(R.id.btnCallVideo);

        btnChat.setOnClickListener(this);
        btnCall.setOnClickListener(this);
        btnCallVideo.setOnClickListener(this);
    }

    public void dismiss() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.dismiss();
            bottomSheetDialog = null;
        }
        btnCall = null;
        btnChat = null;
        btnCallVideo = null;
    }

    public void show() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCall:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + SCConstants.SC_CALL_CENTER));
                activity.startActivity(intent);

                dismiss();
                break;
            case R.id.btnChat:
                String ref = "support";
                String name = "mocha";
                String avatar = "";
                ThreadMessage threadMessage = ApplicationController.self().getMessageBusiness().findExistingOrCreateOfficerThread(ref,
                        name, avatar, OfficerAccountConstant.ONMEDIA_TYPE_NONE);
                NavigateActivityHelper.navigateToChatDetail(activity, threadMessage);

                dismiss();
                break;
        }
    }
}
package com.metfone.selfcare.module.metfoneplus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.activity.AddFeedbackActivity;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.util.FragmentUtils;
import com.metfone.selfcare.util.Utilities;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.OnClick;

public class MPFeedbackSwitchFragment extends MPBaseFragment {

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.btnFeedbackMobileService)
    RadioButton btnFeedbackMobileService;
    @BindView(R.id.btnFeedbackInternetWifi)
    RadioButton btnFeedbackInternetWifi;
    @BindView(R.id.frameLayoutSwitch)
    FrameLayout frameLayoutSwitch;

    private AddFeedbackActivity activity;

    public MPFeedbackSwitchFragment() {
        // Required empty public constructor
    }

    public static MPFeedbackSwitchFragment newInstance() {
        MPFeedbackSwitchFragment fragment = new MPFeedbackSwitchFragment();
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (AddFeedbackActivity) context;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_feedback_switch;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity = (AddFeedbackActivity) getActivity();
        Utilities.adaptViewForInserts(mLayoutActionBar);
        mActionBarTitle.setText(getString(R.string.m_p_add_feedback_us_title));
        gotoMPFeedbackMobile();
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        activity.finish();
    }

    @OnClick(R.id.btnFeedbackInternetWifi)
    public void gotoInternet() {
        FragmentUtils.replaceFragmentWithAnimationDefault(getChildFragmentManager(), R.id.frameLayoutSwitch, MPFeedbackInternetFragment.newInstance());
    }

    @OnClick(R.id.btnFeedbackMobileService)
    void gotoMobile() {
        gotoMPFeedbackMobile();
    }

}
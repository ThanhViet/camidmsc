package com.metfone.selfcare.module.metfoneplus.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.module.metfoneplus.activity.AddFeedbackActivity;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsUpdateComplaintResponse;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;


public class MPFeedbackUpdateFragment extends MPBaseFragment {
    public static final String TAG = MPFeedbackUsFragment.class.getSimpleName();
    public static final String COMPLAIN_ID = "complainId";
    public static final String ERROR_PHONE = "errorPhone";
    public static final String COMPLAINER_PHONE = "complainerPhone";
    public static final String ACCEPT_DATE = "acceptDate";
    public static final String PRO_LIMIT_DATE = "proLimitDate";
    public static final String STAFF_INFO = "staffInfo";
    public static final String SERVICE_TYPE_NAME = "serviceTypeName";
    public static final String GROUP_TYPE_NAME = "groupTypeName";
    public static final String STAFF_PHONE = "staffPhone";
    public static final String COMP_CONTENT = "compContent";
    public static String ADD_UPDATE = "add_update";
    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_option)
    RelativeLayout mLayoutActionBarOption;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.action_bar_option_more)
    AppCompatImageView ivActionBarOption;
    @BindView(R.id.errorPhone)
    TextView tvErrorPhone;
    @BindView(R.id.complainerPhone)
    TextView tvComplainerPhone;
    @BindView(R.id.acceptDate)
    TextView tvAcceptDate;
    @BindView(R.id.proLimitDate)
    TextView tvProLimitDate;
    @BindView(R.id.staffInfo)
    TextView tvStaffInfo;
    @BindView(R.id.serviceTypeName)
    TextView tvServiceTypeName;
    @BindView(R.id.groupTypeName)
    TextView tvGroupTypeName;
    @BindView(R.id.staffPhone)
    TextView tvStaffPhone;
    @BindView(R.id.compContent)
    EditText edtCompContent;
    @BindView(R.id.btnCompUpdate)
    TextView btnCompUpdate;

    String complainId, errorPhone, complainerPhone, acceptDate, proLimitDate, staffInfo, serviceTypeName, groupTypeName, staffPhone, compContent;

    public MPFeedbackUpdateFragment() {
        // Required empty public constructor
    }

    public static MPFeedbackUpdateFragment newInstance(
            String complainId,
            String errorPhone,
            String complainerPhone,
            String acceptDate,
            String proLimitDate,
            String staffInfo,
            String serviceTypeName,
            String groupTypeName,
            String staffPhone,
            String compContent
    ) {
        MPFeedbackUpdateFragment fragment = new MPFeedbackUpdateFragment();
        Bundle args = new Bundle();
        args.putString(COMPLAIN_ID, complainId);
        args.putString(ERROR_PHONE, errorPhone);
        args.putString(COMPLAINER_PHONE, complainerPhone);
        args.putString(ACCEPT_DATE, acceptDate);
        args.putString(PRO_LIMIT_DATE, proLimitDate);
        args.putString(STAFF_INFO, staffInfo);
        args.putString(SERVICE_TYPE_NAME, serviceTypeName);
        args.putString(GROUP_TYPE_NAME, groupTypeName);
        args.putString(STAFF_PHONE, staffPhone);
        args.putString(COMP_CONTENT, compContent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_feedback_update;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mLayoutActionBar);
        mActionBarTitle.setText(getString(R.string.m_p_feedback_us_title));
        mLayoutActionBarOption.setVisibility(View.VISIBLE);
        ivActionBarOption.setBackgroundResource(R.drawable.ic_add);

        if (getArguments() != null) {
            complainId = getArguments().getString(COMPLAIN_ID);
            errorPhone = getArguments().getString(ERROR_PHONE);
            complainerPhone = getArguments().getString(COMPLAINER_PHONE);
            acceptDate = getArguments().getString(ACCEPT_DATE);
            proLimitDate = getArguments().getString(PRO_LIMIT_DATE);
            staffInfo = getArguments().getString(STAFF_INFO);
            serviceTypeName = getArguments().getString(SERVICE_TYPE_NAME);
            groupTypeName = getArguments().getString(GROUP_TYPE_NAME);
            staffPhone = getArguments().getString(STAFF_PHONE);
            compContent = getArguments().getString(COMP_CONTENT);

            tvErrorPhone.setText(errorPhone);
            tvComplainerPhone.setText(complainerPhone);
            tvAcceptDate.setText(acceptDate);
            tvProLimitDate.setText(proLimitDate);
            tvStaffInfo.setText(staffInfo);
            tvServiceTypeName.setText(serviceTypeName);
            tvGroupTypeName.setText(groupTypeName);
            tvStaffPhone.setText(staffPhone);
            tvStaffPhone.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        } else {
            Toast.makeText(getContext(), "Something error! Please go back to the previous step", Toast.LENGTH_SHORT).show();
        }

    }


    @OnClick(R.id.action_bar_back)
    void onBack() {
        popBackStackFragment();
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.action_bar_option)
    void addFeedback() {
        Intent intent = new Intent(getActivity(), AddFeedbackActivity.class);
        getActivity().startActivity(intent);
        popBackStackFragment();
    }

    @OnClick(R.id.staffPhone)
    void callSupport() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + staffPhone));
        mParentActivity.startActivity(callIntent);
    }

    @OnClick(R.id.btnCompUpdate)
    void updateComplaint() {
        mParentActivity.showLoadingDialog("", "");
        String mComplaintContent = edtCompContent.getText().toString().trim();
        if (TextUtils.isEmpty(mComplaintContent)){
            ToastUtils.showToast(getContext(), getString(R.string.error_feedback_input_invalid));
            return;
        }
        try {
            new MetfonePlusClient().wsUpdateComplaint(complainId, mComplaintContent, new MPApiCallback<WsUpdateComplaintResponse>() {
                @Override
                public void onResponse(Response<WsUpdateComplaintResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getResult().getErrorCode().equals("0")) {
                            if (mParentActivity instanceof DeepLinkActivity) {
                                mParentActivity.finish();
                                return;
                            }
                            setValueAdd(true);
                            popBackStackFragment();
                        } else {
                            assert response.body() != null;
                            handleShowErrorDialog(response.body().getResult().getMessage());
                        }
                    }
                    mParentActivity.hideLoadingDialog();
                }

                @Override
                public void onError(Throwable error) {
                    mParentActivity.hideLoadingDialog();
                    Log.e(TAG, "onError: ", error);
                }
            });
        } catch (Exception e) {
            mParentActivity.hideLoadingDialog();
            com.metfone.selfcare.util.Log.e(TAG, "Error: ", e);
        }
    }


    private void handleShowErrorDialog(String message) {
        MPDialogFragment errorDialog = new MPDialogFragment.Builder()
                .setTitle(R.string.m_p_dialog_error_title)
                .setContent(message)
                .setCancelableOnTouchOutside(true)
                .build();
        errorDialog.show(getChildFragmentManager(), null);
        errorDialog.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onDialogDismiss() {
                popBackStackFragment();
            }
        });
    }

    public void setValueAdd(Boolean value) {
        SharedPreferences sharedPref = getContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(ADD_UPDATE, value);
        editor.commit();
    }
}

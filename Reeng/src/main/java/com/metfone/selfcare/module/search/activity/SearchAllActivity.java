package com.metfone.selfcare.module.search.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.BackStackHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.common.CommonSearch;
import com.metfone.selfcare.module.movienew.common.Constant;
import com.metfone.selfcare.module.movienew.fragment.ActionCategoryFragment;
import com.metfone.selfcare.module.movienew.holder.HistorySearchViewHolder;
import com.metfone.selfcare.module.movienew.holder.ResultSearchViewHolder;
import com.metfone.selfcare.module.movienew.holder.TopicViewHolder;
import com.metfone.selfcare.module.movienew.model.CategoryModel;
import com.metfone.selfcare.module.movienew.model.Country;
import com.metfone.selfcare.module.movienew.model.CountryModel;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.search.adapter.SearchAllAdapter;
import com.metfone.selfcare.module.search.adapter.ViewPagerAdapter;
import com.metfone.selfcare.module.search.event.KeySearchChangeEvent;
import com.metfone.selfcare.module.search.fragment.SearchSubmitFragment;
import com.metfone.selfcare.module.search.fragment.SearchSuggestFragment;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.SearchHistory;
import com.metfone.selfcare.module.search.model.SearchHistoryProvisional;
import com.metfone.selfcare.module.search.network.SearchApi;
import com.metfone.selfcare.module.search.utils.ConvertHelper;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchAllActivity extends BaseSlidingFragmentActivity implements ViewPager.OnPageChangeListener
        , TabLayout.OnTabSelectedListener, SearchAllListener.OnClickBoxHistory, OnClickMoreItemListener, ItemViewClickListener, SearchAllListener.OnClickReward {
    private final String TAG = getClass().getSimpleName();
    @BindView(R.id.layout_result)
    View viewResult;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.edit_search)
    EditText editSearch;
    @BindView(R.id.tv_search)
    TextView tv_search;

    RecyclerView recyclerView;
//    @BindView(R.id.layout_no_history_search)
//    View viewNoHistory;

    @BindView(R.id.recyclerViewTopic)
    RecyclerView recyclerViewTopic;
    @BindView(R.id.recyclerMaybe)
    RecyclerView recyclerMaybe;
    @BindView(R.id.recyclerRecentSearchesHistory)
    RecyclerView recyclerRecentSearchesHistory;
    @BindView(R.id.recyclerHistory)
    RecyclerView recyclerHistory;
    @BindView(R.id.layoutSuggest)
    ConstraintLayout layoutSuggest;
    @BindView(R.id.contraisMaybe)
    ConstraintLayout contraisMaybe;
    @BindView(R.id.contrainRecent)
    ConstraintLayout contrainRecent;
    @BindView(R.id.contrasSearchHome)
    LinearLayout contrasSearchHome;
    @BindView(R.id.btnClose)
    ImageView btnClose;

    private ArrayList<Object> topicModels = new ArrayList<>();
    private ArrayList<Movie> movieArrayListMaybe = new ArrayList<>();
    private ArrayList<String> mHistoryArraylist = new ArrayList<>();
    private ArrayList<SearchRewardResponse.Result.ItemReward> mRewardList = new ArrayList<>();
    private com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter baseAdapter;
    private com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter mHistoryAdapterHome;
    private com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter mResultMaybeAdapter;
    private com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter mHistoryAdapter;
    private com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter mRewardAdapter;
    private RetrofitInstance retrofitInstance = new RetrofitInstance();
    private CommonSearch mCommonSearch;
    public int totalAllResult = 0;
    private SearchApi searchApi = new SearchApi();

    private ViewPagerAdapter viewPagerAdapter;
    private int source;
    private String keySearch;
    private ArrayList<Object> data;
    private SearchAllAdapter adapter;
    private boolean showDataSuggest = true;
    private ApplicationController mApplication;
    private int indexChat = -1;
    private int indexVideo = -1;
    private int indexChannel = -1;
    private int indexMovies = -1;
    private int indexRewards = -1;
    private int indexMusic = -1;
    private int indexNews = -1;
    private int indexMV = -1;
    private int indexTiin = -1;
    private boolean doneSearch = false;

    public boolean isShowDataSuggest() {
        return showDataSuggest;
    }

    public void setShowDataSuggest(boolean show) {
        this.showDataSuggest = show;
    }
    private int DELAY_SEARCH = 800;
    private Runnable runnableSearch = new Runnable() {
        @Override
        public void run() {
            pushKeySearch();
        }
    };
    private Handler handlerSearch = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_all);
        ButterKnife.bind(this);
        recyclerView = findViewById(R.id.recycler_view);
        mCommonSearch = new CommonSearch(this);
        mHistoryArraylist = mCommonSearch.getSearchHistoryLimit();
        mApplication = (ApplicationController) getApplication();
        try {
            source = getIntent().getIntExtra(Constants.KEY_POSITION, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initViewPrepare();
        initDataPrepare();

        initAdapter();
        initListener();
        loadDataSuggest();
    }

    private void initDataPrepare() {
        MovieApi.getInstance().getTopicCountry(new ApiCallbackV2<ArrayList<Country>>() {
            @Override
            public void onSuccess(String msg, ArrayList<Country> result) throws JSONException {
                if (result != null)
                    topicModels.addAll(result);
                baseAdapter.updateAdapter(topicModels);
                checkEmptyTopic();
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
        MovieApi.getInstance().getTopicCategory(new ApiCallbackV2<ArrayList<CategoryModel>>() {
            @Override
            public void onSuccess(String msg, ArrayList<CategoryModel> result) throws JSONException {
                if (result != null) {
                    ArrayList<CategoryModel> categoryArr = new ArrayList<>();
                    for (CategoryModel categoryModel : result) {
                        if (!categoryModel.getCategoryid().equals("868462") && !categoryModel.getCategoryid().equals("868459")) {
                            categoryArr.add(categoryModel);
                        }
                    }
                    if (categoryArr != null && categoryArr.size() > 0) {
                        if (topicModels.size() > 0) {
                            topicModels.addAll(0, categoryArr);
                        } else {
                            topicModels.addAll(categoryArr);
                        }
                    }
                }
                baseAdapter.updateAdapter(topicModels);
                checkEmptyTopic();
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });

        MovieApi.getInstance().getMaybeYouLike(new ApiCallbackV2<ArrayList<HomeData>>() {
            @Override
            public void onSuccess(String msg, ArrayList<HomeData> result) throws JSONException {
                if (result != null && result.size() > 0) {
                    movieArrayListMaybe.clear();
                    for (int i = 0; i < result.size(); i++) {
                        movieArrayListMaybe.add(new Movie(result.get(i)));
                    }

                    mResultMaybeAdapter.updateAdapter(movieArrayListMaybe);
                    contraisMaybe.setVisibility(movieArrayListMaybe.size() > 0 ? View.VISIBLE : View.GONE);
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });

    }

    private void checkEmptyTopic() {
        if (topicModels.size() == 0) {
            contrasSearchHome.setVisibility(View.GONE);
        } else {
            contrasSearchHome.setVisibility(View.VISIBLE);
        }
    }

    private void initViewPrepare() {
        if (mHistoryArraylist.size() > 0) {
            contrainRecent.setVisibility(View.VISIBLE);
        }
        changeStatusBar(getResources().getColor(R.color.m_home_tab_background));

        TopicViewHolder topicHolder = new TopicViewHolder(getWindow().getDecorView().getRootView(), this);
        baseAdapter = new com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter(topicModels, this, R.layout.item_topic_search, topicHolder);
        FlexboxLayoutManager layoutManagerCast = new FlexboxLayoutManager(this);
        layoutManagerCast.setFlexDirection(FlexDirection.ROW);
        layoutManagerCast.setJustifyContent(JustifyContent.FLEX_START);
        recyclerViewTopic.setLayoutManager(layoutManagerCast);
        recyclerViewTopic.setAdapter(baseAdapter);


        HistorySearchViewHolder historySearchViewHolder = new HistorySearchViewHolder(getWindow().getDecorView().getRootView(), this, Constant.RESULT);
        mHistoryAdapter = new com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter(mHistoryArraylist, this, R.layout.item_history_search, historySearchViewHolder);
        recyclerHistory.setLayoutManager(new LinearLayoutManager(this));
        mHistoryAdapter.setItemCount(15);
        recyclerHistory.setAdapter(mHistoryAdapter);

        // history search
        HistorySearchViewHolder historySearchViewHomeHolder = new HistorySearchViewHolder(getWindow().getDecorView().getRootView(), this, Constant.HOME);
        mHistoryAdapterHome = new com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter(mHistoryArraylist, this, R.layout.item_history_search, historySearchViewHomeHolder);
        recyclerRecentSearchesHistory.setLayoutManager(new LinearLayoutManager(this));
        mHistoryAdapterHome.setItemCount(15);
        recyclerRecentSearchesHistory.setAdapter(mHistoryAdapterHome);

        // list maybe
        ResultSearchViewHolder resultSearchViewHolder = new ResultSearchViewHolder(getWindow().getDecorView().getRootView(), this);
        mResultMaybeAdapter = new com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter(movieArrayListMaybe, this, R.layout.item_movie_new, resultSearchViewHolder);
        recyclerMaybe.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerMaybe.setAdapter(mResultMaybeAdapter);

    }

    private void initAdapter() {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment fragment;
        Bundle bundle;
        bundle = new Bundle();
        bundle.putInt(Constants.KEY_POSITION, source);
        bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_ALL);
        fragment = SearchSuggestFragment.newInstance(bundle);
//        String title = mApplication.getReengAccountBusiness().isAnonymousLogin() ? getString(R.string.search_tab_all) : getString(R.string.tab_chat);
        String title = getString(R.string.search_tab_all);
        viewPagerAdapter.addFragment(fragment, title, SearchUtils.TAB_SEARCH_ALL);
        int currentTab = 0;
        int countTab = 0;
        if (!mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            indexChat = viewPagerAdapter.getCount();
            bundle = new Bundle();
            bundle.putInt(Constants.KEY_POSITION, source);
            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_CHAT);
            fragment = SearchSubmitFragment.newInstance(bundle);
            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_chat), SearchUtils.TAB_SEARCH_CHAT);
            countTab++;
        }
        ContentConfigBusiness configBusiness = mApplication.getConfigBusiness();
//        if (configBusiness.isEnableTabVideo()) {
//            indexVideo = viewPagerAdapter.getCount();
//            bundle = new Bundle();
//            bundle.putInt(Constants.KEY_POSITION, source);
//            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_VIDEO);
//            fragment = SearchSubmitFragment.newInstance(bundle);
//            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_video), SearchUtils.TAB_SEARCH_VIDEO);
//            countTab++;
//        }
        if (configBusiness.isEnableTabMovie()) {
            indexMovies = viewPagerAdapter.getCount();
            bundle = new Bundle();
            bundle.putInt(Constants.KEY_POSITION, source);
            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_MOVIES);
            fragment = SearchSubmitFragment.newInstance(bundle);
            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_cinema), SearchUtils.TAB_SEARCH_MOVIES);
            countTab++;
        }
        if(configBusiness.isEnableTabRewards()){
            indexRewards = viewPagerAdapter.getCount();
            bundle = new Bundle();
            bundle.putInt(Constants.KEY_POSITION, source);
            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_REWARD);
            fragment = SearchSubmitFragment.newInstance(bundle);
            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_reward), SearchUtils.TAB_SEARCH_REWARD);
            countTab++;
        }
//        if (configBusiness.isEnableTabESport()) {
//            indexMusic = viewPagerAdapter.getCount();
//            bundle = new Bundle();
//            bundle.putInt(Constants.KEY_POSITION, source);
//            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_ESPORT);
//            fragment = SearchSubmitFragment.newInstance(bundle);
//            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_esport), SearchUtils.TAB_SEARCH_ESPORT);
//            countTab++;
//        }
//        if (!mApplication.getReengAccountBusiness().isAnonymousLogin()) {
//            indexMV = viewPagerAdapter.getCount();
//            bundle = new Bundle();
//            bundle.putInt(Constants.KEY_POSITION, source);
//            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_CHAT);
//            fragment = SearchSubmitFragment.newInstance(bundle);
//            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_message), SearchUtils.TAB_SEARCH_CHAT);
//            countTab++;
//        }
//        if (configBusiness.isEnableTabReward()) {
//            indexMusic = viewPagerAdapter.getCount();
//            bundle = new Bundle();
//            bundle.putInt(Constants.KEY_POSITION, source);
//            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_REWARD);
//            fragment = SearchSubmitFragment.newInstance(bundle);
//            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_reward), SearchUtils.TAB_SEARCH_REWARD);
//            countTab++;
//        }
//        if (configBusiness.isEnableTabMusic()) {
//            indexMV = viewPagerAdapter.getCount();
//            bundle = new Bundle();
//            bundle.putInt(Constants.KEY_POSITION, source);
//            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_CHAT);
//            fragment = SearchSubmitFragment.newInstance(bundle);
//            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_message), SearchUtils.TAB_SEARCH_CHAT);
//            countTab++;
//        }
//        if (configBusiness.isEnableTabVideo()) {
//            indexChannel = viewPagerAdapter.getCount();
//            bundle = new Bundle();
//            bundle.putInt(Constants.KEY_POSITION, source);
//            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_CHANNEL_VIDEO);
//            fragment = SearchSubmitFragment.newInstance(bundle);
//            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_channel), SearchUtils.TAB_SEARCH_CHANNEL_VIDEO);
//            countTab++;
//        }
//        if (configBusiness.isEnableTabTiin()) {
//            indexTiin = viewPagerAdapter.getCount();
//            bundle = new Bundle();
//            bundle.putInt(Constants.KEY_POSITION, source);
//            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_TIIN);
//            fragment = SearchSubmitFragment.newInstance(bundle);
//            viewPagerAdapter.addFragment(fragment, getString(R.string.tab_tiin), SearchUtils.TAB_SEARCH_TIIN);
//            countTab++;
//            if (source == Constants.TAB_TIIN_HOME) currentTab = countTab;
//        }
//        if (configBusiness.isEnableTabNews()) {
//            indexNews = viewPagerAdapter.getCount();
//            bundle = new Bundle();
//            bundle.putInt(Constants.KEY_POSITION, source);
//            bundle.putInt(Constants.KEY_TYPE, SearchUtils.TAB_SEARCH_NEWS);
//            fragment = SearchSubmitFragment.newInstance(bundle);
//            viewPagerAdapter.addFragment(fragment, getString(R.string.search_tab_news), SearchUtils.TAB_SEARCH_NEWS);
//            countTab++;
//            if (source == Constants.TAB_NEWS_HOME) currentTab = countTab;
//        }
        viewPager.setAdapter(viewPagerAdapter);
        viewPagerAdapter.notifyDataSetChanged();
        if (viewPagerAdapter != null) {
            int count = viewPagerAdapter.getCount();
            viewPager.setOffscreenPageLimit(count);
            if (count >= 4) {
                tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            } else {
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
            }
        }
        viewPager.setCurrentItem(currentTab);
        viewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(this);
    }

    public void updateTabBarTitle(int currentTabId, String resultSize) {
        if (viewPagerAdapter == null || tabLayout == null) return;
        TabLayout.Tab tab = tabLayout.getTabAt(viewPagerAdapter.getPositionByType(currentTabId));
        if (tab == null) return;
        switch (currentTabId) {
            case SearchUtils.TAB_SEARCH_ALL:
                tab.setText(R.string.search_tab_all);
                break;
            case SearchUtils.TAB_SEARCH_CHAT:
                tab.setText(resultSize.isEmpty() ? getString(R.string.search_tab_chat) :
                        getString(R.string.search_tab_chat)+" ("+resultSize+")");
//                tab.setText(getString(R.string.contact));
                break;
            case SearchUtils.TAB_SEARCH_VIDEO:
                tab.setText(resultSize.isEmpty() ? getString(R.string.search_tab_video) :
                        getString(R.string.search_tab_video_result, resultSize));
                break;
            case SearchUtils.TAB_SEARCH_CHANNEL_VIDEO:
                tab.setText(resultSize.isEmpty() ? getString(R.string.search_tab_channel) :
                        getString(R.string.search_tab_channel_result, resultSize));
                break;
            case SearchUtils.TAB_SEARCH_MOVIES:
                tab.setText(resultSize.isEmpty() ? getString(R.string.search_tab_movies) :
                        getString(R.string.search_tab_movies)+" ("+resultSize+")");
                break;
            case SearchUtils.TAB_SEARCH_MUSIC:
                tab.setText(resultSize.isEmpty() ? getString(R.string.search_tab_music) :
                        getString(R.string.search_tab_music_result, resultSize));
                break;
            case SearchUtils.TAB_SEARCH_NEWS:
                tab.setText(resultSize.isEmpty() ? getString(R.string.search_tab_news) :
                        getString(R.string.search_tab_news_result, resultSize));
                break;
            case SearchUtils.TAB_SEARCH_MV:
                tab.setText(resultSize.isEmpty() ? getString(R.string.search_tab_mv) :
                        getString(R.string.search_tab_mv_result, resultSize));
                break;
            case SearchUtils.TAB_SEARCH_TIIN:
                tab.setText(resultSize.isEmpty() ? getString(R.string.tab_tiin) :
                        getString(R.string.search_tab_tiin_result, resultSize));
                break;
            case SearchUtils.TAB_SEARCH_REWARD:
                tab.setText(resultSize.isEmpty() ? getString(R.string.tab_reward) :
                        getString(R.string.search_tab_reward_result, resultSize));
                break;
        }
    }

    public void resetTabBarTitle() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            updateTabBarTitle(viewPagerAdapter.getTypeByPosition(i), "");
        }
    }

    private boolean initListener = false;

    private void initListener() {
        InputMethodUtils.hideKeyboardWhenTouch(recyclerView, this);
        InputMethodUtils.hideKeyboardWhenTouch(viewResult, this);
        editSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        editSearch.postDelayed(() -> {
            InputMethodUtils.showSoftKeyboard(SearchAllActivity.this, editSearch);
            initListener = true;
        }, 500);

        editSearch.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus && initListener) {
                InputMethodUtils.showSoftKeyboard(SearchAllActivity.this, editSearch);
            }
        });
        editSearch.setOnClickListener(v -> InputMethodUtils.showSoftKeyboard(SearchAllActivity.this, editSearch));
        editSearch.addTextChangedListener(new TextWatcher() {
            private String oldKeySearch;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.d(TAG, "beforeTextChanged s: " + s);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(TAG, "onTextChanged s: " + s);
                if (TextUtils.isEmpty(s)) {
                    keySearch = "";
                    btnClose.setImageResource(R.drawable.ic_search_new);
                }
                else {
                    keySearch = s.toString().trim();
                   // btnClose.setImageResource(R.drawable.ic_back_new);
                    doneSearch = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d(TAG, "afterTextChanged s: " + s + ", keySearch: " + keySearch);
                if (TextUtils.isEmpty(keySearch)) {
                    tv_search.setText(R.string.cancel);
                    tv_search.setTextColor(Color.WHITE);
                    loadDataSuggest();
                    if (recyclerHistory != null) recyclerHistory.setVisibility(View.GONE);
                    if (layoutSuggest != null) layoutSuggest.setVisibility(View.VISIBLE);
                } else {
                    if (!keySearch.equals(oldKeySearch)) {
                        handlerSearch.removeCallbacks(runnableSearch);
                        handlerSearch.postDelayed(runnableSearch,DELAY_SEARCH);
                    }
//                    tv_search.setText(R.string.search);
//                    tv_search.setTextColor(Color.WHITE);
                    if (recyclerHistory != null && mHistoryArraylist.size() > 0)
                        recyclerHistory.setVisibility(View.VISIBLE);
                }
                oldKeySearch = keySearch;
            }
        });
        editSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                return true;
            }
            return false;
        });

        data = new ArrayList<>();
        adapter = new SearchAllAdapter(SearchAllActivity.this);
        adapter.setItems(data);
        adapter.setListener(this);
        BaseAdapter.setupVerticalRecycler(this, recyclerView, null, adapter, false);
    }

    public void loadDataSuggest() {
        if (Utilities.notEmpty(data)) {
            int size = data.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (data.get(i) instanceof SearchHistoryProvisional)
                    data.remove(i);
            }
        }
        SearchHistoryProvisional dataHistory = SearchUtils.getHistorySearch();
        if (dataHistory != null) {
            if (Utilities.notEmpty(dataHistory.getData()))
                data.add(0, dataHistory);
        }
        if (adapter != null) adapter.notifyDataSetChanged();
        if (showDataSuggest) showDataSuggest();
    }


    public void showDataSuggest() {
        showDataSuggest = true;
        if (viewResult != null) viewResult.setVisibility(View.INVISIBLE);
        if (Utilities.isEmpty(data)) {
            if (recyclerView != null) recyclerView.setVisibility(View.GONE);
//            if (viewNoHistory != null){
//                viewNoHistory.setVisibility(View.VISIBLE);
//                if(getKeySearch().isEmpty()){
//                    layoutSuggest.setVisibility(View.VISIBLE);
//                    viewResult.setVisibility(View.INVISIBLE);
//                }
//            }
            layoutSuggest.setVisibility(View.VISIBLE);
        } else {
//            layoutSuggest.setVisibility(View.INVISIBLE);
            if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
//            if (viewNoHistory != null) viewNoHistory.setVisibility(View.GONE);
        }
    }

    public void showDataResult() {
        showDataSuggest = false;
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
//        if (viewNoHistory != null) viewNoHistory.setVisibility(View.GONE);
        if (viewResult != null) viewResult.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (BackStackHelper.getInstance().checkIsLastInStack(getApplication(), SearchAllActivity.class.getName())) {
            goToHome();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void openTabChat() {
        hideSoftKeyboard();
        if (indexChat >= 0) {
            try {
                viewPager.setCurrentItem(indexChat);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openTabVideo() {
        hideSoftKeyboard();
        if (indexVideo >= 0) {
            try {
                viewPager.setCurrentItem(indexVideo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openTabChannel() {
        hideSoftKeyboard();
        if (indexChannel >= 0) {
            try {
                viewPager.setCurrentItem(indexChannel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openTabMovies() {
        hideSoftKeyboard();
        if (indexMovies >= 0) {
            try {
                viewPager.setCurrentItem(indexMovies);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openTabRewards() {
        hideSoftKeyboard();
        if (indexRewards >= 0) {
            try {
                viewPager.setCurrentItem(indexRewards);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openTabMusic() {
        hideSoftKeyboard();
        if (indexMusic >= 0) {
            try {
                viewPager.setCurrentItem(indexMusic);
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace();
            }
        }
    }

    public void openTabNews() {
        hideSoftKeyboard();
        if (indexNews >= 0) {
            try {
                viewPager.setCurrentItem(indexNews);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openTabMV() {
        hideSoftKeyboard();
        if (indexMV >= 0) {
            try {
                viewPager.setCurrentItem(indexMV);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openTabTiin() {
        if (indexNews >= 0) {
            try {
                viewPager.setCurrentItem(indexTiin);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clickItemDetail(SearchHistory object) {
        boolean check = SearchUtils.insertHistorySearch(object);
        if (check) {
            Map<String, Object> mapParams = new HashMap<>();
            loadDataSuggest();
        }
    }

    private void clearRecentSearch() {
        mHistoryArraylist.clear();
        mCommonSearch.clearHistory();
        mHistoryAdapter.notifyDataSetChanged();
        mHistoryAdapterHome.notifyDataSetChanged();
        contrainRecent.setVisibility(View.GONE);
    }

    private void pushKeySearch() {
        doneSearch = true;
        if (editSearch.getText().length() > 0) {
            editSearch.setSelection(editSearch.getText().length());
        }
        layoutSuggest.setVisibility(View.INVISIBLE);
        btnClose.setImageResource(R.drawable.ic_search_new);
        tv_search.setText(R.string.cancel);
        if (recyclerHistory != null) recyclerHistory.setVisibility(View.GONE);
//        KeySearchChangeEvent event = EventBus.getDefault().removeStickyEvent(KeySearchChangeEvent.class);
//        if (event == null) event = new KeySearchChangeEvent();
        KeySearchChangeEvent event = new KeySearchChangeEvent();
        event.setKeySearch(editSearch.getText().toString());
        event.setSource(source);
//        EventBus.getDefault().postSticky(event);
        if (viewPagerAdapter != null)
            viewPagerAdapter.updateKeySearch(event);

        mHistoryArraylist.add(getKeySearch());
        mCommonSearch.insertSearchHistory(getKeySearch());

        mHistoryArraylist.clear();
        mHistoryArraylist.addAll(mCommonSearch.getSearchHistoryLimit());
        mHistoryAdapterHome.updateAdapter(mHistoryArraylist);
        mHistoryAdapter.updateAdapter(mHistoryArraylist);

    }

    @Override
    public void onClickHistorySearchItem(SearchHistory item) {
        hideSoftKeyboard();
        if (item.getType() == SearchHistory.TYPE_KEY_SEARCH && editSearch != null) {
            editSearch.setText(item.getKeySearch());
            editSearch.setSelection(editSearch.getText().length());
            pushKeySearch();
        } else if (item.getType() == SearchHistory.TYPE_CONTACT && item.getContact() != null) {
            ThreadMessage threadTmp = mApplication.getMessageBusiness().findExistingOrCreateNewThread(item.getContact().getJidNumber());
            if (threadTmp == null) {
                NavigateActivityHelper.navigateToContactDetail(this, item.getContact());
            } else {
                NavigateActivityHelper.navigateToChatDetail(this, threadTmp);
            }
        } else if (item.getType() == SearchHistory.TYPE_CHANNEL && item.getChannel() != null) {
            mApplication.getApplicationComponent().providesUtils().openChannelInfo(this, item.getChannel());
        } else if (item.getType() == SearchHistory.TYPE_CHAT && item.getThreadChat() != null) {
            ThreadMessage threadTmp = mApplication.getMessageBusiness().getThreadById(item.getThreadChat().getId());
            if (threadTmp == null) {
                boolean check = SearchUtils.deleteHistorySearch(item);
                if (check) loadDataSuggest();
                return;
            } else {
                NavigateActivityHelper.navigateToChatDetail(this, threadTmp);
            }
        }
        clickItemDetail(item);
    }

    @Override
    public void onClickDeleteHistorySearch() {
        SearchUtils.deleteHistorySearch();
        loadDataSuggest();
    }

    @Override
    public void onClickDeleteItemHistorySearch(SearchHistory item) {
        boolean check = SearchUtils.deleteHistorySearch(item);
        if (check) loadDataSuggest();
    }

    @Override
    public void onClickMenuMore(Object item) {
        if (item == null) return;
        if (item instanceof Video) {
            DialogUtils.showOptionVideoItem(this, (Video) item, this);
        }
        if (item instanceof Movie) {
            DialogUtils.showOptionMovieItem(this, (Movie) item, this);
        }
        if (item instanceof SearchModel) {
            if (((SearchModel) item).getType() == Constants.TYPE_SONG || ((SearchModel) item).getType() == Constants.TYPE_VIDEO) {
                DialogUtils.showOptionMusicItem(this,
                        ConvertHelper.convertToMedia((SearchModel) item, UrlConfigHelper.getInstance(this).getDomainImageSearch()), this);
            }
        }
        if (item instanceof NewsModel) {
            DialogUtils.showOptionNewsItem(this, (NewsModel) item, this);
        }
        if (item instanceof Channel) {
            DialogUtils.showOptionChannelItem(this, item, this);
        }
        if (item instanceof TiinModel) {
            DialogUtils.showOptionTiinItem(this, (TiinModel) item, this);
        }
    }

    @OnClick({R.id.tv_search, R.id.btnClear,R.id.btnClose})
    public void onClickView(View view) {
        SearchUtils.onClickView(view);
        int viewId = view.getId();
        if (viewId == R.id.button_back) {
            onBackPressed();
        } else if (viewId == R.id.btnClear) {
            editSearch.setText("");
            clearRecentSearch();
//            InputMethodUtils.showSoftKeyboard(this, editSearch);
        } else if (viewId == R.id.tv_search) {
            hideSoftKeyboard();
            finish();
        } else if (viewId == R.id.btnClose) {
            if (!doneSearch) {
                finish();
            }
        }
    }

    @Override
    protected void onPause() {
        hideSoftKeyboard();
        super.onPause();
    }

    public void hideSoftKeyboard() {
        if (!isFinishing()) {
            InputMethodUtils.hideSoftKeyboard(editSearch, this);
        }
    }

    public String getKeySearch() {
        return editSearch == null || editSearch.getText() == null ? "" : editSearch.getText().toString();
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        switch (menuId) {
            case Constants.MENU.MENU_SHARE_LINK:
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    showDialogLogin();
                } else {
                    ShareUtils.openShareMenu(this, object);
                }
                break;
            case Constants.MENU.MENU_SAVE_VIDEO:
                if (object instanceof Video) {
                    VideoDataSource.getInstance(mApplication).saveVideoFromMenu((Video) object);
                    showToast(R.string.videoSavedToLibrary);
                }
                break;
            case Constants.MENU.MENU_ADD_FAVORITE:
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    showDialogLogin();
                } else {
                    FeedModelOnMedia feed = null;
                    if (object instanceof Movie) {
                        feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) object);
                    } else if (object instanceof Video) {
                        feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia((Video) object);
                    } else if (object instanceof AllModel) {
                        feed = FeedModelOnMedia.convertMediaToFeedModelOnMedia((AllModel) object);
                    } else if (object instanceof NewsModel) {
                        feed = FeedModelOnMedia.convertNewsToFeedModelOnMedia((NewsModel) object);
                    } else if (object instanceof TiinModel) {
                        feed = FeedModelOnMedia.convertTiinToFeedModelOnMedia((TiinModel) object);
                    }
                    if (feed != null) {
                        new WSOnMedia(mApplication).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) throws JSONException {
                                        showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                }
                break;
            case Constants.MENU.MENU_ADD_LATER:
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    showDialogLogin();
                } else {
                    if (object instanceof Video) {
                        VideoDataSource.getInstance(mApplication).watchLaterVideoFromMenu((Video) object);
                        showToast(R.string.add_later_success);
                    }
                    if (object instanceof Movie) {
                        new MovieApi().insertWatchLater((Movie) object, new ApiCallbackV2<String>() {

                            @Override
                            public void onError(String s) {

                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onSuccess(String msg, String result) throws JSONException {
                                showToast(R.string.add_later_success);
                            }
                        });
                    }
                }
                break;
        }
    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {
        hideSoftKeyboard();
        if (list.get(position) instanceof Country) {
            Country countryModel = (Country) list.get(position);
            String categoryName = countryModel.getCategoryName();
            String categoryNameId = countryModel.getCategoryId();
            ActionCategoryFragment actionCategoryFragment = ActionCategoryFragment.newInstance(categoryNameId,categoryName,ActionCategoryFragment.COUNTRY);
            addFragment(R.id.frameCategory, actionCategoryFragment, true);
        }
        if (list.get(position) instanceof CategoryModel) {
            CategoryModel categoryModel = (CategoryModel) list.get(position);
            String categoryName = categoryModel.getCategoryname();
            String categoryNameId = categoryModel.getCategoryid();
            ActionCategoryFragment actionCategoryFragment = ActionCategoryFragment.newInstance(categoryNameId,categoryName,ActionCategoryFragment.CATEGORY);
            addFragment(R.id.frameCategory, actionCategoryFragment, true);
        }
        if (list.get(position) instanceof String) {
            String data = (String) list.get(position);
            editSearch.setText(data);
            layoutSuggest.setVisibility(View.INVISIBLE);
            showDataResult();
            pushKeySearch();
        }
        if (list.get(position) instanceof Movie) {
            Movie movie = (Movie) list.get(position);
            this.playMovies(movie);
        }
        if (list.get(position) instanceof SearchRewardResponse.Result.ItemReward){
            SearchRewardResponse.Result.ItemReward reward = (SearchRewardResponse.Result.ItemReward) list.get(position);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.KEY_ITEM_REWARD, reward);
            Intent intent = new Intent(this, RewardDetailActivity.class);
            intent.putExtra(Constants.KEY_BUNDLE_REWARD, bundle);
            startActivity(intent);
        }
    }

    @Override
    public void onClickReward(SearchRewardResponse.Result.ItemReward item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_ITEM_REWARD, item);
        Intent intent = new Intent(this, RewardDetailActivity.class);
        intent.putExtra(Constants.KEY_BUNDLE_REWARD, bundle);
        startActivity(intent);
    }

    @Override
    public void onClickRewardMore() {
        //Todo: Click reward more
    }
}

package com.metfone.selfcare.module.tiin.wiget;

import android.text.Layout;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.MotionEvent;
import android.widget.TextView;

public abstract class TextViewLinkHandler extends LinkMovementMethod {

    public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_UP)
            return super.onTouchEvent(widget, buffer, event);

        try
        {
            int x = (int) event.getX();
            int y = (int) event.getY();

            x -= widget.getTotalPaddingLeft();
            y -= widget.getTotalPaddingTop();

            x += widget.getScrollX();
            y += widget.getScrollY();

            Layout layout = widget.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            URLSpan[] link = buffer.getSpans(off, off, URLSpan.class);

            String linkText ="";
            ClickableSpan[] text = buffer.getSpans(off, off, ClickableSpan.class);

            if (text.length != 0) {
                linkText = buffer.subSequence(buffer.getSpanStart(text[0]),
                        buffer.getSpanEnd(text[0])).toString();
            }
            if (link.length != 0) {
                onLinkClick(linkText,link[0].getURL());
            }
        }
        catch (IndexOutOfBoundsException ex)
        {

        }
        return true;
    }

    abstract public void onLinkClick(String linkText,String url);
}
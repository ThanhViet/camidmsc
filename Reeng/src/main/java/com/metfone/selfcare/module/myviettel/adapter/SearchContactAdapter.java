/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/6
 *
 */

package com.metfone.selfcare.module.myviettel.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.module.myviettel.holder.ContactDetailHolder;
import com.metfone.selfcare.module.myviettel.holder.NonContactDetailHolder;
import com.metfone.selfcare.module.myviettel.listener.OnDataChallengeListener;

public class SearchContactAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    public static final int TYPE_NORMAL = 1;
    public static final int TYPE_MORE = 2;

    private OnDataChallengeListener listener;

    public SearchContactAdapter(Activity activity) {
        super(activity);
    }

    public void setListener(OnDataChallengeListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_NORMAL) {
            return new ContactDetailHolder(layoutInflater.inflate(R.layout.holder_suggest_user_mvt_dc, parent, false), activity, listener);
        } else if (viewType == TYPE_MORE) {
            return new NonContactDetailHolder(layoutInflater.inflate(R.layout.holder_invite_non_contact_mvt_dc, parent, false), listener);
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(getItem(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof PhoneNumber) return TYPE_NORMAL;
        if (item instanceof String) return TYPE_MORE;
        return TYPE_EMPTY;
    }
}

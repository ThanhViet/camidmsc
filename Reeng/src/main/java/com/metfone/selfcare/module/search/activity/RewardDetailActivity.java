package com.metfone.selfcare.module.search.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.metfone.esport.common.Constant;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.fragment.rewardsdetail.RewardsDetailKHFragment;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;

public class RewardDetailActivity extends BaseSlidingFragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_detail);
        changeStatusBar(getResources().getColor(R.color.m_home_tab_background));
        SearchRewardResponse.Result.ItemReward
                itemReward = (SearchRewardResponse.Result.ItemReward) getIntent().
                getBundleExtra(Constants.KEY_BUNDLE_REWARD).getSerializable(Constants.KEY_ITEM_REWARD);
        getSupportFragmentManager().beginTransaction().add(R.id.fl_container,
                RewardsDetailKHFragment.newInstance(RewardsDetailKHFragment.FROM_SEARCH,itemReward)).commit();

    }
}
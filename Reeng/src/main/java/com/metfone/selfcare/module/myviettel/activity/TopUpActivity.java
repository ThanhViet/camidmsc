/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/4
 *
 */

package com.metfone.selfcare.module.myviettel.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.myviettel.model.CaptchaModel;
import com.metfone.selfcare.module.myviettel.network.ApiCallback;
import com.metfone.selfcare.module.myviettel.network.MyViettelApi;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopUpActivity extends BaseSlidingFragmentActivity implements OnInternetChangedListener {
    private final String TAG = "TopUpActivity";
    @BindView(R.id.text_input_card_code)
    TextInputLayout textInputCardCode;
    //    @BindView(R.id.text_input_phone)
//    TextInputLayout textInputPhone;
    @BindView(R.id.text_input_captcha)
    TextInputLayout textInputCaptcha;
    @BindView(R.id.loading_captcha)
    View loadingCaptcha;
    @BindView(R.id.iv_captcha)
    ImageView ivCaptcha;
    @BindView(R.id.iv_renew_captcha)
    View ivRenewCaptcha;
    @BindView(R.id.button_submit)
    View btnSubmit;
    @BindView(R.id.button_back)
    View btnBack;
    @BindView(R.id.tv_receiver_number)
    TextView tvReceiverNumber;
    //    @BindView(R.id.sp_choose_card_type)
//    Spinner spChooseCardType;
    @BindView(R.id.layout_root)
    View viewRoot;

    private EditText editCardCode;
    //    private EditText editPhone;
    private EditText editCaptcha;

    private CaptchaModel currentCaptcha;
    private boolean isDisplayedCaptcha;
    //    private List<String> listCardTypes = new ArrayList<>();
    private String contactName = "";
    private String contactPhone = "";
    private ListenerUtils listenerUtils;
    private boolean isLoadingCaptcha;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up_mvt);
        ButterKnife.bind(this);
        initListener();
        loadCaptcha();
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) listenerUtils.addListener(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (editCaptcha != null)
                    InputMethodUtils.showSoftKeyboard(TopUpActivity.this, editCardCode);
            }
        }, 300);
    }

    private String getText(EditText editText) {
        if (editText != null) return editText.getText().toString().trim();
        return "";
    }

    private void focusView(EditText editText) {
        if (editText != null) editText.requestFocus();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initListener() {
        contactPhone = ApplicationController.self().getReengAccountBusiness().getJidNumber();
        if (Utilities.notEmpty(contactPhone))
            contactName = getString(R.string.you);
        if (viewRoot != null) InputMethodUtils.hideKeyboardWhenTouch(viewRoot, this);
        if (tvReceiverNumber != null)
            tvReceiverNumber.setText(getString(R.string.receiver_number, getPhoneText(contactName, contactPhone)));
        if (textInputCardCode != null) editCardCode = textInputCardCode.getEditText();
//        if (textInputPhone != null) editPhone = textInputPhone.getEditText();
        if (textInputCaptcha != null) editCaptcha = textInputCaptcha.getEditText();
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                finish();
                InputMethodUtils.hideSoftKeyboard(TopUpActivity.this);
            }
        });
        btnSubmit.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                submitRecharge();
            }
        });
        ivRenewCaptcha.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (isDisplayedCaptcha) {
                    loadCaptcha();
                }
            }
        });
//        if (editPhone != null) {
//            editPhone.setText(getPhoneText(contactName, contactPhone));
//            editPhone.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    //final int DRAWABLE_LEFT = 0;
//                    //final int DRAWABLE_TOP = 1;
//                    final int DRAWABLE_RIGHT = 2;
//                    //final int DRAWABLE_BOTTOM = 3;
//                    if (event.getAction() == MotionEvent.ACTION_UP) {
//                        if (event.getRawX() >= (editPhone.getRight() - editPhone.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                            Intent chooseFriend = new Intent(TopUpActivity.this, ChooseContactActivity.class);
//                            chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_RECHARGE);
//                            startActivityForResult(chooseFriend, Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_RECHARGE, true);
//                            return true;
//                        }
//                    }
//                    return false;
//                }
//            });
//        }
//        if (editCardCode != null) {
//            editCardCode.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    //final int DRAWABLE_LEFT = 0;
//                    //final int DRAWABLE_TOP = 1;
//                    final int DRAWABLE_RIGHT = 2;
//                    //final int DRAWABLE_BOTTOM = 3;
//                    if (event.getAction() == MotionEvent.ACTION_UP) {
//                        if (event.getRawX() >= (editCardCode.getRight() - editCardCode.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                            if (DeviceHelper.isBackCameraAvailable(TopUpActivity.this)) {
//                                Intent intent = new Intent(TopUpActivity.this, QRCodeActivity.class);
//                                intent.putExtra("KEY_FROM_SOURCE", Constants.QR_CODE.FROM_MVT_RECHARGE);
//                                startActivityForResult(intent, Constants.QR_CODE.TYPE_RECHARGE);
//                            } else {
//                                showToast(R.string.qr_err_camera_not_found);
//                            }
//                            return true;
//                        }
//                    }
//                    return false;
//                }
//            });
//        }
        if (editCaptcha != null) {
            editCaptcha.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        hideSoftKeyboard(editCaptcha);
                        submitRecharge();
                    }
                    return false;
                }
            });
        }
//        if (listCardTypes == null) listCardTypes = new ArrayList<>();
//        listCardTypes.add(getString(R.string.money_top_up_card));
//        listCardTypes.add(getString(R.string.data_top_up_card));
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.holder_card_type_mvt, listCardTypes);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spChooseCardType.setAdapter(adapter);
//        spChooseCardType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = "";
//                if (listCardTypes != null && listCardTypes.size() > position)
//                    value = listCardTypes.get(position);
//                Log.d(TAG, "onItemSelected position: " + position + " - " + value);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                Log.d(TAG, "onNothingSelected");
//            }
//        });
    }

    private void hideSoftKeyboard(EditText editText) {
        if (!isFinishing()) {
            InputMethodUtils.hideSoftKeyboard(editText, this);
        }
    }

    private void loadCaptcha() {
        if (isLoadingCaptcha) return;
        isLoadingCaptcha = true;
        isDisplayedCaptcha = false;
        if (loadingCaptcha != null) loadingCaptcha.setVisibility(View.VISIBLE);
        if (ivCaptcha != null) ivCaptcha.setVisibility(View.INVISIBLE);
        ImageBusiness.setResourceCaptcha(ivCaptcha, R.color.white);
        getMyViettelApi().getCaptcha(new ApiCallback<CaptchaModel>() {
            @Override
            public void onSuccess(String msg, CaptchaModel result) throws Exception {
                currentCaptcha = result;
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {
                isLoadingCaptcha = false;
                displayCaptcha();
            }
        });
    }

    private void displayCaptcha() {
        String urlCaptcha = "";
        if (currentCaptcha != null) {
            urlCaptcha = currentCaptcha.getUrl();
        }
        if (ivCaptcha != null) ivCaptcha.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(urlCaptcha)) {
            if (loadingCaptcha != null) loadingCaptcha.setVisibility(View.GONE);
            ImageBusiness.setResourceCaptcha(ivCaptcha, R.color.white);
            isDisplayedCaptcha = true;
        } else {
            if (loadingCaptcha != null) loadingCaptcha.setVisibility(View.VISIBLE);
            final String finalUrlCaptcha = urlCaptcha;
            ImageBusiness.setImageCaptcha(ivCaptcha, urlCaptcha, new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    Log.d(TAG, "onLoadFailed displayCaptcha: " + finalUrlCaptcha);
                    if (loadingCaptcha != null) loadingCaptcha.setVisibility(View.GONE);
                    isDisplayedCaptcha = true;
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    Log.d(TAG, "onResourceReady displayCaptcha: " + finalUrlCaptcha);
                    if (loadingCaptcha != null) loadingCaptcha.setVisibility(View.GONE);
                    isDisplayedCaptcha = true;
                    return false;
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.QR_CODE.TYPE_RECHARGE:
                    if (data != null) {
                        String qrCode = data.getStringExtra(Constants.QR_CODE.RESULT_SCAN_QR);
                        if (Utilities.notEmpty(qrCode) && editCardCode != null) {
                            editCardCode.setText(qrCode);
                        }
                    }
                    break;
//                case Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_RECHARGE:
//                    if (data != null) {
//                        contactName = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NAME);
//                        contactPhone = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
//                        if (Utilities.notEmpty(contactPhone) && editPhone != null) {
//                            editPhone.setText(getPhoneText(contactName, contactPhone));
//                        }
//                    }
//                    break;
                default:
                    break;
            }
        }
    }

    private String getPhoneText(String contactName, String contactPhone) {
        if (TextUtils.isEmpty(contactPhone)) return "";
        if (TextUtils.isEmpty(contactName))
            return TextHelper.formatPhoneNumberToString(contactPhone);
        return (TextHelper.formatPhoneNumberToString(contactPhone) + " (" + contactName + ")").trim();
    }

    private void submitRecharge() {
        String cardCode = getText(editCardCode);
//        String phone = getText(editPhone);
        String captcha = getText(editCaptcha);

        if (TextUtils.isEmpty(cardCode)) {
            showToast(R.string.msg_enter_pincode);
            focusView(editCardCode);
            return;
        }
//        else if (cardCode.length() != 13 && cardCode.length() != 15) {
//            showToast(R.string.msg_enter_pincode_2);
//            focusView(editCardCode);
//            return;
//        }

//        if (TextUtils.isEmpty(phone)) {
//            focusView(editPhone);
//            return;
//        } else if (phone.equalsIgnoreCase(getPhoneText(contactName, contactPhone))) {
//            phone = contactPhone;
//        }

        if (TextUtils.isEmpty(captcha)) {
            showToast(R.string.msg_enter_captcha);
            focusView(editCaptcha);
            return;
        }

        if (isDisplayedCaptcha) {
            showLoadingDialog("", R.string.processing);
            getMyViettelApi().paymentOnline(cardCode, captcha, currentCaptcha.getSid(), contactPhone, new ApiCallback<Boolean>() {
                @Override
                public void onSuccess(String msg, Boolean result) throws Exception {
                    if (result) {
                        if (editCaptcha != null) editCaptcha.setText("");
                        if (editCardCode != null) editCardCode.setText("");
                    }
                    hideLoadingDialog();
                    showToast(msg);
                }

                @Override
                public void onError(String s) {
                    showToast(s);
                    hideLoadingDialog();
                }

                @Override
                public void onComplete() {
                    loadCaptcha();
                }
            });
        }
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(this)) {
            if (currentCaptcha == null) {
                loadCaptcha();
            } else if (!isDisplayedCaptcha) {
                displayCaptcha();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (listenerUtils != null) listenerUtils.removerListener(this);
        super.onDestroy();
    }

    private MyViettelApi myViettelApi;

    private MyViettelApi getMyViettelApi() {
        if (myViettelApi == null) myViettelApi = new MyViettelApi();
        return myViettelApi;
    }
}

package com.metfone.selfcare.module.tiin.categorynow.presenter;

import com.metfone.selfcare.module.tiin.base.MvpPresenter;

public interface ICategoryNowTiinMvpPresenter extends MvpPresenter {
    void getCategoryEvent(int page, int num);

}

package com.metfone.selfcare.module.games.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

import java.util.ArrayList;

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.GameHolder> {

    private final ArrayList<GameModel> listGame;
    private final Context context;

    private OnItemClick callBack;

    public GameAdapter(ArrayList<GameModel> listGame, Context context) {
        this.listGame = listGame;
        this.context = context;
    }

    public void setOnItemClick(OnItemClick event) {
        callBack = event;
    }

    @NonNull
    @Override
    public GameHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_game_new, parent, false);
        return new GameHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GameHolder holder, int position) {
        GameModel gameData = listGame.get(position);
        Glide.with(context)
                .asBitmap().error(R.drawable.df_image_home_poster)
                .load(gameData.getIconURL())
                .into(holder.ivGameIcon);
        holder.tvGameName.setText(gameData.getName());
    }

    @Override
    public int getItemCount() {
        return listGame.size();
    }

    public class GameHolder extends RecyclerView.ViewHolder {
        private final RoundedImageView ivGameIcon;
        private final TextView tvGameName;

        public GameHolder(@NonNull View itemView) {
            super(itemView);
            ivGameIcon = itemView.findViewById(R.id.iv_cover);
            tvGameName = itemView.findViewById(R.id.tvTitleGame);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callBack != null) {
                        callBack.onItemClick(tvGameName.getText() + "");
                    }
                }
            });
        }
    }
}

package com.metfone.selfcare.module.metfoneplus.topup.model;



import androidx.annotation.StringRes;

import com.metfone.selfcare.R;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ServiceTopUpModel {
    private int img;
    private boolean isSelect;
    private String paymentMethod;

    public ServiceTopUpModel(int img, boolean isSelect, String value) {
        this.img = img;
        this.isSelect = isSelect;
        this.paymentMethod = value;
    }
}

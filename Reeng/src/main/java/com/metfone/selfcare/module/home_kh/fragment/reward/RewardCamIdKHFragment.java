package com.metfone.selfcare.module.home_kh.fragment.reward;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.common.FirebaseEventConstant;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.module.home_kh.activity.BackupKhActivity;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountPointRankResponse;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.RankDefineResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.RewardBaseFragment;
import com.metfone.selfcare.module.home_kh.fragment.benefits.BenefitsKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointContainerFragment;
import com.metfone.selfcare.module.home_kh.fragment.menu.RewardCamIdActivity;
import com.metfone.selfcare.module.home_kh.fragment.reward.adapter.RewardPageAdapter;
import com.metfone.selfcare.module.home_kh.fragment.rewardsdetail.RewardsDetailKHFragment;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.KHAccountPoint;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.model.RankDefine;
import com.metfone.selfcare.module.home_kh.model.RedeemItem;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.movienew.common.Constant;
import com.metfone.selfcare.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;

public class RewardCamIdKHFragment extends RewardBaseFragment implements ViewPager.OnPageChangeListener {

    private KhHomeClient homeKhClient;
    private List<RankDefine> listRank = new ArrayList<>();
    private int totalAccumulate;
    private int totalAvailability;
    private Unbinder unbinder;
    private RankDefine nextRank;

    public static RewardCamIdKHFragment newInstance() {
        RewardCamIdKHFragment fragment = new RewardCamIdKHFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private List<KHAccountPoint> listAccumulate = new ArrayList<>();
    private List<KHAccountPoint> listAvailability = new ArrayList<>();


    private AccountRankDTO accountRankDTO;


    public static final String TAG = RewardCamIdKHFragment.class.getSimpleName();
    private BackupKhActivity mParentActivity;

    @BindView(R.id.pb_rank_percent)
    ProgressBar goldPb;

    @BindView(R.id.ic_account_rank)
    AppCompatImageView icAccountRank;

    @BindView(R.id.ic_next_rank)
    AppCompatImageView icNextRank;

    @BindView(R.id.tv_rank_name)
    AppCompatTextView tvRankName;

    @BindView(R.id.tv_rank_percent)
    AppCompatTextView tvPercent;

    @BindView(R.id.point_label)
    AppCompatTextView pointLabel;

    @BindView(R.id.point_value)
    AppCompatTextView pointValue;

    @BindView(R.id.point_expired)
    AppCompatTextView pointExpired;

    @BindView(R.id.point)
    AppCompatTextView tvPoint;

    @BindView(R.id.reward_page)
    ViewPager vpReward;

    @BindView(R.id.tab_active)
    AppCompatButton tabActive;

    @BindView(R.id.tab_past)
    AppCompatButton tabPast;

    @BindView(R.id.txtTitleToolbar)
    protected AppCompatTextView txtTitle;

    @BindView(R.id.imgNextRank)
    AppCompatImageView imgNextRank;

    private RewardPageAdapter rewardPageAdapter;


    @Override
    protected boolean needShowBottomMenu() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParentActivity = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_reward_kh;
    }

    private void initView() {
        txtTitle.setText(ResourceUtils.getString(R.string.reward_cam_id_title));
        pointValue.setText("0");
        tvPoint.setText(getString(R.string.point));
        initPage();
        //TODO user data list rank and rank account in another screen, only call get list point
        //wsGetAccountPointInfo();
        getListRank();
    }

    private void initPage() {
        List<Fragment> list = new ArrayList<>();
        list.add(GiftActiveKHFragment.newInstance());
        list.add(GiftPastKHFragment.newInstance());
        rewardPageAdapter = new RewardPageAdapter(getChildFragmentManager(),
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, list);
        vpReward.setAdapter(rewardPageAdapter);
        vpReward.addOnPageChangeListener(this);
        onPageSelected(0);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            tabPast.setSelected(false);
            tabActive.setSelected(true);
        } else {
            tabPast.setSelected(true);
            tabActive.setSelected(false);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick(R.id.tab_active)
    void clickActive() {
        if (vpReward != null)
            vpReward.setCurrentItem(0);
    }

    @OnClick(R.id.tab_past)
    void clickPast() {
        if (vpReward != null)
            vpReward.setCurrentItem(1);
    }

    /**
     * get list point
     */
    private void getListRank() {
        if (listRank.isEmpty()) {
            if (homeKhClient == null) {
                homeKhClient = new KhHomeClient();
            }
            homeKhClient.wsGetRankDefineInfo(new KhApiCallback<KHBaseResponse<RankDefineResponse>>() {
                @Override
                public void onSuccess(KHBaseResponse<RankDefineResponse> body) {
                    logH("wsGetRankDefineInfo > onSuccess");
                    listRank.clear();
                    listRank.addAll(body.getData().getListRank());
                    getRankAccount();
                }

                @Override
                public void onFailed(String status, String message) {
                    logH("wsGetRankDefineInfo > onFailed > " + "status: " + status + "message: " + message);
                    if (getActivity() != null)
                        com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                    Log.e("wsGetRankDefineInfo", status + " " + message);
                }

                @Override
                public void onFailure(Call<KHBaseResponse<KHBaseResponse<RankDefineResponse>>> call, Throwable t) {
                    t.printStackTrace();
                    logH("wsGetRankDefineInfo > onFailure");
                }
            });
        } else {
            getRankAccount();
        }

    }

    private void logH(String m) {
        Log.e("H-Reward", m);
    }

    /**
     * get info rank of acc
     */
    private void getRankAccount() {

        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                logH("wsGetAccountRankInfo > onSuccess");
                initRankInfo(body.getData().getAccountRankDTO());
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsGetAccountRankInfo > onFailed > " + "status: " + status + "message: " + message);
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetAccountRankInfo > onFailure");
            }
        });

    }

    private void initRankInfo(AccountRankDTO account) {
        accountRankDTO = account;
        if (accountRankDTO.rankId == 1 && listRank.size() > 2) {
            tvPercent.setText(String.format(tvPercent.getContext().getResources().getString(R.string.percent_gold), "100", listRank.get(1).getRankName()));
            accountRankDTO.rankName = listRank.get(0).getRankName();
            goldPb.setProgress(0);
        }
        if (account != null) {
            tvRankName.setText(account.rankName);
            int rankID = account.rankId;
            KhUserRank.RewardRank myRank = KhUserRank.RewardRank.getById(rankID);
            if (myRank != null) {
                icAccountRank.setImageDrawable(myRank.drawable);
                if (rankID == KhUserRank.RewardRank.PLATINUM.id) {
                    icNextRank.setVisibility(View.GONE);
                } else {
                    KhUserRank.RewardRank nextRank = KhUserRank.RewardRank.getById(rankID + 1);
                    if (nextRank != null)
                        icNextRank.setImageDrawable(nextRank.drawable);
                }
            }


        }
        getPointNextRank();
        wsGetAccountPointInfo();
    }

    /**
     * get info list point of acc
     */
    private void wsGetAccountPointInfo() {

        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountPointInfo(new KhApiCallback<KHBaseResponse<AccountPointRankResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountPointRankResponse> body) {
                logH("wsGetAccountPointInfo > onSuccess");
                subPoint(body.getData().getListPoint());
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsGetAccountPointInfo > onFailed > " + "status: " + status + "message: " + message);
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountPointRankResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetAccountPointInfo > onFailure");
            }
        });
    }

    private void subPoint(List<KHAccountPoint> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        ArrayList<KHAccountPoint> pointsPast = new ArrayList<>();
        totalAccumulate = 0;
        totalAvailability = 0;

        listAvailability.clear();
        listAccumulate.clear();
        KHAccountPoint minExpired = list.get(0);
        for (KHAccountPoint item : list) {
            minExpired = item.getExpireDate() < minExpired.getExpireDate() ? item : minExpired;
            if (item.getPointType() == PointType.ACTIVE.id) {
                totalAccumulate = totalAccumulate + item.getPointValue();
                listAccumulate.add(item);
            } else if (item.getPointType() == PointType.PAST.id) {
                totalAvailability = totalAvailability + item.getPointValue();
                listAvailability.add(item);
                pointsPast.add(item);
            }
        }
        pointExpired.setText(subPointsExpired(pointsPast));
        // show percent up rank level
        if (accountRankDTO.rankId == KhUserRank.RewardRank.PLATINUM.id) {
            tvPercent.setText(ResourceUtils.getString(R.string.highest_rank));
            imgNextRank.setVisibility(View.GONE);
            goldPb.setProgress(100);
        } else if (nextRank != null && nextRank.getMinPoint() >= 0) {
            int minPoint = nextRank.getMinPoint();
            int percent = (int) (totalAccumulate * 100 / minPoint);
            tvPercent.setText(String.format(tvPercent.getContext().getResources().getString(R.string.percent_gold), String.valueOf(100 - percent), nextRank.getRankName()));
            goldPb.setProgress(percent);
        }
        pointValue.setText(formatPoint(totalAvailability));
        if (totalAvailability > 1) {
            tvPoint.setText(getString(R.string.points));
        } else {
            tvPoint.setText(getString(R.string.point));
        }
    }

    public String subPointsExpired(ArrayList<KHAccountPoint> points) {
        try {
            int totalPoint = 0;
            AtomicBoolean sortResult = new AtomicBoolean(true);
            SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DateConvert.DATE_REWARD_INPUT);
            SimpleDateFormat dayFormat = new SimpleDateFormat(DateConvert.DATE_REWARD_OUTPUT_EXPIRED);
            // sap xep diem theo ngay het han tang dan
            Collections.sort(points, (point1, point2) -> {
                try {
                    Date date1 = dateTimeFormat.parse(point1.getPointExpireDate());
                    Date date2 = dateTimeFormat.parse(point2.getPointExpireDate());
                    return date1.before(date2) ? -1 : 1;
                } catch (ParseException e) {
                    e.printStackTrace();
                    sortResult.set(false);
                }
                return 0;
            });
            // Exception khi sort do sai dinh dang
            if (!sortResult.get()) {
                return "";
            }
            Date currentDate = new Date();
            Date minDate = null;
            // tim ngay het han gan nhat tinh tu ngay hien tai
            for (KHAccountPoint point : points) {
                Date date = dateTimeFormat.parse(point.getPointExpireDate());
                if (date.after(currentDate)) {
                    minDate = date;
                    break;
                }
            }
            // tinh tong diem co ngay het han la ngay gan nhat
            for (KHAccountPoint point : points) {
                Date date = dateTimeFormat.parse(point.getPointExpireDate());
                if (dayFormat.format(minDate).equals(dayFormat.format(date))) {
                    totalPoint += point.getPointValue();
                }
            }
            SimpleDateFormat resultFormat = new SimpleDateFormat(DateConvert.DATE_REWARD_OUTPUT_EXPIRED);
            if (LocaleManager.getLanguage(getContext()).equals(Constant.LANGUAGE_KM)) {
                resultFormat = new SimpleDateFormat(DateConvert.DATE_REWARD_OUTPUT_EXPIRED_KM);
            }
            return String.format(getActivity().getResources().getString(R.string.points_expired)
                    , formatPoint(totalPoint), resultFormat.format(minDate));
        } catch (Exception e) {
            Log.d(TAG, "subPointsExpired error : " + e.toString());
        }
        return "";
    }

    private void getPointNextRank() {
        if (accountRankDTO == null) {
            return;
        }
        for (int i = 0; i < listRank.size(); i++) {
            RankDefine item = listRank.get(i);
            if (listRank.get(i).getRankId() == accountRankDTO.rankId) {
                if (item.getRankId() == KhUserRank.RewardRank.PLATINUM.id) {
                    nextRank = item;
                } else if (i + 1 < listRank.size()) {
                    nextRank = listRank.get(i + 1);
                }
            }
        }
    }

    public void addExpired(List<RedeemItem> list) {
        if (vpReward != null && rewardPageAdapter != null) {
            GiftPastKHFragment giftReceivedKHFragment = (GiftPastKHFragment) rewardPageAdapter.getItem(1);
            giftReceivedKHFragment.addExpired(list);
        }
    }

    @OnClick(R.id.reward_point_layout)
    void showHistoryPoint() {
        replaceFragment(R.id.reward_container, HistoryPointContainerFragment.newInstance(false), HistoryPointContainerFragment.TAG);
    }

    @OnClick(R.id.icBackToolbar)
    void onBack() {
        if (getActivity() instanceof RewardCamIdActivity) {
            getActivity().finish();
        } else {
            popBackStackFragment();
        }
    }


    @OnClick(R.id.ic_account_rank)
    void showBenefit() {
        showBenefit(accountRankDTO == null ? 0 : accountRankDTO.rankId);
    }

    @OnClick(R.id.ic_next_rank)
    void showBenefitNext() {
        showBenefit(nextRank == null ? 0 : nextRank.getRankId());
    }

    private void showBenefit(int rankId) {
        if (listRank == null) {
            return;
        }
        // 01-14-22 Fix crash bug when server does not response the data in AccountRankDTO
        if (accountRankDTO != null) {
            replaceFragment(R.id.reward_container, BenefitsKHFragment.newInstance(rankId, accountRankDTO.rankId), BenefitsKHFragment.TAG);
        }
    }

    public void showDetailActive(RedeemItem item) {
        replaceFragment(R.id.reward_container,
                RewardsDetailKHFragment.newInstance(RewardsDetailKHFragment.FROM_ACTIVE, item),
                RewardsDetailKHFragment.TAG);
    }
}
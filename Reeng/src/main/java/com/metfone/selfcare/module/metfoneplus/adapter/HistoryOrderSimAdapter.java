package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.module.metfoneplus.holder.StoreViewHolder;
import com.metfone.selfcare.module.metfoneplus.search.adapter.AvailableNumberAdapter;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.metfoneplus.response.WsLstOrderedNumberResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.List;

public class HistoryOrderSimAdapter extends RecyclerView.Adapter<HistoryOrderSimAdapter.HistoryOrderSimViewHolder> {

    private Context mContext;
    private List<WsLstOrderedNumberResponse> mtOrderedNumberList;
    private HistoryOrderSimAdapter.HistoryOrderSimClickListener mHistoryOrderSimClickListener;
    public HistoryOrderSimAdapter(Context context, List<WsLstOrderedNumberResponse> LstOrderedNumber, HistoryOrderSimAdapter.HistoryOrderSimClickListener onHistoryOrderSimClickListener) {
        this.mContext = context;
        this.mtOrderedNumberList = LstOrderedNumber;
        this.mHistoryOrderSimClickListener = onHistoryOrderSimClickListener;
    }

    private void setList(List<WsLstOrderedNumberResponse> LstOrderedNumber) {
        this.mtOrderedNumberList = LstOrderedNumber;
    }

    public void replaceData(List<WsLstOrderedNumberResponse> storeList) {
        setList(storeList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HistoryOrderSimViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history_order_sim, parent, false);
        return new HistoryOrderSimViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryOrderSimViewHolder holder, int position) {
        WsLstOrderedNumberResponse wsLstOrderedNumberResponse = mtOrderedNumberList.get(position);

        holder.mSdt.setText(Utilities.formatPhoneNumberCambodia(wsLstOrderedNumberResponse.getIsdn()));
        holder.mOrderedDate.setText(wsLstOrderedNumberResponse.getOrderedDate());
        holder.mExpiredDate.setText(wsLstOrderedNumberResponse.getExpiredDate());
        holder.wsLstOrderedNumberResponse = wsLstOrderedNumberResponse;
        holder.itemView.setOnClickListener(v ->{
            mHistoryOrderSimClickListener.registerNumber(wsLstOrderedNumberResponse);
        });
    }

    @Override
    public int getItemCount() {
        if (mtOrderedNumberList == null) {
            return 0;
        }
        return mtOrderedNumberList.size();
    }

    public class HistoryOrderSimViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView mSdt;
        public AppCompatTextView mOrderedDate;
        public AppCompatTextView mExpiredDate;
        public WsLstOrderedNumberResponse wsLstOrderedNumberResponse;

        public HistoryOrderSimViewHolder(View view) {
            super(view);
            mSdt = view.findViewById(R.id.item_sdt);
            mOrderedDate = view.findViewById(R.id.ordered_date);
            mExpiredDate = view.findViewById(R.id.expired_date);
        }
    }
    public interface HistoryOrderSimClickListener{
        void registerNumber(WsLstOrderedNumberResponse availableNumber);
    }

}
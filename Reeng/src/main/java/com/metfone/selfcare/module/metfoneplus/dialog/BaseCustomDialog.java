package com.metfone.selfcare.module.metfoneplus.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.airbnb.lottie.LottieAnimationView;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;

import static com.metfone.selfcare.business.UserInfoBusiness.fastblur;
import static com.metfone.selfcare.business.UserInfoBusiness.takeScreenShot;
import static com.metfone.selfcare.module.home_kh.util.ResourceUtils.getResources;

public class BaseCustomDialog extends Dialog {
    private Activity activity;
    protected AppCompatImageView mImage;
    protected AppCompatTextView mTitle;
    protected AppCompatTextView mContent;
    protected RelativeLayout mButtonTopLayout;
    protected View mButtonTopBackground;
    protected AppCompatButton mButtonTop;
    protected RelativeLayout mButtonMiddleLayout;
    protected View mButtonMiddleBackground;
    protected AppCompatButton mButtonMiddle;
    protected RelativeLayout mButtonBottomLayout;
    protected View mButtonBottomBackground;
    protected AppCompatButton mButtonBottom;
    protected RelativeLayout rlContainer;
    protected LinearLayout lnlContent;
    protected FrameLayout flImage;
    protected ProgressBar progressBar;
    protected LottieAnimationView lavAnimations;

    public BaseCustomDialog(@NonNull Activity activity) {
        super(activity, R.style.DialogSuccessFullScreen);
//        super(activity);
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    public BaseCustomDialog(@NonNull Context context) {
        super(context);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            getWindow().setDimAmount(0.7f);
//            Bitmap map=takeScreenShot(getActivity());
//            Bitmap fast=fastblur(map, 10);
//            final Drawable draw=new BitmapDrawable(getResources(),fast);
//            getWindow().setBackgroundDrawable(draw);
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            getWindow().setGravity(Gravity.CENTER);
            getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.transparent)));
        }
        setContentView(R.layout.dialog_mp_hero);
        mImage = findViewById(R.id.dialog_hero_img);
        mTitle = findViewById(R.id.dialog_hero_title);
        mContent = findViewById(R.id.dialog_hero_content);

        mButtonTopLayout = findViewById(R.id.dialog_hero_top_layout_button);
        mButtonTopBackground = findViewById(R.id.dialog_hero_top_background_button);
        mButtonTop = findViewById(R.id.dialog_hero_top_title_button);

        mButtonMiddleLayout = findViewById(R.id.dialog_hero_middle_layout_button);
        mButtonMiddleBackground = findViewById(R.id.dialog_hero_middle_background_button);
        mButtonMiddle = findViewById(R.id.dialog_hero_middle_title_button);

        mButtonBottomLayout = findViewById(R.id.dialog_hero_bottom_layout_button);
        mButtonBottomBackground = findViewById(R.id.dialog_hero_bottom_background_button);
        mButtonBottom = findViewById(R.id.dialog_hero_bottom_title_button);

        rlContainer = findViewById(R.id.rlContainer);
        lnlContent = findViewById(R.id.lnlContent);
        flImage = findViewById(R.id.flImage);
        lavAnimations = findViewById(R.id.lavAnimations);
        progressBar = findViewById(R.id.progress);
        setupViewDefault();
    }

    private void setupViewDefault() {
        mImage.setVisibility(View.GONE);
        mButtonTopBackground.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_red_button_corner_6));
        mButtonBottomBackground.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_transparent_button_corner_6));
        mButtonTopLayout.setVisibility(View.GONE);
        mButtonMiddleLayout.setVisibility(View.GONE);
        mButtonBottomBackground.setVisibility(View.GONE);
    }

    public void setTopButton(int idStringRes, int idDrawableRes, View.OnClickListener onClickListener) {
        mButtonTop.setText(idStringRes);
        mButtonTopBackground.setBackground(ContextCompat.getDrawable(getActivity(), idDrawableRes));
        mButtonTopLayout.setVisibility(View.VISIBLE);
        mButtonTop.setOnClickListener(onClickListener);
    }

    public void setMiddleButton(int idStringRes, int idDrawableRes, View.OnClickListener onClickListener) {
        mButtonMiddle.setText(idStringRes);
        mButtonMiddleBackground.setBackground(ContextCompat.getDrawable(getActivity(), idDrawableRes));
        mButtonMiddleLayout.setVisibility(View.VISIBLE);
        mButtonMiddle.setOnClickListener(onClickListener);
    }

    public void setBottomButton(int idStringRes, int idDrawableRes, View.OnClickListener onClickListener) {
        mButtonBottom.setText(idStringRes);
        mButtonBottomBackground.setBackground(ContextCompat.getDrawable(getActivity(), idDrawableRes));
        mButtonBottomLayout.setVisibility(View.VISIBLE);
        mButtonBottom.setOnClickListener(onClickListener);
    }

    public void setButtonTopVisibility(int visibility) {
        mButtonTopLayout.setVisibility(visibility);
    }

    public void setButtonMiddleVisibility(int visibility) {
        mButtonMiddleLayout.setVisibility(visibility);
    }

    public void setButtonBottomVisibility(int visibility) {
        mButtonBottomLayout.setVisibility(visibility);
    }
}

package com.metfone.selfcare.module.metfoneplus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogOtpFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPOrderNumberDialog;
import com.metfone.selfcare.module.metfoneplus.search.fragment.BuyPhoneNumberFragment;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpResponse;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.Utilities;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.SneakyThrows;
import retrofit2.Response;

public class  MPApplyingSimFragment extends MPBaseFragment {
    private static final String NUMBER_KEY = "number";
    private static final String OTP = "otp";
    private static final String ORDER_TYPE = "order_type";
    private static final String DURATION_KEY = "duration";
    @BindView(R.id.txtName)
    CamIdTextView txtName;
    @BindView(R.id.idNumber)
    CamIdTextView idNumber;
    @BindView(R.id.DOB)
    CamIdTextView DOB;
    @BindView(R.id.package_name)
    CamIdTextView packageName;
    @BindView(R.id.replace_to_number)
    CamIdTextView replaceToNumber;
    @BindView(R.id.u_Amount)
    CamIdTextView uAmount;
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.duration)
    LinearLayout duration;
    @BindView(R.id.txtDuration)
    CamIdTextView txtDuration;
    @BindView(R.id.month)
    CamIdTextView month;
    @BindView(R.id.btn_yes)
    AppCompatButton btn_yes;
    private BuyPhoneNumberFragment buyPhoneNumberFragment;
    private AvailableNumber availableNumber;
    UserInfoBusiness userInfoBusiness;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private String otp;
    private Integer orderType;
    private Integer countDuration;
    public MPApplyingSimFragment() {
        // Required empty public constructor
    }
    public static MPApplyingSimFragment newInstance(AvailableNumber availableNumber, String otp, int orderType, int duration) {
        Bundle args = new Bundle();
        MPApplyingSimFragment fragment = new MPApplyingSimFragment();
        args.putSerializable(NUMBER_KEY, availableNumber);
        args.putString(OTP, otp);
        args.putInt(ORDER_TYPE, orderType);
        args.putInt(DURATION_KEY, duration);

        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            availableNumber = (AvailableNumber) getArguments().getSerializable(NUMBER_KEY);
            otp = (String) getArguments().getString(OTP);
            orderType = (Integer) getArguments().getInt(ORDER_TYPE);
            countDuration = (Integer) getArguments().getInt(DURATION_KEY);
        }
    }

    @SneakyThrows
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_applying_sim, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        Utilities.adaptViewForInserts(view);
        userInfoBusiness = new UserInfoBusiness(getActivity());
        txtName.setText(userInfoBusiness.getUser().getFull_name());
        idNumber.setText(userInfoBusiness.getUser().getIdentity_number());

        DateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        if (userInfoBusiness.getUser().getDate_of_birth() != null && !userInfoBusiness.getUser().getDate_of_birth().equals("")) {
            String inputDateStr = userInfoBusiness.getUser().getDate_of_birth();
            Date date1 = inputFormat.parse(inputDateStr);
            String outputDateStr = outputFormat.format(date1);
            DOB.setText(outputDateStr);
        } else {
            DOB.setText("");
        }
        replaceToNumber.setText(Utilities.formatPhoneNumberCambodia(availableNumber.getIsdn()));
        packageName.setText("KADO80");

        switch(orderType) {
            case 0:
                action_bar_title.setText(R.string.applying);
                btn_yes.setText(R.string.yes);
                uAmount.setText("$" + availableNumber.getPrice());
                break;
            case 1:
                action_bar_title.setText(R.string.applying_for_full_price);
                btn_yes.setText(R.string.confirm);
                uAmount.setText("$" + availableNumber.getPrice());
                break;
            case 2:
                action_bar_title.setText(R.string.applying_for_commitment);
                duration.setVisibility(View.VISIBLE);
                month.setVisibility(View.VISIBLE);
                uAmount.setText("$" + availableNumber.getMonthlyFee());
                txtDuration.setText(countDuration + " " + getString(R.string.months));
                btn_yes.setText(R.string.confirm);
                break;
            default:
                break;
        }
        if (availableNumber.getType() == 10 && orderType == 1) {
            month.setVisibility(View.GONE);
        }
        return view;
    }
    @OnClick({R.id.action_bar_back, R.id.btnCancel})
    public void onBack() {
        popBackStackFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_applying_sim;
    }

    @OnClick({R.id.btn_yes})
    public void onYes() {
        if (orderType > 0) {
            actionOrderTypeYes();
        } else {
            actionYes();
        }

    }

    public void actionOrderTypeYes() {
        new MetfonePlusClient().wsChangeIsdnOrderSim(userInfoBusiness.getUser().getPhone_number(), otp, availableNumber.getPrice(), availableNumber.getIsdn(), String.valueOf(orderType), availableNumber.getMonthlyFee(), new MPApiCallback<WsCheckOtpResponse>() {
            @Override
            public void onResponse(Response<WsCheckOtpResponse> response) {
                if(response.body() != null && response.body().getResult() != null && response.body().getErrorCode().equals("")){
                    String top_content = getString(R.string.error_six, String.valueOf(availableNumber.getPrice()));
                    String bottom_content = "";
                    String title = getString(R.string.m_p_dialog_service_register_title_unsuccessful);
                    String topButton = getString(R.string.btn_top_up);
                    String bottomButton = getString(R.string.skip);
                    MPOrderNumberDialog dialog = new MPOrderNumberDialog.Builder(getContext(), new MPOrderNumberDialog.OrderDialogListener() {
                        @Override
                        public void confirm() {
                            gotoMPTopUpFragment();
                        }
                        @Override
                        public void cancel() {

                        }
                    }, top_content, bottom_content, title, topButton, bottomButton, "6").build();
                    dialog.show(getChildFragmentManager(), "");
                } else {
                    if (response.body() != null && response.body().getResult() != null && response.body().getResult().getErrorCode().equals("0")) {
                        ApplicationController.self().getFirebaseEventBusiness().logOrderCurrentNumber(true, userInfoBusiness.getUser().getPhone_number(), availableNumber.getIsdn(), String.valueOf(orderType), "0");
                        gotoBuyPhoneNumberFragment(true);
                    } else {
                        ApplicationController.self().getFirebaseEventBusiness().logOrderCurrentNumber(false, userInfoBusiness.getUser().getPhone_number(), availableNumber.getIsdn(), String.valueOf(orderType), response.body().getResult().getErrorCode());
                        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getResult().getUserMsg(), false);
                        baseMPSuccessDialog.show();
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), error.getMessage(), false);
                baseMPSuccessDialog.show();
            }
        });
    }

    public void actionYes() {
        new MetfonePlusClient().wsChangeIsdnKeepSim(userInfoBusiness.getUser().getPhone_number(), otp, availableNumber.getPrice(), availableNumber.getIsdn(), String.valueOf(orderType) , new MPApiCallback<WsCheckOtpResponse>() {
            @Override
            public void onResponse(Response<WsCheckOtpResponse> response) {
                if (response.body() != null && response.body().getResult() != null && response.body().getResult().getErrorCode().equals("0")) {
                    gotoBuyPhoneNumberFragment(true);
                } else {
                    baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getResult().getUserMsg(), false);
                    baseMPSuccessDialog.show();
                }
            }

            @Override
            public void onError(Throwable error) {
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), error.getMessage(), false);
                baseMPSuccessDialog.show();
            }
        });
    }

    @OnClick({R.id.btnCancel})
    public void onCancel() {
        gotoBuyPhoneNumberFragment(false);
    }

    public interface ButtonOnClickListener {
        public void onShowSuccess();
    }


}

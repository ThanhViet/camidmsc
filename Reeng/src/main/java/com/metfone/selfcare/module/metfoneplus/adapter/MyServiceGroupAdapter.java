package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceGroupViewHolder;

import java.util.List;

public class MyServiceGroupAdapter extends RecyclerView.Adapter<BuyServiceGroupViewHolder> {
    private Context mContext;
    private List<ServiceGroup> mServiceGroupList;

    private OnMyServiceListener listener;

    public MyServiceGroupAdapter(Context context, List<ServiceGroup> serviceGroupList) {
        this.mContext = context;
        this.mServiceGroupList = serviceGroupList;
    }

    public void replaceServiceGroupData(List<ServiceGroup> serviceGroupList) {
        this.mServiceGroupList = serviceGroupList;
        notifyDataSetChanged();
    }

    public void setOnMyServiceListener(OnMyServiceListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public BuyServiceGroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mp_item_buy_services_group, parent, false);
        return new BuyServiceGroupViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyServiceGroupViewHolder holder, int position) {
        ServiceGroup item = mServiceGroupList.get(position);
        holder.mGroupName.setText(item.getName());
        holder.mGroupDescription.setText(item.getShortDes());
        holder.mGroupValid.setText(item.getValidity());
        holder.mServiceGroup = item;

        holder.mLayoutGroupStateAutoRenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onAutoRenewButtonClick(item.getCode());
                }
            }
        });

        if ("1".equals(item.getAutoRenew())) {
            holder.mLayoutGroupState.setVisibility(View.VISIBLE);
            holder.mLayoutGroupStateAutoRenew.setVisibility(View.VISIBLE);
        } else {
            holder.mLayoutGroupState.setVisibility(View.GONE);
            holder.mLayoutGroupStateAutoRenew.setVisibility(View.GONE);
        }

        if (position == 0) {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_blue_corner_6);
        } else if (position % 4 == 0) {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_blue_corner_6);
        } else if (position % 2 == 0 && position % 4 != 0) {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_yellow_corner_6);
        } else if (position % 3 == 0) {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_dark_blue_corner_6);
        } else {
            holder.mLayoutGroup.setBackgroundResource(R.drawable.bg_gradient_violet_corner_6);
        }
        holder.itemView.setTag(holder);
    }

    @Override
    public int getItemCount() {
        if (mServiceGroupList != null) {
            return mServiceGroupList.size();
        }
        return 0;
    }

    public interface OnMyServiceListener {
        void onAutoRenewButtonClick(String serviceCode);
    }
}
package com.metfone.selfcare.module.tiin.base;

public interface MvpPresenter<V extends MvpView> {
    void onAttach(V view);
    void onDetach();
    void handleApiError(Exception error);

    void setUserAsLoggedOut();
}
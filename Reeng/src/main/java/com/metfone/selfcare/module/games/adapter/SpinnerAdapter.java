package com.metfone.selfcare.module.games.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

import java.util.ArrayList;

public class SpinnerAdapter extends RecyclerView.Adapter<SpinnerAdapter.SpinnerHolder> {

    private final ArrayList<CustomItem> listItem;
    private final Context context;
    private OnItemClick callback;
    private int selectedPos = 0;

    public SpinnerAdapter(ArrayList<CustomItem> listItem, Context context) {
        this.listItem = listItem;
        this.context = context;
    }

    public void setOnItemClick(OnItemClick event) {
        callback = event;
    }

    @NonNull
    @Override
    public SpinnerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_dropdown_layout, parent, false);
        return new SpinnerHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SpinnerHolder holder, int position) {
        CustomItem item = listItem.get(position);
        holder.ivCheck.setImageResource(item.getSpinnerItemImage());
        holder.tvRangeDay.setText(item.getSpinnerItemName());

        if (selectedPos == position) {
            holder.ivCheck.setImageResource(R.drawable.ic_check_box);
            callback.onItemClick(holder.tvRangeDay.getText().toString());
        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public class SpinnerHolder extends RecyclerView.ViewHolder {
        private final ImageView ivCheck;
        private final TextView tvRangeDay;

        public SpinnerHolder(@NonNull View itemView) {
            super(itemView);
            ivCheck = itemView.findViewById(R.id.ivDropDownLayout);
            tvRangeDay = itemView.findViewById(R.id.tvDropDownLayout);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPos = getLayoutPosition();
                    notifyDataSetChanged();
                }
            });
        }

    }

    public interface OnItemClick {
        void onItemClick(String data);
    }
}

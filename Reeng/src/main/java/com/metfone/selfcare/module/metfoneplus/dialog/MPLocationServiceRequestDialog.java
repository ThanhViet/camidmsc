package com.metfone.selfcare.module.metfoneplus.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;

public class MPLocationServiceRequestDialog extends MPHeroDialog {
    public static final String TAG = MPLocationServiceRequestDialog.class.getSimpleName();

    public static MPLocationServiceRequestDialog newInstance() {
        MPLocationServiceRequestDialog dialog = new MPLocationServiceRequestDialog();
        Bundle bundle = new Bundle();
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mImage.setVisibility(View.GONE);
        mTitle.setText(R.string.m_p_dialog_location_service_request_title);
        mContent.setText(R.string.m_p_dialog_location_service_request_content);

        setTopButton(R.string.m_p_dialog_location_service_request_top_button, R.drawable.bg_red_button_corner_6, buttonTop -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            dismissAllowingStateLoss();
        });

        setBottomButton(R.string.m_p_dialog_location_service_request_bottom_button, R.drawable.bg_transparent_button_corner_6, buttonBottom -> {
            dismissAllowingStateLoss();
        });
    }
}

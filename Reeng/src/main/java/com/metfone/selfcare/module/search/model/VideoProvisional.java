/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class VideoProvisional extends Provisional {
    private ArrayList<Video> videos;
    private ArrayList<Channel> channels;

    public VideoProvisional() {
    }

    public VideoProvisional(ArrayList<Video> data) {
        this.videos = data;
    }

    public ArrayList<Channel> getChannels() {
        return channels;
    }

    public void setChannels(ArrayList<Channel> channels) {
        this.channels = channels;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }

    @Override
    public int getSize() {
        int size = 0;
        if (Utilities.notEmpty(videos)) size++;
        if (Utilities.notEmpty(channels)) size++;
        return size;
    }
}

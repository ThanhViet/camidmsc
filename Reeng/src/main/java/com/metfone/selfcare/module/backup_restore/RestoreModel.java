package com.metfone.selfcare.module.backup_restore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class RestoreModel {
    @SerializedName("code")
    @Expose
    String code;

    @SerializedName("desc")
    @Expose
    String desc;

    @SerializedName("lstFile")
    @Expose
    ArrayList<FileInfo> lstFile = new ArrayList();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<FileInfo> getLstFile() {
        return lstFile;
    }

    public void setLstFile(ArrayList<FileInfo> lstFile) {
        this.lstFile = lstFile;
    }

    public static class FileInfo {
        @SerializedName("id")
        @Expose
        String id;

        @SerializedName("username")
        @Expose
        String username;

        @SerializedName("path")
        @Expose
        String path;

        @SerializedName("capacity")
        @Expose
        long capacity;

        @SerializedName("created_date")
        @Expose
        long created_date;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public long getCapacity() {
            return capacity;
        }

        public void setCapacity(long capacity) {
            this.capacity = capacity;
        }

        public long getCreated_date() {
            return created_date;
        }

        public void setCreated_date(long created_date) {
            this.created_date = created_date;
        }
    }
}

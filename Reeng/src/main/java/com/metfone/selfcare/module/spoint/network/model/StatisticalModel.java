package com.metfone.selfcare.module.spoint.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StatisticalModel implements Serializable {
    @SerializedName("spoint")
    @Expose
    private int spoint;
    @SerializedName("spoint_promotion")
    @Expose
    private int spointPromotion;
    @SerializedName("list")
    @Expose
    private List<StatisticalItemModel> list = null;

    public int getSpoint() {
        return spoint;
    }

    public void setSpoint(int spoint) {
        this.spoint = spoint;
    }

    public int getSpointPromotion() {
        return spointPromotion;
    }

    public void setSpointPromotion(int spointPromotion) {
        this.spointPromotion = spointPromotion;
    }

    public List<StatisticalItemModel> getList() {
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }

    public void setList(List<StatisticalItemModel> list) {
        this.list = list;
    }

    public String getTotalSpointString() {
        return String.valueOf(spoint + spointPromotion);
    }

    public long getTotalSpointInt(){
        return spoint + spointPromotion;
    }
}

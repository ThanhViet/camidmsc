package com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.tiin.base.BasePresenter;
import com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.fragment.ChildTiinDetailFragment;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.TiinContentResponse;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.module.tiin.network.request.TiinRequest;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.viettel.util.LogDebugHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ChildTiinDetailPresenter extends BasePresenter implements IChildTiinDetailMvpPresenter {
    TiinApi mTiinApi;
    String urlDetailTiin = "";
    long startTimeDetail;
    long startTimeRelate;
    long startTimeSibling;
    long startTimeRelateEvent;
    HttpCallBack relateCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                return;
            }
            Gson gson = new Gson();
            List<TiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<List<TiinModel>>() {
            }.getType());

            ((ChildTiinDetailFragment) getMvpView()).bindDataRelate(response);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_RELATE, (endTime - startTimeRelate) + "", startTimeRelate + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                return;
            }
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_RELATE, (endTime - startTimeRelate) + "", startTimeRelate + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
    HttpCallBack siblingCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                return;
            }
            Gson gson = new Gson();
            List<TiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<List<TiinModel>>() {
            }.getType());

            ((ChildTiinDetailFragment) getMvpView()).binDataSibling(response);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_SIBLING, (endTime - startTimeSibling) + "", startTimeSibling + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                return;
            }
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_SIBLING, (endTime - startTimeSibling) + "", startTimeSibling + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
    private HttpCallBack detailCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_DETAIL, (endTime - startTimeDetail) + "", startTimeDetail + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            LogDebugHelper.getInstance().logDebugContent("Tiin detail :" + data + " | " + urlDetailTiin);

//            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
//                return;
//            }
            Gson gson = new Gson();
            TiinContentResponse response = gson.fromJson(data, TiinContentResponse.class);
            ((ChildTiinDetailFragment) getMvpView()).bindData(response.getData());
            ((ChildTiinDetailFragment) getMvpView()).loadDataSuccess(true);
            if(response.isError() || response.getData() == null || response.getData().getBody().size() == 0){
                LogDebugHelper.getInstance().logDebugContent("Tiin detail error:" + data + " | " + urlDetailTiin);
                ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_TIIN_DETAIL, "Tiin detail error:" + data + " | " + urlDetailTiin);
            }
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            Log.d(TAG, "loadData: onFailure - " + message);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_DETAIL, (endTime - startTimeDetail) + "", startTimeDetail + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            LogDebugHelper.getInstance().logDebugContent("Tiin detail error:" + message + " | " + urlDetailTiin);
            ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_TIIN_DETAIL, "Tiin detail error:" + message + " | " + urlDetailTiin);
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                return;
            }
            ((ChildTiinDetailFragment) getMvpView()).loadDataSuccess(false);
            ((ChildTiinDetailFragment) getMvpView()).bindData(null);

        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    HttpCallBack relateEventCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                return;
            }
            Gson gson = new Gson();
//            List<TiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<List<TiinModel>>() {
//            }.getType());
            TiinEventModel response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<TiinEventModel>() {
            }.getType());

            ((ChildTiinDetailFragment) getMvpView()).bindDataRelate(response.getData());
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_RELATE, (endTime - startTimeRelateEvent) + "", startTimeRelateEvent + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                return;
            }
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_RELATE, (endTime - startTimeRelateEvent) + "", startTimeRelateEvent + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    public ChildTiinDetailPresenter() {
        mTiinApi = new TiinApi(ApplicationController.self());
    }

    @Override
    public void getDetailNew(int id, String tag) {
        startTimeDetail = System.currentTimeMillis();
        urlDetailTiin = mTiinApi.getApiUrl(TiinApi.GET_DETAIL_NEW+id);
        mTiinApi.getNewDetail(detailCallback, new TiinRequest(id), tag);
    }

    @Override
    public void getRelateNew(int id) {
        startTimeRelate = System.currentTimeMillis();
        mTiinApi.getNewRelate(relateCallback, new TiinRequest(id));
    }

    @Override
    public void getSiblingNew(int id, int page, int num) {
        startTimeSibling = System.currentTimeMillis();
        mTiinApi.getNewSibling(siblingCallback, new TiinRequest(page, num, id));
    }

    @Override
    public void getNewStatus(String url) {
        mTiinApi.getDetailUrl(url, false, new ApiCallbackV2<RestAllFeedsModel>() {
            @Override
            public void onSuccess(String msg, RestAllFeedsModel result) throws JSONException {
                if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                    return;
                }
                if (result != null && Utilities.notEmpty(result.getData())) {
                    ((ChildTiinDetailFragment) getMvpView()).loadDataFeed(result.getData().get(0));
                }
                ((ChildTiinDetailFragment) getMvpView()).loadDataStatus(true);
            }

            @Override
            public void onError(String s) {
                if (!isViewAttached() || !(getMvpView() instanceof ChildTiinDetailFragment)) {
                    return;
                }
                ((ChildTiinDetailFragment) getMvpView()).loadDataStatus(false);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void getRelateEventNew(int id, int page, int num) {
        startTimeRelateEvent = System.currentTimeMillis();
        TiinRequest tiinRequest = new TiinRequest();
        tiinRequest.setId(id);
        tiinRequest.setPage(page);
        tiinRequest.setNum(num);
        mTiinApi.getNewRelateEvent(relateEventCallback, tiinRequest);
    }
}

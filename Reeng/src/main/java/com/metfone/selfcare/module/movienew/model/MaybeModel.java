package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.tabMovie.Movie2;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MaybeModel {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("result")
    @Expose
    private List<Movie2> result = null;
}

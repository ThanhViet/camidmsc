package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import lombok.Data;

@Data
public class WsGetGiftResponse extends BaseResponse<WsGetGiftResponse.Response> {
    @Data
    public class Response {
        @SerializedName("isGetGift")
        private boolean isGetGift;
        @SerializedName("errorCode")
        private String errorCode;
        @SerializedName("messageCode")
        private String messageCode;
    }
}


/*
package com.metfone.selfcare.module.selfcare.fragment.account;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.network.SCAccountCallback;
import com.metfone.selfcare.module.selfcare.network.SelfCareAccountApi;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

*/
/**
 * Created by thanhnt72 on 2/15/2019.
 *//*


public class SCAccountResetPassFragment extends SCBaseFragment {

    EditText etOtp;
    EditText etPass;
    EditText etRePass;
    @BindView(R.id.btnResetPass)
    RoundTextView btnResetPass;
    Unbinder unbinder;
    @BindView(R.id.tilOtp)
    TextInputLayout tilOtp;
    @BindView(R.id.tilNewPass)
    TextInputLayout tilNewPass;
    @BindView(R.id.tilRePass)
    TextInputLayout tilRePass;

    public static SCAccountResetPassFragment newInstance() {
        SCAccountResetPassFragment fragment = new SCAccountResetPassFragment();
        return fragment;
    }

    @Override
    public String getName() {
        return SCAccountResetPassFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_account_reset_pass;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        initEditText();
        setActionBar(view);
        drawDetailView();

        return view;
    }

    private void initEditText() {
        etOtp = tilOtp.getEditText();
        etPass = tilNewPass.getEditText();
        etRePass = tilRePass.getEditText();

        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilOtp.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilNewPass.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etRePass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilRePass.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void drawDetailView() {

    }

    private void setActionBar(View view) {
        LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        Toolbar abView = view.findViewById(R.id.tool_bar_fragment);
        abView.removeAllViews();
        abView.addView(mLayoutInflater.inflate(
                R.layout.ab_detail, null), new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        (abView.findViewById(R.id.ab_more_btn)).setVisibility(View.GONE);
        ImageView mImBack = abView.findViewById(R.id.ab_back_btn);
        mImBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        EllipsisTextView mTvwTitle = abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mActivity.getString(R.string.sc_create_new_pass));

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnResetPass)
    public void onViewClicked() {
        if (checkValidData()) {
            String otp = etOtp.getText().toString().trim();
            String pass = etPass.getText().toString().trim();
            mActivity.showLoadingSelfCare("", R.string.loading);
            SelfCareAccountApi.getInstant(mApp).changePassword(otp, pass, new SCAccountCallback.SCAccountApiListener() {
                @Override
                public void onSuccess(String response) {
                    mActivity.hideLoadingDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int code = jsonObject.optInt("errorCode", -1);
                        if (code == 0) {
                            doChangePassSuccess();
                        } */
/*else if (code == 1) {
                            mActivity.showToast("Sai số điện thoại");
                        } *//*
 else if (code == 2) {
                            mActivity.showToast(R.string.sc_wrong_otp);
                        } else
                            mActivity.showToast(R.string.e601_error_but_undefined);
                    } catch (JSONException e) {
                        Log.e(TAG, "JSONException", e);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }

                @Override
                public void onError(int code, String message) {
                    mActivity.hideLoadingDialog();
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            });
        }
    }

    private void doChangePassSuccess() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                */
/*Intent intent = new Intent(mApp, SCAccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(intent, false);
                mActivity.clearBackStack();
                mActivity.finish();*//*


                DeepLinkHelper.getInstance().openSchemaLink(mActivity, "mytel://home/selfcare?clearStack=1");
                mActivity.clearBackStack();
                mActivity.finish();

            }
        });
    }

    private boolean checkValidData() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etOtp.getText().toString().trim())) {
            isValid = false;
            tilOtp.setError(mActivity.getString(R.string.sc_input_text));
        }
        if (TextUtils.isEmpty(etPass.getText().toString().trim())) {
            isValid = false;
            etPass.setError(mActivity.getString(R.string.sc_input_text));
        }
        if (TextUtils.isEmpty(etRePass.getText().toString().trim())) {
            isValid = false;
            etRePass.setError(mActivity.getString(R.string.sc_input_text));
        }

        if (isValid) {
            String pass = etPass.getText().toString().trim();
            String repass = etRePass.getText().toString().trim();
            if (!pass.equals(repass)) {
                isValid = false;
                mActivity.showToast(R.string.sc_pass_doesnt_match);
            } else {
                if (pass.length() < 6) {
                    isValid = false;
                    mActivity.showToast(R.string.sc_valid_pass);
                }
            }
        }
        return isValid;
    }
}
*/

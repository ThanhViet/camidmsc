package com.metfone.selfcare.module.spoint.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.spoint.network.model.BuyPackageModel;
import com.metfone.selfcare.module.spoint.network.model.PackageModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BuyPackageResponse implements Serializable {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("data")
    @Expose
    private PackageModel list;

    public BuyPackageResponse() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public PackageModel getList() {
        return list;
    }

    public void setList(PackageModel list) {
        this.list = list;
    }
}

package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.holder.PlaylistNewHolder;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class HotPlaylistAdapter extends BaseAdapterRecyclerView {
    List<PlayListModel> datas;

    public HotPlaylistAdapter(Context context, List<PlayListModel> datas, String ga_source) {
        super(context, ga_source);
        this.datas = datas;
    }

    @Override
    public int getItemViewType(int position) {
        PlayListModel item = datas.get(position);
        if (item == null) {
            return ITEM_LOAD_MORE;
        }
        return ITEM_PLAYLIST;
    }

    @Override
    public int getItemCount() {
        if (datas != null)
            return datas.size();
        return 0;
    }

    @Override
    public PlayListModel getItem(int position) {
        if (datas != null) {
            try {
                return datas.get(position);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, final int position) {
        if (holder instanceof PlaylistNewHolder) {
            PlaylistNewHolder itemHolder = (PlaylistNewHolder) holder;
            final PlayListModel item = getItem(position);
            if (item != null) {
                itemHolder.mTvTitle.setText(item.getName());
                String str = item.getNameUser();
                if (TextUtils.isEmpty(str)) {
                    itemHolder.mTvCreated.setVisibility(View.GONE);
                } else {
                    itemHolder.mTvCreated.setVisibility(View.VISIBLE);
                    itemHolder.mTvCreated.setText(mContext.getString(R.string.create_by, str));
                }
                str = item.getSinger();
                if (TextUtils.isEmpty(str)) {
                    itemHolder.mTvContent.setVisibility(View.GONE);
                } else {
                    itemHolder.mTvContent.setVisibility(View.VISIBLE);
                    itemHolder.mTvContent.setText(str);
                }
                List<String> listImages = item.getListAvatar();
                ImageBusiness.setImagePlaylist(listImages, itemHolder.mTopLeftImage, itemHolder.mTopRightImage
                        , itemHolder.mBottomRightImage, itemHolder.mBottomLeftImage, itemHolder.mRightImageLayout);
                itemHolder.convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.onMediaClick(v, position);
                    }
                });
                itemHolder.viewOption.setVisibility(View.VISIBLE);
                itemHolder.mBtnOption.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.onMediaExpandClick(v, position);
                    }
                });
            }
        }
    }

}

package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

public class SmsChargeViewHolder extends RecyclerView.ViewHolder {
    public AppCompatTextView mPhoneNumber;
    public AppCompatTextView mDate;
    public AppCompatTextView mMoney;

    public SmsChargeViewHolder(@NonNull View itemView) {
        super(itemView);
        mPhoneNumber = itemView.findViewById(R.id.phone_number_sms_code_vas_charge);
        mDate = itemView.findViewById(R.id.date_sms_vas_charge);
        mMoney = itemView.findViewById(R.id.money_sms_vas_charge);
    }
}

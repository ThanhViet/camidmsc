package com.metfone.selfcare.module.movienew.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FavoriteBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.metfoneplus.topup.interfacelistener.RunUi;
import com.metfone.selfcare.module.movienew.adapter.CountryAdapter;
import com.metfone.selfcare.module.movienew.listener.OnClickCountry;
import com.metfone.selfcare.module.movienew.model.Country;
import com.metfone.selfcare.module.movienew.model.CountryModel;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CountryFragment extends BaseFragment implements OnClickCountry, OnInternetChangedListener {

    private Unbinder unbinder;

    @BindView(R.id.recycler_country)
    RecyclerView countryList;

    @BindView(R.id.btn_next)
    AppCompatButton btnNext;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.ll_country_container)
    LinearLayout llCountryContainer;

    private ArrayList<Object> countryModels;
    private CountryAdapter countryAdapter;
    private MovieApi movieApi;
    private ListenerUtils mListenerUtils;
    private static RunUi mRunUi;
    private FavoriteBusiness favoriteBusiness;

    public MovieApi getMovieApi() {
        if (movieApi == null) movieApi = new MovieApi();
        return movieApi;
    }

    public CountryFragment() {
        // Required empty public constructor
    }

    public static CountryFragment newInstance(RunUi runUi) {
        mRunUi = runUi;
        CountryFragment fragment = new CountryFragment();
        return fragment;
    }

    public static CountryFragment newInstance() {
        Bundle args = new Bundle();
        CountryFragment fragment = new CountryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        favoriteBusiness = new FavoriteBusiness(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_country, container, false);
        unbinder = ButterKnife.bind(this, view);

        //Set padding for linear layout that contains recycler view to recycler view above gradient layer
        if (getActivity() != null) {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int deviceHeightInPx = displaymetrics.heightPixels;
            int offsetPaddingInDp = 76;
            //3.5 is weight_sum of linear layout
            int paddingInPx = (int) (deviceHeightInPx / 3.5) + Utilities.convertDpToPixel(offsetPaddingInDp);
            llCountryContainer.setPadding(0, 0, 0, paddingInPx);
        }

        if (countryModels == null) countryModels = new ArrayList<>();
        else countryModels.clear();
        countryAdapter = new CountryAdapter(activity, countryModels);
        countryAdapter.setListener(this);
        CustomGridLayoutManager layoutManager = new CustomGridLayoutManager(activity, 2);
        BaseAdapter.setupGridRecycler(activity, countryList, layoutManager, countryAdapter, 2, R.dimen.cinema_card_spacing, true);
        btnNext.setOnClickListener(this::next);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = application.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        if (canLazyLoad()) {
            new Handler().postDelayed(() -> loadData(), Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData();
        }
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad()) {
            loadData();
        }
    }

    private void loadData() {
        pbLoading.setVisibility(View.VISIBLE);
        getMovieApi().getCountryV2(new ApiCallbackV2<ArrayList<Country>>() {
            @Override
            public void onSuccess(String msg, ArrayList<Country> result) {
                if (countryModels == null) countryModels = new ArrayList<>();
                if (Utilities.notEmpty(result)) {
                    countryModels.addAll(result);
                }
                if (countryAdapter != null) countryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String s) {
                if (countryModels == null) countryModels = new ArrayList<>();
                if (countryAdapter != null) countryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onComplete() {
                pbLoading.setVisibility(View.GONE);
                isDataInitiated = true;
            }
        });
    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onClick(Object item, int position, boolean isChecked) {
        if (item instanceof Country) {
            ((Country) countryModels.get(position)).setChecked(isChecked);
            int checkedCount = 0;
            for (Object categoryObj : countryModels) {
                Country country = (Country) categoryObj;
                if (country.isChecked()) {
                    checkedCount++;
                }
            }
            if (checkedCount >= 1) {
                btnNext.setEnabled(true);
            } else {
                btnNext.setEnabled(false);
            }
            countryAdapter.notifyItemChanged(position);
        }
    }

    @OnClick(R.id.btn_next)
    public void next(View view) {
        favoriteBusiness.addedCountry();
        favoriteBusiness.setCountry(getFavorite());
        getActivity().finish();
    }

    public ArrayList<String> getFavorite() {
        ArrayList<String> result = new ArrayList<>();
        if (countryModels != null) {
            for (Object countryModel : countryModels) {
                Country country = (Country) countryModel;
                if (country.isChecked()) {
                    result.add(country.getCategoryId());
                }
            }
        }
        return result;
    }
}
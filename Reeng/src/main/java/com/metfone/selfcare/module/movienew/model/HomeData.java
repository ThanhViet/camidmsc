
package com.metfone.selfcare.module.movienew.model;

import com.google.android.gms.common.util.Strings;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_group")
    @Expose
    private String idGroup;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image_path")
    @Expose
    private String imagePath;
    @SerializedName("poster_path")
    @Expose
    private String posterPath;
    @SerializedName("categories")
    @Expose
    private List<HomeCategory> categories = null;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("userlogin")
    @Expose
    private String userLogin;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("isView")
    @Expose
    private String isView;
    @SerializedName("isFilmGroups")
    @Expose
    private String isFilmGroups;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("current_film")
    @Expose
    private String currentFilm;
    @SerializedName("link_wap")
    @Expose
    private String linkWap;
    @SerializedName("is_free")
    @Expose
    private String isFree;
    @SerializedName("imdb")
    @Expose
    private String imdb;
    @SerializedName("drm_content_id")
    @Expose
    private String drmContentId;
    @SerializedName("isFreeContent")
    @Expose
    private String isFreeContent;
    @SerializedName("isFreeData")
    @Expose
    private String isFreeData;
    @SerializedName("type_film")
    @Expose
    private String typeFilm;
    @SerializedName("episodes")
    @Expose
    private String episodes;
    @SerializedName("chapter")
    @Expose
    private String chapter;
    @SerializedName("hd_price")
    @Expose
    private String hdPrice;
    @SerializedName("hd_price_FreeData")
    @Expose
    private String hdPriceFreeData;
    @SerializedName("sd_price")
    @Expose
    private String sdPrice;
    @SerializedName("sd_price_FreeData")
    @Expose
    private String sdPriceFreeData;
    @SerializedName("publisher")
    @Expose
    private String publisher;
    @SerializedName("productInfo")
    @Expose
    private String productInfo;
    @SerializedName("trailer_url")
    @Expose
    private String trailerUrl;
    @SerializedName("trailer_image")
    @Expose
    private String trailerImage;
    @SerializedName("intro_Path")
    @Expose
    private String introPath;
    @SerializedName("label_url")
    @Expose
    private String labelUrl;
    @SerializedName("free_content_all")
    @Expose
    private String freeContentAll;
    @SerializedName("label_txt")
    @Expose
    private String labelTxt;
    @SerializedName("isInternational")
    @Expose
    private String isInternational;
    @SerializedName("total_like")
    @Expose
    private String totalLike;
    @SerializedName("total_unlike")
    @Expose
    private String totalUnlike;
    @SerializedName("total_share")
    @Expose
    private String totalShare;
    @SerializedName("total_comment")
    @Expose
    private String totalComment;
    @SerializedName("deepLinkUrl")
    @Expose
    private String deepLinkUrl;
    @SerializedName("displayIMDB")
    @Expose
    private String displayIMDB;
    @SerializedName("displayPrize")
    @Expose
    private String displayPrize;
    @SerializedName("displayDirector")
    @Expose
    private String displayDirector;
    @SerializedName("isEncodeLinkToken")
    @Expose
    private String isEncodeLinkToken;
    @SerializedName("logo_path")
    @Expose
    private String logoPath;

    private boolean isCheckGet;

    public Integer getId() {
        return toInteger(id);
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIdGroup() {
        return toInteger(idGroup);
    }

    public void setIdGroup(String idGroup) {
        this.idGroup = idGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public List<HomeCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<HomeCategory> categories) {
        this.categories = categories;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getIsView() {
        return toInteger(isView);
    }

    public void setIsView(String isView) {
        this.isView = isView;
    }

    public Integer getIsFilmGroups() {
        return toInteger(isFilmGroups);
    }

    public void setIsFilmGroups(String isFilmGroups) {
        this.isFilmGroups = isFilmGroups;
    }

    public Integer getTotal() {
        return toInteger(total);
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Integer getCurrentFilm() {
        return toInteger(currentFilm);
    }

    public void setCurrentFilm(String currentFilm) {
        this.currentFilm = currentFilm;
    }

    public String getLinkWap() {
        return linkWap;
    }

    public void setLinkWap(String linkWap) {
        this.linkWap = linkWap;
    }

    public String getIsFree() {
        return isFree;
    }

    public void setIsFree(String isFree) {
        this.isFree = isFree;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public String getDrmContentId() {
        return drmContentId;
    }

    public void setDrmContentId(String drmContentId) {
        this.drmContentId = drmContentId;
    }

    public String getIsFreeContent() {
        return isFreeContent;
    }

    public void setIsFreeContent(String isFreeContent) {
        this.isFreeContent = isFreeContent;
    }

    public String getIsFreeData() {
        return isFreeData;
    }

    public void setIsFreeData(String isFreeData) {
        this.isFreeData = isFreeData;
    }

    public String getTypeFilm() {
        return typeFilm;
    }

    public void setTypeFilm(String typeFilm) {
        this.typeFilm = typeFilm;
    }

    public String getEpisodes() {
        return episodes;
    }

    public void setEpisodes(String episodes) {
        this.episodes = episodes;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getHdPrice() {
        return hdPrice;
    }

    public void setHdPrice(String hdPrice) {
        this.hdPrice = hdPrice;
    }

    public String getHdPriceFreeData() {
        return hdPriceFreeData;
    }

    public void setHdPriceFreeData(String hdPriceFreeData) {
        this.hdPriceFreeData = hdPriceFreeData;
    }

    public String getSdPrice() {
        return sdPrice;
    }

    public void setSdPrice(String sdPrice) {
        this.sdPrice = sdPrice;
    }

    public String getSdPriceFreeData() {
        return sdPriceFreeData;
    }

    public void setSdPriceFreeData(String sdPriceFreeData) {
        this.sdPriceFreeData = sdPriceFreeData;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public String getTrailerImage() {
        return trailerImage;
    }

    public void setTrailerImage(String trailerImage) {
        this.trailerImage = trailerImage;
    }

    public String getIntroPath() {
        return introPath;
    }

    public void setIntroPath(String introPath) {
        this.introPath = introPath;
    }

    public String getLabelUrl() {
        return labelUrl;
    }

    public void setLabelUrl(String labelUrl) {
        this.labelUrl = labelUrl;
    }

    public String getFreeContentAll() {
        return freeContentAll;
    }

    public void setFreeContentAll(String freeContentAll) {
        this.freeContentAll = freeContentAll;
    }

    public String getLabelTxt() {
        return labelTxt;
    }

    public void setLabelTxt(String labelTxt) {
        this.labelTxt = labelTxt;
    }

    public String getIsInternational() {
        return isInternational;
    }

    public void setIsInternational(String isInternational) {
        this.isInternational = isInternational;
    }

    public Integer getTotalLike() {
        return toInteger(totalLike);
    }

    public void setTotalLike(String totalLike) {
        this.totalLike = totalLike;
    }

    public Integer getTotalUnlike() {
        return toInteger(totalUnlike);
    }

    public void setTotalUnlike(String totalUnlike) {
        this.totalUnlike = totalUnlike;
    }

    public Integer getTotalShare() {
        return toInteger(totalShare);
    }

    public void setTotalShare(String totalShare) {
        this.totalShare = totalShare;
    }

    public Integer getTotalComment() {
        return toInteger(totalComment);
    }

    public void setTotalComment(String totalComment) {
        this.totalComment = totalComment;
    }

    public String getDeepLinkUrl() {
        return deepLinkUrl;
    }

    public void setDeepLinkUrl(String deepLinkUrl) {
        this.deepLinkUrl = deepLinkUrl;
    }

    public String getDisplayIMDB() {
        return displayIMDB;
    }

    public void setDisplayIMDB(String displayIMDB) {
        this.displayIMDB = displayIMDB;
    }

    public String getDisplayPrize() {
        return displayPrize;
    }

    public void setDisplayPrize(String displayPrize) {
        this.displayPrize = displayPrize;
    }

    public String getDisplayDirector() {
        return displayDirector;
    }

    public void setDisplayDirector(String displayDirector) {
        this.displayDirector = displayDirector;
    }

    public Integer getIsEncodeLinkToken() {
        return toInteger(isEncodeLinkToken);
    }

    public void setIsEncodeLinkToken(String isEncodeLinkToken) {
        this.isEncodeLinkToken = isEncodeLinkToken;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public boolean isCheckGet() {
        return isCheckGet;
    }

    public void setCheckGet(boolean checkGet) {
        isCheckGet = checkGet;
    }

    private Integer toInteger(String value){
        if(Strings.isEmptyOrWhitespace(value)){
            return 0;
        }
        else {
            return Integer.parseInt(value);
        }
    }
}

package com.metfone.selfcare.module.home_kh.fragment.rewardshop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.RewardsShopKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.model.FilterItems;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

import java.util.ArrayList;
import java.util.List;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.FilterVH> {

    private List<FilterItems> data = new ArrayList<>();
    private IFilterClick iFilterClick;
    private int positionSelected = 0;
    private int type;

    public FilterAdapter(List<FilterItems> data, IFilterClick iFilterClick, int type) {
        this.data = data;
        this.iFilterClick = iFilterClick;
        this.type = type;
    }

    public void setSelected(int positionSelected) {
        this.positionSelected = positionSelected;
    }

    public void setType(int type) {
        this.type = type;
    }

    @NonNull
    @Override
    public FilterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reward_shop_filter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FilterVH holder, int position) {
        holder.bindData(position);
    }


    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public List<FilterItems> getData() {
        return data;
    }

    class FilterVH extends RecyclerView.ViewHolder {

        private AppCompatImageView icFilter;
        private AppCompatImageView icSelected;
        private AppCompatTextView tvFilter;
        private LinearLayout layout;

        public FilterVH(View itemView) {
            super(itemView);
            icFilter = itemView.findViewById(R.id.icon_filter);
            icSelected = itemView.findViewById(R.id.img_selected);
            tvFilter = itemView.findViewById(R.id.tv_filter);
            layout = itemView.findViewById(R.id.layout);
        }

        public void bindData(int position) {
            FilterItems item = data.get(position);
            tvFilter.setText(item.getName());
            if (type == RewardsShopKHFragment.CATEGORY) {
                if (position == 0 || position == 1) {
                    icFilter.setImageDrawable(ResourceUtils.getDrawable(item.getDrawable()));
                } else {
                    Glide.with(icFilter.getContext())
                            .load(item.getUrl())
                            .into(icFilter);
                }
            } else {
                icFilter.setImageDrawable(ResourceUtils.getDrawable(item.getDrawable()));
            }
            icSelected.setVisibility(position == positionSelected ? View.VISIBLE : View.GONE);
            layout.setOnClickListener(v -> {
                if (iFilterClick != null) {
                    iFilterClick.itemClick(position);
                    positionSelected = position;
                    notifyDataSetChanged();
                }
            });
        }
    }

    public interface IFilterClick {
        void itemClick(int position);
    }
}

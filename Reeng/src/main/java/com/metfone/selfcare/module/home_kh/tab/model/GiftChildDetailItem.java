package com.metfone.selfcare.module.home_kh.tab.model;

import java.util.List;

public class GiftChildDetailItem {

    public String giftTypeId;           //": "11",
    public String giftName;             //": "G Mekong Hotel",
    public String giftCode;             //": "G Mekong Hotel",
    public int giftPoint;               // ": 0,
    public String description;          //": "<p><span style=\"font-size:16px\">Get <span style=\"color:#e74c3c\"><strong>5%-10% OFF</strong></span> from <span style=\"color:#1abc9c\"><strong>G Mekong Hotel!</strong></span></span></p>\n\n<p><span style=\"font-size:16px\">Apply for :<span style=\"color:#e74c3c\"><strong> Member ,Silver</strong></span>, <span style=\"color:#e74c3c\"><strong>Gold</strong></span>, <span style=\"color:#e74c3c\"><strong>Diamond </strong></span>and <span style=\"color:#e74c3c\"><strong>Platinum </strong></span>Rank</span></p>\n\n<p><span style=\"font-size:16px\">Click [<span style=\"color:#e74c3c\"><strong>Redeem</strong></span>] and shows this Code or <span style=\"color:#e74c3c\"><strong>QR </strong></span>code to shop to enjoy it.</span></p>\n\n<p><span style=\"font-size:16px\">Contact partner by: <strong>975767676</strong></span></p>\n",
    public String iconUrl;              //": "https://imagemetfone.viettelglobal.net/1595586849500.jpeg",
    public String imageUrl;             //": "https://imagemetfone.viettelglobal.net/1595586849500.jpeg",
    public String giftId;               //": "222",
    public String partnerName;          //": "G Mekong Hotel",
    public List<String> imageList;      //": [  "https://imagemetfone.viettelglobal.net/1595586855741.jpeg" ],
    public String avatar;               //": "https://imagemetfone.viettelglobal.net/1595586871893.jpeg",
    public String price;                //": "10",
    public String score;                //": "0",
    public String startDate;            //": "2019-09-30 00:00:00",
    public String expireDate;           //": "2020-09-30",
    public int quantity;                //": 9771,
    public int isHot;                   //": 0,
    public int isRecommentGift;         //": 0,
    public String discountRate;         //": "5%-10%",
    public List<String> rankingList;    //": [ "5",  "4", "3", "2",  "1" ],
    public String partnerId;            //": "254",
    public String distance;             //": "2.08693068022899146587775833298637960534E00"

    public List<String> storeList;      //": [ "232"  ],
    public String webSite;

    public GiftChildDetailItem() {
    }

}

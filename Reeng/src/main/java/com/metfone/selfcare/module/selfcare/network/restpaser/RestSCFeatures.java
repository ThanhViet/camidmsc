package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCFeatures;

import java.io.Serializable;

public class RestSCFeatures extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private SCFeatures data;

    public SCFeatures getData() {
        return data;
    }

    public void setData(SCFeatures data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCFeatures [data=" + data + "] errror " + getErrorCode();
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/19
 */

package com.metfone.selfcare.module.saving.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SavingStatisticsModel implements Serializable {
    @SerializedName("dialog_info")
    private DialogInfo dialogInfo;
    @SerializedName("saving_statistics")
    private SavingStatistics savingStatistics;
    @SerializedName("promotion")
    private List<Promotion> promotion;
    @SerializedName("total_saving_shortcut")
    private String totalSavingShortcut;
    private Type type;

    public SavingStatisticsModel() {
    }

    public DialogInfo getDialogInfo() {
        return dialogInfo;
    }

    public void setDialogInfo(DialogInfo dialogInfo) {
        this.dialogInfo = dialogInfo;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public SavingStatistics getSavingStatistics() {
        return savingStatistics;
    }

    public void setSavingStatistics(SavingStatistics savingStatistics) {
        this.savingStatistics = savingStatistics;
    }

    public List<Promotion> getPromotion() {
        if (promotion == null) promotion = new ArrayList<>();
        return promotion;
    }

    public void setPromotion(List<Promotion> promotion) {
        this.promotion = promotion;
    }

    public String getTotalSavingShortcut() {
        return totalSavingShortcut;
    }

    public void setTotalSavingShortcut(String totalSavingShortcut) {
        this.totalSavingShortcut = totalSavingShortcut;
    }

    @Override
    public String toString() {
        return "{" +
                "dialogInfo=" + dialogInfo +
                ", savingStatistics=" + savingStatistics +
                ", promotion=" + promotion +
                ", totalSavingShortcut=" + totalSavingShortcut +
                ", type=" + type +
                '}';
    }

    public enum Type {
        PROMOTION(1), SAVING(2);

        public final int VALUE;

        Type(int value) {
            this.VALUE = value;
        }

    }
}

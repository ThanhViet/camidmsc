package com.metfone.selfcare.module.netnews.CategoryNews.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.newdetails.view.CustomImageRatio;
import com.metfone.selfcare.module.newdetails.view.TextViewWithImages;
import com.metfone.selfcare.module.tiin.DateUtilitis;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

import java.util.List;

/**
 * Created by HaiKE on 8/19/17.
 */

public class CategoryNewsAdapter extends BaseQuickAdapter<NewsModel, BaseViewHolder> {

    private Context context;
    private int categoryId = -1;
    private boolean checkView = false;

    public CategoryNewsAdapter(Context context, int id, List<NewsModel> datas, int categoryId) {
        super(id, datas);
        this.context = context;
        this.categoryId = categoryId;
    }
    public void setCheckView(boolean checkView) {
        this.checkView = checkView;
    }

    @Override
    protected void convert(BaseViewHolder holder, NewsModel model) {

        if (model != null) {
            if (categoryId == 7) {
                if (holder.getView(R.id.tv_title) != null)
                    holder.setText(R.id.tv_title, model.getTitle());
                if (holder.getView(R.id.iv_cover) != null) {
                    if (!checkView) {
                        if (holder.getView(R.id.tv_desc) != null) {
                            holder.setVisible(R.id.tv_desc, false);
                        }
                        CustomImageRatio imageView = holder.getView(R.id.iv_cover);
                        if (holder.getAdapterPosition() == 0)
                            imageView.setRatio(9f / 16.0f);
                        else if (holder.getAdapterPosition() == 1 || holder.getAdapterPosition() == 2)
                            imageView.setRatio(10.5f / 16.0f);
                        else
                            imageView.setRatio(9f / 16.0f);
                        ImageBusiness.setImageNewPoster(model.getImage169(), imageView);
                    }else {
                        if (holder.getView(R.id.tv_desc) != null ) {
                            holder.setVisible(R.id.tv_desc,false);

                        }
                        ImageBusiness.setImageNew(model.getImage(), (ImageView) holder.getView(R.id.iv_cover));
                    }
                }
            } else if(categoryId == 135){
                if (holder.getView(R.id.tv_title) != null)
                    holder.setText(R.id.tv_title, model.getTitle());
                if (holder.getView(R.id.iv_cover) != null) {
                    ImageBusiness.setImageNew(model.getImage169(), (ImageView) holder.getView(R.id.iv_cover));
                }
                if(holder.getView(R.id.tv_desc) != null){
                    holder.setVisible(R.id.tv_desc,false);
                }
            }else {
                if (holder.getView(R.id.tv_title) != null)
                    TiinUtilities.setText(holder.getView(R.id.tv_title), model.getTitle(), model.getTypeIcon());
                if (holder.getView(R.id.iv_cover) != null) {
                    ImageBusiness.setImageNew(model.getImage169(), (ImageView) holder.getView(R.id.iv_cover));
                }
            }

            if (holder.getView(R.id.tv_category) != null)
                holder.setText(R.id.tv_category, TextUtils.isEmpty(model.getSourceName()) ? model.getCategory() : model.getSourceName());

            if (holder.getView(R.id.tv_datetime) != null ) {
                if(model.getTimeStamp() > 0){
                    holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, model.getTimeStamp()));
                }else {
                    holder.setText(R.id.tv_datetime, Html.fromHtml(model.getDatePub()));
                }
                holder.setVisible(R.id.tv_datetime, true);

            } else {
                if (holder.getView(R.id.tv_datetime) != null)
                    holder.setVisible(R.id.tv_datetime, false);
            }
        }
    }
}

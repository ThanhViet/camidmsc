package com.metfone.selfcare.module.games.activity;

import static com.metfone.selfcare.module.games.fragment.GameHomePageFragment.isLoadingRecentlyList;
import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabGame.GameModel;

public class PlayGameActivity extends AppCompatActivity {

    private WebView webView;
    private GameModel game;
    private TextView titleGame;
    private ImageView btnBack;
    private final String TAG = "PlayGameActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_play_game);
        initViews();
        initData();
        controllerView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        finish();
    }

    private void controllerView() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLoadingRecentlyList = true;
                onBackPressed();
            }
        });
        titleGame.setText(game.getName());
    }

    private void initViews() {
        titleGame = findViewById(R.id.tvTitleGame);
        btnBack = findViewById(R.id.ic_close);
        webView = findViewById(R.id.wv_content);
    }

    private void initData() {
        game = (GameModel) getIntent().getSerializableExtra("OPEN_GAME");
        processGame(game);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void processGame(GameModel game) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(true);
        webView.getSettings().setAllowContentAccess(true);
        String token = AccountBusiness.getInstance().getToken().replace("Bearer", "").trim();
        String url = game.getLink() + "?token=" + token + "&gameID=" + game.getID();
        webView.loadUrl(url);
    }

}
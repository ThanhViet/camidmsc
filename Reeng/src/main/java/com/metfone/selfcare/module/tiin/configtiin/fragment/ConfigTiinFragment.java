package com.metfone.selfcare.module.tiin.configtiin.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.netnews.callback.ItemDragAndSwipeCallback;
import com.metfone.selfcare.module.newdetails.view.OnItemDragListener;
import com.metfone.selfcare.module.tiin.AppProvider;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.configtiin.adapter.ConfigTiinAdapter;
import com.metfone.selfcare.module.tiin.network.model.Category;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ConfigTiinFragment extends BaseFragment implements TiinListener.onConfigItemListener {
    @BindView(R.id.btnBack)
    ImageView btnBack;
    Unbinder unbinder;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerConfig;
    int moveStart, moveEnd;
    private ConfigTiinAdapter adapter;
    private ArrayList<Category> datas = new ArrayList<>();
    private String listId = "";
    OnItemDragListener onItemDragListener = new OnItemDragListener() {
        @Override
        public void onItemDragStart(RecyclerView.ViewHolder viewHolder, int pos) {
            moveStart = pos;
        }

        @Override
        public void onItemDragMoving(RecyclerView.ViewHolder source, int from, RecyclerView.ViewHolder target, int to) {

        }

        @Override
        public void onItemDragEnd(RecyclerView.ViewHolder viewHolder, int pos) {
            moveEnd = pos;
            saveTempData();
            checkSetting = true;
        }
    };
    private boolean checkSetting = false; // kiểm tra sự kiện config chuyên mục có bị thay đổi ko
    private boolean changePosition = false;

    public static ConfigTiinFragment newInstance() {
        Bundle args = new Bundle();
        ConfigTiinFragment fragment = new ConfigTiinFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_config_tiin, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUp(inflater);
        return view;
    }

    private void setUp(LayoutInflater inflater) {
        datas = AppProvider.getInstance().getDatas();
        listId = AppProvider.getInstance().getIdCategory();
        tvTitle.setText(getResources().getString(R.string.setting_tiin));
        //todo
        datas = onSortData();
        recyclerConfig.setHasFixedSize(true);
        recyclerConfig.setLayoutManager(new CustomLinearLayoutManager(getTiinActivity(), LinearLayoutManager.VERTICAL, false));
        adapter = new ConfigTiinAdapter(getTiinActivity(), datas, this);
        View viewHeader = inflater.inflate(R.layout.holder_header_new, null);
        adapter.addHeaderView(viewHeader, 0);
        recyclerConfig.setAdapter(adapter);

        ItemDragAndSwipeCallback itemDragAndSwipeCallback = new ItemDragAndSwipeCallback(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemDragAndSwipeCallback);
        itemTouchHelper.attachToRecyclerView(recyclerConfig);

        // open drag
        adapter.enableDragItem(itemTouchHelper, R.id.btnMoveList, true);
        adapter.setOnItemDragListener(onItemDragListener);
    }

    public void saveTempData() {
        String str = "";
        for (int i = 0; i < datas.size(); i++) {
            Category model = datas.get(i);
            if (model.isOn()) {
                str += model.getiD() + ",";
            }
        }
        listId = str;
        changePosition = true;
//        setStateButtonSave(originSortCategory != null && !originSortCategory.equals(sortCategory));
    }

    @OnClick(R.id.btnBack)
    public void onClickBack() {
        if (checkSetting) {
            showDialogSave();
            return;
        }
        getBaseActivity().finish();
    }
    @OnClick(R.id.iv_info)
    public void onClickInfo(){
        String textHelp = getString(R.string.content_help_customize_tab_tiin);
        Spanned spanned = Html.fromHtml(textHelp, arg0 -> {
            int id = 0;
            int width, height;
            float scale = 1f;
            try {
                if (arg0.equals("ic_checkbox_selected.png")) {
                    id = R.drawable.ic_v5_toggle_on;
                    scale = 0.3f;
                } else if (arg0.equals("ic_add_tab.png")) {
                    id = R.drawable.ic_v5_toggle_off;
                    scale = 0.3f;
                } else if (arg0.equals("ic_hold_move.png")) {
                    id = R.drawable.ic_v5_drag_setting;
                    scale = 0.6f;
                }
            } catch (Exception ex) {

            }
            LevelListDrawable d = new LevelListDrawable();
            Drawable empty = getResources().getDrawable(id);
            width = (int) (empty.getIntrinsicWidth() * scale);
            height = (int) (empty.getIntrinsicHeight() * scale);
            d.addLevel(0, 0, empty);
            d.setBounds(0, 0, width, height);
            return d;
        }, null);
        Dialog dialog = new Dialog(getBaseActivity(),R.style.DialogFullscreen);
        dialog.setContentView(R.layout.dialog_config_news);
        TextView tvTitleInfo, tvConfirmInfo, tvContentInfo;
        tvTitleInfo = dialog.findViewById(R.id.tvTitleDialog);
        tvConfirmInfo = dialog.findViewById(R.id.tvAction);
        tvContentInfo = dialog.findViewById(R.id.tvMessageDialog);
        tvTitleInfo.setText(getString(R.string.guideline_config_tab_tiin));
        tvContentInfo.setText(spanned);
        tvConfirmInfo.setText(getString(R.string.btn_understood));
        tvConfirmInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private ArrayList<Category> onSortData() {
        if (datas.size() > 0) {
            for (int i = 0; i < datas.size(); i++) {
                datas.get(i).setOn(false);
            }

            if (!TextUtils.isEmpty(listId)) {
                String[] listCategoryId = listId.split(",");
                for (int i = listCategoryId.length - 1; i >= 0; i--) {
                    for (int j = 0; j < datas.size(); j++) {
                        if (listCategoryId[i].equals(String.valueOf(datas.get(j).getiD()))) {
                            datas.get(j).setOn(true);
                            Category category = datas.get(j);
                            datas.remove(j);
                            datas.add(0,category);
                            break;
                        }
                    }
                }
            }
            return datas;
        }
        return new ArrayList<>();
    }

    private void onSave() {
        if(!changePosition){
            listId = "";
            for (Category model : datas) {
                if (model.isOn()) {
                    listId += model.getiD() + ",";
                }
            }
        }

        AppProvider.getInstance().setIdCategory(listId);
        AppProvider.getInstance().setDatas(datas);
        mPref.put(SharedPrefs.LIST_ID_CATAGORY, listId);
        AppProvider.getInstance().setCheckConfigCategory(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemConfig(Category category, int position) {
        checkSetting = true;
        changePosition = false;
        datas.set(position - 1, category);
    }

    public void showDialogSave() {
        DialogConfirm dialogConfirm = DialogConfirm.newInstance(null,
                getString(R.string.want_to_save_your_change_tab), DialogConfirm.CONFIRM_TYPE,
                R.string.dont_save, R.string.save);
        dialogConfirm.setSelectListener(
                new BaseDialogFragment.DialogListener() {
                    @Override
                    public void dialogRightClick(int value) {
                        checkSetting = false;
                        onSave();
                        dialogConfirm.dismiss();
                        getBaseActivity().finish();
                    }

                    @Override
                    public void dialogLeftClick() {
                        dialogConfirm.dismiss();
                        getBaseActivity().finish();
                    }
                });
        dialogConfirm.show(getChildFragmentManager(), "");
    }
}

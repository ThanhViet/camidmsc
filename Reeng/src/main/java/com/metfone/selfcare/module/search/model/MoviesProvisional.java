/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class MoviesProvisional extends Provisional {
    private ArrayList<Movie> data;

    public MoviesProvisional() {
    }

    public MoviesProvisional(ArrayList<Movie> data) {
        this.data = data;
    }

    public ArrayList<Movie> getData() {
        return data;
    }

    public void setData(ArrayList<Movie> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return Utilities.notEmpty(data) ? 1 : 0;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/13
 *
 */

package com.metfone.selfcare.module.share.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;

import butterknife.BindView;

public class ShareImageOnSocialHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    private Activity activity;

    public ShareImageOnSocialHolder(View view, Activity activity) {
        super(view);
        this.activity = activity;
    }

    @Override
    public void bindData(Object item, int position) {
        //    @BindView(R.id.button_remove)
        //    @Nullable
        //    View btnRemove;
        if (item instanceof FeedContent.ImageContent) {
            FeedContent.ImageContent data = (FeedContent.ImageContent) item;
            if (activity != null) {
                if (!TextUtils.isEmpty(data.getFilePath())) {
                    ImageBusiness.setImageFileShareContent(ivCover, data.getFilePath());
                } else {
                    ImageBusiness.setImageShareContent(ivCover, data.getThumbImage(activity));
                }
            }
        }
    }

}
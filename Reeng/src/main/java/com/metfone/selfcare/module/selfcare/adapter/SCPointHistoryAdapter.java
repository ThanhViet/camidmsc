package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.model.SCPointHistoryModel;

import java.util.ArrayList;

public class SCPointHistoryAdapter<T> extends BaseAdapter<BaseViewHolder> {

    private ArrayList<T> data;

    public SCPointHistoryAdapter(Context context) {
        super(context);
    }

    public void setItemsList(ArrayList<T> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_point_history, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder holder, int position) {
        final SCPointHistoryModel model = (SCPointHistoryModel) data.get(position);
        if(model != null)
        {
            ((TextView)holder.getView(R.id.tvName)).setText(model.getReason());
            ((TextView)holder.getView(R.id.tvDescription)).setText(model.getUpdateTime());
            ((TextView)holder.getView(R.id.tvAmount)).setText(model.getAmount());
        }
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }
}
package com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.DateUtilitis;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.module.tiin.activitytiin.TiinDetailActivity;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.module.tiin.wiget.TextViewLinkHandler;
import com.metfone.selfcare.ui.glide.ImageLoader;
import com.vtm.adslib.AdsNativeHelper;
import com.vtm.adslib.template.TemplateView;

import java.util.ArrayList;
import java.util.List;

public class ChildDetailTiinAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = ChildDetailTiinAdapter.class.toString();
    private static final String CAPTION = "caption";
    private static final String BOX_INFO = "box-info";
    private static final String READ_MORE = "read-more";
    private final int TYPE_NONE = -1;
    private final int TYPE_HEADER = 1;
    private final int TYPE_MUL_HEADER = 11;
    private final int TYPE_TEXT = 2;
    private final int TYPE_IMAGE = 3;
    private final int TYPE_VIDEO = 4;
    private final int TYPE_AUTHOR = 5;
    private final int TYPE_ERROR = 6;
    private final int TYPE_HEADER_RELATED = 7;
    private final int TYPE_RELATED = 8;
    private final int TYPE_HEADER_SIBLING = 9;
    private final int TYPE_SIBLING = 10;
    private final int TYPE_TEXT_NOTE = 12;
    private final int TYPE_TEXT_BOX = 14;
    private final static int TYPE_FB_ADVERTISING = 100;
    private Activity mContext;
    private List<NewsDetailModel> listItemNews = new ArrayList<>();
    private List<TiinModel> listRelated = new ArrayList<>();
    private List<TiinModel> listSibling = new ArrayList<>();
    private TiinModel newsModel;
    private TiinListener.onDetailTiinItemListener listener;
    private int isStopVideo = -1;
    private NewsDetailVideoHolder currentVideoHolder;
    private int demVideo = 0;

    private boolean hasAdvertising = false;
//    private View adContainer;
//    AdView adViewInDetail;
//    AdView adViewInRelate;

    public ChildDetailTiinAdapter(Activity mContext, TiinModel newsModel, TiinListener.onDetailTiinItemListener listener, boolean fromTiin) {
        this.mContext = mContext;
        this.listener = listener;
        if (newsModel != null) {

            if (newsModel.getBody() != null && !fromTiin) {
                newsModel.getBody().add(new NewsDetailModel(mContext.getResources().getString(R.string.taidulieu), 0, "", "", 1, 0));
            }
            listItemNews = newsModel.getBody();
            this.newsModel = newsModel;
        }

    }

    public void addBlock(TiinModel newsModel) {
        this.newsModel = newsModel;

        List<NewsDetailModel> list = newsModel.getBody();
        hasAdvertising = AdsUtils.checkShowAds() && AdsNativeHelper.getInstance().canShowAd();
        if (hasAdvertising && list != null && list.size() > 1) {
            //create fb advertising pos
//            NewsDetailModel fbAdvertisingModle = new NewsDetailModel("", 0, "", "", TYPE_FB_ADVERTISING, 0);
//            int adPos = (list.size() / 2);
//            list.add(adPos, fbAdvertisingModle);

            for(int i = 1; i < list.size(); i++)
            {
                if(i % 10 == 0 && i + 9 < list.size())
                {
                    NewsDetailModel fbAdvertisingModle = new NewsDetailModel("", 0, "", "", TYPE_FB_ADVERTISING, 0);
                    list.add(i, fbAdvertisingModle);
                }
            }
        }
        listItemNews = list;
//        listItemNews = newsModel.getBody();
        notifyDataSetChanged();
    }

    public void addRelated(List<TiinModel> list) {
        listRelated = list;
        notifyDataSetChanged();
    }

    public void addSibling(List<TiinModel> list) {
        listSibling = list;
        notifyDataSetChanged();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        if (holder instanceof NewsDetailVideoHolder) {
            if (isStopVideo == holder.getAdapterPosition() && ((TiinDetailActivity) mContext).isPlaying) {
                listener.onDetachVideoDetail();
                checkCurrentVideo();
            }
        }
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (holder instanceof NewsDetailVideoHolder) {
            if (isStopVideo == holder.getAdapterPosition()) {
                if (((TiinDetailActivity) mContext).isPlaying) {
                    setVisibility(((NewsDetailVideoHolder) holder).ivPlay, View.GONE);
                    setVisibility(((NewsDetailVideoHolder) holder).ivImage, View.GONE);
                } else {
                    setVisibility(((NewsDetailVideoHolder) holder).ivPlay, View.VISIBLE);
                    setVisibility(((NewsDetailVideoHolder) holder).ivImage, View.VISIBLE);
                    checkCurrentVideo();
                }
            } else {
                setVisibility(((NewsDetailVideoHolder) holder).ivPlay, View.VISIBLE);
                setVisibility(((NewsDetailVideoHolder) holder).ivImage, View.VISIBLE);
            }
        }

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        switch (viewType) {
            case TYPE_HEADER:
                View viewHeader = inflater.inflate(R.layout.item_tiin_detail_header, parent, false);
                return new NewsDetailHeaderHolder(viewHeader);
            case TYPE_MUL_HEADER:
                View view = inflater.inflate(R.layout.item_tiin_detail_mul_header, parent, false);
                return new NewsDetailMulHeaderHolder(view);
            case TYPE_TEXT:
                View viewText = inflater.inflate(R.layout.item_tiin_detail_text, parent, false);
                return new NewsDetailTextHolder(viewText);

            case TYPE_IMAGE:
                View viewImage = inflater.inflate(R.layout.item_tiin_detail_image, parent, false);
                return new NewsDetailImageHolder(viewImage);

            case TYPE_VIDEO:
                View viewVideo = inflater.inflate(R.layout.item_tiin_detail_video_exo, parent, false);
                return new NewsDetailVideoHolder(viewVideo);

            case TYPE_AUTHOR:
                View viewAuthor = inflater.inflate(R.layout.item_tiin_detail_author, parent, false);
                return new NewsDetailAuthorHolder(viewAuthor);
            case TYPE_HEADER_RELATED:
                View viewRelated = inflater.inflate(R.layout.item_tiin_detail_text_related, parent, false);
//                if (viewRelated != null && viewRelated instanceof ViewGroup) {
//                    try {
//                        if (hasAdvertising) {
//                            adContainer = inflater.inflate(R.layout.item_advertising_fb, null, false);
//                            ((ViewGroup) viewRelated).addView(adContainer, 0);
//                        }
//                    } catch (Exception e) {
//                    }
//                }
                return new NewsDetailTextRelatedHolder(viewRelated);

            case TYPE_RELATED:
                View viewSame = inflater.inflate(R.layout.holder_normal_tiin, parent, false);
                return new NewsDetailRelatedHolder(viewSame);

            case TYPE_HEADER_SIBLING:
                View convertView = inflater.inflate(R.layout.item_tiin_detail_text_sibling, parent, false);
                return new NewsDetailTextRelatedHolder(convertView);

            case TYPE_SIBLING:
                View viewSibling = inflater.inflate(R.layout.holder_normal_tiin, parent, false);
                return new NewsDetailRelatedHolder(viewSibling);
            case TYPE_TEXT_NOTE:
                View viewTextNote = inflater.inflate(R.layout.item_news_detail_text_note, parent, false);
                return new TiinTextNoteHolder(viewTextNote);
            case TYPE_TEXT_BOX:
                View viewTextBox = inflater.inflate(R.layout.item_news_detail_text_box, parent, false);
                return new TiinTextBoxHolder(viewTextBox);
            case TYPE_FB_ADVERTISING:
                convertView = inflater.inflate(R.layout.ad_unified_medium, parent, false);
                return new NewsFbAdvertisingHolder(mContext, convertView);
            case TYPE_NONE:
                convertView = inflater.inflate(R.layout.holder_footer_new, parent, false);
                return new BaseViewHolder(convertView);
            default:
                View viewTextD = inflater.inflate(R.layout.item_tiin_detail_text, parent, false);
                return new NewsDetailTextHolder(viewTextD);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NewsDetailModel newsItemModel;
        if (newsModel == null || listItemNews == null || listRelated == null) return;
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                NewsDetailHeaderHolder headerHolder = (NewsDetailHeaderHolder) holder;
                headerHolder.bindData(newsModel);
                break;
            case TYPE_MUL_HEADER:
                NewsDetailMulHeaderHolder mulHolder = (NewsDetailMulHeaderHolder) holder;
                mulHolder.bindData(newsModel);
                break;
            case TYPE_TEXT:
                newsItemModel = listItemNews.get(position - 1);
                NewsDetailTextHolder textHolder = (NewsDetailTextHolder) holder;
                textHolder.bindData(newsItemModel);
                break;
            case TYPE_IMAGE:
                newsItemModel = listItemNews.get(position - 1);
                NewsDetailImageHolder imageHolder = (NewsDetailImageHolder) holder;
                imageHolder.bindData(newsItemModel);
                break;
            case TYPE_VIDEO:
                newsItemModel = listItemNews.get(position - 1);
                NewsDetailVideoHolder videoHolder = (NewsDetailVideoHolder) holder;
                videoHolder.setElement(newsItemModel, holder.getAdapterPosition(), (NewsDetailVideoHolder) holder);
                break;
            case TYPE_AUTHOR:
                NewsDetailAuthorHolder authorHolder = (NewsDetailAuthorHolder) holder;
                authorHolder.bindData(newsModel);
                break;
            case TYPE_HEADER_RELATED:
                NewsDetailTextRelatedHolder textRelatedHolder = (NewsDetailTextRelatedHolder) holder;
                textRelatedHolder.binData(newsModel);
//                if (hasAdvertising) {
//                    handleAudienceNetworkAdRelate(adViewInRelate, adContainer);
//                } else {
//                    if (adContainer != null) {
//                        adContainer.setVisibility(View.GONE);
//                    }
//                }
                break;
            case TYPE_RELATED:
                int index = position - (listItemNews.size() + 3);
                TiinModel newsModel = listRelated.get(index);
                NewsDetailRelatedHolder relatedHolder = (NewsDetailRelatedHolder) holder;
                relatedHolder.bindData(newsModel);
                break;
            case TYPE_HEADER_SIBLING:
                NewsDetailTextRelatedHolder textSibHolder = (NewsDetailTextRelatedHolder) holder;
                break;
            case TYPE_SIBLING:
                int countRelate = listRelated.size() == 0 ? 0 : listRelated.size() + 1;
                int pos = position - (listItemNews.size() + 3 + countRelate);
                TiinModel model = listSibling.get(pos);
                NewsDetailRelatedHolder siblingHolder = (NewsDetailRelatedHolder) holder;
                siblingHolder.bindData(model);
                break;
            case TYPE_TEXT_NOTE:
                newsItemModel = listItemNews.get(position - 1);
                TiinTextNoteHolder textNote = (TiinTextNoteHolder) holder;
                textNote.binData(newsItemModel);
                break;
            case TYPE_TEXT_BOX:
                newsItemModel = listItemNews.get(position - 1);
                TiinTextBoxHolder textBox = (TiinTextBoxHolder) holder;
                textBox.binData(newsItemModel);
                break;
            case TYPE_FB_ADVERTISING:
//                handleAudienceNetworkAdDetail(adViewInDetail, ((NewsFbAdvertisingHolder) holder).fbAdViewContainer);
                ((NewsFbAdvertisingHolder) holder).bindData();
                break;
            case TYPE_NONE:
                break;
            default:
//                newsItemModel = listItemNews.get(position - 1);
                NewsDetailTextHolder textDeHolder = (NewsDetailTextHolder) holder;
//                textDeHolder.bindData(newsItemModel);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (listItemNews == null || listItemNews.size() == 0) return 0;
        int countRelateNews = listRelated.size() == 0 ? 0 : listRelated.size() + 1;
        int countSibling = listSibling.size() == 0 ? 0 : listSibling.size() + 1;
        return 1 + listItemNews.size() + 1 + countRelateNews + countSibling + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            if (newsModel.getTypeIcon() == 6) {
                return TYPE_MUL_HEADER;
            } else {
                return TYPE_HEADER;
            }
        } else if (position > 0 && position <= listItemNews.size()) {
            NewsDetailModel item = listItemNews.get(position - 1);
            if (item.getType() == 1) {
                if (!TextUtils.isEmpty(item.getTemplate()) && item.getTemplate().equals(CAPTION)) {
                    return TYPE_TEXT_NOTE;
                } else if (!TextUtils.isEmpty(item.getTemplate()) && item.getTemplate().equals(BOX_INFO)) {
                    return TYPE_TEXT_BOX;
                }
                return TYPE_TEXT;
            } else if (item.getType() == 2) {
                return TYPE_IMAGE;
            } else if (item.getType() == 3) {
                demVideo++;
                return TYPE_VIDEO;
            }
            else if (item.getType() == TYPE_FB_ADVERTISING) {
                return TYPE_FB_ADVERTISING;
            }
            else {
                return TYPE_NONE;
            }

        } else if (position == listItemNews.size() + 1) {
            return TYPE_AUTHOR;

        } else if (listRelated.size() > 0) {
            if (position == listItemNews.size() + 2) {
                return TYPE_HEADER_RELATED;
            } else if (position > listItemNews.size() + 2 && position <= listItemNews.size() + 2 + listRelated.size()) {
                return TYPE_RELATED;
            } else if (listSibling.size() > 0) {
                if (position == (listItemNews.size() + 2 + listRelated.size() + 1)) {
                    return TYPE_HEADER_SIBLING;
                } else if ((position > listItemNews.size() + 2 + listRelated.size() + 1) && (position < listItemNews.size() + 3 + listRelated.size() + listSibling.size() + 1)) {
                    return TYPE_SIBLING;
                } else {
                    return TYPE_NONE;
                }
            } else {
                return TYPE_NONE;
            }
        } else if (listRelated.size() == 0 && listSibling.size() > 0) {
            if (position == (listItemNews.size() + 2)) {
                return TYPE_HEADER_SIBLING;
            } else if ((position > listItemNews.size() + 2) && (position < listItemNews.size() + 2 + listSibling.size() + 1)) {
                return TYPE_SIBLING;
            } else {
                return TYPE_NONE;
            }
        }
        return TYPE_NONE;
    }

    public void checkCurrentVideo() {
        if (currentVideoHolder != null) {
            currentVideoHolder = null;
        }
    }

    private void setVisibility(View view, int visibility) {
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    public class NewsDetailHeaderHolder extends RecyclerView.ViewHolder {
        TextView tvDetailTitle, tvDetailTime, tvDetailBody, tvDetailLoad, tvCategory;
        ImageView ivDos;

        NewsDetailHeaderHolder(View view) {
            super(view);
            tvDetailTitle = view.findViewById(R.id.tv_detail_title);
            tvDetailTime = view.findViewById(R.id.tv_detail_time);
            tvDetailBody = view.findViewById(R.id.tv_detail_body);
            tvDetailLoad = view.findViewById(R.id.tv_detail_load);
            tvCategory = view.findViewById(R.id.tvCategory);
            ivDos = view.findViewById(R.id.imvDot);
        }

        void bindData(TiinModel newsModel) {
            if (!TextUtils.isEmpty(newsModel.getTitle())) {
                if (tvDetailTitle.getVisibility() == View.GONE) {
                    tvDetailTitle.setVisibility(View.VISIBLE);
                }
                tvDetailTitle.setText(newsModel.getTitle());
            } else {
                tvDetailTitle.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(newsModel.getCategory())) {
                ivDos.setVisibility(View.VISIBLE);
                tvCategory.setText(newsModel.getCategory());
            }
            if (newsModel.getDatePub() != 0) {
                if (tvDetailTime.getVisibility() == View.GONE) {
                    tvDetailTime.setVisibility(View.VISIBLE);
                }
                tvDetailTime.setText(DateUtilitis.calculateDate(mContext, newsModel.getDatePub()));
            } else {
                tvDetailTime.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(newsModel.getShapo())) {
                if (tvDetailBody.getVisibility() == View.GONE) {
                    tvDetailBody.setVisibility(View.VISIBLE);
                }
                tvDetailBody.setText(newsModel.getShapo());
            } else {
                tvDetailBody.setVisibility(View.GONE);
            }
            if (newsModel.getBody() != null && newsModel.getBody().size() != 0) {
                tvDetailLoad.setVisibility(View.GONE);
            } else {
                //cho xem xet
                tvDetailLoad.setVisibility(View.VISIBLE);
            }
        }
    }

    public class NewsDetailMulHeaderHolder extends RecyclerView.ViewHolder {
        ImageView ivDetailImage;

        NewsDetailMulHeaderHolder(View view) {
            super(view);
            ivDetailImage = view.findViewById(R.id.ivImage);
        }

        void bindData(final TiinModel itemModel) {
            ImageLoader.setNewsImage(mContext, itemModel.getImage(), ivDetailImage);
        }
    }

    public class NewsDetailTextHolder extends RecyclerView.ViewHolder {
        TextView tvDetailContent;

        NewsDetailTextHolder(View view) {
            super(view);
            tvDetailContent = view.findViewById(R.id.tv_detail_content);
        }

        void bindData(NewsDetailModel itemModel) {
            tvDetailContent.setText(Html.fromHtml(itemModel.getContent()));
            tvDetailContent.setLinksClickable(true);
            tvDetailContent.setMovementMethod(new TextViewLinkHandler() {
                @Override
                public void onLinkClick(String linkText, String url) {
                    listener.onClickLinkText(linkText, url);
                }
            });
        }
    }

    public class NewsDetailImageHolder extends RecyclerView.ViewHolder {
        ImageView ivDetailImage;

        NewsDetailImageHolder(View view) {
            super(view);
            ivDetailImage = view.findViewById(R.id.iv_detail_image);
        }

        void bindData(final NewsDetailModel itemModel) {
            int widthPixels;
            DisplayMetrics displaymetrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            if (wm != null) {
                wm.getDefaultDisplay().getMetrics(displaymetrics);
            }
            if (displaymetrics.widthPixels > displaymetrics.heightPixels) {
                // width>height (set nguoc lai)
                widthPixels = displaymetrics.heightPixels;
            } else {
                widthPixels = displaymetrics.widthPixels;
            }

            float ratio = (float) itemModel.getHeight() / (float) itemModel.getWidth();
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ivDetailImage.getLayoutParams();
            layoutParams.width = widthPixels;
            layoutParams.height = (int) (layoutParams.width * ratio);
            ivDetailImage.setLayoutParams(layoutParams);
            if (itemModel.getContent().contains(".gif")) {
                ImageLoader.setNewImageGif(mContext, itemModel.getContent(), ivDetailImage);
            } else {
                ImageLoader.setNewsImageCache(mContext, itemModel.getContent(), ivDetailImage);
            }
            ivDetailImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickImage(itemModel);
                }
            });
        }
    }

    public class NewsDetailVideoHolder extends RecyclerView.ViewHolder {
        public FrameLayout layoutVideo;
        ImageView ivImage, ivPlay;
        private NewsDetailModel mEntry;

        public NewsDetailVideoHolder(View itemView) {
            super(itemView);
            layoutVideo = itemView.findViewById(R.id.frVideo);
            ivImage = itemView.findViewById(R.id.ivVideo);
            ivPlay = itemView.findViewById(R.id.iv_detail_play);
            TiinUtilities.setWidthHeight(mContext, layoutVideo);
            TiinUtilities.setWidthHeightImage(mContext, ivImage);
            if (ivImage.getVisibility() == View.GONE) {
                setVisibility(ivImage, View.VISIBLE);
            }
            if (ivPlay.getVisibility() == View.GONE) {
                setVisibility(ivPlay, View.VISIBLE);
            }
        }

        public void setElement(Object obj, final int position, final NewsDetailVideoHolder holder) {
            if (obj == null) {
                return;
            }
            mEntry = (NewsDetailModel) obj;
            ImageLoader.setNewsImage(mContext, mEntry.getPoster(), ivImage);
            ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemNews != null && listItemNews.size() > 3) {
                        setVisibility(ivImage, View.GONE);
                        setVisibility(ivPlay, View.GONE);
                        listener.onPlayVideo(mEntry, position, holder);
                        isStopVideo = position;
                        if (currentVideoHolder != null) {
                            setVisibility(currentVideoHolder.ivImage, View.VISIBLE);
                            setVisibility(currentVideoHolder.ivPlay, View.VISIBLE);
                        }
                        if (demVideo >= 2) {
                            currentVideoHolder = holder;
                        }
                    } else {
                        if (BuildConfig.DEBUG) {
                            Toast.makeText(ApplicationController.self(), mContext.getString(R.string.loadding_data), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }

    }

    public class NewsDetailAuthorHolder extends RecyclerView.ViewHolder {
        TextView tvDetailAuthor;
        ImageView ivSource;

        NewsDetailAuthorHolder(View view) {
            super(view);
            tvDetailAuthor = view.findViewById(R.id.tv_detail_author);
            ivSource = view.findViewById(R.id.iv_source);
        }

        @SuppressLint("SetTextI18n")
        void bindData(final TiinModel newsModel) {
            if (!TextUtils.isEmpty(newsModel.getAuthorName())) {
                tvDetailAuthor.setVisibility(View.VISIBLE);
                tvDetailAuthor.setText(newsModel.getAuthorName());
            } else {
                tvDetailAuthor.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(newsModel.getUrlOrigin())) {
                ivSource.setVisibility(View.VISIBLE);
                ivSource.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TiinUtilities.showDialogSource((BaseSlidingFragmentActivity) mContext, newsModel.getUrlOrigin());
                    }
                });

            } else {
                ivSource.setVisibility(View.GONE);
            }
        }
    }

    public class NewsDetailTextRelatedHolder extends RecyclerView.ViewHolder {
        TextView tvRelated;
        TemplateView layout_ads;

        NewsDetailTextRelatedHolder(View itemView) {
            super(itemView);
            tvRelated = itemView.findViewById(R.id.tv_related);
            layout_ads = itemView.findViewById(R.id.layout_ads);
        }

        public void binData(TiinModel model) {
            if (model != null) {
                if (model.getThematicId() > 0 && !TextUtils.isEmpty(model.getThematicName())) {
                    tvRelated.setText(model.getThematicName());
                } else {
                    tvRelated.setText(mContext.getResources().getString(R.string.releate_tiin));
                }
            }

            if(layout_ads != null)
                AdsManager.getInstance().showAdsNative(layout_ads);
        }
    }

    public class NewsDetailRelatedHolder extends RecyclerView.ViewHolder {
        ImageView ivRelatedThumb, ivMore;
        TextView tvRelatedTitle, tvCategory, tvDateTime;
        RelativeLayout rlItem;

        NewsDetailRelatedHolder(View itemView) {
            super(itemView);
            ivRelatedThumb = itemView.findViewById(R.id.iv_cover);
            tvRelatedTitle = itemView.findViewById(R.id.tv_title);
            rlItem = itemView.findViewById(R.id.layout_root);
            ivMore = itemView.findViewById(R.id.button_option);
            tvCategory = itemView.findViewById(R.id.tv_category);
            tvDateTime = itemView.findViewById(R.id.tv_datetime);
        }

        void bindData(final TiinModel tiinModel) {
            TiinUtilities.setText(tvRelatedTitle, tiinModel.getTitle(), tiinModel.getTypeIcon());
            if(!TextUtils.isEmpty(tiinModel.getCategory()) && tvCategory != null){
                tvCategory.setText(tiinModel.getCategory());
            }
            if(tvDateTime != null) {
                tvDateTime.setVisibility(View.VISIBLE);
                tvDateTime.setText(DateUtilitis.calculateDate(mContext, tiinModel.getDatePub()));
            }
            ImageBusiness.setImageNew(tiinModel.getImage(), ivRelatedThumb);
            if (rlItem != null) {
                rlItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClickRelate(tiinModel);
                    }
                });
            }
            if(ivMore != null){
                ivMore.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        listener.onItemClickMore(tiinModel);
                    }
                });
            }
        }
    }

    public class TiinTextNoteHolder extends RecyclerView.ViewHolder {
        TextView tvNote;
        NewsDetailModel mEntry;

        public TiinTextNoteHolder(View view) {
            super(view);
            tvNote = view.findViewById(R.id.tvNote);
        }

        public void binData(NewsDetailModel obj) {
            mEntry = obj;
            if (mEntry != null) {
                tvNote.setText(Html.fromHtml(mEntry.getContent()));
            }
        }
    }

    public class TiinTextBoxHolder extends RecyclerView.ViewHolder {

        TextView tvBox;
        NewsDetailModel mEntry;

        public TiinTextBoxHolder(View itemView) {
            super(itemView);
            tvBox = itemView.findViewById(R.id.tv_text_box);
        }

        public void binData(NewsDetailModel item) {
            mEntry = item;
            if (mEntry != null) {
                tvBox.setText(Html.fromHtml(mEntry.getContent()));
                tvBox.setLinksClickable(true);
                tvBox.setMovementMethod(new TextViewLinkHandler() {
                    @Override
                    public void onLinkClick(String linkText, String url) {
                        listener.onClickLinkText(linkText, url);
                    }
                });
            }
        }
    }

    //Ads
    public class NewsFbAdvertisingHolder extends BaseViewHolder {
        TemplateView fbAdViewContainer;

        public NewsFbAdvertisingHolder(Context context, View view) {
            super(view);
            fbAdViewContainer = view.findViewById(R.id.adContainer);
        }

        public void bindData()
        {
            if(fbAdViewContainer != null)
                AdsManager.getInstance().showAdsNative(fbAdViewContainer);
        }
    }

//    public void loadAdView() {
//        if (!hasAdvertising) return;
//        try {
//            String ad_player_music = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_NEWS);
//            if(TextUtils.isEmpty(ad_player_music)) return;
//            if (adViewInDetail == null) {
//                adViewInDetail = initAdView(ad_player_music, "AD_MIDDLE");
//                if (adViewInDetail != null && !adViewInDetail.isLoading()) {
//                    AdRequest adRequest = new AdRequest.Builder()
//                            .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
//                            .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
//                            .build();
//                    adViewInDetail.loadAd(adRequest);
//                }
//            }
//
//            if (adViewInRelate == null) {
//                adViewInRelate = initAdView(ad_player_music, "AD_END");
//                if (adViewInRelate != null && !adViewInRelate.isLoading()) {
//                    AdRequest adRequest = new AdRequest.Builder()
//                            .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
//                            .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
//                            .build();
//                    adViewInRelate.loadAd(adRequest);
//                }
//            }
//        } catch (Error | Exception e) {
//        }
//    }
//
//    private AdView initAdView(String unitId, String pos) {
//        AdView adView = new AdView(mContext);
//        adView.setAdUnitId(unitId);
//        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
//        adView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                super.onAdLoaded();
//                if (adView != null) {
//                    adView.setTag("Loaded");
//                }
//                if("AD_MIDDLE".equals(pos))
//                {
//                    notifyItemChanged((listItemNews.size() - 1) / 2 + 1);
//                }
//                else if("AD_END".equals(pos))
//                {
//                    notifyItemChanged(2 + listItemNews.size());
//                }
//            }
//
//            @Override
//            public void onAdClosed() {
//                super.onAdClosed();
//            }
//
//            @Override
//            public void onAdFailedToLoad(int i) {
//                super.onAdFailedToLoad(i);
//                if (adView != null) {
//                    adView.setTag("Error");
//                }
//            }
//        });
//        return adView;
//    }
//
//    private boolean handleAudienceNetworkAdDetail(AdView adView, View container) {
//        if (container == null) return false;
//        if (adView == null) {
//            container.setVisibility(View.GONE);
//            return false;
//        }
//        if (adView.getTag() != null) {
//            if (adView.getTag().equals("Loaded")) {
//                container.setVisibility(View.VISIBLE);
//                if (container == adView.getParent()) return true;
//                removeViewParent(adView);
//                ((ViewGroup) container).addView(adView);
//                return true;
//            } else {
//                container.setVisibility(View.GONE);
//                return false;
//            }
//        } else {
//            container.setVisibility(View.GONE);
//        }
//        return false;
//    }
//
//    private boolean handleAudienceNetworkAdRelate(AdView adView, View container) {
//        if (adView == null || container == null) return false;
//        try {
//            if (adView.getTag() != null) {
//                if (adView.getTag().equals("Loaded")) {
//                    container.setVisibility(View.VISIBLE);
//                    if (container.findViewById(R.id.adView) == adView.getParent()) return true;
//                    removeViewParent(adView);
//                    ((ViewGroup) container.findViewById(R.id.adView)).addView(adView);
//                    return true;
//                } else {
//                    container.setVisibility(View.GONE);
//                    return false;
//                }
//            } else {
//                container.setVisibility(View.GONE);
//            }
//            return false;
//        } catch (Exception | Error e) {
//            return false;
//        }
//    }
//
//    private void removeViewParent(View view) {
//        if (view != null && view.getParent() != null && view.getParent() instanceof ViewGroup) {
//            ((ViewGroup) view.getParent()).removeView(view);
//        }
//    }
}

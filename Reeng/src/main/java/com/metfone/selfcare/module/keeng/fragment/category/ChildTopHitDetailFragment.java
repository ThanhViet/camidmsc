package com.metfone.selfcare.module.keeng.fragment.category;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.adapter.category.ChildTopicAdapter;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.network.restpaser.RestAllModel;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.ArrayList;
import java.util.List;

public class ChildTopHitDetailFragment extends RecyclerFragment<AllModel> implements BaseListener.OnLoadMoreListener {

    private int currentType = 1;
    private int currentPage = 1;
    private Topic topic;
    private ChildTopicAdapter adapter;
    private TabKeengActivity mActivity;
    private ListenerUtils listenerUtils;

    public ChildTopHitDetailFragment() {
        clearData();
    }

    public static ChildTopHitDetailFragment instance(Topic topic, int type) {
        ChildTopHitDetailFragment fragment = new ChildTopHitDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, topic);
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "ChildTopHitDetailFragment";
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TabKeengActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            currentType = getArguments().getInt("type");
            topic = (Topic) getArguments().getSerializable(Constants.KEY_DATA);
        }
        adapter = new ChildTopicAdapter(mActivity, getDatas(), TAG);
        if (currentType == Constants.TYPE_SONG) {
            setupRecycler(adapter);
            recyclerView.setPadding(0, mActivity.getResources().getDimensionPixelOffset(R.dimen.padding_8), 0, 0);
        } else {
            setupGridRecycler(adapter, 2, R.dimen.padding_16, true);
        }
        adapter.setRecyclerView(recyclerView, this);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDatas().size() == 0) {
            doLoadData(true);
        }
    }

    private void doLoadData(boolean type) {
        if (!isLoading) {
            if (type)
                loadingBegin();
            isLoading = true;
            loadData();
        }
    }

    private void loadData() {
        if (mActivity == null || topic == null)
            return;
        new KeengApi().getTopHitDetail(topic.getId(), currentType, currentPage, numPerPage, new Listener<RestAllModel>() {

            @Override
            public void onResponse(RestAllModel response) {
                loadMediaComplete(response.getData());
            }
        }, new ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error);
                loadMediaComplete(null);
            }
        });
    }

    protected void loadMediaComplete(List<AllModel> result) {
        isLoading = false;
        checkLoadMoreAbsolute(result);
        try {
            if (result == null) {
                if (isRefresh) {
                    isRefresh = false;
                }
                loadMored();
                adapter.setLoaded();
                loadingError(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        doLoadData(true);

                    }
                });
                return;
            }

            if (getDatas().size() == 0 && result.size() == 0) {
                refreshed();
                loadMored();
                loadingEmpty();
            } else {
                refreshed();
                loadMored();
                loadingFinish();
                if (currentType == Constants.TYPE_SONG)
                    ConvertHelper.convertData(result, MediaLogModel.SRC_TOP_HIT);
                setDatas(result);
                if (getDatas().size() == 0) {
                    loadingEmpty();
                }
                adapter.setLoaded();
                adapter.notifyDataSetChanged();
                currentPage++;
            }

        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    private List<AllModel> initData(List<AllModel> data) {
        List<AllModel> temp = new ArrayList<>();
        for (AllModel am : data) {
            if (am.type == currentType) {
                temp.add(am);
            }
        }
        return temp;
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        canLoadMore = true;
        currentPage = 1;
        doLoadData(false);

    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            loadMore();
            doLoadData(false);
        }
    }

    @Override
    public void onMediaClick(View v, int position) {
        if (mActivity == null || adapter == null) {
            return;
        }
        AllModel item = adapter.getItem(position);
        if (item != null) {
            item.setSource(MediaLogModel.SRC_TOP_HIT);
            switch (item.type) {
                case Constants.TYPE_ALBUM:
                case Constants.TYPE_ALBUM_VIDEO:
                    mActivity.gotoAlbumDetail(item);
                    break;
                case Constants.TYPE_SONG:
                    PlayingList playingList = new PlayingList(getDatas(), PlayingList.TYPE_TOPIC, MediaLogModel.SRC_TOP_HIT);
                    playingList.setName(topic.getName());
                    mActivity.setMediaPlayingAudio(playingList, position - 1);
                    break;
                case Constants.TYPE_VIDEO:
                    mActivity.setMediaToPlayVideo(item);
                    break;
            }
        }
    }

    @Override
    public void onMediaExpandClick(View v, int position) {
        if (mActivity == null || adapter == null) {
            return;
        }
        AllModel item = adapter.getItem(position);
        mActivity.showPopupMore(item);
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && getDatas().size() == 0)
            onRefresh();
    }
}

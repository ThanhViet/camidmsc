/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/3
 *
 */

package com.metfone.selfcare.module.tab_home.holder;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnClickSliderBanner;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.tab_home.adapter.SlideBannerAdapter;
import com.metfone.selfcare.module.tab_home.model.HomeContent;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.ui.view.indicator.OverflowPagerIndicator;
import com.metfone.selfcare.ui.view.indicator.SimpleSnapHelper;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;

public class SlideBannerHolder extends BaseAdapter.ViewHolder {

    private final int CHANGE_SLIDE_BANNER_TIME = 3000;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.indicator)
    OverflowPagerIndicator indicator;
    private ArrayList<Object> data = new ArrayList<>();
    private SlideBannerAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int next;
    private boolean isSlideBanner;
    private Runnable runnableAutoScroll = new Runnable() {
        @Override
        public void run() {
            if (recyclerView == null || layoutManager == null) return;
            if (Utilities.notEmpty(data) && data.size() > 1) {
                try {
                    int current = layoutManager.findFirstVisibleItemPosition();
                    if (next == 0) {
                        next = 1;
                    } else if (current == 0 && next == -1) {
                        next = 1;
                    } else if (current == data.size() - 1 && next == 1) {
                        next = -1;
                    }
                    int positionNext = current + next;
                    layoutManager.smoothScrollToPosition(recyclerView, null, positionNext);
                    indicator.onPageSelected(positionNext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            recyclerView.postDelayed(this, isSlideBanner ? CHANGE_SLIDE_BANNER_TIME : Constants.CHANGE_BANNER_TIME);
        }
    };

    @SuppressLint("ClickableViewAccessibility")
    public SlideBannerHolder(View view, Activity activity, OnClickSliderBanner listener, boolean slideBanner) {
        super(view);
        this.isSlideBanner = slideBanner;
        if (data == null) data = new ArrayList<>();
        adapter = new SlideBannerAdapter(activity, listener);
        adapter.setItems(data);
        layoutManager = new CustomLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, layoutManager, adapter, false);
        indicator.attachToRecyclerView(recyclerView);
        new SimpleSnapHelper(indicator).attachToRecyclerView(recyclerView);
        recyclerView.postDelayed(runnableAutoScroll, isSlideBanner ? CHANGE_SLIDE_BANNER_TIME : Constants.CHANGE_BANNER_TIME);
        recyclerView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                recyclerView.removeCallbacks(runnableAutoScroll);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                recyclerView.removeCallbacks(runnableAutoScroll);
                recyclerView.postDelayed(runnableAutoScroll, isSlideBanner ? CHANGE_SLIDE_BANNER_TIME : Constants.CHANGE_BANNER_TIME);
            }
            return false;
        });
    }

    @Override
    public void bindData(Object item, int position) {
        if (data == null) data = new ArrayList<>();
        else data.clear();
        if (item instanceof HomeContent) {
            data.addAll(((HomeContent) item).getListFlashHot());
        } else if (item instanceof TabHomeModel) {
            data.addAll(((TabHomeModel) item).getList());
        } else if (item instanceof MoviePagerModel) {
            data.addAll(((MoviePagerModel) item).getList());
        }
        if (adapter != null) adapter.notifyDataSetChanged();
        if (layoutManager != null && recyclerView != null) {
            layoutManager.smoothScrollToPosition(recyclerView, null, 0);
        }
        if (indicator != null) {
            indicator.onPageSelected(0);
        }
    }

    public RecyclerView getRecycler() {
        return recyclerView;
    }
}
package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsPushInfoPartnerId extends BaseRequest<WsPushInfoPartnerId.Request> {
    public static class Request {
        @SerializedName("partnerId")
        private String partnerId;

        @SerializedName("deviceId")
        private String deviceId;

        @SerializedName("versionApp")
        private String versionApp;

        @SerializedName("versionDevice")
        private String versionDevice;

        @SerializedName("deviceOS")
        private String deviceOS;

        @SerializedName("modelDevice")
        private String modelDevice;

        @SerializedName("camId")
        private String camId;

        public String getPartnerId() {
            return partnerId;
        }

        public void setPartnerId(String partnerId) {
            this.partnerId = partnerId;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getVersionApp() {
            return versionApp;
        }

        public void setVersionApp(String versionApp) {
            this.versionApp = versionApp;
        }

        public String getVersionDevice() {
            return versionDevice;
        }

        public void setVersionDevice(String versionDevice) {
            this.versionDevice = versionDevice;
        }

        public String getDeviceOS() {
            return deviceOS;
        }

        public void setDeviceOS(String deviceOS) {
            this.deviceOS = deviceOS;
        }

        public String getModelDevice() {
            return modelDevice;
        }

        public void setModelDevice(String modelDevice) {
            this.modelDevice = modelDevice;
        }

        public String getCamId() {
            return camId;
        }

        public void setCamId(String camId) {
            this.camId = camId;
        }
    }
}

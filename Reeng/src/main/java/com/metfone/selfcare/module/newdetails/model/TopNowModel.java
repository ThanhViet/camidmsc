package com.metfone.selfcare.module.newdetails.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HaiKE on 9/11/17.
 */

public class TopNowModel implements Serializable {
    @SerializedName("CategoryID")
    @Expose
    private int categoryID;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;

    @SerializedName("Color")
    @Expose
    private String color;

    @SerializedName("Icon")
    @Expose
    private String icon;

    @SerializedName("Url")
    @Expose
    private String url;

    @SerializedName("TypeDisplay")
    @Expose
    private int typeDisplay;

    @SerializedName("data")
    @Expose
    private ArrayList<NewsModel> data = new ArrayList<>();
    private final static long serialVersionUID = -7706989715033118800L;

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<NewsModel> getData() {
        return data;
    }

    public void setData(ArrayList<NewsModel> data) {
        this.data = data;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getTypeDisplay() {
        return typeDisplay;
    }

    public void setTypeDisplay(int typeDisplay) {
        this.typeDisplay = typeDisplay;
    }

    @Override
    public String toString() {
        return "TopNowModel{" +
                "categoryID=" + categoryID +
                ", categoryName='" + categoryName + '\'' +
                ", data=" + data +
                '}';
    }
}

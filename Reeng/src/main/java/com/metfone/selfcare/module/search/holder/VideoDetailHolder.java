/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.utils.ImageBusiness;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.OnClick;

public class VideoDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_description)
    @Nullable
    TextView tvDescription;
    @BindView(R.id.tv_view_all)
    @Nullable
    TextView tvViewAll;
    @BindView(R.id.tv_channel)
    @Nullable
    TextView tvChannel;
    @BindView(R.id.tv_duration)
    @Nullable
    TextView tvDuration;
    @BindView(R.id.tv_view)
    @Nullable
    TextView tvView;
    @BindView(R.id.tv_created_date)
    @Nullable
    TextView tvCreateDate;
    @BindView(R.id.cv_duration)
    @Nullable
    CardView cvDuration;

    private SearchAllListener.OnAdapterClick listener;
    private Video data;

    public VideoDetailHolder(View view, Activity activity, SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
    }

    public void bindData(Object item, int position, String keySearch) {
        if (item instanceof Video) {
            data = (Video) item;
            if (tvViewAll != null) tvViewAll.setVisibility(View.GONE);
            if (tvTitle != null) {
                tvTitle.setVisibility(View.VISIBLE);
                tvTitle.setText(data.getTitle());
            }
            if (tvDescription != null) {
                tvDescription.setText(data.getDescription());
                tvDescription.setVisibility(TextUtils.isEmpty(data.getDescription()) ? View.GONE : View.VISIBLE);
            }
            if (tvChannel != null && data.getChannel() != null && data.getChannel().getName() != null) {
                tvChannel.setVisibility(View.VISIBLE);
                tvChannel.setText(data.getChannel().getName().trim());
            }
            if (cvDuration != null && tvDuration != null && !TextUtils.isEmpty(data.getDuration())) {
                cvDuration.setVisibility(View.VISIBLE);
                tvDuration.setText(data.getDuration());
            }
            if (tvView != null) {
                tvView.setVisibility(View.VISIBLE);
                tvView.setText(String.format((data.getTotalView() <= 1) ? tvView.getContext().getString(R.string.view)
                        : tvView.getContext().getString(R.string.video_views), Utilities.getTotalView(data.getTotalView())));
            }
            if (tvCreateDate != null) {
                tvCreateDate.setVisibility(View.VISIBLE);
                if (data.getTimeCreate() > 0) {
                    tvCreateDate.setText(DateTimeUtils.calculateTime(tvView.getContext().getResources(), data.getTimeCreate()));
                } else if (data.getPublishTime() > 0) {
                    tvCreateDate.setText(DateTimeUtils.calculateTime(tvView.getContext().getResources(), data.getPublishTime()));
                }
            }
            ImageBusiness.setVideo(data.getImagePath(), ivCover, 11);
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxVideo && data != null) {
            ((SearchAllListener.OnClickBoxVideo) listener).onClickVideoItem(data);
        }
    }

    @OnClick(R.id.iv_more)
    public void onClickItemMore() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            listener.onClickMenuMore(data);
        }
    }
}
package com.metfone.selfcare.module.keeng.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener;

/**
 * Created by namnh40 on 2/22/2017.
 */

public class ChildCategoryHomeHolder extends BaseHolder {
    public ImageView image;
    public TextView tvTitle;

    public ChildCategoryHomeHolder(View itemView, final BaseListener.OnClickMedia listener) {
        super(itemView, listener);
        image = (ImageView) itemView.findViewById(R.id.image);
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onTopicClick(view, getAdapterPosition());
            }
        });
    }

}

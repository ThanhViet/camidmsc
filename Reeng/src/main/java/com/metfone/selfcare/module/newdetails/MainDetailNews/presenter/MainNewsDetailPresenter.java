package com.metfone.selfcare.module.newdetails.MainDetailNews.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.module.netnews.request.NewsContentRequest;
import com.metfone.selfcare.module.newdetails.MainDetailNews.fragment.MainNewsDetailFragment;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.FilterContentSeenUtil;

public class MainNewsDetailPresenter implements IMainNewsDetailPresenter {
    public static final String TAG = MainNewsDetailPresenter.class.getSimpleName();
    private MvpView mMvpView;
    private NetNewsApi mNewsApi;
    long startTime;

    private FilterContentSeenUtil filterContentSeenUtil;

    public MainNewsDetailPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadDataRelate(NewsModel model, int page, long unixTime) {
        startTime = System.currentTimeMillis();
        mNewsApi.getNewsRelate(model, page, unixTime, mLoadRelateCallback);
    }

    @Override
    public void loadDataRelateFromCategory(NewsModel newsModel, int page, long unixTime) {
        startTime = System.currentTimeMillis();
        mNewsApi.loadDataRelateFromCategory(new NewsContentRequest(newsModel.getPid(), newsModel.getID(), page, unixTime), mLoadRelateCallback);
    }

    @Override
    public void loadDataRelateFromCategoryPosition0(NewsModel newsModel, int page, long unixTime) {
        startTime = System.currentTimeMillis();
        mNewsApi.loadDataRelateFromCategoryPosition0(new NewsContentRequest(newsModel.getPid(), newsModel.getID(), page, unixTime), mLoadRelateCallback);
    }

    @Override
    public void loadDataRelateFromEvent(NewsModel newsModel, int page, long unixTime) {
        startTime = System.currentTimeMillis();
        mNewsApi.getNewsRelateFromEvent(new NewsContentRequest(newsModel.getPid(), newsModel.getCid(), newsModel.getID(), page, unixTime), mLoadRelateCallback);
    }

    @Override
    public void addNewModelSeen(String id) {
        getFilterContentSeenUtil().addContentSeen(id);
    }

    @Override
    public void onAttach(MvpView mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
        if (filterContentSeenUtil != null) {
            filterContentSeenUtil = null;
        }
    }

    @Override
    public void handleApiError(Exception error) {

    }

    @Override
    public void setUserAsLoggedOut() {

    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    HttpCallBack mLoadRelateCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(mMvpView instanceof MainNewsDetailFragment)) {
                return;
            }
            Gson gson = new Gson();
            NewsResponse childNewsResponse = gson.fromJson(data, NewsResponse.class);
            // filter
            childNewsResponse.setData(getFilterContentSeenUtil().filterList(childNewsResponse.getData()));
            ((MainNewsDetailFragment) mMvpView).bindDataRelate(childNewsResponse);
            ((MainNewsDetailFragment) mMvpView).loadDataSuccess(true);

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_RELATE_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            long endTime = System.currentTimeMillis();
            Log.d(TAG, "loadRelate: onFailure - " + message);
            if (!isViewAttached() || !(mMvpView instanceof MainNewsDetailFragment)) {
                return;
            }
            ((MainNewsDetailFragment) mMvpView).loadDataSuccess(false);

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_GET_RELATE_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    public FilterContentSeenUtil getFilterContentSeenUtil() {
        if (filterContentSeenUtil == null) {
            filterContentSeenUtil = new FilterContentSeenUtil(Constants.CacheSeen.CACHE_IDS_NEWS);
        }
        return filterContentSeenUtil;
    }
}
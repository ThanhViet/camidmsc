package com.metfone.selfcare.module.home_kh.nearfriend;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;

public class NearYouKhHolder extends BaseViewHolder {
    private static final String TAG = NearYouKhHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private NearYouKhAdapter.NearYouInterface mListener;
    private Context mContext;
    private Resources mRes;
    private StrangerMusic mEntry;
    private View parentView;
    private RoundedImageView mImgAvatar;
    private AppCompatImageView ivGender;
    private AppCompatTextView mTvwName, mTvwInfo;
    private AppCompatTextView mTvwAvatar, mTvwOld, mTvwDistance;
    //    private ImageView mImgGender, mImgHear;
    private RoundTextView btnMessage, btnProfile;

    public NearYouKhHolder(View convertView, Context context, NearYouKhAdapter.NearYouInterface listener) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mListener = listener;
        //
        parentView = convertView.findViewById(R.id.rootView);
        mImgAvatar = convertView.findViewById(R.id.ivAvatar);
        mTvwName = convertView.findViewById(R.id.tvName);
        mTvwInfo = convertView.findViewById(R.id.tvStatus);
//        mTvwAvatar = convertView.findViewById(R.id.holder_avatar_text);
        mTvwOld = convertView.findViewById(R.id.tvAge);
        mTvwDistance = convertView.findViewById(R.id.tvDistance);
        btnMessage = convertView.findViewById(R.id.btnMessage);
        btnProfile = convertView.findViewById(R.id.btnPersonalPage);
        ivGender = convertView.findViewById(R.id.ivGender);
        btnMessage.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClickMessage(mEntry);
            }
        });
        btnProfile.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClickPoster(mEntry);
            }
        });
//        mImgGender =  convertView.findViewById(R.id.holder_ic_gender);
//        mImgHear =  convertView.findViewById(R.id.holder_ic_hear);
    }

    @Override
    public void setElement(Object obj) {
        mEntry = (StrangerMusic) obj;
        drawHolderDetail();
        setListener();
    }

    private void setListener() {
       /* mBtnListen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onClickListen(mEntry);
            }
        });*/
        parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClickPoster(mEntry);
                }
            }
        });
    }

    private void drawHolderDetail() {
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        MediaModel songModel = mEntry.getSongModel();
//        if (songModel != null) {
//            mImgHear.setVisibility(View.VISIBLE);
//            mTvwInfo.setText(songModel.getSongAndSinger());
//            mTvwInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//        } else {
//            mImgHear.setVisibility(View.GONE);
        mTvwInfo.setText(mEntry.getInfo());
//            mTvwInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_quote_left, 0, R.drawable.ic_quote_right, 0);
//        }
       /* mTvwInfo.setText(songAndSinger);
        if (TextUtils.isEmpty(songAndSinger)) {
            mTvwInfo.setText(mEntry.getInfo());
        } else {
            mTvwInfo.setText(songAndSinger);
        }*/
        mTvwName.setText(mEntry.getPosterName());
        if (TextUtils.isEmpty(mEntry.getAgeStr())) {
            mTvwOld.setText("");
        } else {
            mTvwOld.setText(mEntry.getAgeStr());
        }
        mTvwDistance.setText(mEntry.getDistanceStr());
//        if (mEntry.getPosterGender() == Constants.CONTACT.GENDER_MALE) {
//            mImgGender.setImageResource(R.drawable.ic_around_gender_male);
        //mImgGender.setColorFilter(ContextCompat.getColor(mContext, R.color.gender_male));
//        } else {
//            mImgGender.setImageResource(R.drawable.ic_around_gender_female);
        //mImgGender.setColorFilter(ContextCompat.getColor(mContext, R.color.gender_female));
//        }
        if (mEntry.getPosterGender() == Constants.CONTACT.GENDER_MALE) {
            ivGender.setImageResource(R.drawable.ic_kh_men);
        } else {
            ivGender.setImageResource(R.drawable.ic_kh_girl);
        }
        int sizeAvatar = (int) mRes.getDimension(R.dimen.avatar_small_size);
        //String myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();

        avatarBusiness.setStrangerAvatar(mImgAvatar, null, null, mEntry.getPosterJid(),
                mEntry.getPosterName(), mEntry.getPosterLastAvatar(), sizeAvatar);
    }
}
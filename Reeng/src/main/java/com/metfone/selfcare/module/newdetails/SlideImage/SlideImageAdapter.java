package com.metfone.selfcare.module.newdetails.SlideImage;

import android.app.Activity;
import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.glide.ImageLoader;

import java.util.List;

public class SlideImageAdapter extends PagerAdapter {

    private Activity _activity;
    private List<String> _imagePaths;
    private LayoutInflater inflater;

    // constructor
    public SlideImageAdapter(Activity activity, List<String> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        ImageView ivDisPlayGif;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.item_slide_image, container,
                false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        ivDisPlayGif = viewLayout.findViewById(R.id.ivDisplayGif);
        if (_imagePaths.get(position).contains(".gif")) {
            imgDisplay.setVisibility(View.GONE);
            ivDisPlayGif.setVisibility(View.VISIBLE);
            ImageLoader.setNewImageGif(_activity, _imagePaths.get(position), ivDisPlayGif);
        } else {
            imgDisplay.setVisibility(View.VISIBLE);
            ivDisPlayGif.setVisibility(View.GONE);
            ImageLoader.setNewsImageCache(_activity, _imagePaths.get(position), imgDisplay);
        }

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }

}

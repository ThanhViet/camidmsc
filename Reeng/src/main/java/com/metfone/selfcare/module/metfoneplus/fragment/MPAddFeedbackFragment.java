package com.metfone.selfcare.module.metfoneplus.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.model.LatLng;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.model.camid.TypeComplaint;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPLocationServiceRequestDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.NumberPickerBottomSheetDialogFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.WsSubmitComplaintMyMetfoneRequest;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComTypeResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubmitComplaintMyMetfoneResponse;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.metfone.selfcare.helper.Constants.PERMISSION.PERMISSION_REQUEST_LOCATION;

public class MPAddFeedbackFragment extends MPBaseFragment implements MPMapsFragment.OnMapListener {

    @BindView(R.id.layout_content)
    ScrollView mLayoutContent;
    @BindView(R.id.m_p_feedback_us_error_type)
    AppCompatTextView mErrorTypeName;
    @BindView(R.id.m_p_feedback_us_arrow_down)
    AppCompatImageView mImgArrowDown;
    @BindView(R.id.m_p_feedback_us_error_phone)
    EditText mFeedbackErrorPhone;
    @BindView(R.id.m_p_feedback_us_your_phone)
    EditText mFeedbackYourPhone;
    @BindView(R.id.m_p_feedback_us_your_email)
    AppCompatEditText mFeedbackYourEmail;
    @BindView(R.id.m_p_feedback_us_your_feedback)
    AppCompatEditText mFeedbackYourFeedback;
    @BindView(R.id.m_p_feedback_us_txt_title_upload_photo)
    AppCompatTextView mFeedbackTxtTitleUploadPhoto;
    @BindView(R.id.m_p_feedback_us_txt_location)
    AppCompatTextView mFeedbackTxtLocation;
    @BindView(R.id.m_p_feedback_us_layout_map_view)
    RelativeLayout mFeedbackLayoutMapView;
    @BindView(R.id.layout_upload_photo)
    RelativeLayout mFeedbackLayoutUploadPhoto;
    @BindView(R.id.m_p_feedback_us_ic_layout_content_location)
    RelativeLayout mFeedbackLayoutLocationAddress;
    @BindView(R.id.layout_upload_photo_with_content)
    RelativeLayout mFeedbackLayoutUploadPhotoWithContent;
    @BindView(R.id.m_p_feedback_us_img_delete_photo)
    AppCompatImageView mFeedbackImgDeletePhoto;
    @BindView(R.id.layoutParent)
    CoordinatorLayout layoutParent;

    private MPMapsFragment mMPMapsFragment;

    private String mImageEncodeBase64 = "";
    private List<TypeComplaint> mComplaintList;
    private TypeComplaint mTypeComplaintSelected = null;
    private boolean mIsBottomSheetShowing = false;
    private int mPositionSelected = 0;

    private String mLatitude = null;
    private String mLongitude = null;

    public MPAddFeedbackFragment() {

    }

    public static MPAddFeedbackFragment newInstance() {
        MPAddFeedbackFragment mpAddFeedbackFragment = new MPAddFeedbackFragment();
        return mpAddFeedbackFragment;
    }

    private static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(Objects.requireNonNull(drawable))).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(Objects.requireNonNull(drawable).getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_add_feedback_us;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInsertBottom(layoutParent);
        setupUI(layoutParent);
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.GONE);
        }

        mLatitude = String.valueOf(Constants.INVALID_LAT);
        mLongitude = String.valueOf(Constants.INVALID_LNG);

        mLayoutContent.setVisibility(View.VISIBLE);
        mFeedbackLayoutMapView.setVisibility(View.VISIBLE);
        mFeedbackErrorPhone.setText(0 + getCamIdUserBusiness().getMetfoneUsernameIsdn().substring(0, 2) + " " + getCamIdUserBusiness().getMetfoneUsernameIsdn().substring(2));
        mFeedbackYourPhone.setText(0 + getCamIdUserBusiness().getMetfoneUsernameIsdn().substring(0, 2) + " " + getCamIdUserBusiness().getMetfoneUsernameIsdn().substring(2));

        handleUploadPhoto(false, "");

        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_LOCATION);
        } else {
            initMap();
            getComType();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void showSelectionTypeBottomSheet() {
        if (!isAdded() || mComplaintList == null || mComplaintList.size() == 0 || mIsBottomSheetShowing) {
            return;
        }

        List<String> selectionTypeStrings = new ArrayList<>();
        for (TypeComplaint t : mComplaintList) {
            selectionTypeStrings.add(t.getName());
        }

        String[] value = selectionTypeStrings.toArray(new String[0]);
        String title = getString(R.string.m_p_add_feedback_us_selection_type);
        NumberPickerBottomSheetDialogFragment changePhoneBS = NumberPickerBottomSheetDialogFragment.newInstance(title, value, mPositionSelected);
        changePhoneBS.setOnNumberPickerBottomSheetOnClick(new NumberPickerBottomSheetDialogFragment.OnNumberPickerBottomSheetOnClick() {
            @Override
            public void onDone(String valueSelected, int position) {
                android.util.Log.e(TAG, "onDone: valueSelected = " + valueSelected + " -- " + mComplaintList.get(position).toString());
                mPositionSelected = position;
                mTypeComplaintSelected = mComplaintList.get(position);
                mErrorTypeName.setText(mTypeComplaintSelected.getName());
            }

            @Override
            public void onDismiss() {
                mIsBottomSheetShowing = false;
            }
        });
        mIsBottomSheetShowing = true;
        changePhoneBS.show(mParentActivity.getSupportFragmentManager(), changePhoneBS.getClass().getSimpleName());
    }

    private void initMap() {
        mMPMapsFragment = MPMapsFragment.newInstance();
        mMPMapsFragment.setOnTaskCompleted(this);
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.m_p_feedback_us_map_layout, mMPMapsFragment, MPMapsFragment.TAG);
        fragmentTransaction.commitAllowingStateLoss();
        mFeedbackLayoutMapView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        if (mMPMapsFragment != null) {
            mMPMapsFragment.removeLocationCallback();
            mMPMapsFragment.setOnTaskCompleted(null);
            mMPMapsFragment = null;
        }
        super.onDestroyView();
    }

    public void onBack() {
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        } else {
            mParentActivity.finish();
            return;
        }
        popBackStackFragment();
    }

    @OnClick({R.id.m_p_feedback_us_error_type, R.id.m_p_feedback_us_arrow_down})
    void openSelectionType() {
        showSelectionTypeBottomSheet();
    }

    @OnClick({R.id.m_p_feedback_us_img_upload_photo_1,
            R.id.m_p_feedback_us_img_upload_photo_2,
            R.id.m_p_feedback_us_txt_upload_photo,
            R.id.m_p_feedback_us_txt_title_upload_photo})
    void uploadPhotoImage() {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    Constants.PERMISSION.PERMISSION_READ_EXTERNAL_STORAGE);
        } else {
            openGallery();
        }
    }

    @OnClick(R.id.m_p_feedback_us_ic_my_location)
    void myLocation() {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_LOCATION);
        } else {
            if (!Utilities.isLocationEnabled(mParentActivity)) {
                MPLocationServiceRequestDialog dialog = MPLocationServiceRequestDialog.newInstance();
                dialog.show(getParentFragmentManager(), MPLocationServiceRequestDialog.TAG);
                return;
            }
            mMPMapsFragment.getMyLocation();
        }
    }

    @OnClick(R.id.m_p_feedback_us_img_delete_photo)
    void deletePhoto() {
        if (mImageEncodeBase64 != null) {
            mImageEncodeBase64 = "";
        }
        handleUploadPhoto(false, "");
    }

    @OnClick(R.id.m_p_feedback_us_send_feedback)
    void sendFeedback() {
        if (TextUtils.isEmpty(mFeedbackYourPhone.getText())) {
            Toast.makeText(mParentActivity, getString(R.string.m_p_add_feedback_us_your_phone_number_error), Toast.LENGTH_LONG).show();
            return;
        }

        if (!TextUtils.isEmpty(mFeedbackYourEmail.getText().toString().trim())) {
            if (!Utilities.isValidEmail(mFeedbackYourEmail.getText().toString().trim())) {
                Toast.makeText(mParentActivity, getString(R.string.m_p_add_feedback_us_your_email_invalidate), Toast.LENGTH_LONG).show();
                return;
            }
        }

        if (TextUtils.isEmpty(mFeedbackYourFeedback.getText())) {
            Toast.makeText(mParentActivity, getString(R.string.m_p_add_feedback_us_your_feedback), Toast.LENGTH_LONG).show();
            return;
        }

        wsSubmitComplaintMyMetfone(mTypeComplaintSelected);
    }

    private void openGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.ACTION.ACTION_PICK_PICTURE) {
                if (data != null) {
                    Uri uri = data.getData();
                    String fileName = "";
                    if (ContentResolver.SCHEME_FILE.equals(Objects.requireNonNull(uri).getScheme())) {
                        File file = new File(Objects.requireNonNull(uri.getPath()));
                        fileName = file.getName();
                    } else if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) { // Content Scheme.
                        Cursor returnCursor =
                                mParentActivity.getContentResolver().query(uri, null, null, null, null);
                        if (returnCursor != null && returnCursor.moveToFirst()) {
                            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            fileName = returnCursor.getString(nameIndex);
                            returnCursor.close();
                        }
                    }
                    handleUploadPhoto(true, fileName);

                    try {
                        Bitmap selectedImage = MediaStore.Images.Media.getBitmap(mParentActivity.getContentResolver(), uri);
                        new EncodeToBase64Task(selectedImage, (string) -> {
                            mImageEncodeBase64 = string;
                        }).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constants.PERMISSION.PERMISSION_READ_EXTERNAL_STORAGE) {
            if (PermissionHelper.allowedPermission(mParentActivity.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                openGallery();
            } else {
                android.util.Log.e(TAG, "onRequestPermissionsResult: READ_EXTERNAL_STORAGE denied");
            }
        } else if (requestCode == PERMISSION_REQUEST_LOCATION) {
            if (PermissionHelper.allowedPermission(mParentActivity.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                initMap();
            }

            getComType();
        }
    }

    /**
     * Get content for complaint spinner
     */
    private void getComType() {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient().wsGetComType(new MPApiCallback<WsGetComTypeResponse>() {
            @Override
            public void onResponse(Response<WsGetComTypeResponse> response) {
                if (response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null) {
                    if ("0".equals(response.body().getResult().getErrorCode())) {
                        mComplaintList = response.body().getResult().getWsResponse().getTypeComplaints();
                        if (mComplaintList != null) {
                            mTypeComplaintSelected = mComplaintList.get(mPositionSelected);
                            mErrorTypeName.setText(mTypeComplaintSelected.getName());
                            mLayoutContent.setVisibility(View.VISIBLE);
                        }
                    } else {
                        handleShowErrorDialog(response);
                    }
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void wsSubmitComplaintMyMetfone(TypeComplaint typeComplaint) {
        mParentActivity.showLoadingDialog("", R.string.waiting);

        WsSubmitComplaintMyMetfoneRequest request = new WsSubmitComplaintMyMetfoneRequest();

        String yourFeedback = Objects.requireNonNull(mFeedbackYourFeedback.getText()).toString().trim().replace(" ","");
        String yourEmail = Objects.requireNonNull(mFeedbackYourEmail.getText()).toString().trim();
        String errorPhone = Objects.requireNonNull(mFeedbackErrorPhone.getText()).toString().trim();
        String yourComplainerPhone = Objects.requireNonNull(mFeedbackYourPhone.getText()).toString().trim();

        String compContent = request.createCompContent(typeComplaint.getCompTemplate(), mLatitude, mLongitude, yourFeedback, yourEmail);

        new MetfonePlusClient().wsSubmitComplaintMyMetfone("", "",mImageEncodeBase64, compContent, String.valueOf(typeComplaint.getCompTypeId()), yourComplainerPhone,errorPhone,
                new MPApiCallback<WsSubmitComplaintMyMetfoneResponse>() {
                    @Override
                    public void onResponse(Response<WsSubmitComplaintMyMetfoneResponse> response) {
                        if (response != null && response.body() != null && response.code() == 200 && response.body().getResult() != null) {
                            if (response.body().getResult().getErrorCode().equals("0")) {
                                MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                                        .setTitle(R.string.m_p_add_feedback_us_send_notification_title)
                                        .setContent(R.string.m_p_add_feedback_us_send_notification_content)
                                        .setLottieAssertName("lottie/img_dialog_women_success.json")
                                        .setTimeDelayAutoDismiss(3000)
                                        .build();
                                dialogSuccess.show(getChildFragmentManager(), null);
                                dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
                                    @Override
                                    public void onDialogDismiss() {
                                        if (mParentActivity instanceof DeepLinkActivity) {
                                            mParentActivity.finish();
                                            return;
                                        }
                                        popBackStackFragment();
                                    }
                                });
                            } else {
                                Toast.makeText(getContext(), R.string.service_error, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), R.string.service_error, Toast.LENGTH_SHORT).show();
                        }
                        mParentActivity.hideLoadingDialog();
                    }

                    @Override
                    public void onError(Throwable error) {
                        mParentActivity.hideLoadingDialog();
                        Toast.makeText(getContext(), R.string.service_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void handleUploadPhoto(boolean isImageSelected, String titlePhoto) {
        if (isImageSelected) {
            mFeedbackLayoutUploadPhoto.setVisibility(View.GONE);
            mFeedbackTxtTitleUploadPhoto.setText(titlePhoto);
            mFeedbackLayoutUploadPhotoWithContent.setVisibility(View.VISIBLE);
        } else {
            mFeedbackLayoutUploadPhoto.setVisibility(View.VISIBLE);
            mFeedbackTxtTitleUploadPhoto.setText(titlePhoto);
            mFeedbackLayoutUploadPhotoWithContent.setVisibility(View.GONE);
        }
    }

    @Override
    public void onGetAddressCompleted(String result) {
        mFeedbackLayoutLocationAddress.setVisibility(View.VISIBLE);
        mFeedbackTxtLocation.setText(result);
        mMPMapsFragment.removeLocationCallback();
    }

    @Override
    public void onGetLocationCompleted(LatLng latLng) {
        mLatitude = String.valueOf(latLng.latitude);
        mLongitude = String.valueOf(latLng.longitude);
        android.util.Log.e(TAG, "onGetLocationCompleted: lat = " + latLng.latitude);
    }

    @Override
    public void onRequestLocationPermission() {

    }

    private void handleShowErrorDialog(Response<WsGetComTypeResponse> response) {
        String message = response.body().getResult().getUserMsg();
        if (message == null) {
            message = response.body().getResult().getMessage();
            if (message == null) {
                message = getString(R.string.m_p_no_data);
            }
        }
        MPDialogFragment errorDialog = new MPDialogFragment.Builder()
                .setTitle(R.string.m_p_dialog_error_title)
                .setContent(message)
                .setCancelableOnTouchOutside(true)
                .build();
        errorDialog.show(getChildFragmentManager(), null);

        errorDialog.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onDialogDismiss() {
                popBackStackFragment();
            }
        });
    }

    public void setupUI(View view) {

        if (!(view instanceof CoordinatorLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    interface OnBitmapEncodeListener {
        void onSuccess(String string);
    }

    @SuppressLint("StaticFieldLeak")
    static class EncodeToBase64Task extends AsyncTask<Void, Void, String> {
        private Bitmap bitmap;
        private MPAddFeedbackFragment.OnBitmapEncodeListener listener;

        EncodeToBase64Task(Bitmap bitmap, MPAddFeedbackFragment.OnBitmapEncodeListener listener) {
            this.bitmap = bitmap;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(Void... voids) {
            ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayBitmapStream);
            byte[] b = byteArrayBitmapStream.toByteArray();
            return Base64.encodeToString(b, Base64.NO_WRAP);

        }

        @Override
        protected void onPostExecute(String s) {
            listener.onSuccess(s);
        }
    }
}

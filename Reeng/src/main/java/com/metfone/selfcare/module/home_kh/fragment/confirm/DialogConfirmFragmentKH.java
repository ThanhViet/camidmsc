package com.metfone.selfcare.module.home_kh.fragment.confirm;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;

public class DialogConfirmFragmentKH extends DialogFragment {

    private static final String TITLE = "TITLE";
    private static final String CONTENT = "CONTENT";
    private AppCompatTextView title;
    private AppCompatTextView content;
    private AppCompatTextView confirm;
    private AppCompatTextView cancel;
    private IDialogConfirm iDialogConfirm;

    public void setInterface(IDialogConfirm iDialogConfirm) {
        this.iDialogConfirm = iDialogConfirm;
    }

    public static DialogConfirmFragmentKH newInstance(String title, String content) {
        DialogConfirmFragmentKH dialog = new DialogConfirmFragmentKH();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(CONTENT, content);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            if (dialog.getWindow() != null) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dialog_confirm, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title = view.findViewById(R.id.title);
        content = view.findViewById(R.id.content);
        confirm = view.findViewById(R.id.confirm);
        cancel = view.findViewById(R.id.cancel);
        Bundle bundle = getArguments();
        if (bundle != null) {
            title.setText(bundle.getString(TITLE, ""));
            content.setText(bundle.getString(CONTENT, ""));
        }
        confirm.setOnClickListener(view12 -> {
            if (iDialogConfirm != null) {
                iDialogConfirm.confirm();
            }
            dismiss();
        });
        cancel.setOnClickListener(view1 -> dismiss());

    }

    @Override
    public void onResume() {
        if (getDialog() != null) {
            ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        }
        super.onResume();
    }

    public interface IDialogConfirm {
        void confirm();
    }
//    byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
//    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
}
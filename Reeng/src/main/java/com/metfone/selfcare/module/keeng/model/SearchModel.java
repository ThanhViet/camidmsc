/*
 * Copyright (c) 2017.
 * www.bigzun.com
 */

package com.metfone.selfcare.module.keeng.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.utils.Utilities;

import java.io.Serializable;

/**
 * Created by namnh40 on 8/10/2016.
 */

public class SearchModel implements Serializable {
    public static final String TYPE_SINGER = "singer";
    public static final String TYPE_ALBUM = "album";
    public static final String TYPE_SONG = "song";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_PLAYLIST = "playlist";
    public static final String TYPE_MOVIES = "movies";
    public static final String TYPE_YOUTUBE = "youtube";
    public static final String TYPE_ALBUM_VIDEO = "album-video";
    public static final String TYPE_PLAYLIST_VIDEO = "playlist-video";

    private static final long serialVersionUID = -2203319522942390454L;
    @SerializedName("id")
    public String id;

    @SerializedName("full_name")
    public String fullName;

    @SerializedName("type")
    public String type;

    @SerializedName("image")
    public String image;

    @SerializedName("score")
    public float score;

    @SerializedName("search_info")
    public String searchInfo;

    @SerializedName("listen_no")
    public long listenNo;

    @SerializedName("full_singer")
    public String fullSinger;

    @SerializedName("is_singer")
    public int isSinger;

    @SerializedName("is_lossless")
    public int isLossless;

    @SerializedName("description")
    private String description;

    @SerializedName("link")
    private String link;

    @SerializedName("name")
    private String name;

    @SerializedName("image_banner")
    private String posterPath;

    @SerializedName("hd_Price")
    private String hdPrice = "";

    @SerializedName("hd_price_FreeData")
    private String hdFreeDataPrice = "";

    @SerializedName("sd_price")
    private String sdPrice = "";

    @SerializedName("sd_price_FreeData")
    private String sdFreeDataPrice = "";

    @SerializedName("type_film")
    private String typeFilm = "";

    @SerializedName("label_url")
    private String labelUrl = "";

    @SerializedName("isFreeContent")
    private String isFreeContent = "";

    @SerializedName("isFreeData")
    private String isFreeData = "";

    @SerializedName("label_txt")
    private String labelText = "";

    public SearchModel() {
        super();
    }

    public SearchModel(String text) {
        super();
        this.fullName = text;
    }

    public SearchModel(SearchModel item) {
        if (item != null) {
            this.id = item.id;
            this.fullName = item.fullName;
            this.type = item.type;
            this.image = item.image;
            this.score = item.score;
            this.searchInfo = item.searchInfo;
            this.listenNo = item.listenNo;
            this.fullSinger = item.fullSinger;
            this.isSinger = item.isSinger;
            this.isLossless = 0;
        }
    }

    public String getFullName() {
        if (TextUtils.isEmpty(fullName))
            return "";
        return fullName.trim();
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullSinger() {
        if (TextUtils.isEmpty(fullSinger))
            return "";
        return fullSinger.trim();
    }

    public void setFullSinger(String fullSinger) {
        this.fullSinger = fullSinger;
    }

    public String getKeyCoincide() {
        String text = getFullName();
        if (!TextUtils.isEmpty(text)) {
            if (!TextUtils.isEmpty(getFullSinger()) && !text.contains(getFullSinger()))
                text += " + " + getFullSinger();
        } else
            text = getFullSinger();

        String result = text;
        if (!TextUtils.isEmpty(text)) {
            result = text.substring(0, 1).toUpperCase() + text.substring(1);
            result = result.trim();
        }

        return result;
    }

    public String getKeyCheck() {
        String text = getFullName();
        if (!TextUtils.isEmpty(text)) {
            if (!TextUtils.isEmpty(getFullSinger()) && !text.contains(getFullSinger()))
                text += " " + getFullSinger();
        } else
            text = getFullSinger();

        return text.trim();
    }

    public long getListened() {
        return listenNo;
    }

    public void setListened(long listened) {
        this.listenNo = listened;
    }

    public String getListenNo() {
        return Utilities.formatNumber(listenNo);
    }

    public int getIsLossless() {
        return isLossless;
    }

    public void setIsLossless(int isLossless) {
        this.isLossless = isLossless;
    }

    public boolean isLossless() {
        return isLossless == 1;
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "SearchModel [id=" + id + ", listen_no=" + listenNo + ", score=" + score + ", type=" + type + ", fullName=" + fullName + ", fullSinger=" + fullSinger + ", isSinger=" + isSinger + "]";
    }

    public String getSearchInfo() {
        if (TextUtils.isEmpty(searchInfo))
            return "";
        return searchInfo.trim();
    }

    public int getType() {
        if (!TextUtils.isEmpty(type))
            if (type.equals(TYPE_SINGER)) {
                return Constants.TYPE_SINGER;
            } else if (type.equals(TYPE_ALBUM)) {
                return Constants.TYPE_ALBUM;
            } else if (type.equals(TYPE_VIDEO)) {
                return Constants.TYPE_VIDEO;
            } else if (type.equals(TYPE_SONG)) {
                return Constants.TYPE_SONG;
            } else if (type.equals(TYPE_PLAYLIST)) {
                return Constants.TYPE_PLAYLIST;
            } else if (type.equals(TYPE_MOVIES)) {
                return Constants.TYPE_MOVIE;
            } else if (type.equals(TYPE_YOUTUBE)) {
                return Constants.TYPE_YOUTUBE;
            }
        return Constants.TYPE_FAKE_SOMETHING;
    }

    public SearchModel setType(String type) {
        this.type = type;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHdPrice() {
        return hdPrice;
    }

    public void setHdPrice(String hdPrice) {
        this.hdPrice = hdPrice;
    }

    public String getHdFreeDataPrice() {
        return hdFreeDataPrice;
    }

    public void setHdFreeDataPrice(String hdFreeDataPrice) {
        this.hdFreeDataPrice = hdFreeDataPrice;
    }

    public Integer getIdInt() {
        try {
            return Integer.parseInt(id);
        } catch (Exception e) {

        }
        return 0;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getSdPrice() {
        return sdPrice;
    }

    public void setSdPrice(String sdPrice) {
        this.sdPrice = sdPrice;
    }

    public String getSdFreeDataPrice() {
        return sdFreeDataPrice;
    }

    public void setSdFreeDataPrice(String sdFreeDataPrice) {
        this.sdFreeDataPrice = sdFreeDataPrice;
    }

    public String getTypeFilm() {
        return typeFilm;
    }

    public void setTypeFilm(String typeFilm) {
        this.typeFilm = typeFilm;
    }

    public String getLabelUrl() {
        return labelUrl;
    }

    public void setLabelUrl(String labelUrl) {
        this.labelUrl = labelUrl;
    }

    public String getIsFreeContent() {
        return isFreeContent;
    }

    public void setIsFreeContent(String isFreeContent) {
        this.isFreeContent = isFreeContent;
    }

    public String getIsFreeData() {
        return isFreeData;
    }

    public void setIsFreeData(String isFreeData) {
        this.isFreeData = isFreeData;
    }

    public boolean isFreeData() {
        return "1".equals(isFreeData);
    }

    public boolean isAVOD() {
        return "1".equals(isFreeContent);
    }

    public boolean isSVOD() {
        return "0".equals(isFreeContent) && Constants.MOVIE_TYPE_NORMAL.equalsIgnoreCase(typeFilm);
    }

    public boolean isBigsix() {
        return Constants.MOVIE_TYPE_BIGSIX.equalsIgnoreCase(typeFilm);
    }

    public boolean isDRM() {
        return Constants.MOVIE_TYPE_TVOD_DRM.equalsIgnoreCase(typeFilm) || Constants.MOVIE_TYPE_BIGSIX.equalsIgnoreCase(typeFilm);
    }

    public boolean isTVOD() {
        return Constants.MOVIE_TYPE_TVOD_DRM.equalsIgnoreCase(typeFilm)
                || Constants.MOVIE_TYPE_TVOD_ODD.equalsIgnoreCase(typeFilm);
    }

    public String getLabelText() {
        return labelText;
    }

    public void setLabelText(String labelText) {
        this.labelText = labelText;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setScore(double score) {
        this.score =(float) score;
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    public void setListenNo(long listenNo) {
        this.listenNo = listenNo;
    }

    public void setIsSinger(int isSinger) {
        this.isSinger = isSinger;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }
}


package com.metfone.selfcare.module.home_kh.tab.model;

public class KhHomeRewardSearchItem implements IHomeModelType{

    public int id;
    public String icon;
    public String name;

    private int type;

    public void setType(int type) {
        this.type = type;
    }

    public GiftSearchObject giftObject;

    public KhHomeRewardSearchItem() {

    }
    public KhHomeRewardSearchItem(int id, String icon, String name) {
        this.id = id;
        this.icon = icon;
        this.name = name;
    }

    @Override
    public int getItemType() {
        return type;
    }
}

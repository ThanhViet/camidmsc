/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.module.movie.holder.InfoHolder;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV3;

public class InfoAdapter extends BaseAdapterV3 {

    public InfoAdapter(Activity act) {
        super(act);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new InfoHolder((BaseSlidingFragmentActivity) activity, layoutInflater, parent);
    }

}


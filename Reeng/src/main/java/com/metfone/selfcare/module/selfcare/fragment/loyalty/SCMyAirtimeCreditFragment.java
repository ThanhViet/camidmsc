package com.metfone.selfcare.module.selfcare.fragment.loyalty;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.event.SCAccountEvent;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.model.SCAirTimeInfo;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestAirTimeInfo;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;

public class SCMyAirtimeCreditFragment extends SCBaseFragment {

    private static final int TYPE_AIR_TIME_CREDIT_BASIC = 1;
    private static final int TYPE_AIR_TIME_CREDIT_PROMOTION = 2;

    TextView tvDes;
    TextView btnSubmit;
    RadioGroup rgBalance;
    RadioButton rdBasic;
    RadioButton rdPromotion;
    Spinner spAmount;
    RegionSpinnerAdapter amountAdapter;
    ArrayList<SCAirTimeInfo> amountList = new ArrayList<>();

    public static SCMyAirtimeCreditFragment newInstance(Bundle args) {

        SCMyAirtimeCreditFragment fragment = new SCMyAirtimeCreditFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCMyAirtimeCreditFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_my_credit;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        btnSubmit = view.findViewById(R.id.btnSubmit);
        rgBalance = view.findViewById(R.id.rgBalance);
        rdBasic = view.findViewById(R.id.rdBasic);
        rdPromotion = view.findViewById(R.id.rdPromotion);
        spAmount = view.findViewById(R.id.spAmount);

        amountAdapter = new RegionSpinnerAdapter(mActivity, amountList);
        spAmount.setAdapter(amountAdapter);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSubmit();
            }
        });

        if (mActivity != null && mActivity instanceof TabSelfCareActivity)
            ((TabSelfCareActivity) mActivity).hideFloatingButton();

        loadAmountList(TYPE_AIR_TIME_CREDIT_BASIC);

        rgBalance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdBasic:
                        loadAmountList(TYPE_AIR_TIME_CREDIT_BASIC);
                        break;
                    case R.id.rdPromotion:
                        loadAmountList(TYPE_AIR_TIME_CREDIT_PROMOTION);
                        break;
                }
            }
        });

        return view;
    }

    private void loadAmountList(int typeBalance) {
        WSSCRestful restful = new WSSCRestful(mActivity);
        restful.getAirTimeAmount(typeBalance, new Response.Listener<RestAirTimeInfo>() {
            @Override
            public void onResponse(RestAirTimeInfo restAirTimeInfo) {
                if (restAirTimeInfo != null && restAirTimeInfo.getData() != null) {
                    amountList.clear();
                    amountList.addAll(restAirTimeInfo.getData());
                    amountAdapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

    private void doSubmit() {
        if (amountList.size() == 0) return;
        int actionType = 0;
        if (spAmount.getSelectedItem() instanceof SCAirTimeInfo) {
            SCAirTimeInfo scAirTimeInfo = (SCAirTimeInfo) spAmount.getSelectedItem();
            actionType = scAirTimeInfo.getActionType();
        }

//        if(rdPromotion.isChecked())
//            actionType = 3;
//        else
//            actionType = 2;

        WSSCRestful restful = new WSSCRestful(mActivity);
        restful.airTimeCredit("AIRTIME", actionType, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.optInt("errorCode", -1);
                    String message = jsonObject.optString("message");
                    mActivity.showToast(message);
                    if (code == 200)
                        EventBus.getDefault().postSticky(new SCAccountEvent(SCAccountEvent.UPDATE_INFO));
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }
}
package com.metfone.selfcare.module.keeng.widget;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;

public class LoadingView extends LinearLayout {
    private TextView btnRetry;
    private View emptyView;
    private ImageView icon;
    private TextView loadingError;
    private TextView loadingErrorSubtext;
    private TextView loaddingEmpty;
    private View loadingErrorView;
    private View progressBarLayout;
    private LinearLayout layoutMain;
    private ProgressBar progressBar;

    public LoadingView(Context paramContext) {
        super(paramContext);
        initView(paramContext);
    }

    public LoadingView(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        initView(paramContext);
    }

    public LoadingView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
        initView(paramContext);
    }

    private void initView(Context paramContext) {
        if (!isInEditMode()) {
            ((LayoutInflater) paramContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_loading, this, true);
            this.progressBarLayout = getRootView().findViewById(R.id.progress_bar_layout);
            this.loadingError = getRootView().findViewById(R.id.loadingerror);
            this.loadingErrorSubtext = getRootView().findViewById(R.id.loadingerror_subtext);
            this.loaddingEmpty = getRootView().findViewById(R.id.loading_empty);
            this.emptyView = getRootView().findViewById(R.id.empty_layout);
            this.btnRetry = getRootView().findViewById(R.id.btn_retry);
            this.loadingErrorView = getRootView().findViewById(R.id.loading_error_layout);
            this.layoutMain = getRootView().findViewById(R.id.layout_main);
            this.icon = getRootView().findViewById(R.id.icon);
            this.progressBar = getRootView().findViewById(R.id.progress_wheel);
        }
    }

    public void loadBegin() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(VISIBLE);
        this.loadingError.setText(getContext().getResources().getString(R.string.connection_error2));
        this.btnRetry.setText(getContext().getResources().getString(R.string.retry));
        this.loadingError.setVisibility(GONE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(GONE);
        this.emptyView.setVisibility(GONE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(GONE);
    }

    public void loadBeginMocha() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(VISIBLE);
        this.loadingError.setText(getContext().getResources().getString(R.string.connection_error2));
        this.btnRetry.setText(getContext().getResources().getString(R.string.retry));
        this.loadingError.setVisibility(GONE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(GONE);
        this.emptyView.setVisibility(GONE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(GONE);
        this.progressBar.setIndeterminateDrawable(getContext().getResources().getDrawable(R.drawable.circular_progress_bar_mocha));
    }

    public void loadBegin(int resDrawableProgress) {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(VISIBLE);
        this.loadingError.setText(getContext().getResources().getString(R.string.connection_error2));
        this.btnRetry.setText(getContext().getResources().getString(R.string.retry));
        this.loadingError.setVisibility(GONE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(GONE);
        this.emptyView.setVisibility(GONE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(GONE);
        if (resDrawableProgress > 0)
            this.progressBar.setIndeterminateDrawable(getContext().getResources().getDrawable(resDrawableProgress));
    }

    public void loadEmpty() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_no_data);
        this.loadingError.setText(Html.fromHtml(getResources().getString(R.string.pull_to_load_more_no_result)));
    }

    public void loadListSingerEmpty(int songType) {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(VISIBLE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        switch (songType) {
            case Constants.TYPE_SONG:
                this.icon.setImageResource(R.drawable.ic_empty_music);
                this.loadingError.setText(R.string.music_list_empty_error);
                this.loadingErrorSubtext.setText(R.string.music_list_empty_sub_error);
                break;
            case Constants.TYPE_VIDEO:
                this.icon.setImageResource(R.drawable.ic_empty_mv);
                this.loadingError.setText(R.string.music_mv_list_empty_error);
                this.loadingErrorSubtext.setText(R.string.music_mv_list_empty_sub_error);
                break;
            case Constants.TYPE_ALBUM:
                this.icon.setImageResource(R.drawable.ic_empty_album);
                this.loadingError.setText(R.string.music_album_list_empty_error);
                this.loadingErrorSubtext.setText(R.string.music_album_list_empty_sub_error);
                break;
        }
    }

    public void loadEmptyMocha() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_mocha_thumbnail);
        this.loadingError.setText(Html.fromHtml(getResources().getString(R.string.pull_to_load_more_no_result)));
    }

    public void loadEmptySearch() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_not_found);
        this.loadingError.setText(Html.fromHtml(getResources().getString(R.string.pull_to_load_more_no_result_search)));
    }

    public void loadEmpty(String str) {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_no_data);
        if (!TextUtils.isEmpty(str))
            this.loadingError.setText(Html.fromHtml(str));
    }

    public void loadError() {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_no_connect);
    }

    public void loadError(String paramString) {
        setVisibility(VISIBLE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(VISIBLE);
        this.icon.setImageResource(R.drawable.ic_no_connect);
        if (!TextUtils.isEmpty(paramString))
            this.loadingError.setText(Html.fromHtml(paramString));
    }

    public void loadFinish() {
        setVisibility(GONE);
        this.progressBarLayout.setVisibility(GONE);
        this.loadingError.setVisibility(GONE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(GONE);
        this.btnRetry.setVisibility(GONE);
    }

    public void notice(String paramString) {
        setVisibility(VISIBLE);
        layoutMain.setBackgroundColor(getResources().getColor(R.color.transparent));
        emptyView.setBackgroundColor(getResources().getColor(R.color.transparent));
        loaddingEmpty.setBackgroundColor(getResources().getColor(R.color.transparent));
        this.progressBarLayout.setVisibility(GONE);
        this.btnRetry.setVisibility(GONE);
        this.icon.setVisibility(GONE);
        this.emptyView.setVisibility(View.VISIBLE);
        this.loadingError.setVisibility(GONE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(GONE);
        this.loadingError.setVisibility(View.GONE);
        this.loaddingEmpty.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(paramString)) {
            this.loaddingEmpty.setText(paramString);
        }

    }

    public void loadLogin(String str) {
        setVisibility(VISIBLE);
        layoutMain.setBackgroundColor(getResources().getColor(R.color.white));
        this.progressBarLayout.setVisibility(GONE);
        this.icon.setVisibility(GONE);
        this.loadingError.setVisibility(VISIBLE);
        this.loadingErrorSubtext.setVisibility(GONE);
        this.loadingErrorView.setVisibility(VISIBLE);
        this.btnRetry.setVisibility(VISIBLE);
        if (!TextUtils.isEmpty(str))
            this.loadingError.setText(str);
        this.btnRetry.setText(Html.fromHtml(getResources().getString(R.string.sign_in)));
    }

    public void setLoadingErrorListener(OnClickListener paramOnClickListener) {
        if (paramOnClickListener != null)
            this.loadingError.setOnClickListener(paramOnClickListener);
    }

    public void setBtnRetryListener(OnClickListener paramOnClickListener) {
        if (paramOnClickListener != null)
            this.btnRetry.setOnClickListener(paramOnClickListener);
    }
}
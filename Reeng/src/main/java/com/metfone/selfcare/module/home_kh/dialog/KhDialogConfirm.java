package com.metfone.selfcare.module.home_kh.dialog;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.base.BaseDialogKhFragment;

public class KhDialogConfirm extends BaseDialogKhFragment {

    public static final int CONFIRM_TYPE = 0; // have two button
    public static final int INTRO_TYPE = 1; // have one button

    private static final String TYPE_KEY = "type_key";
    private static final String TITLE_KEY = "title_key";
    private static final String MESSAGE_KEY = "message_key";
    private static final String RIGHT_BUTTON_KEY = "right_button_key";
    private static final String LEFT_BUTTON_KEY = "left_button_key";
    private static final String IC_SHORT_CUT_KEY = "ic_short_cut_key";
    private static final String ID_TITLE_SHORT_CUT_KEY = "title_short_cut_key";

    public static KhDialogConfirm newInstance(int idTitle, int idMessage, int type) {
        Bundle args = new Bundle();
        KhDialogConfirm fragment = new KhDialogConfirm();
        fragment.setArguments(args);
        return fragment;
    }

    public static KhDialogConfirm newInstance(String title, String message, int type,
                                              int idTitleLeftButton, int idTitleRightButton) {
        Bundle args = new Bundle();
        args.putString(TITLE_KEY, title);
        args.putString(MESSAGE_KEY, message);
        args.putInt(TYPE_KEY, type);
        args.putInt(LEFT_BUTTON_KEY, idTitleLeftButton);
        args.putInt(RIGHT_BUTTON_KEY, idTitleRightButton);
        KhDialogConfirm fragment = new KhDialogConfirm();
        fragment.setArguments(args);
        return fragment;
    }

    public static KhDialogConfirm newInstance(String title, String message, int type,
                                              int idTitleLeftButton, int idTitleRightButton
            , int idShortcut
            , int titleShortcut) {
        Bundle args = new Bundle();
        args.putString(TITLE_KEY, title);
        args.putString(MESSAGE_KEY, message);
        args.putInt(TYPE_KEY, type);
        args.putInt(LEFT_BUTTON_KEY, idTitleLeftButton);
        args.putInt(RIGHT_BUTTON_KEY, idTitleRightButton);
        args.putInt(IC_SHORT_CUT_KEY, idShortcut);
        args.putInt(ID_TITLE_SHORT_CUT_KEY, titleShortcut);
        KhDialogConfirm fragment = new KhDialogConfirm();
        fragment.setArguments(args);
        return fragment;
    }

    public static KhDialogConfirm newInstance(String idTitle, String idMessage, int idTitleButton) { // type = 1
        Bundle args = new Bundle();
        KhDialogConfirm fragment = new KhDialogConfirm();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getResId() {
        return R.layout.dialog_confirm_kh;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    protected void initView() {
        super.initView();
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();
                if (selectListener != null) {
                    selectListener.dialogRightClick(0);
                }
            }
        });
        if (getArguments() != null) {
            if (!TextUtils.isEmpty(getArguments().getString(TITLE_KEY))) {
                tvTitle.setText(getArguments().getString(TITLE_KEY));
            } else {
                tvTitle.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(getArguments().getString(MESSAGE_KEY))) {
                ((AppCompatTextView) getView().findViewById(R.id.tvMessage)).setText(getArguments().getString(MESSAGE_KEY));

            } else {
                getView().findViewById(R.id.tvMessage).setVisibility(View.GONE);
            }
            btnRight.setText(getArguments().getInt(RIGHT_BUTTON_KEY));
            if (getArguments().getInt(TYPE_KEY) == CONFIRM_TYPE) {
                btnLeft.setText(getArguments().getInt(LEFT_BUTTON_KEY));
                // has short cut icon
                if (getArguments().getInt(ID_TITLE_SHORT_CUT_KEY) != 0) {
                    AppCompatImageView icShortcut = getView().findViewById(R.id.icShortcut);
                    icShortcut.setImageResource(getArguments().getInt(IC_SHORT_CUT_KEY));
                    icShortcut.setVisibility(View.VISIBLE);
                    icShortcut.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            if (selectListener != null) {
                                selectListener.dialogRightClick(0);
                            }
                            getDialog().cancel();
                            return true;
                        }
                    });
                    AppCompatTextView tvTitleShortCut = getView().findViewById(R.id.tvShortCut);
                    tvTitleShortCut.setVisibility(View.VISIBLE);
                    tvTitleShortCut.setText(getArguments().getInt(ID_TITLE_SHORT_CUT_KEY));
                }
            } else {
                btnLeft.setVisibility(View.GONE);
//                getView().findViewById(R.id.lineVertical).setVisibility(View.GONE);
                btnRight.getLayoutParams().width = ConstraintLayout.LayoutParams.MATCH_PARENT;
            }
        }
    }


    public final static class Builder {
        private String title;
        private String message;
        private int type;
        private int titleLeftButton;
        private int titleRightButton;
        private int idIconShortCut;
        private int titleShortcut;

        public Builder(int type) {
            this.type = type;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setType(int type) {
            this.type = type;
            return this;
        }

        public Builder setTitleLeftButton(int titleLeftButton) {
            this.titleLeftButton = titleLeftButton;
            return this;
        }

        public Builder setTitleRightButton(int idTitleRightButton) {
            this.titleRightButton = idTitleRightButton;
            return this;
        }

        public Builder setIdIconShortCut(int idIconShortCut) {
            this.idIconShortCut = idIconShortCut;
            return this;
        }

        public Builder setTitleShortcut(int titleShortcut) {
            this.titleShortcut = titleShortcut;
            return this;
        }

        public KhDialogConfirm create() {
            return KhDialogConfirm.newInstance(title, message
                    , type, titleLeftButton, titleRightButton
                    , idIconShortCut, titleShortcut);
        }
    }

}

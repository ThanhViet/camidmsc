package com.metfone.selfcare.module.selfcare.loginhelper.dialogselectphone;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.selfcare.model.SCNumberVerify;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 5/8/2019.
 */

public class DialogSelectPhone extends Dialog implements View.OnClickListener {

    public static final int TYPE_ADD_NUMBER = 0;
    public static final int TYPE_INPUT_OTP = 1;
    public static final int TYPE_SELECT_NUMBER = 2;

    private BaseSlidingFragmentActivity activity;
    private Resources mRes;
    private NegativeListener<String> negativeListener;
    private PositiveListener<SCNumberVerify> positiveListener;
    private DismissListener dismissListener;

    private RecyclerView mRecyclerView;
    private Button mBtnPositive, mBtnNegative;

    private ArrayList<SCNumberVerify> listNumber = new ArrayList<>();
    private SelectPhoneAdapter mAdapter;


    public DialogSelectPhone(BaseSlidingFragmentActivity activity) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        this.mRes = activity.getResources();
        setCancelable(false);
    }


    public DialogSelectPhone setNegativeListener(NegativeListener listener) {
        this.negativeListener = listener;
        return this;
    }

    public DialogSelectPhone setPositiveListener(PositiveListener<SCNumberVerify> listener) {
        this.positiveListener = listener;
        return this;
    }

    public DialogSelectPhone setListNumber(ArrayList<SCNumberVerify> listNumber) {
        this.listNumber = listNumber;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.dialog_select_phone);
        findComponentViews();
        drawDetail();
        setListener();
    }


    @Override
    public void dismiss() {
        Log.d("DialogEditText", "dismiss");
        //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_confirm_button_negative:
                if (negativeListener != null) {
                    negativeListener.onNegative(null);
                }
                dismiss();
                break;
            case R.id.dialog_button_positive:
                if (positiveListener != null) {
                    SCNumberVerify number = getNumberSelect();
                    if (number == null) {
                        activity.showToast(R.string.e601_error_but_undefined);
                    } else {
                        positiveListener.onPositive(number);
                        dismiss();
                    }
                } else {
                    dismiss();
                }
                break;
        }
    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private void findComponentViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.dialog_recycler_view);
        mBtnPositive = (Button) findViewById(R.id.dialog_button_positive);
        mBtnNegative = findViewById(R.id.dialog_confirm_button_negative);
    }

    private void setListener() {
        mBtnPositive.setOnClickListener(this);
        mBtnNegative.setOnClickListener(this);
    }

    private void drawDetail() {
        setAdapter();
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new SelectPhoneAdapter(activity, listNumber);
            mAdapter.setClickListener(new RecyclerClickListener() {
                @Override
                public void onClick(View v, int pos, Object object) {
                    SCNumberVerify scNumberVerify = listNumber.get(pos);
                    for (int i = 0; i < listNumber.size(); i++) {
                        if (i != pos)
                            listNumber.get(i).setSelected(false);
                        else {
                            scNumberVerify.setSelected(!scNumberVerify.isSelected());
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onLongClick(View v, int pos, Object object) {

                }
            });
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
        } else {
            mAdapter.setListTopic(listNumber);
            mAdapter.notifyDataSetChanged();
        }
    }

    private SCNumberVerify getNumberSelect() {
        for (SCNumberVerify item : listNumber) {
            if (item.isSelected()) return item;
        }
        return null;
    }


}

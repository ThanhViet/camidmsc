package com.metfone.selfcare.module.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.EventModel;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/19/17.
 */

public class EventResponse extends ErrorResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<EventModel> data = new ArrayList<>();

    public ArrayList<EventModel> getData() {
        return data;
    }

    public void setData(ArrayList<EventModel> data) {
        this.data = data;
    }
}

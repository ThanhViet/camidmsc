package com.metfone.selfcare.module.home_kh.notification.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class KhNotificationResponse extends BaseResponse<KhNotificationResponse.Response> {
    public class Response {
        public int total;
        public int page;
        public int unread;
        @SerializedName("new")
        public List<KhNotificationItem> new_;

        @SerializedName("earlier")
        public List<KhNotificationItem> earlier;
    }
}


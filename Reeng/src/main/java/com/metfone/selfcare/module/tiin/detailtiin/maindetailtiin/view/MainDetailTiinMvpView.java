package com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.view;

import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.util.List;

public interface MainDetailTiinMvpView extends MvpView {
    void loadDataSuccess(boolean flag);
    void bindDataSibling(List<TiinModel> response);

}

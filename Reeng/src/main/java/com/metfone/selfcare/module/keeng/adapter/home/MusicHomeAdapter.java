package com.metfone.selfcare.module.keeng.adapter.home;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.keeng.holder.SlidingBannerHolder;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.MusicHomeModel;
import com.metfone.selfcare.module.keeng.model.RankModel;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.widget.CustomDividerDecoration;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.VideoHotDecoration;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.vtm.adslib.AdsHelper;

import java.util.List;

public class MusicHomeAdapter extends BaseAdapter<BaseViewHolder> {

    private final int SECTION_FLASH_HOT = 1;
    //private final int SECTION_MIX = 2;
    private final int SECTION_MV_HOT = 3;
    private final int SECTION_BANNER = 4;
    private final int SECTION_MV_YOUTUBE = 5;
    private final int SECTION_ALBUM_HOT = 6;
    private final int SECTION_PLAYLIST_HOT = 7;
    private final int SECTION_TOP_HIT = 8;
    private final int SECTION_TOPIC = 9;
    private final int SECTION_CHART = 10;
    private final int SECTION_SONG_HOT = 11;
    private final int SECTION_SINGER_HOT = 12;
    private final int SECTION_EMPTY = 13;

    private List<MusicHomeModel> itemsList;
    private AbsInterface.OnItemListener listener;
    private CustomDividerDecoration dividerItemDecoration;
    private AdsHelper adsHelper;

    public MusicHomeAdapter(Context context, List<MusicHomeModel> itemsList, AbsInterface.OnItemListener listener) {
        super(context);
        this.listener = listener;
        this.itemsList = itemsList;
        dividerItemDecoration = new CustomDividerDecoration(mContext, LinearLayoutManager.HORIZONTAL);
        dividerItemDecoration.setDrawable(mContext.getResources().getDrawable(R.drawable.divider_horizonal));
    }

    public void setAdsHelper(AdsHelper adsHelper) {
        this.adsHelper = adsHelper;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case SECTION_FLASH_HOT: {
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_sliding_banner_music, null);
                return new SlidingBannerHolder(mContext, listener, view);
            }
            case SECTION_MV_HOT:
            case SECTION_ALBUM_HOT:
            case SECTION_PLAYLIST_HOT:
            case SECTION_SINGER_HOT:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_music_home_mv_hot, null);
                break;
            case SECTION_MV_YOUTUBE:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_music_home_mv_youtube, null);
                break;
            case SECTION_TOP_HIT:
            case SECTION_TOPIC:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_home_vertical, null);
                break;
            case SECTION_CHART:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_music_home_chart, null);
                break;
            case SECTION_SONG_HOT:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_home_horizontal, null);
                break;
            case SECTION_BANNER:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_home_banner, null);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case SECTION_FLASH_HOT:
                setupSectionSlide(holder, getItem(position));
                break;
            case SECTION_MV_HOT:
                setupSectionMVHot(holder, getItem(position));
                break;
            case SECTION_MV_YOUTUBE:
                setupSectionYoutube(holder, getItem(position));
                break;
            case SECTION_ALBUM_HOT:
                setupSectionAlbum(holder, getItem(position));
                break;
            case SECTION_PLAYLIST_HOT:
                setupSectionPlayListHot(holder, getItem(position));
                break;
            case SECTION_TOPIC:
                setupSectionTopic(holder, getItem(position));
                break;
            case SECTION_TOP_HIT:
                setupSectionTopHit(holder, getItem(position));
                break;
            case SECTION_CHART:
                setupSectionRank(holder, getItem(position));
                break;
            case SECTION_SONG_HOT:
                setupSectionSong(holder, getItem(position));
                break;
            case SECTION_SINGER_HOT:
                setupSectionSinger(holder, getItem(position));
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return itemsList == null ? 0 : itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        MusicHomeModel restTabModel = getItem(position);
        if (restTabModel != null) {
            switch (restTabModel.getType()) {
                case Constants.MUSIC_HOME_TYPE_FLASH_HOT:
                    return SECTION_FLASH_HOT;
                case Constants.MUSIC_HOME_TYPE_MIX:
                case Constants.MUSIC_HOME_TYPE_MV_YOUTUBE:
                    return SECTION_EMPTY;
                case Constants.MUSIC_HOME_TYPE_MV_HOT:
                    return SECTION_MV_HOT;
                case Constants.MUSIC_HOME_TYPE_PLAYLIST_HOT:
                    return SECTION_PLAYLIST_HOT;
                case Constants.MUSIC_HOME_TYPE_ALBUM_HOT:
                    return SECTION_ALBUM_HOT;
                case Constants.MUSIC_HOME_TYPE_TOP_HIT:
                    return SECTION_TOP_HIT;
                case Constants.MUSIC_HOME_TYPE_CHART:
                    return SECTION_CHART;
                case Constants.MUSIC_HOME_TYPE_TOPIC:
                    return SECTION_TOPIC;
                case Constants.MUSIC_HOME_TYPE_SONG_HOT:
                    return SECTION_SONG_HOT;
                case Constants.MUSIC_HOME_TYPE_SINGER_HOT:
                    return SECTION_SINGER_HOT;
                case Constants.MUSIC_HOME_TYPE_BANNER:
                    return SECTION_BANNER;
            }
        }
        return SECTION_EMPTY;
    }

    public MusicHomeModel getItem(int position) {
        return (null != itemsList && itemsList.size() > position ? itemsList.get(position) : null);
    }

    private void setupSectionSlide(BaseViewHolder holder, MusicHomeModel data) {
        if (holder instanceof SlidingBannerHolder) {
            ((SlidingBannerHolder) holder).bindData(data.getFlashHot());
        }
    }

    private void setupSectionMVHot(BaseViewHolder holder, MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        btnHeader.setOnClickListener(v -> {
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_VIDEO_HOT);
        });
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.addItemDecoration(new VideoHotDecoration(2, mContext.getResources().getDimensionPixelOffset(R.dimen.spacing_small), true));
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomGridLayoutManager(mContext, 2));
        }

        VideoAdapter adapter = new VideoAdapter(mContext, data.getVideos(), listener);
        adapter.setTag(String.valueOf(Constants.TAB_VIDEO_HOT));
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
    }

    private void setupSectionYoutube(BaseViewHolder holder, MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        btnHeader.setOnClickListener(v -> {
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_YOUTUBE_HOT);
        });

        View firstYoutubeView = holder.getView(R.id.first_youtube);
        ImageView imvFirstYoutube = holder.getView(R.id.image_first_youtube);
        TextView tvTitleFirstYoutube = holder.getView(R.id.title_first_youtube);
        TextView tvSingerFirstYoutube = holder.getView(R.id.singer_first_youtube);

        final AllModel item = data.getYoutubes().get(0);
        if (item != null) {
            firstYoutubeView.setVisibility(View.VISIBLE);
            ImageBusiness.setVideo(item.getImage310(), imvFirstYoutube, item.getId());
            tvTitleFirstYoutube.setText(item.getName());
            tvSingerFirstYoutube.setText(item.getSinger());
            firstYoutubeView.setOnClickListener(v -> {
//                    ViewUtil.lockClickView(v);
//                    if (mContext instanceof BaseActivity) {
//                        ((BaseActivity) mContext).setMediaToPlayYoutube(item);
//                    }
            });
        } else {
            firstYoutubeView.setVisibility(View.GONE);
        }

        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (data.getYoutubes().size() > 1) {
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.addItemDecoration(dividerItemDecoration);
                recyclerView.setHasFixedSize(true);
                recyclerView.setNestedScrollingEnabled(false);
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            }
            YoutubeAdapter adapter = new YoutubeAdapter(mContext, data.getYoutubes(), listener);
            adapter.setTag(String.valueOf(Constants.TAB_YOUTUBE_HOT));
            recyclerView.setAdapter(adapter);
            //adapter.notifyDataSetChanged();
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void setupSectionPlayListHot(BaseViewHolder holder, MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        btnHeader.setOnClickListener(v -> {
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_PLAYLIST_HOT);
        });
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }
        PlaylistAdapter adapter = new PlaylistAdapter(mContext, data.getPlaylists(), listener);
        adapter.setTag(String.valueOf(Constants.TAB_PLAYLIST_HOT));
        adapter.setCanViewAll(true);
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

        if(adsHelper != null)
            adsHelper.showAd(holder.getView(R.id.layout_ads));
    }

    private void setupSectionAlbum(BaseViewHolder holder, MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        btnHeader.setOnClickListener(v -> {
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_ALBUM_HOT);
        });
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }
        AlbumAdapter adapter = new AlbumAdapter(mContext, data.getAlbums(), listener);
        adapter.setTag(String.valueOf(Constants.TAB_ALBUM_HOT));
        adapter.setCanViewAll(true);
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
    }

    private void setupSectionTopHit(BaseViewHolder holder, MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        btnHeader.setOnClickListener(v -> {
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_TOP_HIT);
        });
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }
        TopHitAdapter adapter = new TopHitAdapter(mContext, data.getTopHits(), listener);
        adapter.setTag(String.valueOf(Constants.TAB_TOP_HIT));
        adapter.setCanViewAll(true);
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
    }

    private void setupSectionRank(BaseViewHolder holder, final MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        final RankModel rankModel = data.getCharts().get(0);
        btnHeader.setOnClickListener(v -> {
//                if (mContext instanceof BaseActivity) {
//                    ((BaseActivity) mContext).gotoRankDetail(rankModel);
//                }
            if (listener != null) listener.onItemRankHeaderClick(rankModel);
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_RANK_DETAIL);
        });
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        }
        ConvertHelper.convertData(rankModel.getListSong(), MediaLogModel.SRC_HOT_HOME);
        RankAdapter adapter = new RankAdapter(mContext, rankModel, listener);
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

        TextView btnKpop = holder.getView(R.id.button_kpop);
        TextView btnUs = holder.getView(R.id.button_us);
        int size = data.getCharts().size();
        if (size >= 3) {
            if (data.getCharts().get(1) != null) {
                btnKpop.setVisibility(View.VISIBLE);
                btnKpop.setText(data.getCharts().get(1).getTitle());
                btnKpop.setOnClickListener(v -> {
//                        if (mContext instanceof BaseActivity) {
//                            ((BaseActivity) mContext).gotoRankDetail(data.getCharts().get(1));
//                        }
                    if (listener != null)
                        listener.onItemRankHeaderClick(data.getCharts().get(1));
                });
            } else {
                btnKpop.setVisibility(View.GONE);
            }

            if (data.getCharts().get(2) != null) {
                btnUs.setVisibility(View.VISIBLE);
                btnUs.setText(data.getCharts().get(2).getTitle());
                btnUs.setOnClickListener(v -> {
//                        if (mContext instanceof BaseActivity) {
//                            ((BaseActivity) mContext).gotoRankDetail(data.getCharts().get(2));
//                        }
                    if (listener != null)
                        listener.onItemRankHeaderClick(data.getCharts().get(2));
                });
            } else {
                btnUs.setVisibility(View.GONE);
            }
        } else {
            btnKpop.setVisibility(View.GONE);
            btnUs.setVisibility(View.GONE);
        }
    }

    private void setupSectionSong(BaseViewHolder holder, MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        btnHeader.setOnClickListener(v -> {
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_SONG_HOT);
        });
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
//            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        }
        ConvertHelper.convertData(data.getSongs(), MediaLogModel.SRC_HOT_HOME);
        SongAdapter adapter = new SongAdapter(mContext, data.getSongs(), listener);
        adapter.setTag(String.valueOf(Constants.TAB_SONG_HOT));
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
    }

    private void setupSectionTopic(BaseViewHolder holder, MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        btnHeader.setOnClickListener(v -> {
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_TOPIC_HOT);
        });
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }
        TopicAdapter adapter = new TopicAdapter(mContext, data.getTopics(), listener);
        adapter.setTag(String.valueOf(Constants.TAB_TOPIC_HOT));
        adapter.setCanViewAll(true);
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
    }

    private void setupSectionSinger(BaseViewHolder holder, MusicHomeModel data) {
        if (holder == null || data == null) return;
        TextView btnHeader = holder.getView(R.id.button_more);
        btnHeader.setText(data.getTitle());
        btnHeader.setOnClickListener(v -> {
            if (listener != null) listener.onHeaderClick(v, "" + Constants.TAB_SINGER_HOT);
        });
        TextView tvViewAll = holder.getView(R.id.tv_listen_all);
        tvViewAll.setText(R.string.view_all);
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }
        SingerAdapter adapter = new SingerAdapter(mContext, data.getSingers(), listener);
        adapter.setTag(String.valueOf(Constants.TAB_SINGER_HOT));
        adapter.setCanViewAll(true);
        recyclerView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
    }
}

package com.metfone.selfcare.module.metfoneplus.dialog;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.model.BaseDataRequest;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.BaseUserResponseData;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MPConfirmUpdateInformationToCamDialog extends BaseCustomDialog {
    public static final String TAG = "MPConfirmInformationDialog";
    private UserInfoBusiness userInfoBusiness;
    private UserInfo userInfo;
    private boolean isBack = true;
    private boolean isSuccess = false;
    private BaseMPErrorDialog baseMPErrorDialog;

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public MPConfirmUpdateInformationToCamDialog(@NonNull Activity activity) {
        super(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getWindow() != null)
            getWindow().setLayout(UserInfoBusiness.getWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
        userInfoBusiness = new UserInfoBusiness(getActivity());
        mImage.setImageResource(R.drawable.img_dialog_3);
        mImage.setVisibility(View.GONE);
        flImage.setVisibility(View.VISIBLE);
        lavAnimations.setAnimation("lottie/img_dialog_women_thinking.json");
        lavAnimations.playAnimation();
        lavAnimations.setVisibility(View.VISIBLE);
        mTitle.setText(getContext().getString(R.string.tilte_dialog_confirm_update_to_camid));
        mButtonBottom.setTextColor(Color.parseColor("#80FFFFFF"));
        setTopButton(R.string.yes, R.drawable.bg_red_button_corner_6, buttonTop -> {
            mButtonTop.setEnabled(false);
            mButtonBottom.setEnabled(false);
            if (userInfo != null) {
                updateUser(userInfo);
            } else {
                ToastUtils.showToast(getActivity(), getActivity().getString(R.string.error));
            }
        });

        setBottomButton(R.string.no, R.drawable.bg_transparent_button_corner_6, buttonBottom -> {
            mButtonTop.setEnabled(false);
            mButtonBottom.setEnabled(false);
            isBack = true;
            dismiss();
        });
    }

    public boolean getBack() {
        return isBack;
    }

    private void updateUser(UserInfo userInfo) {
        progressBar.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.updateUser(token, new BaseDataRequest<>(userInfo, "string", "string", "string", "string")).enqueue(new Callback<BaseResponse<BaseUserResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseUserResponseData>> call, Response<BaseResponse<BaseUserResponseData>> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    BaseResponse<BaseUserResponseData> baseResponse = response.body();
                    if ("00".equals(baseResponse.getCode())) {
                        isBack = false;
                        if (baseResponse.getData() != null) {
                            if (response.body().getData().getUser() != null) {
                                userInfoBusiness.setUser(baseResponse.getData().getUser());
                            }
                            if (response.body().getData().getServices() != null) {
                                userInfoBusiness.setServiceList(baseResponse.getData().getServices());
                            }
                            userInfoBusiness.setIdentifyProviderList(baseResponse.getData().getIdentifyProviders());
                            updateUiDialog();
                        } else {
                            progressBar.setVisibility(View.GONE);
                            dismiss();
                            baseMPErrorDialog = new BaseMPErrorDialog(getActivity(),
                                    R.drawable.image_error_dialog,
                                    getContext().getString(R.string.title_sorry),
                                    baseResponse.getMessage(),
                                    true, R.string.btn_try_again, v -> {
                                dismiss();
                            });
                            baseMPErrorDialog.show();
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        dismiss();
                        baseMPErrorDialog = new BaseMPErrorDialog(getActivity(),
                                R.drawable.image_error_dialog,
                                getContext().getString(R.string.title_sorry),
                                baseResponse.getMessage(),
                                true, R.string.btn_try_again, v -> {
                            dismiss();
                        });
                        baseMPErrorDialog.show();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    dismiss();
                    baseMPErrorDialog = new BaseMPErrorDialog(getActivity(),
                            R.drawable.image_error_dialog,
                            getContext().getString(R.string.title_sorry),
                            getActivity().getString(R.string.text_update_infomation_fail),
                            true, R.string.btn_try_again, v -> {
                        dismiss();
                    });
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseUserResponseData>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                dismiss();
                baseMPErrorDialog = new BaseMPErrorDialog(getActivity(),
                        R.drawable.image_error_dialog,
                        getContext().getString(R.string.title_sorry),
                        getActivity().getString(R.string.service_error),
                        true, R.string.btn_try_again, v -> {
                    dismiss();
                });
            }
        });
    }

    private void updateUiDialog() {
        isSuccess = true;
        mImage.setVisibility(View.GONE);
        mImage.setImageResource(R.drawable.img_dialog_1);
        lavAnimations.setVisibility(View.VISIBLE);
        lavAnimations.cancelAnimation();
        lavAnimations.setAnimation("lottie/img_dialog_women_success.json");
        lavAnimations.playAnimation();
        mTitle.setText(R.string.successfully);
        mContent.setText(R.string.mp_confirm_update_to_cam_successfull);
        setButtonTopVisibility(View.GONE);
        setButtonBottomVisibility(View.GONE);
    }

    @Override
    public void dismiss() {
        if (progressBar.getVisibility() == View.GONE) {
            super.dismiss();
            if (isSuccess) {
                NavigateActivityHelper.navigateToSetUpProfileClearTop(getActivity());
                getActivity().finish();
            }
        }
    }
}

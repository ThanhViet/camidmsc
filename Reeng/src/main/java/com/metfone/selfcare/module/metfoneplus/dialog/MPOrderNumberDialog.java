package com.metfone.selfcare.module.metfoneplus.dialog;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;

public class MPOrderNumberDialog extends MPHeroDialog {
    private final String TAG = "MPOrderNumberDialog";
    private Context context;
    private String title;
    private String top_content;
    private String bottom_content;
    private OrderDialogListener listener;
    private String txt_top_button;
    private String txt_bottom_button;
    private String typeCode;
    TextView tv_title;
    TextView tv_top_content;
    TextView tv_bottom_content;
    RelativeLayout layout_top_button;
    RelativeLayout layout_bottom_button;
    Button btn_top_button;
    Button btn_bottom_button;

    public static class Builder {
        private Context context;
        private OrderDialogListener listener;
        private String title;
        private String top_content;
        private String bottom_content;
        private String txt_top_button;
        private String txt_bottom_button;
        private String typeCode;
        public Builder(Context context, OrderDialogListener listener, String top_content, String bottom_content, String title, String topButton, String bottomButton, String typeCode) {
            this.context = context;
            this.listener = listener;
            this.top_content = top_content;
            this.bottom_content = bottom_content;
            this.title = title;
            this.txt_top_button = topButton;
            this.txt_bottom_button = bottomButton;
            this.typeCode = typeCode;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public MPOrderNumberDialog build() {
            return new MPOrderNumberDialog(this);
        }
    }

    public MPOrderNumberDialog(Builder builder) {
        this.context = builder.context;
        this.title = builder.title;
        this.top_content = builder.top_content;
        this.bottom_content = builder.bottom_content;
        this.listener = builder.listener;
        this.txt_top_button = builder.txt_top_button;
        this.txt_bottom_button = builder.txt_bottom_button;
        this.typeCode = builder.typeCode;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog() != null && getDialog().getWindow() != null) {
            WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
            lp.dimAmount = 0.65f;
            getDialog().getWindow().setAttributes(lp);
            getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
        initView(view);
    }

    private void initView(View view) {
        tv_title = view.findViewById(R.id.dialog_hero_title);
        tv_top_content = view.findViewById(R.id.dialog_hero_content);
        tv_bottom_content = view.findViewById(R.id.dialog_hero_content_2);
        layout_top_button = view.findViewById(R.id.dialog_hero_top_layout_button);
        layout_bottom_button = view.findViewById(R.id.dialog_hero_bottom_layout_button);
        btn_top_button = view.findViewById(R.id.dialog_hero_top_title_button);
        btn_bottom_button = view.findViewById(R.id.dialog_hero_bottom_title_button);

        tv_title.setVisibility(View.VISIBLE);
        tv_top_content.setVisibility(View.VISIBLE);
        tv_bottom_content.setVisibility(View.VISIBLE);
        layout_top_button.setVisibility(View.VISIBLE);
        layout_bottom_button.setVisibility(View.VISIBLE);

        tv_title.setText(title);
        tv_top_content.setText(top_content);
        tv_top_content.setTextColor(Color.WHITE);
        tv_top_content.setAlpha(1.0f);
        if (!bottom_content.equals("")) {
            tv_bottom_content.setText(bottom_content);
            tv_bottom_content.setVisibility(View.VISIBLE);
        } else {
            tv_bottom_content.setVisibility(View.GONE);
        }
        if (!txt_top_button.equals("")) {
            if (typeCode.equals("5")) {
                btn_top_button.setText(R.string.cancel);
            } else {
                btn_top_button.setText(txt_top_button);
            }
        } else {
            btn_top_button.setText(context.getString(R.string.confirm));
        }
        if (!txt_bottom_button.equals("")) {
            btn_bottom_button.setText(txt_bottom_button);
        } else {
            btn_bottom_button.setText(context.getString(R.string.cancel));
        }
        if (typeCode.equals("8") || typeCode.equals("4") || typeCode.equals("9") || typeCode.equals("10")) {
            btn_bottom_button.setVisibility(View.GONE);
        } else {
            btn_bottom_button.setVisibility(View.VISIBLE);
        }

        tv_bottom_content.setTextColor(Color.WHITE);
        tv_bottom_content.setAlpha(1.0f);
        btn_top_button.setOnClickListener(v -> {
            if (listener != null) {
                listener.confirm();
            }
            dismiss();
        });
        btn_bottom_button.setOnClickListener(v -> {
            if (listener != null) {
                listener.cancel();
            }
            dismiss();
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        dismissAllowingStateLoss();
    }

    public interface OrderDialogListener {
        void confirm();
        void cancel();
    }
}

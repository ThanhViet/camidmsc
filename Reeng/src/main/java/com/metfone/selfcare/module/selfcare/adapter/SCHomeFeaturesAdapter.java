package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCDeeplink;
import com.metfone.selfcare.module.selfcare.utils.SCImageLoader;

import java.util.List;

public class SCHomeFeaturesAdapter extends BaseAdapter<BaseViewHolder> {

    private List<SCDeeplink> itemsList;
    private AbsInterface.OnDeeplinkListener listener;

    public SCHomeFeaturesAdapter(Context context, AbsInterface.OnDeeplinkListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(List<SCDeeplink> itemsList) {
        this.itemsList = itemsList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_home_feature, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final SCDeeplink data = itemsList.get(position);
        if(data != null)
        {
            holder.setText(R.id.tvName, data.getName());
            SCImageLoader.setImage(mContext, (ImageView) holder.getView(R.id.imvImage), data.getImagineUrl());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onDeepLinkClick(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }
}

package com.metfone.selfcare.module.keeng.fragment.category;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.adapter.category.ChildTopicAdapter;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class ChildTopicDetailFragment extends RecyclerFragment<AllModel> implements BaseListener.OnLoadMoreListener {

    private int currentType = 1;
    private int currentPage = 1;
    private Topic topic;
    private ChildTopicAdapter adapter;
    private TabKeengActivity mActivity;
    private ListenerUtils listenerUtils;

    public ChildTopicDetailFragment() {
        clearData();
    }

    public static ChildTopicDetailFragment instance(Topic topic, int type) {
        ChildTopicDetailFragment fragment = new ChildTopicDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, topic);
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "ChildTopicDetailFragment";
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TabKeengActivity) activity;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            currentType = getArguments().getInt("type");
            topic = (Topic) getArguments().getSerializable(Constants.KEY_DATA);
        }
        adapter = new ChildTopicAdapter(mActivity, getDatas(), TAG);
        if (currentType == Constants.TYPE_SONG || currentType == Constants.TYPE_ALBUM) {
            setupRecycler(adapter);
            recyclerView.setPadding(0, mActivity.getResources().getDimensionPixelOffset(R.dimen.padding_8), 0, 0);
        } else {
            setupGridRecycler(adapter, 2, R.dimen.padding_16, true);
        }
        adapter.setRecyclerView(recyclerView, this);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDatas().size() == 0) {
            doLoadData(true);
        }
    }

    private void doLoadData(boolean type) {
        if (!isLoading) {
            if (type)
                loadingBegin();
            isLoading = true;
            loadData();
        }
    }

    private void loadData() {
        if (mActivity == null || topic == null)
            return;
        new KeengApi().getTopicDetail(topic.getId(), currentType, currentPage, numPerPage, response -> loadMediaComplete(response.getData()), error -> {
            Log.e(TAG, error);
            loadMediaComplete(null);
        });
    }

    protected void loadMediaComplete(List<AllModel> result) {
        isLoading = false;
        checkLoadMoreAbsolute(result);
        try {
            if (result == null) {
                if (isRefresh) {
                    isRefresh = false;
                }
                loadMored();
                adapter.setLoaded();
                loadingError(v -> doLoadData(true));
                return;
            }

            if (getDatas().size() == 0 && result.size() == 0) {
                refreshed();
                loadMored();
                loadingEmpty();
            } else {
                refreshed();
                loadMored();
                loadingFinish();
                if (currentType == Constants.TYPE_SONG)
                    ConvertHelper.convertData(result, MediaLogModel.SRC_TOPIC);
                setDatas(result);
                if (getDatas().size() == 0) {
                    loadingEmpty();
                }
                adapter.setLoaded();
                adapter.notifyDataSetChanged();
                currentPage++;
            }

        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        canLoadMore = true;
        currentPage = 1;
        doLoadData(false);

    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            loadMore();
            doLoadData(false);
        }
    }

    @Override
    public void onMediaClick(View v, int position) {
        if (mActivity == null || adapter == null) {
            return;
        }
        AllModel item = adapter.getItem(position);
        if (item != null) {
            item.setSource(MediaLogModel.SRC_TOPIC);
            switch (item.type) {
                case Constants.TYPE_ALBUM:
                case Constants.TYPE_ALBUM_VIDEO:
                    mActivity.gotoAlbumDetail(item);
                    break;
                case Constants.TYPE_SONG:
                    mActivity.playMusicSong(item, false);
                    break;
                case Constants.TYPE_VIDEO:
                    mActivity.setMediaToPlayVideo(item);
                    break;
            }
        }
    }

    @Override
    public void onMediaExpandClick(View v, int position) {
        if (mActivity == null || adapter == null) {
            return;
        }
        AllModel item = adapter.getItem(position);
        mActivity.showPopupMore(item);
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && getDatas().size() == 0)
            onRefresh();
    }
}

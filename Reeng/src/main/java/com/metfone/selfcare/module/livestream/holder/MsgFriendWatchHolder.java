package com.metfone.selfcare.module.livestream.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.livestream.listener.MessageActionListener;
import com.metfone.selfcare.module.livestream.model.LiveStreamMessage;
import com.metfone.selfcare.ui.androidtagview.TagGroup;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

public class MsgFriendWatchHolder extends BaseMessageLiveStreamHolder {

    private TextView tvSayHi;
    private TagGroup tagGroup;
    private CircleImageView ivAvatar;
    private TextView tvAvatar;
    private BaseSlidingFragmentActivity activity;
    private ApplicationController app;
    private MessageActionListener listenerMsg;
    private LiveStreamMessage message;

    public MsgFriendWatchHolder(View itemView, BaseSlidingFragmentActivity act, MessageActionListener listener) {
        super(itemView);
        this.activity = act;
        app = (ApplicationController) activity.getApplication();
        this.listenerMsg = listener;
        tagGroup = itemView.findViewById(R.id.tag_group);
        tvSayHi = itemView.findViewById(R.id.tvSayHi);
        ivAvatar = itemView.findViewById(R.id.ivAvatar);
        tvAvatar = itemView.findViewById(R.id.tvAvatar);
        String s1 = activity.getString(R.string.ls_invite_friend);
        String s2 = activity.getString(R.string.ls_share_social);
        tagGroup.setTags(s1, s2);
        tagGroup.setOnTagClickListener((tagView, tag) -> {
            if (listenerMsg != null && !TextUtils.isEmpty(tag))
                if (tag.equals(activity.getString(R.string.ls_invite_friend)))
                    listenerMsg.onInviteFriend();
                else
                    listenerMsg.onShareVideo(true);
        });
        ivAvatar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listenerMsg != null && message != null)
                    listenerMsg.onClickUser(message, getAdapterPosition());
            }
        });
    }

    @Override
    public void setElement(Object obj, int pos) {
        message = (LiveStreamMessage) obj;
        int sizeAvatar = (int) activity.getResources().getDimension(R.dimen.avatar_small_size);
        if (!message.isGetContactPhoneDone()) {
            message.setPhoneNumber(app.getContactBusiness().getPhoneNumberFromNumber(message.getMsisdn()));
            message.setGetContactPhoneDone(true);
        }

        PhoneNumber phoneNumber = message.getPhoneNumber();
        String friendName;
        if (phoneNumber == null) {
            if (TextUtils.isEmpty(message.getNameSender()))
                friendName = Utilities.hidenPhoneNumber(message.getMsisdn());
            else
                friendName = message.getNameSender();
            String mFriendAvatarUrl = app.getAvatarBusiness().getAvatarUrl(message.getLastAvatar(),
                    message.getMsisdn(), sizeAvatar);
            app.getAvatarBusiness().setAvatarOnMedia(ivAvatar, tvAvatar,
                    mFriendAvatarUrl, message.getMsisdn(), message.getNameSender(), sizeAvatar);
        } else {
            friendName = phoneNumber.getName();
            app.getAvatarBusiness().setPhoneNumberAvatar(ivAvatar, tvAvatar, phoneNumber, sizeAvatar);
        }
        tvSayHi.setText(String.format(activity.getString(R.string.ls_friend_watch), friendName));
    }
}

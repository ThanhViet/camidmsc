package com.metfone.selfcare.module.spoint.fragment;

import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.spoint.network.model.FilterSpoint;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

public class FillterSpointFragment extends DialogFragment implements View.OnClickListener {
    private ApplicationController applicationController;
    private PositiveListener<FilterSpoint> mCallBack;
    private RadioGroup radioGroupType, radioGroupDeal;
    private AppCompatTextView tvCancel, tvDeleteFilter;
    private RoundTextView tvFilter;
    private int typePoint, dealPoint;
    private FilterSpoint currentSpoint;

    public static FillterSpointFragment newInstance() {
        Bundle args = new Bundle();
        FillterSpointFragment fragment = new FillterSpointFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public FillterSpointFragment setListener(PositiveListener<FilterSpoint> listener) {
        this.mCallBack = listener;
        return this;
    }
    public FillterSpointFragment setModel(FilterSpoint model) {
        this.currentSpoint = model;
        return this;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFullscreenV5);
        applicationController = ApplicationController.self();
//        filterHelper = StrangerFilterHelper.getInstance(applicationController);
//        if (getArguments() != null) {
//            isAroundFilter = getArguments().getBoolean(AROUND_KEY, false);
//        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter_spoint, container, false);
        tvCancel = view.findViewById(R.id.tvCancel);
        tvFilter = view.findViewById(R.id.btnFilter);
        radioGroupType = view.findViewById(R.id.radioGroupType);
        radioGroupDeal = view.findViewById(R.id.radioGroupDeal);
        tvDeleteFilter = view.findViewById(R.id.tvDeleteFilter);

        tvFilter.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvDeleteFilter.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogV5Animation;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getDialog().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getDialog().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.white));
        }
        drawDetail();
//        radioGroupAgeTwo.setOnCheckedChangeListener(this);
//        radioGroupAgeOne.setOnCheckedChangeListener(this);
    }

    private void drawDetail() {
        tvCancel.setPaintFlags(tvCancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvDeleteFilter.setPaintFlags(tvDeleteFilter.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        radioGroupType.clearCheck();
        radioGroupDeal.clearCheck();
        if(currentSpoint != null){
            if(currentSpoint.getTypePoint() == 0){
                ((RadioButton) getView().findViewById(R.id.radio_spoint)).setChecked(true);
            }else if(currentSpoint.getTypePoint() == 1){
                ((RadioButton) getView().findViewById(R.id.radio_spoint_plus)).setChecked(true);
            }else{
                ((RadioButton) getView().findViewById(R.id.radio_all)).setChecked(true);
            }
            if(currentSpoint.getDealPoint() == 0){
                ((RadioButton) getView().findViewById(R.id.radioDealAll)).setChecked(true);
            }else if(currentSpoint.getDealPoint() == 1){
                ((RadioButton) getView().findViewById(R.id.radio_add)).setChecked(true);
            }else if(currentSpoint.getDealPoint() == 2){
                ((RadioButton) getView().findViewById(R.id.radio_minus)).setChecked(true);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvCancel:
                dismiss();
                break;
            case R.id.btnFilter:
                handleFilterConfirm();
                dismiss();
                break;
            case R.id.tvDeleteFilter:
                if(mCallBack != null){
                    mCallBack.onPositive(new FilterSpoint(2,0));
                }
                dismiss();
                break;
        }
    }
    private void handleFilterConfirm() {
        int idSpoint = radioGroupType.getCheckedRadioButtonId();
        if(idSpoint == R.id.radio_spoint){
            typePoint = 0;
        }else if(idSpoint == R.id.radio_spoint_plus){
            typePoint = 1;
        }else{
            typePoint = 2;
        }
        int idDeal = radioGroupDeal.getCheckedRadioButtonId();
        if(idDeal == R.id.radio_add){
            dealPoint = 1;
        }else if(idDeal == R.id.radio_minus){
            dealPoint = 2;
        }else{
            dealPoint = 0;
        }
        if(mCallBack != null){
            mCallBack.onPositive(new FilterSpoint(typePoint,dealPoint));
        }
    }

}

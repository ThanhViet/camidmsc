package com.metfone.selfcare.module.newdetails.utils;

import android.content.Context;

public class ToastUtils {

    public static void makeText(Context context, String text) {
        com.metfone.selfcare.v5.utils.ToastUtils.showToast(context, text);
    }

    public static void makeText(Context context, int text) {
        String msg = "";
        if (context != null) msg = context.getString(text);
        com.metfone.selfcare.v5.utils.ToastUtils.showToast(context, msg);
    }

    public static void makeText(Context context, String text, int dur) {
        com.metfone.selfcare.v5.utils.ToastUtils.showToast(context, text);
    }

}
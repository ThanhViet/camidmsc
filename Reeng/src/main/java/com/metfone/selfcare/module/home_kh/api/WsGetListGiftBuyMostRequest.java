package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetListGiftBuyMostRequest extends BaseRequest<WsGetListGiftBuyMostRequest.Request> {
    public class Request {
        @SerializedName("language")
        public String language;
        @SerializedName("isdn")
        public String isdn;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/1
 *
 */

package com.metfone.selfcare.module.myviettel.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.fragment.BaseLoginAnonymousFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.myviettel.activity.TopUpActivity;
import com.metfone.selfcare.module.myviettel.adapter.MyViettelAdapter;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;
import com.metfone.selfcare.module.myviettel.model.AccountInfo;
import com.metfone.selfcare.module.myviettel.model.DataPackageInfo;
import com.metfone.selfcare.module.myviettel.model.MoreDataProvisional;
import com.metfone.selfcare.module.myviettel.model.PromotionProvisional;
import com.metfone.selfcare.module.myviettel.model.UtilitiesProvisional;
import com.metfone.selfcare.module.myviettel.network.ApiCallback;
import com.metfone.selfcare.module.myviettel.network.MyViettelApi;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.view.HeaderFrameLayout;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyViettelFragment extends BaseLoginAnonymousFragment implements OnInternetChangedListener
        , BaseLoginAnonymousFragment.EmptyViewListener, OnMyViettelListener {
    // tuyet doi ko dc thay doi bien TAG nay
    protected final String TAG = "MyViettelFragment";
    protected Unbinder unbinder;
    protected ApplicationController mApplication;
    protected boolean isLoadingInfo;
    protected BaseSlidingFragmentActivity mActivity;
    protected Toolbar toolbar;
    protected boolean isVisibleToUser;
    protected boolean isDataInitiated;
    protected boolean isViewInitiated;

    @BindView(R.id.bg_header)
    View bgHeader;
    @BindView(R.id.statusBarHeight)
    View statusBarHeight;
    @BindView(R.id.headerController)
    LinearLayout headerController;
    @BindView(R.id.headerTop)
    HeaderFrameLayout headerTop;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    //    @BindView(R.id.iv_logo)
//    ImageView ivLogo;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_top_up)
    ImageView ivTopUp;
    @BindView(R.id.empty_layout)
    View viewEmpty;

    private AccountInfo accountInfo;
    private MoreDataProvisional moreDataItem;
    private UtilitiesProvisional utilitiesItem;
    private PromotionProvisional promotionItem;
    private ArrayList<Object> datas;
    private MyViettelAdapter adapter;

    private ListenerUtils listenerUtils;

    public static MyViettelFragment newInstance() {
        Bundle args = new Bundle();
        MyViettelFragment fragment = new MyViettelFragment();
        fragment.setArguments(args);
        return fragment;
    }

    protected boolean isLoading() {
        return isLoadingInfo;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_viettel, container, false);
        assert view != null;
        unbinder = ButterKnife.bind(this, view);
        isViewInitiated = false;
        initViewLogin(inflater, container, view);
        initEmptyView(viewEmpty, this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated visible: " + isVisibleToUser + " -------------------------");
        isViewInitiated = true;
        mApplication = (ApplicationController) mActivity.getApplication();
        initListener();
        if (mActivity instanceof ModuleActivity || canLazyLoad()) {
            loadData(true);
        }
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) listenerUtils.addListener(this);
    }

    protected void initListener() {
        if (datas == null) datas = new ArrayList<>();
        adapter = new MyViettelAdapter(mActivity);
        adapter.setListener(this);
        adapter.setItems(datas);
        BaseAdapter.setupVerticalRecycler(mActivity, recyclerView, null, adapter, false);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int currentFirstVisible;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    int index = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setEnabled(index == currentFirstVisible && currentFirstVisible == 0);
                }
            }
        });

        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadData(false);
                }
            });

        ivBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mActivity != null && !mActivity.isFinishing()) mActivity.onBackPressed();
            }
        });

        ivTopUp.setVisibility(View.GONE);
        ivTopUp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onClickTopUp();
            }
        });

        if (mActivity instanceof HomeActivity) {
            //ivLogo.setPadding(Utilities.dpToPixels(16, getResources()), 0, 0, 0);
            //ivLogo.setImageResource(R.drawable.ic_tab_my_viettel);
            tvTitle.setAllCaps(true);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setTextColor(ContextCompat.getColor(mApplication, R.color.videoColorAccent));
            ImageViewCompat.setImageTintList(ivTopUp, ColorStateList.valueOf(ContextCompat.getColor(mApplication, R.color.white)));
        } else {
            //ivLogo.setImageResource(R.drawable.ic_tab_my_viettel);
            //ivLogo.setPadding(Utilities.dpToPixels(6, getResources()), 0, 0, 0);
            ivBack.setVisibility(View.VISIBLE);
            headerTop.setVisibility(View.GONE);
            tvTitle.setPadding(0, 0, 0, 0);
            tvTitle.setAllCaps(false);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setTextColor(ContextCompat.getColor(mApplication, R.color.text_ab_title));
            headerController.setBackgroundColor(ContextCompat.getColor(mApplication, R.color.white));
            ImageViewCompat.setImageTintList(ivTopUp, ColorStateList.valueOf(ContextCompat.getColor(mApplication, R.color.bg_ab_icon)));
        }
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) unbinder.unbind();
        if (listenerUtils != null) listenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        Log.d(TAG, "setUserVisibleHint " + isVisibleToUser);
        if (canLazyLoad()) {
            loadData(true);
        }
    }

    private void loadData(boolean showLoading) {
        if (mApplication != null && !mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            boolean isViettel = mApplication.getReengAccountBusiness().isViettel();
            if (!isViettel) {
                accountInfo = new AccountInfo();
                accountInfo.setViettel(false);
                accountInfo.setUsername(mApplication.getReengAccountBusiness().getUserName());
                accountInfo.setPhoneNumber(mApplication.getReengAccountBusiness().getJidNumber());
                hideRefresh();
                isDataInitiated = true;
                if (datas == null) datas = new ArrayList<>();
                else datas.clear();
                datas.add(accountInfo);
                if (ivTopUp != null) ivTopUp.setVisibility(View.GONE);
                moreDataItem = null;
                utilitiesItem = null;
                promotionItem = null;
                if (adapter != null) adapter.notifyDataSetChanged();
                hideEmptyView();
                return;
            }
            if (showLoading) showProgressLoading();
            getMyViettelApi().getAccountInfo(new ApiCallback<AccountInfo>() {
                @Override
                public void onSuccess(String msg, AccountInfo result) throws Exception {
                    accountInfo = result;
                }

                @Override
                public void onError(String s) {

                }

                @Override
                public void onComplete() {
                    hideRefresh();
                    isDataInitiated = true;
                    if (datas == null) datas = new ArrayList<>();
                    else datas.clear();
                    if (accountInfo == null) {
                        moreDataItem = null;
                        utilitiesItem = null;
                        promotionItem = null;
                        if (!NetworkHelper.isConnectInternet(mActivity)) {
                            if (ivTopUp != null) ivTopUp.setVisibility(View.GONE);
                            showRetryView();
                            if (adapter != null) adapter.notifyDataSetChanged();
                            return;
                        }
                    }
                    hideEmptyView();
                    if (accountInfo == null) accountInfo = new AccountInfo();
                    if (mApplication != null && mApplication.getReengAccountBusiness() != null) {
                        accountInfo.setViettel(mApplication.getReengAccountBusiness().isViettel());
                        accountInfo.setUsername(mApplication.getReengAccountBusiness().getUserName());
                        accountInfo.setPhoneNumber(mApplication.getReengAccountBusiness().getJidNumber());
                    }
                    datas.add(accountInfo);
                    if (accountInfo.isViettel()) {
                        if (utilitiesItem == null) utilitiesItem = new UtilitiesProvisional();
                        datas.add(utilitiesItem);
                        if (moreDataItem != null) {
                            moreDataItem.setHasRegistered(Utilities.notEmpty(accountInfo.getData()));
                            datas.add(1, moreDataItem);
                        }
                        if (promotionItem != null) datas.add(promotionItem);
                        if (ivTopUp != null) ivTopUp.setVisibility(View.VISIBLE);
                    } else {
                        if (ivTopUp != null) ivTopUp.setVisibility(View.GONE);
                        moreDataItem = null;
                        utilitiesItem = null;
                        promotionItem = null;
                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                    if (accountInfo.isViettel()) {
                        loadListMoreData();
                        loadListPromotions();
                    }
                }
            });
        }
    }

    private void loadListMoreData() {
        if (mActivity == null || mActivity.isFinishing() || !NetworkHelper.isConnectInternet(mActivity))
            return;
        getMyViettelApi().getListDataPackagesByType("DATAPLUS", new ApiCallback<ArrayList<DataPackageInfo>>() {
            @Override
            public void onSuccess(String msg, ArrayList<DataPackageInfo> result) throws Exception {
                if (moreDataItem != null && datas != null) {
                    datas.remove(moreDataItem);
                }
                moreDataItem = null;
                if (Utilities.notEmpty(result)) {
                    moreDataItem = new MoreDataProvisional();
                    moreDataItem.setList(result);
                }
            }

            @Override
            public void onError(String s) {
                if (moreDataItem != null && datas != null) {
                    datas.remove(moreDataItem);
                }
                moreDataItem = null;
            }

            @Override
            public void onComplete() {
                if (accountInfo != null && accountInfo.isViettel() && Utilities.notEmpty(datas)) {
                    if (moreDataItem != null) {
                        moreDataItem.setHasRegistered(Utilities.notEmpty(accountInfo.getData()));
                        datas.add(1, moreDataItem);
                    }
                }
                if (adapter != null) adapter.notifyDataSetChanged();
            }
        });
    }

    private void loadListPromotions() {
        if (mActivity == null || mActivity.isFinishing() || !NetworkHelper.isConnectInternet(mActivity))
            return;
        getMyViettelApi().getListDataPackagesByType("HOT", new ApiCallback<ArrayList<DataPackageInfo>>() {
            @Override
            public void onSuccess(String msg, ArrayList<DataPackageInfo> result) throws Exception {
                if (promotionItem != null && datas != null) {
                    datas.remove(promotionItem);
                }
                promotionItem = null;
                if (Utilities.notEmpty(result)) {
                    promotionItem = new PromotionProvisional();
                    promotionItem.setList(result);
                }
            }

            @Override
            public void onError(String s) {
                if (promotionItem != null && datas != null) {
                    datas.remove(promotionItem);
                }
                promotionItem = null;
            }

            @Override
            public void onComplete() {
                if (accountInfo != null && accountInfo.isViettel() && Utilities.notEmpty(datas)) {
                    if (promotionItem != null) datas.add(promotionItem);
                }
                if (adapter != null) adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseSlidingFragmentActivity) context;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent datas) {
        super.onActivityResult(requestCode, resultCode, datas);
        if (BuildConfig.DEBUG)
            Log.d(TAG, "onActivityResult requestCode= " + requestCode + "; resultCode= " + resultCode + "; datas= " + datas + "-------");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume isVisible: " + isVisibleToUser + " -------");
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause ------------------------------");
        super.onPause();
    }

    @Override
    public void onDestroy() {
        isViewInitiated = false;
        Log.d(TAG, "onDestroy ------------------------------");
        super.onDestroy();
    }

    public boolean canLazyLoad() {
        return isVisibleToUser && isViewInitiated && !isDataInitiated;
    }

    private void registerData(final DataPackageInfo item) {
        if (mActivity == null || mActivity.isFinishing() || item == null) return;
        DialogConfirm dialog = new DialogConfirm(mActivity, true)
                .setLabel(null)
                .setMessage(item.getConfirmReg())
                .setNegativeLabel(mActivity.getString(R.string.cancel))
                .setPositiveLabel(item.getLabelReg())
                .setPositiveListener(new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        if (mActivity == null || mActivity.isFinishing() || item == null) return;
                        mActivity.showLoadingDialog("", R.string.processing);
                        getMyViettelApi().registerData(item.getType(), item.getPackageCode(), new ApiCallback<Object>() {
                            @Override
                            public void onSuccess(String msg, Object result) throws Exception {
                                if (mActivity == null || mActivity.isFinishing()) return;
                                mActivity.hideLoadingDialog();
                                mActivity.showToast(msg);
                            }

                            @Override
                            public void onError(String s) {
                                if (mActivity == null || mActivity.isFinishing()) return;
                                mActivity.hideLoadingDialog();
                                mActivity.showToast(s);
                            }

                            @Override
                            public void onComplete() {
                            }
                        });
                    }
                });
        dialog.show();
    }

    public void hideRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    public void onInternetChanged() {
        if (accountInfo == null && mActivity != null && NetworkHelper.isConnectInternet(mActivity)) {
            if (mActivity instanceof ModuleActivity || canLazyLoad()) {
                loadData(true);
            } else {
                loadData(false);
            }
        }
    }

    @Override
    public void onRetryClick() {
        loadData(true);
    }

    @Override
    public void onClickTopUp() {
        if (mActivity == null || mActivity.isFinishing()) return;
        Intent intent = new Intent(mActivity, TopUpActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClickChargingHistory() {
        if (mApplication == null) return;
        String link = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_LINK_CHARGING_HISTORY);
        if (!TextUtils.isEmpty(link) && !"-".equals(link)) {
            DeepLinkHelper.getInstance().openSchemaLink(mActivity, link);
        }
    }

    @Override
    public void onClickViettelPlus() {
        if (mApplication == null) return;
        String link = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_LINK_VIETTEL_PLUS);
        if (!TextUtils.isEmpty(link) && !"-".equals(link)) {
            DeepLinkHelper.getInstance().openSchemaLink(mActivity, link);
        }
    }

    @Override
    public void onClickShop() {
        if (mApplication == null) return;
        String link = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_LINK_SHOP);
        if (!TextUtils.isEmpty(link) && !"-".equals(link)) {
            DeepLinkHelper.getInstance().openSchemaLink(mActivity, link);
        }
    }

    @Override
    public void onClickLearnMoreSwitchToViettel() {
        if (mApplication == null) return;
        String link = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_LINK_SWITCH_TO_VIETTEL);
        if (!TextUtils.isEmpty(link) && !"-".equals(link)) {
            DeepLinkHelper.getInstance().openSchemaLink(mActivity, link);
        }
    }

    @Override
    public void onClickMoreDataInfo(DataPackageInfo item) {

    }

    @Override
    public void onClickBuyMoreData(DataPackageInfo item) {
        registerData(item);
    }

    @Override
    public void onClickPromotionInfo(DataPackageInfo item) {

    }

    @Override
    public void onClickRegisterPromotion(DataPackageInfo item) {
        registerData(item);
    }

    @Override
    public void onLoginSuccess() {
        super.onLoginSuccess();
        loadData(!isDataInitiated);
    }

    @Override
    protected String getSourceClassName() {
        return "MyViettel";
    }

    private MyViettelApi myViettelApi;

    private MyViettelApi getMyViettelApi() {
        if (myViettelApi == null) myViettelApi = new MyViettelApi();
        return myViettelApi;
    }
}

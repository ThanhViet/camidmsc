package com.metfone.selfcare.module.tiin.hometiin.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.vtm.adslib.AdsHelper;
import com.vtm.adslib.AdsListener;

import java.util.List;

public class HomeTiinAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    //    public static final int SECTION_TOP = 0;
//    public static final int SECTION_NOW = 1;
//    public static final int SECTION_FOCUS = 2;
//    public static final int SECTION_FOLLOW = 3;
    public static final int SECTION_QUOTE = 4;
    public static final int SECTION_CATEGORY_NEWS = 5;
    //    public static final int SECTION_CATEGORY_PICTURE = 9;
    //    public static final int SECTION_CATEGRY_MOVIES = 7;
    //    private static final int SECTION_VIDEO = 10;
    public static final int SECTION_EMPTY = 8;
    //todo giao dien moi
    public static final int SECTION_TOP = 0;
    public static final int SECTION_NOW = 1;
    public static final int SECTION_FOCUS = 2;
    public static final int SECTION_FOLLOW = 3;
    private static final int SECTION_VIDEO = 10;
    public static final int SECTION_LATEST = 11;

    private Activity mContext;
    private List<HomeTiinModel> itemsList;
    private TiinListener.OnHomeTiinItemListener listener;
    AdsHelper adsHelper;

    public HomeTiinAdapter(Activity mContext, List<HomeTiinModel> itemsList, TiinListener.OnHomeTiinItemListener listener) {
        this.mContext = mContext;
        this.itemsList = itemsList;
        this.listener = listener;
    }

    public void setAdsHelper(AdsHelper adsHelper) {
        this.adsHelper = adsHelper;
    }

    public void removeAll() {
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<HomeTiinModel> list) {
        this.itemsList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case SECTION_TOP:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_recyclerview, parent, false);
                break;
            case SECTION_NOW:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_event, parent, false);
                break;
            case SECTION_FOCUS:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_recyclerview, parent, false);
                break;
            case SECTION_FOLLOW:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_follow, parent, false);
                break;
//            case SECTION_QUOTE:
//                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_quote, parent, false);
//                break;
//            case SECTION_CATEGORY_NEWS:
//                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_category_1, parent, false);
//                break;
//            case SECTION_CATEGORY_PICTURE:
//                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_category_picture, parent, false);
//                break;
//            case SECTION_CATEGRY_MOVIES:
//                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_category_2, parent, false);
//                break;
            case SECTION_VIDEO:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_video, parent, false);
                break;
            case SECTION_LATEST:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_tiin_latest, parent, false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;

        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case SECTION_TOP:
                setUpSectionTop(holder, getItem(position));
                break;
            case SECTION_NOW:
                setUpSectionNow(holder, getItem(position));
                break;
            case SECTION_FOCUS:
                setUpSectionFocus(holder, getItem(position));
                break;
            case SECTION_FOLLOW:
                setUpSectionFollow(holder, getItem(position));
                break;
            case SECTION_QUOTE:
                setUpSectionQuote(holder, getItem(position));
                break;
//            case SECTION_CATEGORY_NEWS:
//                setUpSectionCategoryNews(holder, getItem(position));
//                break;
//            case SECTION_CATEGORY_PICTURE:
//                setUpSectionCategoryPicture(holder, getItem(position));
//                break;
//            case SECTION_CATEGRY_MOVIES:
//                setUpSectionCategoryMovies(holder, getItem(position));
//                break;
            case SECTION_VIDEO:
                setUpSectionVideo(holder, getItem(position));
                break;
            case SECTION_LATEST:
                setUpSectionLatest(holder, getItem(position));
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return itemsList == null ? 0 : itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        HomeTiinModel tiinModel = getItem(position);
        if (tiinModel != null) {
            if (tiinModel.getType().equals(ConstantTiin.HOME) && tiinModel.getData().size() != 0) {
                switch (tiinModel.getPosition()) {
                    case 0:
                        return SECTION_TOP;
                    case 1:
                        return SECTION_NOW;
                    case 2:
                        return SECTION_FOCUS;
                    case 3:
                        return SECTION_FOLLOW;
                    case 4:
//                        return SECTION_QUOTE;
                        return SECTION_VIDEO;
                    case 5:
                        return SECTION_LATEST;
                }
            }
//            if (tiinModel.getType().equals(ConstantTiin.CATEGORY) && tiinModel.getData().size() != 0) {
//                if (tiinModel.getTypeDisplay() == 1) {
//                    return SECTION_CATEGORY_NEWS;
//                }
//                if (tiinModel.getTypeDisplay() == 2) {
//                    return SECTION_CATEGORY_PICTURE;
//                }
//                if (tiinModel.getTypeDisplay() == 3) {
//                    return SECTION_VIDEO;
//                }
//                if (tiinModel.getTypeDisplay() == 4) {
//                    return SECTION_CATEGRY_MOVIES;
//                }
//            }
        }
        return SECTION_EMPTY;
    }

    public HomeTiinModel getItem(int position) {
        return (null != itemsList && itemsList.size() > position ? itemsList.get(position) : null);
    }

    private void setUpSectionVideo(BaseViewHolder holder, final HomeTiinModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, "Video");
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickHeader(SECTION_CATEGORY_NEWS, data.getCategoryID(), data.getHeader());
                }
            });
        }
        //todo 1
        if(data.getData().size() > 0){
            if (holder.getView(R.id.iv_cover) != null) {
                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.iv_cover));
            }
            if (holder.getView(R.id.tv_title) != null) {
                holder.setText(R.id.tv_title, data.getData().get(0).getTitle());
            }
            if(holder.getView(R.id.tv_category) != null){
                holder.setVisible(R.id.tv_category,false);
            }
            if(holder.getView(R.id.tv_datetime) != null){
                holder.setVisible(R.id.tv_datetime,false);
            }
            if (holder.getView(R.id.layout_root) != null) {
                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(data.getData().get(0));
                    }
                });
            }
            if(holder.getView(R.id.iv_play_news) != null){
                holder.setVisible(R.id.iv_play_news,true);
            }
        }
        //todo other
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView != null) {
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_horizonal_v5, true));
            }
            VideoTiinAdapter childHomeNewAdapterV5 = new VideoTiinAdapter(data, mContext, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }

    private void setUpSectionTop(BaseViewHolder holder, HomeTiinModel data) {
        if (holder == null || data == null) {
            return;
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_default_tiin, true));
        }
        BigTiinAdapter bigTiinAdapter = new BigTiinAdapter(data, mContext, listener);
        recyclerView.setAdapter(bigTiinAdapter);

        if(adsHelper != null)
            adsHelper.showAd(holder.getView(R.id.layout_ads));
    }

    private void setUpSectionNow(BaseViewHolder holder, final HomeTiinModel data) {
        if (holder == null || data == null || data.getData() == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickHeader(SECTION_NOW, -1, data.getHeader());
                }
            });
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setPadding(recyclerView.getPaddingLeft(),recyclerView.getPaddingTop(),recyclerView.getPaddingRight(), (int) mContext.getResources().getDimension(R.dimen.v5_top_header_tiin));
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        }
        NormalTiinAdapter adapter = new NormalTiinAdapter(data, mContext, listener, SECTION_NOW);
        recyclerView.setAdapter(adapter);
    }

    private void setUpSectionFocus(BaseViewHolder holder, HomeTiinModel data) {
        if (holder == null || data == null) {
            return;
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_default_tiin, true));
        }
        SpecialTiinAdapter adapter = new SpecialTiinAdapter(data, mContext, listener);
        recyclerView.setAdapter(adapter);
    }

    private void setUpSectionFollow(BaseViewHolder holder, final HomeTiinModel data) {
        if (holder == null || data == null) {
            return;
        }
        final List<TiinModel> items = data.getData();
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickHeader(SECTION_FOLLOW, -1, data.getHeader());
                }
            });
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        SmallTiinAdapter smallTiinAdapter = new SmallTiinAdapter(data, mContext, listener);
        recyclerView.setAdapter(smallTiinAdapter);
    }

    private void setUpSectionQuote(BaseViewHolder holder, HomeTiinModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (data.getData().size() > 0) {
            final TiinModel model = data.getData().get(0);
            if (holder.getView(R.id.tvContent) != null) {
                holder.setText(R.id.tvContent, model.getQuote());

            }
            if (holder.getView(R.id.tvArtist) != null) {
                holder.setText(R.id.tvArtist, model.getPoster());
            }
            if (holder.getView(R.id.btnReadMore) != null) {
                holder.setOnClickListener(R.id.btnReadMore, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(model);
                    }
                });
            }
        }
    }

//    private void setUpSectionCategoryNews(BaseViewHolder holder, final HomeTiinModel data) {
//        if (holder == null || data == null) {
//            return;
//        }
//        if (holder.getView(R.id.tvHeader) != null) {
//            holder.setText(R.id.tvHeader, data.getHeader());
//            holder.setOnClickListener(R.id.tvHeader, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    listener.onItemClickHeader(SECTION_CATEGORY_NEWS, data.getCategoryID(), data.getHeader());
//                }
//            });
//        }
//        //0
//        if (data.getData().size() > 0) {
//            if (holder.getView(R.id.ivImageBig) != null) {
//                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.ivImageBig));
//            }
//            if (holder.getView(R.id.tvTitleBig) != null) {
//                TiinUtilities.setText(holder.getView(R.id.tvTitleBig), data.getData().get(0).getTitle(), data.getData().get(0).getTypeIcon());
//            }
//            if (holder.getView(R.id.layout_root) != null) {
//                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(0));
//                    }
//                });
//            }
//        }
//        //1
//        if (data.getData().size() > 1) {
//            if (holder.getView(R.id.ivImageSmall1) != null) {
//                ImageBusiness.setImageNew(data.getData().get(1).getImage(), (ImageView) holder.getView(R.id.ivImageSmall1));
//            }
//            if (holder.getView(R.id.tvTitleSmall1) != null) {
//                TiinUtilities.setText(holder.getView(R.id.tvTitleSmall1), data.getData().get(1).getTitle(), data.getData().get(1).getTypeIcon());
//            }
//            if (holder.getView(R.id.layout_root1) != null) {
//                holder.setOnClickListener(R.id.layout_root1, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(1));
//                    }
//                });
//            }
//        }
//        //2
//        if (data.getData().size() > 2) {
//            if (holder.getView(R.id.ivImageSmall2) != null) {
//                ImageBusiness.setImageNew(data.getData().get(2).getImage(), (ImageView) holder.getView(R.id.ivImageSmall2));
//            }
//            if (holder.getView(R.id.tvTitleSmall2) != null) {
//                TiinUtilities.setText(holder.getView(R.id.tvTitleSmall2), data.getData().get(2).getTitle(), data.getData().get(2).getTypeIcon());
//            }
//            if (holder.getView(R.id.layout_root2) != null) {
//                holder.setOnClickListener(R.id.layout_root2, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(2));
//                    }
//                });
//            }
//        }
//        //3
//        if (data.getData().size() > 3) {
//            if (holder.getView(R.id.tvTitle3) != null) {
//                TiinUtilities.setText(holder.getView(R.id.tvTitle3), data.getData().get(3).getTitle(), data.getData().get(3).getTypeIcon());
//            }
//            if (holder.getView(R.id.layout_content3) != null) {
//                holder.setOnClickListener(R.id.layout_content3, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(3));
//                    }
//                });
//            }
//        }
//        //4
//        if (data.getData().size() > 4) {
//            if (holder.getView(R.id.tvTitle4) != null) {
//                TiinUtilities.setText(holder.getView(R.id.tvTitle4), data.getData().get(4).getTitle(), data.getData().get(4).getTypeIcon());
//            }
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    listener.onItemClick(data.getData().get(4));
//                }
//            });
//            if (holder.getView(R.id.layout_content4) != null) {
//                holder.setOnClickListener(R.id.layout_content4, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(4));
//                    }
//                });
//            }
//        }
//    }

//    private void setUpSectionCategoryPicture(BaseViewHolder holder, final HomeTiinModel data) {
//        if (holder == null || data == null) {
//            return;
//        }
//        if (holder.getView(R.id.tvHeader) != null) {
//            holder.setText(R.id.tvHeader, data.getHeader());
//            holder.setOnClickListener(R.id.tvHeader, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    listener.onItemClickHeader(SECTION_CATEGORY_NEWS, data.getCategoryID(), data.getHeader());
//                }
//            });
//        }
//        //0
//        if (data.getData().size() > 0) {
//            if (holder.getView(R.id.ivImagePicture1) != null) {
//                ImageLoader.setNewsImage(mContext, data.getData().get(0).getImage169(), (ImageView) holder.getView(R.id.ivImagePicture1));
//            }
//            if (holder.getView(R.id.tvTitle1) != null) {
//                holder.setText(R.id.tvTitle1, data.getData().get(0).getTitle());
//            }
//            if (holder.getView(R.id.layout_root) != null) {
//                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(0));
//                    }
//                });
//            }
//        }
//        //1
//        if (data.getData().size() > 1) {
//            if (holder.getView(R.id.ivImagePicture2) != null) {
//                ImageLoader.setNewsImage(mContext, data.getData().get(1).getImage169(), (ImageView) holder.getView(R.id.ivImagePicture2));
//            }
//            if (holder.getView(R.id.tvTitle2) != null) {
//                holder.setText(R.id.tvTitle2, data.getData().get(1).getTitle());
//            }
//            if (holder.getView(R.id.layout_root1) != null) {
//                holder.setOnClickListener(R.id.layout_root1, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(1));
//                    }
//                });
//            }
//        }
//        //2
//        if (data.getData().size() > 2) {
//            if (holder.getView(R.id.ivImagePicture3) != null) {
//                ImageLoader.setNewsImage(mContext, data.getData().get(2).getImage169(), (ImageView) holder.getView(R.id.ivImagePicture3));
//            }
//            if (holder.getView(R.id.tvTitle3) != null) {
//                holder.setText(R.id.tvTitle3, data.getData().get(2).getTitle());
//            }
//            if (holder.getView(R.id.layout_root2) != null) {
//                holder.setOnClickListener(R.id.layout_root2, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(2));
//                    }
//                });
//            }
//        }
//        //3
//        if (holder.getView(R.id.tvTitle4) != null && data.getData().size() > 3) {
//            holder.setVisible(R.id.tvTitle4, true);
//            TiinUtilities.setText(holder.getView(R.id.tvTitle4), data.getData().get(3).getTitle(), data.getData().get(3).getTypeIcon());
//            if (holder.getView(R.id.layout_root3) != null) {
//                holder.setOnClickListener(R.id.layout_root3, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(3));
//                    }
//                });
//            }
//        } else {
//            holder.setVisible(R.id.layout_root3, false);
//        }
//        //4
//        if (holder.getView(R.id.tvTitle5) != null && data.getData().size() > 4) {
//            holder.setVisible(R.id.tvTitle5, true);
//            TiinUtilities.setText(holder.getView(R.id.tvTitle5), data.getData().get(4).getTitle(), data.getData().get(4).getTypeIcon());
//            if (holder.getView(R.id.layout_root4) != null) {
//                holder.setOnClickListener(R.id.layout_root4, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(4));
//                    }
//                });
//            }
//        } else {
//            holder.setVisible(R.id.layout_root4, false);
//        }
//    }

//    private void setUpSectionCategoryMovies(BaseViewHolder holder, final HomeTiinModel data) {
//        if (holder == null || data == null) {
//            return;
//        }
//        if (holder.getView(R.id.tvHeader) != null) {
//            holder.setText(R.id.tvHeader, data.getHeader());
//            holder.setOnClickListener(R.id.tvHeader, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    listener.onItemClickHeader(SECTION_CATEGORY_NEWS, data.getCategoryID(), data.getHeader());
//                }
//            });
//        }
//        //0
//        if (data.getData().size() > 0) {
//            if (holder.getView(R.id.ivImageBig) != null) {
////                ImageLoader.setNewsImage(mContext, data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.ivImageBig));
//                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.ivImageBig));
//            }
//            if (holder.getView(R.id.tvTitle1) != null) {
//                holder.setText(R.id.tvTitle1, data.getData().get(0).getTitle());
//            }
//            if (holder.getView(R.id.layout_content) != null) {
//                holder.setOnClickListener(R.id.layout_content, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(0));
//                    }
//                });
//            }
//        }
//        //1
//        if (data.getData().size() > 1 && holder.getView(R.id.tvTitle2) != null) {
//            holder.setText(R.id.tvTitle2, data.getData().get(1).getTitle());
//            if (holder.getView(R.id.layout_content1) != null) {
//                holder.setOnClickListener(R.id.layout_content1, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(1));
//                    }
//                });
//            }
//        }
//        //2
//        if (data.getData().size() > 2 && holder.getView(R.id.tvTitle3) != null) {
//            holder.setText(R.id.tvTitle3, data.getData().get(2).getTitle());
//            if (holder.getView(R.id.layout_content2) != null) {
//                holder.setOnClickListener(R.id.layout_content2, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(2));
//                    }
//                });
//            }
//        }
//    }

    private void setUpSectionLatest(BaseViewHolder holder, HomeTiinModel data) {
        if (holder == null || data == null || data.getData() == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickHeader(SECTION_LATEST, -1, data.getHeader());
                }
            });
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        }
        NormalTiinAdapter adapter = new NormalTiinAdapter(data, mContext, listener, SECTION_LATEST);
        recyclerView.setAdapter(adapter);
    }
}

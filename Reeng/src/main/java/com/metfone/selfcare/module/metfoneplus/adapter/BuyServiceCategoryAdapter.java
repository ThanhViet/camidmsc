package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.metphone.BuyServiceCategoryModel;
import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceCategoryViewHolder;

import java.util.ArrayList;

public class BuyServiceCategoryAdapter extends RecyclerView.Adapter<BuyServiceCategoryViewHolder> {
    private Context mContext;
    private ArrayList<BuyServiceCategoryModel> arr;
    private final View.OnClickListener mServiceDetailListener;

    public BuyServiceCategoryAdapter(Context context, ArrayList<BuyServiceCategoryModel> arr, View.OnClickListener serviceDetailListener) {
        this.mContext = context;
        this.arr = arr;
        this.mServiceDetailListener = serviceDetailListener;
    }

    @NonNull
    @Override
    public BuyServiceCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mp_item_buy_services_category, parent, false);
        return new BuyServiceCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyServiceCategoryViewHolder holder, int position) {
        BuyServiceCategoryModel model = arr.get(position);
        holder.mImage.setImageResource(model.imgIcon);
        holder.mTitle.setText(model.categoryName);
        holder.mContent.setText(model.categoryDescription);
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                model.arrColor);
        gd.setCornerRadius(15f);
        holder.mLayout.setBackground(gd);
        holder.mPosition = position;
        holder.itemView.setOnClickListener(mServiceDetailListener);
        holder.itemView.setTag(holder);
    }

    @Override
    public int getItemCount() {
        if (arr != null) {
            return arr.size();
        }
        return -1;
    }
}

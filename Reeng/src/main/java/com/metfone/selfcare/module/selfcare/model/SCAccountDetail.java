package com.metfone.selfcare.module.selfcare.model;

import java.util.ArrayList;

public class SCAccountDetail {

    private ArrayList<SCAccountDataInfo> accountDataInfo;
    private ArrayList<SCPackage> sevicesList;
    private ArrayList<SCPackage> packageList;

    public ArrayList<SCAccountDataInfo> getAccountDataInfo() {
        if(accountDataInfo == null)
            return new ArrayList<>();
        return accountDataInfo;
    }

    public void setAccountDataInfo(ArrayList<SCAccountDataInfo> accountDataInfo) {
        this.accountDataInfo = accountDataInfo;
    }

    public ArrayList<SCPackage> getSevicesList() {
        if(sevicesList == null)
            return new ArrayList<>();
        return sevicesList;
    }

    public void setSevicesList(ArrayList<SCPackage> sevicesList) {
        this.sevicesList = sevicesList;
    }

    public ArrayList<SCPackage> getPackageList() {
        if(packageList == null)
            return new ArrayList<>();
        return packageList;
    }

    public void setPackageList(ArrayList<SCPackage> packageList) {
        this.packageList = packageList;
    }

    @Override
    public String toString() {
        return "SCAccountDetail{" +
                "accountDataInfo=" + accountDataInfo +
                ", sevicesList=" + sevicesList +
                '}';
    }
}

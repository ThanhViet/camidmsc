package com.metfone.selfcare.module.home_kh.api;

import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationClearResponse;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationReadResponse;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationResponse;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsAccountInfoResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 *
 */
public interface KhHomeServiceDev {

    @POST("CamIDAutoLogin")
    Call<LoginResponse> autoLogin(@Body RequestBody requestBody);

    @POST("UserLogin")
    Call<LoginResponse> login(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsAccountInfoResponse> wsAccountInfo(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsGetAccountRankInfoResponse> wsAccountRank(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsGetGiftSearchResponse> wsGtAllPartnerGiftSearch(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetGiftListResponse> wsGtAllPartnerGiftDetail(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<KhNotificationResponse> wsGetListCamIDNotification(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<KhNotificationReadResponse> wsUpdateIsReadCamIDNotification(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<KhNotificationClearResponse> wsClearAllCamIdNotification(@Body RequestBody requestBody);

}
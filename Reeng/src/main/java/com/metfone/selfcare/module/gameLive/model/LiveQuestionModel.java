package com.metfone.selfcare.module.gameLive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class LiveQuestionModel implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("gameID")
    @Expose
    private int gameID;

    @SerializedName("number")
    @Expose
    private int number;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("timeStart")
    @Expose
    private long timeStart;

    @SerializedName("timeStop")
    @Expose
    private long timeStop;

    @SerializedName("prize")
    @Expose
    private int prize;

    @SerializedName("listAnswer")
    @Expose
    private ArrayList<LiveAnswerModel> listAnswer = new ArrayList<>();

    private boolean isCorrect = false;
    private boolean isWinPrize = false;
    private int questionCorrect = 0;

    public int getPrize() {
        return prize;
    }

    public boolean isWinPrize() {
        return isWinPrize;
    }

    public void setWinPrize(boolean winPrize) {
        isWinPrize = winPrize;
    }

    public int getQuestionCorrect() {
        return questionCorrect;
    }

    public void setQuestionCorrect(int questionCorrect) {
        this.questionCorrect = questionCorrect;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGameID() {
        return gameID;
    }

    public void setGameID(int gameID) {
        this.gameID = gameID;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public long getTimeStop() {
        return timeStop;
    }

    public void setTimeStop(long timeStop) {
        this.timeStop = timeStop;
    }

    public ArrayList<LiveAnswerModel> getListAnswer() {
        return listAnswer;
    }

    public void setListAnswer(ArrayList<LiveAnswerModel> listAnswer) {
        this.listAnswer = listAnswer;
    }

    @Override
    public String toString() {
        return "LiveQuestionModel{" +
                "id=" + id +
                ", gameID=" + gameID +
                ", number=" + number +
                ", title='" + title + '\'' +
                ", timeStart=" + timeStart +
                ", timeStop=" + timeStop +
                ", listAnswer=" + listAnswer +
                '}';
    }
}

package com.metfone.selfcare.module.metfoneplus.topup.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.HistoryResponse;

import java.util.List;

public class HistoryTopUpViewHolder extends BaseViewHolder {
    public HistoryTopUpViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
    }

    private TextView tvPhone;
    private TextView tvAmount;
    private TextView tvTime;
    private TextView tvDate;

    @Override
    public void initViewHolder(View v) {
        if (mData.get(0) instanceof HistoryResponse.History) {
            tvPhone = v.findViewById(R.id.tvPhone);
            tvAmount = v.findViewById(R.id.tvAmount);
            tvTime = v.findViewById(R.id.tvTime);
            tvDate = v.findViewById(R.id.tvDate);
        }
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        if (obj.get(pos) instanceof HistoryResponse.History) {
            HistoryResponse.History history = (HistoryResponse.History) obj.get(pos);
            tvPhone.setText(String.format(context.getString(R.string.m_p_top_up_history_phone_number), history.getIsdn()));
            tvAmount.setText("$ " + history.getAmount());
            tvTime.setText(history.getRefillDate());
            tvDate.setVisibility(history.getTitle().isEmpty()?View.GONE:View.VISIBLE);
            if (!history.getTitle().isEmpty()) {
                tvDate.setText(history.getTitle());
            }

        }

    }

    @Override
    public void onItemViewClick(View v, int pos) {

    }
}

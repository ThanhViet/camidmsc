package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import com.chaos.view.PinView;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.message.ThreadListFragmentRecycleView;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.module.home_kh.activity.SettingKhActivity;
import com.metfone.selfcare.ui.WindowShaker;
import com.metfone.selfcare.ui.dialog.BottomSheetFingerPrint;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.v5.utils.ToastUtils;
import com.metfone.selfcare.v5.widget.ViewPIN;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PINSettingKhFragment extends BaseSettingKhFragment implements BottomSheetFingerPrint.FingerPrintCallback {

    private static final String TYPE_PIN_SETTING = "type_pin";
    private static final String THREAD_MESSAGE = "thread";
    public static final int TYPE_SET_NEW_PIN = 0;
    public static final int TYPE_CHANGE_PIN = 1;
    public static final int TYPE_SET_HIDE_THREAD = 2;
    public static final int TYPE_OPEN_THREAD = 3;

    private static final String TAG = PINSettingKhFragment.class.getSimpleName();
    //    @BindView(R.id.app_lock_input_1)
//    ImageView appLockInput1;
//    @BindView(R.id.app_lock_input_2)
//    ImageView appLockInput2;
//    @BindView(R.id.app_lock_input_3)
//    ImageView appLockInput3;
//    @BindView(R.id.app_lock_input_4)
//    ImageView appLockInput4;
    Unbinder unbinder;
    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView tvTitleToolbar;
    @BindView(R.id.tvTitlePin)
    AppCompatTextView tvTitlePIN;
    @BindView(R.id.ivUseFingerprint)
    CircleImageView ivUseFingerprint;
    @BindView(R.id.tvNotePin)
    AppCompatTextView tvNotePin;
    @BindView(R.id.pinView)
    PinView pinView;

    @BindView(R.id.viewPIN)
    ViewPIN viewPIN;

    @BindView(R.id.icBackToolbar)
    AppCompatImageView icBackToolbar;


    private StringBuilder sbEnterCode = new StringBuilder();
    private int typePINSetting;
    private boolean needReEnterPIN = false;
    private int stateChangePIN = 0;
    private String PINBeforeReEnter;
    private ThreadMessage threadMessage;

    private BaseSlidingFragmentActivity mActivity;
    private ApplicationController mApp;

    private WindowShaker.Option option;

    private BottomSheetFingerPrint mBottomSheetFingerPrint;
    private Handler mHandler = new Handler();

    public static PINSettingKhFragment newInstance(int type) {
        PINSettingKhFragment f = new PINSettingKhFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TYPE_PIN_SETTING, type);
        f.setArguments(bundle);
        return f;
    }

    public static PINSettingKhFragment newInstance(int type, ThreadMessage threadMessage) {
        PINSettingKhFragment f = new PINSettingKhFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TYPE_PIN_SETTING, type);
        bundle.putSerializable(THREAD_MESSAGE, threadMessage);
        f.setArguments(bundle);
        return f;
    }


    @Override
    public String getName() {
        return null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseSlidingFragmentActivity) getActivity();
        if (mActivity == null) return;
        mApp = (ApplicationController) mActivity.getApplication();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Override
    protected void initView(View view) {
        setTitle(R.string.kh_setting_hide_conversation_change_pin);

        if (getArguments() != null) {
            typePINSetting = getArguments().getInt(TYPE_PIN_SETTING);
            threadMessage = (ThreadMessage) getArguments().getSerializable(THREAD_MESSAGE);
        }

        if (!Config.Features.FLAG_SUPPORT_FINGERPRINT_AUTH) {
            ivUseFingerprint.setVisibility(View.GONE);
        } else {
            ivUseFingerprint.setVisibility(View.VISIBLE);
        }
//        InputMethodUtils.showSoftKeyboard(getActivity(), viewPIN);
        InputMethodUtils.showSoftKeyboard(getActivity(), pinView);
        option = new WindowShaker.Option.Builder(mActivity).setOffsetY(0).setVibrate(true).build();

//        mActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
//        View mActionBarView = mActivity.getToolBarView();
//        EllipsisTextView mTvwTitle = (EllipsisTextView) mActionBarView.findViewById(R.id.ab_title);
//        ImageView mImgBack = (ImageView) mActionBarView.findViewById(R.id.ab_back_btn);
//        ImageView mImgOption = (ImageView) mActionBarView.findViewById(R.id.ab_more_btn);
//        mImgOption.setVisibility(View.GONE);
        if (typePINSetting == TYPE_SET_NEW_PIN) {
            tvTitlePIN.setText(mActivity.getString(R.string.kh_setting_pin_enter_old));
//            tvNotePin.setText(mActivity.getString(R.string.des_note_pin));
        } else if (typePINSetting == TYPE_SET_HIDE_THREAD || typePINSetting == TYPE_OPEN_THREAD) {
            String title;
            if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                title = threadMessage.getThreadName();
            } else
                title = mActivity.getString(R.string.enter_pin);
//            tvTitleToolbar.setText(title);
            tvTitlePIN.setText(R.string.enter_pin);
//            tvNotePin.setVisibility(View.VISIBLE);
            // TODO: [START] Cambodia version
//            drawTextResetPin();
            // TODO: [END] Cambodia version
        } else if (typePINSetting == TYPE_CHANGE_PIN) {
            tvTitlePIN.setText(mActivity.getString(R.string.kh_setting_pin_enter_old));
        }
//        mImgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mActivity.onBackPressed();
//            }
//        });

        pinChangedListener();
    }

    private void pinChangedListener() {
        pinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && charSequence.length() == viewPIN.getItemCount()) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (getView() != null) {
                                pinView.setText(null);
                                onPassCodeInputted(charSequence.toString());
                            }
                        }
                    }, 300);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        pinView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE && viewPIN.getText().length() == viewPIN.getItemCount()) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (getView() != null) {
                                pinView.setText(null);
                                onPassCodeInputted(viewPIN.getText().toString());
                            }
                        }
                    }, 300);
                }
                return true;
            }
        });


//        viewPIN.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (charSequence != null && charSequence.length() == viewPIN.getItemCount()) {
//                    mHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (getView() != null) {
//                                viewPIN.reset();
//                                onPassCodeInputted(charSequence.toString());
//                            }
//                        }
//                    }, 300);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
////        }
//        viewPIN.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//                if (i == EditorInfo.IME_ACTION_DONE && viewPIN.getText().length() == viewPIN.getItemCount()) {
//                    mHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (getView() != null) {
//                                viewPIN.reset();
//                                onPassCodeInputted(viewPIN.getText().toString());
//                            }
//                        }
//                    }, 300);
//                }
//                return true;
//            }
//        });
        mVibrator = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
    }

    private void drawTextResetPin() {
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
//                ds.setColor(ds.linkColor);    // you can use custom color
                ds.setUnderlineText(true);    // this remove the underline
            }

            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(mActivity, SettingKhActivity.class);
                intent.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.SETTING_HIDDEN_THREAD);
                intent.putExtra(Constants.SETTINGS.RESET_PIN, true);
                mActivity.startActivity(intent, true);
            }
        };
        String textNormal = mActivity.getString(R.string.des_forget_pin) + " ";
        String textToSpan = mActivity.getString(R.string.forget_pin_reset);
        String textToShow = textNormal + textToSpan;
        SpannableString spannableString = new SpannableString(textToShow);
        int START_TEXT = textNormal.length();
        int END_TEXT = START_TEXT + textToSpan.length();
        spannableString.setSpan(clickableSpan, START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.bg_mocha)),
                START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvNotePin.setText(spannableString);
        tvNotePin.setMovementMethod(LinkMovementMethod.getInstance());
        tvNotePin.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        InputMethodUtils.hideSoftKeyboard(getActivity());
    }

    @OnClick({R.id.icBackToolbar, R.id.ivUseFingerprint})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivUseFingerprint:
                handleFingerPrintAuth(true);
                break;
            case R.id.icBackToolbar:
                getActivity().onBackPressed();
                break;
        }
    }

    private void enterCode(String code) {
        if (sbEnterCode.length() >= 4) return;
        sbEnterCode.append(code);
//        drawInputCode();
        if (sbEnterCode.length() == 4) {
            onPassCodeInputted(sbEnterCode.toString());
        }
    }

//    private void drawInputCode() {
//        if (TextUtils.isEmpty(sbEnterCode)) {
//            appLockInput1.setSelected(false);
//            appLockInput2.setSelected(false);
//            appLockInput3.setSelected(false);
//            appLockInput4.setSelected(false);
//        } else if (sbEnterCode.length() == 1) {
//            appLockInput1.setSelected(true);
//            appLockInput2.setSelected(false);
//            appLockInput3.setSelected(false);
//            appLockInput4.setSelected(false);
//        } else if (sbEnterCode.length() == 2) {
//            appLockInput1.setSelected(true);
//            appLockInput2.setSelected(true);
//            appLockInput3.setSelected(false);
//            appLockInput4.setSelected(false);
//        } else if (sbEnterCode.length() == 3) {
//            appLockInput1.setSelected(true);
//            appLockInput2.setSelected(true);
//            appLockInput3.setSelected(true);
//            appLockInput4.setSelected(false);
//        } else {
//            appLockInput1.setSelected(true);
//            appLockInput2.setSelected(true);
//            appLockInput3.setSelected(true);
//            appLockInput4.setSelected(true);
//        }
//    }

    private void onPassCodeInputted(String code) {
        if (typePINSetting == TYPE_SET_NEW_PIN) {
            onSetNewPIN(code);
        } else if (typePINSetting == TYPE_SET_HIDE_THREAD) {
            String encryptPIN = EncryptUtil.encryptSHA256(code);
            String currentPIN = mApp.getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
            if (!TextUtils.isEmpty(encryptPIN) && encryptPIN.equals(currentPIN)) {
                EventBus.getDefault().post(new ThreadListFragmentRecycleView.HideThreadChatEvent(typePINSetting));
                mActivity.setResult(Activity.RESULT_OK);
                mActivity.finish();
            } else {
                setTextWrongPIN();
                sbEnterCode = new StringBuilder();
//                drawInputCode();
            }
        } else if (typePINSetting == TYPE_OPEN_THREAD) {
            String encryptPIN = EncryptUtil.encryptSHA256(code);
            String currentPIN = mApp.getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
            if (!TextUtils.isEmpty(encryptPIN) && encryptPIN.equals(currentPIN)) {
                EventBus.getDefault().post(new ThreadListFragmentRecycleView.HideThreadChatEvent(typePINSetting, threadMessage));
                mActivity.setResult(Activity.RESULT_OK);
                mActivity.finish();
            } else {
                WindowShaker.shake(this, option);
                setTextWrongPIN();
                sbEnterCode = new StringBuilder();
//                drawInputCode();
            }
        } else if (typePINSetting == TYPE_CHANGE_PIN) {
            if (stateChangePIN == 0) {
                String encryptPIN = EncryptUtil.encryptSHA256(code);
                if (TextUtils.isEmpty(encryptPIN)) {
                    ToastUtils.showToast(getContext(), getString(R.string.e601_error_but_undefined));
                    return;
                }
                String currentPIN = mApp.getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
                if (encryptPIN.equals(currentPIN)) {
                    stateChangePIN++;
                    tvTitlePIN.setText(mActivity.getString(R.string.kh_setting_pin_enter_new));
                    tvNotePin.setText(mActivity.getString(R.string.des_note_pin));
                } else {
                    tvNotePin.setText(mActivity.getString(R.string.incorrect_re_enter_pin));
                }
                sbEnterCode = new StringBuilder();
//                drawInputCode();
            } else if (stateChangePIN == 1) {
                String encryptPIN = EncryptUtil.encryptSHA256(code);
                if (TextUtils.isEmpty(encryptPIN)) {
                    ToastUtils.showToast(getContext(), getString(R.string.e601_error_but_undefined));
                    return;
                }
                String currentPIN = mApp.getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
                if (encryptPIN.equals(currentPIN)) {
                    ToastUtils.showToast(getContext(), getString(R.string.msg_duplicate_pin));
                } else {
                    stateChangePIN++;
                    PINBeforeReEnter = code;
                    needReEnterPIN = true;
                    tvTitlePIN.setText(R.string.kh_setting_pin_enter_new);
                    tvNotePin.setText("");
                }
                sbEnterCode = new StringBuilder();
//                drawInputCode();
            } else if (stateChangePIN == 2) {
                onSetNewPIN(code);
            }
        }
    }

    private void setTextWrongPIN() {
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
//                ds.setColor(ds.linkColor);    // you can use custom color
                ds.setUnderlineText(true);    // this remove the underline
            }

            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(mActivity, SettingKhActivity.class);
                intent.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.SETTING_HIDDEN_THREAD);
                intent.putExtra(Constants.SETTINGS.RESET_PIN, true);
                mActivity.startActivity(intent, true);
            }
        };
        String textNormal = mActivity.getString(R.string.enter_wrong_pin) + " ";
        String textToSpan = mActivity.getString(R.string.forget_pin_reset);
        String textToShow = textNormal + textToSpan;
        SpannableString spannableString = new SpannableString(textToShow);
        int START_TEXT = textNormal.length();
        int END_TEXT = START_TEXT + textToSpan.length();
        spannableString.setSpan(clickableSpan, START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.bg_mocha)),
                START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvNotePin.setText(spannableString);
        tvNotePin.setMovementMethod(LinkMovementMethod.getInstance());
        tvNotePin.setVisibility(View.VISIBLE);
    }

    private void onSetNewPIN(String code) {
        if (needReEnterPIN) {
            if (code.equals(PINBeforeReEnter)) {
                String encryptPIN = EncryptUtil.encryptSHA256(code);
                mApp.getPref().edit().putString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, encryptPIN).apply();
                if (typePINSetting == TYPE_CHANGE_PIN) {
                    ToastUtils.showToastSuccess(getContext(), getString(R.string.msg_change_pin_success));
                    mActivity.onBackPressed();
                } else {
                    EventBus.getDefault().post(new ThreadListFragmentRecycleView.HideThreadChatEvent(typePINSetting));
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                }
            } else {
                tvTitlePIN.setText(mActivity.getString(R.string.incorrect_re_enter_pin));
                sbEnterCode = new StringBuilder();
//                drawInputCode();
            }
        } else {
            PINBeforeReEnter = code;
            needReEnterPIN = true;
            tvTitlePIN.setText(mActivity.getString(R.string.reenter_new_pin));
            tvNotePin.setText("");
            sbEnterCode = new StringBuilder();
//            drawInputCode();
        }
    }

    private void deleteCode() {
        if (!TextUtils.isEmpty(sbEnterCode)) {
            int length = sbEnterCode.length();
            sbEnterCode.deleteCharAt(length - 1);
//            drawInputCode();
        }
    }


    private boolean isSupportFingerPrintAuth = false;
    private boolean isFingerPrintEnrolled = false;

    private void checkSupportFingerPrintAuth() {
        try {
            FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(mActivity);
            isSupportFingerPrintAuth = fingerprintManager.isHardwareDetected();
            isFingerPrintEnrolled = fingerprintManager.hasEnrolledFingerprints();
//            boolean isFingerPrintEnable = mApp.getPref().getBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, false);
//            mApp.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, isFingerPrintEnable && isSupportFingerPrintAuth && isFingerPrintEnrolled).apply();
        } catch (Exception e) {
        }
    }

    boolean isEnableUseFingerprint;

    @Override
    public void onResume() {
        super.onResume();
        isEnableUseFingerprint = mApp.getPref().getBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, false);
        checkSupportFingerPrintAuth();
        if (typePINSetting == TYPE_OPEN_THREAD || typePINSetting == TYPE_SET_HIDE_THREAD) {
            handleFingerPrintAuth(false);
            if (isEnableUseFingerprint)
                ivUseFingerprint.setVisibility(View.VISIBLE);
            else
                ivUseFingerprint.setVisibility(View.GONE);
        } else {
            ivUseFingerprint.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        if (mBottomSheetFingerPrint != null) {
            mBottomSheetFingerPrint.dismiss();
            mBottomSheetFingerPrint = null;
        }
        super.onPause();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_pin_setting_kh;
    }

    private void handleFingerPrintAuth(boolean isFingerPrintImvClick) {

        if (!isSupportFingerPrintAuth || !isEnableUseFingerprint) return;
        if (mFingerPrintErrorCount < 5) {
            if (isFingerPrintEnrolled) {
                mBottomSheetFingerPrint = new BottomSheetFingerPrint(mActivity, this);
                mBottomSheetFingerPrint.show();
            } else {
                if (isFingerPrintImvClick) {
                    mActivity.showToast(R.string.fingerprint_need_register);
                }
            }
        } else {
            mActivity.showToast(R.string.fingerprint_too_many_error);
        }
    }

    private Vibrator mVibrator;
    private int mFingerPrintErrorCount = 0;

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        if (mBottomSheetFingerPrint != null) {
            mBottomSheetFingerPrint.dismiss();
        }
        if (tvNotePin != null) {
            tvNotePin.setText(errString);
        }
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {

    }

    @Override
    public void onAuthenticationFailed() {
        if (mVibrator != null) {
            long[] pattern = {0, 50, 250, 50, 250};
            mVibrator.vibrate(pattern, -1);
        }
        mFingerPrintErrorCount++;
        if (mFingerPrintErrorCount >= 5) {
            if (mBottomSheetFingerPrint != null) {
                mBottomSheetFingerPrint.dismiss();
            }
            if (tvNotePin != null) {
                tvNotePin.setText(R.string.fingerprint_too_many_error);
            }
        }
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        if (mVibrator != null) {
            long[] pattern = {0, 50, 250};
            mVibrator.vibrate(pattern, -1);
        }
        if (mBottomSheetFingerPrint != null) {
            mBottomSheetFingerPrint.dismiss();
        }
//        appLockInput1.setSelected(true);
//        appLockInput2.setSelected(true);
//        appLockInput3.setSelected(true);
//        appLockInput4.setSelected(true);

        EventBus.getDefault().post(new ThreadListFragmentRecycleView.HideThreadChatEvent(typePINSetting));
        mActivity.setResult(Activity.RESULT_OK);
        mActivity.finish();

    }
}

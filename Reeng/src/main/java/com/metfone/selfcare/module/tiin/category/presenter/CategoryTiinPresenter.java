package com.metfone.selfcare.module.tiin.category.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.module.tiin.base.BasePresenter;
import com.metfone.selfcare.module.tiin.category.fragment.CategoryTiinFragment;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.module.tiin.network.request.TiinRequest;

import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryTiinPresenter extends BasePresenter implements ICategoryTiinMvpPresenter {
    private TiinApi mTiinApi;
    long startTimeCategory, startTimeEvent, startTimeMostView;

    public CategoryTiinPresenter() {
        mTiinApi = new TiinApi(ApplicationController.self());
    }

    HttpCallBack httpCallBackCategory = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof CategoryTiinFragment)) {
                return;
            }
            Gson gson = new Gson();
            ArrayList<TiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<ArrayList<TiinModel>>() {
            }.getType());

            ((CategoryTiinFragment) getMvpView()).binDataCategory(response);
            ((CategoryTiinFragment) getMvpView()).loadDataSuccess(true);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_TIIN_BY_CATEGORY, (endTime - startTimeCategory) + "", startTimeCategory + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof CategoryTiinFragment)) {
                return;
            }
            ((CategoryTiinFragment) getMvpView()).loadDataSuccess(false);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_TIIN_BY_CATEGORY, (endTime - startTimeCategory) + "", startTimeCategory + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
    HttpCallBack httpCallBackMostView = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof CategoryTiinFragment)) {
                return;
            }
            Gson gson = new Gson();
            ArrayList<TiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<ArrayList<TiinModel>>() {
            }.getType());

            ((CategoryTiinFragment) getMvpView()).binDataCategory(response);
            ((CategoryTiinFragment) getMvpView()).loadDataSuccess(true);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_MOST_VIEW, (endTime - startTimeMostView) + "", startTimeMostView + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof CategoryTiinFragment)) {
                return;
            }
            ((CategoryTiinFragment) getMvpView()).loadDataSuccess(false);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_MOST_VIEW, (endTime - startTimeMostView) + "", startTimeMostView + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
    HttpCallBack eventCallBack = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof CategoryTiinFragment)) {
                return;
            }
            Gson gson = new Gson();
            TiinEventModel response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<TiinEventModel>() {
            }.getType());

            ((CategoryTiinFragment) getMvpView()).bindDataEvent(response);
            ((CategoryTiinFragment) getMvpView()).loadDataSuccess(true);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_EVENT, (endTime - startTimeEvent) + "", startTimeEvent + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof CategoryTiinFragment)) {
                return;
            }
            ((CategoryTiinFragment) getMvpView()).loadDataSuccess(false);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_EVENT, (endTime - startTimeEvent) + "", startTimeEvent + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    @Override
    public void getCategory(int id, int page, int num) {
        startTimeCategory = System.currentTimeMillis();
        mTiinApi.getCategory(httpCallBackCategory, new TiinRequest(page, num, id));
    }

    @Override
    public void getListEvent(int id, int page, int num) {
        startTimeEvent = System.currentTimeMillis();
        mTiinApi.getListEvent(eventCallBack, new TiinRequest(page, num, id));
    }

    @Override
    public void getMostView(int pid, int cid, int page, int num) {
        startTimeMostView = System.currentTimeMillis();
        mTiinApi.getMostView(httpCallBackMostView, new TiinRequest(cid, pid, page, num));
    }

    @Override
    public void getHashTag(int page, String hashTag) {
        mTiinApi.getHashTag(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                if (!isViewAttached() || !(getMvpView() instanceof CategoryTiinFragment)) {
                    return;
                }
                Gson gson = new Gson();
                ArrayList<TiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<ArrayList<TiinModel>>() {
                }.getType());

                ((CategoryTiinFragment) getMvpView()).binDataCategory(response);
                ((CategoryTiinFragment) getMvpView()).loadDataSuccess(true);
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
            }
        },""+page,hashTag);
    }
}

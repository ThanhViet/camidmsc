/*
 * Copyright (c) 2017.
 * www.bigzun.com
 */

package com.metfone.selfcare.module.keeng.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseHolder;

/**
 * Created by namnh40 on 3/23/2017.
 */

public class RankHomeHolder extends BaseHolder {

    public TextView tvName, tvSinger, tvPosition;
    public ImageView image, cover;
    public ImageView btnOption;
    public View mediaView;

    public RankHomeHolder(View itemView) {
        super(itemView);
        image = (ImageView) itemView.findViewById(R.id.image);
        tvName = (TextView) itemView.findViewById(R.id.title);
        tvSinger = (TextView) itemView.findViewById(R.id.singer);
        tvPosition = (TextView) itemView.findViewById(R.id.position);
        btnOption = (ImageView) itemView.findViewById(R.id.button_option);
        cover = (ImageView) itemView.findViewById(R.id.cover);
        mediaView = itemView.findViewById(R.id.layout_media);
    }

}

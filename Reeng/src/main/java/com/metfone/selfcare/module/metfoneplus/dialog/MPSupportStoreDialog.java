package com.metfone.selfcare.module.metfoneplus.dialog;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.Store;

public class MPSupportStoreDialog extends DialogFragment {
    public static final String TAG = MPSupportStoreDialog.class.getSimpleName();
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String ISDN_STORE = "isdn_store";
    private static final String STORE_NAME = "store_name";

    private Context mContext;
    private String mLatitude;
    private String mLongitude;
    private String mIsdnStore;
    private String mStoreName;

    private View.OnClickListener mDirectionListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mLatitude == null) {
                return;
            }

            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + mLatitude + "," + mLongitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(mContext.getPackageManager()) != null) {
                mContext.startActivity(mapIntent);
            } else {
                Toast.makeText(mContext, mContext.getString(R.string.msg_install_map_app), Toast.LENGTH_LONG).show();
            }
            dismissAllowingStateLoss();
        }
    };

    private View.OnClickListener mCallStoreListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String content = String.format(getString(R.string.m_p_dialog_support_store_call), mStoreName);
            MPConfirmationCallDialog confirmationCallDialog = MPConfirmationCallDialog
                    .newInstance(content, mIsdnStore, R.string.m_p_dialog_confirmation_call_yes, R.string.m_p_dialog_confirmation_call_close);
            confirmationCallDialog.show(getParentFragmentManager(), MPConfirmationCallDialog.TAG);

            dismissAllowingStateLoss();
        }
    };

    public MPSupportStoreDialog() {

    }

    public static MPSupportStoreDialog newInstance(Store store) {
        MPSupportStoreDialog supportStoreDialog = new MPSupportStoreDialog();
        Bundle bundle = new Bundle();
        bundle.putString(LATITUDE, String.valueOf(store.getLatitude()));
        bundle.putString(LONGITUDE, String.valueOf(store.getLongitude()));
        bundle.putString(ISDN_STORE, store.getIsdn());
        bundle.putString(STORE_NAME, store.getName());
        supportStoreDialog.setArguments(bundle);
        return supportStoreDialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFullScreen);

        if (getArguments() != null) {
            mLatitude = getArguments().getString(LATITUDE);
            mLongitude = getArguments().getString(LONGITUDE);
            mIsdnStore = getArguments().getString(ISDN_STORE);
            mStoreName = getArguments().getString(STORE_NAME);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mp_support_store, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.dialog_mp_direction_top_layout_button)
                .setOnClickListener(mDirectionListener);

        view.findViewById(R.id.dialog_mp_call_store_middle_layout_button)
                .setOnClickListener(mCallStoreListener);

        view.findViewById(R.id.layout_root_dialog_mp_support_store)
                .setOnClickListener((v) -> {
                    dismissAllowingStateLoss();
                });
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissAllowingStateLoss();
    }
}

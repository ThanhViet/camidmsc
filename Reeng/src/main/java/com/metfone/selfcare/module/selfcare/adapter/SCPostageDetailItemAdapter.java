package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.model.SCPostageDetail;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class SCPostageDetailItemAdapter extends BaseAdapter<BaseViewHolder> {

    private ArrayList<SCPostageDetail> data;
    private String myFormat = "HH:mm:ss"; //In which you need put here
    private SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    private int postType;

    public SCPostageDetailItemAdapter(Context context, int postType) {
        super(context);
        this.postType = postType;
    }

    public void setItemsList(ArrayList<SCPostageDetail> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_postage_detail, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        SCPostageDetail model = data.get(position);
        if(model != null)
        {
            holder.setText(R.id.tvName, TextUtils.isEmpty(model.getIsdn()) ? "" : model.getIsdn());
            if(postType == SCConstants.POST_TYPE.DATA)
            {
                holder.setText(R.id.tvDuration, model.getDuration() + " " + SCConstants.SC_DATA_CURRENTCY);
            }
            else
            {
                holder.setText(R.id.tvDuration, DateTimeUtils.milliSecondsToTimerSelfcare(model.getDuration() * 1000));
            }
            holder.setText(R.id.tvValue, SCUtils.numberFormat(model.getValue()) + SCConstants.SC_CURRENTCY);
            holder.setText(R.id.tvTime, sdf.format(model.getStart_time()));

            holder.setImageResource(R.id.imvImage, model.getIcon());
        }
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }


}
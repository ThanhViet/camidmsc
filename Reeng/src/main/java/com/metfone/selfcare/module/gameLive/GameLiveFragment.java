package com.metfone.selfcare.module.gameLive;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.gameLive.dialog.DialogGameResult;
import com.metfone.selfcare.module.gameLive.dialog.DialogGameRule;
import com.metfone.selfcare.module.gameLive.model.LiveGameModel;
import com.metfone.selfcare.module.gameLive.model.LiveMessageModel;
import com.metfone.selfcare.module.gameLive.model.LiveQuestionModel;
import com.metfone.selfcare.module.gameLive.model.WinnerModel;
import com.metfone.selfcare.module.gameLive.network.APICallBack;
import com.metfone.selfcare.module.gameLive.network.RetrofitClientInstance;
import com.metfone.selfcare.module.gameLive.network.parse.AbsResultData;
import com.metfone.selfcare.module.gameLive.network.parse.RestGameResult;
import com.metfone.selfcare.module.gameLive.network.parse.RestWinner;
import com.metfone.selfcare.module.gameLive.socket.SocketEvent;
import com.metfone.selfcare.module.gameLive.socket.SocketManager;
import com.metfone.selfcare.module.gameLive.utils.LiveFormatUtils;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

public class GameLiveFragment extends BaseFragment {

    public static GameLiveFragment newInstance(LiveGameModel currentGameInfo) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("LIVE_GAME_DATA", currentGameInfo);
        GameLiveFragment fragment = new GameLiveFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @BindView(R.id.layout_waiting)
    View layout_waiting;

    @BindView(R.id.layout_game)
    RelativeLayout layout_game;

    @BindView(R.id.layout_result)
    RelativeLayout layout_result;

    @BindView(R.id.layout_goal)
    View layout_goal;

//    @BindView(R.id.layout_special_goal)
//    View layout_special_goal;

    @BindView(R.id.layout_result_bottom)
    RelativeLayout layout_result_bottom;

    @BindView(R.id.layout_list_win)
    View layout_list_win;

    @BindView(R.id.layout_guide)
    View layout_guide;


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvError)
    TextView tvError;

    @BindView(R.id.imvAnswerFail)
    ImageView imvAnswerFail;

    @BindView(R.id.tvAnswerTimer)
    TextView tvAnswerTimer;

    @BindView(R.id.tvQuestion)
    TextView tvQuestion;

    @BindView(R.id.tvAnswer1)
    TextView tvAnswer1;

    @BindView(R.id.tvAnswer2)
    TextView tvAnswer2;

    @BindView(R.id.tvAnswer3)
    TextView tvAnswer3;

    @BindView(R.id.tvAnswer4)
    TextView tvAnswer4;

    @BindView(R.id.tvResultTitle)
    TextView tvResultTitle;

    @BindView(R.id.tvResultContent)
    TextView tvResultContent;

    @BindView(R.id.tvResultGoal)
    TextView tvResultGoal;

    @BindView(R.id.tvResultSuggest)
    TextView tvResultSuggest;

//    @BindView(R.id.tvResultContentEnd)
//    TextView tvResultContentEnd;

    @BindView(R.id.btnInvite)
    TextView btnInvite;

    @BindView(R.id.tvGuide)
    TextView tvGuide;

    @BindView(R.id.tvGuideWaiting)
    TextView tvGuideWaiting;

    @BindView(R.id.button_share_facebook)
    TextView btnShare;

    @BindView(R.id.button_view_list_winner)
    TextView btnViewWinner;

    @BindView(R.id.tvWaitingTitle)
    TextView tvWaitingTitle;

    @BindView(R.id.tvWaitingGoal)
    TextView tvWaitingGoal;

    @BindView(R.id.tv_title_count_down)
    TextView tvTitleCountDown;

    @BindView(R.id.tv_time_count_down)
    TextView tvTimeCountDown;

    private static final String TAG = GameLiveFragment.class.getSimpleName();
    private static final int ANSWER_TIMER = 30000;
    private static final int STATE_LOADING = 0;
    private static final int STATE_GAME = 1;
    private static final int STATE_RESULT = 2;
    private static final int STATE_END = 3;

    private Unbinder unbinder;
    private ApplicationController app;
    private BaseSlidingFragmentActivity activity;
    private int currentState = 0;
    private LiveQuestionModel currentQuestion;
    private LiveGameModel currentGameInfo;
    private int totalNumberPeopleCorrect = 0;
    private List<WinnerModel> msisdnSpecialList;
    private int currentGift;
    private int selectAnswer = 0;
    private boolean isExistsAnswer = false;
    private Handler handler = new Handler();
    private RetrofitClientInstance retrofitClientInstance = new RetrofitClientInstance();
    private boolean isGameFinish = false;
    private ListenerUtils mListenerUtils;

    private synchronized RetrofitClientInstance getRetrofitClient() {
        if (retrofitClientInstance == null) retrofitClientInstance = new RetrofitClientInstance();
        return retrofitClientInstance;
    }

    Runnable runnableShowResult = new Runnable() {
        @Override
        public void run() {
//            showResultWaitingScreen();

            if (currentQuestion.getNumber() == 4) {
                showUIByState(STATE_RESULT);
            } else {
                //Goi api check xem co trung thuong khong
                checkResultServer();
            }
        }
    };

    Runnable runnableShowLoading = new Runnable() {
        @Override
        public void run() {
            showLoadingScreen(false);
        }
    };

    Runnable runnableShowWaiting = new Runnable() {
        @Override
        public void run() {
            showLoadingScreen(true);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        app = (ApplicationController) activity.getApplication();
    }

    @Override
    public String getName() {
        return "GameLiveFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_game_live;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_game_live, container, false);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        mListenerUtils = app.getApplicationComponent().providerListenerUtils();
        mListenerUtils.addListener(this);

        Bundle bundle = getArguments();
        if (bundle != null) {
            currentGameInfo = (LiveGameModel) bundle.getSerializable("LIVE_GAME_DATA");
        }

        initData();
        initSocket();

        return view;
    }

    private void initSocket() {
        String domain = app.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_VIDEO_GAME_STREAMING_WS);
//        String domain = "ws://testws187.mocha.com.vn/gameWS/websocket";
        SocketManager.getInstance().connectWebSocket((GameLiveActivity) activity, domain);
    }

    private void initData() {
        showUIByState(STATE_LOADING);
        resetAnswer();
//        numberAnswerCorrect = 0;

        //Neu vao lan dau thi reset number correct
        if(currentGameInfo.getActive() == 3 || currentGameInfo.getCurrentQuestion() == 0 || currentGameInfo.getCurrentQuestion() == 4)
            app.setGameLiveNumberAnswerCorrect(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onDestroyView() {
        SocketManager.getInstance().disconnect();
        unbinder.unbind();
        countDownSchedule.cancel();
//        numberAnswerCorrect = 0;
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(SocketEvent message) {
        if (message != null && message.getType() == SocketEvent.TYPE_ERROR) {
//            DialogMessage dialogMessage = new DialogMessage(activity, true);
//            dialogMessage.setMessage(getString(R.string.e601_error_but_undefined));
//            dialogMessage.setNegativeListener(new NegativeListener() {
//                @Override
//                public void onNegative(Object result) {
//                    if(activity != null)
//                        activity.finish();
//                }
//            });
//            if (dialogMessage != null && !dialogMessage.isShowing())
//                dialogMessage.show();
        }
        EventBus.getDefault().removeStickyEvent(message);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(LiveMessageModel message) {
        //game ket thuc se khong nhan ban tin nua
        if (isGameFinish) {
            EventBus.getDefault().removeStickyEvent(message);
            return;
        }

        if (message != null) {
            if (message.getType() == 1)//tra ve cau hoi
            {
                removeAllCallback();
                if (message.getQuestion() != null) {
                    if(message.getQuestion().getNumber() == 4)
                    {
                        if(app.getGameLiveNumberAnswerCorrect() == 3)//Du dk cau dac biet
                        {
                            showUIByState(STATE_GAME);
                            setDataAnswer(message.getQuestion());
                        }
                        else
                        {
                            //Ko du dieu kien tham gia do chua tra loi dung 3 cau
                            showNoSpecialQuestion(1);
                        }
                    }
                    else
                    {
                        showUIByState(STATE_GAME);
                        setDataAnswer(message.getQuestion());
                    }
                }
            } else if (message.getType() == 2)//Tra ve so nguoi tra loi dung
            {
                removeAllCallback();

                totalNumberPeopleCorrect = message.getTotalNumber();
                currentGift = message.getPrize();

                showResult();
            } else if (message.getType() == 3) {
                removeAllCallback();

                int state = message.getState();
                if (state == 1 && app.getGameLiveNumberAnswerCorrect() == 3) {
                    //Du nguoi choi vao vong dac biet

                } else {
                    //Khong du nguoi choi vao vong dac biet ket thuc game luon
                    showNoSpecialQuestion(state);
                }
            } else if (message.getType() == 5) {
                removeAllCallback();

                //Ket qua giai dac biet
                msisdnSpecialList = message.getMsisdnLucky();
                showResult();
            }

            EventBus.getDefault().removeStickyEvent(message);
        }
    }

    private void removeAllCallback()
    {
        handler.removeCallbacks(runnableShowLoading);
        handler.removeCallbacks(runnableShowWaiting);
        handler.removeCallbacks(runnableShowResult);
    }

    private void showUIByState(int state) {
        currentState = state;
        removeAllCallback();
        switch (state) {
            case STATE_LOADING:
                if (currentGameInfo.getActive() == 3)
                    showWaitingScreen();
                else if (currentGameInfo.getActive() == 2)
                    showLoadingScreen(true);
                else
                    showGameFinish();
                break;
            case STATE_GAME:
                showGameScreen();
                break;
            case STATE_RESULT:
                showResultScreen();
                break;
            case STATE_END:
                showGameFinish();
                break;
        }
    }

    private void showGameFinish() {
        isGameFinish = true;
        if (layout_game == null) return;

        layout_waiting.setVisibility(View.GONE);
        layout_game.setVisibility(View.GONE);
        layout_result.setVisibility(View.VISIBLE);
        layout_goal.setVisibility(View.GONE);
//        layout_special_goal.setVisibility(View.GONE);
        layout_result_bottom.setVisibility(View.GONE);

        tvResultTitle.setText(getString(R.string.live_end));
        tvResultContent.setText(getString(R.string.live_end_content));

        LogDebugHelper.getInstance().logDebugContent("Game Live: showGameFinish");
        ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: showGameFinish");
    }

    private void showNoSpecialQuestion(int state) {
        if (layout_game == null) return;
        isGameFinish = true;

        LogDebugHelper.getInstance().logDebugContent("Game Live: showNoSpecialQuestion state: " + state);
        ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: showNoSpecialQuestion state: " + state);

        layout_waiting.setVisibility(View.GONE);
        layout_game.setVisibility(View.GONE);
        layout_result.setVisibility(View.VISIBLE);
        layout_goal.setVisibility(View.GONE);
//        layout_special_goal.setVisibility(View.GONE);

        layout_result_bottom.setVisibility(View.VISIBLE);
        layout_list_win.setVisibility(View.GONE);
        layout_guide.setVisibility(View.VISIBLE);
        btnInvite.setVisibility(View.GONE);

        if (state != 1) {
            layout_goal.setVisibility(View.VISIBLE);

            tvResultTitle.setText(getString(R.string.live_result_special_miss));
            tvResultContent.setText(getString(R.string.live_result_special_miss_content, LiveFormatUtils.formatNumber(currentGameInfo.getSpecialPrize())));
            tvResultSuggest.setText(getString(R.string.live_result_special_miss_suggest));
            tvResultSuggest.setVisibility(View.VISIBLE);
            tvResultGoal.setVisibility(View.GONE);
        } else {
            layout_goal.setVisibility(View.VISIBLE);

            tvResultTitle.setText(getString(R.string.live_result_special_fail));
            tvResultContent.setText(getString(R.string.live_result_special_fail_content));
            tvResultSuggest.setText(getString(R.string.live_result_special_fail_suggest));
            tvResultSuggest.setVisibility(View.VISIBLE);

            //Set text 5.000.000
            tvResultGoal.setText(getString(R.string.live_prize, LiveFormatUtils.formatNumber(currentGameInfo.getSpecialPrize())));
            tvResultGoal.setVisibility(View.VISIBLE);
        }

//        tvResultTitle.setText(getString(R.string.live_end));
//        tvResultContent.setText(getString(R.string.live_end_content));
    }

    private void showWaitingScreen() {
        if (layout_game == null) return;
        LogDebugHelper.getInstance().logDebugContent("Game Live: showWaitingScreen");
        ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: showWaitingScreen");

        layout_waiting.setVisibility(View.VISIBLE);
        layout_game.setVisibility(View.GONE);
        layout_result.setVisibility(View.GONE);

        tvWaitingTitle.setText(R.string.live_goal);
        tvWaitingGoal.setText(getString(R.string.live_prize, LiveFormatUtils.formatNumber(currentGameInfo.getTotalPrize())));

        tvTitleCountDown.setText(getString(R.string.live_next_game));
//        tvTimeCountDown.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        tvTimeCountDown.setText(DateFormat.format("HH:mm", new Date(currentGameInfo.getTimeStart()))
                + "\n"
                + currentGameInfo.getTimeStartStr());

        //Chay count down dem nguoc
        if (currentGameInfo.getTimeStart() > System.currentTimeMillis()) {
            long timeRemain = currentGameInfo.getTimeStart() - System.currentTimeMillis();
            handler.postDelayed(runnableShowWaiting, timeRemain);
        }
    }

    private void showLoadingScreen(boolean isFirst) {
        if (layout_game == null) return;

        LogDebugHelper.getInstance().logDebugContent("Game Live: showLoadingScreen");
        ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: showLoadingScreen");

        layout_waiting.setVisibility(View.GONE);
        layout_game.setVisibility(View.GONE);
        layout_result.setVisibility(View.VISIBLE);
        layout_goal.setVisibility(View.GONE);
//        layout_special_goal.setVisibility(View.GONE);

        if (isFirst) {
            layout_result_bottom.setVisibility(View.VISIBLE);
            tvGuide.setPaintFlags(tvGuide.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvGuideWaiting.setPaintFlags(tvGuide.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            btnViewWinner.setPaintFlags(tvGuide.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            layout_list_win.setVisibility(View.GONE);
            layout_guide.setVisibility(View.VISIBLE);
        } else
            layout_result_bottom.setVisibility(View.GONE);
        tvResultTitle.setText(R.string.live_loading_title);
        tvResultContent.setText(R.string.live_loading_content);

        layout_goal.setVisibility(View.VISIBLE);
        tvResultGoal.setVisibility(View.GONE);
        tvResultSuggest.setText(getString(R.string.live_loading_suggest));
        tvResultSuggest.setVisibility(View.VISIBLE);
    }

    private void showGameScreen() {
        if (layout_game == null) return;
        layout_waiting.setVisibility(View.GONE);
        layout_game.setVisibility(View.VISIBLE);
        layout_result.setVisibility(View.GONE);
    }

    private void showResultScreen() {
        if (layout_game == null) return;
        layout_waiting.setVisibility(View.GONE);
        layout_game.setVisibility(View.GONE);
        layout_result.setVisibility(View.VISIBLE);
        layout_goal.setVisibility(View.GONE);
//        layout_special_goal.setVisibility(View.GONE);
        layout_result_bottom.setVisibility(View.GONE);

        if (currentQuestion != null) {
            if (currentQuestion.getNumber() == 4)//Cau dac biet
            {
//                tvResultTitle.setText(getString(R.string.live_result_special_title, LiveFormatUtils.formatNumber(currentQuestion.getPrize())));
//                tvResultContent.setVisibility(View.GONE);
//                //Show 3 thang dc giai dac biet
//                layout_special_goal.setVisibility(View.VISIBLE);
//                layout_goal.setVisibility(View.GONE);
//                tvResultContentEnd.setText(R.string.live_result_special_suggest);
//                tvResultContentEnd.setVisibility(View.VISIBLE);

//                if (msisdnSpecialList != null) {
//                    if (msisdnSpecialList.size() >= 3) {
//                        tvMember1.setText(msisdnSpecialList.get(0).getName());
//                        tvMember1.setVisibility(View.VISIBLE);
//                        tvMember2.setText(msisdnSpecialList.get(1).getName());
//                        tvMember2.setVisibility(View.VISIBLE);
//                        tvMember3.setText(msisdnSpecialList.get(2).getName());
//                        tvMember3.setVisibility(View.VISIBLE);
//                    } else if (msisdnSpecialList.size() >= 2) {
//                        tvMember1.setText(msisdnSpecialList.get(0).getName());
//                        tvMember1.setVisibility(View.VISIBLE);
//                        tvMember2.setText(msisdnSpecialList.get(1).getName());
//                        tvMember2.setVisibility(View.VISIBLE);
//                        tvMember3.setVisibility(View.GONE);
//                    } else if (msisdnSpecialList.size() >= 1) {
//                        tvMember1.setText(msisdnSpecialList.get(0).getName());
//                        tvMember1.setVisibility(View.VISIBLE);
//                        tvMember2.setVisibility(View.GONE);
//                        tvMember3.setVisibility(View.GONE);
//                    }
//                } else
//                    layout_special_goal.setVisibility(View.GONE);

                //Check chinh minh co trung giai dac biet ko
                boolean isWin = false;
                String msisdn = app.getReengAccountBusiness().getJidNumber();
                for (int i = 0; i < msisdnSpecialList.size(); i++) {
                    if (msisdn.equals(msisdnSpecialList.get(i).getMsisdn())) {
                        isWin = true;
                        break;
                    }
                }

                if (isWin) {
//                    layout_special_goal.setVisibility(View.GONE);
                    layout_goal.setVisibility(View.VISIBLE);

                    tvResultTitle.setText(R.string.live_result_special_title);
                    tvResultContent.setText(getString(R.string.live_result_special_content));
                    tvResultGoal.setText(getString(R.string.live_prize, LiveFormatUtils.formatNumber(currentQuestion.getPrize())));
                    tvResultGoal.setVisibility(View.VISIBLE);
                    tvResultSuggest.setVisibility(View.GONE);

                    layout_result_bottom.setVisibility(View.VISIBLE);
                    layout_list_win.setVisibility(View.VISIBLE);
//                    btnShare.setVisibility(View.VISIBLE);
                    layout_guide.setVisibility(View.GONE);

                    LogDebugHelper.getInstance().logDebugContent("Game Live: showResultScreen " + getString(R.string.live_result_special_content));
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: showResultScreen" + getString(R.string.live_result_special_content));
                } else {
//                    layout_special_goal.setVisibility(View.GONE);
                    layout_goal.setVisibility(View.VISIBLE);

                    if (currentQuestion.isCorrect()) {
                        tvResultTitle.setText(R.string.result_fail_special_true);
                        tvResultContent.setText(getString(R.string.result_fail_special_content_true));
                    } else {
                        tvResultTitle.setText(R.string.result_fail_special_false);
                        tvResultContent.setText(getString(R.string.result_fail_special_content_false));
                    }
                    tvResultGoal.setVisibility(View.GONE);
                    tvResultSuggest.setText(getString(R.string.result_fail_special_content_suggest));
                    tvResultSuggest.setVisibility(View.VISIBLE);

                    layout_result_bottom.setVisibility(View.VISIBLE);
                    layout_list_win.setVisibility(View.VISIBLE);
                    layout_guide.setVisibility(View.GONE);
                    btnShare.setVisibility(View.GONE);

                    LogDebugHelper.getInstance().logDebugContent("Game Live: showResultScreen " + tvResultTitle.getText().toString());
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: showResultScreen" + tvResultTitle.getText().toString());
                }
                //Reset game
//                numberAnswerCorrect = 0;
                app.setGameLiveNumberAnswerCorrect(0);
            } else//cau 1 den 3
            {
                if (currentQuestion.isWinPrize()) {
//                    layout_special_goal.setVisibility(View.GONE);
                    layout_goal.setVisibility(View.VISIBLE);

                    tvResultTitle.setText(getString(R.string.result_correct));
                    tvResultContent.setText(getString(R.string.result_correct_content, currentQuestion.getNumber(), totalNumberPeopleCorrect));
                    tvResultGoal.setText(getString(R.string.live_prize, LiveFormatUtils.formatNumber(currentGift)));
                    tvResultGoal.setVisibility(View.VISIBLE);
                    tvResultSuggest.setVisibility(View.GONE);

                    layout_result_bottom.setVisibility(View.VISIBLE);
                    layout_list_win.setVisibility(View.VISIBLE);
//                    btnShare.setVisibility(View.VISIBLE);
                    layout_guide.setVisibility(View.GONE);

                    LogDebugHelper.getInstance().logDebugContent("Game Live: showResultScreen " + getString(R.string.result_correct));
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: showResultScreen" + getString(R.string.result_correct));
                } else {
                    if (currentQuestion.isCorrect()) {
                        tvResultTitle.setText(R.string.result_fail_true);
                        tvResultContent.setText(getString(R.string.result_fail_content_true, currentQuestion.getNumber()));
                    } else {
                        tvResultTitle.setText(R.string.result_fail_false);
                        tvResultContent.setText(getString(R.string.result_fail_content_false, currentQuestion.getNumber()));
                    }
                    tvResultSuggest.setVisibility(View.GONE);
                    layout_result_bottom.setVisibility(View.VISIBLE);
                    layout_list_win.setVisibility(View.VISIBLE);
                    layout_guide.setVisibility(View.GONE);
                    btnShare.setVisibility(View.GONE);

                    LogDebugHelper.getInstance().logDebugContent("Game Live: showResultScreen " + tvResultTitle.getText().toString());
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: showResultScreen" + tvResultTitle.getText().toString());
                }
            }

            //10s tu next sang man hinh hien cau hoi tiep theo
            if (currentQuestion.getNumber() == 1 || currentQuestion.getNumber() == 2) {
                handler.postDelayed(runnableShowLoading, 10000);
            }
        }
        else
        {
            LogDebugHelper.getInstance().logDebugContent("Game Live: currentQuestion null");
            ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: currentQuestion null");
        }

        //Neu da tra loi het 3 cau thi hien man hinh quay so
//        if (currentQuestion != null && currentQuestion.getNumber() == 3) {
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    showGiftScreen();
//                }
//            }, 5000);
//        }
    }

    private void showResultWaitingScreen() {
        if (layout_game == null) return;
        layout_waiting.setVisibility(View.GONE);
        layout_game.setVisibility(View.GONE);
        layout_result.setVisibility(View.VISIBLE);
        layout_goal.setVisibility(View.GONE);
//        layout_special_goal.setVisibility(View.GONE);

        layout_result_bottom.setVisibility(View.VISIBLE);
        layout_list_win.setVisibility(View.GONE);
        layout_guide.setVisibility(View.VISIBLE);

        tvResultTitle.setText(R.string.live_result_waiting);
        tvResultContent.setText(R.string.live_result_waiting_content);
    }

    @OnClick({R.id.tvAnswer1, R.id.tvAnswer2, R.id.tvAnswer3, R.id.tvAnswer4, R.id.btnInvite, R.id.tvGuide, R.id.tvGuideWaiting
            , R.id.button_share_facebook, R.id.button_view_list_winner})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvAnswer1:
                if (isExistsAnswer) return;
                isExistsAnswer = true;
                selectAnswer(0);
                postAnswer();
                break;
            case R.id.tvAnswer2:
                if (isExistsAnswer) return;
                isExistsAnswer = true;
                selectAnswer(1);
                postAnswer();
                break;
            case R.id.tvAnswer3:
                if (isExistsAnswer) return;
                isExistsAnswer = true;
                selectAnswer(2);
                postAnswer();
                break;
            case R.id.tvAnswer4:
                if (isExistsAnswer) return;
                isExistsAnswer = true;
                selectAnswer(3);
                postAnswer();
                break;
            case R.id.btnInvite:

                break;
            case R.id.tvGuide:
            case R.id.tvGuideWaiting: {
                DialogGameRule dialog = new DialogGameRule(activity);
                dialog.show();
            }
            break;
            case R.id.button_share_facebook:
                ShareUtils.openShareMenu(activity, "http://mocha.com.vn/");
                break;
            case R.id.button_view_list_winner:
                loadListWinners();
                break;
        }
    }

    private void setDataAnswer(LiveQuestionModel question) {
        resetAnswer();
        currentQuestion = question;

        //Nhan cau dau tien thi reset so cau tra loi dung
        if(currentQuestion.getNumber() == 1)
            app.setGameLiveNumberAnswerCorrect(0);

        tvError.setVisibility(View.GONE);
        tvTitle.setVisibility(View.VISIBLE);
        if (question.getNumber() == 4)
            tvTitle.setText(getString(R.string.answer_special_title));
        else
            tvTitle.setText(getString(R.string.answer_title, currentQuestion.getNumber(), 3));

        imvAnswerFail.setVisibility(View.GONE);
        tvAnswerTimer.setVisibility(View.VISIBLE);

        tvQuestion.setText(currentQuestion.getTitle());
        if (currentQuestion.getListAnswer().size() > 0) {
            tvAnswer1.setVisibility(View.VISIBLE);
            tvAnswer1.setText(currentQuestion.getListAnswer().get(0).getTitle());
        } else
            tvAnswer1.setVisibility(View.GONE);

        if (currentQuestion.getListAnswer().size() > 1) {
            tvAnswer2.setVisibility(View.VISIBLE);
            tvAnswer2.setText(currentQuestion.getListAnswer().get(1).getTitle());
        } else
            tvAnswer2.setVisibility(View.GONE);

        if (currentQuestion.getListAnswer().size() > 2) {
            tvAnswer3.setVisibility(View.VISIBLE);
            tvAnswer3.setText(currentQuestion.getListAnswer().get(2).getTitle());
        } else
            tvAnswer3.setVisibility(View.GONE);

        if (currentQuestion.getListAnswer().size() > 3) {
            tvAnswer4.setVisibility(View.VISIBLE);
            tvAnswer4.setText(currentQuestion.getListAnswer().get(3).getTitle());
        } else
            tvAnswer4.setVisibility(View.GONE);

        //Start countdown thoi gian tra loi
        countDownSchedule.start();
    }

    CountDownTimer countDownSchedule = new CountDownTimer(ANSWER_TIMER, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            if (tvAnswerTimer != null)
                tvAnswerTimer.setText(String.format("%d", (int) (millisUntilFinished / 1000)));
        }

        @Override
        public void onFinish() {
            countDownSchedule.cancel();
            if (tvAnswerTimer != null) {
                //Het thoi gian tra loi
                isExistsAnswer = true;

                tvAnswerTimer.setText("0");
                if (currentQuestion != null) {
                    if (currentQuestion.getNumber() == 4)
                        tvTitle.setText(getString(R.string.waiting_result_special_title));
                    else
                        tvTitle.setText(getString(R.string.waiting_result_title, currentQuestion.getNumber()));
                }
            }

//            showResult();
        }
    };

    private void checkResult() {
        int questionCorrect = 0;
        if (currentQuestion != null) {
            for (int i = 0; i < currentQuestion.getListAnswer().size(); i++) {
                if (currentQuestion.getListAnswer().get(i).getIs_true() == 1) {
                    questionCorrect = i;
                    currentQuestion.setQuestionCorrect(questionCorrect);
                    if (selectAnswer == i) {
                        currentQuestion.setCorrect(true);
                        break;
                    }
                }
            }
        }
    }

    private void showResult() {
        if (currentQuestion != null) {
            checkResult();

            if (currentQuestion.isCorrect()) {
                showResultCorrect(currentQuestion.getQuestionCorrect());
            } else {
                showResultFail(currentQuestion.getQuestionCorrect());
            }
            //Sau 5s chuyen sang man hinh tong hop ket qua
            handler.postDelayed(runnableShowResult, 5000);
        }
    }

    private void selectAnswer(int answer) {
        selectAnswer = answer;
        switch (answer) {
            case 0:
                tvAnswer1.setBackgroundResource(R.drawable.bg_button_answer_selected);
                tvAnswer2.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer3.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer4.setBackgroundResource(R.drawable.bg_button_answer);
                break;
            case 1:
                tvAnswer1.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer2.setBackgroundResource(R.drawable.bg_button_answer_selected);
                tvAnswer3.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer4.setBackgroundResource(R.drawable.bg_button_answer);
                break;
            case 2:
                tvAnswer1.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer2.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer3.setBackgroundResource(R.drawable.bg_button_answer_selected);
                tvAnswer4.setBackgroundResource(R.drawable.bg_button_answer);
                break;
            case 3:
                tvAnswer1.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer2.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer3.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer4.setBackgroundResource(R.drawable.bg_button_answer_selected);
                break;
            default:
                tvAnswer1.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer2.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer3.setBackgroundResource(R.drawable.bg_button_answer);
                tvAnswer4.setBackgroundResource(R.drawable.bg_button_answer);
                break;
        }
    }

    private void checkResultServer() {
        if (currentQuestion == null) return;
        ReengAccount reengAccount = app.getReengAccountBusiness().getCurrentAccount();
        getRetrofitClient().checkResult(reengAccount.getJidNumber(), currentQuestion.getId() + "", currentQuestion.getGameID() + "", new APICallBack<RestGameResult>() {
            @Override
            public void onResponse(Response<RestGameResult> response) {

                if (response.body() != null) {
                    if (response.body().getResult().equals("1"))
                        currentQuestion.setWinPrize(true);
                    else
                        currentQuestion.setWinPrize(false);
                    showUIByState(STATE_RESULT);
                }
                else
                {
                    LogDebugHelper.getInstance().logDebugContent("Game Live: checkResultServer Body null");
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: checkResultServer Body null");
                }
            }

            @Override
            public void onError(Throwable error) {
                LogDebugHelper.getInstance().logDebugContent("Game Live: checkResultServer Fail");
                ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_SCREEN, "Game Live: checkResultServer Fail");
            }
        });
    }

    private void postAnswer() {
        app.setGameLiveNumberAnswerCorrect(app.getGameLiveNumberAnswerCorrect() + 1);
        //Check ket qua goi len server
        checkResult();

        ReengAccount reengAccount = app.getReengAccountBusiness().getCurrentAccount();
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("msisdn", reengAccount.getJidNumber());
//            jsonObject.put("domain", DOMAIN_API);
//            jsonObject.put("questionId", currentQuestion.getId());
//            jsonObject.put("timestamp", System.currentTimeMillis());
//            jsonObject.put("is_true", currentQuestion.isCorrect() ? 1 : 0);
//            jsonObject.put("answer", selectAnswer == -1 ? "" : currentQuestion.getListAnswer().get(selectAnswer).getTitle());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        String json = jsonObject.toString();
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        getRetrofitClient().postAnswer(reengAccount.getJidNumber(), currentQuestion.getId() + "", currentQuestion.isCorrect() ? 1 : 0, selectAnswer == -1 ? "" : currentQuestion.getListAnswer().get(selectAnswer).getTitle(), currentQuestion.getGameID() + "", new APICallBack<AbsResultData>() {
            @Override
            public void onResponse(Response<AbsResultData> response) {

            }

            @Override
            public void onError(Throwable error) {

            }
        });
    }

    private void showResultCorrect(int result) {
        tvTitle.setVisibility(View.GONE);
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(getString(R.string.answer_correct));
        tvError.setTextColor(getResources().getColor(R.color.live_button_correct));

        imvAnswerFail.setVisibility(View.VISIBLE);
        imvAnswerFail.setImageResource(R.drawable.live_correct);
        imvAnswerFail.setColorFilter(ContextCompat.getColor(activity, R.color.live_button_correct), android.graphics.PorterDuff.Mode.MULTIPLY);
        tvAnswerTimer.setVisibility(View.GONE);

        switch (result) {
            case 0:
                tvAnswer1.setBackgroundResource(R.drawable.bg_button_answer_correct);
                break;
            case 1:
                tvAnswer2.setBackgroundResource(R.drawable.bg_button_answer_correct);
                break;
            case 2:
                tvAnswer3.setBackgroundResource(R.drawable.bg_button_answer_correct);
                break;
            case 3:
                tvAnswer4.setBackgroundResource(R.drawable.bg_button_answer_correct);
                break;
        }
    }

    private void showResultFail(int result) {
        tvTitle.setVisibility(View.GONE);
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(getString(R.string.answer_fail));
        tvError.setTextColor(getResources().getColor(R.color.red));

        imvAnswerFail.setVisibility(View.VISIBLE);
        imvAnswerFail.setImageResource(R.drawable.ic_ab_close);
        imvAnswerFail.setColorFilter(ContextCompat.getColor(activity, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
        tvAnswerTimer.setVisibility(View.GONE);

        switch (selectAnswer) {
            case 0:
                tvAnswer1.setBackgroundResource(R.drawable.bg_button_answer_fail);
                break;
            case 1:
                tvAnswer2.setBackgroundResource(R.drawable.bg_button_answer_fail);
                break;
            case 2:
                tvAnswer3.setBackgroundResource(R.drawable.bg_button_answer_fail);
                break;
            case 3:
                tvAnswer4.setBackgroundResource(R.drawable.bg_button_answer_fail);
                break;
        }

        switch (result) {
            case 0:
                tvAnswer1.setBackgroundResource(R.drawable.bg_button_answer_correct);
                break;
            case 1:
                tvAnswer2.setBackgroundResource(R.drawable.bg_button_answer_correct);
                break;
            case 2:
                tvAnswer3.setBackgroundResource(R.drawable.bg_button_answer_correct);
                break;
            case 3:
                tvAnswer4.setBackgroundResource(R.drawable.bg_button_answer_correct);
                break;
        }
    }

    private void resetAnswer() {
        isExistsAnswer = false;
        selectAnswer = 0;
        selectAnswer(-1);
        totalNumberPeopleCorrect = 0;
        currentGift = 0;
        msisdnSpecialList = new ArrayList<>();
    }

    private void loadListWinners() {
        if (currentQuestion == null) return;
        int gameId = currentQuestion.getGameID();
        int questionId = currentQuestion.getId();
        activity.showLoadingDialog("", R.string.loading);
        getRetrofitClient().getListWinners(questionId + "", currentQuestion.getNumber() + "", gameId + "", new APICallBack<RestWinner>() {
            @Override
            public void onResponse(Response<RestWinner> response) {
                activity.hideLoadingDialog();
                if (response.body() != null) {
                    ArrayList<WinnerModel> listWinners = response.body().getResult();
                    Log.d(TAG, "loadListWinners: " + listWinners);
                    if (currentQuestion != null && currentQuestion.getId() == questionId && currentQuestion.getGameID() == gameId) {
                        DialogGameResult dialog = new DialogGameResult(activity);
                        dialog.setCurrentQuestion(currentQuestion);
                        dialog.setListWinners(listWinners);
                        dialog.show();
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                activity.hideLoadingDialog();
            }
        });
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && !SocketManager.getInstance().isConnected()) {
            SocketManager.getInstance().disconnect();
            initSocket();
        }
    }
}

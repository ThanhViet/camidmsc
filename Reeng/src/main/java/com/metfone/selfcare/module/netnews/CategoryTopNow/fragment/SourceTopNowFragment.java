package com.metfone.selfcare.module.netnews.CategoryTopNow.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.netnews.CategoryTopNow.adapter.SourceTopNowAdapter;
import com.metfone.selfcare.module.netnews.CategoryTopNow.presenter.ISourceTopNowPresenter;
import com.metfone.selfcare.module.netnews.CategoryTopNow.presenter.SourceTopNowPresenter;
import com.metfone.selfcare.module.netnews.CategoryTopNow.view.ISourceTopNowView;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.listener.OnItemClickListener;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.CustomLoadMoreView;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by HaiKE on 8/19/17.
 */

public class SourceTopNowFragment extends BaseFragment implements ISourceTopNowView, BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.loadingView)
    View loadingView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    int currentPage = 1;
    int sourceId = 0;
    String sourceName;
    SourceTopNowAdapter adapter;
    //    LinearLayoutManager layoutManager;
    ArrayList<NewsModel> datas = new ArrayList<>();
    View notDataView, errorView;
    boolean isRefresh;

    ISourceTopNowPresenter mPresenter;

    public static SourceTopNowFragment newInstance(Bundle bundle) {

        SourceTopNowFragment fragment = new SourceTopNowFragment();
        fragment.setArguments(bundle);
        fragment.mPresenter = new SourceTopNowPresenter();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_toolbar, container, false);
        if (mPresenter == null) {
            mPresenter = new SourceTopNowPresenter();
        }
        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        setUp(view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        Bundle bundle = getArguments();
        sourceId = bundle.getInt(CommonUtils.KEY_CATEGORY_ID, 0);
        sourceName = bundle.getString(CommonUtils.KEY_CATEGORY_NAME, "");

        tvTitle.setText(sourceName);

        isRefresh = true;
        if (datas == null || (datas.size() == 0)) {
            loadingView.setVisibility(View.VISIBLE);
            mPresenter.loadData(sourceId, currentPage);
        }

        layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        layout_refresh.setOnRefreshListener(this);

        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(getBaseActivity(), LinearLayoutManager.VERTICAL, false));
            recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
        }
        adapter = new SourceTopNowAdapter(getBaseActivity(), R.layout.holder_special_news, datas);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getBaseActivity(), layoutManager.getOrientation());
//        dividerItemDecoration.setDrawable(getBaseActivity().getResources().getDrawable(R.drawable.divider_vertical));
//        recyclerView.addItemDecoration(dividerItemDecoration);

        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(this);
        adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//        adapter.setAutoLoadMoreSize(3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (datas != null && datas.size() > position && position >= 0){
                    readNews(datas.get(position));
                }
//                    UrlConfigHelper.gotoWebViewOnMedia(ApplicationController.self(), getBaseActivity(), datas.get(position).getUrl());
            }
        });

        notDataView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_nodata, (ViewGroup) recyclerView.getParent(), false);
        notDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
        errorView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_failed, (ViewGroup) recyclerView.getParent(), false);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        loadingView.setVisibility(View.GONE);
        hideRefresh();
        if (!flag) {
            loadingFail();
        }
    }

    @Override
    public void bindData(NewsResponse response) {
        if (datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if (response != null) {
            if (response.getData() != null) {
                loadingComplete(response.getData());
            } else {
                loadingFail();
            }
        }
    }

    public void loadingComplete(ArrayList<NewsModel> response) {
        int mCurrentCounter = response.size();
        if (isRefresh) {
            if (mCurrentCounter == 0) {
                adapter.setEmptyView(notDataView);
            } else {
                datas.clear();
                adapter.setNewData(response);
                datas.addAll(response);
            }
        } else {
            if (mCurrentCounter == 0) {
                adapter.loadMoreEnd();
            } else {
                adapter.addData(response);
                datas.addAll(response);
                adapter.loadMoreComplete();
            }
        }
    }

    public void loadingFail() {
        if (isRefresh) {
            adapter.setEmptyView(errorView);
        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        mPresenter.loadData(sourceId, currentPage);
    }

    @Override
    public void onLoadMoreRequested() {
        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                currentPage++;
                isRefresh = false;
                mPresenter.loadData(sourceId, currentPage);
            }

        }, 1000);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.btnBack)
    public void onBackClick() {
        getBaseActivity().onBackPressed();
    }

    @Override
    public void paddingView(boolean flag) {
        if (flag)
            recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getPaddingTop(), recyclerView.getPaddingRight(), getResources().getDimensionPixelOffset(R.dimen.bottom_bar_height));
        else
            recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getPaddingTop(), recyclerView.getPaddingRight(), 0);
    }
    @OnClick(R.id.ivSearch)
    public void clickSearch(){
        Utilities.openSearch(getBaseActivity(), Constants.TAB_NEWS_HOME);
    }
}

package com.metfone.selfcare.module.home_kh.base;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.roundview.RoundTextView;

public abstract class BaseDialogKhFragment extends DialogFragment {

    protected RoundTextView btnRight;
    protected RoundTextView btnLeft;
    protected AppCompatTextView tvTitle;
    protected DialogListener selectListener;
    protected IDialogListener listener;

    protected abstract int getResId();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.V5DialogRadioButton);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getResId(), container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        getDialog().getWindow()
//                .getAttributes().windowAnimations = R.style.V5DialogWindowAnimation;
        initView();

        getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (listener != null) listener.dialogDismiss();
            }
        });

        getDialog().setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (listener != null) listener.dialogCancel();
            }
        });
    }

    public void setSelectListener(DialogListener selectListener) {
        this.selectListener = selectListener;
    }

    public void setDialogListener(IDialogListener listener) {
        this.listener = listener;
    }

    protected void initView(){
        btnLeft = getView().findViewById(R.id.btnLeft);
        btnRight = getView().findViewById(R.id.btnRight);
        tvTitle = getView().findViewById(R.id.txtTitle);
        if(btnLeft != null){
            btnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getDialog().cancel();
                    if( selectListener != null){
                        selectListener.dialogLeftClick();
                    }
                }
            });
        }
    }




    public interface DialogListener {
        void dialogRightClick(int value);
        void dialogLeftClick();
    }

    public interface IDialogListener {
        void dialogDismiss();
        void dialogCancel();
    }

}

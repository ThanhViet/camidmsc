/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/20
 *
 */

package com.metfone.selfcare.module.share.task;

import android.os.AsyncTask;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.module.share.listener.SearchContactsListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.metfone.selfcare.database.constant.ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;

public class SearchContactsTask extends AsyncTask<String, Void, ArrayList<ContactProvisional>> {
    private static final String TAG = "SearchContactsTask";
    private WeakReference<ApplicationController> mApplication;
    private String keySearch;
    private Comparator comparatorName;
    private Comparator comparatorKeySearch;
    private Comparator comparatorContact;
    private Comparator comparatorMessage;
    private SearchContactsListener listener;
    private CopyOnWriteArrayList<ContactProvisional> data;
    private long startTime;
    private int totalDatas;

    public SearchContactsTask(ApplicationController application) {
        this.mApplication = new WeakReference<>(application);
    }

    public void setData(ArrayList<ContactProvisional> data) {
        this.data = new CopyOnWriteArrayList<>(data);
    }

    public void setListener(SearchContactsListener listener) {
        this.listener = listener;
    }

    public void setComparatorName(Comparator comparatorName) {
        this.comparatorName = comparatorName;
    }

    public void setComparatorKeySearch(Comparator comparatorKeySearch) {
        this.comparatorKeySearch = comparatorKeySearch;
    }

    public void setComparatorContact(Comparator comparatorContact) {
        this.comparatorContact = comparatorContact;
    }

    public void setComparatorMessage(Comparator comparatorMessage) {
        this.comparatorMessage = comparatorMessage;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) listener.onPrepareSearchContacts();
    }

    @Override
    protected void onPostExecute(@NonNull ArrayList<ContactProvisional> result) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, "search " + keySearch + " has " + result.size() + "/" + totalDatas
                    + " results on " + (System.currentTimeMillis() - startTime) + " ms.");
        if (listener != null) listener.onFinishedSearchContacts(keySearch, result);
    }

    @Override
    protected ArrayList<ContactProvisional> doInBackground(String... params) {
        startTime = System.currentTimeMillis();
        totalDatas = 0;
        ArrayList<ContactProvisional> list = new ArrayList<>();
        keySearch = params[0];
        if (Utilities.notNull(mApplication) && data != null) {
            totalDatas = data.size();
            if (keySearch == null) keySearch = "";
            else keySearch = keySearch.trim().toLowerCase(Locale.US);
            Log.d(TAG, "search keySearch: " + keySearch);
            if (TextUtils.isEmpty(keySearch)) {
                list.addAll(data);
            } else {
                if (keySearch.startsWith("+")) {
                    String tmp = keySearch.substring(1);
                    if (!TextUtils.isEmpty(tmp) && TextUtils.isDigitsOnly(tmp)) {
                        searchWithDigits(list, data);
                    } else {
                        searchWithText(list, data);
                    }
                } else if (TextUtils.isDigitsOnly(keySearch)) {
                    searchWithDigits(list, data);
                } else {
                    searchWithText(list, data);
                }
            }
        }
        return list;
    }

    private void searchWithText(ArrayList<ContactProvisional> list, CopyOnWriteArrayList<ContactProvisional> data) {
        //todo keySearch là không phải là chữ
        if (Utilities.notEmpty(data)) {
            Log.d(TAG, "searchContactsWithText: " + keySearch);
            ArrayList<ContactProvisional> listResults01 = new ArrayList<>(); //todo tên giống hệt keySearch
            ArrayList<ContactProvisional> listResults02 = new ArrayList<>(); //todo tên bỏ dấu giống hệt keySearch bỏ dấu
            ArrayList<ContactProvisional> listResults03 = new ArrayList<>(); //todo thread chat có: tên bắt đầu bằng keySearch hoặc tên bỏ dấu bắt đầu bằng keySearch bỏ dấu hoặc tên đã bỏ dấu bắt đầu phân tách thành các từ đơn, keySearch đã bỏ dấu phân tách thành các từ đơn, tất cả các từ đơn của kết quả phân tách của keySearch đều là ký tự bắt đầu trong danh sách tất cả các từ đơn của kết qủa phân tách tên. Sắp xếp theo thời gian thread chat.
            ArrayList<ContactProvisional> listResults04 = new ArrayList<>(); //todo danh bạ có: tên bắt đầu bằng keySearch hoặc tên bỏ dấu bắt đầu bằng keySearch bỏ dấu hoặc tên đã bỏ dấu bắt đầu phân tách thành các từ đơn, keySearch đã bỏ dấu phân tách thành các từ đơn, tất cả các từ đơn của kết quả phân tách của keySearch đều là ký tự bắt đầu trong danh sách tất cả các từ đơn của kết qủa phân tách tên. Sắp xếp theo tên abc.
            String keySearchUnmark = TextHelper.convertUnicodeForSearch(keySearch).toLowerCase(Locale.US).trim();
            List<String> listKeySearch = new ArrayList<>(Arrays.asList(keySearchUnmark.split(Constants.PATTERN.KEY_SEARCH_REPLACE)));
            Log.d(TAG, "listKeySearch: " + listKeySearch);
            int sizeKeySearch = listKeySearch.size();
            if (sizeKeySearch > 1 && comparatorKeySearch != null) {
                Collections.sort(listKeySearch, comparatorKeySearch);
                Log.d(TAG, "listKeySearch: " + listKeySearch);
            }
            Map<String, Integer> mapMessages = new HashMap<>();
            ArrayList<ContactProvisional> listMessagesTmp = new ArrayList<>();
            ArrayList<ContactProvisional> listContactsTmp = new ArrayList<>();
            for (ContactProvisional item : data) {
                if (item != null) {
                    if (item.getContact() instanceof ThreadMessage) {
                        String nameForSearch = TextHelper.convertUnicodeForSearch(item.getName()).toLowerCase(Locale.US).trim();
                        if (nameForSearch.contains(keySearchUnmark)) {
                            listMessagesTmp.add(item);
                        } else {
                            boolean check = true;
                            for (String key : listKeySearch) {
                                if (key != null && !nameForSearch.contains(key)) {
                                    check = false;
                                    break;
                                }
                            }
                            if (check) listMessagesTmp.add(item);
                        }
                    } else if (item.getContact() instanceof PhoneNumber) {
                        PhoneNumber model = (PhoneNumber) item.getContact();
                        if (Utilities.notEmpty(model.getJidNumber())) {
                            String nameForSearch = TextHelper.convertUnicodeForSearch(item.getName()).toLowerCase(Locale.US).trim();
                            if (nameForSearch.contains(keySearchUnmark)) {
                                listContactsTmp.add(item);
                            } else {
                                boolean check = true;
                                for (String key : listKeySearch) {
                                    if (key != null && !nameForSearch.contains(key)) {
                                        check = false;
                                        break;
                                    }
                                }
                                if (check) listContactsTmp.add(item);
                            }
                        }
                    }
                }
            }
            //todo thuc hien tim kiem
            for (ContactProvisional item : listMessagesTmp) {
                ThreadMessage model = (ThreadMessage) item.getContact();
                String title = model.getThreadName().trim();
                String name = title.toLowerCase(Locale.US);
                if (Utilities.notEmpty(name)) {
                    int threadType = model.getThreadType();
                    String phoneNumber = "";
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(model.getSoloNumber()))
                        phoneNumber = model.getSoloNumber();
                    String nameForSearch = TextHelper.convertUnicodeForSearch(title);
                    String nameUnmark = nameForSearch.toLowerCase(Locale.US);
                    if (name.equalsIgnoreCase(keySearch)) {
                        listResults01.add(item);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (nameUnmark.equalsIgnoreCase(keySearchUnmark)) {
                        listResults02.add(item);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (name.startsWith(keySearch)) {
                        listResults03.add(item);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (nameUnmark.startsWith(keySearchUnmark)) {
                        listResults03.add(item);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else {
                        //old filter
                        boolean check = false;
                        List<String> listNameUnmark = SearchUtils.getListSplitName(nameForSearch);
                        int sizeNameUnmark = listNameUnmark.size();
                        if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                            if (comparatorName != null && sizeNameUnmark > 1)
                                Collections.sort(listNameUnmark, comparatorName);
                            for (String tmp : listKeySearch) {
                                if (Utilities.notEmpty(tmp)) {
                                    boolean checkTmp = false;
                                    for (String nameTmp : listNameUnmark) {
                                        if (Utilities.notEmpty(nameTmp) && nameTmp.startsWith(tmp)) {
                                            checkTmp = true;
                                            listNameUnmark.remove(nameTmp);
                                            break;
                                        }
                                    }
                                    check = checkTmp;
                                    if (!check) break;
                                }
                            }
                            if (check) {
                                listResults03.add(item);
                                if (Utilities.notEmpty(phoneNumber))
                                    mapMessages.put(phoneNumber, 1);
                            }
                        }
                        if (!check) {
                            //new filter
                            listNameUnmark = SearchUtils.getListSplitKey(nameForSearch);
                            sizeNameUnmark = listNameUnmark.size();
                            if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                                if (comparatorName != null && sizeNameUnmark > 1)
                                    Collections.sort(listNameUnmark, comparatorName);
                                for (String tmp : listKeySearch) {
                                    if (Utilities.notEmpty(tmp)) {
                                        boolean checkTmp = false;
                                        for (String nameTmp : listNameUnmark) {
                                            if (Utilities.notEmpty(nameTmp) && nameTmp.startsWith(tmp)) {
                                                checkTmp = true;
                                                listNameUnmark.remove(nameTmp);
                                                break;
                                            }
                                        }
                                        check = checkTmp;
                                        if (!check) break;
                                    }
                                }
                                if (check) {
                                    listResults03.add(item);
                                    if (Utilities.notEmpty(phoneNumber))
                                        mapMessages.put(phoneNumber, 1);
                                }
                            }
                        }
                    }
                }
            }
            listMessagesTmp.clear();

            for (ContactProvisional item : listContactsTmp) {
                PhoneNumber model = (PhoneNumber) item.getContact();
                String phoneNumber = model.getJidNumber();
                if (!mapMessages.containsKey(phoneNumber)) {
                    String title = model.getName().trim();
                    String name = title.toLowerCase(Locale.US);
                    String nameForSearch = TextHelper.convertUnicodeForSearch(title);
                    String nameUnmark = nameForSearch.toLowerCase(Locale.US);
                    if (name.equalsIgnoreCase(keySearch)) {
                        listResults01.add(item);
                    } else if (nameUnmark.equalsIgnoreCase(keySearchUnmark)) {
                        listResults02.add(item);
                    } else if (name.startsWith(keySearch)) {
                        listResults04.add(item);
                    } else if (nameUnmark.startsWith(keySearchUnmark)) {
                        listResults04.add(item);
                    } else {
                        //old filter
                        boolean check = false;
                        List<String> listNameUnmark = SearchUtils.getListSplitName(nameForSearch);
                        int sizeNameUnmark = listNameUnmark.size();
                        if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                            if (comparatorName != null && sizeNameUnmark > 1)
                                Collections.sort(listNameUnmark, comparatorName);
                            for (String tmp : listKeySearch) {
                                if (Utilities.notEmpty(tmp)) {
                                    boolean checkTmp = false;
                                    for (String nameTmp : listNameUnmark) {
                                        if (nameTmp.startsWith(tmp)) {
                                            checkTmp = true;
                                            listNameUnmark.remove(nameTmp);
                                            break;
                                        }
                                    }
                                    check = checkTmp;
                                    if (!check) break;
                                }
                            }
                            if (check) listResults04.add(item);
                        }
                        if (!check) {
                            //new filter
                            listNameUnmark = SearchUtils.getListSplitKey(nameForSearch);
                            sizeNameUnmark = listNameUnmark.size();
                            if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                                if (comparatorName != null && sizeNameUnmark > 1)
                                    Collections.sort(listNameUnmark, comparatorName);
                                for (String tmp : listKeySearch) {
                                    if (Utilities.notEmpty(tmp)) {
                                        boolean checkTmp = false;
                                        for (String nameTmp : listNameUnmark) {
                                            if (nameTmp.startsWith(tmp)) {
                                                checkTmp = true;
                                                listNameUnmark.remove(nameTmp);
                                                break;
                                            }
                                        }
                                        check = checkTmp;
                                        if (!check) break;
                                    }
                                }
                                if (check) listResults04.add(item);
                            }
                        }
                    }
                }
            }
            listContactsTmp.clear();
            mapMessages.clear();
            if (Utilities.notEmpty(listResults01)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults01, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults01);
                listResults01.clear();
            }
            if (Utilities.notEmpty(listResults02)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults01, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults02);
                listResults02.clear();
            }
            if (Utilities.notEmpty(listResults03)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults03, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults03);
                listResults03.clear();
            }
            if (Utilities.notEmpty(listResults04)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults04, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults04);
                listResults04.clear();
            }
        }
    }

    private void searchWithDigits(ArrayList<ContactProvisional> list, CopyOnWriteArrayList<ContactProvisional> data) {
        //todo keySearch là số
        String oldKeySearch = keySearch;
        Map<String, Integer> mapMessages = new HashMap<>();
        Map<String, Integer> mapContacts = new HashMap<>();
        if (Utilities.notEmpty(data)) {
            keySearch = SearchUtils.getKeySearchChat(keySearch);
            String tmpNumber = PhoneNumberHelper.getInstant().getPhoneNumberFromText(mApplication.get(), keySearch);
            if (Utilities.notEmpty(tmpNumber)) {
                keySearch = tmpNumber;
            }
            Log.d(TAG, "searchContactsWithKeySearchIsDigits oldKeySearch: " + oldKeySearch + ", keySearch: " + keySearch);
            ArrayList<ContactProvisional> listResults01 = new ArrayList<>(); //todo tên giống hệt keySearch
            ArrayList<ContactProvisional> listResults02 = new ArrayList<>(); //todo SĐT giống hệt keySearch
            ArrayList<ContactProvisional> listResults03 = new ArrayList<>(); //todo tên bắt đầu bằng keySearch
            ArrayList<ContactProvisional> listResults04 = new ArrayList<>(); //todo SĐT bắt đầu bằng keySearch
            ArrayList<ContactProvisional> listResults05 = new ArrayList<>(); //todo SĐT chứa bằng keySearch
            ArrayList<ContactProvisional> listResults06 = new ArrayList<>(); //todo SĐT của 1 thành viên bất kỳ trong nhóm chứa keySearch
            ArrayList<ContactProvisional> listMessagesTmp = new ArrayList<>();
            ArrayList<ContactProvisional> listContactsTmp = new ArrayList<>();
            for (ContactProvisional item : data) {
                if (item != null) {
                    if (item.getContact() instanceof ThreadMessage) {
                        ThreadMessage model = (ThreadMessage) item.getContact();
                        String name = model.getThreadName();
                        if (name == null) name = "";
                        else name = name.trim();
                        String phoneNumber = model.getSoloNumber();
                        if (phoneNumber == null) phoneNumber = "";
                        if (name.contains(keySearch) || phoneNumber.contains(keySearch)
                                || model.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                            listMessagesTmp.add(item);
                        }
                    } else if (item.getContact() instanceof PhoneNumber) {
                        PhoneNumber model = (PhoneNumber) item.getContact();
                        String name = model.getName();
                        if (name == null) name = "";
                        else name = name.trim();
                        String phoneNumber = model.getJidNumber();
                        if (phoneNumber == null) phoneNumber = "";
                        if (Utilities.notEmpty(phoneNumber) && (name.contains(keySearch) || phoneNumber.contains(keySearch))) {
                            listContactsTmp.add(item);
                        } else {
                            if (keySearch.startsWith("+") && keySearch.length() > 3) {
                                String rawNumber;
                                if (phoneNumber.startsWith("0")) {// so vn
                                    rawNumber = "+84" + phoneNumber.substring(1);
                                } else {
                                    rawNumber = model.getRawNumber();
                                }
                                if (Utilities.notEmpty(rawNumber) && rawNumber.contains(keySearch)) {
                                    model.setName(name);
                                    listContactsTmp.add(item);
                                }
                            }
                        }
                    }
                }
            }

            //todo thuc hien tim kiem
            for (ContactProvisional item : listMessagesTmp) {
                ThreadMessage model = (ThreadMessage) item.getContact();
                String name = model.getThreadName();
                if (name == null) name = "";
                else name = name.trim();
                int threadType = model.getThreadType();
                String phoneNumber = model.getSoloNumber();
                if (phoneNumber == null) phoneNumber = "";
                if (name.equals(keySearch)) {
                    listResults01.add(item);
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(phoneNumber))
                        mapMessages.put(phoneNumber, 1);
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !model.isStranger() && phoneNumber.equals(keySearch)) {
                    listResults02.add(item);
                    mapMessages.put(phoneNumber, 1);
                } else if (name.startsWith(keySearch)) {
                    listResults03.add(item);
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(phoneNumber))
                        mapMessages.put(phoneNumber, 1);
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !model.isStranger() && phoneNumber.startsWith(keySearch)) {
                    listResults04.add(item);
                    mapMessages.put(phoneNumber, 1);
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !model.isStranger() && phoneNumber.contains(keySearch)) {
                    listResults05.add(item);
                    mapMessages.put(phoneNumber, 1);
                } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    ArrayList<String> phoneNumbers = model.getPhoneNumbers();
                    for (String phone : phoneNumbers) {
                        if (Utilities.notEmpty(phone) && phone.contains(keySearch)) {
                            listResults06.add(item);
                            break;
                        }
                    }
                }
            }
            listMessagesTmp.clear();

            for (ContactProvisional item : listContactsTmp) {
                PhoneNumber model = (PhoneNumber) item.getContact();
                String phoneNumber = model.getJidNumber();
                if (!mapMessages.containsKey(phoneNumber)) {
                    String name = model.getName();
                    if (name == null) name = "";
                    else name = name.trim();
                    if (name.equals(keySearch)) {
                        listResults01.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.equals(keySearch)) {
                        listResults02.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (name.startsWith(keySearch)) {
                        listResults03.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.startsWith(keySearch)) {
                        listResults04.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.contains(keySearch)) {
                        listResults05.add(item);
                        mapContacts.put(phoneNumber, 1);
                    } else if (keySearch.startsWith("+") && keySearch.length() > 3) {
                        String rawNumber;
                        if (phoneNumber.startsWith("0")) {// so vn
                            rawNumber = "+84" + phoneNumber.substring(1);
                        } else {
                            rawNumber = model.getRawNumber();
                        }
                        if (Utilities.notEmpty(rawNumber) && rawNumber.contains(keySearch)) {
                            listResults05.add(item);
                            mapContacts.put(phoneNumber, 1);
                        }
                    }
                }
            }
            listContactsTmp.clear();
            if (Utilities.notEmpty(listResults01)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults01, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults01);
                listResults01.clear();
            }
            if (Utilities.notEmpty(listResults02)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults02, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults02);
                listResults02.clear();
            }
            if (Utilities.notEmpty(listResults03)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults03, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults03);
                listResults03.clear();
            }
            if (Utilities.notEmpty(listResults04)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults04, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults04);
                listResults04.clear();
            }
            if (Utilities.notEmpty(listResults05)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults05, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults05);
                listResults05.clear();
            }
            if (Utilities.notEmpty(listResults06)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults06, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults06);
                listResults06.clear();
            }
        }
        if (!mapMessages.containsKey(keySearch) && !mapContacts.containsKey(keySearch)
                && PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.get(), keySearch)) {
            if (list == null) list = new ArrayList<>();
            ContactProvisional item = new ContactProvisional();
            item.setTitle(keySearch);
            item.setContact(mApplication.get().getString(R.string.share_non_contact_desc));
            item.setContact(keySearch);
            item.setState(ContactProvisional.SEND);
            list.add(item);
        }
        mapMessages.clear();
        mapContacts.clear();
    }

}
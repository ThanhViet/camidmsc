package com.metfone.selfcare.module.netnews.MostNews.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.netnews.MostNews.adapter.MostNewsAdapter;
import com.metfone.selfcare.module.netnews.MostNews.presenter.IMostNewsPresenter;
import com.metfone.selfcare.module.netnews.MostNews.presenter.MostNewsPresenter;
import com.metfone.selfcare.module.netnews.MostNews.view.IMostNewsView;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.CustomLoadMoreView;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MostNewsFragment extends BaseFragment implements AbsInterface.OnNewsListener, IMostNewsView, BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.loadingView)
    View loadingView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.imvMoveTop)
    ImageView imvMoveTop;

    int currentPage = 1;
    MostNewsAdapter adapter;
    ArrayList<NewsModel> datas = new ArrayList<>();
    View notDataView, errorView;
    LinearLayoutManager layoutManager;
    boolean isRefresh;
    boolean isTabRefresh;
    long unixTime;
    int pid;

    IMostNewsPresenter mPresenter;

    public static MostNewsFragment newInstance() {
        MostNewsFragment fragment = new MostNewsFragment();
        fragment.mPresenter = new MostNewsPresenter();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child_base, container, false);
        if (mPresenter == null) {
            mPresenter = new MostNewsPresenter();
        }
        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);
        setUp(view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        pid = 100;
        isRefresh = true;
        if (datas == null || (datas.size() == 0)) {
            loadingView.setVisibility(View.VISIBLE);
            unixTime = 0;
            mPresenter.loadData(pid, currentPage, unixTime);
        }

        layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        layout_refresh.setOnRefreshListener(this);

        layoutManager = new LinearLayoutManager(getBaseActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setPadding(recyclerView.getPaddingLeft(), getResources().getDimensionPixelOffset(R.dimen.padding_news), recyclerView.getPaddingRight(), recyclerView.getPaddingBottom());

        adapter = new MostNewsAdapter(getBaseActivity(), R.layout.item_news_home_normal, datas, this);
        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(this);
        adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//        adapter.setAutoLoadMoreSize(3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        setupMoveTop();

        notDataView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_nodata, (ViewGroup) recyclerView.getParent(), false);
        notDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
        errorView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_failed, (ViewGroup) recyclerView.getParent(), false);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        loadingView.setVisibility(View.GONE);
        hideRefresh();
        if (!flag) {
            loadingFail();
        }
    }

    @Override
    public void bindData(NewsResponse response) {
        if (datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if (response != null) {
            if (response.getData() != null) {
                loadingComplete(response.getData());
            } else {
                loadingFail();
            }
        }

        isTabRefresh = false;
    }

    public void loadingComplete(ArrayList<NewsModel> response) {
        int mCurrentCounter = response.size();
        if (mCurrentCounter > 0)
            unixTime = response.get(response.size() - 1).getUnixTime();

        if (isTabRefresh) {
            //Check refresh
            if (datas.size() > 0 && response.size() > 0) {
                NewsModel newModel = response.get(0);
                NewsModel currentModel = datas.get(0);
                if (newModel.getID() == currentModel.getID()) {
                    return;
                } else {
                    currentPage = 1;
                }
            }
        }

        if (isRefresh) {
            if (mCurrentCounter == 0) {
                adapter.setEmptyView(notDataView);
            } else {
                datas.clear();
                adapter.setNewData(response);
                datas.addAll(response);

                if (datas.size() > 0) {
                    if (layoutManager != null)
                        layoutManager.scrollToPosition(0);
                }
            }
        } else {
            if (mCurrentCounter == 0) {
                adapter.loadMoreEnd();
            } else {
                adapter.addData(response);
                datas.addAll(response);
                adapter.loadMoreComplete();
            }
        }
    }

    public void loadingFail() {
        if (isRefresh) {
            adapter.setEmptyView(errorView);
        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        unixTime = 0;
        if (mPresenter != null)
            mPresenter.loadData(pid, currentPage, unixTime);
    }

    public void onTabRefresh() {
        isTabRefresh = true;
        unixTime = 0;
        if (mPresenter != null)
            mPresenter.loadData(pid, 1, unixTime);
    }

    @Override
    public void onLoadMoreRequested() {
        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                currentPage++;
                isRefresh = false;
                mPresenter.loadData(pid, currentPage, unixTime);
            }

        }, 1000);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onItemClick(int position) {
        try {
            NewsModel model = datas.get(position);
            if (model == null) return;
            model.setReadFromSource(CommonUtils.TYPE_NEWS_DETAIL_FROM_CATEGORY);
            if (position == 0)
                model.setPositionFirst(true);
            else
                model.setPositionFirst(false);

            readNews(model);
        } catch (IndexOutOfBoundsException ex) {
            Log.e(TAG, "Exception", ex);

        }
    }

    @Override
    public void onItemRadioClick(NewsModel model) {

    }

    @Override
    public void onItemClickMore(NewsModel model) {

    }

    public void setupMoveTop() {
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
//                    int lastVisible = layoutManager.findLastCompletelyVisibleItemPosition();
                    int firstVisible = layoutManager.findFirstCompletelyVisibleItemPosition();
                    if (dy <= 0)// || lastVisible == datas.size() - 1)
                    {
                        if (firstVisible == 0) {
                            //Hide
                            hideMoveTop(imvMoveTop);
                        } else {
                            //Show
                            showMoveTop(imvMoveTop);
                        }
                    } else {
                        //Hide
                        hideMoveTop(imvMoveTop);
                    }
                } catch (Exception ex) {
                    Log.e(TAG, "Exception", ex);

                }
            }
        });
        imvMoveTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutManager.scrollToPosition(0);
            }
        });
    }

    public void updateLastNews() {
        mPresenter.updateLastNews(pref, datas.get(0).getID());
    }
}

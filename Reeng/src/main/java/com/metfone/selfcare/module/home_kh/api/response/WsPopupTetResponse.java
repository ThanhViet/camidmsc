package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.ArrayList;

import lombok.Data;

@Data
public class WsPopupTetResponse extends BaseResponse<WsPopupTetResponse.Response> {
    @Data
    public class Response {
        @SerializedName("isShowPopupTet")
        private boolean isShowPopupTet;
    }
}


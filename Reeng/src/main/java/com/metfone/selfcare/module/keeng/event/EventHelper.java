package com.metfone.selfcare.module.keeng.event;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by namnh40 on 4/10/2017.
 */

public class EventHelper {
    public static EventBus getDefault() {
        return EventBus.getDefault();
    }

    public static void removeStickyEvent(Object event) {
        if (event != null) {
            getDefault().removeStickyEvent(event);
        }
    }
}

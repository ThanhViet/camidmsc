/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/3/27
 *
 */

package com.metfone.selfcare.module.security.listener;

import com.metfone.selfcare.module.security.model.PhoneNumberModel;

public interface SecurityListener {

    void onSpamWhiteListRemove(PhoneNumberModel item);

    void onSpamBlackListRemove(PhoneNumberModel item);

    void onFirewallWhiteListRemove(PhoneNumberModel item);

    void onDeleteAllSpamSmsClick();

}

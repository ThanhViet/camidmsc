package com.metfone.selfcare.module.home_kh.tab;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.helper.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateConvert {

    public static String formatDateTime(Context c, String time) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        if (time == null) {
            return "";
        }

        try {
            Locale locale = new Locale(LocaleManager.getLanguage(c));
            Log.e("TND", "Local code - " + LocaleManager.getLanguage(c));
            Log.e("TND", "Local - " + locale.getCountry());
            Date date = format.parse(time);
            SimpleDateFormat formatOut = new SimpleDateFormat("MMMM d, yyyy", locale);

            return formatOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String formatDateTimeNotification(Context c, String time) {
//        2020-12-14 11:44:53.0
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        if (time == null) {
            return "";
        }

        try {
            Locale locale = new Locale(LocaleManager.getLanguage(c));
            Log.e("TND", "Local code - " + LocaleManager.getLanguage(c));
            Log.e("TND", "Local - " + locale.getCountry());
            Date date = format.parse(time);
            SimpleDateFormat formatOut = new SimpleDateFormat("MMMM d, yyyy", locale);

            return formatOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static final String DATE_REWARD_INPUT = "yyyy-MM-dd'T'hh:mm:ss";
    public static final String DATE_GIFT_INPUT = "yyyy/MM/dd HH:mm:ss";
    public static final String DATE_GIFT_REDEEM_OUTPUT = "MMMM dd,yyyy";
    public static final String DATE_REWARD_OUTPUT = "HH:mm aaa MMMM dd,yyyy";
    public static final String DATE_REWARD_OUTPUT_EXPIRED = "MM/dd/yyyy";
    public static final String DATE_REWARD_OUTPUT_EXPIRED_KM = "dd/MM/yyyy";
    public static final String DATE_HISTORY_POINT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_REWARD_DETAIL_EXPIRED = "yyyy-MM-dd";

    public static String convert(String time, String inFormat, String outFormat) {
        if (TextUtils.isEmpty(time) || TextUtils.isEmpty(inFormat) || TextUtils.isEmpty(outFormat)) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat(inFormat, Locale.ENGLISH);
        try {
            Date date = format.parse(time);
            SimpleDateFormat formatOut = new SimpleDateFormat(outFormat, Locale.ENGLISH);
            return formatOut.format(date);
        } catch (Exception e) {
            Log.e("FOMATDATE", e.toString());
            e.printStackTrace();
            return "";
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static long convertStringToLong(String date, String format) {
        if (TextUtils.isEmpty(date)) {
            return 0;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.parse(date).getTime();

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getTodayDate(Context c, String format) {
        Locale locale = new Locale(LocaleManager.getLanguage(c));
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        return sdf.format(new Date());
    }
}

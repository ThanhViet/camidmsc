package com.metfone.selfcare.module.games.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabGame.GameBaseResponseData;
import com.metfone.selfcare.model.tabGame.GameCategoryModel;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.module.games.activity.PlayGameActivity;
import com.metfone.selfcare.module.games.api.GameApi;
import com.metfone.selfcare.module.games.holder.ResultSearchGameViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DetailCategoryGameFragment extends BaseFragment implements ItemViewClickListener {
    public static final String CATEGORY_KEY = "mCategory";

    public static DetailCategoryGameFragment newInstance(GameCategoryModel category) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CATEGORY_KEY, category);
//        bundle.putString(ID_KEY, id);
//        bundle.putString(NAME_KEY, name);
        DetailCategoryGameFragment fragment = new DetailCategoryGameFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @BindView(R.id.recyclerAction)
    RecyclerView recyclerAction;
    @BindView(R.id.icBack)
    AppCompatImageView icBack;
    @BindView(R.id.titleLayout)
    AppCompatTextView titleLayout;


    @BindView(R.id.progress)
    ProgressBar processing;
    private String mCategory;
    private String mCategoryID;
    private Unbinder unbinder;
    private ArrayList<GameModel> result = new ArrayList<>();
    private BaseAdapter actionAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.action_category_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listener();
        loadData();
        ResultSearchGameViewHolder resultCategoryViewHolder = new ResultSearchGameViewHolder(Objects.requireNonNull(getActivity()).getWindow().getDecorView().getRootView(), this);
        actionAdapter = new BaseAdapter(result, getContext(), R.layout.item_game_new, resultCategoryViewHolder);
        recyclerAction.setLayoutManager(new GridLayoutManager(getContext(), 4));
        recyclerAction.setAdapter(actionAdapter);
    }

    private void loadData() {

        GameApi.getInstance().getGameByName("", new GameApi.ListenerGameApi() {
            @Override
            public void onPreRequest() {
                processing.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(Object responseData) {
                if (responseData instanceof GameBaseResponseData) {
                    GameBaseResponseData data = (GameBaseResponseData) responseData;
                    if (data.getItems() == null || data.getItems().length == 0) {
                        return;
                    }
                    result.clear();
                    for (GameCategoryModel items : data.getItems()) {
                        if (mCategoryID.equals(items.getID()+"")) {
                            result.addAll(Arrays.asList(items.getGames()));
                        }
                    }
                    actionAdapter.updateAdapter(result);
                    if (result.size() == 0) {
                        recyclerAction.setVisibility(View.INVISIBLE);
                    } else {
                        recyclerAction.setVisibility(View.VISIBLE);
                    }
                }
                processing.setVisibility(View.GONE);

            }

            @Override
            public void onError(String message) {
                processing.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String exp) {
                processing.setVisibility(View.GONE);
            }
        });
    }

    private void loadCategory(){
    }

    private void listener() {
        if (getArguments() != null) {
            GameCategoryModel category = (GameCategoryModel) getArguments().getSerializable(CATEGORY_KEY);
            mCategoryID = category.getID()+"";
            mCategory = category.getName();
//            mCategory = category.getNameKM();
        }
        if(!StringUtils.isEmpty(mCategory)){
            titleLayout.setText(mCategory);
        }else{
            titleLayout.setText(R.string.category_movie);
            loadCategory();
        }
        icBack.setOnClickListener(v -> {
            popBackStackFragment();
//            if (getActivity() instanceof HomeActivity) {
//                popBackStackFragment();
//            } else {
//                getActivity().finish();
//            }
        });
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }


    @Override
    public void onItemViewClickListener(int position, List<?> list) {

        if (list.get(position) instanceof GameModel) {
            GameModel game = (GameModel) list.get(position);
            Intent intent = new Intent(getActivity(), PlayGameActivity.class);
            intent.putExtra("OPEN_GAME", game);
            startActivity(intent);
        }
    }
}

package com.metfone.selfcare.module.metfoneplus.exchangedamaged.fragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.iknow.trimvideo.view.select.VideoSelectAdapter;
import com.metfone.esport.ui.uploadVideo.choose.ChooseVideoActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.model.camid.ChangeCardDamaged;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.DialogFilterExchangeDamaged;
import com.metfone.selfcare.module.metfoneplus.exchangedamaged.adapter.ExchangeDamagedAdapter;
import com.metfone.selfcare.module.metfoneplus.ishare.adapter.IshareContactAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.WsChangeCardGetListRequest;
import com.metfone.selfcare.network.metfoneplus.response.WsChangeCardListResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComplaintHistoryResponse;
import com.metfone.selfcare.util.Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import iknow.android.utils.callback.SimpleCallback;
import retrofit2.Response;

public class ExchangeDamagedListFragment extends MPBaseFragment implements ItemViewClickListener, View.OnClickListener {
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.action_bar_option)
    RelativeLayout action_bar_option;
    @BindView(R.id.action_bar_option_img)
    ImageView action_bar_option_img;
    @BindView(R.id.rvExchangeDamaged)
    RecyclerView rvExchangeDamaged;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.tvRangeDate)
    TextView tvRangeDate;
    private ExchangeDamagedAdapter exchangeDamagedAdapter;
    private List<ChangeCardDamaged> changeCardDamageds;
    private String mDateFrom, mDateTo;
    private String status = "";
    Date currentDate;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_exchange_damaged;
    }

    public static ExchangeDamagedListFragment newInstance() {
        Bundle args = new Bundle();
        ExchangeDamagedListFragment fragment = new ExchangeDamagedListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        changeCardDamageds = new ArrayList<>();
        currentDate = Calendar.getInstance().getTime();
        mDateTo = simpleDateFormat.format(currentDate);
        mDateFrom = simpleDateFormat.format(currentDate);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(view);
        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void initViews() {
        action_bar_title.setText(R.string.m_p_more_exchange_damaged_card);
        action_bar_option.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setColorSchemeColors(getActivity().getResources().getColor(R.color.keeng_primary));
        exchangeDamagedAdapter = new ExchangeDamagedAdapter(getContext(), changeCardDamageds, new ExchangeDamagedAdapter.IExchangeDamagedListener() {
            @Override
            public void selectContact(int position) {
                if (changeCardDamageds.get(position).isExpand()) {
                    changeCardDamageds.get(position).setExpand(false);
                    exchangeDamagedAdapter.notifyItemChanged(position);
                } else {
                    for (int i = 0; i < changeCardDamageds.size(); i++) {
                        if (changeCardDamageds.get(i).isExpand()) {
                            exchangeDamagedAdapter.notifyItemChanged(i);
                            break;
                        }
                        changeCardDamageds.get(i).setExpand(false);
                    }
                    changeCardDamageds.get(position).setExpand(true);
                    exchangeDamagedAdapter.notifyItemChanged(position);
                }

            }
        });
        rvExchangeDamaged.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvExchangeDamaged.setAdapter(exchangeDamagedAdapter);
        PermissionUtils.permission(PermissionConstants.STORAGE)
                .rationale((activity, shouldRequest) -> {
                    shouldRequest.again(true);
                })
                .callback(new PermissionUtils.SimpleCallback() {
                    @Override
                    public void onGranted() {
                        getWSExchangeListList();
                    }

                    @Override
                    public void onDenied() {
                        ToastUtils.showShort(R.string.msg_grant_camera_storage_permission);
                    }
                }).request();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getWSExchangeListList();
            }
        });
        tvRangeDate.setText(mDateFrom + " - " + mDateTo);
        if (status.isEmpty()) {
            tvStatus.setText(getString(R.string.exchange_damaged_all_status));
        } else if (status.equals("0")) {
            tvStatus.setText(getString(R.string.exchange_damaged_waiting));
        } else if (status.equals("1")) {
            tvStatus.setText(getString(R.string.exchange_damaged_approved));
        } else
            tvStatus.setText(getString(R.string.exchange_damaged_approved));
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        if (mParentActivity instanceof HomeActivity) {
            popBackStackFragment();
        } else {
            mParentActivity.finish();
        }
    }

    @OnClick(R.id.rlFilter)
    public void openFilter() {
        DialogFilterExchangeDamaged dialog = DialogFilterExchangeDamaged.newInstance(mDateFrom, mDateTo, status);
        dialog.dialogSelectDateListener = (dFrom, dTo, mStatus) -> {
            mDateFrom = dFrom;
            mDateTo = dTo;
            status = mStatus;
            if (status.isEmpty()) {
                tvStatus.setText(getString(R.string.exchange_damaged_all_status));
            } else if (status.equals("0")) {
                tvStatus.setText(getString(R.string.exchange_damaged_waiting));
            } else if (status.equals("1")) {
                tvStatus.setText(getString(R.string.exchange_damaged_approved));
            } else
                tvStatus.setText(getString(R.string.exchange_damaged_approved));
            tvRangeDate.setText(mDateFrom + " - " + mDateTo);
            getWSExchangeListList();
//            getWSComplaintHistoryList(mDateFrom, mDateTo, totalComplaint);
        };

        dialog.show(getParentFragmentManager(), DialogFilterExchangeDamaged.TAG);
    }

    @Override
    public void popBackStackFragment() {
        super.popBackStackFragment();
        updateBottomMenuColor(R.color.m_home_tab_background);
    }

    @OnClick(R.id.action_bar_option)
    public void handleClick(View view) {
        if (view.getId() == R.id.action_bar_option)
            replaceFragmentWithAnimationDefault(AddExchangeDamagedFragment.newInstance());
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {
    }

    public void setupUI(View view) {
    }

    private void getWSExchangeListList() {
        mParentActivity.showLoadingDialog("", mRes.getString(R.string.processing), false);
        new MetfonePlusClient().wsGetChangeCardList(mDateFrom, mDateTo, status, new MPApiCallback<WsChangeCardListResponse>() {
            @Override
            public void onResponse(Response<WsChangeCardListResponse> response) {
                mParentActivity.hideLoadingDialog();
                changeCardDamageds.clear();
                if (response != null && response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null) {
                    List<ChangeCardDamaged> temp = response.body().getResult().getWsResponse().getChangeCardDamageds();
                    if (temp != null && !temp.isEmpty()) {
                        for (int i = 0; i < temp.size(); i++) {
                            File storageDir;
                                 storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                            try {
                                File photo = new File(storageDir, "CamID_" + temp.get(i).getId() +  /* prefix */
                                        ".mp4");
                                if (photo.exists()) {
                                    temp.get(i).setPathVideo(photo.getAbsolutePath());
                                    continue;
                                }
                                FileOutputStream fos = new FileOutputStream(photo, true);
                                fos.write(Base64.decode(temp.get(i).getDataVideo(), Base64.DEFAULT));
                                fos.close();
                                temp.get(i).setPathVideo(photo.getAbsolutePath());
                            } catch (Exception e) {
                            }
                        }
                        changeCardDamageds.addAll(temp);
                    } else {
                        showToast("Data not found!");
                    }
                }else {
                    showToast("Data not found!");
                }
                exchangeDamagedAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);


            }

            @Override
            public void onError(Throwable error) {

                mParentActivity.hideLoadingDialog();
                showToast("Data not found!");
            }
        });
    }

    public void showToast(final String stringResourceId) {
        com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(),stringResourceId);

    }
}

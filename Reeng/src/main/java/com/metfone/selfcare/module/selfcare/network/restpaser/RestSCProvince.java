package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCProvince;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCProvince extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCProvince> data;

    public ArrayList<SCProvince> getData() {
        return data;
    }

    public void setData(ArrayList<SCProvince> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCProvince [data=" + data + "] errror " + getErrorCode();
    }
}

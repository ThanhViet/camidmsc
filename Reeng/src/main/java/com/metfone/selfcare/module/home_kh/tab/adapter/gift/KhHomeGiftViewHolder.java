package com.metfone.selfcare.module.home_kh.tab.adapter.gift;

import android.app.Activity;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeGiftItem;

import butterknife.BindView;

public class KhHomeGiftViewHolder extends BaseAdapter.ViewHolder {
    @BindView(R.id.recyclerView)
    public RecyclerView recyclerView;
    private KhHomeGiftAdapter adapter;

    public KhHomeGiftViewHolder(View view, Activity activity, KhHomeGiftAdapter.EventListener listener) {
        super(view);
        adapter = new KhHomeGiftAdapter(activity, listener);
        recyclerView.setLayoutManager(new GridLayoutManager(activity, 2));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void bindData(Object item, int position) {
        super.bindData(item, position);
        if(item != null){
            adapter.setItems(((KhHomeGiftItem)item).giftItems);
            adapter.notifyDataSetChanged();
        }
    }
}

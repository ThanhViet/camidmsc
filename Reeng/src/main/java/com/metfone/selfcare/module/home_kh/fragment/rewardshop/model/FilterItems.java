package com.metfone.selfcare.module.home_kh.fragment.rewardshop.model;

import android.graphics.drawable.Drawable;

import androidx.annotation.DrawableRes;

import java.io.Serializable;

public class FilterItems implements Serializable {
    String id;
    String name;
    @DrawableRes
    int drawable;
    String url;

    public FilterItems(String id, String name, int drawable) {
        this.id = id;
        this.name = name;
        this.drawable = drawable;
    }

    public FilterItems(String id, String name, int drawable, String url) {
        this.id = id;
        this.name = name;
        this.drawable = drawable;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

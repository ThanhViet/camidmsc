package com.metfone.selfcare.module.newdetails.ChildDetailNews.presenter;

import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;
import com.metfone.selfcare.module.newdetails.model.NewsModel;

/**
 * Created by HaiKE on 8/21/17.
 */

public interface INewsDetailPresenter extends MvpPresenter {

//    void loadData(String url);
//    void loadDataRelate(String url, int page);
    void saveFontSizeCache(int size);

    void loadData(NewsModel model);
    void loadDataRelate(NewsModel model, int page, long unixTime);

    void loadNewsStatus(String url);
}


package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Data;

public class WsGetPointTransferHistoryRequest extends BaseRequest<WsGetPointTransferHistoryRequest.Request> {
    public static class Request {
    }
    public static class WsRequest extends Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("language")
        String language;
        @SerializedName("limit")
        int limit;
        @SerializedName("offset")
        int offset;
        @SerializedName("type")
        String type;
        @SerializedName("custId")
        public String custId;

        public WsRequest(String isdn, String language, int limit, int offset, String type) {
            this.isdn = isdn;
            this.language = language;
            this.limit = limit;
            this.offset = offset;
            this.type = type;
        }

        public WsRequest(String isdn, String type) {
            this.isdn = isdn;
            this.type = type;
            this.language = "en";
            this.limit = 10;
            this.offset = 0;
        }
    }
}

package com.metfone.selfcare.module.backup_restore.backup;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Process;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQBackupMsg;

import java.util.Calendar;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class BackupManager {
    public static final String TAG = BackupManager.class.getSimpleName();

    public static void autoBackupMessage(Context context) {
        if (!Config.Features.FLAG_SUPPORT_BACKUP_MESSAGES || context == null) return;
        try {
            if (!NetworkHelper.isConnectInternet(context)) return;
            SharedPreferences pref = context.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
            if (pref == null) return;
            int autoBackupType = pref.getInt(SharedPrefs.KEY.BACKUP_SETTING_AUTO_TYPE, BackupUtils.BACKUP_AUTO_TYPE_OFF);
            if (autoBackupType == BackupUtils.BACKUP_AUTO_TYPE_OFF) return;

            long lastBackupTime = pref.getLong(SharedPrefs.KEY.BACKUP_LAST_TIME, 0l);
            if (lastBackupTime >= System.currentTimeMillis()) return;
            Calendar calander = Calendar.getInstance();
            int currentDate = calander.get(Calendar.DAY_OF_YEAR);
            int currentWeek = calander.get(Calendar.WEEK_OF_YEAR);
            int currentMonth = calander.get(Calendar.MONTH) + 1;
            int currentYear = calander.get(Calendar.YEAR);

            Calendar lastCalendar = Calendar.getInstance();
            lastCalendar.setTimeInMillis(lastBackupTime);
            int lastDate = lastCalendar.get(Calendar.DAY_OF_YEAR);
            int lastWeek = lastCalendar.get(Calendar.WEEK_OF_YEAR);
            int lastMonth = lastCalendar.get(Calendar.MONTH) + 1;
            int lastYear = lastCalendar.get(Calendar.YEAR);

            if (currentYear > lastYear) {
                doAutoBackup(context, pref);
                return;
            }

            if (currentMonth > lastMonth) {
                doAutoBackup(context, pref);
                return;
            }

            if (currentWeek > lastWeek && (autoBackupType == BackupUtils.BACKUP_AUTO_TYPE_WEEKLY || autoBackupType == BackupUtils.BACKUP_AUTO_TYPE_DAILY)) {
                doAutoBackup(context, pref);
                return;
            }

            if (currentDate > lastDate && autoBackupType == BackupUtils.BACKUP_AUTO_TYPE_DAILY) {
                doAutoBackup(context, pref);
                return;
            }
        } catch (Exception e) {
            Log.e(TAG, "autoBackupMessage: Exception! ", e);
        }
    }

    private static void doAutoBackup(Context context, SharedPreferences pref) {
        if (!Config.Features.FLAG_SUPPORT_BACKUP_MESSAGES) return;
        if (pref == null) return;
        boolean isBackupViaWifiOnly = pref.getBoolean(SharedPrefs.KEY.BACKUP_SETTING_NETWORK_ONLY_WIFI, true);

        if (isBackupViaWifiOnly && NetworkHelper.checkTypeConnection(context) == NetworkHelper.TYPE_MOBILE) {
            return;
        }

        DBExporter.getInstance().exportMessageAndThread(null,0);
    }

    public static void doBackup(DBExporter.BackupProgressListener listener, int syncDesktop) {
        if (!Config.Features.FLAG_SUPPORT_BACKUP_MESSAGES) return;
        DBExporter.getInstance().exportMessageAndThread(listener, syncDesktop);
    }

    public static void cancelBackup() {
        DBExporter.getInstance().cancelBackup();
    }

    public static void clearBackupFailData(){
        DBExporter.clearData();
    }

    public static boolean isBackupInProgress() {
        return DBExporter.getInstance().isExporting();
    }

    public static void sendIQBackUpError(int error) {
        if (ApplicationController.self().getXmppManager().isAuthenticated()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    IQBackupMsg iqChange = new IQBackupMsg();
                    iqChange.setType(IQ.Type.SET);
                    iqChange.setErrorCode(error);
                    try {
                        ApplicationController.self().getXmppManager().sendPacketNoWaitResponse(iqChange);
                        /*IQBackupMsg result = (IQBackupMsg) ApplicationController.self().getXmppManager().sendPacketThenWaitingResponse(iqChange, false);
                        if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                            if (result.getErrorCode() == 0) {
                                Log.d(TAG, "S----------done sendIQBackUpError");
                            } else {
                                Log.i(TAG, "S----------error sendIQBackUpError: " + result.getErrorCode());
                            }
                        } else {
                        }*/
                    } catch (Exception e) {
                        Log.e(TAG, "S----------error sendIQBackUpError", e);
                    }
                }
            }).start();
        }
    }
}

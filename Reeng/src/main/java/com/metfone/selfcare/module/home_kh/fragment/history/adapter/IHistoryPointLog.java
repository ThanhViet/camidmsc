package com.metfone.selfcare.module.home_kh.fragment.history.adapter;

public interface IHistoryPointLog {

    long getId();

    String getName();

    String getCreatedAt();

    long getValue();

    String valueShow(String start);
}

package com.metfone.selfcare.module.netnews.bottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.netnews.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.BottomSheetModel;
import com.viettel.util.Log;

import java.util.List;

/**
 * Created by HaiKE on 9/15/17.
 */

public class BottomSheetAdapter extends BaseAdapter<BaseHolder> {

    public static final int ITEM_EMPTY = 0;
    public static final int ITEM_BOTTOM_SHEET = 1;
    List<BottomSheetModel> datas;
    AbsInterface.OnClickBottomSheet listener;
    Context mContext;

    public BottomSheetAdapter(Context context, List<BottomSheetModel> datas, AbsInterface.OnClickBottomSheet listener) {
        super(context);
        this.datas = datas;
        this.listener = listener;
        this.mContext = context;
    }

    @Override
    public int getItemCount() {
        if (datas != null)
            return datas.size();
        return 0;
    }

    public BottomSheetModel getItem(int position) {
        try {
            return datas.get(position);
        } catch (IndexOutOfBoundsException e) {
            Log.e(BottomSheetAdapter.class.getSimpleName(), "Exception", e);
        } catch (Exception e) {
            Log.e(BottomSheetAdapter.class.getSimpleName(), "Exception", e);
        }
        return null;
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int type) {
        if (type == ITEM_BOTTOM_SHEET) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_bottom_sheet_news, parent, false);
            return new BottomSheetHolder(view, listener);
        }
        else
        {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_empty, parent, false);
            return new BaseHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (holder instanceof BottomSheetHolder) {
            BottomSheetHolder itemHolder = (BottomSheetHolder) holder;
            itemHolder.bind(getItem(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        BottomSheetModel item = getItem(position);
        if (item == null) {
            return ITEM_EMPTY;
        }
        return ITEM_BOTTOM_SHEET;
    }
}

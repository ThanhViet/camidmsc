package com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.tiin.AppProvider;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.fragment.ChildTiinDetailFragment;
import com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.adapter.MainDetailTiinAdapter;
import com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.presenter.IMainDetailTiinMvpPresenter;
import com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.presenter.MainDetailTiinPresenter;
import com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.view.MainDetailTiinMvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainTiinDetailFragment extends BaseFragment implements MainDetailTiinMvpView {

    @Nullable
    @BindView(R.id.view_detail_news)
    ViewPager viewPager;
    Unbinder unbinder;
//    @BindView(R.id.pageIndicatorView)
//    PageIndicatorViewCustom pageIndicatorView;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.tv_category_new)
    TextView tvCategory;
    @BindView(R.id.line1)
    View line1;
    @BindView(R.id.line2)
    View line2;
    @BindView(R.id.line3)
    View line3;
    @BindView(R.id.line4)
    View line4;
    @BindView(R.id.line5)
    View line5;
    private MainDetailTiinAdapter adapter;
    private IMainDetailTiinMvpPresenter mPresenter;
    private TiinModel tiinModel;
    private List<TiinModel> datas = new ArrayList<>();
    private int reTryModel = 1;
    private static final int COUNT = 3;
    private int currentPage = 1;
    int logCurrent = 0;
    private final int NUM = 10;
    private int numSize = 0;
    private ChildTiinDetailFragment homeNewsDetailFragment;
    private boolean fromTiin = false;

    public static MainTiinDetailFragment newInstance(Bundle bundle) {
        MainTiinDetailFragment fragment = new MainTiinDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_main_tiin, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (mPresenter == null) {
            mPresenter = new MainDetailTiinPresenter();
        }
        mPresenter.onAttach(this);
        setUpViewPager();
        return view;
    }

    public void loadCatagory(String category) {
        if (tvCategory != null) {
            if(datas != null && datas.size() > 0) {
                datas.get(0).setCategory(category);
            }
            tvCategory.setVisibility(View.VISIBLE);
            tvCategory.setText(category);
        }
    }

    private void setUpViewPager() {
        tiinModel = (TiinModel) getArguments().getSerializable(ConstantTiin.INTENT_MODULE);
        fromTiin = getArguments().getBoolean(ConstantTiin.FULL_CONTENT_TIIN);
        if (tiinModel == null) {
            tiinModel = new TiinModel();
        }
        if (adapter == null) {
            adapter = new MainDetailTiinAdapter(getChildFragmentManager());
            Bundle bundle = new Bundle();
            bundle.putSerializable(ConstantTiin.KEY_ITEM_TIIN, tiinModel);
            bundle.putBoolean(ConstantTiin.FULL_CONTENT_TIIN, fromTiin);
            homeNewsDetailFragment = ChildTiinDetailFragment.newInstance(bundle, true);
            adapter.addFragment(homeNewsDetailFragment, "Home News Detail");
            datas.clear();
            datas.add(tiinModel);
        }
        if (!TextUtils.isEmpty(tiinModel.getCategory())) {
            tvCategory.setVisibility(View.VISIBLE);
            tvCategory.setText(tiinModel.getCategory());
        } else {
            tvCategory.setVisibility(View.INVISIBLE);
        }
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == (numSize - 1) && position > 0) {
                    currentPage++;
//                    unixTime = datas.get(datas.size() - 1).getUnixTime();
                    mPresenter.getDataSibling(tiinModel.getId(), currentPage, NUM);
                }
                setUpIndicator(position);
                if (adapter != null) {
                    ChildTiinDetailFragment fragment = (ChildTiinDetailFragment) adapter.getItem(position);
                    if (fragment != null) {
                        fragment.loadNewsStatus();
                        fragment.scrollFirst();
                    }
                }
                if (tvCategory != null) {
                    tvCategory.setVisibility(View.VISIBLE);
                    tvCategory.setText(datas.get(position).getCategory());
                }
                if (position > logCurrent) {
                    //todo show log
//                    getBaseActivity().trackingEvent(R.string.read_tiin, datas.get(position).getTitle(), "" + datas.get(position).getId());
                    mPresenter.addItemSeen(datas.get(position).getIdFilter());
                    TiinUtilities.logClickTiin(datas.get(position));
                    logCurrent = position;
                }

                if (getTiinDetailActivity() != null) {
                    getTiinDetailActivity().stopVideoDetail();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        TiinUtilities.logClickTiin(tiinModel);
        mPresenter.addItemSeen(tiinModel.getIdFilter());
        setVisibilityClose(AppProvider.CLICK_CLOSE);
        setUpIndicator(0);
    }
    @SuppressLint("ResourceType")
    private void setUpIndicator(int position) {
        if (position % 5 == 0) {
            line1.setBackgroundResource(R.color.v5_main_color);
            line2.setBackgroundResource(R.color.v5_cancel);
            line3.setBackgroundResource(R.color.v5_cancel);
            line4.setBackgroundResource(R.color.v5_cancel);
            line5.setBackgroundResource(R.color.v5_cancel);
        } else if (position % 5 == 1) {
            line1.setBackgroundResource(R.color.v5_cancel);
            line2.setBackgroundResource(R.color.v5_main_color);
            line3.setBackgroundResource(R.color.v5_cancel);
            line4.setBackgroundResource(R.color.v5_cancel);
            line5.setBackgroundResource(R.color.v5_cancel);
        } else if (position % 5 == 2) {
            line1.setBackgroundResource(R.color.v5_cancel);
            line2.setBackgroundResource(R.color.v5_cancel);
            line3.setBackgroundResource(R.color.v5_main_color);
            line4.setBackgroundResource(R.color.v5_cancel);
            line5.setBackgroundResource(R.color.v5_cancel);
        } else if (position % 5 == 3) {
            line1.setBackgroundResource(R.color.v5_cancel);
            line2.setBackgroundResource(R.color.v5_cancel);
            line3.setBackgroundResource(R.color.v5_cancel);
            line4.setBackgroundResource(R.color.v5_main_color);
            line5.setBackgroundResource(R.color.v5_cancel);
        } else if (position % 5 == 4) {
            line1.setBackgroundResource(R.color.v5_cancel);
            line2.setBackgroundResource(R.color.v5_cancel);
            line3.setBackgroundResource(R.color.v5_cancel);
            line4.setBackgroundResource(R.color.v5_cancel);
            line5.setBackgroundResource(R.color.v5_main_color);
        }
    }

    @Override
    public void loadDataSuccess(boolean flag) {

    }

    public void loadData() {
        if (mPresenter != null && tiinModel != null) {
            currentPage = 1;
            mPresenter.getDataSibling(tiinModel.getId(), currentPage, NUM);
        }
    }

    @Override
    public void bindDataSibling(List<TiinModel> response) {
        if (response != null && response.size() > 0) {
            for (int i = 0; i < response.size(); i++) {
                TiinModel model = response.get(i);
                if (tiinModel.getId() != model.getId()) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ConstantTiin.KEY_ITEM_TIIN, model);
                    ChildTiinDetailFragment newsDetailFragment = ChildTiinDetailFragment.newInstance(bundle, false);
                    adapter.addFragment(newsDetailFragment, "");
                    datas.add(model);
                    setVisiableLine(i);
                }
            }

            adapter.notifyDataSetChanged();
//            if (pageIndicatorView != null) {
//                pageIndicatorView.setVisibility(View.VISIBLE);
//                pageIndicatorView.setViewPager(viewPager);
//            }
            numSize = datas.size();
        } else {
            if (reTryModel <= COUNT) {
                reTryModel++;
                mPresenter.getDataSibling(tiinModel.getId(), currentPage, NUM);
            }
        }

    }

    @OnClick(R.id.btnBack)
    public void onClickBack() {
        if (getTiinDetailActivity() != null) {
            getTiinDetailActivity().onBackPressed();
        }
    }

    @OnClick(R.id.iv_close)
    public void onClickClose() {
        if (getTiinDetailActivity() != null) {
            getTiinDetailActivity().clearMainNewsDetailStacks();
            getTiinDetailActivity().onBackPressed();
            AppProvider.CLICK_CLOSE = 1;
        }
    }

    public void setVisibilityClose(int count) {
        if (count > 1) {
            ivClose.setVisibility(View.VISIBLE);
        } else {
            ivClose.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mPresenter.onDestroyView();
        mPresenter = null;
    }

    public void loadDataAfterReconnect() {
        if (homeNewsDetailFragment != null) {
            homeNewsDetailFragment.loadData();
        }
//        loadData();
    }
    private void setVisiableLine(int i){
        if( i == 0){
            line2.setVisibility(View.VISIBLE);
        }else if( i == 1){
            line3.setVisibility(View.VISIBLE);
        }else if( i == 2){
            line4.setVisibility(View.VISIBLE);
        }else if( i == 5){
            line5.setVisibility(View.VISIBLE);
        }
    }
}

package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetNotificationRequest extends BaseRequest<WsGetNotificationRequest.Request> {
    public class Request {
        @SerializedName("language")
        public String language;
        @SerializedName("camId")
        public String camId;
        @SerializedName("page")
        public int page;
        @SerializedName("deviceId")
        public String deviceId;

    }
}

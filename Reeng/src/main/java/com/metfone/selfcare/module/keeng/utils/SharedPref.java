package com.metfone.selfcare.module.keeng.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.App;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SharedPref {
    static final String TAG = "SharedPref";
    private Context mContext;
    private SharedPreferences pref;
    private Editor editor;
    private boolean autoCommit;

    public SharedPref(Context context) {
        this.autoCommit = true;
        if (context != null)
            this.mContext = context;
        else
            this.mContext = ApplicationController.self().getApplicationContext();

        pref = mContext.getSharedPreferences(Constants.KEENG_PRE_APPLICATION, Activity.MODE_PRIVATE);
        editor = pref.edit();
    }

    @SuppressLint("CommitPrefEdits")
    public SharedPref(Context context, boolean autoCommit) {
        this.autoCommit = autoCommit;
        if (context != null)
            this.mContext = context;
        else
            this.mContext = ApplicationController.self().getApplicationContext();
        pref = context.getSharedPreferences(Constants.KEENG_PRE_APPLICATION, Activity.MODE_PRIVATE);
        editor = pref.edit();
    }

    // name truyen vao
    @SuppressLint("CommitPrefEdits")
    public SharedPref(Context context, String paramString) {
        this.autoCommit = true;
        if (context != null)
            this.mContext = context;
        else
            this.mContext = App.getInstance().getBaseContext();
        this.pref = this.mContext.getSharedPreferences(paramString, Activity.MODE_PRIVATE);
        this.editor = this.pref.edit();
    }

    public static SharedPref newInstance(Context context) {
        return new SharedPref(context, BuildConfig.APPLICATION_ID);
    }

//    public static SharedPref getSharedApp(Context context) {
//        return new SharedPref(context, PrefContains.SHARED_APPLICATION);
//    }

    // String----------------------------------------------------------------//
    public void putString(String key, String value) {
        editor.putString(key, value);
        if (autoCommit) {
            commit();
        }
    }

    public String getString(String key, String defaultValue) {
        try {
            return pref.getString(key, defaultValue);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return defaultValue;
    }

    // Int------------------------------------------------------------------//
    public void putInt(String key, int value) {
        editor.putInt(key, value);
        if (autoCommit) {
            commit();
        }
    }

    public int getInt(String key, int defaultValue) {
        try {
            return pref.getInt(key, defaultValue);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return defaultValue;
    }

    // Long-----------------------------------------------------------------//
    public void putLong(String key, long value) {
        editor.putLong(key, value);
        if (autoCommit) {
            commit();
        }
    }

    public long getLong(String key, long defaultValue) {
        try {
            return pref.getLong(key, defaultValue);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return defaultValue;
    }

    // Float------------------------------------------------------------------//
    public void putFloat(String key, float value) {
        editor.putFloat(key, value);
        if (autoCommit) {
            commit();
        }
    }

    public float getFloat(String key, float defaultValue) {
        try {
            return pref.getFloat(key, defaultValue);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return defaultValue;
    }

    // Boolean-------------------------------------------------------------//
    public boolean getBoolean(String key, boolean defaultValue) {
        try {
            return pref.getBoolean(key, defaultValue);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return defaultValue;
    }

    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        if (autoCommit) {
            commit();
        }
    }

    // ListString----------------------------------------------------------------//
    public void putListString(String key, ArrayList<String> list) {
        if (!TextUtils.isEmpty(key)) {
            int size = pref.getInt("List#String#Size" + key, 0);
            if (size > 0) {
                editor.remove("List#String#Size" + key);
                for (int i = 0; i < size; i++) {
                    editor.remove("List#String#Item" + i + key);
                }
            }
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    editor.putString("List#String#Item" + i + key, list.get(i));
                }
                editor.putInt("List#String#Size" + key, list.size());
            }
            if (autoCommit)
                commit();
        }
    }

    public ArrayList<String> getListString(String key) {
        ArrayList<String> result = new ArrayList<>();
        if (!TextUtils.isEmpty(key)) {
            int size = pref.getInt("List#String#Size" + key, 0);
            for (int i = 0; i < size; i++)
                result.add(pref.getString("List#String#Item" + i + key, ""));
        }
        return result;
    }

    // ListInt----------------------------------------------------------------//
    public int putListInt(String key, int[] listInt) {
        for (int i = 0; i < listInt.length; i++)
            editor.putInt("List#Int" + i + key, listInt[i]);
        if (autoCommit)
            commit();
        return listInt.length;
    }

    public int[] getListInt(String key, int sizeList, int defaultValue) {
        int[] result = new int[sizeList];
        for (int i = 0; i < sizeList; i++)
            result[i] = pref.getInt("List#Int" + i + key, defaultValue);
        return result;
    }

    // Commnit-------------------------------------------------------------//
    private void commit() {
        if (editor != null) editor.commit();
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }
    // ---------------------------------------------------------------------//

    public void saveListDatas(Object listData, String keyName) {
        Gson gson = new Gson();
        String json = gson.toJson(listData);
        editor.putString(keyName, json);
        if (autoCommit) {
            commit();
        }
    }

    public Object getDataCache(String keyName, Type type) {
        Object listData;
        Gson gson = new Gson();
        String json = pref.getString(keyName, "");
        listData = gson.fromJson(json, type);
        return listData;
    }

    public Object getListDatas(String keyName, Class<?> output) {
        Object listData = null;
        String json = pref.getString(keyName, "");
        if (!TextUtils.isEmpty(json)) {
            try {
                listData = new Gson().fromJson(json, output);
            } catch (JsonSyntaxException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        return listData;
    }

    public void clear() {
        if (pref != null)
            pref.edit().clear().apply();
    }

    public ArrayList getArrayList(String key, Type type) {
        ArrayList list = null;
        try {
            String tmp = pref.getString(key, "");
            if (!TextUtils.isEmpty(tmp)) {
                list = new Gson().fromJson(tmp, type);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public void remove(String key) {
        if (editor != null) {
            editor.remove(key);
            if (autoCommit) commit();
        }
    }
}


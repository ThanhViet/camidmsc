/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class NewsProvisional extends Provisional {

    private ArrayList<NewsModel> data;

    public NewsProvisional() {
    }

    public NewsProvisional(ArrayList<NewsModel> data) {
        this.data = data;
    }

    public ArrayList<NewsModel> getData() {
        return data;
    }

    public void setData(ArrayList<NewsModel> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return Utilities.notEmpty(data) ? 1 : 0;
    }
}

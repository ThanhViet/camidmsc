/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.listener;

import com.metfone.selfcare.module.myviettel.model.DataPackageInfo;

public interface OnMyViettelListener {
    void onClickTopUp();

    void onClickChargingHistory();

    void onClickViettelPlus();

    void onClickShop();

    void onClickLearnMoreSwitchToViettel();

    void onClickMoreDataInfo(DataPackageInfo item);

    void onClickBuyMoreData(DataPackageInfo item);

    void onClickPromotionInfo(DataPackageInfo item);

    void onClickRegisterPromotion(DataPackageInfo item);
}

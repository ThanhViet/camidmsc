package com.metfone.selfcare.module.selfcare.utils;

/**
 * Created by thanhnt72 on 2/15/2019.
 */

public class SCConstants {

    public static class PREFERENCE {
        public static final String SC_KEY_ACCESS_TOKEN = "SC_KEY_ACCESS_TOKEN";
        public static final String SC_REFRESH_TOKEN = "SC_REFRESH_TOKEN";
        public static final String SC_USER_NAME = "SC_USER_NAME";
        public static final String SC_FULL_NAME = "SC_FULL_NAME";
        public static final String SC_AVATAR = "SC_AVATAR";
        public static final String SC_SUB_TYPE = "SC_SUB_TYPE";
        public static final String SC_UUID = "SC_UUID";
        public static final String KEY_FROM_SOURCE = "KEY_FROM_SOURCE";

        public static final String SC_BIRHDAY_TMP = "SC_BIRHDAY_TMP";
        public static final String SC_GENDER_TMP = "SC_GENDER_TMP";
    }

    public static final String SC_CURRENTCY = " Kip";
    public static final String SC_DATA_CURRENTCY = " MB";

    public static final int SC_CALL_CENTER = 109;
    public static final int NUM_SIZE = 10;

    public static final String SC_FANPAGE = "https://www.facebook.com/mytelmyanmar";
    public static final String SC_WEBSITE = "https://www.mytel.com.mm";
    public static final String SC_FAQ = "https://www.mytel.com.mm/support/faqs";

    public static final class SELF_CARE {
        public static final int TAB_HOME = 1;
        public static final int TAB_ACCOUNT_DETAIL = 2;
        public static final int TAB_POSTAGE_DETAIL = 3;
        public static final int TAB_RECHARGE = 4;
        public static final int TAB_PACKAGE_DETAIL = 5;
        public static final int TAB_STORES = 6;
        public static final int TAB_STORES_DETAIL = 7;
        public static final int TAB_POSTAGE_OVERVIEW = 8;
        public static final int TAB_PACKAGE_LIST = 9;
        public static final int TAB_RECOMMENT_PACKAGE = 10;
        public static final int TAB_MORE = 11;
        public static final int TAB_NEWS_DETAIL = 12;
        public static final int TAB_LOYALTY_HOME = 13;
        public static final int TAB_MY_SHARE = 14;
        public static final int TAB_MY_CLAIM = 15;
        public static final int TAB_MY_CREDIT = 16;
        public static final int TAB_TELECOM_REWARD = 17;
        public static final int TAB_POINT_HISTORY = 18;
        public static final int TAB_MY_SHARE_OTP = 19;
    }

    public static final class KEY_DATA {
        public static final String PACKAGE_DATA = "PACKAGE_DATA";
        public static final String STORE_DATA = "STORE_DATA";
        public static final String START_DATE = "START_DATE";
        public static final String END_DATE = "END_DATE";
        public static final String POST_TYPE = "POST_TYPE";
        public static final String FEE = "FEE";
        public static final String COUNT = "COUNT";
        public static final String TYPE = "TYPE";
    }

    public static final class POST_TYPE {
        public static final int CALL = 0;
        public static final int SMS = 1;
        public static final int OTHER = 2;
        public static final int DATA = 3;
        public static final int VAS = 4;
    }

    public static final class SCREEN_TYPE {
        public final static int TYPE_PACKAGE = 0;
        public final static int TYPE_SERVICES = 1;
        public final static int TYPE_UTILITIES = 2;

        public final static int TYPE_TELECOM_DATA = 4;
        public final static int TYPE_TELECOM_SMS = 4;
        public final static int TYPE_TELECOM_VOICE = 4;
    }

    public static final class TELECOM_REWARD_TYPE {
        public final static String TYPE_TELECOM_DATA = "DATA";
        public final static String TYPE_TELECOM_SMS = "SMS";
        public final static String TYPE_TELECOM_VOICE = "VOICE";
        public final static String TYPE_TELECOM_MMK = "MMK";
    }

    public static final class RANK_TYPE {
        public final static String WELCOME = "WELCOME";
        public final static String SILVER = "SILVER";
        public final static String GOLD = "GOLD";
        public final static String PLATINUM = "PLATINUM";
        public final static String DIAMOND = "DIAMOND";
    }

    public static final class POINT_HISTORY_TYPE {
        public final static String EARN = "EARN";
        public final static String SPENT = "BURN";
    }

    public static final class DEEPLINK {
        public final static String ALL_PACKAGE = "mytel://selfcare/packages?ref=95";
        public final static String ALL_VAS = "mytel://selfcare/vas?ref=95";
        public final static String CHARGING_HISTORY = "mytel://selfcare/charginghistory?ref=95";
        public final static String STORE_CENTER = "mytel://findstores?ref=95";
        public final static String MY_SHARE = "mytel://myshare?ref=95";
        public final static String MY_CLAIM = "mytel://myclaim?ref=95";
        public final static String MY_CREDIT = "mytel://mycredit?ref=95";
    }

    public static final class UMONEY{
        public final static int TAB_CHECK_REGISTER = 1;
        public final static int TAB_REGISTER = 2;
        public final static int TAB_HOME = 3;
        public final static int TAB_MOBILE = 4;
        public final static int TAB_COMPLETE = 5;

        public final static String KEY_CODE_INFO = "code_info";
        public final static String KEY_DESC_INFO = "desc_info";
    }
}

package com.metfone.selfcare.module.netnews.EventNews.view;

import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.EventResponse;

/**
 * Created by HaiKE on 8/18/17.
 */

public interface IEventView extends MvpView {

    void loadDataSuccess(boolean flag);

    void bindData(EventResponse response);
}

package com.metfone.selfcare.module.tiin.network.api;

public final class ApiEndPoints {
//    public static final String URL = "http://apitest.tiin.vn:80/tiinapi/mocha/";
    public static final String URL = "/tiinapi/mocha/";
//    public static final String URL = "mctiin.api.tiin.vn:80/tiinapi/mocha/";
    public static final String GET_HOME = URL +"homepage";
    public static final String GET_CATEGORY_NEW = URL +"latestNews";
    public static final String GET_CATEGORY = URL +"category";
    public static final String GET_DETAIL_NEW = URL +"article";
    public static final String GET_RELATE = URL +"related";
    public static final String GET_SIBLINGS = URL +"siblings";
    public static final String GET_CATEGORY_EVENT = URL +"hotTopic";
    public static final String GET_SEARCH_TIIN = URL + "search";
    public static final String GET_LIST_EVENT = URL + "hottopicarticles.aspx";
    public static final String GET_MOST_VIEW = URL + "mostview";
    public static final String GET_CONFIG_CATEGORY = URL + "settings/categories";
    public static final String GET_RELATE_EVENT = URL +"thematic";
}

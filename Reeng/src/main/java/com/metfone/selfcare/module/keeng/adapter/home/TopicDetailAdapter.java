package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;

import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.holder.ChildCategoryHomeHolder;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class TopicDetailAdapter extends BaseAdapterRecyclerView {

    List<Topic> datas;

    public TopicDetailAdapter(Context context, List<Topic> datas, String ga_source) {
        super(context, ga_source);
        this.datas = datas;
    }

    @Override
    public int getItemCount() {
        if (datas != null)
            return datas.size();
        return 0;
    }

    @Override
    public Topic getItem(int position) {
        try {
            return datas.get(position);
        } catch (IndexOutOfBoundsException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (holder instanceof ChildCategoryHomeHolder) {
            ChildCategoryHomeHolder itemHolder = (ChildCategoryHomeHolder) holder;
            Topic item = getItem(position);
            itemHolder.tvTitle.setText(item.getName());
            ImageBusiness.setCoverTopic(item.getCover(), itemHolder.image, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Topic item = getItem(position);
        if (item == null) {
            return ITEM_LOAD_MORE;
        }
        return ITEM_TOPIC;
    }

}

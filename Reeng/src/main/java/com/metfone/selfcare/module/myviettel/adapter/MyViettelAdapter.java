/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.holder.HeaderProfileHolder;
import com.metfone.selfcare.module.myviettel.holder.MoreDataHolder;
import com.metfone.selfcare.module.myviettel.holder.PromotionHolder;
import com.metfone.selfcare.module.myviettel.holder.UtilitiesHolder;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;
import com.metfone.selfcare.module.myviettel.model.AccountInfo;
import com.metfone.selfcare.module.myviettel.model.MoreDataProvisional;
import com.metfone.selfcare.module.myviettel.model.PromotionProvisional;
import com.metfone.selfcare.module.myviettel.model.UtilitiesProvisional;

public class MyViettelAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    public static final int TYPE_HEADER_PROFILE = 1;
    public static final int TYPE_MORE_DATA = 2;
    public static final int TYPE_UTILITIES = 3;
    public static final int TYPE_PROMOTION = 4;

    private OnMyViettelListener listener;

    public MyViettelAdapter(Activity activity) {
        super(activity);
    }

    public void setListener(OnMyViettelListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER_PROFILE:
                return new HeaderProfileHolder(layoutInflater.inflate(R.layout.holder_header_profile_mvt, parent, false), listener);
            case TYPE_MORE_DATA:
                return new MoreDataHolder(activity, layoutInflater.inflate(R.layout.holder_more_data_mvt, parent, false), listener);
            case TYPE_UTILITIES:
                return new UtilitiesHolder(layoutInflater.inflate(R.layout.holder_utilities_mvt, parent, false), listener);
            case TYPE_PROMOTION:
                return new PromotionHolder(activity, layoutInflater.inflate(R.layout.holder_promotion_mvt, parent, false), listener);
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(getItem(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof AccountInfo) return TYPE_HEADER_PROFILE;
        if (item instanceof MoreDataProvisional) return TYPE_MORE_DATA;
        if (item instanceof UtilitiesProvisional) return TYPE_UTILITIES;
        if (item instanceof PromotionProvisional) return TYPE_PROMOTION;
        return 0;
    }
}

package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCStore;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCStore extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCStore> data;

    public ArrayList<SCStore> getData() {
        return data;
    }

    public void setData(ArrayList<SCStore> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCStore [data=" + data + "] errror " + getErrorCode();
    }
}

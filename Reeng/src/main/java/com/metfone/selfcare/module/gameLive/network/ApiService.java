package com.metfone.selfcare.module.gameLive.network;

import com.metfone.selfcare.module.gameLive.network.parse.AbsResultData;
import com.metfone.selfcare.module.gameLive.network.parse.RestGameInfo;
import com.metfone.selfcare.module.gameLive.network.parse.RestGameResult;
import com.metfone.selfcare.module.gameLive.network.parse.RestWinner;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @GET("gameTH/mocha/getdetailgame")
    Call<RestGameInfo> getGameInfo(@Header("mocha-api") String mochaApi, @Query("msisdn") String msisdn, @Query("domain") String domain
            , @Query("timestamp") long timestamp, @Query("token") String token, @Query("security") String security);

    @GET("gameTH/mocha/checkresult")
    Call<RestGameResult> checkResult(@Header("mocha-api") String mochaApi, @Query("msisdn") String msisdn, @Query("domain") String domain
            , @Query("questionId") String questionId, @Query("gameId") String gameId
            , @Query("timestamp") long timestamp, @Query("token") String token, @Query("security") String security);

    //    @Headers("Content-Type: application/json")
    @POST("gameTH/mocha/answer")
    Call<AbsResultData> postAnswer(@Header("mocha-api") String mochaApi, @Query("msisdn") String msisdn, @Query("domain") String domain
            , @Query("questionId") String questionId, @Query("is_true") int is_true
            , @Query("answer") String answer, @Query("gameId") String gameId
            , @Query("timestamp") long timestamp, @Query("token") String token, @Query("security") String security);

    //    @Headers("Content-Type: application/json")
    @POST("gameTH/mocha/numberlucky")
    Call<AbsResultData> postNumberLucky(@Header("mocha-api") String mochaApi, @Query("msisdn") String msisdn, @Query("domain") String domain
            , @Query("numberlucky") String numberlucky, @Query("gameId") String gameId
            , @Query("timestamp") long timestamp, @Query("token") String token, @Query("security") String security);

    //    @Headers("Content-Type: application/json")
    @GET("gameTH/mocha/getlistuserwin")
    Call<RestWinner> getListWinners(@Header("mocha-api") String mochaApi, @Query("msisdn") String msisdn, @Query("domain") String domain
            , @Query("questionId") String questionId, @Query("questionNumber") String questionNumber, @Query("gameId") String gameId
            , @Query("timestamp") long timestamp, @Query("token") String token, @Query("security") String security);
}

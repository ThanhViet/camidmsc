/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.content.MovieDetailHolder;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.movienew.holder.MovieContinueHolder;
import com.metfone.selfcare.module.movienew.holder.MovieHolder;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;
import com.metfone.selfcare.util.Log;

public class CategoryDetailAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {

    private static final int TYPE_NORMAL = 1;
    private static final int TYPE_MOVIE_LIKED = 2;
    private static final int TYPE_MOVIE_WATCHED = 3;
    private TabMovieListener.OnAdapterClick listener;
    private int widthItem;
    private SubtabInfo tabInfo;
    private int parentViewType;
    private String blockName;

    public CategoryDetailAdapter(Activity act, int viewType) {
        super(act);
        this.parentViewType = viewType;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public void setTabInfo(SubtabInfo tabInfo) {
        this.tabInfo = tabInfo;
    }

    public void setWidthItem(int widthItem) {
        this.widthItem = widthItem;
    }

    public void setListener(TabMovieListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof HomeData) {
            if (tabInfo != null) {
                if (MovieKind.CATEGORYID_GET_LIKED_ODD.equals(tabInfo.getCategoryId())) {
                    return TYPE_MOVIE_LIKED;
                } else if (MovieKind.CATEGORYID_GET_LIKED_SERIES.equals(tabInfo.getCategoryId())) {
                    return TYPE_MOVIE_LIKED;
                } else if (MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId())) {
                    return TYPE_MOVIE_WATCHED;
                }
            }
            return TYPE_NORMAL;
        } else if (item instanceof MovieWatched) {
            return TYPE_MOVIE_WATCHED;
        } else  if (item instanceof ListLogWatchResponse.Result) {
            return TYPE_NORMAL;
        }
        return TYPE_EMPTY;
    }

    @Override
    public int getItemCount() {
        if (items != null && parentViewType == MoviePagerModel.TYPE_BOX_WATCHED) {
            return items.size() > 9 ? 9 : items.size();
        }
        return super.getItemCount();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case TYPE_NORMAL:
                if(parentViewType == MoviePagerModel.TYPE_BOX_WATCHED) {
                    return new MovieContinueHolder(layoutInflater.inflate(R.layout.cinema_continue_item, parent, false), activity, listener);
                } else if(parentViewType == MoviePagerModel.TYPE_CATEGORY) {
                    return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_grid_movie, parent, false), activity, listener, widthItem).setShowPoster(true);
                } else {
                    return new MovieHolder(layoutInflater.inflate(R.layout.cinema_movie_item, parent, false), activity, listener,blockName);
                }
            case TYPE_MOVIE_LIKED:
                return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_grid_movie_liked, parent, false), activity, listener, widthItem)
                        .setCateIdSubTab(tabInfo != null ? tabInfo.getCategoryId() : null);
            case TYPE_MOVIE_WATCHED:
                return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_grid_movie_watched, parent, false), activity, listener, widthItem);
        }

        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

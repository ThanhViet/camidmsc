package com.metfone.selfcare.module.keeng.fragment.player;

import android.os.Bundle;

import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.util.Utilities;

import static com.metfone.selfcare.module.keeng.KeengPlayerActivity.RATIO_SCALE;

public class HomePlayerFragment extends BaseFragment {

    private ImageView imvCover;
    private ImageView imvDocQuyen;
    private TextView tvName;
    private TextView tvSinger;
    private TextView tvListenNo;
    private MediaModel mediaModel;

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_home_player;
    }

    public static HomePlayerFragment newInstance() {
        return new HomePlayerFragment();
    }

    public static HomePlayerFragment newInstance(Bundle bundle) {
        HomePlayerFragment fragment = new HomePlayerFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mediaModel = (MediaModel) bundle.getSerializable(CommonUtils.KEY_SONG);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        imvCover = view.findViewById(R.id.imvCover);
        imvDocQuyen = view.findViewById(R.id.imvDocQuyen);
        tvName = view.findViewById(R.id.tvName);
        tvSinger = view.findViewById(R.id.tvSinger);
        tvListenNo = view.findViewById(R.id.tvListenNo);
        updateData();
        return view;
    }

    public void updateData() {
        if (mediaModel == null) return;
        ImageBusiness.setSongTwoPlayer(imvCover, mediaModel.getImage310(), Utilities.dpToPx(20));
        scaleImage(1 - RATIO_SCALE);
        tvName.setText(mediaModel.getName());
        tvName.setSelected(true);
        tvSinger.setText(mediaModel.getSinger());
        tvListenNo.setText(tvListenNo.getContext().getString(R.string.m_listens_no, mediaModel.getListenNo()));
        if (mediaModel.isMonopoly())
            imvDocQuyen.setVisibility(View.VISIBLE);
        else
            imvDocQuyen.setVisibility(View.GONE);
    }

    public void scaleImage(float scale) {
        if (imvCover == null) return;
        imvCover.setScaleY(scale);
        imvCover.setScaleX(scale);
        imvCover.invalidate();
    }
}

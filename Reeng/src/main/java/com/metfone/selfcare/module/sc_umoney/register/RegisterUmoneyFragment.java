package com.metfone.selfcare.module.sc_umoney.register;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.sc_umoney.UMoneyStateProvider;
import com.metfone.selfcare.module.sc_umoney.main_umoney.TabUmoneyActivity;
import com.metfone.selfcare.module.sc_umoney.network.UMoneyApi;
import com.metfone.selfcare.module.sc_umoney.network.model.Info;
import com.metfone.selfcare.module.sc_umoney.network.request.RegisterRequest;
import com.metfone.selfcare.module.sc_umoney.network.response.FieldMapResponse;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterUmoneyFragment extends Fragment {
    TextView tvRegister;
    TabUmoneyActivity activity;
    Spinner viewType;
    TextView tvDate, tvSdt;
    EditText edtFullName, edtPaperNum;
    RadioButton rbMale, rbFemale;
    LinearLayout llFrame;

    ReengAccount mAccount;
    UMoneyApi uMoneyApi;
    Info infoModel;
    String birthDay = "";
    private int curentType = 0;
    private int currentYear = 0, selectYear = 0;
    private boolean checkFullName = false, checkCMND = false;
    List<String> listPaperType = new ArrayList<String>();
    final Calendar myCalendar = Calendar.getInstance();
    SimpleDateFormat formatShow = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat formatToServer = new SimpleDateFormat("ddMMyyyy");
    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            selectYear = year;
            updateLabel();
        }

    };

    public static RegisterUmoneyFragment newInstance() {
        Bundle args = new Bundle();
        RegisterUmoneyFragment fragment = new RegisterUmoneyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sc_umoney_register, container, false);
        activity = (TabUmoneyActivity) getActivity();
        initView(view);
        loadData();
        initEvent();
        return view;
    }

    private void initView(View view) {
        tvRegister = view.findViewById(R.id.tv_register);
        viewType = view.findViewById(R.id.spinner_type);
        tvDate = view.findViewById(R.id.edt_date);
        tvSdt = view.findViewById(R.id.tv_sdt);
        edtFullName = view.findViewById(R.id.edt_fullname);
        rbMale = view.findViewById(R.id.rb_male);
        rbFemale = view.findViewById(R.id.rb_female);
        edtPaperNum = view.findViewById(R.id.paper_num);
        llFrame = view.findViewById(R.id.ll_frame);
    }

    private void loadData() {
        uMoneyApi = new UMoneyApi(ApplicationController.self());
        mAccount = ApplicationController.self().getReengAccountBusiness().getCurrentAccount();
        if (mAccount != null) {
            tvSdt.setText(mAccount.getJiNumberLao());
            edtFullName.setText(mAccount.getName());
            try {
                String birth = formatShow.format(myFormat.parse(mAccount.getBirthdayString()));
                tvDate.setText(birth);
                birthDay = formatToServer.format(formatShow.parse(birth));
                selectYear = Integer.parseInt(formatYear.format(formatShow.parse(birth)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (mAccount.getGender() == Constants.CONTACT.GENDER_MALE) {
                rbMale.setChecked(true);
            } else {
                rbFemale.setChecked(true);
            }
        }
        List<String> listType = new ArrayList<String>();
        listType.add(activity.getResources().getString(R.string.u_Identity_Card));
        listType.add(activity.getResources().getString(R.string.u_Passport));
        listType.add(activity.getResources().getString(R.string.u_Family_Book));
        listType.add(activity.getResources().getString(R.string.u_Others));
        listPaperType.add("PPID");
        listPaperType.add("PPRT");
        listPaperType.add("FABO");
        listPaperType.add("OTHE");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity,
                R.layout.item_sc_umoney_spinner, listType);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        viewType.setAdapter(dataAdapter);
        //todo load telecom info
        infoModel = UMoneyStateProvider.getInstance().getInfo();

    }

    private void initEvent() {
        tvRegister.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (!checkInfo()) {
                    return;
                }
                RegisterRequest request = new RegisterRequest();
                request.setServiceCode(1); //vi dien tu mac dinh la 1
                request.setName(edtFullName == null ? "" : edtFullName.getText().toString().trim());
                request.setBirthday(birthDay);
                if (rbMale != null && rbMale.isChecked()) {
                    request.setGender(1);
                } else {
                    request.setGender(0);
                }
                request.setPaperType(listPaperType.get(curentType));
                request.setPaperNum(edtPaperNum == null ? "" : edtPaperNum.getText().toString().trim());
                request.setTransDesc(infoModel);
                uMoneyApi.registerUmoney(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        Gson gson = new Gson();
                        FieldMapResponse response = gson.fromJson(data, FieldMapResponse.class);
                        if (response != null) {
                            if (response.getCode() == 200) {
                                UMoneyStateProvider.getInstance().setData(response.getData().getFieldMap());
                                showDialogConfirm();
//                            } else if (response.getCode() == 10103) {
//                                Toast.makeText(activity, activity.getString(R.string.u_10103), Toast.LENGTH_LONG).show();
//                            } else if(response.getCode() == 10102){
//                                Toast.makeText(activity, activity.getString(R.string.u_10103), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(ApplicationController.self(), response.getDesc(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Toast.makeText(ApplicationController.self(), activity.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
                    }
                }, request);
            }
        });
        viewType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                curentType = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tvDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(activity, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        InputMethodUtils.hideKeyboardWhenTouch(llFrame, activity);
        edtFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 1) {
                    edtFullName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//                    checkFullName = true;
//                    if (checkCMND) {
//                        tvRegister.setEnabled(true);
//                    } else {
//                        tvRegister.setEnabled(true);
//                    }
                }
//                else {
//                    checkFullName = false;
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtPaperNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 1) {
                    edtPaperNum.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//                    checkCMND = true;
//                    if (checkFullName) {
//                        tvRegister.setEnabled(true);
//                    } else {
//                        tvRegister.setEnabled(true);
//                    }
                }
//                else {
//                    checkCMND = false;
//                    tvRegister.setEnabled(true);
//                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtFullName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtFullName.setSelected(false);
            }
        });
        edtPaperNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtPaperNum.setSelected(false);
            }
        });
    }

    private void updateLabel() {
        tvDate.setText(formatShow.format(myCalendar.getTime()));
        String myFormat1 = "ddMMyyyy"; //In which you need put here
        SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);
        birthDay = sdf1.format(myCalendar.getTime());
    }

    private boolean checkInfo() {
        long time = System.currentTimeMillis();
        currentYear = Integer.parseInt(formatYear.format(time));
        if (currentYear < selectYear || (currentYear - selectYear) < 16) {
            Toast.makeText(ApplicationController.self(), activity.getString(R.string.u_error_year), Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(edtFullName.getText().toString().trim())) {
            edtFullName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_validate, 0);
            edtFullName.setSelected(true);
            edtFullName.requestFocus();
            Toast.makeText(ApplicationController.self(), activity.getString(R.string.u_error_name_validate), Toast.LENGTH_LONG).show();
            return false;
        } else {
            //todo check co ky tu dac biet se show
            if (checkKyTu(edtFullName.getText().toString().trim())) {
                edtFullName.requestFocus();
                edtFullName.setSelected(true);
                edtFullName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_validate, 0);
                return false;
            }
        }
        if (TextUtils.isEmpty(edtPaperNum.getText())) {
            edtPaperNum.requestFocus();
            edtPaperNum.setSelected(true);
            edtPaperNum.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_validate, 0);
            Toast.makeText(ApplicationController.self(), activity.getString(R.string.u_error_idcard_validate), Toast.LENGTH_LONG).show();
            return false;
        } else {
            if (checkKyTu(edtPaperNum.getText().toString().trim())) {
                edtPaperNum.requestFocus();
                edtPaperNum.setSelected(true);
                edtPaperNum.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_validate, 0);
                return false;
            }
        }
        return true;
    }

    private boolean checkKyTu(String result) {
        Pattern regex = Pattern.compile("[$&+,:;=?@#|_%^!*()></'.~]");
        Matcher matcher = regex.matcher(result);
        return matcher.find();
    }

    private void showDialogConfirm() {
        DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
        dialogConfirm.setLabel(activity.getString(R.string.Information_Umoney));
        dialogConfirm.setMessage(activity.getString(R.string.Please_check_SMS_for_details));
        dialogConfirm.setNegativeLabel("OK");
        dialogConfirm.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                activity.finish();
            }
        });
        dialogConfirm.show();
    }
}

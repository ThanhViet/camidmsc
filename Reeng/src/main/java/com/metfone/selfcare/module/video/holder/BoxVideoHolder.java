/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.video.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.video.adapter.BoxVideoDetailAdapter;
import com.metfone.selfcare.module.video.listener.TabVideoListener;
import com.metfone.selfcare.module.video.model.VideoPagerModel;

import java.util.ArrayList;

import butterknife.BindView;

public class BoxVideoHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.line)
    @Nullable
    View line;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;
    @BindView(R.id.tv_view_all)
    @Nullable
    TextView btnMore;

    private ArrayList<Object> list;
    private BoxVideoDetailAdapter adapter;
    private VideoPagerModel data;
    private int position;

    public BoxVideoHolder(View view, Activity activity, final TabVideoListener.OnAdapterClick listener) {
        super(view);
        list = new ArrayList<>();
        adapter = new BoxVideoDetailAdapter(activity);
        adapter.setListener(listener);
        adapter.setItems(list);
        BaseAdapter.setupHorizontalRecyclerV5(activity, recyclerView, null, adapter, true);
        if (tvTitle != null) tvTitle.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickTitleBox(data, position);
                }
            }
        });
        if (btnMore != null) btnMore.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickTitleBox(data, position);
                }
            }
        });
    }

    @Override
    public void bindData(Object item, int position) {
        this.position = position;
        if (item instanceof VideoPagerModel) {
            if (list == null) list = new ArrayList<>();
            else list.clear();
            data = (VideoPagerModel) item;
            list.addAll(data.getList());
            if (tvTitle != null) tvTitle.setText(data.getTitle());
            if (adapter != null) {
                adapter.setParentViewType(data.getType());
                adapter.notifyDataSetChanged();
            }
            if (btnMore != null)
                btnMore.setVisibility(data.isShowViewAll() ? View.VISIBLE : View.GONE);
        } else {
            data = null;
        }
    }

}

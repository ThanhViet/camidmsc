package com.metfone.selfcare.module.gameLive.network.parse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RestGameResult extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "RestGameResult{" +
                "result='" + result + '\'' +
                '}';
    }
}

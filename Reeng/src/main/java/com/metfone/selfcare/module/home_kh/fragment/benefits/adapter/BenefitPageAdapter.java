package com.metfone.selfcare.module.home_kh.fragment.benefits.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.metfone.selfcare.module.home_kh.fragment.benefits.BenefitPageKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.reward.GiftActiveKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.reward.GiftPastKHFragment;
import com.metfone.selfcare.module.home_kh.model.RankDefine;

import java.util.ArrayList;
import java.util.List;

public class BenefitPageAdapter extends FragmentStatePagerAdapter {
    List<RankDefine> listRank = new ArrayList<>();

    public BenefitPageAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    public void setData(List<RankDefine> listRank) {
        this.listRank = listRank;
    }

    @Override
    public int getCount() {
        return listRank.size();
    }

    @Override
    public Fragment getItem(int position) {
        return BenefitPageKHFragment.newInstance(listRank.get(position));
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return listRank.get(position).getRankName();
    }
}

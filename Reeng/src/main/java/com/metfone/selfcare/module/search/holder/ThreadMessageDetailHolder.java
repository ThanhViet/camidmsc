/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/5
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Utilities;

import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ThreadMessageDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.tv_last_time)
    @Nullable
    TextView tvLastTime;
    @BindView(R.id.thread_avatar)
    @Nullable
    RoundedImageView ivThreadAvatar;
    @BindView(R.id.rlAvatarGroup)
    @Nullable
    View viewAvatarGroup;
    @BindView(R.id.contact_avatar_text)
    @Nullable
    TextView tvAvatar;
    @BindView(R.id.button_action_video)
    @Nullable
    View btnActionVideo;
    @BindView(R.id.button_action_call)
    @Nullable
    View btnActionCall;
    @BindView(R.id.button_action_label)
    @Nullable
    TextView btnActionLabel;

    private SearchAllListener.OnAdapterClick listener;
    private Object data;
    private ApplicationController mApplication;
    private int sizeAvatar;
    //private boolean showCallAVNO = false;
    private boolean isUserViettel = false;

    public ThreadMessageDetailHolder(View view, final Activity activity, SearchAllListener.OnAdapterClick listener) {
        super(view);
        this.listener = listener;
        mApplication = (ApplicationController) activity.getApplication();
        sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
        if (mApplication.getReengAccountBusiness().getCurrentAccount() != null) {
            isUserViettel = mApplication.getReengAccountBusiness().isViettel();
        }
        if (btnActionVideo != null) {
            btnActionVideo.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (mApplication != null && activity instanceof BaseSlidingFragmentActivity && !activity.isFinishing() && data != null) {
                        if (data instanceof ThreadMessage)
                            mApplication.getCallBusiness().checkAndStartVideoCall((BaseSlidingFragmentActivity) activity, (ThreadMessage) data, false);
                        else if (data instanceof PhoneNumber)
                            mApplication.getCallBusiness().checkAndStartVideoCall((BaseSlidingFragmentActivity) activity, (PhoneNumber) data, false);
                        else if (data instanceof ContactProvisional) {
                            ContactProvisional model = (ContactProvisional) data;
                            if (model.getContact() instanceof ThreadMessage)
                                mApplication.getCallBusiness().checkAndStartVideoCall((BaseSlidingFragmentActivity) activity, (ThreadMessage) model.getContact(), false);
                            else if (model.getContact() instanceof PhoneNumber)
                                mApplication.getCallBusiness().checkAndStartVideoCall((BaseSlidingFragmentActivity) activity, (PhoneNumber) model.getContact(), false);
                        }
                    }
                }
            });
        }
        if (btnActionCall != null) {
            btnActionCall.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (mApplication != null && activity instanceof BaseSlidingFragmentActivity && !activity.isFinishing() && data != null) {
                        if (data instanceof ThreadMessage)
                            mApplication.getCallBusiness().checkAndStartCall((BaseSlidingFragmentActivity) activity, ((ThreadMessage) data).getSoloNumber(), false);
                        else if (data instanceof PhoneNumber)
                            mApplication.getCallBusiness().checkAndStartCall((BaseSlidingFragmentActivity) activity, ((PhoneNumber) data).getJidNumber(), false);
                        else if (data instanceof ContactProvisional) {
                            ContactProvisional model = (ContactProvisional) data;
                            if (model.getContact() instanceof ThreadMessage)
                                mApplication.getCallBusiness().checkAndStartCall((BaseSlidingFragmentActivity) activity, ((ThreadMessage) model.getContact()).getSoloNumber(), false);
                            else if (model.getContact() instanceof PhoneNumber)
                                mApplication.getCallBusiness().checkAndStartCall((BaseSlidingFragmentActivity) activity, ((PhoneNumber) model.getContact()).getJidNumber(), false);
                        }
                    }
                }
            });
        }
        if (btnActionLabel != null) {
            btnActionLabel.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (data instanceof PhoneNumber) {
                        showInviteFriendPopup((PhoneNumber) data, (BaseSlidingFragmentActivity) activity);
                    } else if (data instanceof ContactProvisional) {
                        ContactProvisional model = (ContactProvisional) data;
                        if (model.getContact() instanceof PhoneNumber) {
                            showInviteFriendPopup((PhoneNumber) model.getContact(), (BaseSlidingFragmentActivity) activity);
                        }
                    }
                }
            });
        }
    }

    private void showInviteFriendPopup(PhoneNumber phone, BaseSlidingFragmentActivity activity) {
        InviteFriendHelper.getInstance().showInviteFriendPopup(mApplication, activity,
                phone.getName(), phone.getJidNumber(), false);
    }

    public void bindData(Object item, int position, String keySearch) {
        data = item;
        if (item instanceof ThreadMessage) {
            if (tvTitle != null) {
                tvTitle.setText(((ThreadMessage) item).getThreadName());
            }
            if (tvDesc != null) {
                tvDesc.setVisibility(View.GONE);
                tvDesc.setText("");
            }
            setMessageView((ThreadMessage) item);
        } else if (item instanceof PhoneNumber) {
            if (tvTitle != null) {
                tvTitle.setText(((PhoneNumber) item).getName());
            }
            if (tvDesc != null) {
                tvDesc.setVisibility(View.GONE);
                tvDesc.setText("");
            }
            setContactView((PhoneNumber) item);
        } else if (item instanceof ContactProvisional) {
            ContactProvisional contact = (ContactProvisional) item;
            if (tvTitle != null) {
//                if (contact.getTitleSpannable() != null)
//                    tvTitle.setText(contact.getTitleSpannable());
//                else
                if (!TextUtils.isEmpty(contact.getName()))
                    tvTitle.setText(contact.getName());
                else
                    tvTitle.setText(contact.getTitle());
            }
            if (tvDesc != null) {
                if (contact.getDescriptionSpannable() != null) {
                    tvDesc.setVisibility(View.VISIBLE);
                    tvDesc.setText(contact.getDescriptionSpannable());
                } else if (!TextUtils.isEmpty(contact.getDescription())) {
                    tvDesc.setVisibility(View.VISIBLE);
                    tvDesc.setText(contact.getDescription());
                } else if (contact.getContact() instanceof PhoneNumber) {
                    tvDesc.setVisibility(View.VISIBLE);
                    tvDesc.setText(((PhoneNumber) contact.getContact()).getJidNumber());
                } else if (contact.getContact() instanceof ThreadMessage && !((ThreadMessage) contact.getContact()).isStranger()) {
                    tvDesc.setVisibility(View.VISIBLE);
                    tvDesc.setText(((ThreadMessage) contact.getContact()).getSoloNumber());
                } else {
                    tvDesc.setVisibility(View.GONE);
                    tvDesc.setText("");
                }
            }
            if (contact.getContact() instanceof ThreadMessage) {
                setMessageView((ThreadMessage) contact.getContact());
            } else if (contact.getContact() instanceof PhoneNumber) {
                setContactView((PhoneNumber) contact.getContact());
            }
        } else {
            data = null;
        }

        /* hide some info of item message */
        btnActionCall.setVisibility(View.GONE);
        btnActionLabel.setVisibility(View.GONE);
        btnActionVideo.setVisibility(View.GONE);
        tvDesc.setVisibility(View.GONE);
        tvLastTime.setVisibility(View.GONE);
        tvTitle.setTextColor(Color.WHITE);

    }

    private void setContactView(PhoneNumber phoneNumber) {
        if (ivThreadAvatar != null) ivThreadAvatar.setVisibility(View.VISIBLE);
        if (tvAvatar != null) tvAvatar.setVisibility(View.GONE);
        if (tvLastTime != null) tvLastTime.setVisibility(View.GONE);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(ivThreadAvatar, tvAvatar, phoneNumber, sizeAvatar);
        if ("-1".equals(phoneNumber.getContactId())) {
            if (btnActionVideo != null) btnActionVideo.setVisibility(View.GONE);
            if (btnActionCall != null) btnActionCall.setVisibility(View.GONE);
            if (btnActionLabel != null) btnActionLabel.setVisibility(View.GONE);
        } else {
            if (phoneNumber.isReeng()) {
                if (btnActionVideo != null) btnActionVideo.setVisibility(View.VISIBLE);
                if (btnActionCall != null) btnActionCall.setVisibility(View.VISIBLE);
                if (btnActionLabel != null) btnActionLabel.setVisibility(View.GONE);
            } else if (isUserViettel && phoneNumber.isViettel()) {
                if (btnActionVideo != null) btnActionVideo.setVisibility(View.GONE);
                if (btnActionCall != null) btnActionCall.setVisibility(View.VISIBLE);
                if (btnActionLabel != null) btnActionLabel.setVisibility(View.GONE);
            } else {
                if (btnActionVideo != null) btnActionVideo.setVisibility(View.GONE);
                if (btnActionCall != null) btnActionCall.setVisibility(View.GONE);
                if (btnActionLabel != null) btnActionLabel.setVisibility(View.GONE);
                if (btnActionLabel != null) btnActionLabel.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setMessageView(ThreadMessage threadMessage) {
        int threadType = threadMessage.getThreadType();
        if (ivThreadAvatar != null) ivThreadAvatar.setVisibility(View.VISIBLE);
        if (tvAvatar != null) tvAvatar.setVisibility(View.GONE);
        if (btnActionVideo != null) btnActionVideo.setVisibility(View.GONE);
        if (tvLastTime != null) tvLastTime.setVisibility(View.GONE);
        if (btnActionCall != null) btnActionCall.setVisibility(View.GONE);
        if (btnActionLabel != null) btnActionLabel.setVisibility(View.GONE);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String numberFriend = threadMessage.getSoloNumber();
            PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(numberFriend);
            StrangerPhoneNumber stranger = threadMessage.getStrangerPhoneNumber();
            boolean showCallFree;
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(ivThreadAvatar, tvAvatar, phoneNumber, sizeAvatar);
                showCallFree = phoneNumber.isReeng();
            } else {
                NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(numberFriend);
                showCallFree = nonContact != null && nonContact.isReeng();
                if (threadMessage.isStranger()) {
                    if (stranger != null) {
                        mApplication.getAvatarBusiness().setStrangerAvatar(ivThreadAvatar, tvAvatar,
                                stranger, stranger.getPhoneNumber(), null, null, sizeAvatar);
                    } else {
                        mApplication.getAvatarBusiness().setUnknownNumberAvatar(ivThreadAvatar, tvAvatar, numberFriend, sizeAvatar);
                    }
                } else {
                    mApplication.getAvatarBusiness().setUnknownNumberAvatar(ivThreadAvatar, tvAvatar, numberFriend, sizeAvatar);
                }
            }
            if (!threadMessage.isStranger()) {
                if ((showCallFree && mApplication.getReengAccountBusiness().isCallEnable())
                        || mApplication.getReengAccountBusiness().isEnableCallOut()) {
                    if (btnActionCall != null) btnActionCall.setVisibility(View.VISIBLE);
                    if (!showCallFree) {
                        if (btnActionVideo != null) btnActionVideo.setVisibility(View.GONE);
                    } else if (btnActionVideo != null) btnActionVideo.setVisibility(View.VISIBLE);
                }
            }
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
            mApplication.getAvatarBusiness().setGroupThreadAvatar(ivThreadAvatar, viewAvatarGroup, threadMessage);
            CopyOnWriteArrayList<ReengMessage> allMessages = threadMessage.getAllMessages();
            if (Utilities.notEmpty(allMessages)) {
                ReengMessage lastMessage = allMessages.get(allMessages.size() - 1);
                if (lastMessage != null && tvLastTime != null) {
                    tvLastTime.setVisibility(View.VISIBLE);
                    long timerOfMsg = lastMessage.getTime();
                    long currentTime = new Date().getTime();
                    tvLastTime.setText(TimeHelper.formatCommonTime(timerOfMsg, currentTime, mApplication.getResources()));
                }
            }
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {//chat room
            mApplication.getAvatarBusiness().setOfficialThreadAvatar(ivThreadAvatar, sizeAvatar,
                    threadMessage.getServerId(), null, false);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            if (ivThreadAvatar != null)
                ivThreadAvatar.setImageResource(R.drawable.ic_broadcast_group);
        } else {
            OfficerAccount official = mApplication.getOfficerBusiness().getOfficerAccountByServerId(threadMessage.getServerId());
            mApplication.getAvatarBusiness().setOfficialThreadAvatar(ivThreadAvatar, sizeAvatar,
                    threadMessage.getServerId(), official, false);
        }

    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxThreadChat) {
            ((SearchAllListener.OnClickBoxThreadChat) listener).onClickThreadChatItem(data);
        }
    }
}
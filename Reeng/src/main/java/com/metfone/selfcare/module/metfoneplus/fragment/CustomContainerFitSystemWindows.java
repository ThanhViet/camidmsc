package com.metfone.selfcare.module.metfoneplus.fragment;

import static com.metfone.selfcare.fragment.HomePagerFragment.heightBottomMenu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import org.jetbrains.annotations.NotNull;

public class CustomContainerFitSystemWindows extends RelativeLayout {
    public CustomContainerFitSystemWindows(Context context) {
        super(context);
    }

    public CustomContainerFitSystemWindows(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomContainerFitSystemWindows(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private int[] mInsets = new int[4];


    @Override
    protected final boolean fitSystemWindows(@NotNull Rect insets) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // Intentionally do not modify the bottom inset. For some reason,
            // if the bottom inset is modified, window resizing stops working.
            // TODO: Figure out why.

            mInsets[0] = insets.left;
            mInsets[1] = insets.top;
            mInsets[2] = insets.right;
            mInsets[3] = insets.bottom;

            insets.left = 0;
            insets.top = 0;
            insets.right = 0;

            if (heightBottomMenu < insets.bottom)
            insets.bottom = insets.bottom - heightBottomMenu;
        }

        return super.fitSystemWindows(insets);
    }

}
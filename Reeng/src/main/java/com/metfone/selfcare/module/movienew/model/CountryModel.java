package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parentid")
    @Expose
    private Integer parentid;
    @SerializedName("categoryname")
    @Expose
    private String categoryname;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("categoryid")
    @Expose
    private String categoryid;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("url_images")
    @Expose
    private String urlImages;
    @SerializedName("settop")
    @Expose
    private Integer settop;
    @SerializedName("total_sub_tab")
    @Expose
    private Integer totalSubTab;
    @SerializedName("eventID")
    @Expose
    private Integer eventID;
    @SerializedName("typefilmID")
    @Expose
    private Integer typefilmID;
}

package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;

public class AccountRankInfoResponse {
    @SerializedName("code")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("accountRankDTO")
    @Expose
    AccountRankDTO accountRankDTO;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AccountRankDTO getAccountRankDTO() {
        return accountRankDTO;
    }

    public void setAccountRankDTO(AccountRankDTO accountRankDTO) {
        this.accountRankDTO = accountRankDTO;
    }
}

package com.metfone.selfcare.module.livestream.listener;

import com.metfone.selfcare.module.livestream.model.LiveStreamMessage;

import com.metfone.selfcare.ui.androidtagview.TagGroup;

public interface MessageActionListener {
    void onClickUser(LiveStreamMessage message, int pos);

    void onQuickSendText(String content);

    void onClickShowKeyboard();

    void onReplyMessage(LiveStreamMessage message, int pos);

    void onReplyMessageLevel2(LiveStreamMessage message);

    void onInviteFriend();

    void onShareVideo(boolean shareSocial);

    void onClickLikeMessage(LiveStreamMessage message, int pos);

    void onFollowChannel(LiveStreamMessage message, TagGroup tagGroup);

    void onLikeVideo(TagGroup tagGroup);

    void onSendMessage(LiveStreamMessage message);
}

package com.metfone.selfcare.module.tab_home.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.io.File;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class ImageBusiness extends com.metfone.selfcare.module.keeng.utils.ImageBusiness {
    //todo cái này chỉ nên sử dụng trong module tab_home thôi

//    public static void setPosterMovie(String url, ImageView imageView) {
//        if (imageView == null || url == null) return;
//        try {
//            Glide.with(ApplicationController.self())
//                    .asBitmap()
//                    .load(url)
//                    .transition(withCrossFade(500))
//                    .apply(new RequestOptions()
//                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                            .transforms(new FitCenter()
//                                    , ApplicationController.self().getRoundedCornersTransformation())
//                            .placeholder(R.drawable.df_image_home_poster)
//                            .error(R.drawable.df_image_home_poster)
//                    )
//                    .into(imageView);
//        } catch (OutOfMemoryError e) {
//            Log.e(TAG, e);
//        } catch (IllegalArgumentException e) {
//            Log.e(TAG, e);
//        } catch (Exception e) {
//            Log.e(TAG, e);
//        }
//    }

//    public static void setSlideBanner(String url, ImageView imageView) {
//        if (imageView == null) return;
//        try {
//            if (url == null) url = "";
//            Glide.with(ApplicationController.self())
//                    .asBitmap()
//                    .load(url)
//                    .transition(withCrossFade(500))
//                    .apply(new RequestOptions()
//                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                            .transforms(new CenterCrop()
//                                    , ApplicationController.self().getRoundedCornersTransformation())
//                            .placeholder(R.drawable.df_image_home_21_9)
//                            .error(R.drawable.df_image_home_21_9)
//                    )
//                    .into(imageView);
//        } catch (OutOfMemoryError e) {
//            Log.e(TAG, e);
//        } catch (IllegalArgumentException e) {
//            Log.e(TAG, e);
//        } catch (Exception e) {
//            Log.e(TAG, e);
//        }
//    }

//    public static void setFlashHot(String url, ImageView imageView) {
//        if (imageView == null) return;
//        try {
//            if (url == null) url = "";
//            Glide.with(ApplicationController.self())
//                    .asBitmap()
//                    .load(url)
//                    .transition(withCrossFade(500))
//                    .apply(new RequestOptions()
//                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                            .transforms(new FitCenter())
//                            .placeholder(R.drawable.df_image_home_16_9)
//                            .error(R.drawable.df_image_home_16_9)
//                    )
//                    .into(imageView);
//        } catch (OutOfMemoryError e) {
//            Log.e(TAG, e);
//        } catch (IllegalArgumentException e) {
//            Log.e(TAG, e);
//        } catch (Exception e) {
//            Log.e(TAG, e);
//        }
//    }

//    public static void setVideo(String url, ImageView imageView) {
//        setImageTransform(imageView, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9);
//    }
//
//    public static void setVideo(String url, ImageView imageView, int round) {
//        setImageTransform(imageView, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9, new RoundedCornersTransformation(round, 0));
//    }

    public static void setMusic(String url, ImageView imageView) {
        setImageTransform(imageView, url, R.drawable.df_image_home, R.drawable.df_image_home);
    }

//    private static void deleteFileBackgroundHeaderHome() {
//        SharedPrefs sharedPrefs = SharedPrefs.getInstance(Constants.PREFERENCE.PREF_NAME_BANNER);
//        try {
//            String filePath = sharedPrefs.get(Constants.PREFERENCE.PREF_BG_HEADER_HOME_FILE_PATH, String.class);
//            if (Utilities.notEmpty(filePath)) {
//                File file = new File(filePath);
//                if (file.exists()) {
//                    file.delete();
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        sharedPrefs.remove(Constants.PREFERENCE.PREF_BG_HEADER_HOME_FILE_PATH);
//        sharedPrefs.remove(Constants.PREFERENCE.PREF_BG_HEADER_HOME_URL);
//    }

//    public static synchronized void downloadAndSaveBackgroundHeaderHome() {
//        final ApplicationController application = ApplicationController.self();
//        if (application.isDataReady()) {
//            final String url = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.HOME_BANNER_THEME);
//            Log.i(TAG, "downloadAndSaveBackgroundHeaderHome: " + url);
//            if (TextUtils.isEmpty(url)) {
//                deleteFileBackgroundHeaderHome();
//                return;
//            }
//            if (url.equals(SharedPrefs.getInstance(Constants.PREFERENCE.PREF_NAME_BANNER).get(Constants.PREFERENCE.PREF_BG_HEADER_HOME_URL, String.class))) {
//                Log.i(TAG, "downloadAndSaveBackgroundHeaderHome link anh da duoc down ve truoc do roi");
//                return;
//            }
//            Log.i(TAG, "start downloadAndSaveBackgroundHeaderHome: " + url);
//            try {
//                Glide.with(application).asBitmap().load(url).into(new SimpleTarget<Bitmap>() {
//
//                    @Override
//                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
//                        super.onLoadFailed(errorDrawable);
//                        Log.d(TAG, "downloadAndSaveBackgroundHeaderHome onLoadFailed");
//                        deleteFileBackgroundHeaderHome();
//                    }
//
//                    @Override
//                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                        String filePath = ImageHelper.getBackgroundImagePath("bg_header_home");
//                        Log.d(TAG, "downloadAndSaveBackgroundHeaderHome onResourceReady filePath: " + filePath);
//                        boolean check = ImageHelper.saveBitmapToPath(resource, filePath, Bitmap.CompressFormat.JPEG);
//                        SharedPrefs sharedPrefs = SharedPrefs.getInstance(Constants.PREFERENCE.PREF_NAME_BANNER);
//                        if (check) {
//                            sharedPrefs.put(Constants.PREFERENCE.PREF_BG_HEADER_HOME_FILE_PATH, filePath);
//                            sharedPrefs.put(Constants.PREFERENCE.PREF_BG_HEADER_HOME_URL, url);
//                        } else {
//                            sharedPrefs.remove(Constants.PREFERENCE.PREF_BG_HEADER_HOME_FILE_PATH);
//                            sharedPrefs.remove(Constants.PREFERENCE.PREF_BG_HEADER_HOME_URL);
//                        }
//                        Log.i(TAG, "downloadAndSaveBackgroundHeaderHome check: " + check);
//                    }
//                });
//            } catch (OutOfMemoryError e) {
//                Log.e(TAG, e);
//            } catch (IllegalArgumentException e) {
//                Log.e(TAG, e);
//            } catch (Exception e) {
//                Log.e(TAG, e);
//            }
//        }
//    }

//    public static void setBannerHeader(ImageView imageView) {
//        if (imageView == null) return;
//        SharedPrefs sharedPrefs = SharedPrefs.getInstance(Constants.PREFERENCE.PREF_NAME_BANNER);
//        String filePath = sharedPrefs.get(Constants.PREFERENCE.PREF_BG_HEADER_HOME_FILE_PATH, String.class);
//        Log.i(TAG, "setBannerHeaderHome filePath: " + filePath);
//        if (Utilities.notEmpty(filePath)) {
//            File file = new File(filePath);
//            if (file.exists()) {
//                try {
//                    RequestOptions requestOptions = new RequestOptions()
//                            .error(R.drawable.bg_tab_home_header)
//                            .priority(Priority.HIGH)
//                            .centerCrop();
//                    Glide.with(imageView.getContext())
//                            .asBitmap()
//                            .load(file)
//                            .apply(requestOptions)
//                            .into(imageView);
//                } catch (OutOfMemoryError e) {
//                    Log.e(TAG, e);
//                } catch (IllegalArgumentException e) {
//                    Log.e(TAG, e);
//                } catch (Exception e) {
//                    Log.e(TAG, e);
//                }
//            } else {
//                sharedPrefs.remove(Constants.PREFERENCE.PREF_BG_HEADER_HOME_FILE_PATH);
//                sharedPrefs.remove(Constants.PREFERENCE.PREF_BG_HEADER_HOME_URL);
//                setResource(imageView, R.drawable.bg_tab_home_header);
//                downloadAndSaveBackgroundHeaderHome();
//            }
//        } else {
//            setResource(imageView, R.drawable.bg_tab_home_header);
//        }
//    }

    public static void setFeature(ImageView imageView, String url, int resDef) {
        if (imageView == null) return;
        if (TextUtils.isEmpty(url)) {
            try {
                Context context = imageView.getContext();
                int size = Utilities.dpToPx(90);
                RequestOptions requestOptions = new RequestOptions()
                        //.placeholder(resDef)
                        .error(resDef)
                        .priority(Priority.HIGH)
                        .override(size, size)
                        .dontAnimate()
                        .dontTransform()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .clone();
                Glide.with(context)
                        .asDrawable()
                        .load(resDef)
                        .apply(requestOptions)
                        .into(imageView);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        } else {
            try {
                Context context = imageView.getContext();
                int size = Utilities.dpToPx(90);
                RequestOptions requestOptions = new RequestOptions()
                        //.placeholder(resDef)
                        .error(resDef)
                        .priority(Priority.HIGH)
                        .override(size, size)
                        .dontAnimate()
                        .dontTransform()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .clone();
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(imageView);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public static void downloadImagePreview(final BaseSlidingFragmentActivity activity, final String url) {
        final ApplicationController application = ApplicationController.self();
        if (application.isDataReady()) {
            Log.i(TAG, "start download image: " + url);
            try {
                Glide.with(application).asBitmap().load(url).into(new SimpleTarget<Bitmap>() {

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        Log.d(TAG, "download Image onLoadFailed");
                    }

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        File recordDir = new File(Config.Storage.GALLERY_MOCHA);
                        if (!recordDir.exists()) {
                            recordDir.mkdirs();
                        }
                        String saveFileName = ImageHelper.IMAGE_NAME + FileHelper.getNameFileFromURL(url);
                        if (TextUtils.isEmpty(saveFileName))
                            saveFileName = String.valueOf(System.currentTimeMillis());
                        saveFileName = EncryptUtil.encryptMD5(saveFileName) + ".jpg";
                        String imagePath = Config.Storage.GALLERY_MOCHA + "/" + saveFileName;
                        File saveImage = new File(recordDir, saveFileName);
                        if (saveImage.exists()) {
                            activity.showToast(R.string.file_exist);
                        } else {
                            boolean check = ImageHelper.saveBitmapToPath(resource, imagePath, Bitmap.CompressFormat.JPEG);
                            if (check) {
                                FileHelper.refreshGallery(application, saveImage);
                                activity.showToast(R.string.sticker_download_done);
                                Log.i(TAG, "onResourceReady: Tải thành công path :" + imagePath);
                            } else {
                                Log.i(TAG, "onResourceReady: Tải thất bại");
                                activity.showToast(R.string.e601_error_but_undefined);
                            }
                        }

                    }
                });
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public static void setTopLeftPlaylist(ImageView image, String url) {
        if (image == null) return;
        Transformation transformation = new RoundedCornersTransformation(ApplicationController.self().getRound()
                , 0, RoundedCornersTransformation.CornerType.TOP_LEFT);
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, R.drawable.df_image_home_top_left, transformation);
        } else {
            setImage(image, url, R.drawable.df_image_home_top_left, R.drawable.df_image_home_top_left, transformation);
        }
    }

    public static void setTopRightPlaylist(ImageView image, String url) {
        if (image == null) return;
        Transformation transformation = new RoundedCornersTransformation(ApplicationController.self().getRound()
                , 0, RoundedCornersTransformation.CornerType.TOP_RIGHT);
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, R.drawable.df_image_home_top_right, transformation);
        } else {
            setImage(image, url, R.drawable.df_image_home_top_right, R.drawable.df_image_home_top_right, transformation);
        }
    }

    public static void setBottomLeftPlaylist(ImageView image, String url) {
        if (image == null) return;
        Transformation transformation = new RoundedCornersTransformation(ApplicationController.self().getRound()
                , 0, RoundedCornersTransformation.CornerType.BOTTOM_LEFT);
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, R.drawable.df_image_home_bottom_left, transformation);
        } else {
            setImage(image, url, R.drawable.df_image_home_bottom_left, R.drawable.df_image_home_bottom_left, transformation);
        }
    }

    public static void setBottomRightPlaylist(ImageView image, String url) {
        if (image == null) return;
        Transformation transformation = new RoundedCornersTransformation(ApplicationController.self().getRound()
                , 0, RoundedCornersTransformation.CornerType.BOTTOM_RIGHT);
        if (TextUtils.isEmpty(url)) {
            setResourceTransform(image, R.drawable.df_image_home_bottom_right, transformation);
        } else {
            setImage(image, url, R.drawable.df_image_home_bottom_right, R.drawable.df_image_home_bottom_right, transformation);
        }
    }
}

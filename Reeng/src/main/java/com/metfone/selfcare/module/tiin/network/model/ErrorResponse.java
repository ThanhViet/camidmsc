package com.metfone.selfcare.module.tiin.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.ErrorModel;

import java.io.Serializable;

public class ErrorResponse implements Serializable {
    @SerializedName("error")
    @Expose
    private ErrorModel error;

    public ErrorModel getError() {
        return error;
    }

    public void setError(ErrorModel errorModel) {
        this.error = errorModel;
    }
}

package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.GiftDetail;

public class WsGetPartnerGiftDetailResponse {
    @SerializedName("errorCode")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("userMsg")
    @Expose
    String userMsg;

    @SerializedName("wsResponse")
    @Expose
    GiftDetail giftDetail;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public GiftDetail getGiftDetail() {
        return giftDetail;
    }

    public void setGiftDetail(GiftDetail giftDetail) {
        this.giftDetail = giftDetail;
    }
}

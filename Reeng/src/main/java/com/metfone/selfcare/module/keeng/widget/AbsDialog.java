package com.metfone.selfcare.module.keeng.widget;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;

import com.metfone.selfcare.module.keeng.utils.Utilities;


/**
 * Created by namnh40 on 7/13/2017.
 */

public abstract class AbsDialog extends Dialog {
    protected static final String TAG = AbsDialog.class.getSimpleName();

    public AbsDialog(@NonNull Context context) {
        super(context);
    }

    public AbsDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected AbsDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public abstract String getNameClass();

    @Override
    public void dismiss() {
        Utilities.hideKeyboard(getCurrentFocus(), getContext(), getNameClass());
        super.dismiss();
    }
}

package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCDeeplink implements Serializable {
//"id": "0523d390-b74f-4762-98b0-f0d76811f4d3",
//        "name": "Change Number",
//        "imagineUrl": "https://imagemytel.viettelglobal.net/1547803573311.jpeg",
//        "href": "https://mytel.com.mm"

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("imagineUrl")
    private String imagineUrl;

    @SerializedName("href")
    private String href;

    @SerializedName("deepLink")
    private String deepLink;

    public SCDeeplink(String id, String name, String deepLink) {
        this.id = id;
        this.name = name;
        this.deepLink = deepLink;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagineUrl() {
        return imagineUrl;
    }

    public void setImagineUrl(String imagineUrl) {
        this.imagineUrl = imagineUrl;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public String toString() {
        return "SCDeeplink{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", imagineUrl='" + imagineUrl + '\'' +
                ", href='" + href + '\'' +
                '}';
    }
}

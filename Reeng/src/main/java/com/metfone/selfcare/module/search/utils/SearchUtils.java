package com.metfone.selfcare.module.search.utils;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.View;

import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.search.model.ResultSearchContact;
import com.metfone.selfcare.module.search.model.SearchHistory;
import com.metfone.selfcare.module.search.model.SearchHistoryProvisional;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.metfone.selfcare.database.constant.ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;

public class SearchUtils {
    public static final int TAB_SEARCH_ALL = 1;
    public static final int TAB_SEARCH_CHAT = 2;
    public static final int TAB_SEARCH_VIDEO = 3;
    public static final int TAB_SEARCH_CHANNEL_VIDEO = 4;
    public static final int TAB_SEARCH_MOVIES = 5;
    public static final int TAB_SEARCH_MUSIC = 6;
    public static final int TAB_SEARCH_NEWS = 7;
    public static final int TAB_SEARCH_MV = 8;
    public static final int TAB_SEARCH_TIIN = 9;
    public static final int TAB_SEARCH_REWARD = 10;

    public static final int LIMIT_REQUEST_SEARCH = 20;
    public static final int LIMIT_REQUEST_SEARCH_MOVIE = 100;
    public static final int LIMIT_10_REQUEST_SEARCH = 10;
    //    public static final int NUM_SUGGEST = 10;
//    public static final int NUM_SUGGEST_VERTICAL = 10;
    public static final int MIN_SCORE = 5;
    public static final int MAX_HISTORY_SEARCH = 10;
    public static final int NUM_SHOW_VIEW_ALL = 6;
    public static final int NUM_SHOW_VIEW_ALL_CONTACT = 5;
    public static final int NUM_SHOW_VIEW_ALL_REWARD = 3;

    private static final String TAG = "SearchUtils";
    private static final int MARGIN_DEFAULT = Utilities.dpToPx(10);
    private static final String RESULT_REGEX = "[+|-|&|||!|(|)|{|}|[|]|^|~|*|?|:|\\|/]";
    private static final String PREF_HISTORY_SEARCH_ALL = "PREF_HISTORY_SEARCH_ALL";
    private static final String PREF_HISTORY_SEARCH_ALL_NEW = "PREF_HISTORY_SEARCH_ALL_NEW";
    private static Comparator<SearchModel> sortSearchByListenNo = new Comparator<SearchModel>() {
        @Override
        public int compare(SearchModel lhs, SearchModel rhs) {
            int result = 0;
            long tmp = rhs.listenNo - lhs.listenNo;
            if (tmp > 0)
                result = 1;
            else if (tmp < 0)
                result = -1;
            return result;
        }
    };

    public static void openMusicKeeng(BaseSlidingFragmentActivity activity, AllModel item) {
        if (activity == null || item == null)
            return;
        switch (item.getType()) {
            case Constants.TYPE_SONG: {
                activity.setMediaToPlaySong(item);
            }
            break;
            case Constants.TYPE_ALBUM: {
                activity.gotoAlbumDetail(item);
            }
            break;
            case Constants.TYPE_VIDEO: {
                activity.setMediaToPlayVideo(item);
            }
            break;
            case Constants.TYPE_PLAYLIST: {
                activity.gotoPlaylistDetail(ConvertHelper.convertToPlaylist(item));
            }
            break;
            default:
                Utilities.processOpenLink(ApplicationController.self(), activity, item.getUrl());
                break;
        }
    }

    public static int getWidthPosterMovie() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 3) / 3.4);
    }

    public static int getWidthVideo() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 2) / 2.4);
    }

    public static int getWidthMusicPlaylist() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 3) / 3.4);
    }

    public static int getWidthChannelVideo() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 5) / 4.8);
    }

    public static int getWidthContact() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 4) / 4.1);
    }

    public static int getWidthFeature() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 4) / 4.5);
    }

    public static int getWidthMV() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 2) / 2.2);
    }

    public static void onClickView(final View view) {
        if (view != null) {
            view.setEnabled(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (view != null) view.setEnabled(true);
                }
            }, 600L);
        }
    }

    public static String convertQuery(String keySearch) {
        if (TextUtils.isEmpty(keySearch))
            keySearch = "";
        else {
            try {
                Pattern pattern = Pattern.compile(RESULT_REGEX);
                Matcher matcher = pattern.matcher(keySearch);
                if (matcher.find()) {
                    keySearch = matcher.replaceAll(" ");
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        return keySearch.trim();
    }

    public static String convertText(String text) {
        if (TextUtils.isEmpty(text))
            text = "";
        else {
            text = TextHelper.convert(text);
            try {
                Pattern pattern = Pattern.compile(RESULT_REGEX);
                Matcher matcher = pattern.matcher(text);
                if (matcher.find()) {
                    text = matcher.replaceAll(" ");
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        return text.trim();
    }

    private static List<SearchModel> filterCoincide(List<SearchModel> data) {
        if (data == null)
            return new ArrayList<>();
        int size = data.size();
        if (size > 0) {
            try {
                Map<String, ArrayList<SearchModel>> maps = new HashMap<>();
                for (int i = 0; i < size; i++) {
                    SearchModel item = data.get(i);
                    if (!maps.containsKey(item.getKeyCoincide())) {
                        ArrayList<SearchModel> tmp = new ArrayList<>();
                        tmp.add(item);
                        maps.put(item.getKeyCoincide(), tmp);
                    } else {
                        maps.get(item.getKeyCoincide()).add(item);
                    }
                }
                data.clear();
                for (String key : maps.keySet()) {
                    List<SearchModel> tmp = maps.get(key);
                    if (tmp.size() <= 1)
                        data.addAll(tmp);
                    else {
                        size = tmp.size();
                        boolean check = false;
                        for (int j = 0; j < size; j++) {
                            if (tmp.get(j).isSinger == 1) {
                                check = true;
                                data.add(tmp.get(j));
                            }
                        }
                        if (!check)
                            data.addAll(tmp);
                    }
                }
                maps.clear();
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        return data;
    }

    //todo ---------------------------------  new search helper -----------------------------------
    public static void filterSuggestObject(List list, int maxSize) {
        if (list != null && list.size() > maxSize) {
            list.subList(maxSize, list.size()).clear();
        }
    }

    public static void filterSearchVideo(String keySearch, List<Video> data, boolean isSuggest) {
        if (data != null && !data.isEmpty()) {
            List<Video> correctList = new ArrayList<>();
            List<Video> correctList2 = new ArrayList<>();
            List<Video> similarList1 = new ArrayList<>();
            List<Video> similarList2 = new ArrayList<>();
            List<Video> similarList3 = new ArrayList<>();
            List<Video> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            for (Video item : data) {
                if (item != null) {
                    String keyFullName = item.getTitle().toLowerCase(Locale.US);
                    String keyFullNameTemp = convertText(keyFullName);
                    String keyQueryInfo = item.getDescription().toLowerCase(Locale.US);
                    String keyQueryInfoTemp = convertText(keyQueryInfo);

                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    } else if (keyQueryInfoTemp.contains(keySearchTemp)) {
                        similarList3.add(item);
                    } else {
                        otherList.add(item);
                    }
                }
            }
            data.clear();
            List<Video> list = new ArrayList<>(correctList);
            if (correctList2.size() > 1) {
                Collections.sort(correctList2, new CorrectListComparator<>(keySearch, 1));
            }
            list.addAll(correctList2);
            list.addAll(similarList1);
            if (similarList2.size() > 1) {
                Collections.sort(similarList2, new CorrectListComparator<>(keySearchTemp, 2));
            }
            list.addAll(similarList2);
            if (similarList3.size() > 1) {
                Collections.sort(similarList3, new CorrectListComparator<>(keySearchTemp, 3));
            }
            list.addAll(similarList3);
            list.addAll(otherList);
//            if (isSuggest) {
//                filterSuggestObject(list, NUM_SUGGEST);
//            }
            data.addAll(list);
            correctList.clear();
            correctList2.clear();
            similarList1.clear();
            similarList2.clear();
            similarList3.clear();
            otherList.clear();
            list.clear();
        }
    }

    public static void filterSearchChannel(String keySearch, List<Channel> data, boolean isSuggest) {
        if (data != null && !data.isEmpty()) {
            List<Channel> correctList = new ArrayList<>();
            List<Channel> correctList2 = new ArrayList<>();
            List<Channel> similarList1 = new ArrayList<>();
            List<Channel> similarList2 = new ArrayList<>();
//            List<Channel> similarList3 = new ArrayList<>();
//            List<Channel> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            for (Channel item : data) {
                if (item != null) {
                    String keyFullName = item.getName().toLowerCase(Locale.US);
                    String keyFullNameTemp = convertText(keyFullName);
//                    String keyQueryInfo = item.getDescription().toLowerCase(Locale.US);
//                    String keyQueryInfoTemp = convertText(keyQueryInfo);

                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    }
//                    else if (keyQueryInfoTemp.contains(keySearchTemp)) {
//                        similarList3.add(item);
//                    } else {
//                        otherList.add(item);
//                    }
                }
            }
            data.clear();
//            list.addAll(correctList);
//            list.addAll(correctList2);
//            list.addAll(similarList1);
//            list.addAll(similarList2);
////            list.addAll(similarList3);
////            list.addAll(otherList);
            List<Channel> list = new ArrayList<>(correctList);
            if (correctList2.size() > 1) {
                Collections.sort(correctList2, new CorrectListComparator<>(keySearch, 1));
            }
            list.addAll(correctList2);
            list.addAll(similarList1);
            if (similarList2.size() > 1) {
                Collections.sort(similarList2, new CorrectListComparator<>(keySearchTemp, 2));
            }
            list.addAll(similarList2);
//            if (isSuggest) {
//                filterSuggestObject(list, NUM_SUGGEST);
//            }
            data.addAll(list);
            correctList.clear();
            correctList2.clear();
            similarList1.clear();
            similarList2.clear();
//            similarList3.clear();
//            otherList.clear();
            list.clear();
        }
    }

    public static void filterSearchNews(String keySearch, List<NewsModel> data, boolean isSuggest) {
        if (data != null && !data.isEmpty()) {
            List<NewsModel> correctList = new ArrayList<>();
            List<NewsModel> correctList2 = new ArrayList<>();
            List<NewsModel> similarList1 = new ArrayList<>();
            List<NewsModel> similarList2 = new ArrayList<>();
            List<NewsModel> similarList3 = new ArrayList<>();
            List<NewsModel> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            int size = data.size();
            for (int i = 0; i < size; i++) {
                NewsModel item = data.get(i);
                if (item != null) {
                    String keyFullName = item.getTitle().toLowerCase(Locale.US);
                    String keyFullNameTemp = convertText(keyFullName);
                    String keyQueryInfo = item.getShapo().toLowerCase(Locale.US);
                    String keyQueryInfoTemp = convertText(keyQueryInfo);

                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    } else if (keyQueryInfoTemp.contains(keySearchTemp)) {
                        similarList3.add(item);
                    } else {
                        otherList.add(item);
                    }
                }
            }
            data.clear();
            List<NewsModel> list = new ArrayList<>(correctList);
            if (correctList2.size() > 1) {
                Collections.sort(correctList2, new CorrectListComparator<>(keySearch, 1));
            }
            list.addAll(correctList2);
            list.addAll(similarList1);
            if (similarList2.size() > 1) {
                Collections.sort(similarList2, new CorrectListComparator<>(keySearchTemp, 2));
            }
            list.addAll(similarList2);
            if (similarList3.size() > 1) {
                Collections.sort(similarList3, new CorrectListComparator<>(keySearchTemp, 3));
            }
            list.addAll(similarList3);
            list.addAll(otherList);
//            if (isSuggest) {
//                filterSuggestObject(list, NUM_SUGGEST_VERTICAL);
//            }
            data.addAll(list);
            correctList.clear();
            correctList2.clear();
            similarList1.clear();
            similarList2.clear();
            similarList3.clear();
            otherList.clear();
            list.clear();
        }
    }

    public static void filterSearchMovies(String keySearch, List<Movie> data, boolean isSuggest) {
        if (data != null && !data.isEmpty()) {
            List<Movie> correctList = new ArrayList<>();
            List<Movie> correctList2 = new ArrayList<>();
            List<Movie> similarList1 = new ArrayList<>();
            List<Movie> similarList2 = new ArrayList<>();
            List<Movie> similarList3 = new ArrayList<>();
            List<Movie> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            boolean isCambodia = ApplicationController.self().getReengAccountBusiness().isCambodia();
            for (Movie item : data) {
                if (item != null && (item.getScore() >= MIN_SCORE || isCambodia)) {
                    String keyFullName = item.getName().toLowerCase(Locale.US);
                    String keyFullNameTemp = convertText(keyFullName);
                    String keyQueryInfo = item.getSearchInfo().toLowerCase(Locale.US);
                    String keyQueryInfoTemp = convertText(keyQueryInfo);

                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    } else if (keyQueryInfoTemp.contains(keySearchTemp)) {
                        similarList3.add(item);
                    } else {
                        otherList.add(item);
                    }
                }
            }
            data.clear();
            List<Movie> list = new ArrayList<>(correctList);
            if (correctList2.size() > 1) {
                Collections.sort(correctList2, new CorrectListComparator<>(keySearch, 1));
            }
            list.addAll(correctList2);
            list.addAll(similarList1);
            if (similarList2.size() > 1) {
                Collections.sort(similarList2, new CorrectListComparator<>(keySearchTemp, 2));
            }
            list.addAll(similarList2);
            if (similarList3.size() > 1) {
                Collections.sort(similarList3, new CorrectListComparator<>(keySearchTemp, 3));
            }
            list.addAll(similarList3);
            list.addAll(otherList);
//            if (isSuggest) {
//                filterSuggestObject(list, NUM_SUGGEST);
//            }
            data.addAll(list);
            correctList.clear();
            correctList2.clear();
            similarList1.clear();
            similarList2.clear();
            similarList3.clear();
            otherList.clear();
            list.clear();
        }
    }

    public static void filterSearchMusic(String keySearch, ArrayList<SearchModel> data, boolean isSuggest, boolean isSort) {
        if (data != null && !data.isEmpty()) {
            List<SearchModel> correctList = new ArrayList<>();
            List<SearchModel> correctList2 = new ArrayList<>();
            List<SearchModel> similarList1 = new ArrayList<>();
            List<SearchModel> similarList2 = new ArrayList<>();
            List<SearchModel> similarList3 = new ArrayList<>();
            List<SearchModel> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            for (SearchModel item : data) {
                if (item != null && item.getScore() >= MIN_SCORE) {
                    String keyFullName = item.getFullName().toLowerCase(Locale.US);
                    String keyFullNameTemp = convertText(keyFullName);
                    String keyQueryInfo = item.getSearchInfo().toLowerCase(Locale.US);
                    String keyQueryInfoTemp = convertText(keyQueryInfo);

                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    } else if (keyQueryInfoTemp.contains(keySearchTemp)) {
                        similarList3.add(item);
                    } else {
                        otherList.add(item);
                    }
                }
            }
            data.clear();
            if (isSort) {
                Collections.sort(filterCoincide(correctList), sortSearchByListenNo);
                Collections.sort(filterCoincide(correctList2), sortSearchByListenNo);
                Collections.sort(filterCoincide(similarList1), sortSearchByListenNo);
                Collections.sort(filterCoincide(similarList2), sortSearchByListenNo);
                Collections.sort(filterCoincide(similarList3), sortSearchByListenNo);
            }

            List<SearchModel> list = new ArrayList<>(correctList);
            if (correctList2.size() > 1) {
                Collections.sort(correctList2, new CorrectListComparator<>(keySearch, 1));
            }
            list.addAll(correctList2);
            list.addAll(similarList1);
            if (similarList2.size() > 1) {
                Collections.sort(similarList2, new CorrectListComparator<>(keySearchTemp, 2));
            }
            list.addAll(similarList2);
            if (similarList3.size() > 1) {
                Collections.sort(similarList3, new CorrectListComparator<>(keySearchTemp, 3));
            }
            list.addAll(similarList3);
            list.addAll(otherList);
//            if (isSuggest) {
//                filterSuggestObject(list, NUM_SUGGEST_VERTICAL);
//            }
            data.addAll(list);
            correctList.clear();
            correctList2.clear();
            similarList1.clear();
            similarList2.clear();
            similarList3.clear();
            otherList.clear();
            list.clear();
        }
    }

    public static void filterSearchTiin(String keySearch, List<TiinModel> data, boolean isSuggest) {
        if (data != null && !data.isEmpty()) {
            List<TiinModel> correctList = new ArrayList<>();
            List<TiinModel> correctList2 = new ArrayList<>();
            List<TiinModel> similarList1 = new ArrayList<>();
            List<TiinModel> similarList2 = new ArrayList<>();
            List<TiinModel> similarList3 = new ArrayList<>();
            List<TiinModel> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            int size = data.size();
            for (int i = 0; i < size; i++) {
                TiinModel item = data.get(i);
                if (item != null) {
                    String keyFullName = item.getTitle().toLowerCase(Locale.US);
                    String keyFullNameTemp = convertText(keyFullName);
                    String keyQueryInfo = item.getShapo().toLowerCase(Locale.US);
                    String keyQueryInfoTemp = convertText(keyQueryInfo);

                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    } else if (keyQueryInfoTemp.contains(keySearchTemp)) {
                        similarList3.add(item);
                    } else {
                        otherList.add(item);
                    }
                }
            }
            data.clear();
            List<TiinModel> list = new ArrayList<>(correctList);
            if (correctList2.size() > 1) {
                Collections.sort(correctList2, new CorrectListComparator<>(keySearch, 1));
            }
            list.addAll(correctList2);
            list.addAll(similarList1);
            if (similarList2.size() > 1) {
                Collections.sort(similarList2, new CorrectListComparator<>(keySearchTemp, 2));
            }
            list.addAll(similarList2);
            if (similarList3.size() > 1) {
                Collections.sort(similarList3, new CorrectListComparator<>(keySearchTemp, 3));
            }
            list.addAll(similarList3);
            list.addAll(otherList);
//            if (isSuggest) {
//                filterSuggestObject(list, NUM_SUGGEST_VERTICAL);
//            }
            data.addAll(list);
            correctList.clear();
            correctList2.clear();
            similarList1.clear();
            similarList2.clear();
            similarList3.clear();
            otherList.clear();
            list.clear();
        }
    }

    public static void filterSearchChat(String keySearch, List<Object> data, boolean isSuggest) {
        if (data != null && !data.isEmpty()) {
            List<Object> correctList = new ArrayList<>();
            List<Object> correctList2 = new ArrayList<>();
            List<Object> similarList1 = new ArrayList<>();
            List<Object> similarList2 = new ArrayList<>();
            List<Object> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            for (Object item : data) {
                if (item != null) {
                    String keyFullName = getName(item);
                    String keyFullNameTemp = convertText(keyFullName);
                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    } else {
                        otherList.add(item);
                    }
                }
            }
            data.clear();
            List<Object> list = new ArrayList<>(correctList);
            if (correctList2.size() > 1) {
                Collections.sort(correctList2, new CorrectListComparator<>(keySearch, 1));
            }
            list.addAll(correctList2);
            list.addAll(similarList1);
            if (similarList2.size() > 1) {
                Collections.sort(similarList2, new CorrectListComparator<>(keySearchTemp, 2));
            }
            list.addAll(similarList2);
//            if (isSuggest) {
//                filterSuggestObject(list, NUM_SUGGEST_VERTICAL);
//            }
            list.add(otherList);
            data.addAll(list);
            correctList.clear();
            correctList2.clear();
            similarList1.clear();
            similarList2.clear();
            otherList.clear();
            list.clear();
        }
    }

    public static String getName(Object object) {
        String result = "";
        if (object instanceof Video) {
            result = ((Video) object).getTitle();
        } else if (object instanceof NewsModel) {
            result = ((NewsModel) object).getTitle();
        } else if (object instanceof Movie) {
            result = ((Movie) object).getName();
        } else if (object instanceof SearchModel) {
            result = ((SearchModel) object).getFullName();
        } else if (object instanceof Channel) {
            result = ((Channel) object).getName();
        } else if (object instanceof ThreadMessage) {
            result = ((ThreadMessage) object).getThreadName();
        } else if (object instanceof PhoneNumber) {
            result = ((PhoneNumber) object).getName();
        } else if (object instanceof TiinModel) {
            result = ((TiinModel) object).getTitle();
        }
        if (result == null) result = "";
        return result.trim().toLowerCase();
    }

    public static String getDescription(Object object) {
        String result = "";
        if (object instanceof Video) {
            result = ((Video) object).getDescription();
        } else if (object instanceof NewsModel) {
            result = ((NewsModel) object).getShapo();
        } else if (object instanceof Movie) {
            result = ((Movie) object).getSearchInfo();
        } else if (object instanceof SearchModel) {
            result = ((SearchModel) object).getSearchInfo();
        } else if (object instanceof Channel) {
            result = ((Channel) object).getDescription();
        } else if (object instanceof TiinModel) {
            result = ((NewsModel) object).getShapo();
        }
        if (result == null) result = "";
        return result.trim().toLowerCase();
    }

    public static String getScore(double score) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        return df.format(score) + " - ";
    }

    public static void replaceFragment(final BaseSlidingFragmentActivity activity, final int tabId, final Fragment fragment) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    activity.getSupportFragmentManager().beginTransaction()
                            .disallowAddToBackStack()
                            .setCustomAnimations(R.anim.activity_right_to_left_enter, R.anim.activity_right_to_left_exit)
                            .add(R.id.tab_content, fragment, String.valueOf(tabId))
                            .commitAllowingStateLoss();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static String getKeySearchChat(String keySearch) {
        String result = "";
        if (Utilities.notEmpty(keySearch)) {
            result = keySearch.trim();
            if (result.startsWith("00")) {
                String textReplace = result.replaceFirst("00", "+");
                ApplicationController application = ApplicationController.self();
                Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().getPhoneNumberProtocol(application.getPhoneUtil(), textReplace, application.getReengAccountBusiness().getRegionCode());
                if (phoneNumberProtocol != null) {
                    // lay number jid
                    if (PhoneNumberHelper.getInstant().isValidPhoneNumber(application.getPhoneUtil(), phoneNumberProtocol)) {
                        result = textReplace.trim();
                    }
                }
            }
        }
        return result;
    }

    public static ArrayList<Object> mergeContactsToThreadChat(ArrayList<Object> listChat, ArrayList<PhoneNumber> listContacts) {
        ArrayList<Object> list = new ArrayList<>();
        if (Utilities.notEmpty(listChat)) list.addAll(listChat);
        if (Utilities.notEmpty(listContacts)) {
            ArrayList<PhoneNumber> copyListContacts = new ArrayList<>(listContacts);
            int sizeContacts = copyListContacts.size() - 1;
            for (int i = sizeContacts; i >= 0; i--) {
                PhoneNumber tmpContact = copyListContacts.get(i);
                if (tmpContact == null) {
                    copyListContacts.remove(i);
                    continue;
                }
                if (Utilities.notEmpty(list)) {
                    for (Object tmpChat : list) {
                        if (tmpChat instanceof PhoneNumber) {
                            if (Utilities.notEmpty(tmpContact.getContactId()) && tmpContact.getContactId().equals(((PhoneNumber) tmpChat).getContactId())) {
                                copyListContacts.remove(i);
                                break;
                            } else if (Utilities.notEmpty(tmpContact.getJidNumber()) && tmpContact.getJidNumber().equals(((PhoneNumber) tmpChat).getJidNumber())) {
                                copyListContacts.remove(i);
                                break;
                            }
                        } else if (tmpChat instanceof ThreadMessage) {
                            if (Utilities.notEmpty(tmpContact.getJidNumber()) && tmpContact.getJidNumber().equals(((ThreadMessage) tmpChat).getSoloNumber())) {
                                copyListContacts.remove(i);
                                break;
                            }
                        }
                    }
                }
            }
            if (Utilities.notEmpty(copyListContacts)) list.addAll(copyListContacts);
        }
        return list;
    }

    public static boolean insertHistorySearch(SearchHistory object) {
        if (object != null) {
            String keySearch = object.getKeySearch();
            int type = object.getType();
            if (Utilities.notEmpty(keySearch)) {
                try {
                    SearchHistoryProvisional model = SharedPrefs.getInstance().get(PREF_HISTORY_SEARCH_ALL_NEW, SearchHistoryProvisional.class);
                    if (model == null) model = new SearchHistoryProvisional();
                    if (model.getData() == null) model.setData(new ArrayList<SearchHistory>());
                    int size = model.getData().size() - 1;
                    for (int i = size; i >= 0; i--) {
                        if (i >= MAX_HISTORY_SEARCH) {
                            model.getData().remove(i);
                        } else {
                            String keySearchTmp = model.getData().get(i).getKeySearch();
                            int typeTmp = model.getData().get(i).getType();
                            if (keySearch.equals(keySearchTmp) && type == typeTmp) {
                                model.getData().remove(i);
                            }
                        }
                    }
                    model.getData().add(0, object);
                    SharedPrefs.getInstance().put(PREF_HISTORY_SEARCH_ALL_NEW, model);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public static SearchHistoryProvisional getHistorySearch() {
        return SharedPrefs.getInstance().get(PREF_HISTORY_SEARCH_ALL_NEW, SearchHistoryProvisional.class);
    }

    public static void deleteHistorySearch() {
        SharedPrefs.getInstance().remove(PREF_HISTORY_SEARCH_ALL_NEW);
    }

    public static boolean deleteHistorySearch(SearchHistory object) {
        if (object != null) {
            String keySearch = object.getKeySearch();
            int type = object.getType();
            if (Utilities.notEmpty(keySearch)) {
                try {
                    SearchHistoryProvisional model = SharedPrefs.getInstance().get(PREF_HISTORY_SEARCH_ALL_NEW, SearchHistoryProvisional.class);
                    if (model == null) model = new SearchHistoryProvisional();
                    if (model.getData() == null) model.setData(new ArrayList<SearchHistory>());
                    int size = model.getData().size() - 1;
                    for (int i = size; i >= 0; i--) {
                        if (i >= MAX_HISTORY_SEARCH) {
                            model.getData().remove(i);
                        } else {
                            String keySearchTmp = model.getData().get(i).getKeySearch();
                            int typeTmp = model.getData().get(i).getType();
                            if (keySearch.equals(keySearchTmp) && type == typeTmp) {
                                model.getData().remove(i);
                            }
                        }
                    }
                    SharedPrefs.getInstance().put(PREF_HISTORY_SEARCH_ALL_NEW, model);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public synchronized static ArrayList<String> getListSplitKey(String text) {
        text = text.replaceAll("((?<=[a-z])[A-Z]|[A-Z](?=[a-z]))", " $1");
        text = text.replaceAll(Constants.PATTERN.KEY_SEARCH_REPLACE, " ");
        text = text.replaceAll("\\s{2,}", " ");
        text = text.trim().toLowerCase(Locale.US);
        String[] splitText = text.split(" ");
        return new ArrayList<>(Arrays.asList(splitText));
    }

    public synchronized static ArrayList<String> getListSplitName(String text) {
        text = text.replaceAll("\\s{2,}", " ");
        text = text.trim().toLowerCase(Locale.US);
        return new ArrayList<>(Arrays.asList(text.split(Constants.PATTERN.KEY_SEARCH_REPLACE)));
    }

    public static Comparator getComparatorKeySearchForSearch() {
        return (Comparator<String>) (s1, s2) -> s2.compareToIgnoreCase(s1);
    }

    public static Comparator getComparatorNameForSearch() {
        return (Comparator<String>) (s1, s2) -> s2.compareToIgnoreCase(s1);
    }

    public static Comparator getComparatorContactForSearch() {
        return (Comparator<Object>) (obj1, obj2) -> {
            String name1 = "";
            String name2 = "";
            if (obj1 instanceof ThreadMessage) {
                name1 = ((ThreadMessage) obj1).getThreadName();
            } else if (obj1 instanceof PhoneNumber) {
                name1 = ((PhoneNumber) obj1).getName();
            } else if (obj1 instanceof ContactProvisional) {
                name1 = ((ContactProvisional) obj1).getName();
            }

            if (obj2 instanceof ThreadMessage) {
                name2 = ((ThreadMessage) obj2).getThreadName();
            } else if (obj2 instanceof PhoneNumber) {
                name2 = ((PhoneNumber) obj2).getName();
            } else if (obj2 instanceof ContactProvisional) {
                name2 = ((ContactProvisional) obj2).getName();
            }

            if (name1 == null) name1 = "";
            else name1 = name1.trim().toLowerCase(Locale.US);
            if (name2 == null) name2 = "";
            else name2 = name2.trim().toLowerCase(Locale.US);

            return name1.compareToIgnoreCase(name2);
        };
    }

    public static Comparator getComparatorMessageForSearch() {
        return (Comparator<Object>) (obj1, obj2) -> {
            Long time1 = 0L;
            Long time2 = 0L;
            try {
                if (obj1 instanceof ThreadMessage) {
                    time1 = ((ThreadMessage) obj1).getLastChangeThread();
                } else if (obj1 instanceof ContactProvisional) {
                    if (((ContactProvisional) obj1).getContact() instanceof ThreadMessage) {
                        time1 = ((ThreadMessage) ((ContactProvisional) obj1).getContact()).getLastChangeThread();
                    }
                }

                if (obj2 instanceof ThreadMessage) {
                    time2 = ((ThreadMessage) obj2).getLastChangeThread();
                } else if (obj2 instanceof ContactProvisional) {
                    if (((ContactProvisional) obj2).getContact() instanceof ThreadMessage) {
                        time2 = ((ThreadMessage) ((ContactProvisional) obj2).getContact()).getLastChangeThread();
                    }
                }

                return time2.compareTo(time1);
            } catch (Exception e) {
                return 0;
            }
        };
    }

    //Search trong view share chung
    //Search trong tính năng gửi danh thiếp
    //Search trong tính năng mời tạo nhóm
    //Search trong tính năng: new chat, new call, new group
    //Search trong tính năng invite bạn cài Mocha
    private static ResultSearchContact searchWithText(String keySearch, CopyOnWriteArrayList datas, Comparator comparatorKeySearch
            , Comparator comparatorName, Comparator comparatorMessage, Comparator comparatorContact, long startTime) {
        //todo keySearch là không phải là chữ
        ArrayList<Object> list = new ArrayList<>();
        if (Utilities.notEmpty(datas)) {
            Log.d(TAG, "searchContactsWithText: " + keySearch);
            ArrayList<Object> listResults01 = new ArrayList<>(); //todo tên giống hệt keySearch
            ArrayList<Object> listResults02 = new ArrayList<>(); //todo tên bỏ dấu giống hệt keySearch bỏ dấu
            ArrayList<Object> listResults03 = new ArrayList<>(); //todo thread chat có: tên bắt đầu bằng keySearch hoặc tên bỏ dấu bắt đầu bằng keySearch bỏ dấu hoặc tên đã bỏ dấu bắt đầu phân tách thành các từ đơn, keySearch đã bỏ dấu phân tách thành các từ đơn, tất cả các từ đơn của kết quả phân tách của keySearch đều là ký tự bắt đầu trong danh sách tất cả các từ đơn của kết qủa phân tách tên. Sắp xếp theo thời gian thread chat.
            ArrayList<Object> listResults04 = new ArrayList<>(); //todo danh bạ có: tên bắt đầu bằng keySearch hoặc tên bỏ dấu bắt đầu bằng keySearch bỏ dấu hoặc tên đã bỏ dấu bắt đầu phân tách thành các từ đơn, keySearch đã bỏ dấu phân tách thành các từ đơn, tất cả các từ đơn của kết quả phân tách của keySearch đều là ký tự bắt đầu trong danh sách tất cả các từ đơn của kết qủa phân tách tên. Sắp xếp theo tên abc.
            String keySearchUnmark = TextHelper.convertUnicodeForSearch(keySearch).toLowerCase(Locale.US).trim();
            List<String> listKeySearch = new ArrayList<>(Arrays.asList(keySearchUnmark.split(Constants.PATTERN.KEY_SEARCH_REPLACE)));
            Log.d(TAG, "listKeySearch: " + listKeySearch);
            int sizeKeySearch = listKeySearch.size();
            if (sizeKeySearch > 1 && comparatorKeySearch != null) {
                Collections.sort(listKeySearch, comparatorKeySearch);
                Log.d(TAG, "listKeySearch: " + listKeySearch);
            }
            Map<String, Integer> mapMessages = new HashMap<>();
            ArrayList<ThreadMessage> listMessagesTmp = new ArrayList<>();
            ArrayList<PhoneNumber> listContactsTmp = new ArrayList<>();
            for (Object obj : datas) {
                if (obj instanceof ThreadMessage) {
                    ThreadMessage item = (ThreadMessage) obj;
                    if (item.isJoined()) {
                        String nameForSearch = TextHelper.convertUnicodeForSearch(item.getThreadName()).toLowerCase(Locale.US).trim();
                        if (nameForSearch.contains(keySearchUnmark)) {
                            listMessagesTmp.add(item);
                        } else {
                            boolean check = true;
                            for (String key : listKeySearch) {
                                if (key != null && !nameForSearch.contains(key)) {
                                    check = false;
                                    break;
                                }
                            }
                            if (check) listMessagesTmp.add(item);
                        }
                    }
                } else if (obj instanceof PhoneNumber) {
                    PhoneNumber item = (PhoneNumber) obj;
                    if (Utilities.notEmpty(item.getJidNumber())) {
                        String nameForSearch = TextHelper.convertUnicodeForSearch(item.getName()).toLowerCase(Locale.US).trim();
                        if (nameForSearch.contains(keySearchUnmark)) {
                            listContactsTmp.add(item);
                        } else {
                            boolean check = true;
                            for (String key : listKeySearch) {
                                if (key != null && !nameForSearch.contains(key)) {
                                    check = false;
                                    break;
                                }
                            }
                            if (check) listContactsTmp.add(item);
                        }
                    }
                }
            }
            //todo thuc hien tim kiem
            for (ThreadMessage item : listMessagesTmp) {
                String title = item.getThreadName().trim();
                String name = title.toLowerCase(Locale.US);
                if (Utilities.notEmpty(name)) {
                    int threadType = item.getThreadType();
                    String phoneNumber = "";
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(item.getSoloNumber()))
                        phoneNumber = item.getSoloNumber();
                    String nameForSearch = TextHelper.convertUnicodeForSearch(title);
                    String nameUnmark = nameForSearch.toLowerCase(Locale.US);
                    if (name.equalsIgnoreCase(keySearch)) {
                        listResults01.add(item);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (nameUnmark.equalsIgnoreCase(keySearchUnmark)) {
                        listResults02.add(item);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (name.startsWith(keySearch)) {
                        listResults03.add(item);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (nameUnmark.startsWith(keySearchUnmark)) {
                        listResults03.add(item);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else {
                        //old filter
                        boolean check = false;
                        List<String> listNameUnmark = SearchUtils.getListSplitName(nameForSearch);
                        int sizeNameUnmark = listNameUnmark.size();
                        if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                            if (comparatorName != null && sizeNameUnmark > 1)
                                Collections.sort(listNameUnmark, comparatorName);
                            for (String tmp : listKeySearch) {
                                if (Utilities.notEmpty(tmp)) {
                                    boolean checkTmp = false;
                                    for (String nameTmp : listNameUnmark) {
                                        if (Utilities.notEmpty(nameTmp) && nameTmp.startsWith(tmp)) {
                                            checkTmp = true;
                                            listNameUnmark.remove(nameTmp);
                                            break;
                                        }
                                    }
                                    check = checkTmp;
                                    if (!check) break;
                                }
                            }
                            if (check) {
                                listResults03.add(item);
                                if (Utilities.notEmpty(phoneNumber))
                                    mapMessages.put(phoneNumber, 1);
                            }
                        }
                        if (!check) {
                            //new filter
                            listNameUnmark = SearchUtils.getListSplitKey(nameForSearch);
                            sizeNameUnmark = listNameUnmark.size();
                            if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                                if (comparatorName != null && sizeNameUnmark > 1)
                                    Collections.sort(listNameUnmark, comparatorName);
                                for (String tmp : listKeySearch) {
                                    if (Utilities.notEmpty(tmp)) {
                                        boolean checkTmp = false;
                                        for (String nameTmp : listNameUnmark) {
                                            if (Utilities.notEmpty(nameTmp) && nameTmp.startsWith(tmp)) {
                                                checkTmp = true;
                                                listNameUnmark.remove(nameTmp);
                                                break;
                                            }
                                        }
                                        check = checkTmp;
                                        if (!check) break;
                                    }
                                }
                                if (check) {
                                    listResults03.add(item);
                                    if (Utilities.notEmpty(phoneNumber))
                                        mapMessages.put(phoneNumber, 1);
                                }
                            }
                        }
                    }
                }
            }
            listMessagesTmp.clear();

            for (PhoneNumber item : listContactsTmp) {
                String phoneNumber = item.getJidNumber();
                if (!mapMessages.containsKey(phoneNumber)) {
                    String title = item.getName().trim();
                    String name = title.toLowerCase(Locale.US);
                    String nameForSearch = TextHelper.convertUnicodeForSearch(title);
                    String nameUnmark = nameForSearch.toLowerCase(Locale.US);
                    if (name.equalsIgnoreCase(keySearch)) {
                        listResults01.add(item);
                    } else if (nameUnmark.equalsIgnoreCase(keySearchUnmark)) {
                        listResults02.add(item);
                    } else if (name.startsWith(keySearch)) {
                        listResults04.add(item);
                    } else if (nameUnmark.startsWith(keySearchUnmark)) {
                        listResults04.add(item);
                    } else {
                        //old filter
                        boolean check = false;
                        List<String> listNameUnmark = SearchUtils.getListSplitName(nameForSearch);
                        int sizeNameUnmark = listNameUnmark.size();
                        if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                            if (comparatorName != null && sizeNameUnmark > 1)
                                Collections.sort(listNameUnmark, comparatorName);
                            for (String tmp : listKeySearch) {
                                if (Utilities.notEmpty(tmp)) {
                                    boolean checkTmp = false;
                                    for (String nameTmp : listNameUnmark) {
                                        if (nameTmp.startsWith(tmp)) {
                                            checkTmp = true;
                                            listNameUnmark.remove(nameTmp);
                                            break;
                                        }
                                    }
                                    check = checkTmp;
                                    if (!check) break;
                                }
                            }
                            if (check) listResults04.add(item);
                        }
                        if (!check) {
                            //new filter
                            listNameUnmark = SearchUtils.getListSplitKey(nameForSearch);
                            sizeNameUnmark = listNameUnmark.size();
                            if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                                if (comparatorName != null && sizeNameUnmark > 1)
                                    Collections.sort(listNameUnmark, comparatorName);
                                for (String tmp : listKeySearch) {
                                    if (Utilities.notEmpty(tmp)) {
                                        boolean checkTmp = false;
                                        for (String nameTmp : listNameUnmark) {
                                            if (nameTmp.startsWith(tmp)) {
                                                checkTmp = true;
                                                listNameUnmark.remove(nameTmp);
                                                break;
                                            }
                                        }
                                        check = checkTmp;
                                        if (!check) break;
                                    }
                                }
                                if (check) listResults04.add(item);
                            }
                        }
                    }
                }
            }
            listContactsTmp.clear();
            mapMessages.clear();
            if (Utilities.notEmpty(listResults01)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults01, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults01);
                listResults01.clear();
            }
            if (Utilities.notEmpty(listResults02)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults01, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults02);
                listResults02.clear();
            }
            if (Utilities.notEmpty(listResults03)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults03, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults03);
                listResults03.clear();
            }
            if (Utilities.notEmpty(listResults04)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults04, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults04);
                listResults04.clear();
            }
        }
        ResultSearchContact result = new ResultSearchContact();
        result.setResult(list);
        result.setKeySearch(keySearch);
        result.setOldKeySearch(keySearch);
        result.setTimeSearch(System.currentTimeMillis() - startTime);
        return result;
    }

    private static ResultSearchContact searchWithDigits(ApplicationController application, String keySearch
            , CopyOnWriteArrayList datas, Comparator comparatorMessage, Comparator comparatorContact, long startTime) {
        //todo keySearch là số
        ArrayList<Object> list = new ArrayList<>();
        String oldKeySearch = keySearch;
        if (Utilities.notEmpty(datas)) {
            keySearch = SearchUtils.getKeySearchChat(keySearch);
            String tmpNumber = PhoneNumberHelper.getInstant().getPhoneNumberFromText(application, keySearch);
            if (Utilities.notEmpty(tmpNumber)) {
                keySearch = tmpNumber;
            }
            Log.d(TAG, "searchContactsWithKeySearchIsDigits oldKeySearch: " + oldKeySearch + ", keySearch: " + keySearch);
            ArrayList<Object> listResults01 = new ArrayList<>(); //todo tên giống hệt keySearch
            ArrayList<Object> listResults02 = new ArrayList<>(); //todo SĐT giống hệt keySearch
            ArrayList<Object> listResults03 = new ArrayList<>(); //todo tên bắt đầu bằng keySearch
            ArrayList<Object> listResults04 = new ArrayList<>(); //todo SĐT bắt đầu bằng keySearch
            ArrayList<Object> listResults05 = new ArrayList<>(); //todo SĐT chứa bằng keySearch
            ArrayList<Object> listResults06 = new ArrayList<>(); //todo SĐT của 1 thành viên bất kỳ trong nhóm chứa keySearch
            Map<String, Integer> mapMessages = new HashMap<>();
            ArrayList<ThreadMessage> listMessagesTmp = new ArrayList<>();
            ArrayList<PhoneNumber> listContactsTmp = new ArrayList<>();
            for (Object obj : datas) {
                if (obj instanceof ThreadMessage) {
                    ThreadMessage item = (ThreadMessage) obj;
                    if (item.isJoined()) {
                        String name = item.getThreadName();
                        if (name == null) name = "";
                        else name = name.trim();
                        String phoneNumber = item.getSoloNumber();
                        if (phoneNumber == null) phoneNumber = "";
                        if (name.contains(keySearch) || phoneNumber.contains(keySearch)
                                || item.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                            listMessagesTmp.add(item);
                        }
                    }
                } else if (obj instanceof PhoneNumber) {
                    PhoneNumber item = (PhoneNumber) obj;
                    String name = item.getName();
                    if (name == null) name = "";
                    else name = name.trim();
                    String phoneNumber = item.getJidNumber();
                    if (phoneNumber == null) phoneNumber = "";
                    if (Utilities.notEmpty(phoneNumber)
                            && (name.contains(keySearch) || phoneNumber.contains(keySearch))) {
                        item.setName(name);
                        listContactsTmp.add(item);
                    } else {
                        if (keySearch.startsWith("+") && keySearch.length() > 3) {
                            String rawNumber;
                            if (phoneNumber.startsWith("0")) {// so vn
                                rawNumber = "+84" + phoneNumber.substring(1);
                            } else {
                                rawNumber = item.getRawNumber();
                            }
                            if (Utilities.notEmpty(rawNumber) && rawNumber.contains(keySearch)) {
                                item.setName(name);
                                listContactsTmp.add(item);
                            }
                        }
                    }
                }
            }

            //todo thuc hien tim kiem
            for (ThreadMessage item : listMessagesTmp) {
                String name = item.getThreadName();
                if (name == null) name = "";
                else name = name.trim();
                int threadType = item.getThreadType();
                String phoneNumber = item.getSoloNumber();
                if (phoneNumber == null) phoneNumber = "";
                if (name.equals(keySearch)) {
                    listResults01.add(item);
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(phoneNumber))
                        mapMessages.put(phoneNumber, 1);
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !item.isStranger() && phoneNumber.equals(keySearch)) {
                    listResults02.add(item);
                    mapMessages.put(phoneNumber, 1);
                } else if (name.startsWith(keySearch)) {
                    listResults03.add(item);
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(phoneNumber))
                        mapMessages.put(phoneNumber, 1);
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !item.isStranger() && phoneNumber.startsWith(keySearch)) {
                    listResults04.add(item);
                    mapMessages.put(phoneNumber, 1);
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !item.isStranger() && phoneNumber.contains(keySearch)) {
                    listResults05.add(item);
                    mapMessages.put(phoneNumber, 1);
                } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    ArrayList<String> phoneNumbers = item.getPhoneNumbers();
                    for (String phone : phoneNumbers) {
                        if (Utilities.notEmpty(phone) && phone.contains(keySearch)) {
                            listResults06.add(item);
                            break;
                        }
                    }
                }
            }
            listMessagesTmp.clear();

            for (PhoneNumber item : listContactsTmp) {
                String phoneNumber = item.getJidNumber();
                if (!mapMessages.containsKey(phoneNumber)) {
                    String name = item.getName();
                    if (name == null) name = "";
                    else name = name.trim();
                    if (name.equals(keySearch)) {
                        listResults01.add(item);
                    } else if (phoneNumber.equals(keySearch)) {
                        listResults02.add(item);
                    } else if (name.startsWith(keySearch)) {
                        listResults03.add(item);
                    } else if (phoneNumber.startsWith(keySearch)) {
                        listResults04.add(item);
                    } else if (phoneNumber.contains(keySearch)) {
                        listResults05.add(item);
                    } else if (keySearch.startsWith("+") && keySearch.length() > 3) {
                        String rawNumber;
                        if (phoneNumber.startsWith("0")) {// so vn
                            rawNumber = "+84" + phoneNumber.substring(1);
                        } else {
                            rawNumber = item.getRawNumber();
                        }
                        if (Utilities.notEmpty(rawNumber) && rawNumber.contains(keySearch)) {
                            listResults05.add(item);
                        }
                    }
                }
            }
            listContactsTmp.clear();
            if (Utilities.notEmpty(listResults01)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults01, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults01);
                listResults01.clear();
            }
            if (Utilities.notEmpty(listResults02)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults02, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults02);
                listResults02.clear();
            }
            if (Utilities.notEmpty(listResults03)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults03, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults03);
                listResults03.clear();
            }
            if (Utilities.notEmpty(listResults04)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults04, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults04);
                listResults04.clear();
            }
            if (Utilities.notEmpty(listResults05)) {
                if (comparatorContact != null) {
                    try {
                        Collections.sort(listResults05, comparatorContact);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults05);
                listResults05.clear();
            }
            if (Utilities.notEmpty(listResults06)) {
                if (comparatorMessage != null) {
                    try {
                        Collections.sort(listResults06, comparatorMessage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list.addAll(listResults06);
                listResults06.clear();
            }
            mapMessages.clear();
        }
        ResultSearchContact result = new ResultSearchContact();
        result.setResult(list);
        result.setKeySearch(keySearch);
        result.setOldKeySearch(oldKeySearch);
        result.setTimeSearch(System.currentTimeMillis() - startTime);
        return result;
    }

    public static ResultSearchContact searchMessagesAndContacts(ApplicationController application
            , String keySearch, CopyOnWriteArrayList datas, Comparator comparatorKeySearch, Comparator comparatorName
            , Comparator comparatorMessage, Comparator comparatorContact) {
        long startTime = System.currentTimeMillis();
        if (keySearch == null) keySearch = "";
        else keySearch = keySearch.trim().toLowerCase(Locale.US);
        Log.d(TAG, "search keySearch: " + keySearch);
        if (TextUtils.isEmpty(keySearch)) {
            ResultSearchContact result = new ResultSearchContact();
            result.setResult(new ArrayList<>(datas));
            result.setKeySearch(keySearch);
            result.setOldKeySearch(keySearch);
            result.setTimeSearch(System.currentTimeMillis() - startTime);
            return result;
        } else {
            if (keySearch.startsWith("+")) {
                String tmp = keySearch.substring(1);
                if (!TextUtils.isEmpty(tmp) && TextUtils.isDigitsOnly(tmp)) {
                    return searchWithDigits(application, keySearch, datas, comparatorMessage, comparatorContact
                            , startTime);
                } else {
                    return searchWithText(keySearch, datas, comparatorKeySearch, comparatorName, comparatorMessage
                            , comparatorContact, startTime);
                }
            } else if (TextUtils.isDigitsOnly(keySearch)) {
                return searchWithDigits(application, keySearch, datas, comparatorMessage, comparatorContact
                        , startTime);
            } else {
                return searchWithText(keySearch, datas, comparatorKeySearch, comparatorName, comparatorMessage
                        , comparatorContact, startTime);
            }
        }
    }

}

package com.metfone.selfcare.module.metfoneplus.fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.model.camid.ServicePackage;
import com.metfone.selfcare.module.home_kh.notification.detail.RedirectNotificationActivity;
import com.metfone.selfcare.module.keeng.widget.GridSpacingItemDecoration;
import com.metfone.selfcare.module.metfoneplus.adapter.BuyServiceDetailAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceDetailViewHolder;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsDoActionServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class MPBuyServiceDetailFragment extends MPBaseFragment {
    public static final String TAG = MPBuyServiceDetailFragment.class.getSimpleName();
    public static final String SERVICE_GROUP_ID_PARAM = "service_group_id";
    public static final String SERVICE_CODE_PARAM = "service_code";
    public static final String EXCHANGE_PARAM = "exchange";
    public static final String SERVICE_PARAM = "service";
    public static final String GROUP_PARAM = "group";

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.buy_services_detail_image)
    AppCompatImageView mImage;
    @BindView(R.id.buy_services_detail_name)
    AppCompatTextView mName;
    @BindView(R.id.buy_services_detail_short_des)
    AppCompatTextView mShortDes;
    @BindView(R.id.buy_services_detail_valid)
    AppCompatTextView mValid;
    @BindView(R.id.buy_services_detail_full_desc)
    AppCompatTextView mFullDesc;
    @BindView(R.id.buy_services_detail_list_sub_service)
    RecyclerView mListSubServices;
    @BindView(R.id.buy_services_detail_layout_register)
    RelativeLayout mRegister;
    @BindView(R.id.layout_container_buy_Service_detail)
    RelativeLayout mLayoutContainer;
    @BindView(R.id.layout_buy_services_detail)
    NestedScrollView mLayoutBuyServicesDetail;

    private BuyServiceDetailAdapter mBuyServiceDetailAdapter;
    private String mNameService = "";
    private String mServiceGroupId;
    private String mServiceCode;
    private String mSubServiceCodeSelected = "";
    private String mSubServiceNameSelected = "";
    private int mPositionItemInSubServiceList = -1;
    private String mContentServiceRegisterDialog = "";
    /**
     * Get value from fullDes field
     */
    private String mRegisterDialSyntax = "";

    /**
     * Only for Exchange package
     */
    private boolean isPackageAutoRenew = false;
    /**
     * 0: register dial
     * the other cases: register using api
     */
    private int mOtherPackageIsRegisterAble = 0;
    private List<ServiceGroup> mServiceGroups = null;
    private List<ServicePackage> mServicePackages = null;

    /* Data for display from notifications*/
    private ExchangeItem exchangeItem;
    private ServiceGroup serviceGroup;
    private WsGetServiceDetailResponse.Response service;

    private View.OnClickListener mOnSubServiceSelectedListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            BuyServiceDetailViewHolder holder = (BuyServiceDetailViewHolder) view.getTag();
            if ("".equals(mServiceGroupId)) {
                mSubServiceCodeSelected = holder.mServiceGroup.getCode();
                mSubServiceNameSelected = holder.mServiceGroup.getName();
                isPackageAutoRenew = holder.isAutoRenew;
                resetSelectItemOfServiceGroups(mServiceGroups, holder.position);
            } else {
                mSubServiceCodeSelected = holder.mServicePackage.getCode();
                mSubServiceNameSelected = holder.mServicePackage.getName();
                mRegisterDialSyntax = holder.mServicePackage.getFullDes();
                resetSelectItemOfServicePackages(mServicePackages, holder.position);
            }

            Log.d(TAG, "onClick: serviceCode = " + mSubServiceCodeSelected);
        }
    };

    public MPBuyServiceDetailFragment() {

    }


    public static MPBuyServiceDetailFragment newInstance(ServiceGroup serviceGroup, String serviceGroupId, String serviceCode) {
        MPBuyServiceDetailFragment fragment = new MPBuyServiceDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(GROUP_PARAM, serviceGroup);
        bundle.putString(SERVICE_GROUP_ID_PARAM, serviceGroupId);
        bundle.putString(SERVICE_CODE_PARAM, serviceCode);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static MPBuyServiceDetailFragment newInstance(ExchangeItem exchangeItem, String serviceGroupId, String serviceCode) {
        MPBuyServiceDetailFragment fragment = new MPBuyServiceDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXCHANGE_PARAM, exchangeItem);
        bundle.putString(SERVICE_GROUP_ID_PARAM, serviceGroupId);
        bundle.putString(SERVICE_CODE_PARAM, serviceCode);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static MPBuyServiceDetailFragment newInstance(ExchangeItem exchangeItem) {
        Bundle args = new Bundle();
        args.putSerializable(EXCHANGE_PARAM, exchangeItem);
        MPBuyServiceDetailFragment fragment = new MPBuyServiceDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static MPBuyServiceDetailFragment newInstance(WsGetServiceDetailResponse.Response service) {
        Bundle args = new Bundle();
        args.putSerializable(SERVICE_PARAM, service);
        MPBuyServiceDetailFragment fragment = new MPBuyServiceDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_buy_service_detail;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mLayoutActionBar);
        if (mParentActivity instanceof HomeActivity) {
            Utilities.adaptViewForInsertBottom(mLayoutContainer);
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.GONE);
        }
        if (getArguments() != null) {
            mServiceGroupId = getArguments().getString(SERVICE_GROUP_ID_PARAM);
            mServiceCode = getArguments().getString(SERVICE_CODE_PARAM);
            exchangeItem = (ExchangeItem) getArguments().getSerializable(EXCHANGE_PARAM);
            serviceGroup = (ServiceGroup) getArguments().getSerializable(GROUP_PARAM);
            service = (WsGetServiceDetailResponse.Response) getArguments().getSerializable(SERVICE_PARAM);
        }

        mBuyServiceDetailAdapter = new BuyServiceDetailAdapter(mParentActivity,
                new ArrayList<ServiceGroup>(), new ArrayList<ServicePackage>(),
                mOnSubServiceSelectedListener);

        GridSpacingItemDecoration gridSpacingItemDecoration =
                new GridSpacingItemDecoration(2, mParentActivity.getResources().getDimensionPixelOffset(R.dimen.m_p_margin_normal), false);

        mListSubServices.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
        mListSubServices.setHasFixedSize(true);
        mListSubServices.setAdapter(mBuyServiceDetailAdapter);
        mListSubServices.addItemDecoration(gridSpacingItemDecoration);

        Linkify.addLinks(mFullDesc, Linkify.WEB_URLS);
        mFullDesc.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (exchangeItem != null) {
            mServiceGroupId = "";
            initialContentExchangePackDetail();
        }else if(serviceGroup != null){
            getServicesDetail(mServiceCode);
        }
        else if (service != null) {
            mServiceGroupId = !StringUtils.isEmpty(service.getCode()) ? service.getCode() : "Not empty"; /* Logic cu nhu con cac, neu la "" thi la exchange service*/

            serviceGroup = new ServiceGroup();
            serviceGroup.setShortDes(!StringUtils.isEmpty(service.getShortDes()) ? service.getShortDes() : "");
            serviceGroup.setValidity(!StringUtils.isEmpty(service.getValidity()) ? service.getValidity() : "");

            initialContentOtherPackDetail(service);
        }
    }

    private void getServicesDetail(String serviceCode) {
        new MetfonePlusClient().wsGetServiceDetail(serviceCode, new MPApiCallback<WsGetServiceDetailResponse>() {
            @Override
            public void onResponse(Response<WsGetServiceDetailResponse> response) {
                if (response.body() != null && response.body().getResult().getWsResponse() != null) {
                    initialContentOtherPackDetail(response.body().getResult().getWsResponse());
                }
            }

            @Override
            public void onError(Throwable error) {

            }
        });
    }

    private void initialContentExchangePackDetail() {
        if (exchangeItem == null)
            return;
        if (exchangeItem.getImage() != null) {
            Glide.with(mApplication)
                    .load(exchangeItem.getImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            mLayoutBuyServicesDetail.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            mLayoutBuyServicesDetail.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(mImage);
        }

        mNameService = exchangeItem.getGroupName();
        mName.setText(mNameService);
        mShortDes.setText(exchangeItem.getShortDes());
        mValid.setText(exchangeItem.getValidDes());
        htmlForTextView(mFullDesc, exchangeItem.getFullDes());

        List<ServiceGroup> serviceGroups = exchangeItem.getServices();

        if (serviceGroups == null) {
            mListSubServices.setVisibility(View.GONE);
            return;
        }

        if (serviceGroups.size() <= 1) {
            mListSubServices.setVisibility(View.GONE);
            if (serviceGroups.size() == 1) {
                mSubServiceCodeSelected = serviceGroups.get(0).getCode();
            }
        } else {
            mListSubServices.setVisibility(View.VISIBLE);
            mServiceGroups = serviceGroups;
            mBuyServiceDetailAdapter.replaceServiceGroupData(serviceGroups);
            mListSubServices.post(new Runnable() {
                @Override
                public void run() {
                    Objects.requireNonNull(mListSubServices.findViewHolderForAdapterPosition(0)).itemView.performClick();
                }
            });
        }

        mLayoutBuyServicesDetail.setVisibility(View.VISIBLE);
    }

    private void initialContentOtherPackDetail(WsGetServiceDetailResponse.Response response) {
        if (serviceGroup == null)
            return;
        Glide.with(mApplication)
                .load(response.getImgDesUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        mLayoutBuyServicesDetail.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        mLayoutBuyServicesDetail.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(mImage);

        mNameService = response.getName();
        mName.setText(mNameService);
        mShortDes.setText(serviceGroup.getShortDes());
        mValid.setText(serviceGroup.getValidity());
        if (response.getFullDes() != null) {
            htmlForTextView(mFullDesc, response.getFullDes());
        }

        mOtherPackageIsRegisterAble = response.getIsRegisterAble();
        List<ServicePackage> servicePackages = response.getServicePackages();

        if (servicePackages == null) {
            mListSubServices.setVisibility(View.GONE);
            return;
        }

        if (servicePackages.size() <= 1) {
            mListSubServices.setVisibility(View.GONE);
            if (servicePackages.size() == 1) {
                mSubServiceCodeSelected = servicePackages.get(0).getCode();
                mRegisterDialSyntax = servicePackages.get(0).getFullDes();
            }
        } else {
            mListSubServices.setVisibility(View.VISIBLE);
            mServicePackages = response.getServicePackages();
            mBuyServiceDetailAdapter.replaceServicePackageData(response.getServicePackages());
            mListSubServices.post(new Runnable() {
                @Override
                public void run() {
                    Objects.requireNonNull(mListSubServices.findViewHolderForAdapterPosition(0)).itemView.performClick();
                }
            });
        }
    }

    @OnClick(R.id.buy_services_detail_title_register)
    void onRegisterService() {
        if ("".equals(mSubServiceCodeSelected)) {
            Toast.makeText(mParentActivity, getString(R.string.m_p_notification_choose_a_package), Toast.LENGTH_SHORT).show();
            return;
        }

        String content = "";
        if ("".equals(mServiceGroupId)) {
            if (isPackageAutoRenew) {
                content = getString(R.string.m_p_dialog_service_register_content_auto_renew, mNameService);
                mContentServiceRegisterDialog = getString(R.string.m_p_dialog_service_register_content_super_exchange)
                        + " "
                        + mNameService
                        + " "
                        + getString(R.string.m_p_dialog_service_register_content_super_exchange_auto_renew);
            } else {
                content = getString(R.string.m_p_dialog_service_register_content_non_auto_renew, mNameService);
                mContentServiceRegisterDialog = getString(R.string.m_p_dialog_service_register_content_super_exchange)
                        + " "
                        + mNameService;
            }
        } else {
            if ("".equals(mSubServiceNameSelected)) {
                mContentServiceRegisterDialog = mNameService;
            } else {
                mContentServiceRegisterDialog = mNameService + "(" + mSubServiceNameSelected + ") ";
            }
            content = getString(R.string.m_p_dialog_service_register_content_other, mContentServiceRegisterDialog);

            // Register Service using dial only for
            // Mobile Internet packs(Data), Social network packs(Add-on), International packs(International), Vas services(VAS)
            if (mOtherPackageIsRegisterAble == 0 && !"".equals(mRegisterDialSyntax)) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(mRegisterDialSyntax)));
                mParentActivity.startActivity(callIntent);
                Log.d(TAG, "onRegisterService: mRegisterDialSyntax = " + mRegisterDialSyntax);
                return;
            }
        }

        showConfirmDialog(mSubServiceCodeSelected, content);
    }

    private void showConfirmDialog(String serviceCode, String content) {
        MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                .setTitle(R.string.m_p_dialog_service_register_title_confirm)
                .setContent(content)
                .setNameButtonTop(R.string.m_p_dialog_service_register_let_do_it)
                .setNameButtonBottom(R.string.m_p_dialog_service_register_no)
                .setCancelableOnTouchOutside(false)
                .build();

        dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onTopButtonClick() {
                dialogSuccess.dismiss();
                getWSDoActionService(serviceCode);
            }

            @Override
            public void onBottomButtonClick() {
                dialogSuccess.dismiss();
            }
        });
        dialogSuccess.show(getChildFragmentManager(), null);
    }

    private void showSuccessDialog() {
        MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                .setLottieAssertName("lottie/img_dialog_women_success.json")
                .setTitle(R.string.m_p_dialog_service_register_title_successfully)
                .setContent(getString(R.string.m_p_dialog_service_register_content_success, mContentServiceRegisterDialog))
                .setCancelableOnTouchOutside(true)
                .build();

        dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onDialogDismiss() {
                dialogSuccess.dismiss();
            }
        });
        dialogSuccess.show(getChildFragmentManager(), null);
    }

    private void showErrorDialog(String errorCode, String content) {
        MPDialogFragment.Builder builder = new MPDialogFragment.Builder()
                .setTitle(R.string.m_p_dialog_service_register_title_unsuccessful);

        if (Constants.WSCODE.WS_DO_ACTION_SERVICE_RESULT_NOT_ENOUGH_MONEY.equals(errorCode)) {
            builder.setContent(content)
                    .setNameButtonTop(R.string.m_p_dialog_service_register_top_up)
                    .setNameButtonBottom(R.string.m_p_dialog_service_register_later)
                    .setCancelableOnTouchOutside(false);
        } else {
            builder.setContent(R.string.m_p_dialog_service_register_content_general_error)
                    .setCancelableOnTouchOutside(true);
        }

        MPDialogFragment dialogSuccess = builder.build();
        dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {

            @Override
            public void onTopButtonClick() {
                if (Constants.WSCODE.WS_DO_ACTION_SERVICE_RESULT_NOT_ENOUGH_MONEY.equals(errorCode)) {
                    dialogSuccess.dismiss();
                    popBackStackFragment();

                    gotoMPTopUpFragment();
                    if (mParentActivity instanceof HomeActivity) {
                        ((HomeActivity) mParentActivity).setBottomNavigationBarColor(R.color.m_home_tab_background, true);
                    }
                }
            }

            @Override
            public void onBottomButtonClick() {
                dialogSuccess.dismiss();
                popBackStackFragment();

                if (mParentActivity instanceof HomeActivity) {
                    ((HomeActivity) mParentActivity).setBottomNavigationBarColor(R.color.m_home_tab_background_2, true);
                    ((HomeActivity)mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onDialogDismiss() {
                dialogSuccess.dismiss();
            }
        });
        dialogSuccess.show(getChildFragmentManager(), null);
    }

    private void getWSDoActionService(String serviceCode) {
        ApplicationController.self().getFirebaseEventBusiness().logRegisterService(mNameService,serviceCode);
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        new MetfonePlusClient().wsDoActionService(serviceCode, new MPApiCallback<WsDoActionServiceResponse>() {
            @Override
            public void onResponse(Response<WsDoActionServiceResponse> response) {
                if (response.body() != null && response.body().getResult() != null) {
                    if (Constants.WSCODE.WS_DO_ACTION_SERVICE_RESULT_SUCCESS.equals(response.body().getResult().getErrorCode())) {
                        showSuccessDialog();
                        ApplicationController.self().getFirebaseEventBusiness().logRegisterServiceResult(true,mNameService,serviceCode,"0");
                    } else {
                        String errorCode = response.body().getResult().getErrorCode();
                        if (response.body().getResult().getMessage() == null) {
                            showErrorDialog(errorCode, response.body().getResult().getUserMsg());
                        } else {
                            showErrorDialog(errorCode, response.body().getResult().getMessage());
                        }
                        ApplicationController.self().getFirebaseEventBusiness().logRegisterServiceResult(false,mNameService,serviceCode,errorCode);
                    }
                }
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    @OnClick({R.id.action_bar_back})
    public void onBack() {
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        } else if (mParentActivity instanceof RedirectNotificationActivity || mParentActivity instanceof DeepLinkActivity) {
            mParentActivity.finish();
        }
        popBackStackFragment();
    }

    private void resetSelectItemOfServiceGroups(List<ServiceGroup> list, int position) {
        for (ServiceGroup serviceGroup : list) {
            serviceGroup.setSelected(false);
        }

        list.get(position).setSelected(true);
        mBuyServiceDetailAdapter.replaceServiceGroupData(list);
    }

    private void resetSelectItemOfServicePackages(List<ServicePackage> list, int position) {
        for (ServicePackage servicePackage : list) {
            servicePackage.setSelected(false);
        }

        list.get(position).setSelected(true);
        mBuyServiceDetailAdapter.replaceServicePackageData(list);
    }
}

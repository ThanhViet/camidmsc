/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.video.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.ads.AdsNativeHolder;
import com.metfone.selfcare.holder.content.VideoDetailHolder;
import com.metfone.selfcare.module.video.holder.BoxVideoHolder;
import com.metfone.selfcare.module.video.holder.HeaderListVideoHolder;
import com.metfone.selfcare.module.video.listener.TabVideoListener;
import com.metfone.selfcare.module.video.model.VideoPagerModel;
import com.metfone.selfcare.ui.tabvideo.holder.BannerHolder;
import com.metfone.selfcare.util.Log;

public class VideoPagerAdapter extends BaseAdapter<BaseAdapter.ViewHolder, VideoPagerModel> {

    private TabVideoListener.OnAdapterClick listener;

    public VideoPagerAdapter(Activity act) {
        super(act);
    }

    public void setListener(TabVideoListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        VideoPagerModel item = getItem(position);
        if (item != null) return item.getType();
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VideoPagerModel.TYPE_SUGGEST_LIST:
            case VideoPagerModel.TYPE_CHANNEL_LIST:
            case VideoPagerModel.TYPE_NEW_VIDEO_LIST:
            case VideoPagerModel.TYPE_FRIEND_LIST:
                return new BoxVideoHolder(layoutInflater.inflate(R.layout.holder_box_content_tab_video, parent, false), activity, listener);
            case VideoPagerModel.TYPE_HEADER_LIST_VIDEO:
                return new HeaderListVideoHolder(layoutInflater.inflate(R.layout.holder_header_list_video, parent, false), activity, listener);
            case VideoPagerModel.TYPE_VIDEO_NORMAL:
                return new VideoDetailHolder(layoutInflater.inflate(R.layout.holder_large_video, parent, false), activity, listener);
            case VideoPagerModel.TYPE_VIDEO_BANNER:
                return new BannerHolder(activity, layoutInflater, parent);
            case VideoPagerModel.TYPE_ADS:
                return new AdsNativeHolder(layoutInflater.inflate(R.layout.ad_unified_medium, parent, false));
        }
        Log.d(TAG, "onCreateViewHolder other viewType: " + viewType);
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

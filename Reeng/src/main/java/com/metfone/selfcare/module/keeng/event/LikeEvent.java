package com.metfone.selfcare.module.keeng.event;

public class LikeEvent {
   boolean isLike;

    public LikeEvent(boolean isLike) {
        this.isLike = isLike;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    @Override
    public String toString() {
        return "LikeEvent{" +
                "isLike=" + isLike +
                '}';
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/11/5
 *
 */

package com.metfone.selfcare.module.myviettel.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.holder.BoxDetailHolder;
import com.metfone.selfcare.module.myviettel.holder.ResultCommissionHolder;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class HistoryDataChallengeAdapter extends BaseAdapter<BaseAdapter.ViewHolder, DataChallenge> {
    public static final int TYPE_HEADER = 1;
    public static final int TYPE_DETAIL = 2;

    private ArrayList<DataChallenge> data;
    private long totalTransaction;
    private long totalCommission;

    public HistoryDataChallengeAdapter(Activity activity) {
        super(activity);
    }

    public void setData(ArrayList<DataChallenge> data) {
        this.data = data;
    }

    public void setTotalTransaction(long totalTransaction) {
        this.totalTransaction = totalTransaction;
    }

    public void setTotalCommission(long totalCommission) {
        this.totalCommission = totalCommission;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return new ResultCommissionHolder(layoutInflater.inflate(R.layout.holder_result_history_mvt_dc, parent, false), activity);
        } else if (viewType == TYPE_DETAIL) {
            return new BoxDetailHolder(layoutInflater.inflate(R.layout.holder_box_commission_mvt_dc, parent, false), activity, true);
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder instanceof ResultCommissionHolder) {
            ((ResultCommissionHolder) holder).bindData(totalTransaction, totalCommission);
        } else if (holder instanceof BoxDetailHolder) {
            ((BoxDetailHolder) holder).bindData(data);
        }
    }

    @Override
    public int getItemCount() {
        return Utilities.notEmpty(data) ? 2 : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return TYPE_HEADER;
        if (position == 1) return TYPE_DETAIL;
        return TYPE_EMPTY;
    }
}

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class TiinProvisional extends Provisional {

    private ArrayList<TiinModel> data;

    public TiinProvisional() {
    }

    public TiinProvisional(ArrayList<TiinModel> data) {
        this.data = data;
    }

    public ArrayList<TiinModel> getData() {
        return data;
    }

    public void setData(ArrayList<TiinModel> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return Utilities.notEmpty(data) ? 1 : 0;
    }
}

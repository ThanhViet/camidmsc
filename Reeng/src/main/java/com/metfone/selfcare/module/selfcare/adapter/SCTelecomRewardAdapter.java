package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCTelecomRewardModel;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;

import java.util.ArrayList;

public class SCTelecomRewardAdapter<T> extends BaseAdapter<BaseViewHolder> {

    private ArrayList<T> data;
    private String type = SCConstants.TELECOM_REWARD_TYPE.TYPE_TELECOM_DATA;
    private AbsInterface.OnTelecomRewardListener listener;

    public SCTelecomRewardAdapter(Context context, AbsInterface.OnTelecomRewardListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(ArrayList<T> data) {
        this.data = data;
    }

    public void setType(String type) {
        this.type = type;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_telecom_reward, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder holder, int position) {
        final SCTelecomRewardModel model = (SCTelecomRewardModel) data.get(position);
        if (model != null) {
            holder.setText(R.id.tvName, TextUtils.isEmpty(model.getName()) ? "" : model.getName());

            if (model.getCosts().size() > 0) {
                int cost = 0;
                for (int i = 0; i < model.getCosts().size(); i++) {
                    if ("TELCO_EXCHANGEABLE_POINTS".equals(model.getCosts().get(i).getLoyaltyBalanceCode())) {
                        cost = model.getCosts().get(i).getCost();
                        break;
                    }
                }
                holder.setText(R.id.tvDescription, mContext.getString(R.string.sc_cost_point, SCUtils.numberFormat(cost)));
            }

            if (type == SCConstants.TELECOM_REWARD_TYPE.TYPE_TELECOM_DATA)
                ((ImageView) holder.getView(R.id.imvImage)).setImageResource(R.drawable.ic_sc_telecom_data);
            else if (type == SCConstants.TELECOM_REWARD_TYPE.TYPE_TELECOM_SMS)
                ((ImageView) holder.getView(R.id.imvImage)).setImageResource(R.drawable.ic_sc_telecom_sms);
            else if (type == SCConstants.TELECOM_REWARD_TYPE.TYPE_TELECOM_VOICE)
                ((ImageView) holder.getView(R.id.imvImage)).setImageResource(R.drawable.ic_sc_telecom_voice);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onTelecomRewardDetailClick(model);
                }
            });

            holder.getView(R.id.btnBuy).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onTelecomRewardClick(model, holder.getView(R.id.btnBuy));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }
}
package com.metfone.selfcare.module.keeng.widget.buttonSheet;

import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.view.View;
import android.view.Window;

import com.metfone.selfcare.module.keeng.utils.Utilities;

public class BottomDialog extends BottomSheetDialog {
    private boolean touchOutsideToCancel;

    public BottomDialog(@NonNull Context context) {
        super(context);
//        super(context, R.style.style_dialog2);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //R.style.Bottom_sheet_dialog♦

    }

    public BottomDialog(@NonNull Context context, int theme) {
        super(context, theme);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    protected BottomDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    public void setTouchOutsideToCancel(boolean enable) {
        this.touchOutsideToCancel = enable;
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        Utilities.configureBottomSheetBehavior(view);
        if (touchOutsideToCancel) {
            view.setOnClickListener(v -> dismiss());
        } else {
            view.setOnClickListener(null);
        }
    }
}

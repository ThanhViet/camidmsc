package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.holder.PlaylistNewHolder;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

public class PlaylistAdapter extends SectionListDataAdapter<PlayListModel> {

    public PlaylistAdapter(Context context, List<PlayListModel> itemsList, AbsInterface.OnItemListener onClick) {
        super(context, itemsList, onClick);
        setCanViewAll(true);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_playlist_music, null);
        return new PlaylistNewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        final PlayListModel item = getItem(position);
        PlaylistNewHolder itemHolder = (PlaylistNewHolder) holder;
        if (item != null) {
            holder.setVisible(R.id.layout_image, true);
            holder.setVisible(R.id.layout_option, false);
            holder.setVisible(R.id.layout_title, true);
            holder.setVisible(R.id.layout_view_all, false);
            itemHolder.mTvTitle.setText(item.getName());
            String str = item.getNameUser();
            if (TextUtils.isEmpty(str)) {
                itemHolder.mTvCreated.setVisibility(View.GONE);
            } else {
                itemHolder.mTvCreated.setVisibility(View.VISIBLE);
                itemHolder.mTvCreated.setText(mContext.getString(R.string.create_by, str));
            }

            List<String> listImages = item.getListAvatar();
            ImageBusiness.setImagePlaylist(listImages, itemHolder.mTopLeftImage, itemHolder.mTopRightImage
                    , itemHolder.mBottomRightImage, itemHolder.mBottomLeftImage, itemHolder.mRightImageLayout);
            itemHolder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClick != null)
                        onClick.onItemClick(item);
                }
            });
        } else if (isCanViewAll()) {
            holder.setVisible(R.id.layout_image, false);
            holder.setVisible(R.id.layout_option, false);
            holder.setVisible(R.id.layout_title, false);
            holder.setVisible(R.id.layout_view_all, true);
            itemHolder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClick != null)
                        onClick.onHeaderClick(view, getTag());
                }
            });
        }
    }
}
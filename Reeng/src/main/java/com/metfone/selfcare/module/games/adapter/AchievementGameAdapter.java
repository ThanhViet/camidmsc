package com.metfone.selfcare.module.games.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AchievementGameAdapter extends RecyclerView.Adapter<AchievementGameAdapter.AchievementHolder> {

    private final ArrayList<GameModel> listGame;
    private final Context context;

    public AchievementGameAdapter(ArrayList<GameModel> listGame, Context context) {
        this.listGame = listGame;
        this.context = context;
    }


    @NonNull
    @NotNull
    @Override
    public AchievementHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_game_achievement, parent, false);
        return new AchievementHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AchievementHolder holder, int position) {
        GameModel gameData = listGame.get(position);
        Glide.with(context)
                .asBitmap().error(R.drawable.df_image_home_poster)
                .load(gameData.getIconURL())
                .fitCenter()
                .into(holder.ivGameIcon);
        holder.tvGameName.setText(gameData.getName());
        holder.tvGameScore.setText(String.format("%,d", gameData.getBestScore()));
    }

    @Override
    public int getItemCount() {
        return listGame.size();
    }

    public class AchievementHolder extends RecyclerView.ViewHolder {
        private final RoundedImageView ivGameIcon;
        private final TextView tvGameName;
        private final TextView tvGameScore;


        public AchievementHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            ivGameIcon = itemView.findViewById(R.id.iv_achievement_icon);
            tvGameName = itemView.findViewById(R.id.tv_achievement_name);
            tvGameScore = itemView.findViewById(R.id.tv_achievement_score);
        }
    }
}

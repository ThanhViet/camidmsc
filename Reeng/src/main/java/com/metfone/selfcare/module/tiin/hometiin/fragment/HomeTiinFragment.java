package com.metfone.selfcare.module.tiin.hometiin.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.tiin.AppProvider;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.activitytiin.TiinActivity;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.hometiin.adapter.HomeTiinAdapter;
import com.metfone.selfcare.module.tiin.hometiin.presenter.HomeTiinPresenter;
import com.metfone.selfcare.module.tiin.hometiin.presenter.IHomeTiinMvpPresenter;
import com.metfone.selfcare.module.tiin.hometiin.view.HomeTiinMvpView;
import com.metfone.selfcare.module.tiin.maintiin.fragment.TabTiinFragment;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.vtm.adslib.AdsHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HomeTiinFragment extends BaseFragment implements HomeTiinMvpView, SwipeRefreshLayout.OnRefreshListener, TiinListener.OnHomeTiinItemListener {

    IHomeTiinMvpPresenter mPresenter;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    Unbinder unbinder;
    LinearLayoutManager layoutManager;
    @BindView(R.id.loadingView)
    View loadingView;
    @BindView(R.id.loadingFail)
    View loadingFail;
    private List<HomeTiinModel> datas = new ArrayList<>();
    private HomeTiinAdapter adapter;
    private ListenerUtils listenerUtils;
    private AdsHelper adsHelper;

    public static HomeTiinFragment newInstance() {
        Bundle args = new Bundle();
        HomeTiinFragment fragment = new HomeTiinFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_tiin, container, false);
        if (mPresenter == null) {
            mPresenter = new HomeTiinPresenter();
        }
        mPresenter.onAttach(this);
        unbinder = ButterKnife.bind(this, view);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
        setUp();
        return view;
    }

    private void setUp() {
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorNewBg));
            layout_refresh.setOnRefreshListener(this);
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(getBaseActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new HomeTiinAdapter(getBaseActivity(), datas, this);
        recyclerView.setAdapter(adapter);

        adsHelper = new AdsHelper(getBaseActivity());
        if (AdsUtils.checkShowAds()) {
            String adUnit = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_PLAYER_MUSIC);
            adsHelper.init(adUnit, AdSize.MEDIUM_RECTANGLE, new AdsHelper.AdsBannerListener() {
                @Override
                public void onAdShow(AdView adView) {
                    adapter.notifyDataSetChanged();
                }
            });
            adapter.setAdsHelper(adsHelper);
        }

        if (datas == null || (datas.size() == 0)) {
            layout_refresh.setRefreshing(true);
            loadingFail.setVisibility(View.GONE);
            if (mPresenter != null) {
                mPresenter.getHomeCache();
                mPresenter.getHome();
            }

        }
        //

    }

    @Override
    public void loadDataSuccess(boolean flag) {
        if (layout_refresh != null) {
            layout_refresh.setRefreshing(false);
        }
        hideRefresh();
        if (!flag) {
            loadingFail();
        } else {
//            isLoadSuccess = flag;
        }
    }

    public void loadingFail() {
        if (datas.size() == 0) {
            if (loadingFail != null) {
                loadingFail.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void bindData(List<HomeTiinModel> response) {
        if (response == null) {
            return;
        }
        datas.clear();
        datas.addAll(response);
        if (datas == null || datas.size() == 0) {
            return;
        }
        //todo random 1 trong 5 ảnh
//        boolean isOne = SharedPrefs.getInstance().get(ConstantTiin.KEY_CREATE_HOME, Boolean.class);
//        if (isOne && AppProvider.isLoadHome) {
//            Random mRandom = new Random();
//            int number = mRandom.nextInt(5);
//            TiinModel changeTiin = new TiinModel();
//            if (datas.get(0).getData().size() > 0) {
//                for (int i = 0; i < datas.get(0).getData().size(); i++) {
//                    if (i == number) {
//                        changeTiin = datas.get(0).getData().get(i);
//                        datas.get(0).getData().remove(i);
//                        break;
//                    }
//                }
//                datas.get(0).getData().add(0, changeTiin);
//            }
//        }
//        SharedPrefs.getInstance().put(ConstantTiin.KEY_CREATE_HOME, true);

        //todo set video len vi tri thu 5
        HomeTiinModel homeTiinModel = new HomeTiinModel();
        for (int i = 0; i < datas.size(); i++) {
            if (datas.get(i) != null && datas.get(i).getCategoryName().equals("Video")) {
                homeTiinModel = datas.get(i);
                datas.remove(i);
                break;
            }
        }
        datas.add(4, homeTiinModel);

//        datas.addAll(response);
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        if (layout_refresh != null) {
            layout_refresh.setRefreshing(false);
        }
        if (datas.size() > 0) {
            if (layoutManager != null)
                layoutManager.scrollToPosition(0);
        }
    }

    @Override
    public void saveDataCache(String data) {
        if (TextUtils.isEmpty(data)) return;
        SharedPrefs.getInstance().put(SharedPrefs.HOME_DATA_CACHE, data);
    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(getBaseActivity()) || recyclerView == null || mPresenter == null)
            return;
        onRefresh();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        if (adapter != null) {
            adapter = null;
        }
        if (recyclerView != null) {
            recyclerView = null;
        }

        if (adsHelper != null)
            adsHelper.onDestroy();
    }

    @Override
    public void onRefresh() {
        if (loadingFail.getVisibility() == View.VISIBLE) {
            loadingFail.setVisibility(View.GONE);
        }
        if (mPresenter != null) {
            mPresenter.getHomeCache();
            mPresenter.getHome();
            if (adsHelper != null && AdsUtils.checkShowAds())
                adsHelper.loadAd();
        }
    }

    @OnClick(R.id.tvLoadFail)
    public void loadFailClick() {
        loadingFail.setVisibility(View.GONE);
        onRefresh();
    }

    @OnClick(R.id.imvRefresh)
    public void loadImageFailClick() {
        loadingFail.setVisibility(View.GONE);
        onRefresh();
    }

    @Override
    public void onItemClick(TiinModel model) {
        readTiin(model);
    }

    @Override
    public void onItemClickVideo(TiinModel model) {
        readTiin(model);
    }

    @Override
    public void onItemClickEvent(TiinModel model) {
        Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
        intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY);
        intent.putExtra(ConstantTiin.KEY_CATEGORY, model.getId());
        intent.putExtra(ConstantTiin.KEY_TITLE, model.getTitle());
        getBaseActivity().startActivity(intent);
    }

    @Override
    public void onItemClickHeader(int type, int categoryId, String title) {
        //todo neu title co trong tabtiin thi mo trong tab tiin, còn lại mo tab category mơi
        if (type == HomeTiinAdapter.SECTION_NOW) {
            Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
            intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY_NOW);
            intent.putExtra(ConstantTiin.KEY_TITLE, title);
            getBaseActivity().startActivity(intent);
        } else if (type == HomeTiinAdapter.SECTION_FOLLOW) {
            Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
            intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY);
            intent.putExtra(ConstantTiin.KEY_CATEGORY, categoryId);
            intent.putExtra(ConstantTiin.KEY_TITLE, title);
            getBaseActivity().startActivity(intent);
        } else if (type == HomeTiinAdapter.SECTION_CATEGORY_NEWS) {
            //todo neu chua no mo trong tab
            int pos = -1;
            String[] data = AppProvider.getInstance().getIdCategory().split(",");
            if (data.length > 0) {
                for (int i = 0; i < data.length; i++) {
                    if (!TextUtils.isEmpty(data[i])) {
                        int id = Integer.parseInt(data[i]);
                        if (id == categoryId) {
                            pos = i;
                            break;
                        }
                    }
                }
            }
            if (pos != -1) {
                ((TabTiinFragment) getParentFragment()).selectTab(pos + 2);
            } else {
                Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
                intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY);
                intent.putExtra(ConstantTiin.KEY_CATEGORY, categoryId);
                intent.putExtra(ConstantTiin.KEY_TITLE, title);
                getBaseActivity().startActivity(intent);
            }

        } else if (type == HomeTiinAdapter.SECTION_LATEST) {
            ((TabTiinFragment) getParentFragment()).selectTab(1);
        }
    }

    @Override
    public void onItemClickMore(TiinModel model) {
        DialogUtils.showOptionTiinItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionTiin(object, menuId);
            }
        });
    }

    @Override
    public void onItemClickHashTag(TiinModel model) {
        Intent intent = new Intent(getBaseActivity(), TiinActivity.class);
        intent.putExtra(ConstantTiin.KEY_TAB, ConstantTiin.TAB_CATEGORY);
        intent.putExtra(ConstantTiin.KEY_CATEGORY, ConstantTiin.CATEGOTY_HASHTAG);
        intent.putExtra(ConstantTiin.KEY_HASHTAG, model.getHasTagSlug());
        intent.putExtra(ConstantTiin.KEY_TITLE, model.getHasTagName());
        getBaseActivity().startActivity(intent);
    }

    private void scrollToTop() {
        if (layoutManager != null && recyclerView != null) {
            try {
                layoutManager.smoothScrollToPosition(recyclerView, null, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public HomeTiinModel findEventData() {
        if (datas == null || datas.isEmpty()) return null;
        HomeTiinModel model = null;
        for (int i = 0; i < datas.size(); i++) {
            if (datas.get(i).getPosition() == 1) {
                model = datas.get(i);
                break;
            }
        }
        return model;
    }

    public void scrollToPosition(int position) {
        Log.e("---Duong----", "HomeTiinFragment");
        if (layoutManager != null && recyclerView != null) {
            recyclerView.stopScroll();
            layoutManager.scrollToPosition(position);
        }
    }
}

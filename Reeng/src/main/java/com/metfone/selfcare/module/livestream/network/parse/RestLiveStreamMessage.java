package com.metfone.selfcare.module.livestream.network.parse;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.livestream.model.LiveStreamMessage;

import java.io.Serializable;

public class RestLiveStreamMessage extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("data")
    private LiveStreamMessage data;

    public LiveStreamMessage getData() {
        return data;
    }

    public void setData(LiveStreamMessage data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestLiveStreamMessage [data=" + data + "] errror " + getErrorCode();
    }
}

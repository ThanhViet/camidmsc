package com.metfone.selfcare.module.home_kh.tab.adapter.banner;

import android.view.View;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;

public class KhHomeLuckyGameBannerHolder extends BaseAdapter.ViewHolder {
    public KhHomeLuckyGameBannerHolder(View view, TabHomeKhListener.OnAdapterClick listener) {
        super(view);
        ImageView gameTab = view.findViewById(R.id.game_tab);
        gameTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onGameTabClick();
            }
        });

        ImageView luckyWheel = view.findViewById(R.id.lucky_wheel);
        luckyWheel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLuckyWheelGameClick();
            }
        });

        ImageView newYear = view.findViewById(R.id.new_year);
        newYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNewYearGameClick();
            }
        });
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (v.getId() == R.id.lucky_wheel) {
//                    listener.onLuckyWheelGameClick();
//                } else {
//                    listener.onNewYearGameClick();
//                }
//            }
//        });
    }

}

package com.metfone.selfcare.module.home_kh.notification.detail;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewActivity extends BaseSlidingFragmentActivity {
    public static String URL_KEY = "url";
    private String url;
    @BindView(R.id.webView)
    WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        url = getIntent().getStringExtra(URL_KEY);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return true;
            }
        });
    }

    @OnClick(R.id.icBackToolbar)
    public void onClickView(View v) {
        if (v.getId() == R.id.icBackToolbar) {
            finish();
        }
    }
}

package com.metfone.selfcare.module.movie.listener;

import androidx.recyclerview.widget.RecyclerView;

public class LoadmoreListener {

    public interface ILoadmoreAtPositionListener {
        void loadmoreAtPosition(int position, RecyclerView.Adapter adapter);
    }

    public interface IStartLoadmoreListener {
        void startLoadmore();
    }

    public interface ILoadmoreAtPositionRclvInsideRclv {
        void loadmoreAtPositionOfRclvInsideRclv(int position, RecyclerView.Adapter adapter);
    }

}

package com.metfone.selfcare.module.tab_home.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.tab_home.adapter.BoxContentAdapter;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.vtm.adslib.AdsListener;
import com.vtm.adslib.template.TemplateView;

import java.util.ArrayList;

import butterknife.BindView;

public class BoxContentHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;
    @BindView(R.id.button_more)
    @Nullable
    TextView btnMore;
    @BindView(R.id.layout_ads)
    @Nullable
    FrameLayout layout_ads;
    @BindView(R.id.adContainer)
    @Nullable
    TemplateView layout_ads_native;

    private ArrayList<Object> list;
    private BoxContentAdapter adapter;
    private TabHomeModel data;
    private int position;

    public BoxContentHolder(View view, Activity activity, final TabHomeListener.OnAdapterClick listener) {
        super(view);
        list = new ArrayList<>();
        adapter = new BoxContentAdapter(activity);
        adapter.setListener(listener);
        adapter.setItems(list);
        BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, true);
        if (tvTitle != null) tvTitle.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickTitleBox(data, position);
                }
            }
        });
        if (btnMore != null) btnMore.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickTitleBox(data, position);
                }
            }
        });
    }

    @Override
    public void bindData(Object item, int position) {
        this.position = position;
        if (item instanceof TabHomeModel) {
            int type = ((TabHomeModel)item).getType();
            switch (type){
                case TabHomeModel.TYPE_BOX_VIDEO:
                    btnMore.setText(R.string.discover_more_video);
                    AdsManager.getInstance().showAdsBannerMedium(layout_ads, new AdsListener() {
                        @Override
                        public void onAdClosed() {

                        }

                        @Override
                        public void onAdShow() {

                        }
                    });
                    break;
                case TabHomeModel.TYPE_BOX_MUSIC:
                    btnMore.setText(R.string.discover_more_music);
                    break;
                case TabHomeModel.TYPE_BOX_MOVIE:
                    btnMore.setText(R.string.discover_more_movie);
                    break;

                case TabHomeModel.TYPE_BOX_NEWS:
                    btnMore.setText(R.string.discover_more_news);
                    break;

                case TabHomeModel.TYPE_BOX_COMIC:
                    btnMore.setText(R.string.discover_more_comic);
                    break;
                case TabHomeModel.TYPE_BOX_TIIN:
                    btnMore.setText(R.string.discover_more_tiin);
                    if(layout_ads_native != null)
                        AdsManager.getInstance().showAdsNative(layout_ads_native);
                    break;

            }
            data = (TabHomeModel) item;
            if (list == null) list = new ArrayList<>();
            else list.clear();
            list.addAll(data.getList());
            if (tvTitle != null) tvTitle.setText(data.getTitle());
            if (adapter != null) adapter.notifyDataSetChanged();
        } else {
            data = null;
        }
    }

}

package com.metfone.selfcare.module.netnews.HomeNews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.DateUtilitis;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.vtm.adslib.AdsHelper;

import java.util.List;

public class HomeNewsAdapterV5 extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int TYPE_HEADER_CATEGORY = 8; //todo list chuyen muc
    public static final int TYPE_CARE = 0; //todo tin co the quan tam
    public static final int TYPE_HOT = 1; //todo tin nóng
    public static final int TYPE_EVENT = 2; //todo tin sự kiện
    public static final int TYPE_CATEGORY = 9; //todo tin chuyen muc
    private static final int TYPE_HIGHTLINE = 3; //todo tin nổi bật
    public static final int TYPE_NETTV = 4; //todo tin nettv
    public static final int TYPE_CONCERNED = 5; //todo tin đang được quan tâm
    private static final int TYPE_QUOTE = 6; //todo câu trích dẫn
    public static final int TYPE_LOCATION = 7; //todo cgan khu vuc ban
    private static final int TYPE_EMPTY = -1;

    AbsInterface.OnHomeNewsItemListener listener;
    List<CategoryModel> dataCatogory;
    List<HomeNewsModel> datas;
    AdsHelper adsHelper;
    private Context mContext;

    public HomeNewsAdapterV5(Context context, List<HomeNewsModel> datas, AbsInterface.OnHomeNewsItemListener listener) {
        this.datas = datas;
        this.mContext = context;
        this.listener = listener;
    }

    public void setAdsHelper(AdsHelper adsHelper) {
        this.adsHelper = adsHelper;
    }

    public void setDataCatogory(List<CategoryModel> dataCatogory) {
        this.dataCatogory = dataCatogory;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case TYPE_HEADER_CATEGORY:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_recyclerview, parent, false);
                break;
            case TYPE_CARE:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_hot_home_new, parent, false);
                break;
            case TYPE_CATEGORY:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_hot_home_new, parent, false);
                break;
            case TYPE_HOT:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_hot_home_news, parent, false);
                break;
            case TYPE_EVENT:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_event_home_new, parent, false);
                break;
//            case TYPE_HIGHTLINE:
//                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_event_home_new, parent, false);
//                break;
            case TYPE_NETTV:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_video_home_new, parent, false);
                break;
            case TYPE_CONCERNED:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_event_home_new, parent, false);
                break;
            case TYPE_QUOTE:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_quote_home_new, parent, false);
                break;
            case TYPE_LOCATION:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_box_hot_home_new, parent, false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case TYPE_HEADER_CATEGORY:
                setUpSectionHeaderCategory(holder, dataCatogory);
                break;
            case TYPE_CARE:
                setUpSectionCare(holder, getItem(position));
                break;
            case TYPE_HOT:
                setUpSectionTop(holder, getItem(position));
                break;
            case TYPE_EVENT:
                setUpSectionEvent(holder, getItem(position));
                break;
//            case TYPE_HIGHTLINE:
//                setUpSectionHightLine(holder, getItem(position));
//                break;
            case TYPE_NETTV:
                setUpSectionVideo(holder, getItem(position));
                break;
            case TYPE_CONCERNED:
                setUpSectionConerned(holder, getItem(position));
                break;
            case TYPE_QUOTE:
                setUpSectionQuote(holder, getItem(position));
                break;
            case TYPE_LOCATION:
                setUpSectionLocation(holder, getItem(position));
                break;
            case TYPE_CATEGORY:
                setUpSectionCategory(holder, getItem(position));
                break;
            default:
                break;

        }
    }

    @Override
    public int getItemViewType(int position) {

        if (dataCatogory != null && position == 0) {
            return TYPE_HEADER_CATEGORY;
        } else {
            HomeNewsModel homeNewsModel = getItem(position);
            if (homeNewsModel.getData().size() > 0) {
                switch (homeNewsModel.getPosition()) {
                    case TYPE_CARE:
                        return TYPE_CARE;
                    case TYPE_HOT:
                        return TYPE_HOT;
                    case TYPE_EVENT:
                        return TYPE_EVENT;
                    case TYPE_HIGHTLINE:
                        return TYPE_HIGHTLINE;
                    case TYPE_NETTV:
                        return TYPE_NETTV;
                    case TYPE_CONCERNED:
                        return TYPE_CONCERNED;
                    case TYPE_QUOTE:
                        return TYPE_QUOTE;
                    case TYPE_LOCATION:
                        return TYPE_LOCATION;
                    default:
                        if (homeNewsModel.getTypeDisplay() == 0 || homeNewsModel.getTypeDisplay() == 1) {
                            return TYPE_CATEGORY;
                        }
                }
            }
        }
        return TYPE_EMPTY;
    }

    @Override
    public int getItemCount() {
        int index = 0;
        if (dataCatogory != null) {
            index = 1;
        }
        return index + datas.size();
    }

    public HomeNewsModel getItem(int position) {
        return (null != datas && datas.size() > position ? datas.get(position) : null);
    }

    private void setUpSectionHeaderCategory(BaseViewHolder holder, List<CategoryModel> dataCatogory) {
        if (holder == null || dataCatogory == null) {
            return;
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        HomeNewCategoryAdapterV5 childHomeNewAdapterV5 = new HomeNewCategoryAdapterV5(dataCatogory, mContext, listener);
        recyclerView.setAdapter(childHomeNewAdapterV5);
    }

    private void setUpSectionCare(BaseViewHolder holder, final HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setVisible(R.id.tv_view_all, false);
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
        //todo 1
        if (data.getData().size() > 0) {
            if (holder.getView(R.id.iv_cover) != null) {
                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.iv_cover));
            }
            if (holder.getView(R.id.tv_title) != null) {
                holder.setText(R.id.tv_title, data.getData().get(0).getTitle());
            }
            if (holder.getView(R.id.tv_category) != null) {
                holder.setText(R.id.tv_category, data.getData().get(0).getSourceName());
                holder.setOnClickListener(R.id.tv_category, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemCategoryClick(data.getData().get(0).getSourceName(), data.getData().get(0).getSid());
                    }
                });
            }
//            if (holder.getView(R.id.tv_desc) != null) {
//                holder.setText(R.id.tv_desc, data.getData().get(0).getShapo());
//            }
            if (holder.getView(R.id.tv_datetime) != null) {
                holder.setVisible(R.id.tv_datetime, true);
                if (data.getData().get(0).getTimeStamp() > 0) {
                    holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, data.getData().get(0).getTimeStamp()));
                } else {
                    holder.setText(R.id.tv_datetime, data.getData().get(0).getDatePub());
                }
            }
            if (holder.getView(R.id.layout_root) != null) {
                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(data.getData().get(0));
                    }
                });
            }
            if (holder.getView(R.id.button_option) != null) {
                holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClickBtnMore(data.getData().get(0));
                    }
                });
            }
        }
        //todo 2
        if (data.getData().size() > 1) {
            RecyclerView recyclerView = holder.getView(R.id.recycler_view_horizontal);
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_horizonal_v5, true));
            }
            HomeNewVideoAdapterV5 childHomeNewAdapterV5 = new HomeNewVideoAdapterV5(data, mContext, TYPE_CARE, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }

    private void setUpSectionTop(BaseViewHolder holder, final HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setVisible(R.id.tv_view_all, false);
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

        //todo 1
//        if (data.getData().size() > 0) {
//            if (holder.getView(R.id.iv_cover) != null) {
//                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.iv_cover));
//            }
//            if (holder.getView(R.id.tv_title) != null) {
//                holder.setText(R.id.tv_title, data.getData().get(0).getTitle());
//            }
//            if (holder.getView(R.id.tv_category) != null) {
//                holder.setText(R.id.tv_category, data.getData().get(0).getSourceName());
//                holder.setOnClickListener(R.id.tv_category, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemCategoryClick(data.getData().get(0).getSourceName(),data.getData().get(0).getSid());
//                    }
//                });
//            }
////            if (holder.getView(R.id.tv_desc) != null) {
////                holder.setText(R.id.tv_desc, data.getData().get(0).getShapo());
////            }
//            if (holder.getView(R.id.tv_datetime) != null) {
//                holder.setVisible(R.id.tv_datetime, true);
//                if(data.getData().get(0).getTimeStamp() > 0){
//                    holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, data.getData().get(0).getTimeStamp()));
//                }else{
//                    holder.setText(R.id.tv_datetime, data.getData().get(0).getDatePub());
//                }
//            }
//            if (holder.getView(R.id.layout_root) != null) {
//                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClick(data.getData().get(0));
//                    }
//                });
//            }
//            if(holder.getView(R.id.button_option) != null){
//                holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        listener.onItemClickBtnMore(data.getData().get(0));
//                    }
//                });
//            }
//        }
//        //todo 2
//        if (data.getData().size() > 1) {
//            RecyclerView recyclerView = holder.getView(R.id.recycler_view_horizontal);
//            recyclerView.setHasFixedSize(true);
//            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
//            HomeNewVideoAdapterV5 childHomeNewAdapterV5 = new HomeNewVideoAdapterV5(data, mContext, TYPE_HOT, listener);
//            recyclerView.setAdapter(childHomeNewAdapterV5);
//        }
        //todo 3
        if (data.getData().size() > 0) {
            RecyclerView recyclerView = holder.getView(R.id.recycler_view);
            recyclerView.setNestedScrollingEnabled(false);
            if (recyclerView.getItemDecorationCount() <= 0) {
                CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_default_tiin, true));
            }
            HomeNewLargeAdapterV5 childHomeNewAdapterV5 = new HomeNewLargeAdapterV5(data, mContext, TYPE_HOT, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }

    private void setUpSectionEvent(BaseViewHolder holder, HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemEventHeaderClick();
                }
            });
        }
        if (data.getData().size() > 0) {
            RecyclerView recyclerView = holder.getView(R.id.recycler_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            HomeNewNormalAdapterV5 childHomeNewAdapterV5 = new HomeNewNormalAdapterV5(data, mContext, TYPE_EVENT, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }

        if (adsHelper != null)
            adsHelper.showAd(holder.getView(R.id.layout_ads));
    }

    private void setUpSectionHightLine(BaseViewHolder holder, HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setVisible(R.id.tv_view_all, false);
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView != null) {
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.setNestedScrollingEnabled(false);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_default_tiin, true));
            }
//            recyclerView.setNestedScrollingEnabled(false);
//            recyclerView.setHasFixedSize(true);
//            recyclerView.setItemAnimator(new DefaultItemAnimator());
//            recyclerView.setItemViewCacheSize(11);
//            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

            HomeNewSpecialAdapterV5 childHomeNewAdapterV5 = new HomeNewSpecialAdapterV5(data, mContext, TYPE_HIGHTLINE, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }

    private void setUpSectionVideo(BaseViewHolder holder, final HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, "Video");
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemCategoryHeaderClick(TYPE_CATEGORY, data.getCategoryId(), "Video");
                }
            });
        }
        //todo 1
        if (data.getData().size() > 0) {
            if (holder.getView(R.id.iv_cover) != null) {
                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.iv_cover));
            }
            if (holder.getView(R.id.tv_title) != null) {
                holder.setText(R.id.tv_title, data.getData().get(0).getTitle());
            }
            if (holder.getView(R.id.tv_category) != null) {
                holder.setVisible(R.id.tv_category, false);
            }
            if (holder.getView(R.id.tv_datetime) != null) {
                holder.setVisible(R.id.tv_datetime, false);
            }
            if (holder.getView(R.id.iv_play_news) != null) {
                holder.setVisible(R.id.iv_play_news, true);
            }
            if (holder.getView(R.id.layout_root) != null) {
                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(data.getData().get(0));
                    }
                });
            }
            if (holder.getView(R.id.button_option) != null) {
                holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClickBtnMore(data.getData().get(0));
                    }
                });
            }
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        if (recyclerView != null) {
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_horizonal_v5, true));
            }
            HomeNewVideoAdapterV5 childHomeNewAdapterV5 = new HomeNewVideoAdapterV5(data, mContext, TYPE_NETTV, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }

    private void setUpSectionConerned(BaseViewHolder holder, HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setVisible(R.id.tv_view_all, false);
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        HomeNewContentAdapterV5 childHomeNewAdapterV5 = new HomeNewContentAdapterV5(data, mContext, TYPE_CONCERNED, listener);
        recyclerView.setAdapter(childHomeNewAdapterV5);
    }

    private void setUpSectionQuote(BaseViewHolder holder, final HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (data.getData().size() > 0) {
            if (holder.getView(R.id.iv_cover) != null) {
                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.iv_cover));
            }
            if (holder.getView(R.id.tv_title) != null) {
                TiinUtilities.setTextQuote(holder.getView(R.id.tv_title), data.getData().get(0).getQuote());
            }
            if (holder.getView(R.id.tv_desc) != null) {
                holder.setText(R.id.tv_desc, data.getData().get(0).getPoster());
            }
            if (holder.getView(R.id.layout_root) != null) {
                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(data.getData().get(0));
                    }
                });
            }
            if (holder.getView(R.id.button_option) != null) {
                holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClickBtnMore(data.getData().get(0));
                    }
                });
            }
        }
    }

    private void setUpSectionLocation(BaseViewHolder holder, HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setVisible(R.id.tv_view_all, false);
        }
        //todo 1
        if (data.getData().size() > 0) {
            if (holder.getView(R.id.iv_cover) != null) {
                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.iv_cover));
            }
            if (holder.getView(R.id.tv_title) != null) {
                holder.setText(R.id.tv_title, data.getData().get(0).getTitle());
            }
            if (holder.getView(R.id.tv_category) != null) {
                holder.setText(R.id.tv_category, data.getData().get(0).getSourceName());
                holder.setOnClickListener(R.id.tv_category, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemCategoryClick(data.getData().get(0).getSourceName(), data.getData().get(0).getSid());
                    }
                });
            }
//            if (holder.getView(R.id.tv_desc) != null) {
//                holder.setText(R.id.tv_desc, data.getData().get(0).getShapo());
//            }
            if (holder.getView(R.id.tv_datetime) != null) {
                holder.setVisible(R.id.tv_datetime, true);
                if (data.getData().get(0).getTimeStamp() > 0) {
                    holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, data.getData().get(0).getTimeStamp()));
                } else {
                    holder.setText(R.id.tv_datetime, data.getData().get(0).getDatePub());
                }
            }
            if (holder.getView(R.id.layout_root) != null) {
                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(data.getData().get(0));
                    }
                });
            }
            if (holder.getView(R.id.button_option) != null) {
                holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClickBtnMore(data.getData().get(0));
                    }
                });
            }
        }
        //todo 2
        if (data.getData().size() > 1) {
            RecyclerView recyclerView = holder.getView(R.id.recycler_view_horizontal);
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_horizonal_v5, true));
            }
            HomeNewVideoAdapterV5 childHomeNewAdapterV5 = new HomeNewVideoAdapterV5(data, mContext, TYPE_LOCATION, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }

    private void setUpSectionCategory(BaseViewHolder holder, final HomeNewsModel data) {
        if (holder == null || data == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, data.getHeader());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemCategoryHeaderClick(TYPE_CATEGORY, data.getCategoryId(), data.getHeader());
                }
            });
        }

        //todo 1
        if (data.getData().size() > 0) {
            if (holder.getView(R.id.iv_cover) != null) {
                ImageBusiness.setImageNew(data.getData().get(0).getImage(), (ImageView) holder.getView(R.id.iv_cover));
            }
            if (holder.getView(R.id.tv_title) != null) {
                holder.setText(R.id.tv_title, data.getData().get(0).getTitle());
            }
            if (holder.getView(R.id.tv_category) != null) {
                holder.setText(R.id.tv_category, data.getData().get(0).getSourceName());
            }
//            if (holder.getView(R.id.tv_desc) != null) {
//                holder.setText(R.id.tv_desc, data.getData().get(0).getShapo());
//            }
            if (holder.getView(R.id.tv_datetime) != null) {
                holder.setVisible(R.id.tv_datetime, true);
                if (data.getData().get(0).getTimeStamp() > 0) {
                    holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, data.getData().get(0).getTimeStamp()));
                } else {
                    holder.setText(R.id.tv_datetime, data.getData().get(0).getDatePub());
                }
            }
            if (holder.getView(R.id.layout_root) != null) {
                holder.setOnClickListener(R.id.layout_root, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(data.getData().get(0));
                    }
                });
            }
            if (holder.getView(R.id.button_option) != null) {
                holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClickBtnMore(data.getData().get(0));
                    }
                });
            }
        }
        //todo 2
        if (data.getData().size() > 1) {
            RecyclerView recyclerView = holder.getView(R.id.recycler_view_horizontal);
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_horizonal_v5, true));
            }
            HomeNewVideoAdapterV5 childHomeNewAdapterV5 = new HomeNewVideoAdapterV5(data, mContext, TYPE_CATEGORY, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
//        //todo 3
        if (data.getData().size() > 3) {
            RecyclerView recyclerView = holder.getView(R.id.recycler_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            HomeNewContentAdapterV5 childHomeNewAdapterV5 = new HomeNewContentAdapterV5(data, mContext, TYPE_CATEGORY, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }

}


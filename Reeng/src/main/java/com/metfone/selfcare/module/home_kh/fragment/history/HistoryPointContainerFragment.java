package com.metfone.selfcare.module.home_kh.fragment.history;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.viewpager.widget.ViewPager;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.fragment.history.adapter.HistoryPointPagerAdapter;
import com.metfone.selfcare.module.home_kh.fragment.setting.BaseSettingKhFragment;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class HistoryPointContainerFragment extends BaseSettingKhFragment implements ViewPager.OnPageChangeListener {
    public static String TAG = HistoryPointContainerFragment.class.getSimpleName();

    public static final String REMOVE_PADDING = "REMOVE_PADDING";

    public static HistoryPointContainerFragment newInstance(boolean removePadding) {
        HistoryPointContainerFragment fragment = new HistoryPointContainerFragment();
        Bundle args = new Bundle();
        args.putBoolean(REMOVE_PADDING, removePadding);
        fragment.setArguments(args);
        return fragment;
    }

    public static final int TAB_RECEIVED = 0;
    public static final int TAB_USED = 1;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tab_received)
    View tabReceiver;

    @BindView(R.id.tab_used)
    View tabUsed;
    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView txtTitleToolbar;

    @BindView(R.id.container)
    LinearLayout lnContainer;

    @Override
    protected void initView(View view) {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtTitleToolbar.setText(ResourceUtils.getString(R.string.kh_history_points));
        final HistoryPointPagerAdapter adapter = new HistoryPointPagerAdapter(getChildFragmentManager());
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(adapter);
        onPageSelected(0);
        tabReceiver.setOnClickListener(v -> {
            if (viewPager.getCurrentItem() != TAB_RECEIVED) {
                viewPager.setCurrentItem(TAB_RECEIVED);
            }
        });
        tabUsed.setOnClickListener(v -> {
            if (viewPager.getCurrentItem() != TAB_USED) {
                viewPager.setCurrentItem(TAB_USED);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            boolean isRemovePadding = bundle.getBoolean(REMOVE_PADDING);
            if (isRemovePadding) {
                lnContainer.setPadding(0, 0, 0, 0);
            }
        }
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_history_point_container;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            tabReceiver.setSelected(true);
            tabUsed.setSelected(false);
        } else {
            tabReceiver.setSelected(false);
            tabUsed.setSelected(true);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick(R.id.icBackToolbar)
    void onBack() {
        popBackStackFragment();
    }
}

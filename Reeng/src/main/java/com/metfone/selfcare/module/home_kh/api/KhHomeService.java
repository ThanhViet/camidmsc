package com.metfone.selfcare.module.home_kh.api;

import com.metfone.selfcare.module.home_kh.api.response.WsGetGiftResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetHomeMovieApp2Response;
import com.metfone.selfcare.module.home_kh.api.response.WsGetListFilmForUserResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetListLogWatchedResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieCategoryV2Response;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieCountryResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsPopupTetResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemGiftTetResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsSliderFilmResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetCategoryByIdsResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetListFilmOfCategoryResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetListGroupDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetRelatedResponse;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationClearResponse;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationReadResponse;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationResponse;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsAccountInfoResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 *
 */
public interface KhHomeService {

    @POST("CamIDAutoLogin")
    Call<LoginResponse> autoLogin(@Body RequestBody requestBody);

    @POST("UserLogin")
    Call<LoginResponse> login(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsAccountInfoResponse> wsAccountInfo(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsGetAccountRankInfoResponse> wsAccountRank(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsGetGiftSearchResponse> wsGtAllPartnerGiftSearch(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetGiftListResponse> wsGtAllPartnerGiftDetail(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<KhNotificationResponse> wsGetListCamIDNotification(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<KhNotificationReadResponse> wsUpdateIsReadCamIDNotification(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<KhNotificationClearResponse> wsClearAllCamIdNotification(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<SearchRewardResponse> wsGetReward(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetAllAppsResponse> wsGetAllApps(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGiftInfoResponse> wsGetGiftInfo(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGiftSpinResponse> wsGiftSpin(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGiftBoxResponse> wsGiftBoxList(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGiftSpinResponse> wsGiftRedeem(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<BaseResponse> wsLogApp(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetMovieResponse> wsGetHotMovie(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetMovieResponse> wsGetTopMovieCategory(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetHomeMovieApp2Response> wsGetHomeMovieApp2(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetListLogWatchedResponse> wsGetListLogWatched(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetListFilmForUserResponse> wsGetListMovieForUser(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetMovieDetailResponse> wsGetMovieDetail(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetMovieCategoryV2Response> wsGetMovieCategoryV2(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetMovieCountryResponse> wsGetMovieCountry(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsMovieGetRelatedResponse> wsGetMovieRelated(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsMovieGetListFilmOfCategoryResponse> wsGetMovieListOfFilmCategory(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsMovieGetListGroupDetailResponse> wsGetMovieListGroupDetail(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsMovieGetCategoryByIdsResponse> wsGetMovieCategoryByIds(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsSliderFilmResponse> wsGetSliderFilm(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsGetGiftResponse> checkGetGiftTet(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsPopupTetResponse> wsShowPopupTet(@Body RequestBody requestBody);


    @POST("UserRouting")
    Call<WsRedeemGiftTetResponse> redeemGiftTet(@Body RequestBody requestBody);
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.utils.ImageBusiness;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.OnClick;

public class MoviesAllHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;

    private SearchAllListener.OnAdapterClick listener;
    private Movie data;
    private Activity activity;

    public MoviesAllHolder(View view, Activity activity, SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
        this.activity = activity;
    }

    public void bindData(Object item, int position, String keySearch) {
        if (item instanceof Movie) {
            data = (Movie) item;

            Glide.with(activity).load(data.getPosterPath()).into(ivCover);
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxMovies && data != null) {
            ((SearchAllListener.OnClickBoxMovies) listener).onClickMoviesItem(data);
        }
    }

    @OnClick(R.id.iv_more)
    public void onClickItemMore() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            listener.onClickMenuMore(data);
        }
    }
}
package com.metfone.selfcare.module.keeng.fragment.home;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.adapter.home.TopicDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.network.restpaser.RestAllTopic;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.ArrayList;

public class TopicHotFragment extends RecyclerFragment<Topic> implements BaseListener.OnLoadMoreListener {

    private TopicDetailAdapter adapter;
    private ListenerUtils listenerUtils;
    private TextView tvTitle;
    private View btnBack;

    public TopicHotFragment() {
        super();
    }

    public static TopicHotFragment newInstance() {
        Bundle args = new Bundle();
        TopicHotFragment fragment = new TopicHotFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_album_hot;
    }

    @Override
    public String getName() {
        return "TopicHotFragment";
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onBackPressed();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clearData();
                adapter = new TopicDetailAdapter(mActivity, getDatas(), TAG);
                setupRecycler(adapter, R.drawable.home_divider);
                adapter.setRecyclerView(recyclerView, TopicHotFragment.this);
                doLoadData(true);
            }
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        tvTitle = view.findViewById(R.id.tv_title);
        btnBack = view.findViewById(R.id.iv_back);
        tvTitle.setText(getString(R.string.topic_hot));
        return view;
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        App.getInstance().cancelPendingRequests(KeengApi.GET_TOPIC_HOT);
        isLoading = false;
        refreshed();
        loadMored();
        loadingFinish();
        if (adapter != null)
            adapter.setLoaded();
    }

    private void doLoadData(boolean type) {
        if (!isLoading) {
            if (type)
                loadingBegin();
            isLoading = true;
            loadData();
        }
    }

    private void loadData() {
        new KeengApi().getTopicHot(currentPage, numPerPage, new Response.Listener<RestAllTopic>() {
            public void onResponse(final RestAllTopic result) {
                doAddResult(result.getData());
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error);
                if (errorCount < MAX_ERROR_RETRY) {
                    errorCount++;
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            loadData();
                        }
                    }, Constants.TIME_DELAY_RETRY);
                    return;
                }
                doAddResult(null);
            }
        });
    }

    private void doAddResult(ArrayList<Topic> result) {
        Log.d(TAG, "doAddResult ...............");
        errorCount = 0;
        isLoading = false;
        try {
            adapter.setLoaded();
            checkLoadMoreAbsolute(result);
            if (result == null) {
                if (isRefresh) {
                    isRefresh = false;
                }
                loadMored();
                loadingError(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doLoadData(true);
                    }
                });
                return;
            }
            if (getDatas().size() == 0 && result.size() == 0) {
                loadMored();
                loadingEmpty();
                refreshed();
            } else {
                refreshed();
                loadMored();
                loadingFinish();
                setDatas(result);
                adapter.notifyDataSetChanged();
                currentPage++;
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onTopicClick(View v, int position) {
        if (mActivity == null || adapter == null)
            return;
        Topic item = adapter.getItem(position);
        if (item != null) {
            mActivity.gotoTopicDetail(item);
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        canLoadMore = true;
        currentPage = 1;
        doLoadData(false);
    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            loadMore();
            doLoadData(false);
        }
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && getDatas().size() == 0)
            onRefresh();
    }
}

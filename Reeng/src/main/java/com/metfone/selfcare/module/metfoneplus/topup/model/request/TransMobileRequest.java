package com.metfone.selfcare.module.metfoneplus.topup.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransMobileRequest {
    String phoneNumber;
    String topupAmount;
    String paymentMethod;
    String accountEmoney;

    public TransMobileRequest() {
    }
}

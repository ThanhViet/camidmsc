package com.metfone.selfcare.module.metfoneplus.dialog;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lombok.SneakyThrows;

public class DialogSelectDate extends DialogFragment implements View.OnClickListener {

    public static final String TAG = DialogSelectDate.class.getSimpleName();
    public static final String DIALOG_DATE_FROM = "date_from";
    public static final String DIALOG_DATE_TO = "date_to";
    public DialogSelectDateListener dialogSelectDateListener;
    public DialogSelectDate dialogSelectDate;
    TextView tvSelectDateTitle;
    ImageView btnSelectDateCancel;
    TextView tvSelectDateFrom;
    TextView tvSelectDateTo;
    RadioButton rbSelectDateToday;
    RadioButton rbSelectDateWeek;
    RadioButton rbSelectDateMonth;
    RadioButton rbSelectDateYear;
    TextView btnSelectDateSearch;
    String mDateFrom, mDateTo;
    Date dateFrom, dateTo;
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public DialogSelectDate() {

    }

    public static DialogSelectDate newInstance(String dFrom, String dTo) {
        DialogSelectDate dialogSelectDate = new DialogSelectDate();
        Bundle args = new Bundle();
        args.putString(DIALOG_DATE_FROM, dFrom);
        args.putString(DIALOG_DATE_TO, dTo);
        dialogSelectDate.setArguments(args);
        return dialogSelectDate;
    }

    @SneakyThrows
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.dialog_comp_date, container, false);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogConfirmFullScreen);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initView(view);

        if (getArguments() != null) {
            mDateFrom = getArguments().getString(DIALOG_DATE_FROM);
            mDateTo = getArguments().getString(DIALOG_DATE_TO);
            dateFrom = simpleDateFormat.parse(mDateFrom);
            dateTo = simpleDateFormat.parse(mDateTo);
        }
        tvSelectDateFrom.setText(mDateFrom);
        tvSelectDateTo.setText(mDateTo);

        return view;
    }

    public void initView(View view) {
        tvSelectDateTitle = view.findViewById(R.id.tvSelectDateTitle);
        btnSelectDateCancel = view.findViewById(R.id.btnSelectDateCancel);
        tvSelectDateFrom = view.findViewById(R.id.tvSelectDateFrom);
        tvSelectDateTo = view.findViewById(R.id.tvSelectDateTo);
        rbSelectDateToday = view.findViewById(R.id.rbSelectDateToday);
        rbSelectDateWeek = view.findViewById(R.id.rbSelectDateWeek);
        rbSelectDateMonth = view.findViewById(R.id.rbSelectDateMonth);
        rbSelectDateYear = view.findViewById(R.id.rbSelectDateYear);
        btnSelectDateSearch = view.findViewById(R.id.btnSelectDateSearch);

        rbSelectDateToday.setOnClickListener(this);
        rbSelectDateWeek.setOnClickListener(this);
        rbSelectDateMonth.setOnClickListener(this);
        rbSelectDateYear.setOnClickListener(this);
        tvSelectDateFrom.setOnClickListener(this);
        tvSelectDateTo.setOnClickListener(this);
        btnSelectDateSearch.setOnClickListener(this);
        btnSelectDateCancel.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSelectDateCancel: {
                dismissAllowingStateLoss();
                break;
            }
            case R.id.btnSelectDateSearch: {
                if (dialogSelectDateListener != null) {
                    dialogSelectDateListener.doneSelect(mDateFrom, mDateTo);
                }
                dismissAllowingStateLoss();
                break;
            }
            case R.id.tvSelectDateFrom: {
                datePickerDialog(mDateFrom, 1);
                setStateOnClick(5);
                break;
            }
            case R.id.tvSelectDateTo: {
                datePickerDialog(mDateTo, 2);
                setStateOnClick(5);
                break;
            }
            case R.id.rbSelectDateToday: {
                setStateOnClick(1);
                Calendar c = Calendar.getInstance();
                c.setTime(dateTo);
                Date d = c.getTime();
                mDateFrom = simpleDateFormat.format(d);
                tvSelectDateFrom.setText(simpleDateFormat.format(d));
                break;
            }
            case R.id.rbSelectDateWeek: {
                setStateOnClick(2);
                Calendar c = Calendar.getInstance();
                c.setTime(dateTo);
                c.setFirstDayOfWeek(Calendar.SUNDAY);
                c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                c.add(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
//                c.add(Calendar.DATE, -7);
                Date d = c.getTime();
                mDateFrom = simpleDateFormat.format(d);
                tvSelectDateFrom.setText(simpleDateFormat.format(d));
                break;
            }
            case R.id.rbSelectDateMonth: {
                setStateOnClick(3);
                Calendar c = Calendar.getInstance();
                c.setTime(dateTo);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
                Date d = c.getTime();
                mDateFrom = simpleDateFormat.format(d);
                tvSelectDateFrom.setText(simpleDateFormat.format(d));
                break;
            }
            case R.id.rbSelectDateYear: {
                setStateOnClick(4);
                Calendar c = Calendar.getInstance();
                c.setTime(dateTo);
                c.add(Calendar.MONTH, -3);
                Date d = c.getTime();
                mDateFrom = simpleDateFormat.format(d);
                tvSelectDateFrom.setText(simpleDateFormat.format(d));
            }
        }
    }

    @SneakyThrows
    public void datePickerDialog(String date, int id) {
        Date mDate = simpleDateFormat.parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mDate);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        int m = calendar.get(Calendar.MONTH);
        int y = calendar.get(Calendar.YEAR);
        DatePickerDialog.OnDateSetListener dateSetListener = (view, year, month, dayOfMonth) -> {
            try {
                Date selectDate = simpleDateFormat.parse(dayOfMonth + "/" + (month + 1) + "/" + year);
                assert selectDate != null;
                String select = simpleDateFormat.format(selectDate);
                if (id == 1) {
                    tvSelectDateFrom.setText(select);
                    mDateFrom = select;
                    dateFrom = selectDate;
                }
                if (id == 2) {
                    tvSelectDateTo.setText(select);
                    mDateTo = select;
                    dateTo = selectDate;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        };
        DatePickerDialog datePickerDialog = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            datePickerDialog = new DatePickerDialog(getContext(),
                    android.R.style.Theme_Material_Dialog_NoActionBar,
                    dateSetListener, y, m, d);
        }
        calendar.add(Calendar.YEAR, -1);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    public void setStateOnClick(int id) {
        switch (id) {
            case 1: {
                rbSelectDateToday.setChecked(true);
                rbSelectDateWeek.setChecked(false);
                rbSelectDateMonth.setChecked(false);
                rbSelectDateYear.setChecked(false);
                break;
            }
            case 2: {
                rbSelectDateToday.setChecked(false);
                rbSelectDateWeek.setChecked(true);
                rbSelectDateMonth.setChecked(false);
                rbSelectDateYear.setChecked(false);
                break;
            }
            case 3: {
                rbSelectDateToday.setChecked(false);
                rbSelectDateWeek.setChecked(false);
                rbSelectDateMonth.setChecked(true);
                rbSelectDateYear.setChecked(false);
                break;
            }
            case 4: {
                rbSelectDateToday.setChecked(false);
                rbSelectDateWeek.setChecked(false);
                rbSelectDateMonth.setChecked(false);
                rbSelectDateYear.setChecked(true);
                break;
            }
            case 5: {
                rbSelectDateToday.setChecked(false);
                rbSelectDateWeek.setChecked(false);
                rbSelectDateMonth.setChecked(false);
                rbSelectDateYear.setChecked(false);
                break;
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dismissAllowingStateLoss();
    }

    public interface DialogSelectDateListener {
        void doneSelect(String dFrom, String dTo);
    }

}

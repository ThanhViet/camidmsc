package com.metfone.selfcare.module.keeng.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.Topic;

import java.io.Serializable;

public class RestTopicModel extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 6375253896134399858L;

    @SerializedName("data")
    public Topic data;

    public Topic getData() {
        return data;
    }

    public void setData(Topic data) {
        this.data = data;
    }

}

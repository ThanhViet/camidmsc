/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.listener;

import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;

public interface OnDataChallengeListener {

    void onClickChooseContact(PhoneNumber item);

    void onClickChooseNonContact(String item);

    void onClickInviteDataPackage(DataChallenge item);

}

package com.metfone.selfcare.module.home_kh.tab.adapter.banner;

import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;

public class KhHomeGameBannerHolder extends BaseAdapter.ViewHolder {
    public KhHomeGameBannerHolder(View view, TabHomeKhListener.OnAdapterClick listener) {
        super(view);
        view.setOnClickListener((v)-> listener.onGameItemClicked());
    }

}

package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.module.home_kh.fragment.rewards.DetailType;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsRedeemPointRequest extends BaseRequest<WsRedeemPointRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends Request {
        @SerializedName("giftId")
        String giftId;
        @SerializedName("pointAmount")
        String pointAmount;
        @SerializedName("isdn")
        String isdn;
        @SerializedName("datatype")
        String datatype;
        @SerializedName("transferType")
        String transferType;
        @SerializedName("language")
        String language;

        public WsRequest(String giftId, String pointAmount, String isdn, String datatype, String transferType, String language) {
            this.giftId = giftId;
            this.pointAmount = pointAmount;
            this.isdn = isdn;
            this.datatype = datatype;
            this.transferType = transferType;
            this.language = language;
        }


        public WsRequest(String isdn, DetailType type, int point) {
            this.isdn = isdn;
            this.pointAmount = String.valueOf(point);
            this.transferType = "POINT";
            this.language = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
            this.giftId = type.giftId;
            this.datatype = type.dataType;
        }
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.home_kh.tab.adapter.customtopic;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;

import butterknife.BindView;

public class MovieHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.img_movie)
    @Nullable
    ImageView imgMovie;
    @BindView(R.id.card_view)
    @Nullable
    ViewGroup viewRoot;

    private OnClickContentMovie listener;
    private Activity activity;

    public MovieHolder(View view, Activity activity, OnClickContentMovie listener) {
        super(view);
        this.listener = listener;
        this.activity = activity;

        ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
        layoutParams.width = TabHomeUtils.getWidthKhHomeTopic();
        viewRoot.setLayoutParams(layoutParams);
        viewRoot.requestLayout();
    }

    @Override
    public void bindData(Object item, int position) {
        KhHomeMovieItem homeData = (KhHomeMovieItem) item;
        Glide.with(activity)
                .load(homeData.poster_path).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                .into(imgMovie);
        imgMovie.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) {
                    listener.onClickMovieItem(homeData, getAdapterPosition());
                }
            }
        });
    }
}
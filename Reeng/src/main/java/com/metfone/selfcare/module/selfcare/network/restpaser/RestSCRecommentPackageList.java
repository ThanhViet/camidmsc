package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCRecommentPackage;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCRecommentPackageList extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("content")
    private ArrayList<SCRecommentPackage> data;

    public ArrayList<SCRecommentPackage> getData() {
        return data;
    }

    public void setData(ArrayList<SCRecommentPackage> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCRecommentPackage [data=" + data + "] errror " + getErrorCode();
    }
}

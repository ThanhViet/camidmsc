package com.metfone.selfcare.module.gameLive.network.parse;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.gameLive.model.WinnerModel;

import java.io.Serializable;
import java.util.ArrayList;

public class RestWinner extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<WinnerModel> result = new ArrayList<>();

    public ArrayList<WinnerModel> getResult() {
        return result;
    }

    public void setResult(ArrayList<WinnerModel> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "RestWinner{" +
                "result='" + result + '\'' +
                '}';
    }
}

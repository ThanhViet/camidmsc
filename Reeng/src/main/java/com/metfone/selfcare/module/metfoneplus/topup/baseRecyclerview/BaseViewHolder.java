package com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public abstract class BaseViewHolder extends RecyclerView.ViewHolder {

    public ItemViewClickListener itemViewClickListener;
    public List<?> mData;

    public BaseViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView);
        this.itemViewClickListener = itemViewClickListener;
    }
    
    public abstract void initViewHolder(View v);

    public abstract void onBinViewHolder(List<?> obj, int pos, Context context);

    public abstract void onItemViewClick(View v, int pos);

}

package com.metfone.selfcare.module.newdetails.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class EventModel implements Serializable {
    @SerializedName("CategoryID")
    @Expose
    private int categoryID;

    @SerializedName("EventDesc")
    @Expose
    private String eventDesc;

    @SerializedName("EventImage")
    @Expose
    private String eventImage;

    @SerializedName("EventName")
    @Expose
    private String eventName;

    @SerializedName("ParentID")
    @Expose
    private String parentID;

    @SerializedName("data")
    @Expose
    private ArrayList<NewsModel> data = new ArrayList<>();

    private final static long serialVersionUID = -7706989715033118800L;

    @Override
    public String toString() {
        return "EventModel{" +
                "categoryID=" + categoryID +
                ", eventDesc='" + eventDesc + '\'' +
                ", eventImage='" + eventImage + '\'' +
                ", eventName='" + eventName + '\'' +
                ", parentID='" + parentID + '\'' +
                ", data=" + data +
                '}';
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public ArrayList<NewsModel> getData() {
        return data;
    }

    public void setData(ArrayList<NewsModel> data) {
        this.data = data;
    }
}

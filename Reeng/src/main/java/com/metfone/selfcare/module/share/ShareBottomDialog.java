/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/20
 *
 */

package com.metfone.selfcare.module.share;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import android.graphics.Point;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import static com.google.android.material.bottomsheet.BottomSheetBehavior.from;
import static com.metfone.selfcare.util.Utilities.getScreenHeight;

public class ShareBottomDialog extends BottomSheetDialog {
    private KeyboardVisibilityEventListener keyboardVisibilityListener;
    private Unregistrar unregistrar;
    protected BaseSlidingFragmentActivity activity;

    public ShareBottomDialog(@NonNull Context context) {
        super(context, R.style.style_share_content_dialog);
        if (getWindow() != null) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            getWindow().setDimAmount(0.7f);
        }
        if (context instanceof BaseSlidingFragmentActivity) {
            activity = (BaseSlidingFragmentActivity) context;
        }
    }

    public ShareBottomDialog(@NonNull Context context, int theme) {
        super(context, theme);
    }

    protected ShareBottomDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void setKeyboardVisibilityListener(KeyboardVisibilityEventListener listener) {
        this.keyboardVisibilityListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int screenHeight = getScreenHeight(getContext());
        int dialogHeight;
        if (screenHeight != 0) {
            dialogHeight = screenHeight * 2 / 3;
        } else {
            dialogHeight = ViewGroup.LayoutParams.MATCH_PARENT;
        }

        Window window = getWindow();
        assert window != null;
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, dialogHeight);
        FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        if(bottomSheet != null){
            ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();
            if(layoutParams != null){
                layoutParams.height = Utilities.getScreenHeight(getContext()) *2/3;
                bottomSheet.setLayoutParams(layoutParams);
            }
        }
    }

    @Override
    public void show() {
        super.show();
        if (activity != null && keyboardVisibilityListener != null) {
            unregistrar = KeyboardVisibilityEvent.registerEventListener(activity, keyboardVisibilityListener);
        }
    }

    @Override
    public void dismiss() {
        Log.d("ShareBottomDialog", "dismiss");
        if (unregistrar != null) unregistrar.unregister();
        super.dismiss();
    }

    @Nullable
    public BottomSheetBehavior getBottomSheetBehavior() {
        BottomSheetBehavior behavior = null;
        try {
            FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
            if (bottomSheet != null) behavior = from(bottomSheet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return behavior;
    }

    public void setHeight(int height){
        FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        if(bottomSheet != null){
            ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();
            if(layoutParams != null){
                layoutParams.height = height;
                bottomSheet.setLayoutParams(layoutParams);
                bottomSheet.requestLayout();
            }
        }
    }

}

package com.metfone.selfcare.module.netnews.event;

/**
 * Created by HaiKE on 6/15/17.
 */

public class FontSizeEvent {

    public int getFontSize() {
        return type;
    }

    private int type;

    public FontSizeEvent(int type) {
        this.type = type;
    }
}

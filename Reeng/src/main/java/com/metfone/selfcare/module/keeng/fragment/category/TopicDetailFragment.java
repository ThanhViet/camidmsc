package com.metfone.selfcare.module.keeng.fragment.category;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;
import java.util.Random;

public class TopicDetailFragment extends BaseFragment implements OnPageChangeListener, AppBarLayout.OnOffsetChangedListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Topic currentTopic;
    private ViewPagerDetailAdapter adapter;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout appBarLayout;
    private ImageView imvCover;
    private ImageView btnShuffle;
    private View btnPlay;
    private ProgressBar progressBar;
    private Constants.State mCurrentState = Constants.State.IDLE;
    private int currentTab = -1;
    private boolean hasSong;

    public static TopicDetailFragment newInstance() {
        Bundle args = new Bundle();
        TopicDetailFragment fragment = new TopicDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "TopicDetailFragment";
    }

    @Override
    public void onResume() {
        super.onResume();
        if (currentTab >= 0)
            setCurrentItemViewPage(currentTab);
    }

    public void setCurrentItemViewPage(int index) {
        try {
            viewPager.setCurrentItem(index);
//            tabLayout.getTabAt(index).select();
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_topic_detail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        progressBar = view.findViewById(R.id.progress_bar);
        toolbar = view.findViewById(R.id.toolbar);
        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.viewpager);
        toolbar = view.findViewById(R.id.main_toolbar);
        collapsingToolbarLayout = view.findViewById(R.id.main_collapsing);
        appBarLayout = view.findViewById(R.id.main_appbar);
        appBarLayout.addOnOffsetChangedListener(this);
        imvCover = view.findViewById(R.id.imvCover);
        btnShuffle = view.findViewById(R.id.btnShuffle);
        btnPlay = view.findViewById(R.id.button_play);

        return view;
    }

    private void setupNavigation() {
        if (collapsingToolbarLayout == null)
            return;
        collapsingToolbarLayout.setTitleEnabled(false);
        toolbar.setNavigationIcon(mActivity.getResources().getDrawable(R.drawable.ic_v5_back_white));
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        if (appBarLayout != null) {
            appBarLayout.setExpanded(true);
        }
    }

    private void loadInfo() {
        progressBar.setVisibility(View.VISIBLE);
        new KeengApi().getInfoTopic(currentTopic.getId(), response -> {
            currentTopic = response.getData();
            if (currentTopic == null) {
                onBackPressed();
            } else {
                initViewPager();
            }

        }, error -> {
            Log.e(TAG, error);
            initViewPager();
        });
    }

    private void initViewPager() {
        try {
            progressBar.setVisibility(View.GONE);
            setupViewPager(viewPager);
//            TypefaceUtils.changeTabsFont(mActivity, tabLayout);
            if (currentTopic != null) {
                ImageBusiness.setCover(currentTopic.getCover(), imvCover, (int) currentTopic.getId());
                btnShuffle.setOnClickListener(view -> {
                    if (currentTab == 0 && adapter != null && mActivity != null) {
                        try {
                            Fragment fragment = adapter.getItem(currentTab);
                            if (fragment instanceof SongTopicFragment) {
                                List<AllModel> list = ((SongTopicFragment) fragment).getDatas();
                                if (!list.isEmpty()) {
                                    PlayingList playingList = new PlayingList(list, PlayingList.TYPE_TOPIC, MediaLogModel.SRC_TOPIC);
                                    playingList.setId(currentTopic.getId());
                                    playingList.setName(currentTopic.getName());
                                    int position = new Random().nextInt(list.size());
                                    mActivity.setMediaPlayingAudioWithState(playingList, position, Constants.PLAY_MUSIC.REPEAT_All, Constants.PLAY_MUSIC.REPEAT_SUFF);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                        }
                    }
                });
                if (btnPlay != null) {
                    btnPlay.setOnClickListener(view -> {
                        if (currentTab == 0 && adapter != null && mActivity != null) {
                            try {
                                Fragment fragment = adapter.getItem(currentTab);
                                if (fragment instanceof SongTopicFragment) {
                                    List<AllModel> list = ((SongTopicFragment) fragment).getDatas();
                                    if (!list.isEmpty()) {
                                        PlayingList playingList = new PlayingList(list, PlayingList.TYPE_TOPIC, MediaLogModel.SRC_TOPIC);
                                        playingList.setId(currentTopic.getId());
                                        playingList.setName(currentTopic.getName());
                                        mActivity.setMediaPlayingAudioWithState(playingList, 0, Constants.PLAY_MUSIC.REPEAT_All, Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(TAG, e);
                            }
                        }
                    });
                }
            }
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            if (getArguments() != null) {
                currentTopic = (Topic) getArguments().getSerializable(Constants.KEY_DATA);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        if (currentTopic == null) {
            onBackPressed();
            return;
        }
        setupNavigation();
        new Handler().postDelayed(this::loadInfo, 250);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerDetailAdapter(getChildFragmentManager());
        int count = 0;
        if (currentTopic != null) {
            if (currentTopic.getTotalSongs() > 0) {
                adapter.addFragment(SongTopicFragment.newInstance(currentTopic, Constants.TYPE_SONG), getString(R.string.song));
                count++;
                hasSong = true;
            } else
                hasSong = false;
            if (currentTopic.getTotalVideos() > 0) {
                adapter.addFragment(ChildTopicDetailFragment.instance(currentTopic, Constants.TYPE_VIDEO), getString(R.string.video_mv));
                count++;
            }
            if (currentTopic.getTotalAlbums() > 0) {
                adapter.addFragment(ChildTopicDetailFragment.instance(currentTopic, Constants.TYPE_ALBUM), getString(R.string.album_keeng));
                count++;
            }
        }
        if (count > 1) {
            tabLayout.setVisibility(View.VISIBLE);
            viewPager.setOffscreenPageLimit(count - 1);
        } else {
            tabLayout.setVisibility(View.GONE);
        }
        if (count == 0) {
            adapter.addFragment(EmptyFragment.newInstance(), "");
        }
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        currentTab = position;
        if (currentTab == 0 && hasSong && (Float.compare(positionOffset, 0f) == 0)) {
            btnShuffle.setVisibility(View.VISIBLE);
            if (mCurrentState == Constants.State.EXPANDED)
                btnPlay.setVisibility(View.VISIBLE);
            else
                btnPlay.setVisibility(View.GONE);
        } else {
            btnShuffle.setVisibility(View.GONE);
            btnPlay.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        if (offset == 0) {
            if (mCurrentState != Constants.State.EXPANDED) {
                if (currentTab == 0 && hasSong) {
                    btnPlay.setVisibility(View.VISIBLE);
                } else {
                    btnPlay.setVisibility(View.GONE);
                }
            }
            mCurrentState = Constants.State.EXPANDED;
        } else if (Math.abs(offset) >= appBarLayout.getTotalScrollRange()) {
            if (mCurrentState != Constants.State.COLLAPSED) {
                btnPlay.setVisibility(View.GONE);
            }
            mCurrentState = Constants.State.COLLAPSED;
        } else {
            if (mCurrentState != Constants.State.IDLE) {
                btnPlay.setVisibility(View.GONE);
            }
            mCurrentState = Constants.State.IDLE;
        }
    }

    @Override
    public void onDetach() {
        if (viewPager != null)
            viewPager.setAdapter(null);
        adapter = null;
        App.getInstance().cancelPendingRequests(KeengApi.GET_INFO_TOPIC);
        super.onDetach();
    }
}

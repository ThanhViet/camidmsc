package com.metfone.selfcare.module.livestream.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.module.livestream.listener.MessageActionListener;
import com.metfone.selfcare.module.livestream.model.LiveStreamMessage;
import com.metfone.selfcare.ui.TagTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by thanhnt72 on 8/15/2019.
 */

public class MsgNormalHolder extends BaseMessageLiveStreamHolder {

    private BaseSlidingFragmentActivity activity;
    private MessageActionListener listenerMsg;
    private ApplicationController app;

    private View viewAvatar;
    private CircleImageView ivAvatar;
    private TextView tvAvatar;
    private TextView tvName;
    private View layoutReply;
    private TextView tvQuoteName;
    private TextView tvQuoteContent;
    private TextView tvNumberLike;
    private ImageView ivLikeCmt;
    private TagTextView tvContent;
    private TextView tvTime;
    private TextView tvReply;

    private LiveStreamMessage message;
    private ReengAccount account;
    private TagMocha.OnClickTag onClickTag;
    private SmartTextClickListener smartTextClickListener;

    public MsgNormalHolder(View itemView, BaseSlidingFragmentActivity act, MessageActionListener listener
            , TagMocha.OnClickTag onClickTag, SmartTextClickListener smartTextClickListener) {
        super(itemView);
        this.activity = act;
        this.app = (ApplicationController) activity.getApplication();
        account = app.getReengAccountBusiness().getCurrentAccount();
        this.listenerMsg = listener;
        this.onClickTag = onClickTag;
        this.smartTextClickListener = smartTextClickListener;

        viewAvatar = itemView.findViewById(R.id.viewAvatar);
        ivAvatar = itemView.findViewById(R.id.ivAvatar);
        tvAvatar = itemView.findViewById(R.id.tvAvatar);
        tvName = itemView.findViewById(R.id.tvName);
        layoutReply = itemView.findViewById(R.id.layoutReply);
        tvQuoteName = itemView.findViewById(R.id.tvQuoteName);
        tvQuoteContent = itemView.findViewById(R.id.tvQuoteContent);
        tvNumberLike = itemView.findViewById(R.id.tvNumberLike);
        ivLikeCmt = itemView.findViewById(R.id.ivLikeCmt);
        tvContent = itemView.findViewById(R.id.tvContent);
        tvContent.setMaxLines(10);
        tvTime = itemView.findViewById(R.id.tvTime);
        tvReply = itemView.findViewById(R.id.tvReply);
    }

    @Override
    public void setElement(Object obj, final int pos) {
        message = (LiveStreamMessage) obj;
        String content = message.getContent();
        if (TextUtils.isEmpty(content)) {
            tvContent.setText(content);
        } else {
            if (Utilities.notEmpty(message.getListTag())) {
                tvContent.setEmoticonWithTag(app, content, content.hashCode(), content, message.getListTag(), onClickTag, smartTextClickListener);
            } else {
                tvContent.setEmoticon(app, content, content.hashCode(), content, smartTextClickListener);
            }
        }

        long deltaTimeServer = message.getTimeServer() - System.currentTimeMillis();
        tvTime.setText(TimeHelper.caculateTimeFeed(tvTime.getContext(), message.getTimeStamp(), deltaTimeServer));
        int sizeAvatar = (int) activity.getResources().getDimension(R.dimen.avatar_small_size);

        if (message.getMsisdn() != null) {
            if (message.getMsisdn().equals(account.getJidNumber())) {
                tvName.setText(account.getName());
                app.getAvatarBusiness().setMyAvatar(ivAvatar, tvAvatar, null, account, null);
            } else {
                if (!message.isGetContactPhoneDone()) {
                    message.setPhoneNumber(app.getContactBusiness().getPhoneNumberFromNumber(message.getMsisdn()));
                    message.setGetContactPhoneDone(true);
                }

                PhoneNumber phoneNumber = message.getPhoneNumber();
                if (phoneNumber == null) {
                    if (TextUtils.isEmpty(message.getNameSender()))
                        tvName.setText(Utilities.hidenPhoneNumber(message.getMsisdn()));
                    else
                        tvName.setText(message.getNameSender());
                    String mFriendAvatarUrl = app.getAvatarBusiness().getAvatarUrl(message.getLastAvatar(),
                            message.getMsisdn(), sizeAvatar);
                    app.getAvatarBusiness().setAvatarOnMedia(ivAvatar, tvAvatar,
                            mFriendAvatarUrl, message.getMsisdn(), message.getNameSender(), sizeAvatar);
                } else {
                    tvName.setText(phoneNumber.getName());
                    app.getAvatarBusiness().setPhoneNumberAvatar(ivAvatar, tvAvatar, phoneNumber, sizeAvatar);
                }
            }
        }

        LiveStreamMessage quotedMessage = message.getQuotedMessage();
        if (quotedMessage == null)
            layoutReply.setVisibility(View.GONE);
        else {
            if (quotedMessage.getMsisdn().equals(account.getJidNumber())) {
                tvQuoteName.setText(account.getName());
            } else {
                if (!quotedMessage.isGetContactPhoneDone()) {
                    quotedMessage.setPhoneNumber(app.getContactBusiness().getPhoneNumberFromNumber(quotedMessage.getMsisdn()));
                    quotedMessage.setGetContactPhoneDone(true);
                }
                PhoneNumber phoneNumQuote = quotedMessage.getPhoneNumber();
                if (phoneNumQuote == null)
                    if (TextUtils.isEmpty(quotedMessage.getNameSender()))
                        tvQuoteName.setText(Utilities.hidenPhoneNumber(quotedMessage.getMsisdn()));
                    else
                        tvQuoteName.setText(quotedMessage.getNameSender());
                else tvQuoteName.setText(phoneNumQuote.getName());

            }
            tvQuoteContent.setText(quotedMessage.getContent());
            layoutReply.setVisibility(View.VISIBLE);
        }

        if (message.getCountLike() > 0) {
            tvNumberLike.setText(message.getCountLike() + "");
            tvNumberLike.setVisibility(View.VISIBLE);
        } else {
            tvNumberLike.setText("");
            tvNumberLike.setVisibility(View.GONE);
        }
        if (message.isLike())
            ivLikeCmt.setImageResource(R.drawable.ic_onmedia_like_press);
        else
            ivLikeCmt.setImageResource(R.drawable.ic_onmedia_like);

        tvReply.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listenerMsg != null) listenerMsg.onReplyMessage(message, pos);
            }
        });

        ivLikeCmt.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (app.getReengAccountBusiness().isAnonymousLogin()) {
                    activity.showDialogLogin();
                    return;
                }

                message.setLike(message.isLike() ? 0 : 1);
                if (message.isLike()) {
                    ivLikeCmt.setImageResource(R.drawable.ic_onmedia_like_press);
                    message.setCountLike(message.getCountLike() + 1);
                } else {
                    ivLikeCmt.setImageResource(R.drawable.ic_onmedia_like);
                    message.setCountLike(message.getCountLike() - 1);
                    if (message.getCountLike() < 0)
                        message.setCountLike(0);
                }
                if (message.getCountLike() > 0) {
                    tvNumberLike.setText(message.getCountLike() + "");
                    tvNumberLike.setVisibility(View.VISIBLE);
                } else {
                    tvNumberLike.setText("");
                    tvNumberLike.setVisibility(View.GONE);
                }

                if (listenerMsg != null)
                    listenerMsg.onClickLikeMessage(message, pos);
            }
        });

        viewAvatar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listenerMsg != null)
                    listenerMsg.onClickUser(message, pos);
            }
        });

        tvQuoteContent.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (tvQuoteContent.getMaxLines() == 1)
                    tvQuoteContent.setMaxLines(10);
                else
                    tvQuoteContent.setMaxLines(1);
            }
        });
    }
}

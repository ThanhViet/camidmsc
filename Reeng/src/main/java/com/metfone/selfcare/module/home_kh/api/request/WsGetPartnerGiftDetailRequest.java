package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Data;

public class WsGetPartnerGiftDetailRequest extends BaseRequest<WsGetListGiftReceivedRequest.Request> {
    public static class Request {
    }
    public static class WsRequest extends WsGetListGiftReceivedRequest.Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("language")
        String language;
        @SerializedName("latitude")
        String latitude;
        @SerializedName("longitude")
        String longitude;
        @SerializedName("giftId")
        String giftId;
        @SerializedName("custId")
        public String custId;

        public WsRequest(String isdn, String language, String latitude, String longitude, String giftId) {
            this.isdn = isdn;
            this.language = language;
            this.latitude = latitude;
            this.longitude = longitude;
            this.giftId = giftId;
        }

        public WsRequest(String isdn, String giftId) {
            this.isdn = isdn;
            this.giftId = giftId;
            this.language = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
            this.latitude = "11.5503862";
            this.longitude = "104.9045065";
        }
    }
}

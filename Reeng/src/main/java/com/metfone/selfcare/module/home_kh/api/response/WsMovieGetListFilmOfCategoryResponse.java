package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.movienew.model.ActionFilmModel;

public class WsMovieGetListFilmOfCategoryResponse {
    public String errorCode;
    public String errorMessage;
    @SerializedName("result")
    public ActionFilmModel response;
}

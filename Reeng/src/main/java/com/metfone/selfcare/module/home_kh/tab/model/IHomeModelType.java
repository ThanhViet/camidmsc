package com.metfone.selfcare.module.home_kh.tab.model;


public interface IHomeModelType {

    int BANNER = 0;
    int GIFT = 2;
    int GAME_ITEM = 1;
    int LUCKY_GAME_ITEM = 3;
    int POPULAR_LIVE_CHANNEL = 4;
    int CUSTOM_TOPIC = 5;
    int MP_SERVICE = 6;
    int REWARD_CATEGORY = 7;
    int REWARD_TOPIC_1 = 8;
    int REWARD_TOPIC_2 = 9;

    int getItemType();
}

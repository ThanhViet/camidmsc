package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SCFeatures implements Serializable {

    @SerializedName("content")
    private ArrayList<SCDeeplink> content;

    @SerializedName("number")
    private String number;

    @SerializedName("size")
    private String size;

    @SerializedName("numberOfElements")
    private String numberOfElements;

    @SerializedName("totalPages")
    private String totalPages;

    @SerializedName("totalElements")
    private String totalElements;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(String numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public String getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(String totalElements) {
        this.totalElements = totalElements;
    }

    public ArrayList<SCDeeplink> getContent() {
        return content;
    }

    public void setContent(ArrayList<SCDeeplink> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "SCFeatures{" +
                "content=" + content +
                ", number='" + number + '\'' +
                ", size='" + size + '\'' +
                ", numberOfElements='" + numberOfElements + '\'' +
                ", totalPages='" + totalPages + '\'' +
                ", totalElements='" + totalElements + '\'' +
                '}';
    }
}

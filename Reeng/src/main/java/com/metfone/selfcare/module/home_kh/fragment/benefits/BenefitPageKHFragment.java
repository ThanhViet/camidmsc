package com.metfone.selfcare.module.home_kh.fragment.benefits;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.setting.BenefitAdapter;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.model.account.BenefitModel;
import com.metfone.selfcare.module.home_kh.api.response.GetRankingListDetailResponse;
import com.metfone.selfcare.module.home_kh.model.RankDefine;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

//0882989999 / 123456
public class BenefitPageKHFragment extends BaseFragment  {


    public static final String TAG = BenefitPageKHFragment.class.getSimpleName();
    private static final String KEY_DATA = "RANK_DEFINE";
    private final int RANK_MEMBER = 1;
    private final int RANK_SILVER = 2;
    private final int RANK_GOLD = 3;
    private final int RANK_DIAMOND = 4;
    private final int RANK_PLATINUM = 5;
    @BindView(R.id.rvBenefit)
    RecyclerView rvBenefit;
    private Unbinder unbinder;
    private List<RankDefine> listRank = new ArrayList<>();
    private RankDefine rankDefine;
    BenefitsKHFragment benefitsKHFragment;

    public static BenefitPageKHFragment newInstance(RankDefine rankDefine) {
        BenefitPageKHFragment fragment = new BenefitPageKHFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_DATA, rankDefine);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        initData();
        return view;
    }

    private void initData() {
        rvBenefit.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvBenefit.setLayoutManager(layoutManager);
        ArrayList<RankDefine.BenefitDetail> arraylist = new ArrayList<>();
        rankDefine = (RankDefine) getArguments().getSerializable(KEY_DATA);
        if(rankDefine != null && rankDefine.getBenefitDetails() != null){
            arraylist.addAll(rankDefine.getBenefitDetails());
        }
        BenefitAdapter benefitAdapter = new BenefitAdapter(arraylist, getContext());
        rvBenefit.setAdapter(benefitAdapter);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_benefit_page_kh;
    }
}
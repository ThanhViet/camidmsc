package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.movienew.model.Category;

import java.util.List;

public class WsGetMovieCategoryV2Response {
    public String errorCode;
    public String errorMessage;
    @SerializedName("result")
    public Response response;

    public static class Response{
        @SerializedName("result")
        public List<Category> result;
    }
}

package com.metfone.selfcare.module.keeng.fragment.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.adapter.home.MusicHomeAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MusicHomeModel;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.model.RankModel;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.LoadingView;
import com.metfone.selfcare.util.Utilities;
import com.vtm.adslib.AdsHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MusicHomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, AbsInterface.OnItemListener {

    private boolean isLoading;
    private List<MusicHomeModel> mDatas;
    private int errorCount = 0;
    private LinearLayoutManager mLayoutManager;
    private MusicHomeAdapter mAdapter;
    private ListenerUtils listenerUtils;

    private ViewStub viewStub;
    private LoadingView mLoadingView;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;

    private View viewActionBar;
    private View btnBack;
    private View tvTabName;
    private View btnSearch;
    private AdsHelper adsHelper;

    public static MusicHomeFragment newInstance() {
        Bundle args = new Bundle();
        MusicHomeFragment fragment = new MusicHomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "MusicHomeFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_music_home;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        long time = System.currentTimeMillis();
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        viewStub = view.findViewById(R.id.vStub);
        viewActionBar = view.findViewById(R.id.layout_action_bar);
        btnBack = view.findViewById(R.id.iv_back);
        tvTabName = view.findViewById(R.id.tv_tab_name);
        btnSearch = view.findViewById(R.id.iv_search);
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
        if (needLoadOnCreate) {
            com.metfone.selfcare.util.Log.i(TAG, "load on create");
            setUserVisibleHint(true);
            initViewStub();
        }
        com.metfone.selfcare.util.Log.d(TAG, "[perform] onCreateView: " + (System.currentTimeMillis() - time));
        return view;
    }

    boolean initViewStubDone = false;

    private void initViewStub() {
        if (initViewStubDone) return;
        com.metfone.selfcare.util.Log.i(TAG, "initViewStub");

        View view = viewStub.inflate();
        mRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mLoadingView = view.findViewById(R.id.loading_view);

        if (mDatas == null) mDatas = new ArrayList<>();
        mRefreshLayout.setColorSchemeColors(mActivity.getResources().getColor(R.color.colorPrimary));
        mRefreshLayout.setOnRefreshListener(this);
        mLayoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setInitialPrefetchItemCount(11);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(11);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MusicHomeAdapter(mActivity, mDatas, this);
        mRecyclerView.setAdapter(mAdapter);

        adsHelper = new AdsHelper(mActivity);
        if(AdsUtils.checkShowAds())
        {
            String adUnit = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_PLAYER_MUSIC);
            adsHelper.init(adUnit, AdSize.MEDIUM_RECTANGLE, new AdsHelper.AdsBannerListener() {
                @Override
                public void onAdShow(AdView adView) {
                    mAdapter.notifyDataSetChanged();
                }
            });
            mAdapter.setAdsHelper(adsHelper);
        }

//        if (canLazyLoad()) {
        isDataInitiated = true;
        loadingBegin();
        new Handler().postDelayed(this::doLoadData, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
//        }

        initViewStubDone = true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof ModuleActivity)
            needLoadOnCreate = true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mActivity instanceof ModuleActivity) mActivity.finish();
                else if (mActivity != null) mActivity.onBackPressed();
            }
        });
        btnSearch.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                Utilities.openSearch(mActivity, Constants.TAB_MUSIC_HOME);
            }
        });

        if (mActivity instanceof HomeActivity) {
            btnBack.setVisibility(View.GONE);
            Utilities.setMargins(tvTabName, Utilities.dpToPixel(R.dimen.v5_spacing_normal, getResources()), tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), Utilities.dpToPixel(R.dimen.v5_margin_top_action_bar_home, getResources()), viewActionBar.getRight(), viewActionBar.getBottom());
        } else {
            btnBack.setVisibility(View.VISIBLE);
            Utilities.setMargins(tvTabName, 0, tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), 0, viewActionBar.getRight(), viewActionBar.getBottom());
        }
    }

    private boolean needLoadOnCreate = false;           //dung cho deeplink

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && getView() == null && getActivity() == null) {
            needLoadOnCreate = true;
            return;
        }
        if (isVisibleToUser && getActivity() instanceof HomeActivity) {

            if (getView() == null)
                needLoadOnCreate = true;
            else {
//                ChangeABColorHelper.changeTabColor(bgHeader, TabHomeHelper.HomeTab.tab_music, null,
//                        ((HomeActivity) mActivity).getCurrentTab(), ((HomeActivity) mActivity).getCurrentColorTabWap());
                initViewStub();
            }
        }

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(this::doLoadData, 1000);
    }

    private void doLoadData() {
        if (!isLoading) {
            loadData();
            if(adsHelper != null && AdsUtils.checkShowAds())
                adsHelper.loadAd();
        }
    }

    private void loadData() {
        isLoading = true;
        App.getInstance().cancelPendingRequests(KeengApi.GET_HOME_V5);
//        final long userId = LoginObject.getId(mActivity);
//        final long timeRequest = DateTimeUtils.getEpochTime();

        final long startTime = System.currentTimeMillis();
        new KeengApi().getHomeNew(result -> {
            long endTime = System.currentTimeMillis();

            isLoading = false;
//            long time = DateTimeUtils.getEpochTime() - timeRequest;
            if (mDatas == null) mDatas = new ArrayList<>();
            else mDatas.clear();
            List<MusicHomeModel> list = result.getData();
            for (int i = 0; i < list.size(); i++) {
                MusicHomeModel item = list.get(i);
                if (item != null && !item.isEmpty()) {
                    mDatas.add(item);
                }
            }

//                if (mDatas.size() >= 3) {
//                    DeepLinkDataSource dataSource = DeepLinkDataSource.getInstance();
//                    DeepLinkModel banner = dataSource.getItemByPageIndex(DeepLink.PAGE_INDEX_TAB_MUSIC);
//                    if (banner != null) {
//                        MusicHomeModel item = new MusicHomeModel();
//                        item.setType(Constants.MUSIC_HOME_TYPE_BANNER);
//                        item.setBanner(banner);
//                        mDatas.add(3, item);
//                    }
//                }

            hideRefresh();
            mAdapter.notifyDataSetChanged();
            if (mDatas.isEmpty())
                loadingEmpty();
            else
                loadingFinish();

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MUSIC_GET_HOME, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
//                Utilities.logKQI("getHomeV5", "/KeengWSRestful/ws/common/getHomeV5", timeRequest, userId, time);
        }, error -> {
            long endTime = System.currentTimeMillis();

            isLoading = false;
            if (errorCount < RecyclerFragment.MAX_ERROR_RETRY) {
                errorCount++;
                new Handler().postDelayed(this::loadData, Constants.TIME_DELAY_RETRY);
                return;
            }
            hideRefresh();
            isLoading = false;
            errorCount = 0;
//                long time = -1;
            if (mDatas == null || mDatas.isEmpty()) {
                loadingError(v -> {
                    loadingBegin();
                    doLoadData();
                });
            }

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MUSIC_GET_HOME, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
//                Utilities.logKQI("getHomeV5", "/KeengWSRestful/ws/common/getHomeV5", timeRequest, userId, time);
        });
    }

    public void loadingBegin() {
        if (mLoadingView != null) {
            mLoadingView.loadBegin();
        }
    }

    protected void hideRefresh() {
        if (mRefreshLayout != null) {
            mRefreshLayout.setRefreshing(false);
            mRefreshLayout.destroyDrawingCache();
            mRefreshLayout.clearAnimation();
        }
    }

    public void loadingFinish() {
        if (mRefreshLayout != null && mRefreshLayout.isRefreshing()) {
            mRefreshLayout.setRefreshing(false);
        }
        if (mLoadingView != null) {
            mLoadingView.loadFinish();
        }
    }

    public void loadingError(View.OnClickListener paramOnClickListener) {
        if (mRefreshLayout != null && mRefreshLayout.isRefreshing()) {
            mRefreshLayout.setRefreshing(false);
        }
        if (mLoadingView != null) {
            mLoadingView.loadError();
            mLoadingView.setLoadingErrorListener(paramOnClickListener);
        }
    }

    public void loadingEmpty() {
        if (mRefreshLayout != null && mRefreshLayout.isRefreshing()) {
            mRefreshLayout.setRefreshing(false);
        }
        if (mDatas != null && !mDatas.isEmpty()) {
            if (mLoadingView != null)
                mLoadingView.loadFinish();
            return;
        }
        if (mLoadingView != null) {
            mLoadingView.loadEmpty();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventHelper.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventHelper.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
//        mInstance = null;
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        if(adsHelper != null)
            adsHelper.onDestroy();
        super.onDestroyView();
    }

    public boolean recyclerScrollToPosition(int position) {
        if (mLayoutManager != null)
            try {
                mLayoutManager.scrollToPositionWithOffset(position, 0);
                return true;
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        return false;
    }

    @Override
    public void onHeaderClick(View view, String tag) {
        int type = 0;
        switch (tag) {
            case "" + Constants.TAB_VIDEO_HOT:
                type = Constants.TAB_VIDEO_HOT;
                break;
            case "" + Constants.TAB_PLAYLIST_HOT:
                type = Constants.TAB_PLAYLIST_HOT;
                break;
            case "" + Constants.TAB_ALBUM_HOT:
                type = Constants.TAB_ALBUM_HOT;
                break;
            case "" + Constants.TAB_TOPIC_HOT:
                type = Constants.TAB_TOPIC_HOT;
                break;
            case "" + Constants.TAB_TOP_HIT:
                type = Constants.TAB_TOP_HIT;
                break;
            case "" + Constants.TAB_SONG_HOT:
                type = Constants.TAB_SONG_HOT;
                break;
            case "" + Constants.TAB_SINGER_HOT:
                type = Constants.TAB_SINGER_HOT;
                break;
            case "" + Constants.TAB_YOUTUBE_HOT:
                type = Constants.TAB_YOUTUBE_HOT;
                break;
            case "" + Constants.TAB_RANK_DETAIL:
                type = Constants.TAB_RANK_DETAIL;
                break;
            default:
                break;
        }

        if (type == 0) return;
        doKeengDetail(type, null);
    }

    @SuppressWarnings("AccessStaticViaInstance")
    @Override
    public void onItemClick(AllModel item) {

        try {
            if (item != null && mActivity != null) {
                switch (item.getType()) {
                    case Constants.TYPE_SONG:
                        mActivity.playMusicSong(item, false);
                        break;
                    case Constants.TYPE_ALBUM:
                    case Constants.TYPE_ALBUM_VIDEO:
                        doKeengDetail(Constants.TAB_ALBUM_DETAIL, item);
                        break;
                    case Constants.TYPE_VIDEO:
                        mActivity.setMediaToPlayVideo(item);
                        break;
                    case Constants.TYPE_PLAYLIST:
                    case Constants.TYPE_PLAYLIST_VIDEO:
                        doKeengDetail(Constants.TAB_PLAYLIST_DETAIL, ConvertHelper.convertToPlaylist(item));
                        break;
                    case Constants.TYPE_DEEPLINK:
//                    mActivity.processDeepLink(item.getUrl(), item.getName());
                        break;
                    case Constants.TYPE_YOUTUBE:
//                    mActivity.setMediaToPlayYoutube(item);
                        break;
                    default:
                        UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia((ApplicationController) mActivity.getApplication(), mActivity, item.getUrl());
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onItemClick(PlayListModel item) {
        if (item != null && mActivity != null) {
            doKeengDetail(Constants.TAB_PLAYLIST_DETAIL, item);
        }
    }

    @Override
    public void onItemTopicClick(Topic item) {
        if (item != null && mActivity != null) {
            doKeengDetail(Constants.TAB_TOPIC_DETAIL, item);
        }
    }

    @Override
    public void onItemSingerClick(Topic item) {
        if (item != null && mActivity != null) {
            doKeengDetail(Constants.TAB_SINGLE_DETAIL, item);
        }
    }

    @Override
    public void onItemRankClick(RankModel item, int position) {
        if (item != null && mActivity != null) {
            PlayingList playList = new PlayingList();
            playList.setId(item.getType());
            playList.setSongList(item.getListSong());
            playList.setTypePlaying(item.getType());
            playList.setName(item.getTitle());
            mActivity.setMediaPlayingAudio(playList, position);
        }
    }

    @Override
    public void onItemRankHeaderClick(RankModel item) {
        doKeengDetail(Constants.TAB_RANK_DETAIL, item);
    }

    @Override
    public void onItemClickOption(AllModel item) {
        mActivity.showPopupMore(item);
    }

    @Override
    public void onItemTopHitClick(Topic item) {
        if (item != null && mActivity != null) {
            doKeengDetail(Constants.TAB_TOP_HIT_DETAIL, item);
        }
    }

    private void doKeengDetail(int type, Object data) {
        if (mActivity != null) {
            Intent intent = new Intent(mActivity, TabKeengActivity.class);
            intent.putExtra(Constants.KEY_TYPE, type);
            intent.putExtra(Constants.KEY_DATA, (Serializable) data);
            mActivity.startActivity(intent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(final TabMusicEvent event) {
        if (event.reSelected) {
            if (mLayoutManager != null && mRecyclerView != null) {
                mRecyclerView.stopScroll();
                mLayoutManager.smoothScrollToPosition(mRecyclerView, null, 0);
            }
        }
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && mRecyclerView != null && mDatas.size() == 0)
            onRefresh();
    }

    public static class TabMusicEvent {
        boolean reSelected;

        public TabMusicEvent(boolean reSelected) {
            this.reSelected = reSelected;
        }
    }
}

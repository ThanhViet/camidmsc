/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/6
 *
 */

package com.metfone.selfcare.module.share.task;

import android.os.AsyncTask;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.share.listener.GetAllContactsListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static com.metfone.selfcare.database.constant.ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;

public class GetAllContactsTask extends AsyncTask<Void, Void, ArrayList<ContactProvisional>> {
    private final String TAG = "GetAllContactsTask";
    private WeakReference<ApplicationController> mApplication;
    private GetAllContactsListener listener;
    private Comparator comparatorContacts;
    private Comparator comparatorThreadMessages;

    public GetAllContactsTask(ApplicationController application) {
        this.mApplication = new WeakReference<>(application);
    }

    public void setListener(GetAllContactsListener listener) {
        this.listener = listener;
    }

    public void setComparatorContacts(Comparator comparatorContacts) {
        this.comparatorContacts = comparatorContacts;
    }

    public void setComparatorThreadMessages(Comparator comparatorThreadMessages) {
        this.comparatorThreadMessages = comparatorThreadMessages;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) listener.onPrepareGetAllContacts();
    }

    @Override
    protected void onPostExecute(ArrayList<ContactProvisional> results) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "onPostExecute " + results.size() + " results: " + results);
        if (listener != null) listener.onFinishedGetAllContacts(results);
    }

    @Override
    protected ArrayList<ContactProvisional> doInBackground(Void... params) {
        ArrayList<ContactProvisional> list = new ArrayList<>();
        if (Utilities.notNull(mApplication)) {
            boolean addTitleContact = false;
            String myNumber = mApplication.get().getReengAccountBusiness().getJidNumber();
            MessageBusiness messageBusiness = mApplication.get().getMessageBusiness();
            ArrayList<ThreadMessage> listMessages = new ArrayList<>();
            if (Utilities.notEmpty(messageBusiness.getThreadMessageArrayList())) {
                listMessages.addAll(messageBusiness.getThreadMessageArrayList());
            }
            if (BuildConfig.DEBUG) Log.d(TAG, "listMessages: " + listMessages.size());
            ContactBusiness contactBusiness = mApplication.get().getContactBusiness();
            ArrayList<PhoneNumber> listContacts = new ArrayList<>();
            if (Utilities.notEmpty(contactBusiness.getListNumberAlls())) {
                listContacts.addAll(contactBusiness.getListNumberAlls());
            }
            if (BuildConfig.DEBUG) Log.d(TAG, "listContacts: " + listContacts.size());
            Map<String, Integer> mapMessages = new HashMap<>();
            if (Utilities.notEmpty(listMessages)) {
                if (comparatorThreadMessages != null) {
                    try {
                        Collections.sort(listMessages, comparatorThreadMessages);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                for (ThreadMessage item : listMessages) {
                    if (item != null && item.isReadyShow() && item.isJoined() && item.getHiddenThread() != 1) {
                        ContactProvisional tmp = new ContactProvisional();
                        tmp.setContact(item);
                        tmp.setState(ContactProvisional.SEND);
                        list.add(tmp);
                        if (item.getThreadType() == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(item.getSoloNumber()))
                            mapMessages.put(item.getSoloNumber(), 1);
                        addTitleContact = true;
                    }
                }
                listMessages.clear();
            }
            if (Utilities.notEmpty(listContacts)) {
                if (comparatorContacts != null) {
                    try {
                        Collections.sort(listContacts, comparatorContacts);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                for (PhoneNumber item : listContacts) {
                    if (item != null && item.getContactId() != null) {
                        String phoneNumber = item.getJidNumber();
                        if (Utilities.notEmpty(phoneNumber) && !mapMessages.containsKey(phoneNumber) && !phoneNumber.equals(myNumber)) {
                            if (addTitleContact) {
                                list.add(new ContactProvisional());
                                addTitleContact = false;
                            }
                            ContactProvisional tmp = new ContactProvisional();
                            tmp.setContact(item);
                            tmp.setState(ContactProvisional.SEND);
                            list.add(tmp);
                        }
                    }
                }
                listContacts.clear();
            }
            mapMessages.clear();
        }
        return list;
    }
}
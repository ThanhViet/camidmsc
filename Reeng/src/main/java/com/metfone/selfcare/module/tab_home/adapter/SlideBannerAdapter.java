/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/3
 *
 */

package com.metfone.selfcare.module.tab_home.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickSliderBanner;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.tab_home.holder.SlideBannerDetailHolder;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;

public class SlideBannerAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    private final int TYPE_BANNER = 1;
    private final int TYPE_BANNER_MOVIE = 2;
    private OnClickSliderBanner listener;

    public SlideBannerAdapter(Activity act, OnClickSliderBanner listener) {
        super(act);
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object object = getItem(position);
        if (object instanceof Content) {
            return TYPE_BANNER;
        } else if (object instanceof TabHomeModel) {
            int type = ((TabHomeModel) object).getType();
            if (type == TabHomeModel.TYPE_SLIDER_BANNER)
                return TYPE_BANNER;
        } else if (object instanceof Movie) {
            return TYPE_BANNER_MOVIE;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public BaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_BANNER:
                return new SlideBannerDetailHolder(layoutInflater.inflate(R.layout.holder_tab_home_slide_banner_detail, parent, false), listener);
            case TYPE_BANNER_MOVIE:
                return new SlideBannerDetailHolder(layoutInflater.inflate(R.layout.holder_slider_banner_movie_detail, parent, false), listener);
            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseAdapter.ViewHolder holder, int position) {
        holder.bindData(getItem(position), position);
    }
}

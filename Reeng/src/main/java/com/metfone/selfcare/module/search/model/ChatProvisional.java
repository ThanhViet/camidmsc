/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class ChatProvisional extends Provisional {
    private ArrayList<PhoneNumber> listContacts;
    private ArrayList<Object> listChat;

    public ChatProvisional() {
    }

    public ArrayList<PhoneNumber> getListContacts() {
        return listContacts;
    }

    public void setListContacts(ArrayList<PhoneNumber> listContacts) {
        this.listContacts = listContacts;
    }

    public ArrayList<Object> getListChat() {
        return listChat;
    }

    public void setListChat(ArrayList<Object> listChat) {
        this.listChat = listChat;
    }

    @Override
    public int getSize() {
        int size = 0;
        if (Utilities.notEmpty(listChat)) size++;
        if (Utilities.notEmpty(listContacts)) size++;
        return size;
    }

}

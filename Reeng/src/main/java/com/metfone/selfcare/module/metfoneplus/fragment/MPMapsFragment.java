package com.metfone.selfcare.module.metfoneplus.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static androidx.core.content.ContextCompat.checkSelfPermission;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPMapsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPMapsFragment extends MPBaseFragment implements OnMapReadyCallback {
    public static final String TAG = "MapsFragment";

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL = 60000; // Every 60 seconds.

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value, but they may be less frequent.
     */
    private static final long FASTEST_UPDATE_INTERVAL = 30000; // Every 30 seconds

    /**
     * The max time before batched results are delivered by location services. Results may be
     * delivered sooner than this interval.
     */
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 5; // Every 5 minutes.

    /**
     * The entry point to the Fused Location Provider.
     */
    private FusedLocationProviderClient mFusedLocationProviderClient;

    /**
     * The geographical location where the device is currently located. That is, the last-known
     * location retrieved by the Fused Location Provider.
     */
    private Location mLastKnownLocation;
    private LocationCallback mLocationCallback;
    private LatLng mDefaultLocation = new LatLng(Constants.INVALID_LAT, Constants.INVALID_LNG);
    private static final float DEFAULT_ZOOM = Constants.MAP_ZOOM;
    private GoogleMap mMap;

    public OnMapListener mOnMapListener;

    public MPMapsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MPMapsFragment newInstance() {
        MPMapsFragment fragment = new MPMapsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach: ");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_m_p_maps;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.m_p_maps_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        // Initialize the FusedLocationClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mParentActivity);

        // Initialize the location callbacks
        initialLocationCallback();
    }

    private void initialLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }

                Location location = locationResult.getLastLocation();
                if (location != null) {
                    Log.e(TAG, "onLocationResult: " + String.format(Locale.US, "%s - %s", location.getLatitude(), location.getLongitude()));

                    if (mOnMapListener != null) {
                        mOnMapListener.onGetLocationCompleted(new LatLng(location.getLatitude(), location.getLongitude()));
                    }
                    new FetchAddressTask().execute(location);
                }

//                if (mFusedLocationProviderClient != null) {
//                    mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
//                }
            }
        };
    }

    /**
     * Sets up the location request.
     *
     * @return The LocationRequest object containing the desired parameters.
     */
    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        // Note: apps running on "O" devices (regardless of targetSdkVersion) may receive updates
        // less frequently than this interval when the app is no longer in the foreground.
        locationRequest.setInterval(UPDATE_INTERVAL);
        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        locationRequest.setMaxWaitTime(MAX_WAIT_TIME);
        return locationRequest;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e(TAG, "onMapReady: ");
        mMap = googleMap;

        getMyLocation();

        mMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult: ");
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_LOCATION) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }

    @Override
    public void onResume() {
        if (checkSelfPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationProviderClient.requestLocationUpdates(getLocationRequest(),
                    mLocationCallback,
                    Looper.getMainLooper());
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        if (mFusedLocationProviderClient != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
        super.onStop();
    }

    public void getMyLocation() {
        if (checkSelfPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "getMyLocation: PERMISSION_DENIED");
            changeCamera(mDefaultLocation);
            mMap.setMyLocationEnabled(false);
            return;
        }

        mMap.setMyLocationEnabled(true);
        final Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
        locationResult.addOnCompleteListener(mParentActivity, new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    // Set the map's camera position to the current location of the device.
                    mLastKnownLocation = task.getResult();
                    if (mLastKnownLocation != null) {
                        LatLng latLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                        mDefaultLocation = latLng;
                        if (mOnMapListener != null) {
                            mOnMapListener.onGetLocationCompleted(latLng);
                        }
                        new FetchAddressTask().execute(mLastKnownLocation);
                        changeCamera(latLng);
                    }
                } else {
                    Log.e(TAG, "Current location is null. Using defaults. Exception: %s", task.getException());
                    changeCamera(mDefaultLocation);
                    if (mOnMapListener != null) {
                        mOnMapListener.onGetLocationCompleted(mDefaultLocation);
                    }
                    if (checkSelfPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mFusedLocationProviderClient.requestLocationUpdates(getLocationRequest(),
                                mLocationCallback,
                                Looper.getMainLooper());
                    }
                }
            }
        });
    }

    public void removeLocationCallback() {
        if (mFusedLocationProviderClient != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }

    private void changeCamera(LatLng latLng) {
        CameraPosition cameraPosition =
                new CameraPosition.Builder().target(latLng)
                        .zoom(DEFAULT_ZOOM)
                        .build();
        changeCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), null);
    }

    private void changeCamera(LatLng latLng, GoogleMap.CancelableCallback callback) {
        CameraPosition cameraPosition =
                new CameraPosition.Builder().target(latLng)
                        .zoom(DEFAULT_ZOOM)
                        .build();
        changeCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), callback);
    }

    /**
     * Change the camera position by moving or animating the camera
     */
    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback) {
        // The duration must be strictly positive so we make it at least 1.
        mMap.animateCamera(update, callback);
    }

    public void setOnTaskCompleted(OnMapListener onTaskCompleted) {
        this.mOnMapListener = onTaskCompleted;
    }

    public interface OnMapListener {
        void onGetAddressCompleted(String result);

        void onGetLocationCompleted(LatLng latLng);

        void onRequestLocationPermission();
    }


    /**
     * AsyncTask for reverse geocoding coordinates into a physical address.
     */
    @SuppressLint("StaticFieldLeak")
    class FetchAddressTask extends AsyncTask<Location, Void, String> {

        FetchAddressTask() {

        }

        private final String TAG = FetchAddressTask.class.getSimpleName();

        @Override
        protected String doInBackground(Location... params) {
            // Set up the geocoder
            Geocoder geocoder = new Geocoder(mParentActivity,
                    Locale.getDefault());

            // Get the passed in location
            Location location = params[0];
            List<Address> addresses = null;
            String resultMessage = "";

            try {
                addresses = geocoder.getFromLocation(
                        location.getLatitude(),
                        location.getLongitude(),
                        // In this sample, get just a single address
                        1);
            } catch (IOException ioException) {
                // Catch network or other I/O problems
                Log.e(TAG, "Service not available", ioException);
            } catch (IllegalArgumentException illegalArgumentException) {
                // Catch invalid latitude or longitude values
                Log.e(TAG, "Invalid coordinates used" + ". " +
                        "Latitude = " + location.getLatitude() +
                        ", Longitude = " +
                        location.getLongitude(), illegalArgumentException);
            }

            // If no addresses found, print an error message.
            if (addresses == null || addresses.size() == 0) {
                Log.e(TAG, "Invalid coordinates used");
            } else {
                // If an address is found, read it into resultMessage
                Address address = addresses.get(0);
                ArrayList<String> addressParts = new ArrayList<>();

                // Fetch the address lines using getAddressLine,
                // join them, and send them to the thread
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressParts.add(address.getAddressLine(i));
                }

                resultMessage = TextUtils.join(
                        "\n",
                        addressParts);

            }

            return resultMessage;
        }

        /**
         * Called once the background thread is finished and updates the
         * UI with the result.
         *
         * @param address The resulting reverse geocoded address, or error
         *                message if the task failed.
         */
        @Override
        protected void onPostExecute(String address) {
            if (!"".equals(address) && mOnMapListener != null) {
                mOnMapListener.onGetAddressCompleted(address);
            }
            super.onPostExecute(address);
        }
    }
}
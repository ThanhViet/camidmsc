package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HomeCategoryInfo {
    @SerializedName("id")
    String id;
    @SerializedName("parentid")
    int parentid;
    @SerializedName("categoryname")
    String categoryname;
    @SerializedName("url")
    String url;
    @SerializedName("categoryid")
    String categoryid;
    @SerializedName("type")
    String type;
    @SerializedName("order")
    String order;
    @SerializedName("status")
    String status;
    @SerializedName("is_active")
    String is_active;
    @SerializedName("description")
    String description;
    @SerializedName("url_images")
    String url_images;
    @SerializedName("date_set")
    String date_set;
    @SerializedName("settop")
    String settop;
}

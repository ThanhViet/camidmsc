package com.metfone.selfcare.module.netnews.EditCategoryNews.presenter;

import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;

/**
 * Created by HaiKE on 8/19/17.
 */

public interface IEditCategoryNewsPresenter extends MvpPresenter {

    void updateSettingCategory(SharedPref sharedPref, String str, int type);
    void loadNewsCategory();
}

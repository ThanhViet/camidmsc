package com.metfone.selfcare.module.tiin.network.request;

public class TiinRequest {
    private int id;
    private  int cid;
    private  int pid;
    private int page;
    private int num;
    private int categoryId;
    private String keySearch;
    private long unixTime;

    public TiinRequest() {
    }

    public TiinRequest(int page, int num) {
        this.page = page;
        this.num = num;
    }

    public TiinRequest(int cid, int pid, int page, int num) {
        this.cid = cid;
        this.pid = pid;
        this.page = page;
        this.num = num;
    }

    public TiinRequest(int page, int num, int categoryId) {
        this.page = page;
        this.num = num;
        this.categoryId = categoryId;
    }

    public String getUnixTime() {
        return String.valueOf(unixTime);
    }

    public void setUnixTime(long unixTime) {
        this.unixTime = unixTime;
    }

    public String getKeySearch() {
        return keySearch;
    }

    public void setKeySearch(String keySearch) {
        this.keySearch = keySearch;
    }

    public TiinRequest(int id) {
        this.id = id;
    }

    public String getId() {
        return String.valueOf(id);
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryId() {
        return String.valueOf(categoryId);
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCid() {
        return String.valueOf(cid);
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getPid() {
        return String.valueOf(pid);
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPage() {
        return String.valueOf(page);
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getNum() {
        return String.valueOf(num);
    }

    public void setNum(int num) {
        this.num = num;
    }
}

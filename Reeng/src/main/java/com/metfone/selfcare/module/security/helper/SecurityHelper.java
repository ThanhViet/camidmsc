/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/27
 */

package com.metfone.selfcare.module.security.helper;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.security.event.SecurityDetailEvent;
import com.metfone.selfcare.module.security.listener.ScanVulnerabilityListener;
import com.metfone.selfcare.module.security.model.LogModel;
import com.metfone.selfcare.module.security.model.PhoneNumberModel;
import com.metfone.selfcare.module.security.model.VulnerabilityModel;
import com.visc.mobilesecurity.scanVulnerability.Scanner;
import com.visc.mobilesecurity.scanVulnerability.Vulnerability.Vulnerability;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.function.Predicate;

public class SecurityHelper {

    private static boolean resolveHiddenApp(Context context) {
//        Iterator var1 = this.result.iterator();
//        while(var1.hasNext()) {
//            PackageInfo p = (PackageInfo)var1.next();
//            Intent i = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
//            i.setFlags(268435456);
//            i.setData(Uri.parse("package:" + p.packageName));
//            this.mContext.startActivity(i);
//            Log.d("HiddenApp", p.packageName);
//
//            try {
//                Thread.sleep(1000L);
//            } catch (InterruptedException var5) {
//                var5.printStackTrace();
//            }
//        }
//
        return false;
    }

    private static boolean resolveUnknownApp(Context context) {
        try {
            Intent intent;
            if (Build.VERSION.SDK_INT > 15) {
                intent = new Intent("android.settings.SECURITY_SETTINGS");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                return true;
            } else {
                intent = new Intent("android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean resolveRooted(Context context) {
        return false;
    }

    private static boolean resolveUnsafeWifi(Context context) {
        try {
            context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean resolveAdb(Context context) {
        try {
            Intent intent;
            if (Build.VERSION.SDK_INT > 15) {
                intent = new Intent("android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                return true;
            } else {
                intent = new Intent();
                intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.DevelopmentSettings"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean resolveVulnerability(Context context, VulnerabilityModel item) {
        if (context != null && item != null) {
            String key = item.getKey();
            if (VulnerabilityModel.ADB_ENABLE.equals(key)) {
                return resolveAdb(context);
            } else if (VulnerabilityModel.HIDDEN_APP.equals(key)) {
                return resolveHiddenApp(context);
            } else if (VulnerabilityModel.INSTALL_NON_MARKET.equals(key)) {
                return resolveUnknownApp(context);
            } else if (VulnerabilityModel.ROOTED.equals(key)) {
                return resolveRooted(context);
            } else if (VulnerabilityModel.UNSAFE_WIFI.equals(key)) {
                return resolveUnsafeWifi(context);
            }
        }
        return false;
    }

    public static VulnerabilityModel convert2Vulnerability(Vulnerability item) {
        VulnerabilityModel model = null;
        if (item != null) {
            String key = item.getName();
            if (VulnerabilityModel.ADB_ENABLE.equals(key)) {
                model = new VulnerabilityModel(key);
                model.setResName(R.string.security_usb_debug);
                model.setResHint(R.string.security_usb_debug_hint);
                model.setResDescription(R.string.security_usb_debug_description);
                model.setResSuggestion(R.string.security_usb_debug_suggestion);
                model.setResIcon(R.drawable.ic_security_usb_debug_enable);
                model.setResButtonName(R.string.security_open_setting);
            } else if (VulnerabilityModel.HIDDEN_APP.equals(key)) {
                model = new VulnerabilityModel(key);
                model.setResName(R.string.security_hidden_app);
                model.setResHint(R.string.security_hidden_app_hint);
                model.setResDescription(R.string.security_hidden_app_description);
                model.setResSuggestion(R.string.security_hidden_app_suggestion);
                model.setResIcon(R.drawable.ic_security_hidden_app);
                model.setResButtonName(R.string.security_view_apps);
            } else if (VulnerabilityModel.INSTALL_NON_MARKET.equals(key)) {
                model = new VulnerabilityModel(key);
                model.setResName(R.string.security_unknown_app);
                model.setResHint(R.string.security_unknown_app_hint);
                model.setResDescription(R.string.security_unknown_app_description);
                model.setResSuggestion(R.string.security_unknown_app_suggestion);
                model.setResIcon(R.drawable.ic_security_unknown_app);
                model.setResButtonName(R.string.security_open_setting);
            } else if (VulnerabilityModel.ROOTED.equals(key)) {
                model = new VulnerabilityModel(key);
                model.setResName(R.string.security_rooted_device);
                model.setResHint(R.string.security_rooted_device_hint);
                model.setResDescription(R.string.security_rooted_device_description);
                model.setResSuggestion(R.string.security_rooted_device_suggestion);
                model.setResIcon(R.drawable.ic_security_rooted);
                model.setResButtonName(R.string.security_open_setting);
            } else if (VulnerabilityModel.UNSAFE_WIFI.equals(key)) {
                model = new VulnerabilityModel(key);
                model.setResName(R.string.security_unsecured_wifi);
                model.setResHint(R.string.security_unsecured_wifi_hint);
                model.setResDescription(R.string.security_unsecured_wifi_description);
                model.setResSuggestion(R.string.security_unsecured_wifi_suggestion);
                model.setResIcon(R.drawable.ic_security_wifi_unsafe);
                model.setResButtonName(R.string.security_open_setting);
            }
        }
        return model;
    }

    public static VulnerabilityModel getVulnerability(String key) {
        VulnerabilityModel model = null;
        if (VulnerabilityModel.ADB_ENABLE.equals(key)) {
            model = new VulnerabilityModel(key);
            model.setResName(R.string.security_usb_debug);
            model.setResHint(R.string.security_usb_debug_hint);
            model.setResDescription(R.string.security_usb_debug_description);
            model.setResSuggestion(R.string.security_usb_debug_suggestion);
            model.setResIcon(R.drawable.ic_security_usb_debug_enable);
            model.setResButtonName(R.string.security_open_setting);
        } else if (VulnerabilityModel.HIDDEN_APP.equals(key)) {
            model = new VulnerabilityModel(key);
            model.setResName(R.string.security_hidden_app);
            model.setResHint(R.string.security_hidden_app_hint);
            model.setResDescription(R.string.security_hidden_app_description);
            model.setResSuggestion(R.string.security_hidden_app_suggestion);
            model.setResIcon(R.drawable.ic_security_hidden_app);
            model.setResButtonName(R.string.security_view_apps);
        } else if (VulnerabilityModel.INSTALL_NON_MARKET.equals(key)) {
            model = new VulnerabilityModel(key);
            model.setResName(R.string.security_unknown_app);
            model.setResHint(R.string.security_unknown_app_hint);
            model.setResDescription(R.string.security_unknown_app_description);
            model.setResSuggestion(R.string.security_unknown_app_suggestion);
            model.setResIcon(R.drawable.ic_security_unknown_app);
            model.setResButtonName(R.string.security_open_setting);
        } else if (VulnerabilityModel.ROOTED.equals(key)) {
            model = new VulnerabilityModel(key);
            model.setResName(R.string.security_rooted_device);
            model.setResHint(R.string.security_rooted_device_hint);
            model.setResDescription(R.string.security_rooted_device_description);
            model.setResSuggestion(R.string.security_rooted_device_suggestion);
            model.setResIcon(R.drawable.ic_security_rooted);
            model.setResButtonName(R.string.security_open_setting);
        } else if (VulnerabilityModel.UNSAFE_WIFI.equals(key)) {
            model = new VulnerabilityModel(key);
            model.setResName(R.string.security_unsecured_wifi);
            model.setResHint(R.string.security_unsecured_wifi_hint);
            model.setResDescription(R.string.security_unsecured_wifi_description);
            model.setResSuggestion(R.string.security_unsecured_wifi_suggestion);
            model.setResIcon(R.drawable.ic_security_wifi_unsafe);
            model.setResButtonName(R.string.security_open_setting);
        }
        return model;
    }

    public static void updateSpamWhiteList(final BaseSlidingFragmentActivity activity, String phoneNumber, final boolean isAdd) {
        if (activity == null || TextUtils.isEmpty(phoneNumber))
            return;
        phoneNumber = formatPhoneNumber(phoneNumber);
        Log.i("SecurityHelper", "updateSpamWhiteList phoneNumber: " + phoneNumber);
        final ArrayList<String> list = SharedPref.newInstance(activity).getListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_WHITE_LIST);
        String firewallMode = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, "");
        if (TextUtils.isEmpty(firewallMode)) firewallMode = "0";
        if (isAdd) {
            if (list.contains(phoneNumber)) {
                list.remove(phoneNumber);
            }
            list.add(0, phoneNumber);
        } else {
            list.remove(phoneNumber);
        }
        activity.showLoadingDialog("", R.string.processing);
        SecurityApi.getInstance().setupSpamSmsWhitelist(firewallMode, list, new ApiCallbackV2<String>() {
            @Override
            public void onSuccess(String lastId, String s) throws JSONException {
                if (isAdd)
                    activity.showToast(R.string.security_toast_add_phone_number_ignore);
                else
                    activity.showToast(R.string.security_toast_delete_phone_number_ignore);
            }

            @Override
            public void onError(String s) {
                activity.showToast(s);
            }

            @Override
            public void onComplete() {
                activity.hideLoadingDialog();
            }
        });
    }

    public static void updateSpamBlackList(final BaseSlidingFragmentActivity activity, String phoneNumber, final boolean isAdd) {
        if (activity == null || TextUtils.isEmpty(phoneNumber))
            return;
        activity.showLoadingDialog("", R.string.processing);
        phoneNumber = formatPhoneNumber(phoneNumber);
        Log.i("SecurityHelper", "updateSpamBlackList phoneNumber: " + phoneNumber);
        final ArrayList<String> list = SharedPref.newInstance(activity).getListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_BLACK_LIST);
        String firewallMode = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, "");
        if (TextUtils.isEmpty(firewallMode)) firewallMode = "0";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            list.removeIf(new Predicate<String>() {
                @Override
                public boolean test(String s) {
                    return TextUtils.isEmpty(s);
                }
            });
        } else {
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (TextUtils.isEmpty(list.get(i))) list.remove(i);
            }
        }
        if (isAdd) {
            if (list.contains(phoneNumber)) {
                list.remove(phoneNumber);
            }
            list.add(0, phoneNumber);
        } else {
            list.remove(phoneNumber);
        }
        SecurityApi.getInstance().setupSpamSmsBlacklist(firewallMode, list, new ApiCallbackV2<String>() {
            @Override
            public void onSuccess(String lastId, String s) throws JSONException {
                if (isAdd) {
                    activity.showToast(R.string.security_toast_add_phone_number_block);
                } else
                    activity.showToast(R.string.security_toast_delete_phone_number_block);
            }

            @Override
            public void onError(String s) {
                activity.showToast(s);
            }

            @Override
            public void onComplete() {
                activity.hideLoadingDialog();
            }
        });
    }

    public static void updateFirewallWhiteList(final BaseSlidingFragmentActivity activity, String phoneNumber, final boolean isAdd) {
        if (activity == null || TextUtils.isEmpty(phoneNumber))
            return;
        activity.showLoadingDialog("", R.string.processing);
        final ArrayList<String> list = SharedPref.newInstance(activity).getListString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_WHITE_LIST);
        String firewallMode = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, "");
        if (TextUtils.isEmpty(firewallMode)) firewallMode = "0";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            list.removeIf(new Predicate<String>() {
                @Override
                public boolean test(String s) {
                    return TextUtils.isEmpty(s);
                }
            });
        } else {
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (TextUtils.isEmpty(list.get(i))) list.remove(i);
            }
        }
        if (isAdd) {
            if (list.contains(phoneNumber)) {
                list.remove(phoneNumber);
            }
            list.add(0, phoneNumber);
        } else {
            list.remove(phoneNumber);
        }
        SecurityApi.getInstance().setupFirewallSmsWhitelist(firewallMode, list, new ApiCallbackV2<String>() {
            @Override
            public void onSuccess(String lastId, String s) throws JSONException {
                if (isAdd)
                    activity.showToast(R.string.security_toast_add_phone_number_ignore);
                else
                    activity.showToast(R.string.security_toast_delete_phone_number_ignore);
            }

            @Override
            public void onError(String s) {
                activity.showToast(s);
            }

            @Override
            public void onComplete() {
                activity.hideLoadingDialog();
            }
        });
    }

    public static void updateSpamWhiteList(final BaseSlidingFragmentActivity activity, ArrayList<String> phoneNumber
            , boolean isAdd, ApiCallbackV2 apiCallback) {
        if (activity == null || phoneNumber == null || phoneNumber.isEmpty())
            return;
        if (apiCallback != null) activity.showLoadingDialog("", R.string.processing);
        final ArrayList<String> list = SharedPref.newInstance(activity).getListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_WHITE_LIST);
        String firewallMode = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, "");
        if (TextUtils.isEmpty(firewallMode)) firewallMode = "0";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            list.removeIf(new Predicate<String>() {
                @Override
                public boolean test(String s) {
                    return TextUtils.isEmpty(s);
                }
            });
        } else {
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (TextUtils.isEmpty(list.get(i))) list.remove(i);
            }
        }
        if (isAdd) {
            list.removeAll(phoneNumber);
            list.addAll(0, phoneNumber);
        } else {
            list.removeAll(phoneNumber);
        }
        SecurityApi.getInstance().setupSpamSmsWhitelist(firewallMode, list, apiCallback);
    }

    public static void updateSpamBlackList(final BaseSlidingFragmentActivity activity, ArrayList<String> phoneNumber
            , boolean isAdd, ApiCallbackV2 apiCallback) {
        if (activity == null || phoneNumber == null || phoneNumber.isEmpty())
            return;
        activity.showLoadingDialog("", R.string.processing);
        final ArrayList<String> list = SharedPref.newInstance(activity).getListString(Constants.PREFERENCE.PREF_SECURITY_SPAM_BLACK_LIST);
        String firewallMode = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, "");
        if (TextUtils.isEmpty(firewallMode)) firewallMode = "0";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            list.removeIf(new Predicate<String>() {
                @Override
                public boolean test(String s) {
                    return TextUtils.isEmpty(s);
                }
            });
        } else {
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (TextUtils.isEmpty(list.get(i))) list.remove(i);
            }
        }
        if (isAdd) {
            list.removeAll(phoneNumber);
            list.addAll(0, phoneNumber);
        } else {
            list.removeAll(phoneNumber);
        }
        SecurityApi.getInstance().setupSpamSmsBlacklist(firewallMode, list, apiCallback);
    }

    public static void updateFirewallWhiteList(final BaseSlidingFragmentActivity activity, ArrayList<String> phoneNumber
            , final boolean isAdd, ApiCallbackV2 apiCallback) {
        if (activity == null || phoneNumber == null || phoneNumber.isEmpty())
            return;
        activity.showLoadingDialog("", R.string.processing);
        final ArrayList<String> list = SharedPref.newInstance(activity).getListString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_WHITE_LIST);
        String firewallMode = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_MODE, "");
        if (TextUtils.isEmpty(firewallMode)) firewallMode = "0";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            list.removeIf(new Predicate<String>() {
                @Override
                public boolean test(String s) {
                    return TextUtils.isEmpty(s);
                }
            });
        } else {
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (TextUtils.isEmpty(list.get(i))) list.remove(i);
            }
        }
        if (isAdd) {
            list.removeAll(phoneNumber);
            list.addAll(0, phoneNumber);
        } else {
            list.removeAll(phoneNumber);
        }
        SecurityApi.getInstance().setupFirewallSmsWhitelist(firewallMode, list, apiCallback);
    }

    public static ArrayList<PhoneNumberModel> getPhoneNumbersFromCache(BaseSlidingFragmentActivity activity, String keyCache) {
        ArrayList<PhoneNumberModel> result = new ArrayList<>();
        ArrayList<String> list = SharedPref.newInstance(activity).getListString(keyCache);
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                String tmp = list.get(i);
                if (!TextUtils.isEmpty(tmp)) {
                    PhoneNumberModel item = new PhoneNumberModel();
                    item.setPhoneNumber(tmp);
                    String name = getName(tmp, false);
                    if (!TextUtils.isEmpty(name)) {
                        tmp = tmp + " (" + name + ")";
                    }
                    item.setName(tmp);
                    result.add(item);
                }
            }
        }
        return result;
    }

    public static void markAsReadSpamSms(BaseSlidingFragmentActivity activity, ArrayList<LogModel> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<LogModel>>() {
        }.getType();
        ArrayList<LogModel> tmpList = null;
        try {
            String tmpStr = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, "");
            if (!TextUtils.isEmpty(tmpStr)) {
                tmpList = gson.fromJson(tmpStr, type);
            }
        } catch (Exception e) {
        }
        if (tmpList != null) {
            for (LogModel item : list) {
                String phoneNumber = item.getSrc();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    int size = tmpList.size() - 1;
                    for (int i = size; i >= 0; i--) {
                        if (tmpList.get(i) != null && phoneNumber.equals(tmpList.get(i).getSrc())) {
                            tmpList.get(i).setReaded(true);
                        }
                    }
                }
            }
            SharedPref.newInstance(activity).putString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, gson.toJson(tmpList));
        }
    }
    public static void markAsReadFirewallSms(BaseSlidingFragmentActivity activity, ArrayList<LogModel> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<LogModel>>() {
        }.getType();
        ArrayList<LogModel> tmpList = null;
        try {
            String tmpStr = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, "");
            if (!TextUtils.isEmpty(tmpStr)) {
                tmpList = gson.fromJson(tmpStr, type);
            }
        } catch (Exception e) {
        }
        if (tmpList != null) {
            for (LogModel item : list) {
                String phoneNumber = item.getDest();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    int size = tmpList.size() - 1;
                    for (int i = size; i >= 0; i--) {
                        if (tmpList.get(i) != null && phoneNumber.equals(tmpList.get(i).getDest())) {
                            tmpList.get(i).setReaded(true);
                        }
                    }
                }
            }
            SharedPref.newInstance(activity).putString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, gson.toJson(tmpList));
        }
    }

    public static void deleteSpamSms(BaseSlidingFragmentActivity activity, ArrayList<LogModel> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<LogModel>>() {
        }.getType();
        ArrayList<LogModel> tmpList = null;
        try {
            String tmpStr = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, "");
            if (!TextUtils.isEmpty(tmpStr)) {
                tmpList = gson.fromJson(tmpStr, type);
            }
        } catch (Exception e) {
        }
        if (tmpList != null) {
            for (LogModel item : list) {
                String phoneNumber = item.getSrc();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    int size = tmpList.size() - 1;
                    for (int i = size; i >= 0; i--) {
                        if (tmpList.get(i) != null && phoneNumber.equals(tmpList.get(i).getSrc())) {
                            tmpList.remove(i);
                        }
                    }
                }
            }
            SharedPref.newInstance(activity).putString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, gson.toJson(tmpList));
        }
    }

    public static void deleteFirewallSms(BaseSlidingFragmentActivity activity, ArrayList<LogModel> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<LogModel>>() {
        }.getType();
        ArrayList<LogModel> tmpList = null;
        try {
            String tmpStr = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, "");
            if (!TextUtils.isEmpty(tmpStr)) {
                tmpList = gson.fromJson(tmpStr, type);
            }
        } catch (Exception e) {
        }
        if (tmpList != null) {
            for (LogModel item : list) {
                String phoneNumber = item.getDest();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    int size = tmpList.size() - 1;
                    for (int i = size; i >= 0; i--) {
                        if (tmpList.get(i) != null && phoneNumber.equals(tmpList.get(i).getDest())) {
                            tmpList.remove(i);
                        }
                    }
                }
            }
            SharedPref.newInstance(activity).putString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, gson.toJson(tmpList));
        }
    }


    public static class ScanVulnerabilityTask extends AsyncTask<Void, Void, ArrayList<VulnerabilityModel>> {
        private WeakReference<Context> context;
        private ScanVulnerabilityListener listener;

        public static <P, T extends AsyncTask<P, ?, ?>> void execute(T task) {
            execute(task, (P[]) null);
        }

        @SuppressWarnings("unchecked")
        @SuppressLint("NewApi")
        public static <P, T extends AsyncTask<P, ?, ?>> void execute(T task, P... params) {
            if (task.getStatus() != Status.RUNNING) {
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
            } else {
                Log.e("ScanVulnerabilityTask.execute()", "task is running");
            }
        }

        public void setContext(Context context) {
            this.context = new WeakReference<>(context);
        }

        public void setListener(ScanVulnerabilityListener listener) {
            this.listener = listener;
        }

        @Override
        protected ArrayList<VulnerabilityModel> doInBackground(Void... voids) {
            ArrayList<VulnerabilityModel> list = new ArrayList<>();
            if (context != null && context.get() != null) {
                ArrayList<Vulnerability> vulnerabilities = new Scanner(context.get()).startScan();
                for (int i = 0; i < vulnerabilities.size(); i++) {
                    VulnerabilityModel item = SecurityHelper.convert2Vulnerability(vulnerabilities.get(i));
                    if (item != null) {
                        list.add(item);
                    }
                }
            }
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<VulnerabilityModel> list) {
            super.onPostExecute(list);
            if (listener != null) listener.scanCompleted(list);
        }
    }

    public static ArrayList<LogModel> getFirewallSmsDetail(BaseSlidingFragmentActivity activity, String receiver) {
        ArrayList<LogModel> list = new ArrayList<>();
        if (activity != null && !TextUtils.isEmpty(receiver)) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<LogModel>>() {
            }.getType();
            ArrayList<LogModel> tmpList = null;
            try {
                String tmp = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, "");
                if (!TextUtils.isEmpty(tmp)) {
                    tmpList = gson.fromJson(tmp, type);
                }
            } catch (Exception e) {
            }
            if (tmpList != null) {
                for (int i = 0; i < tmpList.size(); i++) {
                    if (tmpList.get(i) != null && receiver.equals(tmpList.get(i).getDest())) {
                        tmpList.get(i).setReaded(true);
                        list.add(tmpList.get(i));
                    }
                }
                if (!list.isEmpty()) {
                    SharedPref.newInstance(activity).putString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, gson.toJson(tmpList));
                    //todo post event update firewall sms
                    SecurityDetailEvent event = new SecurityDetailEvent();
                    event.setReloadData(true);
                    event.setTabId(Constants.TAB_SECURITY_FIREWALL_HISTORY);
                }
            }
        }
        return list;
    }

    public static ArrayList<LogModel> getSpamSmsDetail(BaseSlidingFragmentActivity activity, String receiver) {
        ArrayList<LogModel> list = new ArrayList<>();
        if (activity != null && !TextUtils.isEmpty(receiver)) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<LogModel>>() {
            }.getType();
            ArrayList<LogModel> tmpList = null;
            try {
                String tmp = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, "");
                if (!TextUtils.isEmpty(tmp)) {
                    tmpList = gson.fromJson(tmp, type);
                }
            } catch (Exception e) {
            }
            if (tmpList != null) {
                for (int i = 0; i < tmpList.size(); i++) {
                    if (tmpList.get(i) != null && receiver.equals(tmpList.get(i).getSrc())) {
                        tmpList.get(i).setReaded(true);
                        list.add(tmpList.get(i));
                    }
                }
                if (!list.isEmpty()) {
                    SharedPref.newInstance(activity).putString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, gson.toJson(tmpList));
                    //todo post event update spam sms
                    SecurityDetailEvent event = new SecurityDetailEvent();
                    event.setReloadData(true);
                    event.setTabId(Constants.TAB_SECURITY_SPAM_SMS);
                    EventBus.getDefault().post(event);
                }
            }
        }
        return list;
    }

    public static void allowToSendMessage(final BaseSlidingFragmentActivity activity, ArrayList<LogModel> list, boolean addIgnore) {
        if (activity == null || list == null || list.isEmpty())
            return;
        ArrayList<LogModel> tmpList = null;
        try {
            String tmp = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, "");
            if (!TextUtils.isEmpty(tmp)) {
                tmpList = new Gson().fromJson(tmp, new TypeToken<ArrayList<LogModel>>() {
                }.getType());
            }
        } catch (Exception e) {
        }
        if (tmpList != null) {
            ArrayList<String> whiteList = new ArrayList<>();
            Gson gson = new Gson();
            for (LogModel item : list) {
                String phoneNumber = item.getDest();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    if (addIgnore && !whiteList.contains(phoneNumber))
                        whiteList.add(phoneNumber);
                    if (addIgnore) {
                        int size = tmpList.size() - 1;
                        for (int i = size; i >= 0; i--) {
                            if (tmpList.get(i) != null && phoneNumber.equals(tmpList.get(i).getDest())) {
                                tmpList.remove(i);
                            }
                        }
                    } else tmpList.remove(item);
                }
            }
            if (addIgnore)
                updateFirewallWhiteList(activity, whiteList, true, new ApiCallbackV2<String>() {
                    @Override
                    public void onSuccess(String lastId, String s) throws JSONException {
                        activity.showToast(R.string.security_toast_add_exists_number_firewall);
                    }

                    @Override
                    public void onError(String s) {
                        activity.showToast(s);
                    }

                    @Override
                    public void onComplete() {
                        activity.hideLoadingDialog();
                    }
                });
            SharedPref.newInstance(activity).putString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, gson.toJson(tmpList));
            //todo post event update firewall sms
            SecurityDetailEvent event = new SecurityDetailEvent();
            event.setReloadData(true);
            event.setTabId(Constants.TAB_SECURITY_FIREWALL_HISTORY);
            EventBus.getDefault().post(event);
        }
    }

    public static void reportNotSpam(final BaseSlidingFragmentActivity activity, ArrayList<LogModel> list, boolean addIgnore) {
        if (activity == null || list == null || list.isEmpty())
            return;
        ArrayList<LogModel> tmpList = null;
        try {
            String tmp = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, "");
            if (!TextUtils.isEmpty(tmp)) {
                tmpList = new Gson().fromJson(tmp, new TypeToken<ArrayList<LogModel>>() {
                }.getType());
            }
        } catch (Exception e) {
        }
        if (tmpList != null) {
            ArrayList<String> whiteList = new ArrayList<>();
            Gson gson = new Gson();
            for (LogModel item : list) {
                String phoneNumber = item.getSrc();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    if (addIgnore && !whiteList.contains(phoneNumber))
                        whiteList.add(phoneNumber);
                    if (addIgnore) {
                        int size = tmpList.size() - 1;
                        for (int i = size; i >= 0; i--) {
                            if (tmpList.get(i) != null && phoneNumber.equals(tmpList.get(i).getSrc())) {
                                tmpList.remove(i);
                            }
                        }
                    } else tmpList.remove(item);
                }
            }
            if (addIgnore) {
                activity.showLoadingDialog("", R.string.processing);
                updateSpamBlackList(activity, whiteList, false, new ApiCallbackV2() {
                    @Override
                    public void onSuccess(String lastId, Object o) throws JSONException {
                        activity.showToast(R.string.security_toast_delete_phone_number_block);
                    }

                    @Override
                    public void onError(String s) {
                        activity.showToast(s);
                    }

                    @Override
                    public void onComplete() {
                        activity.hideLoadingDialog();
                    }
                });
            }
            SharedPref.newInstance(activity).putString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, gson.toJson(tmpList));
            //todo post event update spam sms
            SecurityDetailEvent event = new SecurityDetailEvent();
            event.setReloadData(true);
            event.setTabId(Constants.TAB_SECURITY_SPAM_SMS);
            EventBus.getDefault().post(event);
        }
    }

    public static ArrayList<LogModel> getShortListSpamSms(BaseSlidingFragmentActivity activity) {
        ArrayList<LogModel> result;
        ArrayList<LogModel> list = null;
        try {
            String tmp = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_SPAM_SMS_LIST, "");
            if (!TextUtils.isEmpty(tmp)) {
                list = new Gson().fromJson(tmp, new TypeToken<ArrayList<LogModel>>() {
                }.getType());
            }
        } catch (Exception e) {
        }
        if (list != null && !list.isEmpty()) {
            HashMap<String, LogModel> maps = new HashMap<>();
            for (int i = 0; i < list.size(); i++) {
                LogModel item = list.get(i);
                if (item != null) {
                    if (maps.containsKey(item.getSrc())) {
                        LogModel tmp = maps.get(item.getSrc());
                        if (tmp.getTimeInt() < item.getTimeInt()) {
                            maps.put(item.getSrc(), item);
                        }
                    } else {
                        maps.put(item.getSrc(), item);
                    }
                }
            }
            Collection<LogModel> values = maps.values();
            result = new ArrayList<>(values);
        } else result = new ArrayList<>();
        return result;
    }

    public static ArrayList<LogModel> getShortListFirewallSms(BaseSlidingFragmentActivity activity) {
        ArrayList<LogModel> result;
        ArrayList<LogModel> list = null;
        try {
            String tmp = SharedPref.newInstance(activity).getString(Constants.PREFERENCE.PREF_SECURITY_FIREWALL_SMS_LIST, "");
            if (!TextUtils.isEmpty(tmp)) {
                list = new Gson().fromJson(tmp, new TypeToken<ArrayList<LogModel>>() {
                }.getType());
            }
        } catch (Exception e) {
        }
        if (list != null && !list.isEmpty()) {
            HashMap<String, LogModel> maps = new HashMap<>();
            for (int i = 0; i < list.size(); i++) {
                LogModel item = list.get(i);
                if (item != null) {
                    if (maps.containsKey(item.getDest())) {
                        LogModel tmp = maps.get(item.getDest());
                        if (tmp.getTimeInt() < item.getTimeInt()) {
                            maps.put(item.getDest(), item);
                        }
                    } else {
                        maps.put(item.getDest(), item);
                    }
                }
            }
            Collection<LogModel> values = maps.values();
            result = new ArrayList<>(values);
        } else result = new ArrayList<>();
        return result;
    }

    public static String getName(String phoneNumberStr, boolean hasNumber) {
        String result = "";
        if (!TextUtils.isEmpty(phoneNumberStr)) {
            String rawNumber = PhoneNumberHelper.getInstant().getRawNumber(ApplicationController.self(), phoneNumberStr);
            if (!TextUtils.isEmpty(rawNumber) && !rawNumber.startsWith("0")) {
                rawNumber = "0" + rawNumber;
            }
            PhoneNumber phoneNumber = ApplicationController.self().getContactBusiness().getPhoneNumberFromNumber(rawNumber);
            if (phoneNumber != null) {
                result = phoneNumber.getName();
            }
        }
        if (TextUtils.isEmpty(result) && hasNumber) result = phoneNumberStr;
        return result;
    }

    public static String formatPhoneNumber(String phoneNumber) {
        try {
            ApplicationController application = ApplicationController.self();
            String regionCode = application.getReengAccountBusiness().getRegionCode();
            int countryCode = application.getPhoneUtil().getCountryCodeForRegion(regionCode);
            String countryCodeStr = String.valueOf(countryCode);
            if (countryCode > 0) {
                if (phoneNumber.startsWith(countryCodeStr)) {

                } else if (phoneNumber.startsWith("0")) {
                    phoneNumber = phoneNumber.substring(1, phoneNumber.length());
                    phoneNumber = countryCodeStr + phoneNumber;
                } else {
                    phoneNumber = countryCodeStr + phoneNumber;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return phoneNumber;
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        try {
            ApplicationController application = ApplicationController.self();
            String regionCode = application.getReengAccountBusiness().getRegionCode();
            PhoneNumberUtil phoneUtil = application.getPhoneUtil();
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().getPhoneNumberProtocol(phoneUtil, phoneNumber, regionCode);
            return PhoneNumberHelper.getInstant().isValidPhoneNumber(phoneUtil, phoneNumberProtocol);
        } catch (Exception e) {

        }
        return false;
    }
}

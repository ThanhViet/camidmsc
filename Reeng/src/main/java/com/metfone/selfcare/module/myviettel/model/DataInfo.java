/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/4
 *
 */

package com.metfone.selfcare.module.myviettel.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataInfo implements Serializable {
    private static final long serialVersionUID = 9148350635987362625L;

    @SerializedName("Name")
    private String name;
    @SerializedName("PricePlan")
    private String pricePlan;
    @SerializedName("Promotion")
    private String promotion;
    @SerializedName("ExpireDate")
    private String expireDate;
    @SerializedName("isDefaultData")
    private boolean isDefaultData;
    @SerializedName("ussdDetail")
    private String ussdDetail;
    @SerializedName("IsAlreadyCancelExtend")
    private String isAlreadyCancelExtend;
    @SerializedName("Fee")
    private String fee;
    @SerializedName("ussdMenu")
    private String ussdMenu;
    @SerializedName("RegisterDate")
    private String registerDate;
    @SerializedName("Code")
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPricePlan() {
        return pricePlan;
    }

    public void setPricePlan(String pricePlan) {
        this.pricePlan = pricePlan;
    }

    public int getPromotionInt() {
        try {
            return Integer.parseInt(promotion);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public boolean isDefaultData() {
        return isDefaultData;
    }

    public void setDefaultData(boolean defaultData) {
        isDefaultData = defaultData;
    }

    public String getUssdDetail() {
        return ussdDetail;
    }

    public void setUssdDetail(String ussdDetail) {
        this.ussdDetail = ussdDetail;
    }

    public String getIsAlreadyCancelExtend() {
        return isAlreadyCancelExtend;
    }

    public void setIsAlreadyCancelExtend(String isAlreadyCancelExtend) {
        this.isAlreadyCancelExtend = isAlreadyCancelExtend;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public long getFeeLong() {
        try {
            return Long.parseLong(fee);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getUssdMenu() {
        return ussdMenu;
    }

    public void setUssdMenu(String ussdMenu) {
        this.ussdMenu = ussdMenu;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", pricePlan='" + pricePlan + '\'' +
                ", promotion=" + promotion +
                ", expireDate='" + expireDate + '\'' +
                ", isDefaultData=" + isDefaultData +
                ", ussdDetail='" + ussdDetail + '\'' +
                ", isAlreadyCancelExtend='" + isAlreadyCancelExtend + '\'' +
                ", fee=" + fee +
                ", ussdMenu='" + ussdMenu + '\'' +
                ", registerDate='" + registerDate + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}

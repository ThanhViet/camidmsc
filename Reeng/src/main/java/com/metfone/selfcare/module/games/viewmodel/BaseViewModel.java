package com.metfone.selfcare.module.games.viewmodel;

import androidx.lifecycle.ViewModel;

import com.metfone.selfcare.module.games.OnActionCallBack;

public class BaseViewModel extends ViewModel {


    protected OnActionCallBack callBack;

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

//    protected Retrofit getWebService() {
//        OkHttpClient client = new OkHttpClient.Builder()
//                .connectTimeout(30, TimeUnit.SECONDS)
//                .build();
//        return new Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//    }
//
//    protected <T> Callback<T> initHandlerRes(String key) {
//        return new Callback<T>() {
//            @Override
//            public void onResponse(@Nullable Call<T> call,
//                                   @Nullable Response<T> response) {
//                if (response.code() == 200 || response.code() == 201) {
//                    handleSuccess(key, response.body());
//                } else {
//                    handleFailed(key, response.errorBody());
//                }
//            }
//
//            @Override
//            public void onFailure(@Nullable Call<T> call,
//                                  @Nullable Throwable t) {
//                handleFailed(key, t);
//            }
//        };
//    }

//    protected void handleFailed(String key, Object errorBody) {
//        // do nothing
//    }
//
//    protected <T> void handleSuccess(String key, T body) {
//        // do nothing
//    }
}

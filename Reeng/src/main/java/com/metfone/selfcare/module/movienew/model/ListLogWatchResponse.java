package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListLogWatchResponse {


    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("result")
    @Expose
    private ArrayList<Result> result = null;

    @Getter
    @Setter
    public class Result {

        @SerializedName("msisdn")
        @Expose
        private String msisdn;
        @SerializedName("id_phim")
        @Expose
        private String idPhim;
        @SerializedName("id_part")
        @Expose
        private String idPart;
        @SerializedName("id_group")
        @Expose
        private Object idGroup;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("image_path")
        @Expose
        private String imagePath;
        @SerializedName("poster_path")
        @Expose
        private String posterPath;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("isFilmGroups")
        @Expose
        private String isFilmGroups;
        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("current_film")
        @Expose
        private String currentFilm;
        @SerializedName("time_seek")
        @Expose
        private String timeSeek;
        @SerializedName("duration")
        @Expose
        private String duration;
        @SerializedName("link_wap")
        @Expose
        private String linkWap;
        @SerializedName("is_recv_push")
        @Expose
        private String isRecvPush;
    }
}

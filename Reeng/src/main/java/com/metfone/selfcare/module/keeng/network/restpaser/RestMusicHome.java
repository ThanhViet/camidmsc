package com.metfone.selfcare.module.keeng.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.MusicHomeModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RestMusicHome implements Serializable {

    @SerializedName("data")
    private List<MusicHomeModel> data;

    public List<MusicHomeModel> getData() {
        if (data == null) data = new ArrayList<>();
        return data;
    }

    public void setData(List<MusicHomeModel> data) {
        this.data = data;
    }
}

package com.metfone.selfcare.module.metfoneplus.billpayment;

import com.metfone.selfcare.R;

import java.util.ArrayList;
import java.util.List;

public class ServicePaymentGroupModel {
    public int groupTitle;
    public List<ServicePaymentModel> services;

    public static List<ServicePaymentGroupModel> servicePaymentGroupModels;

    public ServicePaymentGroupModel(int groupTitle, List<ServicePaymentModel> services) {
        this.groupTitle = groupTitle;
        this.services = services;
    }

    public static List<ServicePaymentGroupModel> createServices(){
        if(servicePaymentGroupModels == null){
            servicePaymentGroupModels = new ArrayList<>();

            List<ServicePaymentModel> abaGroup = new ArrayList<>();
            abaGroup.add(new ServicePaymentModel(R.drawable.logo_aba_pay, false,"AbaPay"));

            List<ServicePaymentModel> emoneyGroup = new ArrayList<>();
            emoneyGroup.add(new ServicePaymentModel(R.drawable.logo_emoney_pay, false,"eMoney"));

            List<ServicePaymentModel> creditCardGroup = new ArrayList<>();
            creditCardGroup.add(new ServicePaymentModel(R.drawable.logo_master_card_pay, false,"Master"));
            creditCardGroup.add(new ServicePaymentModel(R.drawable.logo_visa_pay, false,"Visa"));
            creditCardGroup.add(new ServicePaymentModel(R.drawable.logo_union_pay, false,"Union"));

            List<ServicePaymentModel> internationalGroup = new ArrayList<>();
            internationalGroup.add(new ServicePaymentModel(R.drawable.logo_wechat_pay, false,"Wechat"));
            internationalGroup.add(new ServicePaymentModel(R.drawable.logo_alipay, false,"Alipay"));

            servicePaymentGroupModels.add(new ServicePaymentGroupModel(R.string.aba_pay, abaGroup));
            servicePaymentGroupModels.add(new ServicePaymentGroupModel(R.string.local_ewallet, emoneyGroup));
            servicePaymentGroupModels.add(new ServicePaymentGroupModel(R.string.credit_debit_card, creditCardGroup));
            servicePaymentGroupModels.add(new ServicePaymentGroupModel(R.string.international_ewallet, internationalGroup));


        }
        return servicePaymentGroupModels;
    }
}

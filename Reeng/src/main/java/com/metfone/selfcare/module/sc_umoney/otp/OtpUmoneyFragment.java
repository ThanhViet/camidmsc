package com.metfone.selfcare.module.sc_umoney.otp;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.sc_umoney.main_umoney.TabUmoneyActivity;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

public class OtpUmoneyFragment extends Fragment {
    TextView tvComfirm;
    TabUmoneyActivity activity;
    public static OtpUmoneyFragment newInstance() {
        
        Bundle args = new Bundle();
        
        OtpUmoneyFragment fragment = new OtpUmoneyFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sc_umoney_otp,container,false);
        activity = (TabUmoneyActivity) getActivity();
        initView(view);
        initEvent();
        return view;
    }

    private void initView(View view) {
        tvComfirm = view.findViewById(R.id.tv_confirm);
    }

    private void initEvent() {
        tvComfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showFragment(SCConstants.UMONEY.TAB_HOME,null,true);
            }
        });
    }
}

package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.appbar.AppBarLayout;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.activity.SettingKhActivity;
import com.metfone.selfcare.module.keeng.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

public abstract class BaseSettingKhFragment extends BaseFragment {
    @Nullable
    @BindView(R.id.icBackToolbar)
    protected AppCompatImageView icBackToolbar;
    @Nullable
    @BindView(R.id.txtTitleToolbar)
    protected AppCompatTextView txtTitle;
    @Nullable
    @BindView(R.id.icOption)
    protected AppCompatImageView icOptionToolbar;
    protected SettingKhActivity mParentActivity;
    protected Unbinder unbinder;
    protected ApplicationController mApplication;
    protected Resources resources;

    @Nullable
    @BindView(R.id.appBar)
    protected AppBarLayout appBarLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        initView(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SettingKhActivity) {
            mParentActivity = (SettingKhActivity) context;
            mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        }
        resources = getResources();
    }

    protected abstract void initView(View view);

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    public void setTitle(int titleId) {
        if (txtTitle != null) {
            txtTitle.setText(titleId);
        }
    }

    public void setTitle(String title) {
        if (txtTitle != null) {
            txtTitle.setText(title);
        }
    }

    @Optional
    @OnClick(R.id.icBackToolbar)
    public void onClickView(View view) {
        switch (view.getId()) {
            case R.id.icBackToolbar:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
        }
    }

}

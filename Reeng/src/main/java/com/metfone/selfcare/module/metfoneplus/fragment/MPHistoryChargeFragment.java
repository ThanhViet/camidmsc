package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.DataHistoryCharge;
import com.metfone.selfcare.model.camid.DataHistoryChargeDetail;
import com.metfone.selfcare.model.camid.DetailType;
import com.metfone.selfcare.model.camid.EBPHistoryCharge;
import com.metfone.selfcare.model.camid.HistoryCharge;
import com.metfone.selfcare.model.camid.HistoryChargeType;
import com.metfone.selfcare.model.camid.HistoryDataInfo;
import com.metfone.selfcare.module.metfoneplus.adapter.HistoryChargeAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.holder.OtherChargeViewHolder;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryChargeResponse;
import com.metfone.selfcare.util.FragmentUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import retrofit2.Response;

public class MPHistoryChargeFragment extends MPBaseFragment {
    private static final String TAG = MPHistoryChargeFragment.class.getSimpleName();
    private static final String BALANCE_NAME_PARAM = "balance_name";
    private static final String DATE_TYPE_PARAM = "date_type";
    private static final String DATE_NAME_PARAM = "date_name";

    @BindView(R.id.list_history_charge)
    RecyclerView mListHistoryCharge;

    private HistoryChargeAdapter mHistoryChargeAdapter;
    private String mBalanceName;
    private String mDateType;
    private String mDateName;

    public MPHistoryChargeFragment() {

    }

    public static MPHistoryChargeFragment newInstance(@DetailType.BalanceName String balanceName,
                                                      @DetailType.DateType String dateType,
                                                      @DetailType.DateName String dateName) {
        MPHistoryChargeFragment fragment = new MPHistoryChargeFragment();
        Bundle args = new Bundle();
        args.putString(BALANCE_NAME_PARAM, balanceName);
        args.putString(DATE_TYPE_PARAM, dateType);
        args.putString(DATE_NAME_PARAM, dateName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mp_detail_balance_default;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mBalanceName = getArguments().getString(BALANCE_NAME_PARAM);
            mDateType = getArguments().getString(DATE_TYPE_PARAM);
            mDateName = getArguments().getString(DATE_NAME_PARAM);
        }

        mHistoryChargeAdapter = new HistoryChargeAdapter(getContext(), new ArrayList<HistoryCharge>(),
                (callSmsDataView) -> {
                    OtherChargeViewHolder holder = (OtherChargeViewHolder) callSmsDataView.getTag();
                    if (holder.mHistoryChargeType.value.equals(HistoryChargeType.SMS_CHARGE.value)) {
                        String tag = MPDetailSmsCallDataFragment.TAG + "SMS";
                        gotoMPDetailSmsCallDataScreen(getString(R.string.m_p_detail_sms_charge),
                                mDateName,
                                mBalanceName,
                                HistoryChargeType.SMS_CHARGE.type,
                                mDateType,
                                tag);
                    } else if (holder.mHistoryChargeType.value.equals(HistoryChargeType.CALLING_CHARGE.value)) {
                        String tag = MPDetailSmsCallDataFragment.TAG + "CALL";
                        gotoMPDetailSmsCallDataScreen(getString(R.string.m_p_detail_calling_charge),
                                mDateName,
                                mBalanceName,
                                HistoryChargeType.CALLING_CHARGE.type,
                                mDateType,
                                tag);
                    } else if (holder.mHistoryChargeType.value.equals(HistoryChargeType.DATA_CHARGE.value)) {
                        String tag = MPDetailSmsCallDataFragment.TAG + "DATA";
                        gotoMPDetailSmsCallDataScreen(getString(R.string.m_p_detail_data_charge),
                                mDateName,
                                mBalanceName,
                                HistoryChargeType.DATA_CHARGE.type,
                                mDateType,
                                tag);
                    } else if (holder.mHistoryChargeType.value.equals(HistoryChargeType.VAS_CHARGE.value)) {
                        String tag = MPDetailSmsCallDataFragment.TAG + "VAS";
                        gotoMPDetailSmsCallDataScreen(getString(R.string.m_p_detail_vas_charge),
                                mDateName,
                                mBalanceName,
                                HistoryChargeType.VAS_CHARGE.type,
                                mDateType,
                                tag);
                    }
                },
                (myServiceView) -> {
                    FragmentUtils.replaceFragmentWithAnimationDefault(mParentActivity.getSupportFragmentManager(),
                            R.id.root_frame,
                            MPServiceFragment.newInstance(MPServiceFragment.TAB_MY_SERVICE));
                });

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mParentActivity, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mParentActivity, R.drawable.divider_list_service)));
        mListHistoryCharge.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListHistoryCharge.addItemDecoration(dividerItemDecoration);
        mListHistoryCharge.setAdapter(mHistoryChargeAdapter);
    }

    @Override
    public void popBackStackFragment() {
        super.popBackStackFragment();
    }

    private void getWSHistoryCharge(String type, String startTime) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        String typeLowerCase = type.toLowerCase();
        MetfonePlusClient client = new MetfonePlusClient();
        client.wsHistoryCharge(typeLowerCase, startTime, new MPApiCallback<WsHistoryChargeResponse>() {
            @Override
            public void onResponse(Response<WsHistoryChargeResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())
                        && response.body().getResult().getWsResponse() != null) {
                    updateUIHistoryCharge(response.body().getResult().getWsResponse());
                } else {
                    Log.e(TAG, "getWSGetSubAccountInfoNew - onResponse: error");
                    mParentActivity.hideLoadingDialog();
                }
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void updateUIHistoryCharge(WsHistoryChargeResponse.Response response) {
        if (!isAdded()) {
            return;
        }

        List<HistoryCharge> historyChargeList = new ArrayList<>();
        EBPHistoryCharge itemEBP;
        DataHistoryCharge itemData;
        DataHistoryChargeDetail itemDataDetail;

        // Total charge - item_history_charge_1
        if (response.getTotal() != null && response.getQuantity() == null) {
            itemEBP = new EBPHistoryCharge(HistoryChargeType.TOTAL_CHARGE,
                    R.drawable.ic_total_charge_24, getString(R.string.m_p_detail_total_charge), response.getTotal());
            if (DetailType.BALANCE_BASIC_NAME.equals(mBalanceName)) {
                itemEBP.setBasicTab(true);
            }
            historyChargeList.add(itemEBP);
        }

        // Calling charge - item_history_charge_2
        if (response.getCalling() != null) {
            itemEBP = new EBPHistoryCharge(HistoryChargeType.CALLING_CHARGE,
                    R.drawable.ic_calling_charge_24, getString(R.string.m_p_detail_calling_charge), response.getCalling());
            historyChargeList.add(itemEBP);
        }

        // SMS charge - item_history_charge_2
        if (response.getSms() != null) {
            itemEBP = new EBPHistoryCharge(HistoryChargeType.SMS_CHARGE,
                    R.drawable.ic_sms_charge_24, getString(R.string.m_p_detail_sms_charge), response.getSms());
            historyChargeList.add(itemEBP);
        }

        // Vas charge - item_history_charge_2
        if (response.getVas() != null) {
            itemEBP = new EBPHistoryCharge(HistoryChargeType.VAS_CHARGE,
                    R.drawable.ic_vas_charge_24, getString(R.string.m_p_detail_vas_charge), response.getVas());
            historyChargeList.add(itemEBP);
        }

        // Data charge - item_history_charge_2
        if (response.getData() != null) {
            itemEBP = new EBPHistoryCharge(HistoryChargeType.DATA_CHARGE,
                    R.drawable.ic_data_charge_24, getString(R.string.m_p_detail_data_charge), response.getData());
            historyChargeList.add(itemEBP);
        }

        // Data parent - item_history_charge_4
        if (response.getQuantity() != null) {
            itemData = new DataHistoryCharge(HistoryChargeType.DATA_CHARGE_PARENT,
                    response.getTotal(), response.getUnit(), response.getQuantity());
            historyChargeList.add(itemData);
        }

        // Data children - item_history_charge_5
        if (response.getQuantity() != null && response.getHistory() != null) {
            List<HistoryDataInfo> historyDataInfos = response.getHistory();
            for (HistoryDataInfo h : historyDataInfos) {
                itemDataDetail = new DataHistoryChargeDetail(HistoryChargeType.DATA_CHARGE_CHILDREN, h);
                historyChargeList.add(itemDataDetail);
            }
        }

        // My Services charge - item_history_charge_3
        if (!DetailType.BALANCE_DATA_NAME.equals(mBalanceName)) {
            itemEBP = new EBPHistoryCharge(HistoryChargeType.MY_SERVICES,
                    R.drawable.ic_my_services_charge_24, getString(R.string.m_p_detail_my_services), null);
            historyChargeList.add(itemEBP);
        }

        mHistoryChargeAdapter.replaceData(historyChargeList);

        mParentActivity.hideLoadingDialog();
    }

    public void forceReloadInfoWSHistoryCharge(String type, String dateType, String dateName) {
        if (type == null || dateType == null) {
            return;
        }

        mDateType = dateType;
        mDateName = dateName;
        getWSHistoryCharge(type, getStartTimeFromDateType(mDateType));
    }

    public String getChargeType() {
        if (mBalanceName != null) {
            return mBalanceName;
        }
        return null;
    }

    private void gotoMPDetailSmsCallDataScreen(String titleScreen,
                                               @DetailType.DateName String dateName,
                                               @DetailType.BalanceName String balanceName,
                                               @DetailType.ChargeType String chargeType,
                                               @DetailType.DateType String dateType,
                                               String tag) {
        gotoMPHistoryChargeFragment(titleScreen, dateName, balanceName, chargeType, dateType);
    }
}

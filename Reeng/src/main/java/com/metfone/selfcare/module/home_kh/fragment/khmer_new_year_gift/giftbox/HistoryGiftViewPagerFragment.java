package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.metfone.selfcare.databinding.FragmentGiftHistoryPageBinding;
import com.metfone.selfcare.module.home_kh.api.WsGiftBoxResponse;

import java.util.ArrayList;

public class HistoryGiftViewPagerFragment extends Fragment{
    private FragmentGiftHistoryPageBinding binding;
    private GiftHistoryAdapter adapter;
    public GiftHistoryAdapter.EventListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentGiftHistoryPageBinding.inflate(getLayoutInflater());
        binding.setHandler(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new GiftHistoryAdapter(getActivity(), listener);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
    }

    public void updateData(ArrayList<WsGiftBoxResponse.PrizeInfo> items){
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

}

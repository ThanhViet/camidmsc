package com.metfone.selfcare.module.home_kh.tab;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.util.Strings;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.common.Constant;
import com.metfone.esport.entity.repsonse.VideoInfoResponse;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.games.luckywheel.FrgmtWebView;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.WebViewNewActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.listeners.ProfileListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.listeners.VipInfoChangeListener;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.home.ItemMoreHome;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.games.activity.ContainerActivity;
import com.metfone.selfcare.module.games.fragment.GameHomePageFragment;
import com.metfone.selfcare.module.home_kh.activity.KhmerNewYearGiftActivity;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.WsGetAllAppsResponse;
import com.metfone.selfcare.module.home_kh.api.WsGetGiftListResponse;
import com.metfone.selfcare.module.home_kh.api.WsGetGiftSearchResponse;
import com.metfone.selfcare.module.home_kh.api.response.CheckUserGamePermissionResponse;
import com.metfone.selfcare.module.home_kh.api.response.GetAuthKeyReponse;
import com.metfone.selfcare.module.home_kh.api.response.WsCheckForceUpdateAppResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsSliderFilmResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.game.GameFragment;
import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointContainerFragment;
import com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.KhmerNewYearGiftHomeFragment;
import com.metfone.selfcare.module.home_kh.fragment.reward.RewardCamIdKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewards.RewardsKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardsdetail.RewardsDetailKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.FilterShopFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.RewardsShopKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.model.CategoryFilter;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.App;
import com.metfone.selfcare.module.home_kh.model.AuthKeyModel;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.model.event.OpenRewardCamID;
import com.metfone.selfcare.module.home_kh.model.event.RequestRefreshInfo;
import com.metfone.selfcare.module.home_kh.notification.model.EventNotificationReadUpdate;
import com.metfone.selfcare.module.home_kh.notification.model.FragmentNotification;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationItem;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationResponse;
import com.metfone.selfcare.module.home_kh.tab.adapter.KhHomeAdapter;
import com.metfone.selfcare.module.home_kh.tab.adapter.events.KhTabHomeListener;
import com.metfone.selfcare.module.home_kh.tab.model.GiftChildDetailItem;
import com.metfone.selfcare.module.home_kh.tab.model.GiftSearchObject;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeBanner;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeChannelTopic;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeCustomTopic;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeGameBanItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeGiftItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeGameLuckyGameItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardCategoryList;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardDetailItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardSearchItem;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardTopics;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardTopics2;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.movienew.model.Category;
import com.metfone.selfcare.module.movienew.model.HomeCategoryInfo;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.HomeResult;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.tab_home.event.TabHomeEvent;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.ConvertHelper;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.IOnBackPressed;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.home.fragment.BaseHomeFragment;
import com.mytelsupportsdk.dialog.CustomDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;

import static com.metfone.selfcare.fragment.HomePagerFragment.mTabGamePosition;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_PHONE_SERVICE;

public class FragmentTabHomeKh extends BaseHomeFragment implements OnInternetChangedListener
        , SwipeRefreshLayout.OnRefreshListener, TabHomeListener.OnAdapterClick, InitDataListener
        , FeedBusiness.FeedBusinessInterface, ProfileListener, ReengMessageListener
        , ContentConfigBusiness.OnConfigChangeListener, VipInfoChangeListener, OnClickMoreItemListener, KhTabHomeListener.OnAdapterClick, TabHomeKhListener.OnAdapterClick, IOnBackPressed, NavigateActivityHelper.EventListener.LoginCallBack {

    private final int MAX_SIZE_BANNER = 7;
    private final int MAX_SIZE_LIVE_CHANNELS = 7;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingView loadingView;
    @BindView(R.id.img_avatar)
    CircleImageView img_avatar;
    @BindView(R.id.btnSignup)
    View btnSignup;
    @BindView(R.id.layout_guest)
    View layout_guest;
    @BindView(R.id.layout_user)
    View layout_user;
    @BindView(R.id.img_rank)
    ImageView img_rank;
    @BindView(R.id.rank_text)
    TextView rank_text;
    @BindView(R.id.layout_rank)
    RelativeLayout layout_rank;
    @BindView(R.id.tvUsername)
    TextView tvUsername;
    @BindView(R.id.block_event_view)
    View block_event_view;
    @BindView(R.id.layout_no_data)
    LinearLayout layout_no_data;
    @BindView(R.id.btn_notify)
    View btn_notify;
    @BindView(R.id.tvNotifyNum)
    TextView tvNotifyNum;
    @BindView(R.id.tab_home_parent)
    RelativeLayout tabHomeParent;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.viewLoadingAnimation)
    LinearLayout viewLoadingAnimation;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.forceUpdateView)
    LinearLayout forceUpdateView;
    @BindView(R.id.tvUpdateInfo)
    TextView tvUpdateInfo;

    public interface GameIconHome {
        void onClick();
    }

    private GameIconHome listenerGame;
    private ApplicationController mApplication;
    private HomeActivity mParentActivity;
    private CamIdUserBusiness mCamIdUserBusiness;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        mContext = context;
        listenerGame = (HomeActivity) context;
        this.mParentActivity = (HomeActivity) context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mCamIdUserBusiness = mApplication.getCamIdUserBusiness();
        super.onAttach(context);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mContext = activity;
        listenerGame = (HomeActivity) activity;
        this.mParentActivity = (HomeActivity) activity;
        this.mApplication = (ApplicationController) activity.getApplicationContext();
        this.mCamIdUserBusiness = mApplication.getCamIdUserBusiness();
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        mContext = null;
        super.onDetach();
    }

    private boolean isLoading;
    private TabHomeKhApi khHomeKhApi;
    private KhHomeClient homeKhClient;
    private ListenerUtils mListenerUtils;
    private ArrayList<IHomeModelType> homeData;
    private ArrayList<CategoryFilter> categoriesFilter = new ArrayList<>();
    private KhHomeAdapter homeAdapter;
    private LinearLayoutManager layoutManager;
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private String topicTitle;
    private AccountRankDTO accountRankDTO;
    private App munnyGame;
    public static boolean gameItemSelected = false;
    public List<KhHomeMovieItem> listBanner = new ArrayList<>();

    private ApiCallbackV2<HomeResult> bannerApiCallbackV2 = new ApiCallbackV2<HomeResult>() {
        @Override
        public void onSuccess(String msg, HomeResult result) throws JSONException {
            try {
                isDataInitiated = true;
                isLoading = false;
                if (homeData == null) homeData = new ArrayList<>();
                // remove old banner
                KhHomeBanner banner = null;
                for (IHomeModelType item : homeData) {
                    if (item.getItemType() == IHomeModelType.BANNER) {
                        banner = (KhHomeBanner) item;
                        break;
                    }
                }
                if (banner != null) {
                    homeData.remove(banner);
                }
                Field[] fields = HomeResult.class.getDeclaredFields();
                for (Field item : fields) {
                    if (item != null) {
                        item.setAccessible(true);
                        String name = item.getName();
                        List<HomeData> homeDatas = (List<HomeData>) item.get(result);
                        if (homeDatas != null && MovieKind.TYPE_RECENTLYADDED.equals(name)) {
                            listBanner.addAll(KhHomeMovieItem.convertHomeDataToBanner(homeDatas));
                            break;
                        }
                    }
                }
                if (listBanner.size() > MAX_SIZE_BANNER) {
                    homeData.add(0, new KhHomeBanner(listBanner.subList(0, MAX_SIZE_BANNER)));
                } else {
                    homeData.add(0, new KhHomeBanner(listBanner));
                }
                setupAdapter();

                KhHomeClient.getInstance().getSliderFilm(callbackSlider);
            } catch (Exception e) {

            }
        }

        @Override
        public void onError(String s) {

        }

        @Override
        public void onComplete() {

        }
    };

    private MPApiCallback<WsSliderFilmResponse> callbackSlider = new MPApiCallback<WsSliderFilmResponse>() {
        @Override
        public void onResponse(retrofit2.Response<WsSliderFilmResponse> response) {
            if (response != null && response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null && response.body().getResult().getWsResponse().getHomeSlider() != null) {
                ArrayList<KhHomeMovieItem> copyBanners = new ArrayList<>();
                for (String id : response.body().getResult().getWsResponse().getHomeSlider()) {
                    for (KhHomeMovieItem movieItem : listBanner) {
                        if (id.equals(String.valueOf(movieItem.id))) {
                            copyBanners.add(movieItem);
                            break;
                        }
                    }
                }
                if (copyBanners.size() > 0) {
                    listBanner.clear();
                    listBanner.addAll(copyBanners);
                    if (homeData == null) homeData = new ArrayList<>();
                    KhHomeBanner banner = null;
                    for (IHomeModelType item : homeData) {
                        if (item.getItemType() == IHomeModelType.BANNER) {
                            banner = (KhHomeBanner) item;
                            break;
                        }
                    }
                    if (banner != null) {
                        homeData.remove(banner);
                    }
                    homeData.add(0, new KhHomeBanner(listBanner));
                    setupAdapter();
                }
            }
        }

        @Override
        public void onError(Throwable error) {

        }
    };

//    private ApiCallbackV2<KhHomeBanner> bannerApiCallback = new ApiCallbackV2<KhHomeBanner>() {
//
//        @Override
//        public void onSuccess(String msg, KhHomeBanner result) throws JSONException {
//            Log.e(TAG, "banner api success: " + result.recommend.size());
//            isDataInitiated = true;
//            isLoading = false;
//            if (homeData == null) homeData = new ArrayList<>();
//            // remove old banner
//            KhHomeBanner banner = null;
//            for (IHomeModelType item : homeData) {
//                if (item.getItemType() == IHomeModelType.BANNER) {
//                    banner = (KhHomeBanner) item;
//                    break;
//                }
//            }
//            if (banner != null) {
//                homeData.remove(banner);
//            }
//            if (result.recommend.size() > MAX_SIZE_BANNER) {
//                homeData.add(0, new KhHomeBanner(result.recommend.subList(0, MAX_SIZE_BANNER)));
//            } else {
//                homeData.add(0, new KhHomeBanner(result.recommend));
//            }
//            setupAdapter();
//        }
//
//        @Override
//        public void onError(String s) {
//            Log.e(TAG, "bannerApiCallback api error: " + s);
//            isLoading = false;
//            hideRefresh();
//        }
//
//        @Override
//        public void onComplete() {
//
//        }
//    };

    private ApiCallbackV2<List<VideoInfoResponse>> channelApiCallback = new ApiCallbackV2<List<VideoInfoResponse>>() {
        @Override
        public void onSuccess(String msg, List<VideoInfoResponse> result) throws JSONException {
            if (result != null & result.size() > 0) {
                Log.e(TAG, "channelApiCallback api success: " + result.size());
                isDataInitiated = true;
                isLoading = false;

                // remove old topic
                KhHomeChannelTopic liveChannel = null;
                for (IHomeModelType item : homeData) {
                    if (item.getItemType() == IHomeModelType.POPULAR_LIVE_CHANNEL) {
                        liveChannel = (KhHomeChannelTopic) item;
                        break;
                    }
                }
                if (liveChannel != null) {
                    homeData.remove(liveChannel);
                }
                homeData.add(new KhHomeChannelTopic(result.size() > MAX_SIZE_LIVE_CHANNELS ? result.subList(0, MAX_SIZE_LIVE_CHANNELS) : result));
                setupAdapter();
            }
        }

        @Override
        public void onError(String s) {

        }

        @Override
        public void onComplete() {

        }
    };

    private ApiCallbackV2<List<KhHomeMovieItem>> topicApiCallback = new ApiCallbackV2<List<KhHomeMovieItem>>() {

        @Override
        public void onSuccess(String msg, List<KhHomeMovieItem> result) throws JSONException {
            Log.e(TAG, "topicApiCallback api sucess: " + result.size());
            isDataInitiated = true;
            isLoading = false;

            // remove old topic
            KhHomeCustomTopic topic = null;
            for (IHomeModelType item : homeData) {
                if (item.getItemType() == IHomeModelType.CUSTOM_TOPIC) {
                    topic = (KhHomeCustomTopic) item;
                    break;
                }
            }
            if (topic != null) {
                homeData.remove(topic);
            }
            if (!StringUtils.isEmpty(topicTitle)) {
                homeData.add(new KhHomeCustomTopic(topicTitle, result));
            } else {
                homeData.add(new KhHomeCustomTopic(result));
            }
            setupAdapter();
        }

        @Override
        public void onError(String s) {
            Log.e(TAG, "topicApiCallback api error: " + s);
            isLoading = false;
            hideRefresh();
        }

        @Override
        public void onComplete() {

        }
    };

    private ApiCallbackV2<List<Category>> topicTitleCallBack = new ApiCallbackV2<List<Category>>() {
        @Override
        public void onSuccess(String msg, List<Category> result) throws JSONException {
            if (result != null && result.size() > 0) {
                KhHomeCustomTopic topic = null;
                for (IHomeModelType item : homeData) {
                    if (item.getItemType() == IHomeModelType.CUSTOM_TOPIC) {
                        topic = (KhHomeCustomTopic) item;
                        break;
                    }
                }
                if (topic != null) {
                    homeData.remove(topic);
                    topic.topicTitle = result.get(0).getCategoryName();
                    homeData.add(topic);
                    setupAdapter();
                } else {
                    topicTitle = result.get(0).getCategoryName();
                }
            }
        }

        @Override
        public void onError(String s) {

        }

        @Override
        public void onComplete() {

        }
    };
    private List<KhNotificationItem> notifications = new ArrayList<>();

    // api 2
    private void wsGtAllPartnerGiftSearch() {
        homeKhClient.wsGtAllPartnerGiftSearch(new MPApiCallback<WsGetGiftSearchResponse>() {
            @Override
            public void onResponse(retrofit2.Response<WsGetGiftSearchResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    WsGetGiftSearchResponse.Response wsResponse = response.body().getResult().getWsResponse();

                    List<KhHomeRewardSearchItem> categoryItems = new ArrayList<>();
                    // default has metfone
                    String metfoneTitle = "";
                    String moreTitle = "";
                    if (getActivity() != null) {
                        metfoneTitle = getActivity().getString(R.string.metfone);
                        moreTitle = getActivity().getString(R.string.rewards_more);
                    } else {
                        metfoneTitle = ApplicationController.self().getString(R.string.metfone);
                        moreTitle = ApplicationController.self().getString(R.string.rewards_more);
                    }
                    KhHomeRewardSearchItem metfone = new KhHomeRewardSearchItem(990999, "metfone", metfoneTitle);
                    metfone.setType(IHomeModelType.REWARD_CATEGORY);
                    categoryItems.add(0, metfone);
                    int i = 0;
                    if (wsResponse.object != null && !wsResponse.object.isEmpty()) {
                        for (GiftSearchObject x : wsResponse.object) {
                            if (x.gifts != null && x.gifts.size() > 0) {
                                i++;
                                KhHomeRewardSearchItem item = new KhHomeRewardSearchItem();
                                item.giftObject = x;
                                item.setType(IHomeModelType.REWARD_CATEGORY);
                                categoryItems.add(item);
                                if (i > 2) break;
                            }
                        }
                    }

                    KhHomeRewardSearchItem addMore = new KhHomeRewardSearchItem(991999, "more", moreTitle);
                    addMore.setType(IHomeModelType.REWARD_CATEGORY);
                    categoryItems.add(addMore);

                    // remove old cate
                    KhHomeRewardCategoryList rewardCate = null;
                    for (IHomeModelType item : homeData) {
                        if (item.getItemType() == IHomeModelType.REWARD_CATEGORY) {
                            rewardCate = (KhHomeRewardCategoryList) item;
                            break;
                        }
                    }
                    if (rewardCate != null) {
                        homeData.remove(rewardCate);
                    }

                    KhHomeRewardCategoryList cate = new KhHomeRewardCategoryList();
                    cate.categoryItems = categoryItems;

                    homeData.add(cate);

                    if (ApplicationController.self().isLogin() == LoginVia.NOT) {
                        if (homeData != null) {
                            ArrayList<IHomeModelType> data = new ArrayList<>();
                            data.addAll(homeData);
                            for (IHomeModelType item : data) {
                                if (item.getItemType() == IHomeModelType.REWARD_TOPIC_1 || item.getItemType() == IHomeModelType.REWARD_TOPIC_2) {
                                    homeData.remove(item);
                                }
                            }
                        }
                    }

                    setupAdapter();
                } else {
                    Log.e(TAG, "getWSGetAccountRank - onResponse: error");
                }
            }

            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
            }
        });
    }

    // api 11
    private void getWsGetListGiftBuyMost() {
        homeKhClient.wsGetListGiftBuyMost(new MPApiCallback<WsGetGiftListResponse>() {
            @Override
            public void onResponse(retrofit2.Response<WsGetGiftListResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    WsGetGiftListResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    // Get value of item have "title" = "ACCOUNTS"
                    Log.e(TAG, "getWsGetListGiftBuyMost - onResponse: OK " + wsResponse.object.size());

                    List<KhHomeRewardDetailItem> categoryItems = new ArrayList<>();
                    int i = 0;
                    if (wsResponse.object != null && !wsResponse.object.isEmpty()) {
                        for (GiftChildDetailItem x : wsResponse.object) {
                            KhHomeRewardDetailItem item = new KhHomeRewardDetailItem();
                            item.giftObject = x;
                            item.setType(IHomeModelType.REWARD_TOPIC_2);
                            categoryItems.add(item);
                            i++;
                            if (i > 3) break;
                        }
                    }

                    // remove old topic
                    KhHomeRewardTopics2 rewardCate = null;
                    for (IHomeModelType item : homeData) {
                        if (item.getItemType() == IHomeModelType.REWARD_TOPIC_2) {
                            rewardCate = (KhHomeRewardTopics2) item;
                            break;
                        }
                    }
                    if (rewardCate != null) {
                        homeData.remove(rewardCate);
                    }

                    KhHomeRewardTopics2 cate = new KhHomeRewardTopics2();
                    cate.categoryItems = categoryItems;

                    homeData.add(cate);
                    setupAdapter();
                } else {
                    Log.e(TAG, "getWsGetListGiftBuyMost - onResponse: error");
                }

            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    // api 15
    private void getWsGetListGiftBuyMostForUser() {
        homeKhClient.wsGetListGiftBuyMostForUser(new MPApiCallback<WsGetGiftListResponse>() {
            @Override
            public void onResponse(retrofit2.Response<WsGetGiftListResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    WsGetGiftListResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    // Get value of item have "title" = "ACCOUNTS"
                    List<KhHomeRewardDetailItem> categoryItems = new ArrayList<>();
                    int i = 0;
                    if (wsResponse.object != null && !wsResponse.object.isEmpty()) {
                        for (GiftChildDetailItem x : wsResponse.object) {
                            KhHomeRewardDetailItem item = new KhHomeRewardDetailItem();
                            item.giftObject = x;
                            item.setType(IHomeModelType.REWARD_TOPIC_1);
                            categoryItems.add(item);
                            i++;
                            if (i > 3) break;
                        }
                    }

                    // remove old topic
                    KhHomeRewardTopics rewardCate = null;
                    for (IHomeModelType item : homeData) {
                        if (item.getItemType() == IHomeModelType.REWARD_TOPIC_1) {
                            rewardCate = (KhHomeRewardTopics) item;
                            break;
                        }
                    }
                    if (rewardCate != null) {
                        homeData.remove(rewardCate);
                    }

                    KhHomeRewardTopics cate = new KhHomeRewardTopics();
                    cate.categoryItems = categoryItems;

                    homeData.add(cate);
                    setupAdapter();
                } else {
                    Log.e(TAG, "getWsGetListGiftBuyMostForUser - onResponse: error");
                }
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    @SuppressLint("HardwareIds")
    private void getWsGetNotifications(int page) {
        if (page == 1) {
            notifications.clear();
        }
        String user_id;
        if (userInfoBusiness.getUser() == null) {
            user_id = "0";
        } else {
            user_id = String.valueOf(userInfoBusiness.getUser().getUser_id());
        }
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        /*
         * fix bug firebase: android.app.Activity.getContentResolver() -> null
         */
        String ss = "";
        if (mContext != null && mContext.getContentResolver() != null) {
            ss = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        homeKhClient.wsGetListCamIDNotification(user_id, page, ss, new MPApiCallback<KhNotificationResponse>() {
            @Override
            public void onResponse(retrofit2.Response<KhNotificationResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    KhNotificationResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    // Get value of item have "title" = "ACCOUNTS"
                    if (wsResponse != null) {
                        if (!wsResponse.new_.isEmpty()) {
                            notifications.addAll(wsResponse.new_);
                        }
                        if (!wsResponse.earlier.isEmpty()) {
                            notifications.addAll(wsResponse.earlier);
                        }
                        Log.e(TAG, "wsGetListCamIDNotification - " + notifications.size());

                        int count = wsResponse.unread;

                        if (tvNotifyNum != null) {
                            if (count > 0) {
                                tvNotifyNum.setVisibility(View.VISIBLE);
                                if (count > 9) {
                                    tvNotifyNum.setText("9+");
                                } else {
                                    tvNotifyNum.setText(String.valueOf(count));
                                }
                            } else {
                                tvNotifyNum.setVisibility(View.GONE);
                            }
                        }
                    }
                } else {
                    Log.e(TAG, "wsGetListCamIDNotification - onResponse: error");
                }
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    private void wsGetAllApps() {
        homeKhClient.wsGetAllApps(new MPApiCallback<WsGetAllAppsResponse>() {
            @Override
            public void onResponse(retrofit2.Response<WsGetAllAppsResponse> response) {
                if (response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null) {
                    List<App> appList = response.body().getResult().getWsResponse().apps;
                    if (appList != null) {
                        for (App app : appList) {
                            if (app.getName().equalsIgnoreCase("game") && app.getShortDes().equalsIgnoreCase("munny")) {
                                munnyGame = app;
                                break;
                            }
                        }

                        if (gameItemSelected) {
                            onGameItemClicked();
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    private void autoLogin(String userName) {
        MetfonePlusClient client = new MetfonePlusClient();
        client.autoLogin(userName, new MPApiCallback<LoginResponse>() {
            @Override
            public void onResponse(retrofit2.Response<LoginResponse> response) {
                if (response.body() != null && response.body().getUsername() != null) {
                    loadData(true);
                }
            }

            @Override
            public void onError(Throwable error) {
                Log.e("errr", "ee -------- " + error.getMessage());
            }
        });
    }

    private void showProgress(boolean isShow) {
        if (isShow) {
            pbLoading.setVisibility(View.VISIBLE);
        } else {
            pbLoading.setVisibility(View.GONE);
        }
    }

    // api 2
    private void wsCheckUserGame() {
        showProgress(true);
        homeKhClient.checkUserGamePermission(new KhApiCallback<CheckUserGamePermissionResponse>() {
            @Override
            public void onFailure(Call<KHBaseResponse<CheckUserGamePermissionResponse>> call, Throwable t) {
                showProgress(false);
                showMessageForLuckyGame();
            }

            @Override
            public void onSuccess(CheckUserGamePermissionResponse body) {
                if (body.getPermission().getPermission() == 1) {
                    wsGetAuthKeyGame();
                } else {
                    showProgress(false);
                    showMessageForLuckyGame();
                }
            }

            @Override
            public void onFailed(String status, String message) {
                showProgress(false);
                showMessageForLuckyGame();
            }
        });
    }

    // api 2
    private void wsGetAuthKeyGame() {
        homeKhClient.getGameAuthKey(new KhApiCallback<GetAuthKeyReponse>() {
            @Override
            public void onFailure(Call<KHBaseResponse<GetAuthKeyReponse>> call, Throwable t) {
                showProgress(false);
                showMessageForLuckyGame();
            }

            @Override
            public void onSuccess(GetAuthKeyReponse body) {
                showProgress(false);
                NavigateActivityHelper.navigateToLuckyGameActivity(activity, body.getAuthKeyModel());
            }

            @Override
            public void onFailed(String status, String message) {
                showProgress(false);
                showMessageForLuckyGame();
            }
        });
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_tab_home_v2;
    }

    private void setupAdapter() {

        Collections.sort(homeData, (o1, o2) -> (o1.getItemType() > o2.getItemType()) ? 1 : -1);

        if (homeAdapter == null) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (viewLoadingAnimation != null) {
                        viewLoadingAnimation.setVisibility(View.GONE);
                    }
                }
            }, 1500);

            homeAdapter = new KhHomeAdapter(activity);
            homeAdapter.setItems(homeData);
            homeAdapter.setListener(this);
            layoutManager = new CustomLinearLayoutManager(activity, LinearLayout.VERTICAL, false);
            BaseAdapter.setupVerticalRecyclerView(activity, recyclerView, layoutManager, homeAdapter, true, 5);
        } else {
            homeAdapter.notifyDataSetChanged();
        }
        showLoadedSuccess();
    }

    @Override
    public void onCreateView() {
        swipeRefreshLayout.setColorSchemeColors(getColor());
        swipeRefreshLayout.setOnRefreshListener(this);
        tvUsername.setEllipsize(TextUtils.TruncateAt.END);
        if (homeData == null) homeData = new ArrayList<>();
        else homeData.clear();
        Utilities.adaptViewForInserts(header);
        Log.e(TAG, "onCreateView ------: ");
    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.kh_red_color);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated");
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        ListenerHelper.getInstance().addConfigChangeListener(this);
        ListenerHelper.getInstance().addProfileChangeListener(this);
        app.getFeedBusiness().setFeedBusinessInterface(this);
        app.getMessageBusiness().addReengMessageListener(this);
        app.getReengAccountBusiness().addVipInfoChangeListener(this);
        // TabHomeApi.TabHomeResponse tmp = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW, TabHomeApi.TabHomeResponse.class);

        if (ApplicationController.self().isLogin() == LoginVia.NOT) {
            Log.d(TAG, "onActivityCreated : not login");
            getWsGetNotifications(1);
            recyclerView.setEnabled(false);
            block_event_view.setOnTouchListener((v, event) -> true);
        } else {
            Log.d(TAG, "onActivityCreated : Login");
            recyclerView.setEnabled(true);

            String phoneService = SharedPrefs.getInstance().get(PREF_PHONE_SERVICE, String.class);
            System.out.println("thanhlvdddddd === ssssssssss nnnnn ");
            new Handler().postDelayed(() -> {
                if (phoneService == null || phoneService.isEmpty()) {
                    autoLogin("");
                } else {
                    autoLogin(phoneService);
                }
            }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
        layout_guest.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, false);
                NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true, FragmentTabHomeKh.this);
            }
        });
        btnSignup.setOnClickListener(v -> {
            SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
            NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true, FragmentTabHomeKh.this);
        });
    }

    public void addGameHomeFragment() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), ContainerActivity.class);
                startActivity(intent);
                mCamIdUserBusiness.setProcessingLoginSignUpGame2(false);
            }
        }, 1500);

    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        ListenerHelper.getInstance().removeConfigChangeListener(this);
        ListenerHelper.getInstance().removeProfileChangeListener(this);
        if (app != null) {
            app.getFeedBusiness().setFeedBusinessInterface(null);
            app.getMessageBusiness().removeReengMessageListener(this);
            app.getReengAccountBusiness().removeVipInfoChangeListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad())
            loadData(Utilities.isEmpty(homeData));
        if (isVisibleToUser && isAdded() && app != null) {
            autoLoadData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (app != null) {
            if (ApplicationController.self().isLogin() == LoginVia.NOT) {
                if (homeData != null) {
                    ArrayList<IHomeModelType> data = new ArrayList<>();
                    data.addAll(homeData);
                    for (IHomeModelType item : data) {
                        if (item.getItemType() == IHomeModelType.REWARD_TOPIC_1 || item.getItemType() == IHomeModelType.REWARD_TOPIC_2) {
                            homeData.remove(item);
                        }
                    }
                    if (homeAdapter != null) {
                        homeAdapter.notifyDataSetChanged();
                    }
                }
                loadDataGuest(true);
                getWsGetNotifications(1);
            } else {
                loadData(true);
                if (mCamIdUserBusiness.isProcessingLoginSignUpGame2()) {
                    if (!StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
                        addGameHomeFragment();
                    }
                }
            }
        }
    }

    private void loadNumberNotify() {
        if (app.getReengAccountBusiness().isAnonymousLogin()) return;
        Log.i(TAG, "loadNumberNotify");
        new WSOnMedia(app).getNumberNotify(new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "loadNumberNotify: " + s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                        int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                        if (code == HTTPCode.E200_OK) {
                            int number = jsonObject.optInt("number");
                            if (app != null) {
                                if (number != app.getFeedBusiness().getTotalNotify()) {
                                    app.getFeedBusiness().setTotalNotify(number);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                Log.e(TAG, e);
            }
        });
    }

    private void addGameBanner() {
        for (IHomeModelType item : homeData) {
            if (item.getItemType() == IHomeModelType.GAME_ITEM) {
                homeData.remove(item);
                break;
            }
        }
        homeData.add(new KhHomeGameBanItem());
        setupAdapter();
    }

    private void addLuckyGameBanner() {
        for (IHomeModelType item : homeData) {
            if (item.getItemType() == IHomeModelType.LUCKY_GAME_ITEM) {
                homeData.remove(item);
                break;
            }
        }
        homeData.add(new KhHomeGameLuckyGameItem());
        setupAdapter();
    }

    private void showLoading() {
        hideRefresh();
    }

    private void showLoadedSuccess() {
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
        if (loadingView != null) loadingView.showLoadedSuccess();
    }

    private void loadData(boolean showLoading) {
        Log.i(TAG, "loadData isLoading: " + isLoading + ", showLoading: " + showLoading);
        if (isLoading) return;
        isLoading = true;
        if (showLoading) showLoading();
        if (khHomeKhApi == null) {
            khHomeKhApi = new TabHomeKhApi();
        }
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
//        khHomeKhApi.getBanner(bannerApiCallback, userInfoBusiness.getUser().getUser_id());
        MovieApi.getInstance().getHomeAppV2(10, 0, bannerApiCallbackV2);
        khHomeKhApi.getPopularLiveChannel(channelApiCallback);
        khHomeKhApi.getCustomTopic(topicApiCallback);
        MovieApi.getInstance().getCategoryByIds(new String[]{"868459"}, topicTitleCallBack);
        wsGtAllPartnerGiftSearch();
        String mochaToken = ApplicationController.self().getReengAccountBusiness().getToken();
        if (mochaToken != null && !mochaToken.isEmpty()) {
            // api 15
            getWsGetListGiftBuyMostForUser();
        }
        getWsGetListGiftBuyMost();
        getWsGetNotifications(1);

        // add game item
        addGameBanner();
        addLuckyGameBanner();
        wsGetAllApps();
    }


    private void loadDataGuest(boolean showLoading) {
        Log.i(TAG, "loadData isLoading: " + isLoading + ", showLoading: " + showLoading);
        if (isLoading) return;
        isLoading = true;
        if (showLoading) showLoading();
        if (khHomeKhApi == null) {
            khHomeKhApi = new TabHomeKhApi();
        }
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
//        khHomeKhApi.getBanner(bannerApiCallback, userInfoBusiness.getUser().getUser_id());
        MovieApi.getInstance().getHomeAppV2(10, 0, bannerApiCallbackV2);
        khHomeKhApi.getCustomTopic(topicApiCallback);
        MovieApi.getInstance().getCategoryByIds(new String[]{"868459"}, topicTitleCallBack);
        khHomeKhApi.getPopularLiveChannel(channelApiCallback);
        wsGtAllPartnerGiftSearch();
        addGameBanner();
        addLuckyGameBanner();
    }


    @Override
    public void onRefresh() {
        if (isLoading) {
            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        }
        EventBus.getDefault().post(new RequestRefreshInfo());
        if (ApplicationController.self().isLogin() == LoginVia.NOT) {
//                drawProfile();
            loadDataGuest(true);
        } else {
            loadData(true);
        }
        loadNumberNotify();
        //AdsManager.getInstance().reloadAdsNative();
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad())
            loadData(Utilities.isEmpty(homeData));
    }

    @Override
    public void onDataReady() {
        Log.i(TAG, "onDataReady");
        if (app != null) app.getMessageBusiness().addReengMessageListener(this);
    }

    @Override
    public void onUpdateNotifyFeed(int totalNotify) {
        Log.i(TAG, "onUpdateNotifyFeed totalNotify: " + totalNotify);
    }

    @Override
    public void onClickTitleBoxContact() {
        if (activity instanceof HomeActivity) {
            ((HomeActivity) activity).openContacts();
        }
    }

    @Override
    public void onClickContact(ThreadMessage item) {
        NavigateActivityHelper.navigateToChatDetail(activity, item);
    }

    @Override
    public void onClickFeature(ItemMoreHome item, int position) {
        TabHomeUtils.openFeature(activity, item);

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickTitleBox(IHomeModelType item, int position) {
        logActiveHome();
        if (item.getItemType() == IHomeModelType.REWARD_CATEGORY) {
            showRewardShops(null);
        } else if (item.getItemType() == IHomeModelType.POPULAR_LIVE_CHANNEL) {
            ((HomeActivity) activity).getNavigationBar().selectTab(1);
        }
    }

    @Override
    public void onScrollData(IHomeModelType homeModelType) {
        if (homeModelType != null) {
            switch (homeModelType.getItemType()) {
                case IHomeModelType.REWARD_CATEGORY:
                case IHomeModelType.REWARD_TOPIC_1:
                case IHomeModelType.REWARD_TOPIC_2: {
                    logActiveReward();
                }
                break;
                default:
                    logActiveHome();
                    break;
            }
        }
    }

    @Override
    public void onClickSignUp() {
        SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
        NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true, this);
    }

    @OnClick(R.id.btn_notify)
    public void showNotify() {
        logActiveHome();
//        if (ApplicationController.self().isLogin() != LoginVia.NOT) {
        FragmentManager fragmentManager = getParentFragmentManager();
        FragmentTransaction mFragmentTransaction = fragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                R.anim.decelerate_slide_out_left_fragment,
                R.anim.decelerate_slide_in_left_fragment,
                R.anim.decelerate_slide_out_right
        );
        mFragmentTransaction.add(R.id.tab_home_parent, new FragmentNotification(), FragmentNotification.class.getSimpleName());
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(HistoryPointContainerFragment.TAG);
        mFragmentTransaction.commitAllowingStateLoss();
//        }
    }

    @Override
    public void onProfileChange() {
    }

    @Override
    public void onRequestFacebookChange(String fullName, String birthDay, int gender) {
    }

    @Override
    public void onAvatarChange(String avatarPath) {
        Log.i(TAG, "onAvatarChange avatarPath: " + avatarPath);
    }

    @Override
    public void onCoverChange(String coverPath) {
    }

    @Override
    public void notifyNewIncomingMessage(ReengMessage message, ThreadMessage thread) {
        Log.i(TAG, "notifyNewIncomingMessage");

    }

    @Override
    public void onRefreshMessage(int threadId) {

    }

    @Override
    public void notifyNewOutgoingMessage() {
    }

    @Override
    public void onGSMSendMessageError(ReengMessage reengMessage, XMPPResponseCode responseCode) {
    }

    @Override
    public void onNonReengResponse(int threadId, ReengMessage reengMessage, boolean showAlert, String msgError) {
    }

    @Override
    public void notifyMessageSentSuccessfully(int threadId) {
    }

    @Override
    public void onUpdateStateTyping(String phoneNumber, ThreadMessage thread) {
    }

    @Override
    public void onSendMessagesError(List<ReengMessage> reengMessageList) {
    }

    @Override
    public void onUpdateMediaDetail(MediaModel mediaModel, int threadId) {
    }

    @Override
    public void onUpdateStateAcceptStranger(String friendJid) {
    }

    @Override
    public void onUpdateStateRoom() {
    }

    @Override
    public void onBannerDeepLinkUpdate(com.metfone.selfcare.database.model.ReengMessage message, ThreadMessage mCorrespondingThread) {
    }

    public void hideRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    public void onConfigStickyBannerChanged() {

    }

    @Override
    public void onConfigTabChange() {

    }

    @Override
    public void onConfigBackgroundHeaderHomeChange() {
    }

    @Override
    public void onChange() {
        Log.i(TAG, "onChange LoadFeatures");
        if (app != null && app.getReengAccountBusiness().isValidAccount()) {

        }
    }

    @Override
    public void onMoreConfigChange() {
        Log.i(TAG, "onMoreConfigChange LoadFeatures");
        if (app != null && app.getReengAccountBusiness().isValidAccount()) {

        }
    }

    @Override
    public void onAVNOChange() {
        //loadFeaturesData();
    }

    private void autoLoadData() {
        Log.d(TAG, "autoLoadData");
        if (app != null && app.isDataReady()) {
            long lastTime = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LAST_TIME_DATA_TAB_HOME, Long.class);
            if (Math.abs(System.currentTimeMillis() - lastTime) > 300000) {
                Log.d(TAG, "autoLoadData start");
                loadData(false);
                if (!app.getReengAccountBusiness().isAnonymousLogin()) {
                    recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            loadNumberNotify();
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final TabHomeEvent event) {
        Log.i(TAG, "onMessageEvent event: " + event);
        if (event.isReselected()) {
            if (layoutManager != null && recyclerView != null) {
                recyclerView.stopScroll();
                layoutManager.smoothScrollToPosition(recyclerView, null, 0);
            }
        }
        if (event.isUpdateNotify()) {
            if (app != null && !app.getReengAccountBusiness().isAnonymousLogin()) {
                loadNumberNotify();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onEvent(final TabHomeEvent event) {
        Log.i(TAG, "onEvent event: " + event);
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Override
    public void onClickTitleBox(TabHomeModel item, int position) {
        if (activity == null || item == null) return;
        switch (item.getType()) {
            case TabHomeModel.TYPE_QUICK_CONTACT:
                if (activity instanceof HomeActivity) {
                    ((HomeActivity) activity).openContacts();
                }
                break;
            case TabHomeModel.TYPE_BOX_VIDEO:
                TabHomeUtils.openTabVideo(activity);
                break;
            case TabHomeModel.TYPE_BOX_MUSIC:
                TabHomeUtils.openTabMusic(activity);
                break;
            case TabHomeModel.TYPE_BOX_MOVIE:
                TabHomeUtils.openTabMovie(activity);
                break;
            case TabHomeModel.TYPE_BOX_NEWS:
                TabHomeUtils.openTabNews(activity);
                break;
            case TabHomeModel.TYPE_BOX_COMIC:
                TabHomeUtils.openTabWap(activity, item.getId());
                break;
            case TabHomeModel.TYPE_BOX_TIIN:
                TabHomeUtils.openTabTiin(activity);
                break;
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickComicItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.COMIC), null);
            String url = ((Content) item).getUrl();
            if (Utilities.notEmpty(url)) {
                if (URLUtil.isNetworkUrl(url))
                    Utilities.gotoWebViewFullScreen(app, activity, url, WebViewNewActivity.ORIENTATION_AUTO, "", "", false);
                else
                    DeepLinkHelper.getInstance().openSchemaLink(activity, url);
            }
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.COMIC), null);
                String url = ((Content) model.getObject()).getUrl();
                if (Utilities.notEmpty(url)) {
                    if (URLUtil.isNetworkUrl(url))
                        Utilities.gotoWebViewFullScreen(app, activity, url, WebViewNewActivity.ORIENTATION_AUTO, "", "", false);
                    else
                        DeepLinkHelper.getInstance().openSchemaLink(activity, url);
                }
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreComicItem(Object item, int position) {
        if (item instanceof Content) {
            DialogUtils.showOptionComicItem(activity, item, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                DialogUtils.showOptionComicItem(activity, model.getObject(), this);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMovieItem(Object item, int position) {
        logActiveHome();
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.MOVIE), null);
            Movie movie = ConvertHelper.convert2Movie((Content) item);
            app.getApplicationComponent().providesUtils().openMovieDetail(activity, movie);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.MOVIE), null);
                Movie movie = ConvertHelper.convert2Movie((Content) model.getObject());
                app.getApplicationComponent().providesUtils().openMovieDetail(activity, movie);
            }
        } else if (item instanceof KhHomeMovieItem) {
            Movie m = ConvertHelper.convert2Movie((KhHomeMovieItem) item);
            app.getApplicationComponent().providesUtils().openMovieDetail((BaseSlidingFragmentActivity) getActivity(), m);
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {
        if (item instanceof Content) {
            Movie movie = ConvertHelper.convert2Movie((Content) item);
            DialogUtils.showOptionMovieItem(activity, movie, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Movie movie = ConvertHelper.convert2Movie((Content) model.getObject());
                DialogUtils.showOptionMovieItem(activity, movie, this);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMusicItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.MUSIC), null);
            AllModel music = ConvertHelper.convert2Music((Content) item);
            TabHomeUtils.openMusicKeeng(activity, music);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.MUSIC), null);
                AllModel music = ConvertHelper.convert2Music((Content) model.getObject());
                TabHomeUtils.openMusicKeeng(activity, music);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreMusicItem(Object item, int position) {
        if (item instanceof Content) {
            AllModel music = ConvertHelper.convert2Music((Content) item);
            DialogUtils.showOptionMusicItem(activity, music, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                AllModel music = ConvertHelper.convert2Music((Content) model.getObject());
                DialogUtils.showOptionMusicItem(activity, music, this);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickNewsItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.NEWS), null);
            NewsModel news = ConvertHelper.convert2News((Content) item);
            CommonUtils.readDetailNews(activity, news, false);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.NEWS), null);
                NewsModel news = ConvertHelper.convert2News((Content) model.getObject());
                CommonUtils.readDetailNews(activity, news, false);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreNewsItem(Object item, int position) {
        if (item instanceof Content) {
            NewsModel news = ConvertHelper.convert2News((Content) item);
            DialogUtils.showOptionNewsItem(activity, news, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                NewsModel news = ConvertHelper.convert2News((Content) model.getObject());
                DialogUtils.showOptionNewsItem(activity, news, this);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    private void logT(String m) {
        Log.e(TAG, m);
    }

    @Override
    public void onClickVideoItem(Object item, int position) {

        logT("onClickVideoItem1013: " + new Gson().toJson(item));
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.VIDEO), null);
            Content content = (Content) item;
            Video video = ConvertHelper.convert2Video(content);
            app.getApplicationComponent().providesUtils().openVideoDetail(activity, video);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.VIDEO), null);
                Content content = (Content) model.getObject();
                Video video = ConvertHelper.convert2Video(content);
                app.getApplicationComponent().providesUtils().openVideoDetail(activity, video);
            }
        } else if (item instanceof VideoInfoResponse) {
            VideoInfoResponse video = (VideoInfoResponse) item;

            com.metfone.esport.util.Utilities.openPlayer(getActivity(), video, true);
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreVideoItem(Object item, int position) {
        logT(" 1033: " + new Gson().toJson(item));
        if (item instanceof Content) {
            Content content = (Content) item;
            Video video = ConvertHelper.convert2Video(content);
            DialogUtils.showOptionVideoItem(activity, video, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Content content = (Content) model.getObject();
                Video video = ConvertHelper.convert2Video(content);
                DialogUtils.showOptionVideoItem(activity, video, this);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickChannelVideoItem(Object item, int position) {
        logT(" 1053: " + new Gson().toJson(item));
        if (item instanceof Content) {
            Content content = (Content) item;
            Video video = ConvertHelper.convert2Video(content);
            Channel channel = video.getChannel();
            if (channel != null)
                app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Content content = (Content) model.getObject();
                Video video = ConvertHelper.convert2Video(content);
                Channel channel = video.getChannel();
                if (channel != null)
                    app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickChannelItem(Object item, int position) {
        logActiveHome();
        logT(" 1076: " + new Gson().toJson(item));
        //TODO : Need convert data and handle action when click popular live channels
//        if (item instanceof Content) {
//            Content content = (Content) item;
//            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log(content, LogApi.ContentType.VIDEO), null);
//            Channel channel = ConvertHelper.convert2ChannelVideo(content);
//            app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
//        } else if (item instanceof TabHomeModel) {
//            TabHomeModel model = (TabHomeModel) item;
//            if (model.getObject() instanceof Content) {
//                Content content = (Content) model.getObject();
//                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log(content, LogApi.ContentType.VIDEO), null);
//                Channel channel = ConvertHelper.convert2ChannelVideo(content);
//                app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
//            }
//        }
        if (item instanceof VideoInfoResponse) {
            VideoInfoResponse channel = (VideoInfoResponse) item;
            if (channel.channelInfo != null)
                AloneFragmentActivity.with(getActivity())
                        .parameters(ChannelContainerFragment.newBundle((long) channel.channelInfo.id))
                        .start(ChannelContainerFragment.class);
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreChannelItem(Object item, int position) {
        logT(" 1096: " + new Gson().toJson(item));
        if (item instanceof Content) {
            Content content = (Content) item;
            Channel channel = ConvertHelper.convert2ChannelVideo(content);
            DialogUtils.showOptionChannelItem(activity, channel, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Content content = (Content) model.getObject();
                Channel channel = ConvertHelper.convert2ChannelVideo(content);
                DialogUtils.showOptionChannelItem(activity, channel, this);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickSliderBannerItem(Object item, int position) {
        logActiveHome();
        logT(" 1116: " + new Gson().toJson(item));
        if (item instanceof Content) {
            Content model = (Content) item;
            app.getLogApi().logAction(LogApi.LogType.CLICK_BANNER, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log(model, LogApi.ContentType.BANNER), null);
            String itemType = model.getItemType();
            if ("deeplink".equals(itemType)) {
                DeepLinkHelper.getInstance().openSchemaLink(activity, model.getUrl());
            } else {
                Utilities.processOpenLink(app, activity, model.getUrl());
            }
        } else if (item instanceof TabHomeModel) {
            TabHomeModel provisional = (TabHomeModel) item;
            if (provisional.getObject() instanceof Content) {
                Content model = (Content) provisional.getObject();
                app.getLogApi().logAction(LogApi.LogType.CLICK_BANNER, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log(model, LogApi.ContentType.BANNER), null);
                String itemType = model.getItemType();
                if ("deeplink".equals(itemType)) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, model.getUrl());
                } else {
                    Utilities.processOpenLink(app, activity, model.getUrl());
                }
            }
        } else if (item instanceof KhHomeMovieItem) {
            Movie m = ConvertHelper.convert2Movie((KhHomeMovieItem) item);
            app.getApplicationComponent().providesUtils().openMovieDetail((BaseSlidingFragmentActivity) getActivity(), m);
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        logT(" 1145: " + new Gson().toJson(object));
        if (activity != null && !activity.isFinishing() && object != null) {
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        ShareUtils.openShareMenu(activity, object);
                    }
                    break;
                case Constants.MENU.MENU_SAVE_VIDEO:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        if (object instanceof Video) {
                            VideoDataSource.getInstance((ApplicationController) activity.getApplication()).saveVideoFromMenu((Video) object);
                            activity.showToast(R.string.videoSavedToLibrary);
                        }
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        FeedModelOnMedia feed = null;
                        if (object instanceof Movie) {
                            feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) object);
                        } else if (object instanceof Video) {
                            feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia((Video) object);
                        } else if (object instanceof AllModel) {
                            feed = FeedModelOnMedia.convertMediaToFeedModelOnMedia((AllModel) object);
                        } else if (object instanceof NewsModel) {
                            feed = FeedModelOnMedia.convertNewsToFeedModelOnMedia((NewsModel) object);
                        } else if (object instanceof TiinModel) {
                            feed = FeedModelOnMedia.convertTiinToFeedModelOnMedia((TiinModel) object);
                        }
                        if (feed != null) {
                            new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                    , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                    , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                        @Override
                                        public void onError(String s) {

                                        }

                                        @Override
                                        public void onComplete() {

                                        }

                                        @Override
                                        public void onSuccess(String msg, String result) throws JSONException {
                                            if (activity != null)
                                                activity.showToast(R.string.add_favorite_success);
                                        }
                                    });
                        }
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        if (object instanceof Video) {
                            VideoDataSource.getInstance((ApplicationController) activity.getApplication()).watchLaterVideoFromMenu((Video) object);
                            activity.showToast(R.string.add_later_success);
                        }
                        if (object instanceof Movie) {
                            new MovieApi().insertWatchLater((Movie) object, new ApiCallbackV2<String>() {

                                @Override
                                public void onError(String s) {

                                }

                                @Override
                                public void onComplete() {

                                }

                                @Override
                                public void onSuccess(String msg, String result) throws JSONException {
                                    if (activity != null)
                                        activity.showToast(R.string.add_later_success);
                                }
                            });
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {

    }

    @Override
    public void onClickTiinItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.TIIN), null);
            TiinModel tiin = ConvertHelper.convert2Tiin((Content) item);
            CommonUtils.readDetailTiin(activity, tiin, false);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.TIIN), null);
                TiinModel tiin = ConvertHelper.convert2Tiin((Content) model.getObject());
                CommonUtils.readDetailTiin(activity, tiin, false);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreTiinItem(Object item, int position) {
        if (item instanceof Content) {
            TiinModel tiin = ConvertHelper.convert2Tiin((Content) item);
            DialogUtils.showOptionTiinItem(activity, tiin, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                TiinModel tiin = ConvertHelper.convert2Tiin((Content) model.getObject());
                DialogUtils.showOptionTiinItem(activity, tiin, this);
            }
        }

        //AdsManager.getInstance().showAdsFullScreenHome();
    }

    @OnClick({R.id.btn_category, R.id.img_avatar, R.id.btn_search, R.id.layout_rank, R.id.tvUsername})
    public void onViewClicked(View view) {
        logActiveHome();
        switch (view.getId()) {
            case R.id.btn_category:
                // TODO: [START] Cambodia version
                // TODO: Goto menu more screen
//                if (app.getReengAccountBusiness().isValidAccount()) {
                NavigateActivityHelper.navigateToSettingKhActivity(activity, -1);
//                } else {
//                    logT("Not login");
//                }
                break;
            case R.id.img_avatar:
            case R.id.tvUsername:
                logApp(Constants.LOG_APP.HOME_DETAIL_ACC);
                NavigateActivityHelper.navigateToSetUpProfile(activity);
                break;
            case R.id.btn_search:
                Utilities.openSearch(activity, 0);
                break;
            case R.id.layout_rank:
                replaceFragment(R.id.tab_home_parent, RewardCamIdKHFragment.newInstance(), RewardCamIdKHFragment.TAG);
                break;
        }
    }

    @Override
    public void onClickRewardItem(Object item, int position) {
        logActiveHome();
        if (item instanceof KhHomeRewardDetailItem) {
            GiftItems g = new GiftItems();
            g.setGiftId(((KhHomeRewardDetailItem) item).giftObject.giftId);

            FragmentManager mFragmentManager = getParentFragmentManager();
            FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                    R.anim.decelerate_slide_out_left_fragment,
                    R.anim.decelerate_slide_in_left_fragment,
                    R.anim.decelerate_slide_out_right
            );
            mFragmentTransaction.replace(R.id.tab_home_parent, RewardsDetailKHFragment.newInstance(g, true), RewardsDetailKHFragment.TAG);
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            mFragmentTransaction.addToBackStack(RewardsDetailKHFragment.TAG);
            mFragmentTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void onClickRewardCategoryItem(Object item, int position) {
        logActiveReward();
        logT(" 1285: " + new Gson().toJson(item)); // TODO
        if (item instanceof KhHomeRewardSearchItem) {
            if (((KhHomeRewardSearchItem) item).id == 991999) {
                // TODO for MORE
                logT(" 1285: " + new Gson().toJson(item));
                AccountRankDTO rankDto = ApplicationController.self().getAccountRankDTO();
                if (rankDto != null) {
                    logT("onClick [More] > rankDTO :" + rankDto.rankId);
                    replaceFragment(R.id.tab_home_parent,
                            FilterShopFragment.newInstance(RewardsShopKHFragment.CATEGORY,
                                    0,
                                    new ArrayList<>(),
                                    String.valueOf(rankDto.rankId), true)
                            , HistoryPointContainerFragment.TAG);
                } else {
                    logT("onClick [More] > rankDTO = null");
                }

            } else if (((KhHomeRewardSearchItem) item).id == 990999) {
                // TODO for Metfone
                logT(" 1285: " + new Gson().toJson(item));

                replaceFragment(R.id.tab_home_parent, RewardsKHFragment.newInstance(), HistoryPointContainerFragment.TAG);
            } else {
//                String categoryId = ((KhHomeRewardSearchItem) item).giftObject.id;
                logT(" onClickRewardCategoryItem > position " + position);
                homeKhClient.wsAppLog(new MPApiCallback<BaseResponse>() {
                    @Override
                    public void onResponse(retrofit2.Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            android.util.Log.d(TAG, String.format("wsLogApp => succeed [%s]", Constants.LOG_APP.REWARD_SHOP));
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        android.util.Log.d(TAG, "wsLogApp => failed");
                    }
                }, Constants.LOG_APP.REWARD_SHOP);
                showRewardShops(String.valueOf(position));
            }
        }
    }

    protected void replaceFragment(int container, Fragment fragment, String tag) {
        FragmentTransaction mFragmentTransaction = getParentFragmentManager().beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                R.anim.decelerate_slide_out_left_fragment,
                R.anim.decelerate_slide_in_left_fragment,
                R.anim.decelerate_slide_out_right
        );

        mFragmentTransaction.replace(container, fragment);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(tag);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    private void showRewardShops(String categoryPosition) {
        FragmentManager mFragmentManager = getParentFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                R.anim.decelerate_slide_out_left_fragment,
                R.anim.decelerate_slide_in_left_fragment,
                R.anim.decelerate_slide_out_right
        );

        mFragmentTransaction.replace(R.id.tab_home_parent, RewardsShopKHFragment.newInstance(categoryPosition, false), RewardsShopKHFragment.TAG);

        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(RewardsShopKHFragment.TAG);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageNotificationEvent(final EventNotificationReadUpdate event) {
        getWsGetNotifications(1);
    }

    // mo reward camid khi tab hien tai khong phai tab home
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openRewardCamID(final OpenRewardCamID event) {
        if (event.currentTab != TabHomeHelper.HomeTab.tab_home) {
            HomeActivity homeActivity = (HomeActivity) activity;
            homeActivity.getNavigationBar().selectTab(0);
        }
        List<Fragment> fragments = getParentFragmentManager().getFragments();
        boolean existInBackStack = false;
        for (Fragment fragment : fragments) {
            if (fragment instanceof RewardCamIdKHFragment) {
                existInBackStack = true;
            }
        }
        if (!existInBackStack) {
            replaceFragment(R.id.tab_home_parent, RewardCamIdKHFragment.newInstance(), RewardCamIdKHFragment.TAG);
        } else {
            while (getParentFragmentManager().getFragments().size() > 0) {
                int size = getParentFragmentManager().getFragments().size();
                Fragment fragment = getParentFragmentManager().getFragments().get(size - 1);
                if (fragment instanceof RewardCamIdKHFragment) {
                    break;
                } else {
                    getParentFragmentManager().popBackStack();
                }
            }
        }
    }

    private void drawProfile(AccountRankDTO accountRankDTO, UserInfo userInfo) {
        tvUsername.setText(userInfo.getFull_name());
        KhUserRank myRank = KhUserRank.getById(accountRankDTO.rankId);
        if (!StringUtils.isEmpty(userInfo.getAvatar())) {
            Glide.with(this)
                    .load(ImageUtils.convertBase64ToBitmap(userInfo.getAvatar()))
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_default)
                    .error(R.drawable.ic_avatar_default)
                    .into(img_avatar);
        } else {
            img_avatar.setImageResource(myRank.resAvatar);
        }
        img_rank.setImageDrawable(ResourceUtils.getDrawable(myRank.resRank));
        rank_text.setText(getActivity().getString(myRank.resRankName));
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void loadInfoUser(final LoadInfoUser event) {
        if (event.loadType == LoadInfoUser.LoadType.LOADING) {
            layout_user.setVisibility(View.GONE);
            layout_guest.setVisibility(View.GONE);
            layout_no_data.setVisibility(View.VISIBLE);
        } else if (event.loadType == LoadInfoUser.LoadType.NOT_LOGIN) {
            layout_user.setVisibility(View.GONE);
            layout_guest.setVisibility(View.VISIBLE);
            layout_no_data.setVisibility(View.GONE);
        } else {
            layout_user.setVisibility(View.VISIBLE);
            layout_guest.setVisibility(View.GONE);
            layout_no_data.setVisibility(View.GONE);
            drawProfile(event.rankDTO, event.userInfo);
        }
    }

    @Override
    public void onGameItemClicked() {
        logApp(Constants.LOG_APP.HOME_MUNNY);
        logActiveHome();
        ApplicationController.self().getFirebaseEventBusiness().logOpenGame("Munny");
        if (ApplicationController.self().isLogin() == LoginVia.NOT) {
            gameItemSelected = true;
            SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
            NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true, this);
        } else {
            gameItemSelected = false;
            if (munnyGame != null && !Strings.isEmptyOrWhitespace(munnyGame.getAndroidLink())) {
                replaceFragment(R.id.tab_home_parent, GameFragment.newInstance(munnyGame.getAndroidLink()), GameFragment.TAG);
            }
        }
    }

    @Override
    public void onGiftClicked(KhHomeGiftItem.GiftItem giftItem) {
        logApp(Constants.LOG_APP.HOME_QRCODE);
        ApplicationController.self().getFirebaseEventBusiness().logOpenGame("New Year");
        Intent intent = new Intent(activity, KhmerNewYearGiftActivity.class);
        startActivity(intent);
    }

    private void addGiftBanner() {
//        for (IHomeModelType item : homeData) {
//            if (item.getItemType() == IHomeModelType.GIFT) {
//                homeData.remove(item);
//                break;
//            }
//        }
//        KhHomeGiftItem item = new KhHomeGiftItem();
//
//        ArrayList<KhHomeGiftItem.GiftItem> giftItems = new ArrayList<>();
//        giftItems.add(new KhHomeGiftItem.GiftItem(R.drawable.khmer_new_year_gift_banner_item, KhHomeGiftItem.GiftType.KHMER_NEW_YEAR, true));
//        giftItems.add(new KhHomeGiftItem.GiftItem(R.drawable.coming_soon_game_banner, KhHomeGiftItem.GiftType.COMING_SOON, false));
//
//        item.giftItems = giftItems;
//        homeData.add(item);
//        setupAdapter();
    }

    @Override
    public void onGameTabClick() {
        if (mTabGamePosition > -1) {
            listenerGame.onClick();
        } else if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
            mCamIdUserBusiness.setProcessingLoginSignUpGame2(true);
            mParentActivity.displayRegisterScreenActivity(true);
        } else {
            //go to game home
            mCamIdUserBusiness.setProcessingLoginSignUpGame2(false);
            Intent intent = new Intent(getActivity(), ContainerActivity.class);
            startActivity(intent);
        }
    }


    @Override
    public void onNewYearGameClick() {
        logApp(Constants.LOG_APP.HOME_QRCODE);
        ApplicationController.self().getFirebaseEventBusiness().logOpenGame("New Year");
        Intent intent = new Intent(activity, KhmerNewYearGiftActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLuckyWheelGameClick() {
        logApp(Constants.LOG_APP.HOME_LUCKY_WHEEL);
        ApplicationController.self().getFirebaseEventBusiness().logOpenGame("Lucky Wheel");
        String phoneNumber = userInfoBusiness.getUser().getPhone_number();
        if (phoneNumber == null) {
            showMessageForLuckyGame();
            return;
        }
        boolean isMetfonePhoneNumber = phoneNumber.matches(Constant.METFONE_NUMBER_REGEX);
        if (isMetfonePhoneNumber) {
            wsCheckUserGame();
        } else {
            showMessageForLuckyGame();
        }
    }

    private void showMessageForLuckyGame() {
        Resources res = activity.getResources();
        new DialogConfirm(activity, true).setLabel(res.getString(R.string.msg_show_error_game_title))
                .setMessage(res.getString(R.string.msg_show_error_game_content)).
                setPositiveLabel(res.getString(R.string.ok)).
                setPositiveListener(result -> {

                }).show();
    }

    @Override
    public void onActionBeforeGoToLogin() {
        checkForceUpdate();
    }

    public enum LoginVia {
        MOCHA, OPEN_ID, MOCHA_OPEN_ID, NOT
    }

    private void logActiveHome() {
        ((HomeActivity) activity).logActiveTab(FirebaseEventBusiness.TabLog.TAB_HOME);
    }

    private void logActiveReward() {
        ((HomeActivity) activity).logActiveTab(FirebaseEventBusiness.TabLog.TAB_REWARD);
        ApplicationController.self().getFirebaseEventBusiness().logSelectTab("Reward");
    }

    protected void hideBottomMenu() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            BottomNavigationBar bottomNavigationBar = ((HomeActivity) activity).getNavigationBar();
            if (bottomNavigationBar != null && bottomNavigationBar.getVisibility() == View.VISIBLE) {
                bottomNavigationBar.setVisibility(View.GONE);
            }
        }
    }


    protected void showBottomMenu() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            BottomNavigationBar bottomNavigationBar = ((HomeActivity) activity).getNavigationBar();
            if (bottomNavigationBar != null && bottomNavigationBar.getVisibility() != View.VISIBLE) {
                bottomNavigationBar.setVisibility(View.VISIBLE);
            }
        }
    }

    @OnClick(R.id.tvSkipUpdate)
    public void skipUpdate() {
        forceUpdateView.setVisibility(View.GONE);
        showBottomMenu();
    }

    @OnClick(R.id.btnUpdate)
    public void updateApp() {
        NavigateActivityHelper.navigateToPlayStore(activity, BuildConfig.APPLICATION_ID);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void forceUpdateEvent(WsCheckForceUpdateAppResponse.WsResponse response) {
        if (response.getForceUpgrade()) {
            forceUpdateView.setVisibility(View.VISIBLE);
            tvUpdateInfo.setText(response.getUpdateInfo());
            hideBottomMenu();
        }
    }

    private void checkForceUpdate() {
        homeKhClient.wsCheckForceUpdateApp(new KhApiCallback<KHBaseResponse<WsCheckForceUpdateAppResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<WsCheckForceUpdateAppResponse> body) {
                if (body.getData() != null && body.getData().getWsResponse() != null) {
                    WsCheckForceUpdateAppResponse.WsResponse response = body.getData().getWsResponse();
                    EventBus.getDefault().post(response);
                }
            }

            @Override
            public void onFailed(String status, String message) {
                logT("wsCheckForceUpdateApp > onFailed");
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logT("wsCheckForceUpdateApp > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsCheckForceUpdateAppResponse>>> call, Throwable t) {
                t.printStackTrace();
                logT("wsCheckForceUpdateApp > onFailure");
            }
        }, BuildConfig.VERSION_NAME);
    }

    private void logApp(String wsCode) {
        homeKhClient.wsAppLog(new MPApiCallback<BaseResponse>() {
            @Override
            public void onResponse(retrofit2.Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    logT(String.format("wsLogApp => succeed [%s]", wsCode));
                }
            }

            @Override
            public void onError(Throwable error) {
                logT("wsLogApp => failed");
            }
        }, wsCode);
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.myviettel.listener.OnDataChallengeListener;
import com.metfone.selfcare.ui.imageview.CircleImageView;

import butterknife.BindView;

public class ContactDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.iv_cover)
    @Nullable
    CircleImageView ivCover;
    @BindView(R.id.tv_avatar)
    @Nullable
    TextView tvAvatar;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.button_submit)
    @Nullable
    View btnSubmit;

    private PhoneNumber data;
    private ApplicationController application;
    private int sizeAvatar;

    public ContactDetailHolder(View view, final Activity activity, final OnDataChallengeListener listener) {
        super(view);
        application = (ApplicationController) activity.getApplication();
        sizeAvatar = (int) application.getResources().getDimension(R.dimen.avatar_small_size);
        if (btnSubmit != null) {
            btnSubmit.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (data != null && listener != null) {
                        listener.onClickChooseContact(data);
                    }
                }
            });
        }
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof PhoneNumber) {
            data = (PhoneNumber) item;
            if (tvTitle != null) {
                tvTitle.setText(data.getName());
            }
            if (tvDesc != null) {
                tvDesc.setText(data.getJidNumber());
            }
            if (tvAvatar != null) tvAvatar.setVisibility(View.GONE);
            if (application != null)
                application.getAvatarBusiness().setPhoneNumberAvatar(ivCover, tvAvatar, data, sizeAvatar);
        } else {
            data = null;
        }
    }

}
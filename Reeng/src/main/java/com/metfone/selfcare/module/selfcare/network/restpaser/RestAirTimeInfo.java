package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCAirTimeInfo;

import java.io.Serializable;
import java.util.ArrayList;

public class RestAirTimeInfo extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCAirTimeInfo> data;

    public ArrayList<SCAirTimeInfo> getData() {
        return data;
    }

    public void setData(ArrayList<SCAirTimeInfo> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestAirTimeInfo [data=" + data + "] errror " + getErrorCode();
    }
}

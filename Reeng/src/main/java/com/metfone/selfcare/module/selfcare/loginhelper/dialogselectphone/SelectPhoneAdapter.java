package com.metfone.selfcare.module.selfcare.loginhelper.dialogselectphone;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.StrangerTopicConfideAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.model.SCNumberVerify;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 5/8/2019.
 */

public class SelectPhoneAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = StrangerTopicConfideAdapter.class.getSimpleName();
    private ArrayList<SCNumberVerify> listNumber = new ArrayList<>();
    private BaseSlidingFragmentActivity mActivity;
    private LayoutInflater inflater;
    private int indexSelected = -1;
    private RecyclerClickListener clickListener;
    private boolean fromMyAccount;
    private VerifyNumberListener verifyNumberListener;

    public SelectPhoneAdapter(BaseSlidingFragmentActivity activity, ArrayList<SCNumberVerify> list) {
        this.mActivity = activity;
        this.inflater = LayoutInflater.from(mActivity);
        this.listNumber = list;
    }

    public void setClickListener(RecyclerClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setVerifyNumberListener(VerifyNumberListener verifyNumberListener) {
        this.verifyNumberListener = verifyNumberListener;
    }

    public void setFromMyAccount(boolean fromMyAccount) {
        this.fromMyAccount = fromMyAccount;
    }

    public void setListTopic(ArrayList<SCNumberVerify> list) {
        this.listNumber = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PhoneViewHolder holder;
        View view = inflater.inflate(R.layout.item_number_verify, parent, false);
        holder = new PhoneViewHolder(mActivity, view);
        if (clickListener != null) {
            holder.setRecyclerClickListener(clickListener);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PhoneViewHolder topicViewHolder = (PhoneViewHolder) holder;
        topicViewHolder.setElement(listNumber.get(position));
        topicViewHolder.setViewClick(position, listNumber.get(position));
    }

    @Override
    public int getItemCount() {
        if (listNumber == null) return 0;
        return listNumber.size();
    }

    private class PhoneViewHolder extends BaseViewHolder {
        private SCNumberVerify mEntry;
        private Context mContext;
        private TextView mTvwMessage, tvVerify, tvVerifyNow;
        private ImageView rbSelect;

        public PhoneViewHolder(Context context, View itemView) {
            super(itemView);
            this.mContext = context;
            mTvwMessage = (TextView) itemView.findViewById(R.id.tvNumber);
            rbSelect = itemView.findViewById(R.id.rbNumber);
            tvVerify = itemView.findViewById(R.id.tvVerify);
            tvVerifyNow = itemView.findViewById(R.id.tvVerifyNow);
            tvVerifyNow.setPaintFlags(tvVerifyNow.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            if (fromMyAccount) {
                mTvwMessage.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.mocha_text_size_level_2_in_dp));
                tvVerify.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.mocha_text_size_level_2_in_dp));
                rbSelect.setVisibility(View.GONE);
                mTvwMessage.setTypeface(mTvwMessage.getTypeface(), Typeface.BOLD);
            }
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (SCNumberVerify) obj;
            if (fromMyAccount && mEntry.isMyAccount()) {
                String content = mEntry.getMsisdn() + " " + mContext.getString(R.string.sc_default);
                mTvwMessage.setText(content);
            } else
                mTvwMessage.setText(mEntry.getMsisdn());
            if (mEntry.isSelected())
                rbSelect.setImageResource(R.drawable.radio_selfcare);
            else
                rbSelect.setImageResource(R.drawable.radio_purple_uncheck);

            if (mEntry.isVerify()) {
                tvVerifyNow.setVisibility(View.GONE);
                tvVerify.setText(mContext.getString(R.string.sc_did_verify));
            } else {
                if(fromMyAccount){
                    tvVerifyNow.setVisibility(View.VISIBLE);
                    tvVerifyNow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (verifyNumberListener != null) {
                                verifyNumberListener.onVerifyNumber(mEntry);
                            }
                        }
                    });
                } else
                    tvVerifyNow.setVisibility(View.GONE);
                tvVerify.setText(mContext.getString(R.string.sc_not_verify));

            }

        }
    }

    public interface VerifyNumberListener {
        void onVerifyNumber(SCNumberVerify number);
    }
}

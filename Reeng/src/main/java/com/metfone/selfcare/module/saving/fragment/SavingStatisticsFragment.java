/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/19
 */

package com.metfone.selfcare.module.saving.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.saving.activity.SavingActivity;
import com.metfone.selfcare.module.saving.adapter.SavingStatisticsAdapter;
import com.metfone.selfcare.module.saving.model.SavingStatisticsModel;
import com.metfone.selfcare.module.tab_home.event.TabHomeEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SavingStatisticsFragment extends Fragment {

    private static final String TAG = SavingStatisticsFragment.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.tv_message)
    TextView tvMessage;

    private SavingActivity mActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private SavingStatisticsAdapter adapter;
    private ArrayList<SavingStatisticsModel> data = new ArrayList<>();

    public static SavingStatisticsFragment newInstance() {
        SavingStatisticsFragment fragment = new SavingStatisticsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (SavingActivity) context;
        mApplication = (ApplicationController) mActivity.getApplicationContext();
        mRes = mApplication.getResources();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_saving_statistics, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (data == null) data = new ArrayList<>();
        if (recycler.getItemDecorationCount() <= 0) {
            recycler.setHasFixedSize(true);
            recycler.setNestedScrollingEnabled(false);
            recycler.addItemDecoration(new DividerItemDecoration(mActivity, R.drawable.divider_listview));
            recycler.setLayoutManager(new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        }
        adapter = new SavingStatisticsAdapter(mActivity, data);
        recycler.setAdapter(adapter);
        loadData();
        recycler.setNestedScrollingEnabled(false);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity.setTitle(mRes.getString(R.string.privilege));
    }

    public void loadData() {
        Log.i(TAG, "getDataAVNO");
        mActivity.showLoadingDialog("", R.string.loading);
        AVNOHelper.getInstance(mApplication).getSavingStatistic(new AVNOHelper.GetSavingStatisticListener() {

            @Override
            public void onGetSavingStatisticSuccess(SavingStatisticsModel result) {
                if (tvMessage != null) {
                    tvMessage.setVisibility(View.GONE);
                }
                mActivity.hideLoadingDialog();
                if (data == null) data = new ArrayList<>();
                else
                    data.clear();
                if (result != null) {
                    String totalSaving = result.getTotalSavingShortcut();
                    if (totalSaving != null) {
                        mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_MONEY_SAVING, totalSaving).apply();
//                        TabMobileFragment.SubTabCallEvent event = new TabMobileFragment.SubTabCallEvent();
//                        event.setTotalSavingShortcut(totalSaving);
//                        EventBus.getDefault().post(event);
                        EventBus.getDefault().postSticky(new TabHomeEvent().setUpdateFeature(true));
                    }
                    if (!result.getPromotion().isEmpty()) {
                        SavingStatisticsModel item = new SavingStatisticsModel();
                        item.setTotalSavingShortcut(result.getTotalSavingShortcut());
                        item.setPromotion(result.getPromotion());
                        item.setType(SavingStatisticsModel.Type.PROMOTION);
                        data.add(item);
                    }
                    if (result.getSavingStatistics() != null) {
                        SavingStatisticsModel item = new SavingStatisticsModel();
                        item.setTotalSavingShortcut(result.getTotalSavingShortcut());
                        result.getSavingStatistics().setShowWithAnimation(true);
                        item.setSavingStatistics(result.getSavingStatistics());
                        item.setType(SavingStatisticsModel.Type.SAVING);
                        item.setDialogInfo(result.getDialogInfo());
                        data.add(item);
                    }
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onGetSavingStatisticError(int code, String msg) {
                if (tvMessage != null) {
                    tvMessage.setText(msg);
                    tvMessage.setVisibility(View.VISIBLE);
                }
                mActivity.hideLoadingDialog();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}

package com.metfone.selfcare.module.libsignal;

import com.android.volley.Request;
import com.android.volley.Response;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.keeng.network.restful.AbsRestful;
import com.metfone.selfcare.restful.GsonRequest;
import com.metfone.selfcare.restful.ResfulString;

public class WSSignalService extends AbsRestful {
    private static final String TAG = WSSignalService.class.getSimpleName();
    private ApplicationController mApplication;

    public static final String TAG_REGISTER_PRE_KEY = "TAG_REGISTER_PRE_KEY";

    public WSSignalService(ApplicationController applicationController) {
        super(applicationController);
        mApplication = applicationController;
    }

    public void registerPreKey(String preKey, Response.Listener<String> response, Response.ErrorListener error) {
        if (!mApplication.getReengAccountBusiness().isValidAccount()) {
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_REGISTER_PRE_KEY);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + "/ReengBackendBiz/e2e/setPreKeys";

        long timestamp = TimeHelper.getCurrentTime();
        String msisdn = mApplication.getReengAccountBusiness().getJidNumber();
        StringBuilder sb = new StringBuilder();
        sb.append(mApplication.getReengAccountBusiness().getJidNumber())
                .append(msisdn)
                .append(preKey)
                .append(Constants.HTTP.CLIENT_TYPE_STRING)
                .append(Config.REVISION)
                .append(mApplication.getReengAccountBusiness().getToken())
                .append(timestamp);
        String security = HttpHelper.encryptDataV2(mApplication, sb.toString(), mApplication.getReengAccountBusiness().getToken());

        ResfulString params = new ResfulString(url);
        params.addParam(Constants.HTTP.REST_MSISDN, msisdn);
        params.addParam(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam(Constants.HTTP.REST_REVISION, Config.REVISION);
        params.addParam(Constants.HTTP.TIME_STAMP, String.valueOf(timestamp));
        params.addParam(Constants.HTTP.DATA_SECURITY, security);
        GsonRequest<String> req = new GsonRequest<>(Request.Method.POST, params.toString(), String.class, null, response, error);
        addReq(req, TAG_REGISTER_PRE_KEY);
    }
}

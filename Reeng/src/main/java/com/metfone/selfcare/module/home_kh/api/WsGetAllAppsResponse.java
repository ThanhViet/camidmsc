package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.App;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetAllAppsResponse extends BaseResponse<WsGetAllAppsResponse.Response> {
    public class Response{
        @SerializedName("apps")
        public List<App> apps;
    }
}

package com.metfone.selfcare.module.home_kh.fragment.game;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.util.Strings;
import com.google.gson.Gson;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.esport.homes.HomeFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.CreateYourAccountActivity;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.activity.RegisterScreenActivity;
import com.metfone.selfcare.activity.SetUpProfileActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.IdentifyProvider;
import com.metfone.selfcare.model.account.LinkAccountRequest;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.SignIn;
import com.metfone.selfcare.model.account.SignInRequest;
import com.metfone.selfcare.model.account.SignInResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.jivesoftware.smack.XMPPException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;
import static com.metfone.selfcare.util.EnumUtils.SINGIN;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

public class GameFragment extends BaseFragment implements SensorEventListener {
    public static final String TAG = GameFragment.class.getSimpleName();
    public static boolean isWaitingForResult = false;

    protected boolean menuShowBefore = false;

    private Unbinder unbinder;
    private static final String PARAM_GAME_URL = "PARAM_GAME_URL";
    private static GameFragment instance;
    private CallbackManager callbackManager;
    private String token;
    public boolean forceToClose = false;
    private ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private UserInfoBusiness userInfoBusiness;
    private ApplicationController mApplication;

    /* Shake handler field */
    private SensorManager mSensorManager;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;



    @BindView(R.id.wv_content)
    WebView wvContent;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    public static GameFragment newInstance(String gameUrl){

        GameFragment fragment = new GameFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_GAME_URL, gameUrl);
        fragment.setArguments(bundle);
        instance = fragment;
        return fragment;
    }

    public static GameFragment get(){
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        menuShowBefore = menuIsShowing();

        mSensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        Objects.requireNonNull(mSensorManager).registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 10f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;

        userInfoBusiness = new UserInfoBusiness(mActivity);
        mApplication = (ApplicationController) getContext().getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        wvContent.getSettings().setJavaScriptEnabled(true);
        wvContent.getSettings().setDomStorageEnabled(true);
        wvContent.getSettings().setUserAgentString("camid_user_agent");
        wvContent.addJavascriptInterface(new WebViewInterface(), "Android");
        wvContent.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        String url = getArguments().getString(PARAM_GAME_URL);
        token = AccountBusiness.getInstance().getToken().replace("Bearer","").trim();
        url = String.format("%s?auth=%s&lang=%s",url, token, LocaleManager.getLanguage(getContext()));
        wvContent.loadUrl(url);

    }




    @OnClick(R.id.ic_close)
    public void onCloseClicked(){
        if(mActivity instanceof DeepLinkActivity){
            mActivity.finish();
            return;
        }
        forceToClose = true;
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        if(forceToClose){
            super.onBackPressed();
        }
        if(wvContent.canGoBack()){
            wvContent.goBack();
        }
        else {
            forceToClose = true;
            super.onBackPressed();
        }

    }

    private boolean menuIsShowing() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            BottomNavigationBar bottomNavigationBar = ((HomeActivity) activity).getNavigationBar();
            if (bottomNavigationBar != null && bottomNavigationBar.getVisibility() == View.VISIBLE) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String getName() {
        return GameFragment.class.getSimpleName();
    }

    @Override
    public void onPause() {
        wvContent.reload();
        mSensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        isWaitingForResult = false;
        super.onDestroy();
    }

    @Override
    public void onResume() {
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        hideBottomMenu();
        super.onResume();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_game;
    }



    @Override
    public void onDetach() {
        super.onDetach();
        showMenuAgain();
        if(wvContent != null){
            wvContent.destroy();
        }
    }

    protected void showMenuAgain(){
        if(menuShowBefore){
            showBottomMenu();
        }else{
            hideBottomMenu();
        }
    }

    protected void hideBottomMenu() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            BottomNavigationBar bottomNavigationBar = ((HomeActivity) activity).getNavigationBar();
            if (bottomNavigationBar != null && bottomNavigationBar.getVisibility() == View.VISIBLE) {
                bottomNavigationBar.setVisibility(View.GONE);
            }
        }
    }

    protected void showBottomMenu() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            BottomNavigationBar bottomNavigationBar = ((HomeActivity) activity).getNavigationBar();
            if (bottomNavigationBar != null && bottomNavigationBar.getVisibility() != View.VISIBLE) {
                bottomNavigationBar.setVisibility(View.VISIBLE);
            }
        }
    }
    protected int getNavigationBarHeight() {
        Activity activity = getActivity();
        Rect rectangle = new Rect();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
        activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        return displayMetrics.heightPixels - (rectangle.top + rectangle.height());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        isWaitingForResult = false;
        if(callbackManager != null){
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        mAccelLast = mAccelCurrent;
        mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
        float delta = mAccelCurrent - mAccelLast;
        mAccel = mAccel * 0.9f + delta;
        if (mAccel > 20) {
            wvContent.evaluateJavascript(String.format("onSendMessageToWeb(%s,%s,%s)", "shake", null ,token),null);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }


    class WebViewInterface implements  FacebookHelper.OnFacebookListener{
        private Message message;
        private List<IdentifyProvider> identifyProviderList;
        /* This method will trigger by javascript code on webView */
        @JavascriptInterface
        public void onSendMessageToNative(String json){
            identifyProviderList = userInfoBusiness.getIdentifyProviderList();
            if (!NetworkHelper.isConnectInternet(mActivity)) {
                new DialogConfirm(mActivity, true).setLabel(null).setMessage(getString(R.string.error_internet_disconnect)).setNegativeLabel(getResources().getString(R.string.close)).show();
            } else {
                message = new Gson().fromJson(json, Message.class);
                if (identifyProviderList != null && identifyProviderList.size() > 0) {
                    if (foundIndex(identifyProviderList, getString(R.string.text_social_type_facebook)) != -1) {
                        shareToFacebook(message);
                    }
                    else {
                        isWaitingForResult = true;
                        FacebookHelper facebookHelper = new FacebookHelper(mActivity);
                        facebookHelper.getProfile(callbackManager, this, FacebookHelper.PendingAction.GET_USER_INFO);
                    }
                }
                else {
                    isWaitingForResult = true;
                    FacebookHelper facebookHelper = new FacebookHelper(mActivity);
                    facebookHelper.getProfile(callbackManager, this, FacebookHelper.PendingAction.GET_USER_INFO);
                }
            }
        }

        private void shareToFacebook(Message message){
            if(ShareDialog.canShow(ShareLinkContent.class) && message != null && message.data != null && !Strings.isEmptyOrWhitespace(message.data.href)){
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(message.data.href))
                        .build();
                ShareDialog shareDialog = new ShareDialog(GameFragment.this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

                    @Override
                    public void onSuccess(Sharer.Result result) {
                        wvContent.evaluateJavascript(String.format("onSendMessageToWeb(%s,%s,%s)", "share", true ,token),null);
                    }

                    @Override
                    public void onCancel() { }

                    @Override
                    public void onError(FacebookException error) {
                        wvContent.evaluateJavascript(String.format("onSendMessageToWeb(%s,%s,%s)", "share", false ,token),null);
                    }
                });
                shareDialog.show(content);
            }
        }

        @Override
        public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
            String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
            AccessToken accessToken = AccessToken.getCurrentAccessToken();
            LinkAccountRequest request = new LinkAccountRequest(accessToken.getToken(), getString(R.string.text_social_type_facebook));
            BaseRequest<LinkAccountRequest> linkAccountRequestBaseRequest = new BaseRequest<>();
            linkAccountRequestBaseRequest.setWsRequest(request);
            String json = new Gson().toJson(linkAccountRequestBaseRequest);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);
            Call<GetUserResponse> call = apiService.linkAccount(token, requestBody);
            call.enqueue(new Callback<GetUserResponse>() {
                @Override
                public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    if (response.code() == 401) {
                        restartApp();
                    } else {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())) {
                                if (response.body().getData() != null) {
                                    userInfoBusiness.setUser(response.body().getData().getUser());
                                    userInfoBusiness.setServiceList(response.body().getData().getServices());
                                    userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                                }
                                if (!NetworkHelper.isConnectInternet(mActivity)) {
                                    showError(getString(R.string.error_internet_disconnect), null);
                                } else {
                                    //call api to pass reject verify id number
                                    getUserInformation();
                                }
                            } else {
                                ToastUtils.showToast(mActivity, response.body().getMessage());
                            }
                        } else {
                            try {
                                JSONObject jObjError = null;
                                if (response.errorBody() != null) {
                                    jObjError = new JSONObject(response.errorBody().string());
                                }
                                if (jObjError != null) {
                                    ToastUtils.showToast(mActivity, jObjError.getJSONObject("error").getString("message"));
                                }
                            } catch (Exception e) {
                                ToastUtils.showToast(mActivity, e.getMessage());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetUserResponse> call, Throwable t) {
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    ToastUtils.showToast(mActivity, getString(R.string.service_error));
                }
            });
        }

        private void getUserInformation() {
            pbLoading.setVisibility(View.VISIBLE);
            String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
            apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
                @Override
                public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    if (response.code() == 401) {
                        restartApp();
                    } else {
                        if (response.body() != null) {
                            if ("00".equals(response.body().getCode())) {
                                UserInfo userInfo = response.body().getData().getUser();
                                userInfoBusiness.setUser(userInfo);
                                if (response.body().getData().getServices() != null) {
                                    userInfoBusiness.setServiceList(response.body().getData().getServices());
                                }
                                userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                                if (response.body().getData().getServices() != null) {
                                    userInfoBusiness.setServiceList(response.body().getData().getServices());
                                }
                                userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                                shareToFacebook(message);

                            } else {
                                ToastUtils.showToast(mActivity, response.body().getMessage());
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<GetUserResponse> call, Throwable t) {
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    ToastUtils.showToast(mActivity,getString(R.string.service_error));
                }
            });
        }

        public void restartApp() {
            com.metfone.selfcare.module.newdetails.utils.ToastUtils.makeText(mActivity, "Account is login by another user!");
            try {
                new ReengAccountBusiness(mApplication).deactivateAccount(mActivity.getApplicationContext());
                new UserInfoBusiness(mActivity.getApplicationContext()).clearCache();
            } catch (XMPPException e) {
                e.printStackTrace();
            }

            mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_MESSAGE);
            mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_OTHER);
            ((ApplicationController) mActivity.getApplication()).getXmppManager().destroyXmpp();
            mApplication.removeCountNotificationIcon();
            mApplication.setRegisterSmsOtp(false);
            //logout facebook + reinit
            if (!Config.Server.FREE_15_DAYS) {
                LoginManager.getInstance().logOut();
            }
            // reset music
            mApplication.getMusicBusiness().clearSessionAndNotifyMusic();
            if (mApplication != null && mApplication.getReengAccountBusiness() != null && mApplication.getReengAccountBusiness().getCurrentAccount() != null){
                mApplication.getReengAccountBusiness().getCurrentAccount().setNumberJid("");
            }
            Intent intent = new Intent(mActivity, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        private int foundIndex(List<IdentifyProvider> identifyProviderList, String type) {
            for (int i = 0; i < identifyProviderList.size(); i++) {
                if (type.equals(identifyProviderList.get(i).getIdentityProvider())) {
                    return i;
                }
            }
            return -1;
        }

        public void showError(String errorMessage, String title) {
            new DialogConfirm(mActivity, true).setLabel(title).setMessage(errorMessage).
                    setNegativeLabel(getResources().getString(R.string.close)).show();
        }

        class Message{
            public String command;
            public Data data;
        }

        class Data{
           public String href;
        }
    }

}

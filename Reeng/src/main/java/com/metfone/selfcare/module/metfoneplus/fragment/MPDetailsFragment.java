package com.metfone.selfcare.module.metfoneplus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.fragment.HomePagerFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.AccountOcsValue;
import com.metfone.selfcare.model.camid.DetailType;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.module.metfoneplus.HalfCircleHigherOverlayView;
import com.metfone.selfcare.module.metfoneplus.activity.ExchangeGuidelineActivity;
import com.metfone.selfcare.module.metfoneplus.adapter.DetailBalanceVPAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.NumberPickerBottomSheetDialogFragment;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.network.camid.response.ServiceResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountsOcsDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetSubAccountInfoNewResponse;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_FIRST_TIME_OPEN_TAB_METFONE;

public class MPDetailsFragment extends MPBaseFragment {
    public static final String TAG = "MPDetailsFragment";

    private RelativeLayout mLayoutActionBar;
    private RelativeLayout mActionBarBack;
    private AppCompatTextView mActionBarTitle;
    private AppCompatTextView mPhoneNumber;
    private AppCompatTextView mBlockOneWayDate;
    private AppCompatTextView mBlockTwoWayDate;
    private AppCompatTextView mTitleActivated;
    private AppCompatTextView mContentActivated;
    private Spinner mHistorySpinner;
    private AppCompatImageView mHistoryArrow;
    private TabLayout mDetailTab;
    private ViewPager mViewpagerDetail;
    private AppCompatImageView mImgAccountDetail;
    private AppCompatTextView mChangePhoneNumberTitle;
    private RelativeLayout mLayoutInformation;
    private AppCompatImageView mImgSim;
    private HalfCircleHigherOverlayView mCircleHigherOverlayView;
    private ConstraintLayout mRootConstrain;
    private DetailBalanceVPAdapter mDetailBalanceVPAdapter;
    private TextView tvDone, tvTutor;
    private boolean mIsChangePhoneNumber = false;
    private boolean mIsBottomSheetShowing = false;
    private static boolean mIsShowAnimation = false;
    private CoordinatorLayout coordinatorLayout;
    private Button btnDisableTouch;
    // Tab titles
    private String[] mTitleBalanceList;
    private String[] mSpinnerType;

    /**
     * key: phone is formatted
     * value: user_service_id
     */
    private LinkedHashMap<String, Integer> mPhoneNumberList = null;
    private List<MPHistoryChargeFragment> mMPHistoryChargeFragmentList;
    private String mPhoneServiceDefault;
    private String mPhoneServiceSelect;
    private static MPDetailsFragment fragment;
    private boolean wasUpdateMetfoneNum = false;
    // Get value spinner: today, 7days, 30days
    @DetailType.DateType
    private String mDateType;
    @DetailType.DateName
    private String mDateName;

    public MPDetailsFragment() {

    }

    public static MPDetailsFragment self() {
        return fragment;
    }

    public static MPDetailsFragment newInstance() {
        fragment = new MPDetailsFragment();
        return fragment;
    }

    public static boolean isGuideline() {
        return mIsShowAnimation;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mp_detail;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onShowAnimation() {
        mLayoutActionBar.setVisibility(View.VISIBLE);
        if(mCircleHigherOverlayView != null)
            mCircleHigherOverlayView.setVisibility(View.VISIBLE);
        btnDisableTouch.setVisibility(View.VISIBLE);
        new CountDownTimer(500, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if(mCircleHigherOverlayView != null)
                    mCircleHigherOverlayView.startAnimation();
            }
        }.start();
    }

    public void setIsShowAnimation(boolean b) {
        mIsShowAnimation = b;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initAllView(view);
        mActionBarTitle.setText(getString(R.string.m_p_detail_title));
        Utilities.adaptViewForInserts(mLayoutActionBar);
        mTitleBalanceList = mParentActivity.getResources().getStringArray(R.array.m_p_detail_history_charge_type);
        mSpinnerType = ApplicationController.self().getResources().getStringArray(R.array.m_p_detail_history_charge_date_type);
        mPhoneServiceDefault = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_PHONE_SERVICE, String.class);
        mMPHistoryChargeFragmentList = new ArrayList<>();

        initHistoryChargeViewPager();
        createSelectHistorySpinner();
        if (mIsShowAnimation) {
            onShowAnimation();
        } else {
            btnDisableTouch.setVisibility(View.GONE);
        }
    }

    private void initAllView(View view) {
        mLayoutActionBar = view.findViewById(R.id.mp_layout_action_bar);
        mActionBarBack = view.findViewById(R.id.action_bar_back);
        mActionBarTitle = view.findViewById(R.id.action_bar_title);
        mPhoneNumber = view.findViewById(R.id.txt_detail_phone_number);
        mBlockOneWayDate = view.findViewById(R.id.txt_block_one_way_date);
        mBlockTwoWayDate = view.findViewById(R.id.txt_block_two_way_date);
        mTitleActivated = view.findViewById(R.id.activated);
        mContentActivated = view.findViewById(R.id.txt_activated);
        mHistorySpinner = view.findViewById(R.id.history_spinner);
        mHistoryArrow = view.findViewById(R.id.history_image);
        mDetailTab = view.findViewById(R.id.tab_detail);
        mViewpagerDetail = view.findViewById(R.id.view_pager_detail);
        mImgAccountDetail = view.findViewById(R.id.img_account_detail);
        mChangePhoneNumberTitle = view.findViewById(R.id.txt_change_phone_number_title);
        mLayoutInformation = view.findViewById(R.id.layout_information);
        mImgSim = view.findViewById(R.id.img_sim);
//        if(mParentActivity instanceof ExchangeGuidelineActivity){
//            mCircleHigherOverlayView = ExchangeGuidelineActivity.self().getHalfCircleHigherOverlayView();
//            tvDone = ExchangeGuidelineActivity.self().getTvDone();
//            tvTutor = ExchangeGuidelineActivity.self().getTvTutor2();
//            if(ExchangeGuidelineActivity.self().getTvTutor() != null)
//                ExchangeGuidelineActivity.self().getTvTutor().setVisibility(View.GONE);
//        }
        if(tvDone != null)
            tvDone.setVisibility(View.VISIBLE);
        if(tvTutor != null)
            tvTutor.setVisibility(View.VISIBLE);
        btnDisableTouch = view.findViewById(R.id.btn_disable_touch);
        btnDisableTouch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                return;
            }
        });
//        mImgSim.setOnClickListener((v) -> gotoMPDetailAccountFragment());
        if (!mIsShowAnimation) {
            handleOnClickListener();
            mDetailTab.setFocusable(false);
            mViewpagerDetail.setFocusable(false);
        } else {
//            mDetailTab.setOnTabSelectedListener(null);
//            mDetailTab.setOnClickListener(null);
//            mViewpagerDetail.setFocusable(false);
//            mDetailTab.setOnTabSelectedListener(null);
//            mViewpagerDetail.setOnTouchListener(null);
//            mViewpagerDetail.setOnTouchListener(null);

            if (tvDone != null) {
                tvDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DynamicSharePref.getInstance().put(PREF_FIRST_TIME_OPEN_TAB_METFONE, false);
                        mParentActivity.finish();
                    }
                });
            }
            if (tvTutor != null) {
                tvTutor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mParentActivity.finish();
                    }
                });
            }
        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onChangeMetfoneNumber(UserInfo userInfo) {
//        mPhoneNumber.setText(Utilities.formatPhoneNumberCambodia(userInfo.getPhone_number()));
//        wasUpdateMetfoneNum = true;
        if(mPhoneNumberList != null && mPhoneNumberList.size() != 0){
            String phone = "0"+getCamIdUserBusiness().getMetfoneUsernameIsdn().substring(0,2)+" "+getCamIdUserBusiness().getMetfoneUsernameIsdn().substring(2);
            int userServiceId = Objects.requireNonNull(mPhoneNumberList.get(phone));
            updatePhoneService(userServiceId);
        }
    }

    public void handleOnClickListener() {
        mActionBarBack.setOnClickListener((v) -> {
            if(mParentActivity instanceof HomeActivity){
                popBackStackFragment();
            }else{
                mParentActivity.finish();
            }
        });
        mPhoneNumber.setOnClickListener((v) -> showBottomSheetChangePhone());
        mImgAccountDetail.setOnClickListener((v) -> showBottomSheetChangePhone());
        mChangePhoneNumberTitle.setOnClickListener((v) -> showBottomSheetChangePhone());
        mLayoutInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMPDetailAccountFragment();
                wasUpdateMetfoneNum = false;
            }
        });
        mHistoryArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHistorySpinner.performClick();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            mParentActivity.showError(mRes.getString(R.string.error_internet_disconnect), null);
        } else {
            mParentActivity.showLoadingDialog("", R.string.waiting);
            getWSGetAccountsOcsDetail();
        }
    }

    private void createSelectHistorySpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.m_p_detail_history_charge_date_type, R.layout.mp_detail_simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.mp_feedback_simple_dropdown_item);
        // Apply the adapter to the spinner
        mHistorySpinner.setAdapter(adapter);

        // Set date default: 7 days
        mHistorySpinner.setSelection(1);
        mHistorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mSpinnerType[i].equals(DetailType.DATE_TODAY)) {
                    mDateType = DetailType.DATE_TYPE_TODAY;
                    mDateName = DetailType.DATE_TODAY;
                } else if (mSpinnerType[i].equals(DetailType.DATE_SEVEN_DAYS)) {
                    mDateType = DetailType.DATE_TYPE_SEVEN_DAYS;
                    mDateName = DetailType.DATE_SEVEN_DAYS;
                } else {
                    mDateType = DetailType.DATE_TYPE_THIRTY_DAYS;
                    mDateName = DetailType.DATE_THIRTY_DAYS;
                }

                reloadHistoryChargeFragment(mDateType, mDateName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    /**
     * BottomSheetDialog for change phone number.
     * the service phone number is always displayed first
     */
    private void showBottomSheetChangePhone() {
        if (!isAdded() || mPhoneNumberList == null || mPhoneNumberList.size() == 0 || mIsBottomSheetShowing) {
            return;
        }

        // Because mPhoneNumberList is sorted, phoneServiceCurrent always is the first element of list
        // position = 0
        int position = 0;
        String[] value = mPhoneNumberList.keySet().toArray(new String[0]);
        String title = getString(R.string.m_p_item_detail_bottom_sheet_change_phone);
        NumberPickerBottomSheetDialogFragment mChangePhoneBottomSheet = NumberPickerBottomSheetDialogFragment.newInstance(title, value, position);
        mChangePhoneBottomSheet.setOnNumberPickerBottomSheetOnClick(new NumberPickerBottomSheetDialogFragment.OnNumberPickerBottomSheetOnClick() {
            @Override
            public void onDone(String valueSelected, int position) {
                Log.e(TAG, "onDone: valueSelected = " + valueSelected + " -- mPhoneServiceDefault = " + mPhoneServiceDefault);
                // format phone number:
                // show: xxx xxxxx but value: xxxxxxxx -> replace(" ", "")
                if (valueSelected != null && !valueSelected.replace(" ", "").equals(mPhoneServiceDefault)) {
                    int userServiceId = Objects.requireNonNull(mPhoneNumberList.get(valueSelected));
                    mIsChangePhoneNumber = true;
                    wasUpdateMetfoneNum = false;
                    updatePhoneService(userServiceId);
                }
            }

            @Override
            public void onDismiss() {
                mIsBottomSheetShowing = false;
            }
        });
        mIsBottomSheetShowing = true;
        mChangePhoneBottomSheet.show(mParentActivity.getSupportFragmentManager(), mChangePhoneBottomSheet.getClass().getSimpleName());
    }

    /**
     * Init all fragment of History charge
     */
    private void initHistoryChargeViewPager() {
        MPHistoryChargeFragment f;

        // Fragment # Exchange
        f = MPHistoryChargeFragment.newInstance(DetailType.BALANCE_EXCHANGE_NAME,
                DetailType.DATE_TYPE_SEVEN_DAYS,
                DetailType.DATE_SEVEN_DAYS);
        mMPHistoryChargeFragmentList.add(f);

        // Fragment # Basic
        f = MPHistoryChargeFragment.newInstance(DetailType.BALANCE_BASIC_NAME,
                DetailType.DATE_TYPE_SEVEN_DAYS,
                DetailType.DATE_SEVEN_DAYS);
        mMPHistoryChargeFragmentList.add(f);

        // Fragment # Promotion
        f = MPHistoryChargeFragment.newInstance(DetailType.BALANCE_PROMOTION_NAME,
                DetailType.DATE_TYPE_SEVEN_DAYS,
                DetailType.DATE_SEVEN_DAYS);
        mMPHistoryChargeFragmentList.add(f);

        // Fragment # Data
        f = MPHistoryChargeFragment.newInstance(DetailType.BALANCE_DATA_NAME,
                DetailType.DATE_TYPE_SEVEN_DAYS,
                DetailType.DATE_SEVEN_DAYS);
        mMPHistoryChargeFragmentList.add(f);

        mDetailBalanceVPAdapter = new DetailBalanceVPAdapter(getChildFragmentManager(),
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
                mTitleBalanceList,
                mMPHistoryChargeFragmentList);

        mViewpagerDetail.setAdapter(mDetailBalanceVPAdapter);
        mViewpagerDetail.setOffscreenPageLimit(4);
        mDetailTab.setupWithViewPager(mViewpagerDetail);
    }

    /**
     * Reload information of all history charge fragment when
     * - change date history charge
     * - change phone number
     */
    private void reloadHistoryChargeFragment(@DetailType.DateType String chargeDateType,
                                             @DetailType.DateName String dateName) {
        for (MPHistoryChargeFragment f : mMPHistoryChargeFragmentList) {
            f.forceReloadInfoWSHistoryCharge(f.getChargeType(), chargeDateType, dateName);
        }
    }

    private void getWSGetAccountsOcsDetail() {
        MetfonePlusClient client = new MetfonePlusClient();
        client.wsGetAccountsOcsDetail(new MPApiCallback<WsGetAccountsOcsDetailResponse>() {
            @Override
            public void onResponse(Response<WsGetAccountsOcsDetailResponse> response) {
                // 01-14-22 Fix crash bug when context is null or not
                if (mParentActivity != null) {
                    if (response.body() != null
                            && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                        if (response.body().getResult().getWsResponse() == null) {
                            ToastUtils.showToast(mParentActivity, response.body().getResult().getUserMsg());
                            mParentActivity.hideLoadingDialog();
                            return;
                        }
                        List<WsGetAccountsOcsDetailResponse.Response> wsResponse = response.body().getResult().getWsResponse();
                        if (wsResponse != null) {
                            for (WsGetAccountsOcsDetailResponse.Response res : wsResponse) {
                                if (res.getTitle().equals(getString(R.string.m_p_detail_title_compare_api_general_info))) {
//                                if (!wasUpdateMetfoneNum) {
                                    mPhoneNumber.setText(Utilities.formatPhoneNumberCambodia(getCamIdUserBusiness().getMetfoneUsernameIsdn()));
//                                }
                                    if (res.getAccountOcsValues() != null) {
                                        updateUIMetfoneCard(res.getAccountOcsValues());
                                    }
                                }
                            }
                        }

                        getWSGetSubAccountInfoNew();

                    } else {
                        Log.e(TAG, "getWSGetAccountsOcsDetail - onResponse: error");
                        mParentActivity.hideLoadingDialog();
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    /**
     * Get value of item have "title" = "GENERAL INFO"
     *
     * @param accountOcsValue list info of item GENERAL_INFO (Number, Block One Way Date, Block Two Ways Date, Status)
     */
    private void updateUIMetfoneCard(List<AccountOcsValue> accountOcsValue) {
        for (AccountOcsValue value : accountOcsValue) {
//            if (value.getTitle().equals(getString(R.string.m_p_detail_phone_number_compare_with_api))) {
//                mPhoneNumber.setText(Utilities.formatPhoneNumberCambodia(value.getValue()));
//            } else
            if (value.getTitle().equals(getString(R.string.m_p_detail_block_one_way_date_compare_api))) {
                mBlockOneWayDate.setText(value.getValue());
            } else if (value.getTitle().equals(getString(R.string.m_p_detail_block_two_way_date_compare_api))) {
                mBlockTwoWayDate.setText(value.getValue());
            } else if (value.getTitle().equals(getString(R.string.m_p_detail_activated_compare_api))) {
                mTitleActivated.setText(value.getValue());
            }
        }
    }

    private void getWSGetSubAccountInfoNew() {
        MetfonePlusClient client = new MetfonePlusClient();
        client.wsGetSubAccountInfoNew(new MPApiCallback<WsGetSubAccountInfoNewResponse>() {
            @Override
            public void onResponse(Response<WsGetSubAccountInfoNewResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    if (response.body().getResult().getWsResponse() == null) {
                        ToastUtils.showToast(mParentActivity, response.body().getResult().getUserMsg());
                        mParentActivity.hideLoadingDialog();
                        return;
                    }

                    WsGetSubAccountInfoNewResponse.Response res = response.body().getResult().getWsResponse();
                    if (res != null) {
                        mContentActivated.setText(res.getName());
                    }

                    getService();
                } else {
                    Log.e(TAG, "getWSGetSubAccountInfoNew - onResponse: error");
                    mParentActivity.hideLoadingDialog();
                }
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void getService() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.getService(new ApiCallback<ServiceResponse>() {
            @Override
            public void onResponse(Response<ServiceResponse> response) {
                if (response.body() != null) {
                    mPhoneNumberList = new LinkedHashMap<>();

                    List<Service> services = response.body().getData();

                    List<PhoneLinked> listNotContainPhoneService = new ArrayList<>();
                    PhoneLinked p = null;

                    for (Service s : services) {
                        List<PhoneLinked> phoneLinkeds = s.getPhoneLinked();
                        for (int i = 0; i < phoneLinkeds.size(); i++) {
                            PhoneLinked phoneLinked = phoneLinkeds.get(i);
                            if (phoneLinked.getMetfonePlus() == 1) {
                                p = phoneLinked;
                            } else {
                                listNotContainPhoneService.add(phoneLinked);
                            }

                        }
                    }

                    // Sort list phone in change phone number
                    // Service number is always the first number in list
                    if (p != null) {
                        String phone = Utilities.formatPhoneNumberCambodia(p.getPhoneNumber());
                        mPhoneNumberList.put(phone, p.getUserServiceId());

                        for (int i = 0; i < listNotContainPhoneService.size(); i++) {
                            phone = Utilities.formatPhoneNumberCambodia(listNotContainPhoneService.get(i).getPhoneNumber());
                            mPhoneNumberList.put(phone, listNotContainPhoneService.get(i).getUserServiceId());
                        }

                        showContent();

                        if (mIsChangePhoneNumber) {
                            reloadHistoryChargeFragment(mDateType, mDateName);
                            mIsChangePhoneNumber = false;
                        }
                    }

                    mParentActivity.hideLoadingDialog();
                }
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    /**
     * Update metfone when user tap "Done" button on BottomSheet Change phone number
     * if the phone number is a service number and != the current service number
     *
     * @param userServiceId
     */
    private void updatePhoneService(int userServiceId) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.updateMetfone(null, null, userServiceId, new ApiCallback<AddServiceResponse>() {
            @Override
            public void onResponse(Response<AddServiceResponse> response) {
                if (response.body() != null && response.body().getData() != null) {
                    getCamIdUserBusiness().setPhoneService(response.body().getData().getServices());
                    getCamIdUserBusiness().setUser(response.body().getData().getUser());
                    getCamIdUserBusiness().setService(response.body().getData().getServices());

                    if (!"".equals(getCamIdUserBusiness().getPhoneService())) {
                        autoLogin(getCamIdUserBusiness().getPhoneService());
                    }
                } else {
                    mParentActivity.hideLoadingDialog();
                }
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    /**
     * autoLogin to get a new token and sesion_Id  of the phone number that has just been updated
     *
     * @param userName phone_number,
     */
    private void autoLogin(String userName) {
        MetfonePlusClient client = new MetfonePlusClient();
        client.autoLogin(userName, new MPApiCallback<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response) {
                if (response.body() != null && response.body().getUsername() != null) {
                    getWSGetAccountsOcsDetail();
                } else {
                    mParentActivity.hideLoadingDialog();
                }
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }

    private void showContent() {
        mBlockOneWayDate.setVisibility(View.VISIBLE);
        mBlockTwoWayDate.setVisibility(View.VISIBLE);
        mTitleActivated.setVisibility(View.VISIBLE);
        mContentActivated.setVisibility(View.VISIBLE);
        mPhoneNumber.setVisibility(View.VISIBLE);
        mImgAccountDetail.setVisibility(View.VISIBLE);
    }

    @Override
    public void popBackStackFragment() {
        getParentFragmentManager().popBackStack();
    }
}

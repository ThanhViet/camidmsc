package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.metfone.selfcare.module.home_kh.api.WsGiftBoxResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GiftBoxFragmentStateAdapter extends FragmentStateAdapter {

    private List<Fragment> fragments;
    private GiftBoxViewPagerItemFragment giftBoxFragment;
    private HistoryGiftViewPagerFragment historyFragment;
    public GiftBoxFragmentStateAdapter(@NonNull FragmentActivity fragmentActivity, GiftBoxAdapter.EventListener listener, GiftHistoryAdapter.EventListener historyItemListener) {
        super(fragmentActivity);
        giftBoxFragment = new GiftBoxViewPagerItemFragment();
        giftBoxFragment.listener = listener;
        historyFragment = new HistoryGiftViewPagerFragment();
        historyFragment.listener = historyItemListener;
        fragments = Arrays.asList(
                giftBoxFragment,
                historyFragment
        );
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return fragments.get(position);
    }

    @Override
    public int getItemCount() {
        return fragments.size();
    }

    public void updateGiftBox(ArrayList<WsGiftBoxResponse.PrizeInfo> items){
        giftBoxFragment.updateData(items);
    }

    public void updateHistory(ArrayList<WsGiftBoxResponse.PrizeInfo> items){
        historyFragment.updateData(items);
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.search.adapter.SearchAllAdapter;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.utils.ImageBusiness;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.OnClick;

public class MoviesDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_current_episode)
    @Nullable
    TextView tvCurrentEpisode;
    @BindView(R.id.tv_total_episode)
    @Nullable
    TextView tvTotalEpisode;
    @BindView(R.id.tv_subtitle)
    @Nullable
    TextView tvSubtitle;
    @BindView(R.id.tv_narrative)
    @Nullable
    TextView tvNarrative;
    @BindView(R.id.tv_view)
    @Nullable
    TextView tvView;
    @BindView(R.id.tv_duration)
    @Nullable
    TextView tvDuration;

    private SearchAllListener.OnAdapterClick listener;
    private Movie data;
    private Activity activity;
    private int type;

    public MoviesDetailHolder(View view, Activity activity, SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
        this.activity = activity;
        this.type = type;
    }

    public void bindData(Object item, int position, String keySearch) {
        if (item instanceof Movie) {
            data = (Movie) item;
            if (tvTitle != null) {
                tvTitle.setText(data.getName());
            }
            if (tvCurrentEpisode != null) {
                tvCurrentEpisode.setText(
                        tvCurrentEpisode.getResources().getString(R.string.episode_movies, String.valueOf(data.getCurrentEpisode())));
                tvCurrentEpisode.setVisibility(data.getCurrentEpisode() > 0 ? View.VISIBLE : View.GONE);
            }
            if (tvTotalEpisode != null) {
                tvTotalEpisode.setText(
                        tvTotalEpisode.getResources().getString(R.string.total_episodes, data.getTotalEpisodes()));
                tvTotalEpisode.setVisibility(data.getTotalEpisodes() > 0 ? View.VISIBLE : View.GONE);
            }
            if (tvSubtitle != null) {
                tvSubtitle.setVisibility(data.isSubtitleFilm() ? View.VISIBLE : View.GONE);
            }
            if (tvNarrative != null) {
                tvNarrative.setVisibility(data.isNarrativeFilm() ? View.VISIBLE : View.GONE);
            }
            boolean showDot = false;
            if (tvDuration != null && data.getDurationMinutes() > 0) {
                tvDuration.setVisibility(View.VISIBLE);
                tvDuration.setText(tvDuration.getResources().getString(R.string.total_duration_minutes, data.getDurationMinutes()));
                showDot = true;
            }
            if (tvView != null && data.getIsView() > 0) {
                tvView.setVisibility(View.VISIBLE);
                tvView.setText(String.format((data.getIsView() <= 1) ? tvView.getContext().getString(R.string.view)
                        : tvView.getContext().getString(R.string.video_views), Utilities.getTotalView(data.getIsView())));
                tvView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? tvView.getContext().getResources().getDrawable(R.drawable.ic_dot) : null, null, null, null);
            }
            // hide UI for like IOS
            viewRoot.setLayoutParams(new RelativeLayout.LayoutParams(pxFromDp(activity,105),pxFromDp(activity,143)));
            Glide.with(activity).load(((Movie) item).getPosterPath()).into(ivCover);
            tvCurrentEpisode.setVisibility(View.GONE);
            tvDuration.setVisibility(View.GONE);
            tvNarrative.setVisibility(View.GONE);
            tvSubtitle.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            tvTotalEpisode.setVisibility(View.GONE);
            tvView.setVisibility(View.GONE);
            tvCurrentEpisode.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            tvView.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxMovies && data != null) {
            ((SearchAllListener.OnClickBoxMovies) listener).onClickMoviesItem(data);
        }
    }

    @OnClick(R.id.iv_more)
    public void onClickItemMore() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            listener.onClickMenuMore(data);
        }
    }
    public int pxFromDp(Context context, final float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }
}
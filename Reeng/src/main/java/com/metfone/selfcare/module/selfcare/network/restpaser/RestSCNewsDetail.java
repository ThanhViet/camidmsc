package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCNewsDetail;

import java.io.Serializable;

public class RestSCNewsDetail extends AbsResultData implements Serializable {

    @SerializedName("result")
    private SCNewsDetail data;

    public SCNewsDetail getData() {
        return data;
    }

    public void setData(SCNewsDetail data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCNewsDetail [data=" + data + "] errror " + getErrorCode();
    }
}

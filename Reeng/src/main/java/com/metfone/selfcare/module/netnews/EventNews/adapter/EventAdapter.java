package com.metfone.selfcare.module.netnews.EventNews.adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.EventModel;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

/**
 * Created by HaiKE on 8/19/17.
 */

public class EventAdapter extends BaseQuickAdapter<EventModel, BaseViewHolder> {

    private Context context;
    private AbsInterface.OnEventListener onItemListener;

    public EventAdapter(Context context, List<EventModel> datas, AbsInterface.OnEventListener onItemListener) {
        super(R.layout.holder_box_event_news, datas);
        this.context = context;
        this.onItemListener = onItemListener;
    }
    @Override
    protected void convert(final BaseViewHolder holder, final EventModel model) {
        if (holder == null || model == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, model.getEventName());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemListener.onItemEventClick(holder.getAdapterPosition());
                }
            });
        }
        if (model.getData().size() > 0) {
            RecyclerView recyclerView = holder.getView(R.id.recycler_view);
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.setNestedScrollingEnabled(false);
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_default_tiin, true));
            }
            EventItemAdapterV5 childHomeNewAdapterV5 = new EventItemAdapterV5(model, mContext, onItemListener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }


}

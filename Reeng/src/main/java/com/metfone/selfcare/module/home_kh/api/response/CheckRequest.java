package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CheckRequest {
    @SerializedName("address")
    @Expose
    String address;
    @SerializedName("customerMail")
    @Expose
    String customerMail;
    @SerializedName("customerName")
    @Expose
    String customerName;
    @SerializedName("customerPhone")
    @Expose
    String customerPhone;
    @SerializedName("id")
    @Expose
    String id;
    @SerializedName("province")
    @Expose
    String province;
    @SerializedName("speed")
    @Expose
    String speed;
    @SerializedName("status")
    @Expose
    int status;
    @SerializedName("color")
    @Expose
    String color;
    @SerializedName("createDateStr")
    @Expose
    String createDateStr;
    @SerializedName("statusDes")
    @Expose
    String statusDes;
    @SerializedName("monthFee")
    @Expose
    String monthFee;
    @SerializedName("payAdvance")
    @Expose
    String payAdvance;
    @SerializedName("instalation")
    @Expose
    String instalation;
    @SerializedName("deposit")
    @Expose
    String deposit;
    @SerializedName("modemWifi")
    @Expose
    String modemWifi;
    @SerializedName("account")
    @Expose
    String account;
    @SerializedName("reason")
    @Expose
    String reason;
    @SerializedName("regOnlineHis")
    @Expose
    ArrayList<CheckRequestHistory> regOnlineHis;


    public String getAddress() {
        if (address == null) return "";
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerMail() {
        if (customerMail == null) return "";
        return customerMail;
    }

    public void setCustomerMail(String customerMail) {
        this.customerMail = customerMail;
    }

    public String getCustomerName() {
        if (customerName == null) return "";
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        if (customerPhone == null) return "";
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getId() {
        if (id == null) return "";
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvince() {
        if (province == null) return "";
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getSpeed() {
        if (speed == null) return "";
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getColor() {
        if (color == null) return "";
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCreateDateStr() {
        if (createDateStr == null) return "";
        return createDateStr;
    }

    public void setCreateDateStr(String createDateStr) {
        this.createDateStr = createDateStr;
    }

    public String getStatusDes() {
        if (statusDes == null) return "";
        return statusDes;
    }

    public void setStatusDes(String statusDes) {
        this.statusDes = statusDes;
    }

    public String getMonthFee() {
        if (monthFee == null) return "";
        return monthFee;
    }

    public void setMonthFee(String monthFee) {
        this.monthFee = monthFee;
    }

    public String getPayAdvance() {
        if (payAdvance == null) return "";
        return payAdvance;
    }

    public void setPayAdvance(String payAdvance) {
        this.payAdvance = payAdvance;
    }

    public String getInstalation() {
        if (instalation == null) return "";
        return instalation;
    }

    public void setInstalation(String instalation) {
        this.instalation = instalation;
    }

    public String getDeposit() {
        if (deposit == null) return "";
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getModemWifi() {
        if (modemWifi == null) return "";
        return modemWifi;
    }

    public void setModemWifi(String modemWifi) {
        this.modemWifi = modemWifi;
    }

    public String getAccount() {
        if (account == null) return "";
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getReason() {
        if (reason == null) return "";
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ArrayList<CheckRequestHistory> getRegOnlineHis() {
        if (regOnlineHis == null) return new ArrayList<>();
        return regOnlineHis;
    }

    public void setRegOnlineHis(ArrayList<CheckRequestHistory> regOnlineHis) {
        this.regOnlineHis = regOnlineHis;
    }
}


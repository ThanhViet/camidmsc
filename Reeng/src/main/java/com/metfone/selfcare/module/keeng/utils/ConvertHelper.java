package com.metfone.selfcare.module.keeng.utils;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.model.UserInfo;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConvertHelper {
    public static void convertData(List<AllModel> datas, int source) {
        if (datas != null && !datas.isEmpty()) {
            long oldSingerId = 0;
            int oldPosition = -1;
            for (int i = 0; i < datas.size(); i++) {
                if (datas.get(i) == null) {
                    continue;
                }
                AllModel item = datas.get(i);
                item.setSource(source);
                if (item.getType() != Constants.TYPE_SONG) {
                    oldSingerId = 0;
                    oldPosition = -1;
                    continue;
                }
                String songImage = item.getImagePath();
                if (TextUtils.isEmpty(songImage)) {
                    List<String> listImage = item.getListImage();
                    if (listImage.isEmpty()) {
                        oldPosition = -1;
                    } else {
                        if (oldSingerId == item.getSinger_id()) {
                            ++oldPosition;
                            if (oldPosition >= listImage.size())
                                oldPosition = 0;
                            item.setImage(listImage.get(oldPosition));
                            item.setImage310(listImage.get(oldPosition));
                        } else {
                            oldPosition = 0;
                            item.setImage(listImage.get(oldPosition));
                            item.setImage310(listImage.get(oldPosition));
                        }
                    }
                } else {
                    oldPosition = -1;
                }
                oldSingerId = item.getSinger_id();
            }
        }
    }

    public static void convertData(List<AllModel> medias, int source, int type) {
        if (medias != null && !medias.isEmpty()) {
            int size = medias.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (medias.get(i) == null || medias.get(i).getType() != type) {
                    medias.remove(i);
                } else {
                    medias.get(i).setSource(source);
                }
            }
            if (type == Constants.TYPE_SONG) {
                long oldSingerId = 0;
                int oldPosition = -1;
                for (int i = 0; i < medias.size(); i++) {
                    AllModel item = medias.get(i);
                    String songImage = item.getImagePath();
                    if (TextUtils.isEmpty(songImage)) {
                        List<String> listImage = item.getListImage();
                        if (listImage.isEmpty()) {
                            oldPosition = -1;
                        } else {
                            if (oldSingerId == item.getSinger_id()) {
                                ++oldPosition;
                                if (oldPosition >= listImage.size())
                                    oldPosition = 0;
                                item.setImage(listImage.get(oldPosition));
                                item.setImage310(listImage.get(oldPosition));
                            } else {
                                oldPosition = 0;
                                item.setImage(listImage.get(oldPosition));
                                item.setImage310(listImage.get(oldPosition));
                            }
                        }
                    } else {
                        oldPosition = -1;
                    }
                    oldSingerId = item.getSinger_id();
                }
            }
        }
    }

    public static void convertDataOld(List<AllModel> medias, int source, int type) {
        if (medias != null && !medias.isEmpty()) {
            long oldSingerId = 0;
            int oldPosition = -1;
            int size = medias.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (medias.get(i) == null || medias.get(i).getType() != type) {
                    medias.remove(i);
                } else {
                    AllModel item = medias.get(i);
                    item.setSource(source);
                    if (type == Constants.TYPE_SONG) {
                        String songImage = item.getImagePath();
                        if (TextUtils.isEmpty(songImage)) {
                            List<String> listImage = item.getListImage();
                            if (listImage.isEmpty()) {
                                oldPosition = -1;
                            } else {
                                if (oldSingerId == item.getSinger_id()) {
                                    ++oldPosition;
                                    if (oldPosition >= listImage.size())
                                        oldPosition = 0;
                                    item.setImage(listImage.get(oldPosition));
                                    item.setImage310(listImage.get(oldPosition));
                                } else {
                                    oldPosition = 0;
                                    item.setImage(listImage.get(oldPosition));
                                    item.setImage310(listImage.get(oldPosition));
                                }
                            }
                        } else {
                            oldPosition = -1;
                        }
                        oldSingerId = item.getSinger_id();
                    }
                }
            }
        }
    }

    public static void convertData(List<AllModel> datas) {
        if (datas != null && !datas.isEmpty()) {
            long oldSingerId = 0;
            int oldPosition = -1;
            for (int i = 0; i < datas.size(); i++) {
                if (datas.get(i) == null) {
                    continue;
                }
                AllModel item = datas.get(i);
                if (item == null || item.getType() != Constants.TYPE_SONG) {
                    oldSingerId = 0;
                    oldPosition = -1;
                    continue;
                }
                String songImage = item.getImagePath();
                if (TextUtils.isEmpty(songImage)) {
                    List<String> listImage = item.getListImage();
                    if (listImage.isEmpty()) {
                        oldPosition = -1;
                    } else {
                        if (oldSingerId == item.getSinger_id()) {
                            ++oldPosition;
                            if (oldPosition >= listImage.size())
                                oldPosition = 0;
                            item.setImage(listImage.get(oldPosition));
                            item.setImage310(listImage.get(oldPosition));
                        } else {
                            oldPosition = 0;
                            item.setImage(listImage.get(oldPosition));
                            item.setImage310(listImage.get(oldPosition));
                        }
                    }
                } else {
                    oldPosition = -1;
                }
                oldSingerId = item.getSinger_id();
            }
        }
    }

    public static PlayListModel convertToPlaylist(AllModel item) {
        PlayListModel data = new PlayListModel();
        if (item != null) {
            data.setId(item.getId());
            data.setName(item.getName());
            data.setType(item.getType());
            data.setUrl(item.getUrl());
            data.setUser(item.getUserInfo());
            data.setFavorite(item.isFavorite());
        }
        return data;
    }

    public static List<AllModel> getListDatas(int position, List<AllModel> datas) {
        List<AllModel> list = new ArrayList<>();
        int count = 0;
        final int max = 20;
        for (int i = position; i < datas.size(); i++) {
            if (count >= max)
                break;
            list.add(datas.get(i));
            count++;
        }
        if (count < max) {
            for (int i = 0; i < position; i++) {
                if (count >= max)
                    break;
                list.add(datas.get(i));
                count++;
            }
        }
        return list;
    }

    public static PlayListModel convertSearchDataToPlaylist(SearchModel item) {
        if (item == null)
            return null;
        PlayListModel media = new PlayListModel();
        try {
            media.id = Long.valueOf(item.id);
        } catch (Exception e) {
//            Log.e(TAG, e);
        }
        media.setName(item.getFullName());
        media.setUser(new UserInfo(item.getFullSinger()));
        media.setType(item.getType());
        return media;
    }

    public static AllModel convertSearchDataToMedia(SearchModel item) {
        if (item == null)
            return null;
        AllModel media = new AllModel();
        try {
            media.id = Long.valueOf(item.id);
        } catch (Exception e) {
//            Log.e(TAG, e);
        }
        media.name = item.getFullName();
        media.singer = item.getFullSinger();
        media.setListened(item.getListened());
        media.type = item.getType();
        media.setIsLossless(item.getIsLossless());
        media.setSource(MediaLogModel.SRC_SEARCH);
        return media;
    }

    public static MediaModel convertKeengToAudioMocha(AllModel item) {
        MediaModel mediaModel = new MediaModel();

        String mediaUrl = item.getMediaUrl();
        try {
            String[] list = mediaUrl.split("\\?");
            if (list.length > 1)
                mediaUrl = UrlConfigHelper.getInstance(ApplicationController.self().getApplicationContext()).getDomainKeengMusic() + "/api/keeng/play?" + list[1];
        } catch (Exception e) {
        }

        mediaModel.setMedia_url(mediaUrl);
        mediaModel.setType(MediaModel.TYPE_BAI_HAT);
        mediaModel.setImage(item.getImage());
        mediaModel.setId(item.getId() + "");
        mediaModel.setSinger(item.getSinger());
        mediaModel.setName(item.getName());
        mediaModel.setUrl(item.getUrl());
        mediaModel.setListened(item.getListened());
        mediaModel.setLyricUrl(item.getLyricUrl());
        mediaModel.setLyric(item.getLyric());
        mediaModel.setImage310(item.getImage310());
        mediaModel.setMonopoly(item.getDocQuyen());
        mediaModel.setFromSource(MediaModel.FROM_SOURCE_KEENG);
        mediaModel.setIdentify(item.getIdentify());
        return mediaModel;
    }

    public static Video convertNetnewsToVideoMocha(NewsModel model) {
        String url = model.getUrl();
        if (!url.contains(CommonUtils.DOMAIN)) {
            url = CommonUtils.DOMAIN + url;
        }

        Video videoModel = new Video();
        videoModel.setLink(url);
        videoModel.setTitle(model.getTitle());
        videoModel.setImagePath(model.getImage());
        videoModel.setOriginalPath(model.getMediaUrl());
        videoModel.setId(model.getID() + "");
        videoModel.setTotalLike(model.getLike());
        videoModel.setTotalComment(model.getComment());
//        videoModel.setTotalShare(model.gets());
        videoModel.setIsPrivate(0);
        return videoModel;
    }

    public static Video convertTiinToVideoMocha(TiinModel model) {
        String url = model.getUrl();
        if (!url.contains(ConstantTiin.DOMAIN)) {
            url = ConstantTiin.DOMAIN + url;
        }
        Video videoModel = new Video();
        videoModel.setLink(url);
        videoModel.setTitle(model.getTitle());
        videoModel.setImagePath(model.getImage());
        videoModel.setOriginalPath(model.getMediaUrl());
        videoModel.setId(model.getId() + "");
        videoModel.setTotalLike(model.getLike());
        videoModel.setTotalComment(model.getComment());
//        videoModel.setTotalShare(model.gets());
        videoModel.setIsPrivate(0);
        return videoModel;
    }

    public static Video convertKeengToVideoMocha(AllModel model) {
        Video videoModel = new Video();
        videoModel.setLink(model.getUrl());
        videoModel.setTitle(model.getName());
        videoModel.setImagePath(model.getImage());
        videoModel.setOriginalPath(model.getMediaUrl());
        videoModel.setId(model.getId() + "");
        videoModel.setTotalLike(model.getTotal_like());
        videoModel.setTotalComment(model.getTotalComment());
        videoModel.setTotalShare(model.getTotal_share());
        videoModel.setIsPrivate(0);

//        Channel channel = new Channel();
//        channel.setId("2");
//        channel.setUrlImage("http://hlvip2.mocha.com.vn/hlmocha92/media3/avatar/offical/Keeng4FC4N.jpg");
//        channel.setName("Keeng");
//        channel.setTypeChannel(Constants.TYPE_VIDEO);
//        channel.setPackageAndroid("com.vttm.keeng");
//        videoModel.setChannel(channel);

        return videoModel;
    }

    public static Movie convertToMovies(SearchModel item) {
        Movie model = new Movie();
        if (item != null) {
            model.setId(item.getId());
            model.setName(item.getName());
            model.setImagePath(item.getImage());
            model.setPosterPath(item.getPosterPath());
            model.setTypeFilm(item.getTypeFilm());
            model.setType(item.getType());
            model.setDescription(item.getDescription());
//            model.setLink(item.getLink());
//            model.setSource(MediaLogModel.SRC_SEARCH);
        }
        return model;
    }

    public static PlayListModel convertToPlaylist(SearchModel item) {
        PlayListModel model = new PlayListModel();
        if (item != null) {
            model.setId(item.getIdInt());
            model.setName(item.getFullName());
            model.setCover(item.getImage());
            model.setType(item.getType());
            model.setUser(new UserInfo(item.getFullSinger()));
            model.setListenNo(item.getListened());
            String imageTmp = item.getImage();
            if (!TextUtils.isEmpty(imageTmp)) {
                String[] listImages = imageTmp.split(";");
                if (listImages.length > 0)
                    model.setListAvatar(Arrays.asList(listImages));
            }
            model.setSource(MediaLogModel.SRC_SEARCH);
        }
        return model;
    }

    public static Topic convertToSinger(SearchModel item, String domainImage) {
        Topic model = new Topic();
        if (item != null) {
            model.setName(item.getFullName());
            model.setSingerId(item.getIdInt());
            model.setImage(domainImage + item.image);
            model.setType(item.getType());
            model.setSource(MediaLogModel.SRC_SEARCH);
        }
        return model;
    }

    public static AllModel convertToMedia(SearchModel item, String domain) {
        AllModel model = new AllModel();
        if (item != null) {
            model.setId(item.getIdInt());
            model.setName(item.getFullName());
            model.setSinger(item.getFullSinger());
            model.setType(item.getType());
            model.setImage(domain + item.getImage());
            model.setListened(item.getListened());
            model.setSource(MediaLogModel.SRC_SEARCH);
        }
        return model;
    }
}

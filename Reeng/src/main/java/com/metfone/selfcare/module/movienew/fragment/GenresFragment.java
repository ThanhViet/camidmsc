package com.metfone.selfcare.module.movienew.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.FavoriteBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.metfoneplus.topup.interfacelistener.RunUi;
import com.metfone.selfcare.module.movienew.adapter.GenresAdapter;
import com.metfone.selfcare.module.movienew.listener.OnClickGenres;
import com.metfone.selfcare.module.movienew.model.Category;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class GenresFragment extends BaseFragment implements OnClickGenres, OnInternetChangedListener {

    private Unbinder unbinder;

    @BindView(R.id.recycler_genres)
    RecyclerView genresList;

    @BindView(R.id.btn_next)
    AppCompatButton btnNext;
    @BindView(R.id.icBack)
    AppCompatImageView btnBack;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.ll_genres_container)
    LinearLayout llGenresContainer;
    private ArrayList<Object> genresModels;
    private GenresAdapter genresAdapter;
    private MovieApi movieApi;
    private ListenerUtils mListenerUtils;
    private static RunUi mRunUi;
    private FavoriteBusiness favoriteBusiness;

    private Handler mHandler;
    private Runnable mRunnable;

    public MovieApi getMovieApi() {
        if (movieApi == null) movieApi = new MovieApi();
        return movieApi;
    }

    public GenresFragment() {
        // Required empty public constructor
    }

    public static GenresFragment newInstance(RunUi runUi) {
        mRunUi = runUi;
        GenresFragment fragment = new GenresFragment();
        return fragment;
    }

    public static GenresFragment newInstance() {
        Bundle args = new Bundle();
        GenresFragment fragment = new GenresFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        favoriteBusiness = new FavoriteBusiness(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_genres, container, false);
        unbinder = ButterKnife.bind(this, view);

        //Set padding for linear layout that contains recycler view to recycler view above gradient layer
        if (getActivity() != null) {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int deviceHeightInPx = displaymetrics.heightPixels;
            int offsetPaddingInDp = 76;
            //3.5 is weight_sum of linear layout
            int paddingInPx = (int) (deviceHeightInPx / 3.5) + Utilities.convertDpToPixel(offsetPaddingInDp);
            llGenresContainer.setPadding(0, 0, 0, paddingInPx);
        }

        if (genresModels == null) genresModels = new ArrayList<>();
        else genresModels.clear();
        genresAdapter = new GenresAdapter(activity, genresModels);
        genresAdapter.setListener(this);

        CustomGridLayoutManager layoutManager = new CustomGridLayoutManager(activity, 2);
        BaseAdapter.setupGridRecycler(activity, genresList, layoutManager, genresAdapter, 2, R.dimen.cinema_card_spacing, true);
        listener();
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = application.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        if (canLazyLoad()) {
            mHandler = new Handler();
            mRunnable = this::loadData;
            mHandler.postDelayed(mRunnable, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData();
        }
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad()) {
            loadData();
        }
    }

    private void loadData() {
        if(pbLoading != null){
            pbLoading.setVisibility(View.VISIBLE);
        }
        getMovieApi().getCategoryV2(new ApiCallbackV2<ArrayList<Category>>() {
            @Override
            public void onSuccess(String msg, ArrayList<Category> result) {
                if (genresModels == null) genresModels = new ArrayList<>();
                if (Utilities.notEmpty(result)) {
                    genresModels.addAll(result);
                }
                if (genresAdapter != null) genresAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String s) {
                if (genresModels == null) genresModels = new ArrayList<>();
                if (genresAdapter != null) genresAdapter.notifyDataSetChanged();
            }

            @Override
            public void onComplete() {
                if(pbLoading != null){
                    pbLoading.setVisibility(View.GONE);
                }
                isDataInitiated = true;
            }
        });
    }

    @Override
    public void onDestroyView() {
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onClick(Object item, int position, boolean isChecked) {
        if (item instanceof Category) {
            ((Category) genresModels.get(position)).setChecked(isChecked);
            int checkedCount = 0;
            for (Object categoryObj : genresModels) {
                Category category = (Category) categoryObj;
                if (category.isChecked()) {
                    checkedCount++;
                }
            }
            if (checkedCount >= 3) {
                btnNext.setEnabled(true);
            } else {
                btnNext.setEnabled(false);
            }
            genresAdapter.notifyItemChanged(position);
        }
    }

    private void listener() {
        btnBack.setOnClickListener(v -> {
            popBackStackFragment();
        });
    }

    @OnClick(R.id.btn_next)
    public void next(View view) {
        favoriteBusiness.addedCategory();
        favoriteBusiness.setCategory(getFavorite());
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameAction,
            CountryFragment.newInstance(mRunUi)).commitAllowingStateLoss();
    }

    public ArrayList<String> getFavorite() {
        ArrayList<String> result = new ArrayList<>();
        if (genresModels != null) {
            for (Object genresModel : genresModels) {
                Category category = (Category) genresModel;
                if (category.isChecked()) {
                    result.add(category.getCategoryId());
                }
            }
        }
        return result;
    }
}
package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;

public class WsGetListLogWatchedResponse {
    public String errorCode;
    public String errorMessage;
    @SerializedName("result")
    public ListLogWatchResponse response;
}

package com.metfone.selfcare.module.netnews.EditCategoryNews.adapter;

import android.content.Context;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.view.BaseItemDraggableAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.widget.SwitchButton;

import java.util.List;

/**
 * Created by HaiKE on 9/13/17.
 */

public class EditCategoryNewsAdapter extends BaseItemDraggableAdapter<CategoryModel, BaseViewHolder> {

    private Context context;
    AbsInterface.OnItemSettingCategoryListener listener;

    public EditCategoryNewsAdapter(Context context, List<CategoryModel> data, AbsInterface.OnItemSettingCategoryListener listener) {
        super(R.layout.item_setting_category, data);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void convert(final BaseViewHolder holder, final CategoryModel item) {
        Log.e(TAG, "convert: " + item);
        if (holder.getView(R.id.tvTitle) != null) {
            holder.setText(R.id.tvTitle, item.getName());
        }
        if (holder.getView(R.id.btnFollow) != null) {
            SwitchButton sbFollow = holder.getView(R.id.btnFollow);
            sbFollow.setChecked(item.isFollow());
            sbFollow.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                    item.setFollow(isChecked);
                    listener.onItemClickFavourite(item, holder.getAdapterPosition());
                }
            });
        }

    }
}

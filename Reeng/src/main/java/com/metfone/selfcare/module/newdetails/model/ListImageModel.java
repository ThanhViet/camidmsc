package com.metfone.selfcare.module.newdetails.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HaiKE on 12/6/17.
 */

public class ListImageModel implements Serializable {

    private ArrayList<String> listImage = new ArrayList<>();

    public ListImageModel(ArrayList<String> listImage) {
        this.listImage = listImage;
    }

    public ArrayList<String> getListImage() {
        return listImage == null ? new ArrayList<String>() : listImage;
    }

    public void setListImage(ArrayList<String> listImage) {
        this.listImage = listImage;
    }
}

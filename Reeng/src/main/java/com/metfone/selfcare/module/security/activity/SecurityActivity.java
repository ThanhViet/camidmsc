/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/25
 */

package com.metfone.selfcare.module.security.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.security.fragment.NewsDetailFragment;
import com.metfone.selfcare.module.security.fragment.SecurityCenterFragment;
import com.metfone.selfcare.module.security.fragment.SecurityDetailFragment;
import com.metfone.selfcare.module.security.fragment.SecurityPagerFragment;
import com.metfone.selfcare.module.security.fragment.VulnerabilityFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecurityActivity extends BaseSlidingFragmentActivity {
    protected final String TAG = getClass().getSimpleName();
    protected Fragment mFragment;
    protected int currentTabId;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.line)
    View line;

    @OnClick(R.id.button_back)
    public void onBackClick() {
        finish();
    }

    public void setTitle(int resTitle) {
        if (tvTitle != null) tvTitle.setText(resTitle);
    }

    public void setTitle(String title) {
        if (tvTitle != null) tvTitle.setText(title);
    }

    public void showLine(boolean isShow) {
        if (line != null) line.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        int type = intent.getIntExtra(Constants.KEY_TYPE, 0);
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) bundle = new Bundle();
        bundle.putInt(Constants.KEY_TYPE, type);
        goNextTab(type, bundle);
    }

    public void goNextTab(int tabId, Bundle args) {
        switch (tabId) {
            case Constants.TAB_SECURITY_CENTER:
                mFragment = SecurityCenterFragment.newInstance();
                break;
            case Constants.TAB_SECURITY_SPAM:
                mFragment = SecurityPagerFragment.newInstance();
                break;
            case Constants.TAB_SECURITY_FIREWALL:
                mFragment = SecurityPagerFragment.newInstance();
                break;
            case Constants.TAB_SECURITY_VULNERABILITY:
                mFragment = VulnerabilityFragment.newInstance();
                break;
            case Constants.TAB_SECURITY_NEWS:
                mFragment = NewsDetailFragment.newInstance();
                break;

            case Constants.TAB_SECURITY_SPAM_SMS_DETAIL:
            case Constants.TAB_SECURITY_FIREWALL_SMS_DETAIL:
                mFragment = SecurityDetailFragment.newInstance();
                break;
            default:
                mFragment = null;
                break;
        }
        if (mFragment != null) {
            currentTabId = tabId;
            try {
                if (!mFragment.isAdded())
                    if (args != null) {
                        if (mFragment.getArguments() == null) {
                            mFragment.setArguments(args);
                        } else {
                            mFragment.getArguments().putAll(args);
                        }
                    }
            } catch (IllegalStateException e) {
                Log.e(TAG, e);
            } catch (RuntimeException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
            replaceFragment(currentTabId, mFragment);
        }
    }

    public void replaceFragment(final int tabId, final Fragment fragment) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                transactionFragment(tabId, fragment);
            }
        });
    }

    public void transactionFragment(final int tabId, final Fragment fragment) {
        try {
            getSupportFragmentManager()
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.activity_right_to_left_enter, R.anim.activity_right_to_left_exit)
                    .add(R.id.tabContent, fragment, String.valueOf(tabId))
                    .commitAllowingStateLoss();
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

}

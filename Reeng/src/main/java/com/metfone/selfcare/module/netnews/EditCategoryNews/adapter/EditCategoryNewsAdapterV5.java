package com.metfone.selfcare.module.netnews.EditCategoryNews.adapter;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 1/27/15.
 */
public class EditCategoryNewsAdapterV5 extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private ArrayList<CategoryModel> mItems = new ArrayList<>();
    private Context mContext;
    private AbsInterface.OnItemSettingCategoryListener listener;

    public EditCategoryNewsAdapterV5(Context context,  AbsInterface.OnItemSettingCategoryListener listener) {
        mContext = context;
        this.listener = listener;
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // set list item
    public void setListItem(ArrayList<CategoryModel> list) {
        mItems = list;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CategoryModel model = (CategoryModel) getItem(position);
        View view = layoutInflater.inflate(R.layout.item_setting_news,parent,false);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        AppCompatImageView sbFavorite = view.findViewById(R.id.btnFollow);
        tvTitle.setText(model.getName());
        if(model.isFollow()){
            sbFavorite.setImageResource(R.drawable.ic_v5_heart_violet);
        }else{
            sbFavorite.setImageResource(R.drawable.ic_v5_heart_normal);
        }
        sbFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null){
                    model.setFollow(!model.isFollow());
                    if(model.isFollow()){
                        sbFavorite.setImageResource(R.drawable.ic_v5_heart_violet);
                    }else{
                        sbFavorite.setImageResource(R.drawable.ic_v5_heart_normal);
                    }
                    listener.onItemClickFavourite(model,position);
                }
            }
        });
//        sbFavorite.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
//                if(listener != null){
//                    model.setFollow(isChecked);
//                    listener.onItemClickFavourite(model,position);
//                }
//            }
//        });
        return view;
    }
}

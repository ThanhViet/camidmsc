package com.metfone.selfcare.module.search.utils;

import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Utilities;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class ImageBusiness extends com.metfone.selfcare.module.keeng.utils.ImageBusiness {
    //todo cái này chỉ nên sử dụng trong module search thôi

//    public static void setPosterMovie(String url, ImageView imageView) {
//        if (imageView == null || url == null) return;
//        try {
//            Glide.with(ApplicationController.self())
//                    .asBitmap()
//                    .load(url)
//                    .transition(withCrossFade(500))
//                    .apply(new RequestOptions()
//                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                            .transforms(new FitCenter()
//                                    , ApplicationController.self().getRoundedCornersTransformation())
//                            .placeholder(R.drawable.df_poster_movie)
//                            .error(R.drawable.df_poster_movie)
//                    )
//                    .into(imageView);
//        } catch (OutOfMemoryError e) {
//            Log.e(TAG, e);
//        } catch (IllegalArgumentException e) {
//            Log.e(TAG, e);
//        } catch (Exception e) {
//            Log.e(TAG, e);
//        }
//    }

    public static void setVideo(String url, ImageView imageView) {
        setImageTransform(imageView, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9);
    }

    public static void setVideo(String url, ImageView imageView, int round) {
        setImage(imageView, url, R.drawable.df_image_home_16_9, R.drawable.df_image_home_16_9,
                new RoundedCornersTransformation(Utilities.dpToPx(round), 0));
    }

//    public static void setSong(String url, ImageView imageView) {
//        setImageTransform(imageView, url, R.drawable.df_image_home, R.drawable.df_image_home);
//    }

    public static void setSong(String url, ImageView imageView, int round) {
        setImage(imageView, url, R.drawable.df_image_home, R.drawable.df_image_home,
                new RoundedCornersTransformation(Utilities.dpToPx(round), 0));
    }
}

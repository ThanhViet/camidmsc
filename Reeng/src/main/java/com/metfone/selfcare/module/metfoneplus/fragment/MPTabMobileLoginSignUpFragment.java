package com.metfone.selfcare.module.metfoneplus.fragment;

import static com.blankj.utilcode.util.ThreadUtils.runOnUiThread;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;
import static com.metfone.selfcare.helper.google.GoogleSignInHelper.RC_SIGN_IN;
import static com.metfone.selfcare.util.EnumUtils.LOG_MODE;
import static com.metfone.selfcare.util.EnumUtils.PRE_LOG_MODE;
import static com.metfone.selfcare.util.EnumUtils.SINGIN;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.blankj.utilcode.util.DeviceUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.CreateYourAccountActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.google.GoogleSignInHelper;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.SignIn;
import com.metfone.selfcare.model.account.SignInRequest;
import com.metfone.selfcare.model.account.SignInResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.MethodLoginResponse;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.listener.OnClickShowFTTH;
import com.metfone.selfcare.module.movie.event.UpdateWatchedEvent;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogLoading;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.Connection;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MPTabMobileLoginSignUpFragment extends MPBaseFragment implements View.OnClickListener,
        FacebookHelper.OnFacebookListener, GoogleSignInHelper.OnGoogleListener, GoogleSignInHelper.OnGoogleSignOutListener {
    private static final String FULL_NAME = "fullname";
    private static final String GENDER = "gender";
    private static final String BIRTHDAY = "birthday";
    private static final String EMAIL = "email";
    private static final String METHOD = "method";
    private static final int FACEBOOK_METHOD = 1;
    private static final int GOOGLE_METHOD = 2;
    private static final String FACEBOOK_METHOD_CODE = "facebook";
    private static final String GOOGLE_METHOD_CODE = "google";

    private static final String TAG = MPTabMobileLoginSignUpFragment.class.getSimpleName();
    CamIdTextView tvEnterPhone;
    CamIdTextView tvRegisterFacebook;
    CamIdTextView tvRegisterGoogle;
    CamIdTextView tvToRegisterAppleId;
    FrameLayout layoutFacebook;
    FrameLayout layoutGoogle;
    TextView tvOr;
    GoogleSignInHelper googleSignInHelper;
    ProgressBar pbLoading;
    SharedPref sharedPref;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    String userName, email, birthday;
    int gender;
    UserInfoBusiness userInfoBusiness;
    private ImageView ivBack;
    private CamIdTextView tvChangeScreenLogin, tvChangeScreenSighUp;
    private CamIdTextView tvTitleCamId, tvTitleFooterCamId;
    private FrameLayout llInputNumber;
    private ApplicationController mApplication;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private boolean isLoginDone = false;
    private KhHomeClient homeClient;
    private FragmentManager mFragmentManager;
    private RadioButton btnSwitchMobile;
    private RadioButton btnSwitchInternet;
    private HomeActivity mParentActivity;

    public static MPTabMobileLoginSignUpFragment instance;
    public MPTabMobileLoginSignUpFragment() {
    }

    public static MPTabMobileLoginSignUpFragment get() { return instance;}

    public static MPTabMobileLoginSignUpFragment newInstance() {
        MPTabMobileLoginSignUpFragment fragment = new MPTabMobileLoginSignUpFragment();
        instance = fragment;
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_register_screen_metfone;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
            initView(view);
    }

    private OnClickShowFTTH onClickShowFTTH;

    public void setListener(OnClickShowFTTH showFTTHListener) {
        onClickShowFTTH = showFTTHListener;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void initView(View view) {
        llInputNumber = view.findViewById(R.id.llInputNumber);
        pbLoading = view.findViewById(R.id.pbLoading);
        tvRegisterFacebook = view.findViewById(R.id.tvToRegisterFacebook);
        tvChangeScreenLogin = view.findViewById(R.id.tvChangeScreenLogin);
        tvChangeScreenSighUp = view.findViewById(R.id.tvChangeScreenSighUp);
        tvTitleCamId = view.findViewById(R.id.tvTitleCamId);
        tvTitleFooterCamId = view.findViewById(R.id.tvTitleFooterCamId);
        layoutFacebook = view.findViewById(R.id.layoutFacebook);
        layoutGoogle = view.findViewById(R.id.layoutGoogle);
        tvOr = view.findViewById(R.id.tv_Or);
        tvTitleCamId.setText(getResources().getString(R.string.title_camid_log_in));
        tvChangeScreenLogin.setText(getResources().getString(R.string.sign_up));
        tvTitleFooterCamId.setText(getResources().getString(R.string.new_to_cam_id));
        llInputNumber.setOnClickListener(this);
        tvRegisterFacebook.setOnClickListener(this);
        tvChangeScreenLogin.setOnClickListener(this);
        tvChangeScreenSighUp.setOnClickListener(this);
        tvTitleCamId.setOnClickListener(this);
        tvRegisterGoogle = view.findViewById(R.id.tvToRegisterGoogle);
        googleSignInHelper = new GoogleSignInHelper(mParentActivity, this);
        sharedPref = SharedPref.newInstance(getContext());
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());
        tvRegisterGoogle.setOnClickListener(this);
        userInfoBusiness = new UserInfoBusiness(getContext());
        homeClient = new KhHomeClient();
        if (SharedPrefs.getInstance().get(PREF_IS_LOGIN, Boolean.class)) {
            tvChangeScreenLogin.setVisibility(View.VISIBLE);
            tvChangeScreenSighUp.setVisibility(View.GONE);
            tvTitleCamId.setText(getResources().getString(R.string.title_camid_sign_up));
            tvTitleFooterCamId.setText(getResources().getString(R.string.already_have_an_account));
            tvChangeScreenLogin.setText(getResources().getString(R.string.log));
            homeClient.logApp(Constants.LOG_APP.SIGN_UP);
        } else {
            tvChangeScreenLogin.setVisibility(View.GONE);
            tvChangeScreenSighUp.setVisibility(View.VISIBLE);
            tvTitleCamId.setText(getResources().getString(R.string.title_camid_log_in));
            tvTitleFooterCamId.setText(getResources().getString(R.string.new_to_cam_id));
            tvChangeScreenSighUp.setText(getResources().getString(R.string.sign_up));
            homeClient.logApp(Constants.LOG_APP.LOG_IN);
        }

        btnSwitchMobile = view.findViewById(R.id.btnSwitchMobile);
        btnSwitchInternet = view.findViewById(R.id.btnSwitchInternet);
        btnSwitchInternet.setOnClickListener(this::handleSwitchInternet);
        getMethodsLogin();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static final void setSystemBarTheme(final Activity pActivity, final boolean pIsDark) {
        // Fetch the current flags.
        final int lFlags = pActivity.getWindow().getDecorView().getSystemUiVisibility();
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        pActivity.getWindow().getDecorView().setSystemUiVisibility(pIsDark ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
                : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
    }

    public void setColorStatusBar(@ColorRes int colorRes) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), colorRes));
        } else {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), colorRes));
            }
        }
    }
    private void handleSwitchInternet(View view) {
        if (onClickShowFTTH != null) onClickShowFTTH.showFTTH();
        btnSwitchMobile.setChecked(true);
        btnSwitchInternet.setChecked(false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        this.mParentActivity = (HomeActivity) context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(final UpdateWatchedEvent event) {
        Log.i(TAG, "onEvent UpdateWatchedEvent: " + event);
        EventHelper.removeStickyEvent(event);
    }

    public void showError(String errorMessage, String title) {
        if (!getActivity().isFinishing())
            new DialogConfirm(getActivity(), true).setLabel(title).setMessage(errorMessage).
                    setNegativeLabel(getResources().getString(R.string.close)).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llInputNumber:
                userInfoBusiness.putLogInMode(EnumUtils.LoginModeTypeDef.PHONE_NUMBER);
                if (tvChangeScreenLogin.getVisibility() == View.GONE) {
                    LOG_MODE = EnumUtils.LogModeTypeDef.LOGIN;
                } else {
                    LOG_MODE = EnumUtils.LogModeTypeDef.SIGN_UP;
                }
                PRE_LOG_MODE = LOG_MODE;
                NavigateActivityHelper.navigateToLoginActivity(mParentActivity, true);
                break;
            case R.id.tvToRegisterFacebook:
                userInfoBusiness.putLogInMode(EnumUtils.LoginModeTypeDef.FACEBOOK);
                if (!NetworkHelper.isConnectInternet(getContext())) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    FacebookHelper facebookHelper = new FacebookHelper(mParentActivity);
                    facebookHelper.getProfile(mParentActivity.getCallbackManager(), this, FacebookHelper.PendingAction.GET_USER_INFO);
                }

                break;
            case R.id.tvChangeScreenLogin:
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, false);
                tvChangeScreenLogin.setVisibility(View.GONE);
                tvChangeScreenSighUp.setVisibility(View.VISIBLE);
                tvTitleCamId.setText(getResources().getString(R.string.title_camid_log_in));
                tvTitleFooterCamId.setText(getResources().getString(R.string.new_to_cam_id));
                tvChangeScreenSighUp.setText(getResources().getString(R.string.sign_up));
                homeClient.logApp(Constants.LOG_APP.LOG_IN);
                break;
            case R.id.tvChangeScreenSighUp:
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
                tvChangeScreenLogin.setVisibility(View.VISIBLE);
                tvChangeScreenSighUp.setVisibility(View.GONE);
                tvTitleCamId.setText(getResources().getString(R.string.title_camid_sign_up));
                tvTitleFooterCamId.setText(getResources().getString(R.string.already_have_an_account));
                tvChangeScreenLogin.setText(getResources().getString(R.string.log));
                homeClient.logApp(Constants.LOG_APP.SIGN_UP);
                break;
            case R.id.tvToRegisterGoogle:
                userInfoBusiness.putLogInMode(EnumUtils.LoginModeTypeDef.GOOGLE);
                if (!NetworkHelper.isConnectInternet(getContext())) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    if (googleSignInHelper.isSignedIn()) {
                        googleSignInHelper.signOut(this);
                        return;
                    }
                    signIn();
                }
                break;
        }
    }

    @Override
    public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
        Log.d(TAG, "name is " + name);
//        mApplication.getReengAccountBusiness().getCurrentAccount().setFacebookId(userId);
        this.userName = name;
        this.email = email;
        this.birthday = birthDayStr;
        this.gender = gender;
        signIn(FACEBOOK_METHOD);
    }

    private void signIn() {
        googleSignInHelper.getProfile();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            googleSignInHelper.handleSignInIntent(data);
        }
    }

    @Override
    public void onGetInfoGoogleFinish(String id, String userName, String email, Uri avatarUri) {
        this.userName = userName;
        this.email = email;
        signIn(GOOGLE_METHOD);
    }

    private void signIn(int currentMethod) {
        pbLoading.setVisibility(View.VISIBLE);
        SignIn signIn = new SignIn();
        switch (currentMethod) {
            case FACEBOOK_METHOD:
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                signIn.setSocialToken(accessToken.getToken());
                Log.d(TAG, "Facebook Access Token is " + accessToken.getToken());
                signIn.setType("facebook");
                break;
            case GOOGLE_METHOD:
                GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(getContext());
                signIn.setSocialToken(googleSignInAccount.getIdToken());
                Log.d(TAG, "Google token id is " + googleSignInAccount.getIdToken());
                signIn.setType("google");
                break;
        }

        SignInRequest signInRequest = new SignInRequest(signIn, "", "", "", "");
        apiService.signIn(DeviceUtils.getUniqueDeviceId(),signInRequest).enqueue(new Callback<SignInResponse>() {

            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                pbLoading.setVisibility(View.GONE);
                SignInResponse signInResponse = response.body();
                if (signInResponse != null) {
                    if (signInResponse.getCode().equals("00")) {
                        String token = "Bearer " + signInResponse.getData().getAccess_token();
                        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, response.body().getData().getRefresh_token());
                        if (signInResponse.getData() != null && signInResponse.getData().getAuth() != null
                                && SINGIN.equals(signInResponse.getData().getAuth().toUpperCase())) {
                            //case sign in social
                            getUserInformation(currentMethod);
                        } else {
                            //case sign up social
                            if (currentMethod == FACEBOOK_METHOD) {
                                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                                Log.d(TAG, "Facebook Access Token is " + accessToken.getToken());
                                Intent intent = new Intent(getContext(), CreateYourAccountActivity.class);
                                intent.putExtra(FULL_NAME, userName);
                                intent.putExtra(EMAIL, email);
                                intent.putExtra(GENDER, gender);
                                intent.putExtra(BIRTHDAY, birthday);
                                intent.putExtra(METHOD, FACEBOOK_METHOD);
                                startActivity(intent);
                            } else if (currentMethod == GOOGLE_METHOD) {
                                Intent intent = new Intent(getContext(), CreateYourAccountActivity.class);
                                intent.putExtra(FULL_NAME, userName);
                                intent.putExtra(EMAIL, email);
                                intent.putExtra(METHOD, GOOGLE_METHOD);
                                startActivity(intent);
                            }
                        }
                    } else {
                        if (currentMethod == FACEBOOK_METHOD) {
                            ToastUtils.showToast(getContext(), "Sign Up Facebook Failed ");
                        } else if (currentMethod == GOOGLE_METHOD) {
                            ToastUtils.showToast(getContext(), "Sign Up Google Failed ");
                        } else {
                            ToastUtils.showToast(getContext(), "Sign Up Failed ");
                        }
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        ToastUtils.showToast(getContext(), "Sign In Failed");
                    } catch (Exception e) {
                        ToastUtils.showToast(getContext(), e.getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                ToastUtils.showToast(getContext(), getString(R.string.service_error));
            }
        });
    }

    private void getUserInformation(int currentMethod) {
        pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                pbLoading.setVisibility(View.GONE);
                if (response.code() == ERROR_UNAUTHORIZED) {
                    mParentActivity.restartApp();
                    return;
                }
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        FireBaseHelper.getInstance(ApplicationController.self()).checkServiceAndRegister(true);
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                        if (reengAccount != null) {
                            reengAccount.setName(userInfo.getFull_name());
                            reengAccount.setEmail(userInfo.getEmail());
                            reengAccount.setBirthday(userInfo.getDate_of_birth());
                            reengAccount.setAddress(userInfo.getAddress());
                            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                        }
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
                        mCurrentRegionCode = "KH";
                        //case have an account
                        if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
                            if (userInfo.getPhone_number().startsWith("0")) {
                                mCurrentNumberJid = "+855" + userInfo.getPhone_number().substring(1);
                            } else {
                                mCurrentNumberJid = "+855" + userInfo.getPhone_number();
                            }
                            String originToken = token.substring(7);
                            generateOldMochaOTP(originToken);
                        } else {
                            RestoreManager.setRestoring(false);
                            NavigateActivityHelper.navigateToHomeScreenActivity(mParentActivity, true, false);
                        }
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getMessage());
                    }

                } else {
                    ToastUtils.showToast(getContext(), "Get user information fail");
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), getString(R.string.service_error));
            }
        });
    }

    private void generateOldMochaOTP(String token) {
        pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(getContext()).detectSubscription(json);
                        doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(getContext(), "generateOldMochaOTP Failed");
                }
            }

            @Override
            public void onError(Throwable error) {
                pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), error.getMessage());
            }
        });

    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
        mApplication.getXmppManager().manualDisconnect();
        new LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    @Override
    public void onSignOutGoogleFinish() {
        googleSignInHelper = new GoogleSignInHelper(mParentActivity, this);
        signIn();
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) getActivity().getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);
            pbLoading.setVisibility(View.GONE);

            try {
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
                    UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                    UserInfo userInfo = userInfoBusiness.getUser();
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    reengAccount.setName(userInfo.getFull_name());
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    RestoreManager.setRestoring(true);
                    userInfoBusiness.autoBackUp(mApplication, new DBImporter.RestoreProgressListener() {
                        @Override
                        public void onStartDownload() {

                        }

                        @Override
                        public void onDownloadProgress(int percent) {

                        }

                        @Override
                        public void onDownloadComplete() {

                        }

                        @Override
                        public void onDowloadFail(String message) {

                        }

                        @Override
                        public void onStartRestore() {

                        }

                        @Override
                        public void onRestoreProgress(int percent) {

                        }

                        @Override
                        public void onRestoreComplete(int messageCount, int threadMessageCount) {
                            try {
                                SharedPreferences pref = getContext().getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                                if (pref != null) {
                                    pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
                                    com.metfone.selfcare.util.Log.i(TAG, "setPassedRestorePhase");
                                }
                            } catch (Exception e) {
                            }
                            RestoreManager.setRestoring(false);
                            mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
                            mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
                            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
                            NavigateActivityHelper.navigateToHomeScreenActivity(mParentActivity, true, false);
                        }

                        @Override
                        public void onRestoreFail(String message) {
                            RestoreManager.setRestoring(false);
                            NavigateActivityHelper.navigateToHomeScreenActivity(mParentActivity, true, false);
                        }
                    }, mParentActivity);
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_complete));
                } else {
                    ToastUtils.showToast(getContext(), responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_fail));
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
            }
        }
    }

    private void getMethodsLogin() {
        showLoadingDialog("", "");
        apiService.getMethodsLogin().enqueue(new Callback<MethodLoginResponse>() {
            @Override
            public void onResponse(Call<MethodLoginResponse> call, Response<MethodLoginResponse> response) {
                if (response != null && response.body() != null && response.body().getData() != null) {
                    List<MethodLoginResponse.MethodLogin> methods = response.body().getData().getMethodsLogin();
                    if (methods != null && methods.size() > 0) {
                        for (MethodLoginResponse.MethodLogin methodLogin : methods) {
                            if (methodLogin.getCode() != null && methodLogin.getCode().equals(FACEBOOK_METHOD_CODE)) {
                                if (methodLogin.getState().equals(MethodLoginResponse.MethodLogin.ACTIVE)) {
                                    layoutFacebook.setVisibility(View.VISIBLE);
                                    tvOr.setVisibility(View.VISIBLE);
                                } else {
                                    layoutFacebook.setVisibility(View.GONE);
                                }
                            } else if (methodLogin.getCode() != null && methodLogin.getCode().equals(GOOGLE_METHOD_CODE)) {
                                if (methodLogin.getState().equals(MethodLoginResponse.MethodLogin.ACTIVE)) {
                                    layoutGoogle.setVisibility(View.VISIBLE);
                                    tvOr.setVisibility(View.VISIBLE);
                                } else {
                                    layoutGoogle.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
                hideLoadingDialog();
            }

            @Override
            public void onFailure(Call<MethodLoginResponse> call, Throwable t) {
                hideLoadingDialog();
            }
        });
    }

    public void showLoadingDialog(final String title, final String message) {
        showLoadingDialog(title, message, false);
    }

    protected DialogLoading mLoadingDialog;
    public void showLoadingDialog(final String title, final String message, final boolean cancelable) {
        if (getActivity().isFinishing()) return;
        runOnUiThread(() -> {
            if (mLoadingDialog == null) {
                mLoadingDialog = new DialogLoading(mParentActivity, cancelable);
            }
            mLoadingDialog.show();
        });
    }
    public void hideLoadingDialog() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }
}
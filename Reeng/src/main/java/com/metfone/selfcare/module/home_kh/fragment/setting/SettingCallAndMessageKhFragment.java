package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.fragment.setting.BaseSettingFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.module.home_kh.base.BaseDialogKhFragment;
import com.metfone.selfcare.module.home_kh.blur.RealtimeBlurView;
import com.metfone.selfcare.module.home_kh.dialog.KhDialogConfirm;
import com.metfone.selfcare.module.home_kh.dialog.KhDialogRadioSelect;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.dialog.DialogRadioSelect;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;
import com.metfone.selfcare.v5.widget.SwitchButton;

public class SettingCallAndMessageKhFragment extends BaseSettingKhFragment implements View.OnClickListener {
    private static final String TAG = SettingCallAndMessageKhFragment.class.getSimpleName();
    private ReengAccountBusiness mAccountBusiness;
    private SettingBusiness mSettingBusiness;
    //    private View mViewDividerReplySms, mViewDividerSms;
    private ConstraintLayout mViewReplySms, mViewAutoSms, mViewReceiveSmsOut, mViewKeyboardSend, mViewFontSize, mViewDeleteData;
    private SwitchButton mToggleReplySms, mToggleAutoSms, mToggleReceiveSmsOut, mToggleKeyboardSend;
    private AppCompatTextView mTvwFontSizeLabel, tvFontSizeInfor;

    RealtimeBlurView blurView;

    public static SettingCallAndMessageKhFragment newInstance() {
        return new SettingCallAndMessageKhFragment();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        /*try {
            mListener = (OnFragmentSettingListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }*/
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_call_message_setting_kh;
    }


    @Override
    protected void initView(View view) {
        findComponentViews(view);
        setTitle(R.string.kh_setting_call_and_message);
        setViewListener();
    }

    private void findComponentViews(View rootView) {
//        mViewDividerReplySms = rootView.findViewById(R.id.setting_reply_sms_divider);
//        mViewDividerSms = rootView.findViewById(R.id.setting_sms_divider);
        mViewDeleteData = rootView.findViewById(R.id.setting_delete_data);
        mViewReplySms = rootView.findViewById(R.id.setting_reply_sms);
        mViewAutoSms = rootView.findViewById(R.id.setting_auto_smsout);
        mViewReceiveSmsOut = rootView.findViewById(R.id.setting_receive_smsout);
        mViewKeyboardSend = rootView.findViewById(R.id.setting_keyboard_send);
        mViewFontSize = rootView.findViewById(R.id.setting_font_size);
        mToggleReplySms = rootView.findViewById(R.id.setting_reply_sms_toggle);
        mToggleAutoSms = rootView.findViewById(R.id.setting_auto_smsout_toggle);
        mToggleReceiveSmsOut = rootView.findViewById(R.id.setting_receive_smsout_toggle);
        mToggleKeyboardSend = rootView.findViewById(R.id.setting_keyboard_send_toggle);
        mTvwFontSizeLabel = rootView.findViewById(R.id.setting_font_size_label);
        tvFontSizeInfor = rootView.findViewById(R.id.tvFontSizeInfor);
        blurView = rootView.findViewById(R.id.blur_view);
        blurView.setVisibility(View.GONE);
        drawDetail();
    }

//    private void initActionbar(LayoutInflater inflater) {
//        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
//        View mViewActionBar = mParentActivity.getToolBarView();
//        mTvwTitle = (EllipsisTextView) mViewActionBar.findViewById(R.id.ab_title);
//        mImgBack = (ImageView) mViewActionBar.findViewById(R.id.ab_back_btn);
//        mTvwTitle.setText(mRes.getString(R.string.setting_call_and_message));
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_reply_sms:
                mToggleReplySms.setChecked(!mToggleReplySms.isChecked());
                processSettingReplySms();
                break;
            case R.id.setting_reply_sms_toggle:
                processSettingReplySms();
                break;
            case R.id.setting_auto_smsout:
                mToggleAutoSms.setChecked(!mToggleAutoSms.isChecked());
                break;
            case R.id.setting_receive_smsout:
                mToggleReceiveSmsOut.setChecked(!mToggleReceiveSmsOut.isChecked());
                break;
            case R.id.setting_keyboard_send:
                mToggleKeyboardSend.setChecked(!mToggleKeyboardSend.isChecked());
                break;
            case R.id.setting_font_size:
                KhDialogRadioSelect dialogRadioSelect = KhDialogRadioSelect.newInstance(DialogRadioSelect.FONT_SIZE_TYPE,
                        mSettingBusiness.getFontSize() - 1, R.string.setting_font_title_dialog);
                dialogRadioSelect.setSelectListener(new BaseDialogKhFragment.DialogListener() {
                    @Override
                    public void dialogRightClick(int value) {
                        mSettingBusiness.setFontSize(value + 1);
                        drawSettingFontSize();
                        blurView.setVisibility(View.GONE);
                    }

                    @Override
                    public void dialogLeftClick() {
                        blurView.setVisibility(View.GONE);
                    }
                });
                dialogRadioSelect.setDialogListener(new BaseDialogKhFragment.IDialogListener() {
                    @Override
                    public void dialogDismiss() {
                        blurView.setVisibility(View.GONE);
                    }

                    @Override
                    public void dialogCancel() {
                        blurView.setVisibility(View.GONE);
                    }
                });
                dialogRadioSelect.show(getChildFragmentManager(), KhDialogRadioSelect.TAG);
                blurView.setVisibility(View.VISIBLE);
                break;
            case R.id.setting_delete_data: {
                KhDialogConfirm dialogConfirm = KhDialogConfirm.newInstance(getString(R.string.delete_data)
                        , getString(R.string.kh_setting_delete_data_message_confirm)
                        , DialogConfirm.CONFIRM_TYPE
                        , R.string.kh_setting_dialog_delete_data_action_delete_all
                        , R.string.kh_setting_dialog_delete_data_action_come_back);
                dialogConfirm.setSelectListener(new BaseDialogKhFragment.DialogListener() {
                    @Override
                    public void dialogRightClick(int value) {
                        blurView.setVisibility(View.GONE);
                    }

                    @Override
                    public void dialogLeftClick() {
                        blurView.setVisibility(View.GONE);
                    }
                });
                dialogConfirm.setDialogListener(new BaseDialogKhFragment.IDialogListener() {
                    @Override
                    public void dialogDismiss() {
                        blurView.setVisibility(View.GONE);
                    }

                    @Override
                    public void dialogCancel() {
                        blurView.setVisibility(View.GONE);
                    }
                });
                dialogConfirm.show(getChildFragmentManager(), "");
                blurView.setVisibility(View.VISIBLE);
                break;
            }
        }
    }

    private void setViewListener() {
//        mImgBack.setOnClickListener(this);
        mViewReplySms.setOnClickListener(this);
        mViewAutoSms.setOnClickListener(this);
        mViewReceiveSmsOut.setOnClickListener(this);
        mViewKeyboardSend.setOnClickListener(this);
        mViewFontSize.setOnClickListener(this);
        mToggleReplySms.setOnClickListener(this);
        mToggleAutoSms.setOnClickListener(this);
        mToggleReceiveSmsOut.setOnClickListener(this);
        mViewDeleteData.setOnClickListener(this);

        mToggleKeyboardSend.setOnCheckedChangeListener((view, isChecked) -> processSettingKeyboardSend());
        mToggleAutoSms.setOnCheckedChangeListener((view, isChecked) -> processSettingAutoSms());
        mToggleReceiveSmsOut.setOnCheckedChangeListener((view, isChecked) -> processSettingReceiveSmsOut());
    }

    private void drawDetail() {
        if (mAccountBusiness.isViettel()) {
            if (mApplication.getReengAccountBusiness().isSmsInEnable()) {
                mViewReplySms.setVisibility(View.VISIBLE);
//                if (mAccountBusiness.isVietnam()) {
//                    mViewDividerReplySms.setVisibility(View.VISIBLE);
//                } else {
//                    mViewDividerReplySms.setVisibility(View.GONE);
//                }
            } else {
                mViewReplySms.setVisibility(View.GONE);
//                mViewDividerReplySms.setVisibility(View.GONE);
            }
//            // VN
//            if (mAccountBusiness.isVietnam()) {
//                mViewAutoSms.setVisibility(View.VISIBLE);
//                mViewDividerSms.setVisibility(View.VISIBLE);
//            } else {
//                mViewDividerSms.setVisibility(View.GONE);
//                mViewAutoSms.setVisibility(View.GONE);
//            }
        } else {
            mViewAutoSms.setVisibility(View.GONE);
            mViewReplySms.setVisibility(View.GONE);
//            mViewDividerReplySms.setVisibility(View.GONE);
//            mViewDividerSms.setVisibility(View.GONE);
            mViewReceiveSmsOut.setVisibility(View.GONE);
        }
        // TODO: [START] Cambodia version
        mViewAutoSms.setVisibility(View.VISIBLE);
        // TODO: [END] Cambodia version

        mToggleReplySms.setChecked(mSettingBusiness.getPrefReplySms());
        mToggleAutoSms.setChecked(mSettingBusiness.isEnableAutoSmsOut());
        mToggleReceiveSmsOut.setChecked(mSettingBusiness.isEnableReceiveSmsOut());
        mToggleKeyboardSend.setChecked(mSettingBusiness.isSetupKeyboardSend());
        drawSettingFontSize();

        //Bo config sms
        mViewReplySms.setVisibility(View.GONE);
        mViewReceiveSmsOut.setVisibility(View.GONE);
    }

    private void processSettingReplySms() {
        boolean state = mToggleReplySms.isChecked();
        mSettingBusiness.setPrefReplySms(state);
    }

    private void processSettingAutoSms() {
        final boolean state = mToggleAutoSms.isChecked();
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
            mToggleAutoSms.setOnCheckedChangeListener(null);
            mToggleAutoSms.setChecked(!state);
            mToggleAutoSms.setOnCheckedChangeListener((view, isChecked) -> processSettingAutoSms());
            return;
        }
        mParentActivity.showLoadingDialog("", R.string.waiting);
        ProfileRequestHelper.onResponseSetAutoSmsOutListener listener = new ProfileRequestHelper.onResponseSetAutoSmsOutListener() {
            @Override
            public void onRequestSuccess() {
                mParentActivity.showToast(R.string.change_setting_success);
                mParentActivity.hideLoadingDialog();
                mSettingBusiness.setPrefEnableAutoSmsOut(state);
            }

            @Override
            public void onError(int errorCode) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(getString(R.string.e601_error_but_undefined), Toast.LENGTH_SHORT);
                if (isAdded()) {
                    mToggleAutoSms.setOnCheckedChangeListener(null);
                    mToggleAutoSms.setChecked(!state);
                    mToggleAutoSms.setOnCheckedChangeListener((view, isChecked) -> processSettingAutoSms());
                }
            }
        };
        ProfileRequestHelper.getInstance(mApplication).setAutoSmsOut(state, listener);
    }

    private void processSettingReceiveSmsOut() {
        final boolean state = mToggleReceiveSmsOut.isChecked();
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
            mToggleReceiveSmsOut.setOnCheckedChangeListener(null);
            mToggleReceiveSmsOut.setChecked(!state);
            mToggleReceiveSmsOut.setOnCheckedChangeListener((view, isChecked) -> processSettingReceiveSmsOut());
            return;
        }

        mParentActivity.showLoadingDialog("", R.string.waiting);
        ProfileRequestHelper.onResponseReceiveSmsOutListener listener = new ProfileRequestHelper.onResponseReceiveSmsOutListener() {
            @Override
            public void onRequestSuccess() {
                mParentActivity.showToast(R.string.change_setting_success);
                mParentActivity.hideLoadingDialog();
                mSettingBusiness.setPrefEnableReceiveSmsOut(state);
            }

            @Override
            public void onError(int errorCode) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(getString(R.string.e601_error_but_undefined), Toast.LENGTH_SHORT);
                if (isAdded()) {
                    mToggleReceiveSmsOut.setOnCheckedChangeListener(null);
                    mToggleReceiveSmsOut.setChecked(!state);
                    mToggleReceiveSmsOut.setOnCheckedChangeListener((view, isChecked) -> processSettingReceiveSmsOut());
                }
            }
        };
        ProfileRequestHelper.getInstance(mApplication).requestSettingReceiveSmsOut(state, listener);
    }

    private void processSettingKeyboardSend() {
        boolean state = mToggleKeyboardSend.isChecked();
        mSettingBusiness.setPrefSetupKeyboardSend(state);
    }

    private void drawSettingFontSize() {
        String fontSizeName;
        if (mSettingBusiness.getFontSize() == Constants.FONT_SIZE.SMALL) {
            fontSizeName = getString(R.string.setting_font_size_small);
        } else if (mSettingBusiness.getFontSize() == Constants.FONT_SIZE.MEDIUM) {
            fontSizeName = getString(R.string.setting_font_size_medium);
        } else {
            fontSizeName = getString(R.string.setting_font_size_large);
        }
        tvFontSizeInfor.setText(fontSizeName);
    }
}
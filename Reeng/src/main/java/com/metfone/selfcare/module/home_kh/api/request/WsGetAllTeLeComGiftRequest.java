package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetAllTeLeComGiftRequest extends BaseRequest<WsGetListGiftReceivedRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends WsGetListGiftReceivedRequest.Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("language")
        String language;

        public WsRequest(String isdn, String language) {
            this.isdn = isdn;
            this.language = language;
        }

        public WsRequest(String isdn) {
            this.isdn = isdn;
            this.language = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
        }
    }
}

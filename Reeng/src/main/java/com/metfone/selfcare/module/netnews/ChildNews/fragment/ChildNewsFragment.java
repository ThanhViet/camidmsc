package com.metfone.selfcare.module.netnews.ChildNews.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.netnews.ChildNews.adapter.ChildNewsAdapter;
import com.metfone.selfcare.module.netnews.ChildNews.presenter.ChildNewsPresenter;
import com.metfone.selfcare.module.netnews.ChildNews.presenter.IChildNewsPresenter;
import com.metfone.selfcare.module.netnews.ChildNews.view.IChildNewsView;
import com.metfone.selfcare.module.netnews.HomeNews.fragment.HomeNewsFragment;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.CustomLoadMoreView;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.home.fragment.TabNewsFragmentV2;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HaiKE on 8/19/17.
 */

public class ChildNewsFragment extends BaseFragment implements AbsInterface.OnNewsListener, IChildNewsView, BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.loadingView)
    View loadingView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.imvMoveTop)
    ImageView imvMoveTop;

    @BindView(R.id.layout_mark_read)
    View layoutMarkRead;

    @BindView(R.id.cbRead)
    CheckBox cbRead;

    int currentPage = 1;
    int categoryId = 0;
    ChildNewsAdapter adapter;
    ArrayList<NewsModel> datas = new ArrayList<>();
    View notDataView, errorView;
    LinearLayoutManager layoutManager;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    boolean isRefresh;
    boolean isTabRefresh;
    boolean isLoadNewsCount;
    long unixTime;

    private ListenerUtils listenerUtils;
    private boolean isLoadSuccess = false;

    IChildNewsPresenter mPresenter;
    private String categoryName;

    public static ChildNewsFragment newInstance(Bundle bundle) {

        ChildNewsFragment fragment = new ChildNewsFragment();
        try {
            if (fragment.getArguments() == null)
                fragment.setArguments(bundle);
            else
                fragment.getArguments().putAll(bundle);
        } catch (Exception ex) {
            Log.e("ChildNewFragment", "Exception", ex);
        }
        fragment.mPresenter = new ChildNewsPresenter();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child_base, container, false);
        if (mPresenter == null) {
            mPresenter = new ChildNewsPresenter();
        }
        setUnBinder(ButterKnife.bind(this, view));
        mPresenter.onAttach(this);
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
        setUp(view);
        return view;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void setUp(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryId = bundle.getInt(CommonUtils.KEY_CATEGORY_ID, 0);
            categoryName = bundle.getString(CommonUtils.KEY_CATEGORY_NAME);
        }
        isRefresh = true;
        if (datas == null || (datas.size() == 0)) {
            if (categoryId == 0)
                isLoadNewsCount = true;
            loadingView.setVisibility(View.VISIBLE);
            unixTime = 0;
            mPresenter.loadData(categoryId, currentPage, unixTime);
        }
        layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        layout_refresh.setOnRefreshListener(this);

        if (categoryId == 3 || categoryId == 135)//Giai tri
        {

            if (recyclerView.getItemDecorationCount() <= 0) {
                CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getBaseActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
            adapter = new ChildNewsAdapter(getBaseActivity(), R.layout.holder_large_news, datas, categoryId, this);
        } else if (categoryId == 7)//Nguoi dep
        {
            adapter = new ChildNewsAdapter(getBaseActivity(), R.layout.holder_lady_new, datas, categoryId, this);
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getTop(), getResources().getDimensionPixelOffset(R.dimen.v5_spacing_normal), recyclerView.getPaddingBottom());
            recyclerView.setLayoutManager(staggeredGridLayoutManager);
        } else {
            if (recyclerView.getItemDecorationCount() <= 0) {
                CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getBaseActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(), R.drawable.divider_default_tiin, true));
            }
            adapter = new ChildNewsAdapter(getBaseActivity(), R.layout.holder_large_news, datas, categoryId, this);
        }

        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(this);
        adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//        adapter.setAutoLoadMoreSize(3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        setupMoveTop();

        notDataView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_nodata, (ViewGroup) recyclerView.getParent(), false);
        notDataView.setOnClickListener(v -> onRefresh());
        errorView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_failed, (ViewGroup) recyclerView.getParent(), false);
        errorView.setOnClickListener(v -> onRefresh());

    }

    @Override
    public void loadDataSuccess(boolean flag) {
        loadingView.setVisibility(View.GONE);
        hideRefresh();
        if (!flag) {
            loadingFail();
        } else {
            isLoadSuccess = flag;
        }
    }

    @Override
    public void bindData(NewsResponse response) {
        if (datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if (response != null) {
            if (response.getData() != null) {
                loadingComplete(response.getData());
            } else {
                loadingFail();
                isLoadSuccess = false;
            }
        }
        isLoadNewsCount = false;
        isTabRefresh = false;
    }

    public void loadingComplete(ArrayList<NewsModel> response) {
        //Check trung
//        ArrayList<NewsModel> response = new ArrayList<>();
//        for(int i = 0; i < modelArrayList.size(); i++)
//        {
//            NewsModel model = modelArrayList.get(i);
//            if(!Utilities.checkTrung(response, model))
//                response.add(model);
//        }

        int mCurrentCounter = response.size();
        if (mCurrentCounter > 0)
            unixTime = response.get(response.size() - 1).getUnixTime();

        //Check refresh
//        if(currentPage == 1 && datas.size() > 0 && response.size() > 0)
//        {
//            NewsModel newModel = response.get(0);
//            NewsModel currentModel = datas.get(0);
//            if(newModel.getID() == currentModel.getID())
//                return;
//        }

        if (isTabRefresh) {
            //Check refresh
            if (datas.size() > 0 && response.size() > 0) {
                NewsModel newModel = response.get(0);
                NewsModel currentModel = datas.get(0);
                if (newModel.getID() == currentModel.getID()) {
                    return;
                } else {
                    currentPage = 1;
                }
            }
        }

        if (isLoadNewsCount && categoryId == 0) {
            try {
                int lastId = mPresenter.getLastNews(pref);
                ArrayList<NewsModel> temp = new ArrayList<>();
                for (int i = 0; i < response.size(); i++) {
                    NewsModel model = response.get(i);
                    if (model.getID() != lastId) {
                        temp.add(model);
                    } else {
                        break;
                    }
                }

                int size = temp.size();
                if (getParentFragment() != null && getParentFragment() instanceof TabNewsFragmentV2 && size > 0) {
                    TabNewsFragmentV2 tabNewsFragment = (TabNewsFragmentV2) getParentFragment();
                    if (tabNewsFragment.getCurrentAdapter() != null && tabNewsFragment.getCurrentAdapter().getItem(0) instanceof HomeNewsFragment) {
                        HomeNewsFragment homeNewsFragment = (HomeNewsFragment) tabNewsFragment.getCurrentAdapter().getItem(0);
                        homeNewsFragment.updateNewsCount(size);
                    }
                }

                if (mCurrentCounter == 0) {
                    adapter.setEmptyView(notDataView);
                } else {
                    datas.clear();
                    datas.addAll(response);
                    adapter.setNewData(datas);

                    if (datas.size() > 0) {
                        if (categoryId == 7) {
                            if (staggeredGridLayoutManager != null)
                                staggeredGridLayoutManager.scrollToPosition(0);
                        } else {
                            if (layoutManager != null)
                                layoutManager.scrollToPosition(0);
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e("ChildNewFragment", "Exception", ex);

            }
        } else {
            if (isRefresh) {
                if (mCurrentCounter == 0) {
                    adapter.setEmptyView(notDataView);
                } else {
                    datas.clear();
                    datas.addAll(response);
                    adapter.setNewData(datas);

                    if (categoryId == 0 && datas.size() > 0) {
                        updateLastNews();
                    }

                    if (datas.size() > 0) {
                        if (categoryId == 7) {
                            if (staggeredGridLayoutManager != null)
                                staggeredGridLayoutManager.scrollToPosition(0);
                        } else {
                            if (layoutManager != null)
                                layoutManager.scrollToPosition(0);
                        }
                    }
                }
            } else {
                if (mCurrentCounter == 0) {
                    adapter.loadMoreEnd();
                } else {
                    adapter.addData(response);
                    //datas.addAll(response);
                    adapter.loadMoreComplete();
                }
            }
        }
    }

    public void loadingFail() {
        if (adapter != null) {
            if (isRefresh) {
                adapter.setEmptyView(errorView);
            } else {
                adapter.loadMoreFail();
            }
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        unixTime = 0;
        if (mPresenter != null) {
            mPresenter.loadData(categoryId, currentPage, unixTime);
        }
    }

    public void forceLoadData() {
        if (datas == null || datas.isEmpty()) {
            onRefresh();
        }
    }

    public void onTabRefresh() {
        isTabRefresh = true;
        unixTime = 0;
        if (mPresenter != null) {
            mPresenter.loadData(categoryId, 1, unixTime);
        }
    }

    public void loadNewsCount() {
        isLoadNewsCount = true;
        currentPage = 1;
        unixTime = 0;
        if (mPresenter != null)
            mPresenter.loadData(categoryId, currentPage, unixTime);
    }

    @Override
    public void onLoadMoreRequested() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                currentPage++;
                isRefresh = false;
                mPresenter.loadData(categoryId, currentPage, unixTime);
            }
        }, 1000);

    }

    @Override
    public void onDestroyView() {
        if (mPresenter != null) {
            mPresenter.onDetach();
        }
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(getBaseActivity()) || isLoadSuccess || recyclerView == null || mPresenter == null)
            return;
        onRefresh();
    }

    @Override
    public void onItemClick(int position) {
        try {
            NewsModel model = datas.get(position);
            if (model == null) return;
            model.setReadFromSource(CommonUtils.TYPE_NEWS_DETAIL_FROM_CATEGORY);
            if (position == 0)
                model.setPositionFirst(true);
            else
                model.setPositionFirst(false);
            model.setSrcTabName(categoryName);
            readNews(model);
        } catch (IndexOutOfBoundsException ex) {
            Log.e("ChildNewFragment", "Exception", ex);

        }
    }

    @Override
    public void onItemRadioClick(NewsModel model) {
    }

    @Override
    public void onItemClickMore(NewsModel model) {
        DialogUtils.showOptionNewsItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionNew(object, menuId);
            }
        });
    }

    public void setupMoveTop() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    if (layoutManager != null) {
//                    int lastVisible = layoutManager.findLastCompletelyVisibleItemPosition();
                        int firstVisible = layoutManager.findFirstCompletelyVisibleItemPosition();
                        if (dy <= 0)// || lastVisible == datas.size() - 1)
                        {
                            if (firstVisible == 0) {
                                //Hide
                                hideMoveTop(imvMoveTop);
                            } else {
                                //Show
                                showMoveTop(imvMoveTop);
                            }
                        } else {
                            //Hide
                            hideMoveTop(imvMoveTop);
                        }
                    }
                } catch (Exception ex) {
                    Log.e("ChildNewFragment", "Exception", ex);

                }
            }
        });
        imvMoveTop.setOnClickListener(view -> layoutManager.scrollToPosition(0));
    }

    public void scrollToPosition(int position) {
        if (layoutManager != null && recyclerView != null) {
            recyclerView.stopScroll();
            layoutManager.smoothScrollToPosition(recyclerView, null, position);
        }
    }

    public void updateLastNews() {
        if (datas != null && datas.size() > 0) {
            mPresenter.updateLastNews(pref, datas.get(0).getID());
        }
    }
}

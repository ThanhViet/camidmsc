package com.metfone.selfcare.module.keeng.widget.buttonSheet;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.model.CategoryModel;

/**
 * Created by namnh40 on 3/21/2017.
 */

public class BottomSheetHolder extends BaseHolder {
    TextView tvTitle;
    ImageView icon;
    CategoryModel item;
    BaseListener.OnClickBottomSheet mListener;

    public BottomSheetHolder(final View itemView, BaseListener.OnClickBottomSheet listener) {
        super(itemView);
        this.mListener = listener;
        tvTitle = itemView.findViewById(R.id.tvTitle);
        icon = itemView.findViewById(R.id.icon);
        itemView.setOnClickListener(v -> {
            if (mListener != null)
                mListener.onClickSheet(item);
        });
    }

    public void bind(CategoryModel item) {
        this.item = item;
        if (item.getResTitle() > 0)
            tvTitle.setText(item.getResTitle());
        else
            tvTitle.setText(item.getTitle());
        icon.setImageResource(item.getResIcon());
    }
}

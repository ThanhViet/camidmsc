/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movienew.holder;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.movienew.listener.OnClickCountry;
import com.metfone.selfcare.module.movienew.model.Country;

import butterknife.BindView;

public class CountryHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.txt_title)
    @Nullable
    TextView tvTitle;

    @BindView(R.id.img_genres)
    @Nullable
    ImageView imgGenres;

//    @BindView(R.id.view_background)
//    @Nullable
//    ToggleButton viewBackground;

    private OnClickCountry listener;
    private View rootView;
    private Activity activity;
    String[] colors = {"#C41627", "#E1660C", "#268854", "#552586", "#012842", "#7A137C", "#068FAD", "#98500D"};

    public CountryHolder(View view, Activity activity, final OnClickCountry listener) {
        super(view);
        this.activity = activity;
        this.rootView = view;
        this.listener = listener;
    }

    @Override
    public void bindData(Object item, int position) {
//        viewBackground.setOnCheckedChangeListener(null);
        Country country = (Country) item;
        if (tvTitle != null) tvTitle.setText(country.getCategoryName());
//        viewBackground.setBackgroundDrawable(createDrawable(colors[position%colors.length]));
        if (country.isChecked()) {
            imgGenres.setAlpha(255);
        } else {
            imgGenres.setAlpha((int) (0.5*255));
        }
//        viewBackground.setBackgroundColor(activity.getResources().getColor(R.color.bg_start_trans));
//        viewBackground.setChecked(country.isChecked());
        String urlImage = "http://image.metfone.com.kh/api/files/image_public-img/"+country.getCategoryId()+".jpg";
        Glide.with(activity)
                .load(urlImage).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                .into(imgGenres);
//        Glide.with(activity)
//                .load(country.getUrlImages()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
//                .into(imgGenres);
        imgGenres.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                country.setChecked(!country.isChecked());

//                country.setChecked(isChecked);
                listener.onClick(item, position, country.isChecked());
            }
        });
//        viewBackground.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            if (isChecked) {
//                viewBackground.setBackgroundColor(Color.TRANSPARENT);
//            } else {
//                viewBackground.setBackgroundColor(activity.getResources().getColor(R.color.bg_start_trans));
//            }
//            country.setChecked(isChecked);
//            listener.onClick(item, position, isChecked);
//        });
    }

    //int enabled, int pressed, int disabled
    private StateListDrawable createDrawable(String color) {
        StateListDrawable stateListDrawable = new StateListDrawable();

//        stateListDrawable.addState(new int[] { -android.R.attr.state_pressed, android.R.attr.state_enabled }, new ColorDrawable(enabled));
//        stateListDrawable.addState(new int[] { android.R.attr.state_pressed, android.R.attr.state_enabled }, new ColorDrawable(pressed));
//        stateListDrawable.addState(new int[] { -android.R.attr.state_enabled }, new ColorDrawable(disabled));
        stateListDrawable.addState(new int[] { android.R.attr.state_checked}, new DrawableGradient(new int[] {Color.parseColor("#00000000"), Color.parseColor(color)}, 0).SetTransparency(0));
        stateListDrawable.addState(new int[] { -android.R.attr.state_checked}, new DrawableGradient(new int[] {Color.parseColor("#00000000"), Color.parseColor(color) }, 0).SetTransparency(50));
        return stateListDrawable;
    }

    public class DrawableGradient extends GradientDrawable {
        DrawableGradient(int[] colors, int cornerRadius) {
            super(GradientDrawable.Orientation.TOP_BOTTOM, colors);

            try {
                this.setShape(GradientDrawable.RECTANGLE);
                this.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                this.setCornerRadius(cornerRadius);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public DrawableGradient SetTransparency(int transparencyPercent) {
            this.setAlpha(255 - ((255 * transparencyPercent) / 100));

            return this;
        }
    }
}

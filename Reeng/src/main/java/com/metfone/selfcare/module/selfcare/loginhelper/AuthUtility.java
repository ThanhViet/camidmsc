/*
package com.metfone.selfcare.module.selfcare.loginhelper;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.TokenResponse;

import org.json.JSONException;

public class AuthUtility {
    public static final String SHARED_PREFERENCES_NAME = "AuthStatePreference";
    public static final String AUTH_STATE = "AUTH_STATE";
    public static AuthState mAuthState;

    public static void persistAuthState(ApplicationController context) {
        context.getPref().edit()
                .putString(AUTH_STATE, mAuthState.toString())
                .apply();
        mAuthState = restoreAuthState(context);
    }

    public static void clearAuthState(ApplicationController context) {
        context.getPref().edit()
                .remove(AUTH_STATE)
                .apply();
    }

    @Nullable
    public static AuthState restoreAuthState(ApplicationController context) {
        String jsonString = context.getPref().getString(AUTH_STATE, null);
        if (!TextUtils.isEmpty(jsonString)) {
            try {
                return AuthState.jsonDeserialize(jsonString);
            } catch (JSONException jsonException) {
                // should never happen
            }
        }
        return null;
    }

    public static void updateAuth(@Nullable TokenResponse tokenResponse, @Nullable AuthorizationException exception) {
        mAuthState.update(tokenResponse, exception);
    }

}
*/

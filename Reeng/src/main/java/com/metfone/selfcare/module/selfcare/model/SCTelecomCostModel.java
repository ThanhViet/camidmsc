package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCTelecomCostModel  implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("rewardCode")
    @Expose
    private String rewardCode;
    @SerializedName("loyaltyBalanceCode")
    @Expose
    private String loyaltyBalanceCode;
    @SerializedName("cost")
    @Expose
    private Integer cost;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public String getLoyaltyBalanceCode() {
        return loyaltyBalanceCode;
    }

    public void setLoyaltyBalanceCode(String loyaltyBalanceCode) {
        this.loyaltyBalanceCode = loyaltyBalanceCode;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "SCTelecomCostModel{" +
                "id=" + id +
                ", rewardCode='" + rewardCode + '\'' +
                ", loyaltyBalanceCode='" + loyaltyBalanceCode + '\'' +
                ", cost=" + cost +
                '}';
    }
}

package com.metfone.selfcare.module.keeng.network.restful;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.model.LoginObject;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.utils.Utilities;
import com.metfone.selfcare.restful.GsonRequest;
import com.metfone.selfcare.restful.StringRequest;

import java.nio.charset.StandardCharsets;
import java.util.Date;

public abstract class AbsRestful {
    private static final String TAG = "AbsRestful";

    private static final int TIME_OUT = 15000;
    private static final int MAX_NUMBER_TO_RETRY = 2;
    private static final String COUNTRY_CODE = "country_code";
    private static final String DEVICE_ID = "device_id";
    private static final String VERSION = "version";
    private static final String REVISION = "revision";
    private static final String LANGUAGE_CODE = "language_code";
    private static final String SESSION_TOKEN = "session_token";
    private static final String USER_ID = "user_id";
    private final RetryPolicy policy = new DefaultRetryPolicy(TIME_OUT, MAX_NUMBER_TO_RETRY, 1.0f);

    protected Context context;

    public AbsRestful(Context context) {
        super();
        this.context = context;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        com.metfone.selfcare.util.Utilities.addDefaultParamsRequestVolley(req);
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        if (req.getMethod() != Request.Method.POST)
            req.setShouldCache(true);
        req.setRetryPolicy(policy);
        App.getInstance().getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        com.metfone.selfcare.util.Utilities.addDefaultParamsRequestVolley(req);
        req.setTag("TAG_REQUEST" + System.currentTimeMillis());
        req.setShouldCache(false);
        req.setRetryPolicy(policy);
        App.getInstance().getRequestQueue().add(req);
    }

    private <T> void initHeader(GsonRequest<T> req) {
        req.setHeader(DEVICE_ID, DeviceHelper.getDeviceId(context));
        req.setHeader(COUNTRY_CODE, Utilities.getCountryCode());
        req.setHeader(LANGUAGE_CODE, Utilities.getLanguageCode());
        req.setHeader(SESSION_TOKEN, LoginObject.getSessionToken(context));
        req.setHeader(USER_ID, LoginObject.getId(context));
        req.setHeader(VERSION, Utilities.getVersionApp());
        req.setHeader(REVISION, Utilities.getVersionCode());
        req.setHeader("msisdn", LoginObject.getPhoneNumber(context));
        req.setHeader(BaseApi.MOCHA_API, ApplicationController.self().getReengAccountBusiness().getMochaApi());
        req.setHeader(BaseApi.UUID, com.metfone.selfcare.util.Utilities.getUuidApp());
    }

    private void initHeader(StringRequest req) {
        req.setHeader(DEVICE_ID, DeviceHelper.getDeviceId(context));
        req.setHeader(COUNTRY_CODE, Utilities.getCountryCode());
        req.setHeader(LANGUAGE_CODE, Utilities.getLanguageCode());
        req.setHeader(SESSION_TOKEN, LoginObject.getSessionToken(context));
        req.setHeader(USER_ID, LoginObject.getId(context));
        req.setHeader(VERSION, Utilities.getVersionApp());
        req.setHeader(REVISION, Utilities.getVersionCode());
        req.setHeader("msisdn", LoginObject.getPhoneNumber(context));
        req.setHeader(BaseApi.MOCHA_API, ApplicationController.self().getReengAccountBusiness().getMochaApi());
        req.setHeader(BaseApi.UUID, com.metfone.selfcare.util.Utilities.getUuidApp());
    }

    private void initHeader(PostFormDataRequest req) {
        req.setHeader(DEVICE_ID, DeviceHelper.getDeviceId(context));
        req.setHeader(COUNTRY_CODE, Utilities.getCountryCode());
        req.setHeader(LANGUAGE_CODE, Utilities.getLanguageCode());
        req.setHeader(SESSION_TOKEN, LoginObject.getSessionToken(context));
        req.setHeader(USER_ID, LoginObject.getId(context));
        req.setHeader(VERSION, Utilities.getVersionApp());
        req.setHeader(REVISION, Utilities.getVersionCode());
        req.setHeader("msisdn", LoginObject.getPhoneNumber(context));
        req.setHeader(BaseApi.MOCHA_API, ApplicationController.self().getReengAccountBusiness().getMochaApi());
        req.setHeader(BaseApi.UUID, com.metfone.selfcare.util.Utilities.getUuidApp());
    }

    public <T> void addReq(GsonRequest<T> req) {
        initHeader(req);
        addToRequestQueue(req);
    }

    public <T> void addReq(GsonRequest<T> req, String tag) {
        initHeader(req);
        addToRequestQueue(req, tag);
    }

    public <T> void addReq(StringRequest req, String tag) {
        initHeader(req);
        addToRequestQueue(req, tag);
    }

    public <T> void addReq(PostFormDataRequest req, String tag) {
        initHeader(req);
        addToRequestQueue(req, tag);
    }

    public Object getCache(String tag, Class<?> output) {
        Object result = null;
        Cache cache = App.getInstance().getRequestQueue().getCache();
        Entry currentEntry = cache.get(tag);
        if (currentEntry != null) {
            Date date = new Date();
            long timeCache = date.getTime() - currentEntry.serverDate;
            // 1 ngay xoa cache
            if (timeCache > DateTimeUtils.DAY) {
                cache.remove(tag);
            } else {
                try {
                    String data = new String(currentEntry.data, StandardCharsets.UTF_8);
                    result = new Gson().fromJson(data, output);
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        }
        return result;
    }

    public void refreshCache(String key) {
        try {
            Cache cache = App.getInstance().getRequestQueue().getCache();
            cache.remove(key);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }
}
package com.metfone.selfcare.module.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/19/17.
 */

public class HomeNewsResponse extends ErrorResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<HomeNewsModel> data = new ArrayList<>();

    public ArrayList<HomeNewsModel> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(ArrayList<HomeNewsModel> data) {
        this.data = data;
    }
}

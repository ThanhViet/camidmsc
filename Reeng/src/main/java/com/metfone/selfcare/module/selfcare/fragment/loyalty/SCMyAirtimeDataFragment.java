package com.metfone.selfcare.module.selfcare.fragment.loyalty;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.selfcare.adapter.SCMyAirtimeDataAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.event.SCAccountEvent;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.model.SCAirTimeInfo;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestAirTimeInfo;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;

public class SCMyAirtimeDataFragment extends SCBaseFragment implements AbsInterface.onAirTimeClickListener, SwipeRefreshLayout.OnRefreshListener {
    private static final int TYPE_AIR_TIME_DATA = 0;
    private LoadingViewSC loadingView;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    ArrayList<SCAirTimeInfo> amountList = new ArrayList<>();
    SCMyAirtimeDataAdapter adapter;

    @Override
    public String getName() {
        return "SCMyAirtimeDataFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_my_credit_data;
    }

    public static SCMyAirtimeDataFragment newInstance() {
        SCMyAirtimeDataFragment fragment = new SCMyAirtimeDataFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        layout_refresh = view.findViewById(R.id.refresh);
        layout_refresh.setColorSchemeResources(R.color.sc_primary);
        if (layout_refresh != null) {
            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.sc_primary));
            layout_refresh.setOnRefreshListener(this);
        }
        loadingView = view.findViewById(R.id.loading_view);
        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCMyAirtimeDataAdapter(mActivity, this);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);

        loadingView.setLoadingErrorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadAmountList(TYPE_AIR_TIME_DATA);
            }
        });

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login lai
                /*Intent intent = new Intent(getActivity(), SCAccountActivity.class);
                mActivity.startActivity(intent);*/
            }
        });

        loadAmountList(TYPE_AIR_TIME_DATA);

        return view;
    }

    private void loadAmountList(int typeBalance) {
        if (!isRefresh)
            loadingView.loadBegin();

        WSSCRestful restful = new WSSCRestful(mActivity);
        restful.getAirTimeAmount(typeBalance, new Response.Listener<RestAirTimeInfo>() {
            @Override
            public void onResponse(RestAirTimeInfo restAirTimeInfo) {
                hideRefresh();
                if (restAirTimeInfo != null && restAirTimeInfo.getData() != null) {
                    loadingView.loadFinish();

                    amountList.clear();
                    amountList.addAll(restAirTimeInfo.getData());
                    adapter.setItemsList(amountList);
                    adapter.notifyDataSetChanged();
                } else {
                    loadingView.loadError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                hideRefresh();
                loadingView.loadError();
            }
        });
    }

    private void doSubmit(SCAirTimeInfo model) {
        if (mActivity == null || model == null) return;
        WSSCRestful restful = new WSSCRestful(mActivity);
        restful.airTimeData("AIRTIME-DATA", model.getActionType(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.optInt("errorCode", -1);
                    String message = jsonObject.optString("message");
                    mActivity.showToast(message);
                    if (code == 200)
                        EventBus.getDefault().postSticky(new SCAccountEvent(SCAccountEvent.UPDATE_INFO));
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }

    @Override
    public void onBorrowClick(SCAirTimeInfo model) {
        doSubmit(model);
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        loadAmountList(TYPE_AIR_TIME_DATA);
    }

}

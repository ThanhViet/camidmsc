package com.metfone.selfcare.module.keeng.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.Utilities;
import com.metfone.selfcare.module.metfoneplus.fragment.MPExchangeFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackUsFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;

import java.text.DecimalFormat;
import java.util.List;

/**
 * @author namnh40
 */
public abstract class BaseFragment extends Fragment implements OnInternetChangedListener {

    // tuyet doi ko dc thay doi bien TAG nay
    protected final String TAG = getName();
    protected BaseSlidingFragmentActivity mActivity;
    protected Toolbar toolbar;
    protected boolean isVisibleToUser;
    protected boolean isDataInitiated;
    protected boolean isViewInitiated;

    protected SwipeRefreshLayout layout_refresh;
    protected boolean isRefresh = false;

    public abstract String getName();

    protected FragmentManager mFragmentManager;
    protected FragmentTransaction mFragmentTransaction;

    public void onBackPressed() {
        if (mActivity == null) {
            Utilities.gotoMain(getActivity());
        } else {
            mActivity.onBackPressed();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseSlidingFragmentActivity) context;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (BuildConfig.DEBUG)
            Log.d(TAG, "onActivityResult requestCode= " + requestCode + "; resultCode= " + resultCode + "; data= " + data + "-------");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume isVisible: " + isVisibleToUser + " -------");
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause ------------------------------");
//        Utilities.hideKeyboard(getView(), getActivity(), TAG);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        isViewInitiated = false;
        Log.d(TAG, "onDestroy ------------------------------");
        super.onDestroy();
    }

    public void setTitle(String title) {
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }

    public abstract int getResIdView();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getResIdView(), container, false);
        isViewInitiated = false;
        Log.d(TAG, "Fragment name: " + this.getClass().getSimpleName() + " -------------------------");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated visible: " + isVisibleToUser + " -------------------------");
        isViewInitiated = true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        Log.d(TAG, "setUserVisibleHint " + isVisibleToUser);
    }

    public boolean canLazyLoad() {
        return isVisibleToUser && isViewInitiated && !isDataInitiated;
    }

    @Override
    public void onInternetChanged() {

    }

    protected void showRefresh() {
        if (layout_refresh != null) {
            layout_refresh.setRefreshing(true);
        }
    }

    @SuppressWarnings("deprecation")
    protected void hideRefresh() {
        if (layout_refresh != null) {
            isRefresh = false;

            layout_refresh.setRefreshing(false);
            layout_refresh.destroyDrawingCache();
            layout_refresh.clearAnimation();
        }
    }

    protected void popBackStackFragment() {
        mFragmentManager.popBackStack();
    }

    protected void replaceFragment(int container, Fragment fragment, String tag) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                R.anim.decelerate_slide_out_left_fragment,
                R.anim.decelerate_slide_in_left_fragment,
                R.anim.decelerate_slide_out_right
        );

        mFragmentTransaction.replace(container, fragment);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(tag);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    protected void addFragment(int container, Fragment fragment, String tag) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
            R.anim.decelerate_slide_out_left_fragment,
            R.anim.decelerate_slide_in_left_fragment,
            R.anim.decelerate_slide_out_right
        );

        mFragmentTransaction.add(container, fragment);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(tag);
        mFragmentTransaction.commitAllowingStateLoss();
    }
    public String formatPoint(double point){
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        String result = formatter.format(point);
        return result.replace(".",",");
    }
}

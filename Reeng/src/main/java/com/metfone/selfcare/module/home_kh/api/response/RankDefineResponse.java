package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.RankDefine;

import java.util.List;

public class RankDefineResponse{
    @SerializedName("code")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("listRankDefine")
    @Expose
    List<RankDefine> listRank;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RankDefine> getListRank() {
        return listRank;
    }

    public void setListRank(List<RankDefine> listRank) {
        this.listRank = listRank;
    }
}

package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import lombok.Data;

@Data
public class WsRedeemGiftTetResponse extends BaseResponse<WsRedeemGiftTetResponse.Response> {
    @Data
    public class Response {
        @SerializedName("errorCode")
        private String errorCode;
        @SerializedName("messageCode")
        private String messageCode;
    }
}


package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.fragment.history.adapter.IHistoryPointLog;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

public class PointTransferHistory implements IHistoryPointLog {
    @SerializedName("hisId")
    @Expose
    private long hisId;
    @SerializedName("vtAccId")
    @Expose
    private long vtAccId;
    @SerializedName("createDate")
    @Expose
    private String createDate;
    @SerializedName("amount")
    @Expose
    private int amount;
    @SerializedName("userTransfer")
    @Expose
    private String userTransfer;
    @SerializedName("transferType")
    @Expose
    private int transferType;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("productId")
    @Expose
    private Object productId;
    @SerializedName("transferTypeName")
    @Expose
    private String transferTypeName;
    @SerializedName("pointId")
    @Expose
    private int pointId;
    @SerializedName("pointName")
    @Expose
    private String pointName;
    @SerializedName("pointExpireDate")
    @Expose
    private Object pointExpireDate;

    public long getHisId() {
        return hisId;
    }

    public void setHisId(long hisId) {
        this.hisId = hisId;
    }

    public long getVtAccId() {
        return vtAccId;
    }

    public void setVtAccId(long vtAccId) {
        this.vtAccId = vtAccId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getUserTransfer() {
        return userTransfer;
    }

    public void setUserTransfer(String userTransfer) {
        this.userTransfer = userTransfer;
    }

    public int getTransferType() {
        return transferType;
    }

    public void setTransferType(int transferType) {
        this.transferType = transferType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Object getProductId() {
        return productId;
    }

    public void setProductId(Object productId) {
        this.productId = productId;
    }

    public String getTransferTypeName() {
        return transferTypeName;
    }

    public void setTransferTypeName(String transferTypeName) {
        this.transferTypeName = transferTypeName;
    }

    public int getPointId() {
        return pointId;
    }

    public void setPointId(int pointId) {
        this.pointId = pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public Object getPointExpireDate() {
        return pointExpireDate;
    }

    public void setPointExpireDate(Object pointExpireDate) {
        this.pointExpireDate = pointExpireDate;
    }

    @Override
    public long getId() {
        return hisId;
    }

    @Override
    public String getName() {
        return pointName;
    }

    @Override
    public String getCreatedAt() {
        return createDate;
    }

    @Override
    public long getValue() {
        return amount;
    }

    @Override
    public String valueShow(String start) {
        if (amount == 0) {
            return ResourceUtils.getString(R.string.free);
        } else {
            return start + ResourceUtils.getString(R.string.kh_format_change_points, amount);
        }
    }
}

package com.metfone.selfcare.module.keeng.widget;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

public class DialogMediaInfo extends AbsDialog implements View.OnClickListener {
    Button buttonClose;
    TextView tvTitle;
    TextView tvSinger;
    TextView tvCreator;
    ImageView image;

    public DialogMediaInfo(Context context, AllModel item) {
        super(context, R.style.style_dialog2);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (item != null) {
            if (item.getType() == Constants.TYPE_VIDEO
                    || item.getType() == Constants.TYPE_YOUTUBE)
                setContentView(R.layout.dialog_video_info);
            else
                setContentView(R.layout.dialog_song_info);
            init();
            try {
                tvTitle.setText(item.getName());
                tvSinger.setText(item.getSinger());
                if (!TextUtils.isEmpty(item.getRecordName())) {
                    tvCreator.setVisibility(View.VISIBLE);
                    tvCreator.setText(Html.fromHtml(getContext().getString(R.string.creator_name, item.getRecordName())));
                } else {
                    tvCreator.setVisibility(View.GONE);
                    tvCreator.setText("");
                }
                if (item.getType() == Constants.TYPE_VIDEO)
                    ImageBusiness.setVideo(item.getImage(), image);
                else
                    ImageBusiness.setSong(image, item.getImage());

            } catch (Exception e) {
                Log.e(TAG, e);
            }
        } else
            setContentView(R.layout.dialog_song_info);
    }

    @Override
    public String getNameClass() {
        return "DialogMediaInfo";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void init() {
        this.setCancelable(true);
        this.setCanceledOnTouchOutside(true);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvSinger = (TextView) findViewById(R.id.tvSinger);
        tvCreator = (TextView) findViewById(R.id.tvCreator);
        image = (ImageView) findViewById(R.id.image);
        buttonClose = (Button) findViewById(R.id.btnClose);
        buttonClose.setOnClickListener(this);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_Zoom;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClose:
                dismiss();
                break;

            default:
                break;
        }
    }

}

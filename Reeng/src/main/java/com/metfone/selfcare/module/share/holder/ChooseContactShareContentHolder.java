/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/15
 *
 */

package com.metfone.selfcare.module.share.holder;

import android.app.Activity;
import android.content.res.Resources;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.module.share.listener.ShareContentListener;
import com.metfone.selfcare.util.EnumUtils;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.OnClick;

import static com.metfone.selfcare.business.UserInfoBusiness.isLogin;

public class ChooseContactShareContentHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.thread_avatar)
    @Nullable
    ImageView ivThreadAvatar;
    @BindView(R.id.rlAvatarGroup)
    @Nullable
    View viewAvatarGroup;
    @BindView(R.id.contact_avatar_text)
    @Nullable
    TextView tvAvatar;

    @BindView(R.id.button_action)
    TextView btnAction;

    private Activity activity;
    private ShareContentListener listener;
    private ContactProvisional data;
    private int position;
    private ApplicationController mApplication;
    private int sizeAvatar;
    private Resources resources;
    private ReengMessageConstant.MessageType messageType;
    private UserInfoBusiness userInfoBusiness;
    private ShareContentBusiness shareContentBusiness;
    private int typeSharing;
    public ChooseContactShareContentHolder(View view, Activity activity, ShareContentListener listener,  int typeSharing) {
        super(view);
        this.listener = listener;
        mApplication = (ApplicationController) activity.getApplication();
        this.resources = mApplication.getResources();
        sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
        userInfoBusiness = new UserInfoBusiness(activity);
        this.typeSharing = typeSharing;
    }
    public ChooseContactShareContentHolder(View view, Activity activity, ShareContentListener listener, ReengMessageConstant.MessageType messageType) {
        super(view);
        this.activity = activity;
        this.listener = listener;
        mApplication = (ApplicationController) activity.getApplication();
        this.resources = mApplication.getResources();
        sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
        this.messageType = messageType;
        userInfoBusiness = new UserInfoBusiness(activity);
    }

    @Override
    public void bindData(Object item, int position) {
        this.position = position;
        if (item instanceof ContactProvisional) {
            data = (ContactProvisional) item;
            switch (data.getState()) {
                case ContactProvisional.SEND:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.VISIBLE);
                        btnAction.setEnabled(true);
                        btnAction.setBackground(resources.getDrawable(R.drawable.button_share_content_state_send));
                        btnAction.setTextColor(resources.getColor(R.color.color_btn_share_content_state_send));
                        btnAction.setText(R.string.btn_share_content_state_send);
                        if (typeSharing == 2) {
                            tvTitle.setTextColor(resources.getColor(R.color.text_password));
                        } else {
                            tvTitle.setTextColor(resources.getColor(R.color.white));
                        }
                    }
                    break;
                case ContactProvisional.SENT:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.VISIBLE);
                        btnAction.setEnabled(false);
                        btnAction.setBackground(resources.getDrawable(R.drawable.button_share_content_state_sent));
                        btnAction.setTextColor(resources.getColor(R.color.color_btn_share_content_state_sent));
                        btnAction.setText(R.string.btn_share_content_state_sent);
                        if (typeSharing == 2) {
                            tvTitle.setTextColor(resources.getColor(R.color.text_password));
                        } else {
                            tvTitle.setTextColor(resources.getColor(R.color.white));
                        }
                    }
                    break;
                case ContactProvisional.RETRY:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.VISIBLE);
                        btnAction.setEnabled(true);
                        btnAction.setBackground(resources.getDrawable(R.drawable.button_share_content_state_retry));
                        btnAction.setTextColor(resources.getColor(R.color.color_btn_share_content_state_retry));
                        btnAction.setText(R.string.btn_share_content_state_retry);
                        if (typeSharing == 2) {
                            tvTitle.setTextColor(resources.getColor(R.color.text_password));
                        } else {
                            tvTitle.setTextColor(resources.getColor(R.color.white));
                        }
                    }
                    break;
                case ContactProvisional.UNDO:
                case ContactProvisional.OPEN_CHAT:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.VISIBLE);
                        btnAction.setEnabled(true);
                        btnAction.setBackground(resources.getDrawable(R.drawable.button_share_content_state_open_chat));
                        btnAction.setTextColor(resources.getColor(R.color.color_btn_share_content_state_open_chat));
                        btnAction.setText(R.string.btn_share_content_state_undo);
                        if (typeSharing == 2) {
                            tvTitle.setTextColor(resources.getColor(R.color.text_password));
                        } else {
                            tvTitle.setTextColor(resources.getColor(R.color.white));
                        }
                    }
                    break;
                default:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.GONE);
                        btnAction.setEnabled(false);
                    }
                    break;
            }
            if (data.getContact() instanceof ThreadMessage) {
                setMessageView((ThreadMessage) data.getContact());
            } else if (data.getContact() instanceof PhoneNumber) {
                setContactView((PhoneNumber) data.getContact());
            } else if (data.getContact() instanceof String) {
                if (tvTitle != null)
                    tvTitle.setText((String) data.getContact());
                if (ivThreadAvatar != null) ivThreadAvatar.setVisibility(View.VISIBLE);
                if (tvAvatar != null) tvAvatar.setVisibility(View.GONE);
                mApplication.getAvatarBusiness().setUnknownNumberAvatar(ivThreadAvatar, tvAvatar, ((String) data.getContact()), sizeAvatar);
            }
        } else {
            data = null;
        }
    }

    private void setContactView(PhoneNumber phoneNumber) {
        if (tvTitle != null)
            tvTitle.setText(phoneNumber.getName());
        if (ivThreadAvatar != null) ivThreadAvatar.setVisibility(View.VISIBLE);
        if (tvAvatar != null) tvAvatar.setVisibility(View.GONE);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(ivThreadAvatar, tvAvatar, phoneNumber, sizeAvatar);
    }

    private void setMessageView(ThreadMessage threadMessage) {
        int threadType = threadMessage.getThreadType();
        if (tvTitle != null)
            tvTitle.setText(mApplication.getMessageBusiness().getThreadName(threadMessage));
        if (ivThreadAvatar != null) ivThreadAvatar.setVisibility(View.VISIBLE);
        if (tvAvatar != null) tvAvatar.setVisibility(View.GONE);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String numberFriend = threadMessage.getSoloNumber();
            PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(numberFriend);
            StrangerPhoneNumber stranger = threadMessage.getStrangerPhoneNumber();
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(ivThreadAvatar, tvAvatar, phoneNumber, sizeAvatar);
            } else {
                if (threadMessage.isStranger()) {
                    if (stranger != null) {
                        mApplication.getAvatarBusiness().setStrangerAvatar(ivThreadAvatar, tvAvatar,
                                stranger, stranger.getPhoneNumber(), null, null, sizeAvatar);
                    } else {
                        mApplication.getAvatarBusiness().setUnknownNumberAvatar(ivThreadAvatar, tvAvatar, numberFriend, sizeAvatar);
                    }
                } else {
                    mApplication.getAvatarBusiness().setUnknownNumberAvatar(ivThreadAvatar, tvAvatar, numberFriend, sizeAvatar);
                }
            }
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
            mApplication.getAvatarBusiness().setGroupThreadAvatar(ivThreadAvatar, viewAvatarGroup, threadMessage);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {//chat room
            mApplication.getAvatarBusiness().setOfficialThreadAvatar(ivThreadAvatar, sizeAvatar,
                    threadMessage.getServerId(), null, false);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            if (ivThreadAvatar != null)
                ivThreadAvatar.setImageResource(R.drawable.chat_ic_broadcast_group);
        } else {
            OfficerAccount official = mApplication.getOfficerBusiness().getOfficerAccountByServerId(threadMessage.getServerId());
            mApplication.getAvatarBusiness().setOfficialThreadAvatar(ivThreadAvatar, sizeAvatar,
                    threadMessage.getServerId(), official, false);
        }
    }

    @OnClick(R.id.button_action)
    public void onClickItem() {
        if (listener != null && data != null) {
            if (isLogin() == EnumUtils.LoginVia.NOT) {
                userInfoBusiness.showLoginDialog((BaseSlidingFragmentActivity) activity, R.string.info, R.string.content_popup_login);
            } else if (StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getJidNumber())) {
                userInfoBusiness.showAddPhoneDialog((BaseSlidingFragmentActivity) activity, R.string.info, R.string.content_popup_add_phone);
            }else{
                listener.onShareToContact(data, position);
            }
        }
    }
}
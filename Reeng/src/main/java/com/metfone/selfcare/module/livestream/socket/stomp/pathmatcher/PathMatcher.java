package com.metfone.selfcare.module.livestream.socket.stomp.pathmatcher;

import com.metfone.selfcare.module.livestream.socket.stomp.dto.StompMessage;

public interface PathMatcher {

    boolean matches(String path, StompMessage msg);
}

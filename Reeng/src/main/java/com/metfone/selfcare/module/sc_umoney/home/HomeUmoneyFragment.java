package com.metfone.selfcare.module.sc_umoney.home;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.sc_umoney.event.HomeUmoneyEvent;
import com.metfone.selfcare.module.sc_umoney.main_umoney.TabUmoneyActivity;
import com.metfone.selfcare.module.sc_umoney.network.UMoneyApi;
import com.metfone.selfcare.module.sc_umoney.network.model.FieldMap;
import com.metfone.selfcare.module.sc_umoney.network.response.FieldMapResponse;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

import org.greenrobot.eventbus.EventBus;

public class HomeUmoneyFragment extends Fragment {
    LinearLayout llMobile;
    UMoneyApi uMoneyApi;
    TabUmoneyActivity activity;

    public static HomeUmoneyFragment newInstance() {

        Bundle args = new Bundle();
        HomeUmoneyFragment fragment = new HomeUmoneyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sc_umoney_home, container, false);
        activity = (TabUmoneyActivity) getActivity();
        initView(view);
        loadData();
        initEvent();
        return view;
    }

    private void loadData() {
        uMoneyApi = new UMoneyApi(ApplicationController.self());
        uMoneyApi.getUmoneyBalance(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                Gson gson = new Gson();
                FieldMapResponse response = gson.fromJson(data, FieldMapResponse.class);
                if (response != null && response.getCode() == 200 && response.getData() != null && response.getData().getFieldMap().size() > 0) {
                    String currentBalance = "", currentSdt = "";
                    for (FieldMap model : response.getData().getFieldMap()) {
                        if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.BALANCE)) {
                            currentBalance = model.getValue();
                            continue;
                        }
                        if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.PHONE_NUMBER)) {
                            currentSdt = model.getValue();
                        }
                    }
                    EventBus.getDefault().post(new HomeUmoneyEvent(currentBalance, currentSdt));
                }
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                Toast.makeText(ApplicationController.self(), getString(R.string.e601_error_but_undefined), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        llMobile = view.findViewById(R.id.ll_mobile);
    }

    private void initEvent() {
        llMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getContext() instanceof TabUmoneyActivity) {
                    ((TabUmoneyActivity) getContext()).showFragment(SCConstants.UMONEY.TAB_MOBILE, null, false);
                }
            }
        });
    }
}

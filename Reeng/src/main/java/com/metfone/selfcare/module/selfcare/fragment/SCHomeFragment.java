package com.metfone.selfcare.module.selfcare.fragment;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.adapter.SCHomeAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.event.SCAccountEvent;
import com.metfone.selfcare.module.selfcare.model.SCBanner;
import com.metfone.selfcare.module.selfcare.model.SCBundle;
import com.metfone.selfcare.module.selfcare.model.SCSubListModel;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCBanner;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCSubContentModel;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.module.selfcare.widget.SCBottomSheetCSKHDialog;
import com.metfone.selfcare.module.selfcare.widget.SCBottomSheetDialog;
import com.metfone.selfcare.module.selfcare.widget.SCBottomSheetUtilitiesDialog;
import com.metfone.selfcare.ui.ChangeABColorHelper;
import com.metfone.selfcare.ui.view.HeaderFrameLayout;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;

public class SCHomeFragment extends SCBaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, AbsInterface.OnSCHomeListener {

    private ImageView btnMore;
    private AppCompatImageView btnQRScan;
    private FloatingActionButton fabSupport;

    private LoadingViewSC loadingView;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private SCHomeAdapter mAdapter;

    private ImageView ivBack;
//    HeaderFrameLayout headerTop;
    View statusBarHeight;
    TextView tvTitle;
    View headerController;

    private View bgHeader;

    public static SCHomeFragment newInstance() {
        SCHomeFragment fragment = new SCHomeFragment();
        return fragment;
    }

    @Override
    public String getName() {
        return "SCHomeFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_home;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        long time = System.currentTimeMillis();
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initView(view);
        loadData();
        Log.d(TAG, "[perform] onCreateView: " + (System.currentTimeMillis() - time));
        return view;
    }

    private void initView(View view) {
        layout_refresh = view.findViewById(R.id.swipe_refresh_layout);
        if (layout_refresh != null) {
//            layout_refresh.setColorSchemeColors(getResources().getColor(R.color.sc_primary));
            layout_refresh.setOnRefreshListener(this);
        }
        mRecyclerView = view.findViewById(R.id.recycler_view);
        bgHeader = view.findViewById(R.id.bgHeader);

        btnMore = view.findViewById(R.id.btnMore);
        btnQRScan = view.findViewById(R.id.btnQRScan);
        btnMore.setOnClickListener(this);
        btnQRScan.setOnClickListener(this);

        ivBack = view.findViewById(R.id.ivBack);
//        headerTop = view.findViewById(R.id.headerTop);
        statusBarHeight = view.findViewById(R.id.statusBarHeight);
        tvTitle = view.findViewById(R.id.tvTitleTop);
        headerController = view.findViewById(R.id.headerController);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        loadingView = view.findViewById(R.id.loading_view);
        loadingView.setLoadingErrorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login lai
                /*Intent intent = new Intent(getActivity(), SCAccountActivity.class);
                mActivity.startActivity(intent);*/
                mActivity.loginFromAnonymous();
            }
        });

        //Hide control toolbar
        btnMore.setVisibility(View.GONE);
        btnQRScan.setVisibility(View.GONE);

        mLayoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setInitialPrefetchItemCount(11);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(11);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new SCHomeAdapter(mActivity, this);
        mRecyclerView.setAdapter(mAdapter);

        fabSupport = view.findViewById(R.id.fabSupport);
        fabSupport.setOnClickListener(this);
        if (mApp.getReengAccountBusiness().isAnonymousLogin()) {
            fabSupport.setVisibility(View.GONE);
        } else {
            fabSupport.setVisibility(View.VISIBLE);
        }

        if (mActivity instanceof HomeActivity) {

        } else {
            ivBack.setVisibility(View.VISIBLE);
//            headerTop.setVisibility(View.GONE);
            statusBarHeight.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setPadding(0, 0, 0, 0);
            tvTitle.setAllCaps(false);
//            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
//            tvTitle.setTextColor(ContextCompat.getColor(mActivity, R.color.text_ab_title));
//            headerController.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.white));
//            ImageViewCompat.setImageTintList(btnQRScan, ColorStateList.valueOf(ContextCompat.getColor(mActivity, R.color.bg_ab_icon)));
//            ImageViewCompat.setImageTintList(btnMore, ColorStateList.valueOf(ContextCompat.getColor(mActivity, R.color.bg_ab_icon)));
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mActivity instanceof HomeActivity) {
//            ChangeABColorHelper.changeTabColor(bgHeader, TabHomeHelper.HomeTab.tab_selfcare, null,
//                    ((HomeActivity) mActivity).getCurrentTab(), ((HomeActivity) mActivity).getCurrentColorTabWap());
        }
    }

    private void loadData() {
        if (!isRefresh)
            loadingView.loadBegin();

        if (mApp.getReengAccountBusiness().isAnonymousLogin() || !mApp.getReengAccountBusiness().isValidAccount()) {
            loadingView.loadLogin(mActivity.getString(R.string.sc_no_login));
            return;
        }

        updateInfo();
//        getLoyalty();
        loadBanner();
    }

    private void updateInfo() {
        String fullname = mApp.getPref().getString(SCConstants.PREFERENCE.SC_FULL_NAME, "");
        String username = mApp.getPref().getString(SCConstants.PREFERENCE.SC_USER_NAME, "");
        String avatar = mApp.getPref().getString(SCConstants.PREFERENCE.SC_AVATAR, "");

        mAdapter.setUserInfo(username, fullname, avatar);
        getSub();
    }

//    private void getLoyalty() {
//        final WSSCRestful restful = new WSSCRestful(mActivity);
//        restful.getLoyalty(new Response.Listener<RestSCLoyaltyModel>() {
//            @Override
//            public void onResponse(RestSCLoyaltyModel result) {
////                super.onResponse(result);
//                if (result != null && result.getData() != null) {
//
//                    SCLoyaltyModel loyaltyModel = result.getData();
//                    mAdapter.setLoyaltyModel(loyaltyModel);
//                    mAdapter.notifyItemChanged(0);
//
//                    SCUtils.setCurrentLoyalty(loyaltyModel);
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//
//            }
//        });
//    }

    private void getSub() {
        final WSSCRestful restful = new WSSCRestful(mActivity);

        restful.getSubList(new Response.Listener<RestSCSubContentModel>() {
            @Override
            public void onResponse(RestSCSubContentModel result) {
//                super.onResponse(result);
                hideRefresh();
                if (result != null && result.getData() != null && result.getData().getContent() != null) {
                    if (result.getStatus() == 200 && result.getData() != null) {
                        loadingView.loadFinish();
                        ArrayList<SCSubListModel> data = result.getData().getContent();
                        ArrayList<SCSubListModel> subList = new ArrayList<>();
                        subList.clear();
                        for (int i = 0; i < data.size(); i++) {
                            if (data.get(i) != null && data.get(i).getOcsAccount() != null) {
                                subList.add(data.get(i));
                            }
                        }

                        SCSubListModel subMe = null;
                        ArrayList<SCSubListModel> subRemain = new ArrayList<>();
                        subRemain.clear();
                        for (int i = 0; i < subList.size(); i++) {
                            SCSubListModel scSubListModel = subList.get(i);
                            if (scSubListModel.getIsdn() != null) {
                                scSubListModel.setIsdn(SCUtils.formatPhoneNumber856(scSubListModel.getIsdn()));
                            }

                            if (scSubListModel.getIsdn() != null && scSubListModel.getIsdn().equals(mApp.getReengAccountBusiness().getJidNumber())) {
                                subMe = scSubListModel;
                            } else {
                                subRemain.add(scSubListModel);
                            }
                        }

                        ArrayList<SCSubListModel> temp = new ArrayList<>();
                        if (subMe != null && subRemain.size() > 0) {
                            temp.clear();
                            temp.add(subMe);
                            temp.addAll(subRemain);
                        } else {
                            temp.addAll(subList);
                        }

                        mAdapter.setSubList(temp);
                        mAdapter.notifyItemChanged(0);

                        //Show control toolbar
//                        btnMore.setVisibility(View.VISIBLE);
                        btnQRScan.setVisibility(View.VISIBLE);

                    } else if (result.getStatus() == 401 || result.getStatus() == 403) {
                        //Login lai
//                        mActivity.deactiveAccount();
//                        loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                    } else {
                        //Fail
                        loadingView.loadError();
                    }
                } else {
                    loadingView.loadError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                hideRefresh();
                if (volleyError != null && volleyError.networkResponse != null) {
                    int errorCode = volleyError.networkResponse.statusCode;
                    if (errorCode == 401 || errorCode == 403) {
                        //Login lai
//                        mActivity.deactiveAccount();
//                        loadingView.loadLogin(mActivity.getString(R.string.sc_token_expire));
                    } else {
                        //Fail
                        loadingView.loadError();
                    }
                } else {
                    //Fail
                    loadingView.loadError();
                }
            }
        });
    }

    private void loadBanner() {
        WSSCRestful restful = new WSSCRestful(mActivity);
        restful.getHomeBanner(new Response.Listener<RestSCBanner>() {
            @Override
            public void onResponse(RestSCBanner result) {
//                super.onResponse(result);

                if (result != null && result.getData() != null) {
                    ArrayList<SCBanner> data = result.getData();
                    mAdapter.setBannerList(data);
                    mAdapter.notifyItemChanged(2);
                } else {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

    private void doSelfCareDetail(int type, Object data) {
        if (mActivity != null) {
            Intent intent = new Intent(mActivity, TabSelfCareActivity.class);
            intent.putExtra(Constants.KEY_TYPE, type);
            intent.putExtra(Constants.KEY_DATA, (Serializable) data);
            mActivity.startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMore:
                SCBottomSheetDialog dialog = new SCBottomSheetDialog(mActivity);
                dialog.show();
                break;
            case R.id.btnQRScan:
                NavigateActivityHelper.navigateToQRScan(mActivity, Constants.QR_SOURCE.FROM_SC_RECHARGE_HOME);
                break;
            case R.id.fabSupport:
                SCBottomSheetCSKHDialog dialogCSKH = new SCBottomSheetCSKHDialog(mActivity);
                dialogCSKH.show();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(final SCAccountEvent event) {
        if (event != null) {
            if (event.getEvent() == SCAccountEvent.CHANGE_AVATAR) {
                updateInfo();
            } else if (event.getEvent() == SCAccountEvent.CHANGE_NAME) {
                updateInfo();
            } else if (event.getEvent() == SCAccountEvent.UPDATE_INFO) {
                updateInfo();
//                getLoyalty();
            }
            EventBus.getDefault().removeStickyEvent(event);
        }
    }

    @Override
    public void onDetach() {
//        userInfo = null;
//        App.getInstance().cancelPendingRequests(WSSCRestful.GET_HOME_INFO);
//        App.getInstance().cancelPendingRequests(WSSCRestful.GET_HOME_RECHARGE);
//        App.getInstance().cancelPendingRequests(WSSCRestful.GET_HOME_FEATURES);
//        App.getInstance().cancelPendingRequests(WSSCRestful.GET_HOME_PROMOTIONS);
//        App.getInstance().cancelPendingRequests(WSSCRestful.GET_HOME_BANNER);
//        App.getInstance().cancelPendingRequests(WSSCRestful.GET_SUB);
        super.onDetach();
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        loadData();
    }

    @Override
    public void onUserInfoClick() {
        NavigateActivityHelper.navigateToMyProfile(mActivity);
//        SCAccountActivity.startMyIDAccountFragment(mActivity, false);
    }

    @Override
    public void onBannerClick(SCBanner item) {
        if (item != null) {
//            if (TextUtils.isEmpty(item.getDeepLink())) {
//                doSelfCareDetail(SCConstants.SELF_CARE.TAB_NEWS_DETAIL, item);
//            } else {
//                DeepLinkHelper.getInstance().openSchemaLink(mActivity, item.getDeepLink());
//            }

            if (!TextUtils.isEmpty(item.getDeepLink()))
                DeepLinkHelper.getInstance().openSchemaLink(mActivity, item.getDeepLink());
        }
    }

    @Override
    public void onAccountClick() {
        doSelfCareDetail(SCConstants.SELF_CARE.TAB_ACCOUNT_DETAIL, null);
    }

    @Override
    public void onTopupClick() {
        doSelfCareDetail(SCConstants.SELF_CARE.TAB_RECHARGE, null);
    }

    @Override
    public void onPackageListClick() {
        doSelfCareDetail(SCConstants.SELF_CARE.TAB_RECOMMENT_PACKAGE, new SCBundle(SCConstants.SCREEN_TYPE.TYPE_PACKAGE));
    }

    @Override
    public void onServicesClick() {
        doSelfCareDetail(SCConstants.SELF_CARE.TAB_RECOMMENT_PACKAGE, new SCBundle(SCConstants.SCREEN_TYPE.TYPE_SERVICES));
    }

    @Override
    public void onUtilitiesClick() {
//        doSelfCareDetail(SCConstants.SELF_CARE.TAB_PACKAGE_LIST, new SCBundle(SCConstants.SCREEN_TYPE.TYPE_UTILITIES));
        SCBottomSheetUtilitiesDialog dialog = new SCBottomSheetUtilitiesDialog(mActivity);
        dialog.show();
    }

    @Override
    public void onStoreClick() {
        doSelfCareDetail(SCConstants.SELF_CARE.TAB_STORES, null);
    }

    @Override
    public void onHistoryClick() {
        doSelfCareDetail(SCConstants.SELF_CARE.TAB_ACCOUNT_DETAIL, new SCBundle(1));
    }

    @Override
    public void onLoyaltyClick() {
//        ToastUtils.makeText(mActivity, getString(R.string.e666_not_support_function));

        doSelfCareDetail(SCConstants.SELF_CARE.TAB_LOYALTY_HOME, null);
    }
}

package com.metfone.selfcare.module.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/20/17.
 */

public class CategoryResponse extends ErrorResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<CategoryModel> data = new ArrayList<>();

    public ArrayList<CategoryModel> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(ArrayList<CategoryModel> data) {
        this.data = data;
    }
}

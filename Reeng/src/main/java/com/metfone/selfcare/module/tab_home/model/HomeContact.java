package com.metfone.selfcare.module.tab_home.model;

import com.metfone.selfcare.database.model.ThreadMessage;

public class HomeContact {
    boolean viewAll;
    ThreadMessage threadMessage;
    long lastTime;

    public HomeContact(ThreadMessage threadMessage) {
        this.threadMessage = threadMessage;
        lastTime = System.currentTimeMillis();
    }

    public HomeContact() {
        lastTime = System.currentTimeMillis();
    }

    public boolean isViewAll() {
        return viewAll;
    }

    public void setViewAll(boolean viewAll) {
        this.viewAll = viewAll;
    }

    public ThreadMessage getThreadMessage() {
        return threadMessage;
    }

    public void setThreadMessage(ThreadMessage threadMessage) {
        this.threadMessage = threadMessage;
    }

    public long getLastTime() {
        return lastTime;
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }
}

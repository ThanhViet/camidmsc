package com.metfone.selfcare.module.home_kh.fragment.history.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.model.PointTransferHistory;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HistoryPointItemAdapter extends RecyclerView.Adapter<HistoryPointItemAdapter.Holder> {

    private static final int TYPE_LOADING = 100;

    private List<IHistoryPointLog> data = new ArrayList<>();
    private boolean isLoading;
    private int type;
    private String strStart = "";

    @ColorInt
    private int pointColorReceived;
    private SimpleDateFormat simpleDateFormat;

    {
        simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
    }

    public HistoryPointItemAdapter(int type) {
        this.type = type;
        strStart = type == PointType.ACTIVE.id ? "+" : "-";
        pointColorReceived = type == PointType.ACTIVE.id ? ResourceUtils.getColor(R.color.green) :
                ResourceUtils.getColor(R.color.orange);
    }

    public void setData(List<? extends IHistoryPointLog> data) {
        this.data = data != null ? new ArrayList<>(data) : new ArrayList<>();
    }

    public void addData(List<? extends IHistoryPointLog> data) {
        if (data != null) {
            this.data.addAll(data);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return isLoading && position == data.size() ? TYPE_LOADING : super.getItemViewType(position);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewType == TYPE_LOADING ? new LoadingHolder(parent) : new ItemHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.onBind(position);
    }

    public void setLoading(boolean isLoading) {
        if (isLoading != this.isLoading) {
            int position = getItemCount();
            this.isLoading = isLoading;
            if (isLoading) {
                notifyItemInserted(position + 1);
            } else {
                notifyItemRemoved(position);
            }
        }
    }

    @Override
    public int getItemCount() {
        return isLoading ? data.size() + 1 : data.size();
    }

    abstract static class Holder extends RecyclerView.ViewHolder {

        public Holder(@NonNull View itemView) {
            super(itemView);
        }

        public abstract void onBind(int position);

        protected static View inflate(@NonNull ViewGroup parent, @LayoutRes int layoutRes) {
            return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        }

    }

    public class ItemHolder extends Holder {

        private TextView txtName, txtDate, txtAmount;

        public ItemHolder(@NonNull ViewGroup parent) {
            super(inflate(parent, R.layout.item_history_point));
            txtName = itemView.findViewById(R.id.tv_name);
            txtDate = itemView.findViewById(R.id.tv_date);
            txtAmount = itemView.findViewById(R.id.tv_points);
        }

        public void onBind(int position) {
            IHistoryPointLog item = data.get(position);
            if (item instanceof PointTransferHistory) {
                txtName.setText(((PointTransferHistory) item).getTransferTypeName());
            } else {
                txtName.setText(item.getName());
            }
            txtAmount.setText(item.valueShow(strStart));
            txtAmount.setTextColor(pointColorReceived);
            txtDate.setText(item.getCreatedAt());
        }
    }

    private static final class LoadingHolder extends Holder {

        public LoadingHolder(@NonNull ViewGroup parent) {
            super(inflate(parent, R.layout.item_loading_kh));
        }

        @Override
        public void onBind(int position) {

        }
    }
}

package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Exchange implements Serializable {
    @SerializedName("returnCode")
    @Expose
    private String returnCode;
    @SerializedName("redeemCode")
    @Expose
    private String redeemCode;
    @SerializedName("giftQrCode")
    @Expose
    private String giftQrCode;
    @SerializedName("isCoupon")
    @Expose
    private int isCoupon;
    @SerializedName("linkCoupon")
    @Expose
    private String linkCoupon;
    @SerializedName("imageCoupon")
    @Expose
    private String imageCoupon;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getRedeemCode() {
        return redeemCode;
    }

    public void setRedeemCode(String redeemCode) {
        this.redeemCode = redeemCode;
    }

    public String getGiftQrCode() {
        return giftQrCode;
    }

    public void setGiftQrCode(String giftQrCode) {
        this.giftQrCode = giftQrCode;
    }

    public int getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(int isCoupon) {
        this.isCoupon = isCoupon;
    }

    public String getLinkCoupon() {
        return linkCoupon;
    }

    public void setLinkCoupon(String linkCoupon) {
        this.linkCoupon = linkCoupon;
    }

    public String getImageCoupon() {
        return imageCoupon;
    }

    public void setImageCoupon(String imageCoupon) {
        this.imageCoupon = imageCoupon;
    }
}

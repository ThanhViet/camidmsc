package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.holder.MediaNewHolder;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

public class SongAdapter extends SectionListDataAdapter<AllModel> {

    SongAdapter(Context context, List<AllModel> itemsList, AbsInterface.OnItemListener onClick) {
        super(context, itemsList, onClick);
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public MediaNewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_media_song_hot, null);
        return new MediaNewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        final AllModel item = getItem(position);
        MediaNewHolder itemHolder = (MediaNewHolder) holder;
        if (item != null) {
            itemHolder.tvName.setText(item.getName());
            if (TextUtils.isEmpty(item.getSinger()))
                itemHolder.tvSinger.setVisibility(View.GONE);
            else {
                itemHolder.tvSinger.setVisibility(View.VISIBLE);
                itemHolder.tvSinger.setText(item.getSinger());
                if (item.isDocQuyen()) {
                    itemHolder.tvSinger.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.ic_docquyen), null, null, null);
                } else {
                    itemHolder.tvSinger.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }
            itemHolder.tvListenNo.setVisibility(View.GONE);
            itemHolder.tvPosition.setVisibility(View.GONE);
            itemHolder.btnOption.setVisibility(View.VISIBLE);
            ImageBusiness.setSong(itemHolder.image, item.getImage(), position, ApplicationController.self().getRound());
            itemHolder.itemView.setOnClickListener(v -> {
                if (onClick != null) {
                    item.setSource(MediaLogModel.SRC_HOT_HOME);
                    onClick.onItemClick(item);
                }
            });
            itemHolder.btnOption.setOnClickListener(v -> {
                if (onClick != null)
                    onClick.onItemClickOption(item);
            });
        }
    }
}
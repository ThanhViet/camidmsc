package com.metfone.selfcare.module.tiin.hometiin.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

public class SpecialTiinAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private final int SECTIONE_HORIZOTAL = 2;
    private final int SECTIONE_EMPTY = 0;
    private HomeTiinModel model;
    private Context mContext;
    private TiinListener.OnHomeTiinItemListener listener;

    public SpecialTiinAdapter(HomeTiinModel model, Context mContext, TiinListener.OnHomeTiinItemListener listener) {
        this.model = model;
        this.mContext = mContext;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case SECTIONE_HORIZOTAL:
                return new SpecialTiinHolder(LayoutInflater.from(mContext).inflate(R.layout.holder_special_tiin, parent,false), TabHomeUtils.getWidthSpecialTiin());
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final TiinModel tiinModel = getItem(position);
        if (holder.getView(R.id.iv_cover) != null) {
//            ImageLoader.setNewsImage(mContext, tiinModel.getImage(), (ImageView) holder.getView(R.id.ivImage));
            ImageBusiness.setImageNew(tiinModel.getImage(), (ImageView) holder.getView(R.id.iv_cover));
        }
        if (holder.getView(R.id.tv_title) != null) {
            TiinUtilities.setText(holder.getView(R.id.tv_title), tiinModel.getTitle(), tiinModel.getTypeIcon());
        }
        if (holder.getView(R.id.tv_category) != null) {
            holder.setText(R.id.tv_category, tiinModel.getCategory());
            holder.setOnClickListener(R.id.tv_category, new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    listener.onItemClickHeader(HomeTiinAdapter.SECTION_CATEGORY_NEWS,tiinModel.getPid(),tiinModel.getCategory());
                }
            });
        }
        if(holder.getView(R.id.tv_desc) != null){
            holder.setText(R.id.tv_desc,tiinModel.getShapo());
        }
        if(holder.getView(R.id.tv_datetime) != null){
            if(!TextUtils.isEmpty(tiinModel.getHasTagName())) {
                holder.setVisible(R.id.tv_datetime, true);
                //todo ten nguoi noi tieng
                holder.setText(R.id.tv_datetime, tiinModel.getHasTagName());
                holder.setOnClickListener(R.id.tv_datetime, new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        if(listener != null){
                            listener.onItemClickHashTag(tiinModel);
                        }
                    }
                });
            }
        }
        if(holder.getView(R.id.button_option) != null){
            holder.setOnClickListener(R.id.button_option,new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    listener.onItemClickMore(tiinModel);
                }
            });
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(tiinModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null) {
            return SECTIONE_HORIZOTAL;
        }
        return SECTIONE_EMPTY;
    }

    public TiinModel getItem(int position) {
        try {
            return model.getData().get(position);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
        return null;
    }

}

package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

/**
 * ViewHolder for History charge Vas: Code, Money, Date
 */
public class VasChargeViewHolder extends RecyclerView.ViewHolder {
    public AppCompatTextView mCode;
    public AppCompatTextView mDate;
    public AppCompatTextView mMoney;

    public VasChargeViewHolder(@NonNull View itemView) {
        super(itemView);
        mCode = itemView.findViewById(R.id.phone_number_sms_code_vas_charge);
        mDate = itemView.findViewById(R.id.date_sms_vas_charge);
        mMoney = itemView.findViewById(R.id.money_sms_vas_charge);
    }
}

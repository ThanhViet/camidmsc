package com.metfone.selfcare.module.home_kh.tab.adapter;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;
import com.metfone.selfcare.module.home_kh.tab.adapter.banner.KhHomeGameBannerHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.banner.KhHomeLuckyGameBannerHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.banner.KhSlideBannerHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.customtopic.BoxContentHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.gift.KhHomeGiftViewHolder;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.util.Log;

public class KhHomeAdapter extends BaseAdapter<BaseAdapter.ViewHolder, IHomeModelType> {

    private TabHomeKhListener.OnAdapterClick listener;

    public void setListener(TabHomeKhListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    private RecyclerView.RecycledViewPool recycledPoolHeader;
    private RecyclerView.RecycledViewPool recycledPoolVertical;
    private RecyclerView.RecycledViewPool recycledPoolHorizontal;

    public KhHomeAdapter(Activity activity) {
        super(activity);
        recycledPoolHeader = new RecyclerView.RecycledViewPool();
        recycledPoolVertical = new RecyclerView.RecycledViewPool();
        recycledPoolHorizontal = new RecyclerView.RecycledViewPool();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case IHomeModelType.BANNER:
                return new KhSlideBannerHolder(layoutInflater.inflate(R.layout.holder_box_banner_tab_home_kh, parent, false), activity, listener, true);
            case IHomeModelType.POPULAR_LIVE_CHANNEL:
            case IHomeModelType.CUSTOM_TOPIC:
                BoxContentHolder holder = new BoxContentHolder(layoutInflater.inflate(R.layout.cinema_simple_movie, parent, false), activity, listener, viewType);
                if (holder.getRecycler() != null) {
                    holder.getRecycler().setRecycledViewPool(recycledPoolVertical);
                }
                return holder;
            case IHomeModelType.MP_SERVICE:
            case IHomeModelType.REWARD_CATEGORY:
            case IHomeModelType.REWARD_TOPIC_1:
            case IHomeModelType.REWARD_TOPIC_2:
                BoxContentHolder holder2 = new BoxContentHolder(layoutInflater.inflate(R.layout.item_home_slide_2, parent, false), activity, listener, viewType);
                if (holder2.getRecycler() != null) {
                    holder2.getRecycler().setRecycledViewPool(recycledPoolVertical);
                }
                return holder2;
            case IHomeModelType.GAME_ITEM:
                KhHomeGameBannerHolder gameBannerHolder = new KhHomeGameBannerHolder(layoutInflater.inflate(R.layout.item_home_game_banner, parent, false), listener);
                return  gameBannerHolder;
            case IHomeModelType.GIFT:
                return new KhHomeGiftViewHolder(layoutInflater.inflate(R.layout.layout_gift_tab_home,parent, false), activity, listener);
            case IHomeModelType.LUCKY_GAME_ITEM:
                KhHomeLuckyGameBannerHolder luckyGameBannerHolder = new KhHomeLuckyGameBannerHolder(layoutInflater.inflate(R.layout.item_home_lucky_game, parent, false), listener);
                return luckyGameBannerHolder;

        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IHomeModelType item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getItemType();
    }
}

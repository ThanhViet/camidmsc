package com.metfone.selfcare.module.metfoneplus.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.home_kh.tab.adapter.events.OnClickHomeReward;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardSearchItem;
import com.metfone.selfcare.module.metfoneplus.model.MPHomeServiceItem;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

import butterknife.BindView;

public class MPHomeServiceListHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tvTitleService)
    @Nullable
    TextView tvName;

    @BindView(R.id.ivService)
    @Nullable
    RoundedImageView icon;

    @BindView(R.id.root)
    @Nullable
    ViewGroup viewRoot;

    private OnClickHomeReward listener;
    private Activity activity;
    public int topUp = R.string.m_p_more_service_top_up;
    public int buyServices = R.string.m_p_more_service_buy_services;
    public int support = R.string.m_p_more_service_support;
    public int iShare = R.string.m_p_more_service_i_share;
    public int buyPhoneNumber = R.string.m_p_more_service_buy_phone_number_v2;
    public int exchange_damaged_card = R.string.m_p_more_exchange_damaged_card;


    public MPHomeServiceListHolder(View view, Activity activity, OnClickHomeReward listener) {
        super(view);
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    public void bindData(Object item, int position) {
        MPHomeServiceItem serviceItem = (MPHomeServiceItem) item;


        switch (serviceItem.id) {
            case 1: {
                int drawable = R.drawable.group_2323589;
                Glide.with(activity)
                        .load(drawable)
                        .error(R.drawable.bg_kh_home_reward_cate)
                        .placeholder(R.drawable.bg_kh_home_reward_cate)
                        .into(icon);
                tvName.setText(buyServices);
            }
            break;
            case 0: {
                int drawable = R.drawable.group_2323588;
                Glide.with(activity)
                        .load(drawable)
                        .error(R.drawable.bg_kh_home_reward_cate)
                        .placeholder(R.drawable.bg_kh_home_reward_cate)
                        .into(icon);
                tvName.setText(topUp);
            }
            break;
            case 2: {
                int drawable = R.drawable.group_2323590;
                Glide.with(activity)
                        .load(drawable)
                        .error(R.drawable.bg_kh_home_reward_cate)
                        .placeholder(R.drawable.bg_kh_home_reward_cate)
                        .into(icon);
                tvName.setText(support);
            }
            break;
            case 3: {
                int drawable = R.drawable.group_2323591;
                Glide.with(activity)
                        .load(drawable)
                        .error(R.drawable.bg_kh_home_reward_cate)
                        .placeholder(R.drawable.bg_kh_home_reward_cate)
                        .into(icon);
                tvName.setText(iShare);
            }
            break;
            case 4: {
                int drawable = R.drawable.group_2323587;
                Glide.with(activity)
                        .load(drawable)
                        .error(R.drawable.bg_kh_home_reward_cate)
                        .placeholder(R.drawable.bg_kh_home_reward_cate)
                        .into(icon);
                tvName.setText(buyPhoneNumber);
            }
            break;
            case 5: {
                int drawable = R.drawable.group_2323607;
                Glide.with(activity)
                        .load(drawable)
                        .error(R.drawable.bg_kh_home_reward_cate)
                        .placeholder(R.drawable.bg_kh_home_reward_cate)
                        .into(icon);
                tvName.setText(exchange_damaged_card);
            }
            break;

        }

        viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) {
                    listener.onClickRewardCategoryItem(serviceItem, getAdapterPosition());
                }
            }
        });
    }
}

package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.CallingCharge;
import com.metfone.selfcare.model.camid.DataCharge;
import com.metfone.selfcare.model.camid.DetailSmsCallingDataCharge;
import com.metfone.selfcare.model.camid.DetailType;
import com.metfone.selfcare.model.camid.SmsCallingDataCharge;
import com.metfone.selfcare.model.camid.SmsCharge;
import com.metfone.selfcare.model.camid.VasCharge;
import com.metfone.selfcare.module.metfoneplus.holder.CallingChargeViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.DataChargeChildrenViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.DataChargeParentViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.SmsChargeViewHolder;
import com.metfone.selfcare.module.metfoneplus.holder.VasChargeViewHolder;

import java.util.List;

public class DetailSmsCallingDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<DetailSmsCallingDataCharge> mDetailSmsCallingDataList;

    public DetailSmsCallingDataAdapter(Context context, List<DetailSmsCallingDataCharge> detailTypes) {
        this.mContext = context;
        this.mDetailSmsCallingDataList = detailTypes;
    }

    private void setList(List<DetailSmsCallingDataCharge> detailTypes) {
        this.mDetailSmsCallingDataList = detailTypes;
    }

    public void replaceData(List<DetailSmsCallingDataCharge> objectList) {
        setList(objectList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == DetailType.CHARGE_TYPE_GROUP_SCDV_CHARGE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_4, parent, false);
            return new DataChargeParentViewHolder(view);
        } else if (viewType == DetailType.CHARGE_TYPE_GROUP_SMS_CHARGE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_sms_vas, parent, false);
            return new SmsChargeViewHolder(view);
        } else if (viewType == DetailType.CHARGE_TYPE_GROUP_CALLING_CHARGE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_calling, parent, false);
            return new CallingChargeViewHolder(view);
        } else if (viewType == DetailType.CHARGE_TYPE_GROUP_DATA_CHARGE){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_5, parent, false);
            return new DataChargeChildrenViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_charge_sms_vas, parent, false);
            return new VasChargeViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == DetailType.CHARGE_TYPE_GROUP_SCDV_CHARGE) {
            SmsCallingDataCharge scd = (SmsCallingDataCharge) mDetailSmsCallingDataList.get(position);
            DataChargeParentViewHolder dcpHolder = (DataChargeParentViewHolder) holder;
            dcpHolder.mTotal.setText(String.format(mContext.getString(R.string.m_p_item_detail_account_value),
                    String.valueOf(scd.getMoney())));
            if (scd.isData()) {
                dcpHolder.mQuantity.setText(String.valueOf(scd.getData()));
                dcpHolder.mContentValue.setText(R.string.m_p_detail_mb);
            } else {
                dcpHolder.mQuantity.setText(String.valueOf(scd.getQuantity()));
                dcpHolder.mContentValue.setText(R.string.m_p_detail_quantity);
            }
        } else if (holder.getItemViewType() == DetailType.CHARGE_TYPE_GROUP_SMS_CHARGE) {
            SmsCharge s = (SmsCharge) mDetailSmsCallingDataList.get(position);
            SmsChargeViewHolder sHolder = (SmsChargeViewHolder) holder;
            sHolder.mDate.setText(String.format(mContext.getString(R.string.m_p_item_detail_sms_charge_date),
                    s.getTime(), s.getDate()));
            sHolder.mPhoneNumber.setText(s.getPhoneNumber());
            sHolder.mMoney.setText(String.format(mContext.getString(R.string.m_p_detail_data_charge_money), String.valueOf(s.getMoney())));
        } else if (holder.getItemViewType() == DetailType.CHARGE_TYPE_GROUP_CALLING_CHARGE) {
            CallingCharge c = (CallingCharge) mDetailSmsCallingDataList.get(position);
            CallingChargeViewHolder cHolder = (CallingChargeViewHolder) holder;
            cHolder.mDate.setText(String.format(mContext.getString(R.string.m_p_item_detail_sms_charge_date),
                    c.getTime(), c.getDate()));
            cHolder.mPhoneNumber.setText(c.getPhoneNumber());
            cHolder.mMoney.setText(String.format(mContext.getString(R.string.m_p_detail_data_charge_money), String.valueOf(c.getMoney())));
            cHolder.mCallingTime.setText(c.getCallingTime());
        } else if (holder.getItemViewType() == DetailType.CHARGE_TYPE_GROUP_DATA_CHARGE){
            DataCharge d = (DataCharge) mDetailSmsCallingDataList.get(position);
            DataChargeChildrenViewHolder dccHolder = (DataChargeChildrenViewHolder) holder;
            dccHolder.mDate.setText(d.getDate());
            dccHolder.mMoney.setText(String.format(mContext.getString(R.string.m_p_detail_data_charge_money), String.valueOf(d.getMoney())));
            dccHolder.mData.setText(String.format(mContext.getString(R.string.m_p_detail_data_charge_data), String.valueOf(d.getTotal()), d.getUnit()));
        } else {
            VasCharge v = (VasCharge) mDetailSmsCallingDataList.get(position);
            VasChargeViewHolder vcHolder = (VasChargeViewHolder) holder;
            vcHolder.mCode.setText(v.getCode());
            vcHolder.mMoney.setText(String.format(mContext.getString(R.string.m_p_detail_data_charge_money), String.valueOf(v.getMoney())));
            vcHolder.mDate.setText(v.getStartTime());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDetailSmsCallingDataList != null) {
            return mDetailSmsCallingDataList.get(position).getDetailCharge();
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        if (mDetailSmsCallingDataList == null) {
            return 0;
        }
        return mDetailSmsCallingDataList.size();
    }
}

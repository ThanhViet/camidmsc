package com.metfone.selfcare.module.netnews.CategoryNews.presenter;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.module.netnews.CategoryNews.fragment.CategoryNewsFragment;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.netnews.request.NewsRequest;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Log;

public class CategoryNewsPresenter extends BasePresenter implements ICategoryNewsPresenter {

    public static final String TAG = CategoryNewsPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;

    public CategoryNewsPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadData(int cateId, int page, long unixTime) {
        mNewsApi.getNewsByCategory(new NewsRequest(cateId, page, CommonUtils.NUM_SIZE, unixTime), callback);
    }

    HttpCallBack callback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            if (!isViewAttached() || !(getMvpView() instanceof CategoryNewsFragment)) {
                return;
            }
            Gson gson = new Gson();
            NewsResponse childNewsResponse = gson.fromJson(data, NewsResponse.class);

            ((CategoryNewsFragment) getMvpView()).bindData(childNewsResponse);
            ((CategoryNewsFragment) getMvpView()).loadDataSuccess(true);
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof CategoryNewsFragment)) {
                return;
            }
            ((CategoryNewsFragment) getMvpView()).loadDataSuccess(false);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
}

package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGtAllPartnerGiftSearchRequest extends BaseRequest<WsGtAllPartnerGiftSearchRequest.Request> {
    public class Request {
        @SerializedName("language")
        private String language;

        public String partnerName;
        public String rankId;
        public String isdn;
        public String giftPoint = GiftPoint.ALL.value;
        public String cityId =  "";

        public double latitude =  11.5503929;
        public String limit =  "1000";
        public String page =  "1";
        public int price =  1;
        public String isType = "2";
        public double longitude = 104.9044962;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

package com.metfone.selfcare.module.home_kh.tab.model;

import java.util.ArrayList;
import java.util.List;

public class KhHomeBanner implements IHomeModelType {
    public List<KhHomeMovieItem> banners = new ArrayList<>();
    public List<KhHomeMovieItem> recommend = new ArrayList<>();
    public List<KhHomeMovieItem> recentlyAdded = new ArrayList<>();
    public List<KhHomeMovieItem> avaiable = new ArrayList<>();
    public List<KhHomeMovieItem> awardWining = new ArrayList<>();


    public KhHomeBanner() {
    }

    public KhHomeBanner(List<KhHomeMovieItem> banners) {
        this.banners = banners;
    }

    @Override
    public int getItemType() {
        return IHomeModelType.BANNER;
    }

}

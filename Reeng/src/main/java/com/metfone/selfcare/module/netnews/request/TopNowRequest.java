package com.metfone.selfcare.module.netnews.request;

/**
 * Created by HaiKE on 8/19/17.
 */

public class TopNowRequest {

    int num;
    int page;
    String categoryList;

    public TopNowRequest(String categoryList, int page, int num)
    {
        this.page = page;
        this.num = num;
        this.categoryList = categoryList;
    }

    public String getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(String categoryList) {
        this.categoryList = categoryList;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}

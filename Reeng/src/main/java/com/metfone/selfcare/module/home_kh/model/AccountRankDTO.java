package com.metfone.selfcare.module.home_kh.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class AccountRankDTO {
    @SerializedName("accountRankId")
    public long accountRankId;
    @SerializedName("vtAccId")
    public long vtAccId;
    @SerializedName("rankId")
    public int rankId;
    @SerializedName("rankName")
    public String rankName;
    @SerializedName("startDate")
    public String startDate;
    @SerializedName("endDate")
    public String endDate;
    @SerializedName("pointValue")
    public String pointValue;
    @SerializedName("pointExpireDate")
    public String pointExpireDate;

    public AccountRankDTO() {
    }
}

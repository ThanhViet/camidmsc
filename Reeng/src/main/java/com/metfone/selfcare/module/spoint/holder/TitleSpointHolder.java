package com.metfone.selfcare.module.spoint.holder;

import android.app.Activity;
import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import androidx.appcompat.widget.AppCompatTextView;

public class TitleSpointHolder extends BaseViewHolder {
    private AppCompatTextView tvTitle;
    private Activity activity;
    public TitleSpointHolder(View view, Activity activity) {
        super(view);
        this.activity = activity;
        tvTitle = view.findViewById(R.id.tv_title);
    }
    public void bindData(AccumulatePointItem model){
        if(model != null){
            tvTitle.setText(model.getTitle());
        }
    }

}

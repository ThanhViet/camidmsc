/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2018/12/19
 *
 */

package com.metfone.selfcare.module.keeng.fragment.category;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.adapter.detail.AlbumDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.AlbumDetailGridItemDecoration;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.keeng.widget.LoadingView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AlbumDetailFragment extends BaseFragment implements BaseListener.AlbumDetailListener,
        SwipeRefreshLayout.OnRefreshListener {
    private final int MAX_NUMBER_TO_RETRY = KeengApi.MAX_NUMBER_TO_RETRY;
    private int currentPage = 1;
    private int countError = 0;
    private boolean canLoadMore = false;
    private boolean isRefresh = true;
    private boolean isLoading = false;
    private SwipeRefreshLayout refreshView;
    private LoadingView loadingView;
    private RecyclerView recyclerView;
    private AlbumDetailAdapter adapter;
    private List<AllModel> data = new ArrayList<>();
    private AllModel mediaInfo = null;
    private TextView tvTitleToolbar, tvCount;
    private Toolbar toolbar;
    private LinearLayoutManager layoutManager;
    private ListenerUtils listenerUtils;

    public static AlbumDetailFragment newInstance() {
        Bundle args = new Bundle();
        AlbumDetailFragment fragment = new AlbumDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "AlbumDetailFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_album_detail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        refreshView = view.findViewById(R.id.refresh_view);
        loadingView = view.findViewById(R.id.loading_view);
        recyclerView = view.findViewById(R.id.recycler_view);
        tvTitleToolbar = view.findViewById(R.id.tv_title_toolbar);
        tvCount = view.findViewById(R.id.tv_count);
        toolbar = view.findViewById(R.id.toolbar);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            assert getArguments() != null;
            mediaInfo = (AllModel) getArguments().getSerializable(Constants.KEY_DATA);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        if (mediaInfo == null) {
            onBackPressed();
            return;
        }
        if (refreshView != null) {
            refreshView.setEnabled(false);
        }
        initListener();
        adapter = new AlbumDetailAdapter(mActivity, getData());
        adapter.setListener(this);
        new Handler().postDelayed(() -> {
            setupMediaInfo();
            setupCountInfo();
            if (getData().isEmpty())
                doLoadData(true);
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    private void initListener() {
        if (toolbar != null) {
            toolbar.setNavigationIcon(mActivity.getResources().getDrawable(R.drawable.ic_v5_back));
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void setupMediaInfo() {
        if (mediaInfo == null || mActivity == null)
            return;
        tvTitleToolbar.setText(mediaInfo.getName());
    }

    private void setupCountInfo() {
        int size = getData().size();
        if (size == 0) {
            tvCount.setVisibility(View.GONE);
            tvCount.setText("");
        } else {
            if (mediaInfo.isVideoList()) {
                tvCount.setVisibility(View.VISIBLE);
                if (size == 1)
                    tvCount.setText(getString(R.string.total_video, size));
                else
                    tvCount.setText(getString(R.string.total_videos, size));
            } else {
                tvCount.setVisibility(View.VISIBLE);
                if (size == 1)
                    tvCount.setText(getString(R.string.music_total_song, size));
                else
                    tvCount.setText(getString(R.string.music_total_songs, size));
            }
        }
    }

    @SuppressWarnings("SameParameterValue")
    private void setupGridRecycler(int spanCount, int resIdSpacing, boolean includeEdge) {
        if (recyclerView != null) {
            if (recyclerView.getItemDecorationCount() <= 0) {
                layoutManager = new CustomGridLayoutManager(mActivity, spanCount);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(new AlbumDetailGridItemDecoration(spanCount, mActivity
                        .getResources()
                        .getDimensionPixelOffset(resIdSpacing), includeEdge));
            }
            recyclerView.setAdapter(adapter);
        }
    }

    private void setupRecycler() {
        if (recyclerView != null) {
            if (recyclerView.getItemDecorationCount() <= 0) {
                LinearLayoutManager layoutManager = new CustomLinearLayoutManager(mActivity);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(new DividerItemDecoration(mActivity, R.drawable
                        .divider_default));
            }
            recyclerView.setAdapter(adapter);
        }
    }

    private List<AllModel> getData() {
        if (data == null) data = new ArrayList<>();
        return data;
    }

    private void clearData() {
        if (data == null) data = new ArrayList<>();
        else data.clear();
    }

    private void checkLoadMore(List<AllModel> result) {
        canLoadMore = result != null && !result.isEmpty();
    }

    private void refreshed() {
        isLoading = false;
        countError = 0;
        if (isRefresh) {
            isRefresh = false;
            currentPage = 1;
            clearData();
        }
    }

    private void loadingNoInfo() {
        if (refreshView != null && refreshView.isRefreshing()) {
            refreshView.setRefreshing(false);
        }
        if (loadingView != null) {
            loadingView.loadEmpty(mActivity.getString(R.string.album_not_exist));
        }
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void loadingFinish() {
        if (refreshView != null && refreshView.isRefreshing()) {
            refreshView.setRefreshing(false);
        }
        if (getData().isEmpty()) {
            if (loadingView != null) {
                loadingView.loadEmpty();
            }
            if (recyclerView != null) {
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            if (loadingView != null) {
                loadingView.loadFinish();
            }
            if (recyclerView != null) {
                recyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    private void loadError() {
        refreshed();
        if (getData().isEmpty()) {
            if (refreshView != null && refreshView.isRefreshing()) {
                refreshView.setRefreshing(false);
            }
            if (loadingView != null) {
                loadingView.loadError();
                loadingView.setLoadingErrorListener(view -> doLoadData(true));
            }
            if (recyclerView != null) {
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            loadingFinish();
        }
    }

    private void doLoadData(boolean showLoading) {
        if (!isLoading) {
            if (showLoading && loadingView != null) {
                loadingView.loadBegin();
            }
            isLoading = true;
            if (isRefresh)
                currentPage = 1;
            if (mediaInfo != null && (mediaInfo.getId() > 0 || !TextUtils.isEmpty(mediaInfo.getIdentify()))) {
                new KeengApi().getAlbumInfo(mediaInfo.getId(), mediaInfo.getIdentify(), mediaInfo.getType()
                        , response -> {
                            if (response != null && response.getData() != null) {
                                boolean isFavorite = mediaInfo.isFavorite();
                                mediaInfo = response.getData();
                                mediaInfo.setFavorite(isFavorite);
                                if (mediaInfo.isVideoList()) {
                                    setupGridRecycler(2, R.dimen.padding_16, true);
                                } else {
                                    setupRecycler();
                                }
                                setupMediaInfo();
                                doAddResult(mediaInfo.getMediaList());
                                setupCountInfo();
                                mediaInfo.setMediaList(null);
                            } else {
                                loadingNoInfo();
                            }
                        }, error -> {
                            Log.e(TAG, error);
                            countError++;
                            if (countError < MAX_NUMBER_TO_RETRY) {
                                isLoading = false;
                                doLoadData(false);
                                return;
                            }
                            loadError();
                        });
            } else {
                loadingNoInfo();
            }
        }
    }

    private void doAddResult(List<AllModel> result) {
        checkLoadMore(result);
        refreshed();
        if (result != null && !result.isEmpty()) {
            if (mediaInfo.isVideoList()) {
                ConvertHelper.convertData(result, MediaLogModel.SRC_ALBUM, Constants.TYPE_VIDEO);
            } else {
                ConvertHelper.convertData(result, MediaLogModel.SRC_ALBUM, Constants.TYPE_SONG);
            }
            getData().addAll(result);
            currentPage++;
        }
        if (adapter != null) adapter.notifyDataSetChanged();
        loadingFinish();
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        doLoadData(false);
    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            doLoadData(false);
        }
    }

    @Override
    public void onClickMedia(View view, int position) {
        if (mActivity == null || mActivity.isFinishing() || mediaInfo == null)
            return;
//        ViewUtil.lockClickView(view);
        onClickPlay(false, position);
    }

    @Override
    public void onClickOptionMedia(View view, AllModel item) {
        if (mActivity == null || mActivity.isFinishing() || item == null)
            return;
        mActivity.showPopupMore(item);
    }

    @SuppressWarnings("SameParameterValue")
    private void onClickPlay(boolean isShuffle, int position) {
        if (mActivity == null || mActivity.isFinishing() || mediaInfo == null || getData().isEmpty())
            return;
        if (mediaInfo.isVideoList()) {
            mActivity.setMediaToPlayVideo(getData().get(position));
        } else {
            PlayingList playingList = new PlayingList(getData(), mediaInfo.getType(), MediaLogModel.SRC_ALBUM);
            playingList.setName(mediaInfo.getName());
            playingList.setId(mediaInfo.getId());
            playingList.setSinger(mediaInfo.getSinger());
            if (isShuffle) {
                int size = getData().size();
                if (size > 0)
                    position = new Random().nextInt(size);
            }
            mActivity.setMediaPlayingAudioWithState(playingList, position, Constants.PLAY_MUSIC.REPEAT_All,
                    isShuffle ? Constants.PLAY_MUSIC.REPEAT_SUFF : Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
        }
    }

    @Override
    public void onDetach() {
        App.getInstance().cancelPendingRequests(KeengApi.GET_ALBUM_INFO);
        super.onDetach();
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && mediaInfo == null)
            onRefresh();
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }
}

package com.metfone.selfcare.module.selfcare.event;

public class SCRechargeEvent {
    private String phoneNumber;

    public SCRechargeEvent(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

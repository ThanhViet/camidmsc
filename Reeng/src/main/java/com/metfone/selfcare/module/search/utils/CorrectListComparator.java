/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/20
 *
 */

package com.metfone.selfcare.module.search.utils;

import java.util.Comparator;

public class CorrectListComparator<T> implements Comparator<T> {
    private String keySearch;
    private int type;

    CorrectListComparator(String keySearch, int type) {
        this.keySearch = keySearch;
        this.type = type;
    }

    @Override
    public int compare(T leftObj, T rightObj) {
        boolean leftHasKey;
        boolean rightHasKey;
        switch (type) {
            case 1: {
                //todo tuong doi co dau
                leftHasKey = SearchUtils.getName(leftObj).startsWith(keySearch);
                rightHasKey = SearchUtils.getName(rightObj).startsWith(keySearch);
                if (leftHasKey && !rightHasKey) return -1;
                else if (rightHasKey && !leftHasKey) return 1;
                else return 0;
            }
            case 2: {
                //todo tuong doi khong dau
                leftHasKey = SearchUtils.convertText(SearchUtils.getName(leftObj)).startsWith(keySearch);
                rightHasKey = SearchUtils.convertText(SearchUtils.getName(rightObj)).startsWith(keySearch);
                if (leftHasKey && !rightHasKey) return -1;
                else if (rightHasKey && !leftHasKey) return 1;
                else return 0;
            }
            case 3: {
                //todo tuong doi khong dau: search info, description ...
                leftHasKey = SearchUtils.convertText(SearchUtils.getDescription(leftObj)).startsWith(keySearch);
                rightHasKey = SearchUtils.convertText(SearchUtils.getDescription(rightObj)).startsWith(keySearch);
                if (leftHasKey && !rightHasKey) return -1;
                else if (rightHasKey && !leftHasKey) return 1;
                else return 0;
            }
            default:
                break;
        }
        return 0;
    }
}
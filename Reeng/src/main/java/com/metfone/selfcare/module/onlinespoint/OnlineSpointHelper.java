package com.metfone.selfcare.module.onlinespoint;

import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQOnlineSpoint;

public class OnlineSpointHelper {

    private static final String TAG = OnlineSpointHelper.class.getSimpleName();

    ModelOnlineSpoint model;
    private static OnlineSpointHelper mInstant;
    Handler handler;
    private ApplicationController app;
    private long timeStartCount = 0;

    public static synchronized OnlineSpointHelper getInstant(ApplicationController mApp) {
        if (mInstant == null) {
            mInstant = new OnlineSpointHelper(mApp);
        }
        return mInstant;
    }

    public OnlineSpointHelper(ApplicationController mApp) {
        app = mApp;
        handler = new Handler(Looper.getMainLooper());
        String data = mApp.getPref().getString(Constants.PREFERENCE.PREF_DATA_ONLINE_SPOINT, "");
        model = new ModelOnlineSpoint();
        if (!TextUtils.isEmpty(data)) {
            model.fromString(data);
        }

    }

    public void restData() {
        app.getPref().edit().putString(Constants.PREFERENCE.PREF_DATA_ONLINE_SPOINT, "").apply();
        model = new ModelOnlineSpoint();
    }

    public void onBackground() {
        if (app.getReengAccountBusiness().getCurrentAccount() == null
                || !app.getReengAccountBusiness().isValidAccount()
                || app.getReengAccountBusiness().isAnonymousLogin()
                || app.getConfigBusiness().getTimeSpointBonus() == 0)
            return;
        if (model == null || model.isDoneInDay()) return;
        Log.i(TAG, "S----------onBackground");
        if (BuildConfig.DEBUG)
            Toast.makeText(app, "onBackground", Toast.LENGTH_SHORT).show();
        handler.removeCallbacks(runnable);
        updateModel(false);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Log.i(TAG, "S----------claim reward");
            updateModel(true);
            sendIQGetReward();
        }
    };

    public void onForeground() {
        if (app.getReengAccountBusiness().getCurrentAccount() == null
                || !app.getReengAccountBusiness().isValidAccount()
                || app.getReengAccountBusiness().isAnonymousLogin()
                || app.getConfigBusiness().getTimeSpointBonus() == 0)
            return;
        if (model == null || model.isDoneInDay()) return;

        boolean check = checkResendIQGetReward();
        if (!check) {
            long remainTime = app.getConfigBusiness().getTimeSpointBonus() - model.getTimeOnline();
            Log.i(TAG, "S----------onForeground remainTime: " + remainTime);
            timeStartCount = System.currentTimeMillis();
            handler.postDelayed(runnable, remainTime);
        }
    }

    private void updateModel(boolean claimReward) {
        if (claimReward) {
            model.setTimeOnline(app.getConfigBusiness().getTimeSpointBonus());
        } else {
//            long deltaTime = System.currentTimeMillis() - timeStartCount;
//            model.setTimeOnline(model.getTimeOnline() + deltaTime);
            model.setTimeOnline(0);
        }
        app.getPref().edit().putString(Constants.PREFERENCE.PREF_DATA_ONLINE_SPOINT, model.toString()).apply();
    }

    public boolean checkResendIQGetReward() {
        long lastTimeSendToServer = model.getLastTimeSendToServer();
        long timeOnline = model.getTimeOnline();
        if (timeOnline >= app.getConfigBusiness().getTimeSpointBonus() && !TimeHelper.checkTimeInDay(lastTimeSendToServer)) {
            Log.d(TAG, "S----------resendIQGetReward");
            sendIQGetReward();
            return true;
        }
        return false;
    }

    private void sendIQGetReward() {
        if (app.getXmppManager().isAuthenticated()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    IQOnlineSpoint iqChange = new IQOnlineSpoint();
                    iqChange.setType(IQ.Type.GET);
                    try {
                        IQOnlineSpoint result = (IQOnlineSpoint) app.getXmppManager().sendPacketThenWaitingResponse(iqChange, false);
                        if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                            if (result.getErrorCode() == 0) {
                                Log.d(TAG, "S----------done sendIQGetReward");
                                model.setLastTimeSendToServer(System.currentTimeMillis());
                                model.setTimeOnline(0);
                                app.getPref().edit().putString(Constants.PREFERENCE.PREF_DATA_ONLINE_SPOINT, model.toString()).apply();
                                int bonus = result.getBonus();
//                                for (int i = 0; i <= 4; i++)
                                showToastSpoint(app.getResources().getString(R.string.online_spoint_bonus, bonus));
                            } else {
                                Log.i(TAG, "S----------error sendIQGetReward: " + result.getErrorCode());
                            }
                        } else {
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "S----------error sendIQGetReward", e);
                    }
                }
            }).start();
        } else {
            if (BuildConfig.DEBUG)
                Toast.makeText(app, "sendIQGetReward not connected", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * toast spoint
     *
     * */
//    Toast mToast;
    public void showToastSpoint(final String msg) {
        handler.post(new Runnable() {
            @Override
            public void run() {
//                if(mToast==null) {
                LayoutInflater inflater = LayoutInflater.from(app);
                View toastLayout = inflater.inflate(R.layout.custom_toast_online_spoint, null, false);
                TextView mTvwToastContent = toastLayout.findViewById(R.id.text);
                Toast mToast = new Toast(app);
                mToast.setGravity(Gravity.TOP, 0, 200);
                mToast.setView(toastLayout);
                if (TextUtils.isEmpty(msg)) {
                    mTvwToastContent.setText(R.string.e601_error_but_undefined);
                } else {
                    mTvwToastContent.setText(msg);
                }
                mToast.setDuration(Toast.LENGTH_LONG);
//                }
//                Log.i(TAG, "show");
                mToast.show();
            }
        });

    }
}

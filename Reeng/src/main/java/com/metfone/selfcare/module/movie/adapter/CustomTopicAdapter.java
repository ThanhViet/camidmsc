package com.metfone.selfcare.module.movie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.movie.listener.LoadmoreListener;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movienew.model.CustomTopic;

import java.util.List;

public class CustomTopicAdapter extends RecyclerView.Adapter<CustomTopicAdapter.CustomTopicHolder> {

    private List<CustomTopic> mListCustomTopic;
    private Context mContext;
    private TabMovieListener.OnAdapterClick listener;
    private LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv mLoadmoreAtPositionRclvInsideRclv;
    private String mBlockName;

    public CustomTopicAdapter(List<CustomTopic> listCustomTopic, String blockName, Context context, TabMovieListener.OnAdapterClick listener,
                              LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv loadmoreAtPositionRclvInsideRclv) {
        this.mListCustomTopic = listCustomTopic;
        this.mBlockName = blockName;
        this.mContext = context;
        this.listener = listener;
        this.mLoadmoreAtPositionRclvInsideRclv = loadmoreAtPositionRclvInsideRclv;
    }

    public String getBlockName() {
        return mBlockName;
    }

    @NonNull
    @Override
    public CustomTopicHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CustomTopicHolder(LayoutInflater.from(mContext).inflate(R.layout.cinema_custom_topic, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CustomTopicHolder holder, int position) {
        holder.tvTitle.setText(mListCustomTopic.get(position).getTopicName());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        holder.rclvFilm.setLayoutManager(linearLayoutManager);
        FilmTopicAdapter filmTopicAdapter = new FilmTopicAdapter(mListCustomTopic.get(position).getListCustomFilm(),
                mListCustomTopic.get(position).getTopicName(), mContext, listener);
        holder.rclvFilm.setAdapter(filmTopicAdapter);
        holder.rclvFilm.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mListCustomTopic.get(position).getListCustomFilm().size() != 0 &&
                        mListCustomTopic.get(position).getListCustomFilm().size() % Constants.LIMIT_LOAD_HOME == 0 &&
                        linearLayoutManager.findLastCompletelyVisibleItemPosition() == mListCustomTopic.get(position).getListCustomFilm().size() - 1) {
                    mLoadmoreAtPositionRclvInsideRclv.loadmoreAtPositionOfRclvInsideRclv(position, filmTopicAdapter);
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_DRAGGING){
                    listener.onScrollData();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListCustomTopic != null ? mListCustomTopic.size() : 0;
    }

    public class CustomTopicHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        RecyclerView rclvFilm;

        public CustomTopicHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            rclvFilm = itemView.findViewById(R.id.recycler_view);
        }
    }

}

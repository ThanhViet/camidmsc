package com.metfone.selfcare.module.games.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.databinding.SearchGameFragmentBinding;
import com.metfone.selfcare.model.tabGame.GameBaseResponseData;
import com.metfone.selfcare.model.tabGame.GameCategoryModel;
import com.metfone.selfcare.model.tabGame.GameListResponseData;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.module.games.activity.PlayGameActivity;
import com.metfone.selfcare.module.games.api.GameApi;
import com.metfone.selfcare.module.games.holder.ResultSearchGameViewHolder;
import com.metfone.selfcare.module.games.holder.TopicGameViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.common.CommonSearch;
import com.metfone.selfcare.module.movienew.common.Constant;
import com.metfone.selfcare.module.movienew.holder.HistorySearchViewHolder;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchGameFragment extends BaseSlidingFragmentActivity implements ItemViewClickListener, View.OnClickListener {

    protected ApplicationController app;
    int page = 0;
    private SearchGameFragmentBinding searchFragmentBinding;
    private BaseAdapter baseAdapter, mHistoryAdapter, mHistoryAdapterHome, mResultSearchAdapter;
    private ArrayList<Object> topicModels = new ArrayList<>();
    private CommonSearch mCommonSearch;
    private ArrayList<String> mHistoryArraylist = new ArrayList<>();
    private ArrayList<GameModel> gamesArrayList = new ArrayList<>();
    private boolean hasReached = false;
    private String keySearchOld = "";
    private ArrayList<GameModel> gameArrayListMaybe = new ArrayList<>();
    private BaseAdapter mResultMaybeAdapter;
    private boolean doneSearch = false;
    private boolean isSearching = false;
    private int DELAY_SEARCH = 300;

    private Runnable runnableSearch = new Runnable() {
        @Override
        public void run() {
            search();
        }
    };

    private Handler handlerSearch = new Handler();
    public static SearchGameFragment newInstance() {
        return new SearchGameFragment();
    }
    private BaseSlidingFragmentActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchFragmentBinding = DataBindingUtil.setContentView(this, R.layout.search_game_fragment);
        mCommonSearch = new CommonSearch(this);
        app = (ApplicationController) this.getApplication();
        activity = this;
        initViews();
        initData();
        listenerViews();
        hideSoftKeyboard();
    }

    private void initViews() {
//        Utilities.setSystemUiVisibilityHideNavigation(this);
        changeStatusBar(getResources().getColor(R.color.m_home_tab_background));
        setupUI(searchFragmentBinding.constraintLayout4);
        mHistoryArraylist = mCommonSearch.getSearchHistoryLimit();
        if (mHistoryArraylist.size() > 0) {
            searchFragmentBinding.contrainRecent.setVisibility(View.VISIBLE);
        }
        TopicGameViewHolder topicHolder = new TopicGameViewHolder(getWindow().getDecorView().getRootView(), this);
        baseAdapter = new BaseAdapter(topicModels, this, R.layout.item_topic_search, topicHolder);
        FlexboxLayoutManager layoutManagerCast = new FlexboxLayoutManager(this);
        layoutManagerCast.setFlexDirection(FlexDirection.ROW);
        layoutManagerCast.setJustifyContent(JustifyContent.FLEX_START);
        searchFragmentBinding.recyclerViewTopic.setLayoutManager(layoutManagerCast);
        searchFragmentBinding.recyclerViewTopic.setAdapter(baseAdapter);

        ResultSearchGameViewHolder resultSearchViewHolder = new ResultSearchGameViewHolder(getWindow().getDecorView().getRootView(), this);
        mResultSearchAdapter = new BaseAdapter(gamesArrayList, this, R.layout.item_game_new2, resultSearchViewHolder);
        searchFragmentBinding.recycleResultSearch.setLayoutManager(new GridLayoutManager(this, 4));
        searchFragmentBinding.recycleResultSearch.setAdapter(mResultSearchAdapter);

        //Search game
        HistorySearchViewHolder historySearchViewHolder = new HistorySearchViewHolder(getWindow().getDecorView().getRootView(), this, Constant.RESULT);
        mHistoryAdapter = new BaseAdapter(mHistoryArraylist, this, R.layout.item_history_search, historySearchViewHolder);
        mHistoryAdapter.setItemCount(10);
        searchFragmentBinding.recyclerHistory.setLayoutManager(new LinearLayoutManager(this));
        searchFragmentBinding.recyclerHistory.setAdapter(mHistoryAdapter);

        // history search
        HistorySearchViewHolder historySearchViewHomeHolder = new HistorySearchViewHolder(getWindow().getDecorView().getRootView(), this, Constant.HOME);
        mHistoryAdapterHome = new BaseAdapter(mHistoryArraylist, this, R.layout.item_history_search, historySearchViewHomeHolder);
        searchFragmentBinding.recyclerRecentSearchesHistory.setLayoutManager(new LinearLayoutManager(this));
        mHistoryAdapterHome.setItemCount(10);
        searchFragmentBinding.recyclerRecentSearchesHistory.setAdapter(mHistoryAdapterHome);

        // list maybe
        mResultMaybeAdapter = new BaseAdapter(gamesArrayList, this, R.layout.item_game_new2, resultSearchViewHolder);
        searchFragmentBinding.recyclerMaybe.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        searchFragmentBinding.recyclerMaybe.setAdapter(mResultMaybeAdapter);

        // config touch hide keyboard
        setupUI(searchFragmentBinding.contrasSearchHome, this);
    }

    private void initData() {
        if (GameHomePageFragment.listCategory.size() > 0) {
            topicModels.addAll(GameHomePageFragment.listCategory);
        }
        baseAdapter.updateAdapter(topicModels);
        checkEmptyTopic();

        mResultMaybeAdapter.updateAdapter(GameHomePageFragment.listTrendingGames);
        if (GameHomePageFragment.listTrendingGames.size() > 0) {
            searchFragmentBinding.contraisMaybe.setVisibility(View.VISIBLE);
        } else {
            searchFragmentBinding.contraisMaybe.setVisibility(View.GONE);
        }
    }

    private void listenerViews() {
        searchFragmentBinding.tvButtonSkip.setOnClickListener(this::onClick);
        searchFragmentBinding.btnClear.setOnClickListener(this::onClick);
        searchFragmentBinding.edtSearch.setOnClickListener(this::onClick);
        searchFragmentBinding.btnClose.setOnClickListener(this::onClick);
        searchFragmentBinding.edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
        searchFragmentBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String textInput = s.toString().trim();
                if (textInput.isEmpty()) {
                    searchFragmentBinding.tvButtonSkip.setText(R.string.text_cancel_search_movie);
                    searchFragmentBinding.tvButtonSkip.setTextColor(Color.WHITE);
                    searchFragmentBinding.btnClose.setImageResource(R.drawable.ic_search_new);
                } else {
                    searchFragmentBinding.tvButtonSkip.setTextColor(Color.parseColor("#D38808"));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    searchFragmentBinding.contrasResultSearch.setVisibility(View.GONE);
                    searchFragmentBinding.recyclerHistory.setVisibility(View.GONE);
                } else {
                    searchFragmentBinding.tvButtonSkip.setTextColor(Color.parseColor("#D38808"));
                    searchFragmentBinding.recyclerHistory.setVisibility(View.VISIBLE);
                    handlerSearch.postDelayed(runnableSearch, DELAY_SEARCH);
                }
            }
        });

        com.metfone.selfcare.adapter.BaseAdapter.setRecyclerViewLoadMore(searchFragmentBinding.recycleResultSearch, new com.metfone.selfcare.adapter.BaseAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isSearching && !hasReached) {
                    page ++;
                    search(keySearchOld);
                }
            }
        });
    }

    private void checkEmptyTopic() {
        //check empty topic
        if (topicModels.size() == 0) {
            searchFragmentBinding.contrasSearchHome.setVisibility(View.GONE);
        } else {
            searchFragmentBinding.contrasSearchHome.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {
        hideKeyboard();
        if (list.get(position) instanceof GameCategoryModel) {
            GameCategoryModel categoryModel = (GameCategoryModel) list.get(position);
            DetailCategoryGameFragment detailCategoryFragment = DetailCategoryGameFragment.newInstance(categoryModel);
            addFragment(R.id.frameCategory, detailCategoryFragment, true);
        }
        if (list.get(position) instanceof String) {
            searchFragmentBinding.edtSearch.setText((String) list.get(position));
            searchFragmentBinding.recyclerHistory.setVisibility(View.GONE);
            page = 0;
            hasReached = false;
            search((String) list.get(position));
        }

        if (list.get(position) instanceof GameModel) {
            GameModel game = (GameModel) list.get(position);
            Intent intent = new Intent(this, PlayGameActivity.class);
            intent.putExtra("OPEN_GAME", game);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        }
    }

    private void updateKeySearch(String keySearch) {
        mHistoryArraylist.clear();
        mCommonSearch.insertSearchHistory(keySearch);
        mHistoryArraylist.addAll(mCommonSearch.getSearchHistoryLimit());
        mHistoryAdapterHome.updateAdapter(mHistoryArraylist);
        mHistoryAdapter.updateAdapter(mHistoryArraylist);
        if (searchFragmentBinding.contrainRecent.getVisibility() == View.GONE) {
            searchFragmentBinding.contrainRecent.setVisibility(View.VISIBLE);
        }
    }

    private void updateResultSearch(ArrayList<GameModel> result) {
        searchFragmentBinding.contrasResultSearch.setVisibility(View.VISIBLE);
        mResultSearchAdapter.updateAdapter(result);
        searchFragmentBinding.tvButtonSkip.setText(R.string.text_cancel_search_movie);
        searchFragmentBinding.filmNotFound.setVisibility(result == null || result.size() == 0 ? View.VISIBLE : View.GONE);
    }

    private void clearKeySearch() {
        mHistoryArraylist.clear();
        mHistoryArraylist.addAll(mCommonSearch.getSearchHistoryLimit());
        mHistoryAdapterHome.updateAdapter(mHistoryArraylist);
        mHistoryAdapter.updateAdapter(mHistoryArraylist);
        searchFragmentBinding.contrainRecent.setVisibility(View.GONE);
    }

    public void search() {
        doneSearch = true;
        searchFragmentBinding.recyclerHistory.setVisibility(View.INVISIBLE);
        page = 0;
        hasReached = false;
        search(searchFragmentBinding.edtSearch.getText().toString());
        searchFragmentBinding.btnClose.setImageResource(R.drawable.ic_search_new);
    }

    public void search(String keySearch) {
        isSearching = true;
        if (keySearch.isEmpty()) return;
        if (searchFragmentBinding.edtSearch.getText().length() > 0) {
            searchFragmentBinding.edtSearch.setSelection(searchFragmentBinding.edtSearch.getText().length());
        }
        searchFragmentBinding.contrasResultSearch.setVisibility(View.VISIBLE);
        searchFragmentBinding.progress.setVisibility(View.VISIBLE);
        keySearchOld = keySearch;
        StringBuilder resultSearchTxt = new StringBuilder(getString(R.string.txt_about_search_movie));

        mCommonSearch.getSearchGameApi().getGameByName(keySearch, new GameApi.ListenerGameApi() {
            @Override
            public void onPreRequest() {

            }

            @Override
            public void onSuccess(Object responseData) {
                if (responseData instanceof GameBaseResponseData) {
                    GameBaseResponseData resultData = (GameBaseResponseData) responseData;
                    ArrayList<GameModel> result = new ArrayList<>();
                    for (GameCategoryModel items : resultData.getItems()) {
                        for (GameModel game : items.getGames()) {
                            result.add(game);
                        }
                    }

                    searchFragmentBinding.progress.setVisibility(View.GONE);
                    resultSearchTxt.append(" " + result.size() + " ");
                    resultSearchTxt.append(getString(R.string.txt_results_with_the_keyword));
                    resultSearchTxt.append(" \"" + keySearch + "\".");
                    searchFragmentBinding.tvResultSearch.setText(resultSearchTxt.toString());

                    updateResultSearch(result);
                    updateKeySearch(keySearch);
                    if (result.size() < SearchUtils.LIMIT_REQUEST_SEARCH) {
                        hasReached = true;
                    }
                    isSearching = false;
                }
            }

            @Override
            public void onError(String message) {
                searchFragmentBinding.progress.setVisibility(View.GONE);
                searchFragmentBinding.filmNotFound.setText(R.string.empty_no_data);
                searchFragmentBinding.filmNotFound.setVisibility(View.VISIBLE);
                searchFragmentBinding.contrasResultSearch.setVisibility(View.VISIBLE);
                searchFragmentBinding.tvResultSearch.setVisibility(View.GONE);
                searchFragmentBinding.recycleResultSearch.setVisibility(View.GONE);
                searchFragmentBinding.tvButtonSkip.setText(R.string.text_cancel_search_movie);
                isSearching = false;
            }

            @Override
            public void onFailure(String exp) {
                searchFragmentBinding.progress.setVisibility(View.GONE);
                searchFragmentBinding.filmNotFound.setText(R.string.empty_no_data);
                searchFragmentBinding.filmNotFound.setVisibility(View.VISIBLE);
                searchFragmentBinding.contrasResultSearch.setVisibility(View.VISIBLE);
                searchFragmentBinding.tvResultSearch.setVisibility(View.GONE);
                searchFragmentBinding.recycleResultSearch.setVisibility(View.GONE);
                searchFragmentBinding.tvButtonSkip.setText(R.string.text_cancel_search_movie);
                isSearching = false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvButtonSkip:
                hideKeyboard();
                finish();
                break;
            case R.id.btnClear:
                mCommonSearch.clearHistory();
                clearKeySearch();
                hideKeyboard();
                break;
            case R.id.btnClose:
                if (!doneSearch) {
                    finish();
                }
                break;
        }
    }

    public void setupUI(View view) {

        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(SearchGameFragment.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }
}

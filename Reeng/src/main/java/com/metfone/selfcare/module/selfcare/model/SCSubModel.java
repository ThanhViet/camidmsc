package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCSubModel implements Serializable {

//  "isdn": "9680718848",
//          "payType": 2,
//          "subType": 1

    @SerializedName("isdn")
    private String isdn;

    @SerializedName("payType")
    private int payType;

    @SerializedName("subType")
    private int subType;

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public int getSubType() {
        if(subType == 0) return 1;
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    @Override
    public String toString() {
        return "SCSubModel{" +
                "isdn='" + isdn + '\'' +
                ", payType=" + payType +
                ", subType=" + subType +
                '}';
    }
}

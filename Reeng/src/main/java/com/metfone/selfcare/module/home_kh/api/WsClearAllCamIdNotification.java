package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsClearAllCamIdNotification extends BaseRequest<WsClearAllCamIdNotification.Request> {
    public class Request {
        @SerializedName("language")
        public String language;
        @SerializedName("camid")
        public String camid;
        @SerializedName("deviceId")
        public String deviceId;
    }
}

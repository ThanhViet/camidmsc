package com.metfone.selfcare.module.newdetails.animation;

import android.animation.Animator;
import android.view.View;

/**
 * Created by huongnd38 on 9/26/18.
 */

public interface  BaseAnimation {

    Animator[] getAnimators(View view);

}

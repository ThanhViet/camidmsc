/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/4
 *
 */

package com.metfone.selfcare.module.myviettel.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CaptchaModel implements Serializable {
    private static final long serialVersionUID = 308304602909217014L;

    @SerializedName("url")
    private String url;
    @SerializedName("sid")
    private String sid;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}

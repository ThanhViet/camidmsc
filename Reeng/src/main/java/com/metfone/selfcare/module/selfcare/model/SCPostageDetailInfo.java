package com.metfone.selfcare.module.selfcare.model;

import java.util.ArrayList;

public class SCPostageDetailInfo {

    private String title;
    private ArrayList<SCPostageDetail> data = new ArrayList<>();

    public SCPostageDetailInfo(String title, ArrayList<SCPostageDetail> data) {
        this.title = title;
        this.data = data;
    }

    public SCPostageDetailInfo() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<SCPostageDetail> getData() {
        return data;
    }

    public void setData(ArrayList<SCPostageDetail> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SCPostageDetailInfo{" +
                "title='" + title + '\'' +
                ", data=" + data +
                '}';
    }
}

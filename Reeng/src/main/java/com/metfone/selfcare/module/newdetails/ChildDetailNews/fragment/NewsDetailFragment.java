package com.metfone.selfcare.module.newdetails.ChildDetailNews.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.adapter.TagOnMediaAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.newdetails.ChildDetailNews.adapter.NewsDetailAdapter;
import com.metfone.selfcare.module.newdetails.ChildDetailNews.presenter.INewsDetailPresenter;
import com.metfone.selfcare.module.newdetails.ChildDetailNews.presenter.NewsDetailPresenter;
import com.metfone.selfcare.module.newdetails.ChildDetailNews.view.INewsDetailView;
import com.metfone.selfcare.module.newdetails.MainDetailNews.fragment.MainNewsDetailFragment;
import com.metfone.selfcare.module.newdetails.SlideImage.SlideImageActivity;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.ListImageModel;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.response.NewsContentResponse;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.tokenautocomplete.FilteredArrayAdapter;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsDetailFragment extends BaseFragment implements INewsDetailView, AbsInterface.OnNewsHotListener {

    public boolean isHomeNewsDetailFragment = false;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loadingView)
    View loadingView;
    @BindView(R.id.retry_layout)
    View retryLayout;
    NewsDetailAdapter adapter;
    LinearLayoutManager layoutManager;
    NewsModel newsModel;
    int currentPage = 1;
    boolean loading;
    boolean isRefresh;
    boolean isRunningAnimation;
    boolean isStatusLoaded = false;
    String mNewsUrl;
    boolean fromNetNews = false;
    boolean fullData = false;
    long unixTime = 0;
    int numberComment = 0;
    ArrayList<NewsModel> relateData = new ArrayList<>();
    ArrayList<NewsModel> newsMost = new ArrayList<>();
    ArrayList<NewsModel> eventDatas = new ArrayList<>();
    INewsDetailPresenter mPresenter;
    LinearLayout mBottomLikeCommentLayout;
    private TagsCompletionView mEditText;
    private ImageButton mSend;
    private AppCompatImageView imgBtnLike, imgBtnComment, imgBtnShare;
    private RelativeLayout rlComment;
    private TextView tvNumberComment;
    private FeedBusiness mFeedBusiness;
    private String urlAction;
    private int gaCategoryId, gaActionId;
    private String currentUrl = "";
    private boolean isLikeUrl = false;
    private WSOnMedia rest;
    private ArrayList<UserInfo> userLikesInComment = new ArrayList<>();
    private ReengAccount mAccount;
    private FeedModelOnMedia mFeed;
    private TagOnMediaAdapter adapterUserTag;

    public static NewsDetailFragment newInstance(Bundle bundle, boolean isHome) {
        NewsDetailFragment fragment = new NewsDetailFragment();
        fragment.setArguments(bundle);
        fragment.isHomeNewsDetailFragment = isHome;
        fragment.mPresenter = new NewsDetailPresenter();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_detail, container, false);
        if (mPresenter == null) {
            mPresenter = new NewsDetailPresenter();
        }
        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);
        setUp(view);

        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().stopVideo();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (canLazyLoad()) {
            loadData();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData();
        }
    }

    @Override
    protected void setUp(View view) {
        if (getArguments() == null) {
            return;
        }
        newsModel = (NewsModel) getArguments().getSerializable(CommonUtils.KEY_NEWS_ITEM_SELECT);
//        fromNetNews = getArguments().getBoolean("fromNetNews");
        fullData = getArguments().getBoolean(CommonUtils.FULL_DATA_DETAIL);
        if (newsModel != null) mNewsUrl = newsModel.getUrl();
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getBaseActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new NewsDetailAdapter(getBaseActivity(), newsModel, this, fullData);
//        recyclerView.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (adapter != null) {
//                    adapter.loadAdView();
//                }
//            }
//        }, 1000);
        recyclerView.setAdapter(adapter);
        if (!TextUtils.isEmpty(mNewsUrl) && (mNewsUrl.contains("netnews.vn") || mNewsUrl.contains("http:") || mNewsUrl.contains("https:"))) {
            urlAction = mNewsUrl;
        } else {
            urlAction = CommonUtils.DOMAIN + mNewsUrl;
        }
        if (TextUtils.isEmpty(urlAction)) {
            return;
        }

        ApplicationController application = (ApplicationController) getBaseActivity().getApplication();
        mFeedBusiness = application.getFeedBusiness();
        int feedType = getArguments().getInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, 0);
        if (feedType == Constants.ONMEDIA.FEED_CONTACT_DETAIL) {
            mFeed = mFeedBusiness.getFeedProfileProcess();
        } else if (feedType == Constants.ONMEDIA.FEED_PROFILE) {
            mFeed = mFeedBusiness.getFeedProfileFromUrl(urlAction);
        } else {
            mFeed = mFeedBusiness.getFeedModelFromUrl(urlAction);
        }

        rest = new WSOnMedia(application);
        if (mFeed == null && newsModel != null) {
            mFeed = new FeedModelOnMedia();
            FeedContent feedContent = new FeedContent();
            feedContent.setItemType(FeedContent.ITEM_TYPE_NEWS);
            feedContent.setUrl(newsModel.getUrl());
            feedContent.setItemName(newsModel.getTitle());
            feedContent.setImageUrl(newsModel.getImage());
            mFeed.setFeedContent(feedContent);
        }
        currentUrl = urlAction;
        mAccount = application.getReengAccountBusiness().getCurrentAccount();

        if (currentUrl == null) {
            currentUrl = "";
        }
        mBottomLikeCommentLayout = view.findViewById(R.id.person_chat_detail_footer);
        mEditText = view.findViewById(R.id.person_chat_detail_input_text);
        mSend = view.findViewById(R.id.person_chat_detail_send_reeng_text);
        imgBtnLike = view.findViewById(R.id.img_like_content);
        imgBtnComment = view.findViewById(R.id.img_comment_new);
        rlComment = view.findViewById(R.id.rl_comment);
        imgBtnShare = view.findViewById(R.id.img_share_new);
        tvNumberComment = view.findViewById(R.id.tv_number_comment_new);
        gaCategoryId = R.string.ga_category_onmedia;
        gaActionId = R.string.ga_onmedia_action_netnews_detail;

        if (DeviceHelper.isTablet(getBaseActivity())) {
            mEditText.setImeOptions(EditorInfo.IME_FLAG_NO_FULLSCREEN);
        } else {
            mEditText.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }

        setupCommentLike();
        if (mFeed != null) {
            isLikeUrl = mFeed.getIsLike() == 1;
            if (mFeed.getIsLike() == 1) {
                imgBtnLike.setImageResource(R.drawable.ic_v5_heart_active);
            } else {
                imgBtnLike.setImageResource(R.drawable.ic_v5_heart_normal);
            }
        }
        if (newsModel != null) {

            if (TextUtils.isEmpty(newsModel.getTitle())) {
                if (loadingView != null)
                    loadingView.setVisibility(View.VISIBLE);
            }

            //Set du lieu
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    try {
                        int posVisible = layoutManager.findFirstVisibleItemPosition();
                        int lastVisible = layoutManager.findLastCompletelyVisibleItemPosition();
                        //Load more
                        int totalItemCount = layoutManager.getItemCount();
                        if (!loading && totalItemCount <= (lastVisible + 5) && newsMost.size() > 0) {
                            onLoadMore();
                            loading = true;
                        }
                    } catch (Exception ex) {
                        Log.e(TAG, "onScrolled Exception:" + ex.toString());
                    }
                }
            });
        }
        InputMethodUtils.hideKeyboardWhenTouch(recyclerView, getNewsDetailActivity());
    }

    public void loadData() {
        if (mPresenter == null) return;
        if (newsModel != null) {
            if (!fullData) {
                mPresenter.loadData(newsModel);
            } else {
                if (isHomeNewsDetailFragment && getParentFragment() != null && getParentFragment() instanceof MainNewsDetailFragment) {
                    ((MainNewsDetailFragment) getParentFragment()).loadData();
                    isHomeNewsDetailFragment = false;
                }
                if (mBottomLikeCommentLayout != null) {
                    mBottomLikeCommentLayout.setVisibility(View.VISIBLE);
                }
                isRefresh = true;
                currentPage = 1;
                unixTime = newsModel.getUnixTime();
                loadDataRelate();
                mPresenter.loadNewsStatus(urlAction);
            }
        }
    }


    public void loadNewsStatus() {
        if (mPresenter != null) {
            mPresenter.loadNewsStatus(urlAction);
        }
    }

    public void retryLoadData() {
        setVisibility(retryLayout, false);

        if (adapter != null) {
            adapter.setLoadingText(true);
        }

        if (recyclerView != null) {
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    loadData();
                }
            });
        }
    }

    public void loadDataRelate() {
        if (mPresenter == null || newsModel == null) return;
        mPresenter.loadDataRelate(newsModel, currentPage, unixTime);

    }

    private void onLoadMore() {
        isRefresh = false;
        currentPage++;
        loadDataRelate();
    }

    @Override
    public void loadDataSuccess(boolean flag) {
        setVisibility(loadingView, false);

        if (isHomeNewsDetailFragment && !flag && newsModel != null && TextUtils.isEmpty(newsModel.getTitle())) {
            if (getParentFragment() != null && getParentFragment() instanceof MainNewsDetailFragment) {
                UrlConfigHelper.gotoWebViewOnMedia(ApplicationController.self(), getBaseActivity(), currentUrl);
                ((MainNewsDetailFragment) getParentFragment()).goPrevious();
            }

        } else {
            setVisibility(retryLayout, !flag);
            if (!flag && adapter != null) {
                adapter.setLoadingText(false);
            }
        }
    }

    public void setVisibility(View view, boolean visibility) {
        if (view != null) {
            view.setVisibility(visibility ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void loadDataRelateSuccess(boolean flag) {
        setVisibility(loadingView, false);
    }

    @Override
    public void bindData(NewsContentResponse response) {
        if (response != null && response.getData() != null && response.getData().getBody().size() != 0) {
            if (newsModel == null || adapter == null) return;
            if (isHomeNewsDetailFragment && getParentFragment() != null && getParentFragment() instanceof MainNewsDetailFragment) {
                ((MainNewsDetailFragment) getParentFragment()).loadData();
                ((MainNewsDetailFragment) getParentFragment()).loadCatagory(response.getData().getCategory());
                isHomeNewsDetailFragment = false;
            }

            if (mBottomLikeCommentLayout != null) {
                mBottomLikeCommentLayout.setVisibility(View.VISIBLE);
            }
            newsModel.getBody().clear();
            newsModel.getBody().addAll(response.getData().getBody());

            adapter.setModelInfo(response.getData());
            adapter.setDatas(response.getData().getBody());
            if (TextUtils.isEmpty(newsModel.getImage())) {
                newsModel.setImage(response.getData().getImage());
            }
            //Load data relate
            isRefresh = true;
            currentPage = 1;
            unixTime = newsModel.getUnixTime();
            loadDataRelate();
            mPresenter.loadNewsStatus(urlAction);

            //load advertising o day
            //mPresenter.loadAdvertising(new AdvertisingRequest(AppConstants.ADVERTISING_DETAIL_NEWS_TYPE, 1, newsModel.getPid()));

        } else {
            //Retry api
            countRetry++;
            if (countRetry < MAX_RETRY) {
                if (newsModel != null && mPresenter != null) {
                    mPresenter.loadData(newsModel);
                }
            }
        }
    }

    @Override
    public void bindDataRelate(NewsResponse response) {
        if (response != null) {
            if (response.getData() != null && response.getData().size() > 0) {
                if (isRefresh) {
                    relateData.clear();
                    newsMost.clear();
                    eventDatas.clear();
                }
                for (int i = 0; i < response.getData().size(); i++) {
                    NewsModel model = response.getData().get(i);

                    if (model.getID() != newsModel.getID()) {
                        if (model.getPosition() == 8) {
                            relateData.add(model);
                        } else if (model.getPosition() == 194) {
                            eventDatas.add(model);
                        } else {
                            newsMost.add(model);
                        }
                    }
                    if (i == response.getData().size() - 1)
                        unixTime = model.getUnixTime();
                }
                loading = false;
                adapter.setRelateDatas(relateData);
                adapter.setNewsMostDatas(newsMost);
                adapter.setNewsEventDatas(eventDatas);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void loadStatusSucess(boolean success) {
        isStatusLoaded = success;
    }

    @Override
    public void loadDataFeed(FeedModelOnMedia mFeedMedia) {
        if (mFeedMedia == null) {
            return;
        }
        mFeed = mFeedMedia;
        if (TextUtils.isEmpty(mFeed.getFeedContent().getUrl())) {
            mFeed.getFeedContent().setUrl(currentUrl);
        }
        if (TextUtils.isEmpty(mFeed.getFeedContent().getItemType()) || !mFeed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_NEWS)) {
            mFeed.getFeedContent().setItemType(FeedContent.ITEM_TYPE_NEWS);
        }
        if (TextUtils.isEmpty(mFeed.getFeedContent().getItemName()) || !mFeed.getFeedContent().getItemName().equals(newsModel.getTitle())) {
            mFeed.getFeedContent().setItemName(newsModel.getTitle());
        }
        if (TextUtils.isEmpty(mFeed.getFeedContent().getImageUrl()) || !mFeed.getFeedContent().getImageUrl().equals(newsModel.getImage())) {
            mFeed.getFeedContent().setImageUrl(newsModel.getImage());
        }
        setLikeFeed(mFeed.getIsLike() == 1);
        setNumberComment((int) mFeed.getFeedContent().getCountComment());
    }

    @Override
    public void onClickImageItem(NewsDetailModel entry) {
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().stopVideoDetail();
        }
        try {
            int position = 0;
            ArrayList<String> listImage = new ArrayList<>();
            for (int i = 0; i < newsModel.getBody().size(); i++) {
                NewsDetailModel model = newsModel.getBody().get(i);
                if (model.getType() == 2) {
                    listImage.add(model.getContent());
                    if (model.getContent().equals(entry.getContent())) {
                        position = listImage.size() - 1;
                    }
                }
            }

            ListImageModel listImageModel = new ListImageModel(listImage);
            Intent intent = new Intent(getBaseActivity(), SlideImageActivity.class);
            intent.putExtra("position", position);
            intent.putExtra("listImage", listImageModel);
            getBaseActivity().startActivity(intent);
        } catch (Exception ex) {
            Log.e(TAG, "onClickImageItem : " + ex.toString());
        }
    }

    @Override
    public void onClickVideoItem(NewsDetailModel entry, FrameLayout view, int pos) {
    }

    @Override
    public void onClickRelateItem(NewsModel model, int type) {
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().stopVideoDetail();
        }
        readNews(model);
        AppStateProvider.CLICK_CLOSE = AppStateProvider.CLICK_CLOSE + 1;
        scrollFirst();
    }

    @Override
    public void onClickListenItem(NewsModel model) {
    }

    @Override
    public void onClickSourceNewsItem(String cateName, String cateId) {
    }

    @Override
    public void onClickLinkItem(int type, NewsModel entry) {
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().stopVideoDetail();
        }
        if (type == 1) {
            readNews(entry);
            AppStateProvider.CLICK_CLOSE = AppStateProvider.CLICK_CLOSE + 1;
        } else {
            UrlConfigHelper.gotoWebViewOnMedia(ApplicationController.self(), getBaseActivity(), entry.getUrl());
        }
    }

    @Override
    public void onItemClickBtnMore(NewsModel model) {
        DialogUtils.showOptionNewsItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionNew(object, menuId);
            }
        });
    }

    @Override
    public void onPlayVideoDetail(NewsDetailModel model, int position, NewsDetailAdapter.NewsHotDetailVideoHolder holder) {
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().playVideoDetailNew(model, holder);
        }
    }

    @Override
    public void onDetachVideoDetail() {
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().stopVideoDetail();
        }
    }

    public void scrollFirst() {
        if (recyclerView != null) {
            if (getNewsDetailActivity() != null && getNewsDetailActivity().isPlaying) {
                recyclerView.scrollToPosition(0);
            }
        }
    }

    @Override
    public void onDestroyView() {
        if (adapter != null) {
            adapter.onDestroy();
        }
        if (mPresenter != null) {
            mPresenter.onDetach();
            mPresenter = null;
        }
        if (mSend != null) {
            mSend.setOnClickListener(null);
        }

        if (imgBtnLike != null) {
            imgBtnLike.setOnClickListener(null);
        }

        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().stopVideoDetail();
        }
        if(adapterUserTag != null){
            adapterUserTag = null;
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (getNewsDetailActivity() != null) {
            getNewsDetailActivity().stopVideo();
        }

        mFeedBusiness = null;
        if (userLikesInComment != null) {
            userLikesInComment.clear();
        }
        rest = null;
        super.onDestroy();
    }

    @Override
    public void paddingView(boolean flag) {
        super.paddingView(flag);
    }

    @OnClick(R.id.retry)
    public void onRetryClick() {
        retryLoadData();
    }

    private void setupCommentLike() {
        ArrayList<PhoneNumber> listPhone = ApplicationController.self().getContactBusiness().getListNumberUseMocha();
        if (listPhone == null) listPhone = new ArrayList<>();
        adapterUserTag = new TagOnMediaAdapter(ApplicationController.self(), listPhone, mEditText);
        mEditText.setAdapter(adapterUserTag);

        mEditText.setThreshold(0);
        adapterUserTag.setListener(new FilteredArrayAdapter.OnChangeItem() {
            @Override
            public void onChangeItem(int count) {
                Log.i(TAG, "onChangeItem: " + count);
                if (count > 2) {
                    int height = getResources().getDimensionPixelOffset(R.dimen.max_height_drop_down_tag);
                    mEditText.setDropDownHeight(height);
                } else
                    mEditText.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });
        TiinUtilities.setVisibilityView(rlComment, true);
        TiinUtilities.setVisibilityView(imgBtnShare, true);

        mSend.setClickable(true);
        mEditText.setHint(getResources().getString(R.string.hint_comment_tiin));
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (getBaseActivity() == null) {
                    return;
                }
                Log.i(TAG, "link: " + charSequence.toString());
                String text = charSequence.toString();
                if (!text.isEmpty()) {
                    getBaseActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSend.setVisibility(View.VISIBLE);
//                            imgBtnLike.setVisibility(View.GONE);
                            TiinUtilities.setVisibilityView(rlComment, false);
                            TiinUtilities.setVisibilityView(imgBtnShare, false);
//                            TiinUtilities.setVisibilityView(tvNumberComment, false);
                            mSend.setClickable(true);
                        }
                    });
                } else {
                    getBaseActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSend.setVisibility(View.GONE);
//                            imgBtnLike.setVisibility(View.VISIBLE);
                            TiinUtilities.setVisibilityView(rlComment, true);
                            TiinUtilities.setVisibilityView(imgBtnShare, true);
//                            TiinUtilities.setVisibilityView(tvNumberComment, true);
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                    getBaseActivity().showDialogLogin();
                else
                    sendComment();
            }
        });

        imgBtnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                    getBaseActivity().showDialogLogin();
                else {
                    getBaseActivity().trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_like);
                    if (mFeed != null && currentUrl.equals(urlAction)
                            && !mFeed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_TOTAL)) {
                        onClickLikeFeed(mFeed);
                    } else {
                        onClickLike(isLikeUrl);
                    }
                }
            }
        });
        rlComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFeed != null && mFeedBusiness != null) {
                    mFeed.getFeedContent().setUrl(urlAction);
                    Intent intent = new Intent(getNewsDetailActivity(), OnMediaActivityNew.class);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, mFeed.getFeedContent().getUrl());
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, mFeed);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, true);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_GET_DETAIL_URL, false);
                    //intent.putExtra(Constants.ONMEDIA.EXTRAS_CONTENT_DATA, mFeed.getFeedContent());
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_TAB_NEWS);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_ROW_ID, mFeed.getBase64RowId());
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, false);
                    startActivity(intent);
                }
            }
        });
        imgBtnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    getBaseActivity().showDialogLogin();
                } else {
                    if (newsModel != null) {
                        ShareUtils.openShareMenu(getBaseActivity(), newsModel);
                    }
                }
            }
        });
    }

    public void onClickLikeFeed(final FeedModelOnMedia feed) {
        if (getBaseActivity() == null || feed == null || mAccount == null || rest == null) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(getBaseActivity())) {
            getBaseActivity().showToast(R.string.no_connectivity_check_again);
            return;
        }

        final boolean isLiked = (feed.getIsLike() == 1);
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(feed, !isLiked);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), action, "", feed.getBase64RowId(), "", null,
                new ResponseListener(NewsDetailFragment.this, 1, isLiked), new ResponseErrorListener(NewsDetailFragment.this, 1, isLiked));
    }

    public void onLikeFeedResponse(String response, FeedModelOnMedia feed, boolean isLiked) {
        Log.i(TAG, "actionLike: onresponse: " + response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                if (code != HTTPCode.E200_OK) {
                    setLikeFeed(feed, isLiked);
                    if (getBaseActivity() != null)
                        getBaseActivity().showToast(R.string.e601_error_but_undefined);
                } else {
                    //islike----unlike, !islike----like
                    if (!isLiked) {
                        userLikesInComment.add(0, new UserInfo(mAccount.getJidNumber(), mAccount.getName()));
                        if (userLikesInComment.size() > 2) {
                            ArrayList<UserInfo> listTmp = new ArrayList<>();
                            listTmp.add(0, userLikesInComment.get(0));
                            listTmp.add(1, userLikesInComment.get(1));
                            userLikesInComment = listTmp;
                        }
                    } else {
                        if (userLikesInComment.size() == 0) {
                            Log.i(TAG, "Loi roi, size phai khac 0");
                        } else if (userLikesInComment.size() == 1) {
                            userLikesInComment.clear();
                        } else if (userLikesInComment.size() == 2) {
                            int indexToDelete = -1;
                            for (int i = 0; i < userLikesInComment.size(); i++) {
                                if (userLikesInComment.get(i).getMsisdn().equals(mAccount.getJidNumber())) {
                                    indexToDelete = i;
                                    break;
                                }
                            }
                            if (indexToDelete != -1) {
                                userLikesInComment.remove(indexToDelete);
                            }
                        } else {
                            Log.i(TAG, "Loi roi, size phai < 3");
                        }
                    }
                }
            } else {
                setLikeFeed(feed, isLiked);
                if (getBaseActivity() != null)
                    getBaseActivity().showToast(R.string.e601_error_but_undefined);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            setLikeFeed(feed, isLiked);
            if (getBaseActivity() != null)
                getBaseActivity().showToast(R.string.e601_error_but_undefined);
        }
    }

    private void setLikeFeed(FeedModelOnMedia feed, boolean isLike) {
        //neu la like thi tang like them 1, neu la unlike thi tru 1.

        int delta;
        if (isLike) {
            delta = 1;
            feed.setIsLike(1);
            imgBtnLike.setImageResource(R.drawable.ic_v5_heart_active);
        } else {
            delta = -1;
            feed.setIsLike(0);
            imgBtnLike.setImageResource(R.drawable.ic_v5_heart_normal);
        }
        long countLike = feed.getFeedContent().getCountLike();
        feed.getFeedContent().setCountLike(countLike + delta);
        if (mFeed != null) {
            mFeed.getFeedContent().setCountLike(countLike + delta);
        }
    }

    public void setLikeFeed(boolean isLike) {
        if (isLike) {
            imgBtnLike.setImageResource(R.drawable.ic_v5_heart_active);
            isLikeUrl = true;
        } else {
            isLikeUrl = false;
            imgBtnLike.setImageResource(R.drawable.ic_v5_heart_normal);
        }
    }

    public void onClickLike(final boolean isLiked) {
        if (getBaseActivity() == null || mAccount == null || rest == null) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(getBaseActivity())) {
            getBaseActivity().showToast(R.string.no_connectivity_check_again);
            return;
        }

        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(!isLiked);
        rest.logAppV6(currentUrl, "", null, action, "", "", "", null,
                new ResponseListener(NewsDetailFragment.this, 2, isLiked), new ResponseErrorListener(NewsDetailFragment.this, 2, isLiked));
    }

    public void onLikeResponse(String response, boolean isLiked) {
        Log.i(TAG, "actionLike: onresponse: " + response);
        try {
            if (isDetached()) {
                return;
            }
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                if (code != HTTPCode.E200_OK) {
                    setLikeFeed(isLiked);
                    if (getBaseActivity() != null)
                        getBaseActivity().showToast(R.string.e601_error_but_undefined);
                } else {
                    //islike----unlike, !islike----like
                    if (mAccount == null) {
                        return;
                    }
                    if (!isLiked) {
                        userLikesInComment.add(0, new UserInfo(mAccount.getJidNumber(), mAccount.getName()));
                        if (userLikesInComment.size() > 2) {
                            ArrayList<UserInfo> listTmp = new ArrayList<>();
                            listTmp.add(0, userLikesInComment.get(0));
                            listTmp.add(1, userLikesInComment.get(1));
                            userLikesInComment = listTmp;
                        }
                    } else {
                        if (userLikesInComment.size() == 0) {
                            Log.i(TAG, "Loi roi, size phai khac 0");
                        } else if (userLikesInComment.size() == 1) {
                            userLikesInComment.clear();
                        } else if (userLikesInComment.size() == 2) {
                            int indexToDelete = -1;
                            for (int i = 0; i < userLikesInComment.size(); i++) {
                                if (userLikesInComment.get(i).getMsisdn().equals(mAccount.getJidNumber())) {
                                    indexToDelete = i;
                                    break;
                                }
                            }
                            if (indexToDelete != -1) {
                                userLikesInComment.remove(indexToDelete);
                            }
                        } else {
                            Log.i(TAG, "Loi roi, size phai < 3");
                        }
                    }
                }
            } else {
                setLikeFeed(isLiked);
                if (getBaseActivity() != null) {
                    getBaseActivity().showToast(R.string.e601_error_but_undefined);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "onClickLikeFeed Exception", e);
            setLikeFeed(isLiked);
            if (getBaseActivity() != null) {
                getBaseActivity().showToast(R.string.e601_error_but_undefined);
            }
        }
    }

    private void sendComment() {
        if (getBaseActivity() == null || mFeedBusiness == null || rest == null || mEditText.getText().toString().length() == 0) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(getBaseActivity())) {
            getBaseActivity().showToast(R.string.no_connectivity_check_again);
            return;
        }
        Log.i(TAG, "send text: " + mEditText.getText().toString());

        ArrayList<TagMocha> mListTagFake = FeedBusiness.getListTagFromListPhoneNumber(mEditText.getUserInfo());
        String textTag = FeedBusiness.getTextTag(mListTagFake);

        String messageContent = TextHelper.trimTextOnMedia(mEditText.getTextTag());
        //messageContent = TextHelper.getInstant().badwordFilter(messageContent);
        mEditText.resetObject();
        Log.i(TAG, "text after getRaw: " + messageContent);
//        addStatus();
        String rowId = "";
        FeedContent feedContent = null;
        if (mFeed != null && currentUrl.equals(urlAction)) {
            rowId = mFeed.getBase64RowId();
            feedContent = mFeed.getFeedContent();
        }
        rest.logAppV6(currentUrl, "", feedContent, FeedModelOnMedia.ActionLogApp.COMMENT, messageContent,
                rowId, textTag, null,
                new ResponseListener(NewsDetailFragment.this, 3), new ResponseErrorListener(NewsDetailFragment.this, 3));

        if (getBaseActivity() != null) {
            getBaseActivity().trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_comment);
        }
    }

    public void onSendCommentResponse(String response) {
        Log.i(TAG, "actionComment: onSendCommentResponse: " + response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                if (getBaseActivity() != null) {
                    if (code == HTTPCode.E200_OK) {
                        getBaseActivity().showToast(R.string.comment_success);
                        numberComment++;
                        mFeed.getFeedContent().setCountComment(numberComment);
                        setNumberComment(numberComment);

                    } else {
                        getBaseActivity().showToast(R.string.e601_error_but_undefined);
                    }
                }
            } else {
                if (getBaseActivity() != null) {
                    getBaseActivity().showToast(R.string.e601_error_but_undefined);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            if (getBaseActivity() != null) {
                getBaseActivity().showToast(R.string.e601_error_but_undefined);
            }
        }
    }

    public NewsModel getNewsModel() {
        return newsModel;
    }

    public void setNumberComment(int number) {
        if (number > 0) {
            numberComment = number;
            TiinUtilities.setVisibilityView(tvNumberComment, true);
            if (number > 99) {
                tvNumberComment.setText("99+");
            } else {
                tvNumberComment.setText(Utilities.shortenLongNumber(number));
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(FeedModelOnMedia noteEvent) {
        if (urlAction.equals(noteEvent.getFeedContent().getUrl())) {
            mFeed = noteEvent;
            setLikeFeed(mFeed.getIsLike() == 1);
            setNumberComment((int) mFeed.getFeedContent().getCountComment());
        }

    }

    private static class ResponseListener implements com.android.volley.Response.Listener<String> {
        WeakReference<NewsDetailFragment> weakReference;
        int type;
        boolean isLiked;

        ResponseListener(NewsDetailFragment fragment, int type, boolean isLiked) {
            weakReference = new WeakReference<>(fragment);
            this.type = type;
            this.isLiked = isLiked;
        }

        ResponseListener(NewsDetailFragment fragment, int type) {
            weakReference = new WeakReference<>(fragment);
            this.type = type;
        }

        @Override
        public void onResponse(String response) {
            if (weakReference == null) return;
            NewsDetailFragment fragment = weakReference.get();
            if (fragment == null) {
                return;
            }
            switch (type) {
                case 1: //like feed response
                    fragment.onLikeFeedResponse(response, fragment.mFeed, isLiked);
                    break;

                case 2: // like response
                    fragment.onLikeResponse(response, isLiked);
                    break;

                case 3: // comment response
                    fragment.onSendCommentResponse(response);
                    break;
            }
        }
    }

    private static class ResponseErrorListener implements Response.ErrorListener {
        WeakReference<NewsDetailFragment> weakReference;
        int type;
        boolean isLiked;

        ResponseErrorListener(NewsDetailFragment fragment, int type, boolean isLiked) {
            weakReference = new WeakReference<>(fragment);
            this.type = type;
            this.isLiked = isLiked;
        }

        ResponseErrorListener(NewsDetailFragment fragment, int type) {
            weakReference = new WeakReference<>(fragment);
            this.type = type;
        }

        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if (weakReference == null) return;
            NewsDetailFragment fragment = weakReference.get();
            if (fragment == null) {
                return;
            }
            switch (type) {
                case 1: //like feed error
                    fragment.setLikeFeed(fragment.mFeed, isLiked);
                    if (fragment.getBaseActivity() != null) {
                        fragment.getBaseActivity().showToast(R.string.e601_error_but_undefined);
                    }
                    break;

                case 2: // like error
                    fragment.setLikeFeed(isLiked);
                    if (fragment.getBaseActivity() != null) {
                        fragment.getBaseActivity().showToast(R.string.e601_error_but_undefined);
                    }
                    break;

                case 3: // comment error
                    Log.i("NewsDetailFragment", "Response error" + volleyError.getMessage());
                    if (fragment.getBaseActivity() != null) {
                        fragment.getBaseActivity().showToast(R.string.e601_error_but_undefined);
                    }
                    break;
            }
        }
    }

}

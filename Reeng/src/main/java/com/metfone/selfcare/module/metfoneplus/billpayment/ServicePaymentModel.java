package com.metfone.selfcare.module.metfoneplus.billpayment;


import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ServicePaymentModel {
    private int img;
    private boolean isSelect;
    private String paymentMethod;

    public ServicePaymentModel(int img, boolean isSelect, String value) {
        this.img = img;
        this.isSelect = isSelect;
        this.paymentMethod = value;
    }

}

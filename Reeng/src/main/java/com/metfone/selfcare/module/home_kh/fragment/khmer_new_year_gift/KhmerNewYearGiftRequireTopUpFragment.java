package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.common.util.Strings;
import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.FragmentKhmerNewYearRequireTopupBinding;
import com.metfone.selfcare.module.home_kh.activity.KhmerNewYearGiftActivity;
import com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox.KhmerNewYearGiftBoxFragment;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.metfoneplus.topup.TopupMetfoneFragment;
import com.metfone.selfcare.util.Utilities;

public class KhmerNewYearGiftRequireTopUpFragment extends BaseFragment {

    public static final String TAG = KhmerNewYearGiftRequireTopUpFragment.class.getSimpleName();

    private FragmentKhmerNewYearRequireTopupBinding binding;
    private KhmerNewYearGiftActivity activity;

    public static KhmerNewYearGiftRequireTopUpFragment newInstance() {
        KhmerNewYearGiftRequireTopUpFragment fragment = new KhmerNewYearGiftRequireTopUpFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentKhmerNewYearRequireTopupBinding.inflate(getLayoutInflater());
        binding.setHandler(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (KhmerNewYearGiftActivity)getActivity();

        Utilities.reCalculateDimensionAnchorView(binding.anchorPointForFrame,
                binding.container,
                binding.imageOriginalSize,
                binding.anchorPointForFrame);
    }

    @Override
    public String getName() {
        return TAG;
    }

    @Override
    public int getResIdView() {
        return 0;
    }


    public void openGiftBoxScreen(){
        activity.playTabSound();
        addFragment(R.id.root_frame, KhmerNewYearGiftBoxFragment.newInstance(), KhmerNewYearGiftBoxFragment.TAG);
    }

    public void openTopUpScreen(){
        activity.playTabSound();
        activity.popBackStackFragment();
        addFragment(R.id.root_frame, TopupMetfoneFragment.newInstance(), TopupMetfoneFragment.class.getSimpleName());
    }
}

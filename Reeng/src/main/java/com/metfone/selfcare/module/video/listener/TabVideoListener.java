/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.video.listener;

import com.metfone.selfcare.listeners.OnClickContentChannel;
import com.metfone.selfcare.listeners.OnClickContentVideo;
import com.metfone.selfcare.module.video.model.VideoPagerModel;

public class TabVideoListener {

    public interface OnAdapterClick extends OnClickContentVideo, OnClickContentChannel {

        void onClickTitleBox(VideoPagerModel item, int position);
    }
}

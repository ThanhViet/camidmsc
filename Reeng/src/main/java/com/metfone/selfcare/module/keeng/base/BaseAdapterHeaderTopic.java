package com.metfone.selfcare.module.keeng.base;

import android.content.Context;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.holder.HeaderTitleHolder;
import com.metfone.selfcare.module.keeng.holder.MediaHolder;
import com.metfone.selfcare.module.keeng.holder.TopicHolder;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class BaseAdapterHeaderTopic extends BaseAdapterRecyclerView {

    private List<AllModel> datas;
    private Topic topic;

    public BaseAdapterHeaderTopic(Context context, Topic topic, List<AllModel> datas, String ga_source) {
        super(context, ga_source);
        this.datas = datas;
        this.topic = topic;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_HEADER_TOPIC;
        }
        AllModel item = getItem(position);
        if (item == null) {
            return ITEM_LOAD_MORE;
        }
        switch (item.getType()) {
            case Constants.TYPE_SONG:
                return ITEM_MEDIA_SONG_HOT;
            case Constants.TYPE_ALBUM:
                return ITEM_MEDIA_ALBUM;
            case Constants.TYPE_VIDEO:
                return ITEM_MEDIA_VIDEO;
            case Constants.TYPE_PLAYLIST:
                return ITEM_MEDIA_ALBUM;
            default:
                return ITEM_MEDIA_ALBUM;
        }
    }

    @Override
    public AllModel getItem(int position) {
        if (datas != null)
            try {
                return datas.get(position - 1);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        return null;
    }

    public int getItemPosition(int position) {
        return position - 1;
    }

    @Override
    public int getItemCount() {
        if (datas != null)
            return datas.size() + 1;
        return 1;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {

        if (holder instanceof TopicHolder) {
            TopicHolder itemHolder = (TopicHolder) holder;
            itemHolder.tvTitle.setText(mContext.getResources().getString(R.string.topic_cungnghe));
            String str = mContext.getResources().getString(R.string.topic_mocha);
            str = str.replace("#listen", topic.getTotalViews() + "");
            itemHolder.tvContent.setText(str);
            if (topic.getTotalViews() > 0) {
                itemHolder.tvContent.setVisibility(View.VISIBLE);
            } else {
                itemHolder.tvContent.setVisibility(View.GONE);
            }
            itemHolder.tvListened.setText(mContext.getResources().getString(R.string.topic_mocha_message));
            ImageBusiness.setAvatarProfile(topic.getAvatar(), itemHolder.ivAvatar);
            ImageBusiness.setCover(topic.getCover(), itemHolder.ivBg, (int) topic.getId());
        } else if (holder instanceof MediaHolder) {
            final AllModel item = getItem(position);
            MediaHolder itemHolder = (MediaHolder) holder;
            itemHolder.bind(mContext, item);
        } else if (holder instanceof HeaderTitleHolder) {
            HeaderTitleHolder itemHolder = (HeaderTitleHolder) holder;
            itemHolder.tvTitle.setText(topic.getName());
        }
    }

}

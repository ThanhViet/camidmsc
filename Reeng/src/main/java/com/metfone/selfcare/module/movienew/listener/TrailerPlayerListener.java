package com.metfone.selfcare.module.movienew.listener;

public interface TrailerPlayerListener {
    void dismissTrailerDialog(boolean continuePlay,boolean isEnd,long currentTime);
}

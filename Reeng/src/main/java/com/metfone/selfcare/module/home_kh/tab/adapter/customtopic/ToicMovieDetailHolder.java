/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.home_kh.tab.adapter.customtopic;

import android.app.Activity;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.ui.androidtagview.TagContainerLayout;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ToicMovieDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.tv_total_view)
    @Nullable
    TextView tvTotalView;
    @BindView(R.id.tv_duration)
    @Nullable
    TextView tvDuration;
    @BindView(R.id.button_option)
    @Nullable
    View btnOption;
    @BindView(R.id.tag_view)
    @Nullable
    TagContainerLayout tagContainerLayout;
    @BindView(R.id.iv_like)
    @Nullable
    View ivLike;
    @BindView(R.id.progress_bar)
    @Nullable
    ProgressBar progressBar;
    @BindView(R.id.layout_title)
    @Nullable
    View layout_title;

    private Object data;
    private Resources res;
    private boolean showPoster = false;
    private String cateIdSubTab = null;

    public ToicMovieDetailHolder(View view, Activity activity, final OnClickContentMovie listener) {
        super(view);
        res = activity.getResources();
        initListener(listener);
    }

    public ToicMovieDetailHolder(View view, Activity activity, OnClickContentMovie listener, int widthLayout) {
        super(view);
        res = activity.getResources();
        initListener(listener);
        if (widthLayout > 0) {
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = widthLayout;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
    }

    public ToicMovieDetailHolder setShowPoster(boolean showPoster) {
        this.showPoster = showPoster;
        return this;
    }

    public ToicMovieDetailHolder setCateIdSubTab(String cateIdSubTab) {
        this.cateIdSubTab = cateIdSubTab;
        return this;
    }

    private void initListener(final OnClickContentMovie listener) {
        if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMovieItem(data, getAdapterPosition());
                }
            }
        });
        if (btnOption != null) btnOption.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMoreMovieItem(data, getAdapterPosition());
                }
            }
        });
        if (ivLike != null) ivLike.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickLikeMovieItem(data, getAdapterPosition());
                }
            }
        });
    }

    private void bindDataHome(Content model) {
        if (tvTitle != null) tvTitle.setText(model.getName());
        boolean showDot = false;
        long totalEpisodes = model.getTotalEpisodes();
        if (tvDesc != null) {
            int durationMinutes = model.getDurationMinutes();
            if (totalEpisodes > 1) {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(res.getString(R.string.total_episodes, totalEpisodes));
                showDot = true;
            } else if (durationMinutes > 0) {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(res.getString(R.string.total_duration_minutes, durationMinutes));
                showDot = true;
            } else {
                tvDesc.setVisibility(View.GONE);
            }
        }
        if (tvTotalView != null) {
            long totalViews = model.getTotalViews();
            if (totalViews > 0) {
                tvTotalView.setVisibility(View.VISIBLE);
                tvTotalView.setText(String.format((totalViews == 1) ? res.getString(R.string.view)
                        : res.getString(R.string.video_views), Utilities.getTotalView(totalViews)));
                tvTotalView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
            } else {
                tvTotalView.setVisibility(View.GONE);
            }
        }
        if (tvDuration != null) {
            int durationInt = model.getDurationInt();
            if (durationInt > 0) {
                tvDuration.setText(DateTimeUtils.getDuration(durationInt));
                tvDuration.setVisibility(View.VISIBLE);
            } else {
                tvDuration.setVisibility(View.GONE);
            }
        }
        //if (btnOption != null) btnOption.setVisibility(showOption ? View.VISIBLE : View.GONE);
        if (tagContainerLayout != null) {
            tagContainerLayout.removeAllTags();
            ArrayList<int[]> colors = new ArrayList<>();
            List<String> tags = new ArrayList<>();
            if (totalEpisodes > 1 && Utilities.notEmpty(model.getChapter())) {
                if (totalEpisodes == model.getChapterInt()) {
                    tags.add(res.getString(R.string.movies_full_set));
                    colors.add(Constants.COLOR_FULL_SET);
                } else {
                    tags.add(res.getString(R.string.episode_movies, model.getChapter()));
                    colors.add(Constants.COLOR_EPISODE);
                }
            }
            if (model.isSubtitleFilm()) {
                tags.add(res.getString(R.string.subtitle));
                colors.add(Constants.COLOR_SUBTITLE);
            }
            if (model.isNarrativeFilm()) {
                tags.add(res.getString(R.string.narrative));
                colors.add(Constants.COLOR_NARRATIVE);
            }
            if (tags.isEmpty()) {
                tagContainerLayout.setVisibility(View.GONE);
            } else {
                tagContainerLayout.setVisibility(View.VISIBLE);
                tagContainerLayout.setTags(tags, colors);
            }
        }
        if (showPoster)
            ImageBusiness.setPosterMovie(model.getImage(), ivCover);
        else
            ImageBusiness.setCoverMovie(model.getImage(), ivCover);
    }

    private void bindData(Video model) {
        if (tvTitle != null) {
            if (model.getFilmGroup() != null && !TextUtils.isEmpty(model.getFilmGroup().getGroupName())) {
                tvTitle.setText(model.getFilmGroup().getGroupName());
            } else if (!TextUtils.isEmpty(model.getTitle())) {
                tvTitle.setText(model.getTitle());
            } else {
                tvTitle.setText("");
            }
        }
        boolean showDot = false;
        if (tvDesc != null) {
            long totalEpisodes = model.getTotalEpisodes();
            int durationMinutes = model.getDurationMinutes();
            if (totalEpisodes > 1) {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(res.getString(R.string.total_episodes, totalEpisodes));
                showDot = true;
            } else if (durationMinutes > 0) {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(res.getString(R.string.total_duration_minutes, durationMinutes));
                showDot = true;
            } else {
                tvDesc.setVisibility(View.GONE);
            }
        }
        if (tvTotalView != null) {
            long totalViews = model.getTotalView();
            if (totalViews > 0) {
                tvTotalView.setVisibility(View.VISIBLE);
                tvTotalView.setText(String.format((totalViews == 1) ? res.getString(R.string.view)
                        : res.getString(R.string.video_views), Utilities.getTotalView(totalViews)));
                tvTotalView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
            } else {
                tvTotalView.setVisibility(View.GONE);
            }
        }
        if (tvDuration != null) tvDuration.setVisibility(View.GONE);
        //if (btnOption != null) btnOption.setVisibility(showOption ? View.VISIBLE : View.GONE);
        if (tagContainerLayout != null) {
            tagContainerLayout.removeAllTags();
            ArrayList<int[]> colors = new ArrayList<>();
            List<String> tags = new ArrayList<>();
            String currentEpisode = "";
            if (model.getFilmGroup() != null)
                currentEpisode = model.getFilmGroup().getCurrentVideo();
            if (Utilities.notEmpty(currentEpisode)) {
                //tags.add(res.getString(R.string.episode_movies, currentEpisode));
                tags.add(currentEpisode);
                colors.add(Constants.COLOR_EPISODE);
            }
            if (model.isSubtitleFilm()) {
                tags.add(res.getString(R.string.subtitle));
                colors.add(Constants.COLOR_SUBTITLE);
            }
            if (model.isNarrativeFilm()) {
                tags.add(res.getString(R.string.narrative));
                colors.add(Constants.COLOR_NARRATIVE);
            }
            if (tags.isEmpty()) {
                tagContainerLayout.setVisibility(View.GONE);
            } else {
                tagContainerLayout.setVisibility(View.VISIBLE);
                tagContainerLayout.setTags(tags, colors);
            }
        }
        if (showPoster)
            ImageBusiness.setPosterMovie(model.getPosterPath(), ivCover);
        else
            ImageBusiness.setCoverMovie(model.getImagePath(), ivCover);
    }

    private void bindData(Movie model) {
        if (tvTitle != null) tvTitle.setText(model.getName());
        boolean showDot = false;
        int totalEpisodes = model.getTotalEpisodes();
        if (tvDesc != null) {
            int durationMinutes = model.getDurationMinutes();
            if (model.isSerial() && totalEpisodes > 1) {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(res.getString(R.string.total_episodes, totalEpisodes));
                showDot = true;
            } else if (durationMinutes > 0) {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(res.getString(R.string.total_duration_minutes, durationMinutes));
                showDot = true;
            } else {
                tvDesc.setVisibility(View.GONE);
            }
        }
        if (tvTotalView != null) {
            int totalViews = model.getIsView();
            if (totalViews > 0) {
                tvTotalView.setVisibility(View.VISIBLE);
                tvTotalView.setText(String.format((totalViews == 1) ? res.getString(R.string.view)
                        : res.getString(R.string.video_views), Utilities.getTotalView(totalViews)));
                tvTotalView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
            } else {
                tvTotalView.setVisibility(View.GONE);
            }
        }
        if (tvDuration != null) {
            int durationInt = model.getDurationInt();
            if (durationInt > 0) {
                tvDuration.setText(DateTimeUtils.getDuration(durationInt));
                tvDuration.setVisibility(View.VISIBLE);
            } else {
                tvDuration.setVisibility(View.GONE);
            }
        }
        //if (btnOption != null) btnOption.setVisibility(showOption ? View.VISIBLE : View.GONE);
        if (tagContainerLayout != null) {
            tagContainerLayout.removeAllTags();
            ArrayList<int[]> colors = new ArrayList<>();
            List<String> tags = new ArrayList<>();
            if (totalEpisodes > 1 && model.getCurrentEpisode() > 0) {
                if (totalEpisodes == model.getCurrentEpisode()) {
                    tags.add(res.getString(R.string.movies_full_set));
                    colors.add(Constants.COLOR_FULL_SET);
                } else {
                    tags.add(res.getString(R.string.episode_movies, String.valueOf(model.getCurrentEpisode())));
                    colors.add(Constants.COLOR_EPISODE);
                }
            }
            if (model.isSubtitleFilm()) {
                tags.add(res.getString(R.string.subtitle));
                colors.add(Constants.COLOR_SUBTITLE);
            }
            if (model.isNarrativeFilm()) {
                tags.add(res.getString(R.string.narrative));
                colors.add(Constants.COLOR_NARRATIVE);
            }
            if (tags.isEmpty()) {
                tagContainerLayout.setVisibility(View.GONE);
            } else {
                tagContainerLayout.setVisibility(View.VISIBLE);
                tagContainerLayout.setTags(tags, colors);
            }
        }
        if (showPoster)
            ImageBusiness.setPosterMovie(model.getPosterPath(), ivCover);
        else
            ImageBusiness.setCoverMovie(model.getImagePath(), ivCover);
    }

    private void bindData(MovieWatched model) {
        if (tvTitle != null) tvTitle.setText(model.getName());
        if (tvDesc != null) {
            tvDesc.setVisibility(View.GONE);
        }
        if (tvTotalView != null) {
            tvTotalView.setVisibility(View.GONE);
        }
        int durationInt = model.getDurationInt();
        if (tvDuration != null) {
            if (durationInt > 0) {
                tvDuration.setText(DateTimeUtils.getDuration(durationInt));
                tvDuration.setVisibility(View.VISIBLE);
            } else {
                tvDuration.setVisibility(View.GONE);
            }
        }
        if (progressBar != null) {
            if (durationInt > 0) {
                progressBar.setMax(durationInt);
                progressBar.setProgress(model.getTimeSeekInt());
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
//        if (btnOption != null) btnOption.setVisibility(showOption ? View.VISIBLE : View.GONE);
        if (tagContainerLayout != null) {
            tagContainerLayout.setVisibility(View.GONE);
        }
        if (showPoster)
            ImageBusiness.setPosterMovie(model.getPosterPath(), ivCover);
        else
            ImageBusiness.setCoverMovie(model.getImagePath(), ivCover);
    }

    @Override
    public void bindData(Object item, int position) {
        data = item;
        if (item instanceof Content) {
            bindDataHome((Content) item);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                bindDataHome((Content) model.getObject());
            }
        } else if (item instanceof Video) {
            bindData((Video) item);
        } else if (item instanceof Movie) {
            if (cateIdSubTab != null && (cateIdSubTab.equals(MovieKind.CATEGORYID_GET_LIKED)
                    || cateIdSubTab.equals(MovieKind.CATEGORYID_GET_LIKED_ODD)
                    || cateIdSubTab.equals(MovieKind.CATEGORYID_GET_LIKED_SERIES))) {
                bindDataLiked((Movie) item);
            } else {
                bindData((Movie) item);
            }
        } else if (item instanceof MoviePagerModel) {
            MoviePagerModel provisional = (MoviePagerModel) item;
            if (provisional.getObject() instanceof Movie)
                bindData((Movie) provisional.getObject());
        } else if (item instanceof MovieWatched) {
            bindData((MovieWatched) item);
        } else if (item instanceof KhHomeMovieItem) {
            bindDataKhHome((KhHomeMovieItem) item);
        }
    }

    private void bindDataKhHome(KhHomeMovieItem model) {
        ImageBusiness.setCoverMovie(model.poster_path, ivCover);
        assert layout_title != null;
        layout_title.setVisibility(View.GONE);
        assert tagContainerLayout != null;
        tagContainerLayout.setVisibility(View.GONE);

    }

    private void bindDataLiked(Movie model) {
        if (tvTitle != null) tvTitle.setText(model.getName());
        boolean showDot = false;
        int totalEpisodes = model.getTotalEpisodes();
        if (tvDesc != null) {
//            int durationMinutes = model.getDurationMinutes();
            if (model.isSerial() && totalEpisodes > 1) {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(res.getString(R.string.total_episodes, totalEpisodes));
                showDot = true;
            }
//            else if (durationMinutes > 0) {
//                tvDesc.setVisibility(View.VISIBLE);
//                tvDesc.setText(res.getString(R.string.total_duration_minutes, durationMinutes));
//                showDot = true;
//            }
            else {
                tvDesc.setVisibility(View.GONE);
            }
        }
        if (tvTotalView != null) {
            int totalViews = model.getIsView();
            if (totalViews > 0) {
                tvTotalView.setVisibility(View.VISIBLE);
                tvTotalView.setText(String.format((totalViews == 1) ? res.getString(R.string.view)
                        : res.getString(R.string.video_views), Utilities.getTotalView(totalViews)));
                tvTotalView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
            } else {
                tvTotalView.setVisibility(View.GONE);
            }
        }
        if (tvDuration != null) {
            int durationInt = model.getDurationInt();
            if (durationInt > 0) {
                tvDuration.setText(DateTimeUtils.getDuration(durationInt));
                tvDuration.setVisibility(View.VISIBLE);
            } else {
                tvDuration.setVisibility(View.GONE);
            }
        }
        //if (btnOption != null) btnOption.setVisibility(showOption ? View.VISIBLE : View.GONE);
        if (tagContainerLayout != null) {
            tagContainerLayout.removeAllTags();
            ArrayList<int[]> colors = new ArrayList<>();
            List<String> tags = new ArrayList<>();
            if (totalEpisodes > 1 && model.getCurrentEpisode() > 0) {
                if (totalEpisodes == model.getCurrentEpisode()) {
                    tags.add(res.getString(R.string.movies_full_set));
                    colors.add(Constants.COLOR_FULL_SET);
                } else {
                    tags.add(res.getString(R.string.episode_movies, String.valueOf(model.getCurrentEpisode())));
                    colors.add(Constants.COLOR_EPISODE);
                }
            }
            if (model.isSubtitleFilm()) {
                tags.add(res.getString(R.string.subtitle));
                colors.add(Constants.COLOR_SUBTITLE);
            }
            if (model.isNarrativeFilm()) {
                tags.add(res.getString(R.string.narrative));
                colors.add(Constants.COLOR_NARRATIVE);
            }
            if (tags.isEmpty()) {
                tagContainerLayout.setVisibility(View.GONE);
            } else {
                tagContainerLayout.setVisibility(View.VISIBLE);
                tagContainerLayout.setTags(tags, colors);
            }
        }
        if (showPoster)
            ImageBusiness.setPosterMovie(model.getPosterPath(), ivCover);
        else
            ImageBusiness.setCoverMovie(model.getImagePath(), ivCover);
    }
}
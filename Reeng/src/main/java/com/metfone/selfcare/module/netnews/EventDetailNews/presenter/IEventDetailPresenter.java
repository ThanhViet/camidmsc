package com.metfone.selfcare.module.netnews.EventDetailNews.presenter;


import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;

/**
 * Created by HaiKE on 8/19/17.
 */

public interface IEventDetailPresenter extends MvpPresenter {

    void loadData(int cateId, int page);
}

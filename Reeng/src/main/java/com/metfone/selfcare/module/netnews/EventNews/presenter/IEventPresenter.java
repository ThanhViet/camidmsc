package com.metfone.selfcare.module.netnews.EventNews.presenter;


import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;

/**
 * Created by HaiKE on 8/19/17.
 */

public interface IEventPresenter extends MvpPresenter {

    void loadData(int page);
}

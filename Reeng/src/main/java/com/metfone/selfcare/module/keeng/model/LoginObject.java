package com.metfone.selfcare.module.keeng.model;

import android.content.Context;

import com.metfone.selfcare.app.dev.ApplicationController;

import java.io.Serializable;

public class LoginObject implements Serializable {
    public static boolean isLogin(Context context) {
        return true;
    }

    public static String getSessionToken(Context context) {
        return "";
    }

    public static int getId(Context context) {
        return 0;
    }

    public static String getPhoneNumber(Context context) {
        try
        {
            ApplicationController mApplication = (ApplicationController) context.getApplicationContext();
            return mApplication.getReengAccountBusiness().getCurrentAccount().getJidNumber();
        }
        catch (NullPointerException ex)
        {
            return "";
        }
    }
}

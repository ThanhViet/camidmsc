package com.metfone.selfcare.module.metfoneplus.ishare.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class IshareContactAdapter extends RecyclerView.Adapter<IshareContactAdapter.ContactHolder> {
    private Context context;
    private ArrayList<PhoneNumber> phoneNumbers;
    private IShareContactListener listener;
    private int indexContactSelect = -1;
    private ApplicationController mApplication;

    public IshareContactAdapter(Context context, ArrayList<PhoneNumber> phoneNumbers, ApplicationController mApplication, IShareContactListener listener) {
        this.context = context;
        this.phoneNumbers = phoneNumbers;
        this.listener = listener;
        this.mApplication = mApplication;
    }

    @NonNull
    @Override
    public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_ishare, parent, false);
        return new ContactHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactHolder holder, int position) {
        PhoneNumber phoneNumber = phoneNumbers.get(position);
        if (indexContactSelect == position || phoneNumber.getSectionType() == 124) {
            holder.root.setBackground(context.getResources().getDrawable(R.drawable.bg_ishare_contact_select));
            indexContactSelect = -1;
        } else {
            holder.root.setBackgroundColor(Color.TRANSPARENT);
        }
//        Glide.with(context).load(phoneNumber.getImageCover().getImageUrl()).into(holder.avatar);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(holder.avatar, holder.contactAvatarText, phoneNumber, 56);
        holder.name.setText(phoneNumber.getName());
        String nationalPhone = Utilities.getNationalPhoneNumber(phoneNumber.getJidNumber(), "KH");
        holder.phone.setText(nationalPhone);
    }

    @Override
    public int getItemCount() {
        if (phoneNumbers == null)
            return 0;
        return phoneNumbers.size();
    }

    public class ContactHolder extends RecyclerView.ViewHolder {
        RelativeLayout root;
        CircleImageView avatar;
        TextView name;
        TextView phone;
        TextView contactAvatarText;

        public ContactHolder(@NonNull View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.root_view);
            avatar = itemView.findViewById(R.id.avatar);
            name = itemView.findViewById(R.id.name);
            phone = itemView.findViewById(R.id.number_phone);
            contactAvatarText = itemView.findViewById(R.id.contact_avatar_text);
            root.setOnClickListener(v -> {
                indexContactSelect = getAdapterPosition();
                listener.selectContact(getAdapterPosition(), phoneNumbers.get(getAdapterPosition()));
                notifyDataSetChanged();
            });
        }
    }

    public interface IShareContactListener {
        void selectContact(int position, PhoneNumber phoneNumber);
    }
}

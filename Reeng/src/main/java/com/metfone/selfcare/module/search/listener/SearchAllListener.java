/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.listener;

import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.search.model.CreateThreadChat;
import com.metfone.selfcare.module.search.model.SearchHistory;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

public class SearchAllListener {

    public interface OnAdapterClick {
        void onClickMenuMore(Object data);
    }

    public interface OnClickBoxThreadChat extends OnAdapterClick {

        void onClickThreadChatItem(Object item);

        void onClickThreadChatMore();

        void onClickLogin();

        void onClickCreateThreadMessage(CreateThreadChat item);
    }

    public interface OnClickBoxVideo extends OnAdapterClick {

        void onClickVideoItem(Video item);

        void onClickChannelItem(Channel item);

        void onClickSubscribeChannelItem(Channel item);

        void onClickVideoMore();

        void onClickChannelMore();
    }

    public interface OnClickBoxNews extends OnAdapterClick {

        void onClickNewsItem(NewsModel item);

        void onClickNewsMore();
    }

    public interface OnClickBoxMovies extends OnAdapterClick {

        void onClickMoviesItem(Movie item);

        void onClickMoviesMore();
    }

    public interface OnClickBoxMusic extends OnAdapterClick {

        void onClickMusicItem(SearchModel item);

        void onClickMusicMore();

        void onClickMVMore();
    }

    public interface OnClickBoxHistory extends OnAdapterClick {

        void onClickHistorySearchItem(SearchHistory item);

        void onClickDeleteHistorySearch();

        void onClickDeleteItemHistorySearch(SearchHistory item);
    }
    public interface OnClickBoxTiin extends OnAdapterClick {

        void onClickTiinItem(TiinModel item);

        void onClickTiinMore();
    }

    public interface OnClickReward extends OnAdapterClick{
        void onClickReward(SearchRewardResponse.Result.ItemReward item);

        void onClickRewardMore();
    }

}

/*
package com.metfone.selfcare.module.selfcare.loginhelper;

import android.os.AsyncTask;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.listeners.LoginStateListener;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;

import org.jivesoftware.smack.Connection;

*/
/**
 * Created by thanhnt72 on 5/8/2019.
 *//*


public class LoginByCodeMytelAsyncTask extends AsyncTask<Boolean, XMPPResponseCode, XMPPResponseCode> {

    private static final String TAG = LoginByCodeMytelAsyncTask.class.getSimpleName();


    private String jid, otp, countryCode, accessToken;
    private LoginStateListener listener;

    public LoginByCodeMytelAsyncTask(String jid, String otp, String countryCode, String accessToken, LoginStateListener listener) {
        this.jid = jid;
        this.otp = otp;
        this.countryCode = countryCode;
        this.accessToken = accessToken;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected XMPPResponseCode doInBackground(Boolean[] params) {
        LoginBusiness loginBusiness = ApplicationController.self().getLoginBusiness();
        XMPPResponseCode responseCode = loginBusiness.loginByCode(ApplicationController.self(), jid,
                otp, countryCode, false, Connection.CODE_AUTH_NON_SASL, null, null);
        return responseCode;
    }

    @Override
    protected void onPostExecute(XMPPResponseCode responseCode) {
        super.onPostExecute(responseCode);
        com.metfone.selfcare.util.Log.i(TAG, "LoginByCodeAsyncTask autologin responseCode: " + responseCode);
        try {
            if (responseCode.getCode() == XMPPCode.E200_OK) {
                com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                ReengAccountBusiness reengAccountBusiness = ApplicationController.self().getReengAccountBusiness();
                ReengAccount reengAccount = reengAccountBusiness.getCurrentAccount();
                reengAccount.setNumberJid(jid);
                reengAccount.setRegionCode(countryCode);
                reengAccountBusiness.setAnonymous(false);
                reengAccountBusiness.updateReengAccount(reengAccount);
                ApplicationController.self().getConfigBusiness().init();
                reengAccountBusiness.checkAndSendIqGetLocation();
                ApplicationController.self().loadDataAfterLogin();
                ApplicationController.self().getApplicationComponent().provideUserApi().unregisterRegid();
                if (listener != null) listener.onLoginSuccess(accessToken);
                */
/*reengAccountBusiness.clearContactData(ApplicationController.self());
                ApplicationController.self().getContactBusiness().clearMemoryDataContact();
                ApplicationController.self().getConfigBusiness().deleteAllKeyConfig();
                ApplicationController.self().getMusicBusiness().resetStrangerList();*//*


            } else {
                if (listener != null) listener.onLoginError("");
            }
        } catch (Exception e) {
            com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
            if (listener != null) listener.onLoginError("");
        }
    }
}*/

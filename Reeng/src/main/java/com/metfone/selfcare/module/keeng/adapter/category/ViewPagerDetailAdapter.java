package com.metfone.selfcare.module.keeng.adapter.category;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by viettel_media on 2/23/18.
 */

public class ViewPagerDetailAdapter extends FragmentStatePagerAdapter {
    static final String TAG = "ViewPagerDetailAdapter";
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerDetailAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        try {
            return mFragmentList.get(position);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return null;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public void removeFragment(int index) {
        if (mFragmentList.size() <= index) return;
        mFragmentList.remove(index);
        notifyDataSetChanged();
    }

    @Override
    public String getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
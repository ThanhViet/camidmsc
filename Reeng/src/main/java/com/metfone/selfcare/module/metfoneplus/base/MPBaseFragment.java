package com.metfone.selfcare.module.metfoneplus.base;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.model.camid.DetailType;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.metfoneplus.CircleOverlayView;
import com.metfone.selfcare.module.metfoneplus.exchangedamaged.fragment.ExchangeDamagedListFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddFeedbackFragment;
import com.metfone.selfcare.module.metfoneplus.billpayment.MPBillPaymentFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPApplyForYouCurrentSimFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPApplyingSimFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPContactUsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailAccountFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailSmsCallDataFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPExchangeFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackInternetFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackMobileFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackSwitchFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackUpdateFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackUsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFragmentOrderNumber;
import com.metfone.selfcare.module.metfoneplus.fragment.MPHistoryNewSimFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPOrderNewSimFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPRootFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPServiceFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPStoreFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPStoreSearchFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPSupportFragment;
import com.metfone.selfcare.module.metfoneplus.ishare.fragment.IShareFragment;
import com.metfone.selfcare.module.metfoneplus.search.fragment.BuyPhoneNumberFragment;
import com.metfone.selfcare.module.metfoneplus.search.fragment.DetailPhoneNumberFragment;
import com.metfone.selfcare.module.metfoneplus.search.fragment.ListNumberFragment;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.module.metfoneplus.topup.HistoryTopUpMPFragment;
import com.metfone.selfcare.module.metfoneplus.topup.QrCodeTopUpFragment;
import com.metfone.selfcare.module.metfoneplus.topup.ServiceTopUpFragment;
import com.metfone.selfcare.module.metfoneplus.topup.TopupMetfoneFragment;
import com.metfone.selfcare.module.metfoneplus.topup.model.Constants;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.request.WsSearchNumberToBuyRequest;
import com.metfone.selfcare.util.FragmentUtils;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;


public abstract class MPBaseFragment extends Fragment {
    public final String TAG = this.getClass().getSimpleName();
    public ApplicationController mApplication;
    public BaseSlidingFragmentActivity mParentActivity;
    public Resources mRes;
    public Unbinder mUnbinder;
    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;
    private CamIdUserBusiness mCamIdUserBusiness;
    private KhHomeClient khHomeClient;

    public MPBaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mParentActivity = (BaseSlidingFragmentActivity) context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mApplication.getResources();
        this.mCamIdUserBusiness = mApplication.getCamIdUserBusiness();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }


    /**
     * Every fragment has to inflate a layout in the onCreateView method. We have added this method to
     * avoid duplicate all the inflate code in every fragment. You only have to return the layout to
     * inflate in this method when extends BaseFragment.
     */
    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    public void addFragmentTransactionAllowLoss(Fragment fragment, String tag) {
//        mFragmentTransaction = mFragmentManager.beginTransaction()
//                .setCustomAnimations(
//                        R.anim.fif_slide_in,  // enter
//                        R.anim.fif_fade_out,  // exit
//                        R.anim.fif_fade_in,   // popEnter
//                        R.anim.fif_slide_out  // popExit
//                );
//        mFragmentTransaction.add(R.id.metfone_root_frame, fragment, tag);
//        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        mFragmentTransaction.addToBackStack(tag);
//        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replaceMPMapFragmentTransactionAllowLoss(int containerViewId, Fragment fragment, String tag) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(containerViewId, fragment, tag);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replaceFragmentWithAnimationDefault(Fragment fragment) {
        FragmentUtils.replaceFragmentWithAnimationDefault(mFragmentManager, R.id.root_frame, fragment);
    }

    public void popBackStackFragment() {
        mFragmentManager.popBackStack();
        Log.e(TAG, "popBackStackFragment: Done");
    }

    public void gotoMPDetailFragment(boolean isGuideLine) {
        if (isGuideLine) {
            if (EventBus.getDefault().isRegistered(this)) {
                MPDetailsFragment f = MPDetailsFragment.newInstance();
            }

        } else {
            logApp(com.metfone.selfcare.helper.Constants.LOG_APP.BALANCE_INFO);
            MPDetailsFragment f = MPDetailsFragment.newInstance();
            f.setIsShowAnimation(false);
            replaceFragmentWithAnimationDefault(f);
        }
    }

    public void gotoMPTopUpFragment() {
        logApp(com.metfone.selfcare.helper.Constants.LOG_APP.SERVICE_TOP_UP);
        replaceFragmentWithAnimationDefault(TopupMetfoneFragment.newInstance());
    }

    public void gotoMPExchangeFragment() {
        replaceFragmentWithAnimationDefault(MPExchangeFragment.newInstance());
    }

    public void gotoMPServiceTopUpFragment(String phone, String amount) {
        ServiceTopUpFragment serviceTopUpFragment = ServiceTopUpFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PHONE, phone);
        bundle.putString(Constants.AMOUNT, amount);
        serviceTopUpFragment.setArguments(bundle);
        replaceFragmentWithAnimationDefault(serviceTopUpFragment);
    }

    public void gotoMPQrCodeFragment(String numberPhone) {
        replaceFragmentWithAnimationDefault(QrCodeTopUpFragment.newInstance(numberPhone));
    }

    public void gotoHistoryTopUpFragment() {
        replaceFragmentWithAnimationDefault(HistoryTopUpMPFragment.newInstance());
    }

    public void gotoMPSupportFragment() {
        mParentActivity.isFromInternet = false;
        logApp(com.metfone.selfcare.helper.Constants.LOG_APP.SERVICE_SUPPORT);
        replaceFragmentWithAnimationDefault(MPSupportFragment.newInstance());
    }

    public void gotoMPRootFragment() {
        replaceFragmentWithAnimationDefault(MPRootFragment.
                newInstance());
    }

    public void gotoMPHistoryNewSimFragment() {
        replaceFragmentWithAnimationDefault(MPHistoryNewSimFragment.newInstance());
    }

    public void gotoMPApplyForYouCurrentSimFragment(AvailableNumber phone, int orderType, int commitDuration) {
        replaceFragmentWithAnimationDefault(MPApplyForYouCurrentSimFragment.newInstance(phone, orderType, commitDuration));
    }

    public void gotoMPDetailAccountFragment() {
        replaceFragmentWithAnimationDefault(MPDetailAccountFragment.newInstance());
    }

    public void gotoMPApplyingSimFragment(AvailableNumber availableNumber, String otp, int orderType, int duration) {
        replaceFragmentWithAnimationDefault(MPApplyingSimFragment.newInstance(availableNumber, otp, orderType, duration));
    }

    public void gotoMPServiceFragment(int tabPosition) {
        replaceFragmentWithAnimationDefault(MPServiceFragment.newInstance(tabPosition));
    }

    public void gotoMPHistoryChargeFragment(String titleScreen,
                                            @DetailType.DateName String dateName,
                                            @DetailType.BalanceName String balanceName,
                                            @DetailType.ChargeType String chargeType,
                                            @DetailType.DateType String dateType) {
        MPDetailSmsCallDataFragment f = MPDetailSmsCallDataFragment.newInstance(
                titleScreen,
                dateName,
                balanceName,
                chargeType,
                dateType);
        FragmentUtils.replaceFragmentWithAnimationDefault(mParentActivity.getSupportFragmentManager(), R.id.root_frame, f);
    }

    public void gotoMPFeedbackUs() {
        logApp(com.metfone.selfcare.helper.Constants.LOG_APP.FEEDBACK_US);
        replaceFragmentWithAnimationDefault(MPFeedbackUsFragment.newInstance());
    }

    public void gotoMPAddFeedbackFragment() {
//        FragmentUtils.replaceFragmentWithAnimationDefault(mParentActivity.getSupportFragmentManager(), R.id.frameLayoutSwitch, MPFeedbackSwitchFragment.newInstance());
        replaceFragmentWithAnimationDefault(MPFeedbackSwitchFragment.newInstance());
//        replaceFragmentWithAnimationDefault(MPAddFeedbackFragment.newInstance());
    }

    public void gotoMPFeedbackMobile() {
        FragmentUtils.replaceFragmentWithAnimationDefault(mParentActivity.getSupportFragmentManager(), R.id.frameLayoutSwitch, MPFeedbackMobileFragment.newInstance());
    }

    public void gotoMPFeedbackInternet() {
        FragmentUtils.replaceFragmentWithAnimationDefault(mParentActivity.getSupportFragmentManager(), R.id.frameLayoutSwitch, MPFeedbackInternetFragment.newInstance());
    }

    public void gotoMPFeedbackUpdate(String complainId, String errorPhone, String complainerPhone, String acceptDate, String proLimitDate, String staffInfo, String serviceTypeName, String groupTypeName, String staffPhone, String compContent) {
        replaceFragmentWithAnimationDefault(MPFeedbackUpdateFragment.newInstance(complainId, errorPhone, complainerPhone, acceptDate, proLimitDate, staffInfo, serviceTypeName, groupTypeName, staffPhone, compContent));
    }


    public void gotoMPContactUs() {
        replaceFragmentWithAnimationDefault(MPContactUsFragment.newInstance());
    }

    public void gotoMPStoreFragment() {
        replaceFragmentWithAnimationDefault(MPStoreFragment.newInstance());
    }

    public void gotoMPStoreSearchFragment() {
        replaceFragmentWithAnimationDefault(MPStoreSearchFragment.newInstance());
    }

    public void gotoBuyPhoneNumberFragment(Boolean success) {
        logApp(com.metfone.selfcare.helper.Constants.LOG_APP.SERVICE_BUY_NUMBER);
        replaceFragmentWithAnimationDefault(BuyPhoneNumberFragment.newInstance(success));
    }

    public void gotoOrderNumberFragment(AvailableNumber availableNumber, int commitmentDuration) {
        replaceFragmentWithAnimationDefault(MPFragmentOrderNumber.newInstance(availableNumber, commitmentDuration));
    }

    public void gotoMPOrderNewSimFragment(String mes) {
        replaceFragmentWithAnimationDefault(MPOrderNewSimFragment.newInstance(mes));
    }

    public void gotoSearchNumberToBuyFragment(ArrayList<AvailableNumber> availableNumbers, WsSearchNumberToBuyRequest request, int commitmentDuration, int fee) {
        replaceFragmentWithAnimationDefault(ListNumberFragment.newInstance(availableNumbers, request, commitmentDuration, fee));
    }

    public void gotoDetailPhoneNumberFragment(AvailableNumber availableNumber, int commitmentDuration, int fee) {
        replaceFragmentWithAnimationDefault(DetailPhoneNumberFragment.newInstance(availableNumber, commitmentDuration, fee));
    }

    public void gotoIShareFragment() {
        logApp(com.metfone.selfcare.helper.Constants.LOG_APP.SERVICE_ISHARE);
        replaceFragmentWithAnimationDefault(IShareFragment.newInstance());
    }
    public void gotoExchangeDamagedFragment(){
        //TODO goto Exchange Damaged Screen
//        logApp(com.metfone.selfcare.helper.Constants.LOG_APP.SERVICE_ISHARE);
        replaceFragmentWithAnimationDefault(ExchangeDamagedListFragment.newInstance());
    }

    public void gotoBillPaymentFragment() {
        replaceFragmentWithAnimationDefault(MPBillPaymentFragment.newInstance(""));
    }

    public CamIdUserBusiness getCamIdUserBusiness() {
        if (mCamIdUserBusiness == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        }
        return mCamIdUserBusiness;
    }

    public String getStartTimeFromDateType(@NotNull String dateType) {
        String startTime = "";
        if (dateType.equals(DetailType.DATE_TYPE_TODAY)) {
            startTime = Utilities.getTimeToday();
        } else if (dateType.equals(DetailType.DATE_TYPE_SEVEN_DAYS)) {
            startTime = Utilities.getTime7daysPrev();
        } else {
            startTime = Utilities.getTime30daysPrev();
        }

        return startTime;
    }

    public void htmlForTextView(TextView textView, String html) {
//        if (!HtmlHelper.isHtml(html)) {
//            textView.setText(html);
//            return;
//        }
        String htmlFormat = html.replace("<p></p>", "<br>");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(htmlFormat, Html.FROM_HTML_MODE_COMPACT));
        } else {
            textView.setText(Html.fromHtml(htmlFormat));
        }
    }

    public void updateBottomMenuColor(int color) {
        if (mParentActivity != null && mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarColor(color, false);
        }
    }

    public void logApp(final String logCode) {
        getKhHomeClient().wsAppLog(new MPApiCallback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    android.util.Log.e(TAG, String.format("wsLogApp => succeed [%s]", logCode));
                }
            }

            @Override
            public void onError(Throwable error) {
                android.util.Log.e(TAG, "wsLogApp => failed");
            }
        }, logCode);
    }

    public KhHomeClient getKhHomeClient() {
        if (khHomeClient == null) {
            khHomeClient = new KhHomeClient();
        }
        return khHomeClient;
    }
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2020/4/12
 *
 */

package com.metfone.selfcare.module.share.listener;

public interface ShareBusinessListener {
    //isPlayingState nên được set false nếu dialog được hiển thị lên
    void onShowShareDialog();

    void onDismissShareDialog(boolean isPlayingState);
}

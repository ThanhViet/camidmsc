/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.event;

public class TabMovieReselectedEvent {
    private boolean reselectedHome;

    public TabMovieReselectedEvent() {
    }

    public boolean isReselectedHome() {
        return reselectedHome;
    }

    public TabMovieReselectedEvent setReselectedHome(boolean reselectedHome) {
        this.reselectedHome = reselectedHome;
        return this;
    }

    @Override
    public String toString() {
        return "TabMovieReselectedEvent{" +
                "reselectedHome=" + reselectedHome +
                '}';
    }
}

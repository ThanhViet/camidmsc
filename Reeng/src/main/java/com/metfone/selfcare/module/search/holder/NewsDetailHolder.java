/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.module.tiin.DateUtilitis;

import butterknife.BindView;
import butterknife.OnClick;

import static com.metfone.selfcare.helper.TimeHelper.SDF_IN_YEAR;

public class NewsDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_description)
    @Nullable
    TextView tvDescription;
    @BindView(R.id.tv_source)
    @Nullable
    TextView tvSource;
    @BindView(R.id.tv_created_date)
    @Nullable
    TextView tvCreatedDate;
    @BindView(R.id.tv_fixed_dot)
    @Nullable
    TextView tvDot;

    private SearchAllListener.OnAdapterClick listener;
    private NewsModel data;
    private Activity activity;

    public NewsDetailHolder(View view, Activity activity, SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
        this.activity = activity;
    }

    public void bindData(Object item, boolean isEnd, String keySearch) {
        if (item instanceof NewsModel) {
            data = (NewsModel) item;
            if (tvTitle != null) {
                tvTitle.setText(data.getTitle());
            }
            if (tvSource != null) {
                tvSource.setText(data.getSourceName());
                tvSource.setVisibility(TextUtils.isEmpty(data.getSourceName()) ? View.GONE : View.VISIBLE);
            }
            if (tvDescription != null) {
                tvDescription.setText(data.getShapo());
                tvDescription.setVisibility(TextUtils.isEmpty(data.getShapo()) ? View.GONE : View.VISIBLE);
            }
            String createDate = null;
            if (tvCreatedDate != null) {
                if(data.getTimeStamp() > 0){
                    createDate =  DateUtilitis.calculateDate(activity, data.getTimeStamp());
                }else {
                    createDate = DateTimeUtils.calculateTime(tvCreatedDate.getResources(),
                        DateTimeUtils.stringToSecondsDate(data.getDatePub(), SDF_IN_YEAR));
                }
                tvCreatedDate.setText(createDate);
                tvCreatedDate.setVisibility(!TextUtils.isEmpty(createDate) ? View.VISIBLE : View.GONE);
            }
            if (tvDot != null) {
                tvDot.setVisibility(!TextUtils.isEmpty(data.getCategory()) && !TextUtils.isEmpty(createDate) ? View.VISIBLE : View.GONE);
            }
            ImageBusiness.setNews(data.getImage169(), ivCover);
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxNews && data != null) {
            ((SearchAllListener.OnClickBoxNews) listener).onClickNewsItem(data);
        }
    }

    @OnClick(R.id.iv_more)
    public void onClickItemMore() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            listener.onClickMenuMore(data);
        }
    }
}
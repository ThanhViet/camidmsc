package com.metfone.selfcare.module.home_kh.onboarding;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.onboarding.fragment.FrmKhOnBoarding;
import com.metfone.selfcare.module.home_kh.onboarding.fragment.FrmKhOnBoardingPage1;
import com.metfone.selfcare.module.home_kh.onboarding.fragment.FrmKhOnBoardingPage2;
import com.metfone.selfcare.module.home_kh.onboarding.fragment.FrmKhOnBoardingPage3;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;

public class OnBoardingPagerAdapter extends FragmentPagerAdapter {
    public OnBoardingPagerAdapter(@NonNull FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public FrmKhOnBoarding getItem(int position) {

        switch (position) {
            case 0:
                return new FrmKhOnBoardingPage1();
            case 1:
                return new FrmKhOnBoardingPage2();
            default:
                return new FrmKhOnBoardingPage3();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }


}

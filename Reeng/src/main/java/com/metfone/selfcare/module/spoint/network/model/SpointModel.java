package com.metfone.selfcare.module.spoint.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SpointModel implements Serializable {
    @SerializedName("gif_type")
    @Expose
    private int gifType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("point_desc")
    @Expose
    private String pointDesc;
    @SerializedName("balance")
    @Expose
    private long balance;
    @SerializedName("balance_desc")
    @Expose
    private String balanceDesc;
    @SerializedName("listTask")
    @Expose
    private List<AccumulatePointItem> listTask = null;
    @SerializedName("listGifChange")
    @Expose
    private List<AccumulatePointItem> listGifChange = null;

    public int getGifType() {
        return gifType;
    }

    public void setGifType(int gifType) {
        this.gifType = gifType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPointDesc() {
        return pointDesc;
    }

    public void setPointDesc(String pointDesc) {
        this.pointDesc = pointDesc;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getBalanceDesc() {
        return balanceDesc;
    }

    public void setBalanceDesc(String balanceDesc) {
        this.balanceDesc = balanceDesc;
    }

    public List<AccumulatePointItem> getListTask() {
        if(listTask == null){
            listTask = new ArrayList<>();
        }
        return listTask;
    }

    public void setListTask(List<AccumulatePointItem> listTask) {
        this.listTask = listTask;
    }

    public List<AccumulatePointItem> getListGifChange() {
        if(listGifChange == null){
            listGifChange = new ArrayList<>();
        }
        return listGifChange;
    }

    public void setListGifChange(List<AccumulatePointItem> listGifChange) {
        this.listGifChange = listGifChange;
    }
}

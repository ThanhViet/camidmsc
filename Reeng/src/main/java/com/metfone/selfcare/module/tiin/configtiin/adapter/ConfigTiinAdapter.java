package com.metfone.selfcare.module.tiin.configtiin.adapter;

import android.content.Context;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.view.BaseItemDraggableAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.Category;
import com.metfone.selfcare.v5.widget.SwitchButton;

import java.util.List;

public class ConfigTiinAdapter extends BaseItemDraggableAdapter<Category, BaseViewHolder> {
    private Context mContext;
    private List<Category> datas;
    private TiinListener.onConfigItemListener listener;

    public ConfigTiinAdapter(Context mContext, List<Category> datas, TiinListener.onConfigItemListener listener) {
        super(R.layout.item_setting_category, datas);
        this.mContext = mContext;
        this.listener = listener;
    }

    @Override
    protected void convert(final BaseViewHolder holder, Category item) {
        final Category category = item;

        if (holder.getView(R.id.tvTitle) != null) {
            holder.setText(R.id.tvTitle, category.getName());
        }
        if (holder.getView(R.id.btnFollow) != null) {
            SwitchButton sbFollow = holder.getView(R.id.btnFollow);
            sbFollow.setChecked(category.isOn());
            sbFollow.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                    category.setOn(isChecked);
                    listener.onItemConfig(category, holder.getAdapterPosition());
                }
            });
        }
    }

}

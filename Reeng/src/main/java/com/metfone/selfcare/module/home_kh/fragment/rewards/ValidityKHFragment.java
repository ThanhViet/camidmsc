package com.metfone.selfcare.module.home_kh.fragment.rewards;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.model.Rewards;

public class ValidityKHFragment extends BasePageRewardsKhFragment {
    public static ValidityKHFragment newInstance() {
        ValidityKHFragment fragment = new ValidityKHFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static ValidityKHFragment newInstance(Rewards item, int max) {
        ValidityKHFragment fragment = new ValidityKHFragment();
        Bundle args = new Bundle();
        args.putSerializable(DATA, item);
        args.putInt(MAX, max);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_balance_page_kh;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detailType = DetailType.VALIDITY;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assert sbPoints != null;
        sbPoints.setVisibility(View.GONE);
        assert tvPointsMax != null;
        tvPointsMax.setVisibility(View.GONE);
        assert tvPointsMin != null;
        tvPointsMin.setVisibility(View.GONE);
        assert layoutValue != null;
        layoutValue.setVisibility(View.GONE);
    }
}
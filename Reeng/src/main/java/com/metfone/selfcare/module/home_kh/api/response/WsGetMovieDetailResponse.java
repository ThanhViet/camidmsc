package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;

import java.util.List;

public class WsGetMovieDetailResponse {
    public String errorCode;
    public String errorMessage;
    @SerializedName("result")
    public Response response;

    public static class Response{
        @SerializedName("result")
        public List<Movie> result;
    }
}

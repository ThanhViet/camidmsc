package com.metfone.selfcare.module.metfoneplus.topup.model;

import android.widget.LinearLayout;

import androidx.annotation.StringRes;

import com.metfone.selfcare.R;

import java.util.ArrayList;
import java.util.List;

public class ServiceGroupTopUpModel {
    private int title;
    private int description;
    private int displayDirection;
    private List<ServiceTopUpModel> topUpModels;

    private static List<ServiceGroupTopUpModel> serviceGroupTopUpModels;

    public ServiceGroupTopUpModel(@StringRes int title, @StringRes int description, int displayDirection, List<ServiceTopUpModel> topUpModels) {
        this.title = title;
        this.description = description;
        this.displayDirection = displayDirection;
        this.topUpModels = topUpModels;
    }

    public int getTitle() {
        return title;
    }

    public int getDescription() {
        return description;
    }

    public int getDisplayDirection() {
        return displayDirection;
    }

    public List<ServiceTopUpModel> getTopUpModels() {
        return topUpModels;
    }

    public static List<ServiceGroupTopUpModel> getServiceGroupTopUp(){
        if(serviceGroupTopUpModels == null){
            serviceGroupTopUpModels = new ArrayList<>();
            ArrayList<ServiceTopUpModel> abaServices = new ArrayList<>();
            ArrayList<ServiceTopUpModel> localEWalletServices = new ArrayList<>();
            ArrayList<ServiceTopUpModel> creditDebitCardServices = new ArrayList<>();
            ArrayList<ServiceTopUpModel> internationalEWalletServices = new ArrayList<>();

            abaServices.add(new ServiceTopUpModel(R.drawable.logo_aba_pay, false,"AbaPay"));
            localEWalletServices.add(new ServiceTopUpModel(R.drawable.logo_emoney_pay, false,"eMoney"));
            creditDebitCardServices.add(new ServiceTopUpModel(R.drawable.logo_master_card_pay, false,"Master"));
            creditDebitCardServices.add(new ServiceTopUpModel(R.drawable.logo_union_pay, false,"Union"));
            creditDebitCardServices.add(new ServiceTopUpModel(R.drawable.logo_visa_pay, false,"Visa"));
            internationalEWalletServices.add(new ServiceTopUpModel(R.drawable.logo_wechat_pay, false,"Wechat"));
            internationalEWalletServices.add(new ServiceTopUpModel(R.drawable.logo_alipay, false,"Alipay"));

            serviceGroupTopUpModels.add(new ServiceGroupTopUpModel(R.string.aba_pay, R.string.des_aba_pay, LinearLayout.HORIZONTAL,abaServices));
            serviceGroupTopUpModels.add(new ServiceGroupTopUpModel(R.string.emoney_pay, R.string.des_emoney_pay,LinearLayout.HORIZONTAL,localEWalletServices));
            serviceGroupTopUpModels.add(new ServiceGroupTopUpModel(R.string.credit_debit_card,R.string.des_credit_card_pay,LinearLayout.VERTICAL,creditDebitCardServices));
            serviceGroupTopUpModels.add(new ServiceGroupTopUpModel(R.string.internation_ewallet, R.string.des_international_pay,LinearLayout.VERTICAL,internationalEWalletServices));
        }

        return serviceGroupTopUpModels;
    }
}

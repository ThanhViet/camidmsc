package com.metfone.selfcare.module.home_kh.tab.model;

import android.graphics.drawable.Drawable;

import androidx.annotation.DrawableRes;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

public enum KhUserRank {
    Member(1,R.drawable.ic_avatar_member, R.drawable.ic_rank_member, R.string.kh_user_rank_member),
    Silver(2,R.drawable.ic_avatar_silver, R.drawable.ic_rank_silver, R.string.kh_user_rank_silver),
    //    Gold(R.drawable.ic_av_gold, R.drawable.ic_rank_gold, R.string.kh_user_rank_gold),
    Gold(3, R.drawable.ic_avatar_gold, R.drawable.ic_cinema_grade_gold, R.string.kh_user_rank_gold),
    Diamond(4,R.drawable.ic_avatar_diamond, R.drawable.ic_rank_diamon, R.string.kh_user_rank_diamon),
    Platinum(5,R.drawable.ic_avatar_platinum, R.drawable.ic_crown_platinum, R.string.kh_user_rank_platinum),
    ;
    public int _id;
    public int resAvatar;
    public int resRank;
    public int resRankName;

    KhUserRank(int id, int resAvatar, int resRank, int resRankName) {
        this._id = id;
        this.resAvatar = resAvatar;
        this.resRank = resRank;
        this.resRankName = resRankName;
    }

    public static KhUserRank getById(int id) {
        for (KhUserRank e : values()) {
            if (e._id == id) return e;
        }
        return null;
    }

    public enum RewardRank {
        MEMBER(1, R.drawable.ic_rank_member_2),
        SILVER(2, R.drawable.ic_rank_silver_2),
        GOLD(3, R.drawable.ic_rank_gold_2),
        DIAMOND(4, R.drawable.ic_rank_diamond),
        PLATINUM(5, R.drawable.ic_rank_platinum);
        public final int id;
        public final Drawable drawable;

        RewardRank(int id, @DrawableRes int color) {
            this.id = id;
            this.drawable = ResourceUtils.getDrawable(color);
        }

        public static RewardRank getById(int id) {
            for (RewardRank e : values()) {
                if (e.id == id) return e;
            }
            return null;
        }
    }
}

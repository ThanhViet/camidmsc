package com.metfone.selfcare.module.spoint.holder;

import android.app.Activity;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.spoint.network.model.StatisticalItemModel;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

public class ItemStatisticalHolder extends RecyclerView.ViewHolder {
    private AppCompatTextView tvTitle, tvDesc, tvNameSpoint, tvNumberSpoint;
    private Activity activity;

    public ItemStatisticalHolder(@NonNull View itemView, Activity activity) {
        super(itemView);
        this.activity = activity;
        tvTitle = itemView.findViewById(R.id.tv_title);
        tvDesc = itemView.findViewById(R.id.tv_desc);
        tvNameSpoint = itemView.findViewById(R.id.tv_name_spoint);
        tvNumberSpoint = itemView.findViewById(R.id.tv_number_spoint);
    }

    public void bindData(StatisticalItemModel model) {
        if (model == null) return;
        tvTitle.setText(model.getTitle());
        tvDesc.setText(model.getCreatedDate());
        tvNameSpoint.setText(model.getUnit());
        if (model.getPoint() > 0) {
            tvNumberSpoint.setText("+ " + model.getPoint() + activity.getString(R.string.spoint));
        } else {
            tvNumberSpoint.setText(model.getPoint() + activity.getString(R.string.spoint));
        }

    }
}

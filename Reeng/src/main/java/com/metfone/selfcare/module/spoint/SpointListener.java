package com.metfone.selfcare.module.spoint;

import com.metfone.selfcare.database.model.game.AccumulatePointItem;

public interface SpointListener {
    interface OnSpointListener{
        void onClickActionSpoint(AccumulatePointItem model);
    }
}

package com.metfone.selfcare.module.tiin.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.activitytiin.TiinActivity;
import com.metfone.selfcare.module.tiin.activitytiin.TiinDetailActivity;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;

import butterknife.BindView;

public abstract class BaseFragment extends Fragment implements MvpView, OnInternetChangedListener {
    public static final String TAG = BaseFragment.class.toString();
    @Nullable
    @BindView(R.id.refresh)
    protected SwipeRefreshLayout layout_refresh;
    private BaseSlidingFragmentActivity mActivity;
    public SharedPrefs mPref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseSlidingFragmentActivity) {
            mActivity = (BaseSlidingFragmentActivity) context;
        }
        mPref = SharedPrefs.getInstance();
    }

    @Override
    public void showLoading() {
        showRefresh();
    }

    @Override
    public void hideLoading() {
        hideRefresh();
    }

    protected void showRefresh() {
        if (layout_refresh != null) {
            layout_refresh.setRefreshing(true);
        }
    }

    protected void hideRefresh() {
        if (layout_refresh != null) {
            layout_refresh.setRefreshing(false);
            layout_refresh.destroyDrawingCache();
            layout_refresh.clearAnimation();
        }
    }


    public TiinActivity getTiinActivity() {
        if (mActivity != null && mActivity instanceof TiinActivity) {
            return (TiinActivity) mActivity;
        }
        return null;
    }

    public TiinDetailActivity getTiinDetailActivity() {
        if (mActivity != null && mActivity instanceof TiinDetailActivity) {
            return (TiinDetailActivity) mActivity;
        }
        return null;
    }

    public BaseSlidingFragmentActivity getBaseActivity() {
        return mActivity;
    }

    public void readTiin(TiinModel model) {
        //todo xu ly logview read o day
        try {
            if (getTiinActivity() != null) {
                getTiinActivity().processItemClick(model);
            } else if (getTiinDetailActivity() != null) {
                getTiinDetailActivity().processItemClick(model, false);
            } else if (getBaseActivity() != null && (getBaseActivity() instanceof HomeActivity || getBaseActivity() instanceof ModuleActivity)) {
                Intent intent = new Intent(getBaseActivity(), TiinDetailActivity.class);
                intent.putExtra(ConstantTiin.FULL_CONTENT_TIIN, false);
                intent.putExtra(ConstantTiin.INTENT_MODULE, model);
                getBaseActivity().startActivity(intent);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    @Override
    public boolean isNetworkConnected() {
        return CommonUtils.isConnected(mActivity);
    }

    @Override
    public void onInternetChanged() {
        Toast.makeText(ApplicationController.self(), "Change network", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    public void clickItemOptionTiin(Object object, int menuId) {
        if (getBaseActivity() != null && !getBaseActivity().isFinishing() && object != null) {
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        getBaseActivity().showDialogLogin();
                    } else {
                        ShareUtils.openShareMenu(getBaseActivity(), object);
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        getBaseActivity().showDialogLogin();
                    } else {
                        FeedModelOnMedia feed = null;
                        if (object instanceof TiinModel) {
                            feed = FeedModelOnMedia.convertTiinToFeedModelOnMedia((TiinModel) object);
                        }
                        if (feed != null) {
                            if (object instanceof TiinModel) {
                                if (((TiinModel) object).isLike()) {
                                    new WSOnMedia(ApplicationController.self()).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                            , FeedModelOnMedia.ActionLogApp.UNLIKE, "", feed.getBase64RowId(), ""
                                            , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                                @Override
                                                public void onError(String s) {

                                                }

                                                @Override
                                                public void onComplete() {

                                                }

                                                @Override
                                                public void onSuccess(String msg, String result) throws JSONException {
                                                    if (getBaseActivity() != null)
                                                        getBaseActivity().showToast(R.string.del_favorite);
                                                    ((TiinModel) object).setLike(false);
                                                }
                                            });
                                } else {
                                    new WSOnMedia(ApplicationController.self()).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                            , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                            , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                                @Override
                                                public void onError(String s) {

                                                }

                                                @Override
                                                public void onComplete() {

                                                }

                                                @Override
                                                public void onSuccess(String msg, String result) throws JSONException {
                                                    if (getBaseActivity() != null)
                                                        getBaseActivity().showToast(R.string.add_favorite_success);
                                                    ((TiinModel) object).setLike(true);
                                                }
                                            });
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }
}

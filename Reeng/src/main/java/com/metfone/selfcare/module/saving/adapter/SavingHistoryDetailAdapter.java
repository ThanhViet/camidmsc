/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/20
 */

package com.metfone.selfcare.module.saving.adapter;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseViewHolder;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.saving.model.SavingDetailModel;
import com.metfone.selfcare.module.saving.model.SavingStatistics;
import com.metfone.selfcare.util.Utilities;

import java.util.List;

public class SavingHistoryDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = SavingHistoryDetailAdapter.class.getSimpleName();
    private final int TYPE_EMPTY = 0;
    private final int TYPE_NORMAL = 1;
    private final int TYPE_HEADER = 2;
    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private List<SavingDetailModel.SavingDetails> data;
    private SavingStatistics dataStatistics;
    private Resources mRes;

    public SavingHistoryDetailAdapter(BaseSlidingFragmentActivity activity, List<SavingDetailModel.SavingDetails> data) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplication();
        this.data = data;
        this.mRes = mApplication.getResources();
    }

    public void setDataStatistics(SavingStatistics dataStatistics) {
        this.dataStatistics = dataStatistics;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && dataStatistics != null)
            return TYPE_HEADER;
        SavingDetailModel.SavingDetails item = getItem(position);
        if (item != null) {
            return TYPE_NORMAL;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_saving_overview, parent, false);
        } else if (viewType == TYPE_NORMAL) {
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_saving_detail, parent, false);
        } else
            view = LayoutInflater.from(mApplication).inflate(R.layout.holder_empty, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemHolder, int position) {
        int viewType = getItemViewType(position);
        if (itemHolder instanceof BaseViewHolder) {
            BaseViewHolder holder = (BaseViewHolder) itemHolder;
            if (viewType == TYPE_HEADER && dataStatistics != null) {
                holder.setText(R.id.tv_datetime, dataStatistics.getRangeDate());
                holder.setText(R.id.tv_total_saving, dataStatistics.getTotalSaving());
                holder.setText(R.id.tv_sms_out_saving_detail, dataStatistics.getSmsOutSaving());
                holder.setText(R.id.tv_call_out_saving_detail, dataStatistics.getCallOutSaving());
                holder.setText(R.id.tv_data_saving_detail, dataStatistics.getDataSaving());
                holder.setVisible(R.id.layout_sms_out_saving_detail, Utilities.notEmpty(dataStatistics.getSmsOutSaving()));
                holder.setVisible(R.id.layout_call_out_saving_detail, Utilities.notEmpty(dataStatistics.getCallOutSaving()));
                holder.setVisible(R.id.layout_data_saving_detail, Utilities.notEmpty(dataStatistics.getDataSaving()));
                PieChart chart = holder.getView(R.id.chart_saving);
                Utilities.drawChartSaving(activity, dataStatistics, chart, true);
            } else if (viewType == TYPE_NORMAL) {
                SavingDetailModel.SavingDetails item = getItem(position);
                if (item != null) {
                    holder.setVisible(R.id.line, item.isFirst() && position > 0);
                    holder.setVisible(R.id.button_more, item.isFirst());
                    holder.setText(R.id.tv_datetime, item.getDate());
                    RecyclerView recyclerView = holder.getView(R.id.recycler);
                    if (recyclerView.getItemDecorationCount() <= 0) {
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setNestedScrollingEnabled(false);
                        recyclerView.setLayoutManager(new CustomLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
                    }
                    SavingDetailAdapter adapter = new SavingDetailAdapter(activity, item.getDetails());
                    recyclerView.setAdapter(adapter);
                }
            }
        }
    }

    public SavingDetailModel.SavingDetails getItem(int position) {
        if (dataStatistics != null) position = position - 1;
        if (data != null && data.size() > position && position >= 0)
            return data.get(position);
        return null;
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (data != null)
            size += data.size();
        if (size > 0 && dataStatistics != null) size++;
        return size;
    }

}

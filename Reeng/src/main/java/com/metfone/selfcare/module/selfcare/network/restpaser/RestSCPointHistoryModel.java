package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCPointHistoryModel;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCPointHistoryModel extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCPointHistoryModel> data = new ArrayList<>();

    public ArrayList<SCPointHistoryModel> getData() {
        return data;
    }

    public void setData(ArrayList<SCPointHistoryModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCPointHistoryModel [data=" + data + "] errror " + getErrorCode();
    }
}

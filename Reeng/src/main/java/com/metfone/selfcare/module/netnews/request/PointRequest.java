package com.metfone.selfcare.module.netnews.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HaiKE on 11/20/17.
 */

public class PointRequest {
    @Expose
    @SerializedName("msisdn")
    private String msisdn;

    @Expose
    @SerializedName("imei")
    private String imei;

    @Expose
    @SerializedName("uuid")
    private String uuid;

    @Expose
    @SerializedName("mocha")
    private int mocha;

    public PointRequest(String msisdn, String imei, String uuid, int mocha) {
        this.msisdn = msisdn;
        this.imei = imei;
        this.uuid = uuid;
        this.mocha = mocha;
    }
}

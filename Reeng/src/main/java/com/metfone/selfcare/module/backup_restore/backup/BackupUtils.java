package com.metfone.selfcare.module.backup_restore.backup;

import android.os.Build;
import android.os.Environment;
import android.os.StatFs;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.backup_restore.BackupMessageModel;
import com.metfone.selfcare.module.backup_restore.BackupThreadMessageModel;
import com.metfone.selfcare.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class BackupUtils {
    public static final String TAG = BackupUtils.class.getSimpleName();
    public static final int BACKUP_AUTO_TYPE_OFF = 0;
    public static final int BACKUP_AUTO_TYPE_DAILY = 1;
    public static final int BACKUP_AUTO_TYPE_WEEKLY = 2;
    public static final int BACKUP_AUTO_TYPE_MONTHLY = 3;


    public static final int BACKUP_CODE_NO_MESSAGES = 0;

    public static final int BACKUP_CODE_FAIL_IOEXCEPTION = -1;
    public static final int BACKUP_CODE_FAIL_EXCEPTION = -2;
    public static final int BACKUP_CODE_FAIL_UPLOAD = -3;
    public static final int BACKUP_CODE_FAIL_NOT_ENOUGH_MEMORY = -4;

    public static final int BACKUP_CODE_SUCCESSFULLY = 1;

    public static BackupMessageModel convertToBackUpMessage(ThreadMessage threadMessage, ReengMessage reengMessage, BackupMessageModel backupMessageModel) {
        //set atttribute here
        backupMessageModel.setpId(reengMessage.getPacketId());
        //backupMessageModel.setStp(reengMessage.getMessageType().toString());
        backupMessageModel.setFrom(reengMessage.getSender());
        backupMessageModel.setTo(reengMessage.getReceiver());
        int threadType = threadMessage.getThreadType();

        //backupMessageModel.setTp(threadType + 100);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT || threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT || threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            backupMessageModel.setSvId(threadMessage.getServerId());
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT || threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            if (reengMessage.getDirection() == ReengMessageConstant.Direction.send) {
                backupMessageModel.setSvId(reengMessage.getReceiver());
            } else if (reengMessage.getDirection() == ReengMessageConstant.Direction.received) {
                backupMessageModel.setSvId(reengMessage.getSender());
            }
        }

        backupMessageModel.setcMode(reengMessage.getChatMode());
        backupMessageModel.setBody(reengMessage.getContent());
        backupMessageModel.setReply(reengMessage.getReplyDetail());
        backupMessageModel.setsName(reengMessage.getSenderName());
        backupMessageModel.setsAvatar(reengMessage.getSenderAvatar());
        backupMessageModel.setT(reengMessage.getTime());
        backupMessageModel.setTag(reengMessage.getTagContent());
        //backupMessageModel.setfName(reengMessage.getFileName());
        backupMessageModel.setStatus(reengMessage.getStatus());
        backupMessageModel.setReadState(reengMessage.getReadState());
        //backupMessageModel.setfLink(reengMessage.getDirectLinkMedia());
        //backupMessageModel.setfId(reengMessage.getFileId());
        //backupMessageModel.setRatio(reengMessage.getVideoContentUri());
        //backupMessageModel.setFp(reengMessage.getFilePath());

        return backupMessageModel;
    }

    public static BackupThreadMessageModel convertToBackupThreadMessageModel(ThreadMessage threadMessage, BackupThreadMessageModel backupThreadMessageModel) {
        backupThreadMessageModel.setName(threadMessage.getThreadName());
        backupThreadMessageModel.setState(threadMessage.getState());
        backupThreadMessageModel.setType(threadMessage.getThreadType() + 100);
        String numbers = "";
        if (threadMessage.getPhoneNumbers() != null && threadMessage.getPhoneNumbers().size() > 0) {
            int size = threadMessage.getPhoneNumbers().size();
            for (int i = 0; i < size; i++) {
                String number = threadMessage.getPhoneNumbers().get(i);
                numbers += number;
                if (i < size - 1) numbers += ",";
            }
        }
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            backupThreadMessageModel.setSvId(numbers);
        } else {
            backupThreadMessageModel.setSvId(threadMessage.getServerId());
        }
        backupThreadMessageModel.setNumbers(numbers);
        backupThreadMessageModel.setUnread(threadMessage.getNumOfUnreadMessage());
        backupThreadMessageModel.settChange(threadMessage.getLastChangeThread());
        backupThreadMessageModel.setDraft(threadMessage.getDraftMessage());
        backupThreadMessageModel.settDraft(threadMessage.getLastTimeSaveDraft());
        backupThreadMessageModel.setAvt(threadMessage.getGroupAvatar());
        backupThreadMessageModel.setIsStranger(threadMessage.isStranger() ? 1 : 0);

        String admins = "";
        if (threadMessage.getAdminNumbers() != null && threadMessage.getAdminNumbers().size() > 0) {

            for (String number : threadMessage.getAdminNumbers()) {
                admins += (number + ",");
            }
        }
        backupThreadMessageModel.setAdmins(admins);
        if (threadMessage.getPinMessage() != null) {
            backupThreadMessageModel.setPin(threadMessage.getPinMessage().toJson());
        }
        backupThreadMessageModel.setCls(threadMessage.getGroupClass());
        return backupThreadMessageModel;
    }

    public static void zipFile(File inputFile, File zipFile, String internalNameInZipFile) throws IOException {
        BufferedInputStream origin = null;
        ZipOutputStream out = null;
        FileInputStream in = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            fos = new FileOutputStream(zipFile);
            bos = new BufferedOutputStream(fos);
            out = new ZipOutputStream(bos);
            byte data[] = new byte[Constants.FILE.BUFFER_SIZE_DEFAULT];
            in = new FileInputStream(inputFile);
            origin = new BufferedInputStream(in, Constants.FILE.BUFFER_SIZE_DEFAULT);
            ZipEntry entry = new ZipEntry(internalNameInZipFile);
            out.putNextEntry(entry);
            int count;
            while ((count = origin.read(data, 0, Constants.FILE.BUFFER_SIZE_DEFAULT)) != -1) {
                out.write(data, 0, count);
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "FileNotFoundException", e);
        } finally {
            try {
                if (origin != null)
                    origin.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (in != null)
                    in.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (bos != null)
                    bos.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (fos != null)
                    fos.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public static long getAvailableInternalMemorySize() {
        try {
            File path = Environment.getDataDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize, availableBlocks;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                blockSize = stat.getBlockSizeLong();
                availableBlocks = stat.getAvailableBlocksLong();
            } else {
                blockSize = stat.getBlockSize();
                availableBlocks = stat.getAvailableBlocks();
            }
            return availableBlocks * blockSize;
        } catch (Exception e) {
            return 0;
        }
    }
}

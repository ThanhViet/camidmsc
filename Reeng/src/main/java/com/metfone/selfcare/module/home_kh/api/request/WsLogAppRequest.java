package com.metfone.selfcare.module.home_kh.api.request;

import com.blankj.utilcode.util.LanguageUtils;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsLogAppRequest extends BaseRequest<WsLogAppRequest.WsRequest> {
    public static class WsRequest {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("language")
        String language;
        @SerializedName("wsCode")
        String wsCode;

        public WsRequest(String wsCode, String isdn) {
            this.isdn = isdn;
            this.language = LanguageUtils.getCurrentLocale().getLanguage();
            this.wsCode = wsCode;
        }

    }
}

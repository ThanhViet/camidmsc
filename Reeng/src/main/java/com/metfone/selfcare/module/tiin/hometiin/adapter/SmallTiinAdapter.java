package com.metfone.selfcare.module.tiin.hometiin.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

public class SmallTiinAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private final int SECTIONE_VERTICAL = 1;
    private final int SECTIONE_EMPTY = 0;
    private HomeTiinModel model;
    private Context mContext;
    private TiinListener.OnHomeTiinItemListener listener;

    public SmallTiinAdapter(HomeTiinModel model, Context mContext, TiinListener.OnHomeTiinItemListener listener) {
        this.model = model;
        this.mContext = mContext;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case SECTIONE_VERTICAL:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_small_tiin, parent,false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final TiinModel tiinModel = getItem(position);
        if(holder.getView(R.id.iv_dos) != null){
            holder.setVisible(R.id.iv_dos,true);
        }
        if(holder.getView(R.id.tv_title) != null){
            holder.setText(R.id.tv_title,tiinModel.getTitle());
        }
        if(holder.getView(R.id.button_option) != null){
            holder.setOnClickListener(R.id.button_option,new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    listener.onItemClickMore(tiinModel);
                }
            });
        }
        holder.setOnClickListener(R.id.layout_root, new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                listener.onItemClick(tiinModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null) {
            return SECTIONE_VERTICAL;
        }
        return SECTIONE_EMPTY;
    }

    public TiinModel getItem(int position) {
        try {
            return model.getData().get(position);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
        return null;
    }

}

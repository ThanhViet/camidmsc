package com.metfone.selfcare.module.metfoneplus.topup;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.databinding.ServiceTopUpFragmentBinding;
import com.metfone.selfcare.module.home_kh.api.request.ABAMobilePaymentTopUpCallBackRequest;
import com.metfone.selfcare.module.home_kh.api.request.ABAMobilePaymentTopUpRequest;
import com.metfone.selfcare.module.home_kh.api.response.ABAMobilePaymentTopUpCallBackResponse;
import com.metfone.selfcare.module.home_kh.api.response.ABAMobilePaymentTopUpResponse;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseBindingFragment;
import com.metfone.selfcare.module.metfoneplus.topup.adapter.ServiceGroupTopUpAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.model.Constants;
import com.metfone.selfcare.module.metfoneplus.topup.model.ServiceGroupTopUpModel;
import com.metfone.selfcare.module.metfoneplus.topup.model.ServiceTopUpModel;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TransMobileRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TransMobileResponse;
import com.metfone.selfcare.module.metfoneplus.topup.viewholder.ServiceGroupTopUpViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.viewholder.ServiceTopUpViewHolder;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import retrofit2.Response;

public class ServiceTopUpFragment extends MPBaseBindingFragment<ServiceTopUpFragmentBinding> implements ServiceTopUpViewHolder.EventListener, View.OnClickListener {

    private final int LAUNCH_SECOND_ACTIVITY = 97;
    private final int LAUNCH_ABA_APP = 100;
    private ServiceGroupTopUpAdapter mAmountAdapter;
    private ArrayList<ArrayList<ServiceTopUpModel>> mServiceTopUpModels = new ArrayList<>();
    private MetfonePlusClient client = new MetfonePlusClient();
    private TransMobileRequest mTransMobileRequest = new TransMobileRequest();
    private CamIdUserBusiness camIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
    private String mPhoneUser;
    private String mTopUpAmount;
    private String mPaymentMethod;
    private String transIdABA = "";
    private boolean paymentIsProcessing = false;

    public static ServiceTopUpFragment newInstance() {
        return new ServiceTopUpFragment();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.service_top_up_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mBinding.viewRootlayout);
        initData();
        initView();
        iniListener();
    }

    private void initData() {
//        mServiceTopUpModels = ServiceTopUpModel.createServiceTopUpModel();
        mPhoneUser = getArguments().getString(Constants.PHONE);
        mTopUpAmount = getArguments().getString(Constants.AMOUNT);
    }

    private void initDataRequest() {
        mTransMobileRequest.setPhoneNumber(mPhoneUser);
        mTransMobileRequest.setTopupAmount(mTopUpAmount);
        mTransMobileRequest.setPaymentMethod(mPaymentMethod);
        mTransMobileRequest.setAccountEmoney(camIdUserBusiness.getMetfoneUsernameIsdn());
    }

    private void initView() {
        mAmountAdapter = new ServiceGroupTopUpAdapter(ServiceGroupTopUpModel.getServiceGroupTopUp(), this::onServiceTopUpClicked);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerView.setAdapter(mAmountAdapter);
        mBinding.btnBack.setOnClickListener(v -> popBackStackFragment());
        mBinding.tvPhone.setText(0 + mPhoneUser.substring(0, 2) + " " + mPhoneUser.substring(2));
        mBinding.tvAmount.setText(getString(R.string.m_p_top_up_top_up_value, mTopUpAmount));
    }

    private void iniListener() {
        mBinding.btnNext.setOnClickListener(this);
    }


    @Override
    public void onServiceTopUpClicked(ServiceTopUpModel selectedItem) {
//        setSateViewSelect(selectedItem);
//        mAmountAdapter.updateAdapter(mServiceTopUpModels);
        if(!paymentIsProcessing){
            mPaymentMethod = selectedItem.getPaymentMethod();
            initDataRequest();
            requestTrans();
        }
//        mBinding.btnNext.setEnabled(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                initDataRequest();
                requestTrans();
                break;
        }
    }

    private void setSateViewSelect(ServiceTopUpModel selectedItem) {
        for (List<ServiceTopUpModel> serviceTopUpModelList : mServiceTopUpModels) {
            for(ServiceTopUpModel serviceTopUpModel : serviceTopUpModelList){
                serviceTopUpModel.setSelect(false);
            }
        }
        selectedItem.setSelect(true);
    }

    private void requestTrans() {
        paymentIsProcessing = true;
        if(mPaymentMethod.equals("AbaPay")){
            ABAMobilePaymentTopUpRequest.Request request = new ABAMobilePaymentTopUpRequest.Request();
            request.isdn = mPhoneUser;
            request.topupAmount = mTopUpAmount;
            client.wsABAMobilePaymentTopUp(new MPApiCallback<ABAMobilePaymentTopUpResponse>() {
                @Override
                public void onResponse(Response<ABAMobilePaymentTopUpResponse> response) {
                    paymentIsProcessing = false;
                    if(response.body() != null && response.body().result != null && response.body().result.isSuccess()){
                        try {
                            transIdABA = response.body().result.txnid;
                            Intent abaIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().result.abapayDeeplink));
                            startActivityForResult(abaIntent, LAUNCH_ABA_APP);
                        }
                        catch (Exception e){
                            if(response.body().result.playStore!=null && !response.body().result.playStore.isEmpty()){
                                Intent abaIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().result.playStore));
                                startActivity(abaIntent);
                                getActivity().finish();
                            }
                        }

                    }
                    else {
                        onError(null);
                    }
                }

                @Override
                public void onError(Throwable error) {
                    paymentIsProcessing = false;
                    ToastUtils.showToast(getContext(), getActivity().getString(R.string.m_p_notify_error_in_processing));
                }
            }, request);
        }
        else {
            client.transMobileMP(mTransMobileRequest, new ApiCallback<TransMobileResponse>() {
                @Override
                public void onResponse(Response<TransMobileResponse> response) {
                    paymentIsProcessing = false;
                    if (response.body() != null && response.body().isStatus()) {
                        ApplicationController.self().getFirebaseEventBusiness().logTopup(true,mTransMobileRequest.getPhoneNumber(),mTransMobileRequest.getPaymentMethod(),mTransMobileRequest.getTopupAmount(),"0");
                        String url = response.body().getRedirect_url();
                        Intent intent = new Intent(getActivity(), WebViewActivity.class);
                        intent.putExtra("url", url);
                        startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
                    } else {
                        ApplicationController.self().getFirebaseEventBusiness().logTopup(false,mTransMobileRequest.getPhoneNumber(),mTransMobileRequest.getPaymentMethod(),mTransMobileRequest.getTopupAmount(),response.body().getResponseCode());
                        ToastUtils.showToast(getContext(), getContext().getString(R.string.m_p_notify_error_in_processing));
                    }
                }

                @Override
                public void onError(Throwable error) {
                    paymentIsProcessing = false;
                    ToastUtils.showToast(getContext(), getActivity().getString(R.string.m_p_notify_error_in_processing));
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                getActivity().getFragmentManager().popBackStack();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
        else if(requestCode == LAUNCH_ABA_APP){
            ABAMobilePaymentTopUpCallBackRequest.Request request = new ABAMobilePaymentTopUpCallBackRequest.Request();
            request.txnid = transIdABA;
            client.wsABAMobilePaymentTopUpCallBack(new MPApiCallback<ABAMobilePaymentTopUpCallBackResponse>() {
                @Override
                public void onResponse(Response<ABAMobilePaymentTopUpCallBackResponse> response) {
                    if(response.body() != null && response.body().result != null && response.body().result.isSuccess()){
                        ToastUtils.showToast(getContext(), getActivity().getString(R.string.successfully));
                        popBackStackFragment();
                    }
                    else {
                        ToastUtils.showToast(getContext(), response.body().result.responseMessage);
                    }
                }

                @Override
                public void onError(Throwable error) {
                    ToastUtils.showToast(getContext(), getActivity().getString(R.string.m_p_notify_error_in_processing));
                }
            }, request);
        }
    }//onActivityResult
}


package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.AuthKeyModel;
import com.metfone.selfcare.module.home_kh.model.KHAccountPoint;

import java.util.List;

public class GetAuthKeyReponse {
    @SerializedName("code")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("wsResponse")
    @Expose
    AuthKeyModel authKeyModel;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AuthKeyModel getAuthKeyModel() {
        return authKeyModel;
    }

    public void setAuthKeyModel(AuthKeyModel authKeyModel) {
        this.authKeyModel = authKeyModel;
    }
}

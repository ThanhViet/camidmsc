package com.metfone.selfcare.module.games.adapter;

public interface OnItemClick {
    void onItemClick(String data);
}

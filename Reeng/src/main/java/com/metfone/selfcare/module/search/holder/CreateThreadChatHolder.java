/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/20
 *
 */

package com.metfone.selfcare.module.search.holder;

import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.CreateThreadChat;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateThreadChatHolder extends BaseAdapter.ViewHolder {
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    private SearchAllListener.OnAdapterClick listener;
    private CreateThreadChat data;

    public CreateThreadChatHolder(View view, final SearchAllListener.OnAdapterClick listener) {
        super(view);
        this.listener = listener;
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof CreateThreadChat) {
            data = (CreateThreadChat) item;
            if (tvTitle != null) tvTitle.setText(data.getName());
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxThreadChat && data != null) {
            ((SearchAllListener.OnClickBoxThreadChat) listener).onClickCreateThreadMessage(data);
        }
    }
}
package com.metfone.selfcare.module.keeng.network.restful;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.WeakHashMap;

public class PostFormDataRequest<T> extends Request<T> {

    private final String TAG = getClass().getSimpleName();
    private final Listener<T> mListener;
    private final String mRequestBody;
    private Gson mGson;
    private Class<T> mJavaClass;
    private WeakHashMap<String, String> headers = new WeakHashMap<>();
    private WeakHashMap<String, String> params = new WeakHashMap<>();

    public PostFormDataRequest(String url, Class<T> cls, String requestBody, Listener<T> listener, ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        mGson = new Gson();
        mRequestBody = requestBody;
        mJavaClass = cls;
        mListener = listener;
        if (BuildConfig.DEBUG)
            Log.e(TAG, "Method: POST - Url: " + url);
    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=UTF-8";
    }

    @Override
    protected String getParamsEncoding() {
        return "UTF-8";
    }

    @Override
    protected void deliverResponse(T response) {
        if (mListener != null && response != null) {
            mListener.onResponse(response);
        } else {
            Log.e(TAG, "Response is null");
            getErrorListener().onErrorResponse(new VolleyError());
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (BuildConfig.DEBUG)
            Log.i(TAG, "HEADERS: " + headers);
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if (BuildConfig.DEBUG)
            Log.i(TAG, "PARAMS: " + params);
        return params != null ? params : super.getParams();
    }

    public void setHeader(String key, Object value) {
        if (value == null) {
            headers.put(key, "");
            return;
        }
        headers.put(key, value.toString());
    }

    public void setParams(String key, Object value) {
        if (value == null) {
            params.put(key, "");
            return;
        }
        params.put(key, value.toString());
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String className = mJavaClass.getCanonicalName();
            String jsonString = new String(response.data, Charset.forName("UTF-8"));
            if (BuildConfig.DEBUG)
                Log.i(TAG, "parseNetworkResponse: " + className + "\n" + jsonString + "\n");
            T parsedGSON = mGson.fromJson(jsonString, mJavaClass);
            return Response.success(parsedGSON, HttpHeaderParser.parseCacheHeaders(response));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public byte[] getBody() {
        try {
            return mRequestBody == null ? super.getBody() : mRequestBody.getBytes(getParamsEncoding());
        } catch (UnsupportedEncodingException | AuthFailureError e) {
            Log.e(TAG, e);
            return null;
        } catch (Exception e) {
            Log.e(TAG, e);
            return null;
        }
    }
}

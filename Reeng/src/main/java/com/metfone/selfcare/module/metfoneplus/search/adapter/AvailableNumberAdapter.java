package com.metfone.selfcare.module.metfoneplus.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class AvailableNumberAdapter extends RecyclerView.Adapter<AvailableNumberAdapter.NumberViewHolder> {
    private Context context;
    private ArrayList<AvailableNumber> availableNumbers;
    private AvailableNumberListener listener;

    public AvailableNumberAdapter(Context context, ArrayList<AvailableNumber> availableNumbers, AvailableNumberListener listener) {
        this.context = context;
        this.availableNumbers = availableNumbers;
        this.listener = listener;
    }

    @NonNull
    @Override
    public NumberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_available_number, parent, false);
        return new NumberViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NumberViewHolder holder, int position) {
        AvailableNumber availableNumber = availableNumbers.get(position);
        String ssID = Utilities.formatPhoneNumberCambodia(availableNumber.getIsdn());
        holder.tv_phone_number.setText(ssID);
        holder.tv_amount.setText("$"+availableNumber.getPrice());
        holder.tv_commitment.setText("$" + availableNumber.getMonthlyFee());
    }

    @Override
    public int getItemCount() {
        return availableNumbers.size();
    }

    public class NumberViewHolder extends RecyclerView.ViewHolder {
        TextView tv_phone_number;
        TextView tv_amount;
        TextView tv_commitment;
        ImageView btn_register;

        public NumberViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_phone_number = itemView.findViewById(R.id.phone_number);
            tv_amount = itemView.findViewById(R.id.amount);
            tv_commitment = itemView.findViewById(R.id.commitment);
            btn_register = itemView.findViewById(R.id.btn_register);
            btn_register.setOnClickListener(v ->{
                listener.registerNumber(availableNumbers.get(getAdapterPosition()));
            });
        }
    }

    public interface AvailableNumberListener{
        void registerNumber(AvailableNumber availableNumber);
    }
}

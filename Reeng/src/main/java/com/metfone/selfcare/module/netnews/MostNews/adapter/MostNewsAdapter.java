package com.metfone.selfcare.module.netnews.MostNews.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.newdetails.view.TextViewWithImages;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.ui.glide.ImageLoader;
import com.metfone.selfcare.util.Log;

import java.util.List;

/**
 * Created by HaiKE on 8/19/17.
 */

public class MostNewsAdapter extends BaseQuickAdapter<NewsModel, BaseViewHolder> {

    private Context context;
    private AbsInterface.OnNewsListener listener;

    public MostNewsAdapter(Context context, int id, List<NewsModel> datas, AbsInterface.OnNewsListener listener) {
        super(id, datas);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void convert(final BaseViewHolder holder, final NewsModel model) {

        try {
            if(model != null)
            {
                if(holder.getView(R.id.tvTitle) != null)
                {
                    TiinUtilities.setText(holder.getView(R.id.tvTitle), model.getTitle(), model.getTypeIcon());
                    if(checkMarkRead(model.getID()))
                    {
                        TextView textView = holder.getView(R.id.tvTitle);
                        textView.setTextColor(textView.getTextColors().withAlpha(120));
                    }
                }

                if(holder.getView(R.id.imvImage) != null)
                    ImageBusiness.setImageNew(model.getImage169(), (ImageView) holder.getView(R.id.imvImage));

                if(model.getIsNghe() == 1 && CommonUtils.FLAG_SUPPORT_RADIO)
                {
                    holder.getView(R.id.btnListen).setVisibility(View.VISIBLE);
                    holder.setText(R.id.btnListen, context.getString(R.string.listen) + " - " + model.getDuration());
                    holder.getView(R.id.btnListen).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(listener != null)
                                listener.onItemRadioClick(model);
                        }
                    });
                }
                else
                {
                    holder.getView(R.id.btnListen).setVisibility(View.GONE);
                }

                if(holder.getView(R.id.tvCategory) != null)
                {
                    if(!TextUtils.isEmpty(model.getSourceName()))
                    {
                        holder.setText(R.id.tvCategory, model.getSourceName());
                    }
                    else if(!TextUtils.isEmpty(model.getCategory()))
                    {
                        holder.setText(R.id.tvCategory, model.getCategory());
                    }
                }

//                if(categoryId != 0)
//                {
//                    if(holder.getView(R.id.tvDate) != null)
//                        holder.setVisible(R.id.tvDate, false);
//
//                    if(holder.getView(R.id.imvDot) != null)
//                        holder.setVisible(R.id.imvDot, false);
//                }
//                else
//                {
                    if(holder.getView(R.id.tvDate) != null && !TextUtils.isEmpty(model.getDatePub()))
                    {
                        holder.setText(R.id.tvDate, Html.fromHtml(model.getDatePub()));
                        holder.setVisible(R.id.tvDate, true);

                        if(holder.getView(R.id.imvDot) != null)
                            holder.setVisible(R.id.imvDot, true);
                    }
                    else
                    {
                        if(holder.getView(R.id.tvDate) != null)
                            holder.setVisible(R.id.tvDate, false);

                        if(holder.getView(R.id.imvDot) != null)
                            holder.setVisible(R.id.imvDot, false);
                    }
//                }

                if(holder.itemView != null)
                {
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(listener != null)
                                listener.onItemClick(holder.getAdapterPosition());

                            if(checkMarkRead(model.getID()))
                            {
                                TextView textView = holder.getView(R.id.tvTitle);
                                textView.setTextColor(textView.getTextColors().withAlpha(120));
                            }
                        }
                    });
                }
            }
        }
        catch (Exception ex)
        {
            Log.e(TAG, "Exception", ex);
        }
    }
    public boolean checkMarkRead(int id)
    {
//        SharedPref pref = new SharedPref(context);
//        String list = pref.getString(AppConstants.KEY_MARK_READ, "");
//        if(!android.text.TextUtils.isEmpty(list) && !list.equals(","))
//        {
//            String[] listId = list.split(",");
//            for(int i = 0; i < listId.length; i++)
//            {
//                if(listId[i].equals(id + ""))
//                {
//                    return true;
//                }
//            }
//        }
        return false;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.video.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentVideo;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.video.model.VideoPagerModel;

import butterknife.BindView;

public class HeaderListVideoHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    VideoPagerModel data;

    public HeaderListVideoHolder(View view, Activity activity, OnClickContentVideo listener) {
        super(view);
        initListener(listener);

    }

    private void initListener(final OnClickContentVideo listener) {
        if (tvTitle != null) tvTitle.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMoreVideoItem(data, getAdapterPosition());
                }
            }
        });
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof VideoPagerModel) {
            data = (VideoPagerModel) item;
            if (tvTitle != null) tvTitle.setText(data.getTitle());

        } else {
            data = null;
        }
    }
}
package com.metfone.selfcare.module.movienew.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.common.Constant;

import java.util.List;

public class HistorySearchViewHolder extends BaseViewHolder {


    String mType;

    public HistorySearchViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener, String type) {
        super(itemView, itemViewClickListener);
        this.mType = type;
    }

    private TextView tvHistory;
    private ImageView iconSearch;

    @Override
    public void initViewHolder(View v) {
        if (mData == null || mData.size() == 0) return;
        if (mData.get(0) instanceof String) {
            tvHistory = v.findViewById(R.id.tvSearch);
            iconSearch = v.findViewById(R.id.iconSearch);
        }
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        String textHis = (String) mData.get(pos);
        if (mData.get(pos) instanceof String) {
            tvHistory.setText(textHis);
        }
        iconSearch.setVisibility(mType.equals(Constant.HOME) ? View.GONE : View.VISIBLE);
        tvHistory.setTextColor(mType.equals(Constant.HOME) ? Color.parseColor("#99FFFFFF") : Color.parseColor("#FFFFFF"));
        if (mType.equals(Constant.HOME))
            tvHistory.setPadding(0, 0, 0, 14);
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null) {
            itemViewClickListener.onItemViewClickListener(pos, mData);
        }
    }
}

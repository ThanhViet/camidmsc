package com.metfone.selfcare.module.home_kh.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.holder.BlockContactKhViewHolder;

import java.util.ArrayList;
import java.util.List;

public class SettingListKhAdapter extends BaseAdapter<BlockContactKhViewHolder, Object> {

    public static final int SCREEN_BLOCK_USER = 1;
    public static final int SCREEN_SAVED_MESSAGE = 2;
    private List<Object> items;
    private int screenType;
    private ItemListener itemListener;

    public SettingListKhAdapter(Activity activity, int screenType) {
        super(activity);
        this.screenType = screenType;
        items = new ArrayList<>();
    }


    @NonNull
    @Override
    public BlockContactKhViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BlockContactKhViewHolder viewHolder = null;
//        if (screenType == SCREEN_BLOCK_USER) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_saved_message_kh, parent, false);
        viewHolder = new BlockContactKhViewHolder(view, screenType);
        ((BlockContactKhViewHolder) viewHolder).setType(screenType);
        ((BlockContactKhViewHolder) viewHolder).setItemListener(itemListener);

//        } else {

//        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BlockContactKhViewHolder holder, int position) {
        holder.setElement(items.get(position));
    }

    public void setItemListener(ItemListener itemListener) {
        this.itemListener = itemListener;
    }


    public <T> void addAll(List<T> listItem) {
        if (listItem == null || listItem.isEmpty()) {
            return;
        }
        items.addAll(listItem);
        notifyItemRangeInserted(items.size() - listItem.size(), listItem.size());
    }

    public <T> void swapList(List<T> listItem) {
        items.clear();
        if (listItem != null) {
            items.addAll(listItem);
        }
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface ItemListener {
        void removeItem(int position);

        void itemClick(int position);

        void avatarClickListener(int position);
    }


}

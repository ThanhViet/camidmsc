package com.metfone.selfcare.module.home_kh.tab.model;

import com.metfone.selfcare.R;

import java.util.ArrayList;
import java.util.List;

public class KhHomeCustomTopic implements IHomeModelType {
    public String topicTitle;
    public int titleRes = R.string.kh_home_custom_topic;
    public List<KhHomeMovieItem> topics = new ArrayList<>();

    public KhHomeCustomTopic(String topicTitle, List<KhHomeMovieItem> topics) {
        this.topicTitle = topicTitle;
        this.topics = topics;
    }

    public KhHomeCustomTopic(List<KhHomeMovieItem> topics) {
        this.topics = topics;
    }

    @Override
    public int getItemType() {
        return IHomeModelType.CUSTOM_TOPIC;
    }
}

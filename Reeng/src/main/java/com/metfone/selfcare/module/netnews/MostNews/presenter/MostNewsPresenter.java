package com.metfone.selfcare.module.netnews.MostNews.presenter;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.module.netnews.MostNews.fragment.MostNewsFragment;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.netnews.request.NewsRequest;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Log;

public class MostNewsPresenter extends BasePresenter implements IMostNewsPresenter {

    public static final String TAG = MostNewsPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;

    public MostNewsPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadData(int cateId, int page, long unixTime) {
        mNewsApi.getMostNews(new NewsRequest(cateId, page, CommonUtils.NUM_SIZE, unixTime), callback);
    }

    @Override
    public void updateLastNews(SharedPref sharedPref, int id) {
        // getDataManager().setLastNews(id);
        if (sharedPref != null) {
            sharedPref.putInt(SharedPref.PREF_LAST_NEWS, id);
        }
    }

    @Override
    public int getLastNews(SharedPref pref) {
        //return getDataManager().getLastNews();
        if (pref != null) {
            return pref.getInt(SharedPref.PREF_LAST_NEWS, 0);
        }
        return 0;
    }

    HttpCallBack callback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            if (!isViewAttached() || !(getMvpView() instanceof MostNewsFragment)) {
                return;
            }
            Gson gson = new Gson();
            NewsResponse childNewsResponse = gson.fromJson(data, NewsResponse.class);

            ((MostNewsFragment) getMvpView()).bindData(childNewsResponse);
            ((MostNewsFragment) getMvpView()).loadDataSuccess(true);
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof MostNewsFragment)) {
                return;
            }
            ((MostNewsFragment) getMvpView()).loadDataSuccess(false);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
}

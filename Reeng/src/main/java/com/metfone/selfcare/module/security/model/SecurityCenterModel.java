/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/29
 */

package com.metfone.selfcare.module.security.model;

import java.io.Serializable;
import java.util.ArrayList;

public class SecurityCenterModel implements Serializable {
    public enum Type {
        VULNERABILITY(0), NEWS(1);
        public int VALUE;

        Type(int VALUE) {
            this.VALUE = VALUE;
        }
    }

    public SecurityCenterModel() {
    }

    public SecurityCenterModel(Type type) {
        this.type = type;
    }

    private Type type;
    private ArrayList<VulnerabilityModel> vulnerabilitiesList;
    private ArrayList<NewsModel> newsList;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public ArrayList<VulnerabilityModel> getVulnerabilitiesList() {
        return vulnerabilitiesList;
    }

    public void setVulnerabilitiesList(ArrayList<VulnerabilityModel> vulnerabilitiesList) {
        this.vulnerabilitiesList = vulnerabilitiesList;
    }

    public ArrayList<NewsModel> getNewsList() {
        return newsList;
    }

    public void setNewsList(ArrayList<NewsModel> newsList) {
        this.newsList = newsList;
    }
}

package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RestSCRecommentPackage extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private RestSCRecommentPackageList data;

    public RestSCRecommentPackageList getData() {
        return data;
    }

    public void setData(RestSCRecommentPackageList data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCRecommentPackage [data=" + data + "] errror " + getErrorCode();
    }
}

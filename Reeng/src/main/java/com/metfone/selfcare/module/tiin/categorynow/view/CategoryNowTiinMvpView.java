package com.metfone.selfcare.module.tiin.categorynow.view;

import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;

import java.util.List;

public interface CategoryNowTiinMvpView extends MvpView {
    void loadDataSuccess(boolean flag);

    void bindData(List<TiinEventModel> response);


}

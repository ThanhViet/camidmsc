package com.metfone.selfcare.module.selfcare.holder;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.adapter.SCAccountInfoAdapter;
import com.metfone.selfcare.module.selfcare.model.SCAccountDataInfo;

public class SCAccountInfoHolder extends BaseViewHolder {
    private TextView tvTitle;
    private RecyclerView recyclerView;
    private SCAccountInfoAdapter adapter;
    private LinearLayoutManager layoutManager;
    private SCAccountDataInfo data;

    public SCAccountInfoHolder(Context context, View view) {
        super(view);

        tvTitle = view.findViewById(R.id.tvTitle);
        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCAccountInfoAdapter(context);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);
    }

    public void setData(SCAccountDataInfo data) {
        this.data = data;
        if (data.getValues().size() > 0) {
            tvTitle.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);

            tvTitle.setText(data.getTitle());
            adapter.setItemsList(data.getValues());

        } else {
            tvTitle.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        }
        adapter.notifyDataSetChanged();
    }
}

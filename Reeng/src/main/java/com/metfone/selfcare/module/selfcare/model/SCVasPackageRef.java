package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCVasPackageRef implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("osType")
    private String osType;

    @SerializedName("href")
    private String href;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public String toString() {
        return "SCVasPackageRef{" +
                "id='" + id + '\'' +
                ", osType='" + osType + '\'' +
                ", href='" + href + '\'' +
                '}';
    }
}

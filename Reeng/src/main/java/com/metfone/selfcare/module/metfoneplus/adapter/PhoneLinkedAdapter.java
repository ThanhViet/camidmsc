package com.metfone.selfcare.module.metfoneplus.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.PhoneLinked;

import java.util.List;

public class PhoneLinkedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_GROUP = 0;
    private static final int TYPE_CONTENT = 1;

    private final View.OnClickListener mOnPhoneClickListener;
    private List<Object> mObjectList;

    public PhoneLinkedAdapter(List<Object> objectList, View.OnClickListener onPhoneClickListener) {
        this.mObjectList = objectList;
        this.mOnPhoneClickListener = onPhoneClickListener;
    }

    private void setList(List<Object> objectList) {
        this.mObjectList = objectList;
    }

    public void replaceData(List<Object> objectList) {
        setList(objectList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_CONTENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_mp_content_phone_linked, parent, false);
            return new ContentViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_mp_title_phone_linked, parent, false);
            return new GroupViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Object object = mObjectList.get(position);
        if (holder.getItemViewType() == TYPE_GROUP) {
            GroupViewHolder titleHolder = (GroupViewHolder) holder;
            titleHolder.txtTitle.setText((CharSequence) object);
        } else {
            PhoneLinked phoneLinkedModel = (PhoneLinked) object;
            ContentViewHolder contentHolder = (ContentViewHolder) holder;

            contentHolder.txtContent.setText((CharSequence) phoneLinkedModel.getPhoneNumber());

            contentHolder.phoneLinked = phoneLinkedModel;
            contentHolder.itemView.setTag(contentHolder);
            contentHolder.itemView.setOnClickListener(mOnPhoneClickListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mObjectList != null) {
            if (mObjectList.get(position) instanceof PhoneLinked) {
                return TYPE_CONTENT;
            }
            return TYPE_GROUP;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        if (mObjectList == null) {
            return 0;
        }
        return mObjectList.size();
    }

    class GroupViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView txtTitle;

        public GroupViewHolder(View view) {
            super(view);
            txtTitle = view.findViewById(R.id.txt_title_phone_linked);
        }
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView txtContent;
        public PhoneLinked phoneLinked;

        public ContentViewHolder(View view) {
            super(view);
            txtContent = view.findViewById(R.id.txt_content_phone_linked);
        }
    }
}

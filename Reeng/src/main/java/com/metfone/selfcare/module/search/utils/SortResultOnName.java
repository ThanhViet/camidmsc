/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/9/14
 *
 */

package com.metfone.selfcare.module.search.utils;

import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.search.model.ContactProvisional;

import java.util.Comparator;
import java.util.Locale;

public class SortResultOnName implements Comparator<Object> {
    public int compare(Object obj1, Object obj2) {
        String name1 = "";
        String name2 = "";
        if (obj1 instanceof ThreadMessage) {
            name1 = ((ThreadMessage) obj1).getThreadName();
            if (name1 == null) name1 = "";
            else
                name1 = name1.trim().toLowerCase(Locale.US);
        } else if (obj1 instanceof PhoneNumber) {
            name1 = ((PhoneNumber) obj1).getName();
            if (name1 == null) name1 = "";
            else
                name1 = name1.trim().toLowerCase(Locale.US);
        } else if (obj1 instanceof ContactProvisional) {
            name1 = ((ContactProvisional) obj1).getName();
            if (name1 == null) name1 = "";
            else
                name1 = name1.trim().toLowerCase(Locale.US);
        }
        if (obj2 instanceof ThreadMessage) {
            name2 = ((ThreadMessage) obj2).getThreadName();
            if (name2 == null) name2 = "";
            else
                name2 = name2.trim().toLowerCase(Locale.US);
        } else if (obj2 instanceof PhoneNumber) {
            name2 = ((PhoneNumber) obj2).getName();
            if (name2 == null) name2 = "";
            else
                name2 = name2.trim().toLowerCase(Locale.US);
        } else if (obj2 instanceof ContactProvisional) {
            name2 = ((ContactProvisional) obj2).getName();
            if (name2 == null) name2 = "";
            else
                name2 = name2.trim().toLowerCase(Locale.US);
        }
        return name1.compareToIgnoreCase(name2);
    }

}
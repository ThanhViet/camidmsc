/*
package com.metfone.selfcare.module.selfcare.fragment.account;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.module.selfcare.activity.AfterLoginMyIDActivity;
import com.metfone.selfcare.module.selfcare.model.SCNumberVerify;
import com.metfone.selfcare.module.selfcare.network.SelfCareAccountApi;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

*/
/**
 * Created by thanhnt72 on 5/13/2019.
 *//*


public class VerifyNumberFragment extends Fragment {


    private static final String TAG = VerifyNumberFragment.class.getSimpleName();
    public static final String NUMBER = "number";
    @BindView(R.id.tilInputOtp)
    TextInputLayout tilInputOtp;
    @BindView(R.id.tvNumberVerify)
    TextView tvNumberVerify;
    @BindView(R.id.tvResend)
    TextView tvResend;
    Unbinder unbinder;

    private EditText etInputOtp;

    private BaseSlidingFragmentActivity activity;
    private Resources mRes;
    private ApplicationController mApp;

    private SCNumberVerify numberVerify;

    public static VerifyNumberFragment newInstance(SCNumberVerify number) {
        VerifyNumberFragment fragment = new VerifyNumberFragment();
        Bundle args = new Bundle();
        args.putSerializable(NUMBER, number);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        mRes = activity.getResources();
        mApp = (ApplicationController) activity.getApplication();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_verify_number, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        etInputOtp = tilInputOtp.getEditText();
        if (getArguments() != null) {
            numberVerify = (SCNumberVerify) getArguments().getSerializable(NUMBER);
        }
        if (numberVerify == null) {
            activity.onBackPressed();
            activity.showToast(R.string.e601_error_but_undefined);
        } else {
            PhoneNumberUtil phoneUtil = mApp.getPhoneUtil();
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                    getPhoneNumberProtocol(phoneUtil, numberVerify.getMsisdn(), "MM");
            StringBuilder sb = new StringBuilder();
            if (PhoneNumberHelper.getInstant().isValidPhoneNumber(phoneUtil,
                    phoneNumberProtocol)) {
                sb.append("(+");
                sb.append(String.valueOf(phoneNumberProtocol.getCountryCode())).append(") ");
                sb.append(String.valueOf(phoneNumberProtocol.getNationalNumber()));
                tvNumberVerify.setText(sb.toString());
            } else {
                tvNumberVerify.setText(numberVerify.getMsisdn());
            }

            setResendPassListener();
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btnFinish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                activity.onBackPressed();
                break;
            case R.id.btnFinish:
                String textInput = etInputOtp.getText().toString().trim();
                if (!TextUtils.isEmpty(textInput)) {
                    numberVerify.setOtp(textInput);
                    ((AfterLoginMyIDActivity) activity).onInputOtp(numberVerify);
                } else {

                }
                break;
        }
    }


    private void setResendPassListener() {
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
//                ds.setColor(ds.linkColor);    // you can use custom color
//                ds.setUnderlineText(false);    // this remove the underline
            }

            @Override
            public void onClick(View textView) {
                activity.showLoadingDialog("", R.string.loading);
                etInputOtp.setText("");
                SelfCareAccountApi.getInstant(mApp).genOtpAfterAddNumber(numberVerify.getSubId(), new ApiCallbackV2<String>() {
                    @Override
                    public void onSuccess(String lastId, String s) throws JSONException {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activity.hideLoadingDialog();
                            }
                        });
                    }

                    @Override
                    public void onError(String s) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activity.hideLoadingDialog();
                            }
                        });
                    }

                    @Override
                    public void onComplete() {

                    }
                });
            }
        };
        String textNormal = activity.getString(R.string.sc_not_received_otp) + " ";
        String textToSpan = activity.getString(R.string.sc_resend);
        String textToShow = textNormal + textToSpan;
        SpannableString spannableString = new SpannableString(textToShow);
        int start = textNormal.length();
        int end = textToShow.length();
        spannableString.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color
                        .sc_primary)),
                start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvResend.setText(spannableString);
        tvResend.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
*/

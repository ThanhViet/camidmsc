package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SCLoyaltyModel implements Serializable {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("remainScore")
    @Expose
    private long remainScore;
    @SerializedName("nextRank")
    @Expose
    private String nextRank;
    @SerializedName("balances")
    @Expose
    private List<Balance> balances = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getRemainScore() {
        return remainScore;
    }

    public void setRemainScore(long remainScore) {
        this.remainScore = remainScore;
    }

    public String getNextRank() {
        return nextRank;
    }

    public void setNextRank(String nextRank) {
        this.nextRank = nextRank;
    }

    public List<Balance> getBalances() {
        return balances;
    }

    public void setBalances(List<Balance> balances) {
        this.balances = balances;
    }

    @Override
    public String toString() {
        return "SCLoyaltyModel{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", remainScore=" + remainScore +
                ", nextRank='" + nextRank + '\'' +
                ", balances=" + balances +
                '}';
    }

    public class Balance {

        @SerializedName("balance")
        @Expose
        private long balance;
        @SerializedName("loyaltyBalanceCode")
        @Expose
        private String loyaltyBalanceCode;

        public long getBalance() {
            return balance;
        }

        public void setBalance(long balance) {
            this.balance = balance;
        }

        public String getLoyaltyBalanceCode() {
            return loyaltyBalanceCode;
        }

        public void setLoyaltyBalanceCode(String loyaltyBalanceCode) {
            this.loyaltyBalanceCode = loyaltyBalanceCode;
        }

    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/7/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.search.adapter.SearchDetailAdapter;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.MusicProvisional;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class BoxMusicHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.line)
    @Nullable
    View line;
    @BindView(R.id.line_middle)
    @Nullable
    View lineMiddle;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_song_title)
    @Nullable
    TextView tvTitleSong;
    @BindView(R.id.recycler_mv)
    @Nullable
    RecyclerView recyclerMV;
    @BindView(R.id.recycler_song)
    @Nullable
    RecyclerView recyclerSong;
    @BindView(R.id.tv_see_all)
    @Nullable
    TextView tvSeeAll;
    @BindView(R.id.tv_see_all_song)
    @Nullable
    TextView tvSeeAllSong;

    private SearchAllListener.OnAdapterClick listener;
    private SearchDetailAdapter adapterMV;
    private ArrayList<Object> dataMV;
    private SearchDetailAdapter adapterSong;
    private ArrayList<Object> dataSong;

    public BoxMusicHolder(View view, Activity activity, final SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
        dataMV = new ArrayList<>();
        adapterMV = new SearchDetailAdapter(activity, dataMV);
        adapterMV.setParentType(type);
        adapterMV.setListener(listener);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerMV, null, adapterMV, true);

        dataSong = new ArrayList<>();
        adapterSong = new SearchDetailAdapter(activity, dataSong);
        adapterSong.setParentType(type);
        adapterSong.setListener(listener);
        BaseAdapter.setupVerticalRecycler(activity, recyclerSong, null, adapterSong, true);
    }

    public void bindData(Object item, int position, String keySearch) {
        if (line != null) line.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
        if (item instanceof MusicProvisional) {
            MusicProvisional model = (MusicProvisional) item;
            dataMV.clear();
            if (Utilities.notEmpty(model.getDataMV())) {
                if (tvTitle != null) {
                    tvTitle.setVisibility(View.VISIBLE);
                    tvTitle.setText(R.string.video_mv);
                }
                int size = model.getDataMV().size();
                dataMV.addAll(new ArrayList<>(model.getDataMV().subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL))));
                if (size > SearchUtils.NUM_SHOW_VIEW_ALL && tvSeeAll != null) {
                    tvSeeAll.setVisibility(View.VISIBLE);
                }
            } else {
                if (tvTitle != null) {
                    tvTitle.setVisibility(View.GONE);
                }
                if (tvSeeAll != null) {
                    tvSeeAll.setVisibility(View.GONE);
                }
            }
            adapterMV.setKeySearch(keySearch);
            adapterMV.notifyDataSetChanged();
            if (recyclerMV != null)
                recyclerMV.setVisibility(Utilities.notEmpty(dataMV) ? View.VISIBLE : View.GONE);

            dataSong.clear();
            if (Utilities.notEmpty(model.getDataSong())) {
                if (tvTitleSong != null) {
                    tvTitleSong.setVisibility(View.VISIBLE);
                    tvTitleSong.setText(R.string.search_tab_music);
                }
                int size = model.getDataSong().size();
                dataSong.addAll(new ArrayList<>(model.getDataSong().subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL))));
                if (size > SearchUtils.NUM_SHOW_VIEW_ALL && tvSeeAllSong != null) {
                    tvSeeAllSong.setVisibility(View.VISIBLE);
                }
            } else {
                if (tvTitleSong != null) {
                    tvTitleSong.setVisibility(View.GONE);
                }
                if (tvSeeAllSong != null) {
                    tvSeeAllSong.setVisibility(View.GONE);
                }
            }
            adapterSong.setKeySearch(keySearch);
            adapterSong.notifyDataSetChanged();
            if (recyclerSong != null)
                recyclerSong.setVisibility(Utilities.notEmpty(dataSong) ? View.VISIBLE : View.GONE);
        }
        boolean showLineMiddle = recyclerMV != null && recyclerMV.getVisibility() == View.VISIBLE
                && recyclerSong != null && recyclerSong.getVisibility() == View.VISIBLE;
        if (lineMiddle != null) lineMiddle.setVisibility(showLineMiddle ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.layout_title)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic)
            ((SearchAllListener.OnClickBoxMusic) listener).onClickMVMore();
    }

    @OnClick(R.id.layout_song_title)
    public void onClickSongItem() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic)
            ((SearchAllListener.OnClickBoxMusic) listener).onClickMusicMore();
    }
}

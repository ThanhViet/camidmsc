package com.metfone.selfcare.module.sc_umoney.main_umoney;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.module.sc_umoney.UMoneyStateProvider;
import com.metfone.selfcare.module.sc_umoney.check_register.CheckRegisterUMoneyFragment;
import com.metfone.selfcare.module.sc_umoney.complete.CompleteUmoneyFragment;
import com.metfone.selfcare.module.sc_umoney.event.HomeUmoneyEvent;
import com.metfone.selfcare.module.sc_umoney.home.HomeUmoneyFragment;
import com.metfone.selfcare.module.sc_umoney.mobile.MobileUmoneyFragment;
import com.metfone.selfcare.module.sc_umoney.network.UMoneyApi;
import com.metfone.selfcare.module.sc_umoney.network.model.FieldMap;
import com.metfone.selfcare.module.sc_umoney.network.response.FieldMapResponse;
import com.metfone.selfcare.module.sc_umoney.network.response.InfoResponse;
import com.metfone.selfcare.module.sc_umoney.register.RegisterUmoneyFragment;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class TabUmoneyActivity extends BaseSlidingFragmentActivity {
    ImageView ivBack;
    FrameLayout frameLayout;
    Fragment currentFragment;
    TextView tvName, tvSdt;
    ProgressBar progressBar;

    ReengAccount mAccount;
    UMoneyApi uMoneyApi;
    String currentPhone = "", currentName = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_umoney);
        initView();
        loadData();
        initEvent();
    }

    private void loadData() {
        mAccount = ApplicationController.self().getReengAccountBusiness().getCurrentAccount();
        uMoneyApi = new UMoneyApi(ApplicationController.self());
        //todo neu chua dang ki mo ra otp, dky r thi vao home
        uMoneyApi.getInfoUmoney(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                Gson gson = new Gson();
                FieldMapResponse response = gson.fromJson(data, FieldMapResponse.class);
                if (response != null) {
                    if (response.getCode() == 200 && response.getData().getFieldMap().size() != 0) {
                        UMoneyStateProvider.getInstance().setData(response.getData().getFieldMap());
                        for (FieldMap model : response.getData().getFieldMap()) {
                            if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.PHONE_NUMBER)) {
                                currentPhone = model.getValue();
                                continue;
                            }
                            if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.CUSTOMER_NAME)) {
                                currentName = model.getValue();
                            }
                        }

                        progressBar.setVisibility(View.GONE);
                        showFragment(SCConstants.UMONEY.TAB_HOME, null, true);
                    } else if (response.getCode() == 10116) {
                        progressBar.setVisibility(View.GONE);
                        Bundle bundle = new Bundle();
                        bundle.putInt(SCConstants.UMONEY.KEY_CODE_INFO, response.getCode());
                        bundle.putString(SCConstants.UMONEY.KEY_DESC_INFO, response.getDesc());
                        showFragment(SCConstants.UMONEY.TAB_CHECK_REGISTER, bundle, true);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt(SCConstants.UMONEY.KEY_CODE_INFO, response.getCode());
                        bundle.putString(SCConstants.UMONEY.KEY_DESC_INFO, response.getDesc());
                        progressBar.setVisibility(View.GONE);
                        showFragment(SCConstants.UMONEY.TAB_CHECK_REGISTER, bundle, true);
                    }
                }
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(ApplicationController.self(), TabUmoneyActivity.this.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
            }
        });
        uMoneyApi.getTelecomInfo(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                Gson gson = new Gson();
                InfoResponse response = gson.fromJson(data, InfoResponse.class);
                if (response != null && response.getCode() == 200) {
                    UMoneyStateProvider.getInstance().setInfo(response.getData());
                } else if (response != null && response.getCode() == 77) {
                    Toast.makeText(ApplicationController.self(), TabUmoneyActivity.this.getString(R.string.This_subcriber_havent_activated), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                Toast.makeText(ApplicationController.self(), TabUmoneyActivity.this.getString(R.string.e601_error_but_undefined), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        ivBack = findViewById(R.id.iv_back);
        frameLayout = findViewById(R.id.frame_sc_umoney);
        tvName = findViewById(R.id.tv_name);
        tvSdt = findViewById(R.id.tv_sdt);
        progressBar = findViewById(R.id.progress_bar);
    }

    private void initEvent() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    public void showFragment(int tabId, Bundle bundle, boolean enterAnimation) {
        enterAnimation = false;
        switch (tabId) {
            case SCConstants.UMONEY.TAB_HOME:
                tvSdt.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(currentName)) {
                    for (FieldMap model : UMoneyStateProvider.getInstance().getData()) {
                        if (model.getFieldName().equalsIgnoreCase(Constants.UMONEY.CUSTOMER_NAME)) {
                            currentName = model.getValue();
                            break;
                        }
                    }
                }
                tvName.setText(currentName);
                currentFragment = HomeUmoneyFragment.newInstance();
                break;
            case SCConstants.UMONEY.TAB_MOBILE:
                tvSdt.setVisibility(View.GONE);
                tvName.setText(this.getResources().getString(R.string.u_Mobile));
                currentFragment = MobileUmoneyFragment.newInstance();
                break;
            case SCConstants.UMONEY.TAB_CHECK_REGISTER:
                tvSdt.setVisibility(View.GONE);
                tvName.setText(this.getResources().getString(R.string.umoney));
                currentFragment = CheckRegisterUMoneyFragment.newInstance(bundle);
                break;
            case SCConstants.UMONEY.TAB_COMPLETE:
                tvSdt.setVisibility(View.GONE);
                tvName.setText(this.getResources().getString(R.string.u_Mobile));
                currentFragment = CompleteUmoneyFragment.newInstance(bundle);
                break;
            case SCConstants.UMONEY.TAB_REGISTER:
                tvSdt.setVisibility(View.GONE);
                tvName.setText(this.getResources().getString(R.string.u_Register));
                currentFragment = RegisterUmoneyFragment.newInstance();
                break;

        }

        if (currentFragment != null) {
            if (!getSupportFragmentManager().getFragments().contains(currentFragment)) {
                try {
                    if (!currentFragment.isAdded()) {
//                        if (currentFragment.getArguments() == null) {
//                            currentFragment.setArguments(bundle);
//                        } else {
//                            currentFragment.getArguments().putAll(bundle);
//                        }
                        getSupportFragmentManager()
                                .beginTransaction()
                                .disallowAddToBackStack()
//                                .setCustomAnimations(enterAnimation ? R.anim.fragment_slide_right : R.anim.fragment_slide_right, R.anim.fragment_slide_right)
                                .replace(R.id.frame_sc_umoney, currentFragment)
                                .commitAllowingStateLoss();

//                        if (originalFragment == null) {
//                            originalFragment = currentFragment;
//                        }
                    }
                } catch (IllegalStateException e) {
//                    Log.e(TAG, "showFragment", e);
                } catch (RuntimeException e) {
//                    Log.e(TAG, "showFragment", e);

                } catch (Exception e) {
//                    Log.e(TAG, "showFragment", e);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(HomeUmoneyEvent event) {
        if (tvSdt != null) {
            tvSdt.setVisibility(View.VISIBLE);
            tvSdt.setText(event.getSdt() + " - " + event.getBalance() + " Kip");
        }
    }

    @Override
    public void onBackPressed() {
        InputMethodUtils.hideSoftKeyboard(this);
        if (currentFragment instanceof HomeUmoneyFragment || currentFragment instanceof RegisterUmoneyFragment || currentFragment instanceof CheckRegisterUMoneyFragment || currentFragment == null) {
            finish();
        } else {
            showFragment(SCConstants.UMONEY.TAB_HOME, null, false);
        }
    }
}

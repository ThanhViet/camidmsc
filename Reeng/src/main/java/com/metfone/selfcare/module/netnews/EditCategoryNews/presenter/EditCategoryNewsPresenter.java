package com.metfone.selfcare.module.netnews.EditCategoryNews.presenter;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.module.netnews.EditCategoryNews.view.IEditCategoryNewsView;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.response.CategoryResponse;
import com.metfone.selfcare.util.Log;

/**
 * Created by HaiKE on 8/19/17.
 */

public class EditCategoryNewsPresenter extends BasePresenter implements IEditCategoryNewsPresenter {
    public static final String TAG = EditCategoryNewsPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;

    public EditCategoryNewsPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void updateSettingCategory(SharedPref pref, String str, int type) {
        if (type == CommonUtils.TYPE_NEWS) {
            if (pref != null) {
                pref.putString(SharedPref.PREF_KEY_NEWS_CATEGORY, str);
            }
        }
        if (getMvpView() != null && getMvpView() instanceof IEditCategoryNewsView) {
            ((IEditCategoryNewsView) getMvpView()).updateDataSuccess(str, type);
        }
    }

    @Override
    public void loadNewsCategory() {
        mNewsApi.getNewsCategory(callback);
    }

    HttpCallBack callback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            if (!isViewAttached() || !(getMvpView() instanceof IEditCategoryNewsView)) {
                return;
            }
            Gson gson = new Gson();
            CategoryResponse childNewsResponse = gson.fromJson(data, CategoryResponse.class);

            ((IEditCategoryNewsView) getMvpView()).bindData(childNewsResponse);
            ((IEditCategoryNewsView) getMvpView()).loadDataSuccess(true);
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof IEditCategoryNewsView)) {
                return;
            }
            ((IEditCategoryNewsView) getMvpView()).loadDataSuccess(false);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
}

package com.metfone.selfcare.module.tiin;

import com.metfone.selfcare.module.tiin.network.model.Category;

import java.util.ArrayList;
import java.util.List;

public class AppProvider {
    private static AppProvider mInstance;
    private ArrayList<Category> datas = new ArrayList<>();
    private String idCategory;
    public static int CLICK_CLOSE = 1;
    public static boolean isOne = false;
    public static boolean isLoadHome = false;
    private boolean isCheckConfigCategory = false;

    public static AppProvider getInstance() {
        if (mInstance == null) {
            //prevent multi instance creator in Multi thread.
            synchronized (AppProvider.class) {
                if (mInstance == null) {
                    mInstance = new AppProvider();
                }
            }
        }
        return mInstance;
    }

    public boolean isCheckConfigCategory() {
        return isCheckConfigCategory;
    }

    public void setCheckConfigCategory(boolean checkConfigCategory) {
        isCheckConfigCategory = checkConfigCategory;
    }

    public ArrayList<Category> getDatas() {
        if(datas == null){
            datas = new ArrayList<>();
        }
        return datas;
    }

    public void setDatas(ArrayList<Category> datas) {
        this.datas = datas;
    }

    public String getIdCategory() {
        if(idCategory == null){
            return "";
        }
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }
}

package com.metfone.selfcare.module.tiin.categorynow.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

public class CategoryNowTiinItemAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private final int SECTIONE_HORIZOTAL = 2;
    private final int SECTIONE_EMPTY = 0;
    private TiinEventModel model;
    private Context mContext;
    private TiinListener.onCategoryNow listener;

    public CategoryNowTiinItemAdapter(TiinEventModel model, Context mContext, TiinListener.onCategoryNow listener) {
        this.model = model;
        this.mContext = mContext;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case SECTIONE_HORIZOTAL:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_special_tiin, parent, false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final TiinModel tiinModel = getItem(position);
        if (holder.getView(R.id.iv_cover) != null) {
            ImageBusiness.setImageNew(tiinModel.getImage(), holder.getView(R.id.iv_cover));
        }
        if (holder.getView(R.id.tv_title) != null) {
            holder.setText(R.id.tv_title, tiinModel.getTitle());
        }
        if (holder.getView(R.id.tv_desc) != null) {
            holder.setText(R.id.tv_desc, tiinModel.getShapo());
        }
        if (holder.getView(R.id.tv_category) != null) {
            holder.setText(R.id.tv_category, tiinModel.getCategory());
        }
        if (holder.getView(R.id.tv_datetime) != null && !TextUtils.isEmpty(tiinModel.getHasTagName())) {
            holder.setVisible(R.id.tv_datetime, true);
            holder.setText(R.id.tv_datetime, tiinModel.getHasTagName());
            holder.setOnClickListener(R.id.tv_datetime, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClickHashTag(tiinModel);
                    }
                }
            });
        } else {
            holder.setVisible(R.id.tv_datetime, false);
        }
        if (holder.getView(R.id.button_option) != null) {
            holder.setOnClickListener(R.id.button_option, new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    listener.onItemClickMore(tiinModel);
                }
            });
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(tiinModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null) {
            return SECTIONE_HORIZOTAL;
        }
        return SECTIONE_EMPTY;
    }

    public TiinModel getItem(int position) {
        try {
            return model.getData().get(position);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
        return null;
    }

}

package com.metfone.selfcare.module.sc_umoney.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FieldMapModel implements Serializable {

    @SerializedName("fieldMap")
    @Expose
    private List<FieldMap> fieldMap = null;

    public List<FieldMap> getFieldMap() {
        if(fieldMap == null){
            fieldMap = new ArrayList<>();
        }
        return fieldMap;
    }

    public void setFieldMap(List<FieldMap> fieldMap) {
        this.fieldMap = fieldMap;
    }
}

package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.movienew.model.CountryModel;

import java.util.List;

public class CustomTopicFilm {

    @SerializedName("id")
    private String mId;

    @SerializedName("id_group")
    private String mIdGroup;

    @SerializedName("duration")
    private String mDuration;

    @SerializedName("content_filter")
    private String mContentFilter;

    @SerializedName("name")
    private String mName;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("image_path")
    private String mImagePath;

    @SerializedName("poster_path")
    private String mPosterPath;

    @SerializedName("logo_path")
    private String mLogoPath;

    @SerializedName("logo_position")
    private String mLogoPosition;

    @SerializedName("categories")
    private List<CountryModel> mListCategory;

    @SerializedName("year")
    private String mYear;

    @SerializedName("country")
    private String mCountry;

    @SerializedName("userlogin")
    private String mUserLogin;

    @SerializedName("created_at")
    private String mCreatedAt;

    @SerializedName("isView")
    private String mIsView;

    @SerializedName("isFilmGroups")
    private String mIsFilmGroups;

    @SerializedName("total")
    private String mTotal;

    @SerializedName("current_film")
    private String mCurrentFilm;

    @SerializedName("link_wap")
    private String mLinkWap;

    @SerializedName("is_free")
    private String mIsFree;

    @SerializedName("imdb")
    private String mImdb;

    @SerializedName("drm_content_id")
    private String mDrmContentId;

    @SerializedName("isFreeContent")
    private String mIsFreeContent;

    @SerializedName("isFreeData")
    private String mIsFreeData;

    @SerializedName("type_film")
    private String mTypeFilm;

    @SerializedName("episodes")
    private String mEpisodes;

    @SerializedName("chapter")
    private String mChapter;

    @SerializedName("hd_price")
    private String mHdPrice;

    @SerializedName("hd_price_FreeData")
    private String mHdPriceFreeData;

    @SerializedName("sd_price")
    private String mSdPrice;

    @SerializedName("sd_price_FreeData")
    private String mSdPriceFreeData;

    @SerializedName("publisher")
    private String mPublisher;

    @SerializedName("productInfo")
    private String mProductInfo;

    @SerializedName("trailer_url")
    private String mTrailerUrl;

    @SerializedName("trailer_image")
    private String mTrailerImage;

    @SerializedName("intro_Path")
    private String mIntroPath;

    @SerializedName("label_url")
    private String mLabelUrl;

    @SerializedName("free_content_all")
    private String mFreeContentAll;

    @SerializedName("label_txt")
    private String mLabelTxt;

    @SerializedName("isInternational")
    private String mIsInternational;

    @SerializedName("total_like")
    private String mTotalLike;

    @SerializedName("total_unlike")
    private String mTotalUnlike;

    @SerializedName("total_share")
    private String mTotalShare;

    @SerializedName("total_comment")
    private String mTotalComment;

    @SerializedName("deepLinkUrl")
    private String mDeeplinkUrl;

    @SerializedName("displayIMDB")
    private String mDisplayIMDB;

    @SerializedName("displayPrize")
    private String mDisplayPrize;

    @SerializedName("displayDirector")
    private String mDisplayDirector;

    @SerializedName("isEncodeLinkToken")
    private String mIsEncodeLinkToken;

    public String getId() {
        return mId;
    }

    public String getIdGroup() {
        return mIdGroup;
    }

    public String getDuration() {
        return mDuration;
    }

    public String getContentFilter() {
        return mContentFilter;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public String getLogoPath() {
        return mLogoPath;
    }

    public String getLogoPosition() {
        return mLogoPosition;
    }

    public List<CountryModel> getListCategory() {
        return mListCategory;
    }

    public String getYear() {
        return mYear;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getUserLogin() {
        return mUserLogin;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public String getIsView() {
        return mIsView;
    }

    public String getIsFilmGroups() {
        return mIsFilmGroups;
    }

    public String getTotal() {
        return mTotal;
    }

    public String getCurrentFilm() {
        return mCurrentFilm;
    }

    public String getLinkWap() {
        return mLinkWap;
    }

    public String getIsFree() {
        return mIsFree;
    }

    public String getImdb() {
        return mImdb;
    }

    public String getProductInfo() {
        return mProductInfo;
    }

    public String getTrailerUrl() {
        return mTrailerUrl;
    }

    public String getTrailerImage() {
        return mTrailerImage;
    }

    public String getIntroPath() {
        return mIntroPath;
    }

    public String getLabelUrl() {
        return mLabelUrl;
    }

    public String getFreeContentAll() {
        return mFreeContentAll;
    }

    public String getLabelTxt() {
        return mLabelTxt;
    }

    public String getIsInternational() {
        return mIsInternational;
    }

    public String getTotalLike() {
        return mTotalLike;
    }

    public String getTotalUnlike() {
        return mTotalUnlike;
    }

    public String getTotalShare() {
        return mTotalShare;
    }

    public String getTotalComment() {
        return mTotalComment;
    }

    public String getDeeplinkUrl() {
        return mDeeplinkUrl;
    }

    public String getDisplayIMDB() {
        return mDisplayIMDB;
    }

    public String getDisplayPrize() {
        return mDisplayPrize;
    }

    public String getDisplayDirector() {
        return mDisplayDirector;
    }

    public String getIsEncodeLinkToken() {
        return mIsEncodeLinkToken;
    }

    public String getDrmContentId() {
        return mDrmContentId;
    }

    public String getIsFreeContent() {
        return mIsFreeContent;
    }

    public String getIsFreeData() {
        return mIsFreeData;
    }

    public String getTypeFilm() {
        return mTypeFilm;
    }

    public String getEpisodes() {
        return mEpisodes;
    }

    public String getChapter() {
        return mChapter;
    }

    public String getHdPrice() {
        return mHdPrice;
    }

    public String getHdPriceFreeData() {
        return mHdPriceFreeData;
    }

    public String getSdPrice() {
        return mSdPrice;
    }

    public String getSdPriceFreeData() {
        return mSdPriceFreeData;
    }

    public String getPublisher() {
        return mPublisher;
    }
}

package com.metfone.selfcare.module.tiin.maintiin.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.module.tiin.base.BasePresenter;
import com.metfone.selfcare.module.tiin.maintiin.fragment.TabTiinFragment;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.Category;

import org.json.JSONObject;

import java.util.ArrayList;

public class MainTiinPresenter extends BasePresenter implements IMainTiinMvpPresenter {
    TiinApi mTiinApi;
    long startTime;
    HttpCallBack httpCallBack = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof TabTiinFragment)) {
                return;
            }
            Gson gson = new Gson();
            ArrayList<Category> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<ArrayList<Category>>() {
            }.getType());

            ((TabTiinFragment) getMvpView()).bindData(response);
            ((TabTiinFragment) getMvpView()).loadDataSuccess(true);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_LIST_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof TabTiinFragment)) {
                return;
            }
            ((TabTiinFragment) getMvpView()).loadDataSuccess(false);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_LIST_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    public MainTiinPresenter() {
        mTiinApi = new TiinApi(ApplicationController.self());
    }

    @Override
    public void loadApiCategory() {
        startTime = System.currentTimeMillis();
        mTiinApi.getConfigCategory(httpCallBack);
    }

    @Override
    public void loadCategory(SharedPrefs mPref) {
        if (mPref != null) {
            String str = mPref.get(SharedPrefs.LIST_ID_CATAGORY, String.class);
            if (getMvpView() != null && getMvpView() instanceof TabTiinFragment) {
                ((TabTiinFragment) getMvpView()).bindDataCategory(str);
            }
        }
    }
}


package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Country {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parentid")
    @Expose
    private Integer parentId;
    @SerializedName("categoryname")
    @Expose
    private String categoryName;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("categoryid")
    @Expose
    private String categoryId;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("url_images")
    @Expose
    private String urlImages;
    @SerializedName("settop")
    @Expose
    private Integer setTop;
    @SerializedName("total_sub_tab")
    @Expose
    private Integer totalSubTab;
    @SerializedName("eventID")
    @Expose
    private Integer eventID;
    @SerializedName("typefilmID")
    @Expose
    private Integer typeFilmID;

    private boolean isChecked;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getUrlImages() {
        return urlImages;
    }

    public void setUrlImages(String urlImages) {
        this.urlImages = urlImages;
    }

    public Integer getSetTop() {
        return setTop;
    }

    public void setSetTop(Integer setTop) {
        this.setTop = setTop;
    }

    public Integer getTotalSubTab() {
        return totalSubTab;
    }

    public void setTotalSubTab(Integer totalSubTab) {
        this.totalSubTab = totalSubTab;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getTypeFilmID() {
        return typeFilmID;
    }

    public void setTypeFilmID(Integer typeFilmID) {
        this.typeFilmID = typeFilmID;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}

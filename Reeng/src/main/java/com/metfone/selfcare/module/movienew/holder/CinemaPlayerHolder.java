/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/3
 *
 */

package com.metfone.selfcare.module.movienew.holder;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.encrypt.XXTEACrypt;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.tab_video.VideoPlayerView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;

public class CinemaPlayerHolder extends BaseAdapter.ViewHolder {
    private final String TAG = "CinemaPlayerHolder";

    @BindView(R.id.img_movie)
    ImageView imgMovie;

    @BindView(R.id.tv_title)
    CamIdTextView tvTitle;

    @BindView(R.id.layoutPlayer)
    FrameLayout layoutPlayer;

    @BindView(R.id.player_play)
    ImageView btnPlay;
    @BindView(R.id.player_mute)
    ImageView btnMute;
    @BindView(R.id.player_full_screen)
    ImageView btnFull;
    @BindView(R.id.player_progress_bar)
    ProgressBar progress_bar;

    VideoPlayerView playerView;

    private TabMovieListener.OnAdapterClick listener;
    private TabMovieListener.IPlayVideoNow mPlayVideoNowCallback;
    private View rootView;
    private Activity activity;
    private SimpleExoPlayer player;
    private long currentTime = 0;
    private MovieApi movieApi;
    private boolean isPlaying = false;
    private boolean isEndVideo = false;
    private String url;
    public boolean autoPlay = true;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (btnPlay != null) {
                btnPlay.setVisibility(View.GONE);
            }
        }
    };
    private Handler handler = new Handler();
    private Runnable runnableTime = new Runnable() {
        @Override
        public void run() {
            if (progress_bar != null) {
                progress_bar.setProgress((int) ((player.getCurrentPosition() * 100) / player.getDuration()));
            }
            if (isPlaying && player != null) {
                currentTime = player.getCurrentPosition();
            }
            handler.postDelayed(this, 1000);
        }
    };

    public CinemaPlayerHolder(View view, Activity activity, TabMovieListener.OnAdapterClick listener, TabMovieListener.IPlayVideoNow mPlayVideoNowCallback) {
        super(view);
        this.activity = activity;
        this.rootView = view;
        this.listener = listener;
        movieApi = new MovieApi();
        this.mPlayVideoNowCallback = mPlayVideoNowCallback;
        if (layoutPlayer != null) {
            playerView = new VideoPlayerView(activity);
            playerView.setUseController(false);
            playerView.enableFast(true);
            layoutPlayer.addView(playerView);
        }
    }

    @Override
    public void bindData(Object item, int position) {
        MoviePagerModel moviePagerModel = (MoviePagerModel) item;
        ArrayList<Object> homeDatas = moviePagerModel.getList();
        if (Utilities.notEmpty(homeDatas)) {
            HomeData homeData = (HomeData) homeDatas.get(0);
            tvTitle.setText(moviePagerModel.getTitle() + " " + homeData.getName());
            Glide.with(activity)
                    .load(homeData.getPosterPath()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                    .into(imgMovie);
            Picasso.with(activity).load(homeData.getPosterPath()).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    playerView.setDefaultArtwork(bitmap);
                    playerView.setUseArtwork(true);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });
            tvTitle.setOnClickListener(v -> listener.onClickTitleBox(homeDatas.get(0), position));
            btnPlay.setOnClickListener(view -> {
                if (player != null) {
                    btnPlay.removeCallbacks(runnable);
                    boolean oldStatus = isPlaying;
                    isPlaying = !isPlaying;
                    if (isEndVideo) {
                        isEndVideo = false;
                        currentTime = 0;
                        player.seekTo(0);
                    }
                    if (oldStatus) {
                        Log.d(TAG, "Pause");
                        btnPlay.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_play_available));
                        currentTime = player.getCurrentPosition();
                        player.setPlayWhenReady(false);
                        handler.removeCallbacks(runnableTime);
                    } else {
                        Log.d(TAG, "Play");
                        btnPlay.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_pause_available));
                        player.setPlayWhenReady(true);
                        if (currentTime == player.getDuration()) {
                            currentTime = 0;
                        }
                        player.seekTo(currentTime);
                        btnPlay.postDelayed(runnable, 2000);
                        handler.removeCallbacks(runnableTime);
                        handler.postDelayed(runnableTime, 0);
                    }
                }
            });
            btnMute.setOnClickListener(v -> {
                if (player != null) {
                    if (player.getVolume() == 0) {
                        player.setVolume(1);
                        btnMute.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_volume_up_new));
                    } else {
                        player.setVolume(0);
                        btnMute.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_volume_off_new));
                    }
                }
            });
            btnFull.setOnClickListener(v -> {
                if (player != null) {
                    player.setPlayWhenReady(false);
                    btnPlay.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_play_available));
                    currentTime = player.getCurrentPosition();
                }
                progress_bar.removeCallbacks(runnableTime);

                mPlayVideoNowCallback.playTrailer(homeData.getName(), url, currentTime);
            });
            itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        showPlay();
                    }
                    return false;
                }
            });
            movieApi.getMovieDetail(homeData.getId().toString(), new ApiCallbackV2<Movie>() {
                @Override
                public void onSuccess(String msg, Movie result) throws JSONException {
                    if (result != null && result.getOriginalPath() != null && result.getOriginalPath().size() > 0) {
                        String urlOriginal = result.getTrailerUrl();
                        String url = XXTEACrypt.decryptBase64StringToString(urlOriginal, BuildConfig.KEY_XXTEA_5DMAX_MOVIES);
                        url = url == null ? urlOriginal : url;
                        init(url);
                    }
                }

                @Override
                public void onError(String s) {

                }

                @Override
                public void onComplete() {

                }
            });
        }
    }

    private MediaSource buildMediaSource(DefaultDataSourceFactory dataSourceFactory, String url) {
        Uri uri = Uri.parse(url);
        @C.ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultDashChunkSource.Factory(dataSourceFactory), dataSourceFactory).createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultSsChunkSource.Factory(dataSourceFactory), dataSourceFactory).createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    private void init(String url) {
        this.url = url;
        if (playerView == null || StringUtils.isEmpty(url)) {
            return;
        }

        if (player == null) {
            TrackSelector trackSelectorDef = new DefaultTrackSelector();
            player = ExoPlayerFactory.newSimpleInstance(activity, trackSelectorDef);
            player.setVolume(0);
        }

        String userAgent = Util.getUserAgent(activity, activity.getString(R.string.app_name));
        DefaultDataSourceFactory sourceFactory = new DefaultDataSourceFactory(activity, userAgent);
        player.prepare(buildMediaSource(sourceFactory, url));
        playerView.setPlayer(player);
        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_BUFFERING || playbackState == Player.STATE_READY) {
                    if (!isPlaying && player.getPlayWhenReady()) {
                        player.setPlayWhenReady(false);
                        updateButtonPlay(false);
                    }
                } else if (playbackState == Player.STATE_ENDED) {
                    currentTime = 0;
                    player.seekTo(currentTime);
                } else if (playbackState == Player.STATE_IDLE) {
                    isPlaying = false;
                    updateButtonPlay(false);
                }
                Log.d(TAG, "isPlaying : " + isPlaying + ". playWhenReady : " + playWhenReady + ", playbackState : " + playbackState);
            }
        });
        if (isPlaying) {
            player.setPlayWhenReady(true);
        }
        progress_bar.setProgress((int) ((currentTime * 100) / player.getDuration()));
        Log.d(TAG, "Init available now");
    }

    private void showPlay() {
        btnPlay.removeCallbacks(runnable);
        btnPlay.setVisibility(View.VISIBLE);
        if (player != null && player.isPlaying()) {
            hidePlay();
        }
    }

    private void hidePlay() {
        btnPlay.postDelayed(runnable, 2000);
    }

    public void startVideo() {
        Log.d(TAG, "startVideo");
        if (isPlaying) {
            return;
        }
        if (player == null) {
            return;
        }
        isPlaying = true;
        btnPlay.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_pause_available));
        player.setPlayWhenReady(true);
        player.seekTo(currentTime);
        btnPlay.postDelayed(runnable, 2000);
        handler.removeCallbacks(runnableTime);
        handler.postDelayed(runnableTime, 0);
    }

    public void stopVideo() {
        Log.d(TAG, "stopVideo");
        if (player == null) {
            return;
        }
        if (!isPlaying) {
            if (player.getPlayWhenReady()) {
                player.setPlayWhenReady(false);
            }
            return;
        }
        isPlaying = false;
        if (!player.isPlaying()) {
            return;
        }
        player.setPlayWhenReady(false);
        btnPlay.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_play_available));
        btnPlay.setVisibility(View.VISIBLE);
        currentTime = player.getCurrentPosition();
        progress_bar.removeCallbacks(runnableTime);
        btnPlay.setVisibility(View.VISIBLE);
    }

    private void updateButtonPlay(boolean play) {
        if (play) {
            btnPlay.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_pause_available));
            handler.removeCallbacks(runnableTime);
            handler.postDelayed(runnableTime, 0);
        } else {
            btnPlay.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_play_available));
            handler.removeCallbacks(runnableTime);
            btnPlay.setVisibility(View.VISIBLE);
        }
    }

    public Rect getRect() {
        Rect rect = new Rect();
        rootView.getGlobalVisibleRect(rect);
        return rect;
    }

    public int getHeight() {
        if (rootView != null) {
            return rootView.getMeasuredHeight();
        }
        return 0;
    }

    public void updateProgress(long time) {
        currentTime = time;
        progress_bar.setProgress((int) ((currentTime * 100) / player.getDuration()));
        if (player != null) {
            player.seekTo(time);
        }
    }

    public void forcePlay() {
        isPlaying = true;
    }
}
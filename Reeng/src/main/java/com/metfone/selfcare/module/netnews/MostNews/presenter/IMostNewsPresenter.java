package com.metfone.selfcare.module.netnews.MostNews.presenter;


import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;

/**
 * Created by HaiKE on 8/19/17.
 */

public interface IMostNewsPresenter extends MvpPresenter {

    void loadData(int cateId, int page, long unixTime);
    void updateLastNews(SharedPref pref, int id);
    int getLastNews(SharedPref pref);
}

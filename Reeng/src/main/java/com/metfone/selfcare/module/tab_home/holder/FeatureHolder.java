/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/7/27
 *
 */

package com.metfone.selfcare.module.tab_home.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.tab_home.adapter.FeatureAdapter;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.widget.HorizontalScrollBarView;
import com.vtm.adslib.template.TemplateView;

import java.util.ArrayList;

import butterknife.BindView;

public class FeatureHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
//    @BindView(R.id.indicator)
//    OverflowPagerIndicator indicator;

    private ArrayList<Object> data;
    private FeatureAdapter adapter;
    private int pastVisibleItems, visibleItemCount, totalItemCount;

    public FeatureHolder(View view, Activity activity, TabHomeListener.OnAdapterClick listener) {
        super(view);
        data = new ArrayList<>();
        adapter = new FeatureAdapter(activity, listener);
//<<<<<<< Updated upstream
        CustomGridLayoutManager customGridLayoutManager = new CustomGridLayoutManager(activity, 2);
        customGridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        BaseAdapter.setupGridRecycler(activity, recyclerView, customGridLayoutManager, adapter, 2, 0, false);
//=======
////        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, false);
////        indicator.attachToRecyclerView(recyclerView);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL, false);
//        recyclerView.setLayoutManager(gridLayoutManager);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setHasFixedSize(true);
//>>>>>>> Stashed changes
        HorizontalScrollBarView horizontalScrollBarView = view.findViewById(R.id.horizontalScrollBarView);
        horizontalScrollBarView.setRecyclerView(recyclerView);

    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            data.clear();
//            adapter.setHas2Lines(false);
            if (Utilities.notEmpty(model.getList())) {
//<<<<<<< HEAD
//                int size = model.getList().size();
//                if (size > 0) {
//                    adapter.setHas2Lines(size > 4);
//                    int page = size / 8;
//                    int fromIndex;
//                    int toIndex = 0;
//                    for (int i = 0; i < page; i++) {
////                        TabHomeModel tmp = new TabHomeModel();
////                        tmp.setType(TabHomeModel.TYPE_FEATURE);
//                        fromIndex = toIndex;
//                        toIndex = (i + 1) * 8;
//                        Log.i("Feature2Holder", "fromIndex: " + fromIndex + " - toIndex: " + toIndex);
//                        data.addAll(model.getList().subList(fromIndex, toIndex));
////                        data.add(tmp);
//                    }
//                    if (size % 8 > 0) {
//                        TabHomeModel tmp = new TabHomeModel();
//                        tmp.setType(TabHomeModel.TYPE_FEATURE);
//                        fromIndex = toIndex;
//                        toIndex = size;
//                        Log.i("Feature2Holder", "fromIndex: " + fromIndex + " - toIndex: " + toIndex);
//                        data.addAll(model.getList().subList(fromIndex, toIndex));
////                        data.add(tmp);
//                    }
//                }
//=======
                data.addAll(model.getList());
//                int size = model.getList().size();
//                if (size > 0) {
//                    adapter.setHas2Lines(size > 4);
//                    int page = size / 8;
//                    int fromIndex;
//                    int toIndex = 0;
//                    for (int i = 0; i < page; i++) {
//                        TabHomeModel tmp = new TabHomeModel();
//                        tmp.setType(TabHomeModel.TYPE_FEATURE);
//                        fromIndex = toIndex;
//                        toIndex = (i + 1) * 8;
//                        Log.i("Feature2Holder", "fromIndex: " + fromIndex + " - toIndex: " + toIndex);
//                        tmp.getList().addAll(model.getList().subList(fromIndex, toIndex));
//                        data.add(tmp);
//                    }
//                    if (size % 8 > 0) {
//                        TabHomeModel tmp = new TabHomeModel();
//                        tmp.setType(TabHomeModel.TYPE_FEATURE);
//                        fromIndex = toIndex;
//                        toIndex = size;
//                        Log.i("Feature2Holder", "fromIndex: " + fromIndex + " - toIndex: " + toIndex);
//                        tmp.getList().addAll(model.getList().subList(fromIndex, toIndex));
//                        data.add(tmp);
//                    }
//                }
//>>>>>>> feature/MochaNewUIv2
            }

            adapter.setItems(data);
            adapter.notifyDataSetChanged();
        }
    }

}
package com.metfone.selfcare.module.keeng.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RankModel implements Serializable {

    private static final long serialVersionUID = -5901187408942120391L;

    @SerializedName("title")
    private String title;

    @SerializedName("type")
    private int type;

    @SerializedName("image")
    private String image;

    @SerializedName("image_video")
    private String imageVideo;

    @SerializedName("listSong")
    private List<AllModel> listSong;

    private int source;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageVideo() {
        if (TextUtils.isEmpty(imageVideo))
            return image;
        return imageVideo;
    }

    public void setImageVideo(String imageVideo) {
        this.imageVideo = imageVideo;
    }

    public List<AllModel> getListSong() {
        if (listSong == null)
            listSong = new ArrayList<>();
        return listSong;
    }

    public boolean isEmpty() {
        return listSong == null || listSong.isEmpty();
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "{" +
                "title='" + title + '\'' +
                ", type=" + type +
                ", image='" + image + '\'' +
                '}';
    }
}

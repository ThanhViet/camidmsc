package com.metfone.selfcare.module.metfoneplus.topup.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransMobileResponse {
    String responseCode;
    String responseMessage;
    String respcode;
    String respdesc;
    String error_field;
    String error_description;
    boolean status;
    String redirect_url;
}

package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;

import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.holder.SingerHolder;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class SingerHotAdapter extends BaseAdapterRecyclerView {
    private List<Topic> datas;

    public SingerHotAdapter(Context context, List<Topic> datas, String ga_source) {
        super(context, ga_source);
        this.datas = datas;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) == null) {
            return ITEM_LOAD_MORE;
        }
        return ITEM_SINGER;
    }

    @Override
    public int getItemCount() {
        if (datas != null)
            return datas.size();
        return 0;
    }

    @Override
    public Topic getItem(int position) {
        if (datas != null)
            try {
                return datas.get(position);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (holder instanceof SingerHolder) {
            SingerHolder itemHolder = (SingerHolder) holder;
            Topic item = getItem(position);
            if (item != null) {
                itemHolder.name.setText(item.getName());
                ImageBusiness.setAvatarSinger(item.getAvatar(), itemHolder.image, position);
            }
        }
    }

}

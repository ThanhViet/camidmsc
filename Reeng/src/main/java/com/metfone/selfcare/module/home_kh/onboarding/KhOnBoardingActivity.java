package com.metfone.selfcare.module.home_kh.onboarding;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.module.movie.fragment.MoviePagerFragment;
import com.rd.PageIndicatorView;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.util.Utilities;

public class KhOnBoardingActivity extends AppCompatActivity {

    private RelativeLayout container;
    private ViewPager vpOnBoarding;
    private PageIndicatorView pageIndicatorView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleManager.setLocale(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding_kh);

        container = findViewById(R.id.container_on_boarding);
        vpOnBoarding = findViewById(R.id.viewpager_onboarding);
        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        Utilities.adaptViewForInsertBottom(container);

        OnBoardingPagerAdapter adt = new OnBoardingPagerAdapter(getSupportFragmentManager());
        vpOnBoarding.setAdapter(adt);
        pageIndicatorView.setViewPager(vpOnBoarding);

        DynamicSharePref.getInstance().put(Constants.PREFERENCE.PREF_ON_BOARDING_SHOWED, true);
    }

    public void setCurrentPage(int position) {
        vpOnBoarding.setCurrentItem(position, true);
    }

    public void gotoHome(Boolean lesdothis) {
        Intent i = new Intent(this, HomeActivity.class);
        i.putExtra(MoviePagerFragment.LOAD_DATA_SERVICE, true);
        i.putExtra("lesdothis", lesdothis);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

}

package com.metfone.selfcare.module.metfoneplus.topup;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.HistoryTopUpMFragmentBinding;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseBindingFragment;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.HistoryRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.BaseResponseMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.HistoryResponse;
import com.metfone.selfcare.module.metfoneplus.topup.viewholder.HistoryTopUpViewHolder;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class HistoryTopUpMPFragment extends MPBaseBindingFragment<HistoryTopUpMFragmentBinding> implements ItemViewClickListener, View.OnClickListener {

    private BaseAdapter mAmountAdapter;
    private MetfonePlusClient client = new MetfonePlusClient();

    public static HistoryTopUpMPFragment newInstance() {
        return new HistoryTopUpMPFragment();
    }

    private List<HistoryResponse.History> mHistoryResponses = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.history_top_up_m_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mBinding.viewRootlayout);
        initData();
        initView();
        initListener();
    }

    private void initData() {
        client.getHistoryTopUpMP(new HistoryRequest(), new ApiCallback<BaseResponseMP<HistoryResponse>>() {
            @Override
            public void onResponse(Response<BaseResponseMP<HistoryResponse>> response) {
                if (response != null && response.body() != null) {
                    mHistoryResponses = HistoryResponse.convertHistoryModel(mParentActivity, response.body().getData().getWsResponse());
                    mAmountAdapter.updateAdapter(mHistoryResponses);
                    mBinding.tvNotData.setVisibility(mHistoryResponses.size() > 0 ? View.GONE : View.VISIBLE);
                } else {
                    mBinding.tvNotData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(Throwable error) {
                mBinding.tvNotData.setVisibility(View.VISIBLE);
            }
        });
    }

    private void initView() {
        HistoryTopUpViewHolder historyTopUpViewHolder = new HistoryTopUpViewHolder(getActivity().getWindow().getDecorView().getRootView(), this);
        mAmountAdapter = new BaseAdapter(mHistoryResponses, getContext(), R.layout.item_history_topup_mp, historyTopUpViewHolder);
        mBinding.recyclerHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerHistory.setAdapter(mAmountAdapter);
    }

    private void initListener() {
        mBinding.btnBack.setOnClickListener(this);
    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                popBackStackFragment();
                break;
        }
    }
}

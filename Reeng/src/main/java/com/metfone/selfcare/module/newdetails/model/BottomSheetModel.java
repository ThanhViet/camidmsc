package com.metfone.selfcare.module.newdetails.model;

/**
 * Created by HaiKE on 9/15/17.
 */

public class BottomSheetModel {

    int type;
    int resIcon = 0;
    int resTitle = 0;
    String title = "";
    NewsModel newsModel;
    boolean isHaveSegment = false;

    public BottomSheetModel(int type, int resIcon, String title) {
        this.type = type;
        this.resIcon = resIcon;
        this.title = title;
        isHaveSegment = false;
    }

    public BottomSheetModel(int type, int resIcon, int resTitle) {
        this.type = type;
        this.resIcon = resIcon;
        this.resTitle = resTitle;
        isHaveSegment = false;
    }

    public BottomSheetModel(int type, int resIcon, String title, boolean isHaveSegment) {
        this.type = type;
        this.resIcon = resIcon;
        this.title = title;
        this.isHaveSegment = isHaveSegment;
    }

    public BottomSheetModel(int type, int resIcon, int resTitle, boolean isHaveSegment) {
        this.type = type;
        this.resIcon = resIcon;
        this.resTitle = resTitle;
        this.isHaveSegment = isHaveSegment;
    }

    public NewsModel getNewsModel() {
        return newsModel;
    }

    public BottomSheetModel setNewsModel(NewsModel newsModel) {
        this.newsModel = newsModel;
        return this;
    }

    public int getResTitle() {
        return resTitle;
    }

    public void setResTitle(int resTitle) {
        this.resTitle = resTitle;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getResIcon() {
        return resIcon;
    }

    public void setResIcon(int resIcon) {
        this.resIcon = resIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isHaveSegment() {
        return isHaveSegment;
    }

    public void setHaveSegment(boolean haveSegment) {
        isHaveSegment = haveSegment;
    }
}

package com.metfone.selfcare.module.metfoneplus.search.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AvailableNumber implements Serializable {
    @SerializedName("isdn")
    String isdn;
    @SerializedName("price")
    String price;
    @SerializedName("status")
    String status;
    @SerializedName("type")
    int type;
    @SerializedName("typeIsdn")
    int typeIsdn;
    @SerializedName("monthlyFee")
    int monthlyFee;
    @SerializedName("registerFee")
    int registerFee;
    @SerializedName("product")
    String product;
}

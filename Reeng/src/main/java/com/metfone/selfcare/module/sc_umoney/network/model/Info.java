package com.metfone.selfcare.module.sc_umoney.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Info {
    @SerializedName("serviceType")
    @Expose
    private int serviceType;
    @SerializedName("shopCode")
    @Expose
    private String shopCode;
    @SerializedName("standard")
    @Expose
    private int standard;
    @SerializedName("startDatetime")
    @Expose
    private String startDatetime;
    @SerializedName("idType")
    @Expose
    private int idType;
    @SerializedName("isdn")
    @Expose
    private long isdn;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("imsi")
    @Expose
    private long imsi;
    @SerializedName("birthDate")
    @Expose
    private String birthDate;
    @SerializedName("idNo")
    @Expose
    private String idNo;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("subId")
    @Expose
    private long subId;
    @SerializedName("serial")
    @Expose
    private long serial;
    @SerializedName("custID")
    @Expose
    private long custID;
    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("subType")
    @Expose
    private int subType;
    @SerializedName("channelCode")
    @Expose
    private String channelCode;
    @SerializedName("status")
    @Expose
    private int status;

    public int getServiceType() {
        return serviceType;
    }

    public void setServiceType(int serviceType) {
        this.serviceType = serviceType;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public int getStandard() {
        return standard;
    }

    public void setStandard(int standard) {
        this.standard = standard;
    }

    public String getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(String startDatetime) {
        this.startDatetime = startDatetime;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public long getIsdn() {
        return isdn;
    }

    public void setIsdn(long isdn) {
        this.isdn = isdn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getImsi() {
        return imsi;
    }

    public void setImsi(long imsi) {
        this.imsi = imsi;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public long getSubId() {
        return subId;
    }

    public void setSubId(long subId) {
        this.subId = subId;
    }

    public long getSerial() {
        return serial;
    }

    public void setSerial(long serial) {
        this.serial = serial;
    }

    public long getCustID() {
        return custID;
    }

    public void setCustID(long custID) {
        this.custID = custID;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

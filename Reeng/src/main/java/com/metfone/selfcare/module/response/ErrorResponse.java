package com.metfone.selfcare.module.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.ErrorModel;

public class ErrorResponse {
    @SerializedName("error")
    @Expose
    private ErrorModel error;

    public ErrorModel getError() {
        return error;
    }

    public void setError(ErrorModel errorModel) {
        this.error = errorModel;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/3
 *
 */

package com.metfone.selfcare.module.movienew.holder;

import android.app.Activity;
import android.graphics.Rect;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.movienew.model.HomeCategory;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;

public class CinemaBannerHolder extends BaseAdapter.ViewHolder {

    @Nullable
    @BindView(R.id.img_movie)
    ImageView imgMovie;

    @Nullable
    @BindView(R.id.txt_title)
    TextView txtTitle;

    @Nullable
    @BindView(R.id.txt_type)
    TextView txtType;

    @Nullable
    @BindView(R.id.img_refresh)
    ImageView imgRefresh;

    @Nullable
    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    @BindView(R.id.card_view)
    CardView cardView;

    private TabMovieListener.OnAdapterClick listener;
    private TabMovieListener.IPlayVideoNow mPlayVideoNowCallback;
    private View rootView;
    private Activity activity;
    public CinemaBannerHolder(View view, Activity activity, TabMovieListener.OnAdapterClick listener, TabMovieListener.IPlayVideoNow iPlayVideoNow) {
        super(view);
        this.activity = activity;
        this.rootView = view;
        this.listener = listener;
        this.mPlayVideoNowCallback = iPlayVideoNow;
    }

    public void startAnimation () {
//        ShimmerFrameLayout shimmerFrameLayout = rootView.findViewById(R.id.shimmerFrameLayout);
//        ShimmerFrameLayout shimmerFrameListLayout = rootView.findViewById(R.id.shimmerFrameListLayout);
//        shimmerFrameListLayout.startShimmer();
//        shimmerFrameLayout.startShimmer();
    }
    @Override
    public void bindData(Object item, int position) {

        MoviePagerModel moviePagerModel = (MoviePagerModel) item;
        ArrayList<Object> homeDatas = moviePagerModel.getList();
        if (Utilities.notEmpty(homeDatas)) {
            HomeData homeData = (HomeData) homeDatas.get(0);
            if (!TextUtils.isEmpty(homeData.getLogoPath())) {
                Glide.with(activity)
                        .load(homeData.getLogoPath()).error(R.drawable.cinema_loading_bg).placeholder(R.drawable.cinema_loading_bg)
                        .into(ivLogo);
            }
            Glide.with(activity)
                    .load(homeData.getImagePath()).error(R.drawable.cinema_loading_bg).placeholder(R.drawable.cinema_loading_bg)
                    .into(imgMovie);

            if (txtTitle != null) {
                txtTitle.setText(homeData.getName());
            }
            if (Utilities.notEmpty(homeData.getCategories()) && txtType != null) {
                txtType.setText(getCategories(homeData));
            }
            if(itemView != null){
                itemView.setOnClickListener(v -> listener.onClickTitleBox(homeDatas.get(0), position));
            }
            if(itemView.findViewById(R.id.btn_play) != null){
                itemView.findViewById(R.id.btn_play).setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        mPlayVideoNowCallback.onPlayNow(new Movie(homeData));
                    }
                });
            }
            if(itemView.findViewById(R.id.txt_play) != null){
                itemView.findViewById(R.id.txt_play).setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        mPlayVideoNowCallback.onPlayNow(new Movie(homeData));
                    }
                });
            }
            if (imgRefresh != null) {
                if (moviePagerModel.isShowRefresh()) {
                    imgRefresh.setVisibility(View.VISIBLE);
                } else {
                    imgRefresh.setVisibility(View.GONE);
                }
                imgRefresh.setOnClickListener(v -> listener.onClickRefresh());
            }
        }
    }
    public int getHeight(){
        Rect rect = new Rect();
        if(cardView != null){
            return cardView.getMeasuredHeight();
        }
        return 0;
    }
    private String getCategories(HomeData homeData) {
        if ( homeData== null || homeData.getCategories() == null || homeData.getCategories().size() == 0)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        int length = homeData.getCategories().size();
        for (int i = 0; i < length; i++) {
            HomeCategory category = homeData.getCategories().get(i);
            if (i != length - 1) {
                stringBuilder.append(category.getCategoryName() + " \u2022 ");
            } else {
                stringBuilder.append(category.getCategoryName());
            }
        }
        return stringBuilder.toString();
    }
}
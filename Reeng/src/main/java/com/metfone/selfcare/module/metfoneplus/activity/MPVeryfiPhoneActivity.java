package com.metfone.selfcare.module.metfoneplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.module.metfoneplus.dialog.MPCombinePhoneDialog;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddPhoneFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPVerifyPhoneFragment;
import com.metfone.selfcare.util.Utilities;

import java.util.List;

public class MPVeryfiPhoneActivity extends BaseSlidingFragmentActivity {
    private static final String TAG = "MPVeryfiPhoneActivity";

    private MPAddPhoneFragment mMPAddPhoneFragment;
    private MPVerifyPhoneFragment mMPVerifyPhoneFragment;
    private static final String TAG_MP_ADD_PHONE_NUMBER = "MF_ADD_PHONE";
    private static final String TAG_MP_VERIFY = "MF_VERIFY";
    private CamIdUserBusiness mCamIdUserBusiness;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp_add_phone);
        mCamIdUserBusiness = ((ApplicationController) getApplicationContext()).getCamIdUserBusiness();
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);

        displayAddPhoneFragment();
    }

    private void displayAddPhoneFragment() {
        Intent intent = getIntent();
        String phoneNumber = intent.getStringExtra("PHONE_NUMBER");
        String codeCheckPhone = intent.getStringExtra("CODE_CHECK_PHONE");
        mMPVerifyPhoneFragment = MPVerifyPhoneFragment.newInstance(phoneNumber, codeCheckPhone);
        executeFragmentTransactionAllowLossAnimation(mMPVerifyPhoneFragment, R.id.mf_add_phone_container, false, TAG_MP_VERIFY);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        ApplicationController.self().getCamIdUserBusiness().setAddOrSelectPhoneCalled(false);
    }

    @Override
    public void onBackPressed() {
//        getFragmentManager().popBackStack();
        finish();
//        List<Fragment> fragments = getSupportFragmentManager().getFragments();
//        int fragSize = fragments.size();
//        Fragment f = fragments.get(fragSize - 1);
//        if (MPVerifyPhoneFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
//            ((MPVerifyPhoneFragment) f).onBack();
//        } else {
//            super.onBackPressed();
//        }
//        ApplicationController.self().getCamIdUserBusiness().setAddOrSelectPhoneCalled(false);
    }
}
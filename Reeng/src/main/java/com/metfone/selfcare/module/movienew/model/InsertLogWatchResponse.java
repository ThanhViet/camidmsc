package com.metfone.selfcare.module.movienew.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsertLogWatchResponse {
    int code;
    String message;
}

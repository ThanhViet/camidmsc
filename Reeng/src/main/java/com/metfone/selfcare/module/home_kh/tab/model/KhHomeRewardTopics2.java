package com.metfone.selfcare.module.home_kh.tab.model;

import java.util.ArrayList;
import java.util.List;

public class KhHomeRewardTopics2 implements IHomeModelType {
    public int id;

    public List<KhHomeRewardDetailItem> categoryItems = new ArrayList<>();


    public KhHomeRewardTopics2() {

    }

    @Override
    public int getItemType() {
        return IHomeModelType.REWARD_TOPIC_2;
    }
}

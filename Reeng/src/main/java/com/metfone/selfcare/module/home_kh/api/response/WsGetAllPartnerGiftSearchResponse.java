package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.PartnerGiftSearch;

import java.util.List;

public class WsGetAllPartnerGiftSearchResponse {
    @SerializedName("errorCode")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("userMsg")
    @Expose
    String userMsg;

    @SerializedName("wsResponse")
    @Expose
    WsResponse wsResponse;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public WsResponse getWsResponse() {
        return wsResponse;
    }

    public void setWsResponse(WsResponse wsRequest) {
        this.wsResponse = wsRequest;
    }

    public class WsResponse {
        @SerializedName("object")
        List<PartnerGiftSearch> object;

        public List<PartnerGiftSearch> getObject() {
            return object;
        }

        public void setObject(List<PartnerGiftSearch> object) {
            this.object = object;
        }
    }


}

package com.metfone.selfcare.module.keeng.fragment.home;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.adapter.home.HotPlaylistAdapter;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.ArrayList;
import java.util.List;

public class PlaylistHotFragment extends RecyclerFragment<PlayListModel> implements BaseListener.OnLoadMoreListener {
    private HotPlaylistAdapter adapter;
    private ListenerUtils listenerUtils;
    private TextView tvTitle;
    private View btnBack;

    public PlaylistHotFragment() {
        super();
    }

    public static PlaylistHotFragment newInstance() {
        return new PlaylistHotFragment();
    }

    @Override
    public String getName() {
        return "PlaylistHotFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_album_hot;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onBackPressed();
            }
        });
        new Handler().postDelayed(() -> {
            clearData();
            adapter = new HotPlaylistAdapter(mActivity, getDatas(), TAG);
            adapter.setHasPosition(false);
            setupRecycler(adapter);
            recyclerView.setPadding(0, mActivity.getResources().getDimensionPixelOffset(R.dimen.padding_8), 0, 0);
            adapter.setRecyclerView(recyclerView, PlaylistHotFragment.this);
            doLoadData(true);
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        tvTitle = view.findViewById(R.id.tv_title);
        btnBack = view.findViewById(R.id.iv_back);
        tvTitle.setText(getString(R.string.playlist_hot));
        return view;
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        App.getInstance().cancelPendingRequests(KeengApi.GET_HOT_PLAYLIST);
        isLoading = false;
        refreshed();
        loadMored();
        loadingFinish();
        if (adapter != null)
            adapter.setLoaded();
    }

    private void doLoadData(boolean type) {
        if (!isLoading) {
            if (type)
                loadingBegin();
            isLoading = true;
            loadData();
        }
    }

    private void loadData() {
        new KeengApi().getHotPlaylist(currentPage, numPerPage, result -> doAddResult(result.getData()), error -> {
            Log.e(TAG, error);
            if (errorCount < MAX_ERROR_RETRY) {
                errorCount++;
                new Handler().postDelayed(this::loadData, Constants.TIME_DELAY_RETRY);
                return;
            }
            doAddResult(null);
        });
    }

    private void doAddResult(ArrayList<PlayListModel> result) {
        Log.d(TAG, "doAddResult ...............");
        errorCount = 0;
        isLoading = false;
        try {
            adapter.setLoaded();
            checkLoadMoreAbsolute(result);
            if (result == null) {
                if (isRefresh) {
                    isRefresh = false;
                }
                loadMored();
                loadingError(v -> doLoadData(true));
                return;
            }
            if (getDatas().isEmpty() && result.isEmpty()) {
                loadMored();
                loadingEmpty();
                refreshed();
            } else {
                refreshed();
                loadMored();
                loadingFinish();
                List<PlayListModel> list = new ArrayList<>();
                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i) != null && result.get(i).getType() == Constants.TYPE_PLAYLIST)
                        list.add(result.get(i));
                }
                setDatas(list);
                adapter.notifyDataSetChanged();
                currentPage++;
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        canLoadMore = true;
        currentPage = 1;
        doLoadData(false);
    }

    @Override
    public void onMediaClick(View v, int position) {
        if (mActivity == null || adapter == null)
            return;
        PlayListModel item = adapter.getItem(position);
        if (item != null) {
            mActivity.gotoPlaylistDetail(item);
        }
    }

    @Override
    public void onMediaExpandClick(View v, int position) {
        if (mActivity == null || adapter == null) {
            return;
        }
        PlayListModel item = adapter.getItem(position);
        mActivity.showPopupMore(item);
    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            loadMore();
            doLoadData(false);
        }
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && getDatas().size() == 0)
            onRefresh();
    }
}

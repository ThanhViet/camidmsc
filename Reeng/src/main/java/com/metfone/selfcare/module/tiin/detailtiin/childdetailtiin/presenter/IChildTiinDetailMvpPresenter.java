package com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.presenter;

import com.metfone.selfcare.module.tiin.base.MvpPresenter;

public interface IChildTiinDetailMvpPresenter extends MvpPresenter {
    void getDetailNew(int id, String tag);
    void getRelateNew(int id);
    void getSiblingNew(int id, int page, int num);
    void getNewStatus(String url);
    void getRelateEventNew(int id, int page, int num);
}

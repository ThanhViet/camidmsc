package com.metfone.selfcare.module.newdetails.ChildDetailNews.view;

import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.NewsContentResponse;
import com.metfone.selfcare.module.response.NewsResponse;


public interface INewsDetailView extends MvpView {
    void loadDataSuccess(boolean flag);
    void loadDataRelateSuccess(boolean flag);
    void bindData(NewsContentResponse response);
    void bindDataRelate(NewsResponse response);
    void loadStatusSucess(boolean success);
    void loadDataFeed(FeedModelOnMedia mFeed);
}

package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCAirTimeInfo implements Serializable {
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("actionType")
    @Expose
    private int actionType;

    @SerializedName("description")
    @Expose
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getActionType() {
        return actionType;
    }

    public void setActionType(int actionType) {
        this.actionType = actionType;
    }

    @Override
    public String toString() {
        return "SCAirTimeInfo{" +
                "name='" + name + '\'' +
                ", actionType=" + actionType +
                '}';
    }
}

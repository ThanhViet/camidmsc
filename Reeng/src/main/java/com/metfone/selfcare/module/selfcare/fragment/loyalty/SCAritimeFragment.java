package com.metfone.selfcare.module.selfcare.fragment.loyalty;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.model.SCBundle;

//import com.metfone.selfcare.module.selfcare.activity.SCAccountActivity;

public class SCAritimeFragment extends BaseFragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView btnBack;
    private ViewPagerDetailAdapter adapter;
    private int pos = 0;

    public static SCAritimeFragment newInstance(Bundle bundle) {
        SCAritimeFragment fragment = new SCAritimeFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCAritimeFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_airtime;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

        loadData();

        return view;
    }

    private void initView(View view) {
        btnBack = view.findViewById(R.id.btnBack);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, new SCBundle(pos));

        adapter = new ViewPagerDetailAdapter(getChildFragmentManager());
        adapter.addFragment(SCMyAirtimeCreditFragment.newInstance(bundle), mActivity.getString(R.string.sc_credit));
        adapter.addFragment(SCMyAirtimeDataFragment.newInstance(), mActivity.getString(R.string.sc_data));

        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        //An floating support di
        if (mActivity != null && mActivity instanceof TabSelfCareActivity)
            ((TabSelfCareActivity) mActivity).hideFloatingButton();
    }

    private void loadData() {
//        Bundle bundleData = getArguments();
//        SCBundle data = (SCBundle) bundleData.getSerializable(Constants.KEY_DATA);
//        pos = data.getType();

        viewPager.setCurrentItem(pos);
    }
}

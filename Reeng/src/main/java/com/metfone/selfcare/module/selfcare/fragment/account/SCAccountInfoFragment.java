/*
package com.metfone.selfcare.module.selfcare.fragment.account;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.model.SCAccount;
import com.metfone.selfcare.module.selfcare.model.SCSecretQuestion;
import com.metfone.selfcare.module.selfcare.network.SCAccountCallback;
import com.metfone.selfcare.module.selfcare.network.SelfCareAccountApi;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

*/
/**
 * Created by thanhnt72 on 2/13/2019.
 *//*


public class SCAccountInfoFragment extends SCBaseFragment {

    public static final String IS_REGISTER = "is_register";
    public static final String USER_NAME = "user_name";
    public static final String PASSWORD = "password";


    @BindView(R.id.btnCreateAcc)
    RoundTextView btnCreateAcc;
    Unbinder unbinder;
    */
/*@BindView(R.id.etFistName)
    AppCompatEditText etFistName;
    @BindView(R.id.etLastname)
    AppCompatEditText etLastname;*//*


    @BindView(R.id.llSecretQuestion)
    LinearLayout llSecretQuestion;
    @BindView(R.id.tilFullName)
    TextInputLayout tilFullName;
    @BindView(R.id.tilPhone)
    TextInputLayout tilPhone;
    @BindView(R.id.tilEmail)
    TextInputLayout tilEmail;
    @BindView(R.id.tilIdentify)
    TextInputLayout tilIdentify;
    @BindView(R.id.spQuestion)
    Spinner spQuestion;
    @BindView(R.id.tilAnswer)
    TextInputLayout tilAnswer;

    EditText etFullName;
    EditText etPhone;
    EditText etIdentity;
    EditText etAnswer;
    EditText etEmail;
    @BindView(R.id.loading_view)
    LoadingViewSC loadingView;


    private String userName, password;
//    private boolean isRegister;

    private RegionSpinnerAdapter mRegionAdapter;

    private ArrayList<SCSecretQuestion> listSecretQuestion = new ArrayList<>();


    public static SCAccountInfoFragment newInstance(String userName, String password) {
        SCAccountInfoFragment fragment = new SCAccountInfoFragment();
        Bundle args = new Bundle();
        args.putString(USER_NAME, userName);
        args.putString(PASSWORD, password);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return SCAccountInfoFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_account_getinfo;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        initEditText();

        setActionBar(view);
        if (getArguments() != null) {
//            isRegister = getArguments().getBoolean(IS_REGISTER);
            userName = getArguments().getString(USER_NAME);
            password = getArguments().getString(PASSWORD);
        }

        mRegionAdapter = new RegionSpinnerAdapter(mActivity, listSecretQuestion);
        spQuestion.setAdapter(mRegionAdapter);
        llSecretQuestion.setVisibility(View.VISIBLE);
        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuestion();
            }
        });
        getQuestion();


        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) loadingView.getLayoutParams();
        params.height = mApp.getHeightPixels() - mApp.getStatusBarHeight() - mActivity.getResources().getDimensionPixelOffset(R.dimen.action_bar_height);

        return view;
    }

    private void initEditText() {
        etFullName = tilFullName.getEditText();
        etPhone = tilPhone.getEditText();
        etEmail = tilEmail.getEditText();
        etIdentity = tilIdentify.getEditText();
        etAnswer = tilAnswer.getEditText();

        etAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilAnswer.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etPhone.setFocusable(false);
        etPhone.setEnabled(false);
        etPhone.setCursorVisible(false);
        etPhone.setKeyListener(null);
        etPhone.setTextColor(ContextCompat.getColor(mActivity, R.color.sc_color_text_normal));
        etPhone.setText(mApp.getReengAccountBusiness().getJidNumber());
    }

    private void getQuestion() {
        loadingView.loadBegin();
//        mActivity.showLoadingSelfCare("", R.string.loading);
        SelfCareAccountApi.getInstant(mApp).getSecretQuestion(new SCAccountCallback.SCAccountApiListener() {
            @Override
            public void onSuccess(final String response) {
//                mActivity.hideLoadingDialog();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optInt("errorCode") == 0) {
                                loadingView.loadFinish();
                                JSONArray jsonArray = jsonObject.optJSONArray("result");
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    listSecretQuestion.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SCSecretQuestion question = new SCSecretQuestion();
                                        question.setQuestion(jsonArray.getJSONObject(i).optString("question"));
                                        question.setQuestionId(jsonArray.getJSONObject(i).optString("id"));
                                        listSecretQuestion.add(question);
                                    }
                                    mRegionAdapter.setListRegions(listSecretQuestion);
                                    mRegionAdapter.notifyDataSetChanged();
                                    spQuestion.setSelection(0, false);
                                } else {
                                    loadingView.loadError();
                                }
                            } else {
                                loadingView.loadError();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            loadingView.loadError();
                        }
                    }
                });


            }

            @Override
            public void onError(int code, String message) {
//                mActivity.hideLoadingDialog();
                Log.e(TAG, "code: " + code + " msg: " + message);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingView.showRetry();
                    }
                });
            }
        });
    }

    private void setActionBar(View view) {
        LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        Toolbar abView = view.findViewById(R.id.tool_bar_fragment);
        abView.removeAllViews();
        abView.addView(mLayoutInflater.inflate(
                R.layout.ab_detail, null), new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        (abView.findViewById(R.id.ab_more_btn)).setVisibility(View.GONE);
        ImageView mImBack = abView.findViewById(R.id.ab_back_btn);
        mImBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        EllipsisTextView mTvwTitle = abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mActivity.getString(R.string.sc_complete_register));

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SelfCareAccountApi.getInstant(mApp).canncelPendingRequest(SelfCareAccountApi.TAG_GET_SECRET_QUESTION);
        unbinder.unbind();
    }

    @OnClick(R.id.btnCreateAcc)
    public void onViewClicked() {
        */
/*SelfCareAccountApi.getInstant(mApp).registerAccountMyID(*//*
*/
/*etAccountName.getText().toString(),
                etAccountPass.getText().toString(),
                etEmail.getText().toString(),
                etFistName.getText().toString(),
                etLastname.getText().toString(),
                etIdentity.getText().toString(),
                mApp.getReengAccountBusiness().getJidNumber()*//*
*/
/*);*//*



        if (checkValidData()) {
            mActivity.showLoadingSelfCare("", R.string.loading);
            SCAccount scAccount = new SCAccount();
            scAccount.setUserName(userName);
            scAccount.setPassWord(password);
            scAccount.setFullName(etFullName.getText().toString().trim());
            scAccount.setEmail(etEmail.getText().toString().trim());
            scAccount.setNrcOrPassport(etIdentity.getText().toString().trim());
            SCSecretQuestion secretQuestion = new SCSecretQuestion();
            secretQuestion.setAnswer(etAnswer.getText().toString().trim());
            secretQuestion.setQuestionId(listSecretQuestion.get(spQuestion.getSelectedItemPosition()).getQuestionId());
            scAccount.setSecretQuestion(secretQuestion);
            String jid = mApp.getReengAccountBusiness().getJidNumber();
            jid = jid.substring(1);
            scAccount.setPhoneNumber(jid);
            SelfCareAccountApi.getInstant(mApp).registerAccountMyID(scAccount, new SCAccountCallback.SCAccountApiListener() {
                @Override
                public void onSuccess(String response) {
                    mActivity.hideLoadingDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int code = jsonObject.optInt("errorCode", -1);
                        if (code == 0) {
                            JSONObject jsResult = jsonObject.optJSONObject("result");
                            if (jsResult != null) {
                                String accessToken = jsResult.optString("accessToken");
                                if (!TextUtils.isEmpty(accessToken)) {
                                    mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_KEY_ACCESS_TOKEN, accessToken).apply();
                                    SelfCareAccountApi.getInstant(mApp).setAccessToken(accessToken);
//                                    mActivity.goToHome();
                                    DeepLinkHelper.getInstance().openSchemaLink(mActivity, "mytel://home/selfcare?clearStack=1");
                                    mActivity.clearBackStack();
                                    mActivity.finish();
                                } else
                                    mActivity.showToast(R.string.e601_error_but_undefined);
                            } else
                                mActivity.showToast(R.string.e601_error_but_undefined);

                        } else if (code == 1) {
                            mActivity.showToast(R.string.sc_nrc_pp_exist);
                        } else if (code == 2) {
                            mActivity.showToast(R.string.sc_email_exist);
                        } else if (code == 3) {
                            mActivity.showToast(R.string.sc_phone_exist);
                        } else if (code == 4) {
                            mActivity.showToast(R.string.sc_user_exist);
                        } else if (code == 5) {
                            mActivity.showToast(R.string.sc_wrong_info);
                        } else {
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "JSONException", e);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }

                @Override
                public void onError(int code, String message) {
                    mActivity.hideLoadingDialog();
                    Log.e(TAG, "onError: " + code + " msg: " + message);
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            });
        }
    }

    private boolean checkValidData() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etAnswer.getText().toString().trim())) {
            isValid = false;
            tilAnswer.setError(mActivity.getString(R.string.sc_input_text));
        }

        return isValid;
    }
}
*/

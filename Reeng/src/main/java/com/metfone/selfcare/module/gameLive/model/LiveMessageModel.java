package com.metfone.selfcare.module.gameLive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LiveMessageModel implements Serializable {

    @SerializedName("type")
    @Expose
    private int type;

    @SerializedName("totalNumber")
    @Expose
    private int totalNumber;

    @SerializedName("question")
    @Expose
    private LiveQuestionModel question;

    @SerializedName("timeServer")
    @Expose
    private long timeServer;

    @SerializedName("prize")
    @Expose
    private int prize;

    @SerializedName("result")
    @Expose
    private List<WinnerModel> msisdnLucky = new ArrayList<>();

    @SerializedName("state")
    @Expose
    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<WinnerModel> getMsisdnLucky() {
        return msisdnLucky;
    }

    public int getPrize() {
        return prize;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }

    public LiveQuestionModel getQuestion() {
        return question;
    }

    public void setQuestion(LiveQuestionModel question) {
        this.question = question;
    }

    public long getTimeServer() {
        return timeServer;
    }

    public void setTimeServer(long timeServer) {
        this.timeServer = timeServer;
    }

    @Override
    public String toString() {
        return "LiveMessageModel{" +
                "type=" + type +
                ", totalNumber=" + totalNumber +
                ", question=" + question +
                ", timeServer=" + timeServer +
                ", prize=" + prize +
                ", msisdnLucky=" + msisdnLucky +
                ", state=" + state +
                '}';
    }
}

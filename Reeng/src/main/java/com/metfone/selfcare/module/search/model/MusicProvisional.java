/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class MusicProvisional extends Provisional {
    //private ArrayList<AllModel> dataSuggest;
    //private ArrayList<SearchModel> data;
    private ArrayList<SearchModel> dataSong;
    private ArrayList<SearchModel> dataMV;

    public MusicProvisional() {
    }

//    public MusicProvisional(ArrayList<SearchModel> data) {
//        this.data = data;
//    }
//
//    public ArrayList<SearchModel> getData() {
//        return data;
//    }
//
//    public void setData(ArrayList<SearchModel> data) {
//        this.data = data;
//    }

//    public ArrayList<AllModel> getDataSuggest() {
//        return dataSuggest;
//    }
//
//    public void setDataSuggest(ArrayList<AllModel> dataSuggest) {
//        this.dataSuggest = dataSuggest;
//    }

    public ArrayList<SearchModel> getDataSong() {
        return dataSong;
    }

    public void setDataSong(ArrayList<SearchModel> dataSong) {
        this.dataSong = dataSong;
    }

    public ArrayList<SearchModel> getDataMV() {
        return dataMV;
    }

    public void setDataMV(ArrayList<SearchModel> dataMV) {
        this.dataMV = dataMV;
    }

    @Override
    public int getSize() {
        //return Utilities.notEmpty(data)  ? 1 : 0;
        return (Utilities.notEmpty(dataMV) || Utilities.notEmpty(dataSong)) ? 1 : 0;
    }
}

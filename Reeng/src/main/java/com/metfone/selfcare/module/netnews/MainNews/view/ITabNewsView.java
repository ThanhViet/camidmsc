package com.metfone.selfcare.module.netnews.MainNews.view;


import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.view.MvpView;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/18/17.
 */

public interface ITabNewsView  extends MvpView {
    void loadDataSuccess(boolean flag);

    void bindData(ArrayList<CategoryModel> categoryResponse);
    void bindSettingData(String str);
}

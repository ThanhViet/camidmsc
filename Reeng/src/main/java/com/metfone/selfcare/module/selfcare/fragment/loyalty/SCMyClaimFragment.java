package com.metfone.selfcare.module.selfcare.fragment.loyalty;

import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;

public class SCMyClaimFragment extends SCBaseFragment {
    public static SCMyClaimFragment newInstance(Bundle args) {
        SCMyClaimFragment fragment = new SCMyClaimFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public String getName() {
        return "SCMyClaimFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_my_claim;
    }
}

/*
package com.metfone.selfcare.module.selfcare.fragment.account;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.module.selfcare.activity.SCAccountActivity;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.model.SCSecretQuestion;
import com.metfone.selfcare.module.selfcare.network.SCAccountCallback;
import com.metfone.selfcare.module.selfcare.network.SelfCareAccountApi;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

*/
/**
 * Created by thanhnt72 on 2/13/2019.
 *//*


public class SCAccountForgetPassFragment extends SCBaseFragment {

    private static String IS_CHANGE_PASS = "change_pass";

    @BindView(R.id.spQuestion)
    Spinner spQuestion;
    EditText etAnswer;
    Unbinder unbinder;
    @BindView(R.id.tilPhone)
    TextInputLayout tilPhone;
    @BindView(R.id.tilAnswer)
    TextInputLayout tilAnswer;
    @BindView(R.id.btnNext)
    RoundTextView btnNext;
    EditText etPhone;
    @BindView(R.id.loading_view)
    LoadingViewSC loadingView;

    private boolean isChangePass;


    private RegionSpinnerAdapter mRegionAdapter;
    private ArrayList<SCSecretQuestion> listSecretQuestion = new ArrayList<>();

    public static SCAccountForgetPassFragment newInstance(boolean isChangePass) {
        SCAccountForgetPassFragment fragment = new SCAccountForgetPassFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_CHANGE_PASS, isChangePass);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return SCAccountForgetPassFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_account_forget_pass;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        if (getArguments() != null) {
//            isRegister = getArguments().getBoolean(IS_REGISTER);
            isChangePass = getArguments().getBoolean(IS_CHANGE_PASS);
        }

        initEditText();

        setActionBar(view);
        mRegionAdapter = new RegionSpinnerAdapter(mActivity, listSecretQuestion);
        spQuestion.setAdapter(mRegionAdapter);
        getQuestion();
        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuestion();
            }
        });
        return view;
    }

    private void initEditText() {
        etPhone = tilPhone.getEditText();
        etAnswer = tilAnswer.getEditText();

        etPhone.setFocusable(false);
        etPhone.setEnabled(false);
        etPhone.setCursorVisible(false);
        etPhone.setKeyListener(null);
        etPhone.setTextColor(ContextCompat.getColor(mActivity, R.color.sc_color_text_normal));
        etPhone.setText(mApp.getReengAccountBusiness().getJidNumber());

        */
/*etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilPhone.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*//*


        etAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilAnswer.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void getQuestion() {
        loadingView.loadBegin();
//        mActivity.showLoadingSelfCare("", R.string.loading);
        SelfCareAccountApi.getInstant(mApp).getSecretQuestion(new SCAccountCallback.SCAccountApiListener() {
            @Override
            public void onSuccess(final String response) {
//                mActivity.hideLoadingDialog();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optInt("errorCode") == 0) {

                                loadingView.loadFinish();
                                JSONArray jsonArray = jsonObject.optJSONArray("result");
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    listSecretQuestion.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SCSecretQuestion question = new SCSecretQuestion();
                                        question.setQuestion(jsonArray.getJSONObject(i).optString("question"));
                                        question.setQuestionId(jsonArray.getJSONObject(i).optString("id"));
                                        listSecretQuestion.add(question);
                                    }
                                    mRegionAdapter.setListRegions(listSecretQuestion);
                                    mRegionAdapter.notifyDataSetChanged();
                                    spQuestion.setSelection(0, false);

                                    etAnswer.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            etAnswer.requestFocus();
                                            InputMethodUtils.showSoftKeyboard(mActivity, etAnswer);
                                        }
                                    }, 50);

                                }
                            } else {
                                loadingView.loadError();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            loadingView.loadError();
                        }

                    }
                });

            }

            @Override
            public void onError(int code, String message) {
                mActivity.hideLoadingDialog();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingView.showRetry();
                    }
                });
            }
        });
    }

    private void setActionBar(View view) {
        LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        Toolbar abView = view.findViewById(R.id.tool_bar_fragment);
        abView.removeAllViews();
        abView.addView(mLayoutInflater.inflate(
                R.layout.ab_detail, null), new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        (abView.findViewById(R.id.ab_more_btn)).setVisibility(View.GONE);
        ImageView mImBack = abView.findViewById(R.id.ab_back_btn);
        mImBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        EllipsisTextView mTvwTitle = abView.findViewById(R.id.ab_title);
        if (isChangePass)
            mTvwTitle.setText(mActivity.getString(R.string.sc_change_password));
        else
            mTvwTitle.setText(mActivity.getString(R.string.sc_forget_pass));

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SelfCareAccountApi.getInstant(mApp).canncelPendingRequest(SelfCareAccountApi.TAG_GET_SECRET_QUESTION);
        unbinder.unbind();
    }

    @OnClick({R.id.btnNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                if (isValidData()) {
                    doNextForgetPass();
                }
                break;
        }
    }

    private void doNextForgetPass() {
        mActivity.showLoadingSelfCare("", R.string.loading);
        SCSecretQuestion secretQuestion = new SCSecretQuestion();
        secretQuestion.setAnswer(etAnswer.getText().toString().trim());
        secretQuestion.setQuestionId(listSecretQuestion.get(spQuestion.getSelectedItemPosition()).getQuestionId());
        SelfCareAccountApi.getInstant(mApp).requestResetPassword(secretQuestion, new SCAccountCallback.SCAccountApiListener() {
            @Override
            public void onSuccess(String response) {
                mActivity.hideLoadingDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("errorCode") == 0) {
                        ((SCAccountActivity) mActivity).resetPass();
                    } else
                    {
                        String message = jsonObject.optString("message");
                        mActivity.showToast(message);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSONException", e);
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }

            @Override
            public void onError(int code, String message) {
                mActivity.hideLoadingDialog();
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }

    private boolean isValidData() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etPhone.getText().toString().trim())) {
            isValid = false;
            tilPhone.setError(mActivity.getString(R.string.sc_input_text));
        }
        if (TextUtils.isEmpty(etAnswer.getText().toString().trim())) {
            isValid = false;
            tilAnswer.setError(mActivity.getString(R.string.sc_input_text));
        }
        return isValid;
    }
}
*/

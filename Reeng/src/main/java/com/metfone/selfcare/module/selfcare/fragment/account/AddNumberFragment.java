/*
package com.metfone.selfcare.module.selfcare.fragment.account;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.module.selfcare.activity.AfterLoginMyIDActivity;
import com.metfone.selfcare.module.selfcare.activity.SCAccountActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

*/
/**
 * Created by thanhnt72 on 5/13/2019.
 *//*


public class AddNumberFragment extends Fragment {

    private static final String TAG = AddNumberFragment.class.getSimpleName();

    public static final String NUMBER = "number";
    @BindView(R.id.tilAddNumber)
    TextInputLayout tilAddNumber;
    Unbinder unbinder;

    private EditText etAddNumber;

    private BaseSlidingFragmentActivity activity;
    private Resources mRes;
    private ApplicationController mApp;

    public static AddNumberFragment newInstance() {
        AddNumberFragment fragment = new AddNumberFragment();
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        mRes = activity.getResources();
        mApp = (ApplicationController) activity.getApplication();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_number, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        etAddNumber = tilAddNumber.getEditText();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        InputMethodUtils.hideSoftKeyboard(activity);
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnSkip, R.id.btnNext, R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSkip:
                EventBus.getDefault().postSticky(new SCAccountActivity.SCAccountEvent(SCAccountActivity.SCAccountEvent.UPDATE_INFO));
                activity.finish();
                break;
            case R.id.btnNext:
                String textInput = etAddNumber.getText().toString().trim();
                if (!TextUtils.isEmpty(textInput)) {
                    if (PhoneNumberHelper.isValidNumberMytel(mApp, textInput)) {
                        if (activity instanceof AfterLoginMyIDActivity)
                            ((AfterLoginMyIDActivity) activity).onClickAddNumber(textInput);
                    } else {
                        activity.showToast(R.string.phone_number_invalid);
                    }
                } else
                    activity.showToast(R.string.phone_number_invalid);
                break;
            case R.id.iv_back:
                InputMethodUtils.hideSoftKeyboard(activity);
                activity.finish();
                break;
        }
    }

}
*/

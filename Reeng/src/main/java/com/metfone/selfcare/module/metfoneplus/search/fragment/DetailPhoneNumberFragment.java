package com.metfone.selfcare.module.metfoneplus.search.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPOrderNumberDialog;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsLockIsdnToBuyResponse;
import com.metfone.selfcare.util.Utilities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class DetailPhoneNumberFragment extends MPBaseFragment {
    private static final String NUMBER_KEY = "number";
    private static final String DURATION_KEY = "duration";
    private static final String FEE_KEY = "fee";
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.phone_number)
    TextView tv_phone_number;
    @BindView(R.id.amount)
    TextView tv_amount;
    @BindView(R.id.commitment)
    TextView tv_commitment;
    @BindView(R.id.duration)
    TextView tv_duration;
    BaseMPSuccessDialog baseMPSuccessDialog;

    private AvailableNumber availableNumber;
    private int commitDuration;
    private int fee;
    public static DetailPhoneNumberFragment newInstance(AvailableNumber number, int commitDuration, int fee) {
        Bundle args = new Bundle();
        args.putSerializable(NUMBER_KEY, number);
        args.putInt(DURATION_KEY, commitDuration);
        args.putInt(FEE_KEY, fee);
        DetailPhoneNumberFragment fragment = new DetailPhoneNumberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_detail_phone_number;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            availableNumber = (AvailableNumber) getArguments().getSerializable(NUMBER_KEY);
            commitDuration = getArguments().getInt(DURATION_KEY);
            fee = getArguments().getInt(FEE_KEY);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(view);
        action_bar_title.setText(R.string.buy_phone_number);
        if (availableNumber != null) {
            tv_phone_number.setText(0 + availableNumber.getIsdn().substring(0,2)+" "+ availableNumber.getIsdn().substring(2));
            tv_amount.setText("$" + availableNumber.getPrice());
            tv_commitment.setText("$" + availableNumber.getMonthlyFee());
            tv_duration.setText(commitDuration + " " + getString(R.string.months));
        }
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        popBackStackFragment();
    }

    @OnClick({R.id.btn_order, R.id.btn_apply})
    public void handleClick(View view) {
        if (view.getId() == R.id.btn_order) {
            String top_content;
            String bottom_content;
            String title;
            if (availableNumber.getMonthlyFee() > 0) {
                top_content = getString(R.string.top_content_order_dialog, (0 + availableNumber.getIsdn().substring(0,2)+" "+ availableNumber.getIsdn().substring(2)), (int) Float.parseFloat(availableNumber.getPrice()), String.valueOf(availableNumber.getMonthlyFee()), String.valueOf(commitDuration));
                bottom_content = getString(R.string.m_p_order_free, String.valueOf(fee));
                title = getString(R.string.title_order_dialog);
            } else {
                top_content = getString(R.string.top_content_order_sim_dialog, (0 + availableNumber.getIsdn().substring(0,2)+" "+ availableNumber.getIsdn().substring(2)), (int) Float.parseFloat(availableNumber.getPrice()));
                bottom_content = getString(R.string.m_p_order_free, String.valueOf(fee));
                title = getString(R.string.title_order_dialog);
            }

            MPOrderNumberDialog dialog = new MPOrderNumberDialog.Builder(getContext(), new MPOrderNumberDialog.OrderDialogListener() {
                @Override
                public void confirm() {
                    new MetfonePlusClient().wsLockIsdnToBuy(availableNumber.getIsdn(), new MPApiCallback<WsLockIsdnToBuyResponse>() {
                        @Override
                        public void onResponse(Response<WsLockIsdnToBuyResponse> response) {
                            if (response.body() != null && response.body().getResult().getErrorCode().equals("1")) {
                                mParentActivity.hideLoadingDialog();
                                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getResult().getUserMsg(), false);
                                baseMPSuccessDialog.show();
                                ApplicationController.self().getFirebaseEventBusiness().logOrderNewSim(false,response.body().getResult().getErrorCode());
                            } else {
                                ApplicationController.self().getFirebaseEventBusiness().logOrderNewSim(true,"0");
                                gotoMPOrderNewSimFragment(response.body().getResult().getUserMsg());
                                mParentActivity.hideLoadingDialog();
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            mParentActivity.hideLoadingDialog();
                        }
                    });
                }

                @Override
                public void cancel() {

                }
            }, top_content, bottom_content, title, "", "", "").build();
            dialog.show(getChildFragmentManager(), "");
        } else if (view.getId() == R.id.btn_apply) {
            if (availableNumber.getMonthlyFee() > 0) {
                gotoOrderNumberFragment(availableNumber, commitDuration);
            } else {
                gotoMPApplyForYouCurrentSimFragment(availableNumber, 0, commitDuration);
            }
        }
    }
}
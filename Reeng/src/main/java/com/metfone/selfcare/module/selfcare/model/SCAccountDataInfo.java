package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SCAccountDataInfo implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("values")
    @Expose
    private ArrayList<SCValue> values = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<SCValue> getValues() {
        if(values == null)
            return new ArrayList<>();
        return values;
    }

    public void setValues(ArrayList<SCValue> values) {
        this.values = values;
    }

    public class SCValue {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("exp")
        @Expose
        private String exp;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getExp() {
            return exp;
        }

        public void setExp(String exp) {
            this.exp = exp;
        }

        @Override
        public String toString() {
            return "SCValue{" +
                    "title='" + title + '\'' +
                    ", value='" + value + '\'' +
                    ", exp=" + exp +
                    '}';
        }
    }
}

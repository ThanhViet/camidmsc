package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCChargeInfo implements Serializable {
//     "isdn": "Data",
//             "direction": 0,
//             "start_time": 1548151391000,
//             "duration": 0,
//             "value": 0,
//             "total": 0

    @SerializedName("isdn")
    private String isdn;

    @SerializedName("direction")
    private String direction;

    @SerializedName("start_time")
    private String start_time;

    @SerializedName("duration")
    private String duration;

    @SerializedName("value")
    private String value;

    @SerializedName("total")
    private String total;

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "SCChargeInfo{" +
                "isdn='" + isdn + '\'' +
                ", direction='" + direction + '\'' +
                ", start_time='" + start_time + '\'' +
                ", duration='" + duration + '\'' +
                ", value='" + value + '\'' +
                ", total='" + total + '\'' +
                '}';
    }
}

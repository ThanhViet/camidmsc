/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movienew.adapter;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.movienew.holder.CountryHolder;
import com.metfone.selfcare.module.movienew.listener.OnClickCountry;

import java.util.ArrayList;

public class CountryAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {

    private OnClickCountry listener;

    public CountryAdapter(Activity activity, ArrayList<Object> list) {
        super(activity, list);
    }

    public void setListener(OnClickCountry listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CountryHolder(layoutInflater.inflate(R.layout.recycler_genres_country_item, parent, false), activity, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        holder.bindData(item, position);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }
}

package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.module.metfoneplus.holder.StoreViewHolder;

import java.util.List;

public class StoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private final View.OnClickListener mOnStoreClickListener;
    private List<Store> mStoreList;

    public StoreAdapter(Context context, List<Store> storeList, View.OnClickListener onStoreClickListener) {
        this.mContext = context;
        this.mStoreList = storeList;
        this.mOnStoreClickListener = onStoreClickListener;
    }

    private void setList(List<Store> storeList) {
        this.mStoreList = storeList;
    }

    public void replaceData(List<Store> storeList) {
        setList(storeList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mp_store_location, parent, false);
        return new StoreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Store store = mStoreList.get(position);

        StoreViewHolder storeViewHolder = (StoreViewHolder) holder;

        storeViewHolder.mStoreName.setText(store.getName());
        storeViewHolder.mStoreAddress.setText(store.getAddress());
        storeViewHolder.mStoreDistance.setText(String.format(mContext.getString(R.string.m_p_store_distance), store.getDistance()));
        storeViewHolder.mStore = store;
        storeViewHolder.itemView.setTag(storeViewHolder);
        storeViewHolder.itemView.setOnClickListener(mOnStoreClickListener);
    }

    @Override
    public int getItemCount() {
        if (mStoreList == null) {
            return 0;
        } else {
            if (mStoreList.size() < 5) {
                return mStoreList.size();
            } else {
                return 5;
            }
        }

    }
}
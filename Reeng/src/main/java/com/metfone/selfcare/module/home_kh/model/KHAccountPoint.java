package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;

import java.io.Serializable;

public class KHAccountPoint implements Serializable{
    @SerializedName("accountPointId")
    long accountPointId;
    @SerializedName("pointId")
    long pointId;
    @SerializedName("vtAccId")
    long vtAccId;
    @SerializedName("pointValue")
    double pointValue;
    @SerializedName("pointExpireDate")
    String pointExpireDate;
    @SerializedName("productId")
    String productId;
    @SerializedName("createDate")
    String createDate;
    @SerializedName("pointName")
    String pointName;
    @SerializedName("pointType")
    int pointType;
    @SerializedName("status")
    int status;
    @SerializedName("description")
    String description;

    public long getAccountPointId() {
        return accountPointId;
    }

    public void setAccountPointId(long accountPointId) {
        this.accountPointId = accountPointId;
    }

    public long getPointId() {
        return pointId;
    }

    public void setPointId(long pointId) {
        this.pointId = pointId;
    }

    public long getVtAccId() {
        return vtAccId;
    }

    public void setVtAccId(long vtAccId) {
        this.vtAccId = vtAccId;
    }

    public int getPointValue() {
        return (int) pointValue;
    }

    public void setPointValue(int pointValue) {
        this.pointValue = pointValue;
    }

    public String getPointExpireDate() {
        return pointExpireDate;
    }

    public void setPointExpireDate(String pointExpireDate) {
        this.pointExpireDate = pointExpireDate;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public int getPointType() {
        return pointType;
    }

    public void setPointType(int pointType) {
        this.pointType = pointType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getExpireDate() {
        return DateConvert.convertStringToLong(pointExpireDate, DateConvert.DATE_REWARD_INPUT);
    }
}

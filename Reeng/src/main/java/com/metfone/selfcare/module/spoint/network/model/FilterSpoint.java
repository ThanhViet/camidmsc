package com.metfone.selfcare.module.spoint.network.model;

public class FilterSpoint {
    private int typePoint = 2;//0: spoint, 1: spoint km, 2: all
    private int dealPoint = 0;//1: cộng point, 2: trừ point, 0: all

    public FilterSpoint(int typePoint, int dealPoint) {
        this.typePoint = typePoint;
        this.dealPoint = dealPoint;
    }

    public int getTypePoint() {
        return typePoint;
    }

    public int getDealPoint() {
        return dealPoint;
    }
}

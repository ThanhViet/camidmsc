package com.metfone.selfcare.module.movie.model;

import java.util.List;

public class PageMovieLoadmore {

    private List<Object> mListTrending;
    private List<Object> mListRecentlyAdded;
    private List<Object> mListCustomTopic;

    public List<Object> getListTrending() {
        return mListTrending;
    }

    public List<Object> getListRecentlyAdded() {
        return mListRecentlyAdded;
    }

    public List<Object> getListCustomTopic() {
        return mListCustomTopic;
    }

    public PageMovieLoadmore() {
    }

    public PageMovieLoadmore(List<Object> listTrending, List<Object> listRecentlyAdded, List<Object> listCustomTopic) {
        this.mListTrending = listTrending;
        this.mListRecentlyAdded = listRecentlyAdded;
        this.mListCustomTopic = listCustomTopic;
    }
}

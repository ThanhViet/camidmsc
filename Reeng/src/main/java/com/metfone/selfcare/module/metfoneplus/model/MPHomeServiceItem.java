package com.metfone.selfcare.module.metfoneplus.model;

import com.metfone.selfcare.module.home_kh.tab.model.GiftSearchObject;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;

public class MPHomeServiceItem implements IHomeModelType {

    public int id;
    public String icon;
    public String name;

    private int type;

    public void setType(int type) {
        this.type = type;
    }

    public MPHomeServiceItem() {

    }
    public MPHomeServiceItem(int id, String icon, String name) {
        this.id = id;
        this.icon = icon;
        this.name = name;
    }

    @Override
    public int getItemType() {
        return type;
    }
}

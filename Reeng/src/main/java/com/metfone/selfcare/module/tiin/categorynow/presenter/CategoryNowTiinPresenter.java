package com.metfone.selfcare.module.tiin.categorynow.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.module.tiin.base.BasePresenter;
import com.metfone.selfcare.module.tiin.category.fragment.CategoryTiinFragment;
import com.metfone.selfcare.module.tiin.categorynow.fragment.CategoryNowTiinFragment;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;
import com.metfone.selfcare.module.tiin.network.request.TiinRequest;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.List;

public class CategoryNowTiinPresenter extends BasePresenter implements ICategoryNowTiinMvpPresenter {
    private TiinApi mTiinApi;
    private HttpCallBack eventCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            if (!isViewAttached() || !(getMvpView() instanceof CategoryNowTiinFragment)) {
                return;
            }
            Gson gson = new Gson();
            List<TiinEventModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<List<TiinEventModel>>() {
            }.getType());
//            TiinContentResponse childNewsResponse = gson.fromJson(data, TiinContentResponse.class);
            ((CategoryNowTiinFragment) getMvpView()).bindData(response);
            ((CategoryNowTiinFragment) getMvpView()).loadDataSuccess(true);

        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof CategoryTiinFragment)) {
                return;
            }
            ((CategoryNowTiinFragment) getMvpView()).loadDataSuccess(false);

        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    public CategoryNowTiinPresenter() {
        mTiinApi = new TiinApi(ApplicationController.self());
    }

    @Override
    public void getCategoryEvent(int page, int num) {
        mTiinApi.getCategoryEvent(eventCallback, new TiinRequest(page, num));
    }

}

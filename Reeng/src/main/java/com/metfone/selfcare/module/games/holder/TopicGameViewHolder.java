package com.metfone.selfcare.module.games.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.model.tabGame.GameCategoryModel;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;

import java.util.List;

public class TopicGameViewHolder extends BaseViewHolder {

    private String colors[] = {"#D80F2C","#E39207","#44CA83","#47A3E2","#AA87CC"};
    @Nullable
    GameCategoryModel gameCategoryModel;

    public TopicGameViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
    }

    private TextView tvTopic;
    private CardView viewRoot;

    @Override
    public void initViewHolder(View v) {
        if (mData == null || mData.size() == 0) return;
        viewRoot = v.findViewById(R.id.viewRoot);
        tvTopic = v.findViewById(R.id.tvTopic);
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        int color = pos % colors.length;
        String currentLang = LocaleManager.getLanguage(context);
        if (mData.get(pos) instanceof GameCategoryModel) {
            gameCategoryModel = (GameCategoryModel) mData.get(pos);
            tvTopic.setText(gameCategoryModel.getName());
            if (currentLang.equals("km"))
                tvTopic.setText(gameCategoryModel.getNameKM());
        }
        viewRoot.setCardBackgroundColor(Color.parseColor(colors[color]));
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null) {
            itemViewClickListener.onItemViewClickListener(pos, mData);
        }
    }
}

package com.metfone.selfcare.module.selfcare.network.request;

import org.json.JSONException;
import org.json.JSONObject;

public class IPCCAuthRequest {
//    {
//        "customerServiceIdentifier": "string",
//            "loyaltyBalanceCode": "string",
//            "loyaltyProgramCode": "string",
//            "quantity": 0,
//            "rewardCode": "string",
//            "serviceCode": "string"
//    }
    private String customerServiceIdentifier;
    private String loyaltyBalanceCode;
    private String loyaltyProgramCode;
    private String quantity;
    private String rewardCode;
    private String serviceCode;

    public IPCCAuthRequest(String customerServiceIdentifier, String loyaltyBalanceCode, String loyaltyProgramCode, String quantity, String rewardCode, String serviceCode) {
        this.customerServiceIdentifier = customerServiceIdentifier;
        this.loyaltyBalanceCode = loyaltyBalanceCode;
        this.loyaltyProgramCode = loyaltyProgramCode;
        this.quantity = quantity;
        this.rewardCode = rewardCode;
        this.serviceCode = serviceCode;
    }

    public String getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customerServiceIdentifier", customerServiceIdentifier);
            jsonObject.put("loyaltyBalanceCode", loyaltyBalanceCode);
            jsonObject.put("loyaltyProgramCode", loyaltyProgramCode);
            jsonObject.put("quantity", quantity);
            jsonObject.put("rewardCode", rewardCode);
            jsonObject.put("serviceCode", serviceCode);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

package com.metfone.selfcare.module.home_kh.fragment.reward.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class RewardPageAdapter extends FragmentStatePagerAdapter {
    List<Fragment> listFragment = new ArrayList<>();

    public List<Fragment> getData() {
        return listFragment;
    }

    public RewardPageAdapter(@NonNull FragmentManager fm, int behavior, List<Fragment> list) {
        super(fm, behavior);
        this.listFragment = list;
    }

    @Override
    public int getCount() {
        return listFragment.size();
    }

    @Override
    public Fragment getItem(int position) {
        return listFragment.get(position);
//        if (position == 0) {
//            return GiftActiveKHFragment.newInstance();
//        } else {
//            return GiftPastKHFragment.newInstance();
//        }
    }
}

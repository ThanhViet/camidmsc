package com.metfone.selfcare.module.spoint.holder;

import android.app.Activity;
import android.view.View;
import android.widget.Button;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.spoint.SpointListener;
import com.metfone.selfcare.ui.glide.ImageLoader;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

public class ItemSpointHolder extends BaseViewHolder {
    private AppCompatImageView ivCover;
    private AppCompatTextView tvTitle, tvDesc;
    private Button btnAction;
    private Activity activity;
    private SpointListener.OnSpointListener listener;

    public ItemSpointHolder(View view, Activity activity, SpointListener.OnSpointListener listener) {
        super(view);
        this.activity = activity;
        this.listener = listener;
        ivCover = view.findViewById(R.id.iv_cover);
        tvTitle = view.findViewById(R.id.tv_title);
        tvDesc = view.findViewById(R.id.tv_desc);
        btnAction = view.findViewById(R.id.btn_action);
    }

    public void bindData(AccumulatePointItem model) {
        if (model != null) {
            tvTitle.setText(model.getTitle());
            tvDesc.setText(model.getDesc());
            btnAction.setText(model.getLabelButton());
            ImageLoader.setImage(activity, model.getIcon(), ivCover);
            btnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClickActionSpoint(model);
                    }
                }
            });
        }
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.home_kh.tab;

import com.metfone.selfcare.listeners.OnClickContentChannel;
import com.metfone.selfcare.listeners.OnClickContentComic;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnClickContentMusic;
import com.metfone.selfcare.listeners.OnClickContentNews;
import com.metfone.selfcare.listeners.OnClickContentTiin;
import com.metfone.selfcare.listeners.OnClickContentVideo;
import com.metfone.selfcare.listeners.OnClickGameBanner;
import com.metfone.selfcare.listeners.OnClickLuckyGameBanner;
import com.metfone.selfcare.listeners.OnClickSliderBanner;
import com.metfone.selfcare.module.home_kh.tab.adapter.events.OnClickHomeReward;
import com.metfone.selfcare.module.home_kh.tab.adapter.gift.KhHomeGiftAdapter;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;

public class TabHomeKhListener {

    public interface OnAdapterClick extends OnClickContentVideo, OnClickContentMovie, OnClickContentMusic
            , OnClickContentNews, OnClickContentComic, OnClickContentChannel, OnClickSliderBanner
            , OnClickContentTiin, OnClickHomeReward, OnClickGameBanner, KhHomeGiftAdapter.EventListener, OnClickLuckyGameBanner {
        void onClickTitleBox(IHomeModelType item, int position);

        void onClickSignUp();

        void onScrollData(IHomeModelType homeModelType);
    }
}

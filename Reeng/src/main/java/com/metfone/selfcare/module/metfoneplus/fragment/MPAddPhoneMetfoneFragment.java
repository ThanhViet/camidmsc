package com.metfone.selfcare.module.metfoneplus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.module.metfoneplus.listener.OnClickShowFTTH;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.network.camid.response.CheckCarrierResponse;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPAddPhoneMetfoneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPAddPhoneMetfoneFragment extends Fragment {
    private static final String TAG = "MfAddPhoneFragment";

    /**
     * code != 00, 42 -> add metfone number
     */
    public static final String CODE_SUCCESS = "1";

    /**
     * Code = 00 or 42 -> combine number
     */
    public static final String CODE_COMBINE = "00";

    @BindView(R.id.btnSwitchMobile)
    RadioButton btnSwitchMobile;
    @BindView(R.id.btnSwitchInternet)
    RadioButton btnSwitchInternet;
    @BindView(R.id.layout_root_add_metfone)
    LinearLayout mLayoutRootAddMetfone;
    @BindView(R.id.edt_input_phone)
    AppCompatEditText mEdtInputPhone;
    @BindView(R.id.btn_next)
    AppCompatButton mBtnNext;
    @BindView(R.id.layout_container_add_number)
    RelativeLayout mLayoutContainerAddNumber;

    public Unbinder mUnbinder;

    private OnFragmentInteractionListener onFragmentInteractionListener;

    private OnClickShowFTTH onClickShowFTTH;

    public MPAddPhoneMetfoneFragment() {
        // Required empty public constructor
    }

    public static MPAddPhoneMetfoneFragment newInstance() {
        MPAddPhoneMetfoneFragment fragment = new MPAddPhoneMetfoneFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(OnFragmentInteractionListener listener, OnClickShowFTTH showFTTHListener) {
        onFragmentInteractionListener = listener;
        onClickShowFTTH = showFTTHListener;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mp_add_number_metfone, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        Utilities.adaptViewForInserts(mLayoutRootAddMetfone);
        Utilities.adaptViewForInsertBottom(mLayoutContainerAddNumber);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InputMethodUtils.showSoftKeyboard(getContext(),mEdtInputPhone);
    }

    @OnClick(R.id.btnSwitchInternet)
    void showFTTH() {
        if (onClickShowFTTH != null) onClickShowFTTH.showFTTH();
        btnSwitchMobile.setChecked(true);
        btnSwitchInternet.setChecked(false);
    }

    long oldPress = 0;
    @OnClick(R.id.btn_next)
    void next() {
//        if (System.currentTimeMillis() - oldPress < 1000) {
//            return;
//        }
//
//        oldPress = System.currentTimeMillis();


        if (isCheckCarrier) return;
        InputMethodUtils.hideSoftKeyboard(mEdtInputPhone,getContext());
        String phoneNumber = Objects.requireNonNull(mEdtInputPhone.getText()).toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
        } else if (phoneNumber.length() < 8 || phoneNumber.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phoneNumber, "KH")) {
            ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
        } else {
            if (!phoneNumber.startsWith("0")){
                phoneNumber = "0"+ phoneNumber;
            }

            checkCarrier(phoneNumber);
            isCheckCarrier = true;
        }
    }
    boolean isCheckCarrier = false;
    // thanhlv - check metfone number
    private void checkCarrier(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.equals("")) return;
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.checkCarrier(phoneNumber, new ApiCallback<CheckCarrierResponse>() {
            @Override
            public void onResponse(Response<CheckCarrierResponse> response) {
                isCheckCarrier = false;
                if (response.body() != null) {
                    String code = response.body().getCode();
                    Log.d(TAG, "onResponse: " + response.body().getMessage());
                    if ("00".equals(code)) {
                        if (response.body().getData().getMetfone()) {
                            handleCheckPhoneNumber(phoneNumber);
                        } else {
                            ToastUtils.showToast(getActivity(), getString(R.string.m_p_add_metfone_number_error_not_metfone_number));
                        }
                    } else {
                        ToastUtils.showToast(getActivity(), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onError(Throwable error) {

                isCheckCarrier = false;
                Log.e(TAG, "onError: " , error);
            }
        });
    }

    private void handleCheckPhoneNumber(String phoneNumber) {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.checkPhone(phoneNumber, new ApiCallback<BodyResponse>() {
            @Override
            public void onResponse(Response<BodyResponse> response) {
                Log.e(TAG, "onResponse: " + response.body());
                isCheckCarrier = false;
                if (response.body() != null) {
                    String code = response.body().getCode();
                    Log.d(TAG, "onResponse: " + response.body().getMessage());
                    if ("00".equals(code) || "42".equals(code) || "68".equals(response.body().getCode())) {

                        initCombineDialog(phoneNumber, CODE_COMBINE);
                    } else {
                        onFragmentInteractionListener.goToVerifyScreen(phoneNumber, CODE_SUCCESS);
                        if (mEdtInputPhone != null)
                            mEdtInputPhone.setText("");
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                isCheckCarrier = false;
                Log.e(TAG, "onError: " , error);
            }
        });
    }

    private void initCombineDialog(String phoneNumber, String code) {
        MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                .setTitle(R.string.m_p_dialog_combine_phone_title)
                .setContent(R.string.m_p_dialog_combine_phone_content)
                .setLottieAssertName("lottie/img_dialog_man_thinking.json")
                .setButtonTop(R.string.m_p_dialog_combine_phone_top_btn, R.drawable.bg_red_button_corner_6)
                .setButtonBottom(R.string.m_p_dialog_combine_phone_bottom_btn, R.drawable.bg_transparent_button_corner_6)
                .setCancelableOnTouchOutside(false)
                .build();

        dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onTopButtonClick() {
                onFragmentInteractionListener.goToVerifyScreen(phoneNumber , code);
                if (mEdtInputPhone != null)
                    mEdtInputPhone.setText("");
            }

            @Override
            public void onBottomButtonClick() {
                dialogSuccess.dismiss();
            }
        });
        dialogSuccess.show(getChildFragmentManager(), null);
    }

    public interface OnFragmentInteractionListener {
        // Container Activity must implement this interface
        void skipToHome();
        void goToVerifyScreen(String phoneNumber, String codeCheckPhone);
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }
}
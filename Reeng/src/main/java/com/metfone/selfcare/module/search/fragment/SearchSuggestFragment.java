/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.widget.LinearLayout;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.SearchMovieResponse;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.model.Rewards;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardDetailItem;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.keeng.network.restpaser.RestSearchSuggest;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.search.activity.SearchAllActivity;
import com.metfone.selfcare.module.search.adapter.SearchAllAdapter;
import com.metfone.selfcare.module.search.model.ChatProvisional;
import com.metfone.selfcare.module.search.model.MoviesProvisional;
import com.metfone.selfcare.module.search.model.MusicProvisional;
import com.metfone.selfcare.module.search.model.NewsProvisional;
import com.metfone.selfcare.module.search.model.NotFound;
import com.metfone.selfcare.module.search.model.Provisional;
import com.metfone.selfcare.module.search.model.RewardProvisional;
import com.metfone.selfcare.module.search.model.TiinProvisional;
import com.metfone.selfcare.module.search.model.VideoProvisional;
import com.metfone.selfcare.module.search.network.ApiCallback;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;
import java.util.ArrayList;

public class SearchSuggestFragment extends SearchFragment {

    private ChatProvisional listChat;
    private VideoProvisional listVideo;
    private NewsProvisional listNews;
    private MoviesProvisional listMovies;
    private MusicProvisional listMusic;
    private TiinProvisional listTiin;
    private RewardProvisional listReward;
    private NotFound notFound;
    private SearchAllAdapter adapter;
    private Http lastRequestVideo;
    private Http lastRequestChannel;
    private Http lastRequestNews;
    private Http lastRequestMovies;
    private Http lastRequestRewards;
    private Http lastRequestMusic;
    private Http lastRequestTiin;
    private boolean isClearDataSearch = false;
    private LinearLayoutManager layoutManager;
    private Handler handlerSearch;
    private boolean isSearchOtherDone = true;
    private Runnable doSearchRunnable = () -> {
        Log.i(TAG, "doSearchRunnable");
        searchOther();
    };

    public static SearchSuggestFragment newInstance() {
        Bundle args = new Bundle();
        SearchSuggestFragment fragment = new SearchSuggestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static SearchSuggestFragment newInstance(Bundle bundle) {
        SearchSuggestFragment fragment = new SearchSuggestFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SearchAllSuggestFragment";
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        handlerSearch = new Handler();
    }

    @Override
    protected void initListener() {
        super.initListener();
        data = new ArrayList<>();
        adapter = new SearchAllAdapter(searchActivity);
        adapter.setItems(data);
        adapter.setListener(this);
        layoutManager = new CustomLinearLayoutManager(searchActivity, LinearLayout.VERTICAL, false);
        BaseAdapter.setupVerticalRecycler(searchActivity, recyclerView, layoutManager, adapter, false);
        if (mApplication != null) {
            if (mApplication.getConfigBusiness().isEnableTabVideo()) {
                apiCallbackVideo = new ApiCallback<ArrayList<Video>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<Video> result) {
                        SearchUtils.filterSearchVideo(keySearch, result, true);
                        if (Utilities.notEmpty(result)) {
                            if (listVideo == null)
                                listVideo = new VideoProvisional();
                            listVideo.setVideos(result);
                        }
                    }

                    @Override
                    public void onError(String s) {

                    }

                    @Override
                    public void onComplete() {
                        isSearchingVideo = false;
                        onSearchCompleted(listVideo);
                        updateTabBarTitle(listVideo == null || listVideo.getVideos() == null ?
                                        0 : listVideo.getVideos().size(),
                                SearchUtils.TAB_SEARCH_VIDEO);
                    }
                };
                apiCallbackChannel = new ApiCallback<ArrayList<Channel>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<Channel> result) {
                        SearchUtils.filterSearchChannel(keySearch, result, true);
                        if (Utilities.notEmpty(result)) {
                            if (listVideo == null)
                                listVideo = new VideoProvisional();
                            listVideo.setChannels(result);
                        }
                    }

                    @Override
                    public void onError(String s) {

                    }

                    @Override
                    public void onComplete() {
                        isSearchingChannel = false;
                        onSearchCompleted(listVideo);
                        updateTabBarTitle(listVideo == null || listVideo.getChannels() == null ?
                                        0 : listVideo.getChannels().size(),
                                SearchUtils.TAB_SEARCH_CHANNEL_VIDEO);
                    }
                };
            }

            if (mApplication.getConfigBusiness().isEnableTabNews()) {
                apiCallbackNews = new ApiCallback<ArrayList<NewsModel>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<NewsModel> result) {
                        if (Utilities.notEmpty(result)) {
                            SearchUtils.filterSearchNews(keySearch, result, true);
                            listNews = new NewsProvisional(result);
                        }
                    }

                    @Override
                    public void onError(String s) {

                    }

                    @Override
                    public void onComplete() {
                        isSearchingNews = false;
                        onSearchCompleted(listNews);
                        updateTabBarTitle(listNews == null || listNews.getData() == null ?
                                        0 : listNews.getData().size(),
                                SearchUtils.TAB_SEARCH_NEWS);
                    }
                };
            }

            if (mApplication.getConfigBusiness().isEnableTabMovie()) {
                mApiCallbackMovies = new ApiCallback<ArrayList<Movie>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<Movie> result) {
//                        SearchUtils.filterSearchMovies(keySearch, result, true);
                        if (Utilities.notEmpty(result)) {
                            listMovies = new MoviesProvisional(result);
                        }
                    }

                    @Override
                    public void onError(String s) {

                    }

                    @Override
                    public void onComplete() {
                        isSearchingMovies = false;
                        onSearchCompleted(listMovies);
                        updateTabBarTitle(listMovies == null || listMovies.getData() == null ?
                                        0 : listMovies.getData().size(),
                                SearchUtils.TAB_SEARCH_MOVIES);
                    }
                };
            }
            if(mApplication.getConfigBusiness().isEnableTabRewards()){
                //TODO SEARCH_REWARDS
                apiCallbackReward = new ApiCallback<ArrayList<SearchRewardResponse.Result.ItemReward>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<SearchRewardResponse.Result.ItemReward> result) throws Exception {
                        if (Utilities.notEmpty(result)) {
                            listReward = new RewardProvisional(result);
                        }
                    }

                    @Override
                    public void onError(String s) {

                    }

                    @Override
                    public void onComplete() {
                        isSearchingRewards = false;
                        onSearchCompleted(listReward);
                        updateTabBarTitle(listReward == null || listReward.getData() == null ?
                                        0 : listReward.getData().size(),
                                SearchUtils.TAB_SEARCH_REWARD);
                    }
                };
            }
            if (mApplication.getConfigBusiness().isEnableTabMusic()) {
                apiCallbackMusic = new ApiCallback<ArrayList<RestSearchSuggest.Group>>() {

                    @Override
                    public void onSuccess(String msg, ArrayList<RestSearchSuggest.Group> result) {
                        if (Utilities.notEmpty(result)) {
                            for (int i = 0; i < result.size(); i++) {
                                if (result.get(i) != null && result.get(i).doclist != null && Utilities.notEmpty(result.get(i).doclist.docs)) {
                                    if (SearchModel.TYPE_SONG.equals(result.get(i).groupValue)) {
                                        SearchUtils.filterSearchMusic(keySearch, result.get(i).doclist.docs, true, false);
                                        if (Utilities.notEmpty(result.get(i).doclist.docs)) {
                                            if (listMusic == null)
                                                listMusic = new MusicProvisional();
                                            listMusic.setDataSong(result.get(i).doclist.docs);
                                        }
                                    } else if (SearchModel.TYPE_VIDEO.equals(result.get(i).groupValue)) {
                                        SearchUtils.filterSearchMusic(keySearch, result.get(i).doclist.docs, true, false);
                                        if (Utilities.notEmpty(result.get(i).doclist.docs)) {
                                            if (listMusic == null)
                                                listMusic = new MusicProvisional();
                                            listMusic.setDataMV(result.get(i).doclist.docs);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(String s) {

                    }

                    @Override
                    public void onComplete() {
                        isSearchingMusic = false;
                        onSearchCompleted(listMusic);
                        updateTabBarTitle(listMusic == null || listMusic.getDataSong() == null ?
                                        0 : listMusic.getDataSong().size(),
                                SearchUtils.TAB_SEARCH_MUSIC);
                        updateTabBarTitle(listMusic == null || listMusic.getDataMV() == null ?
                                        0 : listMusic.getDataMV().size(),
                                SearchUtils.TAB_SEARCH_MV);
                    }
                };
            }

//            if (!mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            ContactBusiness contactBusiness = mApplication.getContactBusiness();
            if (!contactBusiness.isContactReady()) {
                contactBusiness.initContact();
            }
            if (!contactBusiness.isInitArrayListReady()) {
                contactBusiness.initArrayListPhoneNumber();
            }
            initComparatorSearchChat();
//            }
            if (mApplication.getConfigBusiness().isEnableTabTiin()) {
                apiCallbackTiin = new ApiCallback<ArrayList<TiinModel>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<TiinModel> result) {
                        if (Utilities.notEmpty(result)) {
                            SearchUtils.filterSearchTiin(keySearch, result, true);
                            listTiin = new TiinProvisional(result);
                        }
                    }

                    @Override
                    public void onError(String s) {

                    }

                    @Override
                    public void onComplete() {
                        isSearchingTiin = false;
                        onSearchCompleted(listTiin);
                        updateTabBarTitle(listTiin == null || listTiin.getData() == null ?
                                        0 : listTiin.getData().size(),
                                SearchUtils.TAB_SEARCH_TIIN);
                    }
                };
            }
        }
    }

    private void searchOther() {
        Log.i(TAG, "searchOther source: " + source);
        isSearchOtherDone = true;
        switch (source) {
            case Constants.TAB_NEWS_HOME:
                //searchNews(lastRequestNews, 1);
                searchVideo(lastRequestVideo, 0);
                searchChannelVideo(lastRequestChannel, 0);
                searchFilm(lastRequestMovies, 0);
                searchRewards(lastRequestRewards, 1);
                searchMusic(lastRequestMusic, 0);
                //searchTiin(lastRequestTiin, 1);
                break;

            case Constants.TAB_MOVIE_HOME:
                searchFilm(lastRequestMovies, 0);
                searchRewards(lastRequestRewards, 1);
                searchVideo(lastRequestVideo, 0);
                searchChannelVideo(lastRequestChannel, 0);
                //searchNews(lastRequestNews, 1);
                searchMusic(lastRequestMusic, 0);
                //searchTiin(lastRequestTiin, 1);
                break;

            case Constants.TAB_MUSIC_HOME:
                searchMusic(lastRequestMusic, 0);
                searchVideo(lastRequestVideo, 0);
                searchChannelVideo(lastRequestChannel, 0);
                //searchNews(lastRequestNews, 1);
                searchFilm(lastRequestMovies, 0);
                searchRewards(lastRequestRewards, 1);
                //searchTiin(lastRequestTiin, 1);
                break;
            case Constants.TAB_TIIN_HOME:
                //searchTiin(lastRequestTiin, 1);
                searchVideo(lastRequestVideo, 0);
                searchChannelVideo(lastRequestChannel, 0);
                searchFilm(lastRequestMovies, 0);
                searchRewards(lastRequestRewards, 1);
                //searchNews(lastRequestNews, 1);
                searchMusic(lastRequestMusic, 0);
                break;

            case Constants.TAB_VIDEO_HOME:
                break;
            default:
//                searchVideo(lastRequestVideo, 0);
//                searchChannelVideo(lastRequestChannel, 0);
                //searchNews(lastRequestNews, 1);
                searchFilm(lastRequestMovies, 0);
                searchRewards(lastRequestRewards,1);
//                searchMusic(lastRequestMusic, 0);
                //searchTiin(lastRequestTiin, 1);
                break;
        }
        if (Utilities.isEmpty(data) && !isLoading() && isSearchOtherDone) {
            if (NetworkHelper.isConnectInternet(searchActivity))
                showLoadedEmpty();
            else
                showLoadedError();
        }
    }

    @Override
    public void doSearch() {
        mustLoadData = false;
        Log.d(TAG, "doSearch keySearch: " + keySearch);
        if (searchActivity != null && isAdded()) searchActivity.setShowDataSuggest(false);
        cancelSearch();
        //showProgressLoading();
        showLoading();
        isSearchOtherDone = false;
        searchChat();
        // Fix crash firebase : SearchAllActivity.G on a null object reference
        if (handlerSearch != null) {
            if(searchActivity != null){
                searchActivity.totalAllResult = 0;
            }
            handlerSearch.removeCallbacks(doSearchRunnable);
            handlerSearch.postDelayed(doSearchRunnable, 500);
        } else {
            if(searchActivity != null){
                searchActivity.totalAllResult = 0;
            }
            searchOther();
        }
    }

    @Override
    public void cancelSearch() {
        listChat = null;
        listVideo = null;
        listNews = null;
        listMovies = null;
        listReward = null;
        listMusic = null;
        listTiin = null;
        notFound = null;
        if (lastRequestVideo != null) lastRequestVideo.cancel();
        if (lastRequestChannel != null) lastRequestChannel.cancel();
        if (lastRequestNews != null) lastRequestNews.cancel();
        if (lastRequestMovies != null) lastRequestMovies.cancel();
        if (lastRequestRewards != null) lastRequestRewards.cancel();
        if (lastRequestMusic != null) lastRequestMusic.cancel();
        if (lastRequestTiin != null) lastRequestTiin.cancel();
        if (searchContactsTask != null) {
            searchContactsTask.setListener(null);
            searchContactsTask.cancel(true);
            searchContactsTask = null;
        }
        isClearDataSearch = false;
        isSearchingChat = false;
        isSearchingChannel = false;
        isSearchingVideo = false;
        isSearchingNews = false;
        isSearchingMovies = false;
        isSearchingMusic = false;
        isSearchingTiin = false;
    }

    private void onSearchCompleted(Serializable serializable) {
        if (serializable != null)
            Log.i(TAG, "onSearchCompleted " + serializable.getClass().getCanonicalName());
        else
            Log.i(TAG, "onSearchCompleted serializable is null");
        boolean isClearData = !isClearDataSearch || searchActivity.isShowDataSuggest();
        isClearDataSearch = true;
        if (adapter != null) adapter.setKeySearch(keySearch);
        onLoadDataSuccess(adapter, data, serializable, isClearData, true);
        if (!searchActivity.isShowDataSuggest()) {
            int count = 0;
            if (!isSearchingChat) count++;
            if (!isSearchingChannel) count++;
            if (!isSearchingVideo) count++;
            if (!isSearchingNews) count++;
            if (!isSearchingMovies) count++;
            if (!isSearchingMusic) count++;
            if (!isSearchingTiin) count++;
            if (!isSearchingRewards) count++;

            if (Utilities.isEmpty(data) && !isLoading() && isSearchOtherDone) {
                if (NetworkHelper.isConnectInternet(searchActivity))
                    showLoadedEmpty();
                else
                    showLoadedError();
            } else if (count > 3 && Utilities.notEmpty(data)) {
                new Handler().postDelayed(() -> showLoadedSuccess(true), 250);
            }
        }
    }

    private int getIndexOfData(ArrayList<Object> data, Serializable dataChat, Serializable dataVideo
            , Serializable dataNews, Serializable dataMovies, Serializable dataMusic, Serializable dataTiin,Serializable dataReward, Serializable serializable) {
        int index = -1;
        if (serializable instanceof ChatProvisional) {
            //if (dataChat instanceof Provisional) index += 1;
            //if (dataVideo instanceof Provisional) index += ((Provisional) dataVideo).getSize();
            //if (dataNews instanceof Provisional) index += ((Provisional) dataNews).getSize();
            //if (dataMovies instanceof Provisional) index += ((Provisional) dataMovies).getSize();
            //if (dataMusic instanceof Provisional) index += ((Provisional) dataMusic).getSize();
            index = 0;
        } else if (serializable instanceof VideoProvisional) {
            index = 0;
            if (dataVideo != null && source == Constants.TAB_VIDEO_HOME) {
                if (dataChat instanceof Provisional) index += 1;
            } else {
                if (dataChat instanceof Provisional) index += 1;
                //if (dataVideo instanceof Provisional) index += ((Provisional) dataVideo).getSize();
                if (dataNews instanceof Provisional) index += ((Provisional) dataNews).getSize();
                if (dataMovies instanceof Provisional)
                    index += ((Provisional) dataMovies).getSize();
                if (dataMusic instanceof Provisional) index += ((Provisional) dataMusic).getSize();
                if (dataTiin instanceof Provisional) index += ((Provisional) dataTiin).getSize();
            }
        } else if (serializable instanceof NewsProvisional) {
            index = 0;
            if (dataNews != null && source == Constants.TAB_NEWS_HOME) {
                if (dataChat instanceof Provisional) index += 1;
            } else {
                if (dataChat instanceof Provisional) index += 1;
                if (dataVideo instanceof Provisional) index += ((Provisional) dataVideo).getSize();
                //if (dataNews instanceof Provisional) index += ((Provisional) dataNews).getSize();
                if (dataMovies instanceof Provisional)
                    index += ((Provisional) dataMovies).getSize();
                if (dataMusic instanceof Provisional) index += ((Provisional) dataMusic).getSize();
                if (dataTiin instanceof Provisional) index += ((Provisional) dataTiin).getSize();
                if (dataReward instanceof Provisional) index += ((Provisional) dataReward).getSize();
            }
        } else if (serializable instanceof MoviesProvisional) {
            index = 0;
            if (dataMovies != null && source == Constants.TAB_MOVIE_HOME) {
                if (dataChat instanceof Provisional) index += 1;
            } else {
                if (dataChat instanceof Provisional) index += 1;
                if (dataVideo instanceof Provisional) index += ((Provisional) dataVideo).getSize();
                if (dataNews instanceof Provisional) index += ((Provisional) dataNews).getSize();
                //if (dataMovies instanceof Provisional) index += ((Provisional) dataMovies).getSize();
                if (dataMusic instanceof Provisional) index += ((Provisional) dataMusic).getSize();
                if (dataTiin instanceof Provisional) index += ((Provisional) dataTiin).getSize();
                if (dataReward instanceof Provisional) index += ((Provisional) dataReward).getSize();
            }
        } else if (serializable instanceof MusicProvisional) {
            index = 0;
            if (dataMusic != null && source == Constants.TAB_MUSIC_HOME) {
                if (dataChat instanceof Provisional) index += 1;
            } else {
                if (dataChat instanceof Provisional) index += 1;
                if (dataVideo instanceof Provisional) index += ((Provisional) dataVideo).getSize();
                if (dataNews instanceof Provisional) index += ((Provisional) dataNews).getSize();
                if (dataMovies instanceof Provisional)
                    index += ((Provisional) dataMovies).getSize();
                //if (dataMusic instanceof Provisional) index += ((Provisional) dataMusic).getSize();
                if (dataTiin instanceof Provisional) index += ((Provisional) dataTiin).getSize();
            }
        } else if (serializable instanceof TiinProvisional) {
            index = 0;
            if (dataTiin != null && source == Constants.TAB_TIIN_HOME) {
                if (dataChat instanceof Provisional) index += 1;
            } else {
                if (dataChat instanceof Provisional) index += 1;
                if (dataVideo instanceof Provisional) index += ((Provisional) dataVideo).getSize();
                if (dataNews instanceof Provisional) index += ((Provisional) dataNews).getSize();
                if (dataMovies instanceof Provisional)
                    index += ((Provisional) dataMovies).getSize();
                if (dataMusic instanceof Provisional) index += ((Provisional) dataMusic).getSize();
                if (dataReward instanceof Provisional) index += ((Provisional) dataReward).getSize();
//                if (dataTiin instanceof Provisional) index += ((Provisional) dataTiin).getSize();
            }
        } else if (serializable instanceof RewardProvisional) {
            index = 0;
            if (dataReward != null && source == Constants.TAB_REWARD) {
                if (dataReward instanceof Provisional) index += 1;
            } else {
                if (dataReward instanceof Provisional)
                    index += ((Provisional) dataReward).getSize();
//                if (dataTiin instanceof Provisional) index += ((Provisional) dataTiin).getSize();
            }
        }
        if (data != null && index > data.size()) index = data.size();
        return index;
    }

    private void addItemNotFound() {
        if (mApplication != null && Utilities.notEmpty(data) && !isLoading() && isSearchOtherDone && !data.contains(notFound)) {
            switch (source) {
                case Constants.TAB_MOBILE_HOME:
                    if (listChat == null) {
                        notFound = new NotFound(mApplication.getString(R.string.search_tab_chat)
                                , mApplication.getString(R.string.no_results_found_contact, keySearch)
                                , SearchAllAdapter.TYPE_CHAT);
                        data.add(0, notFound);
                    }
                    break;
                case Constants.TAB_VIDEO_HOME:
                    if (listVideo == null || Utilities.isEmpty(listVideo.getVideos())) {
                        notFound = new NotFound(mApplication.getString(R.string.search_tab_video)
                                , mApplication.getString(R.string.no_results_found_video, keySearch)
                                , SearchAllAdapter.TYPE_VIDEO);
                        data.add((listChat == null) ? 0 : 1, notFound);
                    }
                    break;
                case Constants.TAB_NEWS_HOME:
//                    if (listNews == null) {
//                        notFound = new NotFound(mApplication.getString(R.string.search_tab_news)
//                                , mApplication.getString(R.string.no_results_found_news, keySearch)
//                                , SearchAllAdapter.TYPE_NEWS);
//                        data.add((listChat == null) ? 0 : 1, notFound);
//                    }
                    break;
                case Constants.TAB_MOVIE_HOME:
                    if (listMovies == null) {
                        notFound = new NotFound(mApplication.getString(R.string.search_tab_movies)
                                , mApplication.getString(R.string.no_results_found_movies, keySearch)
                                , SearchAllAdapter.TYPE_MOVIES);
                        data.add((listChat == null) ? 0 : 1, notFound);
                    }
                    break;
                case Constants.TAB_MUSIC_HOME:
                    if (listMusic == null) {
                        notFound = new NotFound(mApplication.getString(R.string.search_tab_music)
                                , mApplication.getString(R.string.no_results_found_music, keySearch)
                                , SearchAllAdapter.TYPE_MUSIC);
                        data.add((listChat == null) ? 0 : 1, notFound);
                    }
                    break;
                case Constants.TAB_REWARD:
                    if(listReward == null){
                        notFound = new NotFound(mApplication.getString(R.string.search_tab_music)
                                , mApplication.getString(R.string.no_results_found_music, keySearch)
                                , SearchAllAdapter.TYPE_REWARD);
                        data.add((listReward == null) ? 0 : 1, notFound);

                    }
                case Constants.TAB_TIIN_HOME:
//                    if (listTiin == null) {
//                        notFound = new NotFound(mApplication.getString(R.string.tab_tiin)
//                                , mApplication.getString(R.string.no_results_found_news, keySearch)
//                                , SearchAllAdapter.TYPE_TIIN);
//                        data.add(0, notFound);
//                    }
                    break;
            }
        }
    }

    protected void onLoadDataSuccess(SearchAllAdapter adapter, ArrayList<Object> data, Serializable serializable, boolean isClearData, boolean isSearch) {
        Log.i(TAG, "onLoadDataSuccess isClearData: " + isClearData);
        if (data == null) data = new ArrayList<>();
        if (isClearData) {
            data.clear();
            if (adapter != null) {
                adapter.setKeySearch(keySearch);
                adapter.notifyDataSetChanged();
            }
        }
        if (Utilities.notEmpty(data)) {
            int size = data.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (serializable instanceof ChatProvisional && data.get(i) instanceof ChatProvisional) {
                    data.remove(i);
                    if (adapter != null) adapter.notifyItemRemoved(i);
                } else if (serializable instanceof VideoProvisional && data.get(i) instanceof VideoProvisional) {
                    data.remove(i);
                    if (adapter != null) adapter.notifyItemRemoved(i);
                } else if (serializable instanceof NewsProvisional && data.get(i) instanceof NewsProvisional) {
                    data.remove(i);
                    if (adapter != null) adapter.notifyItemRemoved(i);
                } else if (serializable instanceof MoviesProvisional && data.get(i) instanceof MoviesProvisional) {
                    data.remove(i);
                    if (adapter != null) adapter.notifyItemRemoved(i);
                } else if (serializable instanceof MusicProvisional && data.get(i) instanceof MusicProvisional) {
                    data.remove(i);
                    if (adapter != null) adapter.notifyItemRemoved(i);
                } else if (serializable instanceof TiinProvisional && data.get(i) instanceof TiinProvisional) {
                    data.remove(i);
                    if (adapter != null) adapter.notifyItemRemoved(i);
                }else if (serializable instanceof RewardProvisional && data.get(i) instanceof RewardProvisional) {
                    data.remove(i);
                    if (adapter != null) adapter.notifyItemRemoved(i);
                }
            }
        }

        int index = getIndexOfData(data, listChat, listVideo, listNews, listMovies, listMusic, listTiin,listReward, serializable);
//        if (isSearch) {
//            index = getIndexOfData(data, listChat, listVideo, listNews, listMovies, listMusic, serializable);
//        } else {
//            index = getIndexOfData(data, dataSuggestChat, dataSuggestCall, dataSuggestContact, dataSuggestGroup
//                    , dataSuggestVideo, dataSuggestNews, dataSuggestMovies, dataSuggestMusic, serializable);
//        }
        boolean notifyItemInserted = index < data.size() && index >= 0;
        if (index > data.size()) {
            index = data.size();
        }
        if (serializable != null && !data.contains(serializable)) {
            if (serializable instanceof VideoProvisional) {
                VideoProvisional model = (VideoProvisional) serializable;
                if (model.getChannels() != null) {
                    VideoProvisional tmp = new VideoProvisional();
                    tmp.setChannels(model.getChannels());
                    data.add(index, tmp);
                }
                if (model.getVideos() != null) {
                    VideoProvisional tmp = new VideoProvisional();
                    tmp.setVideos(model.getVideos());
                    data.add(index, tmp);
                }
            } else {
                data.add(index, serializable);
            }
        }
        addItemNotFound();
        if (adapter != null) {
            adapter.setKeySearch(keySearch);
            if (notifyItemInserted) {
                Log.i(TAG, "onLoadDataSuccess notifyItemInserted " + index);
                adapter.notifyItemInserted(index);
            } else {
                Log.i(TAG, "onLoadDataSuccess notifyDataSetChanged " + index);
                adapter.notifyDataSetChanged();
            }
        }
        if (layoutManager != null) layoutManager.scrollToPositionWithOffset(0, 0);
    }

    @Override
    public void onFinishedSearchContacts(String keySearch, ArrayList<Object> list) {
        isSearchingChat = false;
        if (searchActivity != null && isAdded() && !searchActivity.isShowDataSuggest()) {
            listChat = null;
            if (Utilities.notEmpty(list)) {
                listChat = new ChatProvisional();
                listChat.setListChat(list);
            }
            onSearchCompleted(listChat);
            updateTabBarTitle(listChat == null || listChat.getListChat() == null ? 0 : listChat.getListChat().size(),
                    SearchUtils.TAB_SEARCH_CHAT);
        }
    }

    @Override
    protected void resetData() {
        if (data == null) data = new ArrayList<>();
        else data.clear();
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickMenuMore(Object data) {
        if (searchActivity != null) {
            searchActivity.onClickMenuMore(data);
        }
    }
}
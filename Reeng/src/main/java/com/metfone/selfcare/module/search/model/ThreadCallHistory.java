/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/5
 *
 */

package com.metfone.selfcare.module.search.model;

import android.text.TextUtils;

import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.call.CallHistory;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;

public class ThreadCallHistory implements Serializable {
    private CallHistory callHistory;
    private PhoneNumber contact;
    private StrangerPhoneNumber stranger;
    private NonContact nonContact;

    public ThreadCallHistory(CallHistory callHistory) {
        this.callHistory = callHistory;
    }

    public String getTitle() {
        if (contact != null) return contact.getName();
        if (stranger != null) {
            if (TextUtils.isEmpty(stranger.getFriendName())) {
                return Utilities.hidenPhoneNumber(stranger.getFriendName());
            } else {
                return stranger.getFriendName();
            }
        }
        if (callHistory != null) return callHistory.getFriendNumber();
        return "";
    }

    public boolean isEnableVideo() {
        if (contact != null) return contact.isReeng();
        if (stranger != null) return false;
        if (nonContact != null) nonContact.isReeng();
        return false;
    }

    public CallHistory getCallHistory() {
        return callHistory;
    }

    public PhoneNumber getContact() {
        return contact;
    }

    public void setContact(PhoneNumber contact) {
        this.contact = contact;
    }

    public StrangerPhoneNumber getStranger() {
        return stranger;
    }

    public void setStranger(StrangerPhoneNumber stranger) {
        this.stranger = stranger;
    }

    public NonContact getNonContact() {
        return nonContact;
    }

    public void setNonContact(NonContact nonContact) {
        this.nonContact = nonContact;
    }

}

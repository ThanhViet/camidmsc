package com.metfone.selfcare.module.livestream.network;

import retrofit2.Response;

public interface APICallBack<T> {
    void onResponse(Response<T> response);

    void onError(Throwable error);
}

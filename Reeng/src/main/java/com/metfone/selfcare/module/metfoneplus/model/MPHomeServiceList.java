package com.metfone.selfcare.module.metfoneplus.model;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardSearchItem;

import java.util.ArrayList;
import java.util.List;

public class MPHomeServiceList implements IHomeModelType {

    public List<MPHomeServiceItem> serviceItems = new ArrayList<>();

    @Override
    public int getItemType() {
        return IHomeModelType.MP_SERVICE;
    }
}

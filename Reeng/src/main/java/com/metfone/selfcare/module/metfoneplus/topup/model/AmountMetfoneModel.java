package com.metfone.selfcare.module.metfoneplus.topup.model;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AmountMetfoneModel {
    private int amount;
    private boolean isCheckSelect;

    public AmountMetfoneModel(int amount, boolean isCheckSelect) {
        this.amount = amount;
        this.isCheckSelect = isCheckSelect;
    }

    public static ArrayList<AmountMetfoneModel> createAmountValue() {
        ArrayList<AmountMetfoneModel> metfoneModels = new ArrayList<>();
        metfoneModels.add(new AmountMetfoneModel(1, false));
        metfoneModels.add(new AmountMetfoneModel(2, false));
        metfoneModels.add(new AmountMetfoneModel(5, false));
        metfoneModels.add(new AmountMetfoneModel(10, false));
        metfoneModels.add(new AmountMetfoneModel(20, false));
        return metfoneModels;
    }
}



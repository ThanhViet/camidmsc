/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.search.holder.BoxHolder;
import com.metfone.selfcare.module.search.holder.BoxMusicHolder;
import com.metfone.selfcare.module.search.holder.NotFoundHolder;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.ChatProvisional;
import com.metfone.selfcare.module.search.model.MoviesProvisional;
import com.metfone.selfcare.module.search.model.MusicProvisional;
import com.metfone.selfcare.module.search.model.NewsProvisional;
import com.metfone.selfcare.module.search.model.NotFound;
import com.metfone.selfcare.module.search.model.RewardProvisional;
import com.metfone.selfcare.module.search.model.SearchHistoryProvisional;
import com.metfone.selfcare.module.search.model.TiinProvisional;
import com.metfone.selfcare.module.search.model.VideoProvisional;
import com.metfone.selfcare.util.Utilities;

public class SearchAllAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    public static final int TYPE_CHAT = 1;
    public static final int TYPE_VIDEO = 2;
    public static final int TYPE_NEWS = 3;
    public static final int TYPE_MOVIES = 4;
    public static final int TYPE_MUSIC = 5;
    public static final int TYPE_CHANNEL = 6;
    public static final int TYPE_HISTORY = 7;
    public static final int TYPE_NOT_FOUND = 8;
    public static final int TYPE_TIIN = 9;
    public static final int TYPE_MOVIES_ALL = 10;
    public static final int TYPE_REWARD = 11;
    private SearchAllListener.OnAdapterClick listener;
    private String keySearch;

    public SearchAllAdapter(Activity activity) {
        super(activity);
    }

    public void setListener(SearchAllListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    public void setKeySearch(String keySearch) {
        this.keySearch = keySearch;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof ChatProvisional) {
            return TYPE_CHAT;
        } else if (item instanceof VideoProvisional) {
            if (Utilities.notEmpty(((VideoProvisional) item).getVideos()))
                return TYPE_VIDEO;
            if (Utilities.notEmpty(((VideoProvisional) item).getChannels()))
                return TYPE_CHANNEL;
        } else if (item instanceof NewsProvisional) {
            return TYPE_NEWS;
        } else if (item instanceof MoviesProvisional) {
            return TYPE_MOVIES_ALL;
        } else if (item instanceof MusicProvisional) {
            return TYPE_MUSIC;
        } else if (item instanceof SearchHistoryProvisional) {
            return TYPE_HISTORY;
        } else if (item instanceof NotFound) {
            return TYPE_NOT_FOUND;
        } else if (item instanceof TiinProvisional) {
            return TYPE_TIIN;
        }else if(item instanceof RewardProvisional){
            return TYPE_REWARD;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public BaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CHAT:
                return new BoxHolder(layoutInflater.inflate(R.layout.holder_search_all_box_thread_chat, parent, false)
                        , activity, listener, viewType);
            case TYPE_MOVIES_ALL:
            case TYPE_MOVIES:
                return new BoxHolder(layoutInflater.inflate(R.layout.holder_search_all_box_movies, parent, false)
                        , activity, listener, viewType);
            case TYPE_VIDEO:
                return new BoxHolder(layoutInflater.inflate(R.layout.holder_search_all_box_video, parent, false)
                        , activity, listener, viewType);
            case TYPE_CHANNEL:
                return new BoxHolder(layoutInflater.inflate(R.layout.holder_search_all_box_channel, parent, false)
                        , activity, listener, viewType);
            case TYPE_NEWS:
                return new BoxHolder(layoutInflater.inflate(R.layout.holder_search_all_box_news, parent, false)
                        , activity, listener, viewType);
            case TYPE_MUSIC:
                return new BoxMusicHolder(layoutInflater.inflate(R.layout.holder_search_all_box_music, parent, false)
                        , activity, listener, viewType);
            case TYPE_HISTORY:
                return new BoxHolder(layoutInflater.inflate(R.layout.holder_search_all_box_history_search, parent, false)
                        , activity, listener, viewType);
            case TYPE_NOT_FOUND:
                return new NotFoundHolder(layoutInflater.inflate(R.layout.holder_search_all_not_found, parent, false)
                        , listener);
            case TYPE_TIIN:
                return new BoxHolder(layoutInflater.inflate(R.layout.holder_search_all_box_news, parent, false)
                        , activity, listener, viewType);
            case TYPE_REWARD:
                return new BoxHolder(layoutInflater.inflate(R.layout.holder_search_all_box_reward, parent, false)
                    , activity, listener, viewType);
            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseAdapter.ViewHolder holder, int position) {
        if (holder instanceof BoxHolder) {
            ((BoxHolder) holder).bindData(getItem(position), position, keySearch);
        } else if (holder instanceof BoxMusicHolder) {
            ((BoxMusicHolder) holder).bindData(getItem(position), position, keySearch);
        } else
            holder.bindData(getItem(position), position);
    }
}

package com.metfone.selfcare.module.gameLive.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;

public class DialogGameRule extends Dialog {
    public DialogGameRule(@NonNull Context context) {
        super(context, R.style.DialogFullscreen);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        Window window = getWindow();
//        if (window != null) {
//            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            //window.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
//        }

        setContentView(R.layout.dialog_game_rule);
        View btnBack = findViewById(R.id.iv_back);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                dismiss();
            }
        });

        ImageView bg = findViewById(R.id.iv_background);
        ImageBusiness.setResource(bg, R.drawable.bg_game_live_result);
    }
}

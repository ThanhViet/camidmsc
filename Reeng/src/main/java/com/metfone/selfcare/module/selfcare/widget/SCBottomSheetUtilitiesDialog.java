package com.metfone.selfcare.module.selfcare.widget;

import android.content.Intent;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomDialog;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

public class SCBottomSheetUtilitiesDialog implements View.OnClickListener {

    BaseSlidingFragmentActivity activity;
    BottomDialog bottomSheetDialog;

    private View btnMyShare;
    private View btnMyCredit;

    public SCBottomSheetUtilitiesDialog(@NonNull BaseSlidingFragmentActivity activity) {
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_sc_bottom_sheet_utilities, null);
        bottomSheetDialog = new BottomDialog(activity);
        bottomSheetDialog.setContentView(view);
        this.activity = activity;
        init(view);
    }

    protected void init(View view) {
        btnMyShare = view.findViewById(R.id.btnMyShare);
        btnMyCredit = view.findViewById(R.id.btnMyCredit);

        btnMyShare.setOnClickListener(this);
        btnMyCredit.setOnClickListener(this);
    }

    public void dismiss() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.dismiss();
            bottomSheetDialog = null;
        }
        btnMyCredit = null;
        btnMyShare = null;
    }

    public void show() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMyShare:
                if (activity != null) {
                    Intent intent = new Intent(activity, TabSelfCareActivity.class);
                    intent.putExtra(Constants.KEY_TYPE, SCConstants.SELF_CARE.TAB_MY_SHARE);
                    activity.startActivity(intent);
                }

                dismiss();
                break;

            case R.id.btnMyCredit:
                if (activity != null) {
                    Intent intent = new Intent(activity, TabSelfCareActivity.class);
                    intent.putExtra(Constants.KEY_TYPE, SCConstants.SELF_CARE.TAB_MY_CREDIT);
                    activity.startActivity(intent);
                }

                dismiss();
                break;
        }
    }
}
package com.metfone.selfcare.module.netnews.request;

/**
 * Created by HaiKE on 10/7/17.
 */

public class CountNewsRequest {
    int cateId;

    public CountNewsRequest(int cateId)
    {
        this.cateId = cateId;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }
}

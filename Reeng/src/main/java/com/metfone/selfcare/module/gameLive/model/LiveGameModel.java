package com.metfone.selfcare.module.gameLive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LiveGameModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("link_game")
    @Expose
    private String linkGame;

    @SerializedName("type")
    @Expose
    private int type;

    @SerializedName("timeStart")
    @Expose
    private long timeStart;

    @SerializedName("timeStartStr")
    @Expose
    private String timeStartStr;

    @SerializedName("TimeStop")
    @Expose
    private long timeStop;

    @SerializedName("num_play_special")
    @Expose
    private String num_play_special;

    @SerializedName("total_prize")
    @Expose
    private String total_prize;

    @SerializedName("prize_special")
    @Expose
    private String prize_special;

    @SerializedName("active")
    @Expose
    private int active;

    @SerializedName("currentQuestion")
    @Expose
    private int currentQuestion;

    public int getCurrentQuestion() {
        return currentQuestion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getNumPlaySpecial() {
        if (isInteger(num_play_special))
            return Integer.parseInt(num_play_special);
        return 0;
    }

    public int getTotalPrize() {
        if (isInteger(total_prize))
            return Integer.parseInt(total_prize);
        return 0;
    }

    public int getSpecialPrize() {
        if (isInteger(prize_special))
            return Integer.parseInt(prize_special);
        return 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkGame() {
        return linkGame;
    }

    public void setLinkGame(String linkGame) {
        this.linkGame = linkGame;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public long getTimeStop() {
        return timeStop;
    }

    public void setTimeStop(long timeStop) {
        this.timeStop = timeStop;
    }

    public String getTimeStartStr() {
        return timeStartStr;
    }

    @Override
    public String toString() {
        return "LiveGameModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", linkGame='" + linkGame + '\'' +
                ", type=" + type +
                ", timeStart=" + timeStart +
                ", timeStop=" + timeStop +
                '}';
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}

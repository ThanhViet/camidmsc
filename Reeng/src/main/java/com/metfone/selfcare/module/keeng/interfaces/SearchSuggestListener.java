/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2018/12/19
 *
 */

package com.metfone.selfcare.module.keeng.interfaces;

import android.view.View;

import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.SearchModel;

public interface SearchSuggestListener {

    void clickSuggestHistory(View view, SearchModel item);

    void clickSuggestMusic(View view, AllModel item);

    void clickOptionSuggestMusic(View view, AllModel item);

    void clickSuggestMovies(View view, Movie item);

    void searchSubmit(View view, String keyword);

    void clickResultSearch(View view, SearchModel item);

    void clickDeleteSearchHistory(View view);

}

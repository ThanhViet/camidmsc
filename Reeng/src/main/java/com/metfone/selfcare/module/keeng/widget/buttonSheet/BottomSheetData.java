package com.metfone.selfcare.module.keeng.widget.buttonSheet;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.CategoryModel;
import com.metfone.selfcare.module.keeng.model.PlayListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by namnh40 on 3/21/2017.
 */

public class BottomSheetData {
    //    public static final int DOWNLOAD = 1;
//    public static final int ADD_PLAYLIST = 2;
//    public static final int ADD_FAVORITE = 3;
    public static final int SHARE = 4;
    //    public static final int GIFT = 5;
    public static final int ADD_PLAYING = 6;
    //    public static final int CREATE_PLAYLIST = 7;
//    public static final int PLAYLIST = 8;
//    public static final int DELETE_INBOX = 9;
//    public static final int LISTEN_TOGETHER = 10;
//    public static final int REMOVE_SONG_IN_PLAYLIST = 11;
//    public static final int COPY_COMMENT = 12;
//    public static final int REPORT_COMMENT = 13;
//    public static final int DELETE_COMMENT = 14;
//    public static final int EDIT_COMMENT = 15;
//    public static final int UNFOLLOW = 16;
//    public static final int SEND_MESSAGE = 17;
//    public static final int DELETE_PLAYLIST = 18;
//    public static final int EDIT_PLAYLIST = 19;
//    public static final int LISTEN_TOGETHER_ACQUAINTANCES = 20;
//    public static final int LISTEN_TOGETHER_STRANGERS = 21;
//    public static final int TAKE_PHOTO = 22;
//    public static final int OPEN_GALLERY = 23;
//    public static final int DELETE_SONG_UPLOAD = 24;
//    public static final int EDIT_SONG_UPLOAD = 25;
//    public static final int DELETE_VIDEO_UPLOAD = 26;
//    public static final int EDIT_VIDEO_UPLOAD = 27;
//    public static final int SHOW_DOWNLOAD_SONG = 28;
//    public static final int SHOW_DOWNLOAD_ALBUM = 29;
//    public static final int SETUP_RINGBACK_TONE = 30;
//    public static final int SHOW_DOWNLOAD_LOSSLESS = 31;
//    public static final int SHOW_DOWNLOAD_REGIS_VIP = 32;
//    public static final int SHOW_DOWNLOAD_VIDEO = 33;
//    public static final int SHOW_SONG_OFFLINE_UPLOAD = 34;
//    public static final int SHOW_SONG_OFFLINE_DELETE = 35;
//    public static final int SHOW_VIDEO_OFFLINE_UPLOAD = 36;
//    public static final int SHOW_VIDEO_OFFLINE_DELETE = 37;
//    public static final int SHOW_SETTING_RINGTONE = 38;
//    public static final int PLAY_MEDIA_RELATIONSHIP = 39;
    public static final int REMOVE_PLAYING = 40;
    public static final int SHOW_SONG_INFO = 41;
    public static final int SHOW_VIDEO_INFO = 42;
    public static final int SHOW_EXIT = 43;

    public static List<CategoryModel> getPopupOfSong(AllModel item, boolean isPlaying, boolean isAddPlaylist, boolean isListenTogether, boolean isRemove, boolean removePlaying) {
        List<CategoryModel> list = new ArrayList<>();
        if (item.getId() > 0) {
            if (isPlaying)
                list.add(new CategoryModel(ADD_PLAYING, R.string.add_playing, R.drawable.ic_add_white_v5).setMedia(item));
            if (removePlaying)
                list.add(new CategoryModel(REMOVE_PLAYING, R.string.remove_playing, R.drawable.ic_close_option).setMedia(item));
            list.add(new CategoryModel(SHARE, R.string.share, R.drawable.ic_share_option).setMedia(item));
            list.add(new CategoryModel(SHOW_SONG_INFO, R.string.song_info, R.drawable.ic_v5_info_toolbar).setMedia(item));
            list.add(new CategoryModel(SHOW_EXIT, R.string.btn_cancel_option, R.drawable.ic_close_option).setMedia(item));
        }

        return list;
    }

    public static List<CategoryModel> getPopupOfAlbum(AllModel item) {
        List<CategoryModel> list = new ArrayList<>();
        list.add(new CategoryModel(SHARE, R.string.share, R.drawable.ic_share_option).setMedia(item));
        list.add(new CategoryModel(SHOW_EXIT, R.string.btn_cancel_option, R.drawable.ic_close_option).setMedia(item));
        return list;
    }

    public static List<CategoryModel> getPopupOfVideo(AllModel item) {
        List<CategoryModel> list = new ArrayList<>();
        list.add(new CategoryModel(SHARE, R.string.share, R.drawable.ic_share_option).setMedia(item));
        list.add(new CategoryModel(SHOW_VIDEO_INFO, R.string.video_info, R.drawable.ic_v5_info_toolbar).setMedia(item));
        list.add(new CategoryModel(Constants.MENU.MENU_REPORT_VIDEO, R.string.title_report_mv, R.drawable.ic_flag_option).setMedia(item));
        list.add(new CategoryModel(SHOW_EXIT, R.string.btn_cancel_option, R.drawable.ic_close_option).setMedia(item));
        return list;
    }

    public static List<CategoryModel> getPopupOfPlaylist(PlayListModel item) {
        List<CategoryModel> list = new ArrayList<>();
        list.add(new CategoryModel(SHARE, R.string.share, R.drawable.ic_share_option).setPlaylist(item));
        list.add(new CategoryModel(SHOW_EXIT, R.string.btn_cancel_option, R.drawable.ic_close_option).setPlaylist(item));
        return list;
    }

}

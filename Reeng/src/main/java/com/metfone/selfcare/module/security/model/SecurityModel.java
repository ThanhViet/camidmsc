/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/25
 */

package com.metfone.selfcare.module.security.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SecurityModel {
    @SerializedName("sms_spam_whitelist")
    private ArrayList<String> smsSpamWhitelist;
    @SerializedName("sms_spam_blacklist")
    private ArrayList<String> smsSpamBlacklist;
    @SerializedName("sms_firewall_whitelist")
    private ArrayList<String> smsFirewallWhitelist;
    @SerializedName("sms_firewall_mode")
    private String smsFirewallMode;

    public String getSmsFirewallMode() {
        return smsFirewallMode;
    }

    public ArrayList<String> getSmsSpamWhitelist() {
        if (smsSpamWhitelist == null) smsSpamWhitelist = new ArrayList<>();
        return smsSpamWhitelist;
    }

    public void setSmsSpamWhitelist(ArrayList<String> smsSpamWhitelist) {
        this.smsSpamWhitelist = smsSpamWhitelist;
    }

    public ArrayList<String> getSmsSpamBlacklist() {
        if (smsSpamBlacklist == null) smsSpamBlacklist = new ArrayList<>();
        return smsSpamBlacklist;
    }

    public void setSmsSpamBlacklist(ArrayList<String> smsSpamBlacklist) {
        this.smsSpamBlacklist = smsSpamBlacklist;
    }

    public ArrayList<String> getSmsFirewallWhitelist() {
        if (smsFirewallWhitelist == null) smsFirewallWhitelist = new ArrayList<>();
        return smsFirewallWhitelist;
    }

    public void setSmsFirewallWhitelist(ArrayList<String> smsFirewallWhitelist) {
        this.smsFirewallWhitelist = smsFirewallWhitelist;
    }

    public void setSmsFirewallMode(String smsFirewallMode) {
        this.smsFirewallMode = smsFirewallMode;
    }

    public int getSmsFirewallModeInt() {
        try {
            return Integer.parseInt(smsFirewallMode);
        } catch (Exception e) {
        }
        return 0;
    }

    public boolean enableFirewallMode() {
        return "1".equals(smsFirewallMode);
    }
}

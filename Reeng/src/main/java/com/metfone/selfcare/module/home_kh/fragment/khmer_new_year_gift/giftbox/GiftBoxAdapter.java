package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.databinding.ItemKhmerYearGiftBoxBinding;
import com.metfone.selfcare.module.home_kh.api.WsGiftBoxResponse;

public class GiftBoxAdapter extends BaseAdapter<GiftBoxAdapter.ViewHolder, WsGiftBoxResponse.PrizeInfo> {

    private EventListener listener;
    public GiftBoxAdapter(Activity activity, EventListener listener) {
        super(activity);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_khmer_year_gift_box, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    class ViewHolder extends BaseAdapter.ViewHolder{
        public ViewHolder(View view) {
            super(view);
        }

        public void bind(WsGiftBoxResponse.PrizeInfo info){
            ItemKhmerYearGiftBoxBinding binding = DataBindingUtil.bind(itemView);
            binding.setData(info);
            Glide.with(binding.imgView)
                    .load(info.getImage())
                    .placeholder(R.drawable.pchum_ben)
                    .into(binding.imgView);
            if(listener != null){
                binding.btnRedeem.setOnClickListener(v-> listener.onRedeemClicked(info));
                binding.btnQrCode.setOnClickListener(v-> listener.onShowQrClicked(info));
            }
        }
    }

    public interface EventListener{
        void onRedeemClicked(WsGiftBoxResponse.PrizeInfo info);
        void onShowQrClicked(WsGiftBoxResponse.PrizeInfo info);
    }
}

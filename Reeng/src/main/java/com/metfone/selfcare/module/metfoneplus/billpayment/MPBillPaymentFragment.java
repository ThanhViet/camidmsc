package com.metfone.selfcare.module.metfoneplus.billpayment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Observable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.android.gms.common.util.Strings;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.databinding.FragmentBillPaymentBinding;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseBindingFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountPaymentInfoResponse;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import retrofit2.Response;

public class MPBillPaymentFragment extends MPBaseBindingFragment<FragmentBillPaymentBinding> {
    public ObservableField<String> accountNumber = new ObservableField<>("");;
    public ObservableBoolean shouldEnableCheckButton = new ObservableBoolean(false);;
    private MetfonePlusClient client;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_bill_payment;
    }

    public static MPBillPaymentFragment newInstance(String account) {
        Bundle args = new Bundle();
        args.putString("ACCOUNT",account);
        MPBillPaymentFragment fragment = new MPBillPaymentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String account = getArguments().getString("ACCOUNT");
        accountNumber.set(account);
        accountNumber.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                shouldEnableCheckButton.set(!Strings.isEmptyOrWhitespace(accountNumber.get()));
            }
        });
        mBinding.setHandler(this);
        Utilities.adaptViewForInserts(mBinding.viewRoot);
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.GONE);
        }

        client = new MetfonePlusClient();
    }

    public void onBack() {
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarColor(R.color.m_home_tab_background_2, true);
            ((HomeActivity)mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }else{
            mParentActivity.finish();
            return;
        }
        popBackStackFragment();
    }

    public void onCheckClicked(){
        client.getBillPaymentInfo(accountNumber.get(), new MPApiCallback<WsGetAccountPaymentInfoResponse>() {
            @Override
            public void onResponse(Response<WsGetAccountPaymentInfoResponse> response) {
                if(response.body() != null && response.body().getResult() != null){
                    if(response.body().getResult().getErrorCode().equals("0") && response.body().getResult().getWsResponse() != null){
                        replaceFragmentWithAnimationDefault(MPBillPaymentDetailFragment.newInstance(response.body().getResult().getWsResponse()));
                    }
                    else{
                        ToastUtils.showToast(getContext(), response.body().getResult().getMessage());
                    }
                }else {
                    onError(null);
                }

            }

            @Override
            public void onError(Throwable error) {
                ToastUtils.showToast(getContext(), getString(R.string.m_p_notify_error_in_processing));
            }
        });
    }
}

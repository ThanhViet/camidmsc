package com.metfone.selfcare.module.keeng.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.text.ClipboardManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.App;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

@SuppressWarnings("deprecation")
@SuppressLint("DefaultLocale")
public class Utilities {
    private static final String TAG = "Utilities";

    public static void hideKeyboard(View focusingView, Context activity, String screenView) {
        boolean result = false;
        if (activity != null) {
            try {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (focusingView != null && imm != null) {
                    result = imm.hideSoftInputFromWindow(focusingView.getWindowToken(), 0);
                } else {
                    if (activity instanceof Activity) {
                        focusingView = ((Activity) activity).getCurrentFocus();
                    }
                    if (focusingView != null && imm != null)
                        result = imm.hideSoftInputFromWindow(focusingView.getWindowToken(), 0);
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        Log.i(TAG, "hideKeyboard " + result + " - " + screenView);
    }

    public static void showKeyboard(View focusingView, Context activity, String screenView) {
        boolean result = false;
        if (activity != null && focusingView != null) {
            focusingView.requestFocus();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            result = imm != null && imm.showSoftInput(focusingView, InputMethodManager.SHOW_IMPLICIT);
        }
        Log.i(TAG, "showKeyboard " + result + " - " + screenView);
    }

    public static String fixPhoneNumb(String str) {
        if (TextUtils.isEmpty(str) || str.length() < 3)
            return "";
        str = str.replaceAll("[^0-9]", "");
        if (str.startsWith(Constants.KEENG_LOCAL_CODE)) {
//            if (App.isCambodia())
//                str = str.substring(3);
//            else
            str = str.substring(2);
        } else if (str.startsWith("0")) {
            str = str.substring(1);
        }
        if (str.length() < 4)
            return "";
        return str.trim();
    }

    public static String fixPhoneNumbTo0(String str) {
        String msisdn = fixPhoneNumb(str);
        if (!TextUtils.isEmpty(msisdn))
            return "0" + msisdn;
        return "";
    }

    public static String fixPhoneNumbToLocalCode(String str) {
        String msisdn = fixPhoneNumb(str);
        if (!TextUtils.isEmpty(msisdn))
            return Constants.KEENG_LOCAL_CODE + msisdn;
        return "";
    }

    public static String getContentFile(Context ctx, String filename) {
        String line;
        String result = "";
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(ctx.getAssets().open(filename));
            BufferedReader in = new BufferedReader(inputStreamReader);
            while ((line = in.readLine()) != null) {
                result = result + line;
            }
        } catch (IOException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        } finally {
            if (inputStreamReader != null)
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                    Log.e(TAG, e);
                }
        }
        return result;
    }

    public static boolean isFileExistsInSD(String sFileName) {
        if (TextUtils.isEmpty(sFileName))
            return false;
        File file = new File(sFileName);
        return file.exists();
    }

    public static boolean isContactExists(Context context, String number) {
        try {
            Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
            String[] mPhoneNumberProjection = {PhoneLookup._ID, PhoneLookup.NUMBER};
            Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
            if (cur != null && cur.getCount() > 0) {
                return true;
            }
        } catch (SecurityException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return false;
    }

    /**
     * Xu ly khi muon vao URL
     *
     * @param ctx
     */
    public static void gotoUrl(Context ctx, String url) {
        try {
            if (!TextUtils.isEmpty(url)) {
                Intent intent;
                Log.i(TAG, "gotoUrl: " + url);
                url = url.toLowerCase();
                if (!url.startsWith("http")) {
                    url = "http://" + url;
                }
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static String getLanguageCode() {
        if (BuildConfig.IS_TIMOR) {
            return ApplicationController.self().getReengAccountBusiness().getSettingLanguage();
        }
        return ApplicationController.self().getReengAccountBusiness().getCurrentLanguage();
    }

    public static String getCountryCode() {
        return ApplicationController.self().getReengAccountBusiness().getRegionCode();
    }

    public static int getVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    public static String getVersionApp() {
        return BuildConfig.VERSION_NAME;
    }

    private static String getMovieIdFromLink(String urlTmp) {
        String unique_id = "";
        try {
            if (!TextUtils.isEmpty(urlTmp)) {
                int index = urlTmp.indexOf(".html");
                for (int i = index; i >= 0; i--) {
                    if ('f' == urlTmp.charAt(i)) {
                        unique_id = urlTmp.substring(i + 1, index);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        Log.i(TAG, "getMovieIdFromLink url: " + urlTmp + "\n uniqueId: " + unique_id);
        return unique_id;
    }

    public static void gotoSMS(Context context, String number, String smstext) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", smstext);
        context.startActivity(intent);
    }

    public static int getMeasurement(int measureSpec, int contentSize) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        int resultSize = 0;
        switch (specMode) {
            case View.MeasureSpec.UNSPECIFIED:
                // Big as we want to be
                resultSize = contentSize;
                break;
            case View.MeasureSpec.AT_MOST:
                // Big as we want to be, up to the spec
                resultSize = Math.min(contentSize, specSize);
                break;
            case View.MeasureSpec.EXACTLY:
                // Must be the spec size
                resultSize = specSize;
                break;
        }

        return resultSize;
    }

    /**
     * Convert dpi to pixels
     */
    public static int dpToPx(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = Math.round(dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    /**
     * Convert pixels to dpi
     */
    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                try {
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                } catch (SecurityException e) {
                    Log.e(TAG, e);
                } catch (Exception e) {
                    Log.e(TAG, e);
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } catch (SecurityException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static final void openSendSms(Activity activity, String body, String address) {
        try {
            Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
            smsIntent.addCategory(Intent.CATEGORY_DEFAULT);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("sms_body", body);
            smsIntent.setData(Uri.parse("sms:" + address));
            activity.startActivity(smsIntent);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static int getDensity(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int densityDpi = (int) (metrics.density * 160f);
        return densityDpi;
    }

    public static String formatNumber(long number) {
        if (number > 0) {
            return format(number);
        }
        return "";
    }

    public static String format(long value) {
        NavigableMap<Long, String> suffixes = new TreeMap<>();
        suffixes.put(1_000L, "K");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "B");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");

        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + format(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public static void goToLinkMocha(Context context) {
//        if (context == null)
//            return;
//        String msisdn = LoginObject.getPhoneNumber(context);
//        String url = Constants.MOCHA_LINK_INSTALL;
//        if (!TextUtils.isEmpty(msisdn))
//            url += "?ref=" + msisdn;
//
//        gotoUrl(context, url);
    }

    public static String hidenPhoneNumber(String msisdn) {
        String sdt = Utilities.fixPhoneNumbTo0(msisdn);
        if (TextUtils.isEmpty(sdt)) {
            return "";
        }
        try {
            int length = sdt.length();
            sdt = sdt.substring(0, 2) + "******" + sdt.substring(length - 2, length);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return sdt;
    }

    public static String getUsername(String name, String msisdn) {
        name = name == null ? "" : name;
        name = Html.fromHtml(name).toString();
        if (TextUtils.isEmpty(name)) {
            return hidenPhoneNumber(msisdn);
        }
        if (TextUtils.isDigitsOnly(name)) {
            String sdt = fixPhoneNumb(msisdn);
            String name2 = fixPhoneNumb(name);
            if (sdt.equals(name2)) {
                return hidenPhoneNumber(msisdn);
            }
        }
        return name;
    }

    public static int getTextPositionNice(String text, float line) {
        String str = "";
        Character strNext;
        int length = 40;
        int index = (int) (length * line);
        int count = 0;
        try {
            while (true) {
                if (text.length() <= index) {
                    return text.length();
                }
                str = text.substring(0, index);
                if (text.length() <= str.length()) {
                    return text.length();
                }
                strNext = text.charAt(str.length());
                if (strNext == ' ') {
                    return str.length() + 1;
                }
                index++;
                count++;
                if (count == 6) {
                    return index;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e);
            return index;
        }
    }

    public static void copyToClipBroad(Context context, String content) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setText(content);
    }

    public static DataSource.Factory buildDataSourceFactory(Context context, DefaultBandwidthMeter bandwidthMeter) {
        String userAgent = BuildConfig.APPLICATION_ID + "/" + BuildConfig.VERSION_NAME
                + " (Linux;Android " + Build.VERSION.RELEASE
                + ") " + ExoPlayerLibraryInfo.VERSION_SLASHY;
        return new DefaultDataSourceFactory(context, bandwidthMeter, new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
    }

    public static void runPeriodicService(Context context, Class service, int seconds) {
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(context, service);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), seconds * 1000, pendingIntent);
    }

    public static void cancelPeriodicService(Context context, Class service) {
        Intent intent = new Intent(context, service);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pendingIntent);
    }

    public static String getDomainFromLink(String link) {
        String domain = "";
        try {
            URL url = new URL(link);
            domain = url.getProtocol() + "://" + url.getHost();
//            System.out.println("domain: " + domain);
//            domain = url.getFile();
//            System.out.println("getFile: " + domain);
//            domain = url.getAuthority();
//            System.out.println("getAuthority: " + domain);
//            domain = url.getHost();
//            System.out.println("getHost: " + domain);
//            domain = url.getPath();
//            System.out.println("getPath: " + domain);
//            domain = url.getProtocol();
//            System.out.println("getProtocol: " + domain);
//            domain = url.getQuery();
//            System.out.println("getQuery: " + domain);
//            domain = url.getRef();
//            System.out.println("getRef: " + domain);
//            domain = url.getUserInfo();
//            System.out.println("getUserInfo: " + domain);
//            domain = "" + url.getPort();
//            System.out.println("getPort: " + domain);
//            domain = "" + url.getContent();
//            System.out.println("getContent: " + domain);
        } catch (MalformedURLException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return domain;
    }

    public static void restartApplication(Context context) {
        if (context == null) return;
        try {
            Intent intent;
            PackageManager manager = context.getPackageManager();
            intent = manager.getLaunchIntentForPackage(BuildConfig.APPLICATION_ID);
            assert intent != null;
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            PendingIntent pendingIntent = PendingIntent.getActivity(
                    App.getInstance().getBaseContext(), 0, intent, intent.getFlags());
            AlarmManager mgr = (AlarmManager) App.getInstance().getBaseContext()
                    .getSystemService(Context.ALARM_SERVICE);
            assert mgr != null;
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, pendingIntent);
            System.runFinalizersOnExit(true);
            System.exit(0);
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String encrypt512(String plaintext) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, e);
        } catch (NoClassDefFoundError e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        if (md == null) {
            return "";
        }
        try {
            md.update(plaintext.getBytes(StandardCharsets.UTF_8));
        } catch (NoClassDefFoundError e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        byte[] raw = md.digest();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < raw.length; i++) {
            sb.append(Integer.toString((raw[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    public static void gotoMain(Activity activity) {
//        try {
//            Intent intent = new Intent(activity, MainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
//                    | Intent.FLAG_ACTIVITY_NEW_TASK);
//            activity.startActivity(intent);
//            activity.finish();
//        } catch (Exception e) {
//            Log.e(TAG, e);
//        }
    }

    public static void configureBottomSheetBehavior(View contentView) {
        try {
            BottomSheetBehavior mBottomSheetBehavior = BottomSheetBehavior.from((View) contentView.getParent());
            if (mBottomSheetBehavior != null) {
                mBottomSheetBehavior.setPeekHeight(2000);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public static void gotoHomeDevice(Context context) {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startMain);
    }

    public static String getChannelId(Context context) {
        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = BuildConfig.APPLICATION_ID;
            NotificationChannel notificationChannel = new NotificationChannel(channelId, context.getString(R.string.app_name)
                    , NotificationManager.IMPORTANCE_NONE);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
        return channelId;
    }

    public static String hidenText(String text) {
        String result = text;
        if (TextUtils.isEmpty(text)) {
            return "";
        }
        try {
            int length = text.length();
            if (length > 4)
                result = text.substring(0, 2) + "***" + text.substring(length - 2, length);
            else
                result = "***";
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return result;
    }

    public static String encrypt256KQI(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, e);
        } catch (NoClassDefFoundError e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return "";
    }

    public static void openVideoYoutube(Context context, String linkYoutube) {
        Log.i(TAG, "openVideoYoutube " + linkYoutube);
        if (!TextUtils.isEmpty(linkYoutube)) {
            try {
                String videoId = getIdYoutube(linkYoutube);
                Intent intent;
                if (!TextUtils.isEmpty(videoId)) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://" + videoId));
                    if (intent.resolveActivity(context.getPackageManager()) == null) {
                        // If the youtube app doesn't exist, then use the browser
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + videoId));
                    }
                } else
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkYoutube));
                context.startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    public static String getIdYoutube(String linkYoutube) {
        String idYoutube = "";
        if (linkYoutube != null) {
            String[] tmp = linkYoutube.split("v=");
            if (tmp.length > 1) {
                idYoutube = tmp[1];
                if (idYoutube != null) {
                    int ampersandPosition = idYoutube.indexOf('&');
                    if (ampersandPosition > 0)
                        idYoutube = idYoutube.substring(0, ampersandPosition);
                } else {
                    idYoutube = "";
                }
            } else if (linkYoutube.contains("embed")) {
                idYoutube = linkYoutube.substring(linkYoutube.lastIndexOf("/") + 1);
            }
        }
        return idYoutube;
    }

    public static String getRandomList(List<String> list) {
        Random random = new Random();
        int index = random.nextInt(list.size());
        return list.get(index);
    }

    public static int getWidthPosterMovie() {
        return (int) ((ApplicationController.self().getWidthPixels() - ApplicationController.self().getMargin() * 3) / 3.4);
    }
}
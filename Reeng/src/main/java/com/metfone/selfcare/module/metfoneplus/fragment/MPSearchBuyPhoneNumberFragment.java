package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import butterknife.OnClick;

public class MPSearchBuyPhoneNumberFragment extends MPBaseFragment {

        public MPSearchBuyPhoneNumberFragment() {
            // Required empty public constructor
        }

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment MPSupportFragment.
         */
        public static MPSupportFragment newInstance() {
            MPSupportFragment fragment = new MPSupportFragment();
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

        }

        @Override
        protected int getLayoutId() {
            return R.layout.fragment_m_p_support;
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
//            Utilities.adaptViewForInserts(mLayoutActionBar);
//            mActionBarTitle.setText(getString(R.string.m_p_support_title));
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
        }

        @OnClick(R.id.action_bar_back)
        public void onBack() {
            if(mParentActivity instanceof HomeActivity){
                ((HomeActivity)mParentActivity).setBottomNavigationBarColor(R.color.m_home_tab_background_2, true);
            }
            popBackStackFragment();
        }
}

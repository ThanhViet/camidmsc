package com.metfone.selfcare.module.keeng.fragment.player;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.module.keeng.KeengPlayerActivity;
import com.metfone.selfcare.module.keeng.adapter.player.RelatePlayerAdapter;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.CategoryModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Utilities;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DialogMediaInfo;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomDialog;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomSheetAdapter;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomSheetData;

import java.util.ArrayList;
import java.util.List;

public class RelatePlayerFragment extends BottomSheetDialogFragment implements BaseListener.OnClickMedia {
    public static final String TAG = RelatePlayerFragment.class.getSimpleName();

    private RelatePlayerAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView recyclerView;
    private ArrayList<MediaModel> listSongs = new ArrayList<>();
    private int currentIndex = 0;
    private String currentPlaylist;
    private BottomDialog bottomDialog;
    private KeengPlayerActivity keengPlayerActivity;
    private TextView tvCount, tvTitle;
    private ImageView ivBack;

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof KeengPlayerActivity)
            keengPlayerActivity = (KeengPlayerActivity) activity;
    }


    public static RelatePlayerFragment newInstance() {
        return new RelatePlayerFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_play_music, container, false);
        if (view == null) return null;
        recyclerView = view.findViewById(R.id.recycler_view);
        tvCount = view.findViewById(R.id.tv_count);
        tvTitle = view.findViewById(R.id.tv_tab_name);
        ivBack = view.findViewById(R.id.iv_back);
        setupAdapter();
        setupView();
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialogInterface -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
            setupFullHeight(bottomSheetDialog);
        });
        return dialog;
    }


    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        if (bottomSheet == null) return;
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = ScreenManager.getHeight(getActivity());
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    private void setupAdapter() {
        mLayoutManager = new CustomLinearLayoutManager(getActivity());
        adapter = new RelatePlayerAdapter(getContext(), "");
        adapter.setOnclickListener(this);
        adapter.setData(listSongs);
        adapter.setCurrentIndex(currentIndex);

        recyclerView.setPadding(0, Utilities.dpToPx(keengPlayerActivity, 8), 0, 0);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setBackgroundColor(getResources().getColor(R.color.transparent));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    private void setupView() {
        setupListCount();
        if (tvTitle != null) {
            tvTitle.setText(currentPlaylist);
        }
        if (ivBack != null) {
            ivBack.setOnClickListener(v -> dismiss());
        }
    }

    private void setupListCount() {
        if (tvCount != null) {
            if (listSongs.size() == 0) {
                tvCount.setVisibility(View.GONE);
                tvCount.setText("");
            } else {
                tvCount.setVisibility(View.VISIBLE);
                if (listSongs.size() == 1)
                    tvCount.setText(getString(R.string.music_total_song, listSongs.size()));
                else
                    tvCount.setText(getString(R.string.music_total_songs, listSongs.size()));
            }
        }
    }

    public void updateData(PlayMusicController playMusicController) {
        listSongs = playMusicController.getDataSong();
        currentIndex = playMusicController.getCurrentSongIndex();
        currentPlaylist = playMusicController.getCurrentPlayListName();
        playMusicController.getCurrentPlayListName();
        if (adapter != null) {
            adapter.setData(listSongs);
            adapter.setCurrentIndex(currentIndex);
            adapter.notifyDataSetChanged();
        }
        if (mLayoutManager != null) {
            //Set vi tri play len dau
            mLayoutManager.scrollToPosition(currentIndex);
        }
        setupListCount();
    }

    public void showPopupMore(final int index) {
        if (bottomDialog != null)
            bottomDialog.dismiss();

        AllModel item = convertMochaToKeengModel(listSongs.get(index));
        List<CategoryModel> datas = BottomSheetData.getPopupOfSong(item, false, false, false, false, true);

        if (datas.isEmpty())
            return;
        bottomDialog = new BottomDialog(keengPlayerActivity);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_song, null);
        TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
        TextView tvSinger = sheetView.findViewById(R.id.tvSinger);
        ImageView image = sheetView.findViewById(R.id.image);
        RecyclerView mRecycler = sheetView.findViewById(R.id.recycler);

        tvTitle.setText(item.getName());
        tvSinger.setText(item.getSinger());
        ImageBusiness.setSong(image, item.getImage(), item.getId(), ApplicationController.self().getRound());

        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new CustomLinearLayoutManager(keengPlayerActivity));
        BottomSheetAdapter adapter = new BottomSheetAdapter(keengPlayerActivity, datas, TAG, item1 -> onClickBottomSheet(item1, index));
        mRecycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        bottomDialog.setContentView(sheetView);
        bottomDialog.show();
    }

    private void onClickBottomSheet(CategoryModel item, int index) {
        if (item == null) {
            return;
        }
        if (bottomDialog != null)
            bottomDialog.dismiss();
        switch (item.getType()) {
            case BottomSheetData.SHARE:
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                    keengPlayerActivity.showDialogLogin();
                else {
                    if (keengPlayerActivity != null && item.getMedia() != null)
                        keengPlayerActivity.clickPopupShareMedia(item.getMedia());
                }
                break;

            case BottomSheetData.REMOVE_PLAYING:
                if (keengPlayerActivity != null)
                    keengPlayerActivity.removeSongPlaying(index);
                break;
            case BottomSheetData.SHOW_SONG_INFO:
                if (keengPlayerActivity != null && item.getMedia() != null) {
                    DialogMediaInfo dialog = new DialogMediaInfo(keengPlayerActivity, item.getMedia());
                    dialog.show();
                }
                break;
        }
    }

    public static AllModel convertMochaToKeengModel(MediaModel item) {
        AllModel model = new AllModel();
        model.setId(Long.parseLong(item.getId()));
        model.setMediaUrl(item.getMedia_url());
        model.setType(MediaModel.TYPE_BAI_HAT);
        model.setImage(item.getImage());
        model.setSinger(item.getSinger());
        model.setName(item.getName());
        model.setUrl(item.getUrl());
        model.setListened(item.getListened());
        model.setLyricUrl(item.getLyricUrl());
        model.setLyric(item.getLyric());

        return model;
    }

    @Override
    public void onMediaClick(View view, int position) {
        if (keengPlayerActivity != null)
            keengPlayerActivity.playSong(position);
    }

    @Override
    public void onMediaExpandClick(View view, int position) {
        showPopupMore(position);
    }

    @Override
    public void onMediaClick(View view, int parent, int position) {
    }

    @Override
    public void onMediaExpandClick(View view, int parent, int position) {
    }

    @Override
    public void onLongClick(View view, int position) {
    }

    @Override
    public void onTopicClick(View view, int position) {
    }

    @Override
    public void onScrollEnd(boolean flag) {
    }
}

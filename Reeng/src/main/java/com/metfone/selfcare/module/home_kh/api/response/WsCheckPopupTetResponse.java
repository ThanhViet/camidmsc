package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WsCheckPopupTetResponse {
    @SerializedName("errorCode")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("userMsg")
    @Expose
    String userMsg;

    @SerializedName("wsResponse")
    @Expose
    WsResponse wsResponse;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public WsResponse getWsResponse() {
        return wsResponse;
    }

    public void setWsResponse(WsResponse wsRequest) {
        this.wsResponse = wsRequest;
    }

    public class WsResponse {
        @SerializedName("isShowPopupTet")
        boolean isShowPopupTet;

        public boolean isShowPopupTet() {
            return isShowPopupTet;
        }

        public void setShowPopupTet(boolean showPopupTet) {
            isShowPopupTet = showPopupTet;
        }
    }
}

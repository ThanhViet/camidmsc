/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.content.MovieDetailHolder;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.module.movie.holder.HorizontalRecyclerHolder;
import com.metfone.selfcare.module.movie.listener.LoadmoreListener;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.util.Log;

public class BoxContentAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    private TabMovieListener.OnAdapterClick listener;
    private int parentViewType;
    private LoadmoreListener.IStartLoadmoreListener mStartLoadmoreListener;
    private boolean canLoadmore = true;
    private String mBlockName;
    private int mPageLoaded;

    public BoxContentAdapter(Activity act, int viewType, LoadmoreListener.IStartLoadmoreListener startLoadmoreListener) {
        super(act);
        this.parentViewType = viewType;
        this.mPageLoaded = 0;
        this.mStartLoadmoreListener = startLoadmoreListener;
    }

    public int getPageLoaded() {
        return mPageLoaded;
    }

    public void setPageLoaded(int pageLoaded) {
        this.mPageLoaded = pageLoaded;
    }

    public void increasePageLoaded() {
        this.mPageLoaded ++;
    }

    public void setBlockName(String blockName) {
        this.mBlockName = blockName;
    }

    public String getBlockName() {
        return mBlockName;
    }

    public void setCanLoadmore(boolean canLoadmore) {
        this.canLoadmore = canLoadmore;
    }

    public void setListener(TabMovieListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof MoviePagerModel) {
            return ((MoviePagerModel) item).getType();
        } else if (item instanceof MovieWatched) {
            return MoviePagerModel.TYPE_WATCHED_ITEM;
        } else if (item instanceof Movie) {
            return MoviePagerModel.TYPE_LARGE_ITEM;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MoviePagerModel.TYPE_BOX_GRID_ITEM) {
            return new HorizontalRecyclerHolder(layoutInflater.inflate(R.layout.holder_recycler_view, parent, false), activity, listener, parentViewType, new LoadmoreListener.IStartLoadmoreListener() {
                @Override
                public void startLoadmore() {
                    if (canLoadmore) {
                        mStartLoadmoreListener.startLoadmore();
                    }
                }
            });
        } else if (viewType == MoviePagerModel.TYPE_LARGE_ITEM) {
            return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_large_movie, parent, false), activity, listener).setShowPoster(false);
        } else if (viewType == MoviePagerModel.TYPE_GRID_ITEM) {
            return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_grid_movie, parent, false), activity, listener).setShowPoster(true);
        } else if (viewType == MoviePagerModel.TYPE_WATCHED_ITEM) {
            return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_grid_movie_watched, parent, false), activity, listener, TabHomeUtils.getWidthMovieWatched()).setShowPoster(false);
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        if (item instanceof MoviePagerModel) {
            ((MoviePagerModel) item).setTitle(mBlockName);
        }
        holder.bindData(item, position);
    }
}

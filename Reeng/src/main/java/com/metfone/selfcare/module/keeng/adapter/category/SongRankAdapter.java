package com.metfone.selfcare.module.keeng.adapter.category;

import android.content.Context;

import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.holder.MediaHolder;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class SongRankAdapter extends BaseAdapterRecyclerView {
    protected List<AllModel> datas;

    public SongRankAdapter(Context context, List<AllModel> datas, String ga_source) {
        super(context, ga_source);
        this.datas = datas;
    }

    @Override
    public int getItemCount() {
        if (datas != null && datas.size() > 0)
            return datas.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        AllModel item = getItem(position);
        if (item == null) {
            return ITEM_LOAD_MORE;
        }
        switch (item.getType()) {
            case Constants.TYPE_SONG:
            case Constants.TYPE_SONG_UPLOAD:
                return ITEM_MEDIA_SONG_RANK;
            case Constants.TYPE_ALBUM:
                return ITEM_MEDIA_ALBUM;
            case Constants.TYPE_VIDEO:
            case Constants.TYPE_VIDEO_UPLOAD:
                return ITEM_MEDIA_VIDEO_RANK;
            case Constants.TYPE_PLAYLIST:
                return ITEM_MEDIA_ALBUM;
            default:
                break;
        }
        return ITEM_EMPTY;
    }

    @Override
    public AllModel getItem(int position) {
        if (datas != null)
            try {
                return datas.get(position);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (holder instanceof MediaHolder) {
            AllModel item = getItem(position);
            MediaHolder itemHolder = (MediaHolder) holder;
            itemHolder.bind(mContext, item);
        }
    }
}

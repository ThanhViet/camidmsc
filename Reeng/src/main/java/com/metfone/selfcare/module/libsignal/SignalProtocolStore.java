package com.metfone.selfcare.module.libsignal;

import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.InvalidKeyIdException;
import org.whispersystems.libsignal.state.PreKeyRecord;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.libsignal.state.impl.InMemorySignalProtocolStore;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SignalProtocolStore extends InMemorySignalProtocolStore {

    public SignalProtocolStore(IdentityKeyPair identityKeyPair, int registrationId) {
        super(identityKeyPair, registrationId);
    }

    public static SignalProtocolStore from(int pPreKeyId, int pSignedPreKeyId, byte[] serialize)
            throws IOException, InvalidKeyException {
        SignalProtocolStore store = null;
        DataInputStream in = null;
        try {
            in = new DataInputStream(new ByteArrayInputStream(serialize));
            int dataLength = in.readInt();
            byte[] pIdentityKeyPairAsBytes = new byte[dataLength];
            in.read(pIdentityKeyPairAsBytes, 0, dataLength);

            int pRegistrationId = in.readInt();

            dataLength = in.readInt();
            byte[] pPreKeyRecordAsBytes = new byte[dataLength];
            in.read(pPreKeyRecordAsBytes, 0, dataLength);

            dataLength = in.readInt();
            byte[] pSignedPreKeyRecordAsBytes = new byte[dataLength];
            in.read(pSignedPreKeyRecordAsBytes, 0, dataLength);

            IdentityKeyPair pIdentityKeyPair = new IdentityKeyPair(pIdentityKeyPairAsBytes);
            PreKeyRecord pPreKeyRecord = new PreKeyRecord(pPreKeyRecordAsBytes);

            SignedPreKeyRecord pSignedPreKeyRecord = new SignedPreKeyRecord(pSignedPreKeyRecordAsBytes);

            store = new SignalProtocolStore(pIdentityKeyPair, pRegistrationId);

            store.storePreKey(pPreKeyId, pPreKeyRecord);
            store.storeSignedPreKey(pSignedPreKeyId, pSignedPreKeyRecord);
        } catch (IOException e) {
            throw e;
        } catch (InvalidKeyException ie) {
            throw ie;
        } finally {
            if (in != null) {
                in.close();
            }
        }

        return store;
    }

    public byte[] serialize(int pPreKeyId, int pSignedPreKeyId) throws InvalidKeyIdException, IOException {
        /* STEP 1: GETTER */
        IdentityKeyPair pIdentityKeyPair = this.getIdentityKeyPair();
        int pRegistrationId = this.getLocalRegistrationId();
        PreKeyRecord pPreKeyRecord = this.loadPreKey(pPreKeyId);
        SignedPreKeyRecord pSignedPreKeyRecord = this.loadSignedPreKey(pSignedPreKeyId);

        /* STEP 2: CONVERT TO BYTES */
        byte[] pIdentityKeyPairAsBytes = pIdentityKeyPair.serialize();
        byte[] pPreKeyRecordAsBytes = pPreKeyRecord.serialize();
        byte[] pSignedPreKeyRecordAsBytes = pSignedPreKeyRecord.serialize();

        // LENGTH_4BYTE|DATA_IDENTITY|DATA_REGISTRATION_ID|LENGTH_4BYTE|DATA_PREKEY_RECORD|LENGTH_4BYTE|DATA_SIGNED_PREKEY_RECORD
        ByteArrayOutputStream baos = null;
        DataOutputStream out = null;
        byte[] data = null;
        try {
            baos = new ByteArrayOutputStream();
            out = new DataOutputStream(baos);
            out.writeInt(pIdentityKeyPairAsBytes.length);
            out.write(pIdentityKeyPairAsBytes);

            out.writeInt(pRegistrationId);

            out.writeInt(pPreKeyRecordAsBytes.length);
            out.write(pPreKeyRecordAsBytes);

            out.writeInt(pSignedPreKeyRecordAsBytes.length);
            out.write(pSignedPreKeyRecordAsBytes);

            data = baos.toByteArray();
        } catch (IOException e) {
            throw e;
        } finally {
            if (baos != null) {
                baos.close();
            }
            if (out != null) {
                out.close();
            }
        }
        return data;
    }

}

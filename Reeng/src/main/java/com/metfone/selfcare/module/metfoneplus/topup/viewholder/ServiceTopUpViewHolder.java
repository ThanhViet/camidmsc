package com.metfone.selfcare.module.metfoneplus.topup.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.metfoneplus.topup.model.ServiceTopUpModel;

import java.util.List;

public class ServiceTopUpViewHolder extends BaseViewHolder {
    private EventListener listener;
    public ServiceTopUpViewHolder(@NonNull View itemView, EventListener listener) {
        super(itemView, null);
        this.listener = listener;
    }

    private ImageView imgService;
    private ConstraintLayout viewRoot;

    @Override
    public void initViewHolder(View v) {
        if (mData == null || mData.size() == 0) return;
        if (mData.get(0) instanceof ServiceTopUpModel) {
            imgService = v.findViewById(R.id.imgService);
            viewRoot = v.findViewById(R.id.viewRoot);
        }
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        if (mData.get(pos) instanceof ServiceTopUpModel) {
            ServiceTopUpModel serviceTopUpModel = (ServiceTopUpModel) mData.get(pos);
            int imgResource = serviceTopUpModel.getImg();
            Glide.with(context)
                    .load(imgResource)
                    .into(imgService);
//            viewRoot.setBackgroundResource(serviceTopUpModel.isSelect() ? R.drawable.bg_select_item_service_topup : R.drawable.bg_boder_while);

        }
    }

    @Override
    public void onItemViewClick(View v, int pos) {
       if(listener != null){
           listener.onServiceTopUpClicked((ServiceTopUpModel) mData.get(pos));
       }
    }

    public interface EventListener{
        void onServiceTopUpClicked(ServiceTopUpModel selectedItem);
    }
}

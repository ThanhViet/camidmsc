package com.metfone.selfcare.module.metfoneplus.ishare.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.NewEditProfileActivity;
import com.metfone.selfcare.activity.SetYourNewPasswordActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.base.NumberTextWatcher;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogOtpFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogOtpIshareFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPOrderNumberDialog;
import com.metfone.selfcare.module.metfoneplus.ishare.adapter.IshareContactAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.metfoneplus.topup.model.AmountIshareMetfoneModel;
import com.metfone.selfcare.module.metfoneplus.topup.model.AmountMetfoneModel;
import com.metfone.selfcare.module.metfoneplus.topup.viewholder.AmountIshareViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.viewholder.AmountViewHolder;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsLockIsdnToBuyResponse;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class IShareFragment extends MPBaseFragment implements ItemViewClickListener, View.OnClickListener {
    private int PICK_CONTACT = 97;
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.tv_phone)
    EditText tv_phone_number;
    @BindView(R.id.tv_amount)
    EditText edit_amount;
    @BindView(R.id.btn_contacts)
    TextView btn_contacts;
    @BindView(R.id.amount_money)
    TextView amount_money;
    @BindView(R.id.img_clear)
    ImageView btn_clear;
    @BindView(R.id.recycler_amount)
    RecyclerView recycler_amount;
    @BindView(R.id.recycler_contact)
    RecyclerView recycler_contact;
    @BindView(R.id.btn_next)
    Button btn_next;
    @BindView(R.id.lnParent)

    LinearLayout lnParent;
    private String mAmount;
    private boolean checkAmount = false;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private String current = "";
    private boolean isAllowContactPermission;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_ishare;
    }

    public static IShareFragment newInstance() {
        Bundle args = new Bundle();
        IShareFragment fragment = new IShareFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private BaseAdapter amountAdapter;
    private IshareContactAdapter contactAdapter;
    private ArrayList<AmountIshareMetfoneModel> amountMetfoneModels = new ArrayList<>();
    private static final int ZXING_CAMERA_PERMISSION = 1;

    private ContactBusiness contactBusiness;
    private ArrayList<PhoneNumber> phoneNumbers;
    private ArrayList<PhoneNumber> listPhoneNumber;
    public static CamIdUserBusiness mCamIdUserBusiness = null;
    private int countPhone = 0;
    private MPDialogOtpIshareFragment dialogSuccess;
    private boolean isOTPDialogShow = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        contactBusiness = mApplication.getContactBusiness();
        listPhoneNumber = new ArrayList<>();;
        List<PhoneNumber> mList = new ArrayList<>();
        mList = contactBusiness.getRecentPhoneNumber();
        if (mList != null) {
            listPhoneNumber = (ArrayList<PhoneNumber>) removePhoneRecentDuplicates(mList);
        }
        updateBottomMenuColor(R.color.m_home_tab_background);
        isAllowContactPermission = PermissionHelper.checkPermissionContact(this);

    }

    public List<PhoneNumber>  removePhoneRecentDuplicates(List<PhoneNumber> list){
        Set<PhoneNumber> set = new TreeSet(new Comparator<PhoneNumber>() {

            @Override
            public int compare(PhoneNumber o1, PhoneNumber o2) {
                if(o1.getJidNumber().equalsIgnoreCase(o2.getJidNumber())){
                    return 0;
                }
                return 1;
            }
        });
        set.addAll(list);

        final ArrayList newList = new ArrayList(set);
        return newList;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(view);
        initData();
        initViews();
        initListener();
    }
    private void initListener() {
//        mBinding.btnNext.setOnClickListener(this);
//        mBinding.btnBack.setOnClickListener(this);
//        mBinding.btnQrCode.setOnClickListener(this);
//        mBinding.btnHistory.setOnClickListener(this);
//        mBinding.btnContacts.setOnClickListener(this);
        //------------------------------- EVENT EditTExt Change-------------------------------



    }

    private void setSateViewSelect(int position) {
        for (AmountIshareMetfoneModel amountMetfoneModel : amountMetfoneModels) {
            amountMetfoneModel.setCheckSelect(false);
        }
        if (position == -1) return;
        amountMetfoneModels.get(position).setCheckSelect(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        if (dialogSuccess != null && !dialogSuccess.isVisible()) {
            isOTPDialogShow = true;
        }
        super.onAttach(activity);
    }

    @Override
    public void onStart() {
        if (isOTPDialogShow) {
            dialogSuccess.setIsOTPDialogShow(isOTPDialogShow);
            dialogSuccess.show(getChildFragmentManager(), null);
            isOTPDialogShow = false;
        }
        super.onStart();
    }

    @Override
    public void onPause() {
        if (dialogSuccess != null && !dialogSuccess.isVisible()) {
            isOTPDialogShow = true;
        }
        super.onPause();
    }

    private void initViews() {
        action_bar_title.setText(R.string.toolbar_ishare);
        ArrayList<PhoneNumber> lstPhone = new ArrayList<>();
        ArrayList<PhoneNumber> lstPhoneContact = new ArrayList<>();
//        if (contactBusiness != null && contactBusiness.getListContacs().size() > 0) {
        if (contactBusiness != null && contactBusiness.getListContacs() != null && contactBusiness.getListContacs().size() > 0) {
            for (int i = 0; i < contactBusiness.getListContacs().size() ; i++) {
                if (    contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85523") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85531") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85560") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85566") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85567") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85568") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85571") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85588") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85590") ||
                        contactBusiness.getListContacs().get(i).getListNumbers().get(0).getJidNumber().substring(0, 6).equals("+85597")) {
                    lstPhoneContact.add(contactBusiness.getListContacs().get(i).getListNumbers().get(0));
                }
            }
        }
        if (listPhoneNumber != null && listPhoneNumber.size() > 0) {
            for (int i = 0; i < listPhoneNumber.size() ; i++) {
                if (    listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85523") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85531") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85560") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85566") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85567") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85568") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85571") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85588") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85590") ||
                        listPhoneNumber.get(i).getJidNumber().substring(0, 6).equals("+85597")) {
                    lstPhone.add(listPhoneNumber.get(i));
                }
            }
        }
        if (lstPhone.size() > 0 || lstPhoneContact.size() > 0) {
            for (int i = 0; i< listPhoneNumber.size(); i++) {
                for (int j = 0; j< lstPhoneContact.size(); j++) {
                    if(listPhoneNumber.get(i).getJidNumber().equalsIgnoreCase(lstPhoneContact.get(j).getJidNumber())){
                        lstPhoneContact.remove(j);
                    }
                }
            }
            for (int i = 0; i < lstPhoneContact.size() ; i++) {
                lstPhone.add(lstPhoneContact.get(i));
            }
            if (lstPhone != null) {
                phoneNumbers = lstPhone;
            }
        }
        setupUI(lnParent);
        edit_amount.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        AmountIshareViewHolder amountViewHolder = new AmountIshareViewHolder(getActivity().getWindow().getDecorView().getRootView(), this);
        amountAdapter = new BaseAdapter(amountMetfoneModels, getContext(), R.layout.item_amount, amountViewHolder);
        recycler_amount.setLayoutManager(new GridLayoutManager(getContext(), 4));
        recycler_amount.setAdapter(amountAdapter);
        contactAdapter = new IshareContactAdapter(getContext(), phoneNumbers, mApplication, new IshareContactAdapter.IShareContactListener() {
            @Override
            public void selectContact(int position, PhoneNumber phoneNumber) {
                String nationalPhone = Utilities.getNationalPhoneNumber(phoneNumber.getJidNumber(), "KH");
                tv_phone_number.setText(nationalPhone);
            }
        });
        recycler_contact.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recycler_contact.setAdapter(contactAdapter);
        //edit_amount.addTextChangedListener(new NumberTextWatcher(edit_amount));
        tv_phone_number.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String phone = "";
                String phoneZero = "";
                if (s.length() > 0) {
                   // phoneZero = String.valueOf(s.charAt(0));
                    if (s.toString().substring(0, 1).equals("0")){
                        phone = s.toString();
                    } else  if (s.length() >= 3 && s.toString().substring(0, 3).equals("855")){
                        phone = "0" + s.toString().substring(3);
                    } else {
                        phone = s.toString();
                    }
                }


                if (phoneNumbers != null && phoneNumbers.size() > 0) {
                    if (phone.length() > 8 ) {
                        btn_next.setEnabled(true);
                        for (int i = 0; i < phoneNumbers.size(); i++) {
                            if (phone.equals(phoneNumbers.get(i).getRawNumber())) {
                                phoneNumbers.get(i).setSectionType(124);
                            } else {
                                phoneNumbers.get(i).setSectionType(123);
                            }
                        }
                        contactAdapter.notifyDataSetChanged();
                    } else {
                        for (int i = 0; i < phoneNumbers.size(); i++) {
                            phoneNumbers.get(i).setSectionType(123);
                            contactAdapter.notifyDataSetChanged();
                        }
                    }
                }

                if (phone.length() >= 2 && (phone.toString().substring(0, 2).equals("60")
                        || phone.toString().substring(0, 2).equals("66")
                        || phone.toString().substring(0, 2).equals("67")
                        || phone.toString().substring(0, 2).equals("68")
                        || phone.toString().substring(0, 2).equals("90"))
                ) {
                    if (phone.length() >= 8 && checkAmount) {
                        btn_next.setEnabled(true);
                    } else {
                        btn_next.setEnabled(false);
                    }

                } else if (phone.length() > 8 && checkAmount){
                    btn_next.setEnabled(true);
                } else {
                    btn_next.setEnabled(false);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
        edit_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String valueChange = editable.toString().trim();
                edit_amount.setSelection(valueChange.length());
                if (!valueChange.isEmpty()) {
                    // kiểm tra input vs  list amout
                    for (int j = 0; j < amountMetfoneModels.size(); j++) {
                        if (valueChange.equals(amountMetfoneModels.get(j).getAmount() + "")) {
                            setSateViewSelect(j);
                            amountAdapter.updateAdapter(amountMetfoneModels);
                            break;
                        } else {
                            setSateViewSelect(-1);
                            amountAdapter.updateAdapter(amountMetfoneModels);
                        }
                    }
                } else {
                    setSateViewSelect(-1);
                    amountAdapter.updateAdapter(amountMetfoneModels);
                }
                if (editable.length() == 0) {
                    amount_money.setVisibility(View.GONE);
                    btn_clear.setVisibility(View.GONE);
                    checkAmount = false;
                    btn_next.setEnabled(false);
                } else {
                    checkAmount = true;
                    if (tv_phone_number.getText().length() > 8) {
                        btn_next.setEnabled(true);
                    } else if (tv_phone_number.getText().length() == 8 && (tv_phone_number.getText().toString().substring(0, 2).equals("60")
                            || tv_phone_number.getText().toString().substring(0, 2).equals("66")
                            || tv_phone_number.getText().toString().substring(0, 2).equals("67")
                            || tv_phone_number.getText().toString().substring(0, 2).equals("68")
                            || tv_phone_number.getText().toString().substring(0, 2).equals("90"))){
                        btn_next.setEnabled(true);
                    }
                    amount_money.setVisibility(View.VISIBLE);
                    btn_clear.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    public static String formatDecimal(String value) {
        DecimalFormat df = new DecimalFormat("##.00");
        return df.format(Double.valueOf(value));
    }
    @OnClick(R.id.action_bar_back)
    public void onBack() {
        if(mParentActivity instanceof HomeActivity){
            popBackStackFragment();
        }else{
            mParentActivity.finish();
        }
    }

    @Override
    public void popBackStackFragment() {
        super.popBackStackFragment();
        updateBottomMenuColor(R.color.m_home_tab_background);
    }

    private void initData() {
        amountMetfoneModels = AmountIshareMetfoneModel.createAmountValue();
//        currentUser = userInfoBusiness.getUser();
//        mContactTopup = mApplication.getCamIdUserBusiness().getContactTopup();
    }

    @OnClick({R.id.btn_contacts, R.id.img_clear, R.id.btn_next})
    public void handleClick(View view) {
        if (view.getId() == R.id.btn_contacts) {
            if(!isAllowContactPermission){
                if(PermissionHelper.onlyCheckPermissionContact(getContext())){
                    isAllowContactPermission = true;
                    mApplication.reLoadContactAfterPermissionsResult();
                }
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        } else if (view.getId() == R.id.img_clear) {
            edit_amount.setText("");
        } else if (view.getId() == R.id.btn_next) {
            String top_content = "";
            StringBuilder number = new StringBuilder();
            if (0.2 < Float.parseFloat(edit_amount.getText().toString()) && Float.parseFloat(edit_amount.getText().toString()) <= 10) {
                if (tv_phone_number.getText().toString().substring(0, 1).equals("0")){
                    String subPhoneFirst = tv_phone_number.getText().toString().substring(0, 3);
                    String subPhoneLast = tv_phone_number.getText().toString().substring(3);
                    number.append(subPhoneFirst)
                            .append(" ")
                            .append(subPhoneLast);
                    String phone = number.toString();
                    top_content = getString(R.string.do_you_want_tranfer, edit_amount.getText().toString() + "$", phone);
                } else  if (tv_phone_number.length() >= 3 && tv_phone_number.getText().toString().substring(0, 3).equals("855")){
                    String phoneNumber = "0" + tv_phone_number.getText().toString().substring(3);
                    String subPhoneFirst = phoneNumber.toString().substring(0, 3);
                    String subPhoneLast = phoneNumber.toString().substring(3);
                    number.append(subPhoneFirst)
                            .append(" ")
                            .append(subPhoneLast);
                    String phone = number.toString();
                    top_content = getString(R.string.do_you_want_tranfer, edit_amount.getText().toString() + "$", phone);
                } else {
                    String phone = "0" + tv_phone_number.getText().toString();
                    String subPhoneFirst = phone.toString().substring(0, 3);
                    String subPhoneLast = phone.toString().substring(3);
                    number.append(subPhoneFirst)
                            .append(" ")
                            .append(subPhoneLast);
                    String phoneNumber = number.toString();
                    top_content = getString(R.string.do_you_want_tranfer, edit_amount.getText().toString() + "$", phoneNumber);
                }

                String bottom_content = "";
                String title = getString(R.string.m_p_dialog_confirmation_call_title);
                MPOrderNumberDialog dialog = new MPOrderNumberDialog.Builder(getContext(), new MPOrderNumberDialog.OrderDialogListener() {
                    @Override
                    public void confirm() {
                        mParentActivity.hideLoadingDialog();
                        showOTPDialog();
                    }

                    @Override
                    public void cancel() {

                    }
                }, top_content, bottom_content, title, "", "", "").build();
                dialog.show(getChildFragmentManager(), "");
            } else {
                ToastUtils.showToast(getActivity(), getContext().getString(R.string.validate_amount));
            }

        }

    }
    private void showOTPDialog() {
        dialogSuccess = new MPDialogOtpIshareFragment();
        dialogSuccess.setPhone(tv_phone_number.getText().toString());
        dialogSuccess.setAmount((int) Float.parseFloat(edit_amount.getText().toString()));
        dialogSuccess.addButtonOnClickListener(new MPDialogOtpIshareFragment.ButtonOnClickListener() {
            @Override
            public void onDialogDismiss(String errorMes) {
                dialogSuccess.dismiss();
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), errorMes, false);
                baseMPSuccessDialog.show();
            }
            @Override
            public void onCheckOtpSuccess(String otp, WsCheckOtpResponse wsCheckOtpResponse) {
                dialogSuccess.dismiss();
                if(wsCheckOtpResponse.getResult().getErrorCode().equals("0")) {
                    ApplicationController.self().getFirebaseEventBusiness().logIshare(true, edit_amount.getText().toString(), "0");
                }else{
                    ApplicationController.self().getFirebaseEventBusiness().logIshare(true, edit_amount.getText().toString(), wsCheckOtpResponse.getResult().getErrorCode());
                }
                if (wsCheckOtpResponse.getResult().getErrorCode().equals("5")) {
                    String top_content = wsCheckOtpResponse.getResult().getUserMsg();
                    String bottom_content = "";
                    String title = getString(R.string.m_p_dialog_service_register_title_unsuccessful);
                    String topButton = getString(R.string.cancel);
                    String bottomButton = getString(R.string.check_history);
                    MPOrderNumberDialog dialog = new MPOrderNumberDialog.Builder(getContext(), new MPOrderNumberDialog.OrderDialogListener() {
                        @Override
                        public void confirm() {

                        }
                        @Override
                        public void cancel() {
                            gotoMPDetailFragment(false);
                        }
                    }, top_content, bottom_content, title, topButton, bottomButton, wsCheckOtpResponse.getResult().getErrorCode()).build();
                    dialog.show(getChildFragmentManager(), "");
                } else  if (wsCheckOtpResponse.getResult().getErrorCode().equals("6")) {
                    String top_content = getString(R.string.error_six, edit_amount.getText().toString());
                    String bottom_content = "";
                    String title = getString(R.string.m_p_dialog_service_register_title_unsuccessful);
                    String topButton = getString(R.string.btn_top_up);
                    String bottomButton = getString(R.string.late);
                    MPOrderNumberDialog dialog = new MPOrderNumberDialog.Builder(getContext(), new MPOrderNumberDialog.OrderDialogListener() {
                        @Override
                        public void confirm() {
                            gotoMPTopUpFragment();
                        }
                        @Override
                        public void cancel() {

                        }
                    }, top_content, bottom_content, title, topButton, bottomButton, wsCheckOtpResponse.getResult().getErrorCode()).build();
                    dialog.show(getChildFragmentManager(), "");
                } else  if (wsCheckOtpResponse.getResult().getErrorCode().equals("8")) {
                    String top_content = getString(R.string.error_eight);
                    String bottom_content = "";
                    String title = getString(R.string.m_p_dialog_service_register_title_unsuccessful);
                    String topButton = "";
                    String bottomButton = "";
                    MPOrderNumberDialog dialog = new MPOrderNumberDialog.Builder(getContext(), new MPOrderNumberDialog.OrderDialogListener() {
                        @Override
                        public void confirm() {

                        }
                        @Override
                        public void cancel() {

                        }
                    }, top_content, bottom_content, title, topButton, bottomButton, wsCheckOtpResponse.getResult().getErrorCode()).build();
                    dialog.show(getChildFragmentManager(), "");
                } else  if (wsCheckOtpResponse.getResult().getErrorCode().equals("4") || wsCheckOtpResponse.getResult().getErrorCode().equals("9") || wsCheckOtpResponse.getResult().getErrorCode().equals("10")) {
                    String top_content = getString(R.string.error_all_ishare);
                    String bottom_content = "";
                    String title = getString(R.string.m_p_dialog_service_register_title_unsuccessful);
                    String topButton = getString(R.string.late);
                    String bottomButton = "";
                    MPOrderNumberDialog dialog = new MPOrderNumberDialog.Builder(getContext(), new MPOrderNumberDialog.OrderDialogListener() {
                        @Override
                        public void confirm() {

                        }
                        @Override
                        public void cancel() {

                        }
                    }, top_content, bottom_content, title, topButton, bottomButton, wsCheckOtpResponse.getResult().getErrorCode()).build();
                    dialog.show(getChildFragmentManager(), "");
                }else if (wsCheckOtpResponse.getResult().getErrorCode().equals("0")){
                    baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.img_dialog_1, getString(R.string.successfully), wsCheckOtpResponse.getResult().getUserMsg(), false);
                    baseMPSuccessDialog.show();
                } else {
                    String top_content = getString(R.string.error_nice);
                    String bottom_content = "";
                    String title = getString(R.string.m_p_dialog_service_register_title_unsuccessful);
                    String topButton = getString(R.string.late);
                    String bottomButton = "";
                    MPOrderNumberDialog dialog = new MPOrderNumberDialog.Builder(getContext(), new MPOrderNumberDialog.OrderDialogListener() {
                        @Override
                        public void confirm() {

                        }
                        @Override
                        public void cancel() {

                        }
                    }, top_content, bottom_content, title, topButton, bottomButton, wsCheckOtpResponse.getResult().getErrorCode()).build();
                    dialog.show(getChildFragmentManager(), "");
                }
            }
        });
        dialogSuccess.show(getChildFragmentManager(), null);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK && requestCode == PICK_CONTACT) {
                Uri contactData = data.getData();
                Cursor cur = getActivity().managedQuery(contactData, null, null, null, null);
                ContentResolver contect_resolver = getActivity().getContentResolver();
                if (cur.moveToFirst()) {
                    String id = cur.getString(cur.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                    String name = "";
                    String no = "";
                    Cursor phoneCur = contect_resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (phoneCur.moveToFirst()) {
                        name = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        no = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    }
                    Log.e("Phone no & name :***: ", name + " : " + no);
                    if (UserInfoBusiness.isPhoneNumberValid(no, "KH")) {
                        String nationalPhone = Utilities.getNationalPhoneNumber(no, "KH");
                        tv_phone_number.setText(nationalPhone);
                        if (checkAmount) {
                            btn_next.setEnabled(true);
                        } else {
                            btn_next.setEnabled(false);
                        }
                    } else {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error :: ", e.toString());
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {
        if (list.get(position) instanceof AmountIshareMetfoneModel) {
            mAmount = ((AmountIshareMetfoneModel) list.get(position)).getAmount() + "";
            edit_amount.setText(mAmount);
            setSateViewSelect(position);
            amountAdapter.updateAdapter(amountMetfoneModels);
        }
    }
    public void setupUI(View view) {

        if (!(view instanceof LinearLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }
}

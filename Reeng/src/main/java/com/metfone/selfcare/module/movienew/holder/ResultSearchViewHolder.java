package com.metfone.selfcare.module.movienew.holder;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.Movie2;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

import java.util.List;

public class ResultSearchViewHolder extends BaseViewHolder {
    public ResultSearchViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
    }

    private RoundedImageView iv_cover;
    @Override
    public void initViewHolder(View v) {
        iv_cover = v.findViewById(R.id.iv_cover);
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        if (mData.get(pos) instanceof Movie2) {
            Movie2 movie = (Movie2) mData.get(pos);
            Glide.with(itemView.getContext())
                    .asBitmap().error(R.drawable.df_image_home_poster)
                    .load(movie.getPosterPath())
                    .into(iv_cover);
        }else if(mData.get(pos) instanceof Movie){
            Movie movie = (Movie) mData.get(pos);
            Glide.with(itemView.getContext())
                    .asBitmap().error(R.drawable.df_image_home_poster)
                    .load(movie.getPosterPath())
                    .into(iv_cover);
        }
        if (mData.get(pos) instanceof HomeData) {
            HomeData homeData = (HomeData) mData.get(pos);
            Glide.with(itemView.getContext())
                    .asBitmap().error(R.drawable.df_image_home_poster)
                    .load(homeData.getPosterPath())
                    .into(iv_cover);
        }
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null) {
            itemViewClickListener.onItemViewClickListener(pos, mData);
        }
    }
}

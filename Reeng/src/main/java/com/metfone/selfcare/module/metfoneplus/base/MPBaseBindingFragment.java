package com.metfone.selfcare.module.metfoneplus.base;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.model.camid.DetailType;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddFeedbackFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPBuyServiceDetailFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPContactUsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailAccountFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailSmsCallDataFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackUsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPServiceFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPStoreFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPStoreSearchFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPSupportFragment;
import com.metfone.selfcare.module.metfoneplus.topup.HistoryTopUpMPFragment;
import com.metfone.selfcare.module.metfoneplus.topup.QrCodeTopUpFragment;
import com.metfone.selfcare.module.metfoneplus.topup.ServiceTopUpFragment;
import com.metfone.selfcare.module.metfoneplus.topup.TopupMetfoneFragment;
import com.metfone.selfcare.module.metfoneplus.topup.model.Constants;
import com.metfone.selfcare.util.FragmentUtils;
import com.metfone.selfcare.util.Utilities;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class MPBaseBindingFragment<T extends ViewDataBinding> extends Fragment {
    public final String TAG = this.getClass().getSimpleName();

    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;
    public ApplicationController mApplication;
    public BaseSlidingFragmentActivity mParentActivity;
    public Resources mRes;
    private CamIdUserBusiness mCamIdUserBusiness;
    public T mBinding;

    public MPBaseBindingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mParentActivity = (BaseSlidingFragmentActivity) context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mApplication.getResources();
        this.mCamIdUserBusiness = mApplication.getCamIdUserBusiness();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        return mBinding.getRoot();
    }

    /**
     * Every fragment has to inflate a layout in the onCreateView method. We have added this method to
     * avoid duplicate all the inflate code in every fragment. You only have to return the layout to
     * inflate in this method when extends BaseFragment.
     */
    @LayoutRes
    protected abstract int getLayoutId();

    public void addFragmentTransactionAllowLoss(Fragment fragment, String tag) {
//        mFragmentTransaction = mFragmentManager.beginTransaction()
//        .setCustomAnimations(
//                R.anim.fif_slide_in,  // enter
//                R.anim.fif_fade_out,  // exit
//                R.anim.fif_fade_in,   // popEnter
//                R.anim.fif_slide_out  // popExit
//        );
//        mFragmentTransaction.add(R.id.metfone_root_frame, fragment, tag);
//        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        mFragmentTransaction.addToBackStack(tag);
//        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replaceMPMapFragmentTransactionAllowLoss(int containerViewId, Fragment fragment, String tag) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(containerViewId, fragment, tag);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replaceFragmentWithAnimationDefault(Fragment fragment) {
        FragmentUtils.replaceFragmentWithAnimationDefault(mFragmentManager, R.id.root_frame, fragment);
    }

    public void popBackStackFragment() {
        mFragmentManager.popBackStack();
        Log.e(TAG, "popBackStackFragment: Done");
    }

    public void gotoMPDetailFragment() {
        replaceFragmentWithAnimationDefault(MPDetailsFragment.newInstance());
    }

    public void gotoMPTopUpFragment() {
        replaceFragmentWithAnimationDefault(TopupMetfoneFragment.newInstance());
    }

    public void gotoMPServiceTopUpFragment(String phone, String amount) {
        ServiceTopUpFragment serviceTopUpFragment = ServiceTopUpFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PHONE, phone);
        bundle.putString(Constants.AMOUNT, amount);
        serviceTopUpFragment.setArguments(bundle);
        replaceFragmentWithAnimationDefault(serviceTopUpFragment);
    }

    public void gotoMPQrCodeFragment(String numberPhone) {
        replaceFragmentWithAnimationDefault(QrCodeTopUpFragment.newInstance(numberPhone));
    }

    public void gotoHistoryTopUpFragment() {
        replaceFragmentWithAnimationDefault(HistoryTopUpMPFragment.newInstance());
    }

    public void gotoMPSupportFragment() {
        replaceFragmentWithAnimationDefault(MPSupportFragment.newInstance());
    }

    public void gotoMPDetailAccountFragment() {
        replaceFragmentWithAnimationDefault(MPDetailAccountFragment.newInstance());
    }

    public void gotoMPServiceFragment(int tabPosition) {
        replaceFragmentWithAnimationDefault(MPServiceFragment.newInstance(tabPosition));
    }

    public void gotoMPHistoryChargeFragment(String titleScreen,
                                            @DetailType.DateName String dateName,
                                            @DetailType.BalanceName String balanceName,
                                            @DetailType.ChargeType String chargeType,
                                            @DetailType.DateType String dateType) {
        MPDetailSmsCallDataFragment f = MPDetailSmsCallDataFragment.newInstance(
                titleScreen,
                dateName,
                balanceName,
                chargeType,
                dateType);
        FragmentUtils.replaceFragmentWithAnimationDefault(mParentActivity.getSupportFragmentManager(), R.id.root_frame, f);
    }

    public void gotoMPFeedbackUs() {
        replaceFragmentWithAnimationDefault(MPFeedbackUsFragment.newInstance());
    }

    public void gotoMPAddFeedbackFragment() {
        replaceFragmentWithAnimationDefault(MPAddFeedbackFragment.newInstance());
    }

    public void gotoMPContactUs() {
        replaceFragmentWithAnimationDefault(MPContactUsFragment.newInstance());
    }

    public void gotoMPStoreFragment() {
        replaceFragmentWithAnimationDefault(MPStoreFragment.newInstance());
    }

    public void gotoMPStoreSearchFragment() {
        replaceFragmentWithAnimationDefault(MPStoreSearchFragment.newInstance());
    }

    public CamIdUserBusiness getCamIdUserBusiness() {
        if (mCamIdUserBusiness == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        }
        return mCamIdUserBusiness;
    }

    public String getStartTimeFromDateType(String dateType) {
        String startTime = "";
        if (dateType.equals(DetailType.DATE_TYPE_TODAY)) {
            startTime = Utilities.getTimeToday();
        } else if (dateType.equals(DetailType.DATE_TYPE_SEVEN_DAYS)) {
            startTime = Utilities.getTime7daysPrev();
        } else {
            startTime = Utilities.getTime30daysPrev();
        }

        return startTime;
    }
}
package com.metfone.selfcare.module.home_kh.notification.model;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_PHONE_NUMBER;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.PhoneLinkedInfo;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.WsGetGiftResponse;
import com.metfone.selfcare.module.home_kh.fragment.setting.BaseSettingKhFragment;
import com.metfone.selfcare.module.home_kh.notification.detail.DetailNotificationActivity;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class FragmentNotification extends BaseSettingKhFragment implements SwipeRefreshLayout.OnRefreshListener, KhNotificationAdapter.OnNotificationClickCallback {
    private KhHomeClient homeKhClient;

    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView txtTitle;
    @BindView(R.id.layout_empty)
    View layout_empty;
    @BindView(R.id.img_empty)
    LottieAnimationView img_empty;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.loading_view)
    LoadingView loading_view;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    protected ApplicationController app;
    protected BaseSlidingFragmentActivity activity;
    private UserInfoBusiness userInfoBusiness;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading = false;
    private boolean hasReachedMax = false;
    private LinearLayoutManager linearLayoutManager;
    private CamIdUserBusiness mCamIdUserBusiness;

    @Override
    protected void initView(View view) {
        txtTitle.setText(getActivity().getString(R.string.kh_noti_title));
    }

    private KhNotificationAdapter adapter;

    @Override
    public String getName() {
        return "KH-FragmentNotification";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_kh_notifications;
    }

    @OnClick(R.id.icBackToolbar)
    void onBack() {
        popBackStackFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        app = (ApplicationController) activity.getApplication();
        this.mCamIdUserBusiness = app.getCamIdUserBusiness();
    }

    private ArrayList<KhNotificationItem> notifications = new ArrayList<>();
    private int currentPage = 1;

    private void getWsGetNotifications(int page) {
        if (hasReachedMax) {
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.destroyDrawingCache();
                swipeRefreshLayout.clearAnimation();
            }
            return;
        }
        if (layout_empty != null) {
            layout_empty.setVisibility(View.GONE);
        }
        if (page == 1) {
            notifications.clear();
        }
        isLoading = true;
        homeKhClient.wsGetListCamIDNotification(String.valueOf(userInfoBusiness.getUser().getUser_id()), page, Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID), new MPApiCallback<KhNotificationResponse>() {
            @Override
            public void onResponse(retrofit2.Response<KhNotificationResponse> response) {
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.destroyDrawingCache();
                    swipeRefreshLayout.clearAnimation();
                }

                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {

                    KhNotificationResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    // Get value of item have "title" = "ACCOUNTS"
                    if (wsResponse != null) {
                        if (currentPage == 1) {
                            if (!wsResponse.new_.isEmpty()) {
                                notifications.addAll(wsResponse.new_);
                            }
                            if (!wsResponse.earlier.isEmpty()) {
                                notifications.addAll(wsResponse.earlier);
                            }
                            if (notifications.size() == 0) {
                                hasReachedMax = true;
                            }
                        } else {
                            if (wsResponse.earlier == null || wsResponse.earlier.size() == 0) {
                                hasReachedMax = true;
                            } else {
                                notifications.addAll(wsResponse.earlier);
                            }
                        }
                        Log.e(TAG, "wsGetListCamIDNotification - " + notifications.size());
                    } else {
                        Log.e(TAG, "wsGetListCamIDNotification - onResponse: empty");
                    }
                } else {
                    hasReachedMax = true;
                    Log.e(TAG, "wsGetListCamIDNotification - onResponse: error");
                }
                if (notifications.isEmpty()) {
                    showEmpty();
                } else {
                    adapter.notifyDataSetChanged();
                }
                isLoading = false;
            }

            @Override
            public void onError(Throwable error) {
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.destroyDrawingCache();
                    swipeRefreshLayout.clearAnimation();
                }
                showEmpty();
                isLoading = false;
            }
        });
    }

    private void markRead(int notifyId) {
        homeKhClient.wsUpdateIsReadCamIDNotification(String.valueOf(userInfoBusiness.getUser().getUser_id()), notifyId, Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID), new MPApiCallback<KhNotificationReadResponse>() {
            @Override
            public void onResponse(retrofit2.Response<KhNotificationReadResponse> response) {
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    public int getColor() {
        return ContextCompat.getColor(requireActivity(), R.color.kh_red_color);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userInfoBusiness = new UserInfoBusiness(getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        homeKhClient = new KhHomeClient();

        swipeRefreshLayout.setColorSchemeColors(getColor());
        swipeRefreshLayout.setOnRefreshListener(this);

        adapter = new KhNotificationAdapter(getActivity(), notifications, this);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recycler_view.setAdapter(adapter);

        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold) && !hasReachedMax) {
                    currentPage++;
                    getWsGetNotifications(currentPage);
                    isLoading = true;
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                try {
                    int firstPos = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (firstPos > 0) {
                        swipeRefreshLayout.setEnabled(false);
                    } else {
                        swipeRefreshLayout.setEnabled(true);
                        if (recyclerView.getScrollState() == 1)
                            if (swipeRefreshLayout.isRefreshing())
                                recyclerView.stopScroll();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Scroll Error : " + e.getLocalizedMessage());
                }
            }
        });

        new Handler().postDelayed(() -> {
            getWsGetNotifications(currentPage);
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(true);
        currentPage = 1;
        hasReachedMax = false;
        if (notifications != null) {
            notifications.clear();
        }
        getWsGetNotifications(currentPage);
    }

    @Override
    public void onNotificationClicked(KhNotificationItem item, int position) {
        if (item == null) {
            return;
        }
        if (item.readStatus == KhNotificationItem.Status.UN_READ) {
            markRead(item.notifyId);
            item.readStatus = KhNotificationItem.Status.READ;
            notifications.set(position, item);
            adapter.notifyDataSetChanged();
            EventBus.getDefault().post(new EventNotificationReadUpdate());
        }
        if (KhNotificationItem.ActionType.MOVIE_PROFILE.equals(item.actionType) || KhNotificationItem.ActionType.MOVIE_PLAY_DIRECTLY.equals(item.actionType)) {
            getMovie(item.actionObjectId, item);
        } else if (KhNotificationItem.ActionType.GIFT_CATALOG.equals(item.actionType) && item.type.equals(KhNotificationItem.Type.ACTION)) {
            receiverLunarGift(item.actionObjectId, item);
        } else {
            Intent intent = new Intent(getContext(), DetailNotificationActivity.class);
            intent.putExtra(DetailNotificationActivity.NOTIFICATION_KEY, item);
            startActivity(intent);
        }
    }

    private void receiverLunarGift(String actionObjectId, KhNotificationItem item) {
        String isdn = SharedPrefs.getInstance().get(PREF_PHONE_NUMBER, String.class);

        if (isdn == null || isdn.isEmpty() || !Utilities.isMetfoneNumber(isdn)) {
            List<Services> services = UserInfoBusiness.getInstance(requireContext()).getServiceList();
            for (Services s : services) {
                if (s.getServiceName().equals("Mobile")) {
                    for (PhoneLinkedInfo info : s.getPhoneLinkedList()) {
                        if (info.getMetfonePlus() == 1) {
                            isdn = info.getPhoneNumber();
                            break;
                        }
                    }
                    break;
                }
            }
        }

        homeKhClient.checkGetGiftTet(
                isdn,
                DeviceUtils.getAndroidID(requireContext()),
                new MPApiCallback<WsGetGiftResponse>() {
                    @Override
                    public void onResponse(Response<WsGetGiftResponse> response) {
                        if (response != null && response.body() != null
                                && response.body().getResult() != null
                                && response.body().getResult().getErrorCode().equals("0")
                                && response.body().getResult().getWsResponse() != null
                                && response.body().getResult().getWsResponse().isGetGift()
                        ) {
                            requireActivity().onBackPressed();
                            Intent intent = new Intent(getContext(), DetailNotificationActivity.class);
                            intent.putExtra(DetailNotificationActivity.NOTIFICATION_KEY, item);
                            intent.putExtra(DetailNotificationActivity.HIDE_BUTTON_KEY, true);
                            startActivity(intent);
                        } else {
                            requireActivity().onBackPressed();
                            Intent intent = new Intent(getContext(), DetailNotificationActivity.class);
                            intent.putExtra(DetailNotificationActivity.NOTIFICATION_KEY, item);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        requireActivity().onBackPressed();
                        Intent intent = new Intent(getContext(), DetailNotificationActivity.class);
                        intent.putExtra(DetailNotificationActivity.NOTIFICATION_KEY, item);
                        startActivity(intent);
                    }
                });
    }

    @Override
    public void onClearAllNotificationsClicked() {
        if (loading_view != null) {
            loading_view.setVisibility(View.VISIBLE);
            loading_view.showLoading();
        }
        homeKhClient.wsClearAllCamIdNotification(String.valueOf(userInfoBusiness.getUser().getUser_id()), Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID), new MPApiCallback<KhNotificationClearResponse>() {
            @Override
            public void onResponse(retrofit2.Response<KhNotificationClearResponse> response) {
                if (loading_view != null) {
                    loading_view.setVisibility(View.GONE);
                }
                if (response != null && response.body() != null && response.body().getResult() != null) {
                    if (response.body().getResult().getErrorCode().equals("0")) {
                        notifications.clear();
                        adapter.setItems(notifications);
                        adapter.notifyDataSetChanged();
                        showEmpty();
                        EventBus.getDefault().post(new EventNotificationReadUpdate());
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                if (loading_view != null) {
                    loading_view.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showEmpty() {
        if (layout_empty != null) {
            layout_empty.setVisibility(View.VISIBLE);
        }
        if (img_empty != null) {
            img_empty.setProgress(0);
            img_empty.playAnimation();
        }
    }

    private void getMovie(String id, KhNotificationItem item) {
        if (id == null) return;
        if (progressBar != null)
            progressBar.setVisibility(View.VISIBLE);
        MovieApi.getInstance().getMovieDetail(id, new ApiCallbackV2<Movie>() {

            @Override
            public void onError(String s) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onSuccess(String msg, Movie result) throws JSONException {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(getContext(), DetailNotificationActivity.class);
                intent.putExtra(DetailNotificationActivity.NOTIFICATION_KEY, item);
                intent.putExtra(DetailNotificationActivity.TRAILER_KEY, result.getTrailerUrl());
                startActivity(intent);
            }
        });
    }
}

package com.metfone.selfcare.module.home_kh.api.response;

public class ABAMobilePaymentTopUpCallBackResponse {
    public String errorCode;
    public String errorMessage;
    public Response result;
    public static class Response{
        public String responseCode;
        public String responseMessage;

        public boolean isSuccess() {
            return "00".equals(responseCode);
        }
    }
}

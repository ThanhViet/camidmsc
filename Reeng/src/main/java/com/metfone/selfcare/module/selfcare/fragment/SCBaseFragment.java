package com.metfone.selfcare.module.selfcare.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.Utilities;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;

/**
 * @author namnh40
 */
public abstract class SCBaseFragment extends Fragment implements OnInternetChangedListener {

    // tuyet doi ko dc thay doi bien TAG nay
    protected final String TAG = getName();
    protected BaseSlidingFragmentActivity mActivity;
    protected ApplicationController mApp;
    protected Toolbar toolbar;
    protected boolean isVisibleToUser;
    protected boolean isDataInitiated;
    protected boolean isViewInitiated;

    protected SwipeRefreshLayout layout_refresh;
    protected boolean isRefresh = false;

    public abstract String getName();

    public void onBackPressed() {
        if (mActivity == null) {
            Utilities.gotoMain(getActivity());
        } else {
            mActivity.onBackPressed();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseSlidingFragmentActivity) context;
        mApp = (ApplicationController) mActivity.getApplication();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (BuildConfig.DEBUG)
            Log.d(TAG, "onActivityResult requestCode= " + requestCode + "; resultCode= " + resultCode + "; data= " + data + "-------");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume isVisible: " + isVisibleToUser + " -------");
        if (isVisibleToUser)
            Utilities.hideKeyboard(getView(), getActivity(), TAG);
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause ------------------------------");
//        Utilities.hideKeyboard(getView(), getActivity(), TAG);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        isViewInitiated = false;
        Log.d(TAG, "onDestroy ------------------------------");
        super.onDestroy();
    }

    public void setupNavication(String title) {
        if (toolbar != null) {
            toolbar.setVisibility(View.VISIBLE);
            toolbar.setTitle(title);
            toolbar.setContentInsetStartWithNavigation(0);
            toolbar.setBackgroundColor(mActivity.getResources().getColor(R.color.backgroundToolbar));
            toolbar.setTitleTextColor(mActivity.getResources().getColor(R.color.textToolbarColor));
            toolbar.setNavigationIcon(R.drawable.ic_back_black_toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            Log.e(TAG, "Toolbar is null");
        }
    }

    public void setTitle(String title) {
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }

    public abstract int getResIdView();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getResIdView(), container, false);
        isViewInitiated = false;
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated visible: " + isVisibleToUser + " -------------------------");
        isViewInitiated = true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        Log.d(TAG, "setUserVisibleHint " + isVisibleToUser);
    }

    public boolean canLazyLoad() {
        return isVisibleToUser && isViewInitiated && !isDataInitiated;
    }

    @Override
    public void onInternetChanged() {

    }

    protected void showRefresh() {
        if (layout_refresh != null) {
            layout_refresh.setRefreshing(true);
        }
    }

    protected void hideRefresh() {
        if (layout_refresh != null) {
            isRefresh = false;

            layout_refresh.setRefreshing(false);
            layout_refresh.destroyDrawingCache();
            layout_refresh.clearAnimation();
        }
    }
}

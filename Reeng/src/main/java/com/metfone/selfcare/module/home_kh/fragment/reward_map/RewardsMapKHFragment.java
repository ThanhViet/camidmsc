package com.metfone.selfcare.module.home_kh.fragment.reward_map;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.metfone.esport.common.Common;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.fragment.RewardBaseFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.event.ReloadBottomSpace;
import com.metfone.selfcare.module.home_kh.model.StoreListDetail;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RewardsMapKHFragment extends RewardBaseFragment implements OnMapReadyCallback {
    private static final String DATA = "DATA";
    private GoogleMap mMap;
    private Unbinder unbinder;

    SupportMapFragment mapFragment;

    @BindView(R.id.tv_content)
    AppCompatTextView tvContent;
    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView txtTitleToolbar;
    @BindView(R.id.tv_distance)
    AppCompatTextView tvDistance;
    @BindView(R.id.space_bottom_map)
    View space_bottom_map;
    View space_bottom;
    private StoreListDetail storeListDetail;

    public static RewardsMapKHFragment newInstance(StoreListDetail storeListDetail) {
        RewardsMapKHFragment fragment = new RewardsMapKHFragment();
        Bundle args = new Bundle();
        args.putSerializable(DATA, storeListDetail);
        fragment.setArguments(args);
        return fragment;
    }

    public static final String TAG = RewardsMapKHFragment.class.getSimpleName();


    @Override
    protected boolean needShowBottomMenu() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        space_bottom = getActivity().findViewById(R.id.space_bottom);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (space_bottom != null) {
            space_bottom.setVisibility(View.VISIBLE);
        } else {
            ConstraintLayout.LayoutParams  layoutParams = (ConstraintLayout.LayoutParams) space_bottom_map.getLayoutParams();
            layoutParams.height = getNavigationBarHeight();
            space_bottom_map.setLayoutParams(layoutParams);
        }
        return view;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        super.onDetach();
        if (space_bottom != null) {
            EventBus.getDefault()
                    .post(new ReloadBottomSpace(space_bottom.getHeight()));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            storeListDetail = (StoreListDetail) bundle.getSerializable(DATA);
        }
        mapFragment.getMapAsync(this);
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_reward_map_kh;
    }

    @OnClick(R.id.icBackToolbar)
    void onBack() {
        popBackStackFragment();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (getActivity() != null) {
            mMap = googleMap;
            MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.style_map_json);
            mMap.setMapStyle(mapStyleOptions);
            if (storeListDetail != null) {
                LatLng sydney = new LatLng(storeListDetail.getLatitude(), storeListDetail.getLongtitude());
                mMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 20.0f));
                tvContent.setText(storeListDetail.getAddress());
                txtTitleToolbar.setText(storeListDetail.getPartnerName());
                tvDistance.setText(String.format(ResourceUtils.getString(R.string.distance_detail), new DecimalFormat("##.##").format(storeListDetail.getDistance())));
            }
        }
    }
}
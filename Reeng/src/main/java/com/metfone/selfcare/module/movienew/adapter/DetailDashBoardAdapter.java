package com.metfone.selfcare.module.movienew.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;


public class DetailDashBoardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int LAYOUT_CONTROL = 0;
    private static final int LAYOUT_MORE = 1;

    public DetailDashBoardAdapter() {
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return LAYOUT_CONTROL;
        else
            return LAYOUT_MORE;
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        RecyclerView.ViewHolder viewHolder = null;

        if (viewType == LAYOUT_CONTROL) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_control_video_new, parent, false);
            viewHolder = new ViewHolderOne(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_more_detail_video_new, parent, false);
            viewHolder = new ViewHolderTwo(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder.getItemViewType() == LAYOUT_CONTROL) {
            // Typecast Viewholder 
            // Set Viewholder properties 
            // Add any click listener if any 
        } else {

//        ViewHolderOne vaultItemHolder = (ViewHolderOne) holder;
//        vaultItemHolder.name.setText(displayText);
//        vaultItemHolder.name.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View v) {
//            .......
//           }
//         });

        }

    }

    //****************  VIEW HOLDER 1 ******************//

    public class ViewHolderOne extends RecyclerView.ViewHolder {

        public TextView name;

        public ViewHolderOne(View itemView) {
            super(itemView);
//         name = (TextView)itemView.findViewById(R.id.displayName);
        }
    }


    //****************  VIEW HOLDER 2 ******************//

    public class ViewHolderTwo extends RecyclerView.ViewHolder {

        public ViewHolderTwo(View itemView) {
            super(itemView);

//        ..... Do something
        }
    }
}
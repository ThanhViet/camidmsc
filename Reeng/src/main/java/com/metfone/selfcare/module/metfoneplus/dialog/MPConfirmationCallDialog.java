package com.metfone.selfcare.module.metfoneplus.dialog;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;

public class MPConfirmationCallDialog extends MPHeroDialog {
    public static final String TAG = MPConfirmationCallDialog.class.getSimpleName();
    private static final String CONTENT_PARAM = "content";
    private static final String NUMBER_PARAM = "number";
    private static final String TOP_PARAM = "top_button";
    private static final String BOTTOM_PARAM = "bottom_button";

    private Context mContext;
    private String mContentDialog;
    private String mPhoneNumber;
    private int mTopButton;
    private int mBottomButton;

    public static MPConfirmationCallDialog newInstance(String content, String number, int topButton, int bottomButton) {
        MPConfirmationCallDialog confirmationCallDialog = new MPConfirmationCallDialog();
        Bundle bundle = new Bundle();
        bundle.putString(CONTENT_PARAM, content);
        bundle.putString(NUMBER_PARAM, number);
        bundle.putInt(TOP_PARAM, topButton);
        bundle.putInt(BOTTOM_PARAM, bottomButton);
        confirmationCallDialog.setArguments(bundle);
        return confirmationCallDialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mContentDialog = getArguments().getString(CONTENT_PARAM);
            mPhoneNumber = getArguments().getString(NUMBER_PARAM);
            mTopButton = getArguments().getInt(TOP_PARAM);
            mBottomButton = getArguments().getInt(BOTTOM_PARAM);

        }

        mImage.setVisibility(View.GONE);
        mTitle.setText(R.string.m_p_dialog_confirmation_call_title);
        mContent.setText(mContentDialog);

        /*R.string.m_p_dialog_confirmation_call_yes*/
        setTopButton(mTopButton, R.drawable.bg_red_button_corner_6, buttonTop -> {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + mPhoneNumber));
            mContext.startActivity(callIntent);
        });
        /*R.string.m_p_dialog_confirmation_call_close*/
        setBottomButton(mBottomButton, R.drawable.bg_transparent_button_corner_6, buttonBottom -> {
            dismissAllowingStateLoss();
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        dismissAllowingStateLoss();
    }
}

package com.metfone.selfcare.module.movienew.holder;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.model.Category;

import java.util.List;

public class CategoryHolder extends BaseViewHolder {
    public CategoryHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
    }

    AppCompatImageView imgGenres;
    AppCompatTextView tvTitle;
    @Override
    public void initViewHolder(View v) {
        imgGenres = v.findViewById(R.id.img_genres);
        tvTitle = v.findViewById(R.id.txt_title);
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        Category genres = (Category) obj.get(pos);
        if (tvTitle != null) tvTitle.setText(genres.getCategoryName());
        Glide.with(context)
                .load(genres.getUrlImages()).error(R.drawable.ic_no_internet_1).placeholder(R.drawable.ic_place_holder_cinema)
                .into(imgGenres);
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null) {
            itemViewClickListener.onItemViewClickListener(pos, mData);
        }
    }
}

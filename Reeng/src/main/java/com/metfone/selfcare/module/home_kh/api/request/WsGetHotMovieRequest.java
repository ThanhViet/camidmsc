package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetHotMovieRequest extends BaseRequest<WsGetHotMovieRequest.Request> {

    public static class Request {
        private String limit;
        private String offset;
        @SerializedName("ref_id")
        private String refID;
        private String platform;
        private String languageCode;
        private String loginStatus;
        private String uuid;
        private String revision;
        private String security;
        private String clientType;
        private String countryCode;
        private String domain;
        private String msisdn;
        private String vip;
        private String timestamp;

        public Request(){
            limit = "10";
            offset = "0";
        }

        public Request(String refID, String platform, String languageCode, String loginStatus, String uuid, String revision, String security, String clientType, String countryCode, String domain, String msisdn, String vip, String timestamp) {
            this();
            this.refID = refID;
            this.platform = platform;
            this.languageCode = languageCode;
            this.loginStatus = loginStatus;
            this.uuid = uuid;
            this.revision = revision;
            this.security = security;
            this.clientType = clientType;
            this.countryCode = countryCode;
            this.domain = domain;
            this.msisdn = msisdn;
            this.vip = vip;
            this.timestamp = timestamp;
        }

        public String getLimit() { return limit; }
        public void setLimit(String value) { this.limit = value; }

        public String getOffset() { return offset; }
        public void setOffset(String value) { this.offset = value; }

        public String getRefID() { return refID; }
        public void setRefID(String value) { this.refID = value; }

        public String getPlatform() { return platform; }
        public void setPlatform(String value) { this.platform = value; }

        public String getLanguageCode() { return languageCode; }
        public void setLanguageCode(String value) { this.languageCode = value; }

        public String getLoginStatus() { return loginStatus; }
        public void setLoginStatus(String value) { this.loginStatus = value; }

        public String getUUID() { return uuid; }
        public void setUUID(String value) { this.uuid = value; }

        public String getRevision() { return revision; }
        public void setRevision(String value) { this.revision = value; }

        public String getSecurity() { return security; }
        public void setSecurity(String value) { this.security = value; }

        public String getClientType() { return clientType; }
        public void setClientType(String value) { this.clientType = value; }

        public String getCountryCode() { return countryCode; }
        public void setCountryCode(String value) { this.countryCode = value; }

        public String getDomain() { return domain; }
        public void setDomain(String value) { this.domain = value; }

        public String getMsisdn() { return msisdn; }
        public void setMsisdn(String value) { this.msisdn = value; }

        public String getVip() { return vip; }
        public void setVip(String value) { this.vip = value; }

        public String getTimestamp() { return timestamp; }
        public void setTimestamp(String value) { this.timestamp = value; }
    }

}

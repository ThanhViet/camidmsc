package com.metfone.selfcare.module.backup_restore;

import android.util.Base64;

import com.metfone.selfcare.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class BackupSecurityHelper {

    public static byte[] getBytesFromInputStream(InputStream inputStream) {
        byte[] buffer = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
        byte[] b = new byte[1024];
        try {
            int n;
            while ((n = inputStream.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return buffer;
    }

    public static class RSAUtils {
        private static final String ECB_PKCS1_PADDING = "RSA/ECB/PKCS1Padding";

        public static String BACKUP_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgFGVfrY4jQSoZQWWygZ83roKXWD4YeT2x2p41dGkPixe73rT2IW04glagN2vgoZoHuOPqa5and6kAmK2ujmCHu6D1auJhE2tXP+yLkpSiYMQucDKmCsWMnW9XlC5K7OSL77TXXcfvTvyZcjObEz6LIBRzs6+FqpFbUO9SJEfh6wIDAQAB";
        public static String BACKUP_PRIVATE_KEY = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAKAUZV+tjiNBKhlBZbKBnzeugpdYPhh5PbHanjV0aQ+LF7vetPYhbTiCVqA3a+Chmge44+prlqd3qQCYra6OYIe7oPVq4mETa1c/7IuSlKJgxC5wMqYKxYydb1eULkrs5IvvtNddx+9O/JlyM5sTPosgFHOzr4WqkVtQ71IkR+HrAgMBAAECgYAkQLo8kteP0GAyXAcmCAkA2Tql/8wASuTX9ITD4lsws/VqDKO64hMUKyBnJGX/91kkypCDNF5oCsdxZSJgV8owViYWZPnbvEcNqLtqgs7nj1UHuX9S5yYIPGN/mHL6OJJ7sosOd6rqdpg6JRRkAKUV+tmN/7Gh0+GFXM+ug6mgwQJBAO9/+CWpCAVoGxCA+YsTMb82fTOmGYMkZOAfQsvIV2v6DC8eJrSa+c0yCOTa3tirlCkhBfB08f8U2iEPS+Gu3bECQQCrG7O0gYmFL2RX1O+37ovyyHTbst4s4xbLW4jLzbSoimL235lCdIC+fllEEP96wPAiqo6dzmdH8KsGmVozsVRbAkB0ME8AZjp/9Pt8TDXD5LHzo8mlruUdnCBcIo5TMoRG2+3hRe1dHPonNCjgbdZCoyqjsWOiPfnQ2Brigvs7J4xhAkBGRiZUKC92x7QKbqXVgN9xYuq7oIanIM0nz/wq190uq0dh5Qtow7hshC/dSK3kmIEHe8z++tpoLWvQVgM538apAkBoSNfaTkDZhFavuiVl6L8cWCoDcJBItip8wKQhXwHp0O3HLg10OEd14M58ooNfpgt+8D8/8/2OOFaR0HzA+2Dm";

        private static PublicKey mPublicKey;
        private static PrivateKey mPrivateKey;

        private final static int KEYSIZE = 1024;
        private static final int DECODE_LEN = KEYSIZE / 8;
        public static final int ENCODE_LEN = DECODE_LEN - 11;
        public static final byte[] EMPTY_BYTE_ARRAY = new byte[0];

        public static PublicKey getPublicKey(String base64PublicKey) {
            PublicKey publicKey = null;
            try {
                X509EncodedKeySpec keySpec = new X509EncodedKeySpec(base64Decode(base64PublicKey));
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                publicKey = keyFactory.generatePublic(keySpec);
                return publicKey;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
            return publicKey;
        }

        public static PrivateKey getPrivateKey(String base64PrivateKey) {
            PrivateKey privateKey = null;
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(base64Decode(base64PrivateKey));
            KeyFactory keyFactory = null;
            try {
                keyFactory = KeyFactory.getInstance("RSA");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            try {
                privateKey = keyFactory.generatePrivate(keySpec);
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
            return privateKey;
        }

        public static String decrypt(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return new String(cipher.doFinal(data));
        }

        public static String encrypt(String data) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            if (mPublicKey == null) {
                mPublicKey = getPublicKey(BACKUP_PUBLIC_KEY);
            }
            cipher.init(Cipher.ENCRYPT_MODE, mPublicKey);
            return new String(base64Encode(cipher.doFinal(data.getBytes())));
        }

        public static String decrypt(String data) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
            return decrypt(base64Decode(data), mPrivateKey);
        }

        private static String base64Encode(byte[] input) {
            return android.util.Base64.encodeToString(input, android.util.Base64.NO_WRAP);
        }

        private static byte[] base64Decode(String input) {
            return android.util.Base64.decode(input.getBytes(), android.util.Base64.NO_WRAP);
        }

        public static byte[] encryptPublicKey(byte[] encryptedData) throws Exception {
            if (encryptedData == null) {
                throw new IllegalArgumentException("Input encryption data is null");
            }
            byte[] encode = new byte[]{};
            for (int i = 0; i < encryptedData.length; i += ENCODE_LEN) {
                byte[] subarray = subarray(encryptedData, i, i + ENCODE_LEN);
                byte[] doFinal = encryptByPublicKey(subarray);
                encode = addAll(encode, doFinal);
            }
            return encode;
        }

        public static byte[] decryptPrivateKey(byte[] encode) throws Exception {
            if (encode == null) {
                throw new IllegalArgumentException("Input data is null");
            }
            byte[] buffers = new byte[]{};
            for (int i = 0; i < encode.length; i += DECODE_LEN) {
                byte[] subarray = subarray(encode, i, i + DECODE_LEN);
                byte[] doFinal = decryptByPrivateKey(subarray);
                buffers = addAll(buffers, doFinal);
            }
            return buffers;
        }

        private static byte[] decryptByPrivateKey(byte[] data) throws Exception {
            if (data == null) {
                throw new IllegalArgumentException("Input data is null");
            }
            Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
            if (mPrivateKey == null) {
                mPrivateKey = getPrivateKey(BACKUP_PRIVATE_KEY);
            }
            cipher.init(Cipher.DECRYPT_MODE, mPrivateKey);
            return cipher.doFinal(data);
        }

    /*public static String encryptString(String data) throws Exception {
        if (TextUtils.isEmpty(data)) return "";
        byte[] encryptedData = data.getBytes();
        byte[] encode = encryptPublicKey(encryptedData);
        return base64Encode(encode);
    }*/

        private static byte[] encryptByPublicKey(byte[] data) throws Exception {
            if (data == null) {
                throw new IllegalArgumentException("Input data is null");
            }
            Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
            if (mPublicKey == null) {
                mPublicKey = getPublicKey(BACKUP_PUBLIC_KEY);
            }
            cipher.init(Cipher.ENCRYPT_MODE, mPublicKey);

            return cipher.doFinal(data);
        }

        private static byte[] subarray(final byte[] array, int startIndexInclusive, int endIndexExclusive) {
            if (array == null) {
                return null;
            }
            if (startIndexInclusive < 0) {
                startIndexInclusive = 0;
            }
            if (endIndexExclusive > array.length) {
                endIndexExclusive = array.length;
            }
            final int newSize = endIndexExclusive - startIndexInclusive;
            if (newSize <= 0) {
                return EMPTY_BYTE_ARRAY;
            }

            final byte[] subarray = new byte[newSize];
            System.arraycopy(array, startIndexInclusive, subarray, 0, newSize);
            return subarray;
        }

        private static byte[] addAll(final byte[] array1, final byte... array2) {
            if (array1 == null) {
                return clone(array2);
            } else if (array2 == null) {
                return clone(array1);
            }
            final byte[] joinedArray = new byte[array1.length + array2.length];
            System.arraycopy(array1, 0, joinedArray, 0, array1.length);
            System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
            return joinedArray;
        }

        private static byte[] clone(final byte[] array) {
            if (array == null) {
                return null;
            }
            return array.clone();
        }
    }

    public static class AESCrypt {
        private static final String TAG = com.metfone.selfcare.helper.encrypt.AESCrypt.class.getSimpleName();
        //AESCrypt-ObjC uses CBC and PKCS7Padding
        private static final String AES_MODE = "AES/CBC/PKCS7Padding";
        private static final String CHARSET = "UTF-8";
        //AESCrypt-ObjC uses SHA-256 (and so a 256-bit key)
        private static final String HASH_ALGORITHM = "SHA-256";
        //AESCrypt-ObjC uses blank IV (not the best security, but the aim here is compatibility)
        private static final byte[] ivBytes = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        private static AESCrypt mInstance;
        private SecretKeySpec mSecretKeySpec;
        private static final String KEY = "bW9jaGFfYmFja3VwX21lc3NhZ2VfYWVzcGFzc3dvcmQ=";

        private AESCrypt() {
            try {
                this.mSecretKeySpec = generateKey(KEY);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }

        public static synchronized AESCrypt getInStance() {
            if (mInstance == null) {
                mInstance = new AESCrypt();
            }
            return mInstance;
        }

        /**
         * Generates SHA256 hash of the password which is used as key
         *
         * @param password used to generated key
         * @return SHA256 of the password
         */
        private SecretKeySpec generateKey(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {

            byte[] bytes = Base64.decode(password, Base64.NO_WRAP);
            return new SecretKeySpec(bytes, "AES");
        }

        /**
         * Encrypt and encode message using 256-bit AES with key generated from password.
         *
         * @param message the thing you want to encrypt assumed String UTF-8
         * @return Base64 encoded CipherText
         * @throws GeneralSecurityException if problems occur during encryption
         */
        public String encrypt(String message) {
            try {
                byte[] cipherText = encrypt(ivBytes, message.getBytes(CHARSET));
                //NO_WRAP is important as was getting \n at the end
                return Base64.encodeToString(cipherText, Base64.NO_WRAP);
            } catch (GeneralSecurityException e) {
                Log.e(TAG, "GeneralSecurityException ", e);
                return null;
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "UnsupportedEncodingException", e);
                return null;
            }
        }

        /**
         * More flexible AES encrypt that doesn't encode
         *
         * @param iv      Initiation Vector
         * @param message in bytes (assumed it's already been decoded)
         * @return Encrypted cipher text (not encoded)
         * @throws GeneralSecurityException if something goes wrong during encryption
         */
        public byte[] encrypt(final byte[] iv, final byte[] message)
                throws GeneralSecurityException {
            final Cipher cipher = Cipher.getInstance(AES_MODE);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            cipher.init(Cipher.ENCRYPT_MODE, mSecretKeySpec, ivSpec);
            return cipher.doFinal(message);
        }


        public byte[] encrypt(final byte[] message)
                throws GeneralSecurityException {
            final Cipher cipher = Cipher.getInstance(AES_MODE);
            IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            cipher.init(Cipher.ENCRYPT_MODE, mSecretKeySpec, ivSpec);
            return cipher.doFinal(message);
        }

        /**
         * Decrypt and decode ciphertext using 256-bit AES with key generated from password
         *
         * @param base64EncodedCipherText the encrpyted message encoded with base64
         * @return message in Plain text (String UTF-8)
         * @throws GeneralSecurityException if there's an issue decrypting
         */
        public String decrypt(String base64EncodedCipherText)
                throws GeneralSecurityException {
            try {
                byte[] decodedCipherText = Base64.decode(base64EncodedCipherText, Base64.NO_WRAP);
                byte[] decryptedBytes = decrypt(ivBytes, decodedCipherText);
                return new String(decryptedBytes, CHARSET);
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "UnsupportedEncodingException ", e);
                throw new GeneralSecurityException(e);
            }
        }


        /**
         * More flexible AES decrypt that doesn't encode
         *
         * @param iv                Initiation Vector
         * @param decodedCipherText in bytes (assumed it's already been decoded)
         * @return Decrypted message cipher text (not encoded)
         * @throws GeneralSecurityException if something goes wrong during encryption
         */
        public byte[] decrypt(final byte[] iv, final byte[] decodedCipherText)
                throws GeneralSecurityException {
            final Cipher cipher = Cipher.getInstance(AES_MODE);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, mSecretKeySpec, ivSpec);
            return cipher.doFinal(decodedCipherText);
        }

        public byte[] decrypt(final byte[] decodedCipherText)
                throws GeneralSecurityException {
            final Cipher cipher = Cipher.getInstance(AES_MODE);
            IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            cipher.init(Cipher.DECRYPT_MODE, mSecretKeySpec, ivSpec);
            return cipher.doFinal(decodedCipherText);
        }
    }

}

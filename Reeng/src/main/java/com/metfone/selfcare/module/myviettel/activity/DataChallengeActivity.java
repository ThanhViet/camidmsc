/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/30
 *
 */

package com.metfone.selfcare.module.myviettel.activity;

import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.myviettel.fragment.CommissionDataChallengeFragment;
import com.metfone.selfcare.module.myviettel.fragment.HistoryDataChallengeFragment;
import com.metfone.selfcare.module.myviettel.fragment.InviteDataChallengeFragment;
import com.metfone.selfcare.module.myviettel.network.ApiCallback;
import com.metfone.selfcare.module.myviettel.network.MyViettelApi;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataChallengeActivity extends BaseSlidingFragmentActivity implements OnInternetChangedListener {

    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_commission)
    TextView tvCommission;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.button_help)
    View btnHelp;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.button_back)
    View btnBack;

    private ViewPagerDetailAdapter adapter;
    private ListenerUtils listenerUtils;
    private MyViettelApi myViettelApi;
    private String totalCommission = "";

    public MyViettelApi getMyViettelApi() {
        if (myViettelApi == null) myViettelApi = new MyViettelApi();
        return myViettelApi;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_challenge);
        ButterKnife.bind(this);
        initListener();
        setupViewPager();
        setTotalCommission();
        loadData();
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(this)) {

        }
    }

    @Override
    protected void onDestroy() {
        if (listenerUtils != null) listenerUtils.removerListener(this);
        super.onDestroy();
    }

    private void initListener() {
        if (tvTitle != null) tvTitle.setSelected(true);
        ImageBusiness.setResource(ivCover, R.drawable.banner_data_challenge);
        if (btnBack != null) {
            btnBack.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    finish();
                }
            });
        }
        if (btnHelp != null) {
            final String link = ApplicationController.self().getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_DATA_CHALLENGE_RULE);
            if (!TextUtils.isEmpty(link) && !"-".equals(link)) {
                btnHelp.setVisibility(View.VISIBLE);
            } else {
                btnHelp.setVisibility(View.GONE);
            }
            btnHelp.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    DeepLinkHelper.getInstance().openSchemaLink(DataChallengeActivity.this, link);
                }
            });
        }
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) listenerUtils.addListener(this);
    }

    private void setupViewPager() {
        adapter = new ViewPagerDetailAdapter(getSupportFragmentManager());
        adapter.addFragment(InviteDataChallengeFragment.newInstance(), getString(R.string.tab_invite_data_challenge));
        adapter.addFragment(CommissionDataChallengeFragment.newInstance(), getString(R.string.tab_commission_data_challenge));
        adapter.addFragment(HistoryDataChallengeFragment.newInstance(), getString(R.string.tab_history_data_challenge));
        tabLayout.setupWithViewPager(viewPager);
        if (viewPager != null) {
            //viewPager.addOnPageChangeListener(this);
            viewPager.setAdapter(adapter);
            viewPager.setOffscreenPageLimit(3);
        }
        adapter.notifyDataSetChanged();
    }

    private void setTotalCommission() {
        ReengAccountBusiness reengAccountBusiness = ApplicationController.self().getReengAccountBusiness();
        if (tvName != null) tvName.setText(Html.fromHtml(getString(R.string.total_commission_title
                , reengAccountBusiness.getUserName(), reengAccountBusiness.getJidNumber())));
        if (tvCommission != null) {
            if (TextUtils.isEmpty(totalCommission)) totalCommission = "0";
            tvCommission.setText(getString(R.string.total_commission_money, totalCommission));
        }
    }

    private void loadData() {
        getMyViettelApi().getTotalCommissionDC(new ApiCallback<String>() {
            @Override
            public void onSuccess(String msg, String result) throws Exception {
                totalCommission = result;
                setTotalCommission();
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}

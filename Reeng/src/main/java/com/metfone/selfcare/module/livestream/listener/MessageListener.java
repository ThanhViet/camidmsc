package com.metfone.selfcare.module.livestream.listener;

import com.metfone.selfcare.module.livestream.model.LiveStreamMessage;

import java.util.ArrayList;

public interface MessageListener {
    void onNewMessage(LiveStreamMessage message);

    void onGetListMessage(ArrayList<LiveStreamMessage> messages);
}

package com.metfone.selfcare.module.home_kh.fragment.setting.share;

import android.util.Log;
import android.widget.ProgressBar;

import com.metfone.selfcare.listeners.ShareAndGetMoreContract;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.util.RetrofitInstance;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author ITSOL JAPAN
 * Created on 7/1/2021.
 * Copyright � 2020 YSL Solution Co., Ltd. All rights reserved.
 **/
 public class ShareAndGetMorePresenter implements ShareAndGetMoreContract.Presenter {
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private ShareAndGetMoreContract.View view;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ShareAndGetMorePresenter(ShareAndGetMoreContract.View listView) {
        this.view = listView;
        view.setPresenter(this);
    }

    @Override
    public void getListConfig() {
        Disposable disposable = apiService.getListConfig()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(listBaseResponse -> {
                view.onSuccess(listBaseResponse.getData());
                Log.d("TAG", "getListConfig: " + listBaseResponse.getData().size());
            }, error->{
                view.onFail(error.getMessage());
            });
        compositeDisposable.add(disposable);
    }

    public void onClear(){
        compositeDisposable.dispose();
    }

}

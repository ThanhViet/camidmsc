package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCPackage;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCPackageList extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("content")
    private ArrayList<SCPackage> data;

    public ArrayList<SCPackage> getData() {
        return data;
    }

    public void setData(ArrayList<SCPackage> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCPackageList [data=" + data + "] errror " + getErrorCode();
    }
}

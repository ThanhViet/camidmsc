/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/3
 *
 */

package com.metfone.selfcare.module.home_kh.tab.adapter.banner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnClickSliderBanner;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeBanner;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.ui.view.indicator.OverflowPagerIndicator;
import com.metfone.selfcare.ui.view.indicator.SimpleSnapHelper;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;

public class KhSlideBannerHolder extends BaseAdapter.ViewHolder {

    private final int CHANGE_SLIDE_BANNER_TIME = 4000;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.indicator)
    OverflowPagerIndicator indicator;
    private ArrayList<KhHomeMovieItem> data = new ArrayList<>();
    private KhHomeBannerAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int next;
    private boolean isSlideBanner;
    private Runnable runnableAutoScroll = new Runnable() {
        @Override
        public void run() {
            if (recyclerView == null || layoutManager == null) return;
            if (Utilities.notEmpty(data) && data.size() > 1) {
                try {
                    int current = layoutManager.findFirstVisibleItemPosition();
                    int positionNext = current;
                    if (current < data.size() - 1) {
                        positionNext++;
                    } else {
                        positionNext = 0;
                    }
                    layoutManager.smoothScrollToPosition(recyclerView, null, positionNext);
                    indicator.onPageSelected(positionNext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            recyclerView.postDelayed(this, isSlideBanner ? CHANGE_SLIDE_BANNER_TIME : Constants.CHANGE_BANNER_TIME);
        }
    };

    @SuppressLint("ClickableViewAccessibility")
    public KhSlideBannerHolder(View view, Activity activity, OnClickSliderBanner listener, boolean slideBanner) {
        super(view);
        this.isSlideBanner = slideBanner;
        if (data == null) data = new ArrayList<>();
        adapter = new KhHomeBannerAdapter(activity, listener);
        adapter.setItems(data);
        layoutManager = new CustomLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, layoutManager, adapter, false);
        indicator.attachToRecyclerView(recyclerView);
        new SimpleSnapHelper(indicator).attachToRecyclerView(recyclerView);
        recyclerView.postDelayed(runnableAutoScroll, isSlideBanner ? CHANGE_SLIDE_BANNER_TIME : Constants.CHANGE_BANNER_TIME);
        recyclerView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                recyclerView.removeCallbacks(runnableAutoScroll);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                recyclerView.removeCallbacks(runnableAutoScroll);
                recyclerView.postDelayed(runnableAutoScroll, isSlideBanner ? CHANGE_SLIDE_BANNER_TIME : Constants.CHANGE_BANNER_TIME);
            }
            return false;
        });
    }

    @Override
    public void bindData(Object item, int position) {
        if (data == null) data = new ArrayList<>();
        else data.clear();
        data.addAll(((KhHomeBanner) item).banners);
        if (adapter != null) {
            adapter.setItems(data);
            adapter.notifyDataSetChanged();
        }
        if (layoutManager != null && recyclerView != null) {
            layoutManager.smoothScrollToPosition(recyclerView, null, 0);
        }
        if (indicator != null) {
            indicator.onPageSelected(0);
        }
    }

    public RecyclerView getRecycler() {
        return recyclerView;
    }
}
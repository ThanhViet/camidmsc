package com.metfone.selfcare.module.movienew.common;

import android.content.Context;

import com.metfone.selfcare.module.games.api.GameApi;
import com.metfone.selfcare.module.search.network.SearchApi;

import java.util.ArrayList;
import java.util.Collections;

public class CommonSearch {
    private final int SIZE_ARR = 6;
    TinyDB mTinyDB;


    public CommonSearch(Context context) {
        mTinyDB = new TinyDB(context);
    }

    public void clearHistory() {
        mTinyDB.putListString(Constant.DATA_SEARCH, new ArrayList<>());
    }

    public void insertSearchHistory(String data) {
        ArrayList<String> dataSearch = mTinyDB.getListString(Constant.DATA_SEARCH);
        if (dataSearch == null) {
            dataSearch = new ArrayList<>();
        }
        dataSearch.add(data);
        if (dataSearch.size() > SIZE_ARR) {
            dataSearch = new ArrayList<>(dataSearch.subList(dataSearch.size() - SIZE_ARR, dataSearch.size()));
        }
        mTinyDB.putListString(Constant.DATA_SEARCH, dataSearch);
    }

    public ArrayList<String> getSearchHistoryLimit() {
        ArrayList<String> arrayList = new ArrayList();
        ArrayList<String> result = new ArrayList();
        if (mTinyDB.getListString(Constant.DATA_SEARCH) != null)
            arrayList = mTinyDB.getListString(Constant.DATA_SEARCH);
        Collections.reverse(arrayList);
        for (String data : arrayList) {
            if (!result.contains(data)) {
                result.add(data);
            }
            if (result.size() == SIZE_ARR) {
                break;
            }
        }
        return result;
    }

    private SearchApi mSearchApi;

    public synchronized SearchApi getSearchApi() {
        if (mSearchApi == null) {
            synchronized (SearchApi.class) {
                mSearchApi = new SearchApi();
            }
        }
        return mSearchApi;
    }

    private GameApi mSearchGameApi;

    public synchronized GameApi getSearchGameApi() {
        if (mSearchGameApi == null) {
            synchronized (GameApi.class) {
                mSearchGameApi = GameApi.getInstance();
            }
        }
        return mSearchGameApi;
    }
}

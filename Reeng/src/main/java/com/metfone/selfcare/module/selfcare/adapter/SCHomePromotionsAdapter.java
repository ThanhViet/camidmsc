package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCPackage;
import com.metfone.selfcare.module.selfcare.utils.SCImageLoader;

import java.util.List;

public class SCHomePromotionsAdapter extends BaseAdapter<BaseViewHolder> {

    private List<SCPackage> itemsList;
    private AbsInterface.OnPackageRegisterListener listener;

    public SCHomePromotionsAdapter(Context context, AbsInterface.OnPackageRegisterListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(List<SCPackage> itemsList) {
        this.itemsList = itemsList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_home_promotion, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder holder, int position) {
        final SCPackage data = itemsList.get(position);
        if (data != null) {
            holder.setText(R.id.tvName, data.getName() + " - " + data.getCode());
            holder.setText(R.id.tvDescription, data.getShortDes());
            SCImageLoader.setImage(mContext, (ImageView) holder.getView(R.id.imvImage), data.getIconUrl());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onPackageClick(data);
                }
            }
        });

        holder.getView(R.id.btnRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onRegisterClick(data, holder.getView(R.id.btnRegister));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }
}

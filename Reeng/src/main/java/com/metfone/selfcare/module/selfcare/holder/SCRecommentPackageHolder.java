package com.metfone.selfcare.module.selfcare.holder;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.adapter.SCRecommentPackageItemAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCRecommentPackage;

public class SCRecommentPackageHolder extends BaseViewHolder {
    private TextView tvTitle;
    private TextView tvViewAll;
    private View btnMore;
    private RecyclerView recyclerView;
    private SCRecommentPackageItemAdapter adapter;
    private LinearLayoutManager layoutManager;
    private SCRecommentPackage data;
    private AbsInterface.OnPackageHeaderListener listener;

    public SCRecommentPackageHolder(Context context, View view, final AbsInterface.OnPackageHeaderListener listener) {
        super(view);
        this.listener = listener;

        tvTitle = view.findViewById(R.id.tvTitle);
        tvViewAll = view.findViewById(R.id.tvViewAll);
        btnMore = view.findViewById(R.id.btnMore);
        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCRecommentPackageItemAdapter(context, listener);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(context, R.drawable.divider_default));
        }
        recyclerView.setAdapter(adapter);

        tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onHeaderClick(data);
            }
        });

        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onHeaderClick(data);
            }
        });
    }

    public void setData(SCRecommentPackage data) {
        this.data = data;
        tvTitle.setText(data.getGroupName());
        adapter.setItemsList(data.getData());
        adapter.notifyDataSetChanged();
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/7/4
 *
 */

package com.metfone.selfcare.module.tab_home.event;

public class TabHomeEvent {
    private boolean reselected;
    private boolean updateNotify;
    private boolean updateFeature;

    public TabHomeEvent() {

    }

    public boolean isReselected() {
        return reselected;
    }

    public TabHomeEvent setReselected(boolean reselected) {
        this.reselected = reselected;
        return this;
    }

    public boolean isUpdateNotify() {
        return updateNotify;
    }

    public TabHomeEvent setUpdateNotify(boolean updateNotify) {
        this.updateNotify = updateNotify;
        return this;
    }

    public boolean isUpdateFeature() {
        return updateFeature;
    }

    public TabHomeEvent setUpdateFeature(boolean updateFeature) {
        this.updateFeature = updateFeature;
        return this;
    }

    @Override
    public String toString() {
        return "TabHomeEvent{" +
                "reselected=" + reselected +
                ", updateFeature=" + updateFeature +
                ", updateNotify=" + updateNotify +
                '}';
    }
}

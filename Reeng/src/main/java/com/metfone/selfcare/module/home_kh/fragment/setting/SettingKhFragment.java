package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.activity.FeedbackActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.module.home_kh.activity.SettingKhActivity;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.utils.AnimationUtil;
import com.metfone.selfcare.v5.widget.MochaProgressBarV5;

import java.util.ArrayList;

public class SettingKhFragment extends BaseSettingKhFragment implements View.OnClickListener {
    private static final String TAG = SettingKhFragment.class.getSimpleName();
    public static final String ARG_FUNCTION_ID = "arg_func_id";
    private OnFragmentSettingListener mListener;
    private ReengAccountBusiness mAccountBusiness;
    private ContactBusiness mContactBusiness;
    private Resources mRes;
    //    private ImageView mImgOption;
//    private ImageView mImgBack;
//    private View settingChangeLanguageView;

    private ViewGroup mViewAccount, /*mViewAddFriend, mViewInvite,*/
            mViewSyncContact, mViewNoteMessage,
            mViewNotification, mViewCallAndMessage, mViewImageAndSound,
    /*mViewTranslation, */ mViewAboutInfo/*, mViewDeleteAVNO*/;
    //    private View mViewAboutNew, mViewSettingNotiNew;
    private int mFuncId = -1;
    private AppCompatTextView tvTitleSyncContact;
    private View btnSyncContact;
    private MochaProgressBarV5 progressBarSyncContact;
    private AppCompatTextView tvNoConnection;
    private boolean isSyncingContact;
    protected ApplicationController app;

    public static SettingKhFragment newInstance() {
        return new SettingKhFragment();
    }

    public static SettingKhFragment newInstance(int functionId) {
        SettingKhFragment frag = new SettingKhFragment();
        Bundle arg = new Bundle();
        arg.putInt(ARG_FUNCTION_ID, functionId);
        frag.setArguments(arg);
        Log.d(TAG, "functionId" + functionId);
        return frag;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mFuncId = getArguments().getInt(ARG_FUNCTION_ID);
        }
        findComponentViews(view);
        drawDetail();
        setViewListener();
        Log.d(TAG, "functionId" + mFuncId);
        if (mFuncId == Constants.SETTINGS.LOGOUT) {
            showDeactiveAccountPopup(mParentActivity);
        }
        setTitle(R.string.setting);
        // check and set icon upload
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            if (mApplication.getConfigBusiness().isUpdate()) {
                AppCompatImageView icAppInfor = view.findViewById(R.id.icAppInformation);
                ViewGroup.LayoutParams layoutParams = icAppInfor.getLayoutParams();
                layoutParams.width = ConvertHelper.dpToPx(mApplication, 23);
                layoutParams.height = ConvertHelper.dpToPx(mApplication, 23);
                icAppInfor.invalidate();
                icAppInfor.setImageResource(R.drawable.ic_infor_has_update);
                AppCompatTextView tvTitleInformation = view.findViewById(R.id.tvTitleInformation);
                LinearLayoutCompat.LayoutParams layoutParams1 = (LinearLayoutCompat.LayoutParams) tvTitleInformation.getLayoutParams();
                layoutParams1.leftMargin = ConvertHelper.dpToPx(mApplication, 5);
                tvTitleInformation.invalidate();
            }
        }*/
        onShowViewLogged();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mContactBusiness = mApplication.getContactBusiness();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mRes = mParentActivity.getResources();
        app = (ApplicationController) mParentActivity.getApplication();
        try {
            mListener = (OnFragmentSettingListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }

    @Override
    public String getName() {
        return "SettingKhFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_main_setting_kh;
    }

    @Override
    public void onResume() {
        super.onResume();
        drawDetail();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mParentActivity = null;
    }

    private void findComponentViews(View rootView) {
        btnSyncContact = rootView.findViewById(R.id.btnSyncContact);
        mViewNotification = rootView.findViewById(R.id.setting_notification);
        tvTitleSyncContact = rootView.findViewById(R.id.tvTitleSyncContact);
        mViewCallAndMessage = rootView.findViewById(R.id.setting_call_and_message);
        mViewImageAndSound = rootView.findViewById(R.id.setting_image_and_sound);
//        mViewTranslation = rootView.findViewById(R.id.setting_translate);
        //
        mViewAccount = rootView.findViewById(R.id.rl_setting_account);
//        mViewAddFriend = rootView.findViewById(R.id.setting_add_friend);
//        mViewInvite = rootView.findViewById(R.id.setting_invite_friend);
        mViewNoteMessage = rootView.findViewById(R.id.setting_note_message);
        mViewSyncContact = rootView.findViewById(R.id.setting_sync_contact);
        /*
         * chuyển đổi ngôn ngữ
         */

//        settingChangeLanguageView = rootView.findViewById(R.id.setting_change_language);
//        if ("KH".equals(mAccountBusiness.getRegionCode())) {
//            settingChangeLanguageView.setVisibility(View.VISIBLE);
//        } else {
//            settingChangeLanguageView.setVisibility(View.GONE);
//        }
        //
//        mViewDeleteAVNO = rootView.findViewById(R.id.setting_delete_avno);
        mViewAboutInfo = rootView.findViewById(R.id.setting_app_info);
//        mViewAboutNew = rootView.findViewById(R.id.setting_app_info_new);
//        mViewSettingNotiNew = rootView.findViewById(R.id.setting_app_noti_new);
//        mTvwTitle.setText(mRes.getString(R.string.setting));
//
//        if (BuildConfig.DEBUG && mAccountBusiness.isAvnoEnable() && !TextUtils.isEmpty(mAccountBusiness.getAVNONumber())) {
//            mViewDeleteAVNO.setVisibility(View.VISIBLE);
//        } else {
//            mViewDeleteAVNO.setVisibility(View.GONE);
//        }
        progressBarSyncContact = rootView.findViewById(R.id.progressBarSyncContact);
        tvNoConnection = rootView.findViewById(R.id.tvNoConnection);

    }

    private void drawDetail() {
        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
//            mImgOption.setVisibility(View.INVISIBLE);
            mViewAccount.setVisibility(View.GONE);
//            mViewAddFriend.setVisibility(View.GONE);
//            mViewInvite.setVisibility(View.GONE);
            mViewSyncContact.setVisibility(View.GONE);
            mViewNoteMessage.setVisibility(View.GONE);
            mViewCallAndMessage.setVisibility(View.GONE);
            mViewImageAndSound.setVisibility(View.GONE);
//            viewTranslate.setVisibility(View.VISIBLE);
//            mViewDeleteAVNO.setVisibility(View.GONE);
            //mViewCustomizeTab.setVisibility(View.GONE);
        } else {
//            mImgOption.setVisibility(View.INVISIBLE);
            // TODO: [START] Cambodia version
//            if (mAccountBusiness.isCBNV()) {
//                viewTranslate.setVisibility(View.VISIBLE);
//            } else {
//                viewTranslate.setVisibility(View.GONE);
//            }
            // TODO: [END] Cambodia vesion
//            if (mApplication.getConfigBusiness().isUpdate()) {
//                mViewAboutNew.setVisibility(View.VISIBLE);
//            } else {
//                mViewAboutNew.setVisibility(View.GONE);
//            }
//
//            if (Config.Features.FLAG_SUPPORT_ON_OFF_NOTIFICATION && !NotificationManagerCompat.from(mApplication).areNotificationsEnabled()) {
//                if (mViewSettingNotiNew != null) {
//                    mViewSettingNotiNew.setVisibility(View.VISIBLE);
//                }
//            } else {
//                if (mViewSettingNotiNew != null) {
//                    mViewSettingNotiNew.setVisibility(View.GONE);
//                }
//            }
        }
    }

    public LoginVia isLogin() {
        String mochaToken = ApplicationController.self().getReengAccountBusiness().getToken();
        String openIDToken = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_ACCESS_TOKEN, String.class);
        if (mochaToken == null) mochaToken ="";
        if (openIDToken == null) openIDToken ="";

        if (mochaToken.isEmpty() && openIDToken.isEmpty()) {
            return LoginVia.NOT;
        } else if (mochaToken.isEmpty()) {
            return LoginVia.OPEN_ID;
        } else {
            return LoginVia.MOCHA_OPEN_ID;
        }
    }

    public enum LoginVia{
        MOCHA, OPEN_ID, MOCHA_OPEN_ID, NOT
    }

    private void onShowViewLogged() {
        if (isLogin() != LoginVia.NOT) {
            mViewAccount.setVisibility(View.VISIBLE);
            mViewCallAndMessage.setVisibility(View.VISIBLE);
            mViewImageAndSound.setVisibility(View.VISIBLE);
            mViewNoteMessage.setVisibility(View.VISIBLE);
            mViewNotification.setVisibility(View.VISIBLE);
            mViewSyncContact.setVisibility(View.VISIBLE);
            mViewAboutInfo.setVisibility(View.VISIBLE);
        } else {
            mViewAccount.setVisibility(View.VISIBLE);
            mViewAboutInfo.setVisibility(View.VISIBLE);
            mViewNotification.setVisibility(View.VISIBLE);

            mViewCallAndMessage.setVisibility(View.GONE);
            mViewImageAndSound.setVisibility(View.GONE);
            mViewNoteMessage.setVisibility(View.GONE);
            mViewSyncContact.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.setting_notification:
                mListener.navigateToSettingDetail(Constants.SETTINGS.SETTING_NOTIFICATION);
                break;
            case R.id.setting_call_and_message:
                mListener.navigateToSettingDetail(Constants.SETTINGS.SETTING_CALL_MESSAGE);
                break;
            case R.id.setting_image_and_sound:
                mListener.navigateToSettingDetail(Constants.SETTINGS.SETTING_IMAGE_SOUND);
                break;
//            case R.id.setting_add_friend:
//                if (mListener != null) {
//                    mListener.addNewContact();
//                }
//                break;
//            case R.id.setting_invite_friend:
//                if (mListener != null) {
//                    mListener.inviteFriend();
//                    mParentActivity.trackingEvent(R.string.ga_category_more, R.string.invite, R.string.invite);
//                }
//                break;
            case R.id.btnSyncContact:
                if (NetworkHelper.isConnectInternet(mParentActivity) && !isSyncingContact) {
                    synContactToServer();
                } else {
                    if (tvNoConnection.getVisibility() != View.VISIBLE) {
                        if (getView() == null) return;
                        AnimationUtil.slideDownShow(tvNoConnection, getView().findViewById(R.id.rootView));
                        tvNoConnection.postDelayed(() -> {
                            if (tvNoConnection != null) {
                                AnimationUtil.slideUpHide(tvNoConnection, getView().findViewById(R.id.rootView));
                            }
                        }, 2000);
//                    mParentActivity.showToast(mRes.getString(R.string.no_connectivity_not_feature), Toast.LENGTH_LONG);
                    }
                }

                break;
            case R.id.setting_note_message:
                mListener.navigateToSettingDetail(Constants.SETTINGS.NOTE_MESSAGE);
                break;
            case R.id.setting_feedback:
                //NavigateActivityHelper.navigateToSendEmail(mApplication, mParentActivity);
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    mParentActivity.showDialogLogin();
                } else
                    FeedbackActivity.start(mParentActivity);
                break;
            case R.id.setting_app_info:
                mListener.navigateToSettingDetail(Constants.SETTINGS.ABOUT);
                break;
//            case R.id.setting_delete_avno:
//                mParentActivity.showLoadingDialog("", R.string.loading);
//                AVNOHelper.getInstance(mApplication).deleteAVNONumber(mAccountBusiness.getAVNONumber(), new SimpleResponseListener() {
//                    @Override
//                    public void onSuccess(String msg) {
//                        mParentActivity.hideLoadingDialog();
//                        mParentActivity.showToast(msg);
//                        ReengAccount reengAccount = mAccountBusiness.getCurrentAccount();
//                        reengAccount.setAvnoNumber("");
//
//                        mAccountBusiness.updateReengAccount(reengAccount);
//                    }
//
//                    @Override
//                    public void onError(int code, String msg) {
//                        mParentActivity.hideLoadingDialog();
//                        mParentActivity.showToast(msg);
//                    }
//                });
//                break;
            case R.id.rl_setting_account:
                // TODO: [START] Cambodia version
//                mListener.navigateToSettingDetail(Constants.SETTINGS.SETTING_ACCOUNT);
                mListener.navigateToSettingDetail(Constants.SETTINGS.SETTING_ACCOUNT_SECURITY);
                // TODO: [END] Cambodia version
                break;
        }
    }

    private void setViewListener() {
        mViewAccount.setOnClickListener(this);
//        mImgBack.setOnClickListener(this);
        mViewNotification.setOnClickListener(this);
        mViewCallAndMessage.setOnClickListener(this);
        mViewImageAndSound.setOnClickListener(this);
//        mViewAddFriend.setOnClickListener(this);
//        mViewInvite.setOnClickListener(this);
        mViewSyncContact.setOnClickListener(this);
        mViewNoteMessage.setOnClickListener(this);
//        mViewDeleteAVNO.setOnClickListener(this);
        mViewAboutInfo.setOnClickListener(this);
        //setMessageSettingListener();
        btnSyncContact.setOnClickListener(this);
    }

    // gui toan bo contact len lan dau dang nhap
    private void synContactToServer() {
        Log.d(TAG, "init info ");
        Log.d(TAG, "[Cambodia Version] - [synContactToServer] ");
        logT("synContactToServer > start");
        isSyncingContact = true;
//        mParentActivity.showLoadingDialog(mRes.getString(R.string.setting_sync_contact_content), R.string.waiting);
        progressBarSyncContact.setVisibility(View.VISIBLE);
        progressBarSyncContact.setSmoothProgress(50);
        tvTitleSyncContact.setText(mApplication.getString(R.string.sync_contact_progress, "50%"));
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (!mContactBusiness.isContactReady()) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "InterruptedException", e);
                    }
                }
                // dang nhap bang code hoac bang pass xong thi init lai list contact, (luc nay moi biet so dang nhap
                // dung viettel hay ko)
                mContactBusiness.initArrayListPhoneNumber();
                // gui contact len sv
                setInfoAllPhoneNumber();
            }
        };
        thread.start();
        logT("synContactToServer > thread.start()");
    }

    private void logT(String m) {
        Log.e("TVV-Setting", m);
    }

    public void showDeactiveAccountPopup(final SettingKhActivity acitivity) {
        Resources res = acitivity.getResources();
        new DialogConfirm(acitivity, true).setLabel(res.getString(R.string.warning)).setMessage(res.getString(R
                .string.msg_deactive_account)).
                setNegativeLabel(res.getString(R.string.cancel)).setPositiveLabel(res.getString(R.string.ok)).
                setPositiveListener(result -> {
                    if (mListener != null)
                        mListener.deactiveAccount();
                }).show();
    }

    // api replace so tren sv
    public void setInfoAllPhoneNumber() {
        Log.d(TAG, "[Cambodia Version] - [setInfoAllPhoneNumber] ");
        logT("setInfoAllPhoneNumber > start");
        ContactRequestHelper.getInstance(mApplication).setInfoAllPhoneNumber(new ContactRequestHelper
                .onResponseSetContactListener() {
            @Override
            public void onResponse(ArrayList<PhoneNumber> responses) {
                logT("setInfoAllPhoneNumber > onResponse > successfully");
                Log.d(TAG, "[Cambodia Version] - [setInfoAllPhoneNumber] successfully");
                isSyncingContact = false;
                if (mParentActivity != null)
                    mParentActivity.showToast(mParentActivity.getString(R.string.msg_sync_contact_success), Toast.LENGTH_LONG);
                if (progressBarSyncContact != null) progressBarSyncContact.setVisibility(View.GONE);
                if (tvTitleSyncContact != null && mParentActivity != null)
                    mParentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvTitleSyncContact.setText(R.string.setting_sync_contact_suggest);
                        }
                    });

//                mParentActivity.hideLoadingDialog();
//                mParentActivity.showToast(mRes.getString(R.string.msg_sync_contact_success), Toast.LENGTH_LONG);
                if (mContactBusiness != null) mContactBusiness.updateContactInfo(responses);
                /*ArrayList<PhoneNumber> listPhone = mContactBusiness.getListNumberAlls();
                if (listPhone != null && !listPhone.isEmpty()) {
                    Log.i(TAG, "update list phonumber: " + listPhone.size());
                    ContactSyncHelper contactSyncHelper = new ContactSyncHelper(mApplication);
                    contactSyncHelper.setListPhone(listPhone);
                    contactSyncHelper.start();
                }*/
            }

            @Override
            public void onError(int errorCode) {
                logT("setInfoAllPhoneNumber > onError");
                Log.d(TAG, "[Cambodia Version] - [setInfoAllPhoneNumber] failure");
                isSyncingContact = false;
//                mParentActivity.hideLoadingDialog();
                if (mParentActivity != null)
                    mParentActivity.showToast(mParentActivity.getString(R.string.msg_sync_contact_fail), Toast.LENGTH_LONG);
                if (tvTitleSyncContact != null && mParentActivity != null)
                    mParentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvTitleSyncContact.setText(R.string.setting_sync_contact_suggest);
                        }
                    });

            }
        });
    }

//    private void showViewNoInternetConnection() {
//
//    }
//
//    private void changeLanguage() {
//
//    }

    public interface OnFragmentSettingListener {
        void navigateToSettingDetail(int settingId);

        void addNewContact();

        void inviteFriend();

        void navigateToTranslation();

        void deactiveAccount();

        void navigateToBackup();

        void navigateToHideThread();

        void navigateToLockApp();
    }
}

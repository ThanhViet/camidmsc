package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomTopic {

    @SerializedName("topic_name")
    private String mTopicName;

    @SerializedName("list_films")
    private List<CustomTopicFilm> mListCustomFilm;

    public String getTopicName() {
        return mTopicName;
    }

    public List<CustomTopicFilm> getListCustomFilm() {
        return mListCustomFilm;
    }
}

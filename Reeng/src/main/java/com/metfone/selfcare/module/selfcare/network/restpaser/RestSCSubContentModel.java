package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RestSCSubContentModel extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private RestSCSubListModel data;

    public RestSCSubListModel getData() {
        return data;
    }

    public void setData(RestSCSubListModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCSubModel [data=" + data + "] errror " + getErrorCode();
    }
}

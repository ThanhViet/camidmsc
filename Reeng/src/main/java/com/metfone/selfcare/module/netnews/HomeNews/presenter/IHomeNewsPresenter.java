package com.metfone.selfcare.module.netnews.HomeNews.presenter;

import android.content.Context;

import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;

/**
 * Created by HaiKE on 8/19/17.
 */

public interface IHomeNewsPresenter extends MvpPresenter {

//    void loadData();
//    void loadDataCategory();
//    int loadLastNewsId(SharedPref pref);
    void loadDataCacheV5(SharedPref pref);
//    void loadDataCareCacheV5(SharedPref pref);
    void loadDataV5(boolean refresh);
//    void loadCategoryV5(Context context);
    void loadCanCareV5();
}

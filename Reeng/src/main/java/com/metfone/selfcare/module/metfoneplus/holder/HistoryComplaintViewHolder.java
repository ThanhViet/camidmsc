package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.HistoryComplaint;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class HistoryComplaintViewHolder extends RecyclerView.ViewHolder {
//    public RelativeLayout mComplaintRoot;
//    public RelativeLayout layoutContent;
//    public RelativeLayout mComplaintLayoutContent;
//    public RelativeLayout mComplaintContentExpand;
//    public AppCompatTextView mComplaintPreResult;
//    public AppCompatTextView mComplaintParamCode;
//    public AppCompatImageView mComplaintArrowState;
//    public AppCompatTextView mComplaintAcceptDate;
//    public AppCompatTextView mComplaintProLimitDate;
//    public AppCompatTextView mComplaintTitleStaffInfo;
//    public AppCompatTextView mComplaintStaffInfo;
//    public AppCompatTextView mComplaintCompContent;
//    public AppCompatTextView mComplaintFeedback;
//    public Button mComplaintButtonClose;

    public HistoryComplaint mHistoryComplaint;
    public int position;

    public ConstraintLayout clLayoutContent;
    public ConstraintLayout clLayoutTile;
    public TextView tvPreResult;
    public ImageView ivArrowState;
    public TextView tvCompContent;
    public TextView tvComplaintId;
    public ConstraintLayout clContentExpand;
    public TextView tvAcceptDate;
    public TextView tvProLimitDate;
    public TextView tvStaffInfo;
    public TextView tvServiceTypeName;
    public TextView tvStaffPhone;
    public TextView tvStatus;
    public TextView btnCloseComplaint;
    public TextView tvEndDate;

    public HistoryComplaintViewHolder(@NonNull View itemView) {
        super(itemView);
//        mComplaintRoot = itemView.findViewById(R.id.history_complaint_root);
//        layoutContent = itemView.findViewById(R.id.layout_content);
//        mComplaintLayoutContent = itemView.findViewById(R.id.layout_history_complaint_content);
//        mComplaintContentExpand = itemView.findViewById(R.id.history_complaint_content_expand);
//        mComplaintPreResult = itemView.findViewById(R.id.history_complaint_pre_result);
//        mComplaintParamCode = itemView.findViewById(R.id.history_complaint_param_code);
//        mComplaintArrowState = itemView.findViewById(R.id.history_complaint_arrow_state);
//        mComplaintAcceptDate = itemView.findViewById(R.id.history_complaint_accept_date);
//        mComplaintProLimitDate = itemView.findViewById(R.id.history_complaint_content_pro_limit_date);
//        mComplaintTitleStaffInfo = itemView.findViewById(R.id.history_complaint_title_staff_info);
//        mComplaintStaffInfo = itemView.findViewById(R.id.history_complaint_content_staff_info);
//        mComplaintFeedback = itemView.findViewById(R.id.history_complaint_feedback);
//        mComplaintCompContent = itemView.findViewById(R.id.history_complaint_content);
//        mComplaintButtonClose = itemView.findViewById(R.id.btn_close_feedback);

        clLayoutContent = itemView.findViewById(R.id.layout_content);
        clLayoutTile = itemView.findViewById(R.id.clLayoutTitle);
        tvPreResult = itemView.findViewById(R.id.preResult);
        ivArrowState = itemView.findViewById(R.id.arrowState);
        tvCompContent = itemView.findViewById(R.id.compContent);
        tvComplaintId = itemView.findViewById(R.id.complainId);
        clContentExpand = itemView.findViewById(R.id.contentExpand);
        tvAcceptDate = itemView.findViewById(R.id.acceptDate);
        tvProLimitDate = itemView.findViewById(R.id.proLimitDate);
        tvStaffInfo = itemView.findViewById(R.id.staffInfo);
        tvServiceTypeName = itemView.findViewById(R.id.serviceTypeName);
        tvStaffPhone = itemView.findViewById(R.id.staffPhone);
        tvStatus = itemView.findViewById(R.id.status);
        btnCloseComplaint = itemView.findViewById(R.id.closeComplaint);
        tvEndDate = itemView.findViewById(R.id.tvEndDate);

    }

    public void animateExpand() {
        Animation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        ivArrowState.startAnimation(rotate);
    }

    public void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        ivArrowState.startAnimation(rotate);
    }
}

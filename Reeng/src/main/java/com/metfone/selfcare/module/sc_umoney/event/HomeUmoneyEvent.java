package com.metfone.selfcare.module.sc_umoney.event;

public class HomeUmoneyEvent {
    private String balance;
    private String sdt;

    public HomeUmoneyEvent(String balance, String sdt) {
        this.balance = balance;
        this.sdt = sdt;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}

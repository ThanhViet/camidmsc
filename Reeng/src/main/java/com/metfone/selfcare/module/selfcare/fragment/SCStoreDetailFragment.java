package com.metfone.selfcare.module.selfcare.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.model.SCStore;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SCStoreDetailFragment extends BaseFragment implements OnMapReadyCallback, PermissionHelper.RequestPermissionsResult,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private TextView tvName;
    private TextView tvDistance;
    private TextView tvAddress;
    private TextView tvTime;
    private TextView btnCallNow;
    private TextView btnDirection;
    private ImageView btnBack;

    private SupportMapFragment mapFragment;
    private GoogleMap mMap;

    private Location myLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private SCStore data;

    public static SCStoreDetailFragment newInstance(Bundle bundle) {
        SCStoreDetailFragment fragment = new SCStoreDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCStoreDetailFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_store_detail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);
        loadData();

        initGoogleApiClient();

        if (!hasPermissionLocation()) {
            showDialogRequestPermission();
        }

        return view;
    }

    @Override
    public void onResume() {
        connectLocationUpdates();
        super.onResume();
    }

    private void loadData() {
        Bundle bundle = getArguments();
        data = (SCStore) bundle.getSerializable(SCConstants.KEY_DATA.STORE_DATA);
        if (data != null) {
            tvName.setText(data.getName());
            tvAddress.setText(data.getAddr());
            if(!TextUtils.isEmpty(data.getOpenTime()))
            {
                tvTime.setVisibility(View.VISIBLE);
                tvTime.setText(mActivity.getString(R.string.sc_working_hours, data.getOpenTime()));
            }
            else
                tvTime.setVisibility(View.GONE);
            if(data.getDistance() != null)
            {
                tvDistance.setVisibility(View.VISIBLE);
                tvDistance.setText(SCUtils.numberDistanceFormat(data.getDistance() / 1000) + " km");
            }
            else
                tvDistance.setVisibility(View.GONE);
        }
    }

    private void initView(View view) {
        btnBack = view.findViewById(R.id.btnBack);
        tvName = view.findViewById(R.id.tvName);
        tvDistance = view.findViewById(R.id.tvDistance);
        tvAddress = view.findViewById(R.id.tvAddress);
        tvTime = view.findViewById(R.id.tvTime);
        btnCallNow = view.findViewById(R.id.btnCallNow);
        btnDirection = view.findViewById(R.id.btnDirection);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_send_location_map_layout);
        mapFragment.getMapAsync(this);

        btnCallNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (data != null) {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + (TextUtils.isEmpty(data.getIsdn()) ? SCConstants.SC_CALL_CENTER : data.getIsdn())));
                        startActivity(callIntent);
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    ToastUtils.makeText(mActivity, R.string.permission_activity_notfound);
                }
            }
        });

        btnDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myLocation != null && data != null) {
                    LatLng fromLaLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                    LatLng toLatLng = new LatLng(Double.parseDouble(data.getLatitude()), Double.parseDouble(data.getLongitude()));
                    Uri uri = Uri.parse(LocationHelper.getInstant(ApplicationController.self()).getUrlDirectIntent(fromLaLng, toLatLng));
                    NavigateActivityHelper.navigateToActionView(mActivity, uri);
                } else {
                    ToastUtils.makeText(mActivity, getString(R.string.msg_not_load_location));
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        if(mActivity != null && mActivity instanceof TabSelfCareActivity)
            ((TabSelfCareActivity)mActivity).hideFloatingButton();
    }

    private boolean hasPermissionLocation() {
        return !(PermissionHelper.declinedPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                PermissionHelper.declinedPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION));
    }


    private void showDialogRequestPermission() {
        PermissionHelper.setCallBack(this);
        PermissionHelper.requestPermissionWithGuide(mActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (hasPermissionLocation()) {
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //Your code where exception occurs goes here...
                List<LatLng> locations = new ArrayList<>();
                locations.add(new LatLng(Double.parseDouble(data.getLatitude()), Double.parseDouble(data.getLongitude())));

                for (LatLng latLng : locations) {
                    mMap.addMarker(new MarkerOptions().position(latLng).title(data.getAddr()));
                }

                //LatLngBound will cover all your marker on Google Maps
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(locations.get(0)); //Taking Point A (First LatLng)
                builder.include(locations.get(locations.size() - 1)); //Taking Point B (Second LatLng)
                LatLngBounds bounds = builder.build();
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
                mMap.moveCamera(cu);
                mMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
            }
        });
    }

    @Override
    public void onPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

    }

    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, "handleNewLocation: " + location.getLatitude() + " longitude: " + location.getLongitude());
        myLocation = location;
    }

    @SuppressWarnings({"MissingPermission"})
    private void connectLocationUpdates() {
        if (mGoogleApiClient == null) {
            initGoogleApiClient();
        }
        if (mGoogleApiClient.isConnected()) {
            if (hasPermissionLocation()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        } else {
            mGoogleApiClient.connect();
        }
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (hasPermissionLocation()) {
            Log.d(TAG, "onConnected: +");
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location != null) {
                handleNewLocation(location);
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(mActivity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, "Exception", e);
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            handleNewLocation(location);
        }
    }

    @Override
    public void onDestroyView() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();

            mGoogleApiClient.unregisterConnectionCallbacks(this);
            mGoogleApiClient.unregisterConnectionFailedListener(this);
        }
        super.onDestroyView();
    }
}

package com.metfone.selfcare.module.metfoneplus.topup.interfacelistener;

public interface RunUi {
    void run(Object... params);
}
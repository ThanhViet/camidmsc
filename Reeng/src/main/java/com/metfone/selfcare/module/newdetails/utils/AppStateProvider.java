package com.metfone.selfcare.module.newdetails.utils;

import android.util.Log;

import com.metfone.selfcare.module.newdetails.model.CategoryModel;

import java.util.ArrayList;

/**
 * Created by huongnd on 15/06/17.
 */
public class AppStateProvider {

    private static final String TAG = "AppStateProvider";

    private static AppStateProvider mInstance = null;
    public static int CLICK_CLOSE = 1;
    private int newsCount;
    private String mSortNewsCategory = "";
    private int fontSize = CommonUtils.FONT_SIZE_NORMAL;
    private boolean isAudioMuted = false;
    private boolean isLocationFavourite = false;
    private String campId = "";

    //singleton
    private AppStateProvider() {
        Log.d(TAG, "creator!!");
    }

    ArrayList<CategoryModel> datas = new ArrayList<>();

    public static AppStateProvider getInstance() {
        if (mInstance == null) {
            //prevent multi instance creator in Multi thread.
            synchronized (AppStateProvider.class) {
                if (mInstance == null) {
                    mInstance = new AppStateProvider();
                }
            }
        }
        return mInstance;
    }

    public String getCampId() {
        return campId;
    }

    public void setCampId(String campId) {
        this.campId = campId;
    }

    public boolean isLocationFavourite() {
        return isLocationFavourite;
    }

    public void setLocationFavourite(boolean locationFavourite) {
        isLocationFavourite = locationFavourite;
    }

    public int getNewsCount() {
        return newsCount;
    }

    public void setNewsCount(int newsCount) {
        this.newsCount = newsCount;
    }

    public ArrayList<CategoryModel> getNewsCategoryList() {
        return datas;
    }

    public void setNewsCategoryList(ArrayList<CategoryModel> datas) {
        this.datas = datas;
    }

    public String getSortNewsCategory() {
        return mSortNewsCategory;
    }

    public void setSortNewsCategory(String sortCategory) {
        this.mSortNewsCategory = sortCategory;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public String getSortCategory() {
        return mSortNewsCategory;
    }

    public boolean isAudioMuted() {
        return isAudioMuted;
    }

    public void setAudioMuted(boolean audioMuted) {
        isAudioMuted = audioMuted;
    }
}

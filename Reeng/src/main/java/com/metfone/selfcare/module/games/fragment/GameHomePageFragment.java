package com.metfone.selfcare.module.games.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.tabGame.GameBaseResponseData;
import com.metfone.selfcare.model.tabGame.GameByNameRequest;
import com.metfone.selfcare.model.tabGame.GameCategoryModel;
import com.metfone.selfcare.model.tabGame.GameListResponseData;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.module.games.activity.PlayGameActivity;
import com.metfone.selfcare.module.games.adapter.GameCategoryAdapter;
import com.metfone.selfcare.module.games.adapter.GameNewAdapter;
import com.metfone.selfcare.module.games.adapter.GameTrendingAdapter;
import com.metfone.selfcare.module.games.adapter.OnItemClick;
import com.metfone.selfcare.module.games.api.GameApi;
import com.metfone.selfcare.module.games.viewmodel.GameHomePageModel;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.model.event.OpenRewardCamID;
import com.metfone.selfcare.module.home_kh.model.event.RequestRefreshInfo;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.module.movie.event.UpdateWatchedEvent;
import com.metfone.selfcare.module.movie.model.HomeCinemaDataCache;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;
import static com.metfone.selfcare.module.games.api.GameApi.GET_GAMES;
import static com.metfone.selfcare.module.games.api.GameApi.GET_GAMES_PLAY_MOST_TIME;
import static com.metfone.selfcare.module.games.api.GameApi.GET_GAME_TOP_TEN;
import static com.metfone.selfcare.module.games.api.GameApi.GET_PLAYED_GAMES;

public class GameHomePageFragment extends BaseFragment<GameHomePageModel> implements OnItemClick, GameApi.ListenerGameApi, SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout srlHome;

    private LinearLayout lnRecentlyPlayed, lnNewGames, lnTrendingGames;
    private RecyclerView rvListGame, rvRecentlyPlayed, rvNewGames, rvTrendingGames;

    private LinearLayout lnUser, lnNoData, layout_header_in_tab;
    private TextView tvName, tvRankName, tvCategoryName, btnSignUp;
    private ImageView imgAvatar, imgCrown, btnSearch, btnAchievement, btnBack;

    private RelativeLayout layout_rank, layout_header_out_of_tab;

    private ArrayList<GameModel> listGameRecently, listNewGames;
    public static ArrayList<GameModel> listTrendingGames = new ArrayList<>();
    public static ArrayList<GameCategoryModel> listCategory = new ArrayList<>();

    private UserInfo currentUser;
    private UserInfoBusiness userInfoBusiness;
    private boolean fromClickIconGameHome = false;


    private static final String TAG = GameHomePageFragment.class.getName();

    public GameHomePageFragment() {
    }

    public GameHomePageFragment(boolean fromClickIconGameHome) {
        this.fromClickIconGameHome = fromClickIconGameHome;
    }

    @Override
    protected void initViews() {

        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(token);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", token);
            clipboard.setPrimaryClip(clip);
        }

        srlHome = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        srlHome.setOnRefreshListener(() -> {
            isLoadingRecentlyList = true;
            isLoadingNewGames = true;
            isLoadingTrendingGames = true;
            isLoadingListGame = true;
            srlHome.postDelayed(() -> {
                if (srlHome != null) srlHome.setRefreshing(false);
            }, 2000);
            reLoadData();
        });
        layout_header_in_tab = findViewById(R.id.headerInTab);
        layout_header_out_of_tab = findViewById(R.id.headerOutOfTab);
        btnBack = findViewById(R.id.btn_back);
        if (fromClickIconGameHome) {
            layout_header_in_tab.setVisibility(View.GONE);
            layout_header_out_of_tab.setVisibility(View.VISIBLE);
        } else {
            layout_header_in_tab.setVisibility(View.VISIBLE);
            layout_header_out_of_tab.setVisibility(View.GONE);
        }
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                Objects.requireNonNull(getActivity()).onBackPressed();
            }
        });

        userInfoBusiness = new UserInfoBusiness(getActivity());

        lnTrendingGames = findViewById(R.id.ln_trending_games);
        lnNewGames = findViewById(R.id.ln_new_games);
        lnRecentlyPlayed = findViewById(R.id.ln_recently_played_games);
        rvRecentlyPlayed = findViewById(R.id.rv_recently_played_games);
        rvRecentlyPlayed.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));

        rvNewGames = findViewById(R.id.rv_new_games);
        rvNewGames.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));

        rvTrendingGames = findViewById(R.id.rv_trending_games);
        rvTrendingGames.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));

        rvListGame = findViewById(R.id.recycler_view_list_game);
        rvListGame.setLayoutManager(new LinearLayoutManager(getContext()));

        String currentLang = LocaleManager.getLanguage(mContext);

        layout_rank = findViewById(R.id.layout_rank);
        lnUser = findViewById(R.id.layout_user);
        lnNoData = findViewById(R.id.layout_no_data);
        tvName = findViewById(R.id.tvName);
        tvRankName = findViewById(R.id.tvRankName);
        imgAvatar = findViewById(R.id.img_avatar);
        imgCrown = findViewById(R.id.img_grade);

        imgAvatar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                NavigateActivityHelper.navigateToSetUpProfile(getActivity());
            }
        });
        layout_rank.setOnClickListener(v -> {
            EventBus.getDefault().post(new OpenRewardCamID(TabHomeHelper.HomeTab.tab_game));
        });
        tvName.setOnClickListener(v -> {
            NavigateActivityHelper.navigateToSetUpProfile(getActivity());
        });

        btnSearch = findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SearchGameFragment.class);
            Objects.requireNonNull(getActivity()).startActivity(intent);
        });

        btnAchievement = findViewById(R.id.btn_trophy);
        btnAchievement.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AchievementGameFragment.class);
            Objects.requireNonNull(getActivity()).startActivity(intent);
        });
        isLoadingRecentlyList = true;
        isLoadingNewGames = true;
        isLoadingTrendingGames = true;
        isLoadingListGame = true;
        reLoadData();
    }

    public static boolean isLoadingRecentlyList = true;
    public static boolean isLoadingNewGames = true;
    public static boolean isLoadingTrendingGames = true;
    public static boolean isLoadingListGame = true;

    @Override
    public void onResume() {
        super.onResume();
        reLoadData();
    }

    public void reLoadData(){
        if (ApplicationController.self().isLogin() != FragmentTabHomeKh.LoginVia.NOT) {
            if (isLoadingNewGames)
                mModel.getNewGames();
            if (isLoadingRecentlyList)
                mModel.getRecentlyList();
            if (isLoadingTrendingGames)
                mModel.getTrendingGames();
            if (isLoadingListGame) {
//                Toast.makeText(getActivity(), "API have been called!!!", Toast.LENGTH_SHORT).show();
                mModel.getListGame();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(final UpdateWatchedEvent event) {
        Log.i(TAG, "onEvent UpdateWatchedEvent: " + event);
        EventHelper.removeStickyEvent(event);
    }

    @Override
    public void onRefresh() {
//        if (isLoading) {
//            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
//        } else {
//            HomeCinemaDataCache.setMoviePagerModels(null);
//            if (mDataCinema != null && mDataCinema.size() == 0) {
//                prepareLoadingAnim(false);
//            }
//            isRefresh = true;
//            loadData();
//            AdsManager.getInstance().reloadAdsNative();
//        }
//        EventBus.getDefault().post(new RequestRefreshInfo());
    }

    @Override
    protected Class<GameHomePageModel> getClassViewModel() {
        return GameHomePageModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home_game;
    }

    @Override
    public void onCallBack(String key, Object data) {
        switch (key) {
            case GET_GAMES: {
                GameBaseResponseData gameBaseResponseData = (GameBaseResponseData) data;
                listCategory = new ArrayList<>(Arrays.asList(gameBaseResponseData.getItems()));
                GameCategoryAdapter adapter = new GameCategoryAdapter(listCategory, getContext());
                adapter.setOnItemClick(this);
                rvListGame.setAdapter(adapter);
            }
            break;
            case GET_PLAYED_GAMES: {
                GameBaseResponseData gameBaseResponseData = (GameBaseResponseData) data;
                listGameRecently = new ArrayList<>();
                for (GameCategoryModel items : gameBaseResponseData.getItems()) {
                    listGameRecently.addAll(Arrays.asList(items.getGames()));
                }
                GameNewAdapter adapter = new GameNewAdapter(listGameRecently, getContext());
                adapter.setOnItemClick(this);
                rvRecentlyPlayed.setAdapter(adapter);
                if (listGameRecently.size() > 0) {
                    lnRecentlyPlayed.setVisibility(View.VISIBLE);
                } else
                    lnRecentlyPlayed.setVisibility(View.GONE);
            }
            break;
            case GET_GAME_TOP_TEN: {
                GameListResponseData gameListResponseData = (GameListResponseData) data;
                listNewGames = new ArrayList<>(Arrays.asList(gameListResponseData.getItems()));
                GameNewAdapter adapter = new GameNewAdapter(listNewGames, getContext());
                adapter.setOnItemClick(this);
                rvNewGames.setAdapter(adapter);
                if (listNewGames.size() > 0) {
                    lnNewGames.setVisibility(View.VISIBLE);
                } else
                    lnNewGames.setVisibility(View.GONE);
            }
            break;
            case GET_GAMES_PLAY_MOST_TIME: {
                GameBaseResponseData gameBaseResponseData = (GameBaseResponseData) data;
                listTrendingGames = new ArrayList<>();
                for (GameCategoryModel items : gameBaseResponseData.getItems()) {
                    listTrendingGames.addAll(Arrays.asList(items.getGames()));
                }
                GameTrendingAdapter adapter = new GameTrendingAdapter(listTrendingGames, getContext());
                adapter.setOnItemClick(this);
                rvTrendingGames.setAdapter(adapter);
                if (listTrendingGames.size() > 0) {
                    lnTrendingGames.setVisibility(View.VISIBLE);
                } else
                    lnTrendingGames.setVisibility(View.GONE);
            }
            break;
        }
    }

    @Override
    public void onItemClick(String data) {
        openGame(data);
    }

    private void openGame(String name) {
        GameApi api = GameApi.getInstance();
        api.listenerGameApi = this;
        api.callApi(GameApi.GET_GAMES, new GameByNameRequest(name));
    }

    private void drawProfile(AccountRankDTO accountRankDTO, UserInfo userInfo) {
        tvName.setText(userInfo.getFull_name());
        KhUserRank myRank = KhUserRank.getById(accountRankDTO.rankId);
        if (!StringUtils.isEmpty(userInfo.getAvatar())) {
            Glide.with(this)
                    .load(ImageUtils.convertBase64ToBitmap(userInfo.getAvatar()))
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_default)
                    .error(R.drawable.ic_avatar_default)
                    .into(imgAvatar);
        } else {
            imgAvatar.setImageResource(myRank.resAvatar);
        }
        imgCrown.setImageDrawable(ResourceUtils.getDrawable(myRank.resRank));
        tvRankName.setText(getString(myRank.resRankName));
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void loadInfoUser(final LoadInfoUser event) {
        if (event.loadType == LoadInfoUser.LoadType.LOADING) {
            lnUser.setVisibility(View.GONE);
            lnNoData.setVisibility(View.VISIBLE);
        } else if (event.loadType == LoadInfoUser.LoadType.NOT_LOGIN) {
            lnUser.setVisibility(View.GONE);
            lnNoData.setVisibility(View.GONE);
        } else {
            lnUser.setVisibility(View.VISIBLE);
            lnNoData.setVisibility(View.GONE);
            drawProfile(event.rankDTO, event.userInfo);
        }
    }


    @Override
    public void onPreRequest() {

    }

    @Override
    public void onSuccess(Object responseData) {
        if (responseData instanceof GameBaseResponseData) {
            GameBaseResponseData data = (GameBaseResponseData) responseData;
            Intent intent = new Intent(getContext(), PlayGameActivity.class);
            intent.putExtra("OPEN_GAME", data.getItems()[0].getGames()[0]);
            startActivity(intent);
        }
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onFailure(String exp) {

    }


}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.home_kh.tab.adapter.customtopic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeCustomTopic;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeChannelTopic;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardCategoryList;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardTopics;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardTopics2;
import com.metfone.selfcare.module.metfoneplus.model.MPHomeServiceList;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import butterknife.BindView;

public class BoxContentHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;

    @BindView(R.id.view_require_login)
    View view_require_login;
    @BindView(R.id.btnSignup)
    TextView btnSignup;

    private ArrayList<Object> list;
    private BoxContentAdapter adapter;
    private IHomeModelType data;

    public BoxContentHolder(View view, Activity activity, final TabHomeKhListener.OnAdapterClick listener, int viewType) {
        super(view);
        list = new ArrayList<>();
        adapter = new BoxContentAdapter(activity);
        adapter.setListener(listener);
        adapter.setItems(list);
        if (viewType == IHomeModelType.MP_SERVICE) {
            BaseAdapter.setupGridRecyclerLine(activity, recyclerView, null, adapter, 4, R.dimen.spacing_medium, false);
        } else
            BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, false);
        if (tvTitle != null) tvTitle.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickTitleBox(data, getAdapterPosition());
                }
            }
        });

        btnSignup.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) {
                    listener.onClickSignUp();
                }
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_DRAGGING){
                    listener.onScrollData(data);
                }
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void bindData(Object item, int position) {
        if (list == null) list = new ArrayList<>();
        else list.clear();
        if(item instanceof KhHomeChannelTopic){
            data = (KhHomeChannelTopic)item;
            if (tvTitle != null) tvTitle.setText(KhHomeChannelTopic.titleRes);
            list.addAll(((KhHomeChannelTopic)item).khChannels);
        } else if (item instanceof KhHomeCustomTopic) {
            data = (KhHomeCustomTopic) item;
            list.addAll(((KhHomeCustomTopic) data).topics);
            if (tvTitle != null) {
                KhHomeCustomTopic topic = (KhHomeCustomTopic) data;
                if(!StringUtils.isEmpty(topic.topicTitle)){
                    tvTitle.setText(topic.topicTitle);
                }else{
                    tvTitle.setText(topic.titleRes);
                }
            }

        } else if (item instanceof MPHomeServiceList) {
            data = (MPHomeServiceList) item;
            list.addAll(((MPHomeServiceList) data).serviceItems);
            if (tvTitle != null) tvTitle.setVisibility(View.GONE);

        } else if (item instanceof KhHomeRewardCategoryList) {
            data = (KhHomeRewardCategoryList) item;
            list.addAll(((KhHomeRewardCategoryList) data).categoryItems);
            if (tvTitle != null) tvTitle.setText(((KhHomeRewardCategoryList) data).titleRes);

            if (ApplicationController.self().isLogin() == FragmentTabHomeKh.LoginVia.NOT) {
                view_require_login.setVisibility(View.VISIBLE);
                view_require_login.setOnTouchListener((v, event) -> true);
            } else {
                view_require_login.setVisibility(View.GONE);
            }

        } else if (item instanceof KhHomeRewardTopics) {
            data = (KhHomeRewardTopics) item;
            list.addAll(((KhHomeRewardTopics) data).categoryItems);
            if (tvTitle != null) tvTitle.setVisibility(View.GONE);

        } else if (item instanceof KhHomeRewardTopics2) {
            data = (KhHomeRewardTopics2) item;
            list.addAll(((KhHomeRewardTopics2) data).categoryItems);
            if (tvTitle != null) tvTitle.setVisibility(View.GONE);

        } else {
            data = null;
        }

        if (adapter != null) adapter.notifyDataSetChanged();
    }

    public RecyclerView getRecycler() {
        return recyclerView;
    }

}

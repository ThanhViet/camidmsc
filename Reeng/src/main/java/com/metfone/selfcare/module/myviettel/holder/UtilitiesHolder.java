/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;

import butterknife.BindView;

public class UtilitiesHolder extends BaseAdapter.ViewHolder {
    @BindView(R.id.layout_top_up)
    View viewTopUp;
    @BindView(R.id.layout_charging_history)
    View viewChargingHistory;
    @BindView(R.id.layout_viettel_plus)
    View viewViettelPlus;
    @BindView(R.id.layout_shop)
    View viewShop;

    public UtilitiesHolder(View view, final OnMyViettelListener listener) {
        super(view);
        if (viewTopUp != null)
            viewTopUp.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickTopUp();
                }
            });
        if (viewChargingHistory != null)
            viewChargingHistory.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickChargingHistory();
                }
            });
        if (viewViettelPlus != null)
            viewViettelPlus.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickViettelPlus();
                }
            });
        if (viewShop != null)
            viewShop.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickShop();
                }
            });
    }
}

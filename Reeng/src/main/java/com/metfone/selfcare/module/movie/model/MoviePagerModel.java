/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.model;

import com.metfone.selfcare.model.tabMovie.SubtabInfo;

import java.io.Serializable;
import java.util.ArrayList;

public class MoviePagerModel implements Serializable {

    public static final int TYPE_SLIDER_BANNER = 2;
    public static final int TYPE_BOX_CONTENT = 3;
    public static final int TYPE_BOX_HOT = 4;
    public static final int TYPE_BOX_WATCHED = 5;
    public static final int TYPE_LARGE_ITEM = 6;
    public static final int TYPE_BOX_GRID_ITEM = 7;
    public static final int TYPE_GRID_ITEM = 8;
    public static final int TYPE_WATCHED_ITEM = 9;
    public static final int TYPE_ADS = 10;
    public static final int TYPE_AVAILABLE_NOW = 11;
    public static final int TYPE_WATCHED_BY_FRIEND = 12;
    public static final int TYPE_CATEGORY = 13;
    public static final int TYPE_SLIDER_BANNER_EMPTY = 14;
    public static final int TYPE_CUSTOM_TOPIC = 15;

    private static final long serialVersionUID = -1718647155488830119L;

    private Object object;
    private ArrayList<Object> list;
    private String id;
    private int type;
    private String title;
    private long currentTime;
    private boolean showViewAll;
    private boolean showLine;
    private boolean showRefresh;
    private SubtabInfo subtabInfo;
    private String movieKind;

    public MoviePagerModel() {
        currentTime = System.currentTimeMillis();
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public ArrayList<Object> getList() {
        if (list == null) list = new ArrayList<>();
        return list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isShowViewAll() {
        return showViewAll;
    }

    public void setShowViewAll(boolean showViewAll) {
        this.showViewAll = showViewAll;
    }

    public boolean isShowLine() {
        return showLine;
    }

    public void setShowLine(boolean showLine) {
        this.showLine = showLine;
    }

    public SubtabInfo getSubtabInfo() {
        return subtabInfo;
    }

    public void setSubtabInfo(SubtabInfo subtabInfo) {
        this.subtabInfo = subtabInfo;
    }

    public boolean isShowRefresh() {
        return showRefresh;
    }

    public void setShowRefresh(boolean showRefresh) {
        this.showRefresh = showRefresh;
    }

    public String getMovieKind() {
        return movieKind;
    }

    public void setMovieKind(String movieKind) {
        this.movieKind = movieKind;
    }
}

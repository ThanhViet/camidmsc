package com.metfone.selfcare.module.selfcare.network.request;

import org.json.JSONException;
import org.json.JSONObject;

public class RechargeRequest {

    private String isdn;
    private String desIsdn;
    private String language;
    private String serial;

    public RechargeRequest(String isdn, String desIsdn, String language, String serial) {
        this.isdn = isdn;
        this.desIsdn = desIsdn;
        this.language = language;
        this.serial = serial;
    }

    public String getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msisdn", isdn);
//            jsonObject.put("desIsdn", desIsdn);
//            jsonObject.put("language", language);
            jsonObject.put("pinCode", serial);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.video.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.tabs.TabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.AdsRegisterVip;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.movienew.fragment.SearchMovieNewFragment;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.tabvideo.adapter.MovieAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnTabListener;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.home.fragment.BaseHomeFragment;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class MoviePagerFragment extends BaseHomeFragment implements OnInternetChangedListener
        , SwipeRefreshLayout.OnRefreshListener, BaseAdapter.OnLoadMoreListener, OnClickContentMovie
        , OnClickMoreItemListener, OnTabListener {

    private static final int LIMIT = 20;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingView loadingView;

    private int position = -1;
    private String categoryId = "";
    private VideoApi mVideoApi;

    private boolean isLoading;
    private boolean canLoadMore;

    private ArrayList<Object> data;
    private MovieAdapter adapter;
    private int offset;
    private String lastId;
    private ListenerUtils mListenerUtils;
    private GridLayoutManager layoutManager;

    public static Bundle arguments(int position, int currentPosition, String categoryId) {
        return new Bundler()
                .putInt(Constants.TabVideo.POSITION, position)
                .putInt(Constants.TabVideo.CURRENT_POSITION, currentPosition)
                .putString(Constants.TabVideo.CATEGORY_ID, categoryId)
                .get();
    }

    @OnClick(R.id.btnSearch)
    void onClickSearch() {
        Intent intent = new Intent(getActivity(), SearchMovieNewFragment.class);
        startActivity(intent);
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_pager_content_v2;
    }

    @Override
    public void onCreateView() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryId = bundle.getString(Constants.TabVideo.CATEGORY_ID);
            position = bundle.getInt(Constants.TabVideo.POSITION);
        }
        Log.d(TAG, "onCreateView categoryId: " + categoryId + ", position: " + position);
        loadingView.setOnClickRetryListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData(true);
            }
        });
        swipeRefreshLayout.setColorSchemeColors(getColor());
        swipeRefreshLayout.setOnRefreshListener(this);
        if (data == null) data = new ArrayList<>();
        else data.clear();
        adapter = new MovieAdapter(activity);
        adapter.bindData(data);
        adapter.setListener(this);
        layoutManager = new CustomGridLayoutManager(activity, 2);
        BaseAdapter.setupGridRecycler(activity, recyclerView, layoutManager, adapter, 2, R.dimen.v5_spacing_normal, true);
        BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_video);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        mVideoApi = app.getApplicationComponent().providerVideoApi();
        if (canLazyLoad()) {
            new Handler().postDelayed(() -> loadData(Utilities.isEmpty(data)), Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData(Utilities.isEmpty(data));
        }
    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        if (isLoading) {
            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        } else {
            offset = 0;
            lastId = "";
            loadData(Utilities.isEmpty(data));
        }
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad()) {
            loadData(Utilities.isEmpty(data));
        }
    }

    public void hideRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    private void loadData(boolean showLoading) {
        if (isLoading || mVideoApi == null) return;
        isLoading = true;
        canLoadMore = false;
        if (showLoading) {
            if (loadingView != null) loadingView.showLoading();
            offset = 0;
            lastId = "";
        }
        mVideoApi.getMoviesByCategoryV2(categoryId, offset, LIMIT, lastId, new ApiCallbackV2<ArrayList<Video>>() {
            @Override
            public void onSuccess(String msg, ArrayList<Video> result) throws JSONException {
                if (recyclerView != null) recyclerView.stopScroll();
                if (data == null) data = new ArrayList<>();
                if (offset == 0) {
                    data.clear();
                }
                if (Utilities.notEmpty(result)) {
                    for (Video video : result) {
                        if (data.contains(video)) continue;
                        video.setWatchLater(mVideoApi.isWatchLater(video));
                        data.add(video);
                    }
                    canLoadMore = result.size() >= LIMIT;
                }
                if (adapter != null) {
                    adapter.bindData(data);
                    adapter.notifyDataSetChanged();
                }
                if (Utilities.isEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedEmpty();
                } else {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            }

            @Override
            public void onError(String s) {
                if (offset == 0) {
                    if (data == null) data = new ArrayList<>();
                    data.clear();
                    if (adapter != null) {
                        adapter.bindData(data);
                        adapter.notifyDataSetChanged();
                    }
                }
                if (Utilities.isEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedError();
                } else {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            }

            @Override
            public void onComplete() {
                isLoading = false;
                isDataInitiated = true;
                hideRefresh();
            }
        });
    }

    @Override
    public void onLoadMore() {
        if (canLoadMore && !isLoading) {
            offset = offset + LIMIT;
            Log.i(TAG, "onLoadMore loadData ...");
            loadData(false);
        }
    }

    @Override
    public void onClickMovieItem(Object item, int position) {
        if (item instanceof Video) {
            ((Video) item).setCategoryId(categoryId);
            AdsRegisterVip ads = app.getReengAccountBusiness().showDialogAdsRegisterVip(categoryId);
            if (ads != null) {
                String data = "timestamp=" + System.currentTimeMillis() + "|cateId=" + categoryId;
                ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_POPUP_VIP, data);

                ReportHelper.showDialogFromTabVideo(app, activity, ads,
                        result -> app.getApplicationComponent().providesUtils().openVideoDetail(activity, (Video) item));
            } else {
                app.getApplicationComponent().providesUtils().openVideoDetail(activity, (Video) item);
            }
//            ApplicationController.self().getApplicationComponent().providesUtils().openVideoDetail(activity, (Video) item);
        }
    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {
        if (item instanceof Video) {
//            Movie movie = Video.video2Movie((Video) item);
//            DialogUtils.showOptionVideoMovieItem(activity, movie, this);
            DialogUtils.showOptionVideoItem(activity, (Video) item, this);
        }
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            if (menuId != Constants.MENU.MENU_EXIT && ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                activity.showDialogLogin();
                return;
            }
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_SAVE_VIDEO:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).saveVideoFromMenu((Video) object);
                        activity.showToast(R.string.videoSavedToLibrary);
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).watchLaterVideoFromMenu((Video) object);
                        activity.showToast(R.string.add_later_success);
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (object instanceof Video) {
                        FeedModelOnMedia feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia((Video) object);
                        new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                    break;
            }
        }
    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {

    }

    private void scrollToTop() {
        if (layoutManager != null && recyclerView != null) {
            try {
                recyclerView.stopScroll();
                layoutManager.smoothScrollToPosition(recyclerView, null, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTabReselected(int currentPosition) {
        Log.i(TAG, "onTabReselected currentPosition: " + currentPosition + ", position: " + position + ", isVisibleToUser: " + isVisibleToUser);
        if (currentPosition == position) {
            scrollToTop();
        }
    }

    @Override
    public void onTabSelected(int currentPosition) {
        Log.i(TAG, "onTabSelected currentPosition: " + currentPosition + ", position: " + position + ", isVisibleToUser: " + isVisibleToUser);
        if (currentPosition == position && isVisibleToUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onRefresh();
                }
            }, 600L);
        }
    }

    @Override
    public void onDisableLoading() {

    }
}

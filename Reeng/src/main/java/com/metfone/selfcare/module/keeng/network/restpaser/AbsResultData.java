/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.module.keeng.network.restpaser;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.restful.ErrorMessage;

import java.io.Serializable;

public class AbsResultData implements Serializable {
	private static final long serialVersionUID = -5702141301177401541L;

	@SerializedName("error")
	private ErrorMessage error;

	public ErrorMessage getError() {
		return error;
	}

	public void setError(ErrorMessage error) {
		this.error = error;
	}

	public boolean isWrongToken() {
		if (error != null) {
			return error.isWrongtoken();
		}
		return false;
	}

	public void showErrorMessage(Context context) {
		if (context == null)
			return;
		if (error != null && !TextUtils.isEmpty(error.getMessage()) && !isWrongToken())
			ToastUtils.makeText(context, error.getMessage());
		else
			ToastUtils.makeText(context, R.string.error_message_default);
	}
}

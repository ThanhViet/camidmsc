package com.metfone.selfcare.module.home_kh.tab.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchRewardResponse {
    @SerializedName("errorCode")
    @Expose
    public String errorCode;
    @SerializedName("errorMessage")
    @Expose
    public Object errorMessage;
    @SerializedName("result")
    @Expose
    public Result result;

    public String getErrorCode() {
        return errorCode;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public Result getResult() {
        return result;
    }

    public class Result {

        @SerializedName("wsResponse")
        @Expose
        public WsResponse wsResponse;
        @SerializedName("errorCode")
        @Expose
        public String errorCode;
        @SerializedName("message")
        @Expose
        public String message;
        @SerializedName("userMsg")
        @Expose
        public String userMsg;

        public WsResponse getWsResponse() {
            return wsResponse;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public String getMessage() {
            return message;
        }

        public String getUserMsg() {
            return userMsg;
        }

        public class WsResponse {

            @SerializedName("object")
            @Expose
            public ArrayList<ItemReward> object = null;
            @SerializedName("total")
            @Expose
            public String total;

            public ArrayList<ItemReward> getRewards() {
                return object;
            }

            public String getTotal() {
                return total;
            }
        }

        public class ItemReward implements Serializable {

            @SerializedName("giftId")
            @Expose
            public String giftId;
            @SerializedName("partnerId")
            @Expose
            public String partnerId;
            @SerializedName("partnerName")
            @Expose
            public String partnerName;
            @SerializedName("giftName")
            @Expose
            public String giftName;
            @SerializedName("giftTypeId")
            @Expose
            public String giftTypeId;
            @SerializedName("image")
            @Expose
            public String image;
            @SerializedName("price")
            @Expose
            public String price;
            @SerializedName("score")
            @Expose
            public String score;
            @SerializedName("startDate")
            @Expose
            public String startDate;
            @SerializedName("expireDate")
            @Expose
            public String expireDate;
            @SerializedName("status")
            @Expose
            public String status;
            @SerializedName("updatedTime")
            @Expose
            public String updatedTime;
            @SerializedName("updatedBy")
            @Expose
            public String updatedBy;
            @SerializedName("imageList")
            @Expose
            public List<String> imageList = null;
            @SerializedName("description")
            @Expose
            public String description;
            @SerializedName("storeList")
            @Expose
            public List<String> storeList = null;
            @SerializedName("giftCode")
            @Expose
            public String giftCode;
            @SerializedName("giftLevel")
            @Expose
            public String giftLevel;
            @SerializedName("quantity")
            @Expose
            public Integer quantity;
            @SerializedName("giftOrder")
            @Expose
            public Integer giftOrder;
            @SerializedName("isHot")
            @Expose
            public Integer isHot;
            @SerializedName("isRecommentGift")
            @Expose
            public Integer isRecommentGift;
            @SerializedName("isEmoney")
            @Expose
            public Integer isEmoney;
            @SerializedName("discountRate")
            @Expose
            public String discountRate;
            @SerializedName("rankingList")
            @Expose
            public List<String> rankingList = null;
            @SerializedName("limitedQuantity")
            @Expose
            public Integer limitedQuantity;
            @SerializedName("appliedPeriod")
            @Expose
            public Integer appliedPeriod;
            @SerializedName("isLegend")
            @Expose
            public Integer isLegend;

            public String getGiftId() {
                return giftId;
            }

            public String getPartnerId() {
                return partnerId;
            }

            public String getPartnerName() {
                return partnerName;
            }

            public String getGiftName() {
                return giftName;
            }

            public String getGiftTypeId() {
                return giftTypeId;
            }

            public String getImage() {
                return image;
            }

            public String getPrice() {
                return price;
            }

            public String getScore() {
                return score;
            }

            public String getStartDate() {
                return startDate;
            }

            public String getExpireDate() {
                return expireDate;
            }

            public String getStatus() {
                return status;
            }

            public String getUpdatedTime() {
                return updatedTime;
            }

            public String getUpdatedBy() {
                return updatedBy;
            }

            public List<String> getImageList() {
                return imageList;
            }

            public String getDescription() {
                return description;
            }

            public List<String> getStoreList() {
                return storeList;
            }

            public String getGiftCode() {
                return giftCode;
            }

            public String getGiftLevel() {
                return giftLevel;
            }

            public Integer getQuantity() {
                return quantity;
            }

            public Integer getGiftOrder() {
                return giftOrder;
            }

            public Integer getIsHot() {
                return isHot;
            }

            public Integer getIsRecommentGift() {
                return isRecommentGift;
            }

            public Integer getIsEmoney() {
                return isEmoney;
            }

            public String getDiscountRate() {
                return discountRate;
            }

            public List<String> getRankingList() {
                return rankingList;
            }

            public Integer getLimitedQuantity() {
                return limitedQuantity;
            }

            public Integer getAppliedPeriod() {
                return appliedPeriod;
            }

            public Integer getIsLegend() {
                return isLegend;
            }
        }
    }

}

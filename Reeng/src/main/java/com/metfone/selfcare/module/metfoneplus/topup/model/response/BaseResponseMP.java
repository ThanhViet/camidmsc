package com.metfone.selfcare.module.metfoneplus.topup.model.response;

public class BaseResponseMP<T> {
    private T result;
    private String errorMessage;
    private String errorCode;

    public T getData() {
        return result;
    }

    public void setSignInData(T data) {
        this.result = data;
    }

    public void setData(T data) {
        this.result = data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}

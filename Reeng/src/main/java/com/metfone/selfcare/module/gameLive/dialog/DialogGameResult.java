package com.metfone.selfcare.module.gameLive.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.gameLive.model.LiveQuestionModel;
import com.metfone.selfcare.module.gameLive.model.WinnerModel;
import com.metfone.selfcare.module.gameLive.utils.LiveFormatUtils;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;

public class DialogGameResult extends Dialog implements View.OnClickListener {

    private LiveQuestionModel currentQuestion;
    private ArrayList<WinnerModel> listWinners;
    private TextView tvContent;
    private TextView tvEmpty;
    private RecyclerView recyclerView;
    private GameResultAdapter adapter;
    private BaseSlidingFragmentActivity activity;

    public void setCurrentQuestion(LiveQuestionModel currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public void setListWinners(ArrayList<WinnerModel> listWinners) {
        this.listWinners = listWinners;
    }

    public DialogGameResult(@NonNull Context context) {
        super(context, R.style.DialogFullscreen);
        if (context instanceof BaseSlidingFragmentActivity)
            activity = (BaseSlidingFragmentActivity) context;
    }

    public DialogGameResult(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DialogGameResult(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_game_result);
        tvContent = findViewById(R.id.tvContent);
        tvEmpty = findViewById(R.id.tvEmpty);
        View btnShare = findViewById(R.id.btnShare);
        View btnClose = findViewById(R.id.btnClose);
        recyclerView = findViewById(R.id.recyclerView);

        ImageView bg = findViewById(R.id.iv_background);
        ImageBusiness.setResource(bg, R.drawable.bg_game_live_result);

        if (currentQuestion != null && listWinners != null) {
            if (currentQuestion.getNumber() == 4)
                tvContent.setText(getContext().getResources().getString(R.string.number_special));
            else
                tvContent.setText(getContext().getResources().getString(R.string.number_question, currentQuestion.getNumber()));

            if (Utilities.notEmpty(listWinners)) {
                recyclerView.setVisibility(View.VISIBLE);
                tvEmpty.setVisibility(View.GONE);

                adapter = new GameResultAdapter(activity);
                adapter.setItems(listWinners);
                BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, false);
            } else {
                recyclerView.setVisibility(View.INVISIBLE);
                tvEmpty.setVisibility(View.VISIBLE);
                if (currentQuestion.getNumber() == 4)
                    tvEmpty.setText(getContext().getResources().getString(R.string.live_game_win_special_empty));
                else
                    tvEmpty.setText(getContext().getResources().getString(R.string.live_game_win_empty));
            }

        }
        btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                dismiss();
            }
        });
        btnShare.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                ShareUtils.openShareMenu(activity, currentQuestion.getTitle());
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {

    }


    public class GameResultAdapter extends BaseAdapter<BaseAdapter.ViewHolder, WinnerModel> {
        private static final int TYPE_HEADER = 1;
        private static final int TYPE_NORMAL = 2;

        public GameResultAdapter(Activity activity) {
            super(activity);
        }


        @Override
        public int getItemViewType(int position) {
            WinnerModel item = getItem(position);
            if (item != null) {
                if (position == 0) return TYPE_HEADER;
                else return TYPE_NORMAL;
            }
            return TYPE_EMPTY;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case TYPE_HEADER:
                case TYPE_NORMAL:
                    return new WinnerHolder(layoutInflater.inflate(R.layout.holder_winner_game_live, parent, false));
                default:
                    return new EmptyHolder(layoutInflater, parent);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.bindData(getItem(position), position);
        }

    }

    public class WinnerHolder extends BaseAdapter.ViewHolder {

        @BindView(R.id.layout_root)
        View viewRoot;
        @BindView(R.id.iv_avatar)
        ImageView ivAvatar;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_reward)
        TextView tvReward;

        public WinnerHolder(View view) {
            super(view);
        }

        @Override
        public void bindData(Object item, int position) {
            if (item instanceof WinnerModel) {
                WinnerModel model = (WinnerModel) item;
                tvName.setText(model.getName());
                tvReward.setText(activity.getString(R.string.live_prize, LiveFormatUtils.formatNumber(model.getReward())));
            }
        }
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;
import com.metfone.selfcare.module.myviettel.model.DataPackageInfo;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;

public class MoreDataDetailHolder extends BaseAdapter.ViewHolder {
    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.button_submit)
    TextView btnSubmit;

    private DataPackageInfo data;

    public MoreDataDetailHolder(View view, final OnMyViettelListener listener) {
        super(view);
        if (viewRoot != null) {
            int width = getWidthLayout();
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = width;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
            viewRoot.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickMoreDataInfo(data);
                }
            });
        }
        if (btnSubmit != null)
            btnSubmit.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickBuyMoreData(data);
                }
            });
    }

    public static int getWidthLayout() {
        return (int) ((ApplicationController.self().getWidthPixels() - Utilities.dpToPx(8) * 2) / 2.3);
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof DataPackageInfo) {
            data = (DataPackageInfo) item;
            if (tvTitle != null) tvTitle.setText(data.getPackageCode());
            if (tvDesc != null) tvDesc.setText(Html.fromHtml(data.getDisplay()));
            if (btnSubmit != null) btnSubmit.setText(data.getLabelReg());
        }
    }
}

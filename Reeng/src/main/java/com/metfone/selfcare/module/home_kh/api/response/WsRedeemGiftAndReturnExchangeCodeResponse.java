package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.Exchange;
import com.viettel.util.Log;

public class WsRedeemGiftAndReturnExchangeCodeResponse {
    @SerializedName("errorCode")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("userMsg")
    @Expose
    String userMsg;

    @SerializedName("wsResponse")
    @Expose
    Object wsResponse;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public Object getWsResponse() {
        return wsResponse;
    }

    public void setWsResponse(Object wsResponse) {
        this.wsResponse = wsResponse;
    }

    public Exchange getExChange() {
        if (!(wsResponse instanceof String)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(wsResponse);
                return gson.fromJson(json, Exchange.class);
            } catch (Exception e) {
                Log.e("WsExchangeCodeResponse", wsResponse + " " + e.toString());
            }
        }
        return null;
    }
}

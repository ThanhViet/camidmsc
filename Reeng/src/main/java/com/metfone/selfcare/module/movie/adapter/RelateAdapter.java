/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.content.MovieDetailHolder;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.util.Log;

public class RelateAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {

    private static final int TYPE_GRID_MOVIE = 1;
    private OnClickContentMovie listener;
    private int widthItem;

    public RelateAdapter(Activity act) {
        super(act);
    }

    public void setWidthItem(int widthItem) {
        this.widthItem = widthItem;
    }

    public void setListener(OnClickContentMovie listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof Movie) {
            return TYPE_GRID_MOVIE;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_GRID_MOVIE)
            return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_grid_relate_movie, parent, false), activity, listener, widthItem).setShowPoster(true);

        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

package com.metfone.selfcare.module.home_kh.activity;

import androidx.annotation.RawRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.media.MediaPlayer;
import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.KhmerNewYearGiftHomeFragment;
import com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.KhmerNewYearGiftRequireLoginFragment;
import com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.KhmerNewYearGiftScanFragment;
import com.metfone.selfcare.util.IFragmentBackPress;

import java.util.List;

public class KhmerNewYearGiftActivity extends BaseSlidingFragmentActivity {

    private CamIdUserBusiness mCamIdUserBusiness;
    private ApplicationController applicationController;
    private boolean mIsHaveMetfonePlusEqual1 = false;
    private MediaPlayer mediaPlayer;
    private MediaPlayer subMediaPlayer;

    private boolean isGiftScreen = false;
    private boolean soundIsPlaying = true;
    public boolean shouldPlaySound = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_khmer_new_year_gift);
        setWhiteStatusBar();
        applicationController = (ApplicationController)getApplicationContext();
        mCamIdUserBusiness = applicationController.getCamIdUserBusiness();
        mediaPlayer = MediaPlayer.create(this, R.raw.rodov_ben_pchum_ben_gift);
        mediaPlayer.setLooping(true);

        checkUserLoginWithMetfone();

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.root_frame);
            if(fragment != null){
                if(fragment instanceof KhmerNewYearGiftHomeFragment){
                    if(shouldPlaySound){
                        ((KhmerNewYearGiftHomeFragment)fragment).soundOn();
                    }
                }
                else if(fragment instanceof KhmerNewYearGiftScanFragment){
                    ((KhmerNewYearGiftScanFragment)fragment).resumeScan();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.root_frame);
        if(fragment instanceof IFragmentBackPress){
            boolean shouldExit = ((IFragmentBackPress) fragment).onFragmentBackPressed();
            if(shouldExit){
                super.onBackPressed();
            }
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isGiftScreen){
            if(soundIsPlaying){
                mediaPlayer.start();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
    }

    public void playSound(){
        soundIsPlaying = true;
        mediaPlayer.start();
    }

    public void stopSound(){
        soundIsPlaying = false;
        mediaPlayer.pause();
    }

    private void checkUserLoginWithMetfone() {
        List<Service> services = mCamIdUserBusiness.getServices();

        int sumServiceNumber = 0;
        for (Service s : services) {
            List<PhoneLinked> phoneLinkeds = s.getPhoneLinked();
            for (int i = 0; i < phoneLinkeds.size(); i++) {
                sumServiceNumber++;
            }
        }

        if (sumServiceNumber > 0 ) {
            isGiftScreen = true;
            mediaPlayer.start();
            addFragment(R.id.root_frame, KhmerNewYearGiftHomeFragment.newInstance(), false);
        }
        else {
            addFragment(R.id.root_frame, KhmerNewYearGiftRequireLoginFragment.newInstance(), false);
        }
    }

    public void showError(String message){
        showError(message, getString(R.string.error));
    }

    public void connectionError(){
        showError(getString(R.string.connection_error));
    }

    public void playTabSound(){
        playSound(R.raw.am_thanh_tap);
    }

    public void playWonSound(){
        playSound(R.raw.am_thanh_trung_thuong);
    }

    public void playMessageSound(){
        playSound(R.raw.popup_thong_bao);
    }

    public void playSound(@RawRes int sound){
        if(shouldPlaySound){
            if(subMediaPlayer != null && subMediaPlayer.isPlaying()){
                subMediaPlayer.stop();
            }
            subMediaPlayer = MediaPlayer.create(this, sound);
            subMediaPlayer.start();
        }
    }

}
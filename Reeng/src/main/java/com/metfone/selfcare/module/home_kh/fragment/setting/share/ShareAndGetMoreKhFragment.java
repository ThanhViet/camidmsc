package com.metfone.selfcare.module.home_kh.fragment.setting.share;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.facebook.share.widget.ShareDialog;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.ShareAndGetMoreContract;
import com.metfone.selfcare.listeners.SyncContactListner;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.OpenIdConfig;
import com.metfone.selfcare.module.home_kh.fragment.setting.BaseSettingKhFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ShareAndGetMoreKhFragment extends BaseSettingKhFragment implements InitDataListener
    , ContactChangeListener, ConfigGroupListener, SyncContactListner, android.view.View.OnClickListener
    , ShareAndGetMoreAdapter.IOnItemClickListener, ShareAndGetMoreContract.View {
    private static final String TAG = ShareAndGetMoreKhFragment.class.getSimpleName();
    private static final String TIMES_ONLINE = "TIMES_ONLINE";
    private static final String BONUS_SCORE_SENDER = "BONUS_SCORE_SENDER";
    private static final String BONUS_SCORE_CODE = "BONUS_SCORE_CODE";
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    BottomSheetBehavior.BottomSheetCallback mBottomSheetMoreServiceCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull android.view.View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_DRAGGING
                || newState == BottomSheetBehavior.STATE_SETTLING
                || newState == BottomSheetBehavior.STATE_EXPANDED) {
//                mLayoutTextMoreService.setVisibility(View.INVISIBLE);
            } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
//                mLayoutTextMoreService.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onSlide(@NonNull android.view.View bottomSheet, float slideOffset) {
            if (isAdded()) {
                int colorFrom = getResources().getColor(R.color.m_p_base_black_pearl_opacity_0);
                int colorTo = getResources().getColor(R.color.m_p_base_black_pearl);
//                mBottomSheetServices.setBackgroundColor(Utilities.interpolateColor(slideOffset,
//                        colorFrom, colorTo));
            }
        }
    };
    ShareAndGetMorePresenter shareAndGetMorePresenter;
    private ProgressLoading mPbLoading;
    private ContactBusiness mContactBusiness;
    private String myNumber;
    private List<PhoneNumber> mListPhoneRecent;
    private MessageBusiness mMessageBusiness;
    private ReengAccountBusiness mAccountBusiness;
    private ArrayList<PhoneNumber> mListPhoneSelected = new ArrayList<>();
    private ArrayList<PhoneNumber> mListPhoneSearch = new ArrayList<>();
    private ArrayList<PhoneNumber> mListChecked = new ArrayList<>();
    private ArrayList<PhoneNumber> mListPhoneNumbers = new ArrayList<>();
    private ArrayList<PhoneNumber> mListContactPhoneNumbers;
    private GetListPhoneNumberAsyncTask mGetListAsyncTask;
    private ArrayList<String> mMemberThread = new ArrayList<>();
    private ArrayList<String> mListNumberChecked = new ArrayList<>();
    private Resources mRes;
    private Handler mHandler;
    private FacebookHelper facebookHelper;
    private CallbackManager callbackManager;
    private ShareAndGetMoreAdapter adapter;
    private RecyclerView recyclerView;
    private BottomSheetBehavior mBottomSheetBehavior;
    private ConstraintLayout bottomSheet;
    private RoundTextView btnShare;
    private TextView tvCode, tvShareCode, tvLabelDes;
    private ConstraintLayout layoutShare, layoutShareBlur;
    private UserInfoBusiness userInfoBusiness;
    private UserInfo currentUser;
    private String contentShare;
    private BaseMPConfirmDialog confirmDialogMessenger;

    public static ShareAndGetMoreKhFragment newInstance() {
        ShareAndGetMoreKhFragment fragment = new ShareAndGetMoreKhFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initView(android.view.View view) {
        setTitle("");

    }

    @Override
    public String getName() {
        return "ShareAndGetMoreKhFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_share_and_get_more;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRes = mParentActivity.getResources();
        facebookHelper = new FacebookHelper(mParentActivity);
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void onViewCreated(@NonNull android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        bottomSheet = view.findViewById(R.id.bottom_sheet);
        mPbLoading = view.findViewById(R.id.fragment_choose_number_loading_progressbar);
        recyclerView = view.findViewById(R.id.recycler_view);
        tvCode = view.findViewById(R.id.tvCode);
        tvShareCode = view.findViewById(R.id.tvShareCode);
        recyclerView = view.findViewById(R.id.recycler_view);
        tvLabelDes = view.findViewById(R.id.label_des);
        adapter = new ShareAndGetMoreAdapter(mListPhoneNumbers, getContext(), this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        btnShare = view.findViewById(R.id.btn_share);
        btnShare.setOnClickListener(this);
        layoutShare = view.findViewById(R.id.layout_share);
        layoutShareBlur = view.findViewById(R.id.layout_share_blur);
        layoutShareBlur.setVisibility(android.view.View.GONE);
        layoutShare.setVisibility(android.view.View.GONE);
        layoutShare.setOnClickListener(this);
        view.findViewById(R.id.layout_share_other).setOnClickListener(this);
        view.findViewById(R.id.btn_message).setOnClickListener(this);
        view.findViewById(R.id.btn_facebook).setOnClickListener(this);
        view.findViewById(R.id.btn_copylink).setOnClickListener(this);
        view.findViewById(R.id.btn_more).setOnClickListener(this);
        tvCode.setOnClickListener(this);
        tvShareCode.setOnClickListener(this);
        userInfoBusiness = new UserInfoBusiness(mParentActivity);
        currentUser = userInfoBusiness.getUser();
        tvCode.setText(currentUser.getCode());
        shareAndGetMorePresenter = new ShareAndGetMorePresenter(this);
        shareAndGetMorePresenter.getListConfig();
//        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
//        mBottomSheetBehavior.addBottomSheetCallback(mBottomSheetMoreServiceCallback);
        getData();
    }

    @Override
    public void onResume() {
        mHandler = new Handler();
        ListenerHelper.getInstance().addInitDataListener(this);
        ListenerHelper.getInstance().addSyncContactListner(this);
        if (mApplication.isDataReady()) {
            ListenerHelper.getInstance().addContactChangeListener(this);
            mApplication.getMessageBusiness().addConfigGroupListener(this);
            getListPhoneAsynctask();
        } else {
//            mPbLoading.setVisibility(android.view.View.VISIBLE);
            mPbLoading.setEnabled(true);
        }
        super.onResume();
    }

    private void getData() {
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        // lay danh sach ban be da co trong thread
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mMessageBusiness = mApplication.getMessageBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        myNumber = mAccountBusiness.getJidNumber();
        contentShare = getString(R.string.kh_share_and_get_more_content, currentUser.getCode(), getString(R.string.kh_share_and_get_more_link));
        ArrayList<String> listMember = new ArrayList<>();
//        if (getArguments() != null) {
//            //param1 - contactId
//            listMember = getArguments().getStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER);
//            actionType = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
//            mThreadId = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID);
//            message = (ReengMessage) getArguments().getSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE);
//            mThreadGroup = mMessageBusiness.getThreadById(mThreadId);
//            mRoomId = getArguments().getString(Constants.CHOOSE_CONTACT.DATA_ROOM_ID);
//            mDeepLinkCampaignId = getArguments().getString(Constants.CHOOSE_CONTACT.DATA_DEEPLINK_CAMPAIGN);
//        }
        setMemberInThread(listMember);
    }

    private void getListPhoneAsynctask() {
        logT("getListPhoneAsynctask");
        if (mGetListAsyncTask != null) {
            mGetListAsyncTask.cancel(true);
            logT("getListPhoneAsynctask  > cancel");
            mGetListAsyncTask = null;
        }
        mGetListAsyncTask = new GetListPhoneNumberAsyncTask();
        mGetListAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        logT("getListPhoneAsynctask  > executeOnExecutor");
    }

    @Override
    public void onDataReady() {
        logT("onDataReady");
        if (mHandler == null) {
            return;
        }
        ListenerHelper.getInstance().addContactChangeListener(this);
        mApplication.getMessageBusiness().addConfigGroupListener(this);
        mHandler.post(() -> getListPhoneAsynctask());
    }

    @Override
    public void onContactChange() {
        mHandler.post(this::getListPhoneAsynctask);
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> listNumber) {
        mHandler.post(() -> {
//            if (mAdapter != null)
//                mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onRosterChange() {

    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onConfigGroupChange(ThreadMessage threadMessage, int actionChange) {
//        if (mThreadGroup == null) {
//            return;
//        }
//        if (threadMessage.getId() != mThreadGroup.getId()) {
//            return;
//        }
//        if (actionChange == Constants.MESSAGE.CHANGE_GROUP_MEMBER) {
//            mHandler.post(() -> {
//                ArrayList<String> listMemberThread = mThreadGroup.getPhoneNumbers();
//                setMemberInThread(listMemberThread);
//                getListPhoneAsynctask();
//            });
//        }
    }

    @Override
    public void onConfigRoomChange(String roomId, boolean unFollow) {

    }

    @Override
    public void onSyncCompleted() {
        logT("onSyncCompleted");
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                getListPhoneAsynctask();
            }
        });
    }

    private void share() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "");
            String shareMessage = "\nPlease click link install app\n\n";
//            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
            shareMessage = shareMessage + "https://mocha.com";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
//            startActivity(Intent.createChooser(shareIntent, "choose one"));
            startActivityForResult(Intent.createChooser(shareIntent, "choose one"), 100);
            layoutShareBlur.setVisibility(android.view.View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            logT("onActivityResult > 100");

            logT("onActivityResult > " + resultCode);
            layoutShareBlur.setVisibility(android.view.View.GONE);
        }
    }

    @Override
    public void onClick(android.view.View v) {
        switch (v.getId()) {
            case R.id.tvCode:
                TextHelper.copyToClipboard(mParentActivity, tvCode.getText().toString());
                mParentActivity.showToast(R.string.copy_to_clipboard);
                break;
            case R.id.tvShareCode:
//                share();
                layoutShare.setVisibility(android.view.View.VISIBLE);
                break;
            case R.id.btn_share:
//                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                layoutShare.setVisibility(View.VISIBLE);
                share();


                break;
            case R.id.layout_share_other:
                layoutShare.setVisibility(android.view.View.GONE);
                break;
            case R.id.btn_copylink:
                TextHelper.copyToClipboard(mParentActivity, contentShare);
                mParentActivity.showToast(R.string.copy_to_clipboard);
                layoutShare.setVisibility(android.view.View.GONE);
                break;
            case R.id.btn_facebook:
                layoutShare.setVisibility(android.view.View.GONE);
                ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentTitle(contentShare)
                    .setContentUrl(Uri.parse(getString(R.string.kh_share_and_get_more_link)))
                    .build();
                ShareDialog.show(mParentActivity, content);
                break;
            case R.id.btn_more:
                layoutShare.setVisibility(android.view.View.GONE);
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, contentShare);
                startActivity(Intent.createChooser(intent, getString(R.string.kh_menu_share_more)));
                break;
            case R.id.btn_message: {
                if (!Utilities.isPackageInstalled(getContext(), "com.facebook.orca")) {
                    confirmDialogMessenger = new BaseMPConfirmDialog(R.drawable.img_dialog_2, R.string.info,
                        R.string.warning_messenger_was_not_installed, R.string.download_app, R.string.cancel, view -> {
                        try {
                            getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.facebook.orca")));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.facebook.orca")));
                        }
                        if (confirmDialogMessenger != null) {
                            confirmDialogMessenger.dismiss();
                        }
                    }, view -> {
                        if (confirmDialogMessenger != null) {
                            confirmDialogMessenger.dismiss();
                        }
                    });
                    confirmDialogMessenger.show(getActivity().getSupportFragmentManager(), null);
                    return;
                }
                layoutShare.setVisibility(android.view.View.GONE);
                ShareLinkContent contentMessage = new ShareLinkContent.Builder()
                    .setContentTitle(contentShare)
                    .setContentUrl(Uri.parse(getString(R.string.kh_share_and_get_more_link)))
                    .build();
                MessageDialog.show(mParentActivity, contentMessage);

                break;
            }
        }
    }

    @Override
    public void itemClick(int position) {

    }

    @Override
    public void sendClick(PhoneNumber entry, int position) {
//        ArrayList<String> numbers = new ArrayList<>();
//        numbers.add(entry.getJidNumber());
//        InviteFriendHelper.getInstance().inviteFriends(mApplication, mParentActivity, numbers, true);

        String content = getString(R.string.kh_share_and_get_more_content, currentUser.getCode(), getString(R.string.kh_share_and_get_more_link));
        NavigateActivityHelper.openAppSMS(mParentActivity, entry.getJidNumber(), content);
    }

    public CallbackManager getCallbackManager() {
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
            Log.i(TAG, "reinit callbackmangater");
        }
        return callbackManager;
    }

    private ArrayList<PhoneNumber> filter(ArrayList<PhoneNumber> phoneNumbers) {
        ArrayList<PhoneNumber> result = new ArrayList<>();
        for (int i = 0; i < phoneNumbers.size(); i++) {
            PhoneNumber phoneNumber = phoneNumbers.get(i);
            if (phoneNumber.getJidNumber() != null && !phoneNumber.getJidNumber().isEmpty()) {
                result.add(phoneNumber);
            }
        }
        return result;
    }

    private void logT(String m) {
        Log.e("TVV-ShareAndGetMore", m);
    }

//    private ArrayList<PhoneNumber> getListPhoneNumberInviteRoom(ArrayList<String> listChecked) {
//        ArrayList<PhoneNumber> listAll = new ArrayList<>(mContactBusiness.getListNumberAlls());
//        ArrayList<PhoneNumber> listInviteRoom = new ArrayList<>();
//        if (listAll.isEmpty()) {
//            return listInviteRoom;
//        }
//        for (PhoneNumber phone : listAll) {
//            // so dung reeng
//            if (phone.getId() != null && (phone.isReeng() || phone.isViettel())) {
//                if (myNumber != null && phone.getJidNumber().equals(myNumber)) {
//                    // so chinh minh. khong add
//                } else if (listChecked != null && !listChecked.isEmpty() &&
//                        listChecked.contains(phone.getJidNumber())) {
//                    // so chua co trong thread da duoc chon
//                    phone.setDisable(false);
//                    phone.setChecked(true);
//                    listInviteRoom.add(phone);
//                } else {
//                    // so chua co trong thread va chua dc chon
//                    phone.setDisable(false);
//                    phone.setChecked(false);
//                    listInviteRoom.add(phone);
//                }
//            }
//        }
//
//        Comparator<Object> comparator = ComparatorHelper.getComparatorNumberByName();
//        Collections.sort(listInviteRoom, comparator);
//        listInviteRoom = mContactBusiness.sortNumberByName(listInviteRoom);
//        return listInviteRoom;
//    }
/*

    private void notifiChangeAdapter(ArrayList<PhoneNumber> phoneNumbers, boolean isSetList) {
        mListPhoneSearch = phoneNumbers;
        if (phoneNumbers == null || phoneNumbers.isEmpty()) {
            mTvNote.setVisibility(View.VISIBLE);
        } else {
            mTvNote.setVisibility(View.GONE);
        }
        if (!isSetList) {
            mAdapter.notifyDataSetChanged();
        } else {
            mAdapter.setListPhoneNumbers(phoneNumbers);
            mHeaderAndFooterAdapter.notifyDataSetChanged();
        }
        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        if (isSetList) {
            mRecyclerView.changeAdapter(mAdapter);
        }
    }
*/

    /**
     * lay danh sach contact reeng trong truong hop tao thread
     *
     * @param listChecked
     * @return
     */
    private ArrayList<PhoneNumber> getListPhoneNumberReeng(ArrayList<String> listChecked, boolean isGroupClass) {
        //        ArrayList<PhoneNumber> listAll = mContactBusiness.removeConflictPhoneNumber();
        CopyOnWriteArrayList<PhoneNumber> listAll = new CopyOnWriteArrayList<>(mContactBusiness.getListNumberAlls());
        ArrayList<PhoneNumber> listReeng = new ArrayList<>();
        if (listAll == null || listAll.isEmpty()) {
            return listReeng;
        }
        for (PhoneNumber phone : listAll) {
            // so dung reeng
            if (phone.getId() != null && (phone.isReeng() || (isGroupClass && phone.isViettel()))) {
                if (myNumber != null && phone.getJidNumber().equals(myNumber)) {
                    // so chinh minh. khong add
                } else if (mMemberThread != null && !mMemberThread.isEmpty() &&
                    mMemberThread.contains(phone.getJidNumber())) {
                    // so da co trong thread
//                    if (actionType != Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER) {//block thi  disable
//                        phone.setDisable(true);
//                    }
                    phone.setChecked(true);
                    listReeng.add(phone);
                } else if (listChecked != null && !listChecked.isEmpty() &&
                    listChecked.contains(phone.getJidNumber())) {
                    // so chua co trong thread da duoc chon
                    phone.setDisable(false);
                    phone.setChecked(true);
                    listReeng.add(phone);
                } else {
                    // so chua co trong thread va chua dc chon
                    phone.setDisable(false);
                    phone.setChecked(false);
                    listReeng.add(phone);
                }
            }
        }
        // sort oder by name
        listReeng = mContactBusiness.sortNumberByName(listReeng);
        return listReeng;
    }

    /**
     * lay danh sach contact khong dung reeng trong truong hop invite
     *
     * @param listChecked
     * @return
     */
    private ArrayList<PhoneNumber> getListPhoneNumber(ArrayList<String> listChecked, boolean checkNotReeng) {
        logT("getListPhoneNumber > start");
        if (!mContactBusiness.isContactReady()) {
            mContactBusiness.initContact();
            logT("getListPhoneNumber > mContactBusiness#initContact");
        }
        if (!mContactBusiness.isInitArrayListReady()) {
            mContactBusiness.initArrayListPhoneNumber();
            logT("getListPhoneNumber > mContactBusiness#initArrayListPhoneNumber");
        }

        mListChecked = new ArrayList<>();
        ArrayList<PhoneNumber> listAllOld = mContactBusiness.getListNumberAlls();
        if (listAllOld == null) listAllOld = new ArrayList<>();
        CopyOnWriteArrayList<PhoneNumber> listAll = new CopyOnWriteArrayList<>(listAllOld);
        if (listAll == null || listAll.isEmpty()) {
            return new ArrayList<>();
        }
        ArrayList<PhoneNumber> listNonReeng = new ArrayList<>();
        for (PhoneNumber phone : listAll) {
            // so khong dung reeng
            if (phone.getId() != null &&
                (!checkNotReeng || !phone.isReeng())) {
                if (listChecked != null && !listChecked.isEmpty() && listChecked.contains(phone.getJidNumber())) {
                    // so chua co trong thread da duoc chon
                    phone.setDisable(false);
                    phone.setChecked(true);
                    mListChecked.add(phone);
                }
                listNonReeng.add(phone);
            }
            /*else if (phone.getId() == null && phone.getName() != null &&
                    !phone.getName().contains(rowMochaSupport)) {
                listNonReeng.add(phone);
            }*/
        }
        Comparator<Object> comparator = ComparatorHelper.getComparatorNumberByName();
        Collections.sort(listNonReeng, comparator);
        listNonReeng = mContactBusiness.sortNumberByName(listNonReeng);
        // sort oder by name
        logT("getListPhoneNumber > end");
        return listNonReeng;
    }

    private void setMemberInThread(ArrayList<String> lists) {
        if (lists == null || lists.isEmpty()) {
            mMemberThread = new ArrayList<>();
        } else {
            mMemberThread = lists;
        }
    }

    @Override
    public void setPresenter(ShareAndGetMoreContract.Presenter presenter) {

    }

    @Override
    public void onSuccess(List<OpenIdConfig> listConfigResponse) {
        AtomicReference<String> param1 = new AtomicReference<>("");
        AtomicReference<String> param2 = new AtomicReference<>("");
        AtomicReference<String> param3 = new AtomicReference<>("");
        for (OpenIdConfig openIdConfig : listConfigResponse) {
            if (openIdConfig.getKey().equals(TIMES_ONLINE)) {
                param1.set(openIdConfig.getValue());
            }
            if (openIdConfig.getKey().equals(BONUS_SCORE_SENDER)) {
                param2.set(openIdConfig.getValue());
            }
            if (openIdConfig.getKey().equals(BONUS_SCORE_CODE)) {
                param3.set(openIdConfig.getValue());
            }
        }
        tvLabelDes.setText(getString(R.string.kh_share_and_get_more_des, param1.get(), param2.get(), param3.get()));
    }

    @Override
    public void onFail(String message) {
        ToastUtils.showToast(requireContext(), message);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shareAndGetMorePresenter.onClear();
    }

    private class GetListPhoneNumberAsyncTask extends AsyncTask<Void, Void, ArrayList<PhoneNumber>> {
        @Override
        protected void onPreExecute() {
            mPbLoading.setVisibility(android.view.View.VISIBLE);
            mPbLoading.setEnabled(true);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<PhoneNumber> doInBackground(Void... params) {
            ArrayList<PhoneNumber> numbers = new ArrayList<>();
//            if (mContactBusiness.getListContacs().isEmpty() || mContactBusiness.getListNumbers().isEmpty()) {
//                return new ArrayList<>();
//            }
            numbers = getListPhoneNumber(mListNumberChecked, true);
            ArrayList<PhoneNumber> result = new ArrayList<>();
            //add contact
            if (Utilities.notEmpty(numbers)) {
                PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), -1);
                result.add(sectionContacts);
                result.addAll(numbers);
            }
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<PhoneNumber> params) {
            mPbLoading.setVisibility(android.view.View.GONE);
            mPbLoading.setEnabled(false);

            logT("GetListPhoneNumberAsyncTask > params: " + params.size());
            mListPhoneNumbers = filter(params);
            if (mListPhoneNumbers == null || mListPhoneNumbers.isEmpty()) {
                logT("GetListPhoneNumberAsyncTask > Empty");
//                mTvNote.setText(mRes.getString(R.string.list_empty));
            } else {
                logT("GetListPhoneNumberAsyncTask > not find");
//                mTvNote.setText(mRes.getString(R.string.not_find));
            }

            logT("GetListPhoneNumberAsyncTask > size: " + mListPhoneNumbers.size());
            adapter.addData(mListPhoneNumbers);
//            adapter.notifyDataSetChanged();

            //kiem tra search box xem da nhap text chua
//            String contentSearch = mSearchView.getText().toString().trim();
//            if (!TextUtils.isEmpty(contentSearch)) {
//                searchContactAsyncTask(contentSearch);
//            } else {
//                notifiChangeAdapter(mListPhoneNumbers, true);
//            }
//            initHeaderView();
//            if (actionType == Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN
//                    && REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId))
//                selectedAllContact();
            super.onPostExecute(params);
        }
    }
}

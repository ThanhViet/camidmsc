/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.holder;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.module.movie.adapter.BoxContentAdapter;
import com.metfone.selfcare.module.movie.listener.LoadmoreListener;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;

import java.util.ArrayList;

import butterknife.BindView;

public class BoxContentHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;
    @BindView(R.id.rootView)
    RelativeLayout rootView;

    private ArrayList<Object> list;
    private BoxContentAdapter adapter;
    private MoviePagerModel data;
    private int mAdapterPosition;
    private Activity activity;

    public BoxContentHolder(View view, Activity activity, final TabMovieListener.OnAdapterClick listener,
                            int viewType, LoadmoreListener.ILoadmoreAtPositionListener loadmoreAtPositionListener) {
        super(view);
        this.activity = activity;
        list = new ArrayList<>();
        adapter = new BoxContentAdapter(activity, viewType, new LoadmoreListener.IStartLoadmoreListener() {
            @Override
            public void startLoadmore() {
                loadmoreAtPositionListener.loadmoreAtPosition(mAdapterPosition, adapter);
            }
        });
        adapter.setListener(listener);
        adapter.setItems(list);
        BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, true);
        if (tvTitle != null) tvTitle.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickTitleBox(data.getSubtabInfo(), getAdapterPosition());
                }
            }
        });
    }

    @Override
    public void bindData(Object item, int position) {
        mAdapterPosition = position;
        if (list == null) list = new ArrayList<>();
        else list.clear();
        if (item instanceof MoviePagerModel) {
            data = (MoviePagerModel) item;
            list.addAll(data.getList());
            if (tvTitle != null) {
                if (StringUtils.isEmpty(data.getMovieKind())) {
                    tvTitle.setText(data.getTitle());
                } else {
                    if (data.getMovieKind().equals(MovieKind.TYPE_TRENDING)) {
                        tvTitle.setText(activity.getString(R.string.cinema_trending));
                    } else if (data.getMovieKind().equals(MovieKind.TYPE_MOVIE4YOU)) {
                        tvTitle.setText(activity.getString(R.string.cinema_movie_for_you));
                    }
                    else if (data.getMovieKind().equals(MovieKind.TYPE_WATCHED)) {
                        tvTitle.setText(activity.getString(R.string.cinema_continue_watching));
                    }else if(data.getMovieKind().equals(MovieKind.TYPE_RECENTLYADDED)){
                        tvTitle.setText(activity.getString(R.string.cinema_recently_added));
                    }
                }
            }
            adapter.setBlockName(data.getTitle());
        } else {
            data = null;
        }
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    public RecyclerView getRecycler() {
        return recyclerView;
    }
    public int getHeight(){
        if(rootView != null){
            return rootView.getMeasuredHeight();
        }
        return 0;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/12
 *
 */

package com.metfone.selfcare.module.video.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.video.adapter.VideoPagerAdapter;
import com.metfone.selfcare.module.video.listener.TabVideoListener;
import com.metfone.selfcare.module.video.model.TabModel;
import com.metfone.selfcare.module.video.model.VideoPagerModel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.home.fragment.BaseHomeFragment;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;

public class CategoryDetailFragment extends BaseHomeFragment implements OnInternetChangedListener
        , SwipeRefreshLayout.OnRefreshListener, TabVideoListener.OnAdapterClick
        , BaseAdapter.OnLoadMoreListener, OnClickMoreItemListener {

    private static final int LIMIT = 20;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingView loadingView;


    private VideoApi mVideoApi;

    private boolean isLoading;
    private boolean onRefresh = false;
    private boolean isLoadingChannel;
    private boolean canLoadMore;
    private VideoPagerModel dataChannel;
    private ArrayList<VideoPagerModel> data;
    private VideoPagerAdapter adapter;
    private ListenerUtils mListenerUtils;
    private int offset;
    private String lastId;
    private LinearLayoutManager layoutManager;
    private TabModel tabInfo;
    private ApiCallbackV2 apiCallbackVideo = null;
    private ApiCallbackV2<ArrayList<Channel>> apiCallbackChannel = null;

    public static CategoryDetailFragment newInstance(@NonNull TabModel tabInfo) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.KEY_DATA, tabInfo);
        CategoryDetailFragment fragment = new CategoryDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_pager_content_v2;
    }

    @Override
    public void onCreateView() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            tabInfo = (TabModel) bundle.getSerializable(Constants.KEY_DATA);
        }
        loadingView.setOnClickRetryListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData(true);
                loadChannel();
            }
        });
        swipeRefreshLayout.setColorSchemeColors(getColor());
        swipeRefreshLayout.setOnRefreshListener(this);
        if (data == null) data = new ArrayList<>();
        else data.clear();
        adapter = new VideoPagerAdapter(activity);
        adapter.setItems(data);
        adapter.setListener(this);
        layoutManager = new CustomLinearLayoutManager(activity, LinearLayout.VERTICAL, false);
        BaseAdapter.setupVerticalRecyclerView(activity, recyclerView, layoutManager, adapter, true, 2);
        BaseAdapter.setRecyclerViewLoadMore(recyclerView, this);
    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_video);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        mVideoApi = app.getApplicationComponent().providerVideoApi();
        new Handler().postDelayed(() -> {
            loadData(Utilities.isEmpty(data));
            loadChannel();
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData(Utilities.isEmpty(data));
            loadChannel();
        }
    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        if (isLoading) {
            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        } else {
            onRefresh = true;
            offset = 0;
            lastId = "";
            loadData(Utilities.isEmpty(data));
            loadChannel();
        }
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad()) {
            loadData(Utilities.isEmpty(data));
            loadChannel();
        }
    }

    public void hideRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    private void initVideoCallback() {
        if (apiCallbackVideo == null) {
            apiCallbackVideo = new ApiCallbackV2<ArrayList<Object>>() {
                @Override
                public void onSuccess(String title, ArrayList<Object> result) throws JSONException {
                    if (recyclerView != null) recyclerView.stopScroll();
                    if (data == null) data = new ArrayList<>();
                    boolean addHeader = false;
                    if (offset == 0) {
                        data.clear();
                        if (dataChannel != null) {
                            data.add(0, dataChannel);
                            addHeader = true;
                        }
                    }
                    if (Utilities.notEmpty(result)) {
                        if (addHeader) {
                            VideoPagerModel model = new VideoPagerModel();
                            model.setType(VideoPagerModel.TYPE_HEADER_LIST_VIDEO);
                            model.setTitle(tabInfo.getTitle());
                            data.add(model);
                        }
                        for (Object item : result) {
                            if (item != null) {
                                VideoPagerModel model = new VideoPagerModel();
                                model.setType(VideoPagerModel.TYPE_VIDEO_NORMAL);
                                model.setObject(item);
                                data.add(model);
                            }
                        }
                        canLoadMore = result.size() >= LIMIT;
                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                    if (Utilities.isEmpty(data)) {
                        if (loadingView != null) loadingView.showLoadedEmpty();
                    } else {
                        if (loadingView != null) loadingView.showLoadedSuccess();
                    }
                }

                @Override
                public void onError(String s) {
                    if (offset == 0) {
                        if (data == null) data = new ArrayList<>();
                        data.clear();
                        if (dataChannel != null) data.add(0, dataChannel);
                        if (adapter != null) adapter.notifyDataSetChanged();
                    }
                    if (Utilities.isEmpty(data)) {
                        if (loadingView != null) loadingView.showLoadedError();
                    } else {
                        if (loadingView != null) loadingView.showLoadedSuccess();
                    }
                }

                @Override
                public void onComplete() {
                    onRefresh = false;
                    isLoading = false;
                    isDataInitiated = true;
                    hideRefresh();
                }
            };
        }
    }

    private void loadData(boolean showLoading) {
        if (isLoading || mVideoApi == null) return;
        isLoading = true;
        canLoadMore = false;
        if (showLoading) {
            if (loadingView != null) loadingView.showLoading();
            offset = 0;
            lastId = "";
        }
        initVideoCallback();
        switch (tabInfo.getType()) {
            case TabModel.TYPE_RECOMMEND_VIDEO:
                mVideoApi.getSuggestVideo(tabInfo.getId(), offset, LIMIT, onRefresh, apiCallbackVideo);
                break;
            case TabModel.TYPE_HOT_VIDEO:
                mVideoApi.getVideosByCategoryV2(tabInfo.getId(), offset, LIMIT, lastId, "1", apiCallbackVideo);
                break;
            case TabModel.TYPE_NEW_VIDEO:
                mVideoApi.getVideosByCategoryV2(tabInfo.getId(), offset, LIMIT, lastId, "2", apiCallbackVideo);
                break;
        }
    }

    private void initChannelCallback() {
        if (apiCallbackChannel == null) {
            apiCallbackChannel = new ApiCallbackV2<ArrayList<Channel>>() {
                @Override
                public void onSuccess(String title, ArrayList<Channel> result) throws JSONException {
                    if (data == null) data = new ArrayList<>();
                    int index = 0;
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getType() == VideoPagerModel.TYPE_SUGGEST_LIST) {
                            index = i + 1;
                        } else if (data.get(i).getType() == VideoPagerModel.TYPE_CHANNEL_LIST) {
                            data.remove(i);
                            break;
                        } else if (i >= 2) {
                            break;
                        }
                    }
                    if (Utilities.notEmpty(result)) {
                        dataChannel = new VideoPagerModel();
                        dataChannel.setType(VideoPagerModel.TYPE_CHANNEL_LIST);
                        if (TextUtils.isEmpty(title)) {
                            if (res != null)
                                dataChannel.setTitle(res.getString(R.string.trending_channel));
                        } else
                            dataChannel.setTitle(title);
                        dataChannel.getList().addAll(result);
                        data.add(index, dataChannel);
                    } else {
                        dataChannel = null;
                    }
                    if (adapter != null) adapter.notifyDataSetChanged();
                }

                @Override
                public void onError(String s) {

                }

                @Override
                public void onComplete() {
                    isLoadingChannel = false;
                    isDataInitiated = true;
                    if (Utilities.notEmpty(data)) {
                        if (loadingView != null) loadingView.showLoadedSuccess();
                    }
                }
            };
        }
    }

    private void loadChannel() {
        if (!tabInfo.isLoadChannel() || isLoadingChannel || mVideoApi == null) return;
        isLoadingChannel = true;
        initChannelCallback();
        mVideoApi.getHotChannel(false, tabInfo.getId(), 0, LIMIT, apiCallbackChannel);
    }

    @Override
    public void onClickTitleBox(VideoPagerModel item, int position) {

    }

    @Override
    public void onClickChannelItem(Object item, int position) {
        if (item instanceof Channel) {
            app.getApplicationComponent().providesUtils().openChannelInfo(activity, (Channel) item);
        }
    }

    @Override
    public void onClickMoreChannelItem(Object item, int position) {
        if (item instanceof Channel) {
            DialogUtils.showOptionChannelItem(activity, item, this);
        }
    }

    private void onClickVideo(final Video video) {
        if (video == null || activity == null || app == null) return;
        app.getApplicationComponent().providesUtils().openVideoDetail(activity, video);
    }

    @Override
    public void onClickVideoItem(Object item, int position) {
        if (item instanceof VideoPagerModel) {
            VideoPagerModel model = (VideoPagerModel) item;
            if (model.getObject() instanceof Video) {
                onClickVideo((Video) model.getObject());
            }
        } else if (item instanceof Video) {
            onClickVideo((Video) item);
        }
    }

    @Override
    public void onClickMoreVideoItem(Object item, int position) {
        if (item instanceof VideoPagerModel) {
            VideoPagerModel model = (VideoPagerModel) item;
            if (model.getObject() instanceof Video) {
                final Video video = (Video) model.getObject();
                DialogUtils.showOptionVideoItem(activity, video, this);
            }
        } else if (item instanceof Video) {
            DialogUtils.showOptionVideoItem(activity, (Video) item, this);
        }
    }

    @Override
    public void onClickChannelVideoItem(Object item, int position) {
        if (item instanceof VideoPagerModel) {
            VideoPagerModel model = (VideoPagerModel) item;
            if (model.getObject() instanceof Video) {
                Channel channel = ((Video) model.getObject()).getChannel();
                if (channel != null)
                    app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
            }
        } else if (item instanceof Video) {
            Channel channel = ((Video) item).getChannel();
            if (channel != null)
                app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
        }
    }

    @Override
    public void onLoadMore() {
        if (canLoadMore && !isLoading) {
            offset = offset + LIMIT;
            Log.i(TAG, "onLoadMore loadData ...");
            loadData(false);
        }
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_SAVE_VIDEO:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).saveVideoFromMenu((Video) object);
                        activity.showToast(R.string.videoSavedToLibrary);
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).watchLaterVideoFromMenu((Video) object);
                        activity.showToast(R.string.add_later_success);
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (object instanceof Video) {
                        FeedModelOnMedia feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia((Video) object);
                        new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) throws JSONException {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                    break;
            }
        }
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.holder;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV3;
import com.metfone.selfcare.ui.view.LinkTextView;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;

public class InfoHolder extends BaseAdapterV3.ViewHolder implements LinkTextView.OnReadMoreListener, LinkTextView.OnLinkListener {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_info)
    LinkTextView tvInfo;

    private BaseSlidingFragmentActivity activity;
    private String key;
    private String value;

    public InfoHolder(BaseSlidingFragmentActivity activity, LayoutInflater layoutInflater, ViewGroup parent) {
        super(layoutInflater.inflate(R.layout.item_film_info, parent, false));
        tvInfo.setOnReadMoreListener(this);
        tvInfo.setOnLinkListener(this);
        tvInfo.setLengthReadMore(150);
        tvInfo.setColorReadMore(R.color.white);
        this.activity = activity;
    }

    @Override
    public void bindData(ArrayList<Object> items, int position) {
        super.bindData(items, position);
        Object item = items.get(position);
        if (item instanceof Map) {

            Map<String, String> map = (Map<String, String>) item;
            for (Map.Entry<String, String> entry : map.entrySet()) {
                key = entry.getKey();
                value = entry.getValue();
                tvName.setText(key);
                tvInfo.asyncSetText(value, false);
                break;
            }
        }
    }

    @Override
    public void onLink(String content, int type) {
        if (type == Constants.SMART_TEXT.TYPE_MOCHA) {
            DeepLinkHelper.getInstance().openSchemaLink(activity, content);
        } else if (type == Constants.SMART_TEXT.TYPE_URL) {
            Utilities.openLink(activity, content);
        }
    }

    @Override
    public void onReadMore() {
        if (tvInfo == null) return;
        tvInfo.asyncSetText(value, false);
    }
}

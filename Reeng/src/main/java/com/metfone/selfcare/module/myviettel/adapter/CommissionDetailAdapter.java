/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/5
 *
 */

package com.metfone.selfcare.module.myviettel.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.holder.CommissionDetailHolder;
import com.metfone.selfcare.module.myviettel.holder.HistoryDetailHolder;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;

public class CommissionDetailAdapter extends BaseAdapter<BaseAdapter.ViewHolder, DataChallenge> {
    public static final int TYPE_NORMAL = 1;

    private boolean isHistory;

    public CommissionDetailAdapter(Activity activity, boolean isHistory) {
        super(activity);
        this.isHistory = isHistory;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_NORMAL) {
            if (isHistory)
                return new HistoryDetailHolder(layoutInflater.inflate(R.layout.holder_history_detail_mvt_dc, parent, false), activity);
            else
                return new CommissionDetailHolder(layoutInflater.inflate(R.layout.holder_commission_detail_mvt_dc, parent, false), activity);
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(getItem(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        DataChallenge item = getItem(position);
        if (item != null) return TYPE_NORMAL;
        return TYPE_EMPTY;
    }
}

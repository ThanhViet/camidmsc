package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentHistoryItem {
    private String paymentAccount;
    private String ftthAccount;
    private double amount;
    private String ftthName;
    private int contractIdInfo;
    private String paymentDate;

    public String getPaymentAccount() {
        if (paymentAccount == null) return "";
        return paymentAccount;
    }

    public void setPaymentAccount(String paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public String getFtthAccount() {
        if (ftthAccount == null) return "";
        return ftthAccount;
    }

    public void setFtthAccount(String ftthAccount) {
        this.ftthAccount = ftthAccount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getFtthName() {
        if (ftthName == null) return "";
        return ftthName;
    }

    public void setFtthName(String ftthName) {
        this.ftthName = ftthName;
    }

    public int getContractIdInfo() {
        return contractIdInfo;
    }

    public void setContractIdInfo(int contractIdInfo) {
        this.contractIdInfo = contractIdInfo;
    }

    public String getPaymentDate() {
        if (paymentDate == null) return "";
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }
}


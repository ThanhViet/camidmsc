package com.metfone.selfcare.module.netnews.EventNews.presenter;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.module.netnews.EventNews.fragment.EventFragment;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.netnews.request.NormalRequest;
import com.metfone.selfcare.module.response.EventResponse;
import com.metfone.selfcare.util.Log;

/**
 * Created by HaiKE on 8/19/17.
 */

public class EventPresenter extends BasePresenter implements IEventPresenter {
    public static final String TAG = EventPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;

    public EventPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadData(int page) {
        mNewsApi.getEventList(new NormalRequest(page, 10), callback);
    }

    HttpCallBack callback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            if (!isViewAttached() || !(getMvpView() instanceof EventFragment)) {
                return;
            }
            Gson gson = new Gson();
            EventResponse childNewsResponse = gson.fromJson(data, EventResponse.class);

            ((EventFragment) getMvpView()).bindData(childNewsResponse);
            ((EventFragment) getMvpView()).loadDataSuccess(true);
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof EventFragment)) {
                return;
            }
            ((EventFragment) getMvpView()).loadDataSuccess(false);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
}

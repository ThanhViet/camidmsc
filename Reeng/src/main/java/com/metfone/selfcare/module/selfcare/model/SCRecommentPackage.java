package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SCRecommentPackage implements Serializable {

    @SerializedName("groupName")
    @Expose
    private String groupName;

    @SerializedName("groupCode")
    @Expose
    private String groupCode;

    @SerializedName("groupType")
    @Expose
    private String groupType;

    @SerializedName("vasPackages")
    @Expose
    private ArrayList<SCPackage> data = new ArrayList<>();


    public ArrayList<SCPackage> getData() {
        return data;
    }

    public void setData(ArrayList<SCPackage> data) {
        this.data = data;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    @Override
    public String toString() {
        return "SCRecommentPackage{" +
                "groupName='" + groupName + '\'' +
                ", groupCode='" + groupCode + '\'' +
                ", data=" + data +
                '}';
    }
}

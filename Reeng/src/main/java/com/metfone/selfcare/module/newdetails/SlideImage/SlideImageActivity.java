package com.metfone.selfcare.module.newdetails.SlideImage;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.module.newdetails.model.ListImageModel;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;

public class SlideImageActivity extends BaseSlidingFragmentActivity {

    private SlideImageAdapter adapter;
    private ViewPager viewPager;
    private TextView tvCount;
    private ImageView btnClose, btnShare, btnDownload;
    private Resources mRes;
    private ArrayList<ItemContextMenu> mOverFlowItems = new ArrayList<>();
    private EventOnMediaHelper eventOnMediaHelper;
    String imgUrl = "";
    ListImageModel listImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_image);
        eventOnMediaHelper = new EventOnMediaHelper(this);
        mRes = getResources();
        viewPager = (ViewPager) findViewById(R.id.pager);
        tvCount = findViewById(R.id.tvCount);
        btnClose = findViewById(R.id.btnClose);
        btnDownload = findViewById(R.id.ab_download_btn);
        btnShare = findViewById(R.id.img_ab_share);
        btnShare.setOnClickListener(onClickShareListener);
        btnDownload.setOnClickListener(clickDownloadListener);

        Intent i = getIntent();
        int position = i.getIntExtra("position", 0);
        listImage = (ListImageModel) i.getSerializableExtra("listImage");

        if (listImage != null && listImage.getListImage() != null)
            tvCount.setText((position + 1) + " / " + listImage.getListImage().size());

        if (listImage != null && listImage.getListImage() != null)
            imgUrl = listImage.getListImage().get(position);
        Log.i(TAG, "share image SlidImageActivity 0:" + imgUrl);
        adapter = new SlideImageAdapter(SlideImageActivity.this, listImage == null ? new ArrayList<String>() : listImage.getListImage());
        viewPager.setAdapter(adapter);

        // displaying selected image first
        viewPager.setCurrentItem(position);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                imgUrl = listImage.getListImage().get(position);
                Log.i(TAG, "share image SlidImageActivity " + position + ": " + imgUrl);
                if (listImage != null && listImage.getListImage() != null)
                    tvCount.setText((position + 1) + " / " + listImage.getListImage().size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        setUp();
    }

    protected void setUp() {
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initBottomSheet() {
        mOverFlowItems.clear();
        // TODO: 6/21/2019 thainn chinh sua option
//        ItemContextMenu sendImgToFriend = new ItemContextMenu(this, mRes.getString(R.string.menu_send_message),
//                R.drawable.ic_share_send_message, null, Constants.MENU.SEND_IMAGE_TO_FRIEND);
//        mOverFlowItems.add(sendImgToFriend);
        ItemContextMenu postImgOnSocial = new ItemContextMenu(mRes.getString(R.string.post_on_social),
                R.drawable.ic_post_on_social, null, Constants.MENU.POS_ON_SOCIAL);
        mOverFlowItems.add(postImgOnSocial);
        ItemContextMenu shareExternal = new ItemContextMenu(mRes.getString(R.string.share_external),
                R.drawable.ic_onmedia_share_v2, null, Constants.MENU.SHARE_EXTERNAL);
        mOverFlowItems.add(shareExternal);
    }

    private static final String TAG = "SlideImageActivity";
    View.OnClickListener clickDownloadListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (listImage != null && listImage.getListImage() != null && !listImage.getListImage().isEmpty()) {
                ImageBusiness.downloadImagePreview(SlideImageActivity.this, listImage.getListImage().get(viewPager.getCurrentItem()));
            }

        }
    };

    View.OnClickListener onClickShareListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                showDialogLogin();
            else {
                downloadImageAndShare(imgUrl);
            }
        }

    };

    public void downloadImageAndShare(final String imageUrl) {
        final ApplicationController application = ApplicationController.self();
        if (application.isDataReady()) {
            showLoadingDialog("", R.string.processing);
            try {
                Glide.with(application).asBitmap().load(imageUrl).into(new SimpleTarget<Bitmap>() {

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        hideLoadingDialog();
                        showToast(R.string.request_send_error);
                        Log.d(TAG, "download Image onLoadFailed");
                    }

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (application == null) return;
                        File recordDir = new File(Config.Storage.GALLERY_MOCHA);
                        if (!recordDir.exists()) {
                            recordDir.mkdirs();
                        }
                        String imagePath = application.getExternalCacheDir() + "/cache_img_" + System.currentTimeMillis() + ".jpg";
                        File file = ImageHelper.saveBitmapToFile(resource, imagePath, Bitmap.CompressFormat.JPEG);
                        hideLoadingDialog();
                        if (file != null && file.isFile()) {
                            ReengMessage message = new ReengMessage();
                            message.setMessageType(ReengMessageConstant.MessageType.image);
                            message.setFilePath(imagePath);
                            message.setForwardingMessage(false);
                            ShareContentBusiness business = new ShareContentBusiness(SlideImageActivity.this, message);
                            business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_FROM_IMAGE_PREVIEW);
                            business.setTitleDialogChooseContact(getString(R.string.share));
                            business.showPopupShareContent();
                        } else {
                            showToast(R.string.request_send_error);
                        }
                        Log.d(TAG, "download Image onResourceReady imagePath: " + imagePath);

                    }
                });
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                if (resultCode == RESULT_OK && intent != null) {
                    int threadId = intent.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                }
                break;

            default:
                break;
        }
    }
}

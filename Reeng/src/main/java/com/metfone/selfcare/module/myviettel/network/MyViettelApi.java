/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.myviettel.network;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.myviettel.model.AccountInfo;
import com.metfone.selfcare.module.myviettel.model.CaptchaModel;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;
import com.metfone.selfcare.module.myviettel.model.DataChallengeProvisional;
import com.metfone.selfcare.module.myviettel.model.DataPackageInfo;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyViettelApi extends BaseApi {

    public static final String API_GET_LIST_DATA_PACKAGES_FOR_MOCHA = "/ReengBackendBiz/myviettel/getPackageForMocha";
    public static final String API_GET_LIST_DATA_PACKAGES_BY_TYPE = "/ReengBackendBiz/myviettel/getDataByType";
    public static final String API_REGISTER_DATA = "/ReengBackendBiz/myviettel/registerData";
    public static final String API_PAYMENT_ONLINE_FOR_MOCHA = "/ReengBackendBiz/myviettel/paymentOnlineV2ForMocha";
    public static final String API_GET_ACCOUNT_INFO = "/ReengBackendBiz/myviettel/checkKTTK";
    public static final String API_GET_CAPTCHA = "/ReengBackendBiz/myviettel/getCaptcha";
    public static final String API_GET_REGISTERED_VAS_INFO = "/ReengBackendBiz/myviettel/getRegistedVasInfo";

    //todo api Thử thách data //commission
    public static final String API_XHBH_GET_TOTAL_COMMISSION = "/ReengBackendBiz/myviettel/xhbh/v1/total/commission";    //Api kiểm tra thuê bao A (bao gồm cả thông tin hoa hồng tổng trong tháng hiện tại)
    public static final String API_XHBH_GET_SUGGEST_USER = "/ReengBackendBiz/myviettel/xhbh/v1/list/msisdn";    //Api gợi ý user phù hợp
    public static final String API_XHBH_GET_SUGGEST_DATA = "/ReengBackendBiz/myviettel/xhbh/v1/data/package";    //Api hiển thị các gói cước phù hợp theo user
    public static final String API_XHBH_CHECK_USER = "/ReengBackendBiz/myviettel/xhbh/v1/search/msisdn-b";    //Api kiểm tra thuê bao B có thỏa mãn để được mời hay không
    public static final String API_XHBH_INVITE_USER = "/ReengBackendBiz/myviettel/xhbh/v1/invite";    //Api gửi lời mời đến user
    public static final String API_XHBH_GET_DETAIL_COMMISSION = "/ReengBackendBiz/myviettel/xhbh/v1/detail/commission";    //Api tra cứu tiền thưởng
    public static final String API_XHBH_GET_HISTORY = "/ReengBackendBiz/myviettel/xhbh/v1/history";    //Api tra cứu lịch sử
    public static final String API_XHBH_REGISTER = "/ReengBackendBiz/myviettel/xhbh/v1/register";    //Api đăng ký tham gia chương trình

    private final int TIME_OUT = 30;
    private String domain;

    public MyViettelApi() {
        super(ApplicationController.self());
        domain = getDomainFile();
    }

    public void getAccountInfo(@Nullable final ApiCallback<AccountInfo> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_GET_ACCOUNT_INFO;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_GET_ACCOUNT_INFO);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + clientType
                + revision + token + timestamp, token);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                AccountInfo model = gson.fromJson(jsonObject.optString("data"), AccountInfo.class);
                if (model != null) {
                    model.setDesc(jsonObject.optString("desc"));
                }
                if (apiCallback != null) apiCallback.onSuccess(response, model);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void getListDataPackagesByType(String type, @Nullable final ApiCallback<ArrayList<DataPackageInfo>> apiCallback) {
        //type: HOT: top offer, DATAPLUS: mua thêm
        final long startTime = System.currentTimeMillis();
        //String TAG_REQUEST = "API_GET_LIST_DATA_PACKAGES_BY_TYPE";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_GET_LIST_DATA_PACKAGES_BY_TYPE);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + clientType
                + revision + type + token + timestamp, token);
        builder.putParameter("type", type);
        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                ArrayList<DataPackageInfo> list = new ArrayList<>();
                JSONArray jsonArray = new JSONObject(response).optJSONArray("data");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String jsonTmp = jsonArray.optString(i);
                        if (Utilities.notEmpty(jsonTmp)) {
                            try {
                                DataPackageInfo model = gson.fromJson(jsonTmp, DataPackageInfo.class);
                                if (model != null) list.add(model);
                            } catch (Exception e) {
                                Log.e("MyViettelApi", e);
                            }
                        }
                    }
                }
                if (apiCallback != null) apiCallback.onSuccess(response, list);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        //builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void registerData(String type, String packageCode, @Nullable final ApiCallback apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_REGISTER_DATA;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = post(domain, API_REGISTER_DATA);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + clientType
                + revision + type + packageCode + token + timestamp, token);
        builder.putParameter("type", type);
        builder.putParameter("pack_code", packageCode);
        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                String msg = new JSONObject(response).optString("desc");
                if (TextUtils.isEmpty(msg)) {
                    msg = application.getString(R.string.e601_error_but_undefined);
                }
                if (apiCallback != null) apiCallback.onSuccess(msg, response);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void paymentOnline(String cardCode, String captcha, String sid, String receiveMsisdn
            , @Nullable final ApiCallback<Boolean> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_PAYMENT_ONLINE_FOR_MOCHA;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = post(domain, API_PAYMENT_ONLINE_FOR_MOCHA);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + clientType
                + revision + cardCode + captcha + sid + receiveMsisdn + token + timestamp, token);
        builder.putParameter("cardcode", cardCode);
        builder.putParameter("captcha", captcha);
        builder.putParameter("sid", sid);
        builder.putParameter("receiveMsisdn", receiveMsisdn);
        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                String msg = jsonObject.optString("desc");
                if (TextUtils.isEmpty(msg)) {
                    msg = application.getString(R.string.e601_error_but_undefined);
                }
                String code = jsonObject.optString("code");
                boolean isSuccess = "200".equals(code);
                if (apiCallback != null) apiCallback.onSuccess(msg, isSuccess);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void getCaptcha(@Nullable final ApiCallback<CaptchaModel> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_GET_CAPTCHA;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_GET_CAPTCHA);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + clientType
                + revision + token + timestamp, token);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                if (apiCallback != null)
                    apiCallback.onSuccess(response, gson.fromJson(jsonObject.optString("data"), CaptchaModel.class));
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void getRegisteredVasInfo(@Nullable final ApiCallback apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_GET_REGISTERED_VAS_INFO;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_GET_REGISTERED_VAS_INFO);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + clientType
                + revision + token + timestamp, token);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null) apiCallback.onSuccess(response, response);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    //todo DC <=> DataChallenge: Thử thách data
    public void registerDC(@Nullable final ApiCallback<Boolean> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_XHBH_REGISTER;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = post(domain, API_XHBH_REGISTER);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + msisdn
                + token + timestamp, token);

        builder.putParameter("msisdn_a", msisdn);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                int errorCode = jsonObject.optInt("errorCode");
                String msg = jsonObject.optString("message");
                if (TextUtils.isEmpty(msg))
                    msg = application.getString(R.string.e601_error_but_undefined);
                if (apiCallback != null)
                    apiCallback.onSuccess(msg, (errorCode == 0 || errorCode == 1));
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void getTotalCommissionDC(@Nullable final ApiCallback<String> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_XHBH_GET_TOTAL_COMMISSION;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_XHBH_GET_TOTAL_COMMISSION);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String phoneNumber = msisdn;
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + phoneNumber
                + token + timestamp, token);

        builder.putParameter("msisdn_a", phoneNumber);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                /*
                    {
                        "errorCode": 0,
                        "message": "Bạn có thể gửi lời mời đến thuê bao khác",
                        "rule": "Lưu ý: Tiền thưởng tối đa trong tháng 2.000.000 đ", //Tổng số tiền hoa hồng tối đa trong tháng
                        "totalCommission": 0.0 //Tổng hoa hồng giao dịch thành công trong tháng
                    }
                */
                JSONObject jsonObject = new JSONObject(response);
                String totalCommission = jsonObject.optString("totalCommision");
                if (apiCallback != null) apiCallback.onSuccess(response, totalCommission);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void getDetailCommissionDC(long startDate, long endDate, @Nullable final ApiCallback<DataChallengeProvisional> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_XHBH_GET_DETAIL_COMMISSION;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_XHBH_GET_DETAIL_COMMISSION);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String phoneNumber = msisdn;
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + phoneNumber + startDate
                + endDate + token + timestamp, token);

        builder.putParameter("msisdn_check", phoneNumber);
        builder.putParameter("startDate", String.valueOf(startDate));
        builder.putParameter("endDate", String.valueOf(endDate));

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                int errorCode = jsonObject.optInt("errorCode");
                String msg = jsonObject.optString("message");

                int totalRecords = jsonObject.optInt("totalRecords");
                int totalCommission = jsonObject.optInt("totalCommisions");
                ArrayList<DataChallenge> list = gson.fromJson(jsonObject.optString("invitations"), new TypeToken<ArrayList<DataChallenge>>() {
                }.getType());
                DataChallengeProvisional model = new DataChallengeProvisional();
                model.setData(list);
                model.setTotalCommission(totalCommission);
                model.setTotalTransaction(totalRecords);
                if (apiCallback != null) apiCallback.onSuccess(msg, model);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void getSuggestUserDC(String phoneNumbers, @Nullable final ApiCallback<ArrayList<String>> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_XHBH_GET_SUGGEST_USER;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_XHBH_GET_SUGGEST_USER);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + phoneNumbers
                + token + timestamp, token);

        builder.putParameter("listMsisdn", phoneNumbers);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                int errorCode = jsonObject.optInt("errorCode");
                String msg = jsonObject.optString("message");
                if (TextUtils.isEmpty(msg))
                    msg = application.getString(R.string.e601_error_but_undefined);
                ArrayList<String> list = gson.fromJson(jsonObject.optString("listIsdn"), new TypeToken<ArrayList<String>>() {
                }.getType());
                if (apiCallback != null) apiCallback.onSuccess(msg, list);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void getSuggestDataDC(String phoneNumber, @Nullable final ApiCallback<DataChallengeProvisional> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_XHBH_GET_SUGGEST_DATA;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_XHBH_GET_SUGGEST_DATA);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + phoneNumber
                + token + timestamp, token);

        builder.putParameter("msisdn_b", phoneNumber);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                int errorCode = jsonObject.optInt("errorCode");
                String msg = jsonObject.optString("message");
                ArrayList<DataChallenge> list = gson.fromJson(jsonObject.optString("dataPackages"), new TypeToken<ArrayList<DataChallenge>>() {
                }.getType());
                DataChallengeProvisional model = new DataChallengeProvisional();
                model.setData(list);
                model.setSuccess(errorCode == 0);
                if (apiCallback != null) apiCallback.onSuccess(msg, model);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void checkUserDC(String phoneNumber, @Nullable final ApiCallback apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_XHBH_CHECK_USER;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_XHBH_CHECK_USER);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + phoneNumber
                + token + timestamp, token);

        builder.putParameter("msisdn_b", phoneNumber);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                int errorCode = jsonObject.optInt("errorCode");
                String msg = jsonObject.optString("message");
                if (TextUtils.isEmpty(msg))
                    msg = application.getString(R.string.e601_error_but_undefined);
                if (apiCallback != null) apiCallback.onSuccess(msg, errorCode);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void inviteUserDC(String phoneNumber, String dataPackage, @Nullable final ApiCallback apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_XHBH_INVITE_USER;
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = post(domain, API_XHBH_INVITE_USER);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + msisdn + phoneNumber
                + dataPackage + token + timestamp, token);

        builder.putParameter("msisdn_a", msisdn);
        builder.putParameter("msisdn_b", phoneNumber);
        builder.putParameter("datapackage", dataPackage);

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                int errorCode = jsonObject.optInt("errorCode");
                String msg = jsonObject.optString("message");
                if (apiCallback != null) apiCallback.onSuccess(msg, errorCode);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

    public void getHistoryDC(long startDate, long endDate, int limit, int offset, int status
            , @Nullable final ApiCallback<DataChallengeProvisional> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = API_XHBH_GET_HISTORY;
        Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, API_XHBH_GET_HISTORY);
        String timestamp = String.valueOf(startTime);
        String msisdn = getReengAccountBusiness().getJidNumber();
        String phoneNumber = msisdn;
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String token = getReengAccountBusiness().getToken();
        String security = HttpHelper.encryptDataV2(application, msisdn + phoneNumber + startDate
                + endDate + limit + offset + status + token + timestamp, token);

        builder.putParameter("msisdn_check", phoneNumber);
        builder.putParameter("startDate", String.valueOf(startDate));
        builder.putParameter("endDate", String.valueOf(endDate));
        builder.putParameter("limit", String.valueOf(limit));
        builder.putParameter("offset", String.valueOf(offset));
        builder.putParameter("status", String.valueOf(status));

        builder.putParameter("msisdn", msisdn);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("security", security);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                //final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response);
                int errorCode = jsonObject.optInt("errorCode");
                String msg = jsonObject.optString("message");
                int totalRecords = jsonObject.optInt("totalRecords");
                int totalCommission = jsonObject.optInt("totalCommisions");
                ArrayList<DataChallenge> list = gson.fromJson(jsonObject.optString("invitations"), new TypeToken<ArrayList<DataChallenge>>() {
                }.getType());
                DataChallengeProvisional model = new DataChallengeProvisional();
                model.setData(list);
                model.setTotalCommission(totalCommission);
                model.setTotalTransaction(totalRecords);
                if (apiCallback != null) apiCallback.onSuccess(msg, model);
            }

            @Override
            public void onFailure(String message) {
                //final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        builder.execute();
    }

}

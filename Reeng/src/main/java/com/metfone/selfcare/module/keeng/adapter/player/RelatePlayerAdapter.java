package com.metfone.selfcare.module.keeng.adapter.player;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class RelatePlayerAdapter extends BaseAdapter<BaseHolder> {
    private ArrayList<MediaModel> data;
    private Context context;
    private BaseListener.OnClickMedia listener;
    private int currentIndex = 0;

    public RelatePlayerAdapter(Context context, String ga_source) {
        super(context, ga_source);
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if (data != null && data.size() > 0)
            return data.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        MediaModel item = getItem(position);
        if (item == null) {
            return BaseAdapterRecyclerView.ITEM_EMPTY;
        }
        return BaseAdapterRecyclerView.ITEM_MEDIA_SONG_PLAYER;
    }

    public MediaModel getItem(int position) {
        if (data != null)
            try {
                return data.get(position);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        return null;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view;
        switch (type) {
            case BaseAdapterRecyclerView.ITEM_MEDIA_SONG_PLAYER:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song_player, parent, false);
                return new MediaPlayerHolder(view, listener);

            case BaseAdapterRecyclerView.ITEM_MEDIA_SONG_PLAYER_LOSSLESS:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_media_song_player, parent, false);
                return new MediaPlayerHolder(view, listener);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_empty, parent, false);
                return new BaseHolder(view, listener);
        }
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (holder instanceof MediaPlayerHolder) {
            MediaModel item = getItem(position);
            MediaPlayerHolder itemHolder = (MediaPlayerHolder) holder;
            itemHolder.bind(mContext, item);
            if (currentIndex == position) {
                itemHolder.tvName.setTextColor(mContext.getResources().getColor(R.color.v5_main_color));
                itemHolder.tvName.setSelected(true);
            } else {
                itemHolder.tvName.setTextColor(mContext.getResources().getColor(R.color.v5_text));
                itemHolder.tvName.setSelected(false);
            }
        }
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public void setData(ArrayList<MediaModel> data) {
        this.data = data;
    }

    public void setOnclickListener(BaseListener.OnClickMedia listener) {
        this.listener = listener;
    }

    public class MediaPlayerHolder extends BaseHolder implements View.OnClickListener {
        public TextView tvName, tvSinger, tvListenNo;
        public ImageView image;
        public View btnOption, viewSocial;
        protected MediaModel item;
        private View viewMedia;

        public MediaPlayerHolder(View itemView, BaseListener.OnClickMedia listener) {
            super(itemView, listener);
            image = itemView.findViewById(R.id.image);
            tvName = itemView.findViewById(R.id.title);
            tvSinger = itemView.findViewById(R.id.singer);
            tvListenNo = itemView.findViewById(R.id.listen_no);
            btnOption = itemView.findViewById(R.id.button_option);
            viewMedia = itemView.findViewById(R.id.layout_media);
            if (viewMedia != null)
                viewMedia.setOnClickListener(this);
            if (btnOption != null)
                btnOption.setOnClickListener(this);
        }

        public void bind(Context context, MediaModel media) {
            this.item = media;
            if (context == null || item == null) {
                if (viewMedia != null)
                    viewMedia.setVisibility(View.GONE);
                return;
            }
            int type = item.getType();
            tvName.setText(item.getName());
            tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            tvSinger.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            if (TextUtils.isEmpty(item.getSinger()))
                tvSinger.setVisibility(View.GONE);
            else {
                tvSinger.setVisibility(View.VISIBLE);
                tvSinger.setText(item.getSinger());
            }

//            if (item.getListened() < 1) {
//                tvListenNo.setVisibility(View.GONE);
//            } else if (item.getListened() == 1) {
//                tvListenNo.setText(context.getString(R.string.m_listen_no, "1"));
//                tvListenNo.setVisibility(View.VISIBLE);
//            } else {
//
//                tvListenNo.setText(context.getString(R.string.m_listens_no, item.getListenNo()));
//                tvListenNo.setVisibility(View.VISIBLE);
//            }
            btnOption.setVisibility(View.VISIBLE);
            switch (type) {
                case Constants.TYPE_SONG:
                    ImageBusiness.setSong(image, item.getImage(), getAdapterPosition(), Utilities.dpToPx(19));
//                    if (isHasLossless() && item.isLossless())
//                        tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.ic_lossless), null);
                    if (item.isMonopoly())
                        tvSinger.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_docquyen), null, null, null);
                    break;
                default:
                    ImageBusiness.setImage(item.getImage(), image, getAdapterPosition());
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_option:
                    if (listener != null)
                        listener.onMediaExpandClick(v, getAdapterPosition());
                    return;
                case R.id.layout_media:
                    if (listener != null)
                        listener.onMediaClick(v, getAdapterPosition());
                    return;
                default:
                    return;
            }

        }
    }
}

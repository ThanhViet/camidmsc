package com.metfone.selfcare.module.tiin.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.response.ErrorResponse;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;

import java.util.ArrayList;

public class HomeTiinResponse extends ErrorResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<HomeTiinModel> list = new ArrayList<>();

    public ArrayList<HomeTiinModel> getData() {
        return list;
    }

    public void setData(ArrayList<HomeTiinModel> data) {
        this.list = data;
    }

    public HomeTiinResponse() {
    }
}

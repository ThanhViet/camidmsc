package com.metfone.selfcare.module.netnews.EditCategoryNews.fragment;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.module.netnews.EditCategoryNews.adapter.EditCategoryNewsAdapterV5;
import com.metfone.selfcare.module.netnews.EditCategoryNews.presenter.EditCategoryNewsPresenter;
import com.metfone.selfcare.module.netnews.EditCategoryNews.presenter.IEditCategoryNewsPresenter;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.model.FavouriteModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.response.CategoryResponse;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;
import com.metfone.selfcare.v5.widget.SwitchButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditChildCategoriesFragment extends BaseFragment implements AbsInterface.OnItemSettingCategoryListener, ClickListener.IconListener {
    @BindView(R.id.list_view)
    ListView listView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.sc_location_new)
    SwitchButton scLocation;

    ArrayList<CategoryModel> datas = new ArrayList<>();
    boolean currentStateFavorite = false; // check trang thai thay doi favorite
    boolean currentStateLocation = false; // check trang thai thay doi location
    private Resources mRes;
    private ApplicationController mApplication;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1438;
    private boolean checkLocation = false;
    private FavouriteModel oldFavorite, currentFavorite;
    private EditCategoryNewsAdapterV5 adapterV5;

    public static EditChildCategoriesFragment newInstance() {
        EditChildCategoriesFragment fragment = new EditChildCategoriesFragment();
        fragment.mPresenter = new EditCategoryNewsPresenter();
        return fragment;
    }

    IEditCategoryNewsPresenter mPresenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mApplication = (ApplicationController) context.getApplicationContext();
        mRes = getContext().getApplicationContext().getResources();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_category, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        setUp(view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        CategoryResponse dataCategory = SharedPrefs.getInstance().get(SharedPrefs.LIST_CATEGORY_NEW_V5, CategoryResponse.class);
        oldFavorite = SharedPrefs.getInstance().get("FAVOURITE", FavouriteModel.class);
        if (oldFavorite == null) {
            oldFavorite = new FavouriteModel();
        }

        if (oldFavorite.getData().size() > 0 && dataCategory != null && dataCategory.getData().size() > 0) {
            for (CategoryModel model : dataCategory.getData()) {
                for (String i : oldFavorite.getData()) {
                    if (i.equals(model.getId() + "")) {
                        model.setFollow(true);
                        break;
                    }
                }
            }
        }
        if (dataCategory == null) {
            datas = new ArrayList<>();
        } else {
            datas = dataCategory.getData();
        }

//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new CustomLinearLayoutManager(getBaseActivity(), LinearLayoutManager.VERTICAL, false));
//        adapter = new EditCategoryNewsAdapter(getBaseActivity(), datas, this);
//        recyclerView.setAdapter(adapter);
//        ItemDragAndSwipeCallback itemDragAndSwipeCallback = new ItemDragAndSwipeCallback(adapter);
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemDragAndSwipeCallback);
//        itemTouchHelper.attachToRecyclerView(recyclerView);
        adapterV5 = new EditCategoryNewsAdapterV5(getBaseActivity(), this);
        adapterV5.setListItem(datas);
        listView.setAdapter(adapterV5);

//        checkOnOffLocation();
//        scLocation.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
//                if (!isChecked) {
//                    if (!checkLocation) {
//                        LocationHelper.getInstant(ApplicationController.self()).showDialogSettingLocationProvidersNews(getBaseActivity(), EditChildCategoriesFragment.this);
//                    }
//                } else {
//                    if (!checkLocation) {
//                        if (ContextCompat.checkSelfPermission(getBaseActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                            requestPermissions(new String[]{
//                                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
//                        } else {
//                            // No explanation needed, we can request the permission.
//                            checkLocation = true;
//
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    scLocation.setChecked(false);
//                                }
//                            }, 200);
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    checkLocation = false;
//                                }
//                            }, 700);
//                            try {
//                                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
//                                        Constants.ACTION.ACTION_SETTING_LOCATION_ON);
//                            } catch (ActivityNotFoundException ex) {
//                                Log.e(TAG, "Exception" + ex);
//                            }
//                        }
//                    }
//
//                }
//            }
//        });
    }

    private void checkOnOffLocation() {
        if (LocationHelper.getInstant(ApplicationController.self()).checkGooglePlayService()) {
            if (ContextCompat.checkSelfPermission(getBaseActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                scLocation.setChecked(false);
            } else {
                if (!LocationHelper.getInstant(ApplicationController.self()).isLocationServiceEnabled()) {
                    scLocation.setChecked(false);
                } else if (!LocationHelper.getInstant(ApplicationController.self()).isNetworkLocationEnabled()) {
                    scLocation.setChecked(false);
                } else {
                    scLocation.setChecked(true);
                    LocationHelper.getInstant(ApplicationController.self()).getCurrentLatLng(new LocationHelper.GetLocationListener() {
                        @Override
                        public void onResponse(Location location) {
                            Log.d(TAG, "loadListStrangerAround getCurrentLatLng: onResponse: " + location.getLatitude() + "," +
                                    location.getLongitude());
                            LocationHelper.getInstant(ApplicationController.self()).setGetLocationListener(null);
                            LocationHelper.getInstant(ApplicationController.self()).saveLastLocationToPref(location);
                            //todo luu location mới để lần sau gọi request
                        }

                        @Override
                        public void onFail() {
                            Log.d(TAG, "loadListStrangerAround: onFail");
                        }
                    });
                }
            }
        } else {
            scLocation.setChecked(false);
        }
    }

    @OnClick(R.id.btnBack)
    public void onBackClick() {
        if (getBaseActivity() != null) {
            checkChangeFavorite();
            if (currentStateLocation) {
                AppStateProvider.getInstance().setLocationFavourite(true);
                getBaseActivity().finish();
            }
            if (currentStateFavorite) {
                showDialogSave();
                return;
            }
            getBaseActivity().finish();
        }
    }

    @Override
    public void onItemRemoveClick(int pos) {
        CategoryModel categoryModel = datas.get(pos);
        categoryModel.setFollow(false);
    }

    @Override
    public void onItemAddClick(int pos) {
        CategoryModel categoryModel = datas.get(pos);
        categoryModel.setFollow(true);
    }

    @Override
    public void onItemClickFavourite(CategoryModel categoryModel, int pos) {
//        if (categoryModel.isFollow()) {
//            ToastUtils.showToastSuccess(getBaseActivity(), getResources().getString(R.string.content_add_favorite));
//        }
        currentStateFavorite = true;
        datas.set(pos, categoryModel);
    }

    public void saveData() {
        if (datas != null && datas.size() != 0) {
            currentFavorite = new FavouriteModel();
            List<String> listFavotire = new ArrayList<>();
            for (CategoryModel model : datas) {
                if (model.isFollow()) {
                    listFavotire.add(model.getId() + "");
                }
            }
            currentFavorite.setData(listFavotire);
            SharedPrefs.getInstance().put("FAVOURITE", currentFavorite);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!LocationHelper.getInstant(ApplicationController.self()).isLocationServiceEnabled() || !LocationHelper.getInstant(ApplicationController.self()).isNetworkLocationEnabled()) {
            checkLocation = true;
            scLocation.setChecked(false);
            LocationHelper.getInstant(ApplicationController.self()).clearCurrentLocation();
            currentStateLocation = true;
            AppStateProvider.getInstance().setLocationFavourite(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkLocation = false;
                }
            }, 500);
        } else {
            checkLocation = true;
            scLocation.setChecked(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkLocation = false;
                }
            }, 500);
            LocationHelper.getInstant(ApplicationController.self()).getCurrentLatLng(new LocationHelper.GetLocationListener() {
                @Override
                public void onResponse(Location location) {
                    Log.d(TAG, "loadListStrangerAround getCurrentLatLng: onResponse: " + location.getLatitude() + "," +
                            location.getLongitude());
                    LocationHelper.getInstant(ApplicationController.self()).setGetLocationListener(null);
                    LocationHelper.getInstant(ApplicationController.self()).saveLastLocationToPref(location);
                    //todo luu location mới để lần sau gọi request
                    AppStateProvider.getInstance().setLocationFavourite(true);
                }

                @Override
                public void onFail() {
                    Log.d(TAG, "loadListStrangerAround: onFail");
                }
            });
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Duong", "Permission");
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            checkLocation = true;
            scLocation.setChecked(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkLocation = false;
                }
            }, 500);
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                            Constants.ACTION.ACTION_SETTING_LOCATION_ON);
                } catch (ActivityNotFoundException ex) {
                    Log.e(TAG, "Exception" + ex);
                }
            }
        }

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        Log.e("Duong", "---------off location-------" + menuId);
        checkLocation = true;
        if (menuId == Constants.ACTION.ACTION_SETTING_LOCATION) {
            scLocation.setChecked(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkLocation = false;
                }
            }, 500);
            try {
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                        Constants.ACTION.ACTION_SETTING_LOCATION_OFF);
            } catch (Exception e) {
                Log.e(TAG, "Exception" + e);
            }
        } else {
            scLocation.setChecked(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkLocation = false;
                }
            }, 500);

        }
    }

    public void showDialogSave() {
        DialogConfirm dialogConfirm = DialogConfirm.newInstance(null,
                getString(R.string.want_to_save_your_change_tab), DialogConfirm.CONFIRM_TYPE,
                R.string.dont_save, R.string.save);
        dialogConfirm.setSelectListener(
                new BaseDialogFragment.DialogListener() {
                    @Override
                    public void dialogRightClick(int value) {
                        currentStateFavorite = false;
                        SharedPrefs.getInstance().put("FAVOURITE", currentFavorite);
                        AppStateProvider.getInstance().setLocationFavourite(true);
                        dialogConfirm.dismiss();
                        getBaseActivity().finish();
                    }

                    @Override
                    public void dialogLeftClick() {
                        dialogConfirm.dismiss();
                        getBaseActivity().finish();
                    }
                });
        dialogConfirm.show(getChildFragmentManager(), "");
    }

    private void checkChangeFavorite() {
        if (datas != null && datas.size() != 0) {
            currentFavorite = new FavouriteModel();
            List<String> listFavotire = new ArrayList<>();
            for (CategoryModel model : datas) {
                if (model.isFollow()) {
                    listFavotire.add(model.getId() + "");
                }
            }
            currentFavorite.setData(listFavotire);
        }
        if (oldFavorite != null && currentFavorite != null) {
            int old = oldFavorite.getData().hashCode();
            int current = currentFavorite.getData().hashCode();
            if (old != current) {
                currentStateFavorite = true;
                return;
            }
        }
        currentStateFavorite = false;
    }

    @OnClick(R.id.iv_info)
    public void onClickInfo() {
        String textHelp = getString(R.string.content_help_customize_tab_news);
        Spanned spanned = Html.fromHtml(textHelp, arg0 -> {
            int id = 0;
            int width, height;
            float scale = 1f;
            try {
                if (arg0.equals("ic_checkbox_selected.png")) {
                    id = R.drawable.ic_v5_heart_violet;
                    scale = 1.4f;
                } else if (arg0.equals("ic_add_tab.png")) {
                    id = R.drawable.ic_v5_heart_normal;
                    scale = 1.4f;
                }
            } catch (Exception ex) {

            }
            LevelListDrawable d = new LevelListDrawable();
            Drawable empty = getResources().getDrawable(id);
            width = (int) (empty.getIntrinsicWidth() * scale);
            height = (int) (empty.getIntrinsicHeight() * scale);
            d.addLevel(0, 0, empty);
            d.setBounds(0, 0, width, height);
            return d;
        }, null);
        Dialog dialog = new Dialog(getBaseActivity(), R.style.DialogFullscreen);
        dialog.setContentView(R.layout.dialog_config_news);
        TextView tvTitleInfo, tvConfirmInfo, tvContentInfo;
        tvTitleInfo = dialog.findViewById(R.id.tvTitleDialog);
        tvConfirmInfo = dialog.findViewById(R.id.tvAction);
        tvContentInfo = dialog.findViewById(R.id.tvMessageDialog);
        tvTitleInfo.setText(getString(R.string.guideline_config_tab_news));
        tvContentInfo.setText(spanned);
        tvConfirmInfo.setText(getString(R.string.btn_understood));
        tvConfirmInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
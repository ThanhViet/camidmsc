package com.metfone.selfcare.module.metfoneplus.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.module.metfoneplus.activity.MPAddPhoneActivity;
import com.metfone.selfcare.module.metfoneplus.activity.MPVeryfiPhoneActivity;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPCombinePhoneDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPHeroDialog;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.ui.view.PinEntryEditText;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MPVerifyPhoneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MPVerifyPhoneFragment extends Fragment implements
        /*SmsReceiverListener,*/
        ClickListener.IconListener,
        MySMSBroadcastReceiver.OTPReceiveListener {
    private static final String TAG = MPVerifyPhoneFragment.class.getSimpleName();
    private static final String PHONE_NUMBER_PARAM = "phoneNumber";
    private static final String CODE_CHECK_PHONE_PARAM = "codeCheckPhone";

    @BindView(R.id.layout_root_verify_phone)
    LinearLayout mLayoutRootVerifyPhone;
    @BindView(R.id.peet_opt)
    PinEntryEditText etOtp;
    @BindView(R.id.txt_expired)
    AppCompatTextView txtExpired;
    @BindView(R.id.txt_opt_wrong)
    AppCompatTextView txtOtpWrong;
    @BindView(R.id.txt_resend_opt)
    AppCompatTextView txtResendOtp;
    @BindView(R.id.layout_container_verify_number)
    RelativeLayout mLayoutContainerVerifyNumber;

    public Unbinder mUnbinder;
    private String mCurrentNumberJid;
    private String password = "";
    private BaseSlidingFragmentActivity mMPAddPhoneActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private Handler mHandler;
    private static final int TIME_DEFAULT = 59000;
    private boolean isSaveInstanceState = false;
    private long countDownTimer;
    private String mCodeCheckPhone;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param phoneNumber Parameter 1.
     * @return A new instance of fragment LoginFragment.
     */
    public static MPVerifyPhoneFragment newInstance(String phoneNumber, String codeCheckPhone) {
        MPVerifyPhoneFragment fragment = new MPVerifyPhoneFragment();
        Bundle args = new Bundle();
        args.putString(PHONE_NUMBER_PARAM, phoneNumber);
        args.putString(CODE_CHECK_PHONE_PARAM, codeCheckPhone);
        fragment.setArguments(args);
        return fragment;
    }

    public MPVerifyPhoneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        mMPAddPhoneActivity.hideLoadingDialog();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getData(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_mp_verify_phone, container, false);
        mUnbinder = ButterKnife.bind(this, rootView);

        Utilities.adaptViewForInserts(mLayoutRootVerifyPhone);
        Utilities.adaptViewForInsertBottom(mLayoutContainerVerifyNumber);
        setViewListeners();
        mApplication.registerSmsOTPObserver();
        mCodeCheckPhone = getArguments().getString(CODE_CHECK_PHONE_PARAM);
        etOtp.setSingleCharHint("-");
        generateOtpByServer();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        mMPAddPhoneActivity = (BaseSlidingFragmentActivity) activity;
        mApplication = (ApplicationController) mMPAddPhoneActivity.getApplicationContext();
        mRes = mMPAddPhoneActivity.getResources();
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InputMethodUtils.showSoftKeyboard(getContext(),etOtp);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
//        MySMSBroadcastReceiver.addSMSReceivedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
//        MySMSBroadcastReceiver.removeSMSReceivedListener(this);
        InputMethodUtils.hideSoftKeyboard(etOtp, mMPAddPhoneActivity);
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
//        if (countDownTimer != null) countDownTimer.cancel();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER_PARAM, mCurrentNumberJid);
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                //ApplicationController application = (ApplicationController) mLoginActivity.getApplication();
                if (Config.Server.FACEBOOK_TEST && Config.Server.USER_FACEBOOK_TEST.equals(mCurrentNumberJid)) {
                    // review fb and so la Config.Server.USER_FACEBOOK_TEST
                    startCountDown();
                } else {
                    generateOtpByServer();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        if (getActivity() != null && getActivity() instanceof MPVeryfiPhoneActivity) {
            getActivity().finish();
        } else if (getActivity() != null && getActivity() instanceof MPAddPhoneActivity) {
            ((MPAddPhoneActivity)getActivity()).onBack();
        }
//        getActivity().getFragmentManager().popBackStack();
    }

    @OnClick(R.id.txt_resend_opt)
    void onClickResendOtp() {
        generateOtpByServer();
    }

    //XU ly phan sms otp
//    @Override
//    public boolean onOTPReceived(String otp) {
//        this.password = otp;
//        mMPAddPhoneActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (etOtp != null) {
//                    setPasswordEditText(password);
//                    mHandler.removeCallbacksAndMessages(null);
//                }
//            }
//        });
//        return true;
//    }

    @Override
    public void onOTPReceived(Intent intent) {

    }

    @Override
    public void onOTPTimeOut() {
    }

    private void getData(Bundle savedInstanceState) {
        if (getArguments() != null) {
            mCurrentNumberJid = getArguments().getString(PHONE_NUMBER_PARAM);
        } else if (savedInstanceState != null) {
            mCurrentNumberJid = savedInstanceState.getString(PHONE_NUMBER_PARAM);
        }
    }

    private void setViewListeners() {
//        startCountDown();
        etOtp.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                if (str != null) {
                    addService(str.toString());
                }
            }
        });
    }

    private void startCountDown() {
        txtExpired.setVisibility(View.VISIBLE);

        if (mMPAddPhoneActivity == null || mMPAddPhoneActivity.isFinishing() || txtExpired == null) {
            return;
        }

        txtResendOtp.setOnClickListener(null);

        countDownTimer = TIME_DEFAULT;
        String countDownString = String.format(mMPAddPhoneActivity.getString(R.string.m_p_otp_expired), TIME_DEFAULT / 1000);
        txtExpired.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mMPAddPhoneActivity == null || mMPAddPhoneActivity.isFinishing() || txtExpired == null)
                    return;
                countDownTimer = countDownTimer - 1000;
                int remain = (int) (countDownTimer / 1000.0);
                String countDownString = String.format(mMPAddPhoneActivity.getString(R.string.m_p_otp_expired), remain);
                txtExpired.setText(countDownString);
                if (remain == 0) {
                    txtExpired.setVisibility(View.GONE);
                    setResendOtpListener();
                } else {
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                }
            }
        }, 1000);
    }

    private void setPasswordEditText(String pass) {
        etOtp.setText(pass);
        Selection.setSelection(etOtp.getText(), etOtp.getText().length());
    }

    private void setResendOtpListener() {
        txtResendOtp.setOnClickListener(view -> {
            generateOtpByServer();
        });
    }

    private void addService(String otp) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            mMPAddPhoneActivity.showError(mRes.getString(R.string.error_internet_disconnect), null);
        } else {
            RetrofitInstance retrofitInstance = new RetrofitInstance();
            retrofitInstance.addService(mCurrentNumberJid, otp, 1, new ApiCallback<AddServiceResponse>() {
                @Override
                public void onResponse(Response<AddServiceResponse> response) {
                    if (response.body() != null) {
                        if ("00".equals(response.body().getCode())) {
                            List<Service> services = response.body().getData().getServices();
                            for (Service s : services) {
                                List<PhoneLinked> phoneLinkeds = s.getPhoneLinked();
                                for (int i = 0; i < phoneLinkeds.size(); i++) {
                                    PhoneLinked phoneLinked = phoneLinkeds.get(i);
                                    if (mCurrentNumberJid.contains(phoneLinked.getPhoneNumber())) {
                                        mCurrentNumberJid = phoneLinked.getPhoneNumber();
                                        // Udpate UI
                                        updateUIOtpNotification(true);
                                        updatePhoneToPhoneService(phoneLinked.getUserServiceId());
                                        return;
                                    }
                                }
                            }
                        } else if ("1".equals(response.body().getCode())) {
                            updateUIOtpNotification(false);
                        } else {
                            showErrorDialog(response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    com.metfone.selfcare.util.Log.e(TAG, "Exception", error);
                    mMPAddPhoneActivity.hideLoadingDialog();
                }
            });

//            startCountDown();
        }
    }

    public void generateOtpByServer() {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            mMPAddPhoneActivity.showError(mRes.getString(R.string.error_internet_disconnect), null);
        } else {
            RetrofitInstance retrofitInstance = new RetrofitInstance();
            retrofitInstance.generateOtp(mCurrentNumberJid, new ApiCallback<BodyResponse>() {
                @Override
                public void onResponse(Response<BodyResponse> response) {
                    Log.i(TAG, "onResponse:" + response);
                }

                @Override
                public void onError(Throwable error) {
                    com.metfone.selfcare.util.Log.e(TAG, "Exception", error);
                    mMPAddPhoneActivity.hideLoadingDialog();
                }
            });

            startCountDown();
        }
    }

    private void updatePhoneToPhoneService(int userServiceID) {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.updateMetfone(null, null, userServiceID, new ApiCallback<AddServiceResponse>() {
            @Override
            public void onResponse(Response<AddServiceResponse> response) {
                if (response.body() != null && response.body().getData() != null) {
                    mApplication.getCamIdUserBusiness().setPhoneService(mCurrentNumberJid);
                    mApplication.getCamIdUserBusiness().setUser(response.body().getData().getUser());
                    mApplication.getCamIdUserBusiness().setService(response.body().getData().getServices());

                    showSuccessDialog();
                }
            }

            @Override
            public void onError(Throwable error) {
                android.util.Log.e(TAG, "onError: " + error);
            }
        });
    }

    private void updateUIOtpNotification(boolean isSuccess) {
        if (isSuccess) {
            txtOtpWrong.setText(getString(R.string.m_p_otp_success));
        } else {
            txtOtpWrong.setText(getString(R.string.m_p_otp_wrong));
        }
        txtOtpWrong.setSelected(isSuccess);
        txtOtpWrong.setVisibility(View.VISIBLE);
        txtExpired.setVisibility(View.GONE);
        mHandler.removeCallbacksAndMessages(null);
    }

    private void showSuccessDialog() {
        int idStringTitle;
        int idStringContent;
        if (MPAddPhoneFragment.CODE_SUCCESS.equals(mCodeCheckPhone)) {
            idStringTitle = R.string.m_p_dialog_add_metfone_success_title;
            idStringContent = R.string.m_p_dialog_add_metfone_success_content;
        } else {
            idStringTitle = R.string.m_p_dialog_add_metfone_combine_title;
            idStringContent = R.string.m_p_dialog_add_metfone_combine_content;
        }

        MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                .setTitle(idStringTitle)
                .setContent(String.format(getString(idStringContent), Utilities.formatPhoneNumberCambodia(mCurrentNumberJid)))
                .setLottieAssertName("lottie/img_dialog_women_success.json")
                .setCancelableOnTouchOutside(true)
                .build();

        dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onDialogDismiss() {
                android.util.Log.e(TAG, "onDialogDismiss: ");
                mMPAddPhoneActivity.setResult(Activity.RESULT_OK);
//                if (mMPAddPhoneActivity instanceof MPVeryfiPhoneActivity || mMPAddPhoneActivity instanceof MPAddPhoneActivity)
                mMPAddPhoneActivity.finish();
            }
        });

        dialogSuccess.show(getChildFragmentManager(), null);
        InputMethodUtils.hideSoftKeyboard(etOtp, mMPAddPhoneActivity);
    }

    private void showErrorDialog(String message) {
        MPDialogFragment dialogError = new MPDialogFragment.Builder()
                .setTitle(R.string.m_p_dialog_error_title)
                .setContent(message)
                .setImage(R.drawable.image_error_dialog)
                .setCancelableOnTouchOutside(true)
                .build();
        dialogError.show(getChildFragmentManager(), null);
        InputMethodUtils.hideSoftKeyboard(etOtp, mMPAddPhoneActivity);

    }
}
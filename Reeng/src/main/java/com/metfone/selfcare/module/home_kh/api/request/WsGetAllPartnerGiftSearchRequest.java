package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetAllPartnerGiftSearchRequest extends BaseRequest<WsGetAllPartnerGiftSearchRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("partnerName")
        String partnerName;
        @SerializedName("rankId")
        String rankId;
        @SerializedName("giftPoint")
        String giftPoint;
        @SerializedName("cityId")
        String cityId;

        @SerializedName("latitude")
        double latitude;
        @SerializedName("limit")
        String limit;
        @SerializedName("language")
        String language;
        @SerializedName("page")
        String page;
        @SerializedName("price")
        int price;
        @SerializedName("isType")
        String isType;
        @SerializedName("longitude")
        double longitude;

        public WsRequest(String isdn, String partnerName, String rankId, String giftPoint,
                         String cityId, double latitude, String limit, String language, String page,
                         int price, String isType, double longitude) {
            this.isdn = isdn;
            this.partnerName = partnerName;
            this.rankId = rankId;
            this.giftPoint = giftPoint;
            this.cityId = cityId;
            this.latitude = latitude;
            this.limit = limit;
            this.language = language;
            this.page = page;
            this.price = price;
            this.isType = isType;
            this.longitude = longitude;
        }

        public WsRequest(String isdn, String rankId, String giftPoint) {
            this.isdn = isdn;
            this.partnerName = "";
            this.rankId = rankId;
            this.giftPoint = giftPoint;
            this.cityId = "";
            this.latitude = 11.5503929;
            this.limit = "1000";
            this.language = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
            this.page = "1";
            this.price = 1;
            this.isType = "2";
            this.longitude = 104.9044962;
        }
    }
}

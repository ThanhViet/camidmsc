package com.metfone.selfcare.module.selfcare.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by thanhnt72 on 2/15/2019.
 */

public class SCSecretQuestion {
    private String question;
    private String answer;
    private String questionId;

    public SCSecretQuestion() {
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public JSONObject questionAnswerToString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("answer", answer);
            jsonObject.put("questionId", questionId);

            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

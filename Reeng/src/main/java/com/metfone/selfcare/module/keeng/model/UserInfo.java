package com.metfone.selfcare.module.keeng.model;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.utils.Utilities;

import java.io.Serializable;

public class UserInfo implements Serializable {

    /*
     * @parram: is_follow
     * 0 chưa là gì
     * 1 đã là bạn
     * 3 mình gửi yêu cầu kết bạn cho người khác
     * 4 người khác đã gửi yêu cầu kết bạn đến mình
     *
     * */
    public static final int USER_TYPE_NORMAL = 0;
    public static final int USER_TYPE_KEENG = 1;
    public static final int USER_TYPE_SINGER = 2;
    public static final int USER_TYPE_OTHER = 3;
    public static final int USER_DEACTIVE = 0;
    public static final int USER_OFFLINE = 1;
    public static final int USER_ONLINE = 2;
    public static final int USER_INVISIBLE = 3;

    private static final long serialVersionUID = -475872936133638955L;
    @SerializedName("id")
    public long id = 0;
    @SerializedName("msisdn")
    public String msisdn = "";
    @SerializedName("facebook")
    public String facebook = "";
    @SerializedName("birthday")
    public String birthday = "";
    @SerializedName("description")
    public String description = "";
    @SerializedName("user_type")
    public int user_type;
    @SerializedName("gender")
    public int gender = -1;
    @SerializedName("is_contact")
    public int is_contact = 0;
    @SerializedName("name")
    public String name;
    public boolean isChecked = false;
    @SerializedName("blast")
    public String blast;
    public int typeLogin = 0;
    public String social_id = "";

    public int state = USER_OFFLINE;
    @SerializedName("is_follow")
    private int is_follow = -1;
    @SerializedName("is_friend")
    private int isFriend = -1;
    @SerializedName("image")
    private String image = "";
    @SerializedName("image310")
    private String image310;
    @SerializedName("singer_id")
    private long singer_id;
    @SerializedName("cover")
    private String cover = "";
    @SerializedName("avatar")
    private String avatar = "";
    @SerializedName("score")
    private int score = 0;

    private String identityCard = "";
    private String dateOfIssue = "";
    private String placeOfIssue = "";
    private String email = "";
    private int source;
    private int telecommunicationNetwork;

    public UserInfo() {
        super();
    }

    public UserInfo(String name) {
        super();
        this.name = name;
    }

    public UserInfo(ContactInfo item) {
        if (item != null) {
            msisdn = item.getMsisdn();
            name = item.getName();
            avatar = item.getImage();
            id = item.getUserId();
            is_contact = 1;
        }
    }

    public long getSingerId() {
        return singer_id;
    }

    public void setSingerId(long singer_id) {
        this.singer_id = singer_id;
    }

    public boolean isSinger() {
        if (user_type == USER_TYPE_SINGER) {
            return true;
        }
        return false;
    }

    public int getFollow() {
        return is_follow;
    }

    public boolean isFollow() {
        if (is_follow == 1 || isFriend == 1)
            return true;
        return false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAvatar() {
        if (TextUtils.isEmpty(avatar))
            return image;
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCover() {
        if (TextUtils.isEmpty(cover))
            return image310;
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getNameUser() {
        return Utilities.getUsername(name, msisdn);
    }

    public boolean isMyUser(Context context) {
        return id == LoginObject.getId(context);
    }

    public boolean nameIsPhone() {
        String sdt = Utilities.fixPhoneNumb(msisdn);
        String name2 = Utilities.fixPhoneNumb(name);
        return sdt.equals(name2);
    }

    public String getMsisdn() {
        return Utilities.fixPhoneNumbTo0(msisdn);
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPhoneNumber() {
        return Utilities.fixPhoneNumb(msisdn);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        if (name == null)
            name = "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage310() {
        return image310;
    }

    public void setImage310(String image310) {
        this.image310 = image310;
    }

    public void setIs_follow(int is_follow) {
        this.is_follow = is_follow;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getBlast() {
        return blast;
    }

    public void setBlast(String blast) {
        this.blast = blast;
    }

    public int getTypeLogin() {
        return typeLogin;
    }

    public void setTypeLogin(int typeLogin) {
        this.typeLogin = typeLogin;
    }

    public int getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(int isFriend) {
        this.isFriend = isFriend;
    }

    public int getUserType() {
        return user_type;
    }

    public void setUserType(int user_type) {
        this.user_type = user_type;
    }

    public int getIsContact() {
        return is_contact;
    }

    public void setIsContact(int is_contact) {
        this.is_contact = is_contact;
    }

    public String getSocialId() {
        return social_id;
    }

    public void setSocialId(String social_id) {
        this.social_id = social_id;
    }

    public int getIsFollow() {
        return is_follow;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public String getPlaceOfIssue() {
        return placeOfIssue;
    }

    public void setPlaceOfIssue(String placeOfIssue) {
        this.placeOfIssue = placeOfIssue;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public int getTelecommunicationNetwork() {
        return telecommunicationNetwork;
    }

    public void setTelecommunicationNetwork(int telecommunicationNetwork) {
        this.telecommunicationNetwork = telecommunicationNetwork;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", msisdn='" + msisdn + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", image310='" + image310 + '\'' +
                ", singer_id=" + singer_id +
                ", cover='" + cover + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}

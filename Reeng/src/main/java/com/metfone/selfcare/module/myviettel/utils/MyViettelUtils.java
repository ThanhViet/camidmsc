/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/30
 *
 */

package com.metfone.selfcare.module.myviettel.utils;

import android.content.Intent;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.activity.DataChallengeActivity;
import com.metfone.selfcare.module.myviettel.network.ApiCallback;
import com.metfone.selfcare.module.myviettel.network.MyViettelApi;

public final class MyViettelUtils {

    public static void registerDataChallenge(final BaseSlidingFragmentActivity activity) {
        if (activity == null) return;
        activity.showLoadingDialog("", R.string.processing);
        new MyViettelApi().registerDC(new ApiCallback<Boolean>() {
            @Override
            public void onSuccess(String msg, Boolean result) throws Exception {
                if (activity != null) {
                    activity.hideLoadingDialog();
                    if (result) {
                        Intent intent = new Intent(activity, DataChallengeActivity.class);
                        activity.startActivity(intent);
                    } else {
                        activity.showToast(msg);
                    }
                }
            }

            @Override
            public void onError(String s) {
                if (activity != null) {
                    activity.hideLoadingDialog();
                    activity.showToast(R.string.e601_error_but_undefined);
                }
            }

            @Override
            public void onComplete() {

            }
        });
    }
}

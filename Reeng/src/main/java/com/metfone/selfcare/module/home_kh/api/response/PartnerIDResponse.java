package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PartnerIDResponse {
    @SerializedName("errorCode")
    @Expose
    String code;
    @SerializedName("errorMessage")
    @Expose
    String message;
}


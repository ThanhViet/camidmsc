package com.metfone.selfcare.module.netnews.base;

import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by HaiKE on 8/22/16.
 */
public abstract class BaseAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {
//    protected ImageLoadBusiness mImageBuss;
    protected Context context;
    protected static final int ITEM_HEADER = 1;
    protected static final int ITEM_CONTENT = 2;
    protected static final int ITEM_FOOTER = 3;
    protected boolean isLoading = false;
    protected boolean isLoadMore = false;


    // The minimum amount of items to have below your current scroll position
    // before isLoading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public BaseAdapter(Context context) {
//        App app = (App) context.getApplicationContext();
//        mImageBuss = app.getImageBusiness();
        this.context = context;
    }

    public void setRecyclerView(RecyclerView recyclerView)
    {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (isLoadMore && !isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                        if (onLoadMoreListener != null) {
//                            onLoadMoreListener.onLoadMore();
//                        }
                        isLoading = true;
                    }
                }
            });
        }
        else if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {

            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = gridLayoutManager.getItemCount();
                    lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                    if (isLoadMore && !isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                        if (onLoadMoreListener != null) {
//                            onLoadMoreListener.onLoadMore();
//                        }
                        isLoading = true;
                    }
                }
            });
        }

    }

//    public void setOnLoadMoreListener(AbsInterface.OnLoadMoreListener onLoadMoreListener) {
//        this.onLoadMoreListener = onLoadMoreListener;
//    }

    public void setLoaded(boolean flag) {
        this.isLoading = flag;
    }

    public void setIsLoadMore(boolean flag) {
        this.isLoadMore = flag;
    }
}

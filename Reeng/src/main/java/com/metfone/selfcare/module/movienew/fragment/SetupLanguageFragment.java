package com.metfone.selfcare.module.movienew.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;

import java.util.Locale;

public class SetupLanguageFragment extends Fragment implements View.OnClickListener {

    private static final String LANGUAGE_CODE_KHMER = "km";
    private static final String LANGUAGE_CODE_ENGLISH = "en";

    private SetupLanguageListener setupLanguageListener;

    private Button btnYes;
    private Button btnNo;
    private Button btnOk;

    private TextView tvQuestionSetupLanguage;

    public static SetupLanguageFragment newInstance() {
        return new SetupLanguageFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SetupLanguageListener) {
            setupLanguageListener = (SetupLanguageListener) context;
        } else {
            throw new RuntimeException("Parent activity must implement " + SetupLanguageListener.class.getSimpleName());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_setup_language, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialViews(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        initialListeners();
    }

    @Override
    public void onDetach() {
        setupLanguageListener = null;
        super.onDetach();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_no:
                if (isKhmerLanguage(getCurDeviceLanguage())) {
                    changeLanguageBy(LANGUAGE_CODE_ENGLISH);
                } else {
                    changeLanguageBy(LANGUAGE_CODE_KHMER);
                }

                btnYes.setBackgroundResource(R.drawable.bg_grey_button_corner_6);
                btnNo.setBackgroundResource(R.drawable.bg_yellow_button_corner_6);

                callOnClickButtonCallback();
                break;

            case R.id.btn_yes:
                if (isKhmerLanguage(getCurDeviceLanguage())) {
                    changeLanguageBy(LANGUAGE_CODE_KHMER);
                } else {
                    changeLanguageBy(LANGUAGE_CODE_ENGLISH);
                }
                btnYes.setBackgroundResource(R.drawable.bg_yellow_button_corner_6);
                btnNo.setBackgroundResource(R.drawable.bg_grey_button_corner_6);
                callOnClickButtonCallback();
                break;

            case R.id.btn_ok:
                if (isKhmerLanguage(getCurDeviceLanguage())) {
                    changeLanguageBy(LANGUAGE_CODE_KHMER);
                } else {
                    changeLanguageBy(LANGUAGE_CODE_ENGLISH);
                }
                callOnClickButtonCallback();
                break;
        }
    }

    private void callOnClickButtonCallback() {
        if (setupLanguageListener != null) {
            setupLanguageListener.selectLanguage();
        }
    }

    private void initialViews(View view) {
        btnNo = view.findViewById(R.id.btn_no);
        btnYes = view.findViewById(R.id.btn_yes);
        btnOk = view.findViewById(R.id.btn_ok);
        tvQuestionSetupLanguage = view.findViewById(R.id.tv_question_setup_language);

        String yesLanguage;
        String noLanguage;
        if (isKhmerLanguage(getCurDeviceLanguage())) {
            yesLanguage = "Khmer";
            noLanguage = "English";
        } else {
            yesLanguage = "English";
            noLanguage = "Khmer";
        }

        btnYes.setText(getString(R.string.lb_yes_language, yesLanguage));
        btnNo.setText(getString(R.string.lb_no_language, noLanguage));
        tvQuestionSetupLanguage.setText(getString(R.string.lb_question_setup_language, yesLanguage));
    }

    private void initialListeners() {
        btnOk.setOnClickListener(this);
        btnNo.setOnClickListener(this);
        btnYes.setOnClickListener(this);
    }

    private void changeLanguageBy(String language) {
        LocaleManager.setNewLocale(ApplicationController.self().getApplicationContext(), language);
    }

    private boolean isKhmerLanguage(String curLanguage) {
        return curLanguage.equals(new Locale(LANGUAGE_CODE_KHMER).getLanguage());
    }

    private String getCurDeviceLanguage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Resources.getSystem().getConfiguration().getLocales().get(0).getLanguage();
        }

        return Resources.getSystem().getConfiguration().locale.getLanguage();
    }

    public interface SetupLanguageListener {
        void selectLanguage();
    }
}

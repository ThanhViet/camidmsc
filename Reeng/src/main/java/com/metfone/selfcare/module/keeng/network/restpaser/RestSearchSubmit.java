/*
 * Copyright (c) 2017.
 * www.bigzun.com
 */

package com.metfone.selfcare.module.keeng.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.SearchModel;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSearchSubmit implements Serializable {

    private static final long serialVersionUID = -5143100645738432097L;
    @SerializedName("response")
    public ResponseSearch response;

    @Override
    public String toString() {
        return "RestSearchSubmit{" +
                "response=" + response +
                '}';
    }

    public static class ResponseSearch implements Serializable {
        private static final long serialVersionUID = -8512794946210593281L;
        @SerializedName("docs")
        private ArrayList<SearchModel> docs;

        public ArrayList<SearchModel> getDocs() {
            if (docs == null)
                docs = new ArrayList<>();
            return docs;
        }

        @Override
        public String toString() {
            return "ResponseSearch{" +
                    "docs=" + docs +
                    '}';
        }
    }
}
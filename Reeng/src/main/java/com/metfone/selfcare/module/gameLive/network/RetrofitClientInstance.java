package com.metfone.selfcare.module.gameLive.network;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.helper.encrypt.RSAEncrypt;
import com.metfone.selfcare.module.gameLive.network.parse.AbsResultData;
import com.metfone.selfcare.module.gameLive.network.parse.RestGameInfo;
import com.metfone.selfcare.module.gameLive.network.parse.RestGameResult;
import com.metfone.selfcare.module.gameLive.network.parse.RestWinner;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.security.PublicKey;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {
    private static Retrofit retrofit;
    private PublicKey publicKey;
    private static String baseUrl;
    private String domain;

    public RetrofitClientInstance() {
        ContentConfigBusiness configBusiness = ApplicationController.self().getConfigBusiness();
        baseUrl = configBusiness.getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_VIDEO_GAME_STREAMING_API);
//        baseUrl = "http://171.255.193.187:8082/";
        String publicKeyStr = configBusiness.getContentConfigByKey(Constants.PREFERENCE.CONFIG.PUBLIC_KEY_VIDEO_GAME_STREAMING);
        domain = BaseApi.convertDomainToDomainParam(baseUrl);
        try {
            publicKey = RSAEncrypt.getPublicKeyFromString(publicKeyStr);
        } catch (Exception e) {
        }
    }

    private static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(logging);

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    private String encryptDataToSecurity(String msisdn, String token, long timestamp) {
        try {
            Log.d("GameLiveStream", "msisdn: " + msisdn);
            Log.d("GameLiveStream", "token: " + token);
            Log.d("GameLiveStream", "timestamp: " + timestamp);
            StringBuilder sb = new StringBuilder();
            sb.append(msisdn);
            sb.append(token);
            sb.append(timestamp);
            String md5Encrypt = EncryptUtil.encryptMD5(sb.toString());
            Log.d("GameLiveStream", "md5Encrypt: " + md5Encrypt);
            JSONObject data = new JSONObject();
            try {
                data.put(Constants.HTTP.REST_TOKEN, token);
                data.put(Constants.HTTP.REST_MD5, md5Encrypt);
            } catch (Exception e) {
                return null;
            }
            return RSAEncrypt.encrypt(data.toString(), publicKey);
        } catch (Exception e) {
        }
        return "";
    }

    public void getGameInfo(String msisdn, APICallBack<RestGameInfo> callBack) {
        ApiService service = getRetrofitInstance().create(ApiService.class);
        long timestamp = System.currentTimeMillis();
        ApplicationController app = ApplicationController.self();
        String token = app.getReengAccountBusiness().getToken();
        String security = encryptDataToSecurity(msisdn, token, timestamp);
        Call<RestGameInfo> call = service.getGameInfo(app.getReengAccountBusiness().getMochaApi(), msisdn, domain, timestamp, token, security);
        call.enqueue(new Callback<RestGameInfo>() {
            @Override
            public void onResponse(Call<RestGameInfo> call, Response<RestGameInfo> response) {
                callBack.onResponse(response);
            }

            @Override
            public void onFailure(Call<RestGameInfo> call, Throwable t) {
                callBack.onError(t);
            }
        });
    }

    public void checkResult(String msisdn, String questionId, String gameId, APICallBack<RestGameResult> callBack) {
        ApiService service = getRetrofitInstance().create(ApiService.class);
        long timestamp = System.currentTimeMillis();
        ApplicationController app = ApplicationController.self();
        String token = app.getReengAccountBusiness().getToken();
        String security = encryptDataToSecurity(msisdn, token, timestamp);
        Call<RestGameResult> call = service.checkResult(app.getReengAccountBusiness().getMochaApi(), msisdn, domain, questionId, gameId, timestamp, token, security);
        call.enqueue(new Callback<RestGameResult>() {
            @Override
            public void onResponse(Call<RestGameResult> call, Response<RestGameResult> response) {
                callBack.onResponse(response);
            }

            @Override
            public void onFailure(Call<RestGameResult> call, Throwable t) {
                callBack.onError(t);
            }
        });
    }

    public void postAnswer(String msisdn, String questionId, int isCorrect, String answer, String gameId, APICallBack<AbsResultData> callBack) {
        ApiService service = getRetrofitInstance().create(ApiService.class);
        long timestamp = System.currentTimeMillis();
        ApplicationController app = ApplicationController.self();
        String token = app.getReengAccountBusiness().getToken();
        String security = encryptDataToSecurity(msisdn, token, timestamp);
        Call<AbsResultData> call = service.postAnswer(app.getReengAccountBusiness().getMochaApi(), msisdn, domain, questionId, isCorrect, answer, gameId, timestamp, token, security);
        call.enqueue(new Callback<AbsResultData>() {
            @Override
            public void onResponse(Call<AbsResultData> call, Response<AbsResultData> response) {
                callBack.onResponse(response);
            }

            @Override
            public void onFailure(Call<AbsResultData> call, Throwable t) {
                callBack.onError(t);
            }
        });
    }

    public void postNumberLucky(String msisdn, String numberLucky, String gameId, APICallBack<AbsResultData> callBack) {
        ApiService service = getRetrofitInstance().create(ApiService.class);
        long timestamp = System.currentTimeMillis();
        ApplicationController app = ApplicationController.self();
        String token = app.getReengAccountBusiness().getToken();
        String security = encryptDataToSecurity(msisdn, token, timestamp);
        Call<AbsResultData> call = service.postNumberLucky(app.getReengAccountBusiness().getMochaApi(), msisdn, domain, numberLucky, gameId, timestamp, token, security);
        call.enqueue(new Callback<AbsResultData>() {
            @Override
            public void onResponse(Call<AbsResultData> call, Response<AbsResultData> response) {
                callBack.onResponse(response);
            }

            @Override
            public void onFailure(Call<AbsResultData> call, Throwable t) {
                callBack.onError(t);
            }
        });
    }

    public void getListWinners(String questionId, String questionNumber, String gameId, APICallBack<RestWinner> callBack) {
        ApiService service = getRetrofitInstance().create(ApiService.class);
        long timestamp = System.currentTimeMillis();
        ApplicationController app = ApplicationController.self();
        ReengAccountBusiness accountBusiness = app.getReengAccountBusiness();
        String msisdn = accountBusiness.getJidNumber();
        String token = accountBusiness.getToken();
        String security = encryptDataToSecurity(msisdn, token, timestamp);
        Call<RestWinner> call = service.getListWinners(app.getReengAccountBusiness().getMochaApi(), msisdn, domain, questionId, questionNumber, gameId, timestamp, token, security);
        call.enqueue(new Callback<RestWinner>() {
            @Override
            public void onResponse(Call<RestWinner> call, Response<RestWinner> response) {
                callBack.onResponse(response);
            }

            @Override
            public void onFailure(Call<RestWinner> call, Throwable t) {
                callBack.onError(t);
            }
        });
    }
}

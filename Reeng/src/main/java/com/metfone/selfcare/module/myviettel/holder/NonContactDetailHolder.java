/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/6
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.myviettel.listener.OnDataChallengeListener;

import butterknife.BindView;

public class NonContactDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.button_submit)
    @Nullable
    View btnSubmit;

    private String data;

    public NonContactDetailHolder(View view, final OnDataChallengeListener listener) {
        super(view);
        if (btnSubmit != null) {
            btnSubmit.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (data != null && listener != null) {
                        listener.onClickChooseNonContact(data);
                    }
                }
            });
        }
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof String) {
            data = (String) item;
            if (tvTitle != null) tvTitle.setText(data);
        } else {
            data = null;
        }
    }
}
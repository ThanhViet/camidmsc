package com.metfone.selfcare.module.selfcare.model;

import com.metfone.selfcare.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by thanhnt72 on 2/15/2019.
 */

public class SCAccount {

    private static final String TAG = SCAccount.class.getSimpleName();
    private String userName;
    private String passWord;
    private String email;
    private String nrcOrPassport;
    private String accessTokenFacebook;
    private SCSecretQuestion secretQuestion;
    private String phoneNumber;
    private String fullName;


    public SCAccount() {
    }

    public SCAccount(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNrcOrPassport() {
        return nrcOrPassport;
    }

    public void setNrcOrPassport(String nrcOrPassport) {
        this.nrcOrPassport = nrcOrPassport;
    }

    public String getAccessTokenFacebook() {
        return accessTokenFacebook;
    }

    public void setAccessTokenFacebook(String accessTokenFacebook) {
        this.accessTokenFacebook = accessTokenFacebook;
    }

    public SCSecretQuestion getSecretQuestion() {
        return secretQuestion;
    }

    public void setSecretQuestion(SCSecretQuestion secretQuestion) {
        this.secretQuestion = secretQuestion;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String accountRegisterToString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("username", userName);
            jsonObject.put("password", passWord);
            jsonObject.put("fullName", fullName);
            jsonObject.put("phoneNumber", phoneNumber);
            jsonObject.put("nrcOrPassport", nrcOrPassport);
            jsonObject.put("facebookAccessToken", accessTokenFacebook);
            if (secretQuestion != null) {
                JSONObject jsAnswer = new JSONObject();
                jsAnswer.put("answer", secretQuestion.getAnswer());
                jsAnswer.put("questionId", secretQuestion.getQuestionId());
                jsonObject.put("secretQuestionRequest", jsAnswer);
            } else {
                jsonObject.put("secretQuestionRequest", null);
            }
            Log.i(TAG, "accountRegisterToString: " + jsonObject.toString());
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String accountLoginToString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("facebookAccessToken", accessTokenFacebook);
            jsonObject.put("username", userName);
            jsonObject.put("password", passWord);
            jsonObject.put("phoneNumber", phoneNumber);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

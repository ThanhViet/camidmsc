package com.metfone.selfcare.module.selfcare.fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseFragment;

public class SCTopupFragment extends BaseFragment {
    @Override
    public String getName() {
        return "SCTopupFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_topup;
    }
}

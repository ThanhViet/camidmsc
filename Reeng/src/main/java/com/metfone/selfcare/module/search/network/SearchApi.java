/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.network;

import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.api.video.video.VideoApiImpl;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.SearchHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.SearchMovieResponse;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.KhHomeService;
import com.metfone.selfcare.module.home_kh.api.request.WsGetListReward;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.network.restpaser.RestSearchSubmit;
import com.metfone.selfcare.module.keeng.network.restpaser.RestSearchSuggest;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchApi extends BaseApi {
    private final int TIME_OUT_SEARCH_SUGGEST = 10;
    private final int TIME_OUT = 30;

    public SearchApi() {
        super(ApplicationController.self());
    }

    public Http searchVideo(String keyword, int limit, int offset
        , @Nullable final ApiCallback<ArrayList<Video>> apiCallback, boolean isSuggest) {
        final long startTime = System.currentTimeMillis();
        keyword = SearchHelper.convertQuery(keyword);
        String timestamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String TAG_REQUEST = "TAG_SEARCH_VIDEO";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, VideoApi.SEARCH_VIDEO_V1);
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
            + domainParam + keyword + limit + offset
            + getReengAccountBusiness().getToken() + timestamp, getReengAccountBusiness().getToken());

        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putParameter("domain", domainParam);
        builder.putParameter("limit", String.valueOf(limit));
        builder.putParameter("q", keyword);
        builder.putParameter("video_type", "");
        builder.putParameter("cateId", "");
        builder.putParameter("url", "");
        builder.putParameter("lastIdStr", "");
        builder.putParameter("source", "SEARCH");
        builder.putParameter("offset", String.valueOf(offset));
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());
        builder.putParameter("security", security);
        builder.putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter("revision", Config.REVISION);
        builder.putParameter(Parameter.Http.NETWORK_TYPE, NetworkHelper.getNetworkSubType(application));
        builder.putParameter("vip", getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP);

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onSuccess("", VideoApiImpl.convertStringToVideoArrayList(response));
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.VIDEO_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onFailure(String message) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.VIDEO_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        if (isSuggest) builder.setTimeOut(TIME_OUT_SEARCH_SUGGEST);
        else builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    public Http searchChannelVideo(String keyword, int limit, int offset
        , @Nullable final ApiCallback<ArrayList<Channel>> apiCallback, boolean isSuggest) {
        final long startTime = System.currentTimeMillis();
        String timestamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String TAG_REQUEST = "TAG_SEARCH_CHANNEL_VIDEO";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, VideoApi.SEARCH_CHANNEL_VIDEO);
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
            + domainParam + keyword + limit + offset
            + getReengAccountBusiness().getToken() + timestamp, getReengAccountBusiness().getToken());

        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putParameter("domain", domainParam);
        builder.putParameter("limit", String.valueOf(limit));
        builder.putParameter("q", keyword);
        builder.putParameter("offset", String.valueOf(offset));
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("security", security);

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                if (apiCallback != null)
                    apiCallback.onSuccess("", VideoApiImpl.convertStringToChannelArrayListBase(new JSONObject(response).optString("listChannel")));
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        if (isSuggest) builder.setTimeOut(TIME_OUT_SEARCH_SUGGEST);
        else builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    public Http searchNews(String keyword, int num, int page
        , @Nullable final ApiCallback<ArrayList<NewsModel>> apiCallback, boolean isSuggest) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = "TAG_SEARCH_NEWS";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(getDomainNetNews(), NetNewsApi.GET_SEARCH + "/" + page + "/" + num + "/" + keyword);
        builder.putHeader("api_key", CommonUtils.API_KEY);
        builder.putHeader("user", CommonUtils.USER);
        builder.putHeader("password", CommonUtils.PASS);
        builder.putHeader("version", BuildConfig.VERSION_NAME);
        builder.putHeader("device", CommonUtils.DEVICE);
        builder.putHeader("imei", DeviceHelper.getDeviceId(application, true));
        builder.putHeader("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putHeader("uuid", CommonUtils.getUIID(application));
        builder.putHeader("Platform", Constants.HTTP.PLATFORM);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                final long endTime = System.currentTimeMillis();
                NewsResponse data = gson.fromJson(response, NewsResponse.class);
                if (data == null) {
                    onFailure("");
                } else {
                    if (apiCallback != null) apiCallback.onSuccess("", data.getData());
                    LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
                }
            }

            @Override
            public void onFailure(String message) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.NEWS_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        if (isSuggest) builder.setTimeOut(TIME_OUT_SEARCH_SUGGEST);
        else builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    /* Response cua api SEARCH_MOVIES_CINEMA trả về nhiều thuộc tính hơn Movie, nhưng không sử dụng đến vì vậy sử dụng luôn Movie thay vì tạo class mới ( do lười tạo, sửa nhiều code)*/
    public Http searchMoviesCinema(String keyword, int limit, int offset
        , @Nullable final ApiCallback<ArrayList<Movie>> apiCallback, boolean isSuggest) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = "TAG_SEARCH_MOVIES";
        // set default msisdn for case not have phone
        String msisdn = getReengAccountBusiness().getJidNumber();
        if (StringUtils.isEmpty(msisdn)) {
            msisdn = "0111111111";
        }
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(MovieApi.DOMAIN_MOVIE, MovieApi.SEARCH_MOVIES_CINEMA);
        builder.putParameter("keyword", keyword);
        builder.putParameter("offset", String.valueOf(offset));
        builder.putParameter("limit", String.valueOf(limit));
        builder.putParameter("wt", "json");
        builder.putParameter("qt", "search");
        builder.putParameter("fl", "*,score");
        builder.putParameter("fq", "isDisplayOnMocha:1");
        builder.putParameter("sort", "score desc");

        builder.putParameter("revision", Config.REVISION);
        builder.putParameter("client_type", Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter("version", BuildConfig.VERSION_NAME);
        builder.putParameter("device_id", DeviceHelper.getDeviceId(application, true));
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", msisdn);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response).optJSONObject("response");
                ArrayList<Movie> data = new ArrayList<>();
                JSONArray jsonArray = jsonObject.optJSONArray("docs");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String jsonTmp = jsonArray.optString(i);
                        if (Utilities.notEmpty(jsonTmp)) {
                            try {
                                Movie movie = gson.fromJson(jsonTmp, Movie.class);
                                if (movie != null) data.add(movie);
                            } catch (Exception e) {
                                Log.e("SearchApi", e);
                            }
                        }
                    }
                }
                if (apiCallback != null) apiCallback.onSuccess("", data);
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_SEARCH_CINEMA, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onFailure(String message) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_SEARCH_CINEMA, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        if (isSuggest) builder.setTimeOut(TIME_OUT_SEARCH_SUGGEST);
        else builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    public Http searchMovies(String keyword, int limit, int offset
        , @Nullable final ApiCallback<ArrayList<Movie>> apiCallback, boolean isSuggest) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = "TAG_SEARCH_MOVIES";
        // set default msisdn for case not have phone
        String msisdn = getReengAccountBusiness().getJidNumber();
        if (StringUtils.isEmpty(msisdn)) {
            msisdn = "0111111111";
        }
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(MovieApi.DOMAIN_MOVIE, MovieApi.SEARCH_MOVIES);
        builder.putParameter("q", keyword);
        builder.putParameter("start", String.valueOf(offset));
        builder.putParameter("rows", String.valueOf(limit));
        builder.putParameter("wt", "json");
        builder.putParameter("qt", "search");
        builder.putParameter("fl", "*,score");
        builder.putParameter("fq", "isDisplayOnMocha:1");
        builder.putParameter("sort", "score desc");

        builder.putParameter("revision", Config.REVISION);
        builder.putParameter("client_type", Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter("version", BuildConfig.VERSION_NAME);
        builder.putParameter("device_id", DeviceHelper.getDeviceId(application, true));
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", msisdn);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                final long endTime = System.currentTimeMillis();
                JSONObject jsonObject = new JSONObject(response).optJSONObject("response");
                ArrayList<Movie> data = new ArrayList<>();
                JSONArray jsonArray = jsonObject.optJSONArray("docs");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String jsonTmp = jsonArray.optString(i);
                        if (Utilities.notEmpty(jsonTmp)) {
                            try {
                                Movie movie = gson.fromJson(jsonTmp, Movie.class);
                                if (movie != null) data.add(movie);
                            } catch (Exception e) {
                                Log.e("SearchApi", e);
                            }
                        }
                    }
                }
                if (apiCallback != null) apiCallback.onSuccess("", data);
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onFailure(String message) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        if (isSuggest) builder.setTimeOut(TIME_OUT_SEARCH_SUGGEST);
        else builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    //TODO SEARCH_REWARDS
    public Http searchRewards(String keyword, int limit, int offset
        , @Nullable final ApiCallback<ArrayList<SearchRewardResponse.Result.ItemReward>> apiCallback, boolean isSuggest) {
        KhHomeClient client = new KhHomeClient();
        KhHomeService service = KhHomeClient.getKhHomeService();
        WsGetListReward request = client.createBaseRequest(new WsGetListReward(), "uc_getListGift");
        WsGetListReward.Request subRequest = request.new Request();
        subRequest.page = offset + "";
        subRequest.limit = limit + "";
        subRequest.giftId = "";
        subRequest.giftName = keyword;
        subRequest.gifttype = "";
        request.setWsRequest(subRequest);

        Call<SearchRewardResponse> call = service.wsGetReward(request.createRequestBody());
        call.enqueue(new Callback<SearchRewardResponse>() {
            @Override
            public void onResponse(Call<SearchRewardResponse> call, Response<SearchRewardResponse> response) {
                if (response.body() == null) {
                    return;
                }
                try {
                    if (apiCallback != null) {
                        apiCallback.onSuccess("", response.body().getResult().getWsResponse().getRewards());
                        apiCallback.onComplete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SearchRewardResponse> call, Throwable t) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
            }
        });
        return null;
    }

    public Http searchMusic(String keyword, int limit, int offset, String type
        , @Nullable final ApiCallback<ArrayList<SearchModel>> apiCallback, boolean isSuggest) {
        //todo type = song or video or album or singer or playlist
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = "TAG_SEARCH_MUSIC";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(getDomainKeengMusicSearch(), KeengApi.GET_SEARCH);
        builder.putParameter("wt", "json");
        builder.putParameter("q", keyword);
        builder.putParameter("fl", "*,score");
        builder.putParameter("fq", "type:" + type);
        builder.putParameter("start", String.valueOf(offset));
        builder.putParameter("rows", String.valueOf(limit));
        builder.putParameter("sort", "score desc");

        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("revision", Config.REVISION);
        builder.putParameter("client_type", Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter("version", BuildConfig.VERSION_NAME);
        builder.putParameter("device_id", DeviceHelper.getDeviceId(application, true));
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                final long endTime = System.currentTimeMillis();
                RestSearchSubmit data = gson.fromJson(response, RestSearchSubmit.class);
                if (data == null) {
                    onFailure("");
                } else {
                    if (apiCallback != null) apiCallback.onSuccess("", data.response.getDocs());
                    LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MUSIC_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
                }
            }

            @Override
            public void onFailure(String message) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MUSIC_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        if (isSuggest) builder.setTimeOut(TIME_OUT_SEARCH_SUGGEST);
        else builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    public Http searchSuggestMusic(String keyword, int limit, String type
        , @Nullable final ApiCallback<ArrayList<RestSearchSuggest.Group>> apiCallback, boolean isSuggest) {
        //todo type = song or video or album or singer or playlist
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = "TAG_SEARCH_SUGGEST_MUSIC";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(getDomainKeengMusicSearch(), KeengApi.GET_SEARCH);
        builder.putParameter("wt", "json");
        builder.putParameter("q", keyword);
        builder.putParameter("indent", "true");
        builder.putParameter("fl", "*,score");
        builder.putParameter("fq", "type:" + type);
        builder.putParameter("group.limit", String.valueOf(limit));
        builder.putParameter("group.field", "type");
        builder.putParameter("group", "true");
        builder.putParameter("sort", "score desc");

        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("revision", Config.REVISION);
        builder.putParameter("client_type", Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter("version", BuildConfig.VERSION_NAME);
        builder.putParameter("device_id", DeviceHelper.getDeviceId(application, true));
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                final long endTime = System.currentTimeMillis();
                RestSearchSuggest data = gson.fromJson(response, RestSearchSuggest.class);
                if (data != null && data.grouped != null && data.grouped.type != null && data.grouped.type.groups != null) {
                    if (apiCallback != null)
                        apiCallback.onSuccess("", data.grouped.type.groups);
                    LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MUSIC_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
                } else {
                    onFailure("");
                }
            }

            @Override
            public void onFailure(String message) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MUSIC_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        if (isSuggest) builder.setTimeOut(TIME_OUT_SEARCH_SUGGEST);
        else builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    public Http searchTiin(String keyword, int num, int page
        , @Nullable final ApiCallback<ArrayList<TiinModel>> apiCallback, boolean isSuggest) {
        final long startTime = System.currentTimeMillis();
        String TAG_REQUEST = "TAG_SEARCH_TIIN";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(getDomainTiin(), TiinApi.GET_SEARCH_TIIN);
        builder.putParameter("page", page + "");
        builder.putParameter("num", num + "");
        builder.putParameter("keywords", keyword);
//            builder.putHeader("api_key", CommonUtils.API_KEY);
//            builder.putHeader("user", CommonUtils.USER);
//            builder.putHeader("password", CommonUtils.PASS);
        builder.putHeader("version", BuildConfig.VERSION_NAME);
        builder.putHeader("device", CommonUtils.DEVICE);
        builder.putHeader("imei", CommonUtils.getDeviceID(application, true));
        builder.putHeader("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putHeader("uuid", CommonUtils.getUIID(application));
        builder.putHeader("Platform", Constants.HTTP.PLATFORM);

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                final long endTime = System.currentTimeMillis();
                ArrayList<TiinModel> data = gson.fromJson(new JSONObject(response).optString("data"), new TypeToken<ArrayList<TiinModel>>() {
                }.getType());
                if (data == null) {
                    onFailure("");
                } else {
                    if (apiCallback != null) apiCallback.onSuccess("", data);
                    LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
                }
            }

            @Override
            public void onFailure(String message) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_SEARCH, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        if (isSuggest) builder.setTimeOut(TIME_OUT_SEARCH_SUGGEST);
        else builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.utils.ImageBusiness;

import butterknife.BindView;
import butterknife.OnClick;

public class MusicSongHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_description)
    @Nullable
    TextView tvDescription;

    private SearchAllListener.OnAdapterClick listener;
    private SearchModel data;
    private String domainImage;

    public MusicSongHolder(View view, Activity activity, SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        this.listener = listener;
        this.domainImage = UrlConfigHelper.getInstance(activity).getDomainKeengMusicImage();
    }

    @SuppressLint("SetTextI18n")
    public void bindData(Object item, int position, boolean isEnd, String keySearch) {
        if (item instanceof SearchModel) {
            data = (SearchModel) item;
            if (tvTitle != null) {
                tvTitle.setText(data.getFullName());
            }
            if (tvDescription != null) {
                tvDescription.setText(data.getFullSinger());
                tvDescription.setVisibility(TextUtils.isEmpty(data.getFullSinger()) ? View.GONE : View.VISIBLE);
            }
            ImageBusiness.setSong(domainImage + data.getImage(), ivCover, 19);
        }
    }

    @OnClick(R.id.layout_root)
    public void onClickItem() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            ((SearchAllListener.OnClickBoxMusic) listener).onClickMusicItem(data);
        }
    }

    @OnClick(R.id.iv_more)
    public void onClickItemMore() {
        if (listener instanceof SearchAllListener.OnClickBoxMusic && data != null) {
            listener.onClickMenuMore(data);
        }
    }
}
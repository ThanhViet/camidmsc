package com.metfone.selfcare.module.metfoneplus.topup.model.response;

import android.app.Activity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.R;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryResponse {

    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("object")
    @Expose
    private Object object;
    @SerializedName("userMsg")
    @Expose
    private String userMsg;
    @SerializedName("wsResponse")
    @Expose
    private WsResponse wsResponse = null;

    @Getter
    @Setter
    public class WsResponse {
        @SerializedName("14days")
        @Expose
        private List<History> _14days = null;
        @SerializedName("7days")
        @Expose
        private List<History> _7days = null;
        @SerializedName("30days")
        @Expose
        private List<History> _30days = null;

    }

    @Getter
    @Setter
    public static class History {
        @SerializedName("isdn")
        @Expose
        private String isdn;
        @SerializedName("amount")
        @Expose
        private Double amount;
        @SerializedName("refill_date")
        @Expose
        private String refillDate;
        String title = "";
    }

    public static ArrayList<History> convertHistoryModel(Activity activity, WsResponse wsResponse) {
        ArrayList<History> histories = new ArrayList<>();
        if (wsResponse == null) return histories;
        for (int i = 0; i < wsResponse.get_7days().size(); i++) {
            History hist = new History();
            String refillDate = wsResponse.get_7days().get(i).getRefillDate();
            Double amount = wsResponse.get_7days().get(i).getAmount();
            String isdn = wsResponse.get_7days().get(i).getIsdn();
            if (i == 0)
                hist.setTitle(activity.getString(R.string.m_p_top_up_history_last_7_days));
            hist.setRefillDate(refillDate);
            hist.setAmount(amount);
            hist.setIsdn(isdn);
            histories.add(hist);
        }

        for (int i = 0; i < wsResponse.get_14days().size(); i++) {
            History hist = new History();
            String refillDate = wsResponse.get_14days().get(i).getRefillDate();
            Double amount = wsResponse.get_14days().get(i).getAmount();
            String isdn = wsResponse.get_14days().get(i).getIsdn();
            if (i == 0)
                hist.setTitle(activity.getString(R.string.m_p_top_up_history_last_14_days));
            hist.setRefillDate(refillDate);
            hist.setAmount(amount);
            hist.setIsdn(isdn);
            histories.add(hist);
        }
        for (int i = 0; i < wsResponse.get_30days().size(); i++) {
            History hist = new History();
            String refillDate = wsResponse.get_30days().get(i).getRefillDate();
            Double amount = wsResponse.get_30days().get(i).getAmount();
            String isdn = wsResponse.get_30days().get(i).getIsdn();
            if (i == 0)
                hist.setTitle(activity.getString(R.string.m_p_top_up_history_last_30_days));
            hist.setRefillDate(refillDate);
            hist.setAmount(amount);
            hist.setIsdn(isdn);
            histories.add(hist);
        }
        return histories;
    }
}




package com.metfone.selfcare.module.backup_restore.backup;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallbackProgressV2;
import com.metfone.selfcare.common.api.FileApiImpl;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.backup_restore.BackupMessageModel;
import com.metfone.selfcare.module.backup_restore.BackupSecurityHelper;
import com.metfone.selfcare.module.backup_restore.BackupThreadMessageModel;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;

import org.json.JSONException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class DBExporter {
    public static final String TAG = DBExporter.class.getSimpleName();

    private static DBExporter mInstance = null;
    public static final String BACKUP_FILE_NAME = "backup_message.txt";
    public static final String BACKUP_ENCRYPT_NAME = "backup_message";
    public static final String BACKUP_ZIP_NAME = "backup_message.zip";

    private static final int LIMIT_COUNT_PER_QUERY_SESSION = 1000;
    private DBExportAsync mDbExportAsync = null;

    private DBExporter() {
        Log.i(TAG, "singleton creator!!");
    }

    static DBExporter getInstance() {
        if (mInstance == null) {
            synchronized (DBExporter.class) {
                if (mInstance == null) {
                    mInstance = new DBExporter();
                }
            }
        }
        return mInstance;
    }

    //back up message text
    public void exportMessageAndThread(BackupProgressListener listener, int syncDesktop) {
        if (isExporting()) return;
        mDbExportAsync = new DBExportAsync(listener, syncDesktop);
        mDbExportAsync.execute();
    }

    public void cancelBackup() {
        try {
            if (mDbExportAsync != null && mDbExportAsync.isRunning()) {
                mDbExportAsync.cancel(true);
                clearData();
                mDbExportAsync = null;
            }
        } catch (Exception e) {
        }
    }

    public boolean isExporting() {
        return mDbExportAsync != null && mDbExportAsync.isRunning();
    }

    private static class DBExportAsync extends AsyncTask<Void, Integer, Integer> {
        boolean isRunning = false;
        BackupProgressListener mListener;
        int syncDesktop;

        DBExportAsync(BackupProgressListener listener, int syncDesktop) {
            mListener = listener;
            this.syncDesktop = syncDesktop;
        }

        @Override
        protected void onPreExecute() {
            if (mListener != null) {
                mListener.onStartBackup();
            }
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            isRunning = true;

            //export message via thread message
            //export thread
            long messagesNumber = ApplicationController.self().getMessageBusiness().getMessageNumbers();
            if (messagesNumber == 0) {
                return BackupUtils.BACKUP_CODE_NO_MESSAGES;
            }
            clearData();
            int backupMessageCount = 0;
            try {
                Gson gson = new Gson();
                int currentPercent = 0;
                CopyOnWriteArrayList<ThreadMessage> list = ApplicationController.self().getMessageBusiness().getThreadMessageArrayList();
                if (list != null && list.size() > 0) {
                    FileOutputStream foStream = ApplicationController.self().openFileOutput(BACKUP_FILE_NAME, Context.MODE_APPEND);
                    OutputStreamWriter osWriter = new OutputStreamWriter(foStream);
                    PrintWriter printWriter = new PrintWriter(osWriter);

                    for (ThreadMessage threadMessage : list) {
                        if (threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                            continue;
                        }
                        BackupThreadMessageModel backupThreadMessageModel = new BackupThreadMessageModel();
                        backupThreadMessageModel = BackupUtils.convertToBackupThreadMessageModel(threadMessage, backupThreadMessageModel);
                        String json = gson.toJson(backupThreadMessageModel);
                        //Log.d(TAG, json);
                        printWriter.println(1 + json);
                        ArrayList<Integer> mMessageIds = ApplicationController.self().getMessageBusiness().loadAllMessagesByThreadId(threadMessage.getId());

                        if (mMessageIds != null && mMessageIds.size() > 0) {
                            //export message
                            int size = mMessageIds.size();
                            int startId = 0, endId = 0;
                            endId = LIMIT_COUNT_PER_QUERY_SESSION > size ? (size - 1) : LIMIT_COUNT_PER_QUERY_SESSION - 1;
                            while (startId <= endId) {
                                CopyOnWriteArrayList<BackupMessageModel> listMessage = ApplicationController.self().getMessageBusiness().getTextMessagesWithinIds(threadMessage, mMessageIds.get(startId), mMessageIds.get(endId));
                                if (listMessage != null && listMessage.size() > 0) {
                                    for (BackupMessageModel message : listMessage) {
                                        String jsonMessage = gson.toJson(message);
                                        //Log.d(TAG, jsonMessage);
                                        printWriter.println(jsonMessage);
                                        backupMessageCount++;
                                        int percent = (int) (((backupMessageCount * 1.0f) / messagesNumber) * 100);
                                        if (percent > currentPercent) {
                                            currentPercent = percent;
                                            publishProgress(currentPercent);
                                        }
                                    }
                                }

                                startId = endId + 1;
                                endId = startId + LIMIT_COUNT_PER_QUERY_SESSION > size ? (size - 1) : startId + LIMIT_COUNT_PER_QUERY_SESSION - 1;
                            }
                        }
                    }
                    //close stream
                    printWriter.close();
                    osWriter.close();
                    foStream.close();
                }
                if (backupMessageCount == 0) {
                    return BackupUtils.BACKUP_CODE_NO_MESSAGES;
                }

            } catch (IOException e) {
                Log.e(TAG, "can not backup thread", e);
                if (BackupUtils.getAvailableInternalMemorySize() < 10 * 1024 * 1024) {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBExporter: Khong du bo nho");
                    }
                    return BackupUtils.BACKUP_CODE_FAIL_NOT_ENOUGH_MEMORY;
                } else {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBExporter: loi BACKUP_CODE_FAIL_IOEXCEPTION : " + e.toString());
                    }
                    return BackupUtils.BACKUP_CODE_FAIL_IOEXCEPTION;
                }
            } catch (Exception e) {
                Log.e(TAG, "can not backup thread", e);
                if (LogDebugHelper.getInstance() != null) {
                    LogDebugHelper.getInstance().logDebugContent("DBExporter: loi BACKUP_CODE_FAIL_EXCEPTION : " + e.toString());
                }
                return BackupUtils.BACKUP_CODE_FAIL_EXCEPTION;
            }
            File zipFile;

            try {
                //zip file in internal storage
                zipFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ZIP_NAME);
                File originalFile = new File(ApplicationController.self().getFilesDir(), BACKUP_FILE_NAME);
                BackupUtils.zipFile(originalFile, zipFile, BACKUP_FILE_NAME);
                originalFile.delete();

            } catch (IOException e) {
                Log.e(TAG, "can not zip file", e);
                if (BackupUtils.getAvailableInternalMemorySize() < 10 * 1024 * 1024) {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBExporter: Khong du bo nho de zip file");
                    }
                    return BackupUtils.BACKUP_CODE_FAIL_NOT_ENOUGH_MEMORY;
                } else {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBExporter: loi IOEXception khi zip file : " + e.toString());
                    }
                    return BackupUtils.BACKUP_CODE_FAIL_IOEXCEPTION;
                }
            } catch (Exception e) {
                Log.e(TAG, "can not zip file", e);
                if (LogDebugHelper.getInstance() != null) {
                    LogDebugHelper.getInstance().logDebugContent("DBExporter: loi Exception khi zip file : " + e.toString());
                }
                return BackupUtils.BACKUP_CODE_FAIL_EXCEPTION;
            }

            /*//encrypt file by RSA
            File encryptFile = null;
            try {
                InputStream inputStream = new FileInputStream(zipFile);
                byte[] data = BackupSecurityHelper.getBytesFromInputStream(inputStream);
                byte[] encrypted = BackupSecurityHelper.RSAUtils.encryptPublicKey(data);
                encryptFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ENCRYPT_NAME);
                FileOutputStream out = new FileOutputStream(encryptFile);
                out.write(encrypted);
                //delete zip file
                zipFile.delete();

                //close stream
                inputStream.close();
                out.close();
            } catch (IOException e) {
                Log.e(TAG, "can not encrypt file", e);
                try {
                    if (encryptFile != null) {
                        encryptFile.delete();
                    }
                } catch (Exception ex) {
                }
                if (BackupUtils.getAvailableInternalMemorySize() < 10 * 1024 * 1024) {
                    return BackupUtils.BACKUP_CODE_FAIL_NOT_ENOUGH_MEMORY;
                } else {
                    return BackupUtils.BACKUP_CODE_FAIL_IOEXCEPTION;
                }
            } catch (Exception e) {
                Log.e(TAG, "can not encrypt file", e);
                try {
                    if (encryptFile != null) {
                        encryptFile.delete();
                    }
                } catch (Exception ex) {
                }
                return BackupUtils.BACKUP_CODE_FAIL_EXCEPTION;
            }*/

            //encrypt file by AES
            File encryptFile = null;
            try {
                InputStream inputStream = new FileInputStream(zipFile);
                byte[] data = BackupSecurityHelper.getBytesFromInputStream(inputStream);
                byte[] encrypted = BackupSecurityHelper.AESCrypt.getInStance().encrypt(data);
                encryptFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ENCRYPT_NAME);
                FileOutputStream out = new FileOutputStream(encryptFile);
                out.write(encrypted);
                //delete zip file
                zipFile.delete();

                //close stream
                inputStream.close();
                out.close();
            } catch (IOException e) {
                Log.e(TAG, "can not encrypt file", e);
                try {
                    if (encryptFile != null) {
                        encryptFile.delete();
                    }
                } catch (Exception ex) {
                }
                if (BackupUtils.getAvailableInternalMemorySize() < 10 * 1024 * 1024) {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBExporter: khong du bo nho khi ma hoa file : " + e.toString());
                    }
                    return BackupUtils.BACKUP_CODE_FAIL_NOT_ENOUGH_MEMORY;
                } else {
                    if (LogDebugHelper.getInstance() != null) {
                        LogDebugHelper.getInstance().logDebugContent("DBExporter: loi IO khi ma hoa file : " + e.toString());
                    }
                    return BackupUtils.BACKUP_CODE_FAIL_IOEXCEPTION;
                }
            } catch (Exception e) {
                Log.e(TAG, "can not encrypt file", e);
                try {
                    if (encryptFile != null) {
                        encryptFile.delete();
                    }
                } catch (Exception ex) {
                }
                if (LogDebugHelper.getInstance() != null) {
                    LogDebugHelper.getInstance().logDebugContent("DBExporter: EXCEPTION khi ma hoa file : " + e.toString());
                }
                return BackupUtils.BACKUP_CODE_FAIL_EXCEPTION;
            }

            return BackupUtils.BACKUP_CODE_SUCCESSFULLY;
        }

        @Override
        protected void onPostExecute(Integer value) {
            super.onPostExecute(value);
            if (value == null) return;
            int code = value;
            if (code == BackupUtils.BACKUP_CODE_SUCCESSFULLY) {
                // upload to server

                if (mListener != null) {
                    mListener.onBackupUploadProgress(0);
                }
                final File encryptFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ENCRYPT_NAME);
                FileApiImpl fileApi = new FileApiImpl();
                fileApi.uploadBackupFile(encryptFile.getAbsolutePath(), syncDesktop, new ApiCallbackProgressV2<String>() {
                    @Override
                    public void onProgress(float progress) {
                        Log.d(TAG, "uploadProgress: " + progress);
                        if (mListener != null) {
                            mListener.onBackupUploadProgress((int) progress);
                        }
                    }

                    @Override
                    public void onSuccess(String lastId, String s) throws JSONException {
                        Log.d(TAG, "uploadSuccess: " + lastId + " --- " + s);
                        if (mListener != null) {
                            mListener.onBackupUploadProgress(100);
                            mListener.onBackupComplete();
                            mListener = null;
                        }
                        SharedPreferences pref = ApplicationController.self().getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                        if (pref != null) {
                            pref.edit().putLong(SharedPrefs.KEY.BACKUP_LAST_TIME, System.currentTimeMillis()).apply();
                        }
                    }

                    @Override
                    public void onError(String s) {
                        Log.e(TAG, "uploadError: " + s);
                        if (mListener != null) {
                            mListener.onBackupFail(BackupUtils.BACKUP_CODE_FAIL_UPLOAD);
                        }
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "uploadComplete");
                        encryptFile.delete();
                        mListener = null;
                        isRunning = false;
                    }
                });

            } else {
                if (mListener != null) {
                    mListener.onBackupFail(code);
                    mListener = null;
                }
                isRunning = false;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (mListener != null && values != null && values.length > 0) {
                int percent = values[0];
                mListener.onBackupProgress(percent);
            }
        }

        boolean isRunning() {
            return isRunning;
        }

        @Override
        protected void onCancelled() {
            isRunning = false;
            super.onCancelled();
            if (mListener != null) {
                mListener.onBackupFail("Backup was cancelled!");
                mListener = null;
            }
        }

        @Override
        protected void onCancelled(Integer integer) {
            isRunning = false;
            super.onCancelled(integer);
            if (mListener != null) {
                mListener.onBackupFail("Backup was cancelled!");
                mListener = null;
            }
        }
    }

    public static void clearData() {
        try {
            File txtFile = new File(ApplicationController.self().getFilesDir(), BACKUP_FILE_NAME);
            File zipFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ZIP_NAME);
            File encryptFile = new File(ApplicationController.self().getFilesDir(), BACKUP_ENCRYPT_NAME);
            if (txtFile.exists()) {
                txtFile.delete();
            }

            if (zipFile.exists()) {
                zipFile.delete();
            }

            if (encryptFile.exists()) {
                encryptFile.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "clearDataBeforeExportDB: Exception", e);
        }
    }

    public interface BackupProgressListener {
        void onStartBackup();

        void onBackupProgress(int percent);

        void onBackupUploadProgress(int percent);

        void onBackupComplete();

        void onBackupFail(String message);

        void onBackupFail(int code);
    }
}

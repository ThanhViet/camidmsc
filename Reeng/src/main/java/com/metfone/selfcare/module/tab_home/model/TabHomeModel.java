/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/20
 *
 */

package com.metfone.selfcare.module.tab_home.model;

import java.io.Serializable;
import java.util.ArrayList;

public class TabHomeModel implements Serializable {
    public static final int TYPE_SLIDER_BANNER = 2;
    public static final int TYPE_FEATURE = 3;
    public static final int TYPE_QUICK_CONTACT = 4;
    public static final int TYPE_BOX_VIDEO = 5;
    public static final int TYPE_BOX_MUSIC = 6;
    public static final int TYPE_BOX_MOVIE = 7;
    public static final int TYPE_BOX_NEWS = 8;
    public static final int TYPE_BOX_COMIC = 9;
    public static final int TYPE_BOX_TIIN = 10;
    public static final int TYPE_LARGE_VIDEO = 11;
    public static final int TYPE_BOX_GRID_VIDEO = 12;
    public static final int TYPE_GRID_VIDEO = 13;
    public static final int TYPE_LARGE_MOVIE = 14;
    public static final int TYPE_BOX_GRID_MOVIE = 15;
    public static final int TYPE_GRID_MOVIE = 16;
    public static final int TYPE_LARGE_MUSIC = 17;
    public static final int TYPE_BOX_GRID_MUSIC = 18;
    public static final int TYPE_GRID_MUSIC = 19;
    public static final int TYPE_LARGE_NEWS = 20;
    public static final int TYPE_BOX_GRID_NEWS = 21;
    public static final int TYPE_GRID_NEWS = 22;
    public static final int TYPE_LARGE_TIIN = 23;
    public static final int TYPE_BOX_GRID_TIIN = 24;
    public static final int TYPE_GRID_TIIN = 25;
    public static final int TYPE_LARGE_COMIC = 26;
    public static final int TYPE_BOX_GRID_COMIC = 27;
    public static final int TYPE_GRID_COMIC = 28;
    public static final int TYPE_BOX_GRID_CHANNEL = 29;
    public static final int TYPE_GRID_CHANNEL = 30;

    private static final long serialVersionUID = 5713481711408798602L;

    private Object object;
    private ArrayList<Object> list;
    private String id;
    private int type;
    private String title;
    private long currentTime;

    public TabHomeModel() {
        this.currentTime = System.currentTimeMillis();
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public ArrayList<Object> getList() {
        if (list == null) list = new ArrayList<>();
        return list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCurrentTime() {
        this.currentTime = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "TabHomeModel{" +
                "type=" + type +
                ", object=" + object +
                ", list=" + list +
                ", id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", currentTime=" + currentTime +
                '}';
    }
}

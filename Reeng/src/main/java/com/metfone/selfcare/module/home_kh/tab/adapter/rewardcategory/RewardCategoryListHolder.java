package com.metfone.selfcare.module.home_kh.tab.adapter.rewardcategory;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.home_kh.tab.adapter.events.OnClickHomeReward;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardSearchItem;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;

import butterknife.BindView;

public class RewardCategoryListHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv)
    @Nullable
    TextView tvName;

    @BindView(R.id.icon)
    @Nullable
    ImageView icon;

    @BindView(R.id.root)
    @Nullable
    ViewGroup viewRoot;

    @BindView(R.id.badge)
    CardView badge;

    private OnClickHomeReward listener;
    private Activity activity;

    public RewardCategoryListHolder(View view, Activity activity, OnClickHomeReward listener) {
        super(view);
        this.listener = listener;
        this.activity = activity;

//        ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
//        layoutParams.width = TabHomeUtils.getWidthKhHomeRewardCate();
//        viewRoot.setLayoutParams(layoutParams);
//        viewRoot.requestLayout();
    }

    @Override
    public void bindData(Object item, int position) {
        KhHomeRewardSearchItem homeData = (KhHomeRewardSearchItem) item;

        String url = "";
        if (homeData.icon == null || homeData.icon.isEmpty()) {
            url = homeData.giftObject.iconUrl;
        } else {

            url = homeData.icon;
        }
        if (homeData.icon != null) {
            int drawable = homeData.icon.equals("more") ? R.drawable.ic_reward_more : R.drawable.ic_home_reward_metfone;
            Glide.with(activity)
                    .load(drawable)
                    .error(R.drawable.bg_kh_home_reward_cate)
                    .placeholder(R.drawable.bg_kh_home_reward_cate)
                    .into(icon);
        } else {
            Glide.with(activity)
                    .load(url)
                    .error(R.drawable.bg_kh_home_reward_cate)
                    .placeholder(R.drawable.bg_kh_home_reward_cate)
                    .into(icon);
        }
        viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) {
                    listener.onClickRewardCategoryItem(homeData, getAdapterPosition());
                }
            }
        });

        if(homeData.id == 990999){
            badge.setVisibility(View.VISIBLE);
        }

        if (homeData.id == 991999) {
            icon.setPadding(5,5,5,5);
        } else {
            icon.setPadding(0,0,0,0);
        }

        String name = "";
        if (homeData.icon == null || homeData.icon.isEmpty()) {
            name = homeData.giftObject.giftTypeName;
        } else {
            name = homeData.name;
        }
        assert tvName != null;
        tvName.setText(name);
    }
}

package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;

import java.io.Serializable;

public class AuthKeyModel implements Serializable{
    @SerializedName("authenkey")
    String authenkey;
    @SerializedName("linkGame")
    String linkGame;
    @SerializedName("fbShare")
    String fbShare;

    public String getAuthenkey() {
        return authenkey;
    }

    public void setAuthenkey(String authenkey) {
        this.authenkey = authenkey;
    }

    public String getLinkGame() {
        return linkGame;
    }

    public void setLinkGame(String linkGame) {
        this.linkGame = linkGame;
    }

    public String getFbShare() {
        return fbShare;
    }

    public void setFbShare(String fbShare) {
        this.fbShare = fbShare;
    }
}

package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SCPostageDetail implements Serializable {

    @SerializedName("isdn")
    @Expose
    private String isdn;

    @SerializedName("direction")
    @Expose
    private String direction;

    @SerializedName("start_time")
    @Expose
    private long start_time;

    @SerializedName("duration")
    @Expose
    private int duration;

    @SerializedName("value")
    @Expose
    private double value;

    @SerializedName("total")
    @Expose
    private int total;

    private int icon;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }


    @Override
    public String toString() {
        return "SCPostageDetail{" +
                "isdn='" + isdn + '\'' +
                ", direction='" + direction + '\'' +
                ", start_time=" + start_time +
                ", duration=" + duration +
                ", value=" + value +
                ", total=" + total +
                '}';
    }
}

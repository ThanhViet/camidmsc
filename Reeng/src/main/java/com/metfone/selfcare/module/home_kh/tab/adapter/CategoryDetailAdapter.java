/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.home_kh.tab.adapter;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.module.home_kh.tab.adapter.customtopic.MovieHolder;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.util.Log;

public class CategoryDetailAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {

    private OnClickContentMovie listener;
    private int widthItem;
    private SubtabInfo tabInfo;
    private int parentViewType;

    public CategoryDetailAdapter(Activity act, int viewType) {
        super(act);
        this.parentViewType = viewType;
    }

    public void setTabInfo(SubtabInfo tabInfo) {
        this.tabInfo = tabInfo;
    }

    public void setWidthItem(int widthItem) {
        this.widthItem = widthItem;
    }

    public void setListener(OnClickContentMovie listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof IHomeModelType) {
            return ((IHomeModelType) item).getItemType();
        } else if (item instanceof MovieWatched) {
            return TYPE_EMPTY;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case IHomeModelType.CUSTOM_TOPIC:
                return new MovieHolder(layoutInflater.inflate(R.layout.cinema_continue_item, parent, false), activity, listener);
        }

        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

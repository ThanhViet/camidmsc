package com.metfone.selfcare.module.home_kh.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.core.content.ContextCompat;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.setting.BackupFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.fragment.setting.BackupKhFragment;

public class BackupKhActivity extends BaseSlidingFragmentActivity {
    private final String TAG = BackupKhActivity.class.getSimpleName();
    public static final String SYNC_DESKTOP = "sync_desktop";
    SharedPreferences mPref;
    ApplicationController mApplication;
    private BackupKhFragment mBackUpFragment;
    int syncDesktop = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_kh);
//        setColorStatusBar(R.color.kh_background);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.kh_background));
        } else {
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.kh_background));
            }
        }
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        mApplication = (ApplicationController) getApplicationContext();
        if (getIntent() != null)
            syncDesktop = getIntent().getIntExtra(SYNC_DESKTOP, 0);
//        setActionBar();
        if (savedInstanceState == null) {
            displayBackupFragment();
        }
        trackingScreen(TAG);
    }

    private void setActionBar() {
        setToolBar(findViewById(R.id.tool_bar));
    }

    public void displayBackupFragment() {
        mBackUpFragment = BackupKhFragment.newInstance(syncDesktop);
        executeFragmentTransaction(mBackUpFragment, R.id.fragment_container, false, true);
    }

    @Override
    public void onBackPressed() {
        if (mBackUpFragment != null && mBackUpFragment.isBackupInProgress()) {
            //backup is in progress
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        mBackUpFragment = null;
        super.onDestroy();
    }
}

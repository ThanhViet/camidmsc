package com.metfone.selfcare.module.keeng.fragment.category;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.RankModel;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class RankDetailFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    private static final int TAB_SONG_POSITION = 0;
    private static final int TAB_MV_POSITION = 1;

    private RankModel mItem;
    private ViewPagerDetailAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int currentTab = -1;
    private AppBarLayout appBarLayout;
    private TextView tvTitleToolbar;
    private TextView tvTotalSong;
    private ListenerUtils listenerUtils;
    private RelativeLayout rlFilter;

    public static RankDetailFragment newInstance() {
        Bundle args = new Bundle();
        RankDetailFragment fragment = new RankDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_rank_detail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) {
            return null;
        }
        toolbar = view.findViewById(R.id.main_toolbar);
        tvTitleToolbar = view.findViewById(R.id.tvTitleToolbar);
        tvTotalSong = view.findViewById(R.id.tvTotalSong);
        appBarLayout = view.findViewById(R.id.main_appbar);
        tabLayout = view.findViewById(R.id.tabs);
        rlFilter = view.findViewById(R.id.rlFilter);
        viewPager = view.findViewById(R.id.view_pager);
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
        return view;
    }

    @Override
    public void onDetach() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        if (viewPager != null)
            viewPager.setAdapter(null);
        adapter = null;
        App.getInstance().cancelPendingRequests(KeengApi.GET_INFO_RANK);
        super.onDetach();
    }

    private void setupNavigation() {
        toolbar.setNavigationIcon(mActivity.getResources().getDrawable(R.drawable.ic_v5_back));
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        if (appBarLayout != null) {
            appBarLayout.setExpanded(true);
        }
    }

    @Override
    public String getName() {
        return "RankDetailFragment";
    }

    private void initViewPager() {
        currentTab = 0;
        tvTitleToolbar.setText(mItem.getTitle());
        tvTitleToolbar.setSelected(true);
        setupViewPager(viewPager);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            if (getArguments() != null) {
                mItem = (RankModel) getArguments().getSerializable(Constants.KEY_DATA);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        if (mItem == null) {
            onBackPressed();
            return;
        }
        setupNavigation();
        new Handler().postDelayed(this::initViewPager, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (currentTab >= 0)
            setCurrentItemViewPage(currentTab);
    }

    public void setCurrentItemViewPage(int index) {
        try {
            viewPager.setCurrentItem(index);
            if (tabLayout == null) {
                return;
            }
            TabLayout.Tab tab = tabLayout.getTabAt(index);
            if (tab == null) {
                return;
            }
            tab.select();
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        currentTab = position;
        updateTotalSongView();
    }

    private void updateTotalSongView() {
        try {
            Fragment fragment = adapter.getItem(currentTab);
            if (fragment instanceof ChildRankFragment) {
                List<AllModel> list = ((ChildRankFragment) fragment).getDatas();
                int listSize = list.size();
                rlFilter.setVisibility(listSize > 0 ? View.VISIBLE : View.GONE);
                switch (currentTab) {
                    case TAB_SONG_POSITION:
                        if (listSize == 1)
                            tvTotalSong.setText(getString(R.string.music_total_song, listSize));
                        else
                            tvTotalSong.setText(getString(R.string.music_total_songs, listSize));
                        break;
                    case TAB_MV_POSITION:
                        if (listSize == 1)
                            tvTotalSong.setText(getString(R.string.total_mv, listSize));
                        else
                            tvTotalSong.setText(getString(R.string.total_mvs, listSize));
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerDetailAdapter(getChildFragmentManager());
        Bundle bundle1 = new Bundle();
        bundle1.putSerializable(Constants.KEY_DATA, mItem);
        bundle1.putInt("type", Constants.TYPE_SONG);
        adapter.addFragment(ChildRankFragment.newInstances(bundle1, this::updateTotalSongView), getString(R.string.song));

        Bundle bundle2 = new Bundle();
        bundle2.putSerializable(Constants.KEY_DATA, mItem);
        bundle2.putInt("type", Constants.TYPE_VIDEO);
        adapter.addFragment(ChildRankFragment.newInstances(bundle2, this::updateTotalSongView), getString(R.string.video_mv));

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(viewPager);
        adapter.notifyDataSetChanged();
        setCurrentItemViewPage(0);
    }
}

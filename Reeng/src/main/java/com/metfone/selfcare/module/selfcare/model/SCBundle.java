package com.metfone.selfcare.module.selfcare.model;

import java.io.Serializable;

public class SCBundle implements Serializable {
    private int type;
    private Object object;

    public SCBundle(int type, Object object) {
        this.type = type;
        this.object = object;
    }

    public SCBundle(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "SCBundle{" +
                "object=" + object +
                '}';
    }
}

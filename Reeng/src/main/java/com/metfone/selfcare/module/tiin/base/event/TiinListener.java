package com.metfone.selfcare.module.tiin.base.event;

import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;
import com.metfone.selfcare.module.tiin.detailtiin.childdetailtiin.adapter.ChildDetailTiinAdapter;
import com.metfone.selfcare.module.tiin.network.model.Category;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

public interface TiinListener {
    interface OnHomeTiinItemListener {
        void onItemClick(TiinModel model);

        void onItemClickHeader(int type, int categoryId, String title);

        void onItemClickEvent(TiinModel model);

        void onItemClickVideo(TiinModel model);

        void onItemClickMore(TiinModel model);

        void onItemClickHashTag(TiinModel model);
    }

    interface onCategoryItemListener {
        void onItemClick(TiinModel model);

        void onItemClickMore(TiinModel model);

        void onItemClickHashTag(TiinModel model);
    }

    interface onDetailTiinItemListener {
        void onItemClickRelate(TiinModel model);

        void onClickLinkText(String text, String url);

        void onClickImage(NewsDetailModel model);

        void onPlayVideo(NewsDetailModel model, int position, ChildDetailTiinAdapter.NewsDetailVideoHolder holder);

        void onDetachVideoDetail();

        void onItemClickMore(TiinModel model);
    }

    interface onConfigItemListener {
        void onItemConfig(Category category, int position);
    }

    interface onCategoryNow {
        void onItemClick(TiinModel model);

        void onItemClickHeader(String title, int id);

        void onItemClickMore(TiinModel model);

        void onItemClickHashTag(TiinModel model);
    }

}

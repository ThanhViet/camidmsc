package com.metfone.selfcare.module.onlinespoint;

import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

public class ModelOnlineSpoint implements Serializable {

    private static final String TAG = ModelOnlineSpoint.class.getSimpleName();
    /*public static final int STATE_COUNTING = 0;
    public static final int STATE_STOP_COUNTING = 1;
    public static final int STATE_SEND_SERVER = 2;*/

    private long timeOnline;
    //    private int state;
    private long lastTimeSendToServer;


    public ModelOnlineSpoint() {
    }

    public long getTimeOnline() {
        return timeOnline;
    }

    public void setTimeOnline(long timeOnline) {
        this.timeOnline = timeOnline;
    }

//    public int getState() {
//        return state;
//    }
//
//    public void setState(int state) {
//        this.state = state;
//    }

    public long getLastTimeSendToServer() {
        return lastTimeSendToServer;
    }

    public void setLastTimeSendToServer(long lastTimeSendToServer) {
        /*if (timeOnline >= OnlineSpointHelper.TIME_TO_REWARD) {
            this.lastTimeSendToServer = 0;
//            state = STATE_STOP_COUNTING;
            return;
        }*/
        this.lastTimeSendToServer = lastTimeSendToServer;
//        state = STATE_COUNTING;
    }

    public void fromString(String s) {
        Log.i(TAG, "S----------fromString: " + s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            timeOnline = jsonObject.optLong("timeOnline");
//            state = jsonObject.optInt("state");
            setLastTimeSendToServer(jsonObject.optLong("lastTimeSendToServer"));
        } catch (Exception ex) {
            Log.e(TAG, "S----------Exception fromString", ex);
        }
    }

    public String toString() {
        String s = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("timeOnline", timeOnline);
            jsonObject.put("lastTimeSendToServer", lastTimeSendToServer);
//            jsonObject.put("state", state);
            s = jsonObject.toString();
        } catch (Exception ex) {
            Log.e(TAG, "S----------Exception toString", ex);
        }
        Log.i(TAG, "S----------toString: " + s);
        return s;
    }

    public boolean isDoneInDay() {
        return (TimeHelper.checkTimeInDay(lastTimeSendToServer));
    }
}

package com.metfone.selfcare.module.metfoneplus.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.addAccount.AddAccountFtthUserResponse;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.model.event.RequestRefreshInfo;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackSwitchFragment;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class AddFeedbackActivity extends BaseSlidingFragmentActivity implements OnInternetChangedListener {
    private AlertDialog alertDialog;
    public String account = "";
    public String contractPoint = "";
    public UserInfoBusiness userInfoBusiness;
    public ArrayList<ServicesModel> servicesModelList = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_feedback);
        changeStatusBar(true);
        changeStatusBar(Color.parseColor("#161819"));
        if (savedInstanceState == null) {
//            getUserInformation();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frameLayout, new MPFeedbackSwitchFragment())
                    .commitNow();
        }
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else {
            connected = false;
            showDialog("Lost connect", "Lost connection. back to previous page!");
        }
        userInfoBusiness = new UserInfoBusiness(this);
        List<Services> services = userInfoBusiness.getServiceList();
        servicesModelList = UserInfoBusiness.convertServicesToServiceModelList(services);
        //getUserInformation();
    }

    public void getUserInformation() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.getFtthAccount(new ApiCallback<BaseResponse<ArrayList<AddAccountFtthUserResponse>>>() {
            @Override
            public void onResponse(Response<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> response) {

                android.util.Log.e(TAG, "onResponse: " + response.body());
                if (response.body() != null) {
                    if (Integer.valueOf(response.body().getCode()) == 0) {
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            account = response.body().getData().get(0).getPhoneNumber();
                            contractPoint = response.body().getData().get(0).getContractPoint();

                        }
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frameLayout, new MPFeedbackSwitchFragment())
                                .commitNow();
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void showDialog(String title, String messages) {
        alertDialog = new AlertDialog.Builder(AddFeedbackActivity.this)
                .setTitle(title)
                .setMessage(messages)
                .setNegativeButton("Yes", (dialog, which) -> {
                    alertDialog.dismiss();
                    finish();
                })
                .show();
    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(AddFeedbackActivity.this)) {
            EventBus.getDefault().postSticky(new LoadInfoUser(LoadInfoUser.LoadType.LOADING));
            Log.d(TAG, "Lost internet");
        } else {
            EventBus.getDefault().post(new RequestRefreshInfo());
            Log.d(TAG, "Connecting internet");
        }
    }

    public void showError(String message) {
        showError(message, getString(R.string.error));
    }

    public void connectionError() {
        showError(getString(R.string.connection_error));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideKeyboard();
    }

    public void showKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
package com.metfone.selfcare.module.metfoneplus.adapter;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;
import com.metfone.selfcare.module.home_kh.tab.adapter.banner.KhHomeGameBannerHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.banner.KhHomeLuckyGameBannerHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.banner.KhSlideBannerHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.customtopic.BoxContentHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.gift.KhHomeGiftViewHolder;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;

public class MPHomeAdapter extends BaseAdapter<BaseAdapter.ViewHolder, IHomeModelType> {

    private TabHomeKhListener.OnAdapterClick listener;

    public void setListener(TabHomeKhListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    private RecyclerView.RecycledViewPool recycledPoolHeader;
    private RecyclerView.RecycledViewPool recycledPoolVertical;
    private RecyclerView.RecycledViewPool recycledPoolHorizontal;

    public MPHomeAdapter(Activity activity) {
        super(activity);
        recycledPoolHeader = new RecyclerView.RecycledViewPool();
        recycledPoolVertical = new RecyclerView.RecycledViewPool();
        recycledPoolHorizontal = new RecyclerView.RecycledViewPool();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case IHomeModelType.MP_SERVICE:
            case IHomeModelType.REWARD_CATEGORY:
            case IHomeModelType.REWARD_TOPIC_2:
                BoxContentHolder holder = new BoxContentHolder(layoutInflater.inflate(R.layout.item_mp_home_slide, parent, false), activity, listener, viewType);
                if (holder.getRecycler() != null) {
                    holder.getRecycler().setRecycledViewPool(recycledPoolVertical);
                }
                return holder;
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IHomeModelType item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getItemType();
    }
}

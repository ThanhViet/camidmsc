package com.metfone.selfcare.module.home_kh.api.request;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class ABAMobilePaymentTopUpCallBackRequest extends BaseRequest<ABAMobilePaymentTopUpCallBackRequest.Request> {
    public static class Request{
        public String txnid;
    }
}

package com.metfone.selfcare.module.metfoneplus.topup.model.request;

import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseDataRequestMP<T> {

    public T wsRequest;
    public String token = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_TOKEN, String.class);
    public String sessionId = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_SESSION_ID, String.class);
    public String language = "en";
    public String wsCode;

    public BaseDataRequestMP(T wsRequest, String wsCode) {
        this.wsRequest = wsRequest;
        this.wsCode = wsCode;
    }
}

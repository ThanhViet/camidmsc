package com.metfone.selfcare.module.livestream.holder;

import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.livestream.listener.MessageActionListener;
import com.metfone.selfcare.module.livestream.model.LiveStreamMessage;

import com.metfone.selfcare.ui.androidtagview.TagGroup;

public class MsgLikeVideoHolder extends BaseMessageLiveStreamHolder {

    private TagGroup tagGroup;
    private BaseSlidingFragmentActivity activity;
    private ApplicationController app;
    private MessageActionListener listenerMsg;
    private LiveStreamMessage message;

    public MsgLikeVideoHolder(View itemView, BaseSlidingFragmentActivity act, MessageActionListener listener) {
        super(itemView);

        this.activity = act;
        app = (ApplicationController) activity.getApplication();
        this.listenerMsg = listener;
        tagGroup = itemView.findViewById(R.id.tagLikeVideo);
        String s1 = activity.getString(R.string.ls_like);
        String s2 = activity.getString(R.string.ls_share);
        tagGroup.setTags(s1, s2);
        tagGroup.setOnTagClickListener(new TagGroup.OnTagClickListener() {
            @Override
            public void onTagClick(TagGroup.TagView tagView, String tag) {
                if (listenerMsg != null && !TextUtils.isEmpty(tag))
                    if (tag.equals(activity.getString(R.string.ls_like)) || tag.equals(activity.getString(R.string.ls_unlike)))
                        listenerMsg.onLikeVideo(tagGroup);
                    else
                        listenerMsg.onShareVideo(false);
            }
        });
    }

    @Override
    public void setElement(Object obj, int pos) {
        if (obj instanceof LiveStreamMessage) {
            message = (LiveStreamMessage) obj;

            if (message.getCurrentVideo() != null) {
                boolean isLike = message.getCurrentVideo().isLike();
                String s1 = activity.getString(R.string.ls_like);
                if (isLike)
                    s1 = activity.getString(R.string.ls_unlike);
                String s2 = activity.getString(R.string.ls_share);
                tagGroup.setTags(s1, s2);
            }
        }
    }
}

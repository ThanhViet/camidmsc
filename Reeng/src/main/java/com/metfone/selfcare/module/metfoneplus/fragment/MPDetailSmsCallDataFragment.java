package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.CallingCharge;
import com.metfone.selfcare.model.camid.DataCharge;
import com.metfone.selfcare.model.camid.DetailSmsCallingDataCharge;
import com.metfone.selfcare.model.camid.DetailType;
import com.metfone.selfcare.model.camid.HistoryChargeDetail;
import com.metfone.selfcare.model.camid.SmsCallingDataCharge;
import com.metfone.selfcare.model.camid.SmsCharge;
import com.metfone.selfcare.model.camid.VasCharge;
import com.metfone.selfcare.module.metfoneplus.adapter.DetailSmsCallingDataAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryChargeDetailResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

import static com.metfone.selfcare.model.camid.DetailType.DATE_SEVEN_DAYS;
import static com.metfone.selfcare.model.camid.DetailType.DATE_TODAY;

public class MPDetailSmsCallDataFragment extends MPBaseFragment {
    public static final String TAG = "MPDetailSmsCallDataFrag";
    private static final String TITLE_SCREEN_PARAM = "title_screen";

    /**
     * {@link com.metfone.selfcare.model.camid.DetailType.BalanceName}
     */
    private static final String BALANCE_NAME_PARAM = "balance_name";

    /**
     * {@link com.metfone.selfcare.model.camid.DetailType.DateType}
     */
    private static final String DATE_TYPE_PARAM = "date_type";

    /**
     * {@link com.metfone.selfcare.model.camid.DetailType.DateName}
     */
    private static final String DATE_NAME_PARAM = "date_name";

    /**
     * {@link com.metfone.selfcare.model.camid.DetailType.ChargeType} only: sms, calling, data, vas
     */
    private static final String CHARGE_TYPE_PARAM = "charge_type";

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_back)
    RelativeLayout mActionBarBack;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.txt_date_charge)
    AppCompatTextView mContentDateCharge;
    @BindView(R.id.list_detail_history_charge)
    RecyclerView mListDetailCharge;

    private DetailSmsCallingDataAdapter mDetailSmsCallingDataAdapter;
    private String mTitleScreen = "";
    private String mBalanceName = "";
    private String mDateType = "";
    private String mDateName = "";
    private String mChargeType = "";

    public MPDetailSmsCallDataFragment() {

    }

    public static MPDetailSmsCallDataFragment newInstance(String titleScreen,
                                                          @DetailType.DateName String dateName,
                                                          @DetailType.BalanceName String balanceName,
                                                          @DetailType.ChargeType String chargeType,
                                                          @DetailType.DateType String dateType) {
        MPDetailSmsCallDataFragment fragment = new MPDetailSmsCallDataFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TITLE_SCREEN_PARAM, titleScreen);
        bundle.putString(DATE_NAME_PARAM, dateName);
        bundle.putString(BALANCE_NAME_PARAM, balanceName);
        bundle.putString(CHARGE_TYPE_PARAM, chargeType);
        bundle.putString(DATE_TYPE_PARAM, dateType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mp_sms_call_data_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mLayoutActionBar);
        if (getArguments() != null) {
            mTitleScreen = getArguments().getString(TITLE_SCREEN_PARAM);
            mDateName = getArguments().getString(DATE_NAME_PARAM);
            mBalanceName = getArguments().getString(BALANCE_NAME_PARAM);
            mChargeType = getArguments().getString(CHARGE_TYPE_PARAM);
            mDateType = getArguments().getString(DATE_TYPE_PARAM);
        }

        mActionBarTitle.setText(mTitleScreen);
        if (DATE_TODAY.equals(mDateName)) {
            mDateName = getString(R.string.m_p_today);
        } else if (DATE_SEVEN_DAYS.equals(mDateName)) {
            mDateName = getString(R.string.m_p_7_days);
        } else {
            mDateName = getString(R.string.m_p_30_todays);
        }
        mContentDateCharge.setText(mDateName);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mParentActivity, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mParentActivity, R.drawable.divider_list_service)));
        mDetailSmsCallingDataAdapter = new DetailSmsCallingDataAdapter(getContext(), new ArrayList<DetailSmsCallingDataCharge>());
        mListDetailCharge.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListDetailCharge.addItemDecoration(dividerItemDecoration);
        mListDetailCharge.setAdapter(mDetailSmsCallingDataAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getWSHistoryChargeDetail(mBalanceName, mChargeType, getStartTimeFromDateType(mDateType));
    }

    @OnClick(R.id.action_bar_back)
    void onBack() {
        popBackStackFragment();
    }

    private void getWSHistoryChargeDetail(String balanceName, String historyCharge, String startTime) {
//        mParentActivity.showLoadingDialog("", R.string.waiting);
        new MetfonePlusClient()
                .wsHistoryChargeDetail(balanceName, historyCharge, startTime, new MPApiCallback<WsHistoryChargeDetailResponse>() {
                    @Override
                    public void onResponse(Response<WsHistoryChargeDetailResponse> response) {
                        if (response.body() != null && response.body().getResult().getWsResponse() != null) {
                            updateUICharge(response.body().getResult().getWsResponse());
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        mParentActivity.hideLoadingDialog();
                    }
                });
    }

    private void updateUICharge(WsHistoryChargeDetailResponse.Response response) {
        List<DetailSmsCallingDataCharge> list = new ArrayList<>();
        SmsCharge smsCharge;
        CallingCharge callingCharge;
        DataCharge dataCharge;
        SmsCallingDataCharge smsCallingData;
        VasCharge vasCharge;

        if (mChargeType.equals(DetailType.CHARGE_TYPE_SMS)
                || mChargeType.equals(DetailType.CHARGE_TYPE_CALLING)) {
            smsCallingData = new SmsCallingDataCharge(response.getMoney(),
                    response.getTotal());
        } else {
            smsCallingData = new SmsCallingDataCharge(response.getMoney(),
                    response.getQuantity());
        }

        if (mChargeType.equals(DetailType.CHARGE_TYPE_DATA)) {
            smsCallingData.setIsData(true);
            smsCallingData.setData(response.getData().intValue());
        }

        list.add(smsCallingData);

        List<HistoryChargeDetail> historyChargeDetails = response.getHistoryChargeDetailList();
        if (mChargeType.equals(DetailType.CHARGE_TYPE_SMS)) {
            for (int i = 0; i < historyChargeDetails.size(); i++) {
                HistoryChargeDetail charge = historyChargeDetails.get(i);
                smsCharge = new SmsCharge(charge.getDate(),
                        charge.getPhoneNumber(),
                        charge.getMoney(),
                        charge.getTime());
                list.add(smsCharge);
            }
        }

        if (mChargeType.equals(DetailType.CHARGE_TYPE_CALLING)) {
            for (int i = 0; i < historyChargeDetails.size(); i++) {
                HistoryChargeDetail charge = historyChargeDetails.get(i);
                callingCharge = new CallingCharge(charge.getDate(),
                        charge.getPhoneNumber(),
                        charge.getMoney(),
                        charge.getTime(),
                        charge.getTime());
                list.add(callingCharge);
            }
        }

        if (mChargeType.equals(DetailType.CHARGE_TYPE_DATA)) {
            for (int i = 0; i < historyChargeDetails.size(); i++) {
                HistoryChargeDetail charge = historyChargeDetails.get(i);
                dataCharge = new DataCharge(charge.getDate(),
                        charge.getTotalInt(),
                        charge.getUnit(),
                        charge.getMoney());
                list.add(dataCharge);
            }
        }

        if (mChargeType.equals(DetailType.CHARGE_TYPE_VAS)) {
            for (int i = 0; i < historyChargeDetails.size(); i++) {
                HistoryChargeDetail charge = historyChargeDetails.get(i);
                vasCharge = new VasCharge(charge.getTotal(),
                        charge.getMoney(),
                        charge.getType(),
                        charge.getStartTime(),
                        charge.getCode());
                list.add(vasCharge);
            }
        }

        mDetailSmsCallingDataAdapter.replaceData(list);
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/20
 */

package com.metfone.selfcare.module.saving.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.LoadingView;
import com.metfone.selfcare.module.saving.activity.SavingActivity;
import com.metfone.selfcare.module.saving.adapter.SavingHistoryDetailAdapter;
import com.metfone.selfcare.module.saving.model.SavingDetailModel;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SavingHistoryDetailFragment extends Fragment {

    private static final String TAG = SavingHistoryDetailFragment.class.getSimpleName();
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingView loadingView;
    @BindView(R.id.layout_no_data)
    View noDataView;
    private Unbinder unbinder;
    private SavingActivity mActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private SavingHistoryDetailAdapter adapter;
    private ArrayList<SavingDetailModel.SavingDetails> data = new ArrayList<>();
    private int currentPage = 0;
    private LinearLayoutManager mLayoutManager;
    private String startDate = "";
    private String endDate = "";
    private boolean canLoadMore;
    private boolean isLoading;

    public static SavingHistoryDetailFragment newInstance() {
        SavingHistoryDetailFragment fragment = new SavingHistoryDetailFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (SavingActivity) context;
        mApplication = (ApplicationController) mActivity.getApplicationContext();
        mRes = mApplication.getResources();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_saving_history_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (data == null) data = new ArrayList<>();
        if (recyclerView.getItemDecorationCount() <= 0) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            mLayoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
        }
        adapter = new SavingHistoryDetailAdapter(mActivity, data);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mLayoutManager != null) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                    if (totalItemCount == lastVisibleItem + 1) {
                        onLoadMore();
                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity.setTitle(mRes.getString(R.string.saving_history));
        Bundle bundle = getArguments();
        if (bundle != null) {
            String title = bundle.getString(Constants.KEY_TITLE);
            startDate = bundle.getString(Constants.KEY_START_DATE);
            endDate = bundle.getString(Constants.KEY_END_DATE);
            if (TextUtils.isEmpty(title)) {
                mActivity.setTitle(mRes.getString(R.string.saving_history));
            } else {
                mActivity.setTitle(title);
            }
            currentPage = 0;
            loadData(true);
        }
    }

    private void onLoadMore() {
        Log.i(TAG, "onLoadMore nao ...");
        if (canLoadMore) {
            canLoadMore = false;
            currentPage++;
            loadData(false);
        }
    }

    private void loadData(final boolean showLoading) {
        if (isLoading) return;
        if (showLoading && loadingView != null) {
            loadingView.loadBeginMocha();
            if (noDataView != null) noDataView.setVisibility(View.GONE);
        }
        Log.i(TAG, "loadData showLoading: " + showLoading + "\nstartDate: " + startDate + "\nendDate: " + endDate + "\ncurrentPage:" + currentPage);
        canLoadMore = false;
        isLoading = true;
        AVNOHelper.getInstance(mApplication).getSavingDetail(startDate, endDate, currentPage
                , new AVNOHelper.GetSavingDetailListener() {
                    @Override
                    public void onGetSavingDetailSuccess(SavingDetailModel result) {
                        isLoading = false;
                        if (currentPage == 0) {
                            clearData();
                        } else if (data == null) data = new ArrayList<>();
                        if (result != null) {
                            if (currentPage == 0 && adapter != null) {
                                adapter.setDataStatistics(SavingDetailModel.convert2Statistics(result));
                            }
                            int size = result.getListDetails().size() - 1;
                            for (int i = size; i > 0; i--) {
                                if (result.getListDetails().get(i) == null) {
                                    result.getListDetails().remove(i);
                                } else {
                                    result.getListDetails().get(i).setFirst(false);
                                }
                            }
                            canLoadMore = !result.getListDetails().isEmpty();
                            data.addAll(result.getListDetails());
                        }
                        if (data != null && !data.isEmpty() && data.get(0) != null) {
                            data.get(0).setFirst(true);
                        }
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                        loadingFinish();
                    }

                    @Override
                    public void onGetSavingDetailError(int code, String msg) {
                        isLoading = false;
                        loadError(msg);
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void loadingFinish() {
        if (getData().isEmpty()) {
            if (loadingView != null) loadingView.loadFinish();
            if (recyclerView != null) recyclerView.setVisibility(View.GONE);
            if (noDataView != null) noDataView.setVisibility(View.VISIBLE);
        } else {
            if (loadingView != null) loadingView.loadFinish();
            if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
            if (noDataView != null) noDataView.setVisibility(View.GONE);
        }
    }

    private void loadError(String error) {
        if (getData().isEmpty()) {
            if (loadingView != null) {
                loadingView.loadError(error);
                loadingView.setLoadingErrorListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadData(true);
                    }
                });
            }
            if (recyclerView != null) recyclerView.setVisibility(View.GONE);
            if (noDataView != null) noDataView.setVisibility(View.GONE);
        } else {
            loadingFinish();
        }
    }

    private List<SavingDetailModel.SavingDetails> getData() {
        if (data == null) data = new ArrayList<>();
        return data;
    }

    private void clearData() {
        if (data == null) data = new ArrayList<>();
        else data.clear();
    }

}

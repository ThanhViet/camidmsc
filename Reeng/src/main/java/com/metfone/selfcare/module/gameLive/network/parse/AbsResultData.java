package com.metfone.selfcare.module.gameLive.network.parse;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;

import java.io.Serializable;

public class AbsResultData implements Serializable {
    private static final long serialVersionUID = -5702141301177401541L;

    @SerializedName("code")
    private int errorCode;

    @SerializedName("desc")
    private String message;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void showErrorMessage(Context context) {
        if (context == null)
            return;
        ToastUtils.makeText(context, R.string.error_message_default);
    }
}
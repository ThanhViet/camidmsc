package com.metfone.selfcare.module.tiin;

import android.annotation.SuppressLint;
import android.content.Context;

import com.metfone.selfcare.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtilitis {
    /**
     * Giá trị của 1 giây = 1000ms
     */
    public static long SECOND = 1000;
    /**
     * Giá trị của 1 phút = 1000ms*60s
     */
    public static long MINUTE = 60 * SECOND;
    /**
     * Giá trị của 1 giờ = 1000ms*60s*60m
     */
    public static long HOUR = 60 * MINUTE;
    /**
     * Giá trị của 1 ngày = 1000ms*60s*60m*24h
     */
    public static long DAY = 24 * HOUR;
    /**
     * Giá trị của 1 tuần = 1000ms*60s*60m*24h*7d
     */
    public static long WEEK = 7 * DAY;
    @SuppressLint("SimpleDateFormat")
    public static String calculateDate(Context context, long timeStr) {
        /*
        todo neu thoi gian nho hon 1 ngay thi hiện giờ trước, nếu thời gian lớn hơn 1 ngày thì hiện dd/MM/yyyy
         */
        String date = "";
        long currentTime = System.currentTimeMillis();
        long timeLong = currentTime - timeStr*1000;
        if (timeLong < DAY) {
            if (timeLong > 0 && timeLong > HOUR) {
                int num = (int) (timeLong / HOUR);
                date =  context.getString(R.string.last_hours, num);
                long timeMinute = timeLong - num*HOUR;
                if(timeMinute > 0 && timeMinute > MINUTE && num < 2){
                    String dateMinute = "";
                    date =  context.getString(R.string.last_h, num);
                    int minute = (int) (timeMinute/MINUTE);
                    if(minute <10){
                        dateMinute=context.getString(R.string.last_minute, minute);
                        return date+"0"+dateMinute;
                    }
                    dateMinute=context.getString(R.string.last_minute, minute);
                    return date+dateMinute;
                }
                return date;
            }else if(timeLong > 0 && timeLong > MINUTE){
                int num = (int) (timeLong / MINUTE);
                if (num == 1) {
                    return context.getString(R.string.last_minute, num);
                }
                return context.getString(R.string.last_minutes, num);
            }else if(timeLong < 0){
                return context.getString(R.string.last_minute, 1);
            }
            return context.getString(R.string.last_minute, 1);
        } else {
            try {
                timeStr = timeStr * 1000;
            } catch (Exception e) {
                timeStr = System.currentTimeMillis();
            }
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            date = sdfDate.format(new Date(timeStr));
        }
        return date;
    }
}

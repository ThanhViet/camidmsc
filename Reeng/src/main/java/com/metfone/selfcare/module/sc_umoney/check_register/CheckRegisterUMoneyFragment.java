package com.metfone.selfcare.module.sc_umoney.check_register;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.sc_umoney.main_umoney.TabUmoneyActivity;
import com.metfone.selfcare.module.sc_umoney.network.UMoneyApi;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

public class CheckRegisterUMoneyFragment extends Fragment {
    TextView tvRegister, tvTitle;
    TabUmoneyActivity activity;

    private int keyCode = 0;
    private String desc = "";
    UMoneyApi uMoneyApi;

    public static CheckRegisterUMoneyFragment newInstance(Bundle bundle) {
        CheckRegisterUMoneyFragment fragment = new CheckRegisterUMoneyFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sc_umoney_check_register, container, false);
        activity = (TabUmoneyActivity) getActivity();
        initView(view);
        loadData();
        initEvent();
        return view;
    }

    private void loadData() {
        uMoneyApi = new UMoneyApi(ApplicationController.self());
        if (getArguments() != null) {
            keyCode = getArguments().getInt(SCConstants.UMONEY.KEY_CODE_INFO, 0);
            desc = getArguments().getString(SCConstants.UMONEY.KEY_DESC_INFO,"");
            if (keyCode == 10116) {
                tvRegister.setText(activity.getString(R.string.register));
                tvTitle.setText(desc);
            } else {
                tvRegister.setText(activity.getString(R.string.ok));
                tvTitle.setText(desc);
            }
        }
    }

    private void initView(View view) {
        tvRegister = view.findViewById(R.id.tv_register);
        tvTitle = view.findViewById(R.id.tv_title);
    }

    private void initEvent() {
        tvRegister.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (keyCode == 10116) {
                    activity.showFragment(SCConstants.UMONEY.TAB_REGISTER, null, false);
                }else {
                    //todo click ok quay ve home
                    activity.finish();
//                    activity.showFragment(SCConstants.UMONEY.TAB_MOBILE, null, false);
                }
            }
        });
    }
}

package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.holder.MediaNewHolder;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

public class VideoAdapter extends SectionListDataAdapter<AllModel> {

    public VideoAdapter(Context context, List<AllModel> itemsList, AbsInterface.OnItemListener onClick) {
        super(context, itemsList, onClick);
    }

    @Override
    public int getItemCount() {
        return getItemsList().size() > 4 ? 4 : getItemsList().size();
    }

    @Override
    public MediaNewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_media_video_home, null);
        return new MediaNewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        final AllModel item = getItem(position);
        MediaNewHolder itemHolder = (MediaNewHolder) holder;
        if (item != null) {
            itemHolder.tvName.setText(item.getName());
            itemHolder.tvSinger.setText(item.getSinger());
            //TODO Image video của trang chủ chỉ hiển thị ảnh image (390x220)
            ImageBusiness.setVideo(item.getImage(), itemHolder.image, position);
            itemHolder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClick != null) {
                        item.setSource(MediaLogModel.SRC_HOT_HOME);
                        onClick.onItemClick(item);
                    }
                }
            });
        }
    }

}
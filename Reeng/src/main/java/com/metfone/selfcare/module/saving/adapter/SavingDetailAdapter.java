/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/20
 */

package com.metfone.selfcare.module.saving.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.module.keeng.base.BaseViewHolder;
import com.metfone.selfcare.module.saving.model.SavingDetailModel;

import java.util.List;

public class SavingDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = SavingDetailAdapter.class.getSimpleName();
    private final int TYPE_EMPTY = 0;
    private final int TYPE_NORMAL = 1;
    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private List<SavingDetailModel.Details> data;

    public SavingDetailAdapter(BaseSlidingFragmentActivity activity, List<SavingDetailModel.Details> data) {
        this.activity = activity;
        this.data = data;
        mApplication = (ApplicationController) activity.getApplication();
    }

    @Override
    public int getItemViewType(int position) {
        SavingDetailModel.Details item = getItem(position);
        if (item != null) {
            return TYPE_NORMAL;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_NORMAL) {
            view = LayoutInflater.from(activity).inflate(R.layout.holder_saving_detail_of_item, parent, false);
        } else
            view = LayoutInflater.from(activity).inflate(R.layout.holder_empty, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemHolder, int position) {
        final SavingDetailModel.Details item = getItem(position);
        int viewType = getItemViewType(position);
        if (item != null && itemHolder instanceof BaseViewHolder) {
            BaseViewHolder holder = (BaseViewHolder) itemHolder;
            if (viewType == TYPE_NORMAL) {
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(item.getToUser());
                if (phoneNumber != null && !TextUtils.isEmpty(phoneNumber.getName()))
                    holder.setText(R.id.tv_user, phoneNumber.getName());
                else
                    holder.setText(R.id.tv_user, item.getToUser());
                holder.setText(R.id.tv_price, item.getPrice());
                holder.setText(R.id.tv_time_detail, item.getTimeDetail());
                holder.setText(R.id.tv_duration, item.getDuration());
                ImageView ivIcon = holder.getView(R.id.icon);
                if (ivIcon != null) {
                    Glide.with(activity)
                            .load(item.getImageUrl())
                            .apply(new RequestOptions().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher))
                            .into(ivIcon);
                }

            }
        }
    }

    public SavingDetailModel.Details getItem(int position) {
        if (data != null && data.size() > position && position >= 0)
            return data.get(position);
        return null;
    }

    @Override
    public int getItemCount() {
        if (data == null || data.isEmpty())
            return 0;
        return data.size();
    }

}

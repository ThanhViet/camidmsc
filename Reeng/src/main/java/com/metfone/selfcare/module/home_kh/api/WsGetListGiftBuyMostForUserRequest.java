package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Data;

public class WsGetListGiftBuyMostForUserRequest extends BaseRequest<WsGetListGiftBuyMostForUserRequest.Request> {
    public class Request {
        @SerializedName("language")
        public String language;
        @SerializedName("isdn")
        public String isdn;
        @SerializedName("custId")
        public String custId;
    }
}

/*
package com.metfone.selfcare.module.selfcare.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.selfcare.loginhelper.ConnectionBuilderForTesting;

import net.openid.appauth.AppAuthConfiguration;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.ResponseTypeValues;

import static com.metfone.selfcare.module.selfcare.loginhelper.KeycloakConfig.AUTH_ENDPOINT;
import static com.metfone.selfcare.module.selfcare.loginhelper.KeycloakConfig.CLIENT_ID;
import static com.metfone.selfcare.module.selfcare.loginhelper.KeycloakConfig.REDIRECT_URI;
import static com.metfone.selfcare.module.selfcare.loginhelper.KeycloakConfig.TOKEN_ENDPOINT;

*/
/**
 * Created by thanhnt72 on 4/25/2019.
 *//*


public class LoginMyIDActivity extends BaseSlidingFragmentActivity {

    public static void startActivity(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, LoginMyIDActivity.class);
        activity.startActivity(intent);
    }


    ImageView imgvMytel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_myid);
        imgvMytel = findViewById(R.id.imgv_mytel);
        imgvMytel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpAuthorization();
            }
        });
    }

    private void setUpAuthorization() {

        Uri redirectUri = Uri.parse(REDIRECT_URI);
        AuthorizationService authorizationService = null;
        AuthorizationServiceConfiguration serviceConfiguration;

        AppAuthConfiguration.Builder appAuthConfiguration = new AppAuthConfiguration.Builder();
//        appAuthConfiguration.setBrowserMatcher(new BrowserWhitelist(
//                VersionedBrowserMatcher.CHROME_CUSTOM_TAB,
//                VersionedBrowserMatcher.SAMSUNG_CUSTOM_TAB));
        appAuthConfiguration.setConnectionBuilder(ConnectionBuilderForTesting.INSTANCE);

        if (authorizationService != null) {
            authorizationService.dispose();
            authorizationService = null;
        }

        authorizationService = new AuthorizationService(this, appAuthConfiguration.build());
        serviceConfiguration = new AuthorizationServiceConfiguration(
                Uri.parse(AUTH_ENDPOINT) */
/* auth endpoint *//*
,
                Uri.parse(TOKEN_ENDPOINT) */
/* token endpoint *//*

        );
        AuthorizationRequest.Builder builder = new AuthorizationRequest.Builder(
                serviceConfiguration,
                CLIENT_ID,
                ResponseTypeValues.CODE,
                redirectUri
        );
        builder.setScopes("openid token profile");
        builder.setPrompt("login");
        AuthorizationRequest request = builder.build();
        Intent completionIntent = new Intent(this, AfterLoginMyIDActivity.class);
        authorizationService.performAuthorizationRequest(
                request,
                PendingIntent.getActivity(this, 0, completionIntent, 0));
        authorizationService.dispose();
    }
}
*/

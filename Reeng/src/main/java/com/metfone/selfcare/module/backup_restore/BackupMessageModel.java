package com.metfone.selfcare.module.backup_restore;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 *  created by Huongnd38 on 13/11/2018
 */

public class BackupMessageModel {
    @SerializedName("pId")
    @Expose
    String pId; //"pId":"0102001_6D64C5796A5944EE90E19DE5D00D6AE6",	//packet id của message

    @SerializedName("from")
    @Expose
    String from; //"from":"0984272398",									//Số người gửi, dựa luôn vào đây để check tin nhắn gửi đi hay nhận được

    @SerializedName("to")
    @Expose
    String to; //"to":"0987654321",									//Tin nhắn tới sdt nào, chỉ dùng trong chat 1-1

    @SerializedName("svId")
    @Expose
    String svId; //        "svId":"+855973796789_1535515266626",				//server id gửi tin

    @SerializedName("cMode")
    @Expose
    int cMode; //        "cMode":"1",											//chat mode 1 là chat thường, 2 là SMS out

    @SerializedName("body")
    @Expose
    String body; //        "body":"Hello",										//nội dung tin nhắn

    @SerializedName("reply")
    @Expose
    String reply;
    /*
    * "reply":{											// reply tin nhắn (như trong bản tin)
      "subtype":"image",
      "member":"0986860910",
      "body":"",
      "link":"\/hlmocha92\/media1\/file\/201810\/0986\/8609\/10\/5f18bee0bfebf7c6f5eb14a73a5f9daa.jpg",
      "msgId":"0202002_B110DCB849BC4E1BA2482BA4A776D74A"
   },*/

    @SerializedName("sName")
    @Expose
    String sName; //"sName":"thanh pro",									// sender name, dùng trong groupchat/chat/roomchat/offical

    @SerializedName("sAvatar")
    @Expose
    String sAvatar; //        "sAvatar":1540205411409,								// sender avatar, dùng trong groupchat/chat/roomchat/offical

    @SerializedName("t")
    @Expose
    long t; //        "t":1540523979779,									// thời gian gửi tin/nhận

    @SerializedName("tag")
    @Expose
    String tag;
    /*"tag":{												// dữ liệu tag (như trong bản tin)
      "msisdn":"0326695346",
      "name":"0326695346",
      "tag_name":"@0326695346"
   },*/

    @SerializedName("status")
    @Expose
    int status;//        "status":1,											//trạng thái tin nhắn: 1-đã gửi | 2-gửi lỗi, nhận lỗi | 3-tin gửi đi đã nhận | 4-tin đến đã nhận | 5-chưa download | 6-đang down , đang up, đang gui |
                                                                                //7-chưa gửi - dành cho tin mới tạo | 8-đã xem

    @SerializedName("readState")
    @Expose
    int readState; //"readState":1,										//0-tin nhắn nhận chưa đọc | 1-tin nhắn nhận đã đọc | 2-tin nhắn nhận đã gửi seen

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSvId() {
        return svId;
    }

    public void setSvId(String svId) {
        this.svId = svId;
    }

    public int getcMode() {
        return cMode;
    }

    public void setcMode(int cMode) {
        this.cMode = cMode;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsAvatar() {
        return sAvatar;
    }

    public void setsAvatar(String sAvatar) {
        this.sAvatar = sAvatar;
    }

    public long getT() {
        return t;
    }

    public void setT(long t) {
        this.t = t;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getReadState() {
        return readState;
    }

    public void setReadState(int readState) {
        this.readState = readState;
    }
}

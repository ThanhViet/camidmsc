package com.metfone.selfcare.module.keeng.base;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    public static final int ITEM_EMPTY = 0;
    //    public static final int ITEM_TITLE_EXPAND = 1;
    public static final int ITEM_FEED_ALBUM = 3;
    public static final int ITEM_FEED_VIDEO = 4;
    public static final int ITEM_FEED_SONG = 5;
    public static final int ITEM_FEED_PLAYLIST = 6;
    public static final int ITEM_FEED_RANK_SONG = 7;
    public static final int ITEM_FEED_RANK_VIDEO = 8;
    public static final int ITEM_FEED_CHANGE_AVATAR = 9;
    public static final int ITEM_HEADER_TOPIC = 11;
    public static final int ITEM_SINGER = 21;
    //    public static final int ITEM_ADD_PLAYLIST = 22;
    public static final int ITEM_LOAD_MORE = 24;
    public static final int ITEM_REPORT = 25;
    //    public static final int ITEM_FRIEND_FOLLOW = 26;
//    public static final int ITEM_FRIEND_CONTACT = 27;
    public static final int ITEM_BANCUNGGU = 28;
    public static final int ITEM_MEDIA_SONG = 30;
    public static final int ITEM_MEDIA_ALBUM = 31;
    public static final int ITEM_MEDIA_VIDEO = 32;
    public static final int ITEM_MEDIA_SONG_SOCIAL = 34;
    public static final int ITEM_MEDIA_ALBUM_SOCIAL = 35;
    public static final int ITEM_MEDIA_VIDEO_SOCIAL = 36;
    public static final int ITEM_MEDIA_VIDEO_LARGE = 37;
    public static final int ITEM_MEDIA_SONG_SINGER = 38;
    //    public static final int ITEM_SESION = 39;
    public static final int ITEM_TOPIC = 40;
    public static final int ITEM_MEDIA_VIDEO_SOCIAL_LARGE = 41;
    public static final int ITEM_ACTIVE_MEMBER_TOP = 42;
    public static final int ITEM_PLAYLIST = 43;
    public static final int ITEM_DEEPLINK_IMAGE = 44;
    public static final int ITEM_DEEPLINK_BUTTON = 45;
    public static final int ITEM_ACTIVE_MEMBER = 46;
    public static final int ITEM_RECOMMEND_USER = 55;
    public static final int ITEM_CHILD_RECOMMEND_USER_MUSIC = 57;
    public static final int ITEM_MEDIA_ALBUM_HOME_HOT = 59;
    public static final int ITEM_MEDIA_VIDEO_HOME_HOT = 60;
    public static final int ITEM_MEDIA_SONG_HOME_HOT = 61;
    public static final int ITEM_HEADER_PERSONAL = 62;
    public static final int ITEM_PERSONAL_FRIEND = 63;
    public static final int ITEM_PERSONAL_IMAGE = 64;
    public static final int ITEM_PERSONAL_PLAYLIST = 65;
    public static final int ITEM_PERSONAL_FRIEND_DETAIL = 67;
    public static final int ITEM_PERSONAL_LISTENED = 68;
    public static final int ITEM_PERSONAL_PLAYLIST_DETAIL = 69;
    public static final int ITEM_MEDIA_SONG_LOSSLESS = 71;
    public static final int ITEM_MEDIA_SONG_PLAYER = 72;
    public static final int ITEM_MEDIA_SONG_PLAYER_LOSSLESS = 73;
    public static final int ITEM_HEADER_FRIEND_PROFILE = 74;
    public static final int ITEM_BOTTOM_SHEET = 77;
    //    public static final int ITEM_SEARCH_SUGGEST = 78;
    public static final int ITEM_SUGGEST_SINGER = 79;
    //    public static final int ITEM_SUGGEST_HISTORY = 80;
//    public static final int ITEM_SUGGEST_TAG = 81;
//    public static final int ITEM_CHILD_SUGGEST_SINGER = 82;
    public static final int ITEM_CHILD_SUGGEST_HISTORY = 83;
    public static final int ITEM_INBOX = 84;
    public static final int ITEM_MEDIA_SONG_SEARCH_LOSSLESS = 86;
    public static final int ITEM_LOADING_DATA = 87;
    public static final int ITEM_PERSONAL_NO_IMAGE = 88;
    public static final int ITEM_PERSONAL_NO_PLAYLIST = 89;
    public static final int ITEM_COMMENT_LEVER_1 = 92;
    public static final int ITEM_COMMENT_LEVER_2 = 93;
    public static final int ITEM_MEDIA_PLAYLIST_SOCIAL = 94;
    //    public static final int ITEM_HEADER_TITLE = 95;
//    public static final int ITEM_COMMENT_TOP = 96;
    public static final int ITEM_MEDIA_MOVIE_SOCIAL = 97;
    public static final int ITEM_ACCUMULATED_POINT_INFO = 98;
    public static final int ITEM_ACCUMULATED_POINT = 99;
    public static final int ITEM_FEED_MOVIE = 100;
    public static final int ITEM_SUGGEST_SONG = 102;
    public static final int ITEM_SUGGEST_ALBUM = 103;
    public static final int ITEM_SUGGEST_VIDEO = 104;
    public static final int ITEM_RANK_HOME_TOP = 105;
    public static final int ITEM_RANK_HOME = 106;
    public static final int ITEM_MEDIA_SONG_RANK = 107;
    public static final int ITEM_SUGGEST_PLAYLIST = 108;
    public static final int ITEM_PLAYLIST_SUBMIT = 109;
    public static final int ITEM_MEDIA_SONG_HOT = 110;
    public static final int ITEM_MEDIA_VIDEO_RANK = 111;
    public static final int ITEM_SINGER_FAVORITE = 112;
    public static final int ITEM_TYPE_FAVORITE = 113;

    public static final int ITEM_MEDIA_HOT_SONG_EDM = 114;
    public static final int ITEM_MEDIA_HOT_VIDEO_EDM = 115;
    public static final int ITEM_MEDIA_HOT_ALBUM_EDM = 116;
    public static final int ITEM_MEDIA_RADIO_STATION_EDM = 117;
    public static final int ITEM_MEDIA_PLAYLIST_EDM = 118;
    public static final int ITEM_SINGER_EDM = 119;
    public static final int ITEM_MEDIA_SONG_CHART_EDM = 120;
    public static final int ITEM_MEDIA_VIDEO_CHART_EDM = 121;
    public static final int ITEM_MEDIA_HOT_YOUTUBE = 122;
    public static final int ITEM_SUGGEST_MOVIES = 123;
    public static final int ITEM_MOVIES = 124;

    protected final String TAG = getClass().getSimpleName();
    protected Context mContext;
    private String ga_source;

    public BaseAdapter(Context context, String ga_source) {
        this.mContext = context;
        this.ga_source = ga_source;
//        Log.i(TAG, "BaseAdapter -> " + this.ga_source);
    }

    public BaseAdapter(Context context) {
        this.mContext = context;
        this.ga_source = "";
    }
}

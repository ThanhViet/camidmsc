package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.HistoryChargeType;

/**
 * ViewHolder for History charge Exchange, Basic, Promotion: Calling, SMS, VAS, Data charge
 * layout: item_history_charge_2
 */
public class OtherChargeViewHolder extends RecyclerView.ViewHolder {
    public AppCompatImageView mIcon;
    public AppCompatTextView mTitle;
    public AppCompatTextView mValue;
    public HistoryChargeType mHistoryChargeType;

    public OtherChargeViewHolder(View view) {
        super(view);
        mIcon = view.findViewById(R.id.icon_history_charge_2);
        mTitle = view.findViewById(R.id.title_history_charge_2);
        mValue = view.findViewById(R.id.value_history_charge_2);
    }
}

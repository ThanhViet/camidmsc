/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/30
 *
 */

package com.metfone.selfcare.module.myviettel.fragment;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.myviettel.activity.DataChallengeActivity;
import com.metfone.selfcare.module.myviettel.adapter.SearchContactAdapter;
import com.metfone.selfcare.module.myviettel.adapter.SuggestDataAdapter;
import com.metfone.selfcare.module.myviettel.adapter.SuggestUserAdapter;
import com.metfone.selfcare.module.myviettel.listener.GetAllContactsListener;
import com.metfone.selfcare.module.myviettel.listener.OnDataChallengeListener;
import com.metfone.selfcare.module.myviettel.listener.SearchContactsListener;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;
import com.metfone.selfcare.module.myviettel.model.DataChallengeProvisional;
import com.metfone.selfcare.module.myviettel.network.ApiCallback;
import com.metfone.selfcare.module.myviettel.network.MyViettelApi;
import com.metfone.selfcare.module.myviettel.task.GetAllContactsTask;
import com.metfone.selfcare.module.myviettel.task.SearchContactsTask;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class InviteDataChallengeFragment extends BaseFragment implements OnDataChallengeListener {
    private TextView tvTitleInvite;
    private EditText editSearch;
    private ImageView btnClear;
    private ProgressBar loadingView;
    private TextView tvMessage;
    private RecyclerView recyclerView;
    private View viewSuggestUser;
    private RecyclerView recyclerSuggestUser;
    private View viewSuggestData;
    private RecyclerView recyclerSuggestData;

    private ApplicationController application;

    private ArrayList<PhoneNumber> listAllContacts;
    private ArrayList<Object> listContacts;
    private SearchContactAdapter adapterSearch;
    private ArrayList<PhoneNumber> listSuggestUsers;
    private SuggestUserAdapter adapterSuggestUser;
    private ArrayList<DataChallenge> listSuggestData;
    private SuggestDataAdapter adapterSuggestData;

    private boolean canSearch = true;
    private boolean canShowSuggest = true;
    private boolean canClear = false;
    private String keySearch = "";

    private GetAllContactsTask getAllContactsTask;
    private SearchContactsTask searchContactsTask;

    private Object currentContact;

    public static InviteDataChallengeFragment newInstance() {
        Bundle args = new Bundle();
        InviteDataChallengeFragment fragment = new InviteDataChallengeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "InviteDataChallengeFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_inviting_data_challenge;
    }

    private MyViettelApi getMyViettelApi() {
        if (mActivity instanceof DataChallengeActivity) {
            return ((DataChallengeActivity) mActivity).getMyViettelApi();
        }
        return new MyViettelApi();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            tvTitleInvite = view.findViewById(R.id.tv_title_invite);
            editSearch = view.findViewById(R.id.edit_search);
            btnClear = view.findViewById(R.id.button_clear);
            loadingView = view.findViewById(R.id.loading_view);
            tvMessage = view.findViewById(R.id.tv_message);
            recyclerView = view.findViewById(R.id.recycler_view);
            viewSuggestUser = view.findViewById(R.id.layout_suggest_user);
            recyclerSuggestUser = view.findViewById(R.id.recycler_suggest_user);
            viewSuggestData = view.findViewById(R.id.layout_suggest_data);
            recyclerSuggestData = view.findViewById(R.id.recycler_suggest_data);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        application = ApplicationController.self();
        InputMethodUtils.hideKeyboardWhenTouch(tvTitleInvite, mActivity);
        InputMethodUtils.hideKeyboardWhenTouch(recyclerView, mActivity);
        InputMethodUtils.hideKeyboardWhenTouch(viewSuggestData, mActivity);
        InputMethodUtils.hideKeyboardWhenTouch(viewSuggestUser, mActivity);

        if (listContacts == null) listContacts = new ArrayList<>();
        adapterSearch = new SearchContactAdapter(mActivity);
        adapterSearch.setListener(this);
        adapterSearch.setItems(listContacts);
        BaseAdapter.setupVerticalRecycler(mActivity, recyclerView, null, adapterSearch, false);

        if (listSuggestData == null) listSuggestData = new ArrayList<>();
        adapterSuggestData = new SuggestDataAdapter(mActivity);
        adapterSuggestData.setListener(this);
        adapterSuggestData.setItems(listSuggestData);
        BaseAdapter.setupVerticalRecycler(mActivity, recyclerSuggestData, null, adapterSuggestData, false);

        if (listSuggestUsers == null) listSuggestUsers = new ArrayList<>();
        adapterSuggestUser = new SuggestUserAdapter(mActivity);
        adapterSuggestUser.setListener(this);
        adapterSuggestUser.setItems(listSuggestUsers);
        BaseAdapter.setupVerticalRecycler(mActivity, recyclerSuggestUser, null, adapterSuggestUser, false);

        if (btnClear != null) btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (canClear) {
                    canSearch = true;
                    if (editSearch != null) {
                        editSearch.setCursorVisible(true);
                        editSearch.setFocusableInTouchMode(true);
                        editSearch.setInputType(InputType.TYPE_CLASS_TEXT);
                        editSearch.setText("");
                    }
                } else {
                    if (editSearch != null) {
                        if (editSearch.getInputType() == InputType.TYPE_CLASS_PHONE)
                            editSearch.setInputType(InputType.TYPE_CLASS_TEXT);
                        else
                            editSearch.setInputType(InputType.TYPE_CLASS_PHONE);
                        InputMethodUtils.showSoftKeyboard(mActivity, editSearch);
                    }
                }
            }
        });
        if (editSearch != null) {
            editSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);
            editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (mActivity != null) mActivity.hideKeyboard();
                    }
                    return false;
                }
            });
            editSearch.addTextChangedListener(new TextWatcher() {
                private String oldKeySearch;

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (canSearch) {
                        if (TextUtils.isEmpty(charSequence)) {
                            keySearch = "";
                        } else {
                            keySearch = charSequence.toString().trim();
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    Log.i(TAG, "afterTextChanged keySearch= '" + keySearch + "', oldKeySearch= '" + oldKeySearch + "'");
                    if (canSearch) {
                        if (tvTitleInvite != null)
                            tvTitleInvite.setText(R.string.find_friend_to_invite);
                        if (TextUtils.isEmpty(keySearch)) {
                            currentContact = null;
                            canClear = false;
                            if (btnClear != null)
                                btnClear.setImageDrawable(getResources().getDrawable(R.drawable.ic_dialpad));
                            showSuggestUser();
                        } else {
                            canClear = true;
                            if (btnClear != null)
                                btnClear.setImageDrawable(getResources().getDrawable(R.drawable.ic_clear_search));
                            if (!keySearch.equals(oldKeySearch)) {
                                searchContact(keySearch);
                            }
                        }
                    }
                    oldKeySearch = keySearch;
                }
            });
        }
        loadAllContacts();
    }

    private void cancelGetAllContacts() {
        if (getAllContactsTask != null) {
            getAllContactsTask.setListener(null);
            getAllContactsTask.cancel(true);
            getAllContactsTask = null;
        }
    }

    private void loadAllContacts() {
        cancelGetAllContacts();
        getAllContactsTask = new GetAllContactsTask(ApplicationController.self());
        getAllContactsTask.setListener(new GetAllContactsListener() {
            @Override
            public void onPrepareGetAllContacts() {
                if (loadingView != null) loadingView.setVisibility(View.VISIBLE);
                if (editSearch != null) editSearch.setEnabled(false);
            }

            @Override
            public void onFinishedGetAllContacts(ArrayList<PhoneNumber> list, String phoneNumbers) {
                if (loadingView != null) loadingView.setVisibility(View.GONE);
                if (editSearch != null) editSearch.setEnabled(true);
                if (mActivity != null && isAdded()) {
                    if (listAllContacts == null) listAllContacts = new ArrayList<>();
                    if (Utilities.notEmpty(list))
                        listAllContacts.addAll(list);
                    loadSuggestUser(phoneNumbers);
                }
            }
        });
        getAllContactsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadSuggestUser(String phoneNumbers) {
        getMyViettelApi().getSuggestUserDC(phoneNumbers, new ApiCallback<ArrayList<String>>() {
            @Override
            public void onSuccess(String msg, ArrayList<String> result) throws Exception {
                if (Utilities.notEmpty(result)) {
                    ContactBusiness contactBusiness = ApplicationController.self().getContactBusiness();
                    if (listSuggestUsers == null) listSuggestUsers = new ArrayList<>();
                    else listSuggestUsers.clear();
                    for (String number : result) {
                        if (Utilities.notEmpty(number)) {
                            number = PhoneNumberHelper.getInstant().getPhoneNumberFromText(application, number);
                            PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(number);
                            if (phoneNumber != null) {
                                listSuggestUsers.add(phoneNumber);
                            }
                        }
                    }
                    if (adapterSuggestUser != null) adapterSuggestUser.notifyDataSetChanged();
                    if (canShowSuggest && !listSuggestUsers.isEmpty()) {
                        if (tvMessage != null) tvMessage.setVisibility(View.GONE);
                        if (viewSuggestUser != null) viewSuggestUser.setVisibility(View.VISIBLE);
                        if (viewSuggestData != null) viewSuggestData.setVisibility(View.GONE);
                        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void loadSuggestData(String phoneNumber) {
        canShowSuggest = false;
        if (loadingView != null) loadingView.setVisibility(View.VISIBLE);
        if (tvMessage != null) tvMessage.setVisibility(View.GONE);
        //if (viewSuggestUser != null) viewSuggestUser.setVisibility(View.GONE);
        if (viewSuggestData != null) viewSuggestData.setVisibility(View.GONE);
        //if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        getMyViettelApi().getSuggestDataDC(phoneNumber, new ApiCallback<DataChallengeProvisional>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(String msg, DataChallengeProvisional result) throws Exception {
                if (result.isSuccess()) {
                    canClear = true;
                    if (tvTitleInvite != null)
                        tvTitleInvite.setText(R.string.send_invite_to_friend);
                    if (btnClear != null)
                        btnClear.setImageDrawable(getResources().getDrawable(R.drawable.ic_clear_search));
                    if (editSearch != null) {
                        editSearch.setCursorVisible(false);
                        editSearch.setFocusableInTouchMode(false);
                        if (currentContact instanceof PhoneNumber)
                            editSearch.setText(((PhoneNumber) currentContact).getJidNumber() + "(" + ((PhoneNumber) currentContact).getName() + ")");
                        else if (currentContact instanceof String)
                            editSearch.setText((String) currentContact);
                    }
                    if (Utilities.isEmpty(result.getData())) {
                        if (tvMessage != null) {
                            tvMessage.setVisibility(View.VISIBLE);
                            tvMessage.setText(R.string.no_data);
                        }
                    } else {
                        if (listSuggestData == null) listSuggestData = new ArrayList<>();
                        else listSuggestData.clear();
                        listSuggestData.addAll(result.getData());
                        if (adapterSuggestData != null) adapterSuggestData.notifyDataSetChanged();
                        if (viewSuggestData != null) viewSuggestData.setVisibility(View.VISIBLE);
                        if (viewSuggestUser != null) viewSuggestUser.setVisibility(View.GONE);
                        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                    }
                } else {
                    if (editSearch != null) editSearch.setEnabled(true);
                    if (mActivity != null) mActivity.showToast(msg);
                }
            }

            @Override
            public void onError(String s) {
                if (mActivity != null) mActivity.showToast(R.string.e601_error_but_undefined);
//                if (tvMessage != null) {
//                    tvMessage.setVisibility(View.VISIBLE);
//                    tvMessage.setText(R.string.e601_error_but_undefined);
//                }
            }

            @Override
            public void onComplete() {
                if (loadingView != null) loadingView.setVisibility(View.GONE);
                if (editSearch != null) editSearch.setEnabled(true);
            }
        });
    }

    private void showSuggestUser() {
        canShowSuggest = true;
        if (viewSuggestData != null) viewSuggestData.setVisibility(View.GONE);
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (viewSuggestUser != null)
            viewSuggestUser.setVisibility(Utilities.notEmpty(listSuggestUsers) ? View.VISIBLE : View.GONE);
        if (loadingView != null) loadingView.setVisibility(View.GONE);
        if (tvMessage != null) tvMessage.setVisibility(View.GONE);
    }

    private void cancelSearchContact() {
        if (searchContactsTask != null) {
            searchContactsTask.setListener(null);
            searchContactsTask.cancel(true);
            searchContactsTask = null;
        }
    }

    private void searchContact(String keySearch) {
        canShowSuggest = false;
        cancelSearchContact();
        searchContactsTask = new SearchContactsTask(application);
        searchContactsTask.setData(listAllContacts);
        searchContactsTask.setComparatorName(SearchUtils.getComparatorNameForSearch());
        searchContactsTask.setComparatorKeySearch(SearchUtils.getComparatorKeySearchForSearch());
        searchContactsTask.setListener(new SearchContactsListener() {
            @Override
            public void onPrepareSearchContacts() {
                if (loadingView != null) loadingView.setVisibility(View.VISIBLE);
                if (tvMessage != null) tvMessage.setVisibility(View.GONE);
                if (viewSuggestUser != null) viewSuggestUser.setVisibility(View.GONE);
                if (viewSuggestData != null) viewSuggestData.setVisibility(View.GONE);
                if (recyclerView != null) recyclerView.setVisibility(View.GONE);
            }

            @Override
            public void onFinishedSearchContacts(String keySearch, ArrayList<Object> list) {
                if (listContacts == null) listContacts = new ArrayList<>();
                else listContacts.clear();
                if (Utilities.notEmpty(list)) listContacts.addAll(list);
                if (adapterSearch != null) adapterSearch.notifyDataSetChanged();
                if (loadingView != null) loadingView.setVisibility(View.GONE);
                if (listContacts.isEmpty()) {
                    if (tvMessage != null) {
                        tvMessage.setVisibility(View.VISIBLE);
                        tvMessage.setText(getString(R.string.no_results_found_contact, keySearch));
                    }
                    if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                } else {
                    if (tvMessage != null) tvMessage.setVisibility(View.GONE);
                    if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
                }

            }
        });
        searchContactsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, keySearch);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClickChooseContact(PhoneNumber item) {
        currentContact = item;
        if (currentContact != null) {
            canSearch = false;
            if (editSearch != null) editSearch.setEnabled(false);
            loadSuggestData(item.getJidNumber());
        }
    }

    @Override
    public void onClickChooseNonContact(String item) {
        currentContact = item;
        if (currentContact != null) {
            canSearch = false;
            if (editSearch != null) editSearch.setEnabled(false);
            loadSuggestData(item);
        }
    }

    @Override
    public void onClickInviteDataPackage(final DataChallenge item) {
        if (mActivity == null || mActivity.isFinishing() || !isAdded() || currentContact == null || item == null)
            return;
        DialogConfirm dialog = new DialogConfirm(mActivity, true);
        dialog.setLabel(mActivity.getString(R.string.confirm));
        String name = "";
        final String phoneNumber;
        if (currentContact instanceof PhoneNumber) {
            PhoneNumber contact = (PhoneNumber) currentContact;
            phoneNumber = contact.getJidNumber();
            name = contact.getJidNumber() + "(" + contact.getName() + ")";
        } else if (currentContact instanceof String) {
            phoneNumber = (String) currentContact;
            name = phoneNumber;
        } else {
            phoneNumber = "";
        }
        dialog.setMessage(mActivity.getString(R.string.msg_invite_data_challenge
                , name
                , item.getName(), TextHelper.formatCurrencyVN(item.getPackageFee())));
        dialog.setUseHtml(true);
        dialog.setNegativeLabel(mActivity.getString(R.string.cancel));
        dialog.setPositiveLabel(mActivity.getString(R.string.send_to_invite));
        dialog.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                if (mActivity == null || mActivity.isFinishing() || !isAdded() || currentContact == null || item == null)
                    return;
                mActivity.showLoadingDialog("", R.string.processing);
                getMyViettelApi().inviteUserDC(phoneNumber, item.getName(), new ApiCallback() {
                    @Override
                    public void onSuccess(String msg, Object result) throws Exception {
                        if (mActivity != null) {
                            mActivity.hideLoadingDialog();
                            mActivity.showToast(msg);
                        }
                    }

                    @Override
                    public void onError(String s) {
                        if (mActivity != null) {
                            mActivity.hideLoadingDialog();
                            mActivity.showToast(s);
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
            }
        });
        dialog.show();
    }
}

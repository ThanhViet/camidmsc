/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.video.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.content.ChannelDetailHolder;
import com.metfone.selfcare.holder.content.VideoDetailHolder;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.module.video.listener.TabVideoListener;
import com.metfone.selfcare.module.video.model.VideoPagerModel;
import com.metfone.selfcare.util.Log;

public class BoxVideoDetailAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {

    public static final int TYPE_LARGE_VIDEO = 1;
    public static final int TYPE_GRID_VIDEO = 2;
    public static final int TYPE_GRID_CHANNEL = 3;

    protected TabVideoListener.OnAdapterClick listener;
    private int parentViewType = TYPE_EMPTY;
    private int widthVideo;
    private int widthChannel;

    public BoxVideoDetailAdapter(Activity act) {
        super(act);
        widthVideo = TabHomeUtils.getWidthVideo();
        widthChannel = TabHomeUtils.getWidthChannelVideo();
    }

    public void setParentViewType(int parentViewType) {
        this.parentViewType = parentViewType;
    }

    public void setListener(TabVideoListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item != null) {
            if (parentViewType == VideoPagerModel.TYPE_SUGGEST_LIST
                    || parentViewType == VideoPagerModel.TYPE_NEW_VIDEO_LIST
                    || parentViewType == VideoPagerModel.TYPE_FRIEND_LIST
            )
                return TYPE_GRID_VIDEO;
            else if (parentViewType == VideoPagerModel.TYPE_CHANNEL_LIST)
                return TYPE_GRID_CHANNEL;

        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_LARGE_VIDEO:
                return new VideoDetailHolder(layoutInflater.inflate(R.layout.holder_large_video, parent, false), activity, listener, widthVideo);
            case TYPE_GRID_VIDEO:
                return new VideoDetailHolder(layoutInflater.inflate(R.layout.holder_grid_video, parent, false), activity, listener, widthVideo);
            case TYPE_GRID_CHANNEL:
                return new ChannelDetailHolder(layoutInflater.inflate(R.layout.holder_channel_on_tab_video, parent, false), activity, listener, TabHomeUtils.getWidthChannelVideo());
            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

package com.metfone.selfcare.controllers;

import android.app.Activity;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseEmoGridAdapter;
import com.metfone.selfcare.adapter.EmoticonsPagerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.ui.viewpagerindicator.IconIndicator;
import com.metfone.selfcare.ui.viewpagerindicator.TabPageIndicator;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thaodv on 10-Feb-15.
 */
public class EmojiAndStickerController {
    private static final String TAG = EmojiAndStickerController.class.getSimpleName();
    private ViewPager mViewPager;
    private TabPageIndicator mIndicator;
    private Activity mActivity;
    private BaseEmoGridAdapter.KeyClickListener mKeyClickListener;
    private ApplicationController mApplicationController;
    private StickerBusiness mStickerBusiness;
    private List<StickerCollection> allStickerCollection;
    ArrayList<IconIndicator> icons = new ArrayList<>();
    private EmoticonsPagerAdapter pagerAdapter;
    private TextView mTvNewSticker;
    private EditText mEdtContent;
    private ReengAccountBusiness reengAccountBusiness;
    private int threadType;

    public EmojiAndStickerController(Activity activity, BaseEmoGridAdapter.KeyClickListener listener,
                                     ViewPager pager, TabPageIndicator mIndicator,
                                     TextView pTvNewSticker, EditText pEdtContent,
                                     int threadType) {
        this.mActivity = activity;
        this.mViewPager = pager;
        this.mIndicator = mIndicator;
        this.mTvNewSticker = pTvNewSticker;
        this.mKeyClickListener = listener;
        this.mEdtContent = pEdtContent;
        this.threadType = threadType;
        mApplicationController = (ApplicationController) activity.getApplication();
        mStickerBusiness = mApplicationController.getStickerBusiness();
        reengAccountBusiness = mApplicationController.getReengAccountBusiness();
        setNewTextView();
    }

    private void setNewTextView() {
        ArrayList<Integer> newLisst = mStickerBusiness.getStickersNew();
        int newitem = newLisst.size();
        Log.i(TAG, "setNewTextView " + newitem);
        if (newitem < 1) {
            mTvNewSticker.setVisibility(View.GONE);
            return;
        }
        mTvNewSticker.setVisibility(View.VISIBLE);
        if (newitem >= 10) {
            mTvNewSticker.setText("9+");
        } else {
            mTvNewSticker.setText("" + newitem);
            //mTvNewSticker.setPadding(7, 4, 7, 7);
        }
    }

    public void initViewPagerController() {
        allStickerCollection = mStickerBusiness.getListStickyStickerCollection();
        icons = getListIcon(allStickerCollection);
        pagerAdapter = new EmoticonsPagerAdapter(
                mActivity, mKeyClickListener, icons, true, allStickerCollection, mEdtContent);
        mViewPager.setAdapter(pagerAdapter);
        //mViewPager.setOffscreenPageLimit(1);
        mIndicator.setViewPager(mViewPager);
        mIndicator.notifyDataSetChanged();
        mIndicator.setCurrentItem(0);
        mViewPager.setCurrentItem(0);
        // }
    }

    public void refreshViewPager() {
        Log.i(TAG, "refreshViewPager");
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                allStickerCollection = mStickerBusiness.getListStickyStickerCollection();
                icons = getListIcon(allStickerCollection);
                pagerAdapter.setStickerCollections(allStickerCollection);
                pagerAdapter.setIcons(icons);
                pagerAdapter.notifyDataSetChanged();
                mIndicator.notifyDataSetChanged();
                int pos = mApplicationController.getStickerBusiness().getPositionStickerWhenResume();
                if (pos > 0) {
                    mIndicator.setCurrentItem(pos);
                    mViewPager.setCurrentItem(pos);
                }
            }
        });
        //        initViewPagerController();
        setNewTextView();
    }

    private ArrayList<IconIndicator> getListIcon(List<StickerCollection> allStickerCollection) {
        ArrayList<IconIndicator> icons = new ArrayList<>();
        /*if (!reengAccountBusiness.isHaiti()) {
        bo recent sticker neu la ban cho haiti*/
        icons.add(new IconIndicator(R.drawable.ic_sticker_recent_icon, R.drawable.ic_sticker_recent_icon_selected));
        /*}
        if (!reengAccountBusiness.isHaiti()) {*/
        //bo general sticker neu la ban cho haiti
        icons.add(new IconIndicator(R.drawable.ic_voice_sticker_icon, R.drawable.ic_voice_sticker_icon_selected));
//        }
        icons.add(new IconIndicator(R.drawable.ic_emotion_icon, R.drawable.ic_emotion_icon_selected));
        if (allStickerCollection != null && !allStickerCollection.isEmpty()) {
            int sizeStickerCollection = allStickerCollection.size();
            for (int i = 0; i < sizeStickerCollection; i++) {
                StickerCollection stickerCollection = allStickerCollection.get(i);
                String iconUrl = UrlConfigHelper.getInstance(mActivity).getDomainFile()
                        + stickerCollection.getCollectionIconPath();
                Log.i(TAG, "fullAvatarLink " + iconUrl);
                if (stickerCollection.isStickerStore()) {
                    icons.add(new IconIndicator(R.drawable.ic_sticker_default, null, IconIndicator.TYPE_INDICATOR_RES));
                } else {
//                    icons.add(new IconIndicator(0, iconUrl, IconIndicator.TYPE_INDICATOR_URL));
                    icons.add(new IconIndicator(iconUrl, iconUrl));
                }
            }
        }
        return icons;
    }

    /**
     * notify data grid change
     */
    public void notifyDataSetChanged() {
        if (pagerAdapter != null) {
            pagerAdapter.notifyDataSetChanged();
        }
    }

    /**
     * ve lai list recent sticker
     *
     * @param listItems
     */
    public void notifyChangeRecentSticker(ArrayList<StickerItem> listItems) {
        if (pagerAdapter != null) {
            pagerAdapter.notifyChangeRecentAdapter(listItems);
        }
    }
}

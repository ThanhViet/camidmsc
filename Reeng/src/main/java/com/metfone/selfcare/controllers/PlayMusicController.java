package com.metfone.selfcare.controllers;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.RemoteViews;

import com.bumptech.glide.request.target.NotificationTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.app.IMService;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.MusicRequestHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.keeng.KeengPlayerActivity;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.easyvideoplayer.Util;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.media.session.MediaButtonReceiver;
import androidx.palette.graphics.Palette;

/**
 * Created by toanvk2 on 4/6/2016.
 */
public class PlayMusicController implements
        MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnInfoListener,
        AudioManager.OnAudioFocusChangeListener {

    private static final String TAG = PlayMusicController.class.getSimpleName();
    private static final int UPDATE_STATE_NONE = Constants.PLAY_MUSIC.PLAYING_NONE;
    private static final int UPDATE_STATE_GET_DATA = Constants.PLAY_MUSIC.PLAYING_GET_INFO;
    private static final int UPDATE_STATE_PREPARING = Constants.PLAY_MUSIC.PLAYING_PREPARING;
    private static final int UPDATE_STATE_PLAYING = Constants.PLAY_MUSIC.PLAYING_PLAYING;
    private static final int UPDATE_PROGRESS = 40;                     // cap nhat progress
    private static final int UPDATE_BUFFER = 41;                     // cap nhat buffer
    private static final int UPDATE_STATE_REPEAT = 51;
    private static final int UPDATE_CLOSE_MUSIC = 52;
    private static final int UPDATE_ACTION_USER = 53;
    private static final int UPDATE_LOAD_FAIL = 54;
    private static final int UPDATE_ACTION_CLICK_PLAY = 55;
    private static final int UPDATE_SONG_DATA = 56;
    private static final long TIMER_UPDATE_STATE = 1000;//1s
    private ApplicationController mApplication;
    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;
    private static ArrayList<OnPlayMusicStateChange> mPlayMusicStateChanges = new ArrayList<>();
    private ArrayList<MediaModel> mDataSong = new ArrayList<>();
    private Handler mHandler;
    private String mMessageError = "";
    private boolean isStopTimer = true;
    private int currentProgress = 0, currentBuffer = 0;
    private int currentMediaPosition;
    private int mStatePlaying = Constants.PLAY_MUSIC.PLAYING_NONE;
    private int mStateRepeat = Constants.PLAY_MUSIC.REPEAT_All;
    private int mStateShuffle = Constants.PLAY_MUSIC.REPEAT_SUFF_OFF;
    private boolean statePauseChangeSong = false;       // trang thai danh dau dang pause thi chuyen bai
    private boolean stateResumePlaying = false;         // trang thai danh dau dang nghe thi bat voicemail. sticker ...
    private boolean otherAudioPlaying = false;          // trang thai danh dau dang co 1 audio # dc play (ghi am)
    private boolean isPlayFromFeed = false;
    //TODO can sua lai
    private int mCurrentSongIndex = 0;
    private int mLoadDetailErrorCount = 0; // get song detail bi loi
    private int clickNext = 0;
    private int mMediaErrorCount = 0;
    private int mPlaySongErrorCount = 0;   // play song exception
    private boolean mIsHuman = false;//action cua nguoi
    private boolean isRetryWhenError = false;
    private int countErrorIo = 0;
    private boolean isPrepared = false;
    private boolean isBufferFirst = true;
    private boolean isRunningKeengPlayerActivity;
    private long timeBufferStart = 0L;
    private String mCurrentPlaylistName;
    private MediaModel currentSong;
    private boolean isCompletion = false;

    public PlayMusicController(ApplicationController application) {
        this.mApplication = application;
        this.mHandler = new Handler();
        mAudioManager = (AudioManager) mApplication.getSystemService(Context.AUDIO_SERVICE);
    }

    public void initMedia() {
        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
            // mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setOnBufferingUpdateListener(this);
        }
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnCompletionListener(this);
        mMediaPlayer.setOnInfoListener(this);

        setUpMediaSession();
    }

    public static void addPlayMusicStateChange(OnPlayMusicStateChange listener) {
        if (!mPlayMusicStateChanges.contains(listener)) {
            mPlayMusicStateChanges.add(listener);
        }
    }

    public static void removePlayMusicStateChange(OnPlayMusicStateChange listener) {
        if (mPlayMusicStateChanges.contains(listener)) {
            mPlayMusicStateChanges.remove(listener);
        }
    }

    public void setDataSong(ArrayList<MediaModel> songs) {
        this.mDataSong = songs;
    }

    // get state playing
    public int getStatePlaying() {
        return mStatePlaying;
    }

    public String getDurationString() {
        if (mMediaPlayer == null) {
            return "--:--";
        }
        return Util.getDurationString(mMediaPlayer.getDuration(), false);
    }

    public boolean isPrepareForPlaying() {
        return mStatePlaying == Constants.PLAY_MUSIC.PLAYING_PLAYING || mStatePlaying == Constants.PLAY_MUSIC.PLAYING_PREPARING;
    }

    public boolean isPlaying() {
        return mStatePlaying == Constants.PLAY_MUSIC.PLAYING_PLAYING;
    }

    public boolean isPaused() {
        return mStatePlaying == Constants.PLAY_MUSIC.PLAYING_PAUSED;
    }

    public void setStateRepeat(int mStateRepeat) {
        this.mStateRepeat = mStateRepeat;
    }

    public int getStateRepeat() {
        return mStateRepeat;
    }

    public int getStateShuffle() {
        return mStateShuffle;
    }

    public void setStateShuffle(int mStateShuffle) {
        this.mStateShuffle = mStateShuffle;
    }

    public void setStatePauseChangeSong(boolean statePauseChangeSong) {
        this.statePauseChangeSong = statePauseChangeSong;
    }

    public void setStateResumePlaying(boolean stateResumePlaying) {
        this.stateResumePlaying = stateResumePlaying;
    }

    public void setOtherAudioPlaying(boolean otherAudioPlaying) {
        this.otherAudioPlaying = otherAudioPlaying;
    }

    public boolean isPlayFromFeed() {
        return isPlayFromFeed;
    }

    public void setIsPlayFromFeed(boolean isPlayFromFeed) {
        this.isPlayFromFeed = isPlayFromFeed;
    }

    public MediaModel getCurrentSong() {
        if (mDataSong == null || mCurrentSongIndex >= mDataSong.size()) return null;
        return mDataSong.get(mCurrentSongIndex);
    }

    public int getCurrentSongIndex() {
        return mCurrentSongIndex;
    }

    public void setCurrentSongIndex(int mCurrentSongIndex) {
        this.mCurrentSongIndex = mCurrentSongIndex;
    }

    public MediaPlayer getMediaPlayer() {
        return this.mMediaPlayer;
    }

    public ArrayList<MediaModel> getDataSong() {
        return mDataSong;
    }

    public int getCurrentProgress() {
        return currentProgress;
    }

    public int getCurrentMediaPosition() {
        return currentMediaPosition;
    }

    public boolean isPrepared() {
        return isPrepared;
    }

    public boolean isBufferFirst() {
        return isBufferFirst;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        Log.d(TAG, "onAudioFocusChange: " + focusChange);
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (mMediaPlayer != null && mIsHuman) {
                    try {
                        mIsHuman = false;
                        startMusic();
                        mMediaPlayer.setVolume(1.0f, 1.0f);
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                } else if (mMediaPlayer == null) {
                    initMedia();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                /* Lost focus for an unbounded amount of time: stop playback and
                 release media player*/
                if (mMediaPlayer != null && isPlaying()) {
                    pauseAutoMusic();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                /* Lost focus for a short time, but we have to stop
                 playback. We don't release the media player because playback*/
                if (mMediaPlayer != null && isPlaying()) {
                    checkSetIsHuman();
                    pauseAutoMusic();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                /* Lost focus for a short time, but it's ok to keep playing
                 at an attenuated level*/
                /*if (mMediaPlayer != null && isPlaying())
                    mMediaPlayer.setVolume(0.3f, 0.3f);*/
                break;
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        LuckyWheelHelper.getInstance(ApplicationController.self()).doMission(Constants.LUCKY_WHEEL.ITEM_LISTEN_MUSIC_KEENG);
        Log.d(TAG, "onPrepared mStatePlaying: " + mStatePlaying + ", statePauseChangeSong: " + statePauseChangeSong);
        isPrepared = true;
        clickNext = 0;
        if (mStatePlaying != Constants.PLAY_MUSIC.PLAYING_CLOSED) {
            if (statePauseChangeSong) {
                mStatePlaying = Constants.PLAY_MUSIC.PLAYING_PAUSED;
                Log.d(TAG, "state: PLAYING_PAUSED");
                statePauseChangeSong = false;
                pauseMusic();
            } else if (otherAudioPlaying) {//neu dang play voice,sticker,ghi am thi pause, danh dau de resume lai
                mStatePlaying = Constants.PLAY_MUSIC.PLAYING_PAUSED;
                Log.d(TAG, "state: PLAYING_PAUSED");
                stateResumePlaying = true;
                pauseMusic();
            } else {
                mStatePlaying = Constants.PLAY_MUSIC.PLAYING_PREPARED;
                Log.d(TAG, "state: PLAYING_PREPARED");
                startMusic();
            }
        } else {
            mMediaPlayer.stop();
        }

        //Log KQI buffer
        logKQIBuffer();
    }

    private void logKQIBuffer() {
        try {
            MediaModel cSong = mDataSong.get(mCurrentSongIndex);
            if (cSong.getFromSource() == MediaModel.FROM_SOURCE_KEENG) {
                LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.MUSIC_GET_SONG_BUFFER, (System.currentTimeMillis() - timeBufferStart) + "", System.currentTimeMillis() + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            } else {
                LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.MOCHA_SONG_BUFFER, (System.currentTimeMillis() - timeBufferStart) + "", System.currentTimeMillis() + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        Log.d(TAG, "onBufferingUpdate percent: " + percent);
        currentBuffer = percent;
        updateStatePlayMusic(UPDATE_BUFFER);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "onCompletion mStatePlaying: " + mStatePlaying);
        if (mStatePlaying == Constants.PLAY_MUSIC.PLAYING_CLOSED) {// state==close
            mApplication.getMusicBusiness().resetSessionMusic();
            logEnd(currentSong);
        } else if (!mApplication.getMusicBusiness().isExistListenMusic() &&
                !mApplication.getMusicBusiness().isListenAlbum()) {//khong con danh dau cung nghe
            mApplication.getMusicBusiness().resetSessionMusic();
            closeNotifyMusicAndClearDataSong();
            logEnd(currentSong);
        } else if (mDataSong.size() == 0 || isPaused()) {// data empty hoac ng dung an pause trc
            Log.i(TAG, "dataSong empty || isPaused");
        } else if (isRetryWhenError) {
            isRetryWhenError = false;
            if (mHandler == null) mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                public void run() {
                    playSong(true, mCurrentSongIndex);
                    mMediaErrorCount++;
                }
            }, Constants.PLAY_MUSIC.MUSIC_RETRY_DELAY_TIME);
        } else {
            isCompletion = true;
            Log.d(TAG, "onCompletion : mStateRepeat :> " + mStateRepeat);
            if (mStateRepeat == Constants.PLAY_MUSIC.REPEAT_ONE) { // che do repeat 1
                if (currentBuffer > 0 && currentBuffer < 99) {
                    stopAutoMusic();
                    currentBuffer = 0;
                } else {
                    startMusic();
                }
                logEnd(currentSong);
                return;
            }
            if (mStateShuffle == Constants.PLAY_MUSIC.REPEAT_SUFF) { // che do shuffle
                mCurrentSongIndex = getRandom(mCurrentSongIndex);
                playSong(false, mCurrentSongIndex);
                return;
            }
            // play next song
            playSong(false, mCurrentSongIndex + 1);
        }
    }

    String USER_AGENT = Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL;

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e(TAG, "onError MediaPlayer what: " + what + ", extra: " + extra);
        MediaModel cSong = getCurrentSong();
        if (cSong != null && extra == MediaPlayer.MEDIA_ERROR_IO) {
            Log.i(TAG, "getPlayingListDetail go go go!!!!! mMediaErrorCount: " + mMediaErrorCount);
            if (countErrorIo >= Constants.PLAY_MUSIC.MUSIC_RETRY_MAX_COUNT) {
                countErrorIo = 0;
                logErrorPlayMusic(" extra-> " + extra);
                updateStatePlayMusic(UPDATE_LOAD_FAIL);
                String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + cSong.getMedia_url() + "|" + extra;
                ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
            } else {
                forceGetSongDetail(cSong);
                countErrorIo++;
                return true;
            }
        } else {
            Log.d(TAG, " onError mErrorCount: " + mMediaErrorCount + " clickNext: " + clickNext);
            isRetryWhenError = clickNext <= 0;
            clickNext--;
            if (mMediaErrorCount >= Constants.PLAY_MUSIC.MUSIC_RETRY_MAX_COUNT) {
                isRetryWhenError = false;
                mMediaErrorCount = 0;
                logErrorPlayMusic(" extra-> " + extra);
                updateStatePlayMusic(UPDATE_LOAD_FAIL);
                String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + cSong.getMedia_url() + "|" + extra;
                ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
            }
        }
        return false;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_UNKNOWN:
                Log.i(TAG, "MEDIA_INFO_UNKNOWN extra: " + extra);
                break;
            case MediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING:
                Log.i(TAG, "MEDIA_INFO_VIDEO_TRACK_LAGGING extra: " + extra);
                break;
            case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                Log.i(TAG, "MEDIA_INFO_VIDEO_RENDERING_START extra: " + extra);
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                Log.i(TAG, "state: MEDIA_INFO_BUFFERING_START extra: " + extra);
                for (OnPlayMusicStateChange listener : mPlayMusicStateChanges) {
                    listener.onChangeStateInfo(Constants.PLAY_MUSIC.PLAYING_BUFFERING_START);
                }
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                isBufferFirst = false;
                Log.i(TAG, "state: MEDIA_INFO_BUFFERING_END extra: " + extra);
                for (OnPlayMusicStateChange listener : mPlayMusicStateChanges) {
                    listener.onChangeStateInfo(Constants.PLAY_MUSIC.PLAYING_BUFFERING_END);
                }
                break;
            case 703:
//            case MediaPlayer.MEDIA_INFO_NETWORK_BANDWIDTH:
                Log.i(TAG, "MEDIA_INFO_NETWORK_BANDWIDTH extra: " + extra);
                break;
            case MediaPlayer.MEDIA_INFO_BAD_INTERLEAVING:
                Log.i(TAG, "MEDIA_INFO_BAD_INTERLEAVING extra: " + extra);
                break;
            case MediaPlayer.MEDIA_INFO_NOT_SEEKABLE:
                Log.i(TAG, "MEDIA_INFO_NOT_SEEKABLE extra: " + extra);
                break;
            case MediaPlayer.MEDIA_INFO_METADATA_UPDATE:
                Log.i(TAG, "MEDIA_INFO_METADATA_UPDATE extra: " + extra);
                break;
            case MediaPlayer.MEDIA_INFO_UNSUPPORTED_SUBTITLE:
                Log.i(TAG, "MEDIA_INFO_UNSUPPORTED_SUBTITLE extra: " + extra);
                break;
            case MediaPlayer.MEDIA_INFO_SUBTITLE_TIMED_OUT:
                Log.i(TAG, "MEDIA_INFO_SUBTITLE_TIMED_OUT extra: " + extra);
                break;
        }
        return true;

//        return false;
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
//            Log.d(TAG, "TimerUpdate: " + isStopTimer);
            if (!isStopTimer) {
                if (mHandler == null) mHandler = new Handler();
                mHandler.postDelayed(this, TIMER_UPDATE_STATE);
            }
            if (mMediaPlayer != null && isPlaying()) {
                currentMediaPosition = mMediaPlayer.getCurrentPosition();
                currentProgress = TimeHelper.getProgressPercentage(currentMediaPosition, mMediaPlayer.getDuration());
                updateStatePlayMusic(UPDATE_PROGRESS);
            }
        }
    };

    private void startTimerUpdate() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        if (!isStopTimer) return;
        isStopTimer = false;
        mHandler.postDelayed(mRunnable, TIMER_UPDATE_STATE);
    }

    // stop countdown
    private void stopTimerUpdate() {
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
        isStopTimer = true;
    }

    private int getRandom(int last) {
        if (mDataSong.size() == 0 || mDataSong.size() == 1) {
            return 0;
        }
        Random rand = new Random();
        int mNew = rand.nextInt(mDataSong.size());
        if (mNew != last) {
            return mNew;
        } else {
            return getRandom(last);
        }
    }

    private void checkSetIsHuman() {
        if (isPlaying()) {
            mIsHuman = true;
        }
    }

    public void setIsHuman(boolean isHuman) {
        this.mIsHuman = isHuman;
    }

    public void startMusic() {
        Log.d(TAG, "startMusic() ");
        if (mDataSong.size() == 0) {
            return;
        }
        try {
            mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            if (mStatePlaying != Constants.PLAY_MUSIC.PLAYING_PREPARING && mStatePlaying != Constants.PLAY_MUSIC
                    .PLAYING_RETRIEVING) {
                mMediaPlayer.start();
                mStatePlaying = Constants.PLAY_MUSIC.PLAYING_PLAYING;
                updateStatePlayMusic(UPDATE_STATE_PLAYING);
            }
            updateNotification();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            logErrorPlayMusic(e.getMessage());
            updateStatePlayMusic(UPDATE_LOAD_FAIL);
            String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + null + "|" + "startMusic exeption: " + e.getMessage();
            ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
        }
    }

    public void pauseMusic() {
        Log.d(TAG, "pauseMusic");
        if (mDataSong.size() == 0) {
            return;
        }
        if (isPlaying()) {
            mMediaPlayer.pause();
            mStatePlaying = Constants.PLAY_MUSIC.PLAYING_PAUSED;
        }
        updateStatePlayMusic(UPDATE_STATE_PLAYING);
//        if (IMService.isReady()) {
//            IMService.getInstance().stopForegroundCompat(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
//        }
        if (mApplication.isReady())
            mApplication.cancelNotification(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
    }

    private void pauseAutoMusic() {
        Log.d(TAG, "pauseAutoMusic: " + mDataSong.size());
        if (mDataSong.size() == 0) {
            return;
        }
        if (isPlaying()) {
            mMediaPlayer.pause();
            mStatePlaying = Constants.PLAY_MUSIC.PLAYING_PAUSED;
            updateStatePlayMusic(UPDATE_STATE_PLAYING);
        }
        updateNotification();
    }

    private void stopMusic() {
        Log.i(TAG, "stopMusic ");
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
        }
        isPlayFromFeed = false;
        isPrepared = false;
        isBufferFirst = true;
    }

    private void releaseMusic() {
        Log.i(TAG, "releaseMusic ");
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        if (mSession != null)
            mSession.setActive(false);
        if (mSession != null)
            mSession.release();
    }

    public void stopAutoMusic() {
        /*stop timer update
        duoc goi khi khong chon che do auto replay, chua can stop timer,
        sau nay neu xoa notification khi play het bai thi moi stop*/
        stopTimerUpdate();
        Log.i(TAG, "stopAutoMusic ");
        if (mDataSong.size() == 0) {
            return;
        }
        if (isPlaying()) {
            stopMusic();
            mMediaPlayer.reset();
            mStatePlaying = Constants.PLAY_MUSIC.PLAYING_PAUSED;
        }
        updateStatePlayMusic(UPDATE_STATE_NONE);
    }

    public void toggleMusic() {
        Log.d(TAG, "toggleMusic() ");
        if (mDataSong.size() == 0) {
            return;
        }
        // neu timer update khong chay khi start no
        startTimerUpdate();
        if (isPlaying()) {
            pauseAutoMusic();
        } else {
            startMusic();
        }
    }

    public void playNextMusic() {
        if (mDataSong.size() == 0) {
            return;
        }
        clickNext += 2;
        Log.d(TAG, "playNextMusic  clickNext-> " + clickNext);
        mMediaErrorCount = 0;//reset
        if (mStateShuffle == Constants.PLAY_MUSIC.REPEAT_SUFF) {// che do random
            Log.d(TAG, "playNextMusic-> Random ");
            mCurrentSongIndex = getRandom(mCurrentSongIndex);
            playSong(false, mCurrentSongIndex);
        } else if (mCurrentSongIndex == mDataSong.size() - 1) {// che do repeat all + last song
            Log.d(TAG, "playNextMusic-> lastIndex");
            mCurrentSongIndex = 0;
            playSong(false, mCurrentSongIndex);
        } else if (mStateRepeat == Constants.PLAY_MUSIC.REPEAT_ONE) {// che do repeat 1
            Log.d(TAG, "playNextMusic-> lap lai 1 bai hat ");
            playSong(false, mCurrentSongIndex + 1);
        } else { // che do repeat all
            Log.d(TAG, "playNextMusic-> index+1)");
            playSong(false, mCurrentSongIndex + 1);
        }
    }

    public void playForwardMusic() {
        if (mDataSong.size() == 0) {
            return;
        }
        clickNext += 2;
        mMediaErrorCount = 0;//reset
        if (mStateShuffle == Constants.PLAY_MUSIC.REPEAT_SUFF) {// che do random
            Log.d(TAG, "playNextMusic-> Random ");
            mCurrentSongIndex = getRandom(mCurrentSongIndex);
            playSong(false, mCurrentSongIndex);
        } else if (mCurrentSongIndex == 0) { // che do repeat all + 1st song
            mCurrentSongIndex = mDataSong.size() - 1;
            Log.d(TAG, " playForwardMusic-> firstIndex");
            playSong(false, mCurrentSongIndex);
        } else if (mStateRepeat == Constants.PLAY_MUSIC.REPEAT_ONE) {// che do repeat 1
            Log.d(TAG, "playNextMusic-> lap lai 1 bai hat ");
            playSong(false, mCurrentSongIndex - 1);
        } else { // che do repeat all
            Log.d(TAG, " playForwardMusic-> index-1");
            playSong(false, mCurrentSongIndex - 1);
        }
    }

    public void checkAndResumeMusic() {
        otherAudioPlaying = false;
        if (stateResumePlaying) {
            stateResumePlaying = false;
            if (isPaused()) {
                Log.d(TAG, "resume music");
                toggleMusic();
            }
        }
    }

    public void playSong(boolean retryError, int songIndex) {
        Log.i(TAG, "playSong retryError: " + retryError + ", songIndex: " + songIndex);
        if (!retryError) logEnd(currentSong);
        //play nhạc thì tắt hết video mini
        isCompletion = false;
        VideoService.stop(mApplication);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicRequestHelper.TAG_ALBUM);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicRequestHelper.TAG_SONG);
        try {
            //LocalPushReceiver.updateMusicLastUsed(mContext); khong push music nua
            if (mStatePlaying == Constants.PLAY_MUSIC.PLAYING_CLOSED) {
                Log.i(TAG, "mStatePlaying CLOSED");
                return;
            }
            if (mDataSong == null || mDataSong.size() == 0) {
                return;
            }
            if (mMediaPlayer == null) {
                initMedia();
            }
            if (mPlaySongErrorCount >= Constants.PLAY_MUSIC.MUSIC_RETRY_MAX_COUNT) {
                Log.i(TAG, "playSong  error > mExceptionCount: " + mPlaySongErrorCount);
                //pauseMusic();
                mPlaySongErrorCount = 0;
                updateStatePlayMusic(UPDATE_LOAD_FAIL);
                MediaModel cSong = mDataSong.get(mCurrentSongIndex);
                if (cSong != null) {
                    String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + cSong.getMedia_url() + "|" + "MUSIC_RETRY_MAX_COUNT";
                    ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
                }
                return;
            }
            // start timer
            startTimerUpdate();
            Log.i(TAG, "playSong : startCountdownUpdate");
            if (songIndex >= mDataSong.size()) {
                songIndex = 0;
            }
            // khoi tao lai media
            isPrepared = false;
            isBufferFirst = true;
            timeBufferStart = System.currentTimeMillis();
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mCurrentSongIndex = songIndex;
            currentSong = mDataSong.get(mCurrentSongIndex);
            String media_url = currentSong.getMedia_url(); // mac dinh la 128
            if (TextUtils.isEmpty(media_url)) {
                return;
            }
            Log.i(TAG, "media_url: " + media_url);
            media_url = getParamMsisdn(media_url);
            Log.i(TAG, "media_url after add param msisdn: " + media_url);
            currentBuffer = 0;
            mMediaPlayer.setDataSource(media_url);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mStatePlaying = Constants.PLAY_MUSIC.PLAYING_PREPARING;
            updateStatePlayMusic(UPDATE_STATE_REPEAT);
            updateStatePlayMusic(UPDATE_STATE_PREPARING);
            //currentSong = cSong;
            mMediaPlayer.prepareAsync();
            Log.d(TAG, "mMediaPlayer.prepareAsync(): " + media_url);
            logStart(currentSong);
            //Update Data
            updateStatePlayMusic(UPDATE_SONG_DATA);
            updateNotification();//update notification
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            mPlaySongErrorCount++;
            Log.d(TAG, " playNextWhenError mExceptionCount: " + mPlaySongErrorCount);
            if (!isPaused()) {
                playSong(false, songIndex + 1);
            }
        }
    }

    public int getCurrentBuffer() {
        return currentBuffer;
    }

    private String getParamMsisdn(String mediaUrl) {
        StringBuilder sb = new StringBuilder();
        String query = null;
        try {
            URL url = new URL(mediaUrl);
            query = url.getQuery();
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException", e);
        }
        if (TextUtils.isEmpty(query)) {
            sb.append(mediaUrl).append("?msisdn=").append(Uri.encode(mApplication.getReengAccountBusiness()
                    .getJidNumber()));
        } else {
            sb.append(mediaUrl).append("&msisdn=").append(Uri.encode(mApplication.getReengAccountBusiness()
                    .getJidNumber()));
        }
        return sb.toString();
    }

    /**
     * close Music. co notify, stop ping pong
     */
    public void closeMusic() {
        Log.i(TAG, "closeMusic ");
        // stop timer update
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicRequestHelper.TAG_ALBUM);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicRequestHelper.TAG_SONG);
        stopTimerUpdate();
        stopMusic();
        releaseMusic();
        mStatePlaying = Constants.PLAY_MUSIC.PLAYING_CLOSED;
//        if (IMService.isReady()) {
//            IMService.getInstance().stopForegroundCompat(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
//        }
        if (mApplication.isReady())
            mApplication.cancelNotification(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
        updateStatePlayMusic(UPDATE_CLOSE_MUSIC);
        clearMusicNotification();
    }

    public void setPlayList(ArrayList<MediaModel> listSong, int index) {
        mDataSong.clear();
        mDataSong.addAll(listSong);
        mMediaErrorCount = 0;
        mStatePlaying = Constants.PLAY_MUSIC.PLAYING_NONE;
        playSong(false, index);
    }

    public void closeNotifyMusicAndClearDataSong() {
        Log.d(TAG, "closeNotifyMusicAndClearDataSong");
        if (mDataSong == null || mDataSong.isEmpty()) {
            mStatePlaying = Constants.PLAY_MUSIC.PLAYING_CLOSED;
        } else {  // xoa data song
            mDataSong.clear();
            mStatePlaying = Constants.PLAY_MUSIC.PLAYING_NONE;
        }
        stopTimerUpdate();
        stopMusic();
        releaseMusic();
//        if (IMService.isReady()) {
//            IMService.getInstance().stopForegroundCompat(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
//        }
        if (mApplication.isReady())
            mApplication.cancelNotification(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
        updateStatePlayMusic(UPDATE_STATE_NONE);
        clearMusicNotification();
    }

    public void actionNotification(Intent intent) {
        if (intent != null && Constants.PLAY_MUSIC.ACTION_BROADCAST.equals(intent.getAction())) {
            Log.d(TAG, "actionNotification ");
            int value = intent.getIntExtra(Constants.PLAY_MUSIC.ACTION_NAME, Constants.PLAY_MUSIC.ACTION_VALUE_DEFAULT);
            switch (value) {
                case Constants.PLAY_MUSIC.ACTION_VALUE_NEXT:
                    playNextMusic();
                    break;
                case Constants.PLAY_MUSIC.ACTION_VALUE_PREV:
                    playForwardMusic();
                    break;
                case Constants.PLAY_MUSIC.ACTION_VALUE_PAUSE_PLAY:
                    toggleMusic();
                    updateStatePlayMusic(UPDATE_ACTION_CLICK_PLAY);// chay sau de nhan dc trang thai play dung
                    break;
                case Constants.PLAY_MUSIC.ACTION_VALUE_CLOSE:
                case Constants.PLAY_MUSIC.ACTION_VALUE_SWIPE_DISMISS:
                    updateStatePlayMusic(UPDATE_CLOSE_MUSIC);// chay truoc pause
                    closeMusic();
                    break;
            }
        } else {           // app restart. truong hop vua kill va van con cung nghe se nhay vao
            startTimerUpdate();
            // tat nhac, tat notifi khi app bi kill hoac nhac chua chay ma chay countdown
            if ((mDataSong == null || mDataSong.isEmpty())) {
                closeNotifyMusicAndClearDataSong();
            }
        }
    }

    public void getPlayingListDetail(final MediaModel song) {
        // reset bien dem fail khi play bai moi
        mMediaErrorCount = 0;
        mLoadDetailErrorCount = 0;
        mPlaySongErrorCount = 0;
        mStatePlaying = Constants.PLAY_MUSIC.PLAYING_NONE;
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicRequestHelper.TAG_ALBUM);//remove het de
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicRequestHelper.TAG_SONG);
        updateStatePlayMusic(UPDATE_STATE_GET_DATA);
        if (!TextUtils.isEmpty(song.getMedia_url())) {
            Log.i(TAG, "link found");
            mDataSong.clear();
            mDataSong.add(song);
            playSong(false, 0);
        } else if (song.getSongType() == MediaModel.SONG_TYPE_UPLOAD) {
            Log.i(TAG, "song upload -> link not found");
            mMessageError = mApplication.getString(R.string.e601_error_but_undefined);
            updateStatePlayMusic(UPDATE_LOAD_FAIL);
            String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + song.getMedia_url() + "|" + "MediaModel.SONG_TYPE_UPLOAD";
            ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
        } else {
            Log.i(TAG, "link not found");
            String tag;
            if (song.getType() == MediaModel.TYPE_BAI_HAT) {
                tag = MusicRequestHelper.TAG_SONG;
            } else {
                tag = MusicRequestHelper.TAG_ALBUM;
            }
            //VolleyHelper.getInstance(mApplication).cancelPendingRequests(tag);
            MusicRequestHelper.getInstance(mApplication).getMediaDetail(song.getIdInt(), song.getIdentify(), new MusicRequestHelper
                    .GetSongListener() {
                @Override
                public void onResponse(MediaModel object) {
                    mDataSong.clear();
                    if (song.getType() == MediaModel.TYPE_BAI_HAT) {
                        mDataSong.add(object);
                    } else {
                        mDataSong.addAll(object.getSong_list());
                    }
                    playSong(false, 0);
                }

                @Override
                public void onError(String err) {
                    //if (!retryLoadDetail(song)) {

                    mMessageError = mApplication.getString(R.string.e601_error_but_undefined);
                    updateStatePlayMusic(UPDATE_LOAD_FAIL);
                    String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + song.getMedia_url() + "|" + "Get detail fail MediaModel.SONG_TYPE_UPLOAD";
                    ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
                    //}
                }
            }, tag);
        }
    }

    private MediaModel forceSongDetail;

    /**
     * khi link cache cua bai hat die =="
     *
     * @param song
     */
    public void forceGetSongDetail(final MediaModel song) {
        if (song.getType() == MediaModel.TYPE_ALBUM) {
            mMessageError = mApplication.getString(R.string.e601_error_but_undefined);
            updateStatePlayMusic(UPDATE_LOAD_FAIL);
            String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + song.getMedia_url() + "|" + "MediaModel.TYPE_ALBUM";
            ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
            return;
        }
        // bai hat upload thi chua co api get detail nen th fail thi bao loi luon
        if (song.getSongType() == MediaModel.SONG_TYPE_UPLOAD) {
            Log.i(TAG, "song upload -> link not found");
            mMessageError = mApplication.getString(R.string.e601_error_but_undefined);
            updateStatePlayMusic(UPDATE_LOAD_FAIL);
            String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + song.getMedia_url() + "|" + "MediaModel.SONG_TYPE_UPLOAD";
            ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
            return;
        }
        this.forceSongDetail = song;
        // reset bien dem fail khi play bai moi
        /*mMediaErrorCount = 0;
        mLoadDetailErrorCount = 0;
        mPlaySongErrorCount = 0;*/
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicRequestHelper.TAG_ALBUM);//remove het de
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicRequestHelper.TAG_SONG);//remove request
        // truoc do
        updateStatePlayMusic(UPDATE_STATE_GET_DATA);
        MusicRequestHelper.getInstance(mApplication).getMediaDetail(song.getIdInt(), song.getIdentify(), new MusicRequestHelper
                .GetSongListener() {
            @Override
            public void onResponse(MediaModel object) {
                if (!object.isValid()) {
                    mMessageError = mApplication.getString(R.string.e601_error_but_undefined);
                    updateStatePlayMusic(UPDATE_LOAD_FAIL);
                    String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + song.getMedia_url() + "|" + "!object.isValid";
                    ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
                } else {
                    forceSongDetail.setMedia_url(object.getMedia_url());
                    Log.d(TAG, " forceGetPlayingListDetail() ");
                    playSong(false, 0);
                }
            }

            @Override
            public void onError(String err) {
                mMessageError = mApplication.getString(R.string.e601_error_but_undefined);
                logErrorPlayMusic(err);
                updateStatePlayMusic(UPDATE_LOAD_FAIL);
                String data = USER_AGENT + "|" + NetworkHelper.getTextTypeConnectionForLog(mApplication) + "|" + song.getMedia_url() + "|" + "forceGetSongDetail fail MediaModel.SONG_TYPE_UPLOAD";
                ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TOGHETHER_MUSIC, data);
            }
        }, MusicRequestHelper.TAG_SONG);
    }

    private void updateStatePlayMusic(int is) {
        switch (is) {
            case UPDATE_STATE_NONE:
                Log.d(TAG, "state: UPDATE_STATE_NONE");
                break;
            case UPDATE_STATE_GET_DATA:
                Log.d(TAG, "state: UPDATE_STATE_GET_DATA");
                break;
            case UPDATE_STATE_PREPARING:
                Log.d(TAG, "state: UPDATE_STATE_PREPARING");
                break;
            case UPDATE_STATE_PLAYING:
                Log.d(TAG, "state: UPDATE_STATE_PLAYING");
                break;
            case UPDATE_BUFFER:
//                Log.d(TAG, "state: UPDATE_BUFFER");
                break;
            case UPDATE_PROGRESS:

                break;
            case UPDATE_STATE_REPEAT:
                Log.d(TAG, "state: UPDATE_STATE_REPEAT");
                break;
            case UPDATE_CLOSE_MUSIC:
                Log.d(TAG, "state: UPDATE_CLOSE_MUSIC");
                break;
            case UPDATE_ACTION_USER:

                break;
            case UPDATE_LOAD_FAIL:
                Log.d(TAG, "state: UPDATE_LOAD_FAIL");
                break;
            case UPDATE_ACTION_CLICK_PLAY:

                break;
            case UPDATE_SONG_DATA:
                Log.d(TAG, "state: UPDATE_SONG_DATA");
                break;
        }

        if (is == UPDATE_CLOSE_MUSIC) {
            mApplication.getMusicBusiness().closeMusic();
        } else if (is == UPDATE_LOAD_FAIL) {
            mApplication.getMusicBusiness().onMediaPlayerTimeOut();
        } else if (is == UPDATE_ACTION_CLICK_PLAY) {
            setStatePauseChangeSong(false);
        }
        if (mPlayMusicStateChanges == null || mPlayMusicStateChanges.isEmpty()) {
            Log.d(TAG, "updateStatePlayMusic no listener mStatePlaying: " + mStatePlaying);
            return;
        }

        try {
            for (OnPlayMusicStateChange listener : mPlayMusicStateChanges) {
                switch (is) {
                    case UPDATE_STATE_NONE:
                        mStatePlaying = is;
                        listener.onChangeStateNone();
                        break;
                    case UPDATE_STATE_GET_DATA:
                        mStatePlaying = is;
                        listener.onChangeStateGetData();
                        break;
                    case UPDATE_STATE_PREPARING:
                        mStatePlaying = is;
                        listener.onChangeStatePreparing(getCurrentSong());
                        break;
                    case UPDATE_STATE_PLAYING:

                        if (mApplication != null)
                            mApplication.sendBroadcast(new Intent(Constants.TabVideo.PAUSE_VIDEO_SERVICE));
                        listener.onChangeStatePlaying(getCurrentSong());
                        break;
                    case UPDATE_BUFFER:
                        listener.onUpdateSeekBarBuffering(currentBuffer);
                        break;
                    case UPDATE_PROGRESS:
                        listener.onUpdateSeekBarProgress(currentProgress, currentMediaPosition);
                        break;
                    case UPDATE_STATE_REPEAT:
                        listener.onChangeStateRepeat(mStateRepeat);
                        break;
                    case UPDATE_CLOSE_MUSIC:
                        listener.onCloseMusic();
                        currentProgress = 0;
                        currentMediaPosition = 0;
                        break;
                    case UPDATE_ACTION_USER:
                        listener.onActionFromUser(getCurrentSong());
                        break;
                    case UPDATE_LOAD_FAIL:
                        Log.d(TAG, "onCallLoadDataFail playmusicService");
                        listener.onCallLoadDataFail(mMessageError);
                        break;
                    case UPDATE_ACTION_CLICK_PLAY:
                        //listener.onClickPlayFromUser();
                        break;
                    case UPDATE_SONG_DATA:
                        listener.onUpdateData(getCurrentSong());
                        break;
                }
            }
        } catch (ConcurrentModificationException ex) {

        }
    }

    /**
     * update start, pause to Notification
     */
    private void clearMusicNotification() {
        Log.i(TAG, "clearMusicNotification");
//        if (IMService.isReady())
//            IMService.getInstance().cancelNotification(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
        if (mApplication.isReady())
            mApplication.cancelNotification(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);

        mNotificationPostTime = 0;

        IMService.stop(mApplication);
    }

    private void updateNotification() {
        if (mDataSong.size() == 0) {
            return;
        }
//        Notification notify = createNotification(getCurrentSong());
        Notification notify;
        if (mApplication.getMusicBusiness().isPlayFromKeengMusic()) {
            buildNotification();
        } else {
            notify = createNotification(getCurrentSong());
            if (mApplication.isReady())
                mApplication.notifyWrapper(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID, notify, Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
        }

        if (!IMService.isReady() && mApplication != null)
            mApplication.startService();

//        if (IMService.isReady()) {
//            IMService.getInstance().notifyWrapper(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID, notify);
//        } else {
//            Log.d(TAG, "ImService not ready");
//        }

//        if (mApplication.isReady())
//            mApplication.notifyWrapper(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID, notify, Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
//        else {
//            Log.d(TAG, "ImService not ready");
//        }
    }

    private Notification createNotification(MediaModel song) {
        Notification notification;
        String channelID = Utilities.getChannelIdMusicPlayer(mApplication);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mApplication, channelID);
        RemoteViews normalViews = new RemoteViews(mApplication.getPackageName(), R.layout.notification_keeng_normal);
        RemoteViews views = new RemoteViews(mApplication.getPackageName(), R.layout.notification_keeng);
        String title = song.getName();

        ComponentName service = new ComponentName(mApplication, IMService.class);
        Intent next = new Intent(mApplication, IMService.class);
        next.setAction(Constants.PLAY_MUSIC.ACTION_BROADCAST);
        next.putExtra(Constants.PLAY_MUSIC.ACTION_NAME, Constants.PLAY_MUSIC.ACTION_VALUE_NEXT);
        next.setComponent(service);

        Intent playPause = new Intent(mApplication, IMService.class);
        playPause.setAction(Constants.PLAY_MUSIC.ACTION_BROADCAST);
        playPause.putExtra(Constants.PLAY_MUSIC.ACTION_NAME, Constants.PLAY_MUSIC.ACTION_VALUE_PAUSE_PLAY);
        playPause.setComponent(service);

        Intent close = new Intent(mApplication, IMService.class);
        close.setAction(Constants.PLAY_MUSIC.ACTION_BROADCAST);
        close.putExtra(Constants.PLAY_MUSIC.ACTION_NAME, Constants.PLAY_MUSIC.ACTION_VALUE_CLOSE);
        close.setComponent(service);

        views.setOnClickPendingIntent(R.id.play_next, PendingIntent.getService(mApplication, 1, next, 0));
        views.setOnClickPendingIntent(R.id.play_pause, PendingIntent.getService(mApplication, 2, playPause, 0));
        views.setOnClickPendingIntent(R.id.play_close, PendingIntent.getService(mApplication, 4, close, 0));
        // normal notify
        normalViews.setOnClickPendingIntent(R.id.play_next, PendingIntent.getService(mApplication, 1, next, 0));
        normalViews.setOnClickPendingIntent(R.id.play_pause, PendingIntent.getService(mApplication, 2, playPause, 0));
        normalViews.setOnClickPendingIntent(R.id.play_close, PendingIntent.getService(mApplication, 4, close, 0));
        Bitmap bitmap = null;
        if (bitmap == null) {
            views.setImageViewResource(R.id.cover, R.drawable.ic_keeng);
            normalViews.setImageViewResource(R.id.cover, R.drawable.ic_keeng);
        } else {
            views.setImageViewBitmap(R.id.cover, bitmap);
            normalViews.setImageViewBitmap(R.id.cover, bitmap);
        }
        int playButton = isPlaying() ? R.drawable.ic_player_notify_pause : R.drawable.ic_player_notify_play;
        views.setImageViewResource(R.id.play_pause, playButton);
        normalViews.setImageViewResource(R.id.play_pause, playButton);
        if (mApplication.getMusicBusiness().isExistListenerRoom()) {
            if (mApplication.getMusicBusiness().isPlayList()) {
                views.setViewVisibility(R.id.play_next, View.VISIBLE);
                normalViews.setViewVisibility(R.id.play_next, View.VISIBLE);
            } else {
                views.setViewVisibility(R.id.play_next, View.INVISIBLE);
                normalViews.setViewVisibility(R.id.play_next, View.GONE);// normal notify gone action next
            }
        } else if (mApplication.getMusicBusiness().isListenAlbum()) {
            views.setViewVisibility(R.id.play_next, View.VISIBLE);
            normalViews.setViewVisibility(R.id.play_next, View.VISIBLE);
        } else {
            views.setViewVisibility(R.id.play_next, View.INVISIBLE);
            normalViews.setViewVisibility(R.id.play_next, View.GONE);
        }
        views.setTextViewText(R.id.title, title);
        views.setTextViewText(R.id.artist, song.getSinger());
        // normal notify
        normalViews.setTextViewText(R.id.title, title);
        normalViews.setTextViewText(R.id.artist, song.getSinger());

        // notification
        notification = builder.setContentTitle(title)
                .setSmallIcon(R.drawable.ic_stat_notify_listen)
                .setCustomContentView(normalViews)
                .setCustomBigContentView(views)
                //.setStyle(new NotificationCompat.Style())
                .build();

        if (bitmap == null) {
            NotificationTarget bigContentTarget = new NotificationTarget(mApplication, R.id.cover, views, notification, Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
            ImageBusiness.setNotification(mApplication, song.getImage(), R.drawable.ic_keeng, bigContentTarget);

            NotificationTarget contentTarget = new NotificationTarget(mApplication, R.id.cover, normalViews, notification, Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
            ImageBusiness.setNotification(mApplication, song.getImage(), R.drawable.ic_keeng, contentTarget);
        }

        //notification.contentView = views;
        //notification.icon = R.drawable.ic_stat_notify_listen;
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.tickerText = title;
        Intent i;
        Bundle bundle = null;
        if (mApplication.getMusicBusiness().isExistListenerGroup()) {
            ThreadMessage thread = mApplication.getMessageBusiness().
                    findGroupThreadByServerId(mApplication.getMusicBusiness().getCurrentGroupId());
            if (thread != null) {
                bundle = new Bundle();
                bundle.putInt(ThreadMessageConstant.THREAD_ID, thread.getId());
                bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT);
            }
        } else if (mApplication.getMusicBusiness().isExistListenerRoom()) {
            ThreadMessage thread = mApplication.getMessageBusiness().
                    findRoomThreadByRoomId(mApplication.getMusicBusiness().getCurrentRoomId());
            if (thread != null) {
                bundle = new Bundle();
                bundle.putInt(ThreadMessageConstant.THREAD_ID, thread.getId());
                bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT);
            }
        } else if (!TextUtils.isEmpty(mApplication.getMusicBusiness().getCurrentNumberFriend())) {
            String currentNumberInRoom = mApplication.getMusicBusiness().getCurrentNumberFriend();
            bundle = new Bundle();// truyen vao so dien thoai, neu xoa thread thi taoj moi thread do
            bundle.putInt(ThreadMessageConstant.THREAD_ID, -1);
            bundle.putString(Constants.MESSAGE.NUMBER, currentNumberInRoom);
            bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT);
        } else if (isPlayFromFeed()) {
            bundle = new Bundle();
            bundle.putInt(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.ALBUM_DETAIL);
        }
        if (bundle != null) {
            if (isPlayFromFeed()) {
                i = new Intent(mApplication, OnMediaActivityNew.class);
                i.putExtras(bundle);
            } else {
                i = new Intent(mApplication, ChatActivity.class);
                i.putExtras(bundle);
            }
        } else {
            if (mApplication.getMusicBusiness().isPlayFromKeengMusic())
                i = new Intent(mApplication, KeengPlayerActivity.class);
            else
                i = new Intent(mApplication, HomeActivity.class);
        }
        i.setFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        notification.contentIntent = PendingIntent.getActivity(mApplication,
                Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID, i, PendingIntent.FLAG_UPDATE_CURRENT);
        return notification;
    }

    private void logErrorPlayMusic(String msg) {
//        if (BuildConfig.DEBUG)
//            Crashlytics.logException(new Exception("PlayMusic: " + msg));
    }

    public boolean checkContainMediaModel(MediaModel model) {
        for (MediaModel item : mDataSong) {
            if (item.getId().equals(model.getId()))
                return true;
        }
        return false;
    }

    public void setPlayListName(String playListName) {
        mCurrentPlaylistName = playListName;
    }

    public String getCurrentPlayListName() {
        return TextUtils.isEmpty(mCurrentPlaylistName) ? mApplication.getString(R.string.now_playing) : mCurrentPlaylistName;
    }

    public interface OnPlayMusicStateChange {
        void onChangeStateRepeat(int state);

        void onChangeStateNone();

        void onChangeStateGetData();

        void onChangeStatePreparing(MediaModel song);

        void onChangeStatePlaying(MediaModel song);

        void onActionFromUser(MediaModel song);

        void onCloseMusic();

        void onCallLoadDataFail(String message);

        void onUpdateSeekBarProgress(int percent, int currentMediaPosition);

        void onUpdateSeekBarBuffering(int percent);

        void onChangeStateInfo(int state);

        void onUpdateData(MediaModel song);
    }

    private long mNotificationPostTime = 0;
    private MediaSessionCompat mSession;

    private void setUpMediaSession() {
        try {
            ComponentName mediaButtonReceiver = new ComponentName(mApplication, MediaButtonReceiver.class);
            mSession = new MediaSessionCompat(mApplication, "Mocha", mediaButtonReceiver, null);
            mSession.setCallback(new MediaSessionCompat.Callback() {
                @Override
                public void onPause() {
//                pause();
//                mPausedByTransientLossOfFocus = false;
                    Log.i(TAG, "MediaSessionCompat Callback onPause");
                }

                @Override
                public void onPlay() {
//                play();
                    Log.i(TAG, "MediaSessionCompat Callback onPlay");
                }

                @Override
                public void onSeekTo(long pos) {
//                seek(pos);
                    Log.i(TAG, "MediaSessionCompat Callback onSeekTo: " + pos);
                }

                @Override
                public void onSkipToNext() {
//                gotoNext(true);
                    Log.i(TAG, "MediaSessionCompat Callback onSkipToNext");
                }

                @Override
                public void onSkipToPrevious() {
//                prev(false);
                    Log.i(TAG, "MediaSessionCompat Callback onSkipToPrevious");
                }

                @Override
                public void onStop() {
//                pause();
//                mPausedByTransientLossOfFocus = false;
//                seek(0);
//                releaseServiceUiAndStop();
                    Log.i(TAG, "MediaSessionCompat Callback onStop");
                }
            });
            mSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
            mSession.setActive(true);
        } catch (IllegalStateException e) {
            Log.e(TAG, "MediaSessionCompat IllegalStateException", e);
        } catch (RuntimeException e) {
            Log.e(TAG, "MediaSessionCompat RuntimeException", e);
        } catch (Exception e) {
            Log.e(TAG, "MediaSessionCompat Exception", e);
        }
    }

    private void buildNotification() {
        MediaModel song = getCurrentSong();
//        Bitmap artwork = ImageLoader.getInstance().loadImageSync(song.getImage());
//        if (artwork == null) {
//            artwork = ImageLoader.getInstance().loadImageSync("drawable://" + R.drawable.default_player);
//        }

        ImageBusiness.setNotification(mApplication, song.getImage(), R.drawable.default_player, new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition transition) {
                Notification notify = getNotification(resource);
                if (mApplication.isReady() && notify != null)
                    mApplication.notifyWrapper(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID, notify, Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
            }
        });
    }

    @SuppressLint("WrongConstant")
    private Notification getNotification(Bitmap artwork) {
        MediaModel song = getCurrentSong();
        if (song == null) return null;
        final boolean isPlaying = isPlaying();

        int playButtonResId = isPlaying ? R.drawable.exo_controls_pause : R.drawable.exo_controls_play;

        if (mNotificationPostTime == 0) {
            mNotificationPostTime = System.currentTimeMillis();
        }

        Intent nowPlayingIntent = new Intent(mApplication, KeengPlayerActivity.class);
        nowPlayingIntent.setFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        PendingIntent contentIntent = PendingIntent.getActivity(mApplication, Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID, nowPlayingIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        String channelID = Utilities.getChannelIdMusicPlayer(mApplication);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mApplication, channelID)
                .setSmallIcon(R.drawable.ic_stat_notify_listen)
                .setLargeIcon(artwork)
                .setContentIntent(contentIntent)
                .setContentTitle(song.getName())
                .setContentText(song.getSinger())
                .setWhen(mNotificationPostTime)
                .addAction(R.drawable.exo_icon_previous, "", PendingIntent.getService(mApplication, 3, retrievePlaybackAction(Constants.PLAY_MUSIC.ACTION_VALUE_PREV), 0))
                .addAction(playButtonResId, "", PendingIntent.getService(mApplication, 2, retrievePlaybackAction(Constants.PLAY_MUSIC.ACTION_VALUE_PAUSE_PLAY), 0))
                .addAction(R.drawable.exo_icon_next, "", PendingIntent.getService(mApplication, 1, retrievePlaybackAction(Constants.PLAY_MUSIC.ACTION_VALUE_NEXT), 0))
                .addAction(R.drawable.ic_close_black_24dp, "", PendingIntent.getService(mApplication, 4, retrievePlaybackAction(Constants.PLAY_MUSIC.ACTION_VALUE_CLOSE), 0))
                .setDeleteIntent(PendingIntent.getService(mApplication, 4, retrievePlaybackAction(Constants.PLAY_MUSIC.ACTION_VALUE_SWIPE_DISMISS), 0))
                .setOngoing(isPlaying);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            builder.setShowWhen(false);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setVisibility(Notification.VISIBILITY_PUBLIC);
            androidx.media.app.NotificationCompat.MediaStyle mediaStyle = new androidx.media.app.NotificationCompat.MediaStyle();
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                //todo comment lệnh mediaStyle.setMediaSession() là ẩn seekbar trên notify từ android 10
                if (mSession != null)
                    mediaStyle.setMediaSession(mSession.getSessionToken());
            }
            mediaStyle.setShowActionsInCompactView(0, 1, 2, 3);
            builder.setStyle(mediaStyle);
        }
        if (artwork != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            builder.setColor(Palette.from(artwork).generate().getVibrantColor(Color.parseColor("#403f4d")));

        Notification notification = builder.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.tickerText = song.getName();
        return notification;
    }

    private Intent retrievePlaybackAction(final int action) {
        ComponentName service = new ComponentName(mApplication, IMService.class);
        Intent intent = new Intent(mApplication, IMService.class);
        intent.setAction(Constants.PLAY_MUSIC.ACTION_BROADCAST);
        intent.putExtra(Constants.PLAY_MUSIC.ACTION_NAME, action);
        intent.setComponent(service);
        return intent;
    }

    public boolean isRunningKeengPlayerActivity() {
        return isRunningKeengPlayerActivity;
    }

    public void setRunningKeengPlayerActivity(boolean runningKeengPlayerActivity) {
        isRunningKeengPlayerActivity = runningKeengPlayerActivity;
    }

    public void logStart(MediaModel mediaModel) {
        if (mApplication != null && mediaModel != null) {
            mediaModel.setStateLog("START");
            mediaModel.setCallLogEnd(false);
            if (mediaModel.getFromSource() == MediaModel.FROM_SOURCE_KEENG) {
                //todo Log luot nghe
                new WSOnMedia(mApplication).logClickLink(mediaModel.getUrl(), Constants.ONMEDIA.LOG_CLICK.CLICK_TAB_KEENG
                        , AppStateProvider.getInstance().getCampId()
                        , response -> Log.i(TAG, "logClickLink " + response)
                        , error -> Log.e(TAG, "logClickLink", error));
            }
        }
    }

    public void logEnd(MediaModel mediaModel) {
        if (mApplication != null && mediaModel != null && !mediaModel.isCallLogEnd()) {
            mediaModel.setStateLog("END");
            mediaModel.setCallLogEnd(true);
            if (mediaModel.getFromSource() == MediaModel.FROM_SOURCE_KEENG) {

            }
        }
    }
}
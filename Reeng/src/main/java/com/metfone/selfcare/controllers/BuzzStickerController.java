package com.metfone.selfcare.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

/**
 * Created by bigant on 4/13/2015.
 */
public class BuzzStickerController {
    private static final String TAG = BuzzStickerController.class.getSimpleName();
    private static final long SHOW_BUZZ_INTERVAL = 6 * 60 * 60 * 1000; //6h
    private View popupViewPreview;
    private PopupWindow popupWindowPreview;
    private BaseSlidingFragmentActivity mApplicationController;
    private ImageView imgPreview;
    private View mParentView;
    private int offsetY, offsetX;
    private CountDownTimer mCountDownTimer;
    private int showBuzzCount = 0;
    private SharedPreferences mPref;

    public BuzzStickerController(BaseSlidingFragmentActivity app, View parentView) {
        mApplicationController = app;
        mParentView = parentView;
        mPref = mApplicationController.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                Context.MODE_PRIVATE);
    }


    //hien thi buzz sticker khi moi vao man hinh chat
    /*public void showBuzzStickerOnStart(ThreadMessage threadMessage) {
        if (!canShowPopup(threadMessage)) return;
        threadMessage.setLastTimeShowBuzz(System.currentTimeMillis());
        LayoutInflater layoutInflater
                = (LayoutInflater) mApplicationController.getBaseContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupViewPreview = layoutInflater.inflate(R.layout.popup_preview_sticker, null, false);
        popupWindowPreview = new PopupWindow(popupViewPreview,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imgPreview = (ImageView) popupViewPreview.findViewById(R.id.img_sticker_preview);
        popupViewPreview.findViewById(R.id.sticker_guide).setVisibility(View.GONE);
        final StickerItem buzzSticker = EmoticonUtils.GENERAL_STICKER_DEFAULT[EmoticonUtils.BUZZ_STICKER_POSITION];
        ImageLoaderManager.getInstance(mApplicationController).displayStickerImage(imgPreview,
                buzzSticker.getCollectionId(), buzzSticker.getItemId());
        popupViewPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.sendVoiceStickerMessage(buzzSticker.getCollectionId(), buzzSticker.getItemId());
                hideBuzzSticker();
                resetShowBuzzCount();
            }
        });
        if (!popupWindowPreview.isShowing()) {
            imgPreview.post(new Runnable() {
                public void run() {
                    //   To avoid BadTokenException, you need to defer showing the popup until after all the lifecycle methods are called (-> activity window is displayed):
                    calculateOffsetXY();
                    popupWindowPreview.showAtLocation(mParentView, Gravity.NO_GRAVITY, offsetX, offsetY);
                }
            });
        }
        startCountDown();
        increaseShowBuzzCount();
    }*/

    private void increaseShowBuzzCount() {
        showBuzzCount = mPref.getInt(Constants.PREFERENCE.PREF_SHOW_BUZZ_COUNT, 0);
        showBuzzCount++;
        mPref.edit().putInt(Constants.PREFERENCE.PREF_SHOW_BUZZ_COUNT, showBuzzCount).apply();
    }

    private void resetShowBuzzCount() {
        mPref.edit().putInt(Constants.PREFERENCE.PREF_SHOW_BUZZ_COUNT, 0).apply();
    }

    //co the show popup buzz sticker hay ko
    private boolean canShowPopup(ThreadMessage threadMessage) {
//        long lastTimeShowBuzz = threadMessage.getLastTimeShowBuzz();
        int mThreadType = threadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            return false;
        }
        showBuzzCount = mPref.getInt(Constants.PREFERENCE.PREF_SHOW_BUZZ_COUNT, 0);
        if (showBuzzCount > 3) return false;
//        if (lastTimeShowBuzz == -1) {
//            //default --> chua show lan nao
//            return true;
//        } else {
//            long diffTime = System.currentTimeMillis() - lastTimeShowBuzz;
//            if (diffTime > SHOW_BUZZ_INTERVAL) {
//                return true;
//            }
//        }
        return false;
    }

    private void calculateOffsetXY() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        mApplicationController.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        int screenWidth = displaymetrics.widthPixels;
        int popupVoiceStickerWidth = (int) mApplicationController.getResources().getDimension(R.dimen.voice_sticker_size)
                + (int) mApplicationController.getResources().getDimension(R.dimen.margin_more_content_10);
        int emoHeight = (int) mApplicationController.getResources().getDimension(R.dimen.emoticon_icon_height) * 10 / 10;
        offsetX = (screenWidth - popupVoiceStickerWidth) / 2;
        offsetY = screenHeight - popupVoiceStickerWidth - emoHeight
                - (int) mApplicationController.getResources().getDimension(R.dimen.margin_more_content_5);
        Log.i(TAG, "screen screenHeight = " + screenHeight + " - screenWidth = " + screenWidth + " - emoHeight = "
                + emoHeight + " - popupVoiceStickerWidth = " + popupVoiceStickerWidth + " offsetXY = " + offsetX + "-" + offsetY);
    }

    //an buzz sticker khi stop, khi click
    public void hideBuzzSticker() {
//        if (popupWindowPreview != null && popupWindowPreview.isShowing()) {
//            popupWindowPreview.dismiss();
//        }
    }

    private void startCountDown() {
        mCountDownTimer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                hideBuzzSticker();
            }
        };
        mCountDownTimer.start();
    }

}

package com.metfone.selfcare.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;

import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.DeleteNotificationReceiver;
import com.metfone.selfcare.broadcast.ReplyNotificationReceiver;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.GroupAvatarHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;

/**
 * gộp các message của 1 thread vào trung 1 notification
 * Created by toanvk2 on 2/16/2017.
 */
public class MessageNotificationManager {
    private static final String TAG = MessageNotificationManager.class.getSimpleName();
    private static String KEY_REPLY = "reply_message";
    public static String REPLY_ACTION = "com.metfone.selfcare.app.REPLY_ACTION";
    private static final long[] vibrate = {300, 200, 100, 10};
    private static final long[] noVibraPattern = {0, 0, 0, 0};
    private static MessageNotificationManager mInstance;
    private ApplicationController mApplication;
    private SettingBusiness mSettingBusiness;
    private Bitmap largeAvatarDefault, largeAvatarMocha;
    private Resources mRes;
    private int avatarSize;
    private HashMap<ThreadMessage, ArrayList<ReengMessage>> mHashMapNotification;

    public static MessageNotificationManager getInstance(ApplicationController application) {
        if (mInstance == null) {
            synchronized (MessageNotificationManager.class) {
                if (mInstance == null) {
                    mInstance = new MessageNotificationManager(application);
                }
            }
        }
        return mInstance;
    }

    private MessageNotificationManager(ApplicationController application) {
        this.mApplication = application;
        this.mRes = mApplication.getResources();
        this.largeAvatarDefault = BitmapFactory.decodeResource(mRes, R.drawable.ic_avatar_default);
        this.largeAvatarMocha = BitmapFactory.decodeResource(mRes, R.drawable.ic_thread_mocha);
        this.mHashMapNotification = new HashMap<>();
        this.avatarSize = (int) mRes.getDimension(R.dimen.avatar_small_size);
    }

    private boolean isShowFullContent() {
        return mSettingBusiness.getPrefPreviewMsg() && mApplication.getAppLockManager().isShowFullNotification();
    }

    public void notifyNewMessage(ThreadMessage thread, ReengMessage message) {
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        if (thread == null || message == null) {
            return;
        }

        if (mSettingBusiness.checkExistInSilentThread(thread.getId())) {
            boolean hasTagMe = false;
            ArrayList<TagMocha> listTag = message.getListTagContent();
            if (listTag != null && !listTag.isEmpty()) {
                for (TagMocha tagMocha : listTag) {
                    if (mApplication.getReengAccountBusiness().getJidNumber().equals(tagMocha.getMsisdn())) {
                        hasTagMe = true;
                        break;
                    }
                }
            }
            if (!hasTagMe) return;
        }

        if (thread.getHiddenThread() == 1) {
            drawNotificationHiddenThread(thread, message);
            return;
        }

        ArrayList<ReengMessage> messages;
        if (mHashMapNotification.containsKey(thread)) {
            messages = mHashMapNotification.get(thread);
            messages.add(message);
        } else {
            messages = new ArrayList<>();
            messages.add(message);
            mHashMapNotification.put(thread, messages);
        }
        boolean isShowFullContent = isShowFullContent();
        Bitmap bitmap = mApplication.getAvatarBusiness().getBitmapFromCache(null, thread.getId());
        if (messages.size() > 1 && isShowFullContent) {
            if (bitmap != null)
                drawNotificationInboxStyle(thread, messages, bitmap);
            else {
                GroupAvatarHelper.GetBitmapAvatarGroupListener listener = new GroupAvatarHelper.GetBitmapAvatarGroupListener() {
                    @Override
                    public void onGetBitmapSuccess(Bitmap bitmap) {
                        drawNotificationInboxStyle(thread, messages, bitmap);
                    }
                };
                GroupAvatarHelper.getInstance(mApplication).loadBitmapThreadMessage(thread, listener);
            }
        } else {
            if (bitmap != null)
                drawNotificationBigTextStyle(thread, message, getAvatarBitmap(thread), isShowFullContent);
            else {
                GroupAvatarHelper.GetBitmapAvatarGroupListener listener = new GroupAvatarHelper.GetBitmapAvatarGroupListener() {
                    @Override
                    public void onGetBitmapSuccess(Bitmap bitmap) {
                        drawNotificationBigTextStyle(thread, message, getAvatarBitmap(thread), isShowFullContent);
                    }
                };
                GroupAvatarHelper.getInstance(mApplication).loadBitmapThreadMessage(thread, listener);
            }

        }
    }


    public int ID_HIDDEN_THREAD = 123456;

    private void drawNotificationHiddenThread(ThreadMessage thread, ReengMessage message) {


        if (!SettingBusiness.getInstance(mApplication).getPrefSettingOnNoti()) return;
        String lastContent = mRes.getString(R.string.you_have_new_msg);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();

        String title = mRes.getString(R.string.app_name);
        NotificationCompat.BigTextStyle notifyStyle = new NotificationCompat.BigTextStyle();
        notifyStyle.setBigContentTitle(title);
        notifyStyle.bigText(lastContent);
        String channelID = mApplication.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_HIDDEN_THREAD);
        //
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(mApplication, channelID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setLargeIcon(largeAvatarMocha)
                .setContentTitle(title)
                .setContentText(lastContent)
                .setTicker(title + ": " + lastContent)
                .setLights(Color.WHITE, 300, 1000)
                .setWhen(message.getTime())
                .setStyle(notifyStyle)
                .setSound(enableSound ? mSettingBusiness.getCurrentSoundUri() : null)
                .setVibrate(enableVibrate ? vibrate : noVibraPattern);

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelID = mApplication.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_HIDDEN_THREAD);
            notifyBuilder.setChannelId(channelID);
        }*/

        if (mApplication.isReady()) {

            Intent onClickIntent;
            ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
            if (accountBusiness.isValidAccount()) {
                onClickIntent = new Intent(mApplication, HomeActivity.class);
                onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                onClickIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            } else {
                onClickIntent = mApplication.getPackageManager()
                        .getLaunchIntentForPackage(mApplication.getPackageName());
                onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            Random r = new Random();
            int requestCode = r.nextInt(100) + 1000;
            PendingIntent pendingIntent = PendingIntent.getActivity(mApplication,
                    requestCode, onClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            notifyBuilder.setContentIntent(pendingIntent);

            Notification notification = notifyBuilder.build();
            notification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;

            mApplication.notifyWrapper(ID_HIDDEN_THREAD, notification, Constants.NOTIFICATION.NOTIFY_HIDDEN_THREAD);
        }
    }

    public void notifyNewReactionMessage(ThreadMessage thread, String fromJid) {
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        if (thread == null || TextUtils.isEmpty(fromJid) ||
                mSettingBusiness.checkExistInSilentThread(thread.getId())) {
            return;
        }

        Bitmap bitmap = mApplication.getAvatarBusiness().getBitmapFromCache(null, thread.getId());
        if (bitmap != null)
            drawNotificationReaction(thread, fromJid, bitmap);
        else {
            GroupAvatarHelper.GetBitmapAvatarGroupListener listener = new GroupAvatarHelper.GetBitmapAvatarGroupListener() {
                @Override
                public void onGetBitmapSuccess(Bitmap bitmap) {
                    drawNotificationReaction(thread, fromJid, bitmap);
                }
            };
            GroupAvatarHelper.getInstance(mApplication).loadBitmapThreadMessage(thread, listener);
        }
    }

    private void drawNotificationReaction(ThreadMessage threadMessage, String fromJid, Bitmap largeBitmap) {
        if (!SettingBusiness.getInstance(mApplication).getPrefSettingOnNoti()) return;
        String name;
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(fromJid);
        if (phoneNumber != null) {
            name = phoneNumber.getName();
        } else {
            NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(fromJid);
            if (nonContact != null) {
                name = nonContact.getNickName();
            } else {
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    name = fromJid;
                } else
                    name = threadMessage.getThreadName();
            }
        }
        if (TextUtils.isEmpty(name)) {
            StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(fromJid);
            if (strangerPhoneNumber != null) name = strangerPhoneNumber.getFriendName();
            else
                name = fromJid;
        }

        String lastContent = String.format(mRes.getString(R.string.react_message), name);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();

        String title = mRes.getString(R.string.app_name);
        NotificationCompat.BigTextStyle notifyStyle = new NotificationCompat.BigTextStyle();
        notifyStyle.setBigContentTitle(title);
        notifyStyle.bigText(lastContent);
        //
        String channelID = mApplication.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_MESSAGE);
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(mApplication, channelID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setLargeIcon(largeBitmap)
                .setContentTitle(title)
                .setContentText(lastContent)
                .setNumber(0)
                .setTicker(title + ": " + lastContent)
                .setLights(Color.WHITE, 300, 1000)
                .setWhen(System.currentTimeMillis())
                .setStyle(notifyStyle)
                .setSound(enableSound ? mSettingBusiness.getCurrentSoundUri() : null)
                .setVibrate(enableVibrate ? vibrate : noVibraPattern);

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelID = mApplication.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_MESSAGE);
            notifyBuilder.setChannelId(channelID);
        }*/

        if (Version.hasLollipop()) {
            notifyBuilder.addAction(getReplyAction(threadMessage.getId(), threadMessage));
        }
        if (mApplication.isReady()) {
            mApplication.notifyWrapper(threadMessage.getId(), configNotification(notifyBuilder,
                    threadMessage, false), Constants.NOTIFICATION.NOTIFY_MESSAGE);
        }
    }

    private void checkRefreshNotifyWhenLoadedAvatar(ThreadMessage thread, Bitmap avatarBitmap) {
        if (thread == null || avatarBitmap == null) return;
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        if (mSettingBusiness.checkExistInSilentThread(thread.getId())) {
            return;
        }
        if (mHashMapNotification.containsKey(thread)) {
            ArrayList<ReengMessage> messages = mHashMapNotification.get(thread);
            if (messages.isEmpty()) {
                return;
            }
            boolean isShowFullContent = isShowFullContent();
            if (messages.size() > 1 && isShowFullContent) {
                drawNotificationInboxStyle(thread, messages, avatarBitmap);
            } else {
                drawNotificationBigTextStyle(thread, messages.get(0), avatarBitmap, isShowFullContent);
            }
        }
    }

    /**
     * xóa notification khi xóa 1 thread, hoặc khi vào 1 thread xem chi tiết
     *
     * @param thread
     */
    public void clearNotifyThreadMessage(ThreadMessage thread) {
        if (thread == null) return;
        mHashMapNotification.remove(thread);
//        if (IMService.isReady()) {
//            IMService.getInstance().cancelNotification(thread.getId());
//        }
        if (mApplication.isReady()) {
            mApplication.cancelNotification(thread.getId());
            if (thread.getHiddenThread() == 1) {
                mApplication.cancelNotification(ID_HIDDEN_THREAD);
            }
        }
    }

    /**
     * xóa notification khi xóa thread chat
     *
     * @param threadMessages
     */
    public void clearNotifyThreadMessages(ArrayList<ThreadMessage> threadMessages) {
        if (threadMessages == null) return;
        for (ThreadMessage thread : threadMessages) {
            mHashMapNotification.remove(thread);
//            if (IMService.isReady()) {
//                IMService.getInstance().cancelNotification(thread.getId());
//            }
            if (mApplication.isReady()) {
                mApplication.cancelNotification(thread.getId());
            }
        }
    }

    /**
     * xoa data trong hashmap khi nhan dc xự kiên xóa notification từ người dùng
     *
     * @param threadId
     */
    public void deleteNotifyMessage(int threadId) {
        ThreadMessage thread = mApplication.getMessageBusiness().findThreadByThreadId(threadId);
        if (thread != null) {
            mHashMapNotification.remove(thread);
        }
    }

    /**
     * notification với thread co 1 message
     */
    private void drawNotificationBigTextStyle(ThreadMessage threadMessage, ReengMessage lastMessage
            , Bitmap largeBitmap, boolean isFullContent) {
        if (!SettingBusiness.getInstance(mApplication).getPrefSettingOnNoti()) return;
        String lastContent;
        if (isFullContent) {
            lastContent = getContentLastMessage(threadMessage, lastMessage, true);
        } else if (threadMessage.getNumOfUnreadMessage() >= 1) {
            lastContent = String.format(mRes.getString(R.string.have_some_new_message),
                    threadMessage.getNumOfUnreadMessage());
        } else {
            lastContent = mRes.getString(R.string.have_new_message);
        }
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();

        String title = threadMessage.getThreadName();
        NotificationCompat.BigTextStyle notifyStyle = new NotificationCompat.BigTextStyle();
        notifyStyle.setBigContentTitle(title);
        notifyStyle.bigText(lastContent);
        //
        String channelID = mApplication.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_MESSAGE);
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(mApplication, channelID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setLargeIcon(largeBitmap)
                .setContentTitle(title)
                .setContentText(lastContent)
                .setNumber(threadMessage.getNumOfUnreadMessage())
                .setTicker(title + ": " + lastContent)
                .setLights(Color.WHITE, 300, 1000)
                .setWhen(lastMessage.getTime())
                .setStyle(notifyStyle)
                .setSound(enableSound ? mSettingBusiness.getCurrentSoundUri() : null)
                .setVibrate(enableVibrate ? vibrate : noVibraPattern);

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelID = mApplication.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_MESSAGE);
            notifyBuilder.setChannelId(channelID);
        }*/

        if (Version.hasLollipop()) {
            notifyBuilder.addAction(getReplyAction(threadMessage.getId(), threadMessage));
        }

//        if (IMService.isReady()) {
//            IMService.getInstance().notifyWrapper(threadMessage.getId(), configNotification(notifyBuilder,
//                    threadMessage, false));
//        }
        if (mApplication.isReady()) {
            mApplication.notifyWrapper(threadMessage.getId(), configNotification(notifyBuilder,
                    threadMessage, false), Constants.NOTIFICATION.NOTIFY_MESSAGE);
        }
    }

    /**
     * notification với thread có nhiều message
     */
    private void drawNotificationInboxStyle(ThreadMessage threadMessage, ArrayList<ReengMessage> messages, Bitmap
            largeBitmap) {
        if (!SettingBusiness.getInstance(mApplication).getPrefSettingOnNoti()) return;
        NotificationCompat.InboxStyle notifyStyle = new NotificationCompat.InboxStyle();
        int numberUnread = threadMessage.getNumOfUnreadMessage();
        int size = messages.size();
        int start = 0;
        if (size > 6) {
            start = size - 6;
        }
        for (int i = start; i < size; i++) {
            notifyStyle.addLine(getContentLastMessage(threadMessage, messages.get(i), true));
        }

        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();

        String lastContent = getContentLastMessage(threadMessage, messages.get(size - 1), true);
        long lastTime = messages.get(size - 1).getTime();
        String title = threadMessage.getThreadName();
        String summaryText = String.format(mRes.getString(R.string.have_some_new_message), numberUnread);
        notifyStyle.setSummaryText(summaryText);
        String channelID = mApplication.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_MESSAGE);
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(mApplication, channelID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setLargeIcon(largeBitmap)
                .setNumber(numberUnread)
                .setContentTitle(title)
                .setContentText(lastContent)
                .setLights(Color.WHITE, 300, 1000)
                .setWhen(lastTime)
                .setStyle(notifyStyle)
                .setSound(enableSound ? mSettingBusiness.getCurrentSoundUri() : null)
                .setVibrate(enableVibrate ? vibrate : noVibraPattern);

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            notifyBuilder.setChannelId(Constants.NOTIFICATION.NOTIFY_MESSAGE + "");
            String channelID = mApplication.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_MESSAGE);
            notifyBuilder.setChannelId(channelID);
        }*/

        if (Version.hasLollipop()) {
            notifyBuilder.addAction(getReplyAction(threadMessage.getId(), threadMessage));
        }
//        if (IMService.isReady()) {
//            IMService.getInstance().notifyWrapper(threadMessage.getId(), configNotification(notifyBuilder,
//                    threadMessage, true));
//        }
        if (mApplication.isReady()) {
            mApplication.notifyWrapper(threadMessage.getId(), configNotification(notifyBuilder,
                    threadMessage, true), Constants.NOTIFICATION.NOTIFY_MESSAGE);
        }
    }

    private Notification configNotification(NotificationCompat.Builder notifyBuilder, ThreadMessage threadMessage,
                                            boolean isMulti) {
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        Intent onClickIntent = new Intent(mApplication, ChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ThreadMessageConstant.THREAD_ID, threadMessage.getId());
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, threadMessage.getThreadType());
        onClickIntent.putExtras(bundle);
        onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        onClickIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(mApplication, threadMessage.getId(), onClickIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        notifyBuilder.setContentIntent(pendingIntent);
        //
        Intent intent = new Intent(mApplication, DeleteNotificationReceiver.class);
        intent.putExtra(ThreadMessageConstant.THREAD_ID, threadMessage.getId());
        PendingIntent deleteIntent = PendingIntent.getBroadcast(mApplication, threadMessage.getId(), intent, 0);
        notifyBuilder.setDeleteIntent(deleteIntent);
        if (enableSound) {
            notifyBuilder.setSound(mSettingBusiness.getCurrentSoundUri());
        } else {
            notifyBuilder.setSound(null);
        }
        if (enableVibrate) {
            notifyBuilder.setVibrate(vibrate);
        } else {
            notifyBuilder.setVibrate(noVibraPattern);
        }
        if (enableSound && enableVibrate) {//!isMulti &&
            notifyBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        } else {
            notifyBuilder.setPriority(NotificationCompat.PRIORITY_LOW);
        }
        //
        Notification notification = notifyBuilder.build();
        notification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        return notification;
    }

    private Bitmap getAvatarBitmap(final ThreadMessage threadMessage) {
        String avatarUrl = null;
        String groupAvatarFile = null;
        int threadType = threadMessage.getThreadType();
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String friendNumber = threadMessage.getSoloNumber();
            avatarUrl = getFriendAvatarUrl(avatarBusiness, friendNumber);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            if (TextUtils.isEmpty(threadMessage.getGroupAvatar())) {
//                groupAvatarFile = ImageHelper.getAvatarGroupFilePath(threadMessage.getId());
                Log.f(TAG, "gen avatar bitmap start");
                Bitmap bm = GroupAvatarHelper.getInstance(mApplication).genAvatarGroup(threadMessage, new GroupAvatarHelper.GetBitmapAvatarGroupListener() {
                    @Override
                    public void onGetBitmapSuccess(Bitmap bitmap) {
                        Log.f(TAG, "onGetBitmapSuccess");
                        checkRefreshNotifyWhenLoadedAvatar(threadMessage, bitmap);
                    }
                });
                Log.f(TAG, "gen avatar bitmap done");
                return bm;
            } else {
                avatarUrl = avatarBusiness.getGroupAvatarUrl(threadMessage, threadMessage.getGroupAvatar());
            }


        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            avatarUrl = mApplication.getOfficerBusiness().getOfficerAvatarByServerId(threadMessage.getServerId());
        }
        // get bitmap
        if (TextUtils.isEmpty(avatarUrl)) {
            if (TextUtils.isEmpty(groupAvatarFile)) {
                if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    return largeAvatarDefault;
                }
            } else {
                try {
                    return BitmapFactory.decodeFile(groupAvatarFile);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
        } else {
            Bitmap avatarCache = avatarBusiness.getBitmapFromCache(avatarUrl, threadMessage.getId());
            if (avatarCache != null) {
                return avatarCache;
            } else {
                avatarBusiness.loadAvatar(avatarUrl, new GlideImageLoader.ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted() {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, GlideException e) {

                    }

                    @Override
                    public void onLoadingComplete(Bitmap loadedImage) {
                        GroupAvatarHelper.getInstance(mApplication).addBitmapCache(threadMessage.getId(), loadedImage);
                        checkRefreshNotifyWhenLoadedAvatar(threadMessage, loadedImage);
                    }
                }, avatarSize);
            }
        }
        return largeAvatarMocha;
    }

    private String getFriendAvatarUrl(AvatarBusiness avatarBusiness, String friendNumber) {
        String avatarUrl = null;
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendNumber);
        if (phoneNumber != null && phoneNumber.getId() != null) {
            avatarUrl = avatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), friendNumber, avatarSize);
        } else {
            NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(friendNumber);
            if (nonContact != null) {
                avatarUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), friendNumber, avatarSize);
            } else {
                StrangerPhoneNumber strangerPhoneNumber = mApplication.
                        getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(friendNumber);
                if (strangerPhoneNumber != null && strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber
                        .StrangerType.other_app_stranger) {
                    avatarUrl = strangerPhoneNumber.getFriendAvatarUrl();
                } else if (strangerPhoneNumber != null) {
                    avatarUrl = avatarBusiness.getAvatarUrl(strangerPhoneNumber.getFriendAvatarUrl(), friendNumber,
                            avatarSize);
                }
            }
        }
        return avatarUrl;
    }

    private String getContentLastMessage(ThreadMessage threadMessage, ReengMessage lastMessage) {
        return getContentLastMessage(threadMessage, lastMessage, false);
    }

    private String getContentLastMessage(ThreadMessage threadMessage, ReengMessage lastMessage, boolean isNotify) {
        String lastContent = "";
        if (lastMessage.getMessageType() == ReengMessageConstant.MessageType.lixi) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                lastContent = String.format(mRes.getString(R.string.notify_lixi_group_chat),
                        mApplication.getMessageBusiness().getFriendNameOfGroup(lastMessage.getSender()),
                        threadMessage.getThreadName());
            } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                lastContent = String.format(mRes.getString(R.string.notify_lixi_solo_chat), threadMessage.getThreadName());
            } else {
                lastContent = mApplication.getMessageBusiness().getContentOfMessage(lastMessage, mRes, mApplication);
            }
        } else {
            if (isNotify && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                ArrayList<TagMocha> listTag = lastMessage.getListTagContent();
                if (listTag != null && !listTag.isEmpty()) {
                    for (TagMocha tagMocha : listTag) {
                        if (mApplication.getReengAccountBusiness().getJidNumber().equals(tagMocha.getMsisdn())) {
                            return String.format(mRes.getString(R.string.mention_you_in_msg), mApplication.getMessageBusiness().getFriendNameOfGroup(lastMessage.getSender()));
                        }
                    }
                }
            }

            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT &&
                    lastMessage.getMessageType() != ReengMessageConstant.MessageType.notification) {
                lastContent = mApplication.getMessageBusiness().getFriendNameOfGroup(lastMessage.getSender()) + ": ";
            }
            lastContent += mApplication.getMessageBusiness().getContentOfMessage(lastMessage, mRes, mApplication);
        }
        return lastContent;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    private NotificationCompat.Action getReplyAction(int notifyId, ThreadMessage thread) {
        String replyLabel = mRes.getString(R.string.reply);
        RemoteInput remoteInput = new RemoteInput.Builder(KEY_REPLY)
                .setLabel(replyLabel)
                .build();
        return new NotificationCompat.Action
                .Builder(R.drawable.ic_notif_reply, replyLabel, getReplyPendingIntent(notifyId, thread))
                .addRemoteInput(remoteInput)
                .setAllowGeneratedReplies(true)
                .build();
    }

    private PendingIntent getReplyPendingIntent(int notifyId, ThreadMessage thread) {
        Log.d(TAG, "ReplyNotificationReceiver getReplyPendingIntent: " + thread.getId());
        if (Version.hasN()) {
            Intent intent = ReplyNotificationReceiver.getReplyMessageIntent(mApplication, notifyId, thread.getId());
            return PendingIntent.getBroadcast(mApplication, thread.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            Intent intent = new Intent(mApplication, ChatActivity.class);
            intent.setAction(REPLY_ACTION);
            intent.putExtra(ThreadMessageConstant.THREAD_ID, thread.getId());
            intent.putExtra(ThreadMessageConstant.THREAD_IS_GROUP, thread.getThreadType());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            return PendingIntent.getActivity(mApplication, thread.getId(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        }
    }

    public static CharSequence getReplyMessage(Intent intent) {
        if (Version.hasLollipop()) {
            Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
            if (remoteInput != null) {
                return remoteInput.getCharSequence(KEY_REPLY);
            }
        }
        return null;
    }

    public void clearAllNotify() {
        for (Map.Entry<ThreadMessage, ArrayList<ReengMessage>> entry : mHashMapNotification.entrySet()) {
            ThreadMessage key = entry.getKey();
            mApplication.cancelNotification(key.getId());
        }


    }
}
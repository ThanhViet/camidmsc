package com.metfone.selfcare.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.activity.ContactDetailActivity;
import com.metfone.selfcare.activity.ContactListActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.MochaCallActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.activity.ProfileActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.broadcast.DeclineCallReceiver;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.helper.call.CallConstant;
import com.metfone.selfcare.helper.home.TabHomeHelper.HomeTab;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.tab_home.event.TabHomeEvent;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.model.CallData;

import java.io.File;
import java.util.Calendar;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.metfone.selfcare.helper.Constants.NOTIFICATION.NOTIFY_CALL;
import static com.metfone.selfcare.helper.Constants.NOTIFICATION.NOTIFY_CALL_HEAD_UP;
import static com.metfone.selfcare.helper.Constants.NOTIFICATION.NOTIFY_DEEPLINK;
import static com.metfone.selfcare.helper.Constants.NOTIFICATION.NOTIFY_MOCHA_VIDEO;
import static com.metfone.selfcare.helper.Constants.NOTIFICATION.NOTIFY_MOVIE;
import static com.metfone.selfcare.helper.Constants.NOTIFICATION.NOTIFY_MUSIC;
import static com.metfone.selfcare.helper.Constants.NOTIFICATION.NOTIFY_NEWS;
import static com.metfone.selfcare.util.Utilities.getResizedBitmap;
import static com.metfone.selfcare.util.Utilities.getRoundedCornerBitmap;
import static com.metfone.selfcare.util.Utilities.isNotShowActivityCall;

//import com.metfone.selfcare.ui.tabvideo.activity.videoDetailLoading.VideoDetailLoadingActivity;

/**
 * Created by thaodv on 7/16/2014.
 */
public class ReengNotificationManager {
    public static final int AVATAR_SOLO = 0;
    public static final int AVATAR_OFFICER = 1;
    public static final int AVATAR_GROUP = 2;
    public static final int AVATAR_MULTI_THREAD = 3;
    public static final int AVATAR_FAKE_OFFICER = 4;// dung cho notification suggest mocha. avatar mocha nhung click
    // vào thread 1-1
    public static final int NOTIFY_NO_NUMBER = -1;
    private static final String TAG = ReengNotificationManager.class.getSimpleName();
    private static int currentUIThreadId = -1;
    private static ReengNotificationManager instance;
    private ApplicationController mApplicationController;
    private MessageBusiness mMessageBusiness;
    private SettingBusiness mSettingBusiness;
    private SharedPreferences mPref;
    private Resources mRes;
    private boolean isShowingHeadUpCallNotification;

    private ReengNotificationManager(ApplicationController app) {
        Log.i(TAG, "constructor");
        mApplicationController = app;
        mRes = mApplicationController.getResources();

        largeAvatarDefault = BitmapFactory.decodeResource(mApplicationController.getResources(), R.drawable.ic_avatar_default);
        smallIconID = R.mipmap.ic_launcher;
        avatarSize = (int) mApplicationController.getResources().getDimension(R.dimen.avatar_small_size);
        largeAvatarMocha = BitmapFactory.decodeResource(mRes, R.drawable.ic_launcher);

        getBusiness(app);
        mPref = mApplicationController.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    private void getBusiness(ApplicationController app) {
        mMessageBusiness = app.getMessageBusiness();
        mSettingBusiness = SettingBusiness.getInstance(app);
    }

    public static int getCurrentUIThreadId() {
        return currentUIThreadId;
    }

    public static void setCurrentUIThreadId(int currentUIThreadId) {
        ReengNotificationManager.currentUIThreadId = currentUIThreadId;
    }

    public synchronized static ReengNotificationManager getInstance(ApplicationController app) {
        if (instance == null) {
            synchronized (ReengNotificationManager.class) {
                if (instance == null) {
                    instance = new ReengNotificationManager(app);
                }
            }
        }
        return instance;
    }

    /**
     * clear notification of messages
     *
     * @author thaodv
     */
//    public static void clearMessagesNotification(Context context, int idNotify) {
//        if (IMService.isReady()) {
//            IMService.getInstance().cancelNotification(idNotify);
//        }
//    }
    public void drawPresenceNotification(ApplicationController application, PhoneNumber phoneNumber, String msg) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        ApplicationStateManager applicationStateManager = mApplicationController.getAppStateManager();
        //        ap o background khong push
        if (applicationStateManager.isAppWentToBg()) {
            return;
        }
//        if (!IMService.isReady() || phoneNumber == null) {
        if (!application.isReady() || phoneNumber == null) {
            return;
        }
        ThreadMessage threadMessage = mMessageBusiness.findExistingSoloThread(phoneNumber.getJidNumber());
        if (threadMessage != null) {
            if (mSettingBusiness.checkExistInSilentThread(threadMessage.getId())) {
                //ko thong bao j
                return;
            }
        }
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        String title = phoneNumber.getName();
        Intent onClickIntent;
        // intent click
        onClickIntent = new Intent(application, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT_NOTIFICATION);
        bundle.putString("contact_notification_number", phoneNumber.getJidNumber());
        onClickIntent.putExtras(bundle);
//        IMService.getInstance().setParamsNotify(onClickIntent, title,
//                msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                phoneNumber.getJidNumber(), AVATAR_SOLO, -1, true, null, true, null,
//                Constants.NOTIFICATION.NOTIFY_OTHER);
        setParamsNotify(onClickIntent, title,
                msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                phoneNumber.getJidNumber(), AVATAR_SOLO, -1, true, null, true, null,
                Constants.NOTIFICATION.NOTIFY_OTHER, Constants.NOTIFICATION.NOTIFY_OTHER);
    }

    /**
     * notifi tin nhan moi
     */
    public void drawFakeMessagesNotification(ApplicationController application, ThreadMessage threadMessage,
                                             ReengMessage newMessage, String title, String msg) {
//        if (threadMessage == null) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, HomeActivity.class);
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
//        if (IMService.isReady()) {
        if (mApplicationController.isReady()) {
            Log.i(TAG, "drawFakeMessagesNotification null thread");
//            IMService.getInstance().setParamsNotify(onClickIntent, title,
//                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                    null, AVATAR_SOLO, -1, true, null, true, null,
//                    Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG);
            setParamsNotify(onClickIntent, title,
                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                    null, AVATAR_SOLO, -1, true, null, true, null,
                    Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG, Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG);
        }
        /*} else {
            //TODO test notify luật mới (ko dung nua)
            Log.i(TAG, "drawFakeMessagesNotification normal thread");
            if (currentUIThreadId != threadMessage.getId()) {
                MessageNotificationManager.getInstance(application).notifyNewMessage(threadMessage,
                        newMessage);
            }
        }*/
    }

    public void drawMessagesNotification(ApplicationController applicationController, ThreadMessage threadMessage,
                                         int threadType,
                                         ReengMessage newMessage) {
        if (!SettingBusiness.getInstance(applicationController).getPrefSettingOnNoti()) return;
        //TODO test notify luật mới
        if (currentUIThreadId != threadMessage.getId()) {
            MessageNotificationManager.getInstance(applicationController).notifyNewMessage(threadMessage, newMessage);
        }
    }

    public void drawMessagesReactionNotification(ApplicationController applicationController, ThreadMessage threadMessage,
                                                 String fromJid) {
        if (!SettingBusiness.getInstance(applicationController).getPrefSettingOnNoti()) return;
        //TODO test notify luật mới
        if (currentUIThreadId != threadMessage.getId()) {
            MessageNotificationManager.getInstance(applicationController).notifyNewReactionMessage(threadMessage, fromJid);
        }
    }

    /**
     * notification suggest sms
     *
     * @param application
     * @param friendJid
     * @param msg
     */
    public void drawSuggestSmsNotification(ApplicationController application, String friendJid, String msg) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        Log.d(TAG, "drawSuggestSmsNotification - friendJid: " + friendJid + " ,msg: " + msg);
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        String title = mRes.getString(R.string.app_name);
        Intent onClickIntent = new Intent(application, ChatActivity.class);
        Bundle bundle = new Bundle();// truyen vao so dien thoai, neu xoa thread thi taoj moi thread do
        bundle.putInt(ThreadMessageConstant.THREAD_ID, -1);
        bundle.putString(Constants.MESSAGE.NUMBER, friendJid);
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT);
        onClickIntent.putExtras(bundle);
        if (mApplicationController.isReady()) {
//        if (IMService.isReady()) {
//            IMService.getInstance().setParamsNotify(onClickIntent, title,
//                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                    friendJid, AVATAR_FAKE_OFFICER, -1, true, null, true, null,
//                    Constants.NOTIFICATION.NOTIFY_MESSAGE);
            setParamsNotify(onClickIntent, title,
                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                    friendJid, AVATAR_FAKE_OFFICER, -1, true, null, true, null,
                    Constants.NOTIFICATION.NOTIFY_MESSAGE, Constants.NOTIFICATION.NOTIFY_MESSAGE);
        }
    }

    /**
     * notification nguoi dung moi
     *
     * @param application
     * @param phoneNumber
     * @param newUser
     * @param msg
     */
    public void drawNewUserNotification(ApplicationController application, PhoneNumber phoneNumber, String newUser,
                                        String msg) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, ContactDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
            bundle.putString(NumberConstant.ID, phoneNumber.getId());
            onClickIntent.putExtras(bundle);
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        if (mApplicationController.isReady()) {
//        if (IMService.isReady()) {
//            IMService.getInstance().setParamsNotify(onClickIntent, mRes.getString(R.string.app_name),
//                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                    phoneNumber.getJidNumber(), AVATAR_SOLO, -1, true, null, true, null,
//                    Constants.NOTIFICATION.NOTIFY_OTHER);
            setParamsNotify(onClickIntent, mRes.getString(R.string.app_name),
                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                    phoneNumber.getJidNumber(), AVATAR_SOLO, -1, true, null, true, null,
                    Constants.NOTIFICATION.NOTIFY_OTHER, Constants.NOTIFICATION.NOTIFY_OTHER);
        }
    }

    /**
     * notifi onmedia
     *
     * @param application
     * @param msg
     */
    public void drawOnMediaNotification(ApplicationController application, String msg,
                                        String jidNumber, String rowId, String action,
                                        String urlParent, String urlSubCmt, boolean isReply, String avatarUrl) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        String title = mRes.getString(R.string.app_name);
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, OnMediaActivityNew.class);
            Bundle bundle = new Bundle();
            if (isReply) {
                bundle.putInt(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.REPLY_COMMENT);
                bundle.putString(Constants.ONMEDIA.EXTRAS_URL_SUB_COMMENT, urlSubCmt);
                bundle.putString(Constants.ONMEDIA.EXTRAS_DATA, urlParent);
                bundle.putString(Constants.ONMEDIA.EXTRAS_ROW_ID, rowId);
            } else {
                bundle.putInt(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
                bundle.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, true);
                bundle.putString(Constants.ONMEDIA.EXTRAS_ROW_ID, rowId);
                bundle.putString(Constants.ONMEDIA.EXTRAS_ACTION, action);
            }
            bundle.putInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_FROM_PUSH);
            onClickIntent.putExtras(bundle);
            mPref.edit().putBoolean(Constants.PREFERENCE.PREF_HAD_NOTIFY_TAB_HOT, true).apply();
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        if (mApplicationController.isReady()) {
//        if (IMService.isReady()) {
//            IMService.getInstance().setParamsNotify(onClickIntent, title,
//                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                    jidNumber, AVATAR_SOLO, -1, true, null, true, null,
//                    Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            setParamsNotify(onClickIntent, title,
                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                    jidNumber, AVATAR_SOLO, -1, true, null, true, null,
                    Constants.NOTIFICATION.NOTIFY_ONMEDIA, Constants.NOTIFICATION.NOTIFY_ONMEDIA, avatarUrl);
            EventBus.getDefault().post(new TabHomeEvent().setUpdateNotify(true));
        }
    }

    public void drawOnMediaNotificationStranger(ApplicationController application, String msg,
                                                String name, String rowId) {
        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, OnMediaActivityNew.class);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
            bundle.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, true);
            bundle.putString(Constants.ONMEDIA.EXTRAS_ROW_ID, rowId);
            bundle.putInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_FROM_PUSH);
            onClickIntent.putExtras(bundle);
            mPref.edit().putBoolean(Constants.PREFERENCE.PREF_HAD_NOTIFY_TAB_HOT, true).apply();
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        if (mApplicationController.isReady()) {
//        if (IMService.isReady()) {
//            IMService.getInstance().setParamsNotify(onClickIntent, name,
//                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                    "", AVATAR_SOLO, -1, true, null, true, null,
//                    Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            setParamsNotify(onClickIntent, name,
                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                    "", AVATAR_SOLO, -1, true, null, true, null,
                    Constants.NOTIFICATION.NOTIFY_ONMEDIA, Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            EventBus.getDefault().post(new TabHomeEvent().setUpdateNotify(true));
        }
    }

    public void drawOnMediaNotificationShowTab(ApplicationController application, String msg) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        String title = mRes.getString(R.string.app_name);
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, HomeActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.HOME.TAB_ENUM, HomeTab.tab_hot.toString());
            bundle.putBoolean(Constants.HOME.NEED_SHOW_NOTIFY, true);
            onClickIntent.putExtras(bundle);
            mPref.edit().putBoolean(Constants.PREFERENCE.PREF_HAD_NOTIFY_TAB_HOT, true).apply();
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
//        if (IMService.isReady()) {
        if (mApplicationController.isReady()) {
            //----------------------------- ------------------------------------------------
//            IMService.getInstance().setParamsNotify(onClickIntent, title,
//                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                    "", AVATAR_SOLO, -1, true, null, true, null,
//                    Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            setParamsNotify(onClickIntent, title,
                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                    "", AVATAR_SOLO, -1, true, null, true, null,
                    Constants.NOTIFICATION.NOTIFY_ONMEDIA, Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            EventBus.getDefault().post(new TabHomeEvent().setUpdateNotify(true));
        }
    }

    public void drawNotifyPreviewImage(final String title, final String content, String urlImage, String openUrl) {
        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        if (!Version.hasJellyBean() || TextUtils.isEmpty(openUrl) ||
                TextUtils.isEmpty(urlImage) || TextUtils.isEmpty(title) ||
                TextUtils.isEmpty(content)) {
            Log.d(TAG, "drawNotifyPreviewImage : content error");
            return;
        }
        Uri uri = UrlConfigHelper.getInstance(mApplicationController).getUriWebView(openUrl);
        if (uri == null) {
            return;
        }
        final Intent clickIntent = new Intent(Intent.ACTION_VIEW, uri);
        AvatarBusiness avatarBusiness = mApplicationController.getAvatarBusiness();
        Bitmap imageCache = avatarBusiness.getBitmapFromCache(urlImage, currentThreadId);
        if (imageCache != null) {
//            IMService.getInstance().displayNotifyPreviewImage(clickIntent, imageCache, title, content, title, content);
            displayNotifyPreviewImage(clickIntent, imageCache, title, content, title, content);
        } else {
            GlideImageLoader.ImageLoadingListener loadingImageListener = new GlideImageLoader.ImageLoadingListener() {

                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingFailed(String imageUri, GlideException e) {

                }

                @Override
                public void onLoadingComplete(Bitmap loadedImage) {
                    if (loadedImage != null) {
//                        IMService.getInstance().displayNotifyPreviewImage(clickIntent, loadedImage, title, content,
//                                title, content);
                        displayNotifyPreviewImage(clickIntent, loadedImage, title, content,
                                title, content);
                    }
                }
            };
            avatarBusiness.loadAvatar(urlImage, loadingImageListener, 600);
        }
    }


    public void drawMochaVideoNotification(ApplicationController application, String msg,
                                           String name, String urlVideo, String avatar) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, VideoPlayerActivity.class);
            Bundle bundle = new Bundle();
            Video video = new Video();
            video.setLink(urlVideo);
            bundle.putSerializable(Constants.TabVideo.VIDEO, video);
            onClickIntent.putExtras(bundle);
        } else {
            onClickIntent = application.getPackageManager().getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
//        if (IMService.isReady()) {
        if (mApplicationController.isReady()) {
//            IMService.getInstance().drawNotifyWithAvatar(onClickIntent, name, msg, avatar,
//                    NOTIFY_NO_NUMBER, enableSound, enableVibrate, -1, true,
//                    Constants.NOTIFICATION.NOTIFY_MOCHA_VIDEO);

            drawNotifyWithAvatar(onClickIntent, name, msg, avatar,
                    NOTIFY_NO_NUMBER, enableSound, enableVibrate, -1, true,
                    Constants.NOTIFICATION.NOTIFY_MOCHA_VIDEO);
        }
    }

    public static int TYPE_MUSIC = 0;
    public static int TYPE_MOVIE = 1;
    public static int TYPE_NEWS = 2;
    public static int TYPE_VIDEO = 3;
    public static int TYPE_DEEP_LINK = 4;

    public void drawSuperAppNotification(ApplicationController application, String msg,
                                         String name, String url, String avatar, int type, String bigImageUrl, String campId) {
        Log.i(TAG, "drawSuperAppNotification: " + url + " type: " + type);
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        Intent onClickIntent;
        int typeNotify = NOTIFY_MUSIC;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, HomeActivity.class);
            if (type == TYPE_MUSIC) {
                onClickIntent.setData(Uri.parse("mocha://musickeeng?ref=" + url + "&" + DeepLinkHelper.DEEP_LINK.PARAM_CAPM_ID + "=" + campId));
            } else if (type == TYPE_MOVIE) {
                /*onClickIntent = new Intent(application, VideoPlayerActivity.class);
                Bundle bundle = new Bundle();
                Video video = new Video();
                video.setLink(url);
                bundle.putSerializable(Constants.TabVideo.VIDEO, video);
                onClickIntent.putExtras(bundle);*/
                onClickIntent.setData(Uri.parse("mocha://mochavideo?ref=" + url + "&" + DeepLinkHelper.DEEP_LINK.PARAM_CAPM_ID + "=" + campId));
                typeNotify = NOTIFY_MOVIE;
            } else if (type == TYPE_NEWS) {
                onClickIntent.setData(Uri.parse("mocha://netnews?ref=" + url + "&" + DeepLinkHelper.DEEP_LINK.PARAM_CAPM_ID + "=" + campId));
                typeNotify = NOTIFY_NEWS;
            } else if (type == TYPE_VIDEO) {
                /*onClickIntent = new Intent(application, VideoPlayerActivity.class);
                Bundle bundle = new Bundle();
                Video video = new Video();
                video.setLink(url);
                bundle.putSerializable(Constants.TabVideo.VIDEO, video);
                onClickIntent.putExtras(bundle);*/
                onClickIntent.setData(Uri.parse("mocha://mochavideo?ref=" + url + "&" + DeepLinkHelper.DEEP_LINK.PARAM_CAPM_ID + "=" + campId));
                typeNotify = NOTIFY_MOCHA_VIDEO;
            } else if (type == TYPE_DEEP_LINK) {
                if (url.contains("?")) {
                    onClickIntent.setData(Uri.parse(url + "&" + DeepLinkHelper.DEEP_LINK.PARAM_CAPM_ID_DEEPLINK + "=" + campId));
                } else {
                    onClickIntent.setData(Uri.parse(url + "?" + DeepLinkHelper.DEEP_LINK.PARAM_CAPM_ID_DEEPLINK + "=" + campId));
                }
                typeNotify = NOTIFY_DEEPLINK;
            }


            Bundle bundle = new Bundle();
            bundle.putBoolean("from_notify", true);
            bundle.putInt("type_notify", typeNotify);
            bundle.putString("name_new", name);
            onClickIntent.putExtras(bundle);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            onClickIntent.setAction(Intent.ACTION_VIEW);
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        if (mApplicationController.isReady()) {
            /*drawNotifyWithAvatar(onClickIntent, name, msg, avatar,
                    NOTIFY_NO_NUMBER, enableSound, enableVibrate, -1, true,
                    typeNotify);*/

            loadBitmapBigPicture(bigImageUrl, avatar, onClickIntent, name, msg, typeNotify);
            EventBus.getDefault().post(new TabHomeEvent().setUpdateNotify(true));
        }
    }

    public void drawSuperAppNotificationCamId(ApplicationController application, String msg, String type, String actionType, String objectId,int isExchange,
                                              String name, String url, String avatar, String bigImageUrl) {
        Log.i(TAG, "drawSuperAppNotification: " + url + " type: " + type);
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        Intent onClickIntent;
        int typeNotify = NOTIFY_MUSIC;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if ((type != null && type.equals("action_type") && actionType != null) || "link".equals(type)) {
            onClickIntent = new Intent(application, HomeActivity.class);
            if ("link".equals(type)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_LINK + "?link=" + url));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_PROFILE)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_FILM_PROFILE + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_ACTOR)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_FILM_CATEGORY + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_DIRECTOR)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_FILM_PROFILE + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_CATEGORY)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_FILM_PROFILE + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.ESPORT_CHANNEL_PROFILE)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_ESPORT_CHANNEL + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.ESPORT_PLAY_DIRECTLY)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_ESPORT_PLAY + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.ESPORT_TOURNAMENT_PROFILE)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_ESPORT_TOURNAMENT + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.METFONE_SERVICE)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_SERVICE_DETAIL + "?isExchange=" + isExchange + "&id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.METFONE_TOPUP)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_TOPUP));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.REWARD_DETAIL)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_REWARD_PROFILE + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_PLAY_DIRECTLY)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_FILM_PLAY + "?id=" + objectId));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.LINK)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_LINK + "?link=" + url));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.SIGNUP)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_SIGNUP ));
            } else if (actionType.equals(Constants.NOTIFICATION_CAMID.LOGIN)) {
                onClickIntent.setData(Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_LOGIN + "?link=" + url));
            }

            Bundle bundle = new Bundle();
            bundle.putBoolean("from_notify", true);
            onClickIntent.putExtras(bundle);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            onClickIntent.setAction(Intent.ACTION_VIEW);
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        if (mApplicationController.isReady()) {
            /*drawNotifyWithAvatar(onClickIntent, name, msg, avatar,
                    NOTIFY_NO_NUMBER, enableSound, enableVibrate, -1, true,
                    typeNotify);*/

            loadBitmapBigPicture(bigImageUrl, avatar, onClickIntent, name, msg, typeNotify);
            EventBus.getDefault().post(new TabHomeEvent().setUpdateNotify(true));
        }
    }

    private Bitmap bitmapBigPicture, bitmapAvatar;

    private void loadBitmapBigPicture(final String urlBigPicture,
                                      final String avatar,
                                      final Intent onClickIntent,
                                      final String contentTitle,
                                      final String contentText,
                                      final int idNotify) {
        Log.i(TAG, "loadBitmapBigPicture: " + urlBigPicture);
        if (TextUtils.isEmpty(urlBigPicture)) {
            bitmapBigPicture = null;
            loadBitmapAvatar(avatar, onClickIntent, contentTitle, contentText, idNotify);
        } else
            Glide.with(mApplicationController)
                    .asBitmap()
                    .load(urlBigPicture)
                    .into(new SimpleTarget<Bitmap>(600, 600) {
                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            Log.e(TAG, "loadBitmapBigPicture fail");
                            bitmapBigPicture = null;
                            loadBitmapAvatar(avatar, onClickIntent, contentTitle, contentText, idNotify);
                        }

                        @Override
                        public void onResourceReady(Bitmap bitmapBig, Transition<? super Bitmap> transition) {
                            bitmapBigPicture = bitmapBig;
                            loadBitmapAvatar(avatar, onClickIntent, contentTitle, contentText, idNotify);
                        }
                    });

    }

    private void loadBitmapAvatar(String avatar,
                                  final Intent onClickIntent,
                                  final String contentTitle,
                                  final String contentText,
                                  final int idNotify) {
        Log.i(TAG, "loadBitmapAvatar: " + avatar);
        if (TextUtils.isEmpty(avatar)) {
            bitmapAvatar = null;
            displayNotifyMediaBigPicture(bitmapBigPicture, bitmapAvatar, onClickIntent, contentTitle, contentText, idNotify);
        } else
            Glide.with(mApplicationController)
                    .asBitmap()
                    .load(avatar)
                    .into(new SimpleTarget<Bitmap>(600, 600) {
                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            bitmapAvatar = null;
                            Log.e(TAG, "loadBitmapAvatar fail");
                            displayNotifyMediaBigPicture(bitmapBigPicture, bitmapAvatar, onClickIntent, contentTitle, contentText, idNotify);
                        }

                        @Override
                        public void onResourceReady(Bitmap bitmapAva, Transition<? super Bitmap> transition) {
                            bitmapAvatar = bitmapAva;
                            displayNotifyMediaBigPicture(bitmapBigPicture, bitmapAvatar, onClickIntent, contentTitle, contentText, idNotify);
                        }
                    });
    }

    private void displayNotifyMediaBigPicture(final Bitmap bmBigPicture,
                                              Bitmap bmAvatar,
                                              final Intent clickIntent,
                                              final String contentTitle,
                                              final String contentText,
                                              final int idNotify) {
        Log.i(TAG, "displayNotifyMediaBigPicture");
        PendingIntent pendingIntent = PendingIntent.getActivity(mApplicationController,
                1000, clickIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        if (bmAvatar == null)
            bmAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mApplicationController)
                .setAutoCancel(true)
                .setContentTitle(contentTitle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(bmAvatar)
                .setColor(mRes.getColor(R.color.bg))
                .setContentIntent(pendingIntent)
                .setTicker(contentTitle + ": " + contentText)
                .setWhen(System.currentTimeMillis())
                .setContentText(contentText);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            mBuilder.setChannelId(Constants.NOTIFICATION.NOTIFY_OTHER + "");
            String channelID = mApplicationController.getChannelIdFromCache(idNotify);
            mBuilder.setChannelId(channelID);
        }


        if (bmBigPicture != null) {
            NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
            bigPicStyle.setBigContentTitle(contentTitle);
            bigPicStyle.setSummaryText(contentText);
            bigPicStyle.bigPicture(bmBigPicture);
            mBuilder.setStyle(bigPicStyle);
        } else {
            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.setBigContentTitle(contentTitle);
            bigTextStyle.bigText(contentText);
            mBuilder.setStyle(bigTextStyle);
        }

        //stackBuilder
        /*TaskStackBuilder stackBuilder = TaskStackBuilder.create(mApplicationController);
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(clickIntent);
        //pending intent
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);*/
        // mId allows you to update the notification later on.
        Notification mNotification = mBuilder.build();
        boolean enableSound = SettingBusiness.getInstance(mApplicationController).getPrefRingtone();
        boolean enableVibrate = SettingBusiness.getInstance(mApplicationController).getPrefVibrate();
        Log.d(TAG, "displayNotifyPreviewImage - enableSound: " + enableSound + " enableVibrate: " + enableVibrate);
        if (!enableSound) {
            mNotification.sound = null;
        } else {
            mNotification.sound = getCurrentSound();
        }
        if (!enableVibrate) {
            mNotification.vibrate = noVibraPattern;
        } else {
            mNotification.vibrate = vibrate;
        }
        mNotification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        mNotification.ledARGB = Color.GREEN;
        mNotification.ledOnMS = 300;
        mNotification.ledOffMS = 1000;
        mApplicationController.notifyWrapper(idNotify, mNotification, idNotify);
        Log.i(TAG, "notify -- preview image");
    }


    public void playSoundSendMessage(Context context, int threadId) {
        // tam thoi ko dung
       /* boolean isSilentThread = mSettingBusiness.checkExistInSilentThread(threadId);
        if (isSilentThread) {
            //ko thong bao j
            return;
        }
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        if (enableSound && audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL)
            if (enableSound)
                mSendMessageTone.play();*/
    }

    public void playSoundReceiveMessage(Context context, int threadId) {
        // tam thoi khong dung
      /*  boolean isSilentThread = mSettingBusiness.checkExistInSilentThread(threadId);
        if (isSilentThread) {
            //ko thong bao j
            return;
        }
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        if (enableSound && audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL)
            mReceiveMessageTone.play();*/
    }

    /**
     * notify tin nhan ko gui dc
     *
     * @param ctx
     * @param threadMessage
     */
    public void notifySendMessagesError(ApplicationController ctx, ThreadMessage threadMessage) {
        if (!SettingBusiness.getInstance(ctx).getPrefSettingOnNoti()) return;
        //tin nhan tu A, B , group C thi notify 3 lan
        getBusiness(ctx);
        Log.i(TAG, "notifySendMessagesError ");
        Intent onClickIntent;
        onClickIntent = new Intent(ctx, ChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ThreadMessageConstant.THREAD_ID, threadMessage.getId());
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, threadMessage.getThreadType());
        onClickIntent.putExtras(bundle);
        String contentTitle = ctx.getString(R.string.app_name);
        String contentText = ctx.getString(R.string.message_not_sent);
        contentText = String.format(contentText, mApplicationController.getMessageBusiness().getThreadName
                (threadMessage));
//        if (IMService.isReady()) {
        if (mApplicationController.isReady()) {
//            IMService.getInstance().notifyMessageSendError(threadMessage.getId(), onClickIntent,
//                    contentTitle, contentText);
            notifyMessageSendError(threadMessage.getId(), onClickIntent,
                    contentTitle, contentText);
        }
    }

    public void drawSocialFriendNotify(ApplicationController application, String msg,
                                       String friendMsisdn, String friendName,
                                       String friendAvatar, String appId, int action) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        String title = mRes.getString(R.string.app_name);
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            if (action == 1) {// accepted
                //title = friendName;
                ThreadMessage threadMessage = mApplicationController.getStrangerBusiness().
                        createMochaStrangerAndThread(friendMsisdn, friendName, friendAvatar, appId, true);
                onClickIntent = new Intent(application, ChatActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(ThreadMessageConstant.THREAD_ID, threadMessage.getId());
                bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, threadMessage.getThreadType());
                onClickIntent.putExtras(bundle);
            } else if (action == 0) {//requested
                /*onClickIntent = new Intent(application, HomeActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.HOME.TAB_MAIN, 0);
                bundle.putInt(Constants.HOME.TAB_CONTACT, 0);*/
                onClickIntent = new Intent(application, ContactListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.CONTACT.DATA_FRAGMENT, Constants.CONTACT.FRAG_LIST_SOCIAL_REQUEST);
                //SHOW_LIST_STRANGER
                onClickIntent.putExtras(bundle);
            } else return;
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
//        if (IMService.isReady()) {
        if (mApplicationController.isReady()) {
//            IMService.getInstance().setParamsNotify(onClickIntent, title,
//                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                    friendMsisdn, AVATAR_SOLO, -1, true, null, true, null,
//                    Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            setParamsNotify(onClickIntent, title,
                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                    friendMsisdn, AVATAR_SOLO, -1, true, null, true, null,
                    Constants.NOTIFICATION.NOTIFY_ONMEDIA, Constants.NOTIFICATION.NOTIFY_ONMEDIA);
        }
    }

    public void drawNotifyFriendLikeAlbum(ApplicationController application, String msg, String friendMsisdn) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        getBusiness(application);
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
        String title = mRes.getString(R.string.app_name);
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, ProfileActivity.class);
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
//        if (IMService.isReady()) {
        if (mApplicationController.isReady()) {
//            IMService.getInstance().setParamsNotify(onClickIntent, title,
//                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
//                    friendMsisdn, AVATAR_SOLO, -1, true, null, true, null,
//                    Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            setParamsNotify(onClickIntent, title,
                    msg, NOTIFY_NO_NUMBER, enableSound, enableVibrate,
                    friendMsisdn, AVATAR_SOLO, -1, true, null, true, null,
                    Constants.NOTIFICATION.NOTIFY_ONMEDIA, Constants.NOTIFICATION.NOTIFY_ONMEDIA);
        }
    }

    // use for android 10 dont provide permission draw over others app
    public void drawNotifyCall(ApplicationController application, String friendMsisdn,
                               int callType, boolean isAnswered, CallData callData, boolean isShowHeadUp) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, MochaCallActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(CallConstant.JIDNUMBER, friendMsisdn);
            bundle.putParcelable(CallConstant.SDP_CALL_DATA, callData);
            bundle.putInt(CallConstant.TYPE_FRAGMENT, callType);
            bundle.putBoolean(CallConstant.IS_ACCEPT_CALL, false);
            if (!isAnswered && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
                    && !Settings.canDrawOverlays(mApplicationController.getApplicationContext())) {
                bundle.putBoolean(CallConstant.FIRST_START_ACTIVITY, true);
            } else {
                bundle.putBoolean(CallConstant.FIRST_START_ACTIVITY, false);
            }
            onClickIntent.putExtras(bundle);
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
//        if (IMService.isReady()) {
        if (mApplicationController.isReady()) {
            Log.i(TAG, "ready");
//            IMService.getInstance().setParamsNotifyCall(onClickIntent, friendMsisdn, Constants.NOTIFICATION
//                    .NOTIFY_CALL, callType);
            int idNotification = (isShowHeadUp && !isAnswered) ? NOTIFY_CALL_HEAD_UP : NOTIFY_CALL;
            setParamsNotifyCall(onClickIntent, friendMsisdn, idNotification, callType, isAnswered, isShowHeadUp);
        } else {
            Log.i(TAG, "IMService not ready");
        }
    }


    public void drawNotifyCall(ApplicationController application, String friendMsisdn, int callType, boolean isAnswered) {
        drawNotifyCall(application, friendMsisdn, callType, isAnswered, null, false);
    }


    private NotificationCompat.Builder mBuilder;
    private int notificationProgressId = 12345;

    public void drawNotificationProgress(String title) {
        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        if (BuildConfig.DEBUG) {
            mBuilder = new NotificationCompat.Builder(mApplicationController);
            mBuilder.setContentTitle(title)
                    .setContentText("starting upload file")
                    .setSmallIcon(R.mipmap.ic_launcher);
            mBuilder.setProgress(100, 0, true);
            mBuilder.setAutoCancel(true);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                mBuilder.setChannelId(notificationProgressId + "");
                String channelID = mApplicationController.getChannelIdFromCache(notificationProgressId);
                mBuilder.setChannelId(channelID);
            }

//            if (IMService.isReady()) {
//                IMService.getInstance().notifyWrapper(notificationProgressId, mBuilder.build());
//            }
            if (mApplicationController.isReady()) {
                mApplicationController.notifyWrapper(notificationProgressId, mBuilder.build(), notificationProgressId);
            }
            Log.i(TAG, "notify -- draw progress" + notificationProgressId);
        }
    }

    public void updateNotificationProgress(String desc, int progress) {
        if (BuildConfig.DEBUG) {
            if (mBuilder == null) {
                drawNotificationProgress("upload");
            }
            mBuilder.setContentText(desc);
            mBuilder.setProgress(100, progress, false);
            mBuilder.setAutoCancel(true);
//            if (IMService.isReady()) {
//                IMService.getInstance().notifyWrapper(notificationProgressId, mBuilder.build());
//            }
            if (mApplicationController.isReady()) {
                mApplicationController.notifyWrapper(notificationProgressId, mBuilder.build(), notificationProgressId);
            }
            Log.i(TAG, "notify -- progress" + notificationProgressId);
        }
    }


    //HaiNV Chuyen notify tu IMService sang day
    private static final long[] vibrate = {300, 200, 100, 10};
    private static final long[] noVibraPattern = {0, 0, 0, 0};
    //-----------------------------variables from Linphone------------------------------------------
    private Notification mMsgNotif;
    private NotificationManager mNotificationManager;
    //----------------------------------------------------------------------------------------------
    private int currentThreadId = -1;
    //======================NOTIFY=============
    private int smallIconID;
    private Bitmap largeAvatarDefault, largeAvatarMocha;
    private int avatarSize;

    /**
     * set notification
     */

    public void setParamsNotify(final Intent onClickIntent, final String contentTitle,
                                final String contentText, final int contentInfo,
                                final boolean enableSound, final boolean enableVibrate,
                                String number, int avatarType, final long notifyTime,
                                final boolean enableTicker, String officialServerId,
                                final boolean isMultiNotify, ThreadMessage threadMessage,
                                final int idNotify, final int channelId) {
        setParamsNotify(onClickIntent, contentTitle, contentText, contentInfo, enableSound, enableVibrate,
                number, avatarType, notifyTime, enableTicker, officialServerId, isMultiNotify, threadMessage, idNotify, channelId, null);
    }

    public void setParamsNotify(final Intent onClickIntent, final String contentTitle,
                                final String contentText, final int contentInfo,
                                final boolean enableSound, final boolean enableVibrate,
                                String number, int avatarType, final long notifyTime,
                                final boolean enableTicker, String officialServerId,
                                final boolean isMultiNotify, ThreadMessage threadMessage,
                                final int idNotify, final int channelId, String avatarUrlServer) {
        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        Log.i(TAG, "setParamsNotify: " + contentTitle + "--------" + contentText + "-------" + contentInfo);
        currentThreadId = onClickIntent.getIntExtra(ThreadMessageConstant.THREAD_ID, -1);
        Bitmap bitmapAvatar;
        AvatarBusiness avatarBusiness = mApplicationController.getAvatarBusiness();
        Bitmap avatarCache = null;
        String avatarUrl = null;
        onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        onClickIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (avatarType == ReengNotificationManager.AVATAR_FAKE_OFFICER) {
            avatarUrl = null;
            bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(), R.drawable.ic_launcher);
        } else if (avatarType == ReengNotificationManager.AVATAR_MULTI_THREAD) {
            bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(), R.drawable.ic_avatar_default);
        } else if (avatarType == ReengNotificationManager.AVATAR_GROUP) {//TODO change avatar default
            if (threadMessage == null) {
                bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(),
                        R.drawable.ic_avatar_default);
            } else if (TextUtils.isEmpty(threadMessage.getGroupAvatar())) {
                String pathFile = ImageHelper.getAvatarGroupFilePath(threadMessage.getId());
                //GroupAvatarHelper groupAvatarHelper = GroupAvatarHelper.getInstance(mApplication);
                File fileGroupAvatar = new File(pathFile);
                // khong check token khi ve notification, cu ve avatar cu, sau vao detail ve lai sau
                if (fileGroupAvatar.exists()) {
                    Log.i(TAG, "filepath: " + pathFile);
                    try {
                        bitmapAvatar = BitmapFactory.decodeFile(pathFile);
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(),
                                R.drawable.ic_avatar_default);
                    }
                } else {
                    bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(),
                            R.drawable.ic_avatar_default);
                }
            } else {
                bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(),
                        R.drawable.ic_avatar_default);
                avatarUrl = avatarBusiness.getGroupAvatarUrl(threadMessage, threadMessage.getGroupAvatar());
            }
        } else if (avatarType == ReengNotificationManager.AVATAR_SOLO) {
            bitmapAvatar = largeAvatarDefault;
            if (!TextUtils.isEmpty(number)) {
                PhoneNumber phoneNumber = mApplicationController.getContactBusiness().getPhoneNumberFromNumber(number);
                if (phoneNumber != null && phoneNumber.getId() != null) {
                    avatarUrl = avatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), number, avatarSize);
                } else {
                    StrangerPhoneNumber strangerPhoneNumber = mApplicationController.
                            getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(number);
                    NonContact nonContact = mApplicationController.getContactBusiness().getExistNonContact(number);
                    // check so non contact truoc
                    if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                        avatarUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), number, avatarSize);
                    } else if (strangerPhoneNumber != null) {
                        if (strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType
                                .other_app_stranger) {
                            avatarUrl = strangerPhoneNumber.getFriendAvatarUrl();
                        } else {
                            avatarUrl = avatarBusiness.getAvatarUrl(strangerPhoneNumber.getFriendAvatarUrl(),
                                    number, avatarSize);
                        }
                    } else {
                        avatarUrl = null;
                        if (onClickIntent.getIntExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, 0)
                                == Constants.ONMEDIA.COMMENT) {
                            bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(), R.drawable
                                    .ic_launcher);
                        }
                    }
                }
            } else {
                avatarUrl = null;
                bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(), R.drawable.ic_launcher);
            }
        } else if (avatarType == ReengNotificationManager.AVATAR_OFFICER) {
            bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(), R.drawable.ic_launcher);
            avatarUrl = mApplicationController.getOfficerBusiness().getOfficerAvatarByServerId(officialServerId);
        } else {
            bitmapAvatar = largeAvatarDefault;
        }
        // avatar by url
        if (!TextUtils.isEmpty(avatarUrlServer)) {
            avatarUrl = avatarUrlServer;
        }
        if (!TextUtils.isEmpty(avatarUrl) && threadMessage != null) {
            avatarCache = avatarBusiness.getBitmapFromCache(avatarUrl, threadMessage.getId());
        }
        if (avatarCache != null) {// co cache
            bitmapAvatar = avatarCache;
        } else if (!TextUtils.isEmpty(avatarUrl)) {// ko co cache va co duong dan
            GlideImageLoader.ImageLoadingListener loadingImageListener = new GlideImageLoader.ImageLoadingListener() {

                @Override
                public void onLoadingComplete(Bitmap loadedImage) {
                    Log.d(TAG, "draw notification load image ---> onLoadingComplete");

                    if (loadedImage != null) {
                        // load dduowcj avatar thi ve lai notify o che do silent
                        int threadID = onClickIntent.getIntExtra(ThreadMessageConstant.THREAD_ID, -1);
                        if (currentThreadId == threadID) {
                            displayNotification(idNotify, onClickIntent, contentTitle, contentText,
                                    contentInfo, false, false, loadedImage, notifyTime, enableTicker, isMultiNotify, channelId);
                        }
                    }
                }

                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingFailed(String imageUri, GlideException e) {
                    Log.d(TAG, "draw notification load image ---> onLoadingFailed");
                    retryLoadAvatarAndDrawNotification(imageUri, onClickIntent, contentTitle,
                            contentText, contentInfo, notifyTime, enableTicker, isMultiNotify, idNotify);
                }
            };
            Log.d(TAG, "draw notification load image ---> loadAvatar: " + avatarUrl);
            avatarBusiness.loadAvatar(avatarUrl, loadingImageListener, avatarSize);
        }
        // display
        displayNotification(idNotify, onClickIntent, contentTitle, contentText, contentInfo,
                enableSound, enableVibrate, bitmapAvatar, notifyTime, enableTicker, isMultiNotify, channelId);
    }


    public void drawNotifyWithAvatar(final Intent onClickIntent, final String contentTitle,
                                     final String contentText,
                                     final String avatar,
                                     final int contentInfo,
                                     final boolean enableSound, final boolean enableVibrate,
                                     final long notifyTime,
                                     final boolean enableTicker,
                                     final int idNotify) {
        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        Bitmap avatarCache = BitmapFactory.decodeResource(mApplicationController.getResources(), R.drawable.ic_launcher);
        displayNotification(idNotify, onClickIntent, contentTitle, contentText, contentInfo,
                enableSound, enableVibrate, avatarCache, notifyTime, enableTicker, false, idNotify);


        GlideImageLoader.ImageLoadingListener loadingImageListener = new GlideImageLoader.ImageLoadingListener() {

            @Override
            public void onLoadingComplete(Bitmap loadedImage) {
                Log.sys(TAG, "draw notification load image ---> onLoadingComplete");

                if (loadedImage != null) {
                    // load dduowcj avatar thi ve lai notify o che do silent
                    int threadID = onClickIntent.getIntExtra(ThreadMessageConstant.THREAD_ID, -1);
                    if (currentThreadId == threadID) {
                        displayNotification(idNotify, onClickIntent, contentTitle, contentText,
                                contentInfo, false, false, loadedImage, notifyTime, enableTicker, false, idNotify);
                    }
                }
            }

            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingFailed(String imageUri, GlideException e) {
                Log.sys(TAG, "draw notification load image ---> onLoadingFailed");
                retryLoadAvatarAndDrawNotification(imageUri, onClickIntent, contentTitle,
                        contentText, contentInfo, notifyTime, enableTicker, false, idNotify);
            }
        };
        Log.sys(TAG, "draw notification load image ---> loadAvatar: " + avatar);
        mApplicationController.getAvatarBusiness().loadAvatar(avatar, loadingImageListener, avatarSize);
    }

    public void setParamsNotifyCall(final Intent onClickIntent, final String friendNumber, final int notifyId, int callType, boolean isAnswered, boolean isShowHeadUp) {
        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        AvatarBusiness avatarBusiness = mApplicationController.getAvatarBusiness();
        String name;
        final String msg = mApplicationController.getResources().getString(R.string.call_state_calling) + "...";
        String avatarUrl;
        Bitmap bitmapAvatar = null;
        PhoneNumber phoneNumber = mApplicationController.getContactBusiness().getPhoneNumberFromNumber(friendNumber);
        if (phoneNumber != null && phoneNumber.getId() != null) {
            name = phoneNumber.getName();
            avatarUrl = avatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), friendNumber, avatarSize);
        } else {
            StrangerPhoneNumber strangerPhoneNumber = mApplicationController.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(friendNumber);
            NonContact nonContact = mApplicationController.getContactBusiness().getExistNonContact(friendNumber);
            // check so non contact truoc
            if (strangerPhoneNumber != null) {
                name = strangerPhoneNumber.getFriendName();
                if (strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                    avatarUrl = strangerPhoneNumber.getFriendAvatarUrl();
                } else {
                    avatarUrl = avatarBusiness.getAvatarUrl(strangerPhoneNumber.getFriendAvatarUrl(), friendNumber,
                            avatarSize);
                }
            } else {
                name = friendNumber;
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                    avatarUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), friendNumber, avatarSize);
                } else {
                    avatarUrl = null;
                }
            }
        }
        if (!TextUtils.isEmpty(avatarUrl)) {
            new GlideImageLoader(new GlideImageLoader.ImageLoadingListener() {
                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingComplete(Bitmap loadedImage) {
                    Observable.fromCallable(() -> {
                        Bitmap resizedBitmap = getResizedBitmap(loadedImage, 180, 180);
                        Bitmap bitmapRound = getRoundedCornerBitmap(resizedBitmap, 50);
                        return bitmapRound;
                    }).subscribeOn(Schedulers.single())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(bitmap -> {
                                if (mApplicationController.getCallBusiness().isExistCall()
                                        && friendNumber.equals(mApplicationController.getCallBusiness().getCurrentFriendJid())) {
                                    setNotifyCall(onClickIntent, bitmap, name, msg, notifyId, callType,
                                            mApplicationController.getCallBusiness().isCallConnected(), isShowHeadUp);
                                }
                            });
                }

                @Override
                public void onLoadingFailed(String imageUri, GlideException e) {
                    setNotifyCall(onClickIntent, null, name, msg, notifyId, callType,
                            mApplicationController.getCallBusiness().isCallConnected(), isShowHeadUp);
                }
            }).loadBitmap(avatarUrl);

//            mApplicationController.getUniversalImageLoader().loadImage(avatarUrl, );
        }
        setNotifyCall(onClickIntent, null, name, msg, notifyId, callType, isAnswered, isShowHeadUp);
    }


    private void setNotifyCall(Intent onClickIntent, Bitmap bitmapAvatar, String title, String content,
                               int notifyId, int callType, boolean isAnswered, boolean isShowHeadUp) {


        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        PendingIntent pendingIntent;
        pendingIntent = PendingIntent.getActivity(mApplicationController, 1000, onClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        long notifyTime = System.currentTimeMillis();
        int smallIcon;
        if (callType == CallConstant.TYPE_CALLEE) {
            smallIcon = R.drawable.ic_incall_notify;
        } else {
            smallIcon = R.drawable.ic_outcall_notify;
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mApplicationController)
                .setSmallIcon(smallIcon)
                .setAutoCancel(true)
                .setWhen(notifyTime)
                .setContentIntent(pendingIntent)
                .setContentTitle(title)
                .setContentText(content);
        if (bitmapAvatar != null) {
            notificationBuilder.setLargeIcon(bitmapAvatar);
        }
        if (isAnswered) {
//            if(isShowingHeadUpCallNotification){
//                mApplicationController.cancelNotification(NOTIFY_CALL_HEAD_UP);
//                isShowingHeadUpCallNotification = false;
//            }
        } else {
            if (callType == CallConstant.TYPE_CALLEE) {
                Intent intentAnswer = new Intent(mApplicationController, MochaCallActivity.class);
                Bundle bundle = new Bundle();
                Bundle bundleClick = onClickIntent.getExtras();
                bundle.putString(CallConstant.JIDNUMBER, bundleClick.getString(CallConstant.JIDNUMBER));
                bundle.putInt(CallConstant.TYPE_FRAGMENT, bundleClick.getInt(CallConstant.TYPE_FRAGMENT));
                if (isNotShowActivityCall(mApplicationController.getApplicationContext())) {
                    if (isShowHeadUp) {
                        notificationBuilder.setFullScreenIntent(pendingIntent, true)
                                .setPriority(NotificationCompat.PRIORITY_MAX);
                        isShowingHeadUpCallNotification = true;
                    }
                    bundle.putBoolean(CallConstant.FIRST_START_ACTIVITY, true);
                    bundle.putParcelable(CallConstant.SDP_CALL_DATA, bundleClick.getParcelable(CallConstant.SDP_CALL_DATA));
                } else {
                    bundle.putBoolean(CallConstant.FIRST_START_ACTIVITY, false);
                }
                bundle.putBoolean(CallConstant.IS_ACCEPT_CALL, true);
                intentAnswer.putExtras(bundle);
                PendingIntent answerPendingIntent =
                        PendingIntent.getActivity(mApplicationController, 1001, intentAnswer, PendingIntent.FLAG_UPDATE_CURRENT);
                Intent intentDecline = new Intent(mApplicationController, DeclineCallReceiver.class);
                PendingIntent declinePendingIntent =
                        PendingIntent.getBroadcast(mApplicationController, 1002, intentDecline, PendingIntent.FLAG_UPDATE_CURRENT);
                notificationBuilder.addAction(R.drawable.ic_end_call_notificaiton, mRes.getString(R.string.decline), declinePendingIntent);
                notificationBuilder.addAction(R.drawable.ic_hang_call_notification, mRes.getString(R.string.answer), answerPendingIntent);
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            notificationBuilder.setChannelId(notifyId + "");
            String channelID = mApplicationController.getChannelIdFromCache(notifyId);
            // test
//
//            String channelId = "30061995";
//            String channelName = "call mocha channel";
//            notificationBuilder.setFullScreenIntent(pendingIntent, true)
//                    .setOngoing(true)
//                    .setDefaults(NotificationCompat.DEFAULT_ALL)
//                    .setPriority(NotificationCompat.PRIORITY_MAX);
//            NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
//            channel.setDescription("channel mocha description");
//            channel.setVibrationPattern(new long[]{100, 80, 60, 40, 20,0});
//            channel.setLightColor(Color.RED);
//            NotificationManager notificationManager = (NotificationManager) mApplicationController.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelID);
//            notificationManager.notify(notifyId, notificationBuilder.build());
        }
        //
        mMsgNotif = notificationBuilder.build();
        mMsgNotif.flags = Notification.FLAG_ONGOING_EVENT;

        mApplicationController.notifyWrapper(notifyId, mMsgNotif, notifyId);
    }

    private void setNotifyCall(Intent onClickIntent, Bitmap bitmapAvatar, String title, String content, int notifyId, int callType) {
        setNotifyCall(onClickIntent, bitmapAvatar, title, content, notifyId, callType, false, false);
    }

    // update notification after user accept call
    public void updatenotificationAnswerCall() {

    }

    private void retryLoadAvatarAndDrawNotification(String avatarUrl, final Intent onClickIntent,
                                                    final String contentTitle, final String contentText,
                                                    final int contentInfo, final long notifyTime,
                                                    final boolean enableTicker, final boolean isMultiNotify,
                                                    final int idNotify) {
        AvatarBusiness avatarBusiness = mApplicationController.getAvatarBusiness();
        GlideImageLoader.ImageLoadingListener loadingImageListener = new GlideImageLoader.ImageLoadingListener() {

            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingFailed(String imageUri, GlideException e) {

            }

            @Override
            public void onLoadingComplete(Bitmap loadedImage) {
                Log.d(TAG, "draw notification load image ---> onLoadingComplete");
                if (loadedImage != null) {
                    // load dduowcj avatar thi ve lai notify o che do silent
                    displayNotification(idNotify, onClickIntent, contentTitle, contentText,
                            contentInfo, false, false, loadedImage, notifyTime, enableTicker, isMultiNotify, idNotify);
                }
            }
        };
        Log.d(TAG, "draw notification load image ---> loadAvatar: " + avatarUrl);
        avatarBusiness.loadAvatar(avatarUrl, loadingImageListener, avatarSize);
    }

    public void notifyMessageSendError(int threadId, final Intent onClickIntent, final String contentTitle,
                                       final String contentText) {
        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        Bitmap bitmapAvatar;
        bitmapAvatar = BitmapFactory.decodeResource(mApplicationController.getResources(), R.drawable.ic_launcher);
        displayNotification(threadId, onClickIntent, contentTitle, contentText, ReengNotificationManager
                        .NOTIFY_NO_NUMBER,
                false, false, bitmapAvatar, System.currentTimeMillis(), true, true, Constants.NOTIFICATION.NOTIFY_MESSAGE);
    }

    /**
     * draw notification
     */
    private void displayNotification(int notificationId, Intent onClickIntent, String contentTitle,
                                     String contentText, int contentInfo, boolean enableSound,
                                     boolean enableVibrate, Bitmap bitmapAvatar, long notifyTime,
                                     boolean enableTicker, boolean isMultiNotify, int channelId) {
        displayNotificationNormal(notificationId, onClickIntent, contentTitle, contentText,
                contentInfo, enableSound, enableVibrate, bitmapAvatar, notifyTime, enableTicker, isMultiNotify, channelId);
    }

    private void displayNotificationNormal(int notificationId, Intent onClickIntent, String contentTitle,
                                           String contentText, int contentInfo, boolean enableSound,
                                           boolean enableVibrate, Bitmap bitmapAvatar, long notifyTime,
                                           boolean enableTicker, boolean isMultiNotify, int channelId) {
        if (!SettingBusiness.getInstance(mApplicationController).getPrefSettingOnNoti()) return;
        PendingIntent pendingIntent;
        if (isMultiNotify) {
            Random r = new Random();
            int requestCode = r.nextInt(100) + 1000;
            pendingIntent = PendingIntent.getActivity(mApplicationController,
                    requestCode, onClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            pendingIntent = PendingIntent.getActivity(mApplicationController,
                    1000, onClickIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        }
        if (notifyTime == -1) {
            Calendar currentCal = Calendar.getInstance();
            notifyTime = currentCal.getTimeInMillis();
        }
        //TODO bigTextStyle
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(contentTitle);
        bigTextStyle.bigText(contentText);
        //
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mApplicationController)
                .setSmallIcon(smallIconID)
                .setAutoCancel(true)
                .setWhen(notifyTime)
                .setLargeIcon(bitmapAvatar)
                .setContentIntent(pendingIntent)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setStyle(bigTextStyle)
                .setColor(ContextCompat.getColor(mApplicationController, R.color.bg_mocha));
        if (contentInfo != ReengNotificationManager.NOTIFY_NO_NUMBER) {
            notificationBuilder.setNumber(contentInfo);
        }
        if (enableTicker) {
            notificationBuilder.setTicker(contentTitle + ": " + contentText);
        }
        //
        if (!enableSound) {
            notificationBuilder.setSound(null);
        } else {
            notificationBuilder.setSound(getCurrentSound());
        }
        if (!enableVibrate) {
            notificationBuilder.setVibrate(noVibraPattern);
        } else {
            notificationBuilder.setVibrate(vibrate);
        }
        if (enableSound && enableVibrate) {
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        } else {
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_LOW);
        }
        notificationBuilder.setLights(Color.WHITE, 300, 1000);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            notificationBuilder.setChannelId(channelId + "");
            String channelID = mApplicationController.getChannelIdFromCache(channelId);
            notificationBuilder.setChannelId(channelID);
        }
        //
        mMsgNotif = notificationBuilder.build();
        mMsgNotif.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        mApplicationController.notifyWrapper(notificationId, mMsgNotif, channelId);
    }

    /**
     * display notification image full
     *
     * @param bitmap
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void displayNotifyPreviewImage(Intent clickIntent, Bitmap bitmap, String contentTitle,
                                          String contentText, String bigContentTitle, String bigContentText) {
        Bitmap bitmapLarger = BitmapFactory.decodeResource(mApplicationController.getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mApplicationController)
                .setAutoCancel(true)
                .setContentTitle(contentTitle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(bitmapLarger)
                .setContentText(contentText);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            mBuilder.setChannelId(Constants.NOTIFICATION.NOTIFY_OTHER + "");
            String channelID = mApplicationController.getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_OTHER);
            mBuilder.setChannelId(channelID);
        }

        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
        bigPicStyle.bigPicture(bitmap);
        bigPicStyle.setBigContentTitle(bigContentTitle);
        bigPicStyle.setSummaryText(bigContentText);
        mBuilder.setStyle(bigPicStyle);
        //stackBuilder
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mApplicationController);
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(clickIntent);
        //pending intent
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        // mId allows you to update the notification later on.
        Notification mNotification = mBuilder.build();
        boolean enableSound = SettingBusiness.getInstance(mApplicationController).getPrefRingtone();
        boolean enableVibrate = SettingBusiness.getInstance(mApplicationController).getPrefVibrate();
        Log.d(TAG, "displayNotifyPreviewImage - enableSound: " + enableSound + " enableVibrate: " + enableVibrate);
        if (!enableSound) {
            mNotification.sound = null;
        } else {
            mNotification.sound = getCurrentSound();
        }
        if (!enableVibrate) {
            mNotification.vibrate = noVibraPattern;
        } else {
            mNotification.vibrate = vibrate;
        }
        mNotification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        mNotification.ledARGB = Color.GREEN;
        mNotification.ledOnMS = 300;
        mNotification.ledOffMS = 1000;
        mApplicationController.notifyWrapper(Constants.NOTIFICATION.NOTIFY_OTHER, mNotification, Constants.NOTIFICATION.NOTIFY_OTHER);
        Log.i(TAG, "notify -- preview image");
    }

    public Uri getCurrentSound() {
        return SettingBusiness.getInstance(mApplicationController).getCurrentSoundUri();
    }

    public void drawNotifyDeeplink(ApplicationController application, String deeplink, String title, String msg, String avatar) {
        if (!SettingBusiness.getInstance(application).getPrefSettingOnNoti()) return;
        Intent onClickIntent;
        ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()) {
            onClickIntent = new Intent(application, HomeActivity.class);
            onClickIntent.setData(Uri.parse(deeplink));
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            onClickIntent.setAction(Intent.ACTION_VIEW);
        } else {
            onClickIntent = application.getPackageManager()
                    .getLaunchIntentForPackage(application.getPackageName());
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        boolean enableSound = mSettingBusiness.getPrefRingtone();
        boolean enableVibrate = mSettingBusiness.getPrefVibrate();
//        if (IMService.isReady()) {
//            IMService.getInstance().drawNotifyWithAvatar(onClickIntent, title, msg, avatar,
//                    NOTIFY_NO_NUMBER, enableSound, enableVibrate, -1, true,
//                    Constants.NOTIFICATION.NOTIFY_DEEPLINK);
//        }
        if (mApplicationController.isReady()) {
            drawNotifyWithAvatar(onClickIntent, title, msg, avatar,
                    NOTIFY_NO_NUMBER, enableSound, enableVibrate, -1, true,
                    Constants.NOTIFICATION.NOTIFY_DEEPLINK);
        }
    }

    public boolean isShowingHeadUpCallNotification() {
        return isShowingHeadUpCallNotification;
    }
}
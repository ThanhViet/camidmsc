package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.helper.message.SmsReceivedObject;
import com.metfone.selfcare.listeners.SmsReceiverListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.regex.Matcher;

public class SmsReceiver extends BroadcastReceiver {
    private static final String TAG = SmsReceiver.class.getSimpleName();
    private static ArrayList<SmsReceiverListener> mSmsPasswordReceivedListener;
    //    private boolean abortSms = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        /*if (mSmsPasswordReceivedListener == null) {
            mSmsPasswordReceivedListener = new ArrayList<SmsReceiverListener>();
        }*/
        Bundle bundle = intent.getExtras();
        String originatingAddress = null;
        String content = null;
        SmsMessage[] msgs;
        Log.d(TAG, "sms received");
        if (bundle != null) {
            // ---retrieve the SMS message received---
            Object[] pdus = (Object[]) bundle.get("pdus");
            if (pdus == null) return;
            int length = pdus.length;
            msgs = new SmsMessage[length];
            StringBuilder sb = new StringBuilder();
            try {
                for (int i = 0; i < length; i++) {
                    if (Version.hasM()) {
                        String format = bundle.getString("format");
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                    } else {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    }
                    if (msgs[i] == null) break;
                    String c = msgs[i].getMessageBody();
                    sb.append(c);
                }
                originatingAddress = msgs[0].getDisplayOriginatingAddress();
                // msgs[0].get
                content = sb.toString();
                Log.d(TAG, "originatingAddress: " + originatingAddress + " ,content: " + content);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            if (!TextUtils.isEmpty(content) && !TextUtils.isEmpty(originatingAddress)) {
                // Log.d(TAG, "sms received content:= " + content);
                if (isMochaSmsCenter(originatingAddress, content)) {
                    notifySmsPasswordReceived(content);
                /*if (abortSms) {
                    abortBroadcast();
                }*/
                } else if (isLixiSmsCenter(originatingAddress, content)) {
                    notifySmsPasswordLixiRecived(content);
                 /*if (abortSms) {
                    abortBroadcast();
                }*/
                } else if (originatingAddress.length() > 4) {
                    ApplicationController application = (ApplicationController) context.getApplicationContext();
                    if (application.getReengAccountBusiness().isSmsInEnable() && SettingBusiness.getInstance
                            (application).getPrefReplySms()) {
                        application.getProcessSmsQueue().addTask(new SmsReceivedObject(originatingAddress, content));
                    }
                }
                //ko abort sms nua
                /*if (abortSms) {
                    abortBroadcast();
                }*/
                // }
            }
        }
    }

    public static synchronized void addSMSReceivedListener(SmsReceiverListener listener) {
        if (mSmsPasswordReceivedListener == null) {
            mSmsPasswordReceivedListener = new ArrayList<>();
        }
        for(SmsReceiverListener item: mSmsPasswordReceivedListener)
        {
            String className = item.getClass().getSimpleName();
            if(listener.getClass().getSimpleName().equals(className))
                return;
        }
        if (!mSmsPasswordReceivedListener.contains(listener))
            mSmsPasswordReceivedListener.add(listener);
    }

    public static synchronized void removeSMSReceivedListener(SmsReceiverListener listener) {
        if (mSmsPasswordReceivedListener == null) {
            mSmsPasswordReceivedListener = new ArrayList<>();
        }
        mSmsPasswordReceivedListener.remove(listener);
    }

    public static synchronized boolean notifySmsChatReceived(String smsSender, String smsContent) {
        boolean abortSms = false;
        if (mSmsPasswordReceivedListener != null && !mSmsPasswordReceivedListener.isEmpty()) {
            for (SmsReceiverListener listener : mSmsPasswordReceivedListener) {
                abortSms = listener.onSmsChatReceived(smsSender, smsContent);
            }
        }
        return abortSms;
    }

    private boolean isMochaSmsCenter(String originatingAddress, String content) {
        return (originatingAddress.equals(Constants.SMS.SMS_CENTER_NUMBER)
                || originatingAddress.equals("+" + Constants.SMS.SMS_CENTER_NUMBER)
                || originatingAddress.equals(Constants.SMS.SMS_CENTER_NUMBER_3)
                || originatingAddress.equals(Constants.SMS.SMS_CENTER_NUMBER_4)
                || originatingAddress.equals(Constants.SMS.SMS_CENTER_NUMBER_5)
                || originatingAddress.equals(Constants.SMS.SMS_CENTER_NUMBER_6)
                || originatingAddress.equals(Constants.SMS.SMS_CENTER_NUMBER_7)
                || originatingAddress.equals("+" + Constants.SMS.SMS_CENTER_NUMBER_3)
                || originatingAddress.equals("0" + Constants.SMS.SMS_CENTER_NUMBER_2)
                || originatingAddress.equals("+84" + Constants.SMS.SMS_CENTER_NUMBER_2))
                && (content.toLowerCase().contains(Constants.SMS.SMS_PASSWORD_NOTIFY_CONTENT)
                || content.toLowerCase().contains(Constants.SMS.SMS_PASSWORD_NOTIFY_CONTENT_2
        ));
    }

    private boolean isLixiSmsCenter(String originatingAddress, String content) {
        return originatingAddress.equals(Constants.SMS.SMS_LIXI_CENTER_NUMBER) &&
                content.toLowerCase().contains(Constants.SMS.SMS_LIXI_CODE_CONTENT);
    }

    public static void notifySmsPasswordReceived(String content) {
        String[] s = content.split(":");
        String password = null;
        if (s != null && s.length > 0) {
            int size = s.length;
            password = s[size - 1].trim();
        }
        if (!TextUtils.isEmpty(password)) {
            if (mSmsPasswordReceivedListener != null && !mSmsPasswordReceivedListener.isEmpty()) {
                for (SmsReceiverListener listener : mSmsPasswordReceivedListener) {
                    listener.onSmsPasswordReceived(password);
                }
            }
        }
    }

    public static void notifySmsPasswordLixiRecived(String content) {
        try {
            ArrayList<String> s = new ArrayList<>();
            Matcher m = PhoneNumberHelper.getInstant().getPatternLixi().matcher(content);
            while (m.find()) {
                if (m.group().length() > 3) {
                    s.add(m.group());
                }
            }
            String password = null;
            if (!s.isEmpty()) {
                password = s.get(s.size() - 1);
            }
            if (!TextUtils.isEmpty(password)) {
                if (mSmsPasswordReceivedListener != null && !mSmsPasswordReceivedListener.isEmpty()) {
                    for (SmsReceiverListener listener : mSmsPasswordReceivedListener) {
                        listener.onSmsPasswordLixiReceived(password);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }
}
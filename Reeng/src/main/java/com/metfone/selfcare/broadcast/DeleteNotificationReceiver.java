package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.notification.MessageNotificationManager;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 2/21/2017.
 */
public class DeleteNotificationReceiver extends BroadcastReceiver {
    private static final String TAG = DeleteNotificationReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        int threadId = intent.getExtras().getInt(ThreadMessageConstant.THREAD_ID, -1);
        Log.d(TAG, "onReceive: " + threadId);
        if (threadId == -1) return;
        ApplicationController application = (ApplicationController) context.getApplicationContext();
        if (application.isDataReady()){
            MessageNotificationManager.getInstance(application).deleteNotifyMessage(threadId);
        }
    }
}

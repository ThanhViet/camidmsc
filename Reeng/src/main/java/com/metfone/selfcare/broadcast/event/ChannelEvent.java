package com.metfone.selfcare.broadcast.event;

import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;

/**
 * Created by tuanha00 on 3/20/2018.
 */

public class ChannelEvent extends MessageEvent {
    Channel  channel;

    public ChannelEvent(Channel channel) {
        this.channel = channel;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}

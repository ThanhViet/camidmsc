package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

public class MiniReceiver extends BroadcastReceiver {

    private static final String TAG = "MiniReceiver";

    private static final String ACTION_SCREEN_OFF = Intent.ACTION_SCREEN_OFF;

    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) return;

        Log.i(TAG, "onReceive: " + action);
        this.context = context;

        switch (action) {
            case ACTION_SCREEN_OFF:
                stopVideoMini();
                break;
        }
    }

    private void stopVideoMini() {
        if (context != null)
            context.sendBroadcast(new Intent(Constants.TabVideo.PAUSE_VIDEO_SERVICE));
        Log.i(TAG, "stopVideoMini: stop");
    }
}

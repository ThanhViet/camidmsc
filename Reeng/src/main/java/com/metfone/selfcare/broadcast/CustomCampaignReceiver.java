package com.metfone.selfcare.broadcast;

/**
 * Created by bigant on 4/22/2015.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.util.Log;

/*
 *  A simple Broadcast Receiver to receive an INSTALL_REFERRER
 *  intent and pass it to other receivers, including
 *  the Google Analytics receiver.
 */

public class CustomCampaignReceiver extends BroadcastReceiver {//TODO khong chay vao day nưa
    private static final String TAG = CustomCampaignReceiver.class.getSimpleName();
    private static final String UTM_PREFIX = "utm_source=";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive " + intent.getDataString() + " action = " + intent.getAction());
        // Pass the intent to other receivers.

        // When you're done, pass the intent to the Google Analytics receiver.
//        new CampaignTrackingReceiver().onReceive(context, intent);
        String utm_source = intent.getStringExtra("referrer");
        Log.i(TAG, "onReceive var3 = " + utm_source);
        if (TextUtils.isEmpty(utm_source)) {
            utm_source = "natural";
        }
        if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction())) {
            DeviceHelper.sendUtmSource(context, utm_source);
        }
    }
}

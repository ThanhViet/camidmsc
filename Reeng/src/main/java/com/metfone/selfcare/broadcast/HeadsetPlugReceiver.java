package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 23/07/2015.
 */
public class HeadsetPlugReceiver extends BroadcastReceiver {
    private static final String TAG = HeadsetPlugReceiver.class.getSimpleName();
    private static final int STATE_BLUETOOTH_CONNECTED = 0x00000002;
    private static final int STATE_WIRE_CONNECTED = 0x00000001;
    private static final int STATE_DISCONNECTED = 0x00000000;
    protected static final String[] HEADPHONE_ACTIONS = {
            Intent.ACTION_HEADSET_PLUG,
            "android.bluetooth.headset.action.STATE_CHANGED",
            "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"
    };
    private ApplicationController application;

    @Override
    public void onReceive(Context context, Intent intent) {
        // Wired headset monitoring
        int state = -1;
        String action = intent.getAction();
        if (action == null) {
            return;
        }
        application = (ApplicationController) context.getApplicationContext();
        if (action.equals(HEADPHONE_ACTIONS[0])) {
            state = intent.getIntExtra("state", -1);
        } else if (action.equals(HEADPHONE_ACTIONS[1])) {
            // Bluetooth monitoring
            // Works up to and including Honeycomb
            state = intent.getIntExtra("android.bluetooth.headset.extra.STATE", -1);
        } else if (action.equals(HEADPHONE_ACTIONS[2])) {
            // Works for Ice Cream Sandwich
            state = intent.getIntExtra("android.bluetooth.profile.extra.STATE", -1);
        }
        //
        switch (state) {
            case STATE_DISCONNECTED:
                Log.d(TAG, "Headset is unplugged");
                if (application.getPlayMusicController() != null) {
                    if (application.getPlayMusicController().isPlaying()) {
                        application.getPlayMusicController().toggleMusic();
                    }
                }
                application.getCallBusiness().headsetPlug(false);
                break;
            case STATE_WIRE_CONNECTED:
                Log.d(TAG, "Headset is plugged wire");
                application.getCallBusiness().headsetPlug(true);
                break;
            case STATE_BLUETOOTH_CONNECTED:
                Log.d(TAG, "Headset is plugged bluetooth");
                application.getCallBusiness().headsetPlug(true);
                break;
            default:
                Log.d(TAG, "I have no idea what the Headset state is");
        }
    }

    public static String[] getHeadphoneActions() {
        return HEADPHONE_ACTIONS;
    }
}
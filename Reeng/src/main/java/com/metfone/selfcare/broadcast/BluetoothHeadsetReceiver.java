package com.metfone.selfcare.broadcast;

import android.bluetooth.BluetoothHeadset;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.call.CallBluetoothHelper;

/**
 * Created by toanvk2 on 1/31/2018.
 */

public class BluetoothHeadsetReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        int state = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, -1);
        ApplicationController application = (ApplicationController) context.getApplicationContext();
        CallBluetoothHelper.getInstance(application).onBluetoothStateChanged(action, state);
    }
}
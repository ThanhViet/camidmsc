package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.CommonApi;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;

/**
 * Created by thaodv on 6/26/2014.
 */
public class NetworkReceiver extends BroadcastReceiver {
    private static final String TAG = NetworkReceiver.class.getSimpleName();

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.i(TAG, "onReceive timestamp: " + System.currentTimeMillis());
        ApplicationController application = ((ApplicationController) context.getApplicationContext());
        application.getListenerUtils().notifyInternetChanged();
        NetworkHelper.getInstance().onNetworkConnectivityChanged(context);
        boolean isConnectInternet = NetworkHelper.isConnectInternet(application);
        if (isConnectInternet && application.getConfigBusiness() != null && application.getConfigBusiness().isReady()) {
            Log.i(TAG, "onReceive getMyLocation");
            new GetMyLocationTask().execute();
        } else {
            CommonApi.getInstance().setLoadingLocation(false);
        }
        LogDebugHelper.getInstance().logDebugContent("onNetworkConnectivityChanged: hasconnect: " + isConnectInternet);
    }

    public static class GetMyLocationTask extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            CommonApi.getInstance().getMyLocation();
            return null;
        }
    }
}

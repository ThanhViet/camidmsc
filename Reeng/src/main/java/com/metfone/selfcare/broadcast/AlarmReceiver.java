package com.metfone.selfcare.broadcast;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.util.Log;
import com.viettel.util.PowerModeHelper;

import java.util.Calendar;

/**
 * Created by toanvk2 on 1/5/15.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private static final String TAG = AlarmReceiver.class.getSimpleName();
    private XMPPManager mXmppManager;
    private ApplicationController application;
    private ApplicationStateManager applicationStateManager;
    private SharedPreferences mPref;

    @Override
    public void onReceive(Context context, Intent intent) {
        int alarmId = intent.getIntExtra("mocha_alarm_id", -1);
        Log.f(TAG, "AlarmReceiver: onReceive mocha_alarm_id=> " + alarmId);
        application = (ApplicationController) context.getApplicationContext();
        mXmppManager = application.getXmppManager();
        applicationStateManager = application.getAppStateManager();
        if (Version.hasKitKat()) {// api >=19  setInexactRepeating khong con dung nua
            repeatAlarm(application, alarmId);
        }
        checkToKillConnectionOnBackground(applicationStateManager, mXmppManager);

        switch (alarmId) {
            case Constants.ALARM_MANAGER.PING_ID:
                mXmppManager.sendPacketPing();
                break;
            default:
                // TODO get alarm_id  loi xu ly gi day
                mXmppManager.sendPacketPing();
                break;
        }
    }

    private void checkToKillConnectionOnBackground(ApplicationStateManager applicationStateManager, XMPPManager mXmppManager) {
        if (applicationStateManager.getLastTimeAppEnterBackground() == 0) {
            Log.i(TAG, "app is on foreground or (background but not set time)");
            if (applicationStateManager.isAppWentToBg()) {
                Log.i(TAG, "app is went to background --> update ");
                applicationStateManager.setLastTimeAppEnterBackground(System.currentTimeMillis());
            }
        } else {
            //neu da qua thoi gian cho phep o background thi kill connection
            if (TimeHelper.isPassedARangeTime(applicationStateManager.getLastTimeAppEnterBackground(), Constants.ALARM_MANAGER.ENTER_BACKGROUND_MAX_TIME)
                    && TimeHelper.isPassedARangeTime(XMPPManager.getLastTimeConnectedSucceed(), TimeHelper.FIVE_MIN_IN_MILISECOND)) {
                Log.i(TAG, "app enter background too long --> kill connection");
                applicationStateManager.setLastTimeAppEnterBackground(0); //reset to avoid
                if (mXmppManager != null) {
                    mXmppManager.manualDisconnect();
                }
            } else {
                Log.i(TAG, "app just recently entered background --> keep connection");
            }
        }
    }

    /**
     * @param context
     * @param alarmId
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void startAlarmManager(ApplicationController context, int alarmId) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmMgr != null) {
            PendingIntent alarmIntent = getPendingIntentAlarm(context, alarmId);
            Calendar calendar = Calendar.getInstance();
            long pingInterval = getTimeIntervalAlarm(context, alarmId);
            if (Version.hasM()) {
                alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC, calendar.getTimeInMillis() + 1, alarmIntent);
            } else if (Version.hasKitKat()) {
                alarmMgr.setExact(AlarmManager.RTC, calendar.getTimeInMillis() + 1, alarmIntent);
            } else {
                alarmMgr.setInexactRepeating(AlarmManager.RTC,
                        calendar.getTimeInMillis(), pingInterval, alarmIntent);
            }
        }
    }

    public void cancelAlarmManager(Context context, int alarmId) {
        AlarmManager alarmMgrStop = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intentStop = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntentStop = PendingIntent.getBroadcast(context,
                alarmId, intentStop, PendingIntent.FLAG_UPDATE_CURRENT);
        if (alarmMgrStop != null) {
            Log.d(TAG, "cancelAlarm alarmId:-> " + alarmId);
            alarmMgrStop.cancel(pendingIntentStop);
        }
    }

    private PendingIntent getPendingIntentAlarm(ApplicationController context, int alarmId) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("mocha_alarm_id", alarmId);
        return PendingIntent.getBroadcast(context, alarmId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private long getTimeIntervalAlarm(ApplicationController context, int alarmId) {
        long interval;
        if (alarmId == Constants.ALARM_MANAGER.MUSIC_PING_ID) {
            interval = Constants.ALARM_MANAGER.MUSIC_PING_TIMER;
        } else if (alarmId == Constants.ALARM_MANAGER.MUSIC_PONG_ID) {
            interval = Constants.ALARM_MANAGER.MUSIC_PING_TIMER;
        } else {
            mPref = context.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
            if (PowerModeHelper.isPowerSaveMode(ApplicationController.self()))
                interval = Long.valueOf(mPref.getString(Constants.PREFERENCE.CONFIG.PING_INTERVAL_POWER_SAVE_MODE, "10000")) + 2000;
            else
                interval = Long.valueOf(mPref.getString(Constants.PREFERENCE.CONFIG.PING_INTERVAL, "60000")) + 5000;
            //interval = 120000;// 2 phut
        }
        Log.d(TAG, "getTimeIntervalAlarm: " + interval);
        return interval;
    }

    /**
     * api >=19
     *
     * @param context
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void repeatAlarm(ApplicationController context, int alarmId) {
        if (alarmId <= -1 || context == null) return;// id loi ko repeat
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmMgr != null) {
            PendingIntent alarmIntent = getPendingIntentAlarm(context, alarmId);
            Calendar calendar = Calendar.getInstance();
            //setRepeating
            long triggerAtMillis = calendar.getTimeInMillis() + getTimeIntervalAlarm(context, alarmId);
            try {
                if (Version.hasM()) {
                    alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC, triggerAtMillis, alarmIntent);
                } else {
                    alarmMgr.setExact(AlarmManager.RTC, triggerAtMillis, alarmIntent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

//        automatically restart the alarm when the device is rebooted.
//        ComponentName receiver = new ComponentName(context, BootCompletedReceiver.class);
//        PackageManager pm = context.getPackageManager();
//        pm.setComponentEnabledSetting(receiver,
//                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
//                PackageManager.DONT_KILL_APP);
package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.notification.MessageNotificationManager;
import com.metfone.selfcare.util.Log;

import java.lang.ref.WeakReference;

/**
 * Created by toanvk2 on 2/7/2018.
 */

public class ReplyNotificationReceiver extends BroadcastReceiver {
    private static final String TAG = ReplyNotificationReceiver.class.getSimpleName();
    private static String KEY_NOTIFICATION_ID = "noticiation_id";
    private static String KEY_MESSAGE_ID = "thread_id";

    public static Intent getReplyMessageIntent(Context context, int notificationId, int messageId) {
        Intent intent = new Intent(context, ReplyNotificationReceiver.class);
        intent.setAction(MessageNotificationManager.REPLY_ACTION);
        intent.putExtra(KEY_NOTIFICATION_ID, notificationId);
        intent.putExtra(KEY_MESSAGE_ID, messageId);
        return intent;
    }

    public ReplyNotificationReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (MessageNotificationManager.REPLY_ACTION.equals(intent.getAction())) {
            CharSequence message = MessageNotificationManager.getReplyMessage(intent);
            if (message != null) {
                int threadId = intent.getIntExtra(KEY_MESSAGE_ID, 0);
                int notifyId = intent.getIntExtra(KEY_NOTIFICATION_ID, -1);
                Log.d(TAG, "onReceive: --- REPLY_ACTION: " + threadId + " notifyId: " + notifyId + " message: " + message);
                new ReplyMessageAsyncTask((ApplicationController) context.getApplicationContext(),
                        message.toString(), threadId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                Log.d(TAG, "onReceive: --- message null");
            }
        } else {
            Log.d(TAG, "onReceive: --- action: " + intent.getAction());
        }
    }

    private class ReplyMessageAsyncTask extends AsyncTask<Void, Void, ThreadMessage> {
        private WeakReference<ApplicationController> applicationReference;
        private int threadId;
        private String message;

        private ReplyMessageAsyncTask(ApplicationController application, String message, int threadId) {
            this.applicationReference = new WeakReference<>(application);
            this.threadId = threadId;
            this.message = message;
        }

        @Override
        protected ThreadMessage doInBackground(Void... params) {
            Log.d(TAG, "ReplyMessageAsyncTask: " + threadId + " message: " + message);
            if (TextUtils.isEmpty(message)) {
                return null;
            }
            try {
                ApplicationController application = applicationReference.get();
                if (application != null) {
                    ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
                    if (!accountBusiness.isValidAccount()) return null;
                    while (!application.isDataReady()) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                    return application.getMessageBusiness().findThreadByThreadId(threadId);
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ThreadMessage result) {
            super.onPostExecute(result);
            ApplicationController application = applicationReference.get();
            if (result != null && application != null) {
                Log.d(TAG, "ReplyMessageAsyncTask onPostExecute: " + result.getId() + " --- " + result.getThreadName());
                sendTextMessage(application, message, result);
                application.getMessageBusiness().markAllMessageIsReadAndCheckSendSeen(result, result.getAllMessages());
                MessageNotificationManager.getInstance(application).clearNotifyThreadMessage(result);
            }
        }

        private void sendTextMessage(ApplicationController application, String content, ThreadMessage thread) {
            String myNumber = application.getReengAccountBusiness().getJidNumber();
            String receiver;
            if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                receiver = thread.getSoloNumber();
            } else {
                receiver = thread.getServerId();
            }
            if (!TextUtils.isEmpty(receiver)) {
                SoloSendTextMessage textMessage = new SoloSendTextMessage(thread, myNumber, receiver, content);
                textMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
                application.getMessageBusiness().insertNewMessageBeforeSend(thread,
                        thread.getThreadType(), textMessage);
                textMessage.setCState(-1);
                application.getMessageBusiness().sendXMPPMessage(textMessage, thread);
            } else {
                Log.e(TAG, "send message receiver is null");
            }
        }
    }
}
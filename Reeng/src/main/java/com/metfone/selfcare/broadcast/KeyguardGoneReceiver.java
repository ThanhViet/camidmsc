package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.metfone.selfcare.listeners.KeyguardGoneListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by ThanhNT on 2/5/2015.
 */
public class KeyguardGoneReceiver extends BroadcastReceiver {
    private static String TAG = KeyguardGoneReceiver.class.getSimpleName();
    private KeyguardGoneListener mKeyguardGoneListener;

    public void setKeyguardGoneListener(KeyguardGoneListener listener) {
        this.mKeyguardGoneListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive");
        if(mKeyguardGoneListener!=null){
            if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
                Log.i(TAG, "onKeyguardGone");
                mKeyguardGoneListener.onKeyguardGone();
            }
        }
    }
}

package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.call.CallGsmHelper;
import com.metfone.selfcare.util.Log;


public class PhoneCallReceiver extends BroadcastReceiver {
    private static final String TAG = PhoneCallReceiver.class.getSimpleName();
    private ApplicationController mApplication;
    //private TelephonyManager telManager;

    @Override
    public void onReceive(Context context, Intent intent) {

        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        Log.d(TAG, "onCallStateChanged: state: " + state);
        if (TextUtils.isEmpty(state)) {
            return;
        }
        this.mApplication = (ApplicationController) context.getApplicationContext();
        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            CallGsmHelper.getInstance(mApplication).setIncomingNumber(intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER));
            CallGsmHelper.getInstance(mApplication).setRing(true);
            pauseOtherMusic(mApplication);
        } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            CallGsmHelper.getInstance(mApplication).setCallReceived(true);
            pauseOtherMusic(mApplication);
        } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            Log.d(TAG, "EXTRA_STATE_IDLE: " + CallGsmHelper.getInstance(mApplication).getIncomingNumber()
                    + " ,ring: " + CallGsmHelper.getInstance(mApplication).isRing()
                    + " ,callReceived: " + CallGsmHelper.getInstance(mApplication).isCallReceived());
            if (CallGsmHelper.getInstance(mApplication).isRing() && !CallGsmHelper.getInstance(mApplication).isCallReceived()) {
                if (mApplication != null && !TextUtils.isEmpty(CallGsmHelper.getInstance(mApplication).getIncomingNumber()))
                    mApplication.getCallBusiness().onMissCall(CallGsmHelper.getInstance(mApplication).getIncomingNumber());
                CallGsmHelper.getInstance(mApplication).setRing(false);
            }
            CallGsmHelper.getInstance(mApplication).setIncomingNumber(null);
            CallGsmHelper.getInstance(mApplication).setCallReceived(false);
            if (mApplication != null && mApplication.getPlayMusicController() != null) {
                mApplication.getPlayMusicController().checkAndResumeMusic();
            }
            if (mApplication != null) {
                mApplication.getCallBusiness().setExistGsm(false);
            }
        }
       /* telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);*/
    }

    private void pauseOtherMusic(ApplicationController application) {
        if (application != null) {
            application.getCallBusiness().setExistGsm(true);
        }
        if (application != null && application.getPlayMusicController() != null) {
            if (application.getPlayMusicController().isPlaying()) {// dang play thi pause
                application.getPlayMusicController().setStateResumePlaying(true);
                application.getPlayMusicController().toggleMusic();
            }
            application.getPlayMusicController().setOtherAudioPlaying(true);
        }
    }

    /*private PhoneStateListener phoneListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            Log.d(TAG, "onCallStateChanged: " + incomingNumber + " state: " + state);
            ListenerHelper.getInstance().onGsmCallStateChanged(state, incomingNumber);
            if (mApplication == null) return;
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                ring = true;
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                callReceived = true;
            } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                Log.d(TAG, "onCallStateChanged: ring-- " + ring + " callReceived-- " + callReceived);
                if (ring && !callReceived) {
                    Log.d(TAG, "gsm miscall: " + incomingNumber);
                    mApplication.getCallBusiness().onMissCall(incomingNumber);
                    ring = false;
                }
                callReceived = false;
            }
            if (mApplication.getPlayMusicController() == null) {
                return;
            }
            if (state == TelephonyManager.CALL_STATE_IDLE) {
                mApplication.getPlayMusicController().checkAndResumeMusic();
            } else if (state == TelephonyManager.CALL_STATE_RINGING ||
                    state == TelephonyManager.CALL_STATE_OFFHOOK) {
                if (mApplication.getPlayMusicController().isPlaying()) {// dang play thi pause
                    mApplication.getPlayMusicController().setStateResumePlaying(true);
                    mApplication.getPlayMusicController().toggleMusic();
                }
                mApplication.getPlayMusicController().setOtherAudioPlaying(true);
            }
        }
    };*/
}
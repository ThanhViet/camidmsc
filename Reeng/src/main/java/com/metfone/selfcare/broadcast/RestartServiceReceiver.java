package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 11/23/2017.
 */

public class RestartServiceReceiver extends BroadcastReceiver {
    private static final String TAG = RestartServiceReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.f(TAG, "onReceive");
//        context.startService(new Intent(context.getApplicationContext(), IMService.class));

        ApplicationController applicationController = (ApplicationController) context.getApplicationContext();
        if(applicationController != null)
            applicationController.startIMService();
    }
}
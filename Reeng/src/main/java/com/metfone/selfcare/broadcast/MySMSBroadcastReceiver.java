package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MySMSBroadcastReceiver extends BroadcastReceiver {
    //   private static ArrayList<OTPReceiveListener> mSmsPasswordReceivedListener;
    private OTPReceiveListener otpReceiver;

    public void initOTPListener(OTPReceiveListener receiver) {
        this.otpReceiver = receiver;
    }

//    public static synchronized void addSMSReceivedListener(OTPReceiveListener listener) {
//        if (mSmsPasswordReceivedListener == null) {
//            mSmsPasswordReceivedListener = new ArrayList<>();
//        }
//        if (!mSmsPasswordReceivedListener.contains(listener))
//            mSmsPasswordReceivedListener.add(listener);
//    }
//
//    public static synchronized void removeSMSReceivedListener(OTPReceiveListener listener) {
//        if (mSmsPasswordReceivedListener == null) {
//            mSmsPasswordReceivedListener = new ArrayList<>();
//        }
//        if (mSmsPasswordReceivedListener.contains(listener))
//            mSmsPasswordReceivedListener.remove(listener);
//    }
//
//    public static synchronized boolean notifySmsChatReceived(String smsContent) {
//        boolean abortSms = false;
//        if (mSmsPasswordReceivedListener != null && !mSmsPasswordReceivedListener.isEmpty()) {
//            for (OTPReceiveListener listener : mSmsPasswordReceivedListener) {
//                abortSms = listener.onOTPReceived(smsContent);
//            }
//        }
//        return abortSms;
//    }
//
//    public static synchronized boolean notifySmsChatTimeout() {
//        boolean abortSms = false;
//        if (mSmsPasswordReceivedListener != null && !mSmsPasswordReceivedListener.isEmpty()) {
//            for (OTPReceiveListener listener : mSmsPasswordReceivedListener) {
//                abortSms = listener.onOTPTimeOut();
//            }
//        }
//        return abortSms;
//    }

    @Override
    public void onReceive(Context context, Intent intent) {
//        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
//            try {
//                Bundle extras = intent.getExtras();
//                Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
//                if (status != null) {
//                    switch (status.getStatusCode()) {
//                        case CommonStatusCodes.SUCCESS:
//                            // Get SMS message contents
//                            String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
//
//                            Pattern pattern = Pattern.compile("(\\d{6})");
//                            if (message != null) {
//                                Matcher matcher = pattern.matcher(message);
//                                String value = "";
//                                if (matcher.find()) {
//                                    value = matcher.group(1);
//                                }
//                                notifySmsChatReceived(value);
//                            }
//                            break;
//                        case CommonStatusCodes.TIMEOUT:
//                            notifySmsChatTimeout();
//                            break;
//                    }
//                }
//            } catch (Exception ex) {
//
//            }
//        }
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

            switch (status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    Intent messageIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                    if (otpReceiver != null && messageIntent != null) {
                        otpReceiver.onOTPReceived(messageIntent);
                    }
                    break;
                case CommonStatusCodes.TIMEOUT:
                    if (otpReceiver != null) {
                        otpReceiver.onOTPTimeOut();
                    }
                    break;

            }
        }

    }

    public interface OTPReceiveListener {

        void onOTPReceived(Intent intent);

        void onOTPTimeOut();
    }
}
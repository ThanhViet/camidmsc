package com.metfone.selfcare.broadcast;

import android.Manifest;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Telephony;
import android.text.TextUtils;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.InAppHelper;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 4/22/2016.
 */
public class SmsObserver extends ContentObserver {
    private static final String TAG = SmsObserver.class.getSimpleName();
    private Context mContext;
    private String lastSmsId = null;

    public SmsObserver(Handler handler, Context context) {
        super(handler);
        mContext = context;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Log.d(TAG, "onChange selfChange: " + selfChange);
        if (mContext == null) return;
        if (PermissionHelper.declinedPermission(mContext, Manifest.permission.READ_SMS)) {
            return;
        }
        Cursor cursor = null;
        try {
            cursor = mContext.getContentResolver().query(Uri.parse("content://sms"), null, null, null, null);
            if (cursor != null && cursor.moveToNext()) {
                String protocol = cursor.getString(cursor.getColumnIndex("protocol"));
                int type = cursor.getInt(cursor.getColumnIndex("type"));
                Log.d(TAG, "protocol: " + protocol + " ,type " + type);
                if (protocol != null || type != Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT) {
                    return;
                }
                String _id = cursor.getString(cursor.getColumnIndex("_id"));
                long time = cursor.getLong(cursor.getColumnIndex("date"));
                long differentTime = TimeHelper.getCurrentTime() - time;
                Log.d(TAG, "differentTime: " + differentTime);
                if (differentTime < 10000 && !TextUtils.isEmpty(_id) && !_id.equals(lastSmsId)) {
                    lastSmsId = _id;
                    String to = cursor.getString(cursor.getColumnIndex("address"));
                    String body = cursor.getString(cursor.getColumnIndex("body"));
                    Log.d(TAG, "to: " + to + " ,body: " + body);
                    ApplicationController application = (ApplicationController) mContext.getApplicationContext();
                    ProcessSmsSentAsyncTask smsSentAsyncTask = new ProcessSmsSentAsyncTask(application, to, body);
                    smsSentAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    Log.d(TAG, "trung id voi sms sent truoc do || diffirent time <10s khong xu ly");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        Log.d(TAG, "onChange selfChange: " + selfChange + " ,uri: " + uri);
    }

    private class ProcessSmsSentAsyncTask extends AsyncTask<Void, Void, String> {
        private String smsTo, smsContent;
        private ApplicationController mApplication;

        public ProcessSmsSentAsyncTask(ApplicationController application, String smsTo, String smsContent) {
            this.smsTo = smsTo;
            this.smsContent = smsContent;
            this.mApplication = application;
        }

        @Override
        protected String doInBackground(Void... params) {
            if (TextUtils.isEmpty(smsTo)) return null;
            ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
            if (!accountBusiness.isValidAccount()) return null;
            while (!mApplication.isDataReady()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                    getPhoneNumberProtocol(mApplication.getPhoneUtil(), smsTo, accountBusiness.getRegionCode());
            if (phoneNumberProtocol != null) {
                // lay number jid
                smsTo = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                        mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
                if (PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(), phoneNumberProtocol)) {
                    return smsTo;
                }
                Log.d(TAG, "smsTo not valid");
                return null;
                //notifySmsChatReceived(smsSender, smsMessage, smsContent);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (!TextUtils.isEmpty(result) && !result.equals(mApplication.getReengAccountBusiness().getJidNumber())) {//ok
                InAppHelper.getInstance(mApplication).requestLogActionSmsSent(result, smsContent);
            }
            super.onPostExecute(result);
        }
    }
}
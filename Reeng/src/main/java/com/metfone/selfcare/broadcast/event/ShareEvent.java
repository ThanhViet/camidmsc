package com.metfone.selfcare.broadcast.event;

import com.metfone.selfcare.model.tab_video.Video;

/**
 * Created by tuanha00 on 3/20/2018.
 */

public class ShareEvent extends MessageEvent{
    Video video;

    public ShareEvent(Video video) {
        this.video = video;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}

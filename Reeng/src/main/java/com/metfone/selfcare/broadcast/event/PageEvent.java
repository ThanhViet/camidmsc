package com.metfone.selfcare.broadcast.event;

/**
 * Created by tuanha00 on 3/21/2018.
 */

public class PageEvent extends MessageEvent {
    private String id;
    private PageType type;

    public PageEvent(PageType type) {
        this.type = type;
        this.id = "";
    }

    public PageEvent(String id, PageType type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PageType getType() {
        return type;
    }

    public void setType(PageType type) {
        this.type = type;
    }

    public enum PageType {
        TAB_HOME, TAB_CATEGORY
    }
}

package com.metfone.selfcare.broadcast.event;

/**
 * Created by tuanha00 on 3/21/2018.
 */

public class NetworkEvent extends MessageEvent {
    private boolean connected;

    public NetworkEvent(boolean connected) {
        this.connected = connected;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}

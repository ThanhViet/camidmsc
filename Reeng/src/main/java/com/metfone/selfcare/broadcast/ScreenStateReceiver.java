package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.metfone.selfcare.listeners.ScreenStateListener;

/**
 * Created by toanvk2 on 2/4/15.
 */
public class ScreenStateReceiver extends BroadcastReceiver {
    private ScreenStateListener mScreenStateListener;

    public void setStateListener(ScreenStateListener listener) {
        this.mScreenStateListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (mScreenStateListener != null) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                mScreenStateListener.onScreenOff();
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                mScreenStateListener.onScreenOn();
            }
        }
    }
}
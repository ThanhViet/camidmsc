package com.metfone.selfcare.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CallBusiness;

import java.lang.ref.WeakReference;

/**
 * Created by dainv00 on 4/16/2020
 */
public class DeclineCallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        WeakReference<ApplicationController> applicationReference = new WeakReference<ApplicationController>((ApplicationController) context.getApplicationContext());
        if(applicationReference.get() != null){
            CallBusiness callBusiness = applicationReference.get().getCallBusiness();
            callBusiness.handleDeclineCall(false);
        }
    }
}

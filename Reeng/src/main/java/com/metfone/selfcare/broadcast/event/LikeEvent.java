package com.metfone.selfcare.broadcast.event;

import com.metfone.selfcare.model.tab_video.Video;

/**
 * Created by tuanha00 on 3/20/2018.
 */

public class LikeEvent extends MessageEvent {
    Video video;

    public LikeEvent(Video video) {
        this.video = video;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}

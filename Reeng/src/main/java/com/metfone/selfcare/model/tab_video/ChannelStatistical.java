package com.metfone.selfcare.model.tab_video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ChannelStatistical implements Serializable {

    @SerializedName("categoryid")
    @Expose
    private String categoryId;
    @SerializedName("totalPoint")
    @Expose
    private String totalPoint;
    @SerializedName("is_follow")
    @Expose
    private int isFollow;
    @SerializedName("numfollow")
    @Expose
    private long numfollow;
    @SerializedName("is_registered")
    @Expose
    private long isRegistered;
    @SerializedName("type")
    @Expose
    private long type;
    @SerializedName("isMyChannel")
    @Expose
    private int isMyChannel;
    @SerializedName("channelRevenue")
    @Expose
    private ArrayList<ChannelRevenue> channelRevenue = null;
    @SerializedName("VideoRevenue")
    @Expose
    private ArrayList<VideoRevenue> videoRevenue = null;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint;
    }

    public int getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(int isFollow) {
        this.isFollow = isFollow;
    }

    public long getNumfollow() {
        return numfollow;
    }

    public void setNumfollow(long numfollow) {
        this.numfollow = numfollow;
    }

    public long getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(long isRegistered) {
        this.isRegistered = isRegistered;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public int getIsMyChannel() {
        return isMyChannel;
    }

    public void setIsMyChannel(int isMyChannel) {
        this.isMyChannel = isMyChannel;
    }

    public ArrayList<ChannelRevenue> getChannelRevenue() {
        return channelRevenue;
    }

    public void setChannelRevenue(ArrayList<ChannelRevenue> channelRevenue) {
        this.channelRevenue = channelRevenue;
    }

    public ArrayList<VideoRevenue> getVideoRevenue() {
        return videoRevenue;
    }

    public void setVideoRevenue(ArrayList<VideoRevenue> videoRevenue) {
        this.videoRevenue = videoRevenue;
    }

}

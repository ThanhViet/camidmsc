package com.metfone.selfcare.model.account;

import com.metfone.selfcare.model.BaseDataRequest;

public class CheckPhoneRequest extends BaseDataRequest<CheckPhoneInfo> {
    public CheckPhoneRequest(CheckPhoneInfo wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        super(wsRequest, apiKey, sessionId, username, wsCode);
    }
}

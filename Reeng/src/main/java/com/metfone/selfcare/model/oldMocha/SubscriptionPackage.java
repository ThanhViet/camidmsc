package com.metfone.selfcare.model.oldMocha;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionPackage {

    @SerializedName("domain_file")
    @Expose
    private String domainFile;
    @SerializedName("kservice")
    @Expose
    private String kservice;
    @SerializedName("kmedia")
    @Expose
    private String kmedia;
    @SerializedName("kimage")
    @Expose
    private String kimage;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("domain_on_media")
    @Expose
    private String domainOnMedia;
    @SerializedName("ssl")
    @Expose
    private Integer ssl;
    @SerializedName("domain_on_media_v1")
    @Expose
    private String domainOnMediaV1;
    @SerializedName("domain_file_v1")
    @Expose
    private String domainFileV1;
    @SerializedName("domain_img_v1")
    @Expose
    private String domainImgV1;
    @SerializedName("domain_kmusic")
    @Expose
    private String domainKmusic;
    @SerializedName("domain_kmovies")
    @Expose
    private String domainKmovies;
    @SerializedName("domain_netnews")
    @Expose
    private String domainNetnews;
    @SerializedName("domain_tiin")
    @Expose
    private String domainTiin;
    @SerializedName("domain_mcvideo")
    @Expose
    private String domainMcvideo;
    @SerializedName("domain_kmusic_search")
    @Expose
    private String domainKmusicSearch;
    @SerializedName("domain_msg")
    @Expose
    private String domainMsg;
    @SerializedName("content")
    @Expose
    private String content;

    public String getDomainFile() {
        return domainFile;
    }

    public void setDomainFile(String domainFile) {
        this.domainFile = domainFile;
    }

    public String getKservice() {
        return kservice;
    }

    public void setKservice(String kservice) {
        this.kservice = kservice;
    }

    public String getKmedia() {
        return kmedia;
    }

    public void setKmedia(String kmedia) {
        this.kmedia = kmedia;
    }

    public String getKimage() {
        return kimage;
    }

    public void setKimage(String kimage) {
        this.kimage = kimage;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDomainOnMedia() {
        return domainOnMedia;
    }

    public void setDomainOnMedia(String domainOnMedia) {
        this.domainOnMedia = domainOnMedia;
    }

    public Integer getSsl() {
        return ssl;
    }

    public void setSsl(Integer ssl) {
        this.ssl = ssl;
    }

    public String getDomainOnMediaV1() {
        return domainOnMediaV1;
    }

    public void setDomainOnMediaV1(String domainOnMediaV1) {
        this.domainOnMediaV1 = domainOnMediaV1;
    }

    public String getDomainFileV1() {
        return domainFileV1;
    }

    public void setDomainFileV1(String domainFileV1) {
        this.domainFileV1 = domainFileV1;
    }

    public String getDomainImgV1() {
        return domainImgV1;
    }

    public void setDomainImgV1(String domainImgV1) {
        this.domainImgV1 = domainImgV1;
    }

    public String getDomainKmusic() {
        return domainKmusic;
    }

    public void setDomainKmusic(String domainKmusic) {
        this.domainKmusic = domainKmusic;
    }

    public String getDomainKmovies() {
        return domainKmovies;
    }

    public void setDomainKmovies(String domainKmovies) {
        this.domainKmovies = domainKmovies;
    }

    public String getDomainNetnews() {
        return domainNetnews;
    }

    public void setDomainNetnews(String domainNetnews) {
        this.domainNetnews = domainNetnews;
    }

    public String getDomainTiin() {
        return domainTiin;
    }

    public void setDomainTiin(String domainTiin) {
        this.domainTiin = domainTiin;
    }

    public String getDomainMcvideo() {
        return domainMcvideo;
    }

    public void setDomainMcvideo(String domainMcvideo) {
        this.domainMcvideo = domainMcvideo;
    }

    public String getDomainKmusicSearch() {
        return domainKmusicSearch;
    }

    public void setDomainKmusicSearch(String domainKmusicSearch) {
        this.domainKmusicSearch = domainKmusicSearch;
    }

    public String getDomainMsg() {
        return domainMsg;
    }

    public void setDomainMsg(String domainMsg) {
        this.domainMsg = domainMsg;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
package com.metfone.selfcare.model.tab_video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChannelRevenue implements Serializable{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("spoint")
    @Expose
    private String spoint;
    @SerializedName("rangeTime")
    @Expose
    private String rangeTime;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSpoint() {
        return spoint;
    }

    public void setSpoint(String spoint) {
        this.spoint = spoint;
    }

    public String getRangeTime() {
        return rangeTime;
    }

    public void setRangeTime(String rangeTime) {
        this.rangeTime = rangeTime;
    }

}

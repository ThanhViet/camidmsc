package com.metfone.selfcare.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DataPackagesResponse implements Serializable {
    @SerializedName("code")
    private String code;
    @SerializedName("desc")
    private String desc;
    @SerializedName("label_extend")
    private String labelExtend;
    @SerializedName("result")
    private ArrayList<ListDataPackages> result;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLabelExtend() {
        return labelExtend;
    }

    public void setLabelExtend(String labelExtend) {
        this.labelExtend = labelExtend;
    }

    public ArrayList<ListDataPackages> getResult() {
        return result;
    }

    public void setResult(ArrayList<ListDataPackages> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "DataPackagesResponse{" +
                "code='" + code + '\'' +
                ", desc='" + desc + '\'' +
                ", labelExtend='" + labelExtend + '\'' +
                ", result=" + result +
                '}';
    }

    public static class ListDataPackages implements Serializable {
        @SerializedName("title")
        private String title;
        @SerializedName("list_package")
        private ArrayList<DataPackageInfo> listPackages;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public ArrayList<DataPackageInfo> getListPackages() {
            return listPackages;
        }

        public void setListPackages(ArrayList<DataPackageInfo> listPackages) {
            this.listPackages = listPackages;
        }

        @Override
        public String toString() {
            return "ListDataPackages{" +
                    "title='" + title + '\'' +
                    ", listPackages=" + listPackages +
                    '}';
        }
    }

    public static class DataPackageInfo implements Serializable {
        //type = 0: text, = 1: deeplink, = 2: mở sms confirm.

        @SerializedName("id")
        private int id;
        @SerializedName("name")
        private String name;
        @SerializedName("type")
        private int type;
        @SerializedName("short_desc")
        private String shortDesc;
        @SerializedName("desc")
        private String desc;
        @SerializedName("sms_command")
        private String smsCommand;
        @SerializedName("sms_codes")
        private String smsCodes;
        @SerializedName("label_button")
        private String labelButton;
        @SerializedName("url")
        private String url;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getShortDesc() {
            return shortDesc;
        }

        public void setShortDesc(String shortDesc) {
            this.shortDesc = shortDesc;
        }

        public String getDesc() {
            if (desc == null) desc = "";
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getSmsCommand() {
            return smsCommand;
        }

        public void setSmsCommand(String smsCommand) {
            this.smsCommand = smsCommand;
        }

        public String getSmsCodes() {
            return smsCodes;
        }

        public void setSmsCodes(String smsCodes) {
            this.smsCodes = smsCodes;
        }

        public String getLabelButton() {
            return labelButton;
        }

        public void setLabelButton(String labelButton) {
            this.labelButton = labelButton;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public boolean isClickable() {
            return type == 1 || type == 2;
        }

        @Override
        public String toString() {
            return "DataPackageInfo{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", type=" + type +
                    ", shortDesc='" + shortDesc + '\'' +
                    ", desc='" + desc + '\'' +
                    ", smsCommand='" + smsCommand + '\'' +
                    ", smsCodes='" + smsCodes + '\'' +
                    ", labelButton='" + labelButton + '\'' +
                    ", url='" + url + '\'' +
                    '}';
        }
    }

/*
{
	"code": "200",
	"desc": "successfull",
	"label_extend": "Giói cước gia hạn tự động",
	"result": [{
		"title": "Gói cước data trong Mocha",
		"list_package": [{
			"id": 1,
			"name": "Gói Mocha video",
			"short_desc": "MIỄN CƯỚC DATA",
			"desc": "Giải trí thả ga – không lo Data với ngàn phim hay, hài xịn, nhạc đỉnh, tin tức nóng hổi trên Mocha.\r\nGiá cước: 10.000đ/7 ngày\r\n",
			"sms_command": "P7",
			"sms_codes": "5005",
			"label_button": "Đăng ký",
			"type": 1,
			"url": "mocha://fakemo?msg=Gi%E1%BA%A3i%20tr%C3%AD%20th%E1%BA%A3%20ga%20%E2%80%93%20Kh%C3%B4ng%20lo%20Data%20v%E1%BB%9Bi%20ng%C3%A0n%20n%E1%BB%99i%20dung%20phim%20hay%2C%20h%C3%A0i%20x%E1%BB%8Bn%2C%20nh%E1%BA%A1c%20%C4%91%E1%BB%89nh%E2%80%A6%2C%20ch%E1%BB%89%2010.000%C4%91%2Ftu&title=%C4%90%C4%83ng%20k%C3%BD%20Mocha%20Gi%E1%BA%A3i%20tr%C3%AD&cmd=P7&lOk=%C4%90%C4%83ng%20k%C3%BD&lCancel=H%E1%BB%A7y"
		},
		{
			"id": 2,
			"name": "Gói Game TV",
			"short_desc": "MIỄN CƯỚC DATA",
			"desc": "Thoải mái xem Livestream, highlight, video Game cực hot không tốn Data trên Mocha. \r\nGiá cước: 10.000đ/7 ngày\r\n",
			"sms_command": "GA7",
			"sms_codes": "5005",
			"label_button": "Đăng ký",
			"type": 0,
			"url": "mocha://fakemo?msg=X%C3%A1c%20nh%E1%BA%ADn%20%C4%91%C4%83ng%20k%C3%BD%20g%C3%B3i%20ng%C3%A0y%20GameTV.%20%3Cbr%2F%3E%28Ph%C3%AD%20DV%3A%203.000%C4%91%2Fng%C3%A0y%2C%20gia%20h%E1%BA%A1n%20theo%20ng%C3%A0y%29&title=%C4%90%C4%83ng%20k%C3%BD%20g%C3%B3i%20GAMETV&cmd=GAMETVAPP&lOk=%C4%90%C4%83ng%20k%C3%BD&lCancel=H%E1%BB%A7y"
		}]
	},
	{
		"title": "Gói cước internet 3G/4G",
		"list_package": [{
			"id": 3,
			"name": "Gói M70",
			"short_desc": "7GB DATA TỐC ĐỘ CAO",
			"desc": "(3GB truy cập Internet và 4GB truy cập ứng dụng Keeng, Mocha) \r\nGiá cước: 70.000đ/30 ngày\r\n",
			"sms_command": "M70",
			"sms_codes": "191",
			"label_button": "Đăng ký",
			"type": 0,
			"url": "mocha://fakemo?msg=X%C3%A1c%20nh%E1%BA%ADn%20%C4%91%C4%83ng%20k%C3%BD%20g%C3%B3i%20M70&title=X%C3%A1c%20nh%E1%BA%ADn%20%C4%91%C4%83ng%20k%C3%BD%20g%C3%B3i%20M70&cmd=M70&lOk=%C4%90%C4%83ng%20k%C3%BD&lCancel=H%E1%BB%A7y"
		},
		{
			"id": 4,
			"name": "Gói M90",
			"short_desc": "9GB DATA TỐC ĐỘ CAO",
			"desc": "(5GB truy cập Internet và 4GB truy cập ứng dụng Keeng, Mocha) \r\nGiá cước: 90.000đ/30 ngày\r\n",
			"sms_command": "M90",
			"sms_codes": "191",
			"label_button": "Đăng ký",
			"type": 1,
			"url": "mocha://fakemo?msg=X%C3%A1c%20nh%E1%BA%ADn%20%C4%91%C4%83ng%20k%C3%BD%20g%C3%B3i%20M90&title=X%C3%A1c%20nh%E1%BA%ADn%20%C4%91%C4%83ng%20k%C3%BD%20g%C3%B3i%20M90&cmd=M90&lOk=%C4%90%C4%83ng%20k%C3%BD&lCancel=H%E1%BB%A7y"
		},
		{
			"id": 5,
			"name": "Gói M125",
			"short_desc": "Gói M125\r\n12GB DATA TỐC ĐỘ CAO\r\n",
			"desc": "(8GB truy cập Internet và 4GB truy cập ứng dụng Keeng, Mocha) \r\nGiá cước: 125.000đ/30 ngày\r\n",
			"sms_command": "M125",
			"sms_codes": "191",
			"label_button": "Đăng ký",
			"type": 0,
			"url": "mocha://fakemo?msg=X%C3%A1c%20nh%E1%BA%ADn%20%C4%91%C4%83ng%20k%C3%BD%20g%C3%B3i%20M125&title=X%C3%A1c%20nh%E1%BA%ADn%20%C4%91%C4%83ng%20k%C3%BD%20g%C3%B3i%20M125&cmd=M125&lOk=%C4%90%C4%83ng%20k%C3%BD&lCancel=H%E1%BB%A7y%20"
		}]
	}]
}
* */
}

package com.metfone.selfcare.model.camid;

public class HistoryCharge {
    private HistoryChargeType type;

    public HistoryCharge(HistoryChargeType type) {
        this.type = type;
    }

    public HistoryChargeType getType() {
        return type;
    }

    public void setType(HistoryChargeType type) {
        this.type = type;
    }
}

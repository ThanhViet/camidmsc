package com.metfone.selfcare.model.home;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;

public class AdsMainTab implements Serializable {

    @SerializedName("smsCodes")
    private String smsCodes;
    @SerializedName("smsCommand")
    private String smsCommand;
    @SerializedName("isSMS")
    private int isSMS;
    @SerializedName("command")
    private String command;
    @SerializedName("confirmString")
    private String confirmString;
    @SerializedName("titleButton")
    private String titleButton;
    @SerializedName("content")
    private String content;
    @SerializedName("title")
    private String title;
    @SerializedName("campId")
    private int campId;
    @SerializedName("lStartTime")
    private long startTimeLong;
    @SerializedName("lEndTime")
    private long endTimeLong;
    @SerializedName("tab_type")
    private String tabType;
    @SerializedName("btnOk")
    private String buttonOk;
    @SerializedName("btnCancel")
    private String buttonCancel;
    @SerializedName("btnConfirmOk")
    private String buttonConfirmOk;
    @SerializedName("btnConfirmCancel")
    private String buttonConfirmCancel;
    @SerializedName("tab_id")
    private String tabId;

    public AdsMainTab() {
    }

    public String getSmsCodes() {
        return smsCodes;
    }

    public void setSmsCodes(String smsCodes) {
        this.smsCodes = smsCodes;
    }

    public String getSmsCommand() {
        return smsCommand;
    }

    public void setSmsCommand(String smsCommand) {
        this.smsCommand = smsCommand;
    }

    public int getIsSMS() {
        return isSMS;
    }

    public void setIsSMS(int isSMS) {
        this.isSMS = isSMS;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getConfirmString() {
        return confirmString;
    }

    public void setConfirmString(String confirmString) {
        this.confirmString = confirmString;
    }

    public String getTitleButton() {
        return titleButton;
    }

    public void setTitleButton(String titleButton) {
        this.titleButton = titleButton;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCampId() {
        return campId;
    }

    public void setCampId(int campId) {
        this.campId = campId;
    }

    public boolean isEnable() {
        long currentTime = System.currentTimeMillis();
        return Utilities.notEmpty(getContent()) && startTimeLong <= currentTime && currentTime <= endTimeLong;
    }

    public boolean isSMS() {
        return isSMS == 1;
    }

    public String getTabType() {
        return tabType;
    }

    public String getButtonOk() {
        return buttonOk;
    }

    public void setButtonOk(String buttonOk) {
        this.buttonOk = buttonOk;
    }

    public String getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(String buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public String getButtonConfirmOk() {
        return buttonConfirmOk;
    }

    public void setButtonConfirmOk(String buttonConfirmOk) {
        this.buttonConfirmOk = buttonConfirmOk;
    }

    public String getButtonConfirmCancel() {
        return buttonConfirmCancel;
    }

    public void setButtonConfirmCancel(String buttonConfirmCancel) {
        this.buttonConfirmCancel = buttonConfirmCancel;
    }

    public String getTabId() {
        return tabId;
    }

    @Override
    public String toString() {
        return "AdsMainTab{" +
                "smsCodes='" + smsCodes + '\'' +
                ", smsCommand='" + smsCommand + '\'' +
                ", isSMS=" + isSMS +
                ", command='" + command + '\'' +
                ", confirmString='" + confirmString + '\'' +
                ", titleButton='" + titleButton + '\'' +
                ", content='" + content + '\'' +
                ", title='" + title + '\'' +
                ", campId=" + campId +
                '}';
    }
}

package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TotalComplaint implements Serializable {

    @SerializedName("totalComplaint")
    @Expose
    public String totalComplaint;

    public TotalComplaint(String totalComplaint){
        this.totalComplaint = totalComplaint;
    }

}

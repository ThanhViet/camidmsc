package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BaseUserResponseData {
    @SerializedName("user")
    UserInfo user;
    @SerializedName("services")
    List<Services> services;
    @SerializedName("identity_providers")
    List<IdentifyProvider> identifyProviders;

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public List<Services> getServices() {
        return services;
    }

    public void setServices(List<Services> services) {
        this.services = services;
    }

    public List<IdentifyProvider> getIdentifyProviders() {
        return identifyProviders;
    }

    public void setIdentifyProviders(List<IdentifyProvider> identifyProviders) {
        this.identifyProviders = identifyProviders;
    }

    public BaseUserResponseData(UserInfo user, List<Services> services, List<IdentifyProvider> identifyProviders) {
        this.user = user;
        this.services = services;
        this.identifyProviders = identifyProviders;
    }
}

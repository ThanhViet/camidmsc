package com.metfone.selfcare.model.camid;

public class DataHistoryChargeDetail extends HistoryCharge {
    private HistoryDataInfo historyDataInfo;

    public DataHistoryChargeDetail(HistoryChargeType type, HistoryDataInfo historyDataInfo) {
        super(type);
        this.historyDataInfo = historyDataInfo;
    }

    public HistoryDataInfo getHistoryDataInfo() {
        return historyDataInfo;
    }

    public void setHistoryDataInfo(HistoryDataInfo historyDataInfo) {
        this.historyDataInfo = historyDataInfo;
    }
}

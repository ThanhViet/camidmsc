package com.metfone.selfcare.model.tab_video;

import java.io.Serializable;

public class FilmGroup implements Serializable {

    private String groupId;
    private String groupName;
    private String groupImage;
    private String currentVideo;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    public String getCurrentVideo() {
        return currentVideo;
    }

    public void setCurrentVideo(String currentVideo) {
        this.currentVideo = currentVideo;
    }

}

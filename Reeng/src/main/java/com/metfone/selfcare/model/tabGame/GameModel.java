package com.metfone.selfcare.model.tabGame;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GameModel implements Serializable {
    @SerializedName("id")
    private long id;
    @SerializedName("name")
    private String name;
    @SerializedName("link")
    private String link;
    @SerializedName("description")
    private String description;
    @SerializedName("visible")
    private long visible;
    @SerializedName("order")
    private long order;
    @SerializedName("status")
    private String status;
    @SerializedName("name_km")
    private String nameKM;
    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("updated_date")
    private String updatedDate;
    @SerializedName("icon_url")
    private String iconURL;
    @SerializedName("best_score")
    private long bestScore;

    public GameModel() {
    }

    public long getID() {
        return id;
    }

    public long getBestScore() {
        return bestScore;
    }

    public void setBestScore(long bestScore) {
        this.bestScore = bestScore;
    }

    public void setID(long value) {
        this.id = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String value) {
        this.link = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public long getVisible() {
        return visible;
    }

    public void setVisible(long value) {
        this.visible = value;
    }

    public long getOrder() {
        return order;
    }

    public void setOrder(long value) {
        this.order = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String value) {
        this.status = value;
    }

    public String getNameKM() {
        return nameKM;
    }

    public void setNameKM(String value) {
        this.nameKM = value;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String value) {
        this.createdDate = value;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String value) {
        this.updatedDate = value;
    }

    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String value) {
        this.iconURL = value;
    }
}

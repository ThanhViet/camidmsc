package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PhoneLinked implements Serializable {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("metfonePlus")
    @Expose
    private Integer metfonePlus;
    @SerializedName("user_service_id")
    @Expose
    private Integer userServiceId;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getMetfonePlus() {
        return metfonePlus;
    }

    public void setMetfonePlus(Integer metfonePlus) {
        this.metfonePlus = metfonePlus;
    }

    public Integer getUserServiceId() {
        return userServiceId;
    }

    public void setUserServiceId(Integer userServiceId) {
        this.userServiceId = userServiceId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

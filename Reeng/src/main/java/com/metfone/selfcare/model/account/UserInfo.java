package com.metfone.selfcare.model.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {
    private String phone_number = "";
    private String full_name = "";
    private String username;
    private String email;
    private String address;
    private int gender;
    private String avatar = "";
    private String verified;
    private String province;
    private String district;
    private String code;
    private int user_id;
    private String date_of_birth = "";
    private String identity_number = "";
    private String contact;

    public UserInfo(String username, String email, String address, int gender, String avatar,
                    String verified, String province, String district, int user_id,
                    String phone_number, String full_name, String date_of_birth,
                    String identity_number, String contact) {
        this.username = username;
        this.email = email;
        this.address = address;
        this.gender = gender;
        this.avatar = avatar;
        this.verified = verified;
        this.province = province;
        this.district = district;
        this.user_id = user_id;
        this.phone_number = phone_number;
        this.full_name = full_name;
        this.date_of_birth = date_of_birth;
        this.identity_number = identity_number;
        this.contact = contact;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getIdentity_number() {
        return identity_number;
    }

    public void setIdentity_number(String identity_number) {
        this.identity_number = identity_number;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}

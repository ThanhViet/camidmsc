package com.metfone.selfcare.model.camid;

public enum  HistoryChargeType {
    TOTAL_CHARGE("Total charge", 1, "total"),
    CALLING_CHARGE("Calling charge", 2, "calling"),
    SMS_CHARGE("SMS charge", 2, "sms"),
    VAS_CHARGE("VAS charge", 2, "vas"),
    DATA_CHARGE("Data charge", 2, "data"),
    MY_SERVICES("My services", 3, "my_services"),
    DATA_CHARGE_PARENT("Data charge parent", 4, "data_charge_parent"),
    DATA_CHARGE_CHILDREN("Data charge children", 5, "data_charge_children");

    public String value;
    public int typeGroup;
    public String type;

    HistoryChargeType(String value, int typeGroup, String type) {
        this.value = value;
        this.typeGroup = typeGroup;
        this.type = type;
    }
}

package com.metfone.selfcare.model.tab_video;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 7/25/2018.
 */

public class AdsRegisterVip implements Serializable {

    @SerializedName("categoryId")
    private String categoryId;
    @SerializedName("title")
    private String title;
    @SerializedName("content")
    private String content;
    @SerializedName("confirmString")
    private String confirmString;
    @SerializedName("command")
    private String command;
    @SerializedName("isSMS")
    private int isSMS;
    @SerializedName("smsCommand")
    private String smsCommand;
    @SerializedName("smsCodes")
    private String smsCodes;
    @SerializedName("countClick")
    private int countClick;
    @SerializedName("numClick")
    private int numberShowDialog;
    @SerializedName("titleButton")
    private String titleButton;
    @SerializedName("dateLastClick")
    private long dateLastClick;

    @SerializedName("deeplink")
    private String deeplink;

    // TODO: 5/18/2020 Bổ sung
    private long timeFirstShow =0;

    public long getTimeFirstShow() {
        return timeFirstShow;
    }

    public void setTimeFirstShow(long timeFirstShow) {
        this.timeFirstShow = timeFirstShow;
    }

    public int getCountTimeShow() {
        return countTimeShow;
    }

    public void setCountTimeShow(int countTimeShow) {
        this.countTimeShow = countTimeShow;
    }

    private int countTimeShow =0;


    public AdsRegisterVip() {
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getConfirmString() {
        return confirmString;
    }

    public String getCommand() {
        return command;
    }

    public boolean isSMS() {
        return isSMS == 1;
    }

    public boolean isDeeplink() {
        return isSMS == 2;
    }

    public String getSmsCommand() {
        return smsCommand;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setConfirmString(String confirmString) {
        this.confirmString = confirmString;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setSMS(int SMS) {
        this.isSMS = SMS;
    }

    public void setSmsCommand(String smsCommand) {
        this.smsCommand = smsCommand;
    }

    public String getSmsCodes() {
        return smsCodes;
    }

    public void setSmsCodes(String smsCodes) {
        this.smsCodes = smsCodes;
    }

    public int getCountClick() {
        return countClick;
    }

    public void setCountClick(int countClick) {
        this.countClick = countClick;
    }

    public int getNumberShowDialog() {
        return numberShowDialog;
    }

    public void setNumberShowDialog(int numberShowDialog) {
        this.numberShowDialog = numberShowDialog;
    }

    public String getTitleButton() {
        return titleButton;
    }

    public void setTitleButton(String titleButton) {
        this.titleButton = titleButton;
    }

    public long getDateLastClick() {
        return dateLastClick;
    }

    public void setDateLastClick(long dateLastClick) {
        this.dateLastClick = dateLastClick;
    }


    public String getDeeplink() {
        return deeplink;
    }

    public void setDeeplink(String deeplink) {
        this.deeplink = deeplink;
    }
}

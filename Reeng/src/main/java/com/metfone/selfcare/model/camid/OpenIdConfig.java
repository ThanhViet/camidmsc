package com.metfone.selfcare.model.camid;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpenIdConfig {
    private Integer id;
    private String createdTime;
    private String updatedTime;
    private String state;
    private String key;
    private String value;
    private String description;
}
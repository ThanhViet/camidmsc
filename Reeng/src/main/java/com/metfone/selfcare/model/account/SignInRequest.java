package com.metfone.selfcare.model.account;

import com.metfone.selfcare.model.BaseDataRequest;

public class SignInRequest extends BaseDataRequest<SignIn> {
    public SignInRequest(SignIn wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        super(wsRequest, apiKey, sessionId, username, wsCode);
    }
}

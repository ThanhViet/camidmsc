package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SignIn  implements  Serializable {
    @SerializedName("password")
    private String password;
    @SerializedName("phone_number")
    private String phone_number;
    @SerializedName("otp")
    private String otp;
    @SerializedName("type")
    private String type;

    @SerializedName("social_token")
    private String socialToken;

    public SignIn() {
    }

    public SignIn(String password, String phone_number, String otp, String type, String socialToken) {
        this.password = password;
        this.phone_number = phone_number;
        this.otp = otp;
        this.type = type;
        this.socialToken = socialToken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSocialToken() {
        return socialToken;
    }

    public void setSocialToken(String socialToken) {
        this.socialToken = socialToken;
    }
}

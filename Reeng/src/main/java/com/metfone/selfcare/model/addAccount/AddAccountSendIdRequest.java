package com.metfone.selfcare.model.addAccount;

public class AddAccountSendIdRequest {
    String phone_number;

    public AddAccountSendIdRequest(String phone_number) {
        this.phone_number = phone_number;
    }
}

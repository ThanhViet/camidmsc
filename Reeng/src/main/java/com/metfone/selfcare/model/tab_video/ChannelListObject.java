/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/24
 *
 */

package com.metfone.selfcare.model.tab_video;

import java.io.Serializable;
import java.util.ArrayList;

public class ChannelListObject implements Serializable {
    private static final long serialVersionUID = -385384722936032925L;
    ArrayList<Channel> channels;

    public ArrayList<Object> getChannelsListObject() {
        ArrayList<Object> list = new ArrayList<>();
        list.addAll(channels);
        return list;
    }

    public void setChannels(ArrayList<Channel> channels) {
        this.channels = channels;
    }

    @Override
    public String toString() {
        return "{" +
                "channels=" + channels +
                '}';
    }
}

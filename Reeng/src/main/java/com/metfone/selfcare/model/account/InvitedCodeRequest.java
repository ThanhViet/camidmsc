package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.camid.request.BaseRequest;

import lombok.Getter;
import lombok.Setter;

public class InvitedCodeRequest extends BaseRequest<InvitedCodeRequest.Request> {
    @Getter
    @Setter
    public class Request{
        @SerializedName("code")
        String code;
    }
}

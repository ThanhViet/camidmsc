package com.metfone.selfcare.model.tabMovie;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubtitleAudio implements Serializable {

    @SerializedName("id")
    private String mId;

    @SerializedName("video_id")
    private String mVideoId;

    @SerializedName("language_code")
    private String mLanguageCode;

    @SerializedName("path")
    private String mPath;

    public String getId() {
        return mId;
    }

    public String getVideoId() {
        return mVideoId;
    }

    public String getLanguageCode() {
        return mLanguageCode;
    }

    public String getPath() {
        return mPath;
    }

    public SubtitleAudio(String id, String videoId, String languageCode, String path) {
        this.mId = id;
        this.mVideoId = videoId;
        this.mLanguageCode = languageCode;
        this.mPath = path;
    }
}

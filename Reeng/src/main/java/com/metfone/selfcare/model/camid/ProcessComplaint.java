package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProcessListResponse;

import java.io.Serializable;

/**
 * Model for {@link WsGetProcessListResponse}
 */
public class ProcessComplaint implements Serializable {
    @SerializedName("paramCode")
    private String paramCode;

    @SerializedName("paramId")
    private int paramId;

    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    public int getParamId() {
        return paramId;
    }

    public void setParamId(int paramId) {
        this.paramId = paramId;
    }
}

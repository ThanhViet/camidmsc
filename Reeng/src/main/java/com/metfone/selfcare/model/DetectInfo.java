package com.metfone.selfcare.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Locale;

public class DetectInfo implements Parcelable {
    private String fullname;
    private String dob;
    private String sex;
    private String idNumber;
    private String address;
    private String expireDate;

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    private String nationality;

    public DetectInfo() {
        //default
    }

    protected DetectInfo(Parcel in) {
        fullname = in.readString();
        dob = in.readString();
        sex = in.readString();
        idNumber = in.readString();
        address = in.readString();
        expireDate = in.readString();
        nationality = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullname);
        dest.writeString(dob);
        dest.writeString(sex);
        dest.writeString(idNumber);
        dest.writeString(address);
        dest.writeString(expireDate);
        dest.writeString(nationality);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetectInfo> CREATOR = new Creator<DetectInfo>() {
        @Override
        public DetectInfo createFromParcel(Parcel in) {
            return new DetectInfo(in);
        }

        @Override
        public DetectInfo[] newArray(int size) {
            return new DetectInfo[size];
        }
    };

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getGender() {
        if ("male".equals(sex.toLowerCase(Locale.ROOT))) {
            return 1;
        } else if ("female".equals(sex.toLowerCase(Locale.ROOT))) {
            return 2;
        } else {
            return 0;
        }
    }
}

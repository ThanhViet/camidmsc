package com.metfone.selfcare.model.account;

import com.metfone.selfcare.model.BaseDataRequest;

public class SignUpRequest extends BaseDataRequest<SignUpRequestData> {

    public SignUpRequest(SignUpRequestData wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        super(wsRequest, apiKey, sessionId, username, wsCode);
    }
}

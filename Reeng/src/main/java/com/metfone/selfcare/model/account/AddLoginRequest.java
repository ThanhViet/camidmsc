package com.metfone.selfcare.model.account;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddLoginRequest {
    String phone_number;
    String otp;
    String password;

    public AddLoginRequest(String phone_number, String otp, String password) {
        this.phone_number = phone_number;
        this.otp = otp;
        this.password = password;
    }
}

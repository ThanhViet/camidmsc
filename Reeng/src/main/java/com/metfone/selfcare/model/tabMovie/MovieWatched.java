/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/2/15
 *
 */

package com.metfone.selfcare.model.tabMovie;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MovieWatched implements Serializable {
    @SerializedName(value = "id", alternate = "id_phim")
    private String id;

    @SerializedName("id_group")
    private String idGroup;

    @SerializedName("id_part")
    private String idPart;

    @SerializedName("name")
    private String name;

    @SerializedName("image_path")
    private String imagePath;

    @SerializedName("poster_path")
    private String posterPath;

    @SerializedName("time_seek")
    private String timeSeek;

    @SerializedName("duration")
    private String duration;

    @SerializedName("created_at")
    private String createdAt;

    private String typeFilm;

    @SerializedName("total")
    private String totalEpisodes;

    @SerializedName("current_film")
    private String currentEpisode;

    @SerializedName("link_wap")
    private String link;

    @SerializedName("is_subtitle")
    private String isSubtitle;

    @SerializedName("is_narrative")
    private String isNarrative;

    @SerializedName("throughput")
    private String throughput;
    private String totalTimePlay;

    public MovieWatched() {
    }

    public MovieWatched(Movie movie) {
        if (movie != null) {
            this.name = movie.getName();
            this.id = movie.getId();
            this.idGroup = movie.getIdGroup();
            this.typeFilm = movie.getTypeFilm();
            this.imagePath = movie.getImagePath();
            this.posterPath = movie.getPosterPath();
            this.currentEpisode = String.valueOf(movie.getCurrentEpisode());
            this.totalEpisodes = String.valueOf(movie.getTotalEpisodes());
            this.link = movie.getLinkWap();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(String idGroup) {
        this.idGroup = idGroup;
    }

    public String getIdPart() {
        return idPart;
    }

    public void setIdPart(String idPart) {
        this.idPart = idPart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getTimeSeek() {
        return timeSeek;
    }

    public void setTimeSeek(String timeSeek) {
        this.timeSeek = timeSeek;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getIdInt() {
        try {
            return Integer.parseInt(id);
        } catch (Exception e) {
            return 0;
        }
    }

    public int getIdGroupInt() {
        try {
            return Integer.parseInt(idGroup);
        } catch (Exception e) {
            return 0;
        }
    }

    public int getIdPartInt() {
        try {
            return Integer.parseInt(idPart);
        } catch (Exception e) {
            return 0;
        }
    }

    public int getTimeSeekInt() {
        try {
            return Integer.parseInt(timeSeek);
        } catch (Exception e) {
            return 0;
        }
    }

    public int getDurationInt() {
        try {
            return Integer.parseInt(duration);
        } catch (Exception e) {
            return 0;
        }
    }

    public String getTypeFilm() {
        return typeFilm;
    }

    public void setTypeFilm(String typeFilm) {
        this.typeFilm = typeFilm;
    }

    public int getTotalEpisodesInt() {
        try {
            return Integer.parseInt(totalEpisodes);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getTotalEpisodes() {
        return totalEpisodes;
    }

    public void setTotalEpisodes(String totalEpisodes) {
        this.totalEpisodes = totalEpisodes;
    }

    public int getCurrentEpisodeInt() {
        try {
            return Integer.parseInt(currentEpisode);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getCurrentEpisode() {
        return currentEpisode;
    }

    public void setCurrentEpisode(String currentEpisode) {
        this.currentEpisode = currentEpisode;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSubtitle() {
        return isSubtitle;
    }

    public void setSubtitle(String isSubtitle) {
        this.isSubtitle = isSubtitle;
    }

    public String getNarrative() {
        return isNarrative;
    }

    public void setNarrative(String isNarrative) {
        this.isNarrative = isNarrative;
    }

    public String getThroughput() {
        return throughput;
    }

    public void setThroughput(String throughput) {
        this.throughput = throughput;
    }

    public String getTotalTimePlay() {
        return totalTimePlay;
    }

    public void setTotalTimePlay(String totalTimePlay) {
        this.totalTimePlay = totalTimePlay;
    }

    public static Movie convertToMovie(MovieWatched movieWatched) {
        Movie movie = null;
        if (movieWatched != null) {
            movie = new Movie();
            movie.setId(movieWatched.getId());
            movie.setIdGroup(movieWatched.getIdGroup());
            movie.setName(movieWatched.getName());
            movie.setPosterPath(movieWatched.getPosterPath());
            movie.setImagePath(movieWatched.getImagePath());
            movie.setCurrentEpisode(movieWatched.getCurrentEpisodeInt());
            movie.setTotalEpisodes(movieWatched.getTotalEpisodesInt());
            movie.setLinkWap(movieWatched.getLink());
            movie.setSubtitle(movieWatched.getSubtitle());
            movie.setNarrative(movieWatched.getNarrative());
            movie.setConvertFromMovieWatched(true);
        }
        return movie;
    }
}

package com.metfone.selfcare.model.addAccount;

public class AddAccountSendIdResponse<T> {
    private String description;
    private String error_code;

    public String getDescription() {
        if (description == null) return "";
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError_code() {
        if (error_code == null) return "";
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }
}

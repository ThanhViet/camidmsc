package com.metfone.selfcare.model.setting;

/**
 * Created by thanhnt72 on 6/15/2019.
 */

public class TabHomeDefault {

    private int tabIndex;
    private String idTabWap;

    public TabHomeDefault(int tabType, String idTabWap) {
        this.tabIndex = tabType;
        this.idTabWap = idTabWap;
    }

    public int getTabIndex() {
        return tabIndex;
    }

    public String getIdTabWap() {
        return idTabWap;
    }
}

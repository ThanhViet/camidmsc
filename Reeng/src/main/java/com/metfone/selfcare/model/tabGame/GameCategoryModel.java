package com.metfone.selfcare.model.tabGame;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GameCategoryModel implements Serializable {
    @SerializedName("id")
    private long id;
    @SerializedName("status")
    private String status;
    @SerializedName("games")
    private GameModel[] games;
    @SerializedName("name")
    private String name;
    @SerializedName("name_km")
    private String nameKM;

    public GameCategoryModel() {
    }

    public long getID() {
        return id;
    }

    public void setID(long value) {
        this.id = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String value) {
        this.status = value;
    }

    public GameModel[] getGames() {
        return games;
    }

    public void setGames(GameModel[] value) {
        this.games = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getNameKM() {
        return nameKM;
    }

    public void setNameKM(String value) {
        this.nameKM = value;
    }
}

package com.metfone.selfcare.model.camid;

import android.graphics.Bitmap;

public class ContactTopup {
    private String name = null;
    private String phoneNumber = null;
    private Bitmap avatar = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNUmber) {
        this.phoneNumber = phoneNUmber;
    }

    public Bitmap getAvatar() {
        return avatar;
    }

    public void setAvatar(Bitmap avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "ContactTopup{" +
                "name='" + name + '\'' +
                ", phoneNUmber='" + phoneNumber + '\'' +
                ", avatar=" + avatar +
                '}';
    }
}

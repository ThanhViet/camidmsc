package com.metfone.selfcare.model.tab_video;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.ChannelOnMedia;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tuanha00 on 3/8/2018.
 */

public class Channel implements Serializable, BaseAdapter.Clone {

    private String id = "";
    private String name = "";
    private String imageUrl = "";
    private String description = "";
    private String imageCoverUrl = "";
    private String textFollow = "";
    private String packageAndroid = "";
    private String url = "";
    private long numfollow = 0;
    private long lastPublishVideo = System.currentTimeMillis();

    private boolean follow = false;
    private boolean isMyChannel = false;
    private boolean haveNewVideo = false;
    private boolean isInstall = false;
    private int hasFilmGroup = 0;
    private int numVideo = 0;
    private int isOfficial;

    private String createdDate;

    private int thumbnail = R.drawable.error;

    private int typeChannel = TypeChanel.TYPE_DEFAULT.VALUE;
    private String textTotalPoint = "";
    private ArrayList<Video> videos;

    public static Channel convertFromChannelOnMedia(ChannelOnMedia channelOnMedia) {
        if (channelOnMedia == null) return null;
        Channel channel = new Channel();
        channel.setId(channelOnMedia.getId());
        channel.setName(channelOnMedia.getName());
        channel.setUrlImage(channelOnMedia.getAvatarUrl());
        channel.setUrlImageCover(channelOnMedia.getCoverUrl());
        channel.setFollow(channelOnMedia.isFollow());
        channel.setMyChannel(channelOnMedia.isMyChannel());
        channel.setNumFollow(channelOnMedia.getNumFollow());
        channel.setHasFilmGroup(channelOnMedia.getHasFilmGroup());
        channel.setCreatedDate(channelOnMedia.getCreatedDate());
        channel.setTypeChannel(channelOnMedia.getType());
        channel.setTypeChannel(TypeChanel.TYPE_DEFAULT.VALUE);
        return channel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlImage() {
        return imageUrl;
    }

    public void setUrlImage(String url_images) {
        this.imageUrl = url_images;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlImageCover() {
        return imageCoverUrl;
    }

    public void setUrlImageCover(String url_images_cover) {
        this.imageCoverUrl = url_images_cover;
    }

    public long getNumfollow() {
        return numfollow;
    }

    public long getNumFollow() {
        return numfollow;
    }

    public void setNumFollow(long numfollow) {
        this.numfollow = numfollow;
        textFollow = Utilities.shortenLongNumber(numfollow);
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }

    public boolean isFollow() {
        return follow;
    }

    public void setFollow(boolean follow) {
        this.follow = follow;
    }

    public String getTextFollow() {
        return textFollow;
    }

    public int getIsOfficial() {
        return isOfficial;
    }

    public void setIsOfficial(int isOfficial) {
        this.isOfficial = isOfficial;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public int getTypeChannel() {
        return typeChannel;
    }

    public void setTypeChannel(int typeChannel) {
        this.typeChannel = typeChannel;
    }

    public boolean isMyChannel() {
        return isMyChannel;
    }

    public void setMyChannel(boolean myChannel) {
        isMyChannel = myChannel;
    }

    public String getPackageAndroid() {
        return packageAndroid;
    }

    public void setPackageAndroid(String packageAndroid) {
        this.packageAndroid = packageAndroid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getLastPublishVideo() {
        return lastPublishVideo;
    }

    public void setLastPublishVideo(long lastPublishVideo) {
        this.lastPublishVideo = lastPublishVideo;
    }

    public boolean isHaveNewVideo() {
        return haveNewVideo;
    }

    public void setHaveNewVideo(boolean haveNewVideo) {
        this.haveNewVideo = haveNewVideo;
    }

    public boolean isInstall() {
        return isInstall;
    }

    public void setInstall(boolean install) {
        isInstall = install;
    }

    public int getHasFilmGroup() {
        return hasFilmGroup;
    }

    public void setHasFilmGroup(int hasFilmGroup) {
        this.hasFilmGroup = hasFilmGroup;
    }

    public int getNumVideo() {
        return numVideo;
    }

    public void setNumVideo(int numVideo) {
        this.numVideo = numVideo;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getTextTotalPoint() {
        return textTotalPoint;
    }

    public void setTextTotalPoint(String textTotalPoint) {
        this.textTotalPoint = textTotalPoint;
    }

    public boolean isHasFilmGroup() {
        return hasFilmGroup == 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Channel channel = (Channel) o;
        return Utilities.equals(id, channel.id);
    }

    public Channel clone() {
        try {
            Channel channel = new Channel();
            channel.id = id;
            channel.name = name;
            channel.imageUrl = imageUrl;
            channel.description = description;
            channel.imageCoverUrl = imageCoverUrl;
            channel.textFollow = textFollow;
            channel.packageAndroid = packageAndroid;
            channel.numfollow = numfollow;
            channel.follow = follow;
            channel.isMyChannel = isMyChannel;
            channel.thumbnail = thumbnail;
            channel.typeChannel = typeChannel;
            channel.lastPublishVideo = lastPublishVideo;
            channel.haveNewVideo = haveNewVideo;
            channel.numVideo = numVideo;
            channel.videos = videos;
            channel.textTotalPoint = textTotalPoint;
            return channel;
        } catch (Exception e) {
            return this;
        }
    }

    public enum TypeChanel {
        TYPE_DEFAULT(0),
        MOCHA_VIDEO(1),
        UNABLE_SUB(2),
        OPEN_APP(3);
        public int VALUE;

        TypeChanel(int VALUE) {
            this.VALUE = VALUE;
        }
    }
}

package com.metfone.selfcare.model.home;

/**
 * Created by thanhnt72 on 12/11/2018.
 */

public class ItemMoreHome {

    private int function;
    private String title;
    private int resId;
    private String imgUrl;
    private String deepLink = "";
    private String funcName = "";

    public ItemMoreHome(int function, String title, int resId, String imgUrl) {
        this.function = function;
        this.title = title;
        this.resId = resId;
        this.imgUrl = imgUrl;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }

    public int getFunction() {
        return function;
    }

    public String getTitle() {
        return title;
    }

    public int getResId() {
        return resId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    @Override
    public String toString() {
        return "ItemMoreHome{" +
                "title='" + title + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", deepLink='" + deepLink + '\'' +
                '}';
    }
}

package com.metfone.selfcare.model.tabGame;

import com.google.gson.annotations.SerializedName;

public class GameListResponseData {
    @SerializedName("items")
    private GameModel[] items;
    @SerializedName("total")
    private long total;

    public GameListResponseData() {
    }

    public GameModel[] getItems() { return items; }
    public void setItems(GameModel[] value) { this.items = value; }

    public long getTotal() { return total; }
    public void setTotal(long value) { this.total = value; }
}


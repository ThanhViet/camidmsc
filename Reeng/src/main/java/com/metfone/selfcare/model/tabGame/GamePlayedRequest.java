package com.metfone.selfcare.model.tabGame;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GamePlayedRequest {
    int range;
    public GamePlayedRequest(int range) {
        this.range = range;
    }
}

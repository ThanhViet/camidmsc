package com.metfone.selfcare.model.camid;

/**
 * {
 *   "total": 0.1,
 *   "money": 0.58,
 *   "type": "168",
 *   "startTime": "07/10/2020",
 *   "code": "METWORLD_168"
 * }
 */
public class VasCharge extends DetailSmsCallingDataCharge {
    private double total;
    private double money;
    private String type;
    private String startTime;
    private String code;

    public VasCharge(double total, double money, String type, String startTime, String code) {
        super(DetailType.CHARGE_TYPE_GROUP_VAS_CHARGE);
        this.total = total;
        this.money = money;
        this.type = type;
        this.startTime = startTime;
        this.code = code;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Service implements Serializable {
    @SerializedName("service_id")
    @Expose
    private Integer serviceId;
    @SerializedName("service_code")
    @Expose
    private String serviceCode;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("phone_linked")
    @Expose
    private List<PhoneLinked> phoneLinked = null;

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public List<PhoneLinked> getPhoneLinked() {
        return phoneLinked;
    }

    public void setPhoneLinked(List<PhoneLinked> phoneLinked) {
        this.phoneLinked = phoneLinked;
    }
}

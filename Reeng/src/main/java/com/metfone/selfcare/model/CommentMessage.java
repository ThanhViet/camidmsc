package com.metfone.selfcare.model;

public class CommentMessage {
    String comment;
    boolean isCommit;

    public CommentMessage(String comment, boolean isCommit) {
        this.comment = comment;
        this.isCommit = isCommit;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isCommit() {
        return isCommit;
    }

    public void setCommit(boolean commit) {
        isCommit = commit;
    }
}

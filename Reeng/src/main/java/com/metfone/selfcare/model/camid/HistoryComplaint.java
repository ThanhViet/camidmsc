package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComplaintHistoryResponse;

import java.io.Serializable;

/**
 * Model for {@link WsGetComplaintHistoryResponse}
 */
public class HistoryComplaint implements Serializable {

    @SerializedName("acceptDate")
    private String acceptDate;

    @SerializedName("compContent")
    private String compContent;

    @SerializedName("complainId")
    private long complainId;

    @SerializedName("preResult")
    private String preResult;

    @SerializedName("proLimitDate")
    private String proLimitDate;

    @SerializedName("status")
    private int status;

    @SerializedName("staffInfo")
    private String staffInfo;

    @SerializedName("staffPhone")
    private String staffPhone;

    @SerializedName("errorPhone")
    private String errorPhone;

    @SerializedName("complainerPhone")
    private String complainerPhone;

    @SerializedName("resultContent")
    private String resultContent;

    @SerializedName("serviceTypeName")
    private String serviceTypeName;

    @SerializedName("groupTypeName")
    private String groupTypeName;

    @SerializedName("endDate")
    private String endDate;

    private boolean isExpandView = false;

    public String getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }

    public String getCompContent() {
        return compContent;
    }

    public void setCompContent(String compContent) {
        this.compContent = compContent;
    }

    public long getComplainId() {
        return complainId;
    }

    public void setComplainId(long complainId) {
        this.complainId = complainId;
    }

    public String getPreResult() {
        return preResult;
    }

    public void setPreResult(String preResult) {
        this.preResult = preResult;
    }

    public String getProLimitDate() {
        return proLimitDate;
    }

    public void setProLimitDate(String proLimitDate) {
        this.proLimitDate = proLimitDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStaffInfo() {
        return staffInfo;
    }

    public void setStaffInfo(String staffInfo) {
        this.staffInfo = staffInfo;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public String getErrorPhone() {
        return errorPhone;
    }

    public void setErrorPhone(String errorPhone) {
        this.errorPhone = errorPhone;
    }

    public String getComplainerPhone() {
        return complainerPhone;
    }

    public void setComplainerPhone(String complainerPhone) {
        this.complainerPhone = complainerPhone;
    }

    public String getResultContent() {
        return resultContent;
    }

    public void setResultContent(String resultContent) {
        this.resultContent = resultContent;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getGroupTypeName() {
        return groupTypeName;
    }

    public void setGroupTypeName(String groupTypeName) {
        this.groupTypeName = groupTypeName;
    }

    public boolean isExpandView() {
        return isExpandView;
    }

    public void setExpandView(boolean expandView) {
        isExpandView = expandView;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}

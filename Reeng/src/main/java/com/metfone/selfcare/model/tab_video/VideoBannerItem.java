package com.metfone.selfcare.model.tab_video;

import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VideoBannerItem implements Serializable {

    private String id = "";
    private Integer itemType = 0;
    private String itemImage = "";
    private String itemTitle = "";
    private String deeplink = "";
    private Integer iorder = 0;
    private Integer active = 0;
    private Integer position = 0;
    private Float aspectRatio = 1.78f; //ti le 16/9
    private List<VideoBannerItem> listSubItems = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getItemType() {
        return itemType;
    }

    public void setItemType(Integer itemType) {
        this.itemType = itemType;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getDeeplink() {
        return deeplink;
    }

    public void setDeeplink(String deeplink) {
        this.deeplink = deeplink;
    }

    public Integer getIorder() {
        return iorder;
    }

    public void setIorder(Integer iorder) {
        this.iorder = iorder;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public List<VideoBannerItem> getListSubItems() {
        return listSubItems;
    }

    public void setListSubItems(List<VideoBannerItem> listSubItems) {
        this.listSubItems = listSubItems;
    }

    public float getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(float aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public VideoBannerItem clone() {
        try {
            VideoBannerItem videoBannerItem = new VideoBannerItem();
            videoBannerItem.id = id;
            videoBannerItem.itemType = itemType;
            videoBannerItem.itemImage = itemImage;
            videoBannerItem.itemTitle = itemTitle;
            videoBannerItem.deeplink = deeplink;
            videoBannerItem.iorder = iorder;
            videoBannerItem.active = active;
            videoBannerItem.position = position;
            videoBannerItem.listSubItems = listSubItems;
            videoBannerItem.aspectRatio = aspectRatio;
            return videoBannerItem;
        } catch (Exception e) {
            return this;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VideoBannerItem that = (VideoBannerItem) o;
        return Utilities.equals(id, that.id);
    }

}
package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HistoryChargeResponse implements Serializable {
    @SerializedName("total")
    private Double total;
    @SerializedName("data")
    private Double data;
    @SerializedName("calling")
    private Double calling;
    @SerializedName("sms")
    private Double sms;
    @SerializedName("vas")
    private Double vas;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("history")
    private List<HistoryDataInfo> history;

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getData() {
        return data;
    }

    public void setData(Double data) {
        this.data = data;
    }

    public Double getCalling() {
        return calling;
    }

    public void setCalling(Double calling) {
        this.calling = calling;
    }

    public Double getSms() {
        return sms;
    }

    public void setSms(Double sms) {
        this.sms = sms;
    }

    public Double getVas() {
        return vas;
    }

    public void setVas(Double vas) {
        this.vas = vas;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<HistoryDataInfo> getHistory() {
        return history;
    }

    public void setHistory(List<HistoryDataInfo> history) {
        this.history = history;
    }
}

package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HistoryDataInfo  implements Serializable {
    @SerializedName("date")
    private String date;
    @SerializedName("total")
    private Double total;
    @SerializedName("unit")
    private String unit;
    @SerializedName("money")
    private Double money;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }
}

package com.metfone.selfcare.model.tabMovie;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MovieKind implements Serializable {
    /*
    type = 1: Danh sách phim được hiện thị slide dạng banner (flash hot). Danh sách dữ liệu lấy từ trường “movies”.
    type = 2: Danh sách phim được hiện thị dạng box vuốt ngang. Danh sách dữ liệu lấy từ trường “movies”.
    type = 3: Danh sách phim được hiện thị dạng bảng xếp hạng. Danh sách dữ liệu lấy từ trường “movies”.
    type = 4: Danh sách chuyên mục phim dạng box có thể vuốt ngang. Danh sách dữ liệu lấy từ trường “categories”.
    type = 6: Danh sách chuyên mục phim dạng box có thể vuốt ngang. Danh sách dữ liệu lấy từ trường “categories”.
    type = 7: Danh sách phim được hiện thị dạng box vuốt ngang. Danh sách dữ liệu lấy từ trường “movies”. (Danh sách phim theo 1 sự kiện cụ thể)
    type = 8: Danh sách phim được hiện thị dạng box vuốt ngang. Danh sách dữ liệu lấy từ trường “movies”. (Danh sách phim theo 1 giải thưởng)
    type = 9: Danh sách phim được hiện thị dạng box vuốt ngang. Danh sách dữ liệu lấy từ trường “movies”. (Danh sách phim theo 1 nhóm chuyên mục do biên tập viên tạo)
    */

    public static final String CATEGORYID_GET_NEW = "-1";
    public static final String CATEGORYID_GET_HOT = "-2";
    public static final String CATEGORYID_GET_LIST = "-3";
    public static final String CATEGORYID_GET_WATCHED = "-4";
    public static final String CATEGORYID_GET_LIKED = "-5";
    public static final String CATEGORYID_GET_LATER = "-6";
    public static final String CATEGORYID_GET_LIKED_ODD = "-7";
    public static final String CATEGORYID_GET_LIKED_SERIES = "-8";

    public static final int TYPE_LOCAL = 0;
    public static final int TYPE_FLASH_HOT = 1;
    public static final int TYPE_HORIZONTAL = 2;
    public static final int TYPE_CHART = 3;
    public static final int TYPE_CATEGORY = 4;
    public static final int TYPE_KEENG_TV = 5;
    public static final int TYPE_GROUP_CATEGORY = 6;
    public static final int TYPE_EVENT = 7;
    public static final int TYPE_PRIZE = 8;
    public static final int TYPE_TOPIC = 9;
    public static final int TYPE_LIST_HOT = 10;

    public static final String TYPE_RECOMMEND = "recommend";
    public static final String TYPE_TRENDING = "trending";
    public static final String TYPE_CONTINUE = "continueWatching";
    public static final String TYPE_MOVIE4YOU = "movie4you";
    public static final String TYPE_RECENTLYADDED = "recentlyAdded";
    public static final String TYPE_AVAILABLE = "avaiable";
    public static final String TYPE_CUSTOM_TOPIC = "customTopic";
    public static final String TYPE_WATCHED= "watched";

    @SerializedName("type")
    private int type;
    @SerializedName("title")
    private String title;
    @SerializedName("total_sub_tab")
    private int totalSubtabs;
    @SerializedName("parentid")
    private int parentId;
    @SerializedName("subTabID")
    private String subtabId;
    @SerializedName("countryID")
    private String countryId;
    @SerializedName("typeFilmID")
    private int typeFilmId;
    @SerializedName("eventID")
    private int eventId;
    @SerializedName("category_id")
    private String categoryId;

    @SerializedName("movies")
    private ArrayList<Movie> movies;

    @SerializedName("categories")
    private ArrayList<SubtabInfo> categories;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTotalSubtabs() {
        return totalSubtabs;
    }

    public void setTotalSubtabs(int totalSubtabs) {
        this.totalSubtabs = totalSubtabs;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getSubtabId() {
        return subtabId;
    }

    public void setSubtabId(String subtabId) {
        this.subtabId = subtabId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public int getTypeFilmId() {
        return typeFilmId;
    }

    public void setTypeFilmId(int typeFilmId) {
        this.typeFilmId = typeFilmId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<Movie> getMovies() {
        if (movies == null) movies = new ArrayList<>();
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        if (this.movies == null) {
            this.movies = new ArrayList<>();
        } else this.movies.clear();
        this.movies.addAll(movies);
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public ArrayList<SubtabInfo> getCategories() {
        if (categories == null) categories = new ArrayList<>();
        return categories;
    }

    public void setCategories(ArrayList<SubtabInfo> categories) {
        this.categories = categories;
    }

    public void setCategories(List<SubtabInfo> categories) {
        if (this.categories == null) {
            this.categories = new ArrayList<>();
        } else this.categories.clear();
        this.categories.addAll(categories);
    }

    public boolean isEmpty() {
        if (type == TYPE_FLASH_HOT && !getMovies().isEmpty()) {
            List list = getMovies();
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
            }
            return list.isEmpty();
        } else if (type == TYPE_HORIZONTAL && !getMovies().isEmpty()) {
            List<Movie> list = getMovies();
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
            }
            return list.isEmpty();
        } else if (type == TYPE_CHART && !getMovies().isEmpty()) {
            List<Movie> list = getMovies();
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
                else list.get(i).setType(Movie.Type.BANNER_SMALL.VALUE);
            }
            boolean isEmpty = list.isEmpty();
            if (!isEmpty) {
                list.get(0).setType(Movie.Type.BANNER_BIG.VALUE);
            }
            return isEmpty;
        } else if ((type == TYPE_CATEGORY || type == TYPE_GROUP_CATEGORY)
                && !getCategories().isEmpty()) {
            List<SubtabInfo> list = getCategories();
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
                else list.get(i).setSubtab(false);
            }
            return list.isEmpty();
        }
//        else if (type == TYPE_KEENG_TV && !getKeengTvs().isEmpty()) {
//            List list = getKeengTvs();
//            int size = list.size() - 1;
//            for (int i = size; i >= 0; i--) {
//                if (list.get(i) == null) list.remove(i);
//            }
//            return list.isEmpty();
//        }
        else if (type == TYPE_EVENT && !getMovies().isEmpty()) {
            List<Movie> list = getMovies();
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
            }
            return list.isEmpty();
        } else if (type == TYPE_PRIZE && !getMovies().isEmpty()) {
            List<Movie> list = getMovies();
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
            }
            return list.isEmpty();
        } else if (type == TYPE_TOPIC && !getMovies().isEmpty()) {
            List<Movie> list = getMovies();
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
            }
            return list.isEmpty();
        } else if (type == TYPE_LOCAL && !getMovies().isEmpty() && CATEGORYID_GET_WATCHED.equals(categoryId)) {
            List<Movie> list = getMovies();
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
                else list.get(i).setType(Movie.Type.WATCHED.VALUE);
            }
            return list.isEmpty();
        }
        return true;
    }

    public boolean canClickTitle() {
        return (type == TYPE_HORIZONTAL
                || type == TYPE_CHART
                || type == TYPE_LOCAL
                || type == TYPE_EVENT
                || type == TYPE_PRIZE
                || type == TYPE_TOPIC) && getMovies().size() >= 4;
    }

    public boolean canShowViewAll() {
        return (type == TYPE_HORIZONTAL
                || type == TYPE_LOCAL
                || type == TYPE_EVENT
                || type == TYPE_PRIZE
                || type == TYPE_TOPIC) && getMovies().size() >= 4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieKind movieKind = (MovieKind) o;
        return Utilities.equals(type, movieKind.type) && Utilities.equals(title, movieKind.title) && Utilities.equals(categoryId, movieKind.categoryId);
    }

    public enum Type {
        TOP(1), PHIM_BO_HOT(2);

        public int VALUE;

        Type(int value) {
            this.VALUE = value;
        }
    }

}

package com.metfone.selfcare.model;

import lombok.Data;

@Data
public class ClickLikeMessage {
    private String idVideo;
    private boolean isLike;

    public ClickLikeMessage(String idVideo) {
        this.idVideo = idVideo;
    }

    public String getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(String idVideo) {
        this.idVideo = idVideo;
    }
}

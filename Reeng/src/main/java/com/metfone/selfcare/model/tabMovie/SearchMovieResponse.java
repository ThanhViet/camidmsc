package com.metfone.selfcare.model.tabMovie;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class SearchMovieResponse {
    @SerializedName("responseHeader")
    @Expose
    private List<Object> responseHeader = null;
    @SerializedName("response")
    @Expose
    private Response response;

    @Getter
    @Setter
    public static class Response {

        @SerializedName("numFound")
        @Expose
        private Integer numFound;
        @SerializedName("start")
        @Expose
        private Integer start;
        @SerializedName("maxScore")
        @Expose
        private Integer maxScore;
        @SerializedName("docs")
        @Expose
        private List<Doc> docs = null;


    }
    @Getter
    @Setter

    public static class Doc {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("id_group")
        @Expose
        private Integer idGroup;
        @SerializedName("duration")
        @Expose
        private Integer duration;
        @SerializedName("content_filter")
        @Expose
        private String contentFilter;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("image_path")
        @Expose
        private String imagePath;
        @SerializedName("poster_path")
        @Expose
        private String posterPath;
        @SerializedName("logo_path")
        @Expose
        private String logoPath;
        @SerializedName("logo_position")
        @Expose
        private String logoPosition;
        @SerializedName("categories")
        @Expose
        private List<Category> categories = null;
        @SerializedName("year")
        @Expose
        private String year;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("userlogin")
        @Expose
        private String userlogin;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("isView")
        @Expose
        private Integer isView;
        @SerializedName("isFilmGroups")
        @Expose
        private Integer isFilmGroups;
        @SerializedName("total")
        @Expose
        private Integer total;
        @SerializedName("current_film")
        @Expose
        private Integer currentFilm;
        @SerializedName("link_wap")
        @Expose
        private String linkWap;
        @SerializedName("is_free")
        @Expose
        private String isFree;
        @SerializedName("imdb")
        @Expose
        private String imdb;
        @SerializedName("drm_content_id")
        @Expose
        private String drmContentId;
        @SerializedName("isFreeContent")
        @Expose
        private String isFreeContent;
        @SerializedName("isFreeData")
        @Expose
        private String isFreeData;
        @SerializedName("type_film")
        @Expose
        private String typeFilm;
        @SerializedName("episodes")
        @Expose
        private String episodes;
        @SerializedName("chapter")
        @Expose
        private String chapter;
        @SerializedName("hd_price")
        @Expose
        private String hdPrice;
        @SerializedName("hd_price_FreeData")
        @Expose
        private String hdPriceFreeData;
        @SerializedName("sd_price")
        @Expose
        private String sdPrice;
        @SerializedName("sd_price_FreeData")
        @Expose
        private String sdPriceFreeData;
        @SerializedName("publisher")
        @Expose
        private String publisher;
        @SerializedName("productInfo")
        @Expose
        private String productInfo;
        @SerializedName("trailer_url")
        @Expose
        private String trailerUrl;
        @SerializedName("trailer_image")
        @Expose
        private String trailerImage;
        @SerializedName("intro_Path")
        @Expose
        private String introPath;
        @SerializedName("label_url")
        @Expose
        private String labelUrl;
        @SerializedName("free_content_all")
        @Expose
        private String freeContentAll;
        @SerializedName("label_txt")
        @Expose
        private String labelTxt;
        @SerializedName("isInternational")
        @Expose
        private String isInternational;
        @SerializedName("total_like")
        @Expose
        private Integer totalLike;
        @SerializedName("total_unlike")
        @Expose
        private Integer totalUnlike;
        @SerializedName("total_share")
        @Expose
        private Integer totalShare;
        @SerializedName("total_comment")
        @Expose
        private Integer totalComment;
        @SerializedName("deepLinkUrl")
        @Expose
        private String deepLinkUrl;
        @SerializedName("displayIMDB")
        @Expose
        private String displayIMDB;
        @SerializedName("displayPrize")
        @Expose
        private String displayPrize;
        @SerializedName("displayDirector")
        @Expose
        private String displayDirector;
        @SerializedName("isEncodeLinkToken")
        @Expose
        private Integer isEncodeLinkToken;
    }

    @Getter
    @Setter
    public static class Category {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("categoryname")
        @Expose
        private String categoryname;
        @SerializedName("parentid")
        @Expose
        private Integer parentid;
        @SerializedName("type")
        @Expose
        private Integer type;
        @SerializedName("order")
        @Expose
        private Integer order;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("settop")
        @Expose
        private Integer settop;
        @SerializedName("total_sub_tab")
        @Expose
        private Integer totalSubTab;
        @SerializedName("eventID")
        @Expose
        private Integer eventID;
        @SerializedName("typeFilmID")
        @Expose
        private Integer typeFilmID;



    }
}

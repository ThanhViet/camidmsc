
package com.metfone.selfcare.model.tabMovie;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.tab_video.AdSense;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OriginalPath implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("video_path")
    private String videoPath;
    @SerializedName("url_adsense")
    private String urlAdsense;
    @SerializedName("list_resolution")
    private List<ListResolution> listResolution = null;
    @SerializedName("adsense")
    private ArrayList<AdSense> listAds = null;
    @SerializedName("subtitles_url")
    String subTitleUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getUrlAdsense() {
        if (TextUtils.isEmpty(urlAdsense)) return "";
        return urlAdsense;
    }

    public void setUrlAdsense(String urlAdsense) {
        this.urlAdsense = urlAdsense;
    }

    public List<ListResolution> getListResolution() {
        if (listResolution == null) listResolution = new ArrayList<>();
        return listResolution;
    }

    public ArrayList<AdSense> getListAds() {
        if (listAds == null) new ArrayList<>();
        return listAds;
    }

    public void setListAds(ArrayList<AdSense> listAds) {
        this.listAds = listAds;
    }

    public String getSubTitleUrl() {
        return subTitleUrl;
    }

    public void setSubTitleUrl(String subTitleUrl) {
        this.subTitleUrl = subTitleUrl;
    }
}

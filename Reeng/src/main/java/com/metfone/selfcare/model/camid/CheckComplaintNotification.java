package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class CheckComplaintNotification implements Serializable {

    @SerializedName("acceptDate")
    public String acceptDate;
    @SerializedName("compContent")
    public String compContent;
    @SerializedName("compTemplate")
    public String compTemplate;
    @SerializedName("complainId")
    public long complainId;
    @SerializedName("compTypeId")
    public long compTypeId;
    @SerializedName("preResult")
    public String preResult;
    @SerializedName("proLimitDate")
    public String proLimitDate;
    @SerializedName("status")
    public int status;
    @SerializedName("staffInfo")
    public String staffInfo;
    @SerializedName("staffPhone")
    public String staffPhone;
    @SerializedName("errorPhone")
    public String errorPhone;
    @SerializedName("complainerPhone")
    public String complainerPhone;
    @SerializedName("resultContent")
    public String resultContent;
    @SerializedName("serviceTypeName")
    public String serviceTypeName;
    @SerializedName("groupTypeName")
    public String groupTypeName;
    @SerializedName("endDate")
    public String endDate;
    @SerializedName("serviceTypeId")
    public String serviceTypeId;

}

package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

public class PasswordResetInfo {
    @SerializedName("otp")
    String otp;

    @SerializedName("password")
    String password;

    public PasswordResetInfo(String otp, String password) {
        this.otp = otp;
        this.password = password;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

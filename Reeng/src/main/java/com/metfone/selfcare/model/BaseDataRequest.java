package com.metfone.selfcare.model;

public class BaseDataRequest<T> {
    public T wsRequest;
    public String apiKey;
    public String sessionId;
    public String username;
    public String wsCode;


    public BaseDataRequest(T wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        this.wsRequest = wsRequest;
        this.apiKey = apiKey;
        this.sessionId = sessionId;
        this.username = username;
        this.wsCode = wsCode;
    }

    public T getWsRequest() {
        return wsRequest;
    }

    public void setWsRequest(T wsRequest) {
        this.wsRequest = wsRequest;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWsCode() {
        return wsCode;
    }

    public void setWsCode(String wsCode) {
        this.wsCode = wsCode;
    }
}

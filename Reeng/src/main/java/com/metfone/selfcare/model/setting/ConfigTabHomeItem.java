package com.metfone.selfcare.model.setting;

import com.metfone.selfcare.helper.home.TabHomeHelper;

/**
 * Created by thanhnt72 on 12/11/2018.
 */

public class ConfigTabHomeItem implements Cloneable {

    public static final int STATE_UNAVAILABLE = -1;
    public static final int STATE_NOT_ACTIVE = 0;
    public static final int STATE_ACTIVE = 1;

    private int state;
    private TabHomeHelper.HomeTab homeTab;

    private String name;
    private String desc;
    private String link;
    private String img;
    private String id;
    private String color;
    private String imgAct;
    private String imgMore;

    public ConfigTabHomeItem(String name, String desc, String link, String img, String id, String color, String imgAct, String imgMore) {
        this.name = name;
        this.desc = desc;
        this.link = link;
        this.img = img;
        this.id = id;
        this.color = color;
        this.imgAct = imgAct;
        this.imgMore = imgMore;
        this.homeTab = TabHomeHelper.HomeTab.tab_wap;
        this.state = STATE_NOT_ACTIVE;
    }

    public ConfigTabHomeItem(TabHomeHelper.HomeTab homeTab, int state) {
        this.state = state;
        this.homeTab = homeTab;
    }

    public TabHomeHelper.HomeTab getHomeTab() {
        return homeTab;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public String getLink() {
        return link;
    }

    public String getImg() {
        return img;
    }

    public String getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public String getImgAct() {
        return imgAct;
    }

    public String getImgMore() {
        return imgMore;
    }

    public ConfigTabHomeItem clone() throws CloneNotSupportedException {
        return (ConfigTabHomeItem) super.clone();
    }

    @Override
    public String toString() {
        return "ConfigTabHomeItem{" +
                "state=" + state +
                ", homeTab=" + homeTab.name() +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", link='" + link + '\'' +
                ", img='" + img + '\'' +
                ", id='" + id + '\'' +
                ", color='" + color + '\'' +
                ", imgAct='" + imgAct + '\'' +
                ", imgMore='" + imgMore + '\'' +
                '}';
    }
}

package com.metfone.selfcare.model.tabGame;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GamePlayMostTimeRequest {
    @SerializedName("range")
    int range;
    @SerializedName("page_index")
    int page_index;
    @SerializedName("page_size")
    int page_size;

    public GamePlayMostTimeRequest(int range, int page_index, int page_size) {
        this.range = range;
        this.page_index = page_index;
        this.page_size = page_size;
    }
}

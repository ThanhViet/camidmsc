package com.metfone.selfcare.model.tab_video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VideoRevenue implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("viewGr30s")
    @Expose
    private long viewGr30s;
    @SerializedName("pointRevenue")
    @Expose
    private long pointRevenue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getViewGr30s() {
        return viewGr30s;
    }

    public void setViewGr30s(long viewGr30s) {
        this.viewGr30s = viewGr30s;
    }

    public long getPointRevenue() {
        return pointRevenue;
    }

    public void setPointRevenue(long pointRevenue) {
        this.pointRevenue = pointRevenue;
    }

}

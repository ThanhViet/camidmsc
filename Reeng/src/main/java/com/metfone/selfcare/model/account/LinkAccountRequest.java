package com.metfone.selfcare.model.account;

import lombok.Getter;
import lombok.Setter;

//{
//"social_token":"id_token |access_token" // với facebook truyen access_token, google truyền id_token len server Verifier
//"social_type": "google|facebook|apple"
//}
@Setter
@Getter
public class LinkAccountRequest{
    String social_token;
    String social_type;

    public LinkAccountRequest(String social_token, String social_type) {
        this.social_token = social_token;
        this.social_type = social_type;
    }
}

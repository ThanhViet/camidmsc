package com.metfone.selfcare.model.account;

import com.metfone.selfcare.model.BaseDataRequest;

public class AddServiceRequest extends BaseDataRequest<BaseServiceRequestInfo> {
    public AddServiceRequest(BaseServiceRequestInfo wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        super(wsRequest, apiKey, sessionId, username, wsCode);
    }
}

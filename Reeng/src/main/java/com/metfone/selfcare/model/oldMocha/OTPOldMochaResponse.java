package com.metfone.selfcare.model.oldMocha;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OTPOldMochaResponse {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("subscription_package")
    @Expose
    private List<SubscriptionPackage> subscriptionPackage = null;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("ssl")
    @Expose
    private Integer ssl;
    @SerializedName("mocha_install")
    @Expose
    private Boolean mochaInstall;
    @SerializedName("otp")
    @Expose
    private String otp;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public List<SubscriptionPackage> getSubscriptionPackage() {
        return subscriptionPackage;
    }

    public void setSubscriptionPackage(List<SubscriptionPackage> subscriptionPackage) {
        this.subscriptionPackage = subscriptionPackage;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getSsl() {
        return ssl;
    }

    public void setSsl(Integer ssl) {
        this.ssl = ssl;
    }

    public Boolean getMochaInstall() {
        return mochaInstall;
    }

    public void setMochaInstall(Boolean mochaInstall) {
        this.mochaInstall = mochaInstall;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

}
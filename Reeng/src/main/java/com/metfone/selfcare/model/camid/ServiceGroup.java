package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceGroup implements Serializable {
    @SerializedName("name")
    private String name;
    @SerializedName("code")
    private String code;
    @SerializedName("shortDes")
    private String shortDes;
    @SerializedName("iconUrl")
    private String iconUrl;
    @SerializedName("price")
    private Double price;
    @SerializedName("isMultPlan")
    private Integer isMultPlan;
    @SerializedName("isRegisterAble")
    private Integer isRegisterAble;
    @SerializedName("state")
    private Integer state;
    @SerializedName("validity")
    private String validity;
    @SerializedName("autoRenew")
    private String autoRenew;
    private boolean isSelected = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShortDes() {
        return shortDes;
    }

    public void setShortDes(String shortDes) {
        this.shortDes = shortDes;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getIsMultPlan() {
        return isMultPlan;
    }

    public void setIsMultPlan(Integer isMultPlan) {
        this.isMultPlan = isMultPlan;
    }

    public Integer getIsRegisterAble() {
        return isRegisterAble;
    }

    public void setIsRegisterAble(Integer isRegisterAble) {
        this.isRegisterAble = isRegisterAble;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getAutoRenew() {
        return autoRenew;
    }

    public void setAutoRenew(String autoRenew) {
        this.autoRenew = autoRenew;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

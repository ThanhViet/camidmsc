package com.metfone.selfcare.model.camid;

/**
 * {
 *    "phoneNumber": "084987****990",
 *    "date": "09/10/2020",
 *    "time": "02:21:03",
 *    "money": 0.07
 * }
 */
public class CallingCharge extends SmsCharge {
    private String callingTime;

    public CallingCharge(String date, String phoneNumber, double money, String time, String callingTime) {
        super(date, phoneNumber, money, time);
        this.callingTime = callingTime;
        setDetailCharge(DetailType.CHARGE_TYPE_GROUP_CALLING_CHARGE);
    }

    public String getCallingTime() {
        return callingTime;
    }

    public void setCallingTime(String callingTime) {
        this.callingTime = callingTime;
    }
}

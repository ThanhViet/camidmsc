package com.metfone.selfcare.model.account;

import lombok.Getter;

@Getter
public class DeleteServiceRequest {
    String phoneNumber;
    int service_id;
    String serviceName;

    public DeleteServiceRequest(String phoneNumber, int service_id, String serviceName) {
        this.phoneNumber = phoneNumber;
        this.service_id = service_id;
        this.serviceName = serviceName;
    }
}

package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

public class BaseServiceRequestInfo {
    @SerializedName("service_id")
    int serviceId;
    @SerializedName("phone_number")
    String phoneNumber;
    @SerializedName("otp")
    String otp;

    public BaseServiceRequestInfo(int serviceId, String phoneNumber, String otp) {
        this.serviceId = serviceId;
        this.phoneNumber = phoneNumber;
        this.otp = otp;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}

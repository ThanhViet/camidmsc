package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Services {
    @SerializedName("service_id")
    int serviceId;
    @SerializedName("service_code")
    String serviceCode;
    @SerializedName("service_name")
    String serviceName;
    @SerializedName("phone_linked")
    List<PhoneLinkedInfo> phoneLinkedList;

    public Services(int serviceId, String serviceCode, String serviceName, List<PhoneLinkedInfo> phoneLinkedList) {
        this.serviceId = serviceId;
        this.serviceCode = serviceCode;
        this.serviceName = serviceName;
        this.phoneLinkedList = phoneLinkedList;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public List<PhoneLinkedInfo> getPhoneLinkedList() {
        return phoneLinkedList;
    }

    public void setPhoneLinkedList(List<PhoneLinkedInfo> phoneLinkedList) {
        this.phoneLinkedList = phoneLinkedList;
    }
}

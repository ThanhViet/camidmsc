package com.metfone.selfcare.model.setting;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.api.request.WsGetListReward;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class wsControlCamIdNotificationRequest extends BaseRequest<wsControlCamIdNotificationRequest.Request> {
    public class Request {
        @SerializedName("camid")
        public String camid;
        @SerializedName("language")
        public String language;
        @SerializedName("blockReceive")
        public String blockReceive;
        @SerializedName("deviceId")
        public String deviceId;
    }
}

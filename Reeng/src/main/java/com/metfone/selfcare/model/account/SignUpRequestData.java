package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

public class SignUpRequestData {
    @SerializedName("phone_number")
    String phoneNumber;
    @SerializedName("password")
    String password;
    @SerializedName("otp")
    String otp;

    public SignUpRequestData(String phoneNumber, String password, String otp) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.otp = otp;
    }

    public SignUpRequestData() {

    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}

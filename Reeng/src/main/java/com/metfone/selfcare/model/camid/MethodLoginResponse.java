package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MethodLoginResponse {
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ItemsMethod data;

    @Getter
    @Setter
    public class ItemsMethod {
        @SerializedName("items")
        private List<MethodLoginResponse.MethodLogin> methodsLogin;
    }

    @Getter
    @Setter
    public class MethodLogin {
        public static final String ACTIVE = "ACTIVE";
        @SerializedName("id")
        private int id;
        @SerializedName("code")
        private String code;
        @SerializedName("name")
        private String name;
        @SerializedName("icon")
        private String icon;
        @SerializedName("state")
        private String state;
    }
}

package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProcessListResponse;

import java.io.Serializable;

/**
 * Model for {@link WsGetProcessListResponse}
 */
public class ChangeCardDamaged implements Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("requestDateStr")
    private String requestDateStr;

    @SerializedName("statusStr")
    private String statusStr;

    @SerializedName("updateReason")
    private String updateReason;

    @SerializedName("description")
    private String description;

    @SerializedName("serial")
    private String serial;

    @SerializedName("phoneNumber")
    private String phoneNumber;

    @SerializedName("status")
    private String status;

    @SerializedName("dataImage")
    private String dataImage;

    @SerializedName("urlVideo")
    private String dataVideo;

    @SerializedName("updateDateStr")
    private String updateDateStr;

    @SerializedName("updateUser")
    private String updateUser;

    private boolean isExpand;

    private String pathVideo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequestDateStr() {
        return requestDateStr;
    }

    public void setRequestDateStr(String requestDateStr) {
        this.requestDateStr = requestDateStr;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public String getUpdateReason() {
        return updateReason;
    }

    public void setUpdateReason(String updateReason) {
        this.updateReason = updateReason;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDataImage() {
        return dataImage;
    }

    public void setDataImage(String dataImage) {
        this.dataImage = dataImage;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public String getDataVideo() {
        return dataVideo;
    }

    public void setDataVideo(String dataVideo) {
        this.dataVideo = dataVideo;
    }

    public String getUpdateDateStr() {
        return updateDateStr;
    }

    public void setUpdateDateStr(String updateDateStr) {
        this.updateDateStr = updateDateStr;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getPathVideo() {
        return pathVideo;
    }

    public void setPathVideo(String pathVideo) {
        this.pathVideo = pathVideo;
    }
}

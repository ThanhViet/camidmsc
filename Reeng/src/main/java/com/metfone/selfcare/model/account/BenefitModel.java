package com.metfone.selfcare.model.account;

/**
 * @author ITSOL JAPAN
 * Created on 16/12/2020.
 * Copyright � 2020 YSL Solution Co., Ltd. All rights reserved.
 **/
public class BenefitModel {
    private int image;
   private String title;
   private String description;


    public BenefitModel(int image,String title, String description ) {
        this.image = image;
        this.title = title;
        this.description = description;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


}

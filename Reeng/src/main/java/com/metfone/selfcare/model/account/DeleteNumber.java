package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

public class DeleteNumber {
    @SerializedName("service_id")
    int serviceId;

    @SerializedName("phone_number")
    String phoneNumber;

    public DeleteNumber() {

    }

    public DeleteNumber(int serviceId, String phoneNumber) {
        this.serviceId = serviceId;
        this.phoneNumber = phoneNumber;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

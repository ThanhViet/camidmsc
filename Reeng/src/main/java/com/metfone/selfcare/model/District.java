package com.metfone.selfcare.model;

import com.google.gson.annotations.SerializedName;

public class District {
    @SerializedName("title")
    private String title;
    @SerializedName("code")
    private String code;
    @SerializedName("province_code")
    private String provinceCode;

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

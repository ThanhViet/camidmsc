package com.metfone.selfcare.model.addAccount;

public class AddAccountFtthUserResponse {

    private String phoneNumber;
    private String customerName;
    private String debit;
    private String blockDate;
    private String contractIdInfor;
    private String address;
    private String contractServiceTypes;
    private String ftthName;
    private String contractPoint;
    private String level;
    private String packageMonth;

    public String getPhoneNumber() {
        if (phoneNumber == null) return "";
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCustomerName() {
        if (customerName == null) return "";
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDebit() {
        if (debit == null) return "";
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getBlockDate() {
        if (blockDate == null) return "";
        return blockDate;
    }

    public void setBlockDate(String blockDate) {
        this.blockDate = blockDate;
    }

    public String getContractIdInfor() {
        if (contractIdInfor == null) return "";
        return contractIdInfor;
    }

    public void setContractIdInfor(String contractIdInfor) {
        this.contractIdInfor = contractIdInfor;
    }

    public String getAddress() {
        if (address == null) return "";
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContractServiceTypes() {
        if (contractServiceTypes == null) return "";
        return contractServiceTypes;
    }

    public void setContractServiceTypes(String contractServiceTypes) {
        this.contractServiceTypes = contractServiceTypes;
    }

    public String getFtthName() {
        if (ftthName == null) return "";
        return ftthName;
    }

    public void setFtthName(String ftthName) {
        this.ftthName = ftthName;
    }

    public String getContractPoint() {
        if (contractPoint == null) return "";
        return contractPoint;
    }

    public void setContractPoint(String contractPoint) {
        this.contractPoint = contractPoint;
    }

    public String getLevel() {
        if (level == null) return "";
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPackageMonth() {
        if (packageMonth == null) return "";
        return packageMonth;
    }

    public void setPackageMonth(String packageMonth) {
        this.packageMonth = packageMonth;
    }
}

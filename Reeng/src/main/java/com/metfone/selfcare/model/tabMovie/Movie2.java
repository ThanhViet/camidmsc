package com.metfone.selfcare.model.tabMovie;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Movie2 {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id_group")
    @Expose
    private Integer idGroup;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("original_path")
    @Expose
    private OriginalPath originalPath;
    @SerializedName("image_path")
    @Expose
    private String imagePath;
    @SerializedName("poster_path")
    @Expose
    private String posterPath;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("director")
    @Expose
    private List<Director> director = null;
    @SerializedName("actor")
    @Expose
    private List<Actor> actor = null;
    @SerializedName("year")
    @Expose
    private Object year;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("userlogin")
    @Expose
    private String userlogin;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("isView")
    @Expose
    private Integer isView;
    @SerializedName("isFilmGroups")
    @Expose
    private Integer isFilmGroups;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("current_film")
    @Expose
    private String currentFilm;
    @SerializedName("link_wap")
    @Expose
    private String linkWap;
    @SerializedName("is_free")
    @Expose
    private Integer isFree;
    @SerializedName("imdb")
    @Expose
    private String imdb;
    @SerializedName("price_play")
    @Expose
    private String pricePlay;
    @SerializedName("isFreeContent")
    @Expose
    private Integer isFreeContent;
    @SerializedName("isFreeData")
    @Expose
    private Integer isFreeData;
    @SerializedName("type_film")
    @Expose
    private String typeFilm;
    @SerializedName("episodes")
    @Expose
    private String episodes;
    @SerializedName("chapter")
    @Expose
    private String chapter;
    @SerializedName("hd_price")
    @Expose
    private String hdPrice;
    @SerializedName("hd_price_FreeData")
    @Expose
    private String hdPriceFreeData;
    @SerializedName("sd_price")
    @Expose
    private String sdPrice;
    @SerializedName("sd_price_FreeData")
    @Expose
    private String sdPriceFreeData;
    @SerializedName("publisher")
    @Expose
    private String publisher;
    @SerializedName("productInfo")
    @Expose
    private String productInfo;
    @SerializedName("trailer_url")
    @Expose
    private String trailerUrl;
    @SerializedName("trailer_image")
    @Expose
    private String trailerImage;
    @SerializedName("intro_Path")
    @Expose
    private String introPath;
    @SerializedName("label_url")
    @Expose
    private String labelUrl;
    @SerializedName("free_content_all")
    @Expose
    private Integer freeContentAll;
    @SerializedName("label_txt")
    @Expose
    private String labelTxt;
    @SerializedName("isInternational")
    @Expose
    private String isInternational;
    @SerializedName("total_like")
    @Expose
    private Integer totalLike;
    @SerializedName("total_unlike")
    @Expose
    private Integer totalUnlike;
    @SerializedName("total_share")
    @Expose
    private Integer totalShare;
    @SerializedName("total_comment")
    @Expose
    private Integer totalComment;
    @SerializedName("deepLinkUrl")
    @Expose
    private String deepLinkUrl;
    @SerializedName("isEncodeLinkToken")
    @Expose
    private Integer isEncodeLinkToken;
    @SerializedName("type")
    @Expose
    private String type;
}

package com.metfone.selfcare.model.tabGame;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaveGameRequest {
    @SerializedName("game_id")
    String game_id;
    @SerializedName("play_name")
    String play_name;
    @SerializedName("score")
    int score;

    public SaveGameRequest(String game_id, String play_name, int score) {
        this.game_id = game_id;
        this.play_name = play_name;
        this.score = score;
    }
}

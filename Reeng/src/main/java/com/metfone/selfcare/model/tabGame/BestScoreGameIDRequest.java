package com.metfone.selfcare.model.tabGame;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BestScoreGameIDRequest {
    String game_id;
    public BestScoreGameIDRequest(String game_id) {
        this.game_id = game_id;
    }
}

package com.metfone.selfcare.model.camid;

import java.util.List;

public class DataHistoryCharge extends HistoryCharge {
    private Double total;
    private String unit;
    private Integer quantity;

    public DataHistoryCharge(HistoryChargeType type, Double total, String unit, Integer quantity) {
        super(type);
        this.total = total;
        this.unit = unit;
        this.quantity = quantity;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}

package com.metfone.selfcare.model.tabMovie;

import lombok.Data;

@Data
public class TrackItem {
    public static String OFF = "Off";
    public static String AUTO = "Auto";
    private String languageCode;
    private int renderIndex;
    private int groupIndex;
    private int trackIndex;
    private int width;
    private int height;

    public TrackItem(String languageCode, int renderIndex, int groupIndex, int trackIndex) {
        this.languageCode = languageCode;
        this.renderIndex = renderIndex;
        this.groupIndex = groupIndex;
        this.trackIndex = trackIndex;
    }

    public TrackItem(String title,int width, int height, int renderIndex, int groupIndex, int trackIndex) {
        this.languageCode = title;
        this.renderIndex = renderIndex;
        this.groupIndex = groupIndex;
        this.trackIndex = trackIndex;
        this.width = width;
        this.height = height;
    }
}

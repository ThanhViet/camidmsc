package com.metfone.selfcare.model.tabGame;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameByNameRequest {
    String name;
    public GameByNameRequest(String name) {
        this.name = name;
    }
}

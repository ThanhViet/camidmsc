package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ExchangeItem implements Serializable {
    @SerializedName("groupName")
    private String groupName;
    @SerializedName("groupCode")
    private String groupCode;
    @SerializedName("shortDes")
    private String shortDes;
    @SerializedName("fullDes")
    private String fullDes;
    @SerializedName("validDes")
    private String validDes;
    @SerializedName("image")
    private String image;
    @SerializedName("services")
    private List<ServiceGroup> services;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getShortDes() {
        return shortDes;
    }

    public void setShortDes(String shortDes) {
        this.shortDes = shortDes;
    }

    public String getFullDes() {
        return fullDes;
    }

    public void setFullDes(String fullDes) {
        this.fullDes = fullDes;
    }

    public String getValidDes() {
        return validDes;
    }

    public void setValidDes(String validDes) {
        this.validDes = validDes;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ServiceGroup> getServices() {
        return services;
    }

    public void setServices(List<ServiceGroup> services) {
        this.services = services;
    }
}

package com.metfone.selfcare.model.account;

import com.metfone.selfcare.model.BaseDataRequest;

public class UpdateLoginRequest extends BaseDataRequest<UpdateLoginInfo> {
    public UpdateLoginRequest(UpdateLoginInfo wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        super(wsRequest, apiKey, sessionId, username, wsCode);
    }
}


package com.metfone.selfcare.model.tabMovie;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Category  implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parentid")
    @Expose
    private Integer parentid;
    @SerializedName("name")
    @Expose
    private String categoryname;
    @SerializedName("categoryid")
    @Expose
    private Integer categoryid;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("settop")
    @Expose
    private Integer settop;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSettop() {
        return settop;
    }

    public void setSettop(Integer settop) {
        this.settop = settop;
    }

}

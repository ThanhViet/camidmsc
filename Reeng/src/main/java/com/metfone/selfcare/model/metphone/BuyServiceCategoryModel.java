package com.metfone.selfcare.model.metphone;

public class BuyServiceCategoryModel {
    public String serviceGroupId;
    public int imgIcon;
    public int categoryName;
    public int categoryDescription;
    public int[] arrColor = new int[]{0xFF616261, 0xFF131313};

    public BuyServiceCategoryModel(String serviceGroupId, int imgIcon, int name, int description, int[] arrColor) {
        this.serviceGroupId = serviceGroupId;
        this.imgIcon = imgIcon;
        this.categoryName = name;
        this.categoryDescription = description;
        this.arrColor[0] = arrColor[0];
        this.arrColor[1] = arrColor[1];
    }
}

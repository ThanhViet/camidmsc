package com.metfone.selfcare.model.camid;

/**
 *  {
 *     "date": "24/09/2020",
 *     "total": 21,
 *     "money": 0.53,
 *     "unit": "MB"
 *   }
 */
public class DataCharge extends DetailSmsCallingDataCharge {
    private String date;
    private int total;
    private String unit;
    private double money;

    public DataCharge(String date, int total, String unit, double money) {
        super(DetailType.CHARGE_TYPE_GROUP_DATA_CHARGE);
        this.date = date;
        this.total = total;
        this.unit = unit;
        this.money = money;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}

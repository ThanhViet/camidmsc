package com.metfone.selfcare.model.camid;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HistoryChargeDetail implements Serializable {
    private static final String TAG = "HistoryChargeDetail";
    @SerializedName("date")
    private String date;
    @SerializedName("total")
    private Double total;
    @SerializedName("unit")
    private String unit;
    @SerializedName("money")
    private Double money;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("time")
    private String time;
    @SerializedName("callingTime")
    private String callingTime;
    @SerializedName("type")
    private String type;
    @SerializedName("startTime")
    private String startTime;
    @SerializedName("code")
    private String code;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getTotal() {
        return total;
    }

    public int getTotalInt() {
        int intTotal = 0;
        try {
            intTotal = total.intValue();
        } catch (Exception e) {
            Log.e(TAG, "getTotalInt: convert Double to int error ");
        }
        return intTotal;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCallingTime() {
        return callingTime;
    }

    public void setCallingTime(String callingTime) {
        this.callingTime = callingTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

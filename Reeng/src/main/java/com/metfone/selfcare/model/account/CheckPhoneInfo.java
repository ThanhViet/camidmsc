package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

public class CheckPhoneInfo {
    @SerializedName("phone_number")
    String phoneNumber;

    public CheckPhoneInfo(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

package com.metfone.selfcare.model.tabMovie;

import android.content.res.Resources;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.encrypt.XXTEACrypt;
import com.metfone.selfcare.model.tab_video.FilmGroup;
import com.metfone.selfcare.model.tab_video.Resolution;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.movienew.model.CustomTopicFilm;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Movie implements Serializable {

    @SerializedName("search_info")
    public String searchInfo;
    @SerializedName(value = "id", alternate = "id_phim")
    private String id;
    @SerializedName("id_group")
    private String idGroup;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("original_path")
    private List<OriginalPath> originalPath = null;
    @SerializedName("image_path")
    private String imagePath;
    @SerializedName("poster_path")
    private String posterPath;
    @SerializedName("image_banner")
    private String bannerPath; //Search tra ve truong nay thay vi poster_path
    @SerializedName("categories")
    private List<Category> categories = null;
    @SerializedName("director")
    private List<Director> director = null;
    @SerializedName("actor")
    private List<Actor> actor = null;
    @SerializedName("year")
    private String year;
    @SerializedName("country")
    private String country;
    @SerializedName("userlogin")
    private String userlogin;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("isView")
    private int isView;
    @SerializedName("isFilmGroups")
    private int isFilmGroups;
    @SerializedName("link_wap")
    private String linkWap;
    @SerializedName("is_free")
    private String isFree;
    @SerializedName("imdb")
    private String imdb;
    @SerializedName("drm_content_id")
    private String drmContentId;
    @SerializedName("isFreeContent")
    private String isFreeContent;
    @SerializedName("isFreeData")
    private String isFreeData;
    @SerializedName("isGroupFreeData")
    private String isGroupFreeData;
    @SerializedName("type_film")
    private String typeFilm;
    @SerializedName("episodes")
    private String episodes;
    @SerializedName("chapter")
    private String chapter;
    @SerializedName("hd_price")
    private String hdPrice;
    @SerializedName("hd_price_FreeData")
    private String hdPriceFreeData;
    @SerializedName("sd_price")
    private String sdPrice;
    @SerializedName("sd_price_FreeData")
    private String sdPriceFreeData;
    @SerializedName("publisher")
    private String publisher;
    @SerializedName("productInfo")
    private String productInfo;
    @SerializedName("trailer_url")
    private String trailerUrl;
    @SerializedName("trailer_image")
    private String trailerImage;
    @SerializedName("intro_Path")
    private String introPath;
    @SerializedName("label_url")
    private String labelUrl;
    @SerializedName("label_txt")
    private String labelTxt;
    @SerializedName("total_like")
    private int totalLike;
    @SerializedName("total_unlike")
    private int totalUnlike;
    @SerializedName("total_share")
    private int totalShare;
    @SerializedName("total_comment")
    private int totalComment;
    @SerializedName("total_rated")
    private int totalRated;

    @SerializedName("filmPrize")
    private String filmPrize;

    @SerializedName("score")
    private double score;

    private int typeMovie;

    private boolean isLike;
    private boolean isShare;

    @SerializedName("total")
    private int totalEpisodes;
    @SerializedName("current_film")
    private int currentEpisode;
    @SerializedName("is_subtitle")
    private String isSubtitle;
    @SerializedName("is_narrative")
    private String isNarrative;
    @SerializedName("duration")
    private String duration;
    @SerializedName("rate")
    private String rate;
    @SerializedName("is_rated")
    private boolean is_rated;

    @SerializedName("subtitle")
    private List<SubtitleAudio> mListSubtitle;

    @SerializedName("audio")
    private List<SubtitleAudio> mListAudio;

    private boolean decryptVideoPath5dMax = false;
    private boolean isConvertFromMovieWatched = false;
    private String timeWatched = ""; // thoi luong da xem

    public Movie() {
    }

    public Movie(CustomTopicFilm customTopicFilm) {
        setId(customTopicFilm.getId());
        setName(customTopicFilm.getName());
        setLinkWap(customTopicFilm.getLinkWap());
        setChapter(customTopicFilm.getChapter());
        setIsView(Integer.parseInt(customTopicFilm.getIsView()));
        setTotalLike(Integer.parseInt(customTopicFilm.getTotalLike()));
        setImagePath(customTopicFilm.getImagePath());
        setPosterPath(customTopicFilm.getPosterPath());
        setTotalShare(Integer.parseInt(customTopicFilm.getTotalShare()));
        setDescription(customTopicFilm.getDescription());
        setTotalComment(Integer.parseInt(customTopicFilm.getTotalComment()));
        setImdb(customTopicFilm.getImdb());
        setCreatedAt(customTopicFilm.getCreatedAt());
        setIdGroup(customTopicFilm.getIdGroup());
        setCountry(customTopicFilm.getCountry());
    }

    public Movie(HomeData homeData) {
        setId(String.valueOf(homeData.getId()));
        setName(homeData.getName());
        setLinkWap(homeData.getLinkWap());

        setChapter(homeData.getChapter());
        setIsView(homeData.getIsView());

        setTotalLike(homeData.getTotalLike());
        setImagePath(homeData.getImagePath());
        setPosterPath(homeData.getPosterPath());

        setTotalShare(homeData.getTotalShare());
        setDescription(homeData.getDescription());
        setTotalComment(homeData.getTotalComment());

        setImdb(homeData.getImdb());
        setCreatedAt(homeData.getCreatedAt());
        setIdGroup(String.valueOf(homeData.getIdGroup()));
        setCountry(homeData.getCountry());


    }

    public Movie(Movie2 movie2) {
        setId(String.valueOf(movie2.getId()));
        setName(movie2.getName());
        setLinkWap(movie2.getLinkWap());

        setChapter(movie2.getChapter());
        setIsView(movie2.getIsView());

        setTotalLike(movie2.getTotalLike());
        setImagePath(movie2.getImagePath());
        setPosterPath(movie2.getPosterPath());

        setTotalShare(movie2.getTotalShare());
        setDescription(movie2.getDescription());
        setTotalComment(movie2.getTotalComment());

        setImdb(movie2.getImdb());
        setCreatedAt(movie2.getCreatedAt());
        setIdGroup(String.valueOf(movie2.getIdGroup()));


    }
    public static Video movie2Video(Movie movie) {
        Video video = new Video();
        video.setId(String.valueOf(movie.getId()));
        video.setAspectRatio(16d / 9);
        video.setTitle(movie.getName());
        video.setLink(movie.getLinkWap());
        video.setChapter(movie.getChapter());
        video.setTotalView(movie.getIsView());
        video.setTotalLike(movie.getTotalLike());
        video.setImagePath(movie.getImagePath());
        video.setPosterPath(movie.getPosterPath());
        video.setTotalShare(movie.getTotalShare());
        video.setDescription(movie.getDescription());
        video.setTotalComment(movie.getTotalComment());
        video.setCategoryText(movie.getCategoryText());
        video.setLike(movie.isLike());
        video.setShare(movie.isShare());
        video.setMovie(true);
        video.setImdb(movie.getImdb());
        video.setInfo(movie.getVideoInfo());
        video.setPublishTime(TimeHelper.convertCreateAtToPublishTime(movie.getCreatedAt()));
        FilmGroup filmGroup = new FilmGroup();
        filmGroup.setGroupId(movie.getIdGroup());
        video.setFilmGroup(filmGroup);
        video.setDuration(movie.getDuration());
        video.setTrailerImage(movie.getTrailerImage());
        video.setListSubtitle(movie.getListSubtitle());
        video.setListAudio(movie.getListAudio());

        String urlAds = "";
        boolean isCambodia = ApplicationController.self().getReengAccountBusiness().isCambodia();
        ArrayList<Resolution> resolutions = new ArrayList<>();
        if (Utilities.notEmpty(movie.getOriginalPath())) {
            OriginalPath originalPath = movie.getOriginalPath().get(0);
            for (ListResolution listResolution : originalPath.getListResolution()) {
                Resolution resolution = new Resolution();
                resolution.setKey(listResolution.getKey());
                resolution.setTitle(listResolution.getTitle());
                String videoPath = listResolution.getVideoPath();
                if (isCambodia) {
                    videoPath = XXTEACrypt.decryptBase64StringToString(videoPath, BuildConfig.KEY_XXTEA_5DMAX_MOVIES);
                }
                resolution.setVideoPath(videoPath);
                resolutions.add(resolution);
            }
            urlAds = originalPath.getUrlAdsense();
            //   convert 5DMax
            if (isCambodia && !movie.isDecryptVideoPath5dMax()) {
                video.setOriginalPath(XXTEACrypt.decryptBase64StringToString(originalPath.getVideoPath(), BuildConfig.KEY_XXTEA_5DMAX_MOVIES));
                video.setDecryptVideoPath5dMax(true);
            } else {
                video.setOriginalPath(originalPath.getVideoPath());
            }
            video.setOriginalPathId(originalPath.getId());
            video.setListAds(originalPath.getListAds());
        }
        video.setListResolution(resolutions);
        video.setUrlAds(urlAds);
        video.setCountry(movie.getCountry());
        video.setTimeWatched(movie.getTimeWatched());
        if (movie.isConvertFromMovieWatched()) video.setConvertFromMovieWatched(true);
        return video;
    }

    public static Video copyLinkFromMovie(Video video, Movie movie) {
        ArrayList<Resolution> resolutions = new ArrayList<>();
        if (Utilities.notEmpty(movie.getOriginalPath())) {
            OriginalPath originalPath = movie.getOriginalPath().get(0);
            for (ListResolution listResolution : originalPath.getListResolution()) {
                Resolution resolution = new Resolution();
                resolution.setKey(listResolution.getKey());
                resolution.setTitle(listResolution.getTitle());
                String videoPath = listResolution.getVideoPath();
                videoPath = XXTEACrypt.decryptBase64StringToString(videoPath, BuildConfig.KEY_XXTEA_5DMAX_MOVIES);
                resolution.setVideoPath(videoPath);
                resolutions.add(resolution);
            }
            video.setOriginalPath(XXTEACrypt.decryptBase64StringToString(originalPath.getVideoPath(), BuildConfig.KEY_XXTEA_5DMAX_MOVIES));
            video.setDecryptVideoPath5dMax(true);
        }
        video.setListResolution(resolutions);
        return video;
    }

    public boolean isDecryptVideoPath5dMax() {
        return decryptVideoPath5dMax;
    }

    public void setDecryptVideoPath5dMax(boolean decryptVideoPath5dMax) {
        this.decryptVideoPath5dMax = decryptVideoPath5dMax;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdGroup() {
        return idGroup;
    }

    public int getMovieGroupIdInt() {
        try {
            return Integer.parseInt(idGroup);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setIdGroup(String idGroup) {
        this.idGroup = idGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OriginalPath> getOriginalPath() {
        return originalPath;
    }

    public void setOriginalPath(List<OriginalPath> originalPath) {
        this.originalPath = originalPath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPosterPath() {
        if (!TextUtils.isEmpty(posterPath))
            return posterPath;
        if (!TextUtils.isEmpty(bannerPath))
            return bannerPath;
        return "";
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Director> getDirector() {
        return director;
    }

    public void setDirector(List<Director> director) {
        this.director = director;
    }

    public List<Actor> getActor() {
        return actor;
    }

    public void setActor(List<Actor> actor) {
        this.actor = actor;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUserlogin() {
        return userlogin;
    }

    public void setUserlogin(String userlogin) {
        this.userlogin = userlogin;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getIsView() {
        return isView;
    }

    public void setIsView(int isView) {
        this.isView = isView;
    }

    public int getIsFilmGroups() {
        return isFilmGroups;
    }

    public void setIsFilmGroups(int isFilmGroups) {
        this.isFilmGroups = isFilmGroups;
    }

    public String getLinkWap() {
        return linkWap;
    }

    public void setLinkWap(String linkWap) {
        this.linkWap = linkWap;
    }

    public String getIsFree() {
        return isFree;
    }

    public void setIsFree(String isFree) {
        this.isFree = isFree;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public String getDrmContentId() {
        return drmContentId;
    }

    public void setDrmContentId(String drmContentId) {
        this.drmContentId = drmContentId;
    }

    public String getIsFreeContent() {
        return isFreeContent;
    }

    public void setIsFreeContent(String isFreeContent) {
        this.isFreeContent = isFreeContent;
    }

    public String getIsFreeData() {
        return isFreeData;
    }

    public void setIsFreeData(String isFreeData) {
        this.isFreeData = isFreeData;
    }

    public String getIsGroupFreeData() {
        return isGroupFreeData;
    }

    public void setIsGroupFreeData(String isGroupFreeData) {
        this.isGroupFreeData = isGroupFreeData;
    }

    public String getTypeFilm() {
        return typeFilm;
    }

    public void setTypeFilm(String typeFilm) {
        this.typeFilm = typeFilm;
    }

    public String getEpisodes() {
        return episodes;
    }

    public void setEpisodes(String episodes) {
        this.episodes = episodes;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getHdPrice() {
        return hdPrice;
    }

    public void setHdPrice(String hdPrice) {
        this.hdPrice = hdPrice;
    }

    public String getHdPriceFreeData() {
        return hdPriceFreeData;
    }

    public void setHdPriceFreeData(String hdPriceFreeData) {
        this.hdPriceFreeData = hdPriceFreeData;
    }

    public String getSdPrice() {
        return sdPrice;
    }

    public void setSdPrice(String sdPrice) {
        this.sdPrice = sdPrice;
    }

    public String getSdPriceFreeData() {
        return sdPriceFreeData;
    }

    public void setSdPriceFreeData(String sdPriceFreeData) {
        this.sdPriceFreeData = sdPriceFreeData;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public String getTrailerImage() {
        return trailerImage;
    }

    public void setTrailerImage(String trailerImage) {
        this.trailerImage = trailerImage;
    }

    public String getIntroPath() {
        return introPath;
    }

    public void setIntroPath(String introPath) {
        this.introPath = introPath;
    }

    public String getLabelUrl() {
        return labelUrl;
    }

    public void setLabelUrl(String labelUrl) {
        this.labelUrl = labelUrl;
    }

    public String getLabelTxt() {
        return labelTxt;
    }

    public void setLabelTxt(String labelTxt) {
        this.labelTxt = labelTxt;
    }

    public int getTotalLike() {
        return totalLike;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setTotalLike(int totalLike) {
        this.totalLike = totalLike;
    }

    public int getTotalUnlike() {
        return totalUnlike;
    }

    public void setTotalUnlike(int totalUnlike) {
        this.totalUnlike = totalUnlike;
    }

    public int getTotalShare() {
        return totalShare;
    }

    public void setTotalShare(int totalShare) {
        this.totalShare = totalShare;
    }

    public int getTotalComment() {
        return totalComment;
    }

    public void setTotalComment(int totalComment) {
        this.totalComment = totalComment;
    }

    public int getType() {
        return typeMovie;
    }

    public void setType(int type) {
        this.typeMovie = type;
    }

    public String getSearchInfo() {
        if (TextUtils.isEmpty(searchInfo))
            return "";
        return searchInfo.trim();
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Utilities.equals(id, movie.id);
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public boolean isShare() {
        return isShare;
    }

    public void setShare(boolean share) {
        isShare = share;
    }

    public String getCategoryText() {
        StringBuilder sb = new StringBuilder();
        if (categories != null)
            for (int i = 0; i < categories.size(); i++) {
                if (categories.get(i) != null) {
                    String tmp = categories.get(i).getCategoryname();
                    if (!TextUtils.isEmpty(tmp)) {
                        if (!TextUtils.isEmpty(sb.toString()))
                            sb.append(", ");
                        sb.append(tmp);
                    }
                }
            }
        return sb.toString();
    }

    public String getFilmPrize() {
        return filmPrize;
    }

    public Map<String, String> getVideoInfo() {
        StringBuilder actorStr = new StringBuilder();
        if (Utilities.notEmpty(getActor()))
            for (int i = 0; i < getActor().size(); i++) {
                Actor item = getActor().get(i);
                if (item != null) {
                    if (TextUtils.isEmpty(actorStr)) {
                        actorStr.append(item.getName());
                    } else {
                        actorStr.append(", ").append(item.getName());
                    }
                }
            }
        Map<String, String> info = new HashMap<>();
        info.put("actor", actorStr.toString());
        info.put("year", getYear());
        info.put("country", getCountry());
        info.put("publisher", getPublisher());
        info.put("description", getDescription());
        if (Utilities.notEmpty(getDirector()) && getDirector().get(0) != null)
            info.put("director", getDirector().get(0).getName());
        if (Utilities.notEmpty(getCategories()) && getCategories().get(0) != null)
            info.put("category", getCategories().get(0).getCategoryname());
        return info;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getDurationInt() {
        try {
            return Integer.parseInt(duration);
        } catch (Exception e) {
            return 0;
        }
    }

    public int getDurationMinutes() {
        try {
            int tmp = Integer.parseInt(duration);
            return tmp / 60;
        } catch (Exception e) {
        }
        return 0;
    }

    public int getTotalEpisodes() {
        return totalEpisodes;
    }

    public void setTotalEpisodes(int totalEpisodes) {
        this.totalEpisodes = totalEpisodes;
    }

    public int getCurrentEpisode() {
        return currentEpisode;
    }

    public void setCurrentEpisode(int currentEpisode) {
        this.currentEpisode = currentEpisode;
    }

    public boolean isSubtitleFilm() {
        return false;
        //return "1".equals(isSubtitle);
    }

    public String getSubtitle() {
        return isSubtitle;
    }

    public void setSubtitle(String isSubtitle) {
        this.isSubtitle = isSubtitle;
    }

    public boolean isNarrativeFilm() {
        return "1".equals(isNarrative);
    }

    public String getNarrative() {
        return isNarrative;
    }

    public void setNarrative(String isNarrative) {
        this.isNarrative = isNarrative;
    }

    public boolean isConvertFromMovieWatched() {
        return isConvertFromMovieWatched;
    }

    public void setConvertFromMovieWatched(boolean convertFromMovieWatched) {
        isConvertFromMovieWatched = convertFromMovieWatched;
    }

    public String getTimeWatched() {
        return timeWatched;
    }

    public void setTimeWatched(String timeWatched) {
        this.timeWatched = timeWatched;
    }

    public boolean isSerial() {
        return getMovieGroupIdInt() > 0 || isFilmGroups == 1;
    }

    public boolean isIs_rated() {
        return is_rated;
    }

    public void setIs_rated(boolean is_rated) {
        this.is_rated = is_rated;
    }

    public List<SubtitleAudio> getListSubtitle() {
        return mListSubtitle != null ? mListSubtitle : new ArrayList<>();
    }

    public List<SubtitleAudio> getListAudio() {
        return mListAudio != null ? mListAudio : new ArrayList<>();
    }

    public int getTotalRated() {
        return totalRated;
    }

    public enum Type {
        FILM(0), BANNER_SMALL(1), BANNER_BIG(2), WATCHED(3), CATEGORY(4);
        public int VALUE;

        Type(int VALUE) {
            this.VALUE = VALUE;
        }
    }

}

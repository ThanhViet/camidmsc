package com.metfone.selfcare.model.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerifyInfo {
    String image_back;
    String image_front;
    String image_selfie;
    String image_type;

    public VerifyInfo(String image_back, String image_front, String image_selfie, String image_type) {
        this.image_back = image_back;
        this.image_front = image_front;
        this.image_selfie = image_selfie;
        this.image_type = image_type;
    }
}

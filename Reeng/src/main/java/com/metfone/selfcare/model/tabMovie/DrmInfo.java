/*
 * Copyright (c) 2017.
 * www.bigzun.com
 */

package com.metfone.selfcare.model.tabMovie;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DrmInfo implements Serializable {
    private static final long serialVersionUID = 7588477968421188716L;
    @SerializedName("itemmovieID")
    private int id = -1;
    @SerializedName("isPaid")
    private String isPaid;
    @SerializedName("sd_Price")
    private String sdPrice = "";
    @SerializedName("hd_Price")
    private String hdPrice = "";
    @SerializedName("sd_Price_FreeData")
    private String sdFreeDataPrice = "";
    @SerializedName("hd_Price_FreeData")
    private String hdFreeDataPrice = "";
    @SerializedName("link_film")
    private String link = "";
    @SerializedName("expire_date")
    private String expiredDate = "";

    public int getId() {
        return id;
    }

    public boolean isPaid() {
        return "1".equals(isPaid);
    }

    public String getHdPrice() {
        return hdPrice;
    }

    public String getSdPrice() {
        return sdPrice;
    }

    public String getSdFreeDataPrice() {
        return sdFreeDataPrice;
    }

    public String getHdFreeDataPrice() {
        return hdFreeDataPrice;
    }

    public String getLink() {
        return link;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", isPaid=" + isPaid +
                ", sdPrice=" + sdPrice +
                ", hdPrice=" + hdPrice +
                ", sdFreeDataPrice=" + sdFreeDataPrice +
                ", hdFreeDataPrice=" + hdFreeDataPrice +
                ", link=" + link +
                '}';
    }
}

package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataUserOldResponse {
    @SerializedName("username")
    @Expose
    private Object username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private Object address;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("avatar")
    @Expose
    private Object avatar;
    @SerializedName("verified")
    @Expose
    private Object verified;
    @SerializedName("province")
    @Expose
    private Object province;
    @SerializedName("district")
    @Expose
    private Object district;
    @SerializedName("code")
    @Expose
    private Object code;
    @SerializedName("user_id")
    @Expose
    private Object userId;
    @SerializedName("phone_number")
    @Expose
    private Object phoneNumber;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("identity_number")
    @Expose
    private Object identityNumber;
    @SerializedName("invited_code")
    @Expose
    private Object invitedCode;

    public Object getUsername() {
        return username;
    }

    public void setUsername(Object username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Object getAvatar() {
        return avatar;
    }

    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }

    public Object getVerified() {
        return verified;
    }

    public void setVerified(Object verified) {
        this.verified = verified;
    }

    public Object getProvince() {
        return province;
    }

    public void setProvince(Object province) {
        this.province = province;
    }

    public Object getDistrict() {
        return district;
    }

    public void setDistrict(Object district) {
        this.district = district;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public Object getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public Object getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Object getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Object identityNumber) {
        this.identityNumber = identityNumber;
    }

    public Object getInvitedCode() {
        return invitedCode;
    }

    public void setInvitedCode(Object invitedCode) {
        this.invitedCode = invitedCode;
    }

}

package com.metfone.selfcare.model.camid;

public class DetailSmsCallingDataCharge {
    private int detailCharge;

    public DetailSmsCallingDataCharge(int detailCharge) {
        this.detailCharge = detailCharge;
    }

    public int getDetailCharge() {
        return detailCharge;
    }

    public void setDetailCharge(int detailCharge) {
        this.detailCharge = detailCharge;
    }
}

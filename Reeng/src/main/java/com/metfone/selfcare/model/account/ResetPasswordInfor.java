package com.metfone.selfcare.model.account;

public class ResetPasswordInfor  {
    String otp;
    String password;
    String phone_number;

    public ResetPasswordInfor(String otp,  String phone_number,String password) {
        this.otp = otp;
        this.password = password;
        this.phone_number = phone_number;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public void setPhoneNumber(String phone_number) {
        this.phone_number = phone_number;
    }
}

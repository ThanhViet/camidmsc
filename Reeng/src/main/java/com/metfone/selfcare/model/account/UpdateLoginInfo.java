package com.metfone.selfcare.model.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateLoginInfo {
    String phone_number;
    String otp;
    int user_service_id;

    public UpdateLoginInfo(String phone_number, String otp, int user_service_id) {
        this.phone_number = phone_number;
        this.otp = otp;
        this.user_service_id = user_service_id;
    }

    public UpdateLoginInfo(String phone_number, String otp) {
        this.phone_number = phone_number;
        this.otp = otp;
    }

    public UpdateLoginInfo(String phone_number, int user_service_id) {
        this.phone_number = phone_number;
        this.user_service_id = user_service_id;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/2
 *
 */

package com.metfone.selfcare.model.tabMovie;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubtabInfo implements Serializable {
        /*
    Định nghĩa các type trong object categories của getHome:
    - type: 1: đi đến màn hình danh sách phim theo thể loại cụ thể: căn cứ theo id để hiển thị danh sách phim tương ứng
    - type: 2: đi đến màn hình danh sách phim theo quốc gia cụ thể: căn cứ theo id để hiển thị danh sách phim tương ứng
    - type: 3: đi đến màn hình danh sách phim theo danh sách thể loại (gồm danh sách các tab thể loại bên dưới tiêu đề)
    - type: 4: đi đến màn hình danh sách phim theo danh sách quốc gia (gồm danh sách các tab quốc gia bên dưới tiêu đề)
    - type: 5: đi đến màn hình danh sách phim lẻ
    - type: 6: đi đến màn hình danh sách phim bộ
    - type: 7: đi đến màn hình danh sách phim xem nhiều
    - type: 8: đi đến màn hình danh sách phim mới
    */

    public static final int TYPE_CATEGORY_DETAIL = 1;
    public static final int TYPE_COUNTRY_DETAIL = 2;
    public static final int TYPE_LIST_CATEGORIES = 3;
    public static final int TYPE_LIST_COUNTRIES = 4;
    public static final int TYPE_ODD = 5;
    public static final int TYPE_SERIES = 6;
    public static final int TYPE_VIEW_MORE = 7;
    public static final int TYPE_NEW = 8;
    private static final long serialVersionUID = -4666183656638927749L;
    @SerializedName("subTabID")
    private String subtabId;
    @SerializedName("countryID")
    private String countryId;
    @SerializedName("typeFilmID")
    private int typeFilmId;
    @SerializedName("eventID")
    private int eventId;
    @SerializedName("cate_type_name")
    private String cateTypeName;
    @SerializedName("total_sub_tab")
    private int totalSubtabs;
    @SerializedName("description")
    private String description;
    @SerializedName("type")
    private int type;
    @SerializedName("categoryid")
    private String categoryId;
    @SerializedName("categoryname")
    private String categoryName;
    @SerializedName("parentid")
    private int parentId;
    @SerializedName("id")
    private int id;
    @SerializedName("typeDisplay")
    private String typeDisplay;
    @SerializedName("url_images")
    private String image;
    private boolean isTabHome;
    private boolean isSubtab = true;

    public SubtabInfo() {
    }

    public SubtabInfo(MovieKind movieKind) {
        if (movieKind != null) {
            this.type = movieKind.getType();
            this.categoryId = movieKind.getCategoryId();
            this.categoryName = movieKind.getTitle();
            this.parentId = movieKind.getParentId();
            this.typeFilmId = movieKind.getTypeFilmId();
            this.subtabId = movieKind.getSubtabId();
            this.countryId = movieKind.getCountryId();
            this.eventId = movieKind.getEventId();
            this.totalSubtabs = movieKind.getTotalSubtabs();
            this.setSubtab(true);
        }
    }

    public String getSubtabId() {
        return subtabId;
    }

    public void setSubtabId(String subtabId) {
        this.subtabId = subtabId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public int getTypeFilmId() {
        return typeFilmId;
    }

    public void setTypeFilmId(int typeFilmId) {
        this.typeFilmId = typeFilmId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getCateTypeName() {
        return cateTypeName;
    }

    public void setCateTypeName(String cateTypeName) {
        this.cateTypeName = cateTypeName;
    }

    public int getTotalSubtabs() {
        return totalSubtabs;
    }

    public void setTotalSubtabs(int totalSubtabs) {
        this.totalSubtabs = totalSubtabs;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategoryIdInt() {
        try {
            return Integer.parseInt(categoryId);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTabHome() {
        return isTabHome;
    }

    public void setTabHome(boolean tabHome) {
        isTabHome = tabHome;
    }

    public boolean isMustLoadData() {
        return (isSubtab && totalSubtabs > 0) || (!isSubtab && (type == TYPE_LIST_COUNTRIES || type == TYPE_LIST_CATEGORIES));
    }

    public boolean isShowCategoryDetail() {
        return "1".equals(typeDisplay);
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isSubtab() {
        return isSubtab;
    }

    public void setSubtab(boolean subtab) {
        isSubtab = subtab;
    }

    @Override
    public String toString() {
        return "{" +
                "subtabId='" + subtabId + '\'' +
                ", countryId='" + countryId + '\'' +
                ", typeFilmId=" + typeFilmId +
                ", eventId=" + eventId +
                ", cateTypeName='" + cateTypeName + '\'' +
                ", totalSubtabs=" + totalSubtabs +
                ", description='" + description + '\'' +
                ", type=" + type +
                ", categoryId='" + categoryId + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", parentId=" + parentId +
                ", id=" + id +
                ", isTabHome=" + isTabHome +
                '}';
    }
}

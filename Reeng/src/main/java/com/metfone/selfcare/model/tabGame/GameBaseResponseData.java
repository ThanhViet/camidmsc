package com.metfone.selfcare.model.tabGame;

import com.google.gson.annotations.SerializedName;

public class GameBaseResponseData {
    @SerializedName("items")
    private GameCategoryModel[] items;
    @SerializedName("total")
    private long total;

    public GameBaseResponseData() {
    }

    public GameCategoryModel[] getItems() { return items; }
    public void setItems(GameCategoryModel[] value) { this.items = value; }

    public long getTotal() { return total; }
    public void setTotal(long value) { this.total = value; }
}


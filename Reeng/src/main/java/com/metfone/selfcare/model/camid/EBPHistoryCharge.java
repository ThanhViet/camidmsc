package com.metfone.selfcare.model.camid;

/**
 * Item History charge for Exchange, Basic, Promotion
 */
public class EBPHistoryCharge extends HistoryCharge {
    private int idResDrawableIcon;
    private String title;
    private Double value;
    private boolean isBasicTab = false;

    public EBPHistoryCharge(HistoryChargeType chargeType, int idResDrawableIcon, String title, Double value) {
        super(chargeType);
        this.idResDrawableIcon = idResDrawableIcon;
        this.title = title;
        this.value = value;
    }

    public int getIdResDrawableIcon() {
        return idResDrawableIcon;
    }

    public void setIdResDrawableIcon(int idResDrawableIcon) {
        this.idResDrawableIcon = idResDrawableIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public boolean isBasicTab() {
        return isBasicTab;
    }

    public void setBasicTab(boolean basicTab) {
        isBasicTab = basicTab;
    }
}

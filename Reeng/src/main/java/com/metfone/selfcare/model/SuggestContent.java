package com.metfone.selfcare.model;

public class SuggestContent {
    private String title;
    private String img;
    private int type;
    private String deeplink;

    public SuggestContent(String title, String img, int type, String deeplink) {
        this.title = title;
        this.img = img;
        this.type = type;
        this.deeplink = deeplink;
    }

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }

    public int getType() {
        return type;
    }

    public String getDeeplink() {
        return deeplink;
    }

    @Override
    public String toString() {
        return "SuggestContent{" +
                "title='" + title + '\'' +
                ", img='" + img + '\'' +
                ", type=" + type +
                ", deeplink='" + deeplink + '\'' +
                '}';
    }
}

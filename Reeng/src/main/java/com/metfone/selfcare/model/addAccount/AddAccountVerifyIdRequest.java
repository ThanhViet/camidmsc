package com.metfone.selfcare.model.addAccount;

public class AddAccountVerifyIdRequest {
    String phone_number;
    String account_id;

    public AddAccountVerifyIdRequest(String phone_number, String account_id) {
        this.phone_number = phone_number;
        this.account_id = account_id;
    }
}

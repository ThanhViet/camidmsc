package com.metfone.selfcare.model;

import java.io.Serializable;

public class LocationObject implements Serializable {
    private String countryCode = "";
    private String countryName = "";
    private String city = "";
    private String postal = "";
    private String latitude = "";
    private String longitude = "";
    private String IPv4 = "";
    private String state = "";

    public LocationObject() {
    }

    public LocationObject(String countryCode, String countryName, String city, String postal,
                          String latitude, String longitude, String IPv4, String state) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.city = city;
        this.postal = postal;
        this.latitude = latitude;
        this.longitude = longitude;
        this.IPv4 = IPv4;
        this.state = state;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIPv4() {
        return IPv4;
    }

    public void setIPv4(String IPv4) {
        this.IPv4 = IPv4;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

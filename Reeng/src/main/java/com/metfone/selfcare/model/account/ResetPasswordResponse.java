package com.metfone.selfcare.model.account;

public class ResetPasswordResponse {
    String message;
    String code;

    public ResetPasswordResponse(String message, String code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserOldResponse {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataUserOldResponse data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataUserOldResponse getData() {
        return data;
    }

    public void setData(DataUserOldResponse data) {
        this.data = data;
    }
}

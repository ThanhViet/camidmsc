package com.metfone.selfcare.model.camid;

public class SmsCallingDataCharge extends DetailSmsCallingDataCharge {
    private double money;
    private int quantity;
    private boolean isData;
    private int data;

    public SmsCallingDataCharge(double money, int quantity) {
        super(DetailType.CHARGE_TYPE_GROUP_SCDV_CHARGE);
        this.money = money;
        this.quantity = quantity;
        this.isData = false;
    }

    public SmsCallingDataCharge(double money, int quantity, int data, boolean isData) {
        super(DetailType.CHARGE_TYPE_GROUP_SCDV_CHARGE);
        this.money = money;
        this.quantity = quantity;
        this.isData = isData;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isData() {
        return isData;
    }

    public void setIsData(boolean data) {
        isData = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}

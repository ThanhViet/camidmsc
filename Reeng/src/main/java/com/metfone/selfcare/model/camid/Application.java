package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Application implements Serializable {
    @SerializedName("appCode")
    @Expose
    private String appCode;

    @SerializedName("appName")
    @Expose
    private String appName;

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}

package com.metfone.selfcare.model;

public interface Filterable {
    String getIdFilter();
}

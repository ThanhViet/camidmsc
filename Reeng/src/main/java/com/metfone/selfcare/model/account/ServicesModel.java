package com.metfone.selfcare.model.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServicesModel {
     int service_id;
     String service_code;
     String service_name;
     String status;
     int metfonePlus;
     int user_service_id;
     String phone_number;
     boolean header;

    public ServicesModel(int service_id, String service_code, String service_name, String status, int metfonePlus, int user_service_id, String phone_number, boolean header) {
        this.service_id = service_id;
        this.service_code = service_code;
        this.service_name = service_name;
        this.status = status;
        this.metfonePlus = metfonePlus;
        this.user_service_id = user_service_id;
        this.phone_number = phone_number;
        this.header = header;
    }
}

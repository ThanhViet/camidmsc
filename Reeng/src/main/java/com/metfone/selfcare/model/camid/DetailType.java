package com.metfone.selfcare.model.camid;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class DetailType {
    public static final int BALANCE_EXCHANGE = 0;
    public static final int BALANCE_BASIC = 1;
    public static final int BALANCE_DATA = 2;
    public static final int BALANCE_PROMOTION = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({BALANCE_EXCHANGE, BALANCE_BASIC, BALANCE_DATA, BALANCE_PROMOTION})
    public @interface BalanceType {}


    public static final String BALANCE_EXCHANGE_NAME = "exchange";
    public static final String BALANCE_BASIC_NAME = "basic";
    public static final String BALANCE_DATA_NAME = "data";
    public static final String BALANCE_PROMOTION_NAME = "promotion";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({BALANCE_EXCHANGE_NAME, BALANCE_BASIC_NAME, BALANCE_DATA_NAME, BALANCE_PROMOTION_NAME})
    public @interface BalanceName {}


    public static final String DATE_TYPE_TODAY = "today";
    public static final String DATE_TYPE_SEVEN_DAYS = "7days";
    public static final String DATE_TYPE_THIRTY_DAYS = "30days";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({DATE_TYPE_TODAY, DATE_TYPE_SEVEN_DAYS, DATE_TYPE_THIRTY_DAYS})
    public @interface DateType {}


    public static final String DATE_TODAY = "Today";
    public static final String DATE_SEVEN_DAYS = "7 days";
    public static final String DATE_THIRTY_DAYS = "30 days";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({DATE_TODAY, DATE_SEVEN_DAYS, DATE_THIRTY_DAYS})
    public @interface DateName {}


    public static final String CHARGE_TOTAL = "Total charge";
    public static final String CHARGE_CALLING = "Calling charge";
    public static final String CHARGE_SMS = "SMS charge";
    public static final String CHARGE_VAS = "VAS charge";
    public static final String CHARGE_DATA = "Data charge";
    public static final String CHARGE_MY_SERVICE = "My services";
    public static final String CHARGE_DATA_TOTAL = "Data total charge";
    public static final String CHARGE_DATA_DETAIL = "Data detail charge";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({CHARGE_TOTAL, CHARGE_CALLING, CHARGE_SMS, CHARGE_VAS, CHARGE_DATA, CHARGE_MY_SERVICE, CHARGE_DATA_TOTAL, CHARGE_DATA_DETAIL})
    public @interface ChargeName {}


    public static final int CHARGE_TYPE_GROUP_TOTAL = 0;
    public static final int CHARGE_TYPE_GROUP_CALLING_SMS_VAS_DATA = 1;
    public static final int CHARGE_TYPE_GROUP_MY_SERVICE = 2;
    public static final int CHARGE_TYPE_GROUP_DATA_TOTAL = 3;
    public static final int CHARGE_TYPE_GROUP_DATA_CHILDREN = 4;
    public static final int CHARGE_TYPE_GROUP_SMS_CHARGE = 5;
    public static final int CHARGE_TYPE_GROUP_CALLING_CHARGE = 6;
    public static final int CHARGE_TYPE_GROUP_DATA_CHARGE = 7;
    public static final int CHARGE_TYPE_GROUP_VAS_CHARGE = 8;
    // type parent for: Sms, calling, data, vas
    public static final int CHARGE_TYPE_GROUP_SCDV_CHARGE = 9;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({CHARGE_TYPE_GROUP_TOTAL, CHARGE_TYPE_GROUP_CALLING_SMS_VAS_DATA, CHARGE_TYPE_GROUP_MY_SERVICE, CHARGE_TYPE_GROUP_DATA_TOTAL, CHARGE_TYPE_GROUP_DATA_CHILDREN,
            CHARGE_TYPE_GROUP_SMS_CHARGE, CHARGE_TYPE_GROUP_CALLING_CHARGE, CHARGE_TYPE_GROUP_DATA_CHARGE, CHARGE_TYPE_GROUP_SCDV_CHARGE, CHARGE_TYPE_GROUP_VAS_CHARGE})
    public @interface ChargeTypeGroup {}


    public static final String CHARGE_TYPE_S_C_D = "scd";
    public static final String CHARGE_TYPE_CALLING = "calling";
    public static final String CHARGE_TYPE_SMS = "sms";
    public static final String CHARGE_TYPE_DATA = "data";
    public static final String CHARGE_TYPE_VAS = "vas";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({CHARGE_TYPE_S_C_D, CHARGE_TYPE_CALLING, CHARGE_TYPE_SMS, CHARGE_TYPE_DATA, CHARGE_TYPE_VAS})
    public @interface ChargeType {}
}

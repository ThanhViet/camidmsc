package com.metfone.selfcare.model.tab_video;

import java.io.Serializable;

/**
 * Created by tuanha00 on 3/7/2018.
 */

public class Category implements Serializable {

    private String id;
    private String name;
    private String urlImage;
    private String contentType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}

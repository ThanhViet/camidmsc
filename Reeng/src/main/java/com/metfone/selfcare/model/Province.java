package com.metfone.selfcare.model;

import com.google.gson.annotations.SerializedName;

public class Province {
    @SerializedName("title")
    private String title;
    @SerializedName("code")
    private String code;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

package com.metfone.selfcare.model.account;

import com.metfone.selfcare.model.BaseDataRequest;

public class ForgotPasswordResetRequest  extends BaseDataRequest<ResetPasswordInfor> {
    public ForgotPasswordResetRequest(ResetPasswordInfor wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        super(wsRequest, apiKey, sessionId, username, wsCode);
    }
}

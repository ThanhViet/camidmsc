package com.metfone.selfcare.model.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdentifyProvider {
     String identityProvider;
     String userId;
     String userName;
}

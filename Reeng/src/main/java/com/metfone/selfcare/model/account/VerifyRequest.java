package com.metfone.selfcare.model.account;

import com.metfone.selfcare.model.BaseDataRequest;

public class VerifyRequest extends BaseDataRequest<BaseServiceRequestInfo> {
    public VerifyRequest(BaseServiceRequestInfo wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        super(wsRequest, apiKey, sessionId, username, wsCode);
    }
}

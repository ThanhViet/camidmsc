package com.metfone.selfcare.model.account;

import com.metfone.selfcare.model.BaseDataRequest;

public class DeleteNumberRequest extends BaseDataRequest<DeleteNumber> {
    public DeleteNumberRequest(DeleteNumber wsRequest, String apiKey, String sessionId, String username, String wsCode) {
        super(wsRequest, apiKey, sessionId, username, wsCode);
    }
}

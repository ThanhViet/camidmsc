package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComTypeResponse;

import java.io.Serializable;

/**
 * Model for {@link WsGetComTypeResponse}
 */
public class TypeComplaint implements Serializable {
    @SerializedName("code")
    private String code;

    @SerializedName("compTemplate")
    private String compTemplate;

    @SerializedName("compTypeId")
    private long compTypeId;

    @SerializedName("description")
    private String description;

    @SerializedName("groupId")
    private long groupId;

    @SerializedName("name")
    private String name;

    @SerializedName("serviceType")
    private long serviceType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCompTemplate() {
        return compTemplate;
    }

    public void setCompTemplate(String compTemplate) {
        this.compTemplate = compTemplate;
    }

    public long getCompTypeId() {
        return compTypeId;
    }

    public void setCompTypeId(long compTypeId) {
        this.compTypeId = compTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getServiceType() {
        return serviceType;
    }

    public void setServiceType(long serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public String toString() {
        return "TypeComplaint{" +
                "code='" + code + '\'' +
                ", compTemplate='" + compTemplate + '\'' +
                ", compTypeId=" + compTypeId +
                ", description='" + description + '\'' +
                ", groupId=" + groupId +
                ", name='" + name + '\'' +
                ", serviceType=" + serviceType +
                '}';
    }
}

package com.metfone.selfcare.model.account;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneLinkedInfo {
    @SerializedName("user_service_id")
    int userServiceId;
    @SerializedName("phone_number")
    String phoneNumber;
    @SerializedName("status")
    String status;
    int metfonePlus;

}

package com.metfone.selfcare.model.tabMovie;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kind {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parentid")
    @Expose
    private Integer parentid;
    @SerializedName("categoryname")
    @Expose
    private String categoryname;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("categoryid")
    @Expose
    private Integer categoryid;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("url_images")
    @Expose
    private String urlImages;
    @SerializedName("settop")
    @Expose
    private Integer settop;

    private boolean isSelected;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlImages() {
        return urlImages;
    }

    public void setUrlImages(String urlImages) {
        this.urlImages = urlImages;
    }

    public Integer getSettop() {
        return settop;
    }

    public void setSettop(Integer settop) {
        this.settop = settop;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public enum Type {
        KIND(0), NATION(1);

        public int VALUE;

        Type(int VALUE) {
            this.VALUE = VALUE;
        }
    }
}

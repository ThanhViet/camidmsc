package com.metfone.selfcare.model.camid;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Function {
    @SerializedName("functionCode")
    @Expose
    private String functionCode;

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }
}

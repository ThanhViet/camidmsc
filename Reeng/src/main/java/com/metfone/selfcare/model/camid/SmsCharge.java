package com.metfone.selfcare.model.camid;

/**
 * {
 *     "phoneNumber": "096****945",
 *     "date": "06/10/2020",
 *     "time": "09:32:26",
 *     "money": 0.35
 * }
 */
public class SmsCharge extends DetailSmsCallingDataCharge {
    private String date;
    private String phoneNumber;
    private double money;
    private String time;

    public SmsCharge(String date, String phoneNumber, double money, String time) {
        super(DetailType.CHARGE_TYPE_GROUP_SMS_CHARGE);
        this.date = date;
        this.phoneNumber = phoneNumber;
        this.money = money;
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

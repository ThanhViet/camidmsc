package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 6/12/2017.
 */
public class QuickNewsOnMedia implements Serializable {

    @SerializedName("thumb_news")
    private String thumb = "";

    @SerializedName("title_news")
    private String title = "";

    @SerializedName("deeplink")
    private String deepLink = "";

    public QuickNewsOnMedia(String thumb, String title, String deepLink) {
        this.thumb = thumb;
        this.title = title;
        this.deepLink = deepLink;
    }

    public QuickNewsOnMedia() {
    }

    public String getThumb() {
        return thumb;
    }

    public String getTitle() {
        return title;
    }

    public String getDeepLink() {
        return deepLink;
    }

    @Override
    public String toString() {
        return "QuickNewsOnMedia{" +
                "thumb='" + thumb + '\'' +
                ", title='" + title + '\'' +
                ", deepLink='" + deepLink + '\'' +
                '}';
    }
}

package com.metfone.selfcare.database.constant;

/**
 * Created by thaodv on 6/30/2014.
 */
public class ThreadMessageConstant {
    public static final String THREAD_TABLE = "thread";
    public static final String THREAD_ID = "id";
    public static final String THREAD_NAME = "name"; //truong hop la group
    public static final String THREAD_IS_GROUP = "THREAD_IS_GROUP"; //1-1, group joined, group left
    public static final String THREAD_NUMBERS = "numbers"; // seperate by ';'
    public static final String THREAD_SERVER_ID = "thread_chat_room_id";
    public static final String THREAD_IS_JOINED = "thread_is_joined";
    public static final String THREAD_NUMBER_OF_UNREAD_MESSAGES = "num_unread_msg";
    public static final String THREAD_TIME_LAST_CHANGE = "time_last_change";
    public static final String THREAD_DRAFT_MESSAGE = "draft_msg";
    public static final String THREAD_LAST_TIME_SHARE_MUSIC = "last_time_share_music";
    public static final String THREAD_BACKGROUND = "background";
    public static final String THREAD_LAST_TIME_SAVE_DRAFT = "last_time_save_draft";
    public static final String THREAD_STATE = "thread_state";// an hoac hien thread khi ko co message
    public static final String THREAD_GROUP_AVATAR = "time_last_show_invite"; // không co kịch ban show invite nen su dung lai truong nay de luu group avatar
    public static final String THREAD_LAST_MESSAGE_ID = "last_message_id";
    public static final String THREAD_ADMIN_NUMBERS = "admin_numbers";
    public static final String THREAD_GROUP_CLASS = "group_class";
    public static final String THREAD_PIN_MESSAGE_JSON = "pin_message";
    public static final String THREAD_LAST_TIME_PIN_THREAD = "last_time_pin";
    public static final String THREAD_HIDDEN = "thread_hidden";
    //STATEMENT
    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + THREAD_TABLE
            + "("
            + THREAD_ID
            + " INTEGER PRIMARY KEY, "
            + THREAD_NAME
            + " TEXT, "
            + THREAD_IS_GROUP
            + " INTEGER, "
            + THREAD_NUMBERS
            + " TEXT, "
            + THREAD_SERVER_ID
            + " TEXT, "
            + THREAD_IS_JOINED
            + " INTEGER, "
            + THREAD_NUMBER_OF_UNREAD_MESSAGES
            + " INTEGER, "
            + THREAD_TIME_LAST_CHANGE
            + " TEXT, "
            + THREAD_DRAFT_MESSAGE
            + " TEXT, "
            + THREAD_BACKGROUND
            + " TEXT, "
            + THREAD_LAST_TIME_SHARE_MUSIC
            + " INTEGER, "
            + THREAD_LAST_TIME_SAVE_DRAFT
            + " INTEGER, "
            + THREAD_STATE
            + " INTEGER, "
            + THREAD_GROUP_AVATAR
            + " TEXT, "
            + THREAD_LAST_MESSAGE_ID
            + " INTEGER, "
            + THREAD_ADMIN_NUMBERS
            + " TEXT, "
            + THREAD_GROUP_CLASS
            + " INTEGER, "
            + THREAD_PIN_MESSAGE_JSON
            + " TEXT, "
            + THREAD_LAST_TIME_PIN_THREAD
            + " TEXT, "
            + THREAD_HIDDEN
            + " INTEGER"
            + ");";

    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + THREAD_TABLE;
    public static final String THREAD_SELECT_ALL_QUERY = "SELECT * FROM "
            + THREAD_TABLE;
    public static final String THREAD_DELETE_QUERY = "DELETE FROM "
            + THREAD_TABLE + " WHERE " + THREAD_ID + " = ";
    public static final String THREAD_SELECT_BY_ID_QUERY = "SELECT * FROM " + THREAD_TABLE + " WHERE " + THREAD_ID + " = ";
    public static final String THREAD_TRUNCATE_QUERY = "DELETE FROM "
            + THREAD_TABLE;

    public static final String ALTER_COLUMN_DRAFT = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_DRAFT_MESSAGE + " TEXT;";
    public static final String ALTER_COLUMN_BACKGROUND = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_BACKGROUND + " TEXT;";
    public static final String ALTER_COLUMN_LAST_TIME_SHARE_MUSTIC = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_LAST_TIME_SHARE_MUSIC + " INTEGER;";
    public static final String ALTER_COLUMN_LAST_TIME_SAVE_DRAFT = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_LAST_TIME_SAVE_DRAFT + " INTEGER;";
    public static final String ALTER_COLUMN_THREAD_STATE = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_STATE + " INTEGER DEFAULT 0;";
    public static final String ALTER_COLUMN_THREAD_GROUP_AVATAR = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_GROUP_AVATAR + " TEXT;";
    public static final String ALTER_COLUMN_LAST_MESSAGE_ID = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_LAST_MESSAGE_ID + " INTEGER DEFAULT -2;";
    public static final String ALTER_COLUMN_ADMIN_NUMBERS = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_ADMIN_NUMBERS + " TEXT;";
    public static final String ALTER_COLUMN_GROUP_CLASS = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_GROUP_CLASS + " INTEGER;";
    public static final String ALTER_COLUMN_THREAD_PIN_MESSAGE_JSON = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_PIN_MESSAGE_JSON + " TEXT;";
    public static final String ALTER_COLUMN_THREAD_LAST_TIME_PIN_THREAD = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_LAST_TIME_PIN_THREAD + " TEXT;";
    public static final String ALTER_COLUMN_THREAD_HIDDEN = "ALTER TABLE " + THREAD_TABLE
            + " ADD COLUMN " + THREAD_HIDDEN + " INTEGER;";


    /**
     * type of thread is person chat
     */
    public static final int TYPE_THREAD_PERSON_CHAT = 0;
    public static final String THREAD_SELECT_ONE_PERSON_QUERY = "SELECT * FROM "
            + THREAD_TABLE
            + " WHERE "
            + THREAD_IS_GROUP
            + " = "
            + TYPE_THREAD_PERSON_CHAT;
    /**
     * type of thread is group chat
     */
    public static final int TYPE_THREAD_GROUP_CHAT = 1;

    public static final int TYPE_THREAD_GROUP_CHAT_SETTTING = 50;

    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + THREAD_TABLE;
    /**
     * type of thread is official chat
     */
    public static final int TYPE_THREAD_OFFICER_CHAT = 2;
    /**
     * type of thread is room chat
     */
    public static final int TYPE_THREAD_ROOM_CHAT = 3;
    /**
     * type of thread is broadcast chat
     */
    public static final int TYPE_THREAD_BROADCAST_CHAT = 4;
    /**
     * state visible
     */
    public static final int STATE_NEED_GET_INFO = -1;
    public static final int STATE_SHOW = 0;
    public static final int STATE_HIDE = 1;
    public static final int STATE_PRIVATE_SHOW = 2;// nhom private nhung dang de an khong show trong inbox
    public static final int STATE_PRIVATE_HIDE = 3;// nhom private da dc show trong inbox

    public static final int GROUP_TYPE_NORMAL = 0;
    public static final int GROUP_TYPE_CLASS = 1;// group class ==1, còn lại là group thường
    public static final int THREAD_ENCRYPT = 2; //lưu thêm trạng thái là cuộc hội thoại mã hóa
    /**
     * last thread id default
     */
    public static final int LAST_MESSAGE_ID_NOT_SYNC = -2;// nang cap db thi set =-2, load lai lan dau thi set dung gt
    public static final int LAST_MESSAGE_ID_DEFAULT = -1;// tao moi thread, chua co message thi set la -1 de ko query
}

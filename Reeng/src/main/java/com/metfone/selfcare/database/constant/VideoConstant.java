package com.metfone.selfcare.database.constant;

public final class VideoConstant {


    public enum Type {
        LATER("Later"), SAVED("Save");

        public String VALUE;

        Type(String VALUE) {
            this.VALUE = VALUE;
        }
    }

    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String COUNT = "count";
    public static final String TABLE = "VIDEO";
    public static final String CONTENT = "content";
    public static final String VIDEO_ID = "videoId";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE + " (" + ID + " INTEGER PRIMARY KEY, " + VIDEO_ID + " TEXT, " + TYPE + " TEXT, " + CONTENT + " TEXT)";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String SELECT_STATEMENT_BY_ID_AND_TYPE = "SELECT * FROM " + TABLE + " WHERE " + VIDEO_ID + " = ?" + " AND " + TYPE + " = ? ORDER BY ID DESC";
    public static final String SELECT_STATEMENT_BY_TYPE = "SELECT * FROM " + TABLE + " WHERE " + TYPE + " = ? ORDER BY ID DESC";
    public static final String SELECT_STATEMENT_COUNT_BY_TYPE = "SELECT COUNT(" + VIDEO_ID + ") AS " + COUNT + " FROM " + TABLE + " WHERE " + TYPE + " = ? ";
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;
    public static final String DELETE_LAST_STATEMENT_BY_TYPE = "DELETE FROM " + TABLE + " WHERE " +
            ID + " = (SELECT MIN(" + ID + ") FROM " + TABLE + " WHERE " + TYPE + " = ? )" +
            " AND " +
            "(SELECT COUNT(" + ID + ") FROM " + TABLE + " WHERE " + TYPE + " = ?) >= ? ;";


}

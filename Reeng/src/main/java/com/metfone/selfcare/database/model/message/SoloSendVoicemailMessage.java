package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.MessageType;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Date;

/**
 * Created by thaodv on 7/12/2014.
 */
public class SoloSendVoicemailMessage extends ReengMessage {
    //  String filePath, String fileName,
    //  FileType fileType, MessageType messageType, int duration
    public SoloSendVoicemailMessage(ThreadMessage thread, String from, String to,
                                    String filePath, int duration, String fileName) {
        super(thread.getThreadType(), MessageType.voicemail);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setFilePath(filePath);
        setDuration(duration);
        setFileName(fileName);
        setReadState(ReengMessageConstant.READ_STATE_READ);
        setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        setMessageType(MessageType.voicemail);
        setDirection(Direction.send);
        Date date = new Date();
        setTime(date.getTime());
    }
}

package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thaodv on 09-Feb-15.
 */
public class StickerDataSource {
    private static String TAG = StickerDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static StickerDataSource stickerDataSource;

    private StickerDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    public static synchronized StickerDataSource getInstance(ApplicationController application) {
        if (stickerDataSource == null) {
            stickerDataSource = new StickerDataSource(application);
        }
        return stickerDataSource;
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    public List<StickerCollection> getAllStickerCollection() {
        List<StickerCollection> stickerCollections = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return stickerCollections;
            }
            cursor = databaseRead.rawQuery(StickerConstant.COLLECTION_SELECT_ALL_QUERY, null);
            if (cursor != null && cursor.getCount() > 0) {
                stickerCollections = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        StickerCollection stickerCollection = getStickerCollectionFromCursor(cursor);
                        // set phone number
                        stickerCollections.add(stickerCollection);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllStrangerPhoneNumber ", e);
        } finally {
            closeCursor(cursor);
        }
        return stickerCollections;
    }

    private ContentValues fromObjectToValue(StickerCollection stickerCollection) {
        ContentValues values = new ContentValues();
        values.put(StickerConstant.COLLECTION_SERVER_ID, stickerCollection.getServerId());
        values.put(StickerConstant.COLLECTION_NUMBER, stickerCollection.getNumberSticker());
        if (stickerCollection.isDownloaded()) {
            values.put(StickerConstant.COLLECTION_DOWNLOADED, 1);
        } else {
            values.put(StickerConstant.COLLECTION_DOWNLOADED, 0);
        }
        values.put(StickerConstant.COLLECTION_ICON_PATH, stickerCollection.getCollectionIconPath());
        values.put(StickerConstant.COLLECTION_TYPE, stickerCollection.getCollectionType());
        values.put(StickerConstant.COLLECTION_NAME, stickerCollection.getCollectionName());
        values.put(StickerConstant.COLLECTION_STATE, stickerCollection.getCollectionState());
        values.put(StickerConstant.COLLECTION_IMAGE_PREVIEW, stickerCollection.getCollectionPreviewPath());
        values.put(StickerConstant.COLLECTION_LAST_STICKY, String.valueOf(stickerCollection.getLastSticky()));
        if (stickerCollection.isNew()) {
            values.put(StickerConstant.COLLECTION_IS_NEW, 1);
        } else {
            values.put(StickerConstant.COLLECTION_IS_NEW, 0);
        }
        values.put(StickerConstant.COLLECTION_IS_DEFAULT, stickerCollection.isDefault());
        if (stickerCollection.getLastLocalUpdate() != 0) {
            values.put(StickerConstant.COLLECTION_LAST_LOCAL_UPDATE, stickerCollection.getLastLocalUpdate());
        }
        values.put(StickerConstant.COLLECTION_PREFIX, stickerCollection.getCollectionPrefix());
        values.put(StickerConstant.COLLECTION_IS_STICKY, stickerCollection.isSticky());
        values.put(StickerConstant.COLLECTION_LAST_SERVER_UPDATE, stickerCollection.getLastServerUpdate());
        values.put(StickerConstant.COLLECTION_LAST_ITEM_UPDATE, stickerCollection.getLastInfoUpdate());
        values.put(StickerConstant.COLLECTION_ORDER, stickerCollection.getOrder());
        return values;
    }

    public void insertStickerCollection(StickerCollection collection) {
        if (collection == null) {
            return;
        }
        if (databaseWrite == null) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values = fromObjectToValue(collection);
                // da add roster roi =1; chua add =0
                long id = databaseWrite.insert(StickerConstant.COLLECTION_TABLE, null, values);
                collection.setLocalId(id);
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public void insertListStickerCollection(List<StickerCollection> listCollections) {
        if (listCollections == null || listCollections.isEmpty()) {
            return;
        }
        if (databaseWrite == null) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (StickerCollection collection : listCollections) {
                    ContentValues values = fromObjectToValue(collection);
                    // da add roster roi =1; chua add =0
                    long id = databaseWrite.insert(StickerConstant.COLLECTION_TABLE, null, values);
                    collection.setLocalId(id);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public void updateStickerCollection(StickerCollection stickerCollection) {
        if (stickerCollection == null) {
            return;
        }
        try {
            String whereClause = StickerConstant.COLLECTION_SERVER_ID +
                    " = " + stickerCollection.getServerId();
            databaseWrite.update(StickerConstant.COLLECTION_TABLE,
                    fromObjectToValue(stickerCollection), whereClause, null);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public void updateListStickerCollection(List<StickerCollection> stickerCollections) {
        if (stickerCollections == null || stickerCollections.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (StickerCollection collection : stickerCollections) {
                    String whereClause = StickerConstant.COLLECTION_SERVER_ID +
                            " = " + collection.getServerId();
                    databaseWrite.update(StickerConstant.COLLECTION_TABLE,
                            fromObjectToValue(collection), whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public StickerCollection getStickerCollectionFromCursor(Cursor cursor) {
        StickerCollection stickerCollection = new StickerCollection();
        stickerCollection.setLocalId(cursor.getInt(0));
        stickerCollection.setServerId(cursor.getInt(1));
        stickerCollection.setNumberSticker(cursor.getInt(2));
        if (cursor.getInt(3) == 0) {
            stickerCollection.setDownloaded(false);
        } else {
            stickerCollection.setDownloaded(true);
        }
        stickerCollection.setCollectionIconPath(cursor.getString(4));
        stickerCollection.setCollectionName(cursor.getString(5));
        stickerCollection.setCollectionType(cursor.getInt(6));
        stickerCollection.setCollectionPreviewPath(cursor.getString(7));
        // preview =7,COLLECTION_COLUMN_9 = 8
        stickerCollection.setLastSticky(TextHelper.parserLongFromString(cursor.getString(8), 0));
        stickerCollection.setCollectionState(cursor.getInt(9));
        if (cursor.getInt(10) == 0) {
            stickerCollection.setIsNew(false);
        } else {
            stickerCollection.setIsNew(true);
        }
        stickerCollection.setIsDefault(cursor.getInt(11));
        stickerCollection.setLastLocalUpdate(cursor.getLong(12));
        stickerCollection.setCollectionPrefix(cursor.getString(13));
        stickerCollection.setIsSticky(cursor.getInt(14));
        stickerCollection.setLastServerUpdate(cursor.getLong(15));
        stickerCollection.setLastInfoUpdate(cursor.getLong(16));
        stickerCollection.setOrder(cursor.getInt(17));
        return stickerCollection;
    }

    public void deleteAllStickerCollection() {
        try {
            databaseWrite.execSQL(StickerConstant.DELETE_ALL_COLLECTION_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }
    //////////////////// TODO recent sticker /////////////

    /**
     * get contentValues from obj item sticker
     *
     * @param item
     * @return contentValues
     */
    private ContentValues getValuesFromStickerItemRecent(StickerItem item) {
        ContentValues values = new ContentValues();
        values.put(StickerConstant.RECENT_ITEM_ID, item.getItemId());
        values.put(StickerConstant.RECENT_COLLECTION_ID, item.getCollectionId());
        values.put(StickerConstant.RECENT_TIME, item.getRecentTime());
        return values;
    }

    /**
     * insert sticker recent
     *
     * @param item
     * @return table id
     */
    public long insertStickerRecent(StickerItem item) {
        ContentValues values = getValuesFromStickerItemRecent(item);
        long id;
        try {
            id = databaseWrite.insert(StickerConstant.RECENT_TABLE, null, values);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
            id = -1;
        }
        return id;
    }

    /**
     * get stickerItem from cursor
     *
     * @param cursor
     * @return stickerItem
     */
    public StickerItem getStickerRecentFromCursor(Cursor cursor) {
        StickerItem stickerItem = new StickerItem();
        stickerItem.setId(cursor.getLong(0));
        stickerItem.setItemId(cursor.getInt(1));
        stickerItem.setCollectionId(cursor.getInt(3));
        stickerItem.setRecentTime(cursor.getLong(4));
        return stickerItem;
    }

    /**
     * get all stickerItemRecent from DB
     *
     * @return list stickerItem
     */
    public ArrayList<StickerItem> getAllStickerRecent() {
        ArrayList<StickerItem> stickerItems = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return stickerItems;
            }
            cursor = databaseRead.rawQuery(StickerConstant.SELECT_RECENT_ALL_QUERY, null);
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        StickerItem item = getStickerRecentFromCursor(cursor);
                        stickerItems.add(item);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        } finally {
            closeCursor(cursor);
        }
        return stickerItems;
    }

    /**
     * update recent item
     *
     * @param item
     */
    public void updateStickerRecent(StickerItem item) {
        if (item == null) {
            return;
        }
        try {
            String whereClause = StickerConstant.RECENT_ID + " = " + item.getId();
            databaseWrite.update(StickerConstant.RECENT_TABLE,
                    getValuesFromStickerItemRecent(item), whereClause, null);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    /**
     * xoa recent item trong db
     *
     * @param item
     */
    public void removeStickerRecent(StickerItem item) {
        if (item == null) {
            return;
        }
        try {
            String whereClause = StickerConstant.RECENT_ID + " = " + item.getId();
            databaseWrite.delete(StickerConstant.RECENT_TABLE, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    /**
     * xoa list sticker item
     *
     * @param listStickerRecent
     */
    public void removeListStickerRecent(List<StickerItem> listStickerRecent) {
        if (listStickerRecent == null || listStickerRecent.isEmpty() || databaseWrite == null) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (StickerItem item : listStickerRecent) {
                    String whereClause = StickerConstant.RECENT_ID + " = " + item.getId();
                    databaseWrite.delete(StickerConstant.RECENT_TABLE, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public void deleteAllStickerRecent() {
        try {
            databaseWrite.execSQL(StickerConstant.DELETE_ALL_RECENT_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(StickerConstant.DELETE_ALL_COLLECTION_STATEMENT);
            databaseWrite.execSQL(StickerConstant.DELETE_ALL_STICKER_ITEM_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }
    ///////////////////////TODO sticker item /////////////////////

    /**
     * get contentValues from obj item sticker
     *
     * @param item
     * @return contentValues
     */
    private ContentValues getValuesStickerItem(StickerItem item) {
        ContentValues values = new ContentValues();
        values.put(StickerConstant.STICKER_ITEM_ID, item.getItemId());
        values.put(StickerConstant.STICKER_COLLECTION_ID, item.getCollectionId());
        values.put(StickerConstant.STICKER_TYPE, item.getType());
        values.put(StickerConstant.STICKER_IMAGE_PATH, item.getImagePath());
        values.put(StickerConstant.STICKER_VOICE_PATH, item.getVoicePath());
        if (item.isDownloadImg()) {
            values.put(StickerConstant.STICKER_IS_DOWNLOAD_IMAGE, 1);
        } else {
            values.put(StickerConstant.STICKER_IS_DOWNLOAD_IMAGE, 0);
        }
        if (item.isDownloadVoice()) {
            values.put(StickerConstant.STICKER_IS_DOWNLOAD_VOICE, 1);
        } else {
            values.put(StickerConstant.STICKER_IS_DOWNLOAD_VOICE, 0);
        }
        return values;
    }

    /**
     * get stickerItem from cursor
     *
     * @param cursor
     * @return stickerItem
     */
    public StickerItem getStickerItemFromCursor(Cursor cursor) {
        StickerItem stickerItem = new StickerItem();
        stickerItem.setId(cursor.getLong(0));
        stickerItem.setItemId(cursor.getInt(1));
        stickerItem.setCollectionId(cursor.getInt(2));
        stickerItem.setType(cursor.getString(3));
        stickerItem.setImagePath(cursor.getString(4));
        stickerItem.setVoicePath(cursor.getString(5));
        if (cursor.getInt(6) == 0) {
            stickerItem.setDownloadImg(false);
        } else {
            stickerItem.setDownloadImg(true);
        }
        if (cursor.getInt(7) == 0) {
            stickerItem.setDownloadVoice(false);
        } else {
            stickerItem.setDownloadVoice(true);
        }
        return stickerItem;
    }


    /**
     * get all stickerItem from DB
     *
     * @return list stickerItem
     */
    public ArrayList<StickerItem> getAllStickerItem() {
        ArrayList<StickerItem> stickerItems = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return stickerItems;
            }
            cursor = databaseRead.rawQuery(StickerConstant.SELECT_STICKER_ITEM_ALL_QUERY, null);
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        StickerItem item = getStickerItemFromCursor(cursor);
                        stickerItems.add(item);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        } finally {
            closeCursor(cursor);
        }
        return stickerItems;
    }

    /**
     * insert sticker item
     *
     * @param item
     * @return table id
     */
    public void insertStickerItem(StickerItem item) {
        ContentValues values = getValuesStickerItem(item);
        try {
            long id = databaseWrite.insert(StickerConstant.STICKER_TABLE, null, values);
            item.setId(id);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    /**
     * insert list sticker item
     *
     * @param stickerItems
     */
    public void insertListStickerItem(List<StickerItem> stickerItems) {
        if (stickerItems == null || stickerItems.isEmpty() || databaseWrite == null) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (StickerItem sticker : stickerItems) {
                    ContentValues values = getValuesStickerItem(sticker);
                    // da add roster roi =1; chua add =0
                    long id = databaseWrite.insert(StickerConstant.STICKER_TABLE, null, values);
                    sticker.setId(id);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    /**
     * update sticker item
     *
     * @param item
     */
    public void updateStickerItem(StickerItem item) {
        if (item == null || databaseWrite == null) {
            return;
        }
        try {
            String whereClause = StickerConstant.STICKER_ID + " = " + item.getId();
            databaseWrite.update(StickerConstant.STICKER_TABLE,
                    getValuesStickerItem(item), whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * update list sticker item
     *
     * @param listStickerItems
     */
    public void updateListStickerItem(List<StickerItem> listStickerItems) {
        if (listStickerItems == null || listStickerItems.isEmpty() || databaseWrite == null) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (StickerItem item : listStickerItems) {
                    ContentValues values = getValuesStickerItem(item);
                    String whereClause = StickerConstant.STICKER_ID + " = " + item.getId();
                    databaseWrite.update(StickerConstant.STICKER_TABLE, values, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    /**
     * xoa sticker item trong db
     *
     * @param item
     */
    public void deleteStickerItem(StickerItem item) {
        if (item == null || databaseWrite == null) {
            return;
        }
        try {
            String whereClause = StickerConstant.STICKER_ID + " = " + item.getId();
            databaseWrite.delete(StickerConstant.STICKER_TABLE, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * xoa list sticker item
     *
     * @param listStickerItems
     */
    public void deleteListStickerItem(List<StickerItem> listStickerItems) {
        if (listStickerItems == null || listStickerItems.isEmpty() || databaseWrite == null) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (StickerItem item : listStickerItems) {
                    String whereClause = StickerConstant.STICKER_ID + " = " + item.getId();
                    databaseWrite.delete(StickerConstant.STICKER_TABLE, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public void deleteAllStickerItem() {
        try {
            databaseWrite.execSQL(StickerConstant.DELETE_ALL_STICKER_ITEM_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }

    /**
     * xoa tat ca bang sticker khi deactive
     */
    public void deleteAllTabelStickers() {
        deleteAllStickerCollection();
        deleteAllStickerRecent();
        deleteAllStickerItem();
    }
}
package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.PollConstant;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 10/29/2018.
 */

public class PollDataSource {
    private static String TAG = PollDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static PollDataSource mInstance;
    private ApplicationController mApp;


    public static synchronized PollDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new PollDataSource(application);
        }
        return mInstance;
    }

    private PollDataSource(ApplicationController application) {
        mApp = application;
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null)
            cursor.close();
    }

    public boolean insertPollObject(String serverId, String content) {
        try {
            if (databaseWrite == null) {
                return false;
            }
            long index = databaseWrite.insert(PollConstant.TABLE, null, setValues(serverId, content));
            Log.i(TAG, "index: " + index);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "insertLog fail", e);
        }
        return false;
    }


    public boolean updatePollObject(String serverId, String content) {
        if (TextUtils.isEmpty(serverId)) {
            Log.f(TAG, "updatePollObject empty server id");
            return false;
        }
        if (TextUtils.isEmpty(content)) {
            Log.f(TAG, "updatePollObject empty content");
            return false;
        }
        try {
            String whereClause = PollConstant.SERVER_ID +
                    " = '" + serverId + "'";
            int numb = databaseWrite.update(PollConstant.TABLE,
                    setValues(serverId, content), whereClause, null);
            Log.i(TAG, "numb: " + numb);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return false;
    }

    public PollObject findPollObject(String serverId) {
        if (TextUtils.isEmpty(serverId)) return null;
        PollObject PollObject = null;
        Cursor cursor = null;
        try {
            String query = PollConstant.POLL_SELECT_BY_ID_QUERY
                    + "'" + serverId + "'";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    PollObject = getPollObjectFromCursor(cursor);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return PollObject;
    }

    public boolean isExistPollId(String serverId) {
        if (TextUtils.isEmpty(serverId)) return false;
        Cursor cursor = null;
        try {
            String query = PollConstant.POLL_SELECT_BY_ID_QUERY
                    + "'" + serverId + "'";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                return true;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return false;
    }

    private PollObject getPollObjectFromCursor(Cursor cursor) {
        String content = cursor.getString(2);
        PollObject pollObject = PollRequestHelper.getInstance(mApp).parserPollObjectFromMessage(content);
        return pollObject;
    }

    private ContentValues setValues(String serverId, String content) {
        ContentValues values = new ContentValues();
        values.put(PollConstant.CONTENT, content);
        values.put(PollConstant.SERVER_ID, serverId);
        return values;
    }

    // del table
    public void deleteTable() {
        try {
            databaseWrite.execSQL(PollConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }
}

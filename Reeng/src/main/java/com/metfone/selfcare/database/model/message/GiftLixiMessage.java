package com.metfone.selfcare.database.model.message;

import android.text.TextUtils;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.GiftLixiModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by thanhnt72 on 1/25/2018.
 */

public class GiftLixiMessage extends ReengMessage {

    public GiftLixiMessage(ThreadMessage thread, String from, String to, GiftLixiModel giftLixiModel) {
        super(thread.getThreadType(), ReengMessageConstant.MessageType.lixi);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        setDirection(ReengMessageConstant.Direction.send);
        Date date = new Date();
        setTime(date.getTime());
        setReadState(ReengMessageConstant.READ_STATE_READ);

        setMessageType(ReengMessageConstant.MessageType.lixi);
        setImageUrl(String.valueOf(giftLixiModel.getAmountMoney()));
        setContent(giftLixiModel.getMessageContent());
        setDuration(giftLixiModel.getSplitRandom());
        setFileId(giftLixiModel.getOrderId());
        setFilePath(giftLixiModel.getRequestId());
        setVideoContentUri(getListMemberLixi(giftLixiModel.getListMember()));
    }

    private String getListMemberLixi(ArrayList<String> listMember) {
        StringBuilder listMemberStr = new StringBuilder();
        for (String s : listMember) {
            if (!TextUtils.isEmpty(listMemberStr.toString())) {
                listMemberStr.append(",");
            }
            listMemberStr.append(s);
        }
        return listMemberStr.toString();
    }
}

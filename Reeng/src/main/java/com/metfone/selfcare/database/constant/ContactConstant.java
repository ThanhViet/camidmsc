package com.metfone.selfcare.database.constant;

/**
 * Created by toanvk2 on 6/26/14.
 */
public class ContactConstant {
    public static final String TABLE = "contact";
    public static final String ID = "contact_id";
    public static final String NAME = "name";
    public static final String FAVORITE = "favorite";
    public static final String NAME_UNICODE = "name_unicode";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE +
            " (" + ID + " TEXT, " + NAME + " TEXT, " +
            FAVORITE + " INTEGER, " + NAME_UNICODE + " TEXT)";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String SELECT_BY_CONTACT_ID_STATEMENT = "SELECT * FROM "
            + TABLE + " WHERE " + ID + " = ";
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;
    public static final String WHERE_ID = ID + " = ?";
}
package com.metfone.selfcare.database.model.message;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 3/1/2018.
 */

public class PinMessage implements Serializable {

    private static final String TAG = PinMessage.class.getSimpleName();

    @SerializedName("type")
    private long type;
    @SerializedName("content")
    private String content;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("target")
    private String target;
    @SerializedName("threadType")
    private int threadType;
    @SerializedName("expired")
    private long expired;

    public PinMessage() {
    }

    public PinMessage(long type, String content, String title, String image, String target) {
        this.type = type;
        this.content = content;
        this.title = title;
        this.image = image;
        this.target = target;
    }

    public long getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getTarget() {
        return target;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getThreadType() {
        return threadType;
    }

    public void setThreadType(int threadType) {
        this.threadType = threadType;
    }

    public long getExpired() {
        return expired;
    }

    public void setExpired(long expired) {
        this.expired = expired;
    }

    public String toJson() {
        if (TextUtils.isEmpty(title) || TextUtils.isEmpty(target)) return "";
        JSONObject obj = new JSONObject();
        try {
            obj.put("title", title);
            obj.put("img", image);
            obj.put("type", type);
            obj.put("target", target);
            obj.put("content", content);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return "";
        }
        return obj.toString();
    }

    public void fromJson(String json) {
        if (TextUtils.isEmpty(json)) return;
        try {
            JSONObject object = new JSONObject(json);
            setTitle(object.optString("title"));
            setImage(object.optString("img"));
            setType(object.optInt("type"));
            setTarget(object.optString("target"));
            setContent(object.optString("content"));

        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public enum ActionPin {
        ACTION_PIN(1),
        ACTION_UNPIN(0);
        public int VALUE;

        ActionPin(int VALUE) {
            this.VALUE = VALUE;
        }
    }

    public enum TypePin {
        TYPE_DEEPLINK(0),
        TYPE_MESSAGE(1),
        TYPE_SPECIAL_THREAD(2),
        TYPE_VOTE(3);
        public int VALUE;

        TypePin(int VALUE) {
            this.VALUE = VALUE;
        }
    }

    public enum SpecialThread {
        THREAD_CONTACT(1),
        THREAD_STRANGER(2),
        THREAD_GROUP(3);
        public int VALUE;

        SpecialThread(int VALUE) {
            this.VALUE = VALUE;
        }

    }
}
package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class Page implements Serializable {
    @SerializedName("id_memory")
    private String bookId;
    @SerializedName("page")
    private int page;
    @SerializedName("assigned")
    private String assigned;
    @SerializedName("preview")
    private String preview;
    @SerializedName("name")
    private String name;
    @SerializedName("owner_name")
    private String ownerName;
    @SerializedName("common_info")
    private Common common = new Common();
    @SerializedName("background_info")
    private Background background;
    @SerializedName("objects_info")
    private ArrayList<PageItem> pageItems;

    private int position = -1;

    public Page() {
    }

    public Page(Page other) {
        if (other != null) {
            this.page = other.getPage();
            this.bookId = other.getBookId();
            this.name = other.getName();
            this.assigned = other.getAssigned();
            this.preview = other.getPreview();
            this.common = new Common(other.getCommon());
            this.position = other.getPosition();
            this.ownerName = other.getOwnerName();
            Log.d("PAGE", "copy value ownerName: " + other.getOwnerName());
        }
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getAssigned() {
        return assigned;
    }

    public void setAssigned(String assigned) {
        this.assigned = assigned;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }

    public Background getBackground() {
        return background;
    }

    public void setBackground(Background background) {
        this.background = background;
    }

    public ArrayList<PageItem> getPageItems() {
        return pageItems;
    }

    public void setPageItems(ArrayList<PageItem> pageItems) {
        this.pageItems = pageItems;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}

package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/5/2015.
 */
public class ImageProfileDataSource {
    private static String TAG = ImageProfileDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static ImageProfileDataSource mImageProfileDataSource;

    private ImageProfileDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    public static synchronized ImageProfileDataSource getInstance(ApplicationController application) {
        if (mImageProfileDataSource == null) {
            mImageProfileDataSource = new ImageProfileDataSource(application);
        }
        return mImageProfileDataSource;
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    public ArrayList<ImageProfile> getAllImageProfile() {
        ArrayList<ImageProfile> listImageProfile = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return listImageProfile;
            }
            cursor = databaseRead.rawQuery(ImageProfileConstant.SELECT_ALL_IMAGES_PROFILE_UPLOADED_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                listImageProfile = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        ImageProfile item = getImageProfileFromCursor(cursor);
                        //   Log.e(TAG, "imageprofile: " + item);
                        listImageProfile.add(item);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        } finally {
            closeCursor(cursor);
        }
        return listImageProfile;
    }

    public ImageProfile getImageCover() {
        ImageProfile cover = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return cover;
            }
            cursor = databaseRead.rawQuery(ImageProfileConstant.SELECT_COVER_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                cover = new ImageProfile();
                if (cursor.moveToFirst()) {
                    cover = getImageProfileFromCursor(cursor);
                }
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        } finally {
            closeCursor(cursor);
        }
        return cover;
    }

    public ImageProfile getImageProfileFromCursor(Cursor cursor) {
        ImageProfile imageProfile = new ImageProfile();
        imageProfile.setId(cursor.getLong(0));
        imageProfile.setTypeImage(cursor.getInt(1));
        imageProfile.setImagePathLocal(cursor.getString(2));
        imageProfile.setImageUrl(cursor.getString(3));
        if (cursor.getInt(4) == 0) {
            imageProfile.setUpload(false);
        } else {
            imageProfile.setUpload(true);
        }
        imageProfile.setTime(Long.valueOf(cursor.getString(5)));
//        imageProfile.setIdServer(cursor.getLong(6));
        String idTmp = cursor.getString(7);
        if (TextUtils.isEmpty(idTmp)) {
            idTmp = String.valueOf(cursor.getLong(6));
        }
        imageProfile.setIdServerString(idTmp);
        return imageProfile;
    }

    // set values
    private ContentValues setValues(ImageProfile imageProfile) {
        ContentValues values = new ContentValues();
        values.put(ImageProfileConstant.TYPE_IMAGE, imageProfile.getTypeImage());
        values.put(ImageProfileConstant.IMAGE_PATH, imageProfile.getImagePathLocal());
        values.put(ImageProfileConstant.IMAGE_URL, imageProfile.getImageUrl());
        if (imageProfile.isUploaded()) {
            values.put(ImageProfileConstant.IS_UPLOADED, 1);
        } else {
            values.put(ImageProfileConstant.IS_UPLOADED, 0);
        }
        values.put(ImageProfileConstant.TIME, imageProfile.getTime());
//        values.put(ImageProfileConstant.ID_SERVER, imageProfile.getIdServer());
        values.put(ImageProfileConstant.ID_SERVER_STRING, imageProfile.getIdServerString());
        return values;
    }

    // insert item
    public void insertImageProfile(ImageProfile imageProfile) {
        try {
            ContentValues values = setValues(imageProfile);
            long id = databaseWrite.insert(ImageProfileConstant.TABLE_IMAGE_PROFILE, null, values);
            imageProfile.setId(id);
            Log.d(TAG, "insertImageProfile id=" + imageProfile.getId());
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public void insertListImageProfile(ArrayList<ImageProfile> listImages) {
        if (listImages == null || listImages.isEmpty()) {
            return;
        }
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (ImageProfile item : listImages) {
                    values = setValues(item);
                    long id = databaseWrite.insert(ImageProfileConstant.TABLE_IMAGE_PROFILE, null, values);
                    item.setId(id);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public void updateImageProfile(ImageProfile imageProfile) {
        if (imageProfile == null) {
            return;
        }

        try {
            String whereClause = ImageProfileConstant.ID_IMAGE +
                    " = " + imageProfile.getId();
            databaseWrite.update(ImageProfileConstant.TABLE_IMAGE_PROFILE,
                    setValues(imageProfile), whereClause, null);
            Log.d(TAG, "updateImageProfile id=" + imageProfile.getId());
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    /**
     * xoa image trogng db
     *
     * @param imageProfile
     */
    public void deleteImageProfile(ImageProfile imageProfile) {
        if (imageProfile == null) {
            return;
        }
        try {
            String whereClause = ImageProfileConstant.ID_IMAGE + " = " + imageProfile.getId();
            databaseWrite.delete(ImageProfileConstant.TABLE_IMAGE_PROFILE, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(ImageProfileConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }
}
package com.metfone.selfcare.database;

/**
 * Created by thanhnt72 on 1/15/2019.
 */

public class SubscriptionConfig {
    private String title;
    private String confirm;
    private String reconfirm;
    private String cmd;
    private String cmdCancel;
    private String unVip;


    public SubscriptionConfig() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getReconfirm() {
        return reconfirm;
    }

    public void setReconfirm(String reconfirm) {
        this.reconfirm = reconfirm;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getCmdCancel() {
        return cmdCancel;
    }

    public void setCmdCancel(String cmdCancel) {
        this.cmdCancel = cmdCancel;
    }

    public String getUnVip() {
        return unVip;
    }

    public void setUnVip(String unVip) {
        this.unVip = unVip;
    }
}

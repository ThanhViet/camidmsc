package com.metfone.selfcare.database.constant;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 6/30/2014.
 */
public class ReengMessageConstant {
    private static final String TAG = ReengMessageConstant.class.getSimpleName();
    public static final String MESSAGE_TABLE = "message";
    public static final String MESSAGE_ID = "id";
    public static final String MESSAGE_CONTENT = "content";
    public static final String MESSAGE_THREAD_ID = "thread_id";
    public static final String MESSAGE_IS_READ = "is_read";
    public static final String MESSAGE_SENDER = "sender";
    public static final String MESSAGE_RECEIVER = "receiver";
    public static final String MESSAGE_TIME = "time";
    public static final String MESSAGE_IS_SENT = "is_send";
    public static final String MESSAGE_FILE_TYPE = "file_type";
    public static final String MESSAGE_PACKET_ID = "packet_id";
    public static final String MESSAGE_STATUS = "status";
    public static final String MESSAGE_DURATION = "duration";
    public static final String MESSAGE_LOCAL_FILE_PATH = "file_path";       //luu them url voice sticker
    public static final String MESSAGE_SONG_ID = "server_file_id";// truoc la luu file id, -> chuyen luu songId va
    // stickerId
    public static final String MESSAGE_TYPE = "type";
    public static final String MESSAGE_FILE_NAME = "file_name";
    public static final String MESSAGE_CHAT_MODE = "chat_mode";
    public static final String MESSAGE_FILE_SIZE = "file_size";
    public static final String MESSAGE_VIDEO_CONTENT_URI = "delivered_members";     //cai nay bay h luu contenturi
    public static final String MESSAGE_DIRECTION = "MESSAGE_DIRECTION";
    public static final String MESSAGE_MUSIC_STATE = "MESSAGE_MUSIC_STATE";
    public static final String MESSAGE_IMAGE_URL = "IMAGE_URL";
    public static final String MESSAGE_DIRECT_LINK_MEDIA = "DIRECT_LINK_MEDIA";
    public static final String MESSAGE_ROOM_INFO = "ROOM_INFO";
    public static final String MESSAGE_FILE_ID_NEW = "MESSAGE_FILE_ID_NEW";// file id moi luu dang string
    public static final String MESSAGE_REPLY_DETAIL = "MESSAGE_REPLY_DETAIL";// luu json tin nhan reply
    public static final String MESSAGE_EXPIRED = "MESSAGE_EXPIRED";//thoi gian expired để đánh dấu tin nhắn thành đã đọc
    public static final String MESSAGE_TAG_CONTENT = "TAG_CONTENT";
    public static final String MESSAGE_REACTION = "REACTION";
    public static final String MESSAGE_TARGET_ID_E2E = "MESSAGE_TARGET_ID_E2E";
    public static final String MESSAGE_STATE_MY_REACTION = "MY_REACTION";

    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + MESSAGE_TABLE;

    public static final String CREATE_INDEX = "CREATE INDEX thread_idx ON " + MESSAGE_TABLE + "(" + MESSAGE_THREAD_ID
            + ");";

    /**
     * Represents the type of a message.
     */
    public enum MessageType {
        text, file, image, voicemail, notification,
        shareContact, shareVideo, voiceSticker,
        greeting_voicesticker,
        shareLocation, restore,
        watch_video,

        /**
         * goi y cung nghe
         */
        suggestShareMusic,
        /**
         * moi tham gia nghe chung
         */
        inviteShareMusic,
        /**
         * loai message chuyen bai, doi nbai, them bai ...
         */
        actionShareMusic,
        /**
         * chuyen tien
         */
        transferMoney,
        /**
         * event star room
         */
        event_follow_room,
        crbt_gift,
        deep_link,
        gift,
        fake_mo,
        notification_fake_mo,
        image_link,
        advertise,
        poll_create,
        poll_action,
        call,
        talk_stranger,
        luckywheel_help,
        bank_plus,
        lixi,
        message_banner,
        pin_message,
        suggest_voice_sticker,
        update_app,
        enable_e2e;

        public static MessageType fromString(String name) {
            try {
                return MessageType.valueOf(name);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return text;
            }
        }

        public static boolean containsNormal(MessageType type) {// tin nhan thuong
            return type == text || type == shareContact || type == transferMoney ||
                    type == voiceSticker || type == shareLocation || type == gift ||
                    type == image_link || type == bank_plus;
        }

        public static boolean containsFile(MessageType type) {// tin nhan file
            return type == file || type == image ||
                    type == voicemail || type == shareVideo;
        }

        public static boolean containsNotification(MessageType type) {
            return type == notification || type == notification_fake_mo ||
                    type == poll_action || type == pin_message || type == enable_e2e;
        }

        public static int toStringResource(MessageType messageType) {
            int stringResourceId;
            switch (messageType) {
                case text:
                    stringResourceId = R.string.message_text;
                    break;
                case image:
                    stringResourceId = R.string.message_image;
                    break;
                case voicemail:
                    stringResourceId = R.string.message_voice_mail;
                    break;
                case voiceSticker:
                    stringResourceId = R.string.message_voice_sticker;
                    break;
                case shareContact:
                    stringResourceId = R.string.message_share_contact;
                    break;
                case shareVideo:
                    stringResourceId = R.string.message_share_video;
                    break;
                case shareLocation:
                    stringResourceId = R.string.message_share_location;
                    break;
                case transferMoney:
                    stringResourceId = R.string.message_transfer_money;
                    break;
                case event_follow_room:
                    stringResourceId = R.string.message_event_follow_room;
                    break;
                default:
                    stringResourceId = R.string.message_text;
                    break;
            }
            return stringResourceId;
        }
    }

    public enum Direction {
        send, received;

        public static Direction fromString(String name) {
            return Direction.valueOf(name);
        }
    }

    public enum ActionChangeNumber {
        saveContact, createChat, syncChat
    }

    //------------------------------------------------------------------------//
    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + MESSAGE_TABLE
            + "("
            + MESSAGE_ID
            + " INTEGER PRIMARY KEY, "
            + MESSAGE_CONTENT + " TEXT, "
            + MESSAGE_IS_READ + " INTEGER, "
            + MESSAGE_SENDER + " TEXT, "
            + MESSAGE_RECEIVER + " TEXT, "
            + MESSAGE_TIME + " TEXT, "
            + MESSAGE_THREAD_ID + " INTEGER, "
            + MESSAGE_IS_SENT + " INTEGER, "
            + MESSAGE_FILE_TYPE + " TEXT, "
            + MESSAGE_PACKET_ID + " TEXT, "
            + MESSAGE_STATUS + " INTEGER, "
            + MESSAGE_DURATION + " INTEGER, "
            + MESSAGE_LOCAL_FILE_PATH + " TEXT, "
            + MESSAGE_TYPE + " TEXT, "
            + MESSAGE_SONG_ID + " INTEGER, "
            + MESSAGE_CHAT_MODE + " INTEGER, "
            + MESSAGE_FILE_NAME + " TEXT, "
            + MESSAGE_FILE_SIZE + " INTEGER, "
            + MESSAGE_VIDEO_CONTENT_URI + " TEXT, "
            + MESSAGE_DIRECTION + " TEXT, "
            + MESSAGE_MUSIC_STATE + " INTEGER, "
            + MESSAGE_IMAGE_URL + " TEXT, "
            + MESSAGE_DIRECT_LINK_MEDIA + " TEXT, "
            + MESSAGE_ROOM_INFO + " TEXT, "
            + MESSAGE_FILE_ID_NEW + " TEXT, "
            + MESSAGE_REPLY_DETAIL + " TEXT, "
            + MESSAGE_EXPIRED + " INTEGER, "
            + MESSAGE_TAG_CONTENT + " TEXT, "
            + MESSAGE_REACTION + " TEXT, "
            + MESSAGE_TARGET_ID_E2E + " TEXT, "
            + MESSAGE_STATE_MY_REACTION + " TEXT DEFAULT '-1'"
            + ");";

    public static final String ALTER_COLUMN_MESSAGE_IMAGE_URL = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_IMAGE_URL + " TEXT;";
    public static final String ALTER_COLUMN_STATE_MUSIC = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_MUSIC_STATE + " INTEGER;";
    public static final String ALTER_COLUMN_DIRECT_LINK_MEDIA = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_DIRECT_LINK_MEDIA + " TEXT;";
    public static final String ALTER_COLUMN_ROOM_INFO = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_ROOM_INFO + " TEXT;";
    public static final String ALTER_COLUMN_FILE_ID_NEW = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_FILE_ID_NEW + " TEXT;";
    public static final String ALTER_COLUMN_MESSAGE_REPLY_DETAIL = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_REPLY_DETAIL + " TEXT;";
    public static final String ALTER_COLUMN_MESSAGE_EXPIRED = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_EXPIRED + " INTEGER DEFAULT -1;";
    public static final String ALTER_COLUMN_MESSAGE_TAG_CONTENT = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_TAG_CONTENT + " TEXT;";
    public static final String ALTER_COLUMN_MESSAGE_REACTION = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_REACTION + " TEXT;";
    public static final String ALTER_COLUMN_MESSAGE_TARGET_ID_E2E = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_TARGET_ID_E2E + " TEXT;";
    public static final String ALTER_COLUMN_MESSAGE_MY_REACTION = "ALTER TABLE " + MESSAGE_TABLE
            + " ADD COLUMN " + MESSAGE_STATE_MY_REACTION + " TEXT DEFAULT '-1';";


    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + MESSAGE_TABLE;
    public static final String MESSAGE_DELETE_OF_THREAD_QUERY = "DELETE FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_THREAD_ID + " = ";
    public static final String MESSAGE_SELECT_ALL_QUERY = "SELECT * FROM "
            + MESSAGE_TABLE;
    public static final String MESSAGE_SELECT_BY_PACKET_ID_QUERY = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_PACKET_ID + " LIKE ";
    public static final String MESSAGE_DELETE_QUERY = "DELETE FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_ID + " = ";
    public static final String MESSAGE_SELECT_OF_THREAD_QUERY = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_THREAD_ID + " = ";
    public static final String MESSAGE_SELECT_NOT_SENT_QUERY = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_IS_SENT + " = 0";
    public static final String MESSAGE_SELECT_UNREAD_QUERY_BY_THREAD = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_IS_READ + " = " + ReengMessageConstant.READ_STATE_UNREAD + " AND "
            + MESSAGE_THREAD_ID + " = ";
    public static final String MESSAGE_UPDATE_QUERY = "UPDATE " + MESSAGE_TABLE
            + " SET " + MESSAGE_IS_SENT + " = 1" + " WHERE " + MESSAGE_ID
            + " = ";
    public static final String VOICEMAIL_UPDATE_FILEPATH_QUERY = "UPDATE "
            + MESSAGE_TABLE + " SET " + MESSAGE_LOCAL_FILE_PATH + " = %s"
            + " WHERE " + MESSAGE_ID + " = %s";
    public static final String MESSAGE_SELECT_BY_LOCAL_FILE_PATH_QUERY = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_LOCAL_FILE_PATH + " LIKE ";
    public static final String TRUNCATE_MESSAGE_QUERY = "DELETE FROM "
            + MESSAGE_TABLE;
    public static final String IMAGE_MESSAGE_SELECT_BY_THREAD_ID = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_THREAD_ID + " = %s AND " + MESSAGE_LOCAL_FILE_PATH
            + " IS NOT NULL AND " + MESSAGE_TYPE + " like 'image' ORDER BY id ASC";

    public static final String IMAGE_MESSAGE_SELECT_BY_THREAD_ID_SORT_DESC = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_THREAD_ID + " = %s AND " + MESSAGE_LOCAL_FILE_PATH
            + " IS NOT NULL AND " + MESSAGE_TYPE + " like 'image' ORDER BY id DESC";

    public static final String MESSAGE_SELECT_LIMIT_QUERY_STATEMENT = "SELECT * FROM " + MESSAGE_TABLE +
            " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID + " = %1$d ORDER BY %2$s DESC LIMIT %3$d";

    public static final String MESSAGE_SELECT_ALL_IDS_QUERY_STATEMENT = "SELECT " + MESSAGE_ID + " FROM " + MESSAGE_TABLE +
            " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID + " = %1$d ORDER BY %2$s DESC";

    public static final String MESSAGE_SELECT_NEAREST_QUERY_STATEMENT = "SELECT * FROM " + MESSAGE_TABLE +
            " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID + " = %1$d AND (" + ReengMessageConstant.MESSAGE_ID + " BETWEEN %2$d AND %3$d) ORDER BY %4$s DESC";

    public static final String MESSAGE_SEARCH_QUERY_STATEMENT = "SELECT * FROM " + MESSAGE_TABLE +
            " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID + " = %1$d AND " + ReengMessageConstant.MESSAGE_TYPE + " LIKE 'text' AND " + ReengMessageConstant.MESSAGE_CONTENT + " LIKE '%2$s' ORDER BY %3$s ASC";

    public static final String MESSAGE_SELECT_MORE_BY_FIRST_MSG_ID_QUERY_STATEMENT = "SELECT * FROM " + MESSAGE_TABLE +
            " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID + " = %1$d AND " +
            ReengMessageConstant.MESSAGE_ID + " < %2$d ORDER BY %3$s DESC LIMIT %4$d";

    public static final String MESSAGE_SELECT_LIMIT_PACKETID_QUERY_STATEMENT = "SELECT " + MESSAGE_PACKET_ID + " FROM "
            + MESSAGE_TABLE + " WHERE " + ReengMessageConstant.MESSAGE_DIRECTION + " = '" + Direction.received + "'" +
            " ORDER BY " + ReengMessageConstant.MESSAGE_ID + " DESC LIMIT %d";

    public static final String MESSAGE_BACKUP_NEAREST_QUERY_STATEMENT = "SELECT * FROM " + MESSAGE_TABLE +
            " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID + " = %1$d AND " + ReengMessageConstant.MESSAGE_TYPE + " LIKE 'text' AND (" + ReengMessageConstant.MESSAGE_ID + " BETWEEN %2$d AND %3$d) ORDER BY %4$s DESC";

    public static final String MESSAGE_BACKUP_NEAREST_QUERY_PROJECTION_STATEMENT = "SELECT %1$s FROM " + MESSAGE_TABLE +
            " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID + " = %2$d AND " + ReengMessageConstant.MESSAGE_TYPE + " LIKE 'text' AND (" + ReengMessageConstant.MESSAGE_ID + " BETWEEN %3$d AND %4$d) ORDER BY %5$s DESC";

    public static final String MESSAGE_UPDATE_SEEN_STATE_MESSAGE_BY_THREAD_ID = "UPDATE " + MESSAGE_TABLE + " SET " + MESSAGE_IS_READ
            + " = %d WHERE " + MESSAGE_ID + " IN (SELECT " + MESSAGE_ID + " FROM " + MESSAGE_TABLE
            + " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID + " = %d ORDER BY " + MESSAGE_ID + " DESC LIMIT %d)";

    public static final String PIN_MESSAGE_SELECT_BY_THREAD_ID = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_THREAD_ID + " = %s AND "
            + MESSAGE_TYPE + " like 'pin_message' ORDER BY id ASC";

    public static final String QUERY_ALL_UNKNOWN_MESSAGE = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_TYPE + " like '" + MessageType.update_app.toString() + "'";


    public static final String QUERY_ALL_NUMBER_OLD = "SELECT * FROM "
            + MESSAGE_TABLE + " WHERE " + MESSAGE_SENDER + " LIKE %s AND " + MESSAGE_THREAD_ID + " = %d";

    public static final String UPDATE_PREFIX_NUMBER = "UPDATE " + MESSAGE_TABLE
            + " SET " + MESSAGE_SENDER + " = %s "
            + " WHERE " + MESSAGE_THREAD_ID + " = %d AND " + MESSAGE_SENDER + " = %s";


    public static final String MESSAGE_BANNER_LIXI_SELECT_LIMIT_QUERY_STATEMENT = "SELECT * FROM " + MESSAGE_TABLE +
            " WHERE "
            + ReengMessageConstant.MESSAGE_THREAD_ID + " = %1$d AND "
            + ReengMessageConstant.MESSAGE_TYPE + " like \'message_banner\' AND "
            + ReengMessageConstant.MESSAGE_LOCAL_FILE_PATH + " like \'lixi\'" + " ORDER BY %2$s DESC LIMIT %3$d";

    public static final int STATUS_CHANGE_SONG = -1;
    public static final int STATUS_SENT = 1; //da gui
    public static final int STATUS_FAIL = 2; //gui loi, nhan loi
    public static final int STATUS_DELIVERED = 3; //da nhan
    public static final int STATUS_RECEIVED = 4;// file, image
    public static final int STATUS_NOT_LOAD = 5; //chua download
    public static final int STATUS_LOADING = 6;//dang down , dang up, dang gui
    public static final int STATUS_NOT_SEND = 7; //chua gui - luc moi tao tin nhan thi la trang thai nay
    public static final int STATUS_SEEN = 8;
    //
    public static final int MODE_GSM = 2;
    public static final int MODE_IP_IP = 1;
    public static final int MODE_VIDEO_CALL = 3;

    public static final int SEEN_FLAG_SEEING = 0; //dang trong man hinh chat
    public static final int SEEN_FLAG_NOT_SEEING = -1; //dang ko trong man hinh chat
    public static final int SEEN_FLAG_LIST = 1; //bat dau vao trong man hinh chat
    //
    public static final int READ_STATE_UNREAD = 0; //dung cho tin nhan nhan - chua doc
    public static final int READ_STATE_READ = 1;//dung cho tin nhan nhan - da doc
    public static final int READ_STATE_SENT_SEEN = 2;//dung cho tin nhan nhan - da gui ban tin seen
    //
    // luu trang thai cua ban tin cung nghe
    public static final int MUSIC_STATE_NONE = 0;   // state none
    public static final int MUSIC_STATE_ACCEPTED = 1;   // dong y nghe chung
    public static final int MUSIC_STATE_CANCEL = 2;     // hoac tu choi
    public static final int MUSIC_STATE_TIME_OUT = 3;   // time out,
    public static final int MUSIC_STATE_WAITING = 4;    // chua dong y, chua timeout
    public static final int MUSIC_STATE_GROUP = 5;      // cung nghe group
    public static final int MUSIC_STATE_ROOM = 6;      // cung nghe room
    public static final int MUSIC_STATE_REQUEST_CHANGE = 7;// yêu cầu đồi bài trong nhóm

    public static final String BPLUS_PAY = "pay";
    public static final String BPLUS_CLAIM = "claim";


    public static final String MESSAGE_ENCRYPTED = "encrypt";

    public enum FileType {
        doc,
        xls,
        ppt,
        pdf,
        txt;

        public static FileType fromString(String extension) {
            if ("doc".equals(extension) || "docx".equals(extension)) {
                return doc;
            } else if ("xls".equals(extension) || "xlsx".equals(extension)) {
                return xls;
            } else if ("ppt".equals(extension) || "pptx".equals(extension)) {
                return ppt;
            } else if ("pdf".equals(extension)) {
                return pdf;
            }
            return txt;
        }

        public static boolean isSupported(String extension) {
            /*if ("doc".equals(extension) || "docx".equals(extension)
                    || "xls".equals(extension) || "xlsx".equals(extension)
                    || "ppt".equals(extension) || "pptx".equals(extension)
                    || "pdf".equals(extension)
                    || "txt".equals(extension)) {
                return true;
            }
            return false;*/
            return true;
        }
    }

    public enum Reaction {
        LIKE(0), LOVE(1), SMILE(2), SURPRISE(3), SAD(4), HUH(5), DEFAULT(6);
        public int VALUE;

        Reaction(int VALUE) {
            this.VALUE = VALUE;
        }

        public static Reaction fromInteger(int react) {
            switch (react) {
                case 0:
                    return LIKE;
                case 1:
                    return LOVE;
                case 2:
                    return SMILE;
                case 3:
                    return SURPRISE;
                case 4:
                    return SAD;
                case 5:
                    return HUH;
                default:
                    return DEFAULT;
            }
        }

    }


}
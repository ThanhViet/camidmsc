package com.metfone.selfcare.database.model;

import com.metfone.selfcare.helper.Constants.HTTP.STRANGER_STICKY;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by toanvk2 on 4/5/2016.
 */
public class StrangerSticky implements Serializable {
    private String title = "";
    private String imageUrl;
    private int type;
    private String confirm;
    private String action;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setJsonObject(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has(STRANGER_STICKY.STICKY_TITLE)) {
            title = jsonObject.getString(STRANGER_STICKY.STICKY_TITLE);
        }
        if (jsonObject.has(STRANGER_STICKY.STICKY_IMAGE_URL)) {
            imageUrl = jsonObject.getString(STRANGER_STICKY.STICKY_IMAGE_URL);
        }
        if (jsonObject.has(STRANGER_STICKY.STICKY_TYPE)) {
            type = jsonObject.getInt(STRANGER_STICKY.STICKY_TYPE);
        }
        if (jsonObject.has(STRANGER_STICKY.STICKY_CONFIRM)) {
            confirm = jsonObject.getString(STRANGER_STICKY.STICKY_CONFIRM);
        }
        if (jsonObject.has(STRANGER_STICKY.STICKY_ACTION)) {
            action = jsonObject.getString(STRANGER_STICKY.STICKY_ACTION);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("StrangerSticky");
        sb.append("title: ").append(title);
        sb.append(" imageUrl: ").append(imageUrl);
        sb.append(" type: ").append(type);
        sb.append(" confirm: ").append(confirm);
        sb.append(" action: ").append(action);
        return sb.toString();
    }
}

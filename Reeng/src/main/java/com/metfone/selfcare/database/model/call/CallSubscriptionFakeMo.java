package com.metfone.selfcare.database.model.call;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 10/5/2017.
 */

public class CallSubscriptionFakeMo implements Serializable {
    @SerializedName("description")
    private String desc;
    @SerializedName("label")
    private String label;
    @SerializedName("cmd")
    private String cmd;
    @SerializedName("confirm")
    private String confirm;

    public CallSubscriptionFakeMo() {

    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }
}

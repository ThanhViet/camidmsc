package com.metfone.selfcare.database.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class UserIdentityInfo implements Serializable {

    @SerializedName("addedDate")
    private String addedDate;

    @SerializedName("addedUser")
    private String addedUser = "";

    @SerializedName("address")
    private String address = "";

    @SerializedName("birthDate")
    private String birthDate = "";

    @SerializedName("correctCus")
    private String correctCus = "";

    @SerializedName("busType")
    private String busType;

    @SerializedName("custId")
    private String custId = "";

    @SerializedName("district")
    private String district;

    @SerializedName("expireVisa")
    private String expireVisa = "";

    @SerializedName("home")
    private String home = "";

    @SerializedName("idExpireDate")
    private String idExpireDate;

    @SerializedName("idIssueDate")
    private String idIssueDate;

    @SerializedName("idNo")
    private String idNo;

    @SerializedName("idType")
    private String idType = "0";

    @SerializedName("isUpdate")
    private String isUpdate;

    @SerializedName("name")
    private String name;

    @SerializedName("nationality")
    private String nationality;

    @SerializedName("notes")
    private String notes;

    @SerializedName("precinct")
    private String precinct;

    @SerializedName("province")
    private String province;

    @SerializedName("relationship")
    private String relationship;

    @SerializedName("sex")
    private String sex;

    @SerializedName("status")
    private String status;

    @SerializedName("streetName")
    private String streetName;

    @SerializedName("subDateBirth")
    private String subDateBirth;

    @SerializedName("subGender")
    private String subGender;

    @SerializedName("telFax")
    private String telFax;

    @SerializedName("subName")
    private String subName;

    @SerializedName("vip")
    private String vip;

    @SerializedName("email")
    private String email;

    @SerializedName("isScan")
    private String isScan;

    @Expose
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIsScan() {
        return isScan;
    }

    public void setIsScan(String isScan) {
        this.isScan = isScan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserIdentityInfo() {
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getAddedUser() {
        return addedUser;
    }

    public void setAddedUser(String addedUser) {
        this.addedUser = addedUser;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getCorrectCus() {
        return correctCus;
    }

    public void setCorrectCus(String correctCus) {
        this.correctCus = correctCus;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getExpireVisa() {
        return expireVisa;
    }

    public void setExpireVisa(String expireVisa) {
        this.expireVisa = expireVisa;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getIdExpireDate() {
        return idExpireDate;
    }

    public void setIdExpireDate(String idExpireDate) {
        this.idExpireDate = idExpireDate;
    }

    public String getIdIssueDate() {
        return idIssueDate;
    }

    public void setIdIssueDate(String idIssueDate) {
        this.idIssueDate = idIssueDate;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(String isUpdate) {
        this.isUpdate = isUpdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getSubDateBirth() {
        return subDateBirth;
    }

    public void setSubDateBirth(String subDateBirth) {
        this.subDateBirth = subDateBirth;
    }

    public String getSubGender() {
        return subGender;
    }

    public void setSubGender(String subGender) {
        this.subGender = subGender;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public UserIdentityInfo copy() {
        UserIdentityInfo clone = new UserIdentityInfo();
        clone.addedDate = addedDate;
        clone.addedUser = addedUser;
        clone.address = address;
        clone.birthDate = birthDate;
        clone.correctCus = correctCus;
        clone.busType = busType;
        clone.custId = custId;
        clone.district = district;
        clone.expireVisa = expireVisa;
        clone.home = home;
        clone.idExpireDate = idExpireDate;
        clone.idIssueDate = idIssueDate;
        clone.idNo = idNo;
        clone.idType = idType;
        clone.isUpdate = isUpdate;
        clone.name = name;
        clone.nationality = nationality;
        clone.notes = notes;
        clone.precinct = precinct;
        clone.province = province;
        clone.relationship = relationship;
        clone.sex = sex;
        clone.status = status;
        clone.streetName = streetName;
        clone.subDateBirth = subDateBirth;
        clone.subGender = subGender;
        clone.telFax = telFax;
        clone.subName = subName;
        clone.vip = vip;
        clone.email = email;
        return clone;
    }

    @Override
    public String toString() {
        return "UserIdentityInfo{" +
                "district='" + district + '\'' +
                ", province='" + province + '\'' +
                ", streetName='" + streetName + '\'' +
                ", subDateBirth='" + subDateBirth + '\'' +
                ", subGender='" + subGender + '\'' +
                ", subName='" + subName + '\'' +
                '}';
    }

    public String showInfo() {
        return "UserIdentityInfo{" +
                "contact ='" + telFax + '\'' +
                ", nationality='" + nationality + '\'' +
                ", gender='" + sex + '\'' +
                '}';
    }

}

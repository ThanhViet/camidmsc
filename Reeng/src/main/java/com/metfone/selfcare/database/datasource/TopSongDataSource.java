package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.MediaConstant;
import com.metfone.selfcare.database.constant.TopSongConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 6/26/14.
 */
public class TopSongDataSource {
    // Database fields
    private static String TAG = TopSongDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static TopSongDataSource mInstance;

    public static synchronized TopSongDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new TopSongDataSource(application);
        }
        return mInstance;
    }

    private TopSongDataSource(ApplicationController application) {
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    /**
     * close cursor
     *
     * @param cursor
     */
    private void closeCursor(Cursor cursor) {
        if (cursor != null)
            cursor.close();
    }

    /**
     * set ContentValues from mediaModel
     *
     * @param item
     * @return contentValues
     */
    private ContentValues setValues(MediaModel item) {
        ContentValues values = new ContentValues();
        values.put(TopSongConstant.ID, item.getId());
        values.put(TopSongConstant.NAME, item.getName());
        values.put(TopSongConstant.SINGER, item.getSinger());
        values.put(TopSongConstant.MEDIA_URL, item.getMedia_url());
        values.put(TopSongConstant.URL, item.getUrl());
        values.put(TopSongConstant.IMAGE, item.getImage());
        values.put(TopSongConstant.TYPE, item.getType());
        values.put(MediaConstant.CRBT_CODE, item.getCrbtCode());
        values.put(MediaConstant.CRBT_PRICE, item.getCrbtPrice());
        return values;
    }

    /**
     * insert list top song to DB
     *
     * @param listTopSong
     */
    public void insertListTopSong(ArrayList<MediaModel> listTopSong) {
        if (listTopSong == null || listTopSong.isEmpty())
            return;
        try {
            databaseWrite.beginTransaction();
            try {
                for (MediaModel allModel : listTopSong) {
                    ContentValues values = setValues(allModel);
                    databaseWrite.insert(TopSongConstant.TABLE, null, values);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * get TopSong from cursor
     *
     * @param cur
     * @return mediaModel
     */
    private MediaModel getTopSongFromCursor(Cursor cur) {
        MediaModel item = new MediaModel();
        item.setId(cur.getString(0));
        item.setName(cur.getString(1));
        item.setSinger(cur.getString(2));
        item.setImage(cur.getString(3));
        item.setMedia_url(cur.getString(4));
        item.setUrl(cur.getString(5));
        item.setType(cur.getInt(6));
        item.setCrbtCode(cur.getString(7));
        item.setCrbtPrice(cur.getString(8));
        return item;
    }

    /**
     * get top song from songId
     *
     * @param id
     * @return mediaModel
     */
    public MediaModel getTopSongFromId(String id) {
        MediaModel allModel = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null)
                return null;
            String query = TopSongConstant.SELECT_BY_ALLMODEL_ID_STATEMENT
                    + id;
            cursor = databaseRead.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    allModel = getTopSongFromCursor(cursor);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return allModel;
    }

    /**
     * get all topSong from DB
     *
     * @return list MediaModel
     */
    public ArrayList<MediaModel> getListTopSong() {
        ArrayList<MediaModel> listAllmodel = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null)
                return listAllmodel;
            cursor = databaseRead.rawQuery(TopSongConstant.SELECT_ALL_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                listAllmodel = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        MediaModel allModel = getTopSongFromCursor(cursor);
                        // set phone number
                        listAllmodel.add(allModel);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return listAllmodel;
    }

    /**
     * delete top song by song id
     *
     * @param id
     */
    public void deleteTopSongFromID(String id) {
        try {
            databaseWrite.delete(TopSongConstant.TABLE, TopSongConstant.WHERE_ID, new String[]{id});
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * delete list topsong
     *
     * @param listTopSong
     */
    public void deleteListTopSong(ArrayList<MediaModel> listTopSong) {
        if (listTopSong == null || listTopSong.size() <= 0)
            return;
        try {
            if (databaseWrite == null)
                return;
            databaseWrite.beginTransaction();
            try {
                for (MediaModel allModel : listTopSong) {
                    databaseWrite.delete(TopSongConstant.TABLE,
                            TopSongConstant.WHERE_ID, new String[]{allModel.getId()});
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * delete all topsong from db
     */
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(TopSongConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * drop table top song
     */
    public void dropTable() {
        try {
            databaseWrite.execSQL(TopSongConstant.DROP_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }
}
package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/21/2017.
 */
public class TemplateDetail implements Serializable {
    @SerializedName("song")
    private Song song;
    @SerializedName("templateCollection")
    private ArrayList<Page> pages;

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public ArrayList<Page> getPages() {
        return pages;
    }

    public void setPages(ArrayList<Page> pages) {
        this.pages = pages;
    }
}

package com.metfone.selfcare.database.model.call;

/**
 * Created by toanvk2 on 1/10/2018.
 */

public class CallQuality {
    private long bytesReceivedVideo;
    private long bytesReceivedAudio;
    private long mPrevBytesAudioSent, mPrevBytesVideoSent;
    private int currentMode;

    public CallQuality() {
    }

    public CallQuality(long bytesReceivedAudio, long bytesReceivedVideo, int currentMode) {
        this.bytesReceivedAudio = bytesReceivedAudio;
        this.bytesReceivedVideo = bytesReceivedVideo;
        this.currentMode = currentMode;
    }

    public CallQuality(long bytesReceivedAudio, long bytesReceivedVideo, long mPrevBytesAudioSent, long mPrevBytesVideoSent, int currentMode) {
        this.bytesReceivedAudio = bytesReceivedAudio;
        this.bytesReceivedVideo = bytesReceivedVideo;
        this.currentMode = currentMode;
        this.mPrevBytesAudioSent = mPrevBytesAudioSent;
        this.mPrevBytesVideoSent = mPrevBytesVideoSent;
    }

    public String toReport() {
        return mPrevBytesAudioSent + ":" + mPrevBytesVideoSent + ":" + bytesReceivedAudio + ":" + bytesReceivedVideo + ":" + currentMode;
    }

    public String toString() {
        return "bytesReceivedAudio: " + bytesReceivedAudio + " bytesReceivedVideo: " + bytesReceivedVideo;
    }
}

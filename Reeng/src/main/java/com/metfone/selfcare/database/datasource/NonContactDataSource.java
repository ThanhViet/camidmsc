package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.NonContactConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thaodv on 5/8/2015.
 */
public class NonContactDataSource {
    private static String TAG = NonContactDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static NonContactDataSource mNonContactDataSource;

    private NonContactDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    public static synchronized NonContactDataSource getInstance(ApplicationController application) {
        if (mNonContactDataSource == null) {
            mNonContactDataSource = new NonContactDataSource(application);
        }
        return mNonContactDataSource;
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    public List<NonContact> getAllNonContact() {
        List<NonContact> nonContacts = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return nonContacts;
            }
            cursor = databaseRead.rawQuery(NonContactConstant.SELECT_ALL_QUERY, null);
            if (cursor != null && cursor.getCount() > 0) {
                nonContacts = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        NonContact nonContact = getNonContactFromCursor(cursor);
                        // set phone number
                        nonContacts.add(nonContact);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return nonContacts;
    }

    public NonContact getNonContactFromNumber(String number) {
        NonContact nonContact = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(NonContactConstant.SELECT_NON_CONTACT_STATEMENT, new String[]{number});
            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                nonContact = getNonContactFromCursor(cursor);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return nonContact;
    }

    public NonContact getNonContactFromCursor(Cursor cursor) {
        NonContact nonContact = new NonContact();
        nonContact.setId(cursor.getLong(0));
        nonContact.setJidNumber(cursor.getString(1));
        nonContact.setState(cursor.getInt(2));
        nonContact.setStatus(cursor.getString(3));
        nonContact.setLAvatar(cursor.getString(4));
        nonContact.setGender(cursor.getInt(5));
        nonContact.setLastOnline(-1);
        nonContact.setLastSeen(-1);
        /*String lastSeen = cursor.getString(8);
        if (lastSeen == null || lastSeen.length() <= 0) {
            nonContact.setLastSeen(-1);
        } else {
            nonContact.setLastSeen(Long.parseLong(lastSeen));
        }*/
        nonContact.setBirthDayString(cursor.getString(9));
        //        nonContact.setCoverUrl(cursor.getString(10));
        nonContact.setNickName(cursor.getString(11));
        nonContact.setPermission(cursor.getInt(12));
        nonContact.setOperator(cursor.getString(13));
        nonContact.setPreKey(cursor.getString(14));
        return nonContact;
    }

    // set values
    private ContentValues setValues(NonContact nonContact) {
        ContentValues values = new ContentValues();
        values.put(NonContactConstant.NON_CONTACT_NUMBER, nonContact.getJidNumber());
        values.put(NonContactConstant.NON_CONTACT_STATE, nonContact.getState());
        values.put(NonContactConstant.NON_CONTACT_STATUS, nonContact.getStatus());
        values.put(NonContactConstant.NON_CONTACT_LAST_AVATAR, nonContact.getLAvatar());
        values.put(NonContactConstant.NON_CONTACT_GENDER, nonContact.getGender());
//        values.put(NonContactConstant.NON_CONTACT_BIRTHDAY, nonContact.getBirthDay());
        values.put(NonContactConstant.NON_CONTACT_LAST_ONLINE, String.valueOf(nonContact.getLastOnline()));
        // values.put(NonContactConstant.NON_CONTACT_LAST_SEEN, String.valueOf(nonContact.getLastSeen()));
        values.put(NonContactConstant.NON_CONTACT_BIRTHDAY_STRING, nonContact.getBirthDayString());
//        values.put(NonContactConstant.NON_CONTACT_COVER, nonContact.getCoverUrl());
        values.put(NonContactConstant.NON_CONTACT_NICK_NAME, nonContact.getNickName());
        values.put(NonContactConstant.NON_CONTACT_PERMISSION, nonContact.getPermission());
        values.put(NonContactConstant.NON_CONTACT_OPERATOR, nonContact.getOperator());
        values.put(NonContactConstant.NON_CONTACT_PREKEY, nonContact.getPreKey());
        return values;
    }

    // insert item
    public void insertNonContact(NonContact nonContact) {
        try {
            ContentValues values = setValues(nonContact);
            long id = databaseWrite.insert(NonContactConstant.NON_CONTACT_TABLE, null, values);
            nonContact.setId(id);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void updateNonContact(NonContact nonContact) {
        if (nonContact == null) {
            return;
        }
        try {
            String whereClause = NonContactConstant.NON_CONTACT_ID +
                    " = " + nonContact.getId();
            databaseWrite.update(NonContactConstant.NON_CONTACT_TABLE,
                    setValues(nonContact), whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void deleteNonContact(NonContact nonContact) {
        if (nonContact == null) {
            return;
        }
        try {
            String whereClause = NonContactConstant.NON_CONTACT_ID +
                    " = " + nonContact.getId();
            databaseWrite.delete(NonContactConstant.NON_CONTACT_TABLE, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    // update list item
    public void insertListNonContact(ArrayList<NonContact> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (NonContact nonContact : list) {
                    long id = databaseWrite.insert(NonContactConstant.NON_CONTACT_TABLE, null, setValues(nonContact));
                    nonContact.setId(id);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    // update list item
    public void updateListNonContact(ArrayList<NonContact> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                String whereClause;
                for (NonContact nonContact : list) {
                    whereClause = NonContactConstant.NON_CONTACT_ID +
                            " = " + nonContact.getId();
                    databaseWrite.update(NonContactConstant.NON_CONTACT_TABLE, setValues(nonContact), whereClause,
                            null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(NonContactConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }
}
package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 11/26/14.
 */
public class OfficerDataSource {
    private static String TAG = OfficerDataSource.class.getSimpleName();
    private static OfficerDataSource officerDataSource;
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;

    private OfficerDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    public static synchronized OfficerDataSource getInstance(ApplicationController application) {
        if (officerDataSource == null) {
            officerDataSource = new OfficerDataSource(application);
        }
        return officerDataSource;
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    private OfficerAccount getOfficalAccontFromCursor(Cursor cursor) {
        // create new thread
        OfficerAccount officer = new OfficerAccount();
        try {
            // set attribute
            officer.setId(Integer.parseInt(cursor.getString(0)));
            officer.setServerId(cursor.getString(1));
            officer.setName(cursor.getString(2));
            officer.setAvatarUrl(cursor.getString(3));
            officer.setState(Integer.parseInt(cursor.getString(4)));
            officer.setType(cursor.getInt(5));
            officer.setRoomState(cursor.getInt(6));
            officer.setServiceAction(cursor.getString(7));
        } catch (Exception e) {
            Log.e(TAG, "getOfficalAccontFromCursor", e);
        }
        // return
        return officer;
    }

    public ArrayList<OfficerAccount> getAllOfficalAccount() {
        Cursor cursor = null;
        ArrayList<OfficerAccount> list = new ArrayList<>();
        try {
            // Select all
            cursor = databaseRead.rawQuery(OfficerAccountConstant.OFFICAL_SELECT_ALL_QUERY, null);
            // looping through all rows and adding to list
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        OfficerAccount offical = getOfficalAccontFromCursor(cursor);
                        list.add(offical);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public OfficerAccount createAccount(String serverId, String name, String avatarUrl,
                                        int state, int type, int roomState) {
        OfficerAccount officer = new OfficerAccount();
        officer.setServerId(serverId);
        officer.setName(name);
        officer.setAvatarUrl(avatarUrl);
        officer.setState(state);
        officer.setType(type);
        officer.setRoomState(roomState);
        long id = this.createRow(officer);
        officer.setId((int) id);
        return officer;
    }

    public long createRow(OfficerAccount officer) {
        try {
            ContentValues values = setContentValues(officer);
            return databaseWrite.insert(OfficerAccountConstant.OFFICER_TABLE, null, values);
        } catch (Exception e) {
            Log.e(TAG, "createThread", e);
            return 0;
        }
    }

    private ContentValues setContentValues(OfficerAccount officer) {
        ContentValues values = new ContentValues();
        values.put(OfficerAccountConstant.SERVER_ID, officer.getServerId());
        values.put(OfficerAccountConstant.NAME, officer.getName());
        values.put(OfficerAccountConstant.AVATAR_URL, officer.getAvatarUrl());
        values.put(OfficerAccountConstant.OFFICER_STATE, officer.getState());
        values.put(OfficerAccountConstant.OFFICER_TYPE, officer.getType());
        values.put(OfficerAccountConstant.ROOM_STATE, officer.getRoomState());
        values.put(OfficerAccountConstant.SERVICE_ACTION, officer.getServiceAction());
        return values;
    }


    public void updateOfficalAccount(OfficerAccount officer) {
        String whereId = OfficerAccountConstant.ID + " = " + officer.getId();
        try {
            ContentValues values = setContentValues(officer);
            databaseWrite.update(OfficerAccountConstant.OFFICER_TABLE, values, whereId, null);
        } catch (Exception e) {
            Log.e(TAG, "updateOfficalAccount", e);
        }
    }

    public void updateListOfficerAccount(ArrayList<OfficerAccount> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (OfficerAccount account : list) {
                    values = setContentValues(account);
                    String whereId = OfficerAccountConstant.ID + " = " + account.getId();
                    databaseWrite.update(OfficerAccountConstant.OFFICER_TABLE, values, whereId, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction updateListOfficerAccount", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "updateListOfficerAccount", e);
        }
    }

    public void insertListOfficerAccount(ArrayList<OfficerAccount> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (OfficerAccount account : list) {
                    values = setContentValues(account);
                    long id = databaseWrite.insert(OfficerAccountConstant.OFFICER_TABLE, null, values);
                    account.setId((int) id);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction updateListOfficerAccount", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "updateListOfficerAccount", e);
        }
    }

    public void deleteOfficerAccount(int id) {
        String whereClause = OfficerAccountConstant.ID + " = " + id;
        databaseWrite.delete(OfficerAccountConstant.OFFICER_TABLE, whereClause, null);
    }

    public OfficerAccount findOfficerByChatServerId(String serverId) {
        OfficerAccount officer = null;
        Cursor cursor = null;
        try {
            String query = OfficerAccountConstant.OFFICAL_SELECT_BY_SERVER_ID_QUERY
                    + "'%" + serverId + "%'";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    officer = getOfficalAccontFromCursor(cursor);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "findMessageInDBByPacketId", e);
        } finally {
            closeCursor(cursor);
        }
        return officer;
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(OfficerAccountConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }
}
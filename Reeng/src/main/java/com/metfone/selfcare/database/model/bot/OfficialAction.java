package com.metfone.selfcare.database.model.bot;

import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 1/17/2017.
 */
public class OfficialAction {
    private String officialId;
    private ArrayList<Action> defaultActions;
    private ArrayList<Action> currentActions;
    private ArrayList<String> keywords;
    private boolean isOptionRequesting = false;

    public OfficialAction() {

    }

    public OfficialAction(String officialId, ArrayList<String> keywords, ArrayList<Action> defaultActions) {
        this.officialId = officialId;
        this.keywords = keywords;
        this.defaultActions = defaultActions;
    }

    public String getOfficialId() {
        return officialId;
    }

    public void setOfficialId(String officialId) {
        this.officialId = officialId;
    }

    public ArrayList<Action> getDefaultActions() {
        return defaultActions;
    }

    public void setDefaultActions(ArrayList<Action> defaultActions) {
        this.defaultActions = defaultActions;
    }

    public ArrayList<Action> getCurrentActions() {
        return currentActions;
    }

    public void setCurrentActions(ArrayList<Action> currentActions) {
        this.currentActions = currentActions;
    }

    public ArrayList<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(ArrayList<String> keywords) {
        this.keywords = keywords;
    }

    public boolean isOptionRequesting() {
        return isOptionRequesting;
    }

    public void setOptionRequesting(boolean optionRequesting) {
        isOptionRequesting = optionRequesting;
        if (defaultActions != null) {
            for (Action dAction : defaultActions) {
                dAction.setSelected(false);
            }
        }
        if (currentActions != null) {
            for (Action cAction : currentActions) {
                cAction.setSelected(false);
            }
        }
    }

    public boolean isShowCurrentAction() {
        return !(currentActions == null || currentActions.isEmpty());
    }

    public boolean isExistKeyword(String input) {
        if (TextUtils.isEmpty(input)) {
            return false;
        } else if (keywords == null || keywords.isEmpty()) {
            return false;
        } else {
            for (String key : keywords) {
                if (input.equals(key)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void onBackPress() {
        currentActions = new ArrayList<>(); //reset
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("officialId: ").append(officialId);
        sb.append(" keywords: ");
        if (keywords != null) {
            for (String key : keywords) {
                sb.append(key).append("-");
            }
        }
        sb.append(" defaultActions: ");
        if (defaultActions != null) {
            for (Action action : defaultActions) {
                sb.append(action.toString()).append("-");
            }
        }
        sb.append(" currentActions: ");
        if (currentActions != null) {
            for (Action action : currentActions) {
                sb.append(action.toString()).append("-");
            }
        }
        return sb.toString();
    }
}

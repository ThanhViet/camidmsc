package com.metfone.selfcare.database.model;

import java.io.Serializable;

public class LogKQI implements Serializable {
    private static final String TAG = LogKQI.class.getSimpleName();
    private String type;
    private String content;
    private String info;
    private String comment;

    public LogKQI(String type, String content, String info, String comment) {
        this.type = type;
        this.content = content;
        this.info = info;
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return type + "|" + content + "|" + comment + "|" + info;
    }
}

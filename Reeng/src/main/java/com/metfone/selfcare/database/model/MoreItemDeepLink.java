package com.metfone.selfcare.database.model;

import java.io.Serializable;

/**
 * Created by toanvk2 on 12/20/2017.
 */

public class MoreItemDeepLink implements Serializable {
    private String icon;
    private String title;
    private String deepLink;
    private boolean isNew;

    public MoreItemDeepLink() {

    }

    public MoreItemDeepLink(String icon, String title, String deepLink, boolean isNew) {
        this.icon = icon;
        this.title = title;
        this.deepLink = deepLink;
        this.isNew = isNew;
    }

    public String getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public boolean isNew() {
        return isNew;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("MoreItemDeepLink");
        sb.append(" icon: ").append(icon);
        sb.append(" title: ").append(title);
        sb.append(" deepLink: ").append(deepLink);
        sb.append(" isNew: ").append(isNew);
        return sb.toString();
    }
}

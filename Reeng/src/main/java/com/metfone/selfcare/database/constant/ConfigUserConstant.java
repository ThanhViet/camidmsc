package com.metfone.selfcare.database.constant;

/**
 * Created by toanvk2 on 6/26/14.
 */
public class ConfigUserConstant {
    public static final String TABLE = "config_user";
    public static final String ID = "id";
    public static final String KEY = "key";
    public static final String VALUE = "value";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE +
            " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY + " TEXT, " + VALUE + " TEXT)";

    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;

    public static final String SELECT_BY_KEY_QUERY = "SELECT * FROM "
            + TABLE + " WHERE " + KEY + " = ";
}
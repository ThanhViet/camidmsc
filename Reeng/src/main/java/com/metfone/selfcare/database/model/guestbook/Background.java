package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 4/13/2017.
 */
//http://stackoverflow.com/questions/3323074/android-difference-between-parcelable-and-serializable TODO rảnh sẽ sửa
public class Background implements Serializable {
    /*@SerializedName("id")
    private int id;*/
    @SerializedName("path")
    private String path;
    @SerializedName("width")
    private float width;
    @SerializedName("height")
    private float height;

    public Background() {
    }

    public Background(Background other) {
        if (other != null) {
           // this.id = other.getId();
            this.path = other.getPath();
            this.width = other.getWidth();
            this.height = other.getHeight();
        }
    }

    /*public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}

package com.metfone.selfcare.database.model.avno;

/**
 * Created by thanhnt72 on 11/3/2018.
 */

public class FakeMOItem {

    private String label;
    private String desc;
    private String cmd;
    private String content;
    private ConfirmFakeMO confirmFakeMO;
    private int stateButton;

    public FakeMOItem(String label, String desc, String cmd, String content, int stateButton) {
        this.label = label;
        this.desc = desc;
        this.cmd = cmd;
        this.content = content;
        this.stateButton = stateButton;
    }

    public void setConfirmFakeMO(ConfirmFakeMO confirmFakeMO) {
        this.confirmFakeMO = confirmFakeMO;
    }

    public String getLabel() {
        return label;
    }

    public String getDesc() {
        return desc;
    }

    public String getCmd() {
        return cmd;
    }

    public String getContent() {
        return content;
    }

    public int getStateButton() {
        return stateButton;
    }

    public ConfirmFakeMO getConfirmFakeMO() {
        return confirmFakeMO;
    }

}

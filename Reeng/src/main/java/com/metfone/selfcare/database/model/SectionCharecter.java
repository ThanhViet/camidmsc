package com.metfone.selfcare.database.model;

/**
 * Created by toanvk2 on 1/7/15.
 */
public class SectionCharecter {
    private static final String TAG = SectionCharecter.class.getSimpleName();
    private String charAt = "#";
    private int position = 0;

    public String getCharAt() {
        return charAt;
    }

    public void setCharAt(String charAt) {
        this.charAt = charAt;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
package com.metfone.selfcare.database.constant;

public interface ChannelDataConstants {

    String ID = "id";
    String TABLE = "Channel";
    String TIME_NEW = "timeNew";
    String CONTENT = "content";
    String CHANNEL_ID = "channelId";

    String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE + " (" + ID + " INTEGER PRIMARY KEY, " + CHANNEL_ID + " TEXT, " + CONTENT + " TEXT, " + TIME_NEW + " TEXT)";
    String SELECT_STATEMENT_BY_CHANNEL_ID = "SELECT * FROM " + TABLE + " WHERE " + CHANNEL_ID + " = ?";

}

package com.metfone.selfcare.database.model.message;

import com.stringee.StringeeIceServer;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.message.PacketMessageId;

import org.jivesoftware.smack.model.IceServer;
import org.jivesoftware.smack.packet.ReengCallOutPacket;
import org.jivesoftware.smack.packet.ReengCallPacket;
import org.jivesoftware.smack.packet.ReengMessagePacket;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by toanvk2 on 3/3/2017.
 */
public class MochaCallOutMessage extends ReengMessage {
    private String caller;
    private String callee;
    private ReengCallOutPacket.CallStatus callStatus;
    private ReengCallOutPacket.CallOutType callOutType;
    private String callData;
    private ArrayList<IceServer> iceServers;
    private String callSession;
    private boolean isCallIn = false;

    private long timeConnect;
    private boolean isRestartICESuccess;
    private ReengCallPacket.RestartReason restartReason;
    private boolean isEnableCallViaFS = false;

    public MochaCallOutMessage() {
        super();
    }

    public MochaCallOutMessage(String from, String to, ThreadMessage thread, boolean isCallIn) {
        super();
        initMessage(from, to, thread, isCallIn);
    }

    public MochaCallOutMessage(String from, String to, ThreadMessage thread) {
        super();
        initMessage(from, to, thread, false);
    }

    private void initMessage(String from, String to, ThreadMessage thread, boolean isCallIn) {
        this.isCallIn = isCallIn;
        String subtypeStr = isCallIn ? ReengMessagePacket.SubType.call_in.toString() : ReengMessagePacket.SubType.call_out.toString();
        setPacketId(PacketMessageId.getInstance().genPacketId(thread.getThreadType(), subtypeStr));
        setThreadId(thread.getId());
        setSender(from);
        setReceiver(to);
        Date date = new Date();
        setTime(date.getTime());
        setMessageType(ReengMessageConstant.MessageType.call);
    }

    public boolean isCallIn() {
        return isCallIn;
    }

    public void setCallIn(boolean callIn) {
        isCallIn = callIn;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getCallee() {
        return callee;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }

    public boolean isEnableCallViaFS() {
        return isEnableCallViaFS;
    }

    public void setEnableCallViaFS(boolean enableCallViaFS) {
        isEnableCallViaFS = enableCallViaFS;
    }

    public ReengCallOutPacket.CallOutType getCallOutType() {
        return callOutType;
    }

    public void setCallOutType(ReengCallOutPacket.CallOutType callOutType) {
        this.callOutType = callOutType;
    }

    public ReengCallOutPacket.CallStatus getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(ReengCallOutPacket.CallStatus callStatus) {
        this.callStatus = callStatus;
    }

    public String getCallData() {
        return callData;
    }

    public void setCallData(String callData) {
        this.callData = callData;
    }


    public void setStringeeIceServers(LinkedList<StringeeIceServer> stringeeIceServers) {
        iceServers = new ArrayList<>();
        if (stringeeIceServers != null) {
            for (StringeeIceServer stringeeServer : stringeeIceServers) {
                iceServers.add(new IceServer(stringeeServer.username, stringeeServer.password, stringeeServer.uri));
            }
        }
    }

    public void setIceServers(ArrayList<IceServer> iceServers) {
        this.iceServers = iceServers;
    }

    public ArrayList<IceServer> getIceServers() {
        return iceServers;
    }

    public String getCallSession() {
        return callSession;
    }

    public void setCallSession(String callSession) {
        this.callSession = callSession;
    }

    public long getTimeConnect() {
        return timeConnect;
    }

    public void setTimeConnect(long timeConnect) {
        this.timeConnect = timeConnect;
    }

    public boolean isRestartICESuccess() {
        return isRestartICESuccess;
    }

    public void setRestartICESuccess(boolean restartICESuccess) {
        isRestartICESuccess = restartICESuccess;
    }

    public ReengCallPacket.RestartReason getRestartReason() {
        return restartReason;
    }

    public void setRestartReason(ReengCallPacket.RestartReason restartReason) {
        this.restartReason = restartReason;
    }
}

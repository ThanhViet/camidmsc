package com.metfone.selfcare.database.constant;

/**
 * Created by thaodv on 5/8/2015.
 */
public class StrangerConstant {
    public static final String STRANGER_TABLE = "STRANGER_TABLE";
    public static final String STRANGER_ID = "STRANGER_TABLE";// nham ten cam ai sua
    public static final String STRANGER_JID_NUMBER = "STRANGER_PHONE_NUMBER";
    public static final String STRANGER_MY_NAME = "STRANGER_PHONE_MY_NAME";
    public static final String STRANGER_FRIEND_NAME = "STRANGER_PHONE_FRIEND_NAME";
    public static final String STRANGER_APP_ID = "STRANGER_APP_ID";
    public static final String STRANGER_FRIEND_AVATAR = "STRANGER_UNKNOWN_COLUMN_1";
    public static final String STRANGER_STATE = "STRANGER_UNKNOWN_COLUMN_2";
    public static final String STRANGER_CREATE_DATE = "STRANGER_CREATE_DATE";

    public static final String CREATE_STRANGER_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + STRANGER_TABLE
            + " ("
            + STRANGER_ID + " INTEGER PRIMARY KEY, "
            + STRANGER_JID_NUMBER + " TEXT, "
            + STRANGER_MY_NAME + " TEXT, "
            + STRANGER_FRIEND_NAME + " TEXT, "
            + STRANGER_APP_ID + " TEXT, "
            + STRANGER_FRIEND_AVATAR + " TEXT, "
            + STRANGER_STATE + " TEXT, "
            + STRANGER_CREATE_DATE + " INTEGER)";

    public static final String SELECT_ALL_STRANGER_QUERY = "SELECT * FROM "
            + STRANGER_TABLE;

    public static final String ALTER_COLUMN_CREATE_DATE = "ALTER TABLE " + STRANGER_TABLE
            + " ADD COLUMN " + STRANGER_CREATE_DATE + " INTEGER DEFAULT 0;";

    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + STRANGER_TABLE;
    // trang thai danh dau chua dong y lam quen.
}
package com.metfone.selfcare.database.model.guestbook;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class Book implements Serializable {
    @SerializedName("id_memory")
    private String id;
    @SerializedName("owner")
    private String owner;
    @SerializedName("is_public")
    private int publicState;
    @SerializedName("is_lock")
    private int lockState;
    @SerializedName("status")
    private int status;
    @SerializedName("name")
    private String bookName;
    @SerializedName("lop")
    private String bookClass;
    @SerializedName("preview")
    private String preview;
    @SerializedName("song")
    private Song song;
    @SerializedName("lst_page")
    private ArrayList<Page> pages;

    public Book() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getPublicState() {
        return publicState;
    }

    public void setPublicState(int publicState) {
        this.publicState = publicState;
    }

    public void setLockState(int lockState) {
        this.lockState = lockState;
    }

    public int getLockState() {
        return lockState;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookClass() {
        if (bookClass == null) bookClass = "";
        return bookClass;
    }

    public void setBookClass(String bookClass) {
        this.bookClass = bookClass;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public ArrayList<Page> getPages() {
        if (pages == null) pages = new ArrayList<>();
        return pages;
    }

    public void setPages(ArrayList<Page> pages) {
        this.pages = pages;
    }

    public boolean isEmpty() {// dùng để check holder trống còn vẽ giao diện add
        return TextUtils.isEmpty(id) && TextUtils.isEmpty(owner);
    }
}

package com.metfone.selfcare.database.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by toanvk2 on 9/12/2016.
 */
public class WheelSpin implements Serializable {
    private int id;
    private String iconUrl;
    private String title;
    private String desc;
    private String guideLabel;
    private String actionLabel;
    private String guideDesc;
    private String fakeOaContent;
    private String alertContent;
    private String fakeOAMessageGift;
    // TODO: 5/21/2020 Bổ sung link image point share
    private String imageShareFb;
    private String serviceCode;
    private String linkShareFb; //todo link nhiệm vụ bí mật
    private String descriptionClosePopup;
    private String actionClosePopup;
    private String contentSuccessExtend;
    private long timeStamp = System.currentTimeMillis();

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDeepLink() {
        return deepLink;
    }

    private String deepLink;

    public String getDescriptionClosePopup() {
        return descriptionClosePopup;
    }

    public String getActionClosePopup() {
        return actionClosePopup;
    }

    public String getImageShareFb() {
        return imageShareFb;
    }

    public void setImageShareFb(String imageShareFb) {
        this.imageShareFb = imageShareFb;
    }

    public WheelSpin() {

    }

    public WheelSpin(JSONObject jsonObject) {
        setJSONObject(jsonObject);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getGuideLabel() {
        return guideLabel;
    }

    public void setGuideLabel(String guideLabel) {
        this.guideLabel = guideLabel;
    }

    public String getActionLabel() {
        return actionLabel;
    }

    public void setActionLabel(String actionLabel) {
        this.actionLabel = actionLabel;
    }

    public String getGuideDesc() {
        return guideDesc;
    }

    public void setGuideDesc(String guideDesc) {
        this.guideDesc = guideDesc;
    }

    public String getFakeOaContent() {
        return fakeOaContent;
    }

    public void setFakeOaContent(String fakeOaContent) {
        this.fakeOaContent = fakeOaContent;
    }

    public String getAlertContent() {
        return alertContent;
    }

    public void setAlertContent(String alertContent) {
        this.alertContent = alertContent;
    }

    public String getFakeOAMessageGift() {
        return fakeOAMessageGift;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getLinkShareFb() {
        return linkShareFb;
    }

    public void setLinkShareFb(String linkShareFb) {
        this.linkShareFb = linkShareFb;
    }

    public String getContentSuccessExtend() {
        return contentSuccessExtend;
    }

    private void setJSONObject(JSONObject object) {
        id = object.optInt("id");
        iconUrl = object.optString("icon");
        title = object.optString("title", "");
        desc = object.optString("description", "");
        guideLabel = object.optString("btnGuide");
        actionLabel = object.optString("btnAction");
        guideDesc = object.optString("guideDesc");
        fakeOaContent = object.optString("contentSuccess");
        alertContent = object.optString("contentAlert");
        fakeOAMessageGift = object.optString("contentNotify");
        imageShareFb = object.optString("imageShareFb"); // TODO: 5/21/2020 bổ sung ảnh share
        serviceCode = object.optString("serviceCode"); // TODO: 5/25/2020 Id log lại sau khi share
        linkShareFb = object.optString("linkShareFb");
        descriptionClosePopup = object.optString("descriptionClosePopup");
        actionClosePopup = object.optString("btnActionClosePopup");
        deepLink = object.optString("deepLink");
        contentSuccessExtend = object.optString("contentSuccessExtend");
    }
}
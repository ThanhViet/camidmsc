package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ReengAccountConstant;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.util.Log;

/**
 * Created by ThaoDV on 6/6/14.
 */
public class ReengAccountDataSource {
    private static String TAG = ReengAccountDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static ReengAccountDataSource reengAccountDataSource;

    private ReengAccountDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    public static synchronized ReengAccountDataSource getInstance(ApplicationController application) {
        if (reengAccountDataSource == null) {
            reengAccountDataSource = new ReengAccountDataSource(application);
        }
        return reengAccountDataSource;
    }

    public ReengAccount createReengAccount(ReengAccount reengAccount) {
        ContentValues values = new ContentValues();
        // set field
        //values.put(ReengAccountConstant.ID, 1);
        values.put(ReengAccountConstant.IS_ACTIVE, reengAccount.isActive());
        values.put(ReengAccountConstant.NAME, reengAccount.getName());
        values.put(ReengAccountConstant.PHONE_NUMBER, reengAccount.getJidNumber());
        values.put(ReengAccountConstant.TOKEN, reengAccount.getToken());
        values.put(ReengAccountConstant.AVATAR_LAST_CHANGE, reengAccount.getLastChangeAvatar());
        values.put(ReengAccountConstant.STATUS, reengAccount.getStatus());
        values.put(ReengAccountConstant.GENDER, reengAccount.getGender());
        values.put(ReengAccountConstant.BIRTHDAY, reengAccount.getBirthday());
        values.put(ReengAccountConstant.REGION_CODE, reengAccount.getRegionCode());
        values.put(ReengAccountConstant.BIRTHDAY_STRING, reengAccount.getBirthdayString());
        values.put(ReengAccountConstant.AVATAR_PATH, reengAccount.getAvatarPath());
        values.put(ReengAccountConstant.NEED_UPLOAD, reengAccount.getNeedUpload());
        values.put(ReengAccountConstant.PERMISSION, reengAccount.getPermission());
        values.put(ReengAccountConstant.AVNO_NUMBER, reengAccount.getAvnoNumber());
        values.put(ReengAccountConstant.AVNO_IDENTIFY_CARD_FRONT, reengAccount.getAvnoICFront());
        values.put(ReengAccountConstant.AVNO_IDENTIFY_CARD_BACK, reengAccount.getAvnoICBack());
        values.put(ReengAccountConstant.PREKEY, reengAccount.getPreKey());
        values.put(ReengAccountConstant.EMAIL, reengAccount.getEmail());
        values.put(ReengAccountConstant.ADDRESS, reengAccount.getAddress());
        values.put(ReengAccountConstant.CURRENT_ADDRESS, reengAccount.getCurrentAddress());
        // insert user to database
        deleteReengAccount();
        long id = databaseWrite.insert(ReengAccountConstant.TABLE, null, values);
        reengAccount.setId(id);
        return reengAccount;
    }

    public void deleteReengAccount() {
        databaseWrite.delete(ReengAccountConstant.TABLE, null, null);
    }

    public ReengAccount getAccount() {
        Cursor cursor = null;
        ReengAccount reengAccount = null;
        try {
//            cursor = databaseRead.rawQuery(ReengAccountConstant.GET_CURRENT_ACCOUNT_STATEMENT, null);
            cursor = databaseRead.query(ReengAccountConstant.TABLE,
                    null,
                    null,
                    null, null, null, null + " DESC", "" + 1);
            if (cursor == null) {
                return null;
            }
            cursor.moveToFirst();
            reengAccount = getReengAccountFromCursor(cursor);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return reengAccount;
    }

    private ReengAccount getReengAccountFromCursor(Cursor cursor) {
        if (cursor == null || cursor.getCount() <= 0) {
            return null;
        }
        ReengAccount reengAccount = new ReengAccount();
        reengAccount.setId(cursor.getLong(0));
        if (cursor.getInt(1) > 0) {
            reengAccount.setActive(true);
        } else {
            reengAccount.setActive(false);
        }
        reengAccount.setNumberJid(cursor.getString(2));
        reengAccount.setName(cursor.getString(3));
        reengAccount.setToken(cursor.getString(4));
        reengAccount.setLastChangeAvatar(cursor.getString(5));
        reengAccount.setStatus(cursor.getString(6));
        reengAccount.setGender(Integer.parseInt(cursor.getString(7)));
        reengAccount.setBirthday(cursor.getString(8));
        reengAccount.setRegionCode(cursor.getString(9));
        reengAccount.setBirthdayString(cursor.getString(10));
        reengAccount.setAvatarPath(cursor.getString(11));
        if (cursor.getInt(12) == 0) {
            reengAccount.setNeedUpload(false);
        } else {
            reengAccount.setNeedUpload(true);
        }
        reengAccount.setPermission(cursor.getInt(13));
        reengAccount.setAvnoNumber(cursor.getString(14));
        reengAccount.setAvnoICFront(cursor.getString(15));
        reengAccount.setAvnoICBack(cursor.getString(16));
        reengAccount.setPreKey(cursor.getString(17));
        reengAccount.setEmail(cursor.getString(18));
        reengAccount.setAddress(cursor.getString(19));
        reengAccount.setCurrentAddress(cursor.getString(20));
        return reengAccount;
    }

    public void updateToken(long reengAccountID, String token) {
        ContentValues values = new ContentValues();
        values.put(ReengAccountConstant.TOKEN, token);
        values.put(ReengAccountConstant.IS_ACTIVE, 1);
        String whereClause = ReengAccountConstant.ID + " = "
                + reengAccountID;
        databaseWrite.update(ReengAccountConstant.TABLE, values, whereClause, null);
    }

    public void updateReengAccount(ReengAccount account) {
        Log.d(TAG, "updateDb : " + account.toString());
        long reengAccountID = account.getId();
        ContentValues values = new ContentValues();
        values.put(ReengAccountConstant.IS_ACTIVE, account.isActive());
        values.put(ReengAccountConstant.NAME, account.getName());
        values.put(ReengAccountConstant.PHONE_NUMBER, account.getJidNumber());
        values.put(ReengAccountConstant.TOKEN, account.getToken());
        values.put(ReengAccountConstant.AVATAR_LAST_CHANGE, account.getLastChangeAvatar());
        values.put(ReengAccountConstant.STATUS, account.getStatus());
        values.put(ReengAccountConstant.GENDER, account.getGender());
        values.put(ReengAccountConstant.BIRTHDAY, account.getBirthday());
        values.put(ReengAccountConstant.REGION_CODE, account.getRegionCode());
        values.put(ReengAccountConstant.BIRTHDAY_STRING, account.getBirthdayString());
        values.put(ReengAccountConstant.BIRTHDAY_STRING, account.getBirthdayString());
        values.put(ReengAccountConstant.AVATAR_PATH, account.getAvatarPath());
        if (account.getNeedUpload()) {
            values.put(ReengAccountConstant.NEED_UPLOAD, 1);
        } else {
            values.put(ReengAccountConstant.NEED_UPLOAD, 0);
        }
        values.put(ReengAccountConstant.PERMISSION, account.getPermission());
        values.put(ReengAccountConstant.AVNO_NUMBER, account.getAvnoNumber());
        values.put(ReengAccountConstant.AVNO_IDENTIFY_CARD_FRONT, account.getAvnoICFront());
        values.put(ReengAccountConstant.AVNO_IDENTIFY_CARD_BACK, account.getAvnoICBack());
        values.put(ReengAccountConstant.PREKEY, account.getPreKey());
        values.put(ReengAccountConstant.EMAIL, account.getEmail());
        values.put(ReengAccountConstant.ADDRESS, account.getAddress());
        values.put(ReengAccountConstant.CURRENT_ADDRESS, account.getCurrentAddress());
        String whereClause = ReengAccountConstant.ID + " = " + reengAccountID;
        databaseWrite.update(ReengAccountConstant.TABLE, values, whereClause, null);
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(ReengAccountConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }
}
package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.BlockConstant;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/10/14.
 */
public class BlockDataSource {
    private static String TAG = ContactDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static BlockDataSource mInstance;

    public static synchronized BlockDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new BlockDataSource(application);
        }
        return mInstance;
    }

    private BlockDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    // insert list item
    public void insertNumber(String number) {
        try {
            if (databaseWrite == null) {
                return;
            }
            ContentValues values = new ContentValues();
            values.put(BlockConstant.NUMBER, number);
            databaseWrite.insert(BlockConstant.TABLE, null, values);
        } catch (Exception e) {
            Log.e(TAG, "insertNumber", e);
        }
    }

    // insert list item
    public void insertListNumber(ArrayList<String> listNumbers) {
        if (listNumbers == null || listNumbers.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (String number : listNumbers) {
                    ContentValues values = new ContentValues();
                    values.put(BlockConstant.NUMBER, number);
                    databaseWrite.insert(BlockConstant.TABLE, null, values);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Transaction", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "insertListNumber", e);
        }
    }

    // get all contact
    public ArrayList<String> getAllNumber() {
        Cursor cursor = null;
        ArrayList<String> listNumbers = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(BlockConstant.SELECT_ALL_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                listNumbers = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        String number = cursor.getString(0);
                        listNumbers.add(number);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllContact", e);
        } finally {
            closeCursor(cursor);
        }
        return listNumbers;
    }

    // del item
    public boolean isExistNumber(String number) {
        Cursor cursor = null;
        try {
            if (databaseRead == null)
                return false;
            cursor = databaseRead.rawQuery(BlockConstant.SELECT_NUMBER_STATEMENT, new String[]{number});
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    String item = cursor.getString(0);
                    if (item != null && item.equals(number)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "isExistNumber", e);
            return false;
        } finally {
            closeCursor(cursor);
        }
        return false;
    }

    // del item
    public void deleteNumber(String number) {
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.delete(BlockConstant.TABLE,
                    BlockConstant.NUMBER + " =? ", new String[]{number});
        } catch (Exception e) {
            Log.e(TAG, "deleteNumber", e);
        }
    }

    // del list item
    public void deleteListNumber(ArrayList<String> listNumbers) {
        if (listNumbers == null || listNumbers.size() <= 0) {
            return;
        }
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.beginTransaction();
            try {
                for (String number : listNumbers) {
                    databaseWrite.delete(BlockConstant.TABLE,
                            BlockConstant.NUMBER + " =? ", new String[]{number});
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteListNumber", e);
        }
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(BlockConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }

    /*public void dropTable() {
        try {
            databaseWrite.execSQL(BlockConstant.DROP_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "dropTable", e);
        }
    }*/
}
package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.FileHelper;

import java.util.Date;

/**
 * Created by toanvk2 on 11/20/2017.
 */

public class ReengFileMessage extends ReengMessage {
    public ReengFileMessage(ThreadMessage thread, String from, String to, String filePath, String fileType, String fileName) {
        super(thread.getThreadType(), ReengMessageConstant.MessageType.file);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setFileName(fileName);
        setFilePath(filePath);
        setSize(FileHelper.getSizeOfFile(filePath));
        setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        setDirection(ReengMessageConstant.Direction.send);
        Date date = new Date();
        setTime(date.getTime());
        setReadState(ReengMessageConstant.READ_STATE_READ);
        //get file name from file path
        setMessageType(ReengMessageConstant.MessageType.file);
        setFileType(fileType);
    }
}

package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ChannelDataConstants;
import com.metfone.selfcare.model.tab_video.Channel;

public final class ChannelDataSource implements ChannelDataConstants {

    private static ChannelDataSource mInstance;

    public static synchronized ChannelDataSource getInstance() {
        if (mInstance == null)
            mInstance = new ChannelDataSource();
        return mInstance;
    }

    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private Gson gson;

    private ChannelDataSource() {
        if (ApplicationController.self() != null) {
            this.gson = new Gson();
            this.databaseRead = ReengSQLiteHelper.getMyReadableDatabase();
            this.databaseWrite = ReengSQLiteHelper.getMyWritableDatabase();
        }
    }

    public long getTimeNewChannel(String channelId) {
        long timeNew = 0;
        Cursor cursor = null;
        try {
            if (databaseRead != null) {
                cursor = databaseRead.rawQuery(SELECT_STATEMENT_BY_CHANNEL_ID, new String[]{channelId});
                if (cursor != null && cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        String time = cursor.getString(cursor.getColumnIndex(TIME_NEW));
                        timeNew = Long.valueOf(time);
                    }
                }
            }
        } catch (Exception ignored) {
            timeNew = 0;
        } finally {
            if (cursor != null) cursor.close();
        }
        return timeNew;
    }

    public void saveTimeNewChannel(Channel channel) {
        try {
            if (channel == null || databaseWrite == null)
                return;

            long time = getTimeNewChannel(channel.getId());
            if (!channel.isFollow() && channel.getLastPublishVideo() < time)
                return;
            deleteChannel(channel.getId());
            ContentValues values = new ContentValues();
            values.put(CHANNEL_ID, channel.getId());
            values.put(CONTENT, gson.toJson(channel));
            values.put(TIME_NEW, channel.getLastPublishVideo() + "");
            databaseWrite.insert(TABLE, null, values);
        } catch (Exception ignored) {
        }
    }

    public void deleteChannel(String channelId) {
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.delete(TABLE, CHANNEL_ID + " = ?", new String[]{channelId});
        } catch (Exception ignored) {
        }
    }
}

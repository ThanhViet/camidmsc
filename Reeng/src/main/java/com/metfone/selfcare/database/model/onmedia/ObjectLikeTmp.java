package com.metfone.selfcare.database.model.onmedia;

/**
 * Created by thanhnt72 on 3/18/2016.
 */
public class ObjectLikeTmp {
    private String rowId;
    private FeedModelOnMedia.ActionLogApp actionLogApp;
    private FeedContent feedContent;

    public ObjectLikeTmp(String rowId, FeedModelOnMedia.ActionLogApp actionLogApp, FeedContent feedContent) {
        this.rowId = rowId;
        this.actionLogApp = actionLogApp;
        this.feedContent = feedContent;
    }

    public String getRowId() {
        return rowId;
    }

    public FeedModelOnMedia.ActionLogApp getActionLogApp() {
        return actionLogApp;
    }

    public FeedContent getFeedContent() {
        return feedContent;
    }
}

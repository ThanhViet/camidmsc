package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.VideoConstant;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

public final class VideoDataSource {

    private static final String TAG = VideoDataSource.class.getSimpleName();
    private static final int LIMIT_DATA = 50;
    private static VideoDataSource mInstance;

    public static synchronized VideoDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new VideoDataSource(application);
        }
        return mInstance;
    }

    private Gson gson;
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;

    private VideoDataSource(ApplicationController application) {
        gson = new Gson();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null)
            cursor.close();
    }

    public void saveOrUnSaveVideo(Video video) {
        if (video.isSave()) {
            deleteVideoLast(VideoConstant.Type.SAVED.VALUE);
            insertVideoSave(video);
        } else {
            deleteVideoSave(video);
        }
    }

    public void saveVideoFromMenu(Video video) {
        if (!isSave(video)) {
            deleteVideoLast(VideoConstant.Type.SAVED.VALUE);
            insertVideoSave(video);
        }
    }

    public void addOrRemoveWatchLater(Video video) {
        if (video.isWatchLater()) {
            deleteVideoLast(VideoConstant.Type.LATER.VALUE);
            insertVideoWatchLater(video);
        } else {
            deleteVideoWatchLater(video);
        }
    }

    public void watchLaterVideoFromMenu(Video video) {
        if (!isWatchLater(video)) {
            deleteVideoLast(VideoConstant.Type.LATER.VALUE);
            insertVideoWatchLater(video);
        }
    }

    public boolean isSave(Video video) {
        return getVideoSave(video) != null;
    }

    public boolean isWatchLater(Video video) {
        return getVideoWatchLater(video) != null;
    }

    public ArrayList<Video> getAllVideoByType(String type) {
        return getVideosByType(type);
    }

    private void insertVideoWatchLater(Video video) {
        insertVideo(video, VideoConstant.Type.LATER);
    }

    private void insertVideoSave(Video video) {
        insertVideo(video, VideoConstant.Type.SAVED);
    }

    private void insertVideo(Video video, VideoConstant.Type type) {
        insertVideo(video, type.VALUE);
    }

    private void deleteVideoSave(Video video) {
        deleteVideo(video, VideoConstant.Type.SAVED);
    }

    private void deleteVideoWatchLater(Video video) {
        deleteVideo(video, VideoConstant.Type.LATER);
    }

    private void deleteVideo(Video video, VideoConstant.Type type) {
        deleteVideo(video, type.VALUE);
    }

    private Video getVideoSave(Video video) {
        return getVideo(video, VideoConstant.Type.SAVED);
    }

    private Video getVideoWatchLater(Video video) {
        return getVideo(video, VideoConstant.Type.LATER);
    }

    private Video getVideo(Video video, VideoConstant.Type type) {
        return getVideo(video, type.VALUE);
    }

    /**
     * thực thêm video vào trong database
     *
     * @param video video cần thêm
     * @param type  loại video
     */
    private void insertVideo(Video video, String type) {
        try {
            if (databaseWrite == null) {
                return;
            }
            ContentValues values = new ContentValues();
            values.put(VideoConstant.VIDEO_ID, video.getId());
            values.put(VideoConstant.TYPE, type);
            values.put(VideoConstant.CONTENT, gson.toJson(video));
            databaseWrite.insert(VideoConstant.TABLE, null, values);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * thực hiện xóa video
     *
     * @param video video cần xóa
     * @param type  loại video
     */
    private void deleteVideo(Video video, String type) {
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.delete(VideoConstant.TABLE, VideoConstant.VIDEO_ID + " = ? AND " + VideoConstant.TYPE + " = ?", new String[]{video.getId(), type});
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * lấy tra thông tin video
     *
     * @param video video cần lấy thông tin
     * @param type  loại video
     * @return thông tin video
     */
    private Video getVideo(Video video, String type) {
        Video videoLocal = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(VideoConstant.SELECT_STATEMENT_BY_ID_AND_TYPE, new String[]{video.getId(), type});
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    String data = cursor.getString(cursor.getColumnIndex(VideoConstant.CONTENT));
                    videoLocal = gson.fromJson(data, Video.class);
                }
            }
        } catch (Exception e) {
        } finally {
            closeCursor(cursor);
        }
        return videoLocal;
    }

    /**
     * lấy tất cả video theo type
     *
     * @param type loại video
     * @return danh sách video
     */
    private ArrayList<Video> getVideosByType(String type) {
        deleteAllVideoEmpty();

        ArrayList<Video> videos = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(VideoConstant.SELECT_STATEMENT_BY_TYPE, new String[]{type});
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        String data = cursor.getString(cursor.getColumnIndex(VideoConstant.CONTENT));
                        Video video = gson.fromJson(data, Video.class);
                        if (video != null && TextUtils.isEmpty(video.getId()))
                            deleteVideo(video, type);
                        else if (video != null) {
                            videos.add(video);
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
        } finally {
            closeCursor(cursor);
        }
        return videos;
    }

    /**
     * xóa đi phần tử cuối cũng nếu số lượng phần tử vượt quá limit
     *
     * @param type loại video
     */
    private void deleteVideoLast(String type) {
        try {
            if (databaseWrite == null)
                return;
            databaseWrite.execSQL(VideoConstant.DELETE_LAST_STATEMENT_BY_TYPE, new Object[]{type, type, LIMIT_DATA});
        } catch (Exception e) {
        }
    }

    /**
     * xóa tất cả video rỗng
     */
    private void deleteAllVideoEmpty() {
        try {
            if (databaseWrite == null) return;
            databaseWrite.delete(VideoConstant.TABLE, VideoConstant.VIDEO_ID + " = ? ", new String[]{""});
        } catch (Exception e) {
        }
    }
}

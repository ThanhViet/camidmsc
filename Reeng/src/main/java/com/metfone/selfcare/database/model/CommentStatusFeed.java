/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.database.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author datdh
 */
public class CommentStatusFeed implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    //TODO: id chinh la thoi gian comment @@
    @SerializedName("id")
    public String id;

    //chua dung
    @SerializedName("comment")
    public String comment;

    // thay = id
//    @SerializedName("time")
//    private long time;

    //chua dung
    @SerializedName("channel")
    public String channel = "android";

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("user")
    private UserInfo user;


    @SerializedName("total_comment")
    private int total_comment = 0;

    @SerializedName("total_like")
    private int total_like = 0;


    //chua dung
    @SerializedName("total_share")
    private int total_share = 0;

    @SerializedName("status")
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

//    public long getTime() {
//        return time;
//    }

//    public void setTime(long time) {
//        this.time = time;
//    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public int getTotal_comment() {
        return total_comment;
    }

    public void setTotal_comment(int total_comment) {
        this.total_comment = total_comment;
    }

    public int getTotal_like() {
        return total_like;
    }

    public void setTotal_like(int total_like) {
        this.total_like = total_like;
    }

    public int getTotal_share() {
        return total_share;
    }

    public void setTotal_share(int total_share) {
        this.total_share = total_share;
    }

    public String getUserName() {
        if (user == null) return "***";
        return user.getNameUser();
    }

    public String getAvatar() {
        if (user == null) return "";
        return user.getAvatar();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CommentStatusFeed{" +
                "id='" + id + '\'' +
                ", comment='" + comment + '\'' +
//                ", time=" + time +
                ", channel='" + channel + '\'' +
                ", created_at='" + created_at + '\'' +
                ", user=" + user +
                ", total_comment=" + total_comment +
                ", total_like=" + total_like +
                ", total_share=" + total_share +
                '}';
    }
}

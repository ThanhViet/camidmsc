package com.metfone.selfcare.database.constant;

/**
 * Created by toanvk2 on 6/26/14.
 */
public class NumberConstant {
    public static final String TABLE = "phone_number";
    public static final String ID = "number_id";
    public static final String NUMBER = "number";// se luu jid number  chuan e164
    public static final String STATUS = "status";
    public static final String LAST_CHANGE_AVATAR = "lc_avatar";
    public static final String STATE = "state";
    public static final String IS_NEW = "is_new";
    public static final String IS_ROSTER = "add_roster";
    public static final String FAVORITE = "favorite";
    public static final String GENDER = "gender";
    public static final String BIRTHDAY = "birthday";
    //
    public static final String CONTACT_ID = "contact_id";
    public static final String NAME = "name";
    public static final String NAME_UNICODE = "name_unicode";
    public static final String NUMBER_RAW = "number_format_e164";// luu so dien thoai chuan quoc gia
    public static final String BIRTHDAY_STRING = "birthday_string";
    public static final String COVER = "cover";
    public static final String NICK_NAME = "albums";// không lưu album vào DB nên sẽ dung cot này lưu nick mocha
    public static final String PERMISSION = "permission";
    public static final String OPERATOR = "operator";
    public static final String PREKEY = "prekey";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE
            + " (" + ID + " TEXT, " +
            NUMBER + " TEXT, " +
            STATUS + " TEXT, " +
            LAST_CHANGE_AVATAR + " TEXT, " +
            STATE + " INTEGER, " +
            IS_NEW + " INTEGER, " +
            CONTACT_ID + " TEXT, " +
            NAME + " TEXT, " +
            IS_ROSTER + " INTEGER, " +
            NAME_UNICODE + " TEXT, " +
            FAVORITE + " INTEGER, " +
            GENDER + " INTEGER, " +
            BIRTHDAY + " TEXT, " +
            NUMBER_RAW + " TEXT, " +
            BIRTHDAY_STRING + " TEXT, " +
            COVER + " TEXT, " +
            NICK_NAME + " TEXT, " +
            PERMISSION + " INTEGER, " +
            OPERATOR + " TEXT, " +
            PREKEY + " TEXT" +
            ")";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String SELECT_ALL_QUERY_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String SELECT_BY_CONTACT_ID_STATEMENT = "SELECT * FROM "
            + TABLE + " WHERE " + CONTACT_ID + " = ";
    public static final String SELECT_UNSEND_ROSTER_STATEMENT = "SELECT * FROM "
            + TABLE + " WHERE " + IS_ROSTER + " = 0";
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM "
            + TABLE;
    public static final String WHERE_ID = ID + " = ?";

    public static final String WHERE_NUMBER = NUMBER + " = ?";

    public static final String WHERE_CONTACT_ID = CONTACT_ID + " = ?";

    public static final String CONTACT_DETAIL_TYPE = "CONTACT_DETAIL_TYPE";

    public static final int TYPE_CONTACT = 1;
    public static final int TYPE_CONTACT_NOTIFICATION = 2;  // vao tu notification
    public static final int TYPE_STRANGER_EXIST = 3;        // stranger da ton tai
    public static final int TYPE_STRANGER_MOCHA = 4;        // chat giau so tu mocha
    public static final int TYPE_OFFICIAL_ONMEDIA = 5;

    public static final String ALTER_COLUMN_NUMBER_JID = "ALTER TABLE " + TABLE + " ADD COLUMN " + NUMBER_RAW + " TEXT;";

    public static final int TYPE_HOLDER_PHONE = 0;
    public static final int TYPE_HOLDER_OFFICER = 1;
    public static final int TYPE_HOLDER_SECTION = 2;
    public static final int TYPE_HOLDER_DIVIDER = 9;

    public static final String ALTER_COLUMN_BIRTHDAY_STRING = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + BIRTHDAY_STRING + " TEXT;";

    public static final long TIME_OUT_GET_NON_CONTACT = 5 * 60 * 1000;// 5 phut

    public static final String ALTER_COLUMN_COVER = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + COVER + " TEXT;";

    public static final String ALTER_COLUMN_NICK_NAME = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + NICK_NAME + " TEXT;";
    public static final String ALTER_COLUMN_PERMISSION = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + PERMISSION + " INTEGER DEFAULT -1;";
    public static final String ALTER_COLUMN_OPERATOR = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + OPERATOR + " TEXT;";
    public static final String ALTER_COLUMN_PREKEY = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + PREKEY + " TEXT;";

    public static final int PERMISSION_BIRTHDAY_ON = 1;// 00000001
    public static final int PERMISSION_HIDE_STRANGER_HISTORY = 2;// 00000010

    // section type
    public static final int SECTION_TYPE_OFFICIAL = 1;
    public static final int SECTION_TYPE_NEW_FRIENDS = 2;
    public static final int SECTION_TYPE_SOCIAL_REQUEST = 3;
    public static final int SECTION_TYPE_SUGGEST_FRIEND = 4;
    public static final int SECTION_TYPE_CONTACT = 5;
    public static final int SECTION_TYPE_CONTACT_HISTORY = 6;
}

package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.constant.ReengMessageConstant.MessageType;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Date;

/**
 * Created by thanhnt72 on 06/04/2015.
 */
public class SoloShareVideoMessage extends ReengMessage {
    //String filePath, String fileName,
//    FileType fileType, MessageType messageType, int duration
    public SoloShareVideoMessage(ThreadMessage thread, String from, String to, String filePath, int duration,
                                 String fileName, int fileSize, String contentUri, String thumb) {
        super(thread.getThreadType(), MessageType.shareVideo);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setFilePath(filePath);
        setDuration(duration);
        setFileName(fileName);
        setSize(fileSize);
        setReadState(ReengMessageConstant.READ_STATE_READ);
        setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        setMessageType(MessageType.shareVideo);
        setDirection(Direction.send);
        Date date = new Date();
        setTime(date.getTime());
        setImageUrl(thumb);
        setVideoContentUri(contentUri);
    }
}
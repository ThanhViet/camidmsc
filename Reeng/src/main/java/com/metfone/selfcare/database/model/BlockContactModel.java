package com.metfone.selfcare.database.model;

/**
 * Created by thanhnt72 on 12/18/2017.
 */

public class BlockContactModel {

    public static final int STATE_BLOCK = 1;
    public static final int STATE_UNBLOCK = 0;

    private String msisdn;
    private int status;


    public BlockContactModel(String msisdn, int status) {
        this.msisdn = msisdn;
        this.status = status;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "{msisdn=\"" + msisdn + '\"' +
                ", status=" + status + '}';
    }
}

package com.metfone.selfcare.database.model.avno;

import java.io.Serializable;

public class ResultOtp implements Serializable {
    int code ;
    String error_msg;
    String session_id;

    public ResultOtp(int code, String error_msg) {
        this.code = code;
        this.error_msg = error_msg;
    }

    public ResultOtp(int code, String error_msg, String session_id) {
        this.code = code;
        this.error_msg = error_msg;
        this.session_id = session_id;
    }

    public int getCode() {
        return code;
    }

    public String getError_msg() {
        return error_msg;
    }

    public String getSession_id() {
        return session_id;
    }
}

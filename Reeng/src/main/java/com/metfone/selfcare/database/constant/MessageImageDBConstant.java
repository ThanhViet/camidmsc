package com.metfone.selfcare.database.constant;

/**
 * Created by thanhnt72 on 8/1/2018.
 */

public class MessageImageDBConstant {
    public static final String TABLE = "MessageImage";
    public static final String ID = "id";
    public static final String FILE_PATH_ORI = "path_ori";
    public static final String FILE_PATH_NEW = "path_new";
    public static final String DATA_RESPONSE = "data_response";
    public static final String TIME_UPLOAD = "time_upload";
    public static final String RATIO = "ratio";
    public static final String DIGIT = "digit";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE +
            " (" + ID + " INTEGER PRIMARY KEY, "
            + FILE_PATH_ORI + " TEXT, "
            + FILE_PATH_NEW + " TEXT, "
            + DATA_RESPONSE + " TEXT, "
            + RATIO + " TEXT, "
            + TIME_UPLOAD + " TEXT, "
            + DIGIT + " TEXT)";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;

    public static final String ALTER_COLUMN_DIGIT = "ALTER TABLE " + TABLE
            + " ADD COLUMN " + DIGIT + " TEXT;";

}

package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ConfigUserConstant;
import com.metfone.selfcare.database.model.KeyValueModel;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 2/26/2015.
 */
public class ConfigUserDataSource {
    // Database fields
    private static String TAG = ConfigUserDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static ConfigUserDataSource mInstance;

    public static synchronized ConfigUserDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new ConfigUserDataSource(application);
        }
        return mInstance;
    }

    private ConfigUserDataSource(ApplicationController application) {
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    /**
     * close cursor
     *
     * @param cursor
     */
    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    /**
     * set content values
     *
     * @param eventMessage
     * @return
     */
    public ContentValues setValues(KeyValueModel eventMessage) {
        ContentValues values = new ContentValues();
        values.put(ConfigUserConstant.KEY, eventMessage.getKey());
        values.put(ConfigUserConstant.VALUE, eventMessage.getValue());
        return values;
    }

    /**
     * insert list event message to database
     *
     * @param list
     */
    public void insertListConfigUser(ArrayList<KeyValueModel> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.beginTransaction();
            try {
                for (KeyValueModel item : list) {
                    ContentValues values = setValues(item);
                    long id = databaseWrite.insert(ConfigUserConstant.TABLE, null, values);
                    item.setId(id);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction insertListKeyValueModel", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "insertListKeyValueModel", e);
        }
    }

    /**
     * insert event message to database
     *
     * @param eventMessage
     */
    public void insertConfigUser(KeyValueModel eventMessage) {
        try {
            if (databaseWrite == null) {
                return;
            }
            ContentValues values = setValues(eventMessage);
            long id = databaseWrite.insert(ConfigUserConstant.TABLE, null, values);
            eventMessage.setId(id);
        } catch (Exception e) {
            Log.e(TAG, "insertKeyValueModel", e);
        }
    }

    public void updateConfigUser(KeyValueModel keyValueModel) {
        ContentValues values = new ContentValues();
        values.put(ConfigUserConstant.KEY, keyValueModel.getKey());
        values.put(ConfigUserConstant.VALUE, keyValueModel.getValue());
        String whereClause = ConfigUserConstant.KEY + " = '" + keyValueModel.getKey() + "'";
        databaseWrite.update(ConfigUserConstant.TABLE, values, whereClause, null);
    }

    /**
     * get values from cursor
     *
     * @param cur
     * @return
     */
    public KeyValueModel getValuesFromCursor(Cursor cur) {
        KeyValueModel eventMessage = new KeyValueModel();
        eventMessage.setId(cur.getLong(0));
        eventMessage.setKey(cur.getString(1));
        eventMessage.setValue(cur.getString(2));
        return eventMessage;
    }

    /**
     * get list event message from dabatabsae by message id, and packet id
     *
     * @param messageId
     * @return listEvenetMessage
     */
    public ArrayList<KeyValueModel> getListConfigUserFromKey(String key) {
        ArrayList<KeyValueModel> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query = ConfigUserConstant.SELECT_BY_KEY_QUERY + "'" + key + "'";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    KeyValueModel item = getValuesFromCursor(cursor);
                    list.add(item);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getListKeyValueModelFromMessageId", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public ArrayList<KeyValueModel> getAllConfigUser() {
        ArrayList<KeyValueModel> listConfigUser = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(ConfigUserConstant.SELECT_ALL_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                listConfigUser = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        listConfigUser.add(getValuesFromCursor(cursor));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllConfigUser", e);
        } finally {
            closeCursor(cursor);
        }
        return listConfigUser;
    }

    /**
     * xoa event message khi xoa 1 message
     *
     * @param messageId
     */
    public void deleteConfigUserByKey(String key) {
        if (databaseWrite == null) {
            return;
        }
        try {
            String whereClause = ConfigUserConstant.KEY + " = '" + key + "'";
            databaseWrite.delete(ConfigUserConstant.TABLE, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    /**
     * delete list event message from database
     *
     * @param list
     */
    public void deleteListConfigUser(ArrayList<KeyValueModel> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (KeyValueModel item : list) {
                    String whereClause = ConfigUserConstant.KEY + " = '" + item.getKey() + "'";
                    databaseWrite.delete(ConfigUserConstant.TABLE, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction deleteListKeyValueModel", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteListKeyValueModel", e);
        }
    }

    /**
     * delete all event message of table
     */
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(ConfigUserConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllContact", e);
        }
    }
}

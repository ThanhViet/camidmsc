package com.metfone.selfcare.database.model.call;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 10/5/2017.
 */

public class CallSubscription implements Serializable {
    @SerializedName("remain_time")
    private String remainTime;
    @SerializedName("change_sub_price")
    private String changeSubPrice;
    @SerializedName("change_sub_confirm")
    private String changeSubConfirm;
    @SerializedName("free_phone_number")
    private ArrayList<String> freeNumbers;
    @SerializedName("fake_mo")
    private CallSubscriptionFakeMo fakeMo;
    @SerializedName("confirm")
    private CallSubsConfirm callSubsConfirm;

    public CallSubscription() {
    }

    public CallSubscriptionFakeMo getFakeMo() {
        return fakeMo;
    }

    public String getRemainTime() {
        return remainTime;
    }

    public String getChangeSubPrice() {
        return changeSubPrice;
    }

    public String getChangeSubConfirm() {
        return changeSubConfirm;
    }

    public ArrayList<String> getFreeNumbers() {
        return freeNumbers;
    }

    public void setFreeNumbers(ArrayList<String> freeNumbes) {
        this.freeNumbers = freeNumbes;
    }

    public CallSubsConfirm getCallSubsConfirm() {
        return callSubsConfirm;
    }
}

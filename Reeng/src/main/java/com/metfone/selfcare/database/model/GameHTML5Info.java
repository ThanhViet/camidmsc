package com.metfone.selfcare.database.model;

/**
 * Created by thanhnt72 on 5/29/2017.
 */
public class GameHTML5Info {

    private int idGame=-1;
    private String titleGame="";
    private String desGame="";
    private String link="";
    private String thumb="";

    public GameHTML5Info(int idGame, String titleGame, String desGame, String link, String thumb) {
        this.idGame = idGame;
        this.titleGame = titleGame;
        this.desGame = desGame;
        this.link = link;
        this.thumb = thumb;
    }

    public String getTitleGame() {
        return titleGame;
    }

    public String getDesGame() {
        return desGame;
    }

    public String getLink() {
        return link;
    }

    public String getThumb() {
        return thumb;
    }
}

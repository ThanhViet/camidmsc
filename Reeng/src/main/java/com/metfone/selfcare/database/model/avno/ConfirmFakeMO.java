package com.metfone.selfcare.database.model.avno;

/**
 * Created by thanhnt72 on 11/3/2018.
 */

public class ConfirmFakeMO {
    private String label;
    private String desc;
    private String url;

    public ConfirmFakeMO(String label, String desc, String url) {
        this.label = label;
        this.desc = desc;
        this.url = url;
    }

    public String getLabel() {
        return label;
    }

    public String getDesc() {
        return desc;
    }

    public String getUrl() {
        return url;
    }
}
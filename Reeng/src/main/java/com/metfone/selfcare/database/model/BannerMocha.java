package com.metfone.selfcare.database.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by toanvk2 on 11/09/2015.
 */
public class BannerMocha implements Serializable {
    private static final String TAG = BannerMocha.class.getSimpleName();

    public static final String TARGET_TAB_CHAT = "TAB_CHAT";
//    public static final String TARGET_TAB_CALL = "TAB_CALL";

    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("desc")
    private String desc;
    @SerializedName("type")
    private int type;
    @SerializedName("icon")
    private String icon;
    @SerializedName("link")
    private String link;
    @SerializedName("confirm")
    private String confirm;
    @SerializedName("timesend")
    private long timeSend;
    @SerializedName("expire")
    private long expire;
    @SerializedName("image")
    private String image;
    @SerializedName("isImage")
    private boolean isImage = false;

    @SerializedName("target")
    private String target;

    public BannerMocha() {

    }

    public BannerMocha(String id, String title, String desc, String icon, String link, int type, boolean isImage, long expire) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.icon = icon;
        this.link = link;
        this.type = type;
        this.isImage = isImage;
        this.expire = expire;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getIcon() {
        return icon;
    }

    public String getLink() {
        return link;
    }

    public int getType() {
        return type;
    }

    public String getConfirm() {
        return confirm;
    }

    public String getImage() {
        return image;
    }

    public boolean isImage() {
        return isImage;
    }

    public boolean isExpired() {
        if (expire == -1) return false;
        return TimeHelper.getCurrentTime() >= timeSend + expire;
    }

    public boolean isValid() {
        if (TextUtils.isEmpty(title) || TextUtils.isEmpty(link) || TextUtils.isEmpty(desc)) {
            // title, des hoac link null
            return false;
        } else if (type < Constants.BANNER.TYPE_WEB_VIEW || type > Constants.BANNER.TYPE_FAKE_MO) {
            // type < 1 hoac > 3 (ngoai type da dinh nghia)
            return false;
        }
        return true;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void parserFromJSON(JSONObject json) {
        this.id = json.optString("id", "");
        this.type = json.optInt("type", 0);
        this.title = json.optString("title", null);
        this.desc = json.optString("desc", null);
        this.icon = json.optString("icon", null);
        this.confirm = json.optString("confirm", null);
        this.image = json.optString("image", null);
        this.isImage = json.optBoolean("isImage", false);
        this.link = json.optString("link", null);
        this.timeSend = json.optLong("timesend", 0);
        this.expire = json.optLong("expire", 0);
        this.target = json.optString("target", TARGET_TAB_CHAT);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TAG).append(" id: ").append(id);
        sb.append(" title: ").append(title);
        sb.append(" desc: ").append(desc);
        sb.append(" icon: ").append(icon);
        sb.append(" link: ").append(link);
        sb.append(" type: ").append(type);
        sb.append(" image: ").append(image);
        sb.append(" isImage: ").append(isImage);
        sb.append(" timesend: ").append(timeSend);
        sb.append(" expire: ").append(expire);
        sb.append(" target: ").append(target);
        return sb.toString();
    }
}
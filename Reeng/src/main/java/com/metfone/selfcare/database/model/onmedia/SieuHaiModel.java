package com.metfone.selfcare.database.model.onmedia;

/**
 * Created by thanhnt72 on 7/12/2017.
 */
public class SieuHaiModel {

    private long id;
    private String title;
    private String description;
    private String videoUrl;
    private String imageUrl;
    private String timeCreate;
    private String link;
    private long countView = -1;

    public SieuHaiModel(){

    }

    public SieuHaiModel(long id, String title, String description, String videoUrl, String imageUrl, String
            timeCreate, String link, long countView) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.videoUrl = videoUrl;
        this.imageUrl = imageUrl;
        this.timeCreate = timeCreate;
        this.link = link;
        this.countView = countView;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTimeCreate() {
        return timeCreate;
    }

    public String getLink() {
        return link;
    }

    public long getCountView() {
        return countView;
    }

    public void setCountView(long countView) {
        this.countView = countView;
    }

    @Override
    public String toString() {
        return "SieuHaiModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", timeCreate='" + timeCreate + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}

package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 3/5/2016.
 */
public class TagMocha implements Serializable {

    @SerializedName("tag_name")
    private String tagName = "";

    @SerializedName("name")
    private String name = "";

    @SerializedName("msisdn")
    private String msisdn = "";

    private transient String contactName = "";

    private transient int offset;

    public TagMocha(String tagName, String name, String msisdn) {
        this.tagName = tagName;
        this.name = name;
        this.msisdn = msisdn;
    }

    public TagMocha() {
    }

    public String getTagName() {
        return tagName;
    }

    public String getName() {
        return name;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public interface OnClickTag {
        void OnClickUser(String msisdn, String name);
    }

    @Override
    public String toString() {
        return "TagMocha{" +
                "tagName='" + tagName + '\'' +
                ", name='" + name + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", contactName='" + contactName + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TagMocha) {
            TagMocha c = (TagMocha) o;
            if (this.name.equals(c.name))
                return true;
        }
        return false;
    }
}

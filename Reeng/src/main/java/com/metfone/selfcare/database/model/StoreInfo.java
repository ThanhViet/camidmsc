package com.metfone.selfcare.database.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class StoreInfo implements Serializable {
    @SerializedName("storeName")
    String storeName;
    @SerializedName("storeNumber")
    int storeNumber;
    @SerializedName("storeAvatar")
    String storeAvatar;
    @SerializedName("storeIcon")
    String storeIcon;
    @SerializedName("newCollections")
    ArrayList<Integer> newCollections;
    // danh sach bo sticker bi disable tren sv,user down ve thi van dung bt
    @SerializedName("deletedCollections")
    ArrayList<Integer> hiddenCollections;
    // danh sach bo sticker bi xoa tren sv. user chua down hay da down de an het di
    @SerializedName("disabledCollections")
    ArrayList<Integer> removedCollections;

    public String getStoreName() {
        return storeName;
    }

    public int getStoreNumber() {
        return storeNumber;
    }

    public String getStoreAvatar() {
        return storeAvatar;
    }

    public String getStoreIcon() {
        return storeIcon;
    }

    public ArrayList<Integer> getNewCollections() {
        return newCollections;
    }

    public ArrayList<Integer> getHiddenCollections() {
        return hiddenCollections;
    }

    public ArrayList<Integer> getRemovedCollections() {
        return removedCollections;
    }
}

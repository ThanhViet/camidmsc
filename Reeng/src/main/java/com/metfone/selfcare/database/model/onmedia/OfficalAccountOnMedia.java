package com.metfone.selfcare.database.model.onmedia;

import android.text.TextUtils;

import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 4/27/2016.
 */
public class OfficalAccountOnMedia {
    private static final String TAG = OfficalAccountOnMedia.class.getSimpleName();
    public static final int STATE_FOLLOW = 1;
    public static final int STATE_NOT_FOLLOW = -1;

    private String nameOfficial;
    private String idOfficial;
    private String urlCover;
    private String urlAvatar;
    private long follower = 0;
    private boolean userStateFollow;
    private String status;
    private String albumString = null;
    private ArrayList<ImageProfile> mImageProfile;
    private int isCoverDefault = 0;     // =0 la default

    private OfficialChatInfo ofChatInfo;

    public OfficalAccountOnMedia(String nameOfficial, String idOfficial) {
        this.nameOfficial = nameOfficial;
        this.idOfficial = idOfficial;
    }

    public String getNameOfficial() {
        return nameOfficial;
    }

    public void setNameOfficial(String nameOfficial) {
        this.nameOfficial = nameOfficial;
    }

    public String getIdOfficial() {
        return idOfficial;
    }

    public void setIdOfficial(String idOfficial) {
        this.idOfficial = idOfficial;
    }

    public String getUrlCover() {
        return urlCover;
    }

    public void setUrlCover(String urlCover) {
        this.urlCover = urlCover;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public long getFollower() {
        return follower;
    }

    public void setFollower(long follower) {
        this.follower = follower;
    }

    public ArrayList<ImageProfile> getImageProfile() {
        return mImageProfile;
    }

    public void setImageProfile(ArrayList<ImageProfile> mImageProfile) {
        this.mImageProfile = mImageProfile;
    }

    public ImageProfile getImageCover() {
        if (TextUtils.isEmpty(urlCover))
            return null;
        ImageProfile imageProfile = new ImageProfile();
        imageProfile.setTypeImage(ImageProfileConstant.IMAGE_COVER);
        imageProfile.setImageUrl(urlCover);
        imageProfile.setHasCover(isCoverDefault != 0);
        return imageProfile;
    }

    public ArrayList<ImageProfile> getAlbums() {
        if (TextUtils.isEmpty(albumString))
            return new ArrayList<>();

        ArrayList<ImageProfile> albums = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(albumString);
            int length = jsonArray.length();
            if (length > 0) {
                for (int i = 0; i < length; i++) {
                    JSONObject jsonObj = jsonArray.getJSONObject(i);
                    if (jsonObj != null) {
                        ImageProfile image = new ImageProfile();
                        image.setId(jsonObj.getInt("imageId"));
                        image.setImageUrl(jsonObj.getString("imageUrl"));
                        image.setTypeImage(ImageProfileConstant.IMAGE_NORMAL);
                        albums.add(image);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getAlbums", e);
        }
        return albums;
    }

    public void setAlbumString(String albumString) {
        this.albumString = albumString;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isUserFollow() {
        return userStateFollow;
    }

    public void setUserStateFollow(boolean userStateFollow) {
        this.userStateFollow = userStateFollow;
    }

    public OfficialChatInfo getOfChatInfo() {
        return ofChatInfo;
    }

    public void setOfChatInfo(String id, String name, String avatar) {
        this.ofChatInfo = new OfficialChatInfo(id, name, avatar);
    }

    public int getIsCoverDefault() {
        return isCoverDefault;
    }

    public void setIsCoverDefault(int isCoverDefault) {
        this.isCoverDefault = isCoverDefault;
    }

    @Override
    public String toString() {
        return "OfficalAccountOnMedia{" +
                "albumString='" + albumString + '\'' +
                ", status='" + status + '\'' +
                ", userStateFollow=" + userStateFollow +
                ", follower=" + follower +
                ", urlAvatar='" + urlAvatar + '\'' +
                ", urlCover='" + urlCover + '\'' +
                ", idOfficial='" + idOfficial + '\'' +
                ", nameOfficial='" + nameOfficial + '\'' +
                '}';
    }

    public class OfficialChatInfo {
        private String ofChatName;
        private String ofChatId;
        private String ofChatAvatar;

        public OfficialChatInfo(String ofChatId, String ofChatName, String ofChatAvatar) {
            this.ofChatId = ofChatId;
            this.ofChatName = ofChatName;
            this.ofChatAvatar = ofChatAvatar;
        }

        public String getOfChatName() {
            return ofChatName;
        }

        public String getOfChatId() {
            return ofChatId;
        }

        public String getOfChatAvatar() {
            return ofChatAvatar;
        }
    }
}

package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HaiKE on 8/11/17.
 */

public class NewsHotModel implements Serializable {
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("Cid")
    @Expose
    private Integer cid;
    @SerializedName("Comment")
    @Expose
    private Integer comment;
    @SerializedName("Content")
    @Expose
    private String content;
    @SerializedName("DateAlarm")
    @Expose
    private String dateAlarm;
    @SerializedName("DatePub")
    @Expose
    private String datePub;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("Image169")
    @Expose
    private String image169;
    @SerializedName("Like")
    @Expose
    private Integer like;
    @SerializedName("Media_url")
    @Expose
    private String mediaUrl;
    @SerializedName("Pid")
    @Expose
    private Integer pid;
    @SerializedName("Reads")
    @Expose
    private Integer reads;
    @SerializedName("Shapo")
    @Expose
    private String shapo;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("cateevent")
    @Expose
    private Integer cateevent;
    @SerializedName("idStory")
    @Expose
    private Integer idStory;
    @SerializedName("isRead")
    @Expose
    private Integer isRead;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("typeIcon")
    @Expose
    private Integer typeIcon;

    @SerializedName("_body")
    @Expose
    private ArrayList<NewsHotDetailModel> shortContent = new ArrayList<>();

    @SerializedName("Body")
    @Expose
    private ArrayList<NewsHotDetailModel> body = new ArrayList<>();

    private final static long serialVersionUID = -1901301058722164937L;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateAlarm() {
        return dateAlarm;
    }

    public void setDateAlarm(String dateAlarm) {
        this.dateAlarm = dateAlarm;
    }

    public String getDatePub() {
        return datePub;
    }

    public void setDatePub(String datePub) {
        this.datePub = datePub;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage169() {
        return image169;
    }

    public void setImage169(String image169) {
        this.image169 = image169;
    }

    public Integer getLike() {
        return like;
    }

    public void setLike(Integer like) {
        this.like = like;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getReads() {
        return reads;
    }

    public void setReads(Integer reads) {
        this.reads = reads;
    }

    public String getShapo() {
        return shapo;
    }

    public void setShapo(String shapo) {
        this.shapo = shapo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getCateevent() {
        return cateevent;
    }

    public void setCateevent(Integer cateevent) {
        this.cateevent = cateevent;
    }

    public Integer getIdStory() {
        return idStory;
    }

    public void setIdStory(Integer idStory) {
        this.idStory = idStory;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTypeIcon() {
        return typeIcon;
    }

    public void setTypeIcon(Integer typeIcon) {
        this.typeIcon = typeIcon;
    }

    public ArrayList<NewsHotDetailModel> getBody() {
        return body;
    }

    public void setBody(ArrayList<NewsHotDetailModel> body) {
        this.body = body;
    }

    public ArrayList<NewsHotDetailModel> getShortContent() {
        return shortContent;
    }

    @Override
    public String toString() {
        return "";
    }
}

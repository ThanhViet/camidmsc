package com.metfone.selfcare.database.model.message;

/**
 * Created by thanhnt72 on 8/1/2018.
 */

public class MessageImageDB {

    private long id;
    private String pathOri;
    private String pathNew;
    private String dataResponse;
    private long timeUpload;
    private String ratio;
    private String digit;

    public MessageImageDB(String pathOri, String pathNew, String dataResponse, long timeUpload
            , String ratio, String digit) {
        this.pathOri = pathOri;
        this.pathNew = pathNew;
        this.dataResponse = dataResponse;
        this.timeUpload = timeUpload;
        this.ratio = ratio;
        this.digit = digit;
    }

    public MessageImageDB(long id, String pathOri, String pathNew, String dataResponse, String ratio
            , long timeUpload, String digit) {
        this.id = id;
        this.pathOri = pathOri;
        this.pathNew = pathNew;
        this.dataResponse = dataResponse;
        this.timeUpload = timeUpload;
        this.ratio = ratio;
        this.digit = digit;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPathOri() {
        return pathOri;
    }

    public void setPathOri(String pathOri) {
        this.pathOri = pathOri;
    }

    public String getPathNew() {
        return pathNew;
    }

    public void setPathNew(String pathNew) {
        this.pathNew = pathNew;
    }

    public String getDataResponse() {
        return dataResponse;
    }

    public void setDataResponse(String dataResponse) {
        this.dataResponse = dataResponse;
    }

    public long getTimeUpload() {
        return timeUpload;
    }

    public void setTimeUpload(long timeUpload) {
        this.timeUpload = timeUpload;
    }

    public String getRatio() {
        return ratio;
    }

    public void setRatio(String ratio) {
        this.ratio = ratio;
    }

    public String getDigit() {
        return digit;
    }

    public void setDigit(String digit) {
        this.digit = digit;
    }

    @Override
    public String toString() {
        return "MessageImageDB{" +
                "id=" + id +
                ", pathOri='" + pathOri + '\'' +
                ", pathNew='" + pathNew + '\'' +
                ", dataResponse='" + dataResponse + '\'' +
                ", timeUpload=" + timeUpload +
                ", digit=" + digit +
                ", ratio='" + ratio + '\'' +
                '}';
    }
}

package com.metfone.selfcare.database.model.avno;

/**
 * Created by thanhnt72 on 9/20/2019.
 */

public class AgencyHistory {
    private String id;
    private String name;
    private String msisdn;
    private String unit;
    private String lastAvt;
    private String timeStamp;
    private String amount;

    public AgencyHistory() {
    }

    public AgencyHistory(String id, String name, String msisdn, String amount, String unit, String lastAvt, String timeStamp) {
        this.id = id;
        this.name = name;
        this.msisdn = msisdn;
        this.unit = unit;
        this.lastAvt = lastAvt;
        this.amount = amount;
        this.timeStamp = timeStamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getLastAvt() {
        return lastAvt;
    }

    public void setLastAvt(String lastAvt) {
        this.lastAvt = lastAvt;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}

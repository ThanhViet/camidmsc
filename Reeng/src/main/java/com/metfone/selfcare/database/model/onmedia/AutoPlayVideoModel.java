package com.metfone.selfcare.database.model.onmedia;

import android.graphics.Rect;
import android.view.View;

import com.metfone.selfcare.holder.onmedia.AutoPlayVideoHolder;
import com.metfone.selfcare.ui.autoplay.items.ListItem;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 1/20/2018.
 */

public class AutoPlayVideoModel implements ListItem {
    private static final String TAG = AutoPlayVideoModel.class.getSimpleName();
    private final Rect mCurrentViewRect = new Rect();
    private FeedModelOnMedia onMediaModel;
    private String url;
    private AutoPlayVideoHolder holder;
    private boolean isUserPause = false;
    private int currentDuration = -1;
    private int totalDuration = -1;
    private int videoWidth = -1;
    private int videoHeight = -1;
    private boolean isLogPlay = false;
    private boolean isLog5s = false;
    private boolean isLog30s = false;

    public AutoPlayVideoModel(String url) {
        this.url = url;
    }

    public AutoPlayVideoModel(FeedModelOnMedia model) {
        this.onMediaModel = model;
    }

    public AutoPlayVideoModel() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public FeedModelOnMedia getOnMediaModel() {
        return onMediaModel;
    }

    public void setOnMediaModel(FeedModelOnMedia model) {
        this.onMediaModel = model;
    }

    public boolean isUserPause() {
        return isUserPause;
    }

    public void setUserPause(boolean userPause) {
        isUserPause = userPause;
    }

    public int getCurrentDuration() {
        return currentDuration;
    }

    public void setCurrentDuration(int currentDuration) {
        this.currentDuration = currentDuration;
    }

    public int getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(int totalDuration) {
        this.totalDuration = totalDuration;
    }

    public void setVideoSize(int width, int height) {
        this.videoWidth = width;
        this.videoHeight = height;
    }

    public int getVideoWidth() {
        return videoWidth;
    }

    public int getVideoHeight() {
        return videoHeight;
    }

    public void bindHolder(AutoPlayVideoHolder holder) {
        this.holder = holder;
    }

    public void updateMediaUrl(String mediaUrl) {
        if (onMediaModel != null) {
            onMediaModel.getFeedContent().setMediaUrl(mediaUrl);
        }
        if (holder != null)
            holder.onResponseUrl();
    }

    private void resetStateLog() {
        isLogPlay = false;
        isLog5s = false;
        isLog30s = false;
    }

    public boolean isLogPlay() {
        return isLogPlay;
    }

    public void setLogPlay(boolean logPlay) {
        isLogPlay = logPlay;
    }

    public boolean isLog5s() {
        return isLog5s;
    }

    public void setLog5s(boolean log5s) {
        isLog5s = log5s;
    }

    public boolean isLog30s() {
        return isLog30s;
    }

    public void setLog30s(boolean log30s) {
        isLog30s = log30s;
    }

    @Override
    public int getVisibilityPercents(View view) {
        int percents = 100;
        view.getLocalVisibleRect(mCurrentViewRect);
        int height = view.getHeight();
        if (viewIsPartiallyHiddenTop()) {
            percents = (height - mCurrentViewRect.top) * 100 / height;
        } else if (viewIsPartiallyHiddenBottom(height)) {
            percents = mCurrentViewRect.bottom * 100 / height;
        }
        Log.d(TAG, "<< getVisibilityPercents, percents " + percents);
        return percents;
    }

    @Override
    public void setActive(View newActiveView, int newActiveViewPosition) {
        Log.d(TAG, "setActive ");
        if (holder != null)
            holder.onActive();
    }

    @Override
    public void deactivate(View currentView, int position) {
        Log.d(TAG, "deactivate ");
        if (holder != null)
            holder.onDeActive();
    }

    private boolean viewIsPartiallyHiddenBottom(int height) {
        return mCurrentViewRect.bottom > 0 && mCurrentViewRect.bottom < height;
    }

    private boolean viewIsPartiallyHiddenTop() {
        return mCurrentViewRect.top > 0;
    }
}

package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.constant.ReengMessageConstant.MessageType;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Date;

/**
 * Created by thaodv on 7/12/2014.
 */
public class SuggestShareMusicMessage extends ReengMessage {
    public SuggestShareMusicMessage(ThreadMessage thread, String from, String to, MediaModel mediaModel) {
        super(thread.getThreadType(), null);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setSongId(SONG_ID_DEFAULT_NEW);    // song id
        setSongModel(mediaModel);
        setReadState(ReengMessageConstant.READ_STATE_READ);
        setChatMode(ReengMessageConstant.MODE_IP_IP);
        setStatus(ReengMessageConstant.STATUS_RECEIVED);
        setMessageType(MessageType.suggestShareMusic);
        setDirection(Direction.received);
        Date date = new Date();
        setTime(date.getTime());
    }
}
package com.metfone.selfcare.database.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thaodv on 9/9/2015.
 */
public class StickerCollectionResponse implements Serializable {
    @SerializedName("code")
    int code = -1;
    @SerializedName("stickerCollection")
    ArrayList<StickerCollection> stickerCollection;
    @SerializedName("storeInfo")
    StoreInfo storeInfo;

    public int getCode() {
        return code;
    }

    public ArrayList<StickerCollection> getStickerCollection() {
        return stickerCollection;
    }

    public StoreInfo getStoreInfo() {
        return storeInfo;
    }


}
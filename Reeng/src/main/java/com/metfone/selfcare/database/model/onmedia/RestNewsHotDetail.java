package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.restful.AbsResultData;

import java.io.Serializable;

/**
 * Created by HaiKE on 8/11/17.
 */

public class RestNewsHotDetail extends AbsResultData implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("data")
    private NewsHotModel data;

    public NewsHotModel getData() {
        return data;
    }

    public void setData(NewsHotModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestNewsHotDetail [data=" + data + "] error " + getError();
    }
}

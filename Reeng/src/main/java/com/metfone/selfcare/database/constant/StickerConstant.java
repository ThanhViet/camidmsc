package com.metfone.selfcare.database.constant;

/**
 * Created by thaodv on 09-Feb-15.
 */
public class StickerConstant {
    //constant for collection
    public static final String COLLECTION_TABLE = "STICKER_COLLECTION";
    public static final String COLLECTION_ID = "COLLECTION_ID";
    public static final String COLLECTION_SERVER_ID = "COLLECTION_SERVER_ID";
    public static final String COLLECTION_NUMBER = "COLLECTION_NUMBER";
    public static final String COLLECTION_DOWNLOADED = "COLLECTION_DOWNLOADED";
    public static final String COLLECTION_ICON_PATH = "COLLECTION_ICON_PATH";
    public static final String COLLECTION_NAME = "COLLECTION_NAME";
    public static final String COLLECTION_TYPE = "COLLECTION_TYPE";
    public static final String COLLECTION_IMAGE_PREVIEW = "COLLECTION_IMAGE_PREVIEW";
    public static final String COLLECTION_LAST_STICKY = "COLLECTION_COLUMN_9"; // luu thoi gian download hoac sticky
    public static final String COLLECTION_STATE = "COLLECTION_COLUMN_10"; // luu trang thai enable,disable sticker
    public static final String COLLECTION_IS_NEW = "IS_NEW";
    public static final String COLLECTION_IS_DEFAULT = "COLLECTION_IS_DEFAULT";
    public static final String COLLECTION_LAST_LOCAL_UPDATE = "COLLECTION_LAST_LOCAL_UPDATE";
    public static final String COLLECTION_PREFIX = "COLLECTION_PREFIX";
    public static final String COLLECTION_IS_STICKY = "COLLECTION_IS_STICKY";
    public static final String COLLECTION_LAST_SERVER_UPDATE = "COLLECTION_LAST_SERVER_UPDATE";
    public static final String COLLECTION_LAST_ITEM_UPDATE = "COLLECTION_LAST_ITEM_UPDATE";
    public static final String COLLECTION_ORDER = "COLLECTION_ORDER";
    //------------------------------------------
    // state collection sticker
    public static final int COLLECTION_STATE_ENABLE = 0; //enable
    public static final int COLLECTION_STATE_DISABLE = -1; //state an bo sticker chua down
    public static final int COLLECTION_STATE_DELETED = -2; //state an bo sticker chua down va da down

    public static final String COLLECTION_STICKY = ""; //ko co gi thi la sticky
    public static final String COLLECTION_NOT_STICKY = "collection_not_sticky";

    public static final String DELETE_ALL_COLLECTION_STATEMENT = "DELETE FROM " + COLLECTION_TABLE;

    public static final String CREATE_COLLECTION_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + COLLECTION_TABLE
            + "("
            + COLLECTION_ID + " INTEGER PRIMARY KEY, "
            + COLLECTION_SERVER_ID + " INTEGER, "
            + COLLECTION_NUMBER + " INTEGER, "
            + COLLECTION_DOWNLOADED + " INTEGER, "
            + COLLECTION_ICON_PATH + " TEXT, "
            + COLLECTION_NAME + " TEXT, "
            + COLLECTION_TYPE + " INTEGER, "
            + COLLECTION_IMAGE_PREVIEW + " TEXT, "
            + COLLECTION_LAST_STICKY + " TEXT, "
            + COLLECTION_STATE + " INTEGER, "
            + COLLECTION_IS_NEW + " INTEGER, "
            + COLLECTION_IS_DEFAULT + " INTEGER, "
            + COLLECTION_LAST_LOCAL_UPDATE + " LONG, "
            + COLLECTION_PREFIX + " TEXT, "
            + COLLECTION_IS_STICKY + " INTEGER, "
            + COLLECTION_LAST_SERVER_UPDATE + " LONG, "
            + COLLECTION_LAST_ITEM_UPDATE + " LONG, "
            + COLLECTION_ORDER + " INTEGER"
            + ")";

    public static final String ALTER_COLUMN_IS_NEW = "ALTER TABLE " + COLLECTION_TABLE
            + " ADD COLUMN " + COLLECTION_IS_NEW + " INTEGER" + ";";
    //
    public static final String ALTER_COLUMN_DEFAULT = "ALTER TABLE " + COLLECTION_TABLE
            + " ADD COLUMN " + COLLECTION_IS_DEFAULT + " INTEGER;";
    public static final String ALTER_COLUMN_UPDATE = "ALTER TABLE " + COLLECTION_TABLE
            + " ADD COLUMN " + COLLECTION_LAST_LOCAL_UPDATE + " LONG;";
    public static final String ALTER_COLUMN_PREFIX = "ALTER TABLE " + COLLECTION_TABLE
            + " ADD COLUMN " + COLLECTION_PREFIX + " TEXT" + ";";
    public static final String ALTER_COLUMN_STICKY = "ALTER TABLE " + COLLECTION_TABLE
            + " ADD COLUMN " + COLLECTION_IS_STICKY + " INTEGER" + ";";
    public static final String ALTER_COLUMN_SERVER_UPDATE = "ALTER TABLE " + COLLECTION_TABLE
            + " ADD COLUMN " + COLLECTION_LAST_SERVER_UPDATE + " LONG" + ";";
    public static final String ALTER_COLUMN_ITEM_UPDATE = "ALTER TABLE " + COLLECTION_TABLE
            + " ADD COLUMN " + COLLECTION_LAST_ITEM_UPDATE + " LONG" + ";";
    public static final String ALTER_COLUMN_COLLECTION_ORDER = "ALTER TABLE " + COLLECTION_TABLE
            + " ADD COLUMN " + COLLECTION_ORDER + " INTEGER DEFAULT 0;";
    //
    public static final String DROP_COLLECTION_STATEMENT = "DROP TABLE IF EXISTS " + COLLECTION_TABLE;
    public static final String COLLECTION_SELECT_ALL_QUERY = "SELECT * FROM "
            + COLLECTION_TABLE;
    public static final String COLLECTION_SELLECT_QUERY = "SELECT * FROM " + COLLECTION_TABLE
            + " WHERE " + COLLECTION_SERVER_ID + " = ";

    //constant for item recent old***STICKER_ITEM_TABLE
    public static final String RECENT_TABLE = "sticker_recent_table";
    public static final String RECENT_ID = "id";                                //id cua bang DB
    public static final String RECENT_ITEM_ID = "recent_item_id";               // id cua item sticker
    public static final String RECENT_SERVER_ID = "recent_server_id";           // id server
    public static final String RECENT_COLLECTION_ID = "recent_collection_id";   // id bo sticker
    public static final String RECENT_TIME = "recent_time";
    public static final String CREATE_RECENT_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + RECENT_TABLE
            + "("
            + RECENT_ID + " INTEGER PRIMARY KEY, "
            + RECENT_ITEM_ID + " INTEGER, "
            + RECENT_SERVER_ID + " INTEGER, "
            + RECENT_COLLECTION_ID + " INTEGER, "
            + RECENT_TIME + " INTEGER "
            + ");";
    public static final String DROP_RECENT_STATEMENT = "DROP TABLE IF EXISTS " + RECENT_TABLE;
    public static final String SELECT_RECENT_ALL_QUERY = "SELECT * FROM " + RECENT_TABLE;
    public static final String DELETE_ALL_RECENT_STATEMENT = "DELETE FROM " + RECENT_TABLE;
    // xoa bang item cu
    public static final String DROP_ITEM_STATEMENT = "DROP TABLE IF EXISTS STICKER_ITEM_TABLE";

    ///////////constant for sticker item/////////////////
    public static final String STICKER_TABLE = "sticker_table";
    public static final String STICKER_ID = "sticker_id";
    public static final String STICKER_ITEM_ID = "item_id";
    public static final String STICKER_COLLECTION_ID = "collection_id";
    public static final String STICKER_TYPE = "type";
    public static final String STICKER_IMAGE_PATH = "image_path";
    public static final String STICKER_VOICE_PATH = "voice_path";
    public static final String STICKER_IS_DOWNLOAD_IMAGE = "is_download_image";
    public static final String STICKER_IS_DOWNLOAD_VOICE = "is_download_voice";
    public static final String STICKER_COLUMN_8 = "sticker_column_8";
    public static final String STCIKER_COLUMS_9 = "sticer_column_9";

    public static final String CREATE_STICKER_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + STICKER_TABLE
            + "("
            + STICKER_ID + " INTEGER PRIMARY KEY, "
            + STICKER_ITEM_ID + " INTEGER, "
            + STICKER_COLLECTION_ID + " INTEGER, "
            + STICKER_TYPE + " TEXT, "
            + STICKER_IMAGE_PATH + " TEXT, "
            + STICKER_VOICE_PATH + " TEXT, "
            + STICKER_IS_DOWNLOAD_IMAGE + " INTEGER, "
            + STICKER_IS_DOWNLOAD_VOICE + " INTEGER, "
            + STICKER_COLUMN_8 + " TEXT, "
            + STCIKER_COLUMS_9 + " INTEGER"
            + ")";

    public static final String SELECT_STICKER_ITEM_ALL_QUERY = "SELECT * FROM " + STICKER_TABLE;
    public static final String DELETE_ALL_STICKER_ITEM_STATEMENT = "DELETE FROM " + STICKER_TABLE;
    public static final String DROP_STICKER_ITEM_STATEMENT = "DROP TABLE IF EXISTS " + STICKER_TABLE;
    /*public static final String SELECT_STICKER_ITEM_QUERY = "SELECT * FROM " + STICKER_TABLE + " WHERE "
            + STICKER_ITEM_ID + " = %d " + " AND " + STICKER_COLLECTION_ID + " = %d";
    public static final String WHERE_STICKER_BY_ID = STICKER_ID + " = ?";*/
    public static final String STICKER_TYPE_GIF = "gif";

}
package com.metfone.selfcare.database.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 8/24/2017.
 */

public class StrangerLocation implements Serializable {
    @SerializedName("id")
    private String locationId;
    @SerializedName("name")
    private String name;
    @SerializedName("short_name")
    private String shortName;
    private String nameUnicode;

    private boolean isSelected = false;

    public StrangerLocation() {

    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getNameUnicode() {
        return nameUnicode;
    }

    public void setNameUnicode(String nameUnicode) {
        this.nameUnicode = nameUnicode;
    }
}

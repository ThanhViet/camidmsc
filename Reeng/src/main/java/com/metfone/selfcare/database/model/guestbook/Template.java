package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class Template implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("preview")
    private String preview;
    @SerializedName("name")
    private String name;
    @SerializedName("avatar")
    private String avatar;

    public Template() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}

package com.metfone.selfcare.database.model;

import com.metfone.selfcare.database.model.contact.SocialFriendInfo;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

public class MochaFriendAccountWrapper implements Serializable {

    private CopyOnWriteArrayList<SocialFriendInfo> listOfficerAccounts;

    public MochaFriendAccountWrapper(CopyOnWriteArrayList<SocialFriendInfo> listOfficerAccounts) {
        this.listOfficerAccounts = listOfficerAccounts;
    }

    public CopyOnWriteArrayList<SocialFriendInfo> getListMochaAccounts() {
        return listOfficerAccounts;
    }

    public void setListMochaAccounts(CopyOnWriteArrayList<SocialFriendInfo> listOfficerAccounts) {
        this.listOfficerAccounts = listOfficerAccounts;
    }
}
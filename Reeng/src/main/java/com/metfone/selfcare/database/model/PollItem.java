package com.metfone.selfcare.database.model;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/23/2016.
 */
public class PollItem implements Serializable {
    private String itemId;
    private String title;
    private int totalVoted = 0;
    private ArrayList<String> memberVoted = new ArrayList<>();
    //
    private boolean isSelected = false;

    public PollItem() {

    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc(ApplicationController application) {
        String myNumber = application.getReengAccountBusiness().getJidNumber();
        return application.getContactBusiness().getListNameOfListNumber(getMemberVoted(), myNumber, isSelected());
    }

    public int getTotalVoted() {
        return totalVoted;
    }

    public void setTotalVoted(int totalVoted) {
        this.totalVoted = totalVoted;
    }

    public ArrayList<String> getMemberVoted() {
        return memberVoted;
    }

    public void addMemberVoted(String member) {
        if (memberVoted == null) memberVoted = new ArrayList<>();
        memberVoted.add(member);
    }

    public void setMemberVoted(ArrayList<String> memberVoted) {
        this.memberVoted = memberVoted;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public void setSelectedAndChangeCount(boolean isSelected, String myNumber) {
        this.isSelected = isSelected;
        if (isSelected) {
            memberVoted.add(myNumber);
            totalVoted++;
        } else {
            memberVoted.remove(myNumber);
            totalVoted--;
        }
    }

    public void setJsonObject(JSONObject object) throws JSONException {
        itemId = object.optString(Constants.HTTP.POLL.ID);
        title = object.optString(Constants.HTTP.POLL.POLL_TITLE, "");
        totalVoted = object.optInt(Constants.HTTP.POLL.RESULT, 0);
        setMemberVoted(parserListMember(object));
    }

    private ArrayList<String> parserListMember(JSONObject object) throws JSONException {
        JSONArray array = object.optJSONArray(Constants.HTTP.POLL.VOTERS);
        ArrayList<String> arrayList = new ArrayList<>();
        if (array != null && array.length() > 0) {
            int length = array.length();
            for (int i = 0; i < length; i++) {
                arrayList.add(array.get(i).toString());
            }
        }
        return arrayList;
    }

    public JSONObject toJSonObject() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", itemId);
            jsonObject.put("title", title);
            jsonObject.put("result", totalVoted);
            String poll = new Gson().toJson(memberVoted);
            jsonObject.put("voters", poll);
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Poll item");
        sb.append(",itemId: ").append(itemId);
        sb.append(",title: ").append(title);
        sb.append(",totalVoted: ").append(totalVoted);
        if (memberVoted != null) {
            sb.append(",memberVoted: ").append(memberVoted.toArray().toString());
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PollItem pollItem = (PollItem) o;
        return Utilities.equals(title, pollItem.title);
    }

}

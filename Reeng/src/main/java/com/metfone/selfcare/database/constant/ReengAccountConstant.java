package com.metfone.selfcare.database.constant;

/**
 * Created by ThaoDV on 6/6/14.
 */
public class ReengAccountConstant {
    public static final String TABLE = "my_account";
    public static final String ID = "id";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String NAME = "name";
    public static final String TOKEN = "token";
    public static final String AVATAR_LAST_CHANGE = "avatar_last_change";
    public static final String STATUS = "status";
    public static final String IS_ACTIVE = "is_active"; //acc nay token con active ko
    public static final String GENDER = "gender";
    public static final String BIRTHDAY = "birthday";
    public static final String REGION_CODE = "region_code";
    public static final String BIRTHDAY_STRING = "birthday_string";
    public static final String AVATAR_PATH = "avatar_path";
    public static final String NEED_UPLOAD = "need_upload";
    public static final String PERMISSION = "permission";
    public static final String AVNO_NUMBER = "avno_number";
    public static final String AVNO_IDENTIFY_CARD_FRONT = "ic_front";
    public static final String AVNO_IDENTIFY_CARD_BACK = "ic_back";
    public static final String PREKEY = "prekey";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String CURRENT_ADDRESS = "current_address";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE +
            " (" + ID + " INTEGER PRIMARY KEY, " +
            IS_ACTIVE + " INTEGER, " +
            PHONE_NUMBER + " TEXT, " +
            NAME + " TEXT, " +
            TOKEN + " TEXT, " +
            AVATAR_LAST_CHANGE + " TEXT, " +
            STATUS + " TEXT, " +
            GENDER + " INTEGER, " +
            BIRTHDAY + " TEXT, " +
            REGION_CODE + " TEXT, " +
            BIRTHDAY_STRING + " TEXT, " +
            AVATAR_PATH + " TEXT, " +
            NEED_UPLOAD + " INTEGER, " +
            PERMISSION + " INTEGER, " +
            AVNO_NUMBER + " TEXT, " +
            AVNO_IDENTIFY_CARD_FRONT + " TEXT, " +
            AVNO_IDENTIFY_CARD_BACK + " TEXT, " +
            PREKEY + " TEXT, " +
            EMAIL + " TEXT, " +
            ADDRESS + " TEXT, " +
            CURRENT_ADDRESS + " TEXT" +
            ")";
    public static final String DROP_STATEMENT = "DROP TABLE " + TABLE;
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;

    public static final String ALTER_COLUMN_REGION_CODE = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + REGION_CODE + " TEXT;";

    public static final String ALTER_COLUMN_BIRTHDAY_STRING = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + BIRTHDAY_STRING + " TEXT;";

    public static final String ALTER_COLUMN_AVATAR_PATH = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + AVATAR_PATH + " TEXT;";

    public static final String ALTER_COLUMN_NEED_UPLOAD = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + NEED_UPLOAD + " INTEGER;";

    public static final String ALTER_COLUMN_PERMISSION = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + PERMISSION + " INTEGER DEFAULT -1;";
    public static final String ALTER_COLUMN_AVNO_NUMBER = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + AVNO_NUMBER + " TEXT;";

    public static final String ALTER_COLUMN_AVNO_IC_FRONT = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + AVNO_IDENTIFY_CARD_FRONT + " TEXT;";

    public static final String ALTER_COLUMN_AVNO_IC_BACK = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + AVNO_IDENTIFY_CARD_BACK + " TEXT;";

    public static final String ALTER_COLUMN_PREKEY = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + PREKEY + " TEXT;";
}

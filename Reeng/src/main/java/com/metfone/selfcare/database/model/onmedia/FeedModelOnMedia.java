package com.metfone.selfcare.database.model.onmedia;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/22/2015.
 */
public class FeedModelOnMedia implements Serializable, Comparable<FeedModelOnMedia> {

    private static final String TAG = FeedModelOnMedia.class.getSimpleName();

    @SerializedName("userInfo")
    private UserInfo userInfo;

    @SerializedName("status")
    private String userStatus = "";

    @SerializedName("base64RowID")
    private String base64RowId = "";

    @SerializedName("stamp")
    private long timeStamp;

    @SerializedName("current_time")
    private long timeServer;

    @SerializedName("content")
    private FeedContent feedContent;

    @SerializedName("action_type")
    private ActionLogApp actionType = ActionLogApp.UNKNOW;

    @SerializedName("isLike")
    private int isLike;

    @SerializedName("isShare")
    private int isShare;

    @SerializedName("lstComment")
    private ArrayList<FeedAction> listComment;

    @SerializedName("tags")
    private ArrayList<TagMocha> listTag = new ArrayList<>();

    private ArrayList<UserInfo> listSuggestFriend = new ArrayList<>();

    public FeedModelOnMedia() {
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public FeedContent getFeedContent() {
        return feedContent;
    }

    public void setFeedContent(FeedContent feedContent) {
        this.feedContent = feedContent;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public ActionLogApp getActionType() {
        return actionType;
    }

    public int getIsLike() {
        return isLike;
    }

    public void setIsLike(int isLike) {
        this.isLike = isLike;
    }

    public int getIsShare() {
        return isShare;
    }

    public void setIsShare(int isShare) {
        this.isShare = isShare;
    }

    public void setBase64RowId(String base64RowId) {
        this.base64RowId = base64RowId;
    }

    public String getBase64RowId() {
        return base64RowId;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public void setTimeServer(long timeServer) {
        this.timeServer = timeServer;
    }

    public void setActionType(ActionLogApp actionType) {
        this.actionType = actionType;
    }

    public ArrayList<FeedAction> getListComment() {
        return listComment;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public long getTimeServer() {
        return timeServer;
    }

    public ArrayList<TagMocha> getListTag() {
        return listTag;
    }

    public void setListTag(ArrayList<TagMocha> listTag) {
        this.listTag = listTag;
    }

    public ArrayList<UserInfo> getListSuggestFriend() {
        return listSuggestFriend;
    }

    public void setListSuggestFriend(ArrayList<UserInfo> listSuggestFriend) {
        this.listSuggestFriend = listSuggestFriend;
    }

    public boolean isLike() {
        return isLike == 1;
    }

    public boolean isShare() {
        return isShare == 1;
    }

    @Override
    public int compareTo(FeedModelOnMedia feedModelOnMedia) {
        return Long.valueOf(feedModelOnMedia.getTimeStamp()).compareTo(this.timeStamp);
    }

    public enum ActionLogApp {
        @SerializedName("LIKE")
        LIKE,
        @SerializedName("UNLIKE")
        UNLIKE,
        @SerializedName("SHARE")
        SHARE,
        @SerializedName("COMMENT")
        COMMENT,
        @SerializedName("POST")
        POST,
        @SerializedName("EDIT")
        EDIT,
        @SerializedName("DELETE")
        DELETE,
        @SerializedName("FOLLOW")
        FOLLOW,
        UNKNOW
    }

    public enum ActionFrom {
        mochavideo,
        onmedia
    }

    @Override
    public String toString() {
        return "FeedModelOnMedia{" +
                "userInfo=" + userInfo +
                ", feedContent=" + feedContent +
                ", actionType=" + actionType +
                ", userStatus='" + userStatus + '\'' +
                ", isLike=" + isLike +
                ", isShare=" + isShare +
                '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        if (feedContent != null) {
            result = prime * result
                    + ((TextUtils.isEmpty(feedContent.getUrl())) ? 0 : feedContent.getUrl().hashCode());
        } else {
            result = 0;
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (FeedModelOnMedia.class != obj.getClass()) {
            return false;
        }
        FeedModelOnMedia otherObject = (FeedModelOnMedia) obj;
        if (otherObject.getFeedContent() == null) return false;
        String otherUrl = otherObject.getFeedContent().getUrl();
        if (TextUtils.isEmpty(otherUrl)) return false;
        /*String otherRowId = otherObject.getBase64RowId();
        if(feedContent.getUrl().equals(otherUrl) && this.getBase64RowId().equals(otherRowId)){
            return true;
        }
        return false; */
        return otherUrl.equals(feedContent.getUrl());
    }

    public static FeedModelOnMedia convertVideoToFeedModelOnMedia(Video video) {
        FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
        feedModelOnMedia.setIsLike(video.isLike() ? 1 : 0);
        feedModelOnMedia.setFeedContent(FeedContent.convertVideoToFeedContent(video));
        feedModelOnMedia.setIsLike(video.isLike() ? 1 : 0);
        feedModelOnMedia.setIsShare(video.isShare() ? 1 : 0);
        return feedModelOnMedia;
    }

    public static FeedModelOnMedia convertMediaToFeedModelOnMedia(MediaModel model) {
        FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
        feedModelOnMedia.setFeedContent(FeedContent.convertMediaToFeedContent(model));
        feedModelOnMedia.setIsLike(model.isLike() ? 1 : 0);
        feedModelOnMedia.setIsShare(model.isShare() ? 1 : 0);
        return feedModelOnMedia;
    }

    public static FeedModelOnMedia convertMovieToFeedModelOnMedia(Movie model) {
        FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
        feedModelOnMedia.setFeedContent(FeedContent.convertMovieToFeedContent(model));
        feedModelOnMedia.setIsLike(model.isLike() ? 1 : 0);
        feedModelOnMedia.setIsShare(model.isShare() ? 1 : 0);
        return feedModelOnMedia;
    }

    public static FeedModelOnMedia convertMediaToFeedModelOnMedia(AllModel model) {
        FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
        feedModelOnMedia.setFeedContent(FeedContent.convertMediaToFeedContent(model));
        feedModelOnMedia.setIsLike(0);
        feedModelOnMedia.setIsShare(0);
        return feedModelOnMedia;
    }

    public static FeedModelOnMedia convertNewsToFeedModelOnMedia(NewsModel model) {
        FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
        feedModelOnMedia.setFeedContent(FeedContent.convertNewsToFeedContent(model));
        feedModelOnMedia.setIsLike(0);
        feedModelOnMedia.setIsShare(0);
        return feedModelOnMedia;
    }

    public static FeedModelOnMedia convertTiinToFeedModelOnMedia(TiinModel model) {
        FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
        feedModelOnMedia.setFeedContent(FeedContent.convertTiinToFeedContent(model));
        feedModelOnMedia.setIsLike(0);
        feedModelOnMedia.setIsShare(0);
        return feedModelOnMedia;
    }
}

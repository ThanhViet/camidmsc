package com.metfone.selfcare.database.constant;

/**
 * Created by toanvk2 on 2/26/2015.
 */
public class EventMessageConstant {
    public static final String EVENT_TABLE = "event_message";
    public static final String EVENT_ID = "id";
    public static final String EVENT_MESSAGE_ID = "mesage_id";
    public static final String EVENT_SENDER = "sender";
    public static final String EVENT_TIME = "time";
    public static final String EVENT_STATE = "state";
    public static final String EVENT_PACKET_ID = "packet_id";
    public static final String EVENT_THREAD_ID = "thread_id";
    public static final String EVENT_REACTION = "reaction";


    public static final int EVENT_REACTION_STATE = -1;

    //----------------create table--------------------//
    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + EVENT_TABLE
            + "("
            + EVENT_ID
            + " INTEGER PRIMARY KEY, "
            + EVENT_MESSAGE_ID + " INTEGER, "
            + EVENT_SENDER + " TEXT, "
            + EVENT_TIME + " INTEGER, "
            + EVENT_STATE + " INTEGER, "
            + EVENT_PACKET_ID + " TEXT, "
            + EVENT_THREAD_ID + " INTEGER, "
            + EVENT_REACTION + " INTEGER"
            + ");";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + EVENT_TABLE;

    public static final String SELECT_BY_MESSAGE_ID_QUERY = "SELECT * FROM "
            + EVENT_TABLE + " WHERE " + EVENT_MESSAGE_ID + " = ";

    public static final String SELECT_SENDER_BY_MESSAGE_ID_QUERY = "SELECT " + EVENT_SENDER + " FROM "
            + EVENT_TABLE + " WHERE " + EVENT_MESSAGE_ID + " = ";

    public static final String SELECT_REACTION_BY_MESSAGE_ID_QUERY = "SELECT " + EVENT_REACTION + " FROM "
            + EVENT_TABLE + " WHERE " + EVENT_MESSAGE_ID + " = ";

    public static final String SELECT_REACTION_EVENT = "SELECT * FROM " + EVENT_TABLE + " WHERE "
            + EVENT_MESSAGE_ID + " = ?" + " AND "
            + EVENT_PACKET_ID + " = ?" + " AND "
            + EVENT_SENDER + " = ?" + " AND "
            + EVENT_STATE + " = ?";

    public static final String ALTER_COLUMN_REACTION = "ALTER TABLE " + EVENT_TABLE
            + " ADD COLUMN " + EVENT_REACTION + " INTEGER;";


    public static final String SELECT_AND_PACKET_ID_QUERY = " AND " + EVENT_PACKET_ID + " LIKE ";

    public static final String DELETE_ALL_STATEMENT = "DELETE FROM "
            + EVENT_TABLE;
    public static final String WHERE_ID = EVENT_ID + " = ?";
    public static final String WHERE_MESSAGE_ID = EVENT_MESSAGE_ID + " = ?";
}

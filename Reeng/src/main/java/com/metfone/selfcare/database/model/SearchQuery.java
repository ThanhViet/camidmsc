package com.metfone.selfcare.database.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class SearchQuery {
    public static final String TYPE_SINGER = "singer";
    public static final String TYPE_ALBUM = "album";
    public static final String TYPE_SONG = "song";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_PLAYLIST = "playlist";

    private static final long serialVersionUID = 1L;

    @SerializedName("id")
    private long id;

    @SerializedName("full_name")
    private String fullName = "";

    @SerializedName("type")
    private String type = "";

    @SerializedName("image")
    private String image = "";

    @SerializedName("score")
    private float score;

    @SerializedName("search_info")
    private String searchInfo = "";

    @SerializedName("listen_no")
    private long listenNo;

    @SerializedName("full_singer")
    private String fullSinger = "";

    @SerializedName("is_singer")
    private int isSinger;

    @SerializedName("identify")
    private String identify;

    public SearchQuery() {
        super();
    }

    public SearchQuery(String text) {
        super();
        this.fullName = text;
    }

    @Override
    public String toString() {
        return "SearchModel [id=" + id + ", listen_no=" + listenNo + ", score=" + score + ", type=" + type + ", " +
                "fullName=" + fullName + ", fullSinger=" + fullSinger + ", isSinger=" + isSinger + "]";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    public long getListenNo() {
        return listenNo;
    }

    public void setListenNo(long listenNo) {
        this.listenNo = listenNo;
    }

    public String getFullSinger() {
        return fullSinger;
    }

    public void setFullSinger(String fullSinger) {
        this.fullSinger = fullSinger;
    }

    public int getIsSinger() {
        return isSinger;
    }

    public void setIsSinger(int isSinger) {
        this.isSinger = isSinger;
    }

    public int getType() {
        if (type.equals(TYPE_SINGER)) {
            return MediaModel.TYPE_SINGER;
        } else if (type.equals(TYPE_ALBUM)) {
            return MediaModel.TYPE_ALBUM;
        } else if (type.equals(TYPE_VIDEO)) {
            return MediaModel.TYPE_VIDEO;
        } else if (type.equals(TYPE_SONG)) {
            return MediaModel.TYPE_BAI_HAT;
        } else if (type.equals(TYPE_PLAYLIST)) {
            return MediaModel.TYPE_PLAYLIST;
        }
        return MediaModel.TYPE_FAKE_SOMETHING;
    }

    public String getKeyCoincide() {
        String text = getFullName();
        if (!TextUtils.isEmpty(text)) {
            if (!TextUtils.isEmpty(getFullSinger()) && !text.contains(getFullSinger()))
                text += " + " + getFullSinger();
        } else
            text = getFullSinger();

        String result = text;
        if (!TextUtils.isEmpty(text)) {
            result = text.substring(0, 1).toUpperCase() + text.substring(1);
            result = result.trim();
        }

        return result;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }
}
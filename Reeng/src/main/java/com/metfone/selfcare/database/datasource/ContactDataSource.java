package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ContactConstant;
import com.metfone.selfcare.database.model.Contact;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 6/26/14.
 */
public class ContactDataSource {
    // Database fields
    private static String TAG = ContactDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static ContactDataSource mInstance;

    public static synchronized ContactDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new ContactDataSource(application);
        }
        return mInstance;
    }

    private ContactDataSource(ApplicationController application) {
        databaseRead = ReengSQLiteHelper.getMyReadableDatabase();
        databaseWrite = ReengSQLiteHelper.getMyWritableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    // set values
    private ContentValues setValues(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(ContactConstant.ID, contact.getContactId());
        values.put(ContactConstant.NAME, contact.getName());
        values.put(ContactConstant.FAVORITE, contact.getFavorite());
        contact.setNameUnicode(TextHelper.getInstant().convertUnicodeToAscci(contact.getName()));
        values.put(ContactConstant.NAME_UNICODE, contact.getNameUnicode());
        return values;
    }

    // insert item
    public void insertContact(Contact contact) {
        try {
            ContentValues values = setValues(contact);
            databaseWrite.insert(ContactConstant.TABLE, null, values);
        } catch (Exception e) {
            Log.e(TAG, "insertContact", e);
        }
    }

    // insert list item
    public void insertListContact(ArrayList<Contact> listContacts) {
        if (listContacts == null || listContacts.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (Contact contact : listContacts) {
                    ContentValues values = setValues(contact);
                    databaseWrite.insert(ContactConstant.TABLE, null, values);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "insertListContact", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "insertListContact", e);
        }
    }

    // get contact from cursor
    private Contact getContactFromCursor(Cursor cur) {
        Contact contact = new Contact();
        contact.setContactID(cur.getString(0));
        contact.setName(cur.getString(1));
        contact.setFavorite(cur.getInt(2));
        contact.setNameUnicode(cur.getString(3));
        return contact;
    }

    // get contact from contact id
    public Contact getContactFromId(String contactId) {
        contactId = contactId.trim();
        Contact contact = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            String query = ContactConstant.SELECT_BY_CONTACT_ID_STATEMENT
                    + contactId;
            cursor = databaseRead.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    contact = getContactFromCursor(cursor);
//                    contact.setListNumbers(getPhoneNumberFromContactId(contactId));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getContactFromId", e);
        } finally {
            closeCursor(cursor);
        }
        return contact;
    }

    // get all contact
    public ArrayList<Contact> getAllContact() {
        ArrayList<Contact> listContacts = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(ContactConstant.SELECT_ALL_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                listContacts = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        //Contact contact = getContactFromCursor(cursor);
                        // set phone number
                        listContacts.add(getContactFromCursor(cursor));
                    } while (cursor.moveToNext());
                }
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "getAllContact", e);
        } catch (Exception e) {
            Log.e(TAG, "getAllContact", e);
        } finally {
            closeCursor(cursor);
        }
        return listContacts;
    }

    // update contact
    public void updateContact(Contact contact) {
        try {
            ContentValues values = setValues(contact);
            databaseWrite.update(ContactConstant.TABLE, values, ContactConstant.WHERE_ID,
                    new String[]{contact.getContactId()});
        } catch (Exception e) {
            Log.e(TAG, "updateContact", e);
        }
    }

    // update list item
    public void updateListContact(ArrayList<Contact> listContacts) {
        if (listContacts == null || listContacts.isEmpty()) {
            return;
        }
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.beginTransaction();
            try {
                for (Contact contact : listContacts) {
                    ContentValues values = setValues(contact);
                    databaseWrite.update(ContactConstant.TABLE, values, ContactConstant.WHERE_ID,
                            new String[]{contact.getContactId()});
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction updateListContact", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "updateListContact", e);
        }
    }

    // del contact by id
    public void deleteContactFromContactID(String contactId) {
        try {
            databaseWrite.delete(ContactConstant.TABLE, ContactConstant.WHERE_ID, new String[]{contactId});
        } catch (Exception e) {
            Log.e(TAG, "deleteContactFromContactID", e);
        }
    }

    //del contact by obj
    public void deleteContact(String contactId) {
        try {
            databaseWrite.delete(ContactConstant.TABLE, ContactConstant.WHERE_ID,
                    new String[]{contactId});
        } catch (Exception e) {
            Log.e(TAG, "deleteContact", e);
        }
    }

    // del list item
    public void deleteListContact(ArrayList<Contact> listContacts) {
        if (listContacts == null || listContacts.size() <= 0) {
            return;
        }
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.beginTransaction();
            try {
                String whereClause;
                for (Contact contact : listContacts) {
                    whereClause = ContactConstant.ID + " = " + contact.getContactId();
                    databaseWrite.delete(ContactConstant.TABLE, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteListContact", e);
        }
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(ContactConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }

    public void dropTable() {
        try {
            databaseWrite.execSQL(ContactConstant.DROP_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "dropTable", e);
        }
    }
}
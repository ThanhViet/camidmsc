package com.metfone.selfcare.database.model.avno;

public class NumberAVNO {

    private String number;
    private String price;
    private String alert;


    public NumberAVNO(String number, String price, String alert) {
        this.number = number;
        this.price = price;
        this.alert = alert;
    }

    public String getNumber() {
        return number;
    }

    public String getPrice() {
        return price;
    }

    public String getAlert() {
        return alert;
    }
}

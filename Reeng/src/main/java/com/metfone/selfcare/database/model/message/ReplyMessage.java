package com.metfone.selfcare.database.model.message;

import android.text.TextUtils;

import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by toanvk2 on 5/30/2016.
 */
public class ReplyMessage implements Serializable {
    private static final String TAG = ReplyMessage.class.getSimpleName();
    private String member;
    private ReengMessagePacket.SubType subType;
    private String body;
    private String imgLink;
    private boolean expandedContent;
    private String filePath;
    private String msgId;

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public ReengMessagePacket.SubType getSubType() {
        return subType;
    }

    public void setSubType(ReengMessagePacket.SubType subType) {
        this.subType = subType;
    }

    public void setSubTypeStr(String subTypeStr) {
        this.subType = ReengMessagePacket.SubType.fromString(subTypeStr);
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }

    public boolean isExpandedContent() {
        return expandedContent;
    }

    public void setExpandedContent(boolean expandedContent) {
        this.expandedContent = expandedContent;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public boolean isValidReplyText() {
        if (TextUtils.isEmpty(member) || TextUtils.isEmpty(body)) {
            return false;
        }
        return true;
    }

    public boolean isValidReplyImage() {
        if (TextUtils.isEmpty(member) || TextUtils.isEmpty(imgLink)) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ReplyMessage");
        sb.append(" member: ").append(member);
        sb.append(" subType: ").append(subType);
        sb.append(" body: ").append(body);
        sb.append(" imglink: ").append(imgLink);
        sb.append(" msgId: ").append(msgId);
        return sb.toString();
    }

    public void fromJson(String json) {
        try {
            JSONObject object = new JSONObject(json);
            setSubTypeStr(object.optString("subtype"));
            setMember(object.optString("member", null));
            setBody(object.optString("body", null));
            setImgLink(object.optString("link", null));
            setFilePath(object.optString("filePath", null));
            setMsgId(object.optString("msgId", null));
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public String toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("subtype", subType.toString());
            obj.put("member", member);
            obj.put("body", body);
            obj.put("link", imgLink);
            obj.put("filePath", filePath);
            obj.put("msgId", msgId);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
        return obj.toString();
    }

    public String toJsonWithoutFilePath() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("subtype", subType.toString());
            obj.put("member", member);
            obj.put("body", body);
            obj.put("link", imgLink);
            obj.put("msgId", msgId);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
        return obj.toString();
    }
}
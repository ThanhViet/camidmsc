package com.metfone.selfcare.database.constant;

/**
 * Created by toanvk2 on 7/10/14.
 */
public class BlockConstant {
    public static final String TABLE = "block_number";
    public static final String NUMBER = "number";
    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE + " (" + NUMBER + " TEXT)";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String SELECT_NUMBER_STATEMENT = "SELECT * FROM " + TABLE + " WHERE " + NUMBER + " =?";
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;
}

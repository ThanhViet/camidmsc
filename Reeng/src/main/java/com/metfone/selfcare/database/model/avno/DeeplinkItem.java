package com.metfone.selfcare.database.model.avno;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 11/3/2018.
 */

public class DeeplinkItem implements Serializable{
    private String label;
    private String url;
    private int stateButton;
    private String nextLabel;

    public DeeplinkItem(String label, String url, int stateButton) {
        this.label = label;
        this.url = url;
        this.stateButton = stateButton;
    }

    public String getLabel() {
        return label;
    }

    public String getUrl() {
        return url;
    }

    public int getStateButton() {
        return stateButton;
    }

    public String getNextLabel() {
        return nextLabel;
    }

    public void setNextLabel(String nextLabel) {
        this.nextLabel = nextLabel;
    }
}

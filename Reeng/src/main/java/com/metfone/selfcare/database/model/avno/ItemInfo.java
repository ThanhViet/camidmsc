package com.metfone.selfcare.database.model.avno;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 7/26/2018.
 */

public class ItemInfo implements Serializable{

    private String title;
    private String desc;
    private String deeplinkLabel;
    private String deeplink;
    private boolean isLastItem;


    public ItemInfo(String title, String desc, String deeplinkLabel, String deeplink) {
        this.title = title;
        this.desc = desc;
        this.deeplinkLabel = deeplinkLabel;
        this.deeplink = deeplink;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getDeeplinkLabel() {
        return deeplinkLabel;
    }

    public String getDeeplink() {
        return deeplink;
    }

    public void setLastItem(boolean lastItem) {
        isLastItem = lastItem;
    }

    public boolean isLastItem() {
        return isLastItem;
    }
}

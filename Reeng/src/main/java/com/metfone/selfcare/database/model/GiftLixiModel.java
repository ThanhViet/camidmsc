package com.metfone.selfcare.database.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 1/24/2018.
 */

public class GiftLixiModel implements Serializable {

    private String messageContent;
    private long amountMoney;
    private String orderId; //gui len server bankplus
    private String requestId;// server bankplus tra ve
    private ArrayList<String> listMember;
    private int splitRandom = -1;

    public GiftLixiModel(String messageContent, long amountMoney, String orderId, ArrayList<String> listMember,
                         int splitRandom) {
        this.messageContent = messageContent;
        this.amountMoney = amountMoney;
        this.orderId = orderId;
        this.listMember = listMember;
        this.splitRandom = splitRandom;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public long getAmountMoney() {
        return amountMoney;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public ArrayList<String> getListMember() {
        return listMember;
    }

    public int getSplitRandom() {
        return splitRandom;
    }
}

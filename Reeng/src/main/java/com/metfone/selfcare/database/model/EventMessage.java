package com.metfone.selfcare.database.model;

import java.io.Serializable;

/**
 * Created by toanvk2 on 2/26/2015.
 */
public class EventMessage implements Serializable {
    private static final String TAG = EventMessage.class.getSimpleName();
    private long id;
    private long messageId;
    private String sender;
    private long time;
    private int status;
    private String packetId;
    private int threadId;
    private int reaction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPacketId() {
        return packetId;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public int getThreadId() {
        return threadId;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    public int getReaction() {
        return reaction;
    }

    public void setReaction(int reaction) {
        this.reaction = reaction;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((sender == null) ? 0 : sender.hashCode()) + status;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (EventMessage.class != obj.getClass()) {
            return false;
        }
        EventMessage other = (EventMessage) obj;
        String otherSender = other.getSender();
        int otherStatus = other.getStatus();
        // so sanh status
        if (otherStatus == status) {
            // so sanh sender va status
            // neu cung status, cung sender, khong quan tam thoi gian, coi la 2 obj trung
            return otherSender != null && sender != null && otherSender.equals(sender);
        } else {
            return false;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(TAG);
        sb.append(" id: ").append(id).append("\n");
        sb.append(" message_id: ").append(messageId).append("\n");
        sb.append(" sender: ").append(sender).append("\n");
        sb.append(" time: ").append(time).append("\n");
        sb.append(" status: ").append(status).append("\n");
        sb.append(" packetId: ").append(packetId).append("\n");
        sb.append(" threadId: ").append(threadId).append("\n");
        sb.append(" reaction: ").append(reaction).append("\n");
        return sb.toString();
    }
}

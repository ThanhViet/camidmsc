package com.metfone.selfcare.database.model.call;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 7/12/2018.
 */

public class CallSubsConfirm implements Serializable {

    @SerializedName("label")
    private String label;

    @SerializedName("deeplink")
    private String deeplink;

    @SerializedName("description")
    private String description;


    public String getLabel() {
        return label;
    }

    public String getDeeplink() {
        return deeplink;
    }

    public String getDescription() {
        return description;
    }
}

package com.metfone.selfcare.database.model.call;

/**
 * Created by toanvk2 on 10/12/2016.
 */

public class CallHistoryDetail {
    private long id;
    private long callId;
    private String friendNumber;
    private String ownerNumber;
    private int callOut;
    private int callState;
    private int callDirection;// send or received
    private long callTime;
    private int duration;

    public CallHistoryDetail() {

    }

    public CallHistoryDetail(String friendNumber, String ownerNumber, int state, int outState, int direction, long time, int duration) {
        this.friendNumber = friendNumber;
        this.ownerNumber = ownerNumber;
        this.callState = state;
        this.callOut = outState;
        this.callDirection = direction;
        this.callTime = time;
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCallId() {
        return callId;
    }

    public void setCallId(long callId) {
        this.callId = callId;
    }

    public String getFriendNumber() {
        return friendNumber;
    }

    public void setFriendNumber(String friendNumber) {
        this.friendNumber = friendNumber;
    }

    public String getOwnerNumber() {
        return ownerNumber;
    }

    public void setOwnerNumber(String ownerNumber) {
        this.ownerNumber = ownerNumber;
    }

    public int getCallType() {
        return callOut;
    }

    public void setCallType(int callOut) {
        this.callOut = callOut;
    }

    public int getCallState() {
        return callState;
    }

    public void setCallState(int callState) {
        this.callState = callState;
    }

    public int getCallDirection() {
        return callDirection;
    }

    public void setCallDirection(int callDirection) {
        this.callDirection = callDirection;
    }

    public long getCallTime() {
        return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CallHistoryDetail");
        sb.append(" id: ").append(id);
        sb.append(" callId: ").append(callId);
        sb.append(" friend: ").append(friendNumber);
        sb.append(" owner: ").append(ownerNumber);
        sb.append(" callOut: ").append(callOut);
        sb.append(" callState: ").append(callState);
        sb.append(" callDirection: ").append(callDirection);
        sb.append(" callTime: ").append(callTime);
        sb.append(" duration: ").append(duration);
        return sb.toString();
    }
}

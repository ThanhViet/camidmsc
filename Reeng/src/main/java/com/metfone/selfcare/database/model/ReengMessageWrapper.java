package com.metfone.selfcare.database.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by THANHNT72 on 5/5/2015.
 */
public class ReengMessageWrapper implements Serializable {

    private ArrayList<ReengMessage> reengMessages;

    public ReengMessageWrapper(ArrayList<ReengMessage> data) {
        this.reengMessages = data;
    }

    public ArrayList<ReengMessage> getReengMessages() {
        return this.reengMessages;
    }
}

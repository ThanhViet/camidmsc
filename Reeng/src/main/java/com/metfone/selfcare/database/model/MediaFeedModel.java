package com.metfone.selfcare.database.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by gianglv3 on 3/19/2015.
 */
public class MediaFeedModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @SerializedName("album_url")
    private String album_url;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("description")
    private String description;

    @SerializedName("id")
    private String id;

    @SerializedName("image_url")
    private String image;

    @SerializedName("item_id")
    private String item_id;

    @SerializedName("item_type")
    private int item_type = 0;

    @SerializedName("media_url")
    private String media_url;

    @SerializedName("singer")
    private String singer;

    @SerializedName("site")
    private String site;

    @SerializedName("title")
    private String title;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("url")
    private String url;

    public String getAlbum_url() {
        return album_url;
    }

    public void setAlbum_url(String album_url) {
        this.album_url = album_url;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public int getItem_type() {
        return item_type;
    }

    public void setItem_type(int item_type) {
        this.item_type = item_type;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MediaModel getMediaMode() {
        MediaModel mediaModel = new MediaModel();
        mediaModel.setMedia_url(getMedia_url());
        mediaModel.setSinger(getSinger());
        mediaModel.setId(getItem_id());
        mediaModel.setUrl(getUrl());
        mediaModel.setName(getTitle());
        mediaModel.setType(getItem_type());

        return mediaModel;
    }


}

package com.metfone.selfcare.database.model;

import androidx.annotation.NonNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by thanhnt72 on 4/3/2018.
 */

public class ThreadGroup extends ThreadMessage implements Comparable<ThreadGroup> {


    private DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String lastActive;

    public ThreadGroup() {
    }

    public String getLastActive() {
        return lastActive;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    @Override
    public int compareTo(@NonNull ThreadGroup threadGroup) {
        try {
            return f.parse(getLastActive()).compareTo(f.parse(threadGroup.getLastActive()));
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

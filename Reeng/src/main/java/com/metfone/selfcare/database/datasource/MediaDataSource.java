package com.metfone.selfcare.database.datasource;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.MediaConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 25/03/2015.
 */
public class MediaDataSource {
    private static final String TAG = MediaDataSource.class.getSimpleName();
    // Database fields
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static MediaDataSource mInstance;

    public static synchronized MediaDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new MediaDataSource(application);
        }
        return mInstance;
    }

    private MediaDataSource(ApplicationController application) {
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    /**
     * dong con tro
     *
     * @param cursor
     */
    private void closeCursor(Cursor cursor) {
        if (cursor != null)
            cursor.close();
    }

    /**
     * get MediaModel from cursor
     *
     * @param cur
     * @return MediaModel
     */
    private MediaModel getMediaFromCursor(Cursor cur) {
        MediaModel item = new MediaModel();
        item.setId(cur.getString(0));
        item.setName(cur.getString(1));
        item.setSinger(cur.getString(2));
        item.setImage(cur.getString(3));
        item.setMedia_url(cur.getString(4));
        item.setUrl(cur.getString(5));
        item.setType(cur.getInt(6));
        item.setCrbtCode(cur.getString(7));
        item.setCrbtPrice(cur.getString(8));
        return item;
    }

    /**
     * get MediaModel from songId
     *
     * @param id
     * @return MediaModel
     */
    public MediaModel getMediaFromSongId(String id) {
        MediaModel mediaModel = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null)
                return null;
            String query = MediaConstant.SELECT_MEDIA_BY_ID_STATEMENT
                    + id;
            cursor = databaseRead.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    mediaModel = getMediaFromCursor(cursor);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return mediaModel;
    }

    /**
     * get all media from db
     *
     * @return listMediaModel
     */
    public ArrayList<MediaModel> getAllMediaFromDb() {
        ArrayList<MediaModel> listMedia = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return listMedia;
            }
            cursor = databaseRead.rawQuery(MediaConstant.SELECT_ALL_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                listMedia = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        MediaModel item = getMediaFromCursor(cursor);
                        // set phone number
                        listMedia.add(item);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return listMedia;
    }

    /**
     * xoa het media
     */
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(MediaConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * xoa bang media
     */
    public void dropTable() {
        try {
            databaseWrite.execSQL(MediaConstant.DROP_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }
}
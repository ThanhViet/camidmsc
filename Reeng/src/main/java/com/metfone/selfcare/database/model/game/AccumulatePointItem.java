package com.metfone.selfcare.database.model.game;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 11/30/2016.
 */
public class AccumulatePointItem implements Serializable {

    public static final int GIFT_TYPE_CASH = 1;
    public static final int GIFT_TYPE_CARD = 2;
    public static final int GIFT_TYPE_DEEPLINK = 3;
    public static final int GIFT_TYPE_VTPLUS = 4;

    public static final int ITEM_CPOINT = 0;
    public static final int ITEM_SPOINT = 1;
    public static final int ITEM_MPOINT = 2;
    public static final int ITEM_VTPLUS = 3;

    public static final int TYPE_CONVERT_POINT_VTPLUS = 1;


    public static final String MISSION_TYPE = "INSTALLAPP";
    public static final String MISSION_STATUS_NEW = "new";
    public static final String MISSION_STATUS_REGISTED = "registed";

    @SerializedName("id")
    private long id;
    @SerializedName("title")
    private String title;
    @SerializedName("point_desc")
    private String desc;
    @SerializedName("deep_link")
    private String deepLink;
    @SerializedName("label")
    private String labelButton;
    @SerializedName("point")
    private String point;
    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("type")
    private int type;
    private int typeView = 0;


    @SerializedName("icon")
    private String icon;

    @SerializedName("missionStatus")
    private String missionStatus = "";

    @SerializedName("appId")
    private String appId = "";

    @SerializedName("missionType")
    private String missionType = "";
    @SerializedName("unit")
    private String unit;
    @SerializedName("title_fake_mo")
    private String titleFakeMo = "";
    @SerializedName("confirm_fake_mo")
    private String confirmFakeMo = "";

    private int itemType;
    private int itemPoint;


    private int giftType;
    private String balance;


    public AccumulatePointItem() {

    }

    public AccumulatePointItem(String title, int typeView) {
        this.title = title;
        this.typeView = typeView;
    }

    public String getTitleFakeMo() {
        return titleFakeMo;
    }

    public void setTitleFakeMo(String titleFakeMo) {
        this.titleFakeMo = titleFakeMo;
    }

    public String getConfirmFakeMo() {
        return confirmFakeMo;
    }

    public void setConfirmFakeMo(String confirmFakeMo) {
        this.confirmFakeMo = confirmFakeMo;
    }

    public int getTypeView() {
        return typeView;
    }

    public long getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public String getLabelButton() {
        return labelButton;
    }

    public void setLabelButton(String labelButton) {
        this.labelButton = labelButton;
    }

    public String getPoint() {
        return point;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public String getIcon() {
        return icon;
    }

    public String getMissionStatus() {
        return missionStatus;
    }

    public void setMissionStatus(String missionStatus) {
        this.missionStatus = missionStatus;
    }

    public String getAppId() {
        return appId;
    }

    public String getMissionType() {
        return missionType;
    }

    public int getGiftType() {
        return giftType;
    }

    public void setGiftType(int giftType) {
        this.giftType = giftType;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUnit() {
        return unit;
    }

    public int getItemPoint() {
        return itemPoint;
    }

    public void setItemPoint(int itemPoint) {
        this.itemPoint = itemPoint;
    }

    public enum ACTION {
        INSTALLED,
        REGISTER,
        CONFIRM
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AccumulatePointItem: ");
        sb.append(" id: ").append(id);
        sb.append(" type: ").append(type);
        sb.append(" title: ").append(title);
        sb.append(" desc: ").append(desc);
        sb.append(" labelButton: ").append(labelButton);
        sb.append(" deepLink: ").append(deepLink);
        sb.append(" point: ").append(point);
        sb.append(" icon: ").append(icon);
        sb.append(" created_date: ").append(createdDate);
        sb.append(" missionStatus: ").append(missionStatus);
        sb.append(" appId: ").append(appId);
        sb.append(" missionType: ").append(missionType);
        sb.append(" giftType: ").append(giftType);
        sb.append(" balance: ").append(balance);
        return sb.toString();
    }
}
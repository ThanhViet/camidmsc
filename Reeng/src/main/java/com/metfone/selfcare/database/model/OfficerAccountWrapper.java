package com.metfone.selfcare.database.model;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

public class OfficerAccountWrapper implements Serializable {

    private CopyOnWriteArrayList<OfficerAccount> listOfficerAccounts;

    public OfficerAccountWrapper(CopyOnWriteArrayList<OfficerAccount> listOfficerAccounts) {
        this.listOfficerAccounts = listOfficerAccounts;
    }

    public CopyOnWriteArrayList<OfficerAccount> getListOfficerAccounts() {
        return listOfficerAccounts;
    }

    public void setListOfficerAccounts(CopyOnWriteArrayList<OfficerAccount> listOfficerAccounts) {
        this.listOfficerAccounts = listOfficerAccounts;
    }
}

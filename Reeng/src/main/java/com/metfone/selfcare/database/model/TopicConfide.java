package com.metfone.selfcare.database.model;

import java.io.Serializable;

/**
 * Created by toanvk2 on 7/21/2017.
 */

public class TopicConfide implements Serializable {
    private String message;
    private boolean isSelected = false;

    public TopicConfide() {
    }

    public TopicConfide(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

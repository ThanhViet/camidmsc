package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.model.call.CallHistory;
import com.metfone.selfcare.database.model.call.CallHistoryDetail;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 10/12/2016.
 */

public class CallHistoryDataSource {
    private static String TAG = CallHistoryDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static CallHistoryDataSource mInstance;

    public static synchronized CallHistoryDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new CallHistoryDataSource(application);
        }
        return mInstance;
    }

    private CallHistoryDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    private CallHistory getCallHistoryFromCursor(Cursor cursor) {
        CallHistory callHistory = new CallHistory();
        callHistory.setId(cursor.getLong(0));
        callHistory.setFriendNumber(cursor.getString(1));
        callHistory.setOwnerNumber(cursor.getString(2));
        callHistory.setCallDirection(cursor.getInt(3));
        callHistory.setCallState(cursor.getInt(4));
        callHistory.setCallType(cursor.getInt(5));
        callHistory.setCallTime(cursor.getLong(6));
        callHistory.setDuration(cursor.getInt(7));
        callHistory.setCount(cursor.getInt(8));
        return callHistory;
    }

    private ContentValues getContentValuesCallHistory(CallHistory callHistory) {
        ContentValues values = new ContentValues();
        values.put(CallHistoryConstant.HISTORY_FRIEND, callHistory.getFriendNumber());
        values.put(CallHistoryConstant.HISTORY_OWNER, callHistory.getOwnerNumber());
        values.put(CallHistoryConstant.HISTORY_DIRECTION, callHistory.getCallDirection());
        values.put(CallHistoryConstant.HISTORY_STATE, callHistory.getCallState());
        values.put(CallHistoryConstant.HISTORY_CALL_OUT, callHistory.getCallType());
        values.put(CallHistoryConstant.HISTORY_CALL_TIME, callHistory.getCallTime());
        values.put(CallHistoryConstant.HISTORY_CALL_DURATION, callHistory.getDuration());
        values.put(CallHistoryConstant.HISTORY_COUNT, callHistory.getCount());
        return values;
    }

    public void insertCallHistory(CallHistory callHistory) {
        try {
            ContentValues values = getContentValuesCallHistory(callHistory);
            long id = databaseWrite.insert(CallHistoryConstant.HISTORY_TABLE, null, values);
            callHistory.setId(id);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void updateCallHistory(CallHistory callHistory) {
        String whereId = CallHistoryConstant.HISTORY_ID + " = " + callHistory.getId();
        try {
            ContentValues values = getContentValuesCallHistory(callHistory);
            databaseWrite.update(CallHistoryConstant.HISTORY_TABLE, values, whereId, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public ArrayList<CallHistory> getAllCallHistory() {
        Cursor cursor = null;
        ArrayList<CallHistory> list = new ArrayList<>();
        try {
            cursor = databaseRead.rawQuery(CallHistoryConstant.SELECT_ALL_HISTORY_STATEMENT, null);
            if (cursor.moveToFirst()) {
                do {
                    CallHistory history = getCallHistoryFromCursor(cursor);
                    Log.i(TAG, "getCallHistoryFromCursor: " + history.toString());
                    list.add(history);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public CallHistory getFirstCallHistory() {
        CallHistory history = null;
        Cursor cursor = null;
        try {
            cursor = databaseRead.rawQuery(CallHistoryConstant.SELECT_TOP_HISTORY_QUERY, null);
            if (cursor.moveToFirst()) {
                history = getCallHistoryFromCursor(cursor);

            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return history;
    }

    public void deleteHistory(CallHistory history) {
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.beginTransaction();
            try {
                databaseWrite.delete(CallHistoryConstant.HISTORY_TABLE, CallHistoryConstant.HISTORY_ID + " = " + history.getId(), null);
                databaseWrite.delete(CallHistoryConstant.HISTORY_DETAIL_TABLE, CallHistoryConstant.HISTORY_DETAIL_CALL_ID + " = " + history.getId(), null);
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * call history detail
     */
    private CallHistoryDetail getCallHistoryDetailFromCursor(Cursor cursor) {
        CallHistoryDetail callHistoryDetail = new CallHistoryDetail();
        callHistoryDetail.setId(cursor.getLong(0));
        callHistoryDetail.setCallId(cursor.getLong(1));
        callHistoryDetail.setFriendNumber(cursor.getString(2));
        callHistoryDetail.setOwnerNumber(cursor.getString(3));
        callHistoryDetail.setCallDirection(cursor.getInt(4));
        callHistoryDetail.setCallState(cursor.getInt(5));
        callHistoryDetail.setCallType(cursor.getInt(6));
        callHistoryDetail.setCallTime(cursor.getLong(7));
        callHistoryDetail.setDuration(cursor.getInt(8));
        return callHistoryDetail;
    }

    private ContentValues getContentValuesCallHistoryDetail(CallHistoryDetail callHistoryDetail) {
        ContentValues values = new ContentValues();
        values.put(CallHistoryConstant.HISTORY_DETAIL_CALL_ID, callHistoryDetail.getCallId());
        values.put(CallHistoryConstant.HISTORY_DETAIL_FRIEND, callHistoryDetail.getFriendNumber());
        values.put(CallHistoryConstant.HISTORY_DETAIL_OWNER, callHistoryDetail.getOwnerNumber());
        values.put(CallHistoryConstant.HISTORY_DETAIL_DIRECTION, callHistoryDetail.getCallDirection());
        values.put(CallHistoryConstant.HISTORY_DETAIL_STATE, callHistoryDetail.getCallState());
        values.put(CallHistoryConstant.HISTORY_DETAIL_CALL_OUT, callHistoryDetail.getCallType());
        values.put(CallHistoryConstant.HISTORY_DETAIL_CALL_TIME, callHistoryDetail.getCallTime());
        values.put(CallHistoryConstant.HISTORY_DETAIL_CALL_DURATION, callHistoryDetail.getDuration());
        return values;
    }

    public void insertCallHistoryDetail(CallHistoryDetail callHistoryDetail) {
        try {
            ContentValues values = getContentValuesCallHistoryDetail(callHistoryDetail);
            long id = databaseWrite.insert(CallHistoryConstant.HISTORY_DETAIL_TABLE, null, values);
            callHistoryDetail.setId(id);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public ArrayList<CallHistoryDetail> getCallHistoryDetailByCallId(long callId) {
        Cursor cursor = null;
        String where = CallHistoryConstant.SELECT_DETAIL_BY_CALL_ID_QUERY + callId + " ORDER BY " + CallHistoryConstant.HISTORY_DETAIL_ID + " DESC";
        ArrayList<CallHistoryDetail> list = new ArrayList<>();
        try {
            cursor = databaseRead.rawQuery(where, null);
            if (cursor.moveToFirst()) {
                do {
                    CallHistoryDetail detail = getCallHistoryDetailFromCursor(cursor);
                    list.add(detail);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(CallHistoryConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void deleteAllDetailTable() {
        try {
            databaseWrite.execSQL(CallHistoryConstant.DELETE_ALL_DETAIL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }
}
package com.metfone.selfcare.database.model.avno;

import org.xbill.DNS.ARecord;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 4/12/2018.
 */

public class InfoAVNO {

    private String avnoNumber;
    private String balanceAccount;
    private int activeState = 0;
    private int topUp = 0;
    private int transferMoney = 0;
    private ArrayList<ItemInfo> accountInfoList = new ArrayList<>();            //dùng chung cả thằng avno, là list info
    private ArrayList<PackageAVNO> listPackageActive = new ArrayList<>();       //dùng chung cả thằng avno, là list package thường
    private ArrayList<PackageAVNO> listPackageAvailable = new ArrayList<>();    //dùng chung cả thằng avno, là list package hot
    private ArrayList<ItemInfo> listGift;
    private String introPackage;
    private ItemInfo intro;


    public String getBalanceAccount() {
        return balanceAccount;
    }

    public void setBalanceAccount(String balanceAccount) {
        this.balanceAccount = balanceAccount;
    }

    public ArrayList<ItemInfo> getAccountInfoList() {
        return accountInfoList;
    }

    public void setAccountInfoList(ArrayList<ItemInfo> accountInfoList) {
        this.accountInfoList = accountInfoList;
    }

    public ArrayList<PackageAVNO> getListPackageActive() {
        return listPackageActive;
    }

    public void setListPackageActive(ArrayList<PackageAVNO> listPackageActive) {
        this.listPackageActive = listPackageActive;
    }

    public ArrayList<PackageAVNO> getListPackageAvailable() {
        return listPackageAvailable;
    }

    public void setListPackageAvailable(ArrayList<PackageAVNO> listPackageAvailable) {
        this.listPackageAvailable = listPackageAvailable;
    }


    public int getActiveState() {
        return activeState;
    }

    public void setActiveState(int activeState) {
        this.activeState = activeState;
    }

    public String getAvnoNumber() {
        return avnoNumber;
    }

    public void setAvnoNumber(String avnoNumber) {
        this.avnoNumber = avnoNumber;
    }

    public int getTopUp() {
        return topUp;
    }

    public void setTopUp(int topUp) {
        this.topUp = topUp;
    }

    public ArrayList<ItemInfo> getListGift() {
        return listGift;
    }

    public void setListGift(ArrayList<ItemInfo> listGift) {
        this.listGift = listGift;
    }

    public String getIntroPackage() {
        return introPackage;
    }

    public void setIntroPackage(String introPackage) {
        this.introPackage = introPackage;
    }

    public ItemInfo getIntro() {
        return intro;
    }

    public void setIntro(ItemInfo intro) {
        this.intro = intro;
    }

    public int getTransferMoney() {
        return transferMoney;
    }

    public void setTransferMoney(int transferMoney) {
        this.transferMoney = transferMoney;
    }
}

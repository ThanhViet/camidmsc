/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.restful.AbsResultData;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * @author thanhnt72
 */
public class RestAllFeedsModel extends AbsResultData implements Serializable {

    @SerializedName("listContentTimeline")
    private ArrayList<FeedModelOnMedia> data;

    @SerializedName("timeserver")
    private long currentTimeServer;

    @SerializedName("list_quick_news")
    private ArrayList<QuickNewsOnMedia> listQuickNews;

    public ArrayList<FeedModelOnMedia> getData() {
        return data;
    }

    public void setData(ArrayList<FeedModelOnMedia> data) {
        this.data = data;
    }

    public long getCurrentTimeServer() {
        return currentTimeServer;
    }

    public ArrayList<QuickNewsOnMedia> getListQuickNews() {
        return listQuickNews;
    }

    @Override
    public String toString() {
        return "RestAllFeedsModel [data=" + data + "] error " + getError();
    }

}

package com.metfone.selfcare.database.model.message;

/**
 * Created by thanhnt72 on 3/16/2019.
 */

public class CPresence {

    private String operatorPresence = null;
    private int statePresence = -1;
    private int usingDesktop = -1;


    public String getOperatorPresence() {
        return operatorPresence;
    }

    public void setOperatorPresence(String operatorPresence) {
        this.operatorPresence = operatorPresence;
    }

    public int getStatePresence() {
        return statePresence;
    }

    public void setStatePresence(int statePresence) {
        this.statePresence = statePresence;
    }

    public int getUsingDesktop() {
        return usingDesktop;
    }

    public void setUsingDesktop(int usingDesktop) {
        this.usingDesktop = usingDesktop;
    }
}

package com.metfone.selfcare.database.model.message;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 8/3/2016.
 */
public class AdvertiseItem implements Serializable {
    private static final String TAG = AdvertiseItem.class.getSimpleName();
    @SerializedName("title")
    private String title = "";

    @SerializedName("des")
    private String des = "";

    @SerializedName("icon")
    private String iconUrl = "";

    @SerializedName("action")
    private String action = "";

    public AdvertiseItem(String title, String des, String iconUrl, String action) {
        this.title = title;
        this.des = des;
        this.iconUrl = iconUrl;
        this.action = action;
    }

    public String getTitle() {
        return title;
    }

    public String getDes() {
        return des;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getAction() {
        return action;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void fromJson(String json) {
        try {
            JSONObject object = new JSONObject(json);
            setTitle(object.optString("title"));
            setDes(object.optString("des", null));
            setIconUrl(object.optString("icon", null));
            setAction(object.optString("action", null));
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

}

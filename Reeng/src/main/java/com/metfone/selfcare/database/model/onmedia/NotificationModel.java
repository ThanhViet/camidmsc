package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.UserInfo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 11/5/2015.
 */
public class NotificationModel implements Serializable {

    public static final int FEED_NOT_SEEN = 0;  //chua click vao
    public static final int FEED_SEEN = 1;      //da click vao

    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_DEEPLINK = 1;

    @SerializedName("msisdn")
    private String msisdn = "";

    @SerializedName("content")
    private FeedContent content;

    @SerializedName("stamp")
    private long time;

    @SerializedName("type")
    private int type = 0;

    @SerializedName("action_type")
    private FeedModelOnMedia.ActionLogApp actionType = FeedModelOnMedia.ActionLogApp.UNKNOW;

    @SerializedName("isActive")
    private int isActive;

    @SerializedName("users_follow")
    private ArrayList<UserInfo> userFollow = new ArrayList<>();

    @SerializedName("base64RowID")
    private String base64RowId;

    @SerializedName("feedIdBase64")
    private String feedId;

    @SerializedName("userInfo")
    private UserInfo userInfo;

    @SerializedName("body")
    private String body = "";

    @SerializedName("url_sub_comment")
    private String urlSubComment;

    @SerializedName("comment_type")
    private int commentType;

    @SerializedName("status")
    private String status;

    @SerializedName("tags")
    private ArrayList<TagMocha> listTag = new ArrayList<>();

   /* private boolean seenState;

    public void setSeenState(boolean state) {
        this.seenState = state;
    }

    public boolean seenState() {
        return seenState;
    }*/

    public int getActive() {
        return isActive;
    }

    public void setActive(int isActive) {
        this.isActive = isActive;
    }

    public String getBase64RowId() {
        return base64RowId;
    }

    public String getFeedId() {
        return feedId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public FeedContent getContent() {
        return content;
    }

    public FeedModelOnMedia.ActionLogApp getActionType() {
        return actionType;
    }

    public int getIsActive() {
        return isActive;
    }

    public ArrayList<UserInfo> getUserFollow() {
        return userFollow;
    }

    public long getTime() {
        return time;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getBody() {
        return body;
    }

    public String getUrlSubComment() {
        return urlSubComment;
    }

    public boolean isReply() {
        return commentType == 1;
    }

    public String getStatus() {
        return status;
    }

    public ArrayList<TagMocha> getListTag() {
        return listTag;
    }

    public int getType() {
        return type;
    }
}

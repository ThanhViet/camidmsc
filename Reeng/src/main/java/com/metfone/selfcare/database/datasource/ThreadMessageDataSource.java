package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thaodv on 6/30/2014.
 */
public class ThreadMessageDataSource {
    private static String TAG = ThreadMessageDataSource.class.getSimpleName();
    private static ThreadMessageDataSource threadMessageDataSource;
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private ArrayList<ThreadMessage> allThreadMessage;

    private ThreadMessageDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    public static synchronized ThreadMessageDataSource getInstance(ApplicationController application) {
        if (threadMessageDataSource == null) {
            threadMessageDataSource = new ThreadMessageDataSource(application);
        }
        return threadMessageDataSource;
    }

    private ThreadMessage getThreadFromCursor(Cursor cursor) {
        // create new thread
        ThreadMessage thread = new ThreadMessage();
        try {
            // set attribute
            thread.setId(cursor.getInt(0));
            // get name
            thread.setName(cursor.getString(1));
            // get number if messages
            thread.setThreadType(cursor.getInt(2));
            // get numbers of thread
            String numbers = cursor.getString(3);
            // get server room id
            thread.setServerId(cursor.getString(4));
            // member
            thread.setPhoneNumbers(convertStringToArray(numbers));
            // get joined
            thread.setJoined(cursor.getInt(5) != 0);
            thread.setNumOfUnreadMessage(cursor.getInt(6));
            thread.setTimeOfLast(cursor.getLong(7));
            thread.setDraftMessage(cursor.getString(8));
            thread.setBackground(cursor.getString(9));
            thread.setDhVtt(cursor.getLong(10));
            thread.setLastTimeSaveDraft(cursor.getLong(11));
            thread.setState(cursor.getInt(12));
            //thread.setLastTimeShowInvite(cursor.getLong(13));
            thread.setGroupAvatar(cursor.getString(13));
            thread.setLastMessageId(cursor.getInt(14));
            // admin
            thread.setAdminNumbers(convertStringToArray(cursor.getString(15)));
            thread.setGroupClass(cursor.getInt(16));
            thread.setPinMessage(cursor.getString(17));
            thread.setLastTimePinThread(cursor.getLong(18));
            thread.setHiddenThread(cursor.getInt(19));
        } catch (Exception e) {
            Log.e(TAG, "getThreadFromCursor", e);
        }
        // return
        return thread;
    }

    /**
     * Get all threads in database
     *
     * @return list all of threads
     * @author thaodv
     */
    public CopyOnWriteArrayList<ThreadMessage> getAllThreadMessages() {
        Cursor cursor = null;
        CopyOnWriteArrayList<ThreadMessage> list = new CopyOnWriteArrayList<>();
        try {
            // Select all
            cursor = databaseRead.rawQuery(ThreadMessageConstant.THREAD_SELECT_ALL_QUERY, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    //ThreadMessage thread = getThreadFromCursor(cursor, new ThreadMessage());
                    list.add(getThreadFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    private ContentValues setContentValues(ThreadMessage thread) {
        ContentValues values = new ContentValues();
        values.put(ThreadMessageConstant.THREAD_NAME, thread.getThreadName());
        values.put(ThreadMessageConstant.THREAD_IS_GROUP, thread.getThreadType());
        values.put(ThreadMessageConstant.THREAD_NUMBERS, convertArrayToString(thread.getPhoneNumbers()));
        values.put(ThreadMessageConstant.THREAD_SERVER_ID, thread.getServerId());
        values.put(ThreadMessageConstant.THREAD_IS_JOINED, thread.isJoined());
        values.put(ThreadMessageConstant.THREAD_NUMBER_OF_UNREAD_MESSAGES,
                thread.getNumOfUnreadMessage());
        values.put(ThreadMessageConstant.THREAD_TIME_LAST_CHANGE, thread.getTimeOfLast());
        values.put(ThreadMessageConstant.THREAD_DRAFT_MESSAGE, thread.getDraftMessage());
        values.put(ThreadMessageConstant.THREAD_BACKGROUND, thread.getBackground());
        values.put(ThreadMessageConstant.THREAD_LAST_TIME_SHARE_MUSIC, thread.getDhVtt());
        values.put(ThreadMessageConstant.THREAD_LAST_TIME_SAVE_DRAFT, thread.getLastTimeSaveDraft());
        values.put(ThreadMessageConstant.THREAD_STATE, thread.getState());
        values.put(ThreadMessageConstant.THREAD_GROUP_AVATAR, thread.getGroupAvatar());
        values.put(ThreadMessageConstant.THREAD_LAST_MESSAGE_ID, thread.getLastMessageId());
        values.put(ThreadMessageConstant.THREAD_ADMIN_NUMBERS, convertArrayToString(thread.getAdminNumbers()));
        values.put(ThreadMessageConstant.THREAD_GROUP_CLASS, thread.getGroupClass());
        values.put(ThreadMessageConstant.THREAD_PIN_MESSAGE_JSON,
                thread.getPinMessage() == null ? "" : thread.getPinMessage().toJson());
        values.put(ThreadMessageConstant.THREAD_LAST_TIME_PIN_THREAD, thread.getLastTimePinThread());
        values.put(ThreadMessageConstant.THREAD_HIDDEN, thread.getHiddenThread());
        return values;
    }

    public void insertNewThreadMessage(ThreadMessage thread) {
        try {
            ContentValues values = setContentValues(thread);
            long id = databaseWrite.insert(ThreadMessageConstant.THREAD_TABLE, null, values);
            thread.setId((int) id);
        } catch (Exception e) {
            Log.e(TAG, "createThread", e);
        }
    }

    public void updateThreadMessage(ThreadMessage threadMessage) {
        String whereId = ThreadMessageConstant.THREAD_ID + " = " + threadMessage.getId();
        try {
            ContentValues values = setContentValues(threadMessage);
            databaseWrite.update(ThreadMessageConstant.THREAD_TABLE, values, whereId, null);
        } catch (Exception e) {
            Log.e(TAG, "createThread", e);
        }
    }

    public void updateThreadMessageAfterDeleteMessage(ThreadMessage threadMessage) {
        int lastMessageId = ThreadMessageConstant.LAST_MESSAGE_ID_DEFAULT;
        CopyOnWriteArrayList<ReengMessage> allMessages = threadMessage.getAllMessages();
        if (allMessages != null && !allMessages.isEmpty()) {
            lastMessageId = allMessages.get(allMessages.size() - 1).getId();
        }
        threadMessage.setLastMessageId(lastMessageId);
        updateThreadMessageAfterChangeLastId(threadMessage);
    }

    public void updateThreadMessageAfterChangeLastId(ThreadMessage threadMessage) {
        String whereId = ThreadMessageConstant.THREAD_ID + " = " + threadMessage.getId();
        try {
            ContentValues values = new ContentValues();
            values.put(ThreadMessageConstant.THREAD_LAST_MESSAGE_ID, threadMessage.getLastMessageId());
            databaseWrite.update(ThreadMessageConstant.THREAD_TABLE, values, whereId, null);
        } catch (Exception e) {
            Log.e(TAG, "createThread", e);
        }
    }

    public void updateListThreadMessageAfterChangeLastId(ArrayList<ThreadMessage> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (ThreadMessage threadMessage : list) {
                    String whereId = ThreadMessageConstant.THREAD_ID + " = " + threadMessage.getId();
                    values = new ContentValues();
                    values.put(ThreadMessageConstant.THREAD_LAST_MESSAGE_ID, threadMessage.getLastMessageId());
                    values.put(ThreadMessageConstant.THREAD_NUMBER_OF_UNREAD_MESSAGES, threadMessage
                            .getNumOfUnreadMessage());// bổ xung thêm numberOfUnred
                    databaseWrite.update(ThreadMessageConstant.THREAD_TABLE, values, whereId, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction updateListMessage", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "updateListMessage", e);
        }
    }

    public void updateListThreadMessageAfterChangeBackground(ArrayList<ThreadMessage> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (ThreadMessage threadMessage : list) {
                    String whereId = ThreadMessageConstant.THREAD_ID + " = " + threadMessage.getId();
                    values = new ContentValues();
                    values.put(ThreadMessageConstant.THREAD_BACKGROUND, threadMessage.getBackground());
                    databaseWrite.update(ThreadMessageConstant.THREAD_TABLE, values, whereId, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction updateListMessage", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "updateListMessage", e);
        }
    }

    public void updateListThreadMessage(ArrayList<ThreadMessage> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (ThreadMessage threadMessage : list) {
                    String whereId = ThreadMessageConstant.THREAD_ID + " = " + threadMessage.getId();
                    values = setContentValues(threadMessage);
                    databaseWrite.update(ThreadMessageConstant.THREAD_TABLE, values, whereId, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction updateListMessage", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "updateListMessage", e);
        }
    }

    public void deleteThreadMessage(int mThreadId) {
        String whereClause = ThreadMessageConstant.THREAD_ID + " = " + mThreadId;
        databaseWrite.delete(ThreadMessageConstant.THREAD_TABLE, whereClause, null);
    }

    public void deleteListThreadMessage(ArrayList<ThreadMessage> listThread) {
        try {
            databaseWrite.beginTransaction();
            try {
                String whereClause;
                for (ThreadMessage thread : listThread) {
                    whereClause = ThreadMessageConstant.THREAD_ID + " = " + thread.getId();
                    databaseWrite.delete(ThreadMessageConstant.THREAD_TABLE, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public ThreadMessage findThreadById(int threadId) {
        ThreadMessage threadMessage = null;
        Cursor cursor = null;
        try {
            String query = ThreadMessageConstant.THREAD_SELECT_BY_ID_QUERY + threadId + "";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    threadMessage = getThreadFromCursor(cursor);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "findMessageInDBByPacketId", e);
        } finally {
            closeCursor(cursor);
        }
        return threadMessage;
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(ThreadMessageConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }

    private String convertArrayToString(ArrayList<String> array) {
        if (array != null && array.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String number : array) {
                sb.append(number).append(",");
            }
            int lengText = sb.length();
            sb.deleteCharAt(lengText - 1);
            return sb.toString();
        } else {
            return "";
        }
    }

    private ArrayList<String> convertStringToArray(String input) {
        // member
        ArrayList<String> numbersList = new ArrayList<>();
        if (!TextUtils.isEmpty(input)) {
            String[] numbersArray = input.split(",");
            for (String item : numbersArray) {
                if (item.length() > 0) {
                    numbersList.add(item);
                }
            }
        }
        return numbersList;
    }
}
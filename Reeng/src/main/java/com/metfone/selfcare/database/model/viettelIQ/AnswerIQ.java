package com.metfone.selfcare.database.model.viettelIQ;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AnswerIQ implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("stat")
    @Expose
    private long stat;

    /*@SerializedName("isCorrect")
    @Expose
    private int isCorrect;*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getStat() {
        return stat;
    }

    public void setStat(long stat) {
        this.stat = stat;
    }

    /*public int getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        this.isCorrect = isCorrect;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()||id==null||id.isEmpty()) return false;
        AnswerIQ that = (AnswerIQ) o;
        return id.equals(that.id);
    }

}

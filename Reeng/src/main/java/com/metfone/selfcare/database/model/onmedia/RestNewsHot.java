package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.restful.AbsResultData;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HaiKE on 8/11/17.
 */

public class RestNewsHot extends AbsResultData implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("data")
    private ArrayList<NewsHotModel> data;

    public ArrayList<NewsHotModel> getData() {
        return data;
    }

    public void setData(ArrayList<NewsHotModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestNewsHot [data=" + data + "] error " + getError();
    }
}

package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.UserInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by thanhnt72 on 9/28/2015.
 * like share comment
 */
public class FeedAction implements Serializable {

    @SerializedName("userInfo")
    private UserInfo userInfo;

    @SerializedName("stamp")
    private long timeStamp = 0L;

    @SerializedName("base64RowID")
    private String base64RowId;

    @SerializedName("status")
    private String comment = "";

    @SerializedName("current_time")
    private long timeServer = 0L;

    @SerializedName("tags")
    private ArrayList<TagMocha> listTag = new ArrayList<>();

    @SerializedName("number_comment")
    private int numberCmt;

    @SerializedName("number_like")
    private int numberLike;

    @SerializedName("is_like")
    private int isLike;

    private String idCmtLocal;

    private boolean isReplyCmt;

    public long getTime() {
        return (System.currentTimeMillis() - (timeServer - timeStamp));
    }

    public String getComment() {
        return comment;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setTimeServer(long timeServer) {
        this.timeServer = timeServer;
    }

    public long getTimeServer() {
        return timeServer;
    }

    public ArrayList<TagMocha> getListTag() {
        return listTag;
    }

    public void setListTag(ArrayList<TagMocha> listTag) {
        this.listTag = listTag;
    }

    public String getBase64RowId() {
        return base64RowId;
    }

    public void setBase64RowId(String base64RowId) {
        this.base64RowId = base64RowId;
    }

    public int getNumberCmt() {
        return numberCmt;
    }

    public void setNumberCmt(int numberCmt) {
        this.numberCmt = numberCmt;
    }

    public boolean isReplyCmt() {
        return isReplyCmt;
    }

    public void setReplyCmt(boolean replyCmt) {
        isReplyCmt = replyCmt;
    }

    public int getNumberLike() {
        return numberLike;
    }

    public void setNumberLike(int numberLike) {
        this.numberLike = numberLike;
    }

    public int getIsLike() {
        return isLike;
    }

    public void setIsLike(int isLike) {
        this.isLike = isLike;
    }

    public String getIdCmtLocal() {
        return idCmtLocal;
    }

    public void setIdCmtLocal(String idCmtLocal) {
        this.idCmtLocal = idCmtLocal;
    }

    public FeedAction() {
    }

    public FeedAction(String msisdn, String comment, long timeStamp, long timeServer) {
        this.userInfo = new UserInfo();
        this.userInfo.setMsisdn(msisdn);
        this.timeStamp = timeStamp;
        this.comment = comment;
        this.timeServer = timeServer;
    }

    public FeedAction(UserInfo userInfo, String comment, ArrayList<TagMocha> listTag) {
        this.listTag = listTag;
        this.userInfo = userInfo;
        this.comment = comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}

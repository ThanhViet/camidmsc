package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;

import com.metfone.selfcare.database.constant.NoteMessageContant;
import com.metfone.selfcare.database.model.message.NoteMessageItem;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by sonnn00 on 7/17/2017.
 */

public class NoteMessageDataSource {
    private static String TAG = NoteMessageDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static NoteMessageDataSource mInstance;

    public static synchronized NoteMessageDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new NoteMessageDataSource(application);
        }
        return mInstance;
    }

    private NoteMessageDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }
//     this.thread_type = thread_type;
//        this.thread_jid = thread_jid;
//        this.thread_name = thread_name;
//        this.thread_avatar = thread_avatar;
//        this.content = content;
//        this.stranger_id = stranger_id;
//        this.timestamp = timestamp;

    private NoteMessageItem getNoteMessageItemFromCursor(Cursor cursor) {
        NoteMessageItem noteMessageItem = new NoteMessageItem();
        Log.i(TAG, "getfromcusor=-----------------------=" + cursor.getColumnName(4));
        Log.i(TAG, "getfromcusor=-----------------------=" + cursor.getString(4));
        noteMessageItem.setId(cursor.getLong(0));
        noteMessageItem.setThread_type(cursor.getInt(1));
        noteMessageItem.setThread_jid(cursor.getString(2));
        noteMessageItem.setThread_name(cursor.getString(3));
        noteMessageItem.setThread_avatar(cursor.getString(4));
        noteMessageItem.setContent(cursor.getString(5));
        noteMessageItem.setStranger_id(cursor.getString(6));
        noteMessageItem.setTimestamp(cursor.getLong(7));

        return noteMessageItem;
    }

    //
    private ContentValues getContentValuesNoteMessageItem(NoteMessageItem noteMessageItem) {
        ContentValues values = new ContentValues();
        //values.put(NoteMessageContant.ID, noteMessageItem.getId());
        values.put(NoteMessageContant.THREAD_TYPE, noteMessageItem.getThread_type());
        values.put(NoteMessageContant.THREAD_JID, noteMessageItem.getThread_jid());
        values.put(NoteMessageContant.THREAD_NAME, noteMessageItem.getThread_name());
        values.put(NoteMessageContant.THREAD_AVATAR, noteMessageItem.getThread_avatar());
        values.put(NoteMessageContant.CONTENT, noteMessageItem.getContent());
        values.put(NoteMessageContant.STRANGER_ID, noteMessageItem.getStranger_id());
        values.put(NoteMessageContant.TIMESTAMP, noteMessageItem.getTimestamp());
        return values;
    }

    //
    public void insertNoteMessageItem(NoteMessageItem noteMessageItem) {
        try {
            ContentValues values = getContentValuesNoteMessageItem(noteMessageItem);
//            long id =
            Log.i(TAG, "insert------------" + noteMessageItem.getId());
            long id = databaseWrite.insert(NoteMessageContant.TABLE, null, values);
            noteMessageItem.setId(id);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    //
    public void updateNoteMessageItem(NoteMessageItem noteMessageItem) {
        String whereId = NoteMessageContant.ID + " = " + noteMessageItem.getId();

        try {

            ContentValues values = getContentValuesNoteMessageItem(noteMessageItem);
            databaseWrite.update(NoteMessageContant.TABLE, values, whereId, null);
            Log.i(TAG, "update------------------" + noteMessageItem.getId());
            Log.i(TAG, "update------------------" + noteMessageItem.getContent());
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    //
    public ArrayList<NoteMessageItem> getAllNoteMessageItem() {
        Cursor cursor = null;
        ArrayList<NoteMessageItem> list = new ArrayList<>();
        try {
            cursor = databaseRead.rawQuery(NoteMessageContant.SELECT_ALL_STATEMENT + NoteMessageContant.ORDER_BY, null);
            if (cursor.moveToFirst()) {
                do {
                    NoteMessageItem history = getNoteMessageItemFromCursor(cursor);
                    Log.i(TAG, "get all------------------" + history.getId());

                    Log.i(TAG, "getNoteMessageItemFromCursor: " + history.getContent());
                    list.add(history);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public ArrayList<NoteMessageItem> getListNote(int page) {
        int offset = (page - 1) * NoteMessageContant.SIZE;
        Cursor cursor = null;
        ArrayList<NoteMessageItem> list = new ArrayList<>();
        try {
            cursor = databaseRead.rawQuery(NoteMessageContant.SELECT_ALL_STATEMENT + NoteMessageContant.ORDER_BY + NoteMessageContant.LIMIT + offset, null);
            if (cursor.moveToFirst()) {
                do {
                    NoteMessageItem history = getNoteMessageItemFromCursor(cursor);
                    Log.i(TAG, "get all------------------" + history.getId());

                    Log.i(TAG, "getNoteMessageItemFromCursor: " + history.getContent());
                    list.add(history);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }


    public void deleteNoteMessage(NoteMessageItem history) {
        try {
            if (databaseWrite == null) {
                return;
            }
//            databaseWrite.beginTransaction();
            try {
                databaseWrite.delete(NoteMessageContant.TABLE, NoteMessageContant.ID + " = " + history.getId(), null);
//                databaseWrite.delete(NoteMessageItemConstant.HISTORY_DETAIL_TABLE, NoteMessageItemConstant.HISTORY_DETAIL_CALL_ID + " = " + history.getId(), null);
//                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void deleteAllDetailTable() {
        try {
            databaseWrite.execSQL(NoteMessageContant.DELETE_ALL_DETAIL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

}

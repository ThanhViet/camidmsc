package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thaodv on 5/8/2015.
 */
public class StrangerDataSource {
    private static String TAG = StrangerDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static StrangerDataSource mStrangerDataSource;

    private StrangerDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    public static synchronized StrangerDataSource getInstance(ApplicationController application) {
        if (mStrangerDataSource == null) {
            mStrangerDataSource = new StrangerDataSource(application);
        }
        return mStrangerDataSource;
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    public List<StrangerPhoneNumber> getAllStrangerPhoneNumber() {
        List<StrangerPhoneNumber> strangerPhoneNumbers = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return strangerPhoneNumbers;
            }
            cursor = databaseRead.rawQuery(StrangerConstant.SELECT_ALL_STRANGER_QUERY, null);
            if (cursor != null && cursor.getCount() > 0) {
                strangerPhoneNumbers = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        StrangerPhoneNumber item = getStrangerPhoneNumberFromCursor(cursor);
                        // set phone number
                        strangerPhoneNumbers.add(item);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        } finally {
            closeCursor(cursor);
        }
        return strangerPhoneNumbers;
    }

    public StrangerPhoneNumber getStrangerPhoneNumberFromCursor(Cursor cursor) {
        StrangerPhoneNumber strangerPhoneNumber = new StrangerPhoneNumber();
        strangerPhoneNumber.setStrangerId(cursor.getLong(0));
        strangerPhoneNumber.setPhoneNumber(cursor.getString(1));
        strangerPhoneNumber.setMyName(cursor.getString(2));
        strangerPhoneNumber.setFriendName(cursor.getString(3));
        strangerPhoneNumber.setAppId(cursor.getString(4));
        strangerPhoneNumber.setFriendAvatarUrl(cursor.getString(5));
        strangerPhoneNumber.setStateString(cursor.getString(6));
        strangerPhoneNumber.setCreateDate(cursor.getLong(7));
        return strangerPhoneNumber;
    }

    // set values
    private ContentValues setValues(StrangerPhoneNumber phoneNumber) {
        ContentValues values = new ContentValues();
        values.put(StrangerConstant.STRANGER_JID_NUMBER, phoneNumber.getPhoneNumber());
        values.put(StrangerConstant.STRANGER_MY_NAME, phoneNumber.getMyName());
        values.put(StrangerConstant.STRANGER_FRIEND_NAME, phoneNumber.getFriendName());
        values.put(StrangerConstant.STRANGER_APP_ID, phoneNumber.getAppId());
        values.put(StrangerConstant.STRANGER_FRIEND_AVATAR, phoneNumber.getFriendAvatarUrl());
        values.put(StrangerConstant.STRANGER_STATE, phoneNumber.getStateString());
        values.put(StrangerConstant.STRANGER_CREATE_DATE, phoneNumber.getCreateDate());
        return values;
    }

    // insert item
    public void insertStrangerPhoneNumber(StrangerPhoneNumber phoneNumber) {
        try {
            phoneNumber.setCreateDate(TimeHelper.getCurrentTime());
            ContentValues values = setValues(phoneNumber);
            long id = databaseWrite.insert(StrangerConstant.STRANGER_TABLE, null, values);
            phoneNumber.setStrangerId(id);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    public void updateStrangerPhoneNumber(StrangerPhoneNumber phoneNumber) {
        if (phoneNumber == null) {
            return;
        }
        try {
            String whereClause = StrangerConstant.STRANGER_ID +
                    " = " + phoneNumber.getStrangerId();
            databaseWrite.update(StrangerConstant.STRANGER_TABLE,
                    setValues(phoneNumber), whereClause, null);
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
    }

    /**
     * xoa stranger trogng db
     *
     * @param strangerPhoneNumber
     */
    public void deleteStrangerPhoneNumber(StrangerPhoneNumber strangerPhoneNumber) {
        if (strangerPhoneNumber == null) {
            return;
        }
        try {
            String whereClause = StrangerConstant.STRANGER_ID + " = " + strangerPhoneNumber.getStrangerId();
            databaseWrite.delete(StrangerConstant.STRANGER_TABLE, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(StrangerConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }
}
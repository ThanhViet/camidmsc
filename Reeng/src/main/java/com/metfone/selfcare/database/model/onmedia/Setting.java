package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tungt on 10/22/2015.
 */
public class Setting implements Serializable{
    private static final long serialVersionUID = 1L;

    @SerializedName("setting")
    private ArrayList<SourceType> data;

    @SerializedName("code")
    public int code;

    public ArrayList<SourceType> getData() {
        return data;
    }

    public void setData(ArrayList<SourceType> data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }
}

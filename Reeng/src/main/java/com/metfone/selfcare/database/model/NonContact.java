package com.metfone.selfcare.database.model;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TimeHelper;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 11/13/14.
 */
public class NonContact implements Serializable {
    private static final String TAG = NonContact.class.getSimpleName();
    private long id;
    private String jidNumber;
    private int state = Constants.CONTACT.NONE;          // =0 ko dungg, =1 dang dung, =2 deactive
    private String status;
    private String lAvatar = null;
    private int gender = -1;  //male=1, female=0; default=-1;
    private long birthDay = -1;                          // ngay sinh (mac dinh 1/1/1990)631126800000L
    private long lastOnline = 0;
    //last seen
    //=0 online, -1 chua co gia tri
    private long lastSeen = -1;
    private String rawNumber;
    private String birthdayString = "";
    private long timeLastRequest = -1;
    private String nickName;
    private int permission = -1;
    private ArrayList<ImageProfile> mImageProfile = new ArrayList<>();

    private int stateFollow = -2;// chua follow ==-1 lên để mặc định là -2
    private String newJidNumber;
    private String operator;
    private String operatorPresence;
    private int usingDesktop = -1;
    private int statePresence;
    private String preKey;

    public NonContact() {

    }

    public NonContact(String jidNumber, int state, String status, String lastAvatar,
                      int gender, long lastOnline, long lastSeen, String birthDayString,
                      String nickName, int stateFollow, String operator, String preKey) {
        this.jidNumber = jidNumber;
        this.state = state;
        this.status = status;
        this.lAvatar = lastAvatar;
        this.gender = gender;
        this.lastOnline = lastOnline;
        this.lastSeen = lastSeen;
        this.birthdayString = birthDayString;
        this.nickName = nickName;
        this.stateFollow = stateFollow;
        this.operator = operator;
        this.preKey = preKey;
    }

    public ArrayList<ImageProfile> getImageProfile() {
        return mImageProfile;
    }

    public void setImageProfile(ArrayList<ImageProfile> mImageProfile) {
        this.mImageProfile = mImageProfile;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJidNumber() {
        return jidNumber;
    }

    public void setJidNumber(String jidNumber) {
        this.jidNumber = jidNumber;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getLAvatar() {
        return lAvatar;
    }

    public void setLAvatar(String lAvatar) {
        this.lAvatar = lAvatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public long getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(long birthDay) {
        this.birthDay = birthDay;
    }

    public long getLastOnline() {
        return lastOnline;
    }

    public void setLastOnline(long lastOnline) {
        this.lastOnline = lastOnline;
    }

    public long getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(long lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getRawNumber(ApplicationController application) {
        if (TextUtils.isEmpty(rawNumber)) {
            rawNumber = PhoneNumberHelper.getInstant().getRawNumber(application, jidNumber);
        }
        return rawNumber;
    }

    public String getBirthDayString() {
        return birthdayString;
    }

    public void setBirthDayString(String birthDayString) {
        this.birthdayString = birthDayString;
    }

    public long getTimeLastRequest() {
        return timeLastRequest;
    }

    public void setTimeLastRequest(long timeLastRequest) {
        this.timeLastRequest = timeLastRequest;
    }

    public boolean isTimeOutGetInfo() {
        if (timeLastRequest <= 0) return true;
        return TimeHelper.checkTimeOutForDurationTime(timeLastRequest, NumberConstant.TIME_OUT_GET_NON_CONTACT);
    }

    public boolean isReeng() {
        return state == Constants.CONTACT.ACTIVE;
    }

    public int getStateFollow() {
        return stateFollow;
    }

    public void setStateFollow(int stateFollow) {
        this.stateFollow = stateFollow;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((jidNumber == null) ? 0 : jidNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (NonContact.class != obj.getClass())
            return false;
        NonContact other = (NonContact) obj;
        String otherNumber = other.getJidNumber();
        // so sanh so
        return otherNumber != null && jidNumber != null && otherNumber.equals(jidNumber);
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    public boolean isShowBirthday() {

        // ngay sinh trong
        if (TextUtils.isEmpty(birthdayString)) {
            return false;
        } else if (permission == -1) {
            return true;
        } else {
            // and bit de lay bit =1
            int permissionBirth = permission & NumberConstant.PERMISSION_BIRTHDAY_ON;
            return permissionBirth == 1;
        }
    }

    public String getNewJidNumber() {
        return newJidNumber;
    }

    public void setNewJidNumber(String newJidNumber) {
        this.newJidNumber = newJidNumber;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public boolean isViettel() {
        return PhoneNumberHelper.isViettel(operator);
    }

    public String getOperatorPresence() {
        return operatorPresence;
    }

    public void setOperatorPresence(String operatorPresence) {
        this.operatorPresence = operatorPresence;
    }

    public String getPreKey() {
        if (isReeng())
            return preKey;
        return null;
    }

    public void setPreKey(String preKey) {
        this.preKey = preKey;
    }

    public int getUsingDesktop() {
        return usingDesktop;
    }

    public void setUsingDesktop(int usingDesktop) {
        this.usingDesktop = usingDesktop;
    }

    public int getStatePresence() {
        return statePresence;
    }

    public void setStatePresence(int statePresence) {
        this.statePresence = statePresence;
    }
}
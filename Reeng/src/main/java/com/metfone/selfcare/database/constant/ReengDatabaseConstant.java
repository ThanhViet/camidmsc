package com.metfone.selfcare.database.constant;

/**
 * Created by ThaoDV on 6/6/14.
 */
public class ReengDatabaseConstant {
    // database info
    public static final String DATABASE_NAME = "MOCHA.db";
    public static final int DATABASE_VERSION = 66;
}

package com.metfone.selfcare.database.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.helper.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ThanhNT on 12/31/2014.
 */
public class StickerCollection implements Serializable {
    private long localId;
    @SerializedName("collectionId")
    private int serverId;
    @SerializedName("collectionName")
    private String collectionName;
    @SerializedName("numberSticker")
    private int numberSticker;
    @SerializedName("collectionAvatar")
    private String collectionIconPath;
    @SerializedName("collectionPreview")
    private String collectionPreviewPath;
    @SerializedName("type")
    private int collectionType; //loai sticker: sticker - voice sticker - ..
    @SerializedName("general")
    private int isDefault;
    @SerializedName("prefix")
    private String collectionPrefix; //domain + port + prefix + filename = full path;
    @SerializedName("tick")
    private int isSticky;//
    @SerializedName("modifyItem")
    private long lastServerUpdate; //thoi gian update lan cuoi cung tren server
    @SerializedName("modifyDate")
    private long lastInfoUpdate; //thoi gian cuoi cung thay doi cac thuoc tinh cua item nhu sticky, name
    @SerializedName("order")
    private int order;// thu tu sap xep danh sach sticker store

    private long lastLocalUpdate;
    private boolean isDownloaded; //collection da dc download hay chua
    private int collectionState; //enable la chua xoa, disable la xoa
    private boolean isStickerStore;
    private boolean isNew;
    private boolean isDownloading = false;
    private long lastSticky = 0;   //dung de luu thoi gian download hoac thoi gian sticky

    public StickerCollection() {

    }

    public StickerCollection(String collectionName, int numberSticker) {
        this.collectionName = collectionName;
        this.numberSticker = numberSticker;
    }

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public int getNumberSticker() {
        return numberSticker;
    }

    public void setNumberSticker(int numberSticker) {
        this.numberSticker = numberSticker;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(boolean isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

    public String getCollectionIconPath() {
        return collectionIconPath;
    }

    public void setCollectionIconPath(String collectionIconPath) {
        this.collectionIconPath = collectionIconPath;
    }

    public int getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(int collectionType) {
        this.collectionType = collectionType;
    }

    public int getCollectionState() {
        return collectionState;
    }

    public void setCollectionState(int collectionState) {
        this.collectionState = collectionState;
    }

    public boolean isStickerStore() {
        return isStickerStore;
    }

    public String getCollectionPreviewPath() {
        return collectionPreviewPath;
    }

    public void setCollectionPreviewPath(String collectionPreviewPath) {
        this.collectionPreviewPath = collectionPreviewPath;
    }

    public long getLastSticky() {
        return lastSticky;
    }

    public void setLastSticky(long lastSticky) {
        this.lastSticky = lastSticky;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean isDownloading() {
        return isDownloading;
    }

    public void setIsDownloading(boolean isDownloading) {
        this.isDownloading = isDownloading;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public int hashCode() {
//        final int prime = 31;
//        int result = 1;
//        result = prime * result
//                //      + ((collectionName == null) ? 0 : collectionName.hashCode())
//                + serverId;
//        return result;
        return serverId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (StickerCollection.class != obj.getClass()) {
            return false;
        }
        StickerCollection otherObject = (StickerCollection) obj;
        long otherServerId = otherObject.getServerId();
        return serverId == otherServerId;
    }

    public int isDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public long getLastLocalUpdate() {
        return lastLocalUpdate;
    }

    public void setLastLocalUpdate(long lastLocalUpdate) {
        this.lastLocalUpdate = lastLocalUpdate;
    }

    public long getLastServerUpdate() {
        return lastServerUpdate;
    }

    public void setLastServerUpdate(long lastServerUpdate) {
        this.lastServerUpdate = lastServerUpdate;
    }

    public String getCollectionPrefix() {
        return collectionPrefix;
    }

    public int isSticky() {
        return isSticky;
    }

    public void setIsSticky(int isSticky) {
        this.isSticky = isSticky;
    }

    public void setCollectionPrefix(String collectionPrefix) {
        this.collectionPrefix = collectionPrefix;
    }

    public long getLastInfoUpdate() {
        return lastInfoUpdate;
    }

    public void setLastInfoUpdate(long lastInfoUpdate) {
        this.lastInfoUpdate = lastInfoUpdate;
    }

    public String toString() {
        return "StickerCollection: " + "\n localId: " + localId + "\n serverId: " + serverId + " \n collectionName: " +
                collectionName + "\n numberSticker: " + numberSticker + "\n collectionAvatar" +
                collectionIconPath + "\n type: " + collectionType + "\n isDownload: " + isDownloaded + "\n isDefault: " + isDefault
                + " \n lastLocalUpdate: " + lastLocalUpdate + " \n collectionPrefix: " + collectionPrefix
                + " \n isSticky: " + isSticky + " \n lastInfoUpdate " + lastInfoUpdate;
    }

    public void setDataFromJsonObject(JSONObject object) throws JSONException {
        serverId = object.getInt(Constants.HTTP.STICKER.STICKER_COLLECTION_ID);
        collectionName = object.getString(Constants.HTTP.STICKER.STICKER_COLLECTION_NAME);
        numberSticker = object.getInt(Constants.HTTP.STICKER.STICKER_COLLECTION_NUMBER);
        collectionIconPath = object.getString(Constants.HTTP.STICKER.STICKER_COLLECTION_AVATAR);
        collectionType = object.getInt(Constants.HTTP.STICKER.STICKER_COLLECTION_TYPE);
        collectionPreviewPath = object.getString(Constants.HTTP.STICKER.STICKER_COLLECTION_PREVIEW);
    }

    public void setStickerStoreFromJsonObject(JSONObject object) throws JSONException {
        collectionName = object.getString("storeName");
        numberSticker = object.getInt("storeNumber");
        isStickerStore = true;
    }

    public boolean isUpdateCollection() {
        if (collectionState == StickerConstant.COLLECTION_STATE_DISABLE) {
            return false;
        } else return lastLocalUpdate < lastServerUpdate;
    }
}
package com.metfone.selfcare.database.constant;

/**
 * Created by thanhnt72 on 10/29/2018.
 */

public class PollConstant {
    public static final String TABLE = "POLL";
    public static final String ID = "id";
    public static final String SERVER_ID = "serverId";
    public static final String CONTENT = "content";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE +
            " (" + ID + " INTEGER PRIMARY KEY, " +
            SERVER_ID + " TEXT, " + CONTENT + " TEXT)";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String DROP_STATEMENT_OLD = "DROP TABLE IF EXISTS LOG";
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;

    public static final String POLL_SELECT_BY_ID_QUERY = "SELECT * FROM "
            + TABLE + " WHERE " + SERVER_ID + " LIKE ";
}

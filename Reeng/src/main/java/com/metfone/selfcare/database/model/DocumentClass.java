package com.metfone.selfcare.database.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Toanvk2 on 5/6/2017.
 */

public class DocumentClass implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("desc")
    private String desc;

    public DocumentClass() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

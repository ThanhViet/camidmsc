package com.metfone.selfcare.database.model.viettelIQ;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class QuestionIQ implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("matchId")
    @Expose
    private String matchId;
    @SerializedName("questInx")
    @Expose
    private int questInx;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("listResponse")
    @Expose
    private List<AnswerIQ> listResponse = null;

    @SerializedName("useHeart")
    @Expose
    private int canUseHeart = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public int getQuestInx() {
        return questInx;
    }

    public void setQuestInx(int questInx) {
        this.questInx = questInx;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<AnswerIQ> getListResponse() {
        return listResponse;
    }

    public void setListResponse(List<AnswerIQ> listResponse) {
        this.listResponse = listResponse;
    }

    public int getCanUseHeart() {
        return canUseHeart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass() || id == null || id.isEmpty()) return false;
        QuestionIQ that = (QuestionIQ) o;
        return id.equals(that.id);
    }

}

package com.metfone.selfcare.database.model.contact;

import com.google.gson.JsonObject;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.util.Log;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by toanvk2 on 5/23/2016.
 */
public class SocialFriendInfo implements Serializable {
    private static final String TAG = SocialFriendInfo.class.getSimpleName();
    private String userName;
    private String userNameUnicode;
    private String userFriendJid;
    private String userAvatar;
    private String userStatus = "";
    private int userMocha = 0;
    private int userType = 0;
    private String rowIdRequest = null;
    private byte[] rowId;
    //private String rowId;
    private int socialStatus = -1;
    private int socialType = 1;// tab

    private PhoneNumber phoneNumber;

    public SocialFriendInfo() {

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFriendJid() {
        return userFriendJid;
    }

    public void setUserFriendJid(String userFriendJid) {
        this.userFriendJid = userFriendJid;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public boolean isUserMocha() {
        return userMocha == 1;
    }

    public void setUserMocha(int userMocha) {
        this.userMocha = userMocha;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getStringRowId() {
        if (rowId == null || rowId.length == 0) {
            return "";
        }
        return Arrays.toString(rowId);
    }

    public int getSocialStatus() {
        return socialStatus;
    }

    public void setSocialStatus(int socialStatus) {
        this.socialStatus = socialStatus;
    }

    public int getSocialType() {
        return socialType;
    }

    public void setSocialType(int socialType) {
        this.socialType = socialType;
    }

    public String getUserNameUnicode() {
        return userNameUnicode;
    }

    public void setUserNameUnicode(String userName) {
        this.userNameUnicode = TextHelper.getInstant().convertUnicodeToAscci(userName);
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public String getRowIdRequest() {
        return rowIdRequest;
    }

    public void setRowIdRequest(String rowIdRequest) {
        this.rowIdRequest = rowIdRequest;
    }

    public void setJsonObject(JsonObject userInfoObject) {
        try {
            /*if (object.has("userInfo")) {
                JsonObject userInfoObject = object.getAsJsonObject("userInfo");*/
            if (userInfoObject.has("name")) {
                userName = userInfoObject.get("name").getAsString();
                setUserNameUnicode(userName);
            }
            if (userInfoObject.has("msisdn"))
                userFriendJid = userInfoObject.get("msisdn").getAsString();
            if (userInfoObject.has("avatar"))
                userAvatar = userInfoObject.get("avatar").getAsString();
            if (userInfoObject.has("status"))
                userStatus = userInfoObject.get("status").getAsString();
            if (userInfoObject.has("isMochaUser"))
                userMocha = userInfoObject.get("isMochaUser").getAsInt();
            if (userInfoObject.has("user_type"))
                userType = userInfoObject.get("user_type").getAsInt();

            if (userInfoObject.has("rowIdRequest"))
                rowIdRequest = userInfoObject.get("rowIdRequest").getAsString();
           /* }
            if (object.has("row_id")) {
                Gson gson = new Gson();
                rowId = gson.fromJson(object.getAsJsonArray("row_id"), byte[].class);
            }*/
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SocialFriendInfo");
        sb.append(" name: ").append(userName);
        sb.append(" number: ").append(userFriendJid);
        sb.append(" avatar: ").append(userAvatar);
        sb.append(" status: ").append(userStatus);
        sb.append(" isMocha: ").append(userMocha);
        sb.append(" userType: ").append(userType);
        sb.append(" rowIdString: ").append(getStringRowId());
        sb.append(" rowIdRequest: ").append(rowIdRequest);
        return sb.toString();
    }
}
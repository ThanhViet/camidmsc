package com.metfone.selfcare.database.constant;

import com.metfone.selfcare.database.model.UserInfo;

/**
 * Created by toanvk2 on 11/26/14.
 */
public class OfficerAccountConstant {
    public static final String OFFICER_TABLE = "offical";
    public static final String ID = "id";
    public static final String SERVER_ID = "offical_id";
    public static final String NAME = "name";
    public static final String AVATAR_URL = "avatar_url";
    public static final String OFFICER_STATE = "state";
    public static final String OFFICER_TYPE = "officer_type";
    public static final String ROOM_STATE = "room_state";
    public static final String SERVICE_ACTION = "service_action";
    // sql
    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + OFFICER_TABLE + "("
            + ID
            + " INTEGER PRIMARY KEY, "
            + SERVER_ID
            + " TEXT, "
            + NAME
            + " TEXT, "
            + AVATAR_URL
            + " TEXT, "
            + OFFICER_STATE
            + " INTEGER, "
            + OFFICER_TYPE
            + " INTEGER, "
            + ROOM_STATE
            + " INTEGER, "
            + SERVICE_ACTION
            + " TEXT"
            + ");";

    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + OFFICER_TABLE;
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + OFFICER_TABLE;
    public static final String OFFICAL_SELECT_ALL_QUERY = "SELECT * FROM "
            + OFFICER_TABLE;
    public static final String OFFICAL_DELETE_QUERY = "DELETE FROM "
            + OFFICER_TABLE + " WHERE " + ID + " = ";
    public static final String OFFICAL_SELECT_BY_ID_QUERY = "SELECT * FROM "
            + OFFICER_TABLE + " WHERE " + ID + " = ";
    public static final String OFFICAL_SELECT_BY_SERVER_ID_QUERY = "SELECT * FROM "
            + OFFICER_TABLE + " WHERE " + SERVER_ID + " = ";
    // ALTER_COLUMN_OFFICER_TYPE
    public static final String ALTER_COLUMN_OFFICER_TYPE = "ALTER TABLE " +
            OFFICER_TABLE + " ADD COLUMN " + OFFICER_TYPE + " INTEGER DEFAULT 0;";
    public static final String ALTER_COLUMN_ROOM_STATE = "ALTER TABLE " +
            OFFICER_TABLE + " ADD COLUMN " + ROOM_STATE + " INTEGER DEFAULT 0;";

    public static final String ALTER_COLUMN_SERVICE_ACTION = "ALTER TABLE " +
            OFFICER_TABLE + " ADD COLUMN " + SERVICE_ACTION + " TEXT;";

    public static final int OFFICER_STATE_NONE = 1;
    public static final int OFFICER_STATE_STICKY = 2;// gim trong danh ba
    /**
     * type officer
     */
    public static final int TYPE_OFFICER = 0;
    public static final int TYPE_STAR_ROOM = 1;
    /**
     * join room state
     */
    public static final int ROOM_STATE_NONE = 0;
    public static final int ROOM_STATE_SUCCESS = 1;
    public static final int ROOM_STATE_FOLLOW = 2;  // neu da follow thi la da success, cai nay dung de ko hien popup luc exit
    /**
     * onmedia official type dung khi type=TYPE_OFFICER
     */
    public static final int ONMEDIA_TYPE_NONE = 0;
    public static final int ONMEDIA_TYPE_VERIFIED = UserInfo.USER_ONMEDIA_VERIFY;

    public static final int OFFICER_TYPE_DIVIDER = -2;
    public static final int OFFICER_TYPE_NORMAL = 0;
    public static final int OFFICER_TYPE_BUSSINESS = 1;
}

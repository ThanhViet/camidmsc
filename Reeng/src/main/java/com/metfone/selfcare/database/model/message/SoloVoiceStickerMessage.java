package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.constant.ReengMessageConstant.MessageType;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Date;

/**
 * Created by toanvk2 on 12/30/14.
 */
public class SoloVoiceStickerMessage extends ReengMessage {
    public SoloVoiceStickerMessage(ThreadMessage thread, String from, String to, int collectionId, int itemId) {
        super(thread.getThreadType(), MessageType.voiceSticker);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setFileName(String.valueOf(collectionId));
        setSongId(itemId);
        setReadState(ReengMessageConstant.READ_STATE_READ);
        setStatus(ReengMessageConstant.STATUS_LOADING);
        setMessageType(MessageType.voiceSticker);
        setDirection(Direction.send);
        Date date = new Date();
        setTime(date.getTime());
        setPlayedGif(false);
    }
}
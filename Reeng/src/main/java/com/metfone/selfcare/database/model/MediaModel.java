package com.metfone.selfcare.database.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.utils.Utilities;

import java.io.Serializable;
import java.util.ArrayList;

public class MediaModel implements Serializable {

    private static final long serialVersionUID = 1L;

    //---------------------------------------------------------------------------------//
    public static final int TYPE_FAKE_SOMETHING = 0;
    public static final int TYPE_BAI_HAT = 1;
    public static final int TYPE_ALBUM = 2;
    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_SINGER = 4;
    public static final int TYPE_USER = 5;
    public static final int TYPE_PLAYLIST = 7;
    public static final int TYPE_VIDEO_OFFLINE = 8;
    public static final int TYPE_HISTORY = 9;
    public static final int TYPE_NOTIFICATION = 10;

    public static final int SONG_TYPE_UPLOAD = 1;

    public static final int SONG_NONE = 0;
    public static final int SONG_MONOPOLY = 1;
    public static final int SONG_MONOPOLY_SPECIFY = 3;

    public static final int FROM_SOURCE_NONE = 0;
    public static final int FROM_SOURCE_KEENG = 1;

    @SerializedName("songType")
    private int songType = 0;

    @SerializedName("item_type")
    private int type;

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name = "";

    @SerializedName("singer")
    private String singer;

    @SerializedName("image")
    private String image = "";

    @SerializedName("image310")
    private String image310;

    @SerializedName("media_url")
    private String media_url;

    @SerializedName("media_url_mono")
    private String media_url_mono;

    @SerializedName("media_url_pre")
    private String media_url_pre;

    /*@SerializedName("price")
    private int price;*/

    @SerializedName("listen_type")
    private int listen_type;

    @SerializedName("url")
    private String url;

    @SerializedName("download_url")
    private String download_url;

    @SerializedName("listen_no")
    private long listened;

    @SerializedName("is_download")
    private int is_downloadable = 1;

    /*@SerializedName("copyright")
    private int copyright;*/

    //private int freeState;

    /*@SerializedName("lyric")
    private String lyric = "";*/

    @SerializedName("firstSong")
    private MediaModel firstSong;

    @SerializedName("album_id")
    private long album_id = -1;

    @SerializedName("song_list")
    private ArrayList<MediaModel> song_list;

   /* @SerializedName("total_share")
    private long total_share = 0;*/

    @SerializedName("ringbacktone_code")
    private String crbtCode;

    @SerializedName("crbt_price")
    private String crbtPrice;

    @SerializedName("doc_quyen")
    private int monopoly = SONG_NONE;
    @SerializedName("autoplay_video")
    private int autoplayVideo = 0;

    @SerializedName("identify")
    private String identify;

    private String lyric;

    private String lyricUrl;

    private boolean isLike;
    private boolean isShare;
    private long totalLike = 0;
    private long totalShare = 0;
    private long totalComment = 0;
    private int fromSource = 0;
    private boolean isCallLogEnd = false;
    private String stateLog;

    @Override
    public String toString() {
        return "MediaModel id=" + id + "/" + name + type + "/" + song_list.size() + "\n  " + media_url
                + " /down " + download_url + " -- image: " + image + " crbt_code: " + crbtCode + " crbt_price: " + crbtPrice;
    }

    public MediaModel() {
        super();
        song_list = new ArrayList<>();
    }

    public MediaModel(String songId) {
        super();
        this.id = songId;
        song_list = new ArrayList<>();
    }

    public int getFromSource() {
        return fromSource;
    }

    public void setFromSource(int fromSource) {
        this.fromSource = fromSource;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public boolean isShare() {
        return isShare;
    }

    public void setShare(boolean share) {
        isShare = share;
    }

    public String getLyric() {
        return lyric;
    }

    public void setLyric(String lyric) {
        this.lyric = lyric;
    }

    public String getLyricUrl() {
        return lyricUrl;
    }

    public void setLyricUrl(String lyricUrl) {
        this.lyricUrl = lyricUrl;
    }

    public String getSinger() {
        if (singer == null) return "";
        return singer;
    }

    public long getListened() {
        return listened;
    }

    public String getListenNo() {
        return Utilities.formatNumber(listened);
    }

    public void setListened(long listened) {
        this.listened = listened;
    }

    public ArrayList<MediaModel> getSong_list() {
        return song_list;
    }

    public void setSong_list(ArrayList<MediaModel> song_list) {
        this.song_list = song_list;
    }

    public int getSongType() {
        return songType;
    }

    public void setSongType(int songType) {
        this.songType = songType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public long getIdInt() {
        try {
            return Long.parseLong(id);
        } catch (Exception e) {
        }
        return 0;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        if (TextUtils.isEmpty(name))
            name = "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage310() {
        return image310;
    }

    public void setImage310(String image310) {
        this.image310 = image310;
    }

    public String getMedia_url() {
        if (media_url == null) return "";
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getCrbtCode() {
        return crbtCode;
    }

    public void setCrbtCode(String crbtCode) {
        this.crbtCode = crbtCode;
    }

    public String getCrbtPrice() {
        return crbtPrice;
    }

    public void setCrbtPrice(String crbtPrice) {
        this.crbtPrice = crbtPrice;
    }

    public String getUrl() {
        if (url == null) return "";
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isValid() {
        return !(TextUtils.isEmpty(id) || TextUtils.isEmpty(media_url));
    }

    public String getSongAndSinger() {
        String songAndSinger = "";
        if (!TextUtils.isEmpty(name)) {
            songAndSinger = name;
        }
        if (!TextUtils.isEmpty(singer)) {
            songAndSinger += " - " + singer;
        }
        return songAndSinger;
    }

    public boolean isMonopoly() {
        return monopoly == SONG_MONOPOLY || monopoly == SONG_MONOPOLY_SPECIFY;
    }

    public void setMonopoly(int monopoly) {
        this.monopoly = monopoly;
    }

    public int getAutoplayVideo() {
        return autoplayVideo;
    }

    public void setAutoplayVideo(int autoplayVideo) {
        this.autoplayVideo = autoplayVideo;
    }

    public long getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(long totalLike) {
        this.totalLike = totalLike;
    }

    public long getTotalShare() {
        return totalShare;
    }

    public void setTotalShare(long totalShare) {
        this.totalShare = totalShare;
    }

    public long getTotalComment() {
        return totalComment;
    }

    public void setTotalComment(long totalComment) {
        this.totalComment = totalComment;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public boolean isCallLogEnd() {
        return isCallLogEnd;
    }

    public void setCallLogEnd(boolean callLogEnd) {
        isCallLogEnd = callLogEnd;
    }

    public String getStateLog() {
        return stateLog;
    }

    public void setStateLog(String stateLog) {
        this.stateLog = stateLog;
    }
}

package com.metfone.selfcare.database.datasource;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.metfone.selfcare.database.constant.BlockConstant;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.ChannelDataConstants;
import com.metfone.selfcare.database.constant.ConfigUserConstant;
import com.metfone.selfcare.database.constant.ContactConstant;
import com.metfone.selfcare.database.constant.EventMessageConstant;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.constant.LogConstant;
import com.metfone.selfcare.database.constant.MediaConstant;
import com.metfone.selfcare.database.constant.MessageImageDBConstant;
import com.metfone.selfcare.database.constant.NonContactConstant;
import com.metfone.selfcare.database.constant.NoteMessageContant;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.constant.PollConstant;
import com.metfone.selfcare.database.constant.ReengAccountConstant;
import com.metfone.selfcare.database.constant.ReengDatabaseConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.constant.TopSongConstant;
import com.metfone.selfcare.database.constant.VideoConstant;
import com.metfone.selfcare.util.Log;

/**
 * Created by ThaoDV on 6/6/14.
 */
public class ReengSQLiteHelper extends SQLiteOpenHelper {
    private static final String TAG = ReengSQLiteHelper.class.getSimpleName();
    private static SQLiteDatabase mWritableDb;
    private static SQLiteDatabase mReadableDb;
    private static ReengSQLiteHelper mInstance;

    public ReengSQLiteHelper(Context context) {
        super(context, ReengDatabaseConstant.DATABASE_NAME, null, ReengDatabaseConstant.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        createAllTable(database);
    }

    private void createAllTable(SQLiteDatabase database) {
        database.execSQL(TopSongConstant.CREATE_STATEMENT);
        database.execSQL(ContactConstant.CREATE_STATEMENT);
        database.execSQL(ReengMessageConstant.CREATE_STATEMENT);
        database.execSQL(ReengAccountConstant.CREATE_STATEMENT);
        database.execSQL(NumberConstant.CREATE_STATEMENT);
        database.execSQL(ThreadMessageConstant.CREATE_STATEMENT);
        database.execSQL(OfficerAccountConstant.CREATE_STATEMENT);
        database.execSQL(BlockConstant.CREATE_STATEMENT);
        database.execSQL(StickerConstant.CREATE_COLLECTION_STATEMENT);
        database.execSQL(EventMessageConstant.CREATE_STATEMENT);
        database.execSQL(MediaConstant.CREATE_STATEMENT);
        database.execSQL(StickerConstant.CREATE_RECENT_STATEMENT);
        database.execSQL(StrangerConstant.CREATE_STRANGER_STATEMENT);
        database.execSQL(NonContactConstant.CREATE_STATEMENT);
        database.execSQL(StickerConstant.CREATE_STICKER_STATEMENT);
        //database.execSQL(KeengConstant.CREATE_STATEMENT);
        database.execSQL(ReengMessageConstant.CREATE_INDEX);
        database.execSQL(ImageProfileConstant.CREATE_STATEMENT);
//        database.execSQL(MoreAppConstant.CREATE_STATEMENT);
        database.execSQL(CallHistoryConstant.CREATE_HISTORY_STATEMENT);
        database.execSQL(CallHistoryConstant.CREATE_DETAIL_STATEMENT);
        database.execSQL(NoteMessageContant.CREATE_STATEMENT);
        database.execSQL(ConfigUserConstant.CREATE_STATEMENT);

        database.execSQL(LogConstant.CREATE_STATEMENT);
        database.execSQL(VideoConstant.CREATE_STATEMENT);
        database.execSQL(ChannelDataConstants.CREATE_STATEMENT);
        database.execSQL(MessageImageDBConstant.CREATE_STATEMENT);
        database.execSQL(PollConstant.CREATE_STATEMENT);
        Log.i("sql", "on create table");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade oldVersion = " + oldVersion + " newVersion = " + newVersion);
        // db ver 6 la ban dau tien up len store
        if (oldVersion <= 6 && newVersion > 6) {
            // up ver >6 them colum draft
            db.execSQL(ThreadMessageConstant.ALTER_COLUMN_DRAFT);
        }
        if (oldVersion <= 7 && newVersion > 7) {
            // up ver >7 them colum background
            db.execSQL(ThreadMessageConstant.ALTER_COLUMN_BACKGROUND);
        }
        // version 9,10,11,12 khong duoc up len store
        if (oldVersion <= 12 && newVersion > 12) {
            //tu version 12 se co bang sticker
            Log.i(TAG, "drop and re-create sticker");
            db.execSQL(StickerConstant.DROP_COLLECTION_STATEMENT);
            db.execSQL(StickerConstant.DROP_ITEM_STATEMENT);
            db.execSQL(StickerConstant.CREATE_COLLECTION_STATEMENT);
            //            db.execSQL(StickerConstant.CREATE_ITEM_STATEMENT); ver 17 xoa bang nay nen ko them o day nua
        }
        //version 14 them bang event_message
        if (oldVersion <= 13 && newVersion > 13) {
            Log.d(TAG, "create table event_message");
            db.execSQL(EventMessageConstant.CREATE_STATEMENT);
        }
        //version 15 them cot last_time_share_music vao bang thread
        if (oldVersion <= 14 && newVersion > 14) {
            Log.d(TAG, "alter table thread");
            db.execSQL(ThreadMessageConstant.ALTER_COLUMN_LAST_TIME_SHARE_MUSTIC);
        }

        // vesion 16 them bang luu media cho cac mesage cung nghe
        if (oldVersion <= 15 && newVersion > 15) {
            Log.d(TAG, "create media_table");
            db.execSQL(MediaConstant.CREATE_STATEMENT);
        }

        // ver 17, xoa bang STICKER_ITEM_TABLE, them bang recent_sticker_table, them cot state music trong bang message
        if (oldVersion <= 16 && newVersion > 16) {
            Log.d(TAG, "create recent_sticker_table, drop STICKER_ITEM_TABLE");
            db.execSQL(ReengMessageConstant.ALTER_COLUMN_STATE_MUSIC);
            db.execSQL(StickerConstant.CREATE_RECENT_STATEMENT);
            db.execSQL(StickerConstant.DROP_ITEM_STATEMENT);
        }

        if (oldVersion <= 17 && newVersion > 17) {
            Log.d(TAG, "create CREATE_STRANGER_STATEMENT, create CREATE_STATEMENT");
            db.execSQL(StrangerConstant.CREATE_STRANGER_STATEMENT);
            db.execSQL(NonContactConstant.CREATE_STATEMENT);
        }

        if (oldVersion <= 18 && newVersion > 18) {
            Log.d(TAG, "ALTER_COLUMN_MESSAGE_IMAGE_URL");
            db.execSQL(ReengMessageConstant.ALTER_COLUMN_MESSAGE_IMAGE_URL);
            // tao lai bang event message
            db.execSQL(EventMessageConstant.DROP_STATEMENT);
            db.execSQL(EventMessageConstant.CREATE_STATEMENT);
        }

        if (oldVersion <= 19 && newVersion > 19) {
            Log.d(TAG, "create item sticker");
            db.execSQL(StickerConstant.CREATE_STICKER_STATEMENT);
        }

        if (oldVersion <= 20 && newVersion > 20) {
            Log.d(TAG, "alter draft time, alter account region code, alter number e164");
            db.execSQL(ThreadMessageConstant.ALTER_COLUMN_LAST_TIME_SAVE_DRAFT);
            db.execSQL(ReengAccountConstant.ALTER_COLUMN_REGION_CODE);
            db.execSQL(NumberConstant.ALTER_COLUMN_NUMBER_JID);
        }

        if (oldVersion <= 21 && newVersion > 21) {
            Log.d(TAG, "create table keeng");
            // db.execSQL(KeengConstant.CREATE_STATEMENT);
        }

        if (oldVersion <= 22 && newVersion > 22) {
            if (!checkExistColumnInTable(db, ReengAccountConstant.TABLE, ReengAccountConstant.BIRTHDAY_STRING)) {
                Log.i(TAG, "alter birthday string table reengAccount");
                db.execSQL(ReengAccountConstant.ALTER_COLUMN_BIRTHDAY_STRING);
            }
            if (!checkExistColumnInTable(db, NumberConstant.TABLE, NumberConstant.BIRTHDAY_STRING)) {
                Log.i(TAG, "alter birthday string table number");
                db.execSQL(NumberConstant.ALTER_COLUMN_BIRTHDAY_STRING);
            }
            if (!checkExistColumnInTable(db, NonContactConstant.NON_CONTACT_TABLE,
                    NonContactConstant.NON_CONTACT_BIRTHDAY_STRING)) {
                Log.i(TAG, "alter birthday string table nonContact");
                db.execSQL(NonContactConstant.ALTER_COLUMN_BIRTHDAY_STRING);
            }
        }

        if (oldVersion <= 23 && newVersion > 23) {
            if (!checkExistColumnInTable(db, ReengAccountConstant.TABLE, ReengAccountConstant.AVATAR_PATH)) {
                Log.i(TAG, "alter avatar path table reengAccount");
                db.execSQL(ReengAccountConstant.ALTER_COLUMN_AVATAR_PATH);
            }
            if (!checkExistColumnInTable(db, ReengAccountConstant.TABLE, ReengAccountConstant.NEED_UPLOAD)) {
                Log.i(TAG, "alter ALTER_COLUMN_NEED_UPLOAD table reengAccount");
                db.execSQL(ReengAccountConstant.ALTER_COLUMN_NEED_UPLOAD);
            }
        }
        if (oldVersion <= 24 && newVersion > 24) {
            if (!checkExistColumnInTable(db, StickerConstant.COLLECTION_TABLE, StickerConstant.COLLECTION_IS_NEW)) {
                Log.i(TAG, "alter is new table sticker collection");
                db.execSQL(StickerConstant.ALTER_COLUMN_IS_NEW);
            }
        }
        if (oldVersion <= 27 && newVersion > 27) {// tang db ver len 28 (bo qua 26,27)
            if (!checkExistColumnInTable(db, OfficerAccountConstant.OFFICER_TABLE, OfficerAccountConstant
                    .OFFICER_TYPE)) {
                Log.i(TAG, "alter officer_type  table officer_table");
                db.execSQL(OfficerAccountConstant.ALTER_COLUMN_OFFICER_TYPE);
            }
            if (!checkExistColumnInTable(db, OfficerAccountConstant.OFFICER_TABLE, OfficerAccountConstant.ROOM_STATE)) {
                Log.i(TAG, "alter room_state  table officer_table");
                db.execSQL(OfficerAccountConstant.ALTER_COLUMN_ROOM_STATE);
            }
            if (!checkExistColumnInTable(db, ThreadMessageConstant.THREAD_TABLE, ThreadMessageConstant.THREAD_STATE)) {
                Log.i(TAG, "alter thread_state  table thread_message");
                db.execSQL(ThreadMessageConstant.ALTER_COLUMN_THREAD_STATE);
            }
        }
        if (oldVersion <= 28 && newVersion > 28) {
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant
                    .MESSAGE_DIRECT_LINK_MEDIA)) {
                Log.i(TAG, "alter message directlink to message table");
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_DIRECT_LINK_MEDIA);
            }
        }
        if (oldVersion <= 29 && newVersion > 29) {
            if (!checkExistColumnInTable(db, ThreadMessageConstant.THREAD_TABLE, ThreadMessageConstant
                    .THREAD_GROUP_AVATAR)) {
                db.execSQL(ThreadMessageConstant.ALTER_COLUMN_THREAD_GROUP_AVATAR);
            }
        }
        if (oldVersion <= 30 && newVersion > 30) {
            db.execSQL(ReengMessageConstant.CREATE_INDEX);
        }
        if (oldVersion <= 31 && newVersion > 31) {
            if (!checkExistColumnInTable(db, ThreadMessageConstant.THREAD_TABLE, ThreadMessageConstant
                    .THREAD_LAST_MESSAGE_ID)) {
                Log.d(TAG, "alter last_message_id to thread table");
                db.execSQL(ThreadMessageConstant.ALTER_COLUMN_LAST_MESSAGE_ID);
            }
        }
        //merge tu nhanh sticker
        if (oldVersion <= 32 && newVersion > 32) {
            if (!checkExistColumnInTable(db, StickerConstant.COLLECTION_TABLE, StickerConstant.COLLECTION_IS_DEFAULT)) {
                Log.i(TAG, "alter is default table sticker collection");
                db.execSQL(StickerConstant.ALTER_COLUMN_DEFAULT);
                db.execSQL(StickerConstant.ALTER_COLUMN_UPDATE);
                db.execSQL(StickerConstant.ALTER_COLUMN_PREFIX);
                db.execSQL(StickerConstant.ALTER_COLUMN_STICKY);
                db.execSQL(StickerConstant.ALTER_COLUMN_SERVER_UPDATE);
                db.execSQL(StickerConstant.ALTER_COLUMN_ITEM_UPDATE);
            }
        }

        if (oldVersion <= 33 && newVersion > 33) {
            if (!checkExistColumnInTable(db, ThreadMessageConstant.THREAD_TABLE, ThreadMessageConstant
                    .THREAD_ADMIN_NUMBERS)) {
                Log.d(TAG, "alter admin numbers to thread table");
                db.execSQL(ThreadMessageConstant.ALTER_COLUMN_ADMIN_NUMBERS);
            }
        }

        if (oldVersion <= 34 && newVersion > 34) {
            Log.i(TAG, "create table image profile");
            db.execSQL(ImageProfileConstant.CREATE_STATEMENT);
            if (!checkExistColumnInTable(db, NonContactConstant.NON_CONTACT_TABLE,
                    NonContactConstant.NON_CONTACT_COVER)) {
                Log.i(TAG, "alter cover string table nonContact");
                db.execSQL(NonContactConstant.ALTER_COLUMN_COVER);
            }
            if (!checkExistColumnInTable(db, NonContactConstant.NON_CONTACT_TABLE,
                    NonContactConstant.NON_CONTACT_NICK_NAME)) {
                Log.i(TAG, "alter nickname string table nonContact");
                db.execSQL(NonContactConstant.ALTER_COLUMN_NICK_NAME);
            }
            if (!checkExistColumnInTable(db, NumberConstant.TABLE, NumberConstant.COVER)) {
                Log.i(TAG, "alter cover table number");
                db.execSQL(NumberConstant.ALTER_COLUMN_COVER);
            }
            if (!checkExistColumnInTable(db, NumberConstant.TABLE, NumberConstant.NICK_NAME)) {
                Log.i(TAG, "alter albums table number");
                db.execSQL(NumberConstant.ALTER_COLUMN_NICK_NAME);
            }
        }
        // nang version 36 nhung khong lam gi, bo qua len 37
        if (oldVersion <= 36 && newVersion > 36) {
            if (!checkExistColumnInTable(db, NumberConstant.TABLE, NumberConstant.PERMISSION)) {
                db.execSQL(NumberConstant.ALTER_COLUMN_PERMISSION);
            }
            if (!checkExistColumnInTable(db, NonContactConstant.NON_CONTACT_TABLE,
                    NonContactConstant.NON_CONTACT_PERMISSION)) {
                db.execSQL(NonContactConstant.ALTER_COLUMN_PERMISSION);
            }
            if (!checkExistColumnInTable(db, ReengAccountConstant.TABLE, ReengAccountConstant.PERMISSION)) {
                db.execSQL(ReengAccountConstant.ALTER_COLUMN_PERMISSION);
            }
        }

        if (oldVersion <= 37 && newVersion > 37) {
            if (!checkExistColumnInTable(db, StickerConstant.COLLECTION_TABLE, StickerConstant.COLLECTION_ORDER)) {
                db.execSQL(StickerConstant.ALTER_COLUMN_COLLECTION_ORDER);
            }
        }
        if (oldVersion <= 38 && newVersion > 38) {
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant
                    .MESSAGE_ROOM_INFO)) {
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_ROOM_INFO);
            }
        }
        if (oldVersion <= 39 && newVersion > 39) {
            if (!checkExistColumnInTable(db, StrangerConstant.STRANGER_TABLE, StrangerConstant.STRANGER_CREATE_DATE)) {
                db.execSQL(StrangerConstant.ALTER_COLUMN_CREATE_DATE);
            }
        }
        if (oldVersion <= 40 && newVersion > 40) {
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant
                    .MESSAGE_FILE_ID_NEW)) {
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_FILE_ID_NEW);
            }
            if (!checkExistColumnInTable(db, ImageProfileConstant.TABLE_IMAGE_PROFILE, ImageProfileConstant
                    .ID_SERVER_STRING)) {
                db.execSQL(ImageProfileConstant.ALTER_COLUMN_ID_SERVER_STRING);
            }
        }
        if (oldVersion <= 41 && newVersion > 41) {
            if (!checkExistColumnInTable(db, MediaConstant.TABLE, MediaConstant.CRBT_CODE)) {
                db.execSQL(MediaConstant.ALTER_COLUMN_CRBT_CODE);
                db.execSQL(MediaConstant.ALTER_COLUMN_CRBT_PRICE);
            }
            if (!checkExistColumnInTable(db, TopSongConstant.TABLE, TopSongConstant.CRBT_CODE)) {
                db.execSQL(TopSongConstant.ALTER_COLUMN_CRBT_CODE);
                db.execSQL(TopSongConstant.ALTER_COLUMN_CRBT_PRICE);
            }
        }
        if (oldVersion <= 42 && newVersion > 42) {
//            db.execSQL(MoreAppConstant.CREATE_STATEMENT);
        }
        if (oldVersion <= 43 && newVersion > 43) {
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant
                    .MESSAGE_REPLY_DETAIL)) {
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_MESSAGE_REPLY_DETAIL);
            }
        }
        if (oldVersion <= 44 && newVersion > 44) {
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant
                    .MESSAGE_EXPIRED)) {
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_MESSAGE_EXPIRED);
            }
        }
        if (oldVersion <= 45 && newVersion > 45) {
            db.execSQL(CallHistoryConstant.CREATE_HISTORY_STATEMENT);
            db.execSQL(CallHistoryConstant.CREATE_DETAIL_STATEMENT);
        }
        if (oldVersion <= 46 && newVersion > 46) {
            if (!checkExistColumnInTable(db, ThreadMessageConstant.THREAD_TABLE, ThreadMessageConstant
                    .THREAD_GROUP_CLASS)) {
                Log.d(TAG, "alter group class to thread table");
                db.execSQL(ThreadMessageConstant.ALTER_COLUMN_GROUP_CLASS);
            }
        }
        if (oldVersion <= 47 && newVersion > 47) {
            db.execSQL(NoteMessageContant.CREATE_STATEMENT);
        }

        if (oldVersion <= 48 && newVersion > 48) {
//            db.execSQL(LogConstant.CREATE_STATEMENT);
        }
        if (oldVersion <= 49 && newVersion > 49) {
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant
                    .MESSAGE_TAG_CONTENT)) {
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_MESSAGE_TAG_CONTENT);
            }
        }
        if (oldVersion <= 50 && newVersion > 50) {
            if (!checkExistColumnInTable(db, ReengAccountConstant.TABLE, ReengAccountConstant.AVNO_NUMBER)) {
                db.execSQL(ReengAccountConstant.ALTER_COLUMN_AVNO_NUMBER);
            }
        }
        if (oldVersion <= 51 && newVersion > 51) {
            if (!checkExistColumnInTable(db, ThreadMessageConstant.THREAD_TABLE, ThreadMessageConstant
                    .THREAD_PIN_MESSAGE_JSON)) {
                db.execSQL(ThreadMessageConstant.ALTER_COLUMN_THREAD_PIN_MESSAGE_JSON);
            }
        }
        if (oldVersion <= 52 && newVersion > 52) {
            db.execSQL(ConfigUserConstant.CREATE_STATEMENT);
            if (!checkExistColumnInTable(db, ReengAccountConstant.TABLE, ReengAccountConstant
                    .AVNO_IDENTIFY_CARD_FRONT)) {
                db.execSQL(ReengAccountConstant.ALTER_COLUMN_AVNO_IC_FRONT);
                db.execSQL(ReengAccountConstant.ALTER_COLUMN_AVNO_IC_BACK);
            }
        }
        if (oldVersion <= 53 && newVersion > 53) {
            db.execSQL(VideoConstant.CREATE_STATEMENT);
        }
        if (oldVersion <= 54 && newVersion > 54) {
            /*db.execSQL(LogConstant.CREATE_STATEMENT);
            if (!checkExistColumnInTable(db, LogConstant.TABLE, LogConstant.TYPE)) {
                db.execSQL(LogConstant.ALTER_COLUMN_TYPE);
                db.execSQL(LogConstant.ALTER_COLUMN_COMMENT);
                db.execSQL(LogConstant.ALTER_COLUMN_INFO);
            }*/
        }
        if (oldVersion <= 55 && newVersion > 55) {
            db.execSQL(LogConstant.DROP_STATEMENT_OLD);
            db.execSQL(LogConstant.CREATE_STATEMENT);
        }

        if (oldVersion <= 56 && newVersion > 56) {
            db.execSQL(ChannelDataConstants.CREATE_STATEMENT);
        }
        if (oldVersion <= 57 && newVersion > 57) {
            db.execSQL(MessageImageDBConstant.CREATE_STATEMENT);
        }
        if (oldVersion <= 58 && newVersion > 58) {
            if (!checkExistColumnInTable(db, MessageImageDBConstant.TABLE, MessageImageDBConstant.DIGIT)) {
                db.execSQL(MessageImageDBConstant.ALTER_COLUMN_DIGIT);
            }
        }
        if (oldVersion <= 59 && newVersion > 59) {
            if (!checkExistColumnInTable(db, NonContactConstant.NON_CONTACT_TABLE, NonContactConstant.NON_CONTACT_OPERATOR)) {
                db.execSQL(NonContactConstant.ALTER_COLUMN_OPERATOR);
            }
            if (!checkExistColumnInTable(db, NumberConstant.TABLE, NumberConstant.OPERATOR)) {
                db.execSQL(NumberConstant.ALTER_COLUMN_OPERATOR);
            }
        }
        if (oldVersion <= 60 && newVersion > 60) {
            db.execSQL(PollConstant.CREATE_STATEMENT);

            if (!checkExistColumnInTable(db, ThreadMessageConstant.THREAD_TABLE, ThreadMessageConstant.THREAD_LAST_TIME_PIN_THREAD)) {
                db.execSQL(ThreadMessageConstant.ALTER_COLUMN_THREAD_LAST_TIME_PIN_THREAD);
            }
        }

        if (oldVersion <= 61 && newVersion > 61) {
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant.MESSAGE_REACTION)) {
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_MESSAGE_REACTION);
                db.execSQL(EventMessageConstant.ALTER_COLUMN_REACTION);
            }
        }

        if (oldVersion <= 62 && newVersion > 62) {
            if (!checkExistColumnInTable(db, OfficerAccountConstant.OFFICER_TABLE, OfficerAccountConstant.SERVICE_ACTION)) {
                db.execSQL(OfficerAccountConstant.ALTER_COLUMN_SERVICE_ACTION);
            }
        }

        if (oldVersion <= 63 && newVersion > 63) {
            if (!checkExistColumnInTable(db, ThreadMessageConstant.THREAD_TABLE, ThreadMessageConstant.THREAD_HIDDEN)) {
                db.execSQL(ThreadMessageConstant.ALTER_COLUMN_THREAD_HIDDEN);
            }
        }

        if (oldVersion <= 64 && newVersion > 64) {
            if (!checkExistColumnInTable(db, ReengAccountConstant.TABLE, ReengAccountConstant.PREKEY)) {
                db.execSQL(ReengAccountConstant.ALTER_COLUMN_PREKEY);
            }
            if (!checkExistColumnInTable(db, NumberConstant.TABLE, NumberConstant.PREKEY)) {
                db.execSQL(NumberConstant.ALTER_COLUMN_PREKEY);
            }
            if (!checkExistColumnInTable(db, NonContactConstant.NON_CONTACT_TABLE, NonContactConstant.NON_CONTACT_PREKEY)) {
                db.execSQL(NonContactConstant.ALTER_COLUMN_PREKEY);
            }
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant.MESSAGE_TARGET_ID_E2E)) {
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_MESSAGE_TARGET_ID_E2E);
            }
        }

        if (oldVersion <= 65 && newVersion > 65) {
            if (!checkExistColumnInTable(db, ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant.MESSAGE_STATE_MY_REACTION)) {
                db.execSQL(ReengMessageConstant.ALTER_COLUMN_MESSAGE_MY_REACTION);
            }
        }




        /*
         * chú ý nâng ReengDatabaseConstant.DATABASE_VERSION khi thêm bảng
         */

        Log.i("sql", "on create table");

    }

    public static synchronized ReengSQLiteHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ReengSQLiteHelper(context);
        }
        return mInstance;
    }

    public void closeDatabaseConnection(SQLiteDatabase database) {
        try {
            if (database != null) {
                database.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "closeDatabaseConnection", e);
        }
    }

    public static synchronized SQLiteDatabase getMyWritableDatabase() {
        if ((mWritableDb == null) || (!mWritableDb.isOpen())) {
            mWritableDb = mInstance.getWritableDatabase();
        }
        return mWritableDb;
    }

    public static synchronized SQLiteDatabase getMyReadableDatabase() {
        if ((mReadableDb == null) || (!mReadableDb.isOpen())) {
            mReadableDb = mInstance.getReadableDatabase();
        }
        return mReadableDb;
    }

    private boolean checkExistColumnInTable(SQLiteDatabase db, String tableName, String columnName) {
        Cursor cur = null;
        boolean isExist = false;
        try {
            cur = db.rawQuery("PRAGMA table_info(" + tableName + ")", null);
            if (cur != null) {
                if (cur.moveToFirst()) {
                    do {
                        String name = cur.getString(1);
                        if (columnName != null && columnName.equals(name)) {
                            isExist = true;
                            break;
                        }
                    } while (cur.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            isExist = false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return isExist;
    }
}
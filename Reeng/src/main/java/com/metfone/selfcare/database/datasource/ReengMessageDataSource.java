package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.backup_restore.BackupMessageModel;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thaodv on 7/1/2014.
 */
public class ReengMessageDataSource {
    private static String TAG = ReengMessageDataSource.class.getSimpleName();
    private static ReengMessageDataSource reengMessageDataSource;
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;

    private ReengMessageDataSource(ApplicationController application) {
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
    }

    public static synchronized ReengMessageDataSource getInstance(ApplicationController application) {
        if (reengMessageDataSource == null) {
            reengMessageDataSource = new ReengMessageDataSource(application);
        }
        return reengMessageDataSource;
    }

    public long createMessage(ReengMessage message) {
        try {
            ContentValues values = getContentValuesMessage(message);
            long id = databaseWrite.insert(ReengMessageConstant.MESSAGE_TABLE, null, values);
            Log.d(TAG, "create new message id = " + id);
            return id;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return 0;
        }
    }

    public void insertNewListMessage(List<ReengMessage> listMessage) {
        if (listMessage == null || listMessage.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (ReengMessage message : listMessage) {
                    values = getContentValuesMessage(message);
                    long id = databaseWrite.insert(ReengMessageConstant.MESSAGE_TABLE, null, values);
                    message.setId((int) id);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public CopyOnWriteArrayList<ReengMessage> getLimitMessagesOfThread_V2(int threadId, int limit,
                                                                          MessageBusiness messageBusiness,
                                                                          int firstOldMsgId) {
        CopyOnWriteArrayList<ReengMessage> list = new CopyOnWriteArrayList<>();
        Cursor cursor = null;
        String where;
        if (firstOldMsgId == -1) {// chua tung load message hoac thread ko co message thi lay  "limit" message
            where = String.format(ReengMessageConstant.MESSAGE_SELECT_LIMIT_QUERY_STATEMENT,
                    threadId, ReengMessageConstant.MESSAGE_ID, limit);
        } else {
            where = String.format(ReengMessageConstant.MESSAGE_SELECT_MORE_BY_FIRST_MSG_ID_QUERY_STATEMENT,
                    threadId, firstOldMsgId, ReengMessageConstant.MESSAGE_ID, limit);
        }
        // Select all
        try {
            cursor = databaseRead.rawQuery(where, null);
            //ReengMessageConstant.MESSAGE_THREAD_ID + " = " + threadId
            // if (cursor.moveToFirst()) {
            if (cursor.moveToLast()) {
                do {
                    ReengMessage message = getMessageFromCursor(cursor);
                    list.add(message);
                    //Log.d(TAG, "message: " + message.toString());
                    messageBusiness.checkMessageWhenLoadFromDB(message);
                } while (cursor.moveToPrevious());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public CopyOnWriteArrayList<ReengMessage> getMessagesWithinIds(int threadId, int startId, int endId) {
        CopyOnWriteArrayList<ReengMessage> list = new CopyOnWriteArrayList<>();
        Cursor cursor = null;
        String where = String.format(ReengMessageConstant.MESSAGE_SELECT_NEAREST_QUERY_STATEMENT,
                threadId, startId, endId, ReengMessageConstant.MESSAGE_ID);

        try {
            cursor = databaseRead.rawQuery(where, null);
            if (cursor.moveToLast()) {
                do {
                    ReengMessage message = getMessageFromCursor(cursor);
                    list.add(message);
                } while (cursor.moveToPrevious());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public ArrayList<Integer> getAllMessagesIdsByThreadId(int threadId) {
        ArrayList<Integer> list = new ArrayList<>();
        Cursor cursor = null;
        String where = String.format(ReengMessageConstant.MESSAGE_SELECT_ALL_IDS_QUERY_STATEMENT,
                threadId, ReengMessageConstant.MESSAGE_ID);

        // Select all
        try {
            cursor = databaseRead.rawQuery(where, null);
            //ReengMessageConstant.MESSAGE_THREAD_ID + " = " + threadId
            // if (cursor.moveToFirst()) {
            if (cursor.moveToLast()) {
                do {
                    int messageId = cursor.getInt(0);
                    list.add(messageId);
                } while (cursor.moveToPrevious());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public CopyOnWriteArrayList<ReengMessage> searchMessagesByTag(int threadId, String tag) {
        CopyOnWriteArrayList<ReengMessage> list = new CopyOnWriteArrayList<>();
        Cursor cursor = null;
        String where = String.format(ReengMessageConstant.MESSAGE_SEARCH_QUERY_STATEMENT,
                threadId, "%" + tag + "%", ReengMessageConstant.MESSAGE_ID);

        // Select all
        try {
            cursor = databaseRead.rawQuery(where, null);
            //ReengMessageConstant.MESSAGE_THREAD_ID + " = " + threadId
            // if (cursor.moveToFirst()) {
            if (cursor.moveToLast()) {
                do {
                    ReengMessage message = getMessageFromCursor(cursor);
                    list.add(message);
                } while (cursor.moveToPrevious());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public CopyOnWriteArrayList<ReengMessage> getLastMessagesOfThread(int messageId, MessageBusiness messageBusiness) {
        CopyOnWriteArrayList<ReengMessage> list = new CopyOnWriteArrayList<>();
        Cursor cursor = null;
        try {
            cursor = databaseRead.query(ReengMessageConstant.MESSAGE_TABLE,
                    null,
                    ReengMessageConstant.MESSAGE_ID + " = " + messageId,
                    null, null, null, null, null);
            // if (cursor.moveToFirst()) {
            if (cursor.moveToLast()) {
                do {
                    ReengMessage message = getMessageFromCursor(cursor);
                    list.add(message);
                    messageBusiness.checkMessageWhenLoadFromDB(message);
                } while (cursor.moveToPrevious());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public CopyOnWriteArrayList<ReengMessage> getLimitMessagesOfThread(int threadId, int limit,
                                                                       int offset) {
        CopyOnWriteArrayList<ReengMessage> list = new CopyOnWriteArrayList<>();
        Cursor cursor = null;
        // Select all
        try {
            cursor = databaseRead.query(ReengMessageConstant.MESSAGE_TABLE,
                    null,
                    ReengMessageConstant.MESSAGE_THREAD_ID + " = " + threadId,
                    null, null, null, ReengMessageConstant.MESSAGE_ID
                            + " DESC", "" + limit
            );
            //if (cursor.moveToFirst()) {
            if (cursor.moveToLast()) {
                do {
                    list.add(getMessageFromCursor(cursor));
                } while (cursor.moveToPrevious());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    private ReengMessage getMessageFromCursor(Cursor cursor) {
        ReengMessage message = new ReengMessage();
        // set id
        message.setId(cursor.getInt(0));
        // set content
        message.setContent(cursor.getString(1));
        // set is read
        int readState = cursor.getInt(2);
        message.setNewMessage(readState == ReengMessageConstant.READ_STATE_UNREAD);
        message.setReadState(readState);
        // set sender
        message.setSender(cursor.getString(3));
        // set receiver
        message.setReceiver(cursor.getString(4));
        // set time
        message.setTime(cursor.getLong(5));
        // set thread id
        message.setThreadId(cursor.getInt(6));
        // set sent
        message.setSent(cursor.getInt(7) != 0);
        // set file type
        message.setFileType(cursor.getString(8));
        // set packet id
        message.setPacketId(cursor.getString(9));
        // set status
        message.setStatusFromDb(cursor.getInt(10));
        //
        message.setDuration(cursor.getInt(11));
        //
        message.setFilePath(cursor.getString(12));
        message.setMessageType(ReengMessageConstant.MessageType.fromString(cursor.getString(13)));
        message.setSongId(cursor.getInt(14));
        message.setChatMode(cursor.getInt(15));
        message.setFileName(cursor.getString(16));
        message.setSize(cursor.getInt(17));
        message.setVideoContentUri(cursor.getString(18));
        message.setDirection(ReengMessageConstant.Direction.fromString(cursor.getString(19)));
        message.setMusicState(cursor.getInt(20));
        message.setImageUrl(cursor.getString(21));
        message.setDirectLinkMedia(cursor.getString(22));
        message.setRoomInfo(cursor.getString(23));
        message.setFileId(cursor.getString(24));
        message.setReplyDetail(cursor.getString(25));
        message.setExpired(cursor.getLong(26));
        message.setTagContent(cursor.getString(27));
        message.setListReaction(convertStringToArray(cursor.getString(28)));
        message.setMessageEncrpyt(cursor.getString(29));
        message.setStateMyReaction(cursor.getInt(30));
        return message;
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    /**
     * chuyen obj message sang content values
     *
     * @param message
     * @return contentValues
     */
    private ContentValues getContentValuesMessage(ReengMessage message) {
        ContentValues values = new ContentValues();
        values.put(ReengMessageConstant.MESSAGE_CONTENT, message.getContent());
        values.put(ReengMessageConstant.MESSAGE_RECEIVER, message.getReceiver());
        values.put(ReengMessageConstant.MESSAGE_SENDER, message.getSender());
        values.put(ReengMessageConstant.MESSAGE_IS_READ, message.getReadState());
        values.put(ReengMessageConstant.MESSAGE_THREAD_ID, message.getThreadId());
        values.put(ReengMessageConstant.MESSAGE_TIME, message.getTime());
        values.put(ReengMessageConstant.MESSAGE_IS_SENT, message.isSent());
        values.put(ReengMessageConstant.MESSAGE_FILE_TYPE, message.getFileType());
        values.put(ReengMessageConstant.MESSAGE_PACKET_ID, message.getPacketId());
        values.put(ReengMessageConstant.MESSAGE_STATUS, message.getStatus());
        values.put(ReengMessageConstant.MESSAGE_DURATION, message.getDuration());
        values.put(ReengMessageConstant.MESSAGE_LOCAL_FILE_PATH,
                message.getFilePath());
        values.put(ReengMessageConstant.MESSAGE_TYPE, message.getMessageType()
                .toString());
        values.put(ReengMessageConstant.MESSAGE_SONG_ID, message.getSongId());
        values.put(ReengMessageConstant.MESSAGE_CHAT_MODE, message.getChatMode());
        values.put(ReengMessageConstant.MESSAGE_FILE_NAME, message.getFileName());
        values.put(ReengMessageConstant.MESSAGE_FILE_SIZE, message.getSize());
        values.put(ReengMessageConstant.MESSAGE_VIDEO_CONTENT_URI,
                message.getVideoContentUri());
        values.put(ReengMessageConstant.MESSAGE_DIRECTION, message.getDirection().toString());
        values.put(ReengMessageConstant.MESSAGE_MUSIC_STATE, message.getMusicState());
        values.put(ReengMessageConstant.MESSAGE_IMAGE_URL, message.getImageUrl());
        values.put(ReengMessageConstant.MESSAGE_DIRECT_LINK_MEDIA, message.getDirectLinkMedia());
        values.put(ReengMessageConstant.MESSAGE_ROOM_INFO, message.getRoomInfo());
        values.put(ReengMessageConstant.MESSAGE_FILE_ID_NEW, message.getFileId());
        values.put(ReengMessageConstant.MESSAGE_REPLY_DETAIL, message.getReplyDetail());
        values.put(ReengMessageConstant.MESSAGE_EXPIRED, message.getExpired());
        values.put(ReengMessageConstant.MESSAGE_TAG_CONTENT, message.getTagContent());
        values.put(ReengMessageConstant.MESSAGE_REACTION, convertArrayToString(message.getListReaction()));
        values.put(ReengMessageConstant.MESSAGE_TARGET_ID_E2E, message.getMessageEncrpyt());
        values.put(ReengMessageConstant.MESSAGE_STATE_MY_REACTION, message.getStateMyReaction());
        return values;
    }

    /**
     * update one reeng message
     *
     * @param message
     */
    public void updateMessage(ReengMessage message) {
        ContentValues values = getContentValuesMessage(message);
        String whereClause = ReengMessageConstant.MESSAGE_ID + " = "
                + message.getId();
        databaseWrite.update(ReengMessageConstant.MESSAGE_TABLE, values, whereClause, null);
    }

    /**
     * update list reeng message
     *
     * @param listMessage
     */
    public void updateListMessage(List<ReengMessage> listMessage) {
        if (listMessage == null || listMessage.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (ReengMessage message : listMessage) {
                    values = getContentValuesMessage(message);
                    String whereClause = ReengMessageConstant.MESSAGE_ID + " = "
                            + message.getId();
                    databaseWrite.update(ReengMessageConstant.MESSAGE_TABLE, values, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception updateListMessage", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public ReengMessage findMessageInDBByPacketId(String packetId) {
        ReengMessage message = null;
        Cursor cursor = null;
        try {
            String query = ReengMessageConstant.MESSAGE_SELECT_BY_PACKET_ID_QUERY
                    + "'%" + packetId + "%'";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    message = getMessageFromCursor(cursor);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return message;
    }

    public ArrayList<ReengMessage> getImageMessageOfThread(String threadId, boolean isDesc) {
        ArrayList<ReengMessage> ret = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query;
            if (isDesc) {
                query = String.format(ReengMessageConstant.IMAGE_MESSAGE_SELECT_BY_THREAD_ID_SORT_DESC, threadId);
            } else {
                query = String.format(ReengMessageConstant.IMAGE_MESSAGE_SELECT_BY_THREAD_ID, threadId);
            }

            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    ret.add(getMessageFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        } finally {
            closeCursor(cursor);
        }
        return ret;
    }

    public ReengMessage findMessageInDBByLocalFilePath(String filePath) {
        ReengMessage message = null;
        Cursor cursor = null;
        try {
            String query = ReengMessageConstant.MESSAGE_SELECT_BY_LOCAL_FILE_PATH_QUERY
                    + "'%" + filePath + "%'";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    message = getMessageFromCursor(cursor);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return message;
    }


    public int getNumberOfUnreadMessage(int threadId) {
        Cursor cursor = null;
        int count = 0;
        try {
            String query = ReengMessageConstant.MESSAGE_SELECT_UNREAD_QUERY_BY_THREAD + threadId;
            cursor = databaseRead.rawQuery(query, null);
            count = cursor.getCount();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return count;
    }

    public void markAllMessageIsRead(int mThreadId) {
        ContentValues values = new ContentValues();
        values.put(ReengMessageConstant.MESSAGE_IS_READ, 1);
        String whereClause = ReengMessageConstant.MESSAGE_THREAD_ID + " = " + mThreadId;
        databaseWrite.update(ReengMessageConstant.MESSAGE_TABLE, values, whereClause, null);
    }

    public void markAllMessageIsReadState(int mThreadId, int state) {
        ContentValues values = new ContentValues();
        values.put(ReengMessageConstant.MESSAGE_IS_READ, state);
        String whereClause = ReengMessageConstant.MESSAGE_THREAD_ID + " = " + mThreadId;
        databaseWrite.update(ReengMessageConstant.MESSAGE_TABLE, values, whereClause, null);
    }

    public void markAllMessageIsReadState(int threadId, int state, int limit) {
        ContentValues values = new ContentValues();
        values.put(ReengMessageConstant.MESSAGE_IS_READ, state);
        String whereClause = ReengMessageConstant.MESSAGE_ID + " IN (SELECT " + ReengMessageConstant.MESSAGE_ID
                + " FROM " + ReengMessageConstant.MESSAGE_TABLE + " WHERE " + ReengMessageConstant.MESSAGE_THREAD_ID
                + " = " + threadId + " ORDER BY " + ReengMessageConstant.MESSAGE_ID + " DESC LIMIT " + limit + ")";
        Log.d(TAG, "markReadMessage whereClause: " + whereClause);
        databaseWrite.update(ReengMessageConstant.MESSAGE_TABLE, values, whereClause, null);
    }

    public void deleteListMessage(int mThreadId) {
        String whereClause = ReengMessageConstant.MESSAGE_THREAD_ID + " = " + mThreadId;
        databaseWrite.delete(ReengMessageConstant.MESSAGE_TABLE, whereClause, null);
    }

    public void deleteListMessageOfListThread(ArrayList<ThreadMessage> listThread) {
        try {
            databaseWrite.beginTransaction();
            try {
                String whereClause;
                for (ThreadMessage thread : listThread) {
                    whereClause = ReengMessageConstant.MESSAGE_THREAD_ID + " = " + thread.getId();
                    databaseWrite.delete(ReengMessageConstant.MESSAGE_TABLE, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void deleteAMessage(int id) {
        databaseWrite.delete(ReengMessageConstant.MESSAGE_TABLE, ReengMessageConstant.MESSAGE_ID
                + " = " + id, null);
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(ReengMessageConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    // get all message unread
    public ArrayList<ReengMessage> getMessagesUnreadInThread(int threadID) {
        ArrayList<ReengMessage> list = new ArrayList<>();
        Cursor cursor = null;
        // Select all
        try {
            cursor = databaseRead.query(ReengMessageConstant.MESSAGE_TABLE,
                    null,
                    ReengMessageConstant.MESSAGE_IS_READ + " = " + ReengMessageConstant.READ_STATE_UNREAD + " AND "
                            + ReengMessageConstant.MESSAGE_THREAD_ID + " = " + threadID,
                    null, null, null, ReengMessageConstant.MESSAGE_ID
                            + " DESC", null
            );
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    list.add(getMessageFromCursor(cursor));
                } while (cursor.moveToNext());
            }
            Collections.reverse(list);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    /**
     * get message cuoi cung trong db
     *
     * @param threadId
     * @return
     */
    public ReengMessage getLastMessageByThreadId(int threadId) {
        Cursor cursor = null;
        ReengMessage message = null;
        try {
            String where = String.format(ReengMessageConstant.MESSAGE_SELECT_LIMIT_QUERY_STATEMENT,
                    threadId, ReengMessageConstant.MESSAGE_ID, 1);
            cursor = databaseRead.rawQuery(where, null);
            if (cursor.moveToFirst()) {
                message = getMessageFromCursor(cursor);
            }
        } catch (Exception e) {
            Log.e(TAG, "getLastMessageByThreadId", e);
        } finally {
            closeCursor(cursor);
        }
        return message;
    }

    public ArrayList<String> getPacketIdCheckDuplicate(int size) {
        Cursor cursor = null;
        ArrayList<String> packetIds = new ArrayList<>();
        try {
            String query = String.format(ReengMessageConstant.MESSAGE_SELECT_LIMIT_PACKETID_QUERY_STATEMENT, size);
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    packetIds.add(cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return packetIds;
    }


    public ReengMessage getLastPinMessageOfThread(String threadId) {
        ReengMessage pinMessage = null;
        Cursor cursor = null;
        try {
            String query = String.format(ReengMessageConstant.PIN_MESSAGE_SELECT_BY_THREAD_ID, threadId);
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                pinMessage = getMessageFromCursor(cursor);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        } finally {
            closeCursor(cursor);
        }
        return pinMessage;
    }


    public ArrayList<ReengMessage> getListMessageUnknown() {
        ArrayList<ReengMessage> ret = new ArrayList<>();
        Cursor cursor = null;
        try {

            cursor = databaseRead.rawQuery(ReengMessageConstant.QUERY_ALL_UNKNOWN_MESSAGE, null);
            if (cursor.moveToFirst()) {
                do {
                    ret.add(getMessageFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        } finally {
            closeCursor(cursor);
        }
        return ret;
    }

    public int updateListMessageChangeNumber(int threadId, String oldNumber, String newNumber) {
        int update = -1;
        try {
            ContentValues values = new ContentValues();
            values.put(ReengMessageConstant.MESSAGE_SENDER, newNumber);
            String whereClause = ReengMessageConstant.MESSAGE_THREAD_ID + "=" + threadId + " AND "
                    + ReengMessageConstant.MESSAGE_SENDER + "=\'" + oldNumber + "\'";
            update = databaseWrite.update(ReengMessageConstant.MESSAGE_TABLE, values, whereClause, null);

        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        } finally {
        }
        return update;
    }

    public int updateListMessageChangeNumber(String oldNumber, String newNumber) {
        int update = -1;
        try {
            ContentValues values = new ContentValues();
            values.put(ReengMessageConstant.MESSAGE_SENDER, newNumber);
            String whereClause = ReengMessageConstant.MESSAGE_SENDER + "=\'" + oldNumber + "\'";
            update = databaseWrite.update(ReengMessageConstant.MESSAGE_TABLE, values, whereClause, null);

        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        } finally {
        }
        return update;
    }

    public long getMessageNumber() {
        try {
            return DatabaseUtils.queryNumEntries(databaseRead, ReengMessageConstant.MESSAGE_TABLE);
        } catch (Exception e) {
            return 0;
        }
    }

    public CopyOnWriteArrayList<ReengMessage> getTextMessagesWithinIds(int threadId, int startId, int endId) {
        CopyOnWriteArrayList<ReengMessage> list = new CopyOnWriteArrayList<>();
        Cursor cursor = null;
        String where = String.format(ReengMessageConstant.MESSAGE_BACKUP_NEAREST_QUERY_STATEMENT, threadId, startId, endId, ReengMessageConstant.MESSAGE_ID);
        try {
            cursor = databaseRead.rawQuery(where, null);
            if (cursor.moveToLast()) {
                do {
                    ReengMessage message = getMessageFromCursor(cursor);
                    list.add(message);
                } while (cursor.moveToPrevious());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public void deleteAllMessages() {
        try {
            databaseWrite.execSQL("delete from " + ReengMessageConstant.MESSAGE_TABLE);
            databaseWrite.execSQL("delete from " + ThreadMessageConstant.THREAD_TABLE);
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    public long insertNewListMessage(List<BackupMessageModel> listMessage, int threadId) {
        if (listMessage == null || listMessage.isEmpty()) {
            return -1;
        }
        long lastId = -1;
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (BackupMessageModel message : listMessage) {
                    values = getContentValuesMessage(message, threadId);
                    lastId = databaseWrite.insert(ReengMessageConstant.MESSAGE_TABLE, null, values);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return lastId;
    }

    private ContentValues getContentValuesMessage(BackupMessageModel message, int threadId) {
        ContentValues values = new ContentValues();
        values.put(ReengMessageConstant.MESSAGE_CONTENT, message.getBody());
        values.put(ReengMessageConstant.MESSAGE_RECEIVER, message.getTo());
        values.put(ReengMessageConstant.MESSAGE_SENDER, message.getFrom());
        values.put(ReengMessageConstant.MESSAGE_IS_READ, message.getReadState());
        values.put(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        values.put(ReengMessageConstant.MESSAGE_TIME, message.getT());
        values.put(ReengMessageConstant.MESSAGE_PACKET_ID, message.getpId());
        values.put(ReengMessageConstant.MESSAGE_STATUS, message.getStatus());
        values.put(ReengMessageConstant.MESSAGE_TYPE, "text");
        values.put(ReengMessageConstant.MESSAGE_CHAT_MODE, message.getcMode());
        ReengMessageConstant.Direction direction = message.getFrom().equals(ApplicationController.self().getReengAccountBusiness().getJidNumber()) ? ReengMessageConstant.Direction.send : ReengMessageConstant.Direction.received;
        values.put(ReengMessageConstant.MESSAGE_DIRECTION, direction.toString());
        values.put(ReengMessageConstant.MESSAGE_REPLY_DETAIL, message.getReply());
        values.put(ReengMessageConstant.MESSAGE_TAG_CONTENT, message.getTag());
        return values;
    }

    public CopyOnWriteArrayList<BackupMessageModel> getTextMessagesWithinIds(ThreadMessage threadMessage, int startId, int endId) {
        CopyOnWriteArrayList<BackupMessageModel> list = new CopyOnWriteArrayList<>();
        Cursor cursor = null;
        StringBuilder projection = new StringBuilder();
        projection.append(ReengMessageConstant.MESSAGE_PACKET_ID).append(",");
        projection.append(ReengMessageConstant.MESSAGE_SENDER).append(",");
        projection.append(ReengMessageConstant.MESSAGE_RECEIVER).append(",");
        projection.append(ReengMessageConstant.MESSAGE_CHAT_MODE).append(",");
        projection.append(ReengMessageConstant.MESSAGE_CONTENT).append(",");
        projection.append(ReengMessageConstant.MESSAGE_REPLY_DETAIL).append(",");
        projection.append(ReengMessageConstant.MESSAGE_TIME).append(",");
        projection.append(ReengMessageConstant.MESSAGE_TAG_CONTENT).append(",");
        projection.append(ReengMessageConstant.MESSAGE_STATUS).append(",");
        projection.append(ReengMessageConstant.MESSAGE_IS_READ).append(",");
        projection.append(ReengMessageConstant.MESSAGE_DIRECTION);
        String where = String.format(ReengMessageConstant.MESSAGE_BACKUP_NEAREST_QUERY_PROJECTION_STATEMENT, projection.toString(), threadMessage.getId(), startId, endId, ReengMessageConstant.MESSAGE_ID);
        try {
            cursor = databaseRead.rawQuery(where, null);
            if (cursor.moveToLast()) {
                do {
                    BackupMessageModel message = getBackupMessageFromCursor(cursor, threadMessage);
                    list.add(message);
                } while (cursor.moveToPrevious());
            }
        } catch (Exception e) {
            Log.e(TAG, "getLimitMessagesOfThread", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }


    private BackupMessageModel getBackupMessageFromCursor(Cursor cursor, ThreadMessage threadMessage) {
        BackupMessageModel message = new BackupMessageModel();
        message.setpId(cursor.getString(0));
        message.setFrom(cursor.getString(1));
        message.setTo(cursor.getString(2));
        message.setcMode(cursor.getInt(3));
        message.setBody(cursor.getString(4));
        message.setReply(cursor.getString(5));
        message.setT(cursor.getLong(6));
        message.setTag(cursor.getString(7));
        message.setStatus(cursor.getInt(8));
        message.setReadState(cursor.getInt(9));
        String direction = cursor.getString(10);

        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT || threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            message.setSvId(threadMessage.getServerId());
        } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            if (direction.equals("send")) {
                message.setSvId(message.getTo());
            } else if (direction.equals("received")) {
                message.setSvId(message.getFrom());
            }
        }
        return message;
    }


    public static String convertArrayToString(ArrayList<Integer> array) {
        if (array != null && array.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (int number : array) {
                sb.append(number).append(",");
            }
            int lengText = sb.length();
            sb.deleteCharAt(lengText - 1);
            return sb.toString();
        } else {
            return "";
        }
    }

    public static ArrayList<Integer> convertStringToArray(String input) {
        ArrayList<Integer> list = new ArrayList<>();
        if (!TextUtils.isEmpty(input)) {
            String[] numbersArray = input.split(",");
            for (String item : numbersArray) {
                if (item.length() > 0) {
                    list.add(Integer.parseInt(item));
                }
            }
        }
        return list;
    }

    public ReengMessage getLastMessageBannerLixiByThreadId(int threadId) {
        Cursor cursor = null;
        ReengMessage message = null;
        try {
            String where = String.format(ReengMessageConstant.MESSAGE_BANNER_LIXI_SELECT_LIMIT_QUERY_STATEMENT,
                    threadId, ReengMessageConstant.MESSAGE_ID, 1);
            cursor = databaseRead.rawQuery(where, null);
            if (cursor.moveToFirst()) {
                Log.i(TAG, "----- cursor: " + cursor);
                message = getMessageFromCursor(cursor);
                Log.i(TAG, "----- msg: " + message);
            }
        } catch (Exception e) {
            Log.e(TAG, "----- getLastMessageBannerLixiByThreadId", e);
        } finally {
            closeCursor(cursor);
            Log.i(TAG, "----- finally msg: " + message);
        }
        return message;
    }
}
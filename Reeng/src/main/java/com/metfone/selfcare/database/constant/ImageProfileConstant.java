package com.metfone.selfcare.database.constant;

/**
 * Created by thanhnt72 on 9/5/2015.
 */
public class ImageProfileConstant {

    public static final int IMAGE_AVATAR = 0;
    public static final int IMAGE_COVER = 1;
    public static final int IMAGE_NORMAL = 2;
    public static final int IMAGE_IC_FRONT = 3;
    public static final int IMAGE_IC_BACK = 4;
    public static final String TYPE_UPLOAD = "image";

    public static final String STRING_IMAGE_AVATAR = "AVATAR";
    public static final String STRING_IMAGE_ALBUM = "ALBUM";

    public static final String TABLE_IMAGE_PROFILE = "TABLE_IMAGE_PROFILE";
    public static final String ID_IMAGE = "ID_IMAGE";
    public static final String TYPE_IMAGE = "TYPE_IMAGE";
    public static final String IMAGE_PATH = "IMAGE_PATH";
    public static final String IMAGE_URL = "IMAGE_URL";
    public static final String IS_UPLOADED = "IS_UPLOADED";
    public static final String TIME = "TIME";
    public static final String ID_SERVER = "ID_SERVER";
    public static final String ID_SERVER_STRING = "ID_SERVER_STRING";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_IMAGE_PROFILE
            + " ("
            + ID_IMAGE + " INTEGER PRIMARY KEY, "
            + TYPE_IMAGE + " INTEGER, "
            + IMAGE_PATH + " TEXT, "
            + IMAGE_URL + " TEXT, "
            + IS_UPLOADED + " INTEGER, "
            + TIME + " TEXT, "
            + ID_SERVER + " INTEGER, "
            + ID_SERVER_STRING + " TEXT "
            + ")";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE_IMAGE_PROFILE;
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE_IMAGE_PROFILE;
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE_IMAGE_PROFILE;
    public static final String SELECT_ALL_IMAGES_PROFILE_STATEMENT = "SELECT * FROM " + TABLE_IMAGE_PROFILE + " WHERE TYPE_IMAGE = " + IMAGE_NORMAL;
    public static final String SELECT_ALL_IMAGES_PROFILE_UPLOADED_STATEMENT =
            "SELECT * FROM " + TABLE_IMAGE_PROFILE + " WHERE TYPE_IMAGE = " + IMAGE_NORMAL +
                    " AND " + IS_UPLOADED + " = 1 ORDER BY " + ID_IMAGE + " DESC ";
    public static final String SELECT_COVER_STATEMENT = "SELECT * FROM " + TABLE_IMAGE_PROFILE + " WHERE TYPE_IMAGE = " + IMAGE_COVER + " ORDER BY " + ID_IMAGE + " DESC LIMIT 1";

    public static final String ALTER_COLUMN_ID_SERVER_STRING = "ALTER TABLE " + TABLE_IMAGE_PROFILE
            + " ADD COLUMN " + ID_SERVER_STRING + " TEXT;";
}

package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 4/18/2017.
 */
public class Common implements Serializable {
    @SerializedName("default_font_name")
    private String defaultFont;
    @SerializedName("default_text_color")
    private String defaultColor;

    public Common() {
    }

    public Common(Common other) {
        if (other != null) {
            this.defaultFont = other.getDefaultFont();
            this.defaultColor = other.getDefaultColor();
        }
    }

    public String getDefaultFont() {
        return defaultFont;
    }

    public void setDefaultFont(String defaultFont) {
        this.defaultFont = defaultFont;
    }

    public String getDefaultColor() {
        return defaultColor;
    }

    public void setDefaultColor(String defaultColor) {
        this.defaultColor = defaultColor;
    }
}

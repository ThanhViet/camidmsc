package com.metfone.selfcare.database.model.message;

import com.google.gson.annotations.SerializedName;

/**
 * Created by thanhnt72 on 4/1/2019.
 */

public class MediaBoxItem {


    @SerializedName("id")
    long id;

    @SerializedName("title")
    String title;

    @SerializedName("url")
    String url;

    @SerializedName("img_url")
    String img;

    @SerializedName("description")
    String desc;

    @SerializedName("type")
    String type;

    @SerializedName("title_type")
    String titleType;

    @SerializedName("time_post")
    long time;

    @SerializedName("is_pin")
    int isPin;

    @SerializedName("is_last")
    boolean isLastItem;

    @SerializedName("is_first")
    boolean isFirstItem;

    @SerializedName("is_new")
    boolean isNewItem;

    @SerializedName("title_header")
    String titleHeader;

    public MediaBoxItem(long id, String title, String url, String img, String desc, String type, String titleType, long time) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.img = img;
        this.desc = desc;
        this.type = type;
        this.titleType = titleType;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getImg() {
        return img;
    }

    public String getDesc() {
        return desc;
    }

    public String getType() {
        return type;
    }

    public String getTitleType() {
        return titleType;
    }

    public long getTime() {
        return time;
    }

    public boolean isLastItem() {
        return isLastItem;
    }

    public void setLastItem(boolean lastItem) {
        isLastItem = lastItem;
    }

    public boolean isFirstItem() {
        return isFirstItem;
    }

    public void setFirstItem(boolean firstItem) {
        isFirstItem = firstItem;
    }

    public boolean isPin() {
        return isPin == 1;
    }

    public boolean isNewItem() {
        return isNewItem;
    }

    public void setNewItem(boolean newItem) {
        isNewItem = newItem;
    }

    public String getTitleHeader() {
        return titleHeader;
    }

    public void setTitleHeader(String titleHeader) {
        this.titleHeader = titleHeader;
    }

    public static class ItemType {
        public static final String VIDEO = "video";
        public static final String MUSIC = "music";
        public static final String MOVIE = "movie";
        public static final String NEWS = "news";
        public static final String DEEPLINK = "deeplink";
    }
}

package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class EmoCollection implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("objects_info")
    private ArrayList<EmoItem> emoItems = new ArrayList<>();

    public EmoCollection() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public ArrayList<EmoItem> getEmoItems() {
        return emoItems;
    }

    public void setEmoItems(ArrayList<EmoItem> emoItems) {
        this.emoItems = emoItems;
    }
}

package com.metfone.selfcare.database.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 1/5/2016.
 */
public class LocationMusic implements Serializable {
    private static final String TAG = LocationMusic.class.getSimpleName();
    @SerializedName("alias")
    private String label;
    @SerializedName("groupId")
    private int groupId;
    @SerializedName("iOrder")
    private int order;
    private boolean selected = false;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TAG).append(" label: ").append(label);
        sb.append(" groupId: ").append(groupId);
        sb.append(" order: ").append(order);
        return sb.toString();
    }
}
package com.metfone.selfcare.database.model;

import android.text.TextUtils;

import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.TextHelper;

import java.io.Serializable;

/**
 * Created by tungt on 2/25/16.
 */
public class LocalSongInfo implements Serializable {
    private long id;
    private String name;
    private String asciiName;
    private String author;
    private String singer;
    private String path;
    private long fileSize;
    private String media_url;
    private boolean uploaded = false;
    private String urlThumb;

    public LocalSongInfo(String name, String author, String path, String singer, long fileSize) {
        this.name = name;
        this.asciiName = TextHelper.getInstant().convertUnicodeToAscci(name).toLowerCase();
        this.author = author;
        this.path = path;
        this.singer = singer;
        this.fileSize = fileSize;
    }

    public LocalSongInfo() {
    }

    public String getName() {
        return name;
    }

    public String getAsciiName() {
        return asciiName;
    }

    public void setName(String name) {
        this.name = name;
        this.asciiName = TextHelper.getInstant().convertUnicodeToAscci(name).toLowerCase();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSinger() {
        return !TextUtils.isEmpty(singer) ? singer : "Various artists";
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileSizeStr() {
        return FileHelper.formatFileSize(fileSize);
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getMediaUrl() {
        return media_url;
    }

    public void setMediaUrl(String media_url) {
        this.media_url = media_url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public String getUrlThumb() {
        return urlThumb;
    }

    public void setUrlThumb(String urlThumb) {
        this.urlThumb = urlThumb;
    }

    public String toString() {
        return "Name: " + name + "\n" +
                "Singer: " + singer + "\n" +
                "size : " + fileSize + "\n" +
                "media_url: " + media_url + "\n" +
                "id: " + id + "\n";
    }
}

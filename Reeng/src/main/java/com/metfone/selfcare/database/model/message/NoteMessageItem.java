package com.metfone.selfcare.database.model.message;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by sonnn00 on 7/17/2017.
 */

public class NoteMessageItem implements Serializable {

    private static final String TAG = AdvertiseItem.class.getSimpleName();
    @SerializedName("id")
    private long id ;

    @SerializedName("thread_type")
    private int thread_type;

    @SerializedName("thread_jid")
    private String thread_jid = "";

    @SerializedName("thread_name")
    private String thread_name = "";

    @SerializedName("thread_avatar")
    private String thread_avatar = "";

    @SerializedName("content")
    private String content = "";

    @SerializedName("stranger_id")
    private String stranger_id = "";

    @SerializedName("timestamp")
    private long timestamp;

    public NoteMessageItem() {
    }

    public NoteMessageItem(int thread_type, String thread_jid, String thread_name, String thread_avatar, String content, String stranger_id, long timestamp) {
        this.thread_type = thread_type;
        this.thread_jid = thread_jid;
        this.thread_name = thread_name;
        this.thread_avatar = thread_avatar;
        this.content = content;
        this.stranger_id = stranger_id;
        this.timestamp = timestamp;
    }


    public void fromJson(String json) {
        try {
            JSONObject object = new JSONObject(json);
            setId(object.optLong("id"));
            setThread_type(object.optInt("thread_type"));
            setThread_jid(object.optString("thread_jid", null));
            setThread_name(object.optString("thread_name", null));
            setThread_avatar(object.optString("thread_avatar", null));
            setContent(object.optString("content", null));
            setStranger_id(object.optString("stranger_id", null));
            setTimestamp(object.optLong("timestamp"));
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getThread_type() {
        return thread_type;
    }

    public void setThread_type(int thread_type) {
        this.thread_type = thread_type;
    }

    public String getThread_jid() {
        return thread_jid;
    }

    public void setThread_jid(String thread_jid) {
        this.thread_jid = thread_jid;
    }

    public String getThread_name() {
        return thread_name;
    }

    public void setThread_name(String thread_name) {
        this.thread_name = thread_name;
    }

    public String getThread_avatar() {
        return thread_avatar;
    }

    public void setThread_avatar(String thread_avatar) {
        this.thread_avatar = thread_avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStranger_id() {
        return stranger_id;
    }

    public void setStranger_id(String stranger_id) {
        this.stranger_id = stranger_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}

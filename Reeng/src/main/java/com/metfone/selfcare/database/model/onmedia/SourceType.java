package com.metfone.selfcare.database.model.onmedia;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tungt on 10/22/2015.
 */
public class SourceType implements Serializable {
    public static final String VIDEO = "video";
    public static final String IMAGE = "image";
    public static final String SONG = "song";
    public static final String NEWS = "news";
    public static final String ALBUM = "album";
    public static final String AUDIO = "audio";

    public static final int TYPE = 0;
    public static final int SITE = 1;
    public static final int ENABLE_SITE = 2;

    private int type;

    @SerializedName("name")
    private String name;

    @SerializedName("url")
    private String url;

    @SerializedName("row_id")
    private byte[] row_id;

    @SerializedName("status")
    private int status;

    public SourceType(String name, String url, byte[] row_id, int status) {
        this.name = name;
        this.url = url;
        this.row_id = row_id;
        this.status = status;
        if(TextUtils.isEmpty(url)){
            type = SourceType.TYPE;
        }else if(status == 0){
            type = SourceType.SITE;
        }else {
            type = SourceType.ENABLE_SITE;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getRow_id() {
        return row_id;
    }

    public void setRow_id(byte[] row_id) {
        this.row_id = row_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void changeStatus(){
        if(status == 0) {
            status = 1;
        }
        else {
            status = 0;
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
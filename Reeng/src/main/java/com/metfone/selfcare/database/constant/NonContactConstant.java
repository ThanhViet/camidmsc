package com.metfone.selfcare.database.constant;

/**
 * Created by thaodv on 5/8/2015.
 */
public class NonContactConstant {
    public static final String NON_CONTACT_TABLE = "non_contact_table";         // bang trang thai so chua luu danh ba
    public static final String NON_CONTACT_ID = "non_contact_id";
    public static final String NON_CONTACT_NUMBER = "non_contact_number";
    public static final String NON_CONTACT_STATE = "non_contact_state";
    public static final String NON_CONTACT_STATUS = "non_contact_status";
    public static final String NON_CONTACT_LAST_AVATAR = "non_contact_last_avatar";
    public static final String NON_CONTACT_GENDER = "non_contact_gender";
    public static final String NON_CONTACT_BIRTHDAY = "non_contact_birthday";
    public static final String NON_CONTACT_LAST_ONLINE = "non_contact_unknown_column_1";
    public static final String NON_CONTACT_LAST_SEEN = "non_contact_unknown_column_2";
    public static final String NON_CONTACT_BIRTHDAY_STRING = "non_contact_birthday_string";
    public static final String NON_CONTACT_COVER = "non_contact_cover";
    public static final String NON_CONTACT_NICK_NAME = "non_contact_albums";// không lưu album vào DB nên sẽ dung cot này lưu nick mocha
    public static final String NON_CONTACT_PERMISSION = "non_contact_permission";
    public static final String NON_CONTACT_OPERATOR = "non_contact_operator";
    public static final String NON_CONTACT_PREKEY = "non_contact_prekey";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + NON_CONTACT_TABLE
            + " ("
            + NON_CONTACT_ID + " INTEGER PRIMARY KEY, "
            + NON_CONTACT_NUMBER + " TEXT, "
            + NON_CONTACT_STATE + " INTEGER, "
            + NON_CONTACT_STATUS + " TEXT, "
            + NON_CONTACT_LAST_AVATAR + " TEXT, "
            + NON_CONTACT_GENDER + " INTEGER, "
            + NON_CONTACT_BIRTHDAY + " TEXT, "
            + NON_CONTACT_LAST_ONLINE + " TEXT, "
            + NON_CONTACT_LAST_SEEN + " TEXT, "
            + NON_CONTACT_BIRTHDAY_STRING + " TEXT, "
            + NON_CONTACT_COVER + " TEXT, "
            + NON_CONTACT_NICK_NAME + " TEXT, "
            + NON_CONTACT_PERMISSION + " INTEGER, "
            + NON_CONTACT_OPERATOR + " TEXT, "
            + NON_CONTACT_PREKEY + " TEXT"
            + ")";

    public static final String SELECT_ALL_QUERY = "SELECT * FROM "
            + NON_CONTACT_TABLE;
    public static final String SELECT_NON_CONTACT_STATEMENT = "SELECT * FROM " + NON_CONTACT_TABLE + " WHERE " + NON_CONTACT_NUMBER + " =?";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + NON_CONTACT_TABLE;
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + NON_CONTACT_TABLE;

    public static final String ALTER_COLUMN_BIRTHDAY_STRING = "ALTER TABLE " + NON_CONTACT_TABLE
            + " ADD COLUMN " + NON_CONTACT_BIRTHDAY_STRING + " TEXT;";
    public static final String ALTER_COLUMN_COVER = "ALTER TABLE " + NON_CONTACT_TABLE
            + " ADD COLUMN " + NON_CONTACT_COVER + " TEXT;";

    public static final String ALTER_COLUMN_NICK_NAME = "ALTER TABLE " + NON_CONTACT_TABLE
            + " ADD COLUMN " + NON_CONTACT_NICK_NAME + " TEXT;";

    public static final String ALTER_COLUMN_PERMISSION = "ALTER TABLE " + NON_CONTACT_TABLE
            + " ADD COLUMN " + NON_CONTACT_PERMISSION + " INTEGER DEFAULT -1;";

    public static final String ALTER_COLUMN_OPERATOR = "ALTER TABLE " + NON_CONTACT_TABLE
            + " ADD COLUMN " + NON_CONTACT_OPERATOR + " TEXT;";

    public static final String ALTER_COLUMN_PREKEY = "ALTER TABLE " + NON_CONTACT_TABLE
            + " ADD COLUMN " + NON_CONTACT_PREKEY + " TEXT;";
}
package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class EmoItem implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("path")
    private String path;
    @SerializedName("width")
    private float width;
    @SerializedName("height")
    private float height;

    public EmoItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getWidth() {
        return (int) width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public int getHeight() {
        return (int) height;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}

package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.UserInfo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 11/30/2016.
 */
public class RestAllFeedContent implements Serializable {

    @SerializedName("code")
    private int code;

    @SerializedName("content")
    private FeedContent feedContent;

    @SerializedName("isLike")
    private int isLike;

    @SerializedName("isShare")
    private int isShare;

    @SerializedName("userLike")
    private ArrayList<UserInfo> userLikes = new ArrayList<>();

    public int getCode() {
        return code;
    }

    public FeedContent getFeedContent() {
        return feedContent;
    }

    public ArrayList<UserInfo> getUserLikes() {
        return userLikes;
    }

    public int getIsLike() {
        return isLike;
    }

    public void setIsLike(int isLike) {
        this.isLike = isLike;
    }

    public int getIsShare() {
        return isShare;
    }

    public void setIsShare(int isShare) {
        this.isShare = isShare;
    }

    public void setUserLikes(ArrayList<UserInfo> userLikes) {
        this.userLikes = userLikes;
    }
}

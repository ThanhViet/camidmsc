package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.restful.AbsResultData;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/28/2015.
 */
public class RestAllFeedAction extends AbsResultData implements Serializable {

    @SerializedName("listComment")
    private ArrayList<FeedAction> data;

    @SerializedName("actioners")
    private ArrayList<UserInfo> userLikes = new ArrayList<>();

    @SerializedName("userLike")
    private ArrayList<UserInfo> userLikesInComment = new ArrayList<>();

    @SerializedName("timeserver")
    private long currentTimeServer;

    @SerializedName("total")
    private long totalLike;

    @SerializedName("isLike")
    private int isLike;

    @SerializedName("isBackList")
    private int isBlackList;

    @SerializedName("ads_url")
    private String adsUrl;

    @SerializedName("ads_image_url")
    private String adsImageUrl;

    public ArrayList<FeedAction> getData() {
        return data;
    }

    public void setData(ArrayList<FeedAction> data) {
        this.data = data;
    }

    public ArrayList<UserInfo> getUserLikes() {
        return userLikes;
    }

    public ArrayList<UserInfo> getUserLikesInComment() {
        return userLikesInComment;
    }

    public long getCurrentTimeServer() {
        return currentTimeServer;
    }

    public long getTotalLike() {
        return totalLike;
    }

    public int getIsLike() {
        return isLike;
    }

    public String getAdsImageUrl() {
        return adsImageUrl;
    }

    public String getAdsUrl() {
        return adsUrl;
    }

    public int getIsBlackList() {
        return isBlackList;
    }
}

package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 4/19/2017.
 */
public class Song implements Serializable {
    @SerializedName("song_id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("media_url")
    private String mediaUrl;

    public Song() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (Song.class != obj.getClass()) {
            return false;
        }
        Song other = (Song) obj;
        String otherId = other.getId();
        String otherName = other.getName();
        // so sanh so va name
        if (otherId != null && otherId.equals(id)) {
            return otherName != null && otherName.equals(name);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((id == null) ? 0 : id.hashCode())
                + ((name == null) ? 0 : name.hashCode());
        //  + ((avatar == null) ? 0 : avatar.hashCode());
        return result;
    }
}

package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 8/11/17.
 */

public class NewsHotDetailModel implements Serializable
{

    @SerializedName("Content")
    @Expose
    private String content;
    @SerializedName("Height")
    @Expose
    private Integer height;
    @SerializedName("Media")
    @Expose
    private String media;
    @SerializedName("Poster")
    @Expose
    private String poster;
    @SerializedName("Type")
    @Expose
    private Integer type;
    @SerializedName("Width")
    @Expose
    private Integer width;
    private final static long serialVersionUID = 8266931797940501935L;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "";
    }

}
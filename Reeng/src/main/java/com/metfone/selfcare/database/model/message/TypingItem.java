package com.metfone.selfcare.database.model.message;

import java.io.Serializable;

/**
 * Created by toanvk2 on 9/27/2017.
 */

public class TypingItem implements Serializable {
    private int iconRes;
    private int titleRes;
    private int type;
    private String key;

    public TypingItem(int type, String key, int iconRes, int titleRes) {
        this.type = type;
        this.key = key;
        this.iconRes = iconRes;
        this.titleRes = titleRes;
    }

    public TypingItem(int type, String key) {
        this.type = type;
        this.key = key;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getIconRes() {
        return iconRes;
    }

    public int getTitleRes() {
        return titleRes;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.database.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Log;

import java.io.Serializable;

/**
 *
 */
public class CommentFeed implements Serializable {
    private static final String TAG = CommentFeed.class.getSimpleName();
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @SerializedName("_id")
    public String id;

    @SerializedName("id")
    protected String st_id = "";

    @SerializedName("comment")
    protected String comment;

    @SerializedName("count_like_status")
    private String count_like_status = "0";

    @SerializedName("count_comment_status")
    private String count_comment_status = "0";

    @SerializedName("count_status")
    private String count_status = "0";

    @Override
    public String toString() {
        return "CommentFeed [id=" + id + ", comment=" + comment
                + ", count_like_status=" + count_like_status
                + ", count_comment_status=" + count_comment_status
                + ", count_status=" + count_status + "]";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCount_like_status() {
        if ("0".equals(count_like_status)) return "";
        return count_like_status;
    }

    public void setCount_like_status(String count_like_status) {
        this.count_like_status = count_like_status;
    }

    public void setCountLikeAdd() {
        try {
            long count = Long.parseLong(count_like_status);
            count++;
            count_like_status = "" + count;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void setCountCommentAdd() {
        try {
            long count = Long.parseLong(count_comment_status);
            count++;
            count_comment_status = "" + count;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public String getCount_comment_status() {
        if ("0".equals(count_comment_status)) return "";
        return count_comment_status;
    }

    public long getCountCommentStatus() {
        try {
            long count = Long.parseLong(count_comment_status);

            return count;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }

        return 0;
    }

    public void setCount_comment_status(String count_comment_status) {
        this.count_comment_status = count_comment_status;
    }

    public String getCount_status() {
        if ("0".equals(count_status)) return " ";
        return count_status;
    }

    public void setCount_status(String count_status) {
        this.count_status = count_status;
    }


}

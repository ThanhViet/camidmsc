package com.metfone.selfcare.database.constant;

/**
 * Created by toanvk2 on 10/12/2016.
 */

public class CallHistoryConstant {
    public static final String HISTORY_TABLE = "call_history";
    public static final String HISTORY_ID = "id";
    public static final String HISTORY_FRIEND = "friend_number";
    public static final String HISTORY_OWNER = "owner_number";
    public static final String HISTORY_DIRECTION = "direction";
    public static final String HISTORY_STATE = "state";
    public static final String HISTORY_CALL_OUT = "call_out";
    public static final String HISTORY_CALL_TIME = "time";
    public static final String HISTORY_CALL_DURATION = "duration";
    public static final String HISTORY_COUNT = "count";
    public static final String HISTORY_COLUMN_1 = "column_1";

    public static final String CREATE_HISTORY_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + HISTORY_TABLE + "("
            + HISTORY_ID + " INTEGER PRIMARY KEY, "
            + HISTORY_FRIEND + " TEXT, "
            + HISTORY_OWNER + " TEXT, "
            + HISTORY_DIRECTION + " INTEGER, "
            + HISTORY_STATE + " INTEGER, "
            + HISTORY_CALL_OUT + " INTEGER, "
            + HISTORY_CALL_TIME + " INTEGER, "
            + HISTORY_CALL_DURATION + " INTEGER, "
            + HISTORY_COUNT + " INTEGER, "
            + HISTORY_COLUMN_1 + " TEXT "
            + ");";
    public static final String DROP_HISTORY_STATEMENT = "DROP TABLE IF EXISTS " + HISTORY_TABLE;
    public static final String SELECT_ALL_HISTORY_STATEMENT = "SELECT * FROM " + HISTORY_TABLE + " ORDER BY " + HISTORY_ID + " DESC";
    public static final String SELECT_TOP_HISTORY_QUERY = "SELECT * FROM " + HISTORY_TABLE + " LIMIT 1";
    public static final String WHERE_CALL_HISTORY_ID = HISTORY_ID + " = ?";
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + HISTORY_TABLE;

    /**
     * history detail table
     */
    public static final String HISTORY_DETAIL_TABLE = "call_history_detail";
    public static final String HISTORY_DETAIL_ID = "id";
    public static final String HISTORY_DETAIL_CALL_ID = "history_id";
    public static final String HISTORY_DETAIL_FRIEND = "friend_number";
    public static final String HISTORY_DETAIL_OWNER = "owner_number";
    public static final String HISTORY_DETAIL_DIRECTION = "direction";
    public static final String HISTORY_DETAIL_STATE = "state";
    public static final String HISTORY_DETAIL_CALL_OUT = "call_out";
    public static final String HISTORY_DETAIL_CALL_TIME = "time";
    public static final String HISTORY_DETAIL_CALL_DURATION = "duration";
    public static final String HISTORY_DETAIL_COLUMN_1 = "column_1";

    public static final String CREATE_DETAIL_STATEMENT = "CREATE TABLE IF NOT EXISTS "
            + HISTORY_DETAIL_TABLE + "("
            + HISTORY_DETAIL_ID + " INTEGER PRIMARY KEY, "
            + HISTORY_DETAIL_CALL_ID + " INTEGER, "
            + HISTORY_DETAIL_FRIEND + " TEXT, "
            + HISTORY_DETAIL_OWNER + " TEXT, "
            + HISTORY_DETAIL_DIRECTION + " INTEGER, "
            + HISTORY_DETAIL_STATE + " INTEGER, "
            + HISTORY_DETAIL_CALL_OUT + " INTEGER, "
            + HISTORY_DETAIL_CALL_TIME + " INTEGER, "
            + HISTORY_DETAIL_CALL_DURATION + " INTEGER, "
            + HISTORY_DETAIL_COLUMN_1 + " TEXT "
            + ");";
    public static final String DROP_DETAIL_STATEMENT = "DROP TABLE IF EXISTS " + HISTORY_DETAIL_TABLE;
    public static final String SELECT_ALL_DETAIL_STATEMENT = "SELECT * FROM " + HISTORY_DETAIL_TABLE;
    public static final String SELECT_DETAIL_BY_CALL_ID_QUERY = "SELECT * FROM "
            + HISTORY_DETAIL_TABLE + " WHERE " + HISTORY_DETAIL_CALL_ID + " = ";
    public static final String DELETE_ALL_DETAIL_STATEMENT = "DELETE FROM " + HISTORY_DETAIL_TABLE;

    public static final int DIRECTION_SEND = 1;
    public static final int DIRECTION_RECEIVED = 2;
    //
    public static final int CALL_FREE = 1;
    public static final int CALL_OUT = 2;
    public static final int CALL_VIDEO = 3;
    //
    public static final int STATE_IN_COMING = 1;
    public static final int STATE_OUT_GOING = 2;
    public static final int STATE_MISS = 3;
    public static final int STATE_CANCELLED = 4;
    public static final int STATE_BUSY = 5;
    public static final int STATE_REJECTED = 6;

    public static final String FRAGMENT_HISTORY_TYPE = "FRAGMENT_HISTORY_TYPE";
    public static final String FRAGMENT_HISTORY_DETAIL_ID = "FRAGMENT_HISTORY_DETAIL_ID";
    public static final String FRAGMENT_CALLOUT_DATA = "FRAGMENT_CALLOUT_DATA";
    public static final String FRAGMENT_CALLOUT_FREE_USER = "FRAGMENT_CALLOUT_FREE_USER";

    public static final int HISTORY_TYPE_DETAIL = 1;
    public static final int HISTORY_TYPE_SUBSCRIPTION = 2;
}

package com.metfone.selfcare.database.model.onmedia;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.URLUtil;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/22/2015.
 */
public class FeedContent implements Serializable, Cloneable {

    public static final String ITEM_TYPE_SONG = "song";
    public static final String ITEM_TYPE_ALBUM = "album";
    public static final String ITEM_TYPE_VIDEO = "video";
    public static final String ITEM_TYPE_TOTAL = "total";
    public static final String ITEM_TYPE_SOCIAL = "social";
    public static final String ITEM_TYPE_NEWS = "news";
    public static final String ITEM_TYPE_IMAGE = "image";
    public static final String ITEM_TYPE_AUDIO = "audio";
    public static final String ITEM_TYPE_BANNER = "banner";
    public static final String ITEM_TYPE_FILM = "film";

    public static final String ITEM_TYPE_PROFILE_ALBUM = "profile_album";
    public static final String ITEM_TYPE_PROFILE_AVATAR = "profile_avatar";
    public static final String ITEM_TYPE_PROFILE_STATUS = "profile_status";
    public static final String ITEM_TYPE_PROFILE_COVER = "profile_cover";

    public static final String ITEM_TYPE_SUGGEST_FRIEND = "make_friend";

    //subtype for feed total
    public static final String ITEM_SUB_TYPE_HOROSCOPE = "horoscope";
    public static final String ITEM_SUB_TYPE_LUCKY_WHEEL = "luckywheel";
    public static final String ITEM_SUB_TYPE_DEEPLINK = "deeplink";

    //subtype for feed social
    public static final String ITEM_SUB_TYPE_SOCIAL_STATUS = "social_status";
    public static final String ITEM_SUB_TYPE_SOCIAL_LINK = "social_link";
    public static final String ITEM_SUB_TYPE_SOCIAL_IMAGE = "social_image"; // share ảnh offline
    public static final String ITEM_SUB_TYPE_SOCIAL_VIDEO = "social_video";
    public static final String ITEM_SUB_TYPE_SOCIAL_PUBLISH_VIDEO = "social_publish_video";
    public static final String ITEM_SUB_TYPE_SOCIAL_MOVIE = "social_film";
    public static final String ITEM_SUB_TYPE_SOCIAL_AUDIO = "social_audio";
    public static final String ITEM_SUB_TYPE_SOCIAL_CHANNEL = "social_channel";

    public static final String CONTENT_ACTION = "COMMENT";

    @SerializedName("id")
    private String[] id;

    @SerializedName("item_type")
    private String itemType = "";

    @SerializedName("item_sub_type")
    private String itemSubType = "";//ITEM_SUB_TYPE_HOROSCOPE;

    @SerializedName("item_id")
    private String itemId = "";

    @SerializedName("description")
    private String description = "";

    @SerializedName("SourceName")
    private String sourceName = "";

    @SerializedName("category")
    private String category = "";

    @SerializedName("desc_deeplink")        //phuc vu cho kich ban quang cao, truyen thong
    private String descDeeplink;

    @SerializedName("sub_desc_deeplink")
    private String subDescDeeplink;

    @SerializedName("singer")
    private String singer = "";

    @SerializedName("img_url")
    private String imageUrl = "";

    @SerializedName("img_url_poster")
    private String posterUrl = "";

    @SerializedName("media_url")
    private String mediaUrl = "";

    @SerializedName("album_url")
    private String albumUrl = "";

    @SerializedName("create_at")
    private String createAt = "";

    @SerializedName("url")
    private String url = "";

    @SerializedName("site")
    private String site = "";

    @SerializedName("item_name")
    private String itemName = "";

    @SerializedName("like")
    private long countLike = 0;

    @SerializedName("share")
    private long countShare = 0;

    @SerializedName("comment")
    private long countComment = 0;

    @SerializedName("open_link")
    private String openLink = "";

    @SerializedName("background_url")
    private String backgroundUrl = "";

    @SerializedName("left_label")
    private String leftLabel;

    @SerializedName("right_label")
    private String rightLabel;

    @SerializedName("left_deeplink")
    private String leftDeeplink;

    @SerializedName("right_deeplink")
    private String rightDeeplink;

    @SerializedName("list_img_id")
    private ArrayList<ImageContent> listImage = new ArrayList<>();

    @SerializedName("stamp")
    private long stamp;

    @SerializedName("userInfo")
    private UserInfo userInfo;

    @SerializedName("content_status")
    private String contentStatus;

    @SerializedName("content_tags")
    private ArrayList<TagMocha> contentListTag = new ArrayList<>();

    @SerializedName("content_url")
    private String contentUrl;

    @SerializedName("total_action")
    private int totalAction;

    @SerializedName("lst_user_action")
    private ArrayList<UserInfo> listUserAction;

    @SerializedName("channel")
    private ChannelOnMedia channel;

    @SerializedName("content_action")
    private String contentAction;

    @SerializedName("id_cmt_local") // MD5(sdt+timestamp)
    private String idCmtLocal;

    @SerializedName("package_android")
    private String packageAndroid;

    @SerializedName("is_fast_news")
    private int isFastNews;

    @SerializedName("isPrivate")
    private int isPrivate;

    @SerializedName("total_view")
    private long countView = -1;

    public FeedContent() {
    }

    public static FeedContent convertVideoToFeedContent(Video video) {
        FeedContent feedContent = new FeedContent();
        feedContent.setUrl(video.getLink());
        /*feedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        if (video.isMovie()) {
            feedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_MOVIE);
        } else {
            feedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_VIDEO);
        }*/
        if (video.isMovie()) {
            feedContent.setItemType(FeedContent.ITEM_TYPE_FILM);
        } else
            feedContent.setItemType(FeedContent.ITEM_TYPE_VIDEO);
        feedContent.setItemName(video.getTitle());
        feedContent.setImageUrl(video.getImagePath());
        feedContent.setPosterUrl(video.getPosterPath());
        feedContent.setMediaUrl(video.getOriginalPath());
        feedContent.setCategory(video.getCategoryName());
        feedContent.setItemId(video.getId());
        feedContent.setCountLike(video.getTotalLike());
        feedContent.setCountComment(video.getTotalComment());
        feedContent.setCountShare(video.getTotalShare());
        feedContent.setChannel(getChannelOnMedia(video.getChannel()));
        feedContent.setIsPrivate(video.getIsPrivate());
        feedContent.setContentUrl(video.getLink());
        if (!TextUtils.isEmpty(video.getLink()))
            try {
                URL url = new URL(video.getLink());
                feedContent.setSite(url.getHost());
            } catch (Exception e) {
            }
        return feedContent;
    }

    public static FeedContent convertMediaToFeedContent(MediaModel model) {
        FeedContent feedContent = new FeedContent();
        feedContent.setUrl(model.getUrl());
        feedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        feedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_AUDIO);
        feedContent.setItemName(model.getName());
        feedContent.setImageUrl(model.getImage());
        feedContent.setMediaUrl(model.getMedia_url());
        feedContent.setSinger(model.getSinger());
        feedContent.setItemId(model.getId());
        feedContent.setCountLike(model.getTotalLike());
        feedContent.setCountComment(model.getTotalComment());
        feedContent.setCountShare(model.getTotalShare());
        feedContent.setContentUrl(model.getUrl());
        feedContent.setChannel(null);
        feedContent.setIsPrivate(0);
        try {
            URL url = new URL(model.getUrl());
            feedContent.setSite(url.getHost());
        } catch (Exception e) {
        }
        return feedContent;
    }

    public static FeedContent convertMovieToFeedContent(Movie model) {
        FeedContent feedContent = new FeedContent();
        feedContent.setUrl(model.getLinkWap());
        feedContent.setItemType(FeedContent.ITEM_TYPE_FILM);
        feedContent.setItemName(model.getName());
        feedContent.setImageUrl(model.getImagePath());
        feedContent.setPosterUrl(model.getPosterPath());
        feedContent.setCategory(model.getCategoryText());
        feedContent.setItemId(model.getId());
        feedContent.setCountLike(model.getTotalLike());
        feedContent.setCountComment(model.getTotalComment());
        feedContent.setCountShare(model.getTotalShare());
        feedContent.setContentUrl(model.getLinkWap());
        feedContent.setChannel(null);
        feedContent.setIsPrivate(0);
        try {
            URL url = new URL(model.getLinkWap());
            feedContent.setSite(url.getHost());
        } catch (Exception e) {
        }
        return feedContent;
    }

    public static FeedContent convertMediaToFeedContent(AllModel model) {
        FeedContent feedContent = new FeedContent();
        feedContent.setUrl(model.getUrl());
        feedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        feedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_AUDIO);
        feedContent.setItemName(model.getName());
        feedContent.setImageUrl(model.getImage());
        feedContent.setMediaUrl(model.getMediaUrl());
        feedContent.setSinger(model.getSinger());
        feedContent.setItemId(String.valueOf(model.getId()));
        feedContent.setCountLike(0);
        feedContent.setCountComment(0);
        feedContent.setCountShare(0);
        feedContent.setContentUrl(model.getUrl());
        feedContent.setChannel(null);
        feedContent.setIsPrivate(0);
        try {
            URL url = new URL(model.getUrl());
            feedContent.setSite(url.getHost());
        } catch (Exception e) {
        }
        return feedContent;
    }

    public static FeedContent convertNewsToFeedContent(NewsModel model) {
        FeedContent feedContent = new FeedContent();
        feedContent.setUrl(ShareUtils.getLinkShare(model));
        feedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        feedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK);
        feedContent.setItemName(model.getTitle());
        feedContent.setImageUrl(model.getImage());
        feedContent.setMediaUrl(model.getMediaUrl());
        feedContent.setSinger(model.getCategoryName());
        feedContent.setItemId(String.valueOf(model.getID()));
        feedContent.setCountLike(0);
        feedContent.setCountComment(0);
        feedContent.setCountShare(0);
        feedContent.setContentUrl(ShareUtils.getLinkShare(model));
        feedContent.setChannel(null);
        feedContent.setIsPrivate(0);
        try {
            URL url = new URL(model.getUrl());
            feedContent.setSite(url.getHost());
        } catch (Exception e) {
        }
        return feedContent;
    }

    public static FeedContent convertTiinToFeedContent(TiinModel model) {
        FeedContent feedContent = new FeedContent();
        feedContent.setUrl(ShareUtils.getLinkShareTiin(model));
        feedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        feedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK);
        feedContent.setItemName(model.getTitle());
        feedContent.setImageUrl(model.getImage());
        feedContent.setMediaUrl(model.getMediaUrl());
        feedContent.setSinger(model.getCategory());
        feedContent.setItemId(String.valueOf(model.getId()));
        feedContent.setCountLike(0);
        feedContent.setCountComment(0);
        feedContent.setCountShare(0);
        feedContent.setContentUrl(ShareUtils.getLinkShareTiin(model));
        feedContent.setChannel(null);
        feedContent.setIsPrivate(0);
        try {
            URL url = new URL(model.getUrl());
            feedContent.setSite(url.getHost());
        } catch (Exception e) {
        }
        return feedContent;
    }

    private static ChannelOnMedia getChannelOnMedia(Channel channel) {
        if (channel == null) return null;
        ChannelOnMedia c = new ChannelOnMedia();
        c.setId(channel.getId());
        c.setAvatarUrl(channel.getUrlImage());
        c.setName(channel.getName());
        return c;
    }

    public static Movie convert2Movie(FeedContent model) {
        if (model != null) {
            if (ITEM_TYPE_SOCIAL.equals(model.getItemType()) && ITEM_SUB_TYPE_SOCIAL_MOVIE.equals(model.getItemSubType())
                    || ITEM_TYPE_FILM.equals(model.getItemType())) {
                if (model.getItemIdInt() > 0) {
                    Movie item = new Movie();
                    item.setId(model.getItemId());
                    item.setName(model.getItemName());
                    item.setDescription(model.getDescription());
                    item.setLinkWap(model.getLink());
                    item.setImagePath(model.getImageUrl());
                    item.setPosterPath(model.getPosterUrl());
                    return item;
                }
            }
        }
        return null;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public long getCountLike() {
        return countLike;
    }

    public void setCountLike(long countLike) {
        this.countLike = countLike;
    }

    public long getCountShare() {
        return countShare;
    }

    public void setCountShare(long countShare) {
        this.countShare = countShare;
    }

    public long getCountComment() {
        return countComment;
    }

    public void setCountComment(long countComment) {
        this.countComment = countComment;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public long getItemIdInt() {
        try {
            return Long.parseLong(itemId);
        } catch (Exception e) {
            return 0;
        }
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescDeeplink() {
        return descDeeplink;
    }

    public String getSubDescDeeplink() {
        return subDescDeeplink;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getAlbumUrl() {
        return albumUrl;
    }

    public String getCreateAt() {
        return createAt;
    }

    public String getOpenLink() {
        return openLink;
    }

    public String getBackgroundUrl() {
        return backgroundUrl;
    }

    public String getItemSubType() {
        return itemSubType;
    }

    public void setItemSubType(String itemSubType) {
        this.itemSubType = itemSubType;
    }

    public String getLeftLabel() {
        return leftLabel;
    }

    public String getRightLabel() {
        return rightLabel;
    }

    public String getLeftDeeplink() {
        return leftDeeplink;
    }

    public void setLeftDeeplink(String leftDeeplink) {
        this.leftDeeplink = leftDeeplink;
    }

    public String getRightDeeplink() {
        return rightDeeplink;
    }

    public ArrayList<ImageContent> getListImage() {
        if (listImage == null) listImage = new ArrayList<>();
        return listImage;
    }

    public long getStamp() {
        return stamp;
    }

    public void setStamp(long stamp) {
        this.stamp = stamp;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public ArrayList<UserInfo> getListUserAction() {
        return listUserAction;
    }

    public int getTotalAction() {
        return totalAction;
    }

    public void setContentAction(String contentAction) {
        this.contentAction = contentAction;
    }

    public String getIdCmtLocal() {
        return idCmtLocal;
    }

    public void setIdCmtLocal(String idCmtLocal) {
        this.idCmtLocal = idCmtLocal;
    }

    public String getPackageAndroid() {
        return packageAndroid;
    }

    public MediaModel getMediaMode() {
        MediaModel mediaModel = new MediaModel();
        mediaModel.setMedia_url(mediaUrl);
        mediaModel.setSinger(singer);
        mediaModel.setUrl(getUrl());
        mediaModel.setName(itemName);
        mediaModel.setId(itemId);
        mediaModel.setImage(imageUrl);
        if (ITEM_TYPE_SONG.equals(itemType)
                || (ITEM_TYPE_SOCIAL.equals(itemType) && ITEM_SUB_TYPE_SOCIAL_AUDIO.equals(itemSubType))) {
            mediaModel.setType(MediaModel.TYPE_BAI_HAT);
        } else if (ITEM_TYPE_ALBUM.equals(itemType)) {
            mediaModel.setType(MediaModel.TYPE_ALBUM);
        }

        return mediaModel;
    }

    public String getLink() {
        if (URLUtil.isNetworkUrl(contentUrl)) return contentUrl;
        if (URLUtil.isNetworkUrl(url)) return url;
        return "";
    }

    @Override
    public String toString() {
        return "FeedContent{" +
                "itemType='" + itemType + '\'' +
                ", itemSubType='" + itemSubType + '\'' +
                ", itemId='" + itemId + '\'' +
                ", itemName='" + itemName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", posterUrl='" + posterUrl + '\'' +
                ", mediaUrl='" + mediaUrl + '\'' +
                ", url='" + url + '\'' +
                ", contentUrl='" + contentUrl + '\'' +
                ", site='" + site + '\'' +
                ", openLink='" + openLink + '\'' +
                ", category='" + category + '\'' +
                ", singer='" + singer + '\'' +
                ", countLike=" + countLike +
                ", countShare=" + countShare +
                ", countComment=" + countComment +
                '}';
    }

    public void addImageContent(String idImage, String imgUrl, String thumbUrl, float ratioImage) {
        listImage.add(new ImageContent(idImage, imgUrl, thumbUrl, ratioImage));
    }

    public String getContentStatus() {
        return contentStatus;
    }

    public void setContentStatus(String contentStatus) {
        this.contentStatus = contentStatus;
    }

    public ArrayList<TagMocha> getContentListTag() {
        return contentListTag;
    }

    public void setContentListTag(ArrayList<TagMocha> listTag) {
        this.contentListTag = listTag;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public long getCountView() {
        return countView;
    }

    public void setCountView(long countView) {
        this.countView = countView;
    }

    public ChannelOnMedia getChannel() {
        return channel;
    }

    public void setChannel(ChannelOnMedia channel) {
        this.channel = channel;
    }

    public int getIsFastNews() {
        return isFastNews;
    }

    public int getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(int isPrivate) {
        this.isPrivate = isPrivate;
    }

    public FeedContent clone() throws CloneNotSupportedException {
        return (FeedContent) super.clone();
    }

    public static class ImageContent implements Serializable {

        public static final float MAX_RATIO_IMAGE = 2.0f;
        public static final float MIN_RATIO_IMAGE = 0.5f;

        @SerializedName("id")
        private String idImage;

        @SerializedName("image_url")
        private String imgUrl = "";

        @SerializedName("thumb_url")
        private String thumbUrl = "";

        @SerializedName("ratio")
        private float ratioImage = 1.0f;

        private int widthTarget = 0, heightTarget = 0;
        private String filePath = "";

        public ImageContent() {
        }

        public ImageContent(String idImage, String imgUrl, String thumbUrl, float ratioImage) {
            this.idImage = idImage;
            this.imgUrl = imgUrl;
            this.thumbUrl = thumbUrl;
            this.ratioImage = ratioImage;
        }

        public String getImgUrl(Context context) {
            return UrlConfigHelper.getInstance(context).getDomainImage() + "/" + imgUrl;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public String getThumbUrl() {
            if (TextUtils.isEmpty(thumbUrl)) {
                return imgUrl;
            } else {
                return thumbUrl;
            }
        }

        public String getThumbUrl(Context context) {
            if (TextUtils.isEmpty(thumbUrl)) {
                return UrlConfigHelper.getInstance(context).getDomainImage() + "/" + imgUrl;
            } else {
                return UrlConfigHelper.getInstance(context).getDomainImage() + "/" + thumbUrl;
            }
        }

        public String getIdImage() {
            return idImage;
        }

        public float getRatioImage() {
            return ratioImage;
        }

        public int getWidthTarget() {
            return widthTarget;
        }

        public void setWidthTarget(int widthTarget) {
            this.widthTarget = widthTarget;
        }

        public int getHeightTarget() {
            return heightTarget;
        }

        public void setHeightTarget(int heightTarget) {
            this.heightTarget = heightTarget;
        }

        public void setIdImage(String idImage) {
            this.idImage = idImage;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public void setThumbUrl(String thumbUrl) {
            this.thumbUrl = thumbUrl;
        }

        public void setRatioImage(float ratioImage) {
            this.ratioImage = ratioImage;
        }

        public String getImageUrl(Context context) {
            if (imgUrl != null && (imgUrl.startsWith("http://") || imgUrl.startsWith("https://")))
                return imgUrl;
            return UrlConfigHelper.getInstance(context).getDomainImage() + "/" + imgUrl;
        }

        public String getThumbImage(Context context) {
            if (TextUtils.isEmpty(thumbUrl)) {
                return getImageUrl(context);
            } else if (thumbUrl.startsWith("http://") || thumbUrl.startsWith("https://"))
                return thumbUrl;
            return UrlConfigHelper.getInstance(context).getDomainImage() + "/" + thumbUrl;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }
}

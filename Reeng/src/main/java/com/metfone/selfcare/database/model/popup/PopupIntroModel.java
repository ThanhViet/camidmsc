package com.metfone.selfcare.database.model.popup;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/9/2018.
 */

public class PopupIntroModel {

    @SerializedName("title")
    private String title;

    @SerializedName("labelBtn")
    private String labelBtn;

    @SerializedName("deeplinkBtn")
    private String deeplinkBtn;

    @SerializedName("labelBtnNext")
    private String labelBtnNext;

    @SerializedName("deeplinkBtnNext")
    private String deeplinkBtnNext;

    @SerializedName("intro")
    private ArrayList<PopupIntroDetail> intros;


    public String getTitle() {
        return title;
    }

    public String getLabelBtn() {
        return labelBtn;
    }

    public String getDeeplinkBtn() {
        return deeplinkBtn;
    }

    public ArrayList<PopupIntroDetail> getIntros() {
        return intros;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLabelBtn(String labelBtn) {
        this.labelBtn = labelBtn;
    }

    public void setDeeplinkBtn(String deeplinkBtn) {
        this.deeplinkBtn = deeplinkBtn;
    }

    public void setIntros(ArrayList<PopupIntroDetail> intros) {
        this.intros = intros;
    }

    public String getLabelBtnNext() {
        return labelBtnNext;
    }

    public void setLabelBtnNext(String labelBtnNext) {
        this.labelBtnNext = labelBtnNext;
    }

    public String getDeeplinkBtnNext() {
        return deeplinkBtnNext;
    }

    public void setDeeplinkBtnNext(String deeplinkBtnNext) {
        this.deeplinkBtnNext = deeplinkBtnNext;
    }
}

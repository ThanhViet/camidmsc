package com.metfone.selfcare.database.model.guestbook;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class PageItem implements Serializable {// các đối tượng sticker, image, ảnh để vẽ
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type = "text";
    @SerializedName("img")
    private String path;
    @SerializedName("x")
    private float x;
    @SerializedName("y")
    private float y;
    @SerializedName("width")
    private float width;
    @SerializedName("height")
    private float height;
    @SerializedName("rotation")
    private float rotate;
    @SerializedName("content")
    private String text;
    @SerializedName("font_name")
    private String textFont = "Roboto-Regular";
    @SerializedName("text_color")
    private String textColor;
    @SerializedName("underline")
    private int textUnderLine;
    @SerializedName("bold")
    private int textBold;
    @SerializedName("italic")
    private int textItalic;

    public PageItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getRotate() {
        return rotate;
    }

    public void setRotate(float rotate) {
        this.rotate = rotate;
    }

    public String getText() {
        if (text == null) text = "";
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextFont() {
        return textFont;
    }

    public void setTextFont(String textFont) {
        this.textFont = textFont;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public int getTextUnderLine() {
        return textUnderLine;
    }

    public void setTextUnderLine(int textUnderLine) {
        this.textUnderLine = textUnderLine;
    }

    public int getTextBold() {
        return textBold;
    }

    public void setTextBold(int textBold) {
        this.textBold = textBold;
    }

    public int getTextItalic() {
        return textItalic;
    }

    public void setTextItalic(int textItalic) {
        this.textItalic = textItalic;
    }
}
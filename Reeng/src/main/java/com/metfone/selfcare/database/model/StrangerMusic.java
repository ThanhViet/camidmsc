package com.metfone.selfcare.database.model;

import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.HTTP.STRANGER_MUSIC;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by toanvk2 on 02/06/2015.
 */
// luu danh sách cùng nghe voi người lạ
public class StrangerMusic implements Serializable {
    private long roomId;
    private String sessionId;
    private String createDate;
    private int state;
    private String confideStatus;
    //poster
    private String posterJid;
    private String posterName;
    private String posterLastAvatar;
    private String posterBirthDay = null;
    private String posterStatus;
    private int posterGender = -1;
    private int posterIsStar = 0;// mac dinh la user thuong =0,star=1, acceptor khong can truong nay
    private HashMap<String, String> mapMessageSticky;
    private int posterGroupId;
    //acceptor
    private String acceptorJid;
    private String acceptorName;
    private String acceptorLastAvatar;
    private String acceptorBirthDay = null;
    private String acceptorStatus;
    private int acceptorGender = -1;
    //songInfo
    private MediaModel songModel;
    //around
    private String lastGeoHash;
    private String distanceStr;
    private String info;
    private String ageStr;
    private int typeObj = Constants.STRANGER_MUSIC.TYPE_MUSIC;

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getConfideStatus() {
        return confideStatus;
    }

    public void setConfideStatus(String confideStatus) {
        this.confideStatus = confideStatus;
    }

    public String getPosterJid() {
        return posterJid;
    }

    public void setPosterJid(String posterJid) {
        this.posterJid = posterJid;
    }

    public String getPosterName() {
        return posterName;
    }

    public void setPosterName(String posterName) {
        this.posterName = posterName;
    }

    public String getPosterLastAvatar() {
        return posterLastAvatar;
    }

    public void setPosterLastAvatar(String posterLastAvatar) {
        this.posterLastAvatar = posterLastAvatar;
    }

    public String getPosterStatus() {
        return posterStatus;
    }

    public void setPosterStatus(String posterStatus) {
        this.posterStatus = posterStatus;
    }

    public String getPosterBirthDay() {
        return posterBirthDay;
    }

    public void setPosterBirthDay(String posterBirthDay) {
        this.posterBirthDay = posterBirthDay;
    }

    public int getPosterGender() {
        return posterGender;
    }

    public void setPosterGender(int posterGender) {
        this.posterGender = posterGender;
    }

    public boolean isStarRoom() {
        return posterIsStar == 1;
    }

    public void setPosterIsStar(int posterIsStar) {
        this.posterIsStar = posterIsStar;
    }

    public String getMessageSticky(String languageCode) {
        if (mapMessageSticky == null || mapMessageSticky.isEmpty()) {
            return null;
        } else if (mapMessageSticky.containsKey(languageCode)) {
            return mapMessageSticky.get(languageCode);
        } else if (mapMessageSticky.containsKey("en")) {
            return mapMessageSticky.get("en");
        } else {
            return "";
        }
    }

    public void setMapMessageSticky(JSONArray arrayValue) throws JSONException {
        if (arrayValue != null && arrayValue.length() > 0) {
            int lengthArray = arrayValue.length();
            String key;
            String value;
            for (int i = 0; i < lengthArray; i++) {
                JSONObject object = arrayValue.getJSONObject(i);
                // get keys trong object, sau do lay values theo keys
                if (object.has("languageCode") && object.has("message")) {
                    key = object.optString("languageCode");
                    value = object.optString("message");
                    if (mapMessageSticky == null) mapMessageSticky = new HashMap<>();
                    mapMessageSticky.put(key, value);
                }
            }
        }
    }

    public String getAcceptorJid() {
        return acceptorJid;
    }

    public void setAcceptorJid(String acceptorJid) {
        this.acceptorJid = acceptorJid;
    }

    public String getAcceptorName() {
        return acceptorName;
    }

    public void setAcceptorName(String acceptorName) {
        this.acceptorName = acceptorName;
    }

    public String getAcceptorLastAvatar() {
        return acceptorLastAvatar;
    }

    public void setAcceptorLastAvatar(String acceptorLastAvatar) {
        this.acceptorLastAvatar = acceptorLastAvatar;
    }

    public String getAcceptorBirthDay() {
        return acceptorBirthDay;
    }

    public void setAcceptorBirthDay(String acceptorBirthDay) {
        this.acceptorBirthDay = acceptorBirthDay;
    }

    public String getAcceptorStatus() {
        return acceptorStatus;
    }

    public void setAcceptorStatus(String acceptorStatus) {
        this.acceptorStatus = acceptorStatus;
    }

    public int getAcceptorGender() {
        return acceptorGender;
    }

    public void setAcceptorGender(int acceptorGender) {
        this.acceptorGender = acceptorGender;
    }

    public MediaModel getSongModel() {
        return songModel;
    }

    public void setSongModel(MediaModel songModel) {
        this.songModel = songModel;
    }

    public int getTypeObj() {
        return typeObj;
    }

    public void setTypeObj(int typeObj) {
        this.typeObj = typeObj;
    }

    public String getLastGeoHash() {
        return lastGeoHash;
    }

    public void setLastGeoHash(String lastGeoHash) {
        this.lastGeoHash = lastGeoHash;
    }

    public String getDistanceStr() {
        return distanceStr;
    }

    public void setDistanceStr(String distanceStr) {
        this.distanceStr = distanceStr;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAgeStr() {
        return ageStr;
    }

    public void setAgeStr(String age) {
        this.ageStr = age;
    }

    public void setJsonObject(JSONObject jsonObject, boolean isConfide) throws JSONException {
        typeObj = isConfide ? Constants.STRANGER_MUSIC.TYPE_CONFIDE : Constants.STRANGER_MUSIC.TYPE_MUSIC;
        roomId = jsonObject.optLong(STRANGER_MUSIC.ROOM_ID);
        sessionId = jsonObject.optString(STRANGER_MUSIC.SESSION_ID);
        createDate = jsonObject.optString(STRANGER_MUSIC.CREATE_DATE);
        state = jsonObject.optInt(STRANGER_MUSIC.STATE);
        confideStatus = jsonObject.optString(STRANGER_MUSIC.CONFIDE_STATUS);
        // par song
        if (jsonObject.has(STRANGER_MUSIC.SONG_INFO)) {
            JSONObject songObject = jsonObject.getJSONObject(STRANGER_MUSIC.SONG_INFO);
            songModel = new MediaModel();
            songModel.setId(songObject.optString(STRANGER_MUSIC.SONG_ID));
            songModel.setName(songObject.optString(STRANGER_MUSIC.SONG_NAME));
            songModel.setSinger(songObject.optString(STRANGER_MUSIC.SONG_SINGER));
            songModel.setImage(songObject.optString(STRANGER_MUSIC.SONG_IMAGE));
            songModel.setMedia_url(songObject.optString(STRANGER_MUSIC.SONG_MEDIA_URL));
            songModel.setUrl(songObject.optString(STRANGER_MUSIC.SONG_URL));
        }
        // par poster
        if (jsonObject.has(STRANGER_MUSIC.POSTER_INFO)) {
            JSONObject posterObject = jsonObject.getJSONObject(STRANGER_MUSIC.POSTER_INFO);
            posterJid = posterObject.optString(STRANGER_MUSIC.MSISDN);
            posterName = posterObject.optString(STRANGER_MUSIC.NAME);
            posterLastAvatar = posterObject.optString(STRANGER_MUSIC.LAST_AVATAR);
            posterBirthDay = posterObject.optString(STRANGER_MUSIC.BIRTHDAY);
            posterGender = posterObject.optInt(STRANGER_MUSIC.GENDER);
            posterStatus = posterObject.optString(STRANGER_MUSIC.STATUS);
            posterIsStar = posterObject.optInt(STRANGER_MUSIC.IS_STAR);
            if (posterObject.has(STRANGER_MUSIC.MESSAGE_STICKY)) {
                setMapMessageSticky(posterObject.getJSONArray(STRANGER_MUSIC.MESSAGE_STICKY));
            }
            posterGroupId = posterObject.optInt(STRANGER_MUSIC.GROUP_ID);
        }
        // acceptor
        if (jsonObject.has(STRANGER_MUSIC.ACCEPTOR_INFO)) {
            JSONObject acceptorObject = jsonObject.getJSONObject(STRANGER_MUSIC.ACCEPTOR_INFO);
            acceptorJid = acceptorObject.optString(STRANGER_MUSIC.MSISDN);
            acceptorName = acceptorObject.optString(STRANGER_MUSIC.NAME);
            acceptorLastAvatar = acceptorObject.optString(STRANGER_MUSIC.LAST_AVATAR);
            acceptorBirthDay = acceptorObject.optString(STRANGER_MUSIC.BIRTHDAY);
            acceptorGender = acceptorObject.optInt(STRANGER_MUSIC.GENDER);
            acceptorStatus = acceptorObject.optString(STRANGER_MUSIC.STATUS);
        }
    }

    public void setAroundJsonObject(JSONObject jsonObject) throws JSONException {
        typeObj = Constants.STRANGER_MUSIC.TYPE_AROUND;
        posterIsStar = 0;
        // par poster
        posterJid = jsonObject.optString(STRANGER_MUSIC.MSISDN);
        posterName = jsonObject.optString(STRANGER_MUSIC.NAME);
        posterLastAvatar = jsonObject.optString(STRANGER_MUSIC.LAST_AVATAR);
        posterBirthDay = jsonObject.optString(STRANGER_MUSIC.BIRTHDAY);
        posterGender = jsonObject.optInt(STRANGER_MUSIC.GENDER);
        posterStatus = jsonObject.optString(STRANGER_MUSIC.STATUS);
        //around
        distanceStr = jsonObject.optString(STRANGER_MUSIC.STRANGER_DISTANCE_STR);
        lastGeoHash = jsonObject.optString(STRANGER_MUSIC.STRANGER_GEO_HASH);
        info = jsonObject.optString(STRANGER_MUSIC.STRANGER_AROUND_INFO, "");
        ageStr = jsonObject.optString(STRANGER_MUSIC.STRANGER_AGE);
        // par song
        if (jsonObject.has(STRANGER_MUSIC.SONG_INFO)) {
            JSONObject songObject = jsonObject.getJSONObject(STRANGER_MUSIC.SONG_INFO);
            songModel = new MediaModel();
            songModel.setId(songObject.optString(STRANGER_MUSIC.SONG_ID));
            songModel.setName(songObject.optString(STRANGER_MUSIC.SONG_NAME));
            songModel.setSinger(songObject.optString(STRANGER_MUSIC.SONG_SINGER));
            songModel.setImage(songObject.optString(STRANGER_MUSIC.SONG_IMAGE));
            songModel.setMedia_url(songObject.optString(STRANGER_MUSIC.SONG_MEDIA_URL));
            songModel.setUrl(songObject.optString(STRANGER_MUSIC.SONG_URL));
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("StrangerMusic");
        sb.append("roomId: ").append(roomId);
        sb.append(" sessionId: ").append(sessionId);
        sb.append(" createDate: ").append(createDate);
        sb.append(" state: ").append(state);
        sb.append(" posterJid: ").append(posterJid).append(" posterName: ").
                append(posterName).append(" posterLastAvatar: ").append(posterLastAvatar).
                append(" posterBirthDay: ").append(posterBirthDay).
                append(" posterGender: ").append(posterGender).append(" posterStatus: ").
                append(posterStatus).append(" groupId: ").append(posterGroupId);
        sb.append(" acceptorJid: ").append(acceptorJid).append(" acceptorName: ").
                append(acceptorName).append(" acceptorLastAvatar: ").append(acceptorLastAvatar).
                append(" acceptorBirthDay: ").append(acceptorBirthDay).
                append(" acceptorGender: ").append(acceptorGender).append(" acceptorStatus: ").append(acceptorStatus);
        if (songModel != null) {
            sb.append(" songId: ").append(songModel.getId()).append(" songName: ").append(songModel.getName()).
                    append(" songSinger: ").append(songModel.getSinger()).append(" songThumb: ").
                    append(songModel.getImage()).append(" songUrl: ").append(songModel.getUrl());
        }
        return sb.toString();
    }
}
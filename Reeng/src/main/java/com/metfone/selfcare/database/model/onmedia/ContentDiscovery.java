package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 7/7/2016.
 */
public class ContentDiscovery implements Serializable {

    @SerializedName("avatar")
    private String imageUrl = "";

    @SerializedName("link")
    private String videoUrl = "";

    @SerializedName("title")
    private String title = "";

    private String urlNetnews = "";

    private String videoType = "";

    private int position = 0;

    public ContentDiscovery(String imageUrl, String videoUrl, String title, String videoType, String urlNetnews) {
        this.imageUrl = imageUrl;
        this.videoUrl = videoUrl;
        this.title = title;
        this.videoType = videoType;
        this.urlNetnews = urlNetnews;
    }

    public String getImageUrl() {
        return imageUrl;
    }


    public String getTitle() {
        return title;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getVideoType() {
        return videoType;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getUrlNetnews() {
        return urlNetnews;
    }

    @Override
    public String toString() {
        return "ContentDiscovery{" +
                "imageUrl='" + imageUrl + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", title='" + title + '\'' +
                ", urlNetnews='" + urlNetnews + '\'' +
                ", videoType='" + videoType + '\'' +
                ", position=" + position +
                '}';
    }
}

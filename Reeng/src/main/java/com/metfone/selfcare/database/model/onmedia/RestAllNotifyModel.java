package com.metfone.selfcare.database.model.onmedia;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.restful.AbsResultData;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 11/10/2015.
 */
public class RestAllNotifyModel extends AbsResultData implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("notifies")
    private ArrayList<NotificationModel> data;

    @SerializedName("total")
    private int totalNotify;

    @SerializedName("timeserver")
    private long currentTimeServer;

    public ArrayList<NotificationModel> getData() {
        return data;
    }

    public void setData(ArrayList<NotificationModel> data) {
        this.data = data;
    }


    public int getTotalNotify() {
        return totalNotify;
    }

    public long getCurrentTimeServer() {
        return currentTimeServer;
    }

    @Override
    public String toString() {
        return "RestAllFeedsModel [data=" + data + "] error " + getError();
    }
}

package com.metfone.selfcare.database.model.call;

/**
 * Created by toanvk2 on 10/12/2016.
 */

public class CallHistory {
    private long id;
    private String friendNumber;
    private String ownerNumber;
    private int callOut;
    private int callState;
    private int callDirection;// send or received
    private long callTime;
    private int duration;
    private int count;

    public CallHistory() {

    }

    public CallHistory(CallHistoryDetail historyDetail, int count) {
        this.friendNumber = historyDetail.getFriendNumber();
        this.ownerNumber = historyDetail.getOwnerNumber();
        this.callState = historyDetail.getCallState();
        this.callOut = historyDetail.getCallType();
        this.callDirection = historyDetail.getCallDirection();
        this.callTime = historyDetail.getCallTime();
        this.duration = historyDetail.getDuration();
        this.count = count;

    }

    public void updateValueFromHistoryDetail(CallHistoryDetail historyDetail) {// cập nhật history khi insert thêm detail
        this.callState = historyDetail.getCallState();
        this.callOut = historyDetail.getCallType();
        this.callDirection = historyDetail.getCallDirection();
        this.callTime = historyDetail.getCallTime();
        this.duration = historyDetail.getDuration();
        count++;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFriendNumber() {
        return friendNumber;
    }

    public void setFriendNumber(String friendNumber) {
        this.friendNumber = friendNumber;
    }

    public String getOwnerNumber() {
        return ownerNumber;
    }

    public void setOwnerNumber(String ownerNumber) {
        this.ownerNumber = ownerNumber;
    }

    public int getCallType() {
        return callOut;
    }

    public void setCallType(int callOut) {
        this.callOut = callOut;
    }

    public int getCallState() {
        return callState;
    }

    public void setCallState(int callState) {
        this.callState = callState;
    }

    public long getCallTime() {
        return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCallDirection() {
        return callDirection;
    }

    public int getCallOut() {
        return callOut;
    }

    public void setCallDirection(int callDirection) {
        this.callDirection = callDirection;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CallHistory");
        sb.append(" id: ").append(id);
        sb.append(" friend: ").append(friendNumber);
        sb.append(" owner: ").append(ownerNumber);
        sb.append(" callOut: ").append(callOut);
        sb.append(" callState: ").append(callState);
        sb.append(" callDirection: ").append(callDirection);
        sb.append(" callTime: ").append(callTime);
        sb.append(" count: ").append(count);
        sb.append(" duration: ").append(duration);
        return sb.toString();
    }
}
package com.metfone.selfcare.database.model.message;

import com.stringee.StringeeIceServer;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import org.jivesoftware.smack.model.CallData;
import org.jivesoftware.smack.model.IceServer;
import org.jivesoftware.smack.packet.ReengCallPacket;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by toanvk2 on 7/25/2016.
 */
public class MochaCallMessage extends ReengMessage {
    private String caller;
    private String callee;
    private ReengCallPacket.CallError callError;
    private CallData callData;
    private ArrayList<IceServer> iceServers;
    private String callSession;
    private long timeConnect;
    private boolean isRestartICESuccess;

    private String settingXML;
    private boolean enableRestartICE = false;
    private int iceTimeout;
    private long restartICEDelay;
    private long restartICEPeriod;
    private int restartICELoop;
    private int zeroBwEndCall;
    private long network2failedTime;
    private long timedis2recon;
    private long timeRestartBw;
    private long delayRestartOnFailed;

    private String bundlePolicy;
    private String rtcpMuxPolicy;
    private String iceTransportsType;

    private ReengCallPacket.RestartReason restartReason;

    public MochaCallMessage() {

    }

    public MochaCallMessage(String from, String to, ThreadMessage thread) {
        super(thread.getThreadType(), ReengMessageConstant.MessageType.call);
        setThreadId(thread.getId());
        setSender(from);
        setReceiver(to);
        Date date = new Date();
        setTime(date.getTime());
        setMessageType(ReengMessageConstant.MessageType.call);
    }

    public String getCaller() {
        return caller;
    }

    public String getCallee() {
        return callee;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }

    public ReengCallPacket.CallError getCallError() {
        return callError;
    }

    public void setCallError(ReengCallPacket.CallError callError) {
        this.callError = callError;
    }

    public CallData getCallData() {
        return callData;
    }

    public void setCallData(CallData callData) {
        this.callData = callData;
    }


    public void setStringeeIceServers(LinkedList<StringeeIceServer> stringeeIceServers) {
        iceServers = new ArrayList<>();
        if (stringeeIceServers != null) {
            for (StringeeIceServer stringeeServer : stringeeIceServers) {
                iceServers.add(new IceServer(stringeeServer.username, stringeeServer.password, stringeeServer.uri));
            }
        }
    }

    public void setIceServers(ArrayList<IceServer> iceServers) {
        this.iceServers = iceServers;
    }

    public ArrayList<IceServer> getIceServers() {
        return iceServers;
    }

    public String getCallSession() {
        return callSession;
    }

    public void setCallSession(String callSession) {
        this.callSession = callSession;
    }

    public long getTimeConnect() {
        return timeConnect;
    }

    public void setTimeConnect(long timeConnect) {
        this.timeConnect = timeConnect;
    }

    public boolean isRestartICESuccess() {
        return isRestartICESuccess;
    }

    public void setRestartICESuccess(boolean restartICESuccess) {
        isRestartICESuccess = restartICESuccess;
    }

    public String getSettingXML() {
        return settingXML;
    }

    public void setSettingXML(String settingXML) {
        this.settingXML = settingXML;
    }

    public boolean isEnableRestartICE() {
        return enableRestartICE;
    }

    public void setEnableRestartICE(boolean enableRestartICE) {
        this.enableRestartICE = enableRestartICE;
    }

    public int getIceTimeout() {
        return iceTimeout;
    }

    public void setIceTimeout(int iceTimeout) {
        this.iceTimeout = iceTimeout;
    }

    public long getRestartICEDelay() {
        return restartICEDelay;
    }

    public void setRestartICEDelay(long restartICEDelay) {
        this.restartICEDelay = restartICEDelay;
    }

    public long getRestartICEPeriod() {
        return restartICEPeriod;
    }

    public void setRestartICEPeriod(long restartICEPeriod) {
        this.restartICEPeriod = restartICEPeriod;
    }

    public int getRestartICELoop() {
        return restartICELoop;
    }

    public void setRestartICELoop(int restartICELoop) {
        this.restartICELoop = restartICELoop;
    }

    public int getZeroBwEndCall() {
        return zeroBwEndCall;
    }

    public void setZeroBwEndCall(int zeroBwEndCall) {
        this.zeroBwEndCall = zeroBwEndCall;
    }

    public long getNetwork2failedTime() {
        return network2failedTime;
    }

    public void setNetwork2failedTime(long network2failedTime) {
        this.network2failedTime = network2failedTime;
    }

    public long getTimedis2recon() {
        return timedis2recon;
    }

    public void setTimedis2recon(long timedis2recon) {
        this.timedis2recon = timedis2recon;
    }

    public long getTimeRestartBw() {
        return timeRestartBw;
    }

    public void setTimeRestartBw(long timeRestartBw) {
        this.timeRestartBw = timeRestartBw;
    }

    public long getDelayRestartOnFailed() {
        return delayRestartOnFailed;
    }

    public void setDelayRestartOnFailed(long delayRestartOnFailed) {
        this.delayRestartOnFailed = delayRestartOnFailed;
    }

    public ReengCallPacket.RestartReason getRestartReason() {
        return restartReason;
    }

    public void setRestartReason(ReengCallPacket.RestartReason restartReason) {
        this.restartReason = restartReason;
    }

    public String getBundlePolicy() {
        return bundlePolicy;
    }

    public void setBundlePolicy(String bundlePolicy) {
        this.bundlePolicy = bundlePolicy;
    }

    public String getRtcpMuxPolicy() {
        return rtcpMuxPolicy;
    }

    public void setRtcpMuxPolicy(String rtcpMuxPolicy) {
        this.rtcpMuxPolicy = rtcpMuxPolicy;
    }

    public String getIceTransportsType() {
        return iceTransportsType;
    }

    public void setIceTransportsType(String iceTransportsType) {
        this.iceTransportsType = iceTransportsType;
    }
}
package com.metfone.selfcare.database.model.bot;

import org.json.JSONObject;

/**
 * Created by toanvk2 on 1/17/2017.
 */
public class Action {
    private String key;
    private String title;
    private boolean isSelected;
    private boolean actionBack = false;

    public Action() {

    }

    public Action(JSONObject object) {
        this.title = object.optString("action_title", "");
        this.key = object.optString("action_key", "");
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isActionBack() {
        return actionBack;
    }

    public void setActionBack(boolean actionBack) {
        this.actionBack = actionBack;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Action");
        sb.append("title: ").append(title);
        sb.append(" key: ").append(key);
        sb.append(" selected: ").append(isSelected);
        return sb.toString();
    }
}

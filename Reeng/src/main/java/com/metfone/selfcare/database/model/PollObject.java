package com.metfone.selfcare.database.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/25/2016.
 */
public class PollObject implements Serializable {

    public static final int STATUS_PIN = 1;
    public static final int STATUS_UNPIN = 0;
    public static final int STATUS_PUSH_TOP = 2;
    public static final int STATUS_CLOSE = 3;

    private String pollId;
    private String title;
    private String creator;

    private int choice;
    private int anonymous;
    private int enableAddVote;

    private long createdAt;
    private long expireAt;

    private int pinStatus;
    private int close;

    private ArrayList<PollItem> listItems = new ArrayList<>();
    private ArrayList<String> listSelected = new ArrayList<>();

    public PollObject() {

    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public int getChoice() {
        return choice;
    }

    public void setChoice(int choice) {
        this.choice = choice;
    }

    public int getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(int anonymous) {
        this.anonymous = anonymous;
    }

    public int getEnableAddVote() {
        return enableAddVote;
    }

    public void setEnableAddVote(int enableAddVote) {
        this.enableAddVote = enableAddVote;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(long expireAt) {
        this.expireAt = expireAt;
    }

    public ArrayList<PollItem> getListItems() {
        return listItems;
    }

    public void setListItems(ArrayList<PollItem> listItems) {
        this.listItems = listItems;
    }

    public void addPollItem(PollItem item) {
        if (listItems == null) listItems = new ArrayList<>();
        listItems.add(item);
    }

    public void addListPollItem(ArrayList<PollItem> items) {
        if (listItems == null) listItems = new ArrayList<>();
        listItems.addAll(items);
    }

    public ArrayList<String> getListSelected() {
        return listSelected;
    }

    public void setListSelected(ArrayList<String> listSelected) {
        this.listSelected = listSelected;
    }

    public int getPinStatus() {
        return pinStatus;
    }

    public void setPinStatus(int pinStatus) {
        this.pinStatus = pinStatus;
    }

    public int getClose() {
        return close;
    }

    public void setClose(int close) {
        this.close = close;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Poll Object");
        sb.append(",pollId: ").append(pollId);
        sb.append(",title: ").append(title);
        sb.append(",choice: ").append(choice);
        sb.append(",pinstatus: ").append(pinStatus);
        if (listItems != null) {
            for (PollItem item : listItems) {
                sb.append(",item: ").append(item.toString()).append("\n");
            }
        }
        if (listSelected != null) {
            sb.append(",listSelected: ").append(listSelected.toArray());
        }
        return sb.toString();
    }
}
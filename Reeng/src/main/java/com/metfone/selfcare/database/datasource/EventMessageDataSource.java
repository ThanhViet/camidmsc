package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.EventMessageConstant;
import com.metfone.selfcare.database.model.EventMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 2/26/2015.
 */
public class EventMessageDataSource {
    // Database fields
    private static String TAG = EventMessageDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static EventMessageDataSource mInstance;

    public static synchronized EventMessageDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new EventMessageDataSource(application);
        }
        return mInstance;
    }

    private EventMessageDataSource(ApplicationController application) {
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    /**
     * close cursor
     *
     * @param cursor
     */
    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    /**
     * set content values
     *
     * @param eventMessage
     * @return
     */
    public ContentValues setValues(EventMessage eventMessage) {
        ContentValues values = new ContentValues();
        values.put(EventMessageConstant.EVENT_MESSAGE_ID, eventMessage.getMessageId());
        values.put(EventMessageConstant.EVENT_SENDER, eventMessage.getSender());
        values.put(EventMessageConstant.EVENT_TIME, eventMessage.getTime());
        values.put(EventMessageConstant.EVENT_STATE, eventMessage.getStatus());
        values.put(EventMessageConstant.EVENT_PACKET_ID, eventMessage.getPacketId());
        values.put(EventMessageConstant.EVENT_THREAD_ID, eventMessage.getThreadId());
        values.put(EventMessageConstant.EVENT_REACTION, eventMessage.getReaction());
        return values;
    }

    /**
     * insert list event message to database
     *
     * @param list
     */
    public void insertListEventMessage(ArrayList<EventMessage> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.beginTransaction();
            try {
                for (EventMessage item : list) {
                    ContentValues values = setValues(item);
                    long id = databaseWrite.insert(EventMessageConstant.EVENT_TABLE, null, values);
                    item.setId(id);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction insertListEventMessage", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "insertListEventMessage", e);
        }
    }

    /**
     * insert event message to database
     *
     * @param eventMessage
     */
    public void insertEventMessage(EventMessage eventMessage) {
        try {
            if (databaseWrite == null) {
                return;
            }
            ContentValues values = setValues(eventMessage);
            long id = databaseWrite.insert(EventMessageConstant.EVENT_TABLE, null, values);
            eventMessage.setId(id);
        } catch (Exception e) {
            Log.e(TAG, "insertEventMessage", e);
        }
    }

    /**
     * get values from cursor
     *
     * @param cur
     * @return
     */
    public EventMessage getValuesFromCursor(Cursor cur) {
        EventMessage eventMessage = new EventMessage();
        eventMessage.setId(cur.getLong(0));
        eventMessage.setMessageId(cur.getLong(1));
        eventMessage.setSender(cur.getString(2));
        eventMessage.setTime(cur.getLong(3));
        eventMessage.setStatus(cur.getInt(4));
        eventMessage.setPacketId(cur.getString(5));
        eventMessage.setThreadId(cur.getInt(6));
        eventMessage.setReaction(cur.getInt(7));
        return eventMessage;
    }

    /**
     * get list event message from dabatabsae by message id, and packet id
     *
     * @param messageId
     * @return listEvenetMessage
     */
    public ArrayList<EventMessage> getListEventMessageFromMessageId(long messageId, String packetId) {
        ArrayList<EventMessage> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query = EventMessageConstant.SELECT_BY_MESSAGE_ID_QUERY + messageId
                    + EventMessageConstant.SELECT_AND_PACKET_ID_QUERY + "'%" + packetId + "%'";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    EventMessage item = getValuesFromCursor(cursor);
                    if (!TextUtils.isEmpty(item.getSender()))
                        list.add(item);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getListEventMessageFromMessageId", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }


    public EventMessage getListEventMessageReaction(long messageId, String packetId, String sender) {
        Cursor cursor = null;
        EventMessage item = null;
        try {
            String query = EventMessageConstant.SELECT_REACTION_EVENT;
            cursor = databaseRead.rawQuery(query, new String[]{String.valueOf(messageId), packetId, sender, "-1"});
            if (cursor != null && cursor.moveToFirst()) {
                item = getValuesFromCursor(cursor);
            }
        } catch (Exception e) {
            Log.e(TAG, "getListEventMessageReaction", e);
        } finally {
            closeCursor(cursor);
        }
        return item;
    }

    public ArrayList<EventMessage> getListEventMessageReactionFromMessageId(long messageId, String packetId) {
        ArrayList<EventMessage> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query = EventMessageConstant.SELECT_BY_MESSAGE_ID_QUERY + messageId
                    + EventMessageConstant.SELECT_AND_PACKET_ID_QUERY + "'%" + packetId + "%' AND "
                    + EventMessageConstant.EVENT_STATE + " = " + EventMessageConstant.EVENT_REACTION_STATE;
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    EventMessage item = getValuesFromCursor(cursor);
                    list.add(item);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getListEventMessageReactionFromMessageId", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public ArrayList<String> getListReactionSenderFromMessageId(long messageId, String packetId, int reaction) {
        ArrayList<String> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query = EventMessageConstant.SELECT_SENDER_BY_MESSAGE_ID_QUERY + messageId
                    + EventMessageConstant.SELECT_AND_PACKET_ID_QUERY + "'%" + packetId + "%' AND "
                    + EventMessageConstant.EVENT_STATE + " = " + EventMessageConstant.EVENT_REACTION_STATE + " AND "
                    + EventMessageConstant.EVENT_REACTION + " = " + reaction;
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getListEventMessageReactionFromMessageId", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public int getReactionByNumber(long messageId, String packetId, String number) {
        Cursor cursor = null;
        try {
            String query = EventMessageConstant.SELECT_REACTION_BY_MESSAGE_ID_QUERY + messageId
                    + EventMessageConstant.SELECT_AND_PACKET_ID_QUERY + "'%" + packetId + "%' AND "
                    + EventMessageConstant.EVENT_STATE + " = " + EventMessageConstant.EVENT_REACTION_STATE + " AND "
                    + EventMessageConstant.EVENT_SENDER + " LIKE " + "'%" + number + "%'";
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    return cursor.getInt(0);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getReactionByNumber", e);
        } finally {
            closeCursor(cursor);
        }
        return -1;
    }

    public void updateEventMessage(EventMessage eventMessage) {
        ContentValues values = setValues(eventMessage);
        String whereClause = EventMessageConstant.EVENT_ID + " = "
                + eventMessage.getId();
        databaseWrite.update(EventMessageConstant.EVENT_TABLE, values, whereClause, null);
    }

    /**
     * xoa event message khi xoa 1 message
     *
     * @param messageId
     */
    public void deleteEventMessageByMessageId(long messageId) {
        if (databaseWrite == null) {
            return;
        }
        try {
            String whereClause = EventMessageConstant.EVENT_MESSAGE_ID + " = " + messageId;
            databaseWrite.delete(EventMessageConstant.EVENT_TABLE, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * xoa event message khi xoa 1 thread
     *
     * @param threadId
     */
    public void deleteEventMessageByThreadId(int threadId) {
        if (databaseWrite == null) {
            return;
        }
        try {
            String whereClause = EventMessageConstant.EVENT_THREAD_ID + " = " + threadId;
            databaseWrite.delete(EventMessageConstant.EVENT_TABLE, whereClause, null);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void deleteEventMessageOfListThread(ArrayList<ThreadMessage> listThread) {
        try {
            databaseWrite.beginTransaction();
            try {
                String whereClause;
                for (ThreadMessage thread : listThread) {
                    whereClause = EventMessageConstant.EVENT_THREAD_ID + " = " + thread.getId();
                    databaseWrite.delete(EventMessageConstant.EVENT_TABLE, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    /**
     * delete list event message from database
     *
     * @param list
     */
    public void deleteListEventMessage(ArrayList<EventMessage> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (EventMessage item : list) {
                    String whereClause = EventMessageConstant.EVENT_ID + " = " + item.getId();
                    databaseWrite.delete(EventMessageConstant.EVENT_TABLE, whereClause, null);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction deleteListEventMessage", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteListEventMessage", e);
        }
    }

    public void deleteEventMessage(EventMessage eventMessage) {
        if (eventMessage == null) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                String whereClause = EventMessageConstant.EVENT_ID + " = " + eventMessage.getId();
                databaseWrite.delete(EventMessageConstant.EVENT_TABLE, whereClause, null);
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction deleteListEventMessage", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteListEventMessage", e);
        }
    }

    /**
     * delete all event message of table
     */
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(EventMessageConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllContact", e);
        }
    }
}

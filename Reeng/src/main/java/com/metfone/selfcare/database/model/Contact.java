package com.metfone.selfcare.database.model;

import com.metfone.selfcare.helper.ComparatorHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by toanvk2 on 6/26/14.
 */
public class Contact implements Serializable {
    private static final String TAG = Contact.class.getSimpleName();
    private String contactId = null;
    private String name = null;
    private int favorite = 0;
    private boolean isChecked = false;
    private ArrayList<PhoneNumber> listNumbers = null;
    private String nameUnicode = null;

    /**
     * get,set contactId
     *
     * @return contactId
     */
    public String getContactId() {
        return contactId;
    }

    public void setContactID(String contactId) {
        this.contactId = contactId;
    }

    /**
     * get, set contactName
     *
     * @return Name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * get, set favorite
     *
     * @return favorite
     */
    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    /**
     * get, set isChecked
     *
     * @return isChecked
     */
    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    /**
     * get, set listNumbers
     *
     * @return listNumbers
     */
    public ArrayList<PhoneNumber> getListNumbers() {
        return this.listNumbers;
    }

    public void setListNumbers(ArrayList<PhoneNumber> listNumbers) {
        this.listNumbers = listNumbers;
    }

    //
    public ArrayList<PhoneNumber> getLisNumberAfterSort() {
        if (listNumbers == null || listNumbers.size() == 1)
            return listNumbers;
        Collections.sort(listNumbers, ComparatorHelper.getComparatorNumberByNumber());
        HashSet<PhoneNumber> setNumbers = new HashSet<>(listNumbers);
        ArrayList<PhoneNumber> numbers = new ArrayList<>(setNumbers);
        Collections.sort(numbers, ComparatorHelper.getComparatorNumberByNumber());
        return numbers;
    }

    public boolean isReeng() {
        ArrayList<PhoneNumber> numbers = getLisNumberAfterSort();
        for (PhoneNumber nb : numbers) {
            if (nb.isReeng())
                return true;
        }
        return false;
    }

    public ArrayList<String> getListStringNumbers() {
        ArrayList<PhoneNumber> numbers = getLisNumberAfterSort();
        ArrayList<String> listStrings = new ArrayList<>();
        for (PhoneNumber nb : numbers) {
            listStrings.add(nb.getJidNumber());
        }
        return listStrings;
    }

//    public String getLastChangeAvatar() {
//        ArrayList<PhoneNumber> numbers = getLisNumberAfterSort();
//        for (PhoneNumber nb : numbers) {
//            if (nb.getReengState() != MessageConstants.CONTACT.ROOM_STATE_NONE)
//                return nb.getLastChangeAvatar();
//        }
//        return null;
//    }

    public PhoneNumber getPhoneNumberReeng() {
        ArrayList<PhoneNumber> numbers = getLisNumberAfterSort();
        for (PhoneNumber nb : numbers) {
            if (nb.isReeng()) {
                return nb;
            }
        }
        return null;
    }

    public String getStringFromListPhone() {
        ArrayList<PhoneNumber> numbers = getLisNumberAfterSort();
        if (numbers != null && numbers.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (PhoneNumber number : numbers) {
                sb.append(number.getJidNumber()).append(" ");
            }
            int lengText = sb.length();
            sb.deleteCharAt(lengText - 1);
            return sb.toString();
        } else {
            return "";
        }
    }

    public String getStatus() {
        ArrayList<PhoneNumber> numbers = getLisNumberAfterSort();
        for (PhoneNumber nb : numbers) {
            if (nb.isReeng()) {
                return nb.getStatus();
            }
        }
        return null;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((listNumbers == null) ? 0 : getHashCodeNumbers(listNumbers));
        return result;
    }

    // 2 contact co danh sach so dien thoai giong nhau coi la 1
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (Contact.class != obj.getClass()) {
            return false;
        }
        Contact other = (Contact) obj;
        ArrayList<PhoneNumber> otherNumbers = other.getListNumbers();
        ArrayList<PhoneNumber> numbers = this.getListNumbers();
        return compareListNumber(otherNumbers, numbers);
    }

    public boolean equalsValues(Object obj) {
        if (obj == null) {
            return false;
        }
        if (Contact.class != obj.getClass()) {
            return false;
        }
        Contact other = (Contact) obj;
        String otherName = other.getName();
        int otherFavorite = other.getFavorite();
        if (name != null && otherName != null && name.equals(otherName)) {
            if (favorite == otherFavorite) {
                return true;
            }
        }
        return false;
    }

    private int getHashCodeNumbers(List<PhoneNumber> numbers) {
        HashSet<PhoneNumber> setNumber = new HashSet<>(numbers);
        int result = 0;
        for (PhoneNumber number : setNumber) {
            result = result + number.hashCode();
        }
        return result;
    }

    // compare listNumbers
    private boolean compareListNumber(ArrayList<PhoneNumber> srcListNumber,
                                      ArrayList<PhoneNumber> dstListNumber) {
        HashSet<PhoneNumber> srcSet1 = new HashSet<>(
                srcListNumber);
        HashSet<PhoneNumber> dstSet = new HashSet<>(
                dstListNumber);
        srcSet1.removeAll(dstSet);
        if (!srcSet1.isEmpty()) {
            return false;
        }
        HashSet<PhoneNumber> srcSet2 = new HashSet<>(
                srcListNumber);
        dstSet.removeAll(srcSet2);
        return dstSet.isEmpty();
    }

    public String getNameUnicode() {
        return nameUnicode;
    }

    public void setNameUnicode(String nameUnicode) {
        this.nameUnicode = nameUnicode;
    }

    @Override
    public String toString() {
        StringBuilder temp = new StringBuilder(TAG).append(" : ");
        temp.append("ContactId: ").append(contactId).append(",");
        temp.append("Name: ").append(name).append(",");
        temp.append("Favorite: ").append(favorite).append(";");
        if (listNumbers != null) {
            temp.append("Number Size: ").append(listNumbers.size()).append(";");
        } else {
            temp.append("Number Size: ").append("null").append(";");
        }
        return temp.toString();
    }
}

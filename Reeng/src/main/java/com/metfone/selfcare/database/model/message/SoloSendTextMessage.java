package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.MessageType;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Date;

/**
 * Created by thaodv on 7/12/2014.
 */
public class SoloSendTextMessage extends ReengMessage {
    public SoloSendTextMessage(ThreadMessage thread, String from, String to, String content) {
        super(thread.getThreadType(), MessageType.text);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setContent(content);
        setReadState(ReengMessageConstant.READ_STATE_READ);
        setStatus(ReengMessageConstant.STATUS_LOADING);
        setMessageType(MessageType.text);
        setDirection(Direction.send);
        Date date = new Date();
        setTime(date.getTime());
    }
}

package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.MessageType;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Date;

/**
 * Created by thaodv on 7/18/2014.
 */
public class SoloSendShareContactMessage extends ReengMessage {
    public SoloSendShareContactMessage(ThreadMessage thread, String from, String to, String contactName, String phoneNumber) {
        super(thread.getThreadType(), MessageType.shareContact);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setContent(contactName);
        setReadState(ReengMessageConstant.READ_STATE_READ);
        setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        setMessageType(MessageType.shareContact);
        setDirection(Direction.send);
        Date date = new Date();
        setTime(date.getTime());
        setFileName(phoneNumber);
    }
}

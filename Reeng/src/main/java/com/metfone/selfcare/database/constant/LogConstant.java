package com.metfone.selfcare.database.constant;

/**
 * Created by thanhnt72 on 10/16/2017.
 */

public class LogConstant {

    public static final String TABLE = "LOGKQI";
    public static final String ID = "id";
    public static final String CONTENT = "content";
    public static final String TYPE = "type";
    public static final String INFO = "info";
    public static final String COMMENT = "comment";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE +
            " (" + ID + " INTEGER PRIMARY KEY, " +
            CONTENT + " TEXT, " + TYPE + " TEXT, " + INFO + " TEXT, " + COMMENT + " TEXT)";
    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String DROP_STATEMENT_OLD = "DROP TABLE IF EXISTS LOG";
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;

    public static final String ALTER_COLUMN_TYPE = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + TYPE + " TEXT;";

    public static final String ALTER_COLUMN_INFO = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + INFO + " TEXT;";

    public static final String ALTER_COLUMN_COMMENT = "ALTER TABLE " + TABLE + " ADD COLUMN "
            + COMMENT + " TEXT;";
}

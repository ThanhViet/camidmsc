package com.metfone.selfcare.database.model;

import android.text.TextUtils;

import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.HTTP.OFFICER;
import com.metfone.selfcare.helper.TextHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by toanvk2 on 11/26/14.
 */
public class OfficerAccount implements Serializable {
    private int id;
    protected String serverId = null;
    protected String name = null;
    protected String nameUnicode;
    protected String avatarUrl = null;
    protected String description = null;
    private int follows = -1;
    private int iOrder = -1;
    private int groupId = -1;
    private String background = null;
    private String serviceAction;

    protected int state;
    protected int type = OfficerAccountConstant.TYPE_OFFICER;
    protected int roomState = OfficerAccountConstant.ROOM_STATE_NONE; //chua join thanh cong

    //=1 neu la CSKH, Mocha; =2 neu la cac OA nghiep vu
    private int officerType = OfficerAccountConstant.OFFICER_TYPE_NORMAL;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if (!TextUtils.isEmpty(name)) {
            nameUnicode = TextHelper.getInstant().convertUnicodeToAscci(name);
        } else {
            nameUnicode = "";
        }
    }

    public String getNameUnicode() {
        return nameUnicode;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRoomState() {
        return roomState;
    }

    public int getJoinStateFromRoomState() {
        if (roomState > 1) {
            return 1;
        } else {
            return roomState;
        }
    }

    public void setRoomState(int roomState) {
        this.roomState = roomState;
    }

    public int getFollows() {
        return follows;
    }

    public void setFollows(int follows) {
        this.follows = follows;
    }

    public int getiOrder() {
        return iOrder;
    }

    public void setiOrder(int iOrder) {
        this.iOrder = iOrder;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }


    public String getServiceAction() {
        return serviceAction;
    }

    public void setServiceAction(String serviceAction) {
        this.serviceAction = serviceAction;
    }

    public void setJsonObject(JSONObject object, int type) throws JSONException {
        // serviceId
        if (object.has(OFFICER.SERVER_ID)) {
            serverId = object.getString(OFFICER.SERVER_ID);
        }
        // name
        if (object.has(OFFICER.ALIAS)) {
            setName(object.getString(OFFICER.ALIAS));
        }
        //status
        if (object.has(OFFICER.AVATAR_URL)) {
            avatarUrl = object.getString(OFFICER.AVATAR_URL);
        }
        //description
        if (object.has(OFFICER.DESCRIPTION)) {
            description = object.getString(OFFICER.DESCRIPTION);
        }
        //avatar
        if (object.has(Constants.HTTP.OFFICER.ROOM_BACKGROUND)) {
            background = object.getString(Constants.HTTP.OFFICER.ROOM_BACKGROUND);
        }
        //follows
        if (object.has(Constants.HTTP.OFFICER.ROOM_FOLLOWS)) {
            follows = object.getInt(Constants.HTTP.OFFICER.ROOM_FOLLOWS);
        }
        // iOrder
        if (object.has(Constants.HTTP.OFFICER.ROOM_ORDER)) {
            iOrder = object.getInt(Constants.HTTP.OFFICER.ROOM_ORDER);
        }
        // group id
        if (object.has(Constants.HTTP.OFFICER.ROOM_GROUP_ID)) {
            groupId = object.getInt(Constants.HTTP.OFFICER.ROOM_GROUP_ID);
        }
        //officer type
        if (object.has(OFFICER.OFFICIAL_TYPE)) {
            officerType = object.getInt(OFFICER.OFFICIAL_TYPE);
        }
        // lay tu server thi mac dinh la sticky
        state = OfficerAccountConstant.OFFICER_STATE_STICKY;
        this.type = type;
    }

    public int getOfficerType() {
        return officerType;
    }

    public void setOfficerType(int officerType) {
        this.officerType = officerType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb
                .append("id: ").append(id)
                .append(",serverId: ").append(serverId)
                .append(",name ").append(name)
                .append(",avatarUrl ").append(avatarUrl)
                .append(",description ").append(description)
                .append(",state ").append(state)
                .append(",type ").append(type).toString();
    }

}
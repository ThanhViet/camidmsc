package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Date;

/**
 * Created by thaodv on 26-Nov-14.
 */
public class SoloShareMusicMessage extends ReengMessage {
    public SoloShareMusicMessage(ThreadMessage thread, String from, String to) {
        super();// thương hop nay co nhieu subtype. tuy th make packet se set packetId sau.
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setReadState(ReengMessageConstant.READ_STATE_READ);
        setChatMode(ReengMessageConstant.MODE_IP_IP);
        setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        setDirection(Direction.send);
        setSize(1);
        Date date = new Date();
        setTime(date.getTime());
    }
}

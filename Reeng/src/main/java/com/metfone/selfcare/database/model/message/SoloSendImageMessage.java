package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.constant.ReengMessageConstant.MessageType;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.MessageHelper;

import java.util.Date;

/**
 * Created by thaodv on 7/12/2014.
 */
public class SoloSendImageMessage extends ReengMessage {
    public SoloSendImageMessage(ThreadMessage thread, String from, String to, String filePath) {
        super(thread.getThreadType(), MessageType.image);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setFilePath(filePath);
        setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        setMessageType(MessageType.image);
        setDirection(Direction.send);
        Date date = new Date();
        setTime(date.getTime());
        setReadState(ReengMessageConstant.READ_STATE_READ);
        //get file name from file path
        setFileName(MessageHelper.getNameFileImage(getPacketId()));
    }
}

package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 6/26/14.
 */
public class NumberDataSource {
    // Database fields
    private static String TAG = NumberDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static NumberDataSource mInstance;

    public static synchronized NumberDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new NumberDataSource(application);
        }
        return mInstance;
    }

    private NumberDataSource(ApplicationController application) {
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    // set value
    public ContentValues setValues(PhoneNumber number) {
        ContentValues values = new ContentValues();
        values.put(NumberConstant.ID, number.getId());
        values.put(NumberConstant.NUMBER, number.getJidNumber());
        values.put(NumberConstant.STATUS, number.getStatus());
        values.put(NumberConstant.LAST_CHANGE_AVATAR, number.getLastChangeAvatar());
        // dung reeng =1, khong dung =0
        values.put(NumberConstant.STATE, number.getState());
        // moi dang ky =1; chua dk va da dk cu =0
        values.put(NumberConstant.IS_NEW, number.getRegister());
        values.put(NumberConstant.CONTACT_ID, number.getContactId());
        values.put(NumberConstant.NAME, number.getName());
        number.setAvatarName(number.getName());
        values.put(NumberConstant.NAME_UNICODE, number.getNameUnicode());
        values.put(NumberConstant.FAVORITE, number.getFavorite());
        values.put(NumberConstant.GENDER, number.getGender());
        values.put(NumberConstant.NUMBER_RAW, number.getRawNumber());
        values.put(NumberConstant.BIRTHDAY_STRING, number.getBirthdayString());
//        values.put(NumberConstant.COVER, number.getCoverUrl());
        values.put(NumberConstant.NICK_NAME, number.getNickName());
        values.put(NumberConstant.PERMISSION, number.getPermission());
        values.put(NumberConstant.OPERATOR, number.getOperator());
        values.put(NumberConstant.PREKEY, number.getPreKey());
        return values;
    }

    //insert list number
    public void insertListPhoneNumber(ArrayList<PhoneNumber> list, PhoneNumberUtil mPhoneUtil, String regionCode) {
        if (list == null || list.isEmpty()) {
            return;
        }
        // checkExistPhone();
        try {
            if (databaseWrite == null) {
                return;
            }
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (PhoneNumber number : list) {
                    // insert db
                    values = setValues(number);
                    // da add roster roi =1; chua add =0
                    values.put(NumberConstant.IS_ROSTER, number.getAddRoster());
                    databaseWrite.insert(NumberConstant.TABLE, null, values);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    // get values from cursor
    public PhoneNumber getValuesFromCursor(Cursor cur) {
        PhoneNumber number = new PhoneNumber();
        number.setId(cur.getString(0));
        number.setJidNumber(cur.getString(1));
        number.setStatus(cur.getString(2));
        number.setLastChangeAvatar(cur.getString(3));
        number.setState(cur.getInt(4));
        number.setRegister(cur.getInt(5));
        number.setContactId(cur.getString(6));
        number.setName(cur.getString(7));
        number.setAddRoster(cur.getInt(8));
        number.setNameUnicode(cur.getString(9));
        number.setFavorite(cur.getInt(10));
        number.setGender(cur.getInt(11));
        /*        String birthDay = cur.getString(12);
        if (birthDay == null || birthDay.length() <= 0) {
            number.setBirthDay(-1);
        } else {
            number.setBirthDay(Long.parseLong(birthDay));
        }*/
        String rawNumber = cur.getString(13);
        if (TextUtils.isEmpty(rawNumber)) {// truong hop update tu ban cu len ban moi thi truong jid = raw
            rawNumber = number.getJidNumber();
        }
        number.setRawNumber(rawNumber);
        number.setBirthdayString(cur.getString(14));
//        number.setCoverUrl(cur.getString(15));
        number.setNickName(cur.getString(16));
        number.setPermission(cur.getInt(17));
        number.setOperator(cur.getString(18));
        number.setPreKey(cur.getString(19));
        return number;
    }

    // get all phone number
    public ArrayList<PhoneNumber> getAllPhoneNumber() {
        ArrayList<PhoneNumber> listNumber = null;
        Cursor cursor = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(NumberConstant.SELECT_ALL_QUERY_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                listNumber = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        //PhoneNumber number = getValuesFromCursor(cursor);
                        listNumber.add(getValuesFromCursor(cursor));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllPhoneNumber", e);
        } finally {
            closeCursor(cursor);
        }
        return listNumber;
    }

    public ArrayList<PhoneNumber> getPhoneNumberUnSendRoster() {
        ArrayList<PhoneNumber> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query = NumberConstant.SELECT_UNSEND_ROSTER_STATEMENT;
            cursor = databaseRead.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    PhoneNumber number = getValuesFromCursor(cursor);
                    list.add(number);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getPhoneNumberUnSendRoster", e);
        } finally {
            closeCursor(cursor);
        }
        return list;
    }

    public void updateAllStateSendRoster() {
        try {
            Log.d(TAG, " update all state roster: ");
            ContentValues values = new ContentValues();
            values.put(NumberConstant.IS_ROSTER, 1);
            databaseWrite.update(NumberConstant.TABLE, values, null,
                    null);
            Log.d(TAG, " update all state roster end: ");
        } catch (Exception e) {
            Log.e(TAG, "updateListPhoneNumber", e);
        }
    }

    public void updateListStateSendRoster(ArrayList<PhoneNumber> listPhone) {
        if (listPhone == null || listPhone.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values = new ContentValues();
                for (PhoneNumber number : listPhone) {
                    number.setAddRoster(1);
                    values.put(NumberConstant.IS_ROSTER, 1);
                    databaseWrite.update(NumberConstant.TABLE, values, NumberConstant.WHERE_NUMBER,
                            new String[]{number.getJidNumber()});
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction updateListPhoneNumber", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "updateListPhoneNumber", e);
        }
    }

    // update list item
    public void updateListPhoneNumber(ArrayList<PhoneNumber> list, boolean updateRoster) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                ContentValues values;
                for (PhoneNumber number : list) {
                    number.setNameUnicode(TextHelper.getInstant().convertUnicodeToAscci(number.getName()));
                    values = setValues(number);
                    if (updateRoster) {
                        values.put(NumberConstant.IS_ROSTER, number.getAddRoster());
                    }
                    databaseWrite.update(NumberConstant.TABLE, values, NumberConstant.WHERE_ID,
                            new String[]{number.getId()});
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction updateListPhoneNumber", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "updateListPhoneNumber", e);
        }
    }

    // delete list item
    public void deleteListPhoneNumber(ArrayList<PhoneNumber> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (PhoneNumber number : list) {
                    databaseWrite.delete(NumberConstant.TABLE,
                            NumberConstant.WHERE_ID, new String[]{number.getId()});
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "transaction deleteListPhoneNumber", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteListPhoneNumber", e);
        }
    }

    public void deleteListPhoneNumberByContactId(String contactId) {
        if (contactId == null) {
            return;
        }
        try {
            databaseWrite.delete(NumberConstant.TABLE,
                    NumberConstant.WHERE_CONTACT_ID, new String[]{contactId});
        } catch (Exception e) {
            Log.e(TAG, "deleteListPhoneNumber", e);
        }
    }

    // del table
    public void deleteAllTable() {
        try {
            databaseWrite.execSQL(NumberConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllContact", e);
        }
    }

    public void dropTable() {
        try {
            databaseWrite.execSQL(NumberConstant.DROP_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllContact", e);
        }
    }
}
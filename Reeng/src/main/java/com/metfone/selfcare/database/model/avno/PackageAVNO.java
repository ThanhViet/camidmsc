package com.metfone.selfcare.database.model.avno;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 4/12/2018.
 */

public class PackageAVNO implements Serializable {

    public static final int PACKAGE_AVAILABLE = 0;
    public static final int PACKAGE_ACTIVE = 1;
    public static final int PACKAGE_CHARGE_CALL = -1;
    public static final int PACKAGE_CHARGE_SMS = -2;

    public static final int PACKAGE_TITLE = 100;
    public static final int PACKAGE_TITLE_SMS = 101;
    public static final int PACKAGE_TITLE_CALL = 102;


    private int id;
    private String title;
    private String desc;
    private int status;
    private String price;
    private String imgUrl;
    private String alert;
    private int stateFreeNumber;
    private int allowCancel;
    private int inUse;
    /*private String labelDeeplink;
    private String urlDeeplink;*/

    private String minutes;
    private String numbInString;
    private int maxNumb;
    private ArrayList<String> listNumber;

    private ArrayList<DeeplinkItem> deeplinkItems = new ArrayList<>();
    private ArrayList<FakeMOItem> fakeMOItems = new ArrayList<>();

    private int indexColor;


    public PackageAVNO() {
    }

    public PackageAVNO(int id, String title, String desc, int status) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.status = status;
    }

    public PackageAVNO(String title, String desc, int status, String price) {
        this.title = title;
        this.desc = desc;
        this.status = status;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public int getStateFreeNumber() {
        return stateFreeNumber;
    }

    public void setStateFreeNumber(int stateFreeNumber) {
        this.stateFreeNumber = stateFreeNumber;
    }

    public int getAllowCancel() {
        return allowCancel;
    }

    public void setAllowCancel(int allowCancel) {
        this.allowCancel = allowCancel;
    }

    /*public String getLabelDeeplink() {
        return labelDeeplink;
    }

    public void setLabelDeeplink(String labelDeeplink) {
        this.labelDeeplink = labelDeeplink;
    }

    public String getUrlDeeplink() {
        return urlDeeplink;
    }

    public void setUrlDeeplink(String urlDeeplink) {
        this.urlDeeplink = urlDeeplink;
    }*/

    public ArrayList<DeeplinkItem> getDeeplinkItems() {
        return deeplinkItems;
    }

    public void setDeeplinkItems(ArrayList<DeeplinkItem> deeplinkItems) {
        this.deeplinkItems = deeplinkItems;
    }

    public ArrayList<FakeMOItem> getFakeMOItems() {
        return fakeMOItems;
    }

    public void setFakeMOItems(ArrayList<FakeMOItem> fakeMOItems) {
        this.fakeMOItems = fakeMOItems;
    }

    public int getInUse() {
        return inUse;
    }

    public void setInUse(int inUse) {
        this.inUse = inUse;
    }


    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }


    public String getNumbInString() {
        return numbInString;
    }

    public void setNumbInString(String numbInString) {
        this.numbInString = numbInString;
    }

    public int getMaxNumb() {
        return maxNumb;
    }

    public void setMaxNumb(int maxNumb) {
        this.maxNumb = maxNumb;
    }

    public ArrayList<String> getListNumber() {
        return listNumber;
    }

    public void setListNumber(ArrayList<String> listNumber) {
        this.listNumber = listNumber;
    }

    public int getIndexColor() {
        return indexColor;
    }

    public void setIndexColor(int indexColor) {
        this.indexColor = indexColor;
    }
}

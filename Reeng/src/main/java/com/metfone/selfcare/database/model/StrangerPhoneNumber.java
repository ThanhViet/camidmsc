package com.metfone.selfcare.database.model;

import android.text.TextUtils;

import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.util.Log;

import java.io.Serializable;

/**
 * Created by toanvk2 on 5/8/2015.
 */
public class StrangerPhoneNumber implements Serializable {
    private static final String TAG = StrangerPhoneNumber.class.getSimpleName();
    private long strangerId;
    private String phoneNumber;
    private String myName;     // ten cua minh tren app khac
    private String friendName; // tem cua ban be tren app khac
    private String appId;
    private String friendAvatarName;
    private String friendAvatarUrl;
    private String stateString;   // trang thai dong y hay chua
    private StrangerType strangerType = StrangerType.mocha_stranger;
    private StateAccept state = StateAccept.accepted;
    private long createDate = 0;

    public long getStrangerId() {
        return strangerId;
    }

    public void setStrangerId(long strangerId) {
        this.strangerId = strangerId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
        setFriendAvatarName(friendName);
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        if (TextUtils.isEmpty(appId)) {
            appId = "mocha_stranger";
        } else if ("stranger_music".equals(appId)) {
            appId = "music_stranger"; // doi app id, th ban cu luu db la stranger music
        }
        this.appId = appId;
        this.strangerType = setStrangerType(this.appId);
    }

    public String getFriendAvatarName() {
        return friendAvatarName;
    }

    private void setFriendAvatarName(String friendName) {
        this.friendAvatarName = PhoneNumberHelper.getInstant().getAvatarNameFromName(friendName);
    }

    public String getFriendAvatarUrl() {
        return friendAvatarUrl;
    }

    public void setFriendAvatarUrl(String avatarFriendUrl) {
        this.friendAvatarUrl = avatarFriendUrl;
    }

    public String getStateString() {
        return stateString;
    }

    public void setStateString(String stateString) {
        this.stateString = stateString;
        this.state = StateAccept.fromString(stateString);
    }

    public StateAccept getState() {
        return state;
    }

    public StrangerType getStrangerType() {
        return strangerType;
    }

    public enum StateAccept {
        accepted, not_accept;

        public static StateAccept fromString(String state) {
            if (TextUtils.isEmpty(state)) {
                return accepted;
            } else {
                try {
                    return StateAccept.valueOf(state);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return accepted;
                }
            }
        }
    }

    public enum StrangerType {
        other_app_stranger, music_stranger, mocha_stranger, talk_stranger
    }

    private StrangerType setStrangerType(String appId) {
        if (!TextUtils.isEmpty(appId)) {
            if ("music_stranger".equals(appId) || "stranger_music".equals(appId)) {
                return StrangerType.music_stranger;
            } else if (appId.startsWith("mocha_")) {
                return StrangerType.mocha_stranger;
            } else if ("talk_stranger".equals(appId)) {
                return StrangerType.talk_stranger;
            } else {
                return StrangerType.other_app_stranger;
            }
        } else {
            return StrangerType.mocha_stranger;
        }
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public boolean equalsValues(Object obj) {
        if (obj == null) {
            return false;
        }
        if (StrangerPhoneNumber.class != obj.getClass()) {
            return false;
        }
        StrangerPhoneNumber other = (StrangerPhoneNumber) obj;
        String otherName = other.getMyName();
        String otherFriendName = other.getFriendName();
        String otherAvatar = other.getFriendAvatarUrl();
        if (otherFriendName != null && otherFriendName.equals(friendName)) {
            if (otherAvatar != null && otherAvatar.equals(friendAvatarUrl)) {
                if (otherName != null && otherName.equals(myName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean equalsValues(String otherName, String otherFriendName, String otherAvatar) {
        if (otherFriendName != null && otherFriendName.equals(friendName)) {
            if (otherAvatar != null && otherAvatar.equals(friendAvatarUrl)) {
                if (otherName != null && otherName.equals(myName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
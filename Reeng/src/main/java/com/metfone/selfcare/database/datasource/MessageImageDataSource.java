package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.MessageImageDBConstant;
import com.metfone.selfcare.database.model.message.MessageImageDB;
import com.metfone.selfcare.util.Log;

import java.util.HashMap;

/**
 * Created by thanhnt72 on 8/1/2018.
 */

public class MessageImageDataSource {

    private static String TAG = MessageImageDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static MessageImageDataSource mInstance;
    private ApplicationController mApp;


    public static synchronized MessageImageDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new MessageImageDataSource(application);
        }
        return mInstance;
    }

    private MessageImageDataSource(ApplicationController application) {
        mApp = application;
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null)
            cursor.close();
    }

    public long insertMessageImageUpload(MessageImageDB model) {
        try {
            if (databaseWrite == null) {
                return -1;
            }
            ContentValues values = new ContentValues();
            values.put(MessageImageDBConstant.FILE_PATH_ORI, model.getPathOri());
            values.put(MessageImageDBConstant.FILE_PATH_NEW, model.getPathNew());
            values.put(MessageImageDBConstant.DATA_RESPONSE, model.getDataResponse());
            values.put(MessageImageDBConstant.TIME_UPLOAD, String.valueOf(model.getTimeUpload()));
            values.put(MessageImageDBConstant.RATIO, model.getRatio());
            long id = databaseWrite.insert(MessageImageDBConstant.TABLE, null, values);
            Log.i(TAG, "insertMessageImageUpload  success " + id);
            return id;
        } catch (Exception e) {
            Log.e(TAG, "insertMessageImageUpload fail", e);
            return -1;
        }
    }


    public HashMap<String, MessageImageDB> getAllMessageImageUpload() {
        Cursor cursor = null;
        HashMap<String, MessageImageDB> listMessageImageUpload = new HashMap<>();
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(MessageImageDBConstant.SELECT_ALL_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        long id = cursor.getLong(0);
                        String pathOri = cursor.getString(1);
                        String pathNew = cursor.getString(2);
                        String dataResponse = cursor.getString(3);
                        String ratio = cursor.getString(4);
                        long timeStamp = cursor.getLong(5);
                        String digit = cursor.getString(6);

                        MessageImageDB messageImageDB = new MessageImageDB(id, pathOri, pathNew
                                , dataResponse, ratio, timeStamp, digit);
                        listMessageImageUpload.put(pathOri, messageImageDB);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllLog", e);
        } finally {
            closeCursor(cursor);
        }
        return listMessageImageUpload;
    }

    // del table
    public void deleteTable() {
        try {
            databaseWrite.execSQL(MessageImageDBConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }

    public void deleteMessageImage(long id) {
        databaseWrite.delete(MessageImageDBConstant.TABLE, MessageImageDBConstant.ID
                + " = " + id, null);
    }
}

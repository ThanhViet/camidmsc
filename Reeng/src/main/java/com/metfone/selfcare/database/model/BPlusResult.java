package com.metfone.selfcare.database.model;

import java.io.Serializable;

/**
 * Created by toanvk2 on 10/24/2017.
 */

public class BPlusResult implements Serializable {
    private String friendJid;
    private String friendName;
    private long amount;
    private String amountStr;
    private String desc;
    private String transferId;
    private int type;

    public BPlusResult() {
    }

    public BPlusResult(String friendJid, String friendName, long amount, String amountStr, String desc, String transferId, int type) {
        this.friendJid = friendJid;
        this.friendName = friendName;
        this.amount = amount;
        this.amountStr = amountStr;
        this.desc = desc;
        this.transferId = transferId;
        this.type = type;
    }

    public String getFriendJid() {
        return friendJid;
    }

    public void setFriendJid(String friendJid) {
        this.friendJid = friendJid;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getAmountStr() {
        return amountStr;
    }

    public void setAmountStr(String amountStr) {
        this.amountStr = amountStr;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

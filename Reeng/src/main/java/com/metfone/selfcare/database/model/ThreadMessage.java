package com.metfone.selfcare.database.model;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.message.PinMessage;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thaodv on 6/30/2014.
 */
public class ThreadMessage implements Serializable {
    private static final String TAG = ThreadMessage.class.getSimpleName();
    private int id;
    private int threadType;
    private String name;
    private ArrayList<String> phoneNumbers;
    private ArrayList<String> adminNumbers;// danh sach admin group, danh sach nay voi danh sach member co the trung nhau
    private String serverId;
    private boolean isRead;
    private boolean isJoined = true;
    private int numOfUnreadMessage = 0;
    private long timeOfLast;
    private String draftMessage = null;
    private String background = null;
    private int state = ThreadMessageConstant.STATE_SHOW;
    private CopyOnWriteArrayList<ReengMessage> allMessages = new CopyOnWriteArrayList<>();
    private boolean mLoadMoreFinish = false;
    private boolean showTyping = false;
    // luu tat ca cac anh da load duoc tu DB,
    private ArrayList<String> mListImage = new ArrayList<>();
    private String mAvatarToken = ""; // group
    private String mOldAvatarToken = null;
    private long dhVtt;
    private String groupAvatar;
    private boolean isStranger = false;
    private StrangerPhoneNumber strangerPhoneNumber;
    private long lastTimeSaveDraft;
    private String phoneNumberTyping;
    private String numberSearchGroup;
    private boolean hasNewMessage = false;
    private int groupClass = 0;
    // room chat info. luu tren mem
    private String descSongStar;
    private ArrayList<MediaModel> listSongStars;
    private int stateOnlineStar, stateMusicRoom = 0;
    private int followStar = -1;
    private boolean isReeng = true;
    //test luu last message id
    private int lastMessageId;
    private boolean isLoadDetail = false;
    private boolean isAdmin = false;// check xem minh co phai admin ko, luu tren mem
    private String soloNumber = "";
    private boolean isLoadNonContact = true;
    private String avatarVerify;
    private boolean isForeUpdateBg = false;
    private boolean isChecked = false;
    private int lastPositionVideo;
    private MediaModel mediaModel;
    private PinMessage pinMessage;
    private long lastTimePinThread;
    private boolean isLastPin;
    private int hiddenThread = 0;
    private String messageInvite;
    private boolean actionInvite;

    public boolean isActionInvite() {
        return actionInvite;
    }

    public void setActionInvite(boolean actionInvite) {
        this.actionInvite = actionInvite;
    }

    public String getMessageInvite() {
        return messageInvite;
    }

    public void setMessageInvite(String messageInvite) {
        this.messageInvite = messageInvite;
    }

    //4 tham so duoi dung cho gen avatar group
    private ArrayList<Object> listMemberAvatar = new ArrayList<>();
    private ArrayList<String> listAllMember;
    private boolean forceCalculatorAllMember;

    private HashMap<String, Integer> pollLastId = new HashMap<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getThreadType() {
        return threadType;
    }

    public void setThreadType(int threadType) {
        this.threadType = threadType;
    }

    public String getThreadName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        } else {
            this.name = "";
        }
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getServerId() {
        return this.serverId;
    }

    public boolean isJoined() {
        return isJoined;
    }

    public void setJoined(boolean isJoined) {
        this.isJoined = isJoined;
    }

    public int getNumOfUnreadMessage() {
        return numOfUnreadMessage;
    }

    public void setNumOfUnreadMessage(int numOfUnreadMessage) {
        if (numOfUnreadMessage < 0) numOfUnreadMessage = 0;
        this.numOfUnreadMessage = numOfUnreadMessage;
    }

    public long getTimeOfLast() {
        return timeOfLast;
    }

    public void setTimeOfLast(long timeOfLast) {
        this.timeOfLast = timeOfLast;
    }

    public String getDraftMessage() {
        return draftMessage;
    }

    public void setDraftMessage(String draftMessage) {
        this.draftMessage = draftMessage;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public boolean isHasNewMessage() {
        return hasNewMessage;
    }

    public void setHasNewMessage(boolean hasNewMessage) {
        this.hasNewMessage = hasNewMessage;
    }

    public CopyOnWriteArrayList<ReengMessage> getAllMessages() {
        if (allMessages == null) allMessages = new CopyOnWriteArrayList<>();
        return allMessages;
    }

    public void setAllMessages(CopyOnWriteArrayList<ReengMessage> allMessages) {
        this.allMessages = allMessages;
    }

    public void addMoreMessage(CopyOnWriteArrayList<ReengMessage> moreMessages) {
        if (allMessages == null || allMessages.isEmpty()) {
            this.allMessages = moreMessages;
        } else {
            this.allMessages.addAll(0, moreMessages);
        }
    }

    public void setLoadMoreFinish(boolean loadMoreFinish) {
        this.mLoadMoreFinish = loadMoreFinish;
    }

    public boolean isLoadMoreFinish() {
        return mLoadMoreFinish;
    }

    public void setPhoneNumbers(ArrayList<String> numbers) {
        this.phoneNumbers = numbers;
        setSoloNumberAndNumberSearchGroup();
    }

    public void setSoloNumberAndNumberSearchGroup() {
        setSoloNumber();
        numberSearchGroup = getStringFromListPhone(phoneNumbers);
    }

    public ArrayList<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    private void setSoloNumber() {
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            if (phoneNumbers != null && !phoneNumbers.isEmpty())
                soloNumber = phoneNumbers.get(0);
            else
                soloNumber = "";
        }
    }

    public String getSoloNumber() {
        return soloNumber;
    }

    public ArrayList<String> getAdminNumbers() {
        return adminNumbers;
    }

    public void setAdminNumbers(ArrayList<String> adminNumbers) {
        this.adminNumbers = adminNumbers;
    }

    public boolean isShowTyping() {
        return showTyping;
    }

    public void setShowTyping(boolean showTyping) {
        this.showTyping = showTyping;
    }

    public ArrayList<String> getListImage() {
        return mListImage;
    }

    public void setListImage(ArrayList<String> mListImage) {
        this.mListImage = mListImage;
    }

    public String getNumberSearchGroup() {
        if (numberSearchGroup == null) {
            numberSearchGroup = getStringFromListPhone(phoneNumbers);
        }
        return numberSearchGroup;
    }

    public void addNumberToThread(String number) {
        if (phoneNumbers == null) {
            phoneNumbers = new ArrayList<>();
        }
        if (!phoneNumbers.contains(number)) {
            phoneNumbers.add(number);
            numberSearchGroup = getStringFromListPhone(phoneNumbers);
        }
        setSoloNumber();
    }

    public void removeNumberFromThread(String number) {
        if (phoneNumbers == null) {
            phoneNumbers = new ArrayList<>();
            return;
        }
        if (phoneNumbers.contains(number)) {
            phoneNumbers.remove(number);
            numberSearchGroup = getStringFromListPhone(phoneNumbers);
        }

        if (listAllMember != null && listAllMember.contains(number))
            listAllMember.remove(number);
    }

    public void addAdminToThread(String number) {
        if (adminNumbers == null) {
            adminNumbers = new ArrayList<>();
        }
        if (!adminNumbers.contains(number)) {
            adminNumbers.add(number);
        }
    }

    public void removeAdminFromThread(String number) {
        if (adminNumbers == null) {
            adminNumbers = new ArrayList<>();
            return;
        }
        adminNumbers.remove(number);
    }

    public void insertNewMessage(ReengMessage reengMessage) {
        Log.i(TAG, "insert new message " + reengMessage);
        if (allMessages == null) allMessages = new CopyOnWriteArrayList<>();
        allMessages.add(reengMessage);
    }

    public void insertListMessage(List<ReengMessage> reengMessageList) {
        Log.i(TAG, "insertListMessage size " + reengMessageList.size());
        if (allMessages == null) allMessages = new CopyOnWriteArrayList<>();
        allMessages.addAll(reengMessageList);
    }

    public long getLastChangeThread() {
        if (allMessages != null && !allMessages.isEmpty()) {
            ReengMessage lastMsg = allMessages.get(allMessages.size() - 1);
            return lastMsg.getTime();
        }
        return timeOfLast;
    }

    public long getDhVtt() {
        return dhVtt;
    }

    public void setDhVtt(long dhVtt) {
        this.dhVtt = dhVtt;
    }

    public boolean isStranger() {
        return isStranger;
    }

    public void setStranger(boolean isStranger) {
        this.isStranger = isStranger;
    }

    public StrangerPhoneNumber getStrangerPhoneNumber() {
        return strangerPhoneNumber;
    }

    public void setStrangerPhoneNumber(StrangerPhoneNumber strangerPhoneNumber) {
        this.strangerPhoneNumber = strangerPhoneNumber;
    }

    public long getLastTimeSaveDraft() {
        return lastTimeSaveDraft;
    }

    public void setLastTimeSaveDraft(long lastTimeSaveDraft) {
        this.lastTimeSaveDraft = lastTimeSaveDraft;
    }

    public String getPhoneNumberTyping() {
        return phoneNumberTyping;
    }

    public void setPhoneNumberTyping(String phoneNumberTyping) {
        this.phoneNumberTyping = phoneNumberTyping;
    }

    public String genAvatarToken(ApplicationController mApplication) {
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        ReengAccountBusiness reengAccountBusiness = mApplication.getReengAccountBusiness();
        String myLastChangeAvatar = reengAccountBusiness.getLastChangeAvatar();
        if (phoneNumbers == null) {
            phoneNumbers = new ArrayList<>();
        }
        PhoneNumber phoneNumber;
        StringBuilder sb = new StringBuilder(id);
        sb.append(phoneNumbers.size() + 1).append("-").append(myLastChangeAvatar);
        for (String numberFriend : phoneNumbers) {
            phoneNumber = contactBusiness.getPhoneNumberFromNumber(numberFriend);
            if (phoneNumber != null && phoneNumber.isReeng() && phoneNumber.getLastChangeAvatar() != null) {
                sb.append("|").append(numberFriend).append(phoneNumber.getLastChangeAvatar());
            }
        }
        mAvatarToken = EncryptUtil.encryptMD5(sb.toString());
        return mAvatarToken;
    }

    public String getOldAvatarToken() {
        return mOldAvatarToken;
    }

    public void setOldAvatarToken(String oldAvatarToken) {
        this.mOldAvatarToken = oldAvatarToken;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public boolean isShowThread() {
        return state == ThreadMessageConstant.STATE_SHOW || state == ThreadMessageConstant.STATE_PRIVATE_SHOW;
    }

    public boolean isPrivateThread() {
        return state == ThreadMessageConstant.STATE_PRIVATE_SHOW || state == ThreadMessageConstant.STATE_PRIVATE_HIDE;
    }

    public void setPrivateThread(int statePrivate) {
        if (statePrivate == 0) {// nhom thuong
            // truoc do la private an thi chuyen thanh state an, nguoc lai thi de hien
            if (state == ThreadMessageConstant.STATE_PRIVATE_HIDE) {
                state = ThreadMessageConstant.STATE_HIDE;
            } else {
                state = ThreadMessageConstant.STATE_SHOW;
            }
        } else if (statePrivate == 1) {// nhom private
            if (state == ThreadMessageConstant.STATE_HIDE) {
                state = ThreadMessageConstant.STATE_PRIVATE_HIDE;
            } else {
                state = ThreadMessageConstant.STATE_PRIVATE_SHOW;
            }
        }
    }

    public String getDescSongStar() {
        return descSongStar;
    }

    public void setDescSongStar(String descSongStar) {
        this.descSongStar = descSongStar;
    }

    public ArrayList<MediaModel> getListSongStars() {
        return listSongStars;
    }

    public void setListSongStars(ArrayList<MediaModel> listSongStars) {
        this.listSongStars = listSongStars;
    }

    public int getStateOnlineStar() {
        return stateOnlineStar;
    }

    public void setStateOnlineStar(int stateOnlineStar) {
        this.stateOnlineStar = stateOnlineStar;
    }

    public int getStateMusicRoom() {
        return stateMusicRoom;
    }

    public void setStateMusicRoom(int stateMusicRoom) {
        this.stateMusicRoom = stateMusicRoom;
    }

    public int getFollowStar() {
        return followStar;
    }

    public void setFollowStar(int followStar) {
        this.followStar = followStar;
    }

    public boolean isReeng() {
        return isReeng;
    }

    public void setIsReeng(boolean isReeng) {
        this.isReeng = isReeng;
    }

    public boolean isExitRoomInfo() {
        if (state != 0) {
            return false;
        }
        return listSongStars != null && !listSongStars.isEmpty() &&
                !TextUtils.isEmpty(descSongStar);
    }

    public boolean isReadyShow(MessageBusiness messageBusiness) {
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return getAllMessages() != null && (!getAllMessages().isEmpty() || isShowThread());
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            /*String numberFriend = getPhoneNumbers().get(0);
                setName(messageBusiness.getFriendName(numberFriend));*/
            return getAllMessages() != null && (!getAllMessages().isEmpty() || !TextUtils.isEmpty(getDraftMessage()));
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            return getAllMessages() != null && (!getAllMessages().isEmpty() || !TextUtils.isEmpty(getDraftMessage()));
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            return getAllMessages() != null && (!getAllMessages().isEmpty() || !TextUtils.isEmpty(getDraftMessage()));
        } else {
            return getAllMessages() != null;
        }
    }

    public boolean isReadyShow() {
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return getAllMessages() != null && (!getAllMessages().isEmpty() || isShowThread());
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            /*String numberFriend = getPhoneNumbers().get(0);
                setName(messageBusiness.getFriendName(numberFriend));*/
            return getAllMessages() != null && (!getAllMessages().isEmpty() || !TextUtils.isEmpty(getDraftMessage()));
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            return getAllMessages() != null && (!getAllMessages().isEmpty() || !TextUtils.isEmpty(getDraftMessage()));
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            return true;
        } else {
            return getAllMessages() != null;
        }
    }

    public int getLastMessageId() {
        return lastMessageId;
    }

    public void setLastMessageId(int lastMessageId) {
        this.lastMessageId = lastMessageId;
    }

    public boolean isLoadDetail() {
        return isLoadDetail;
    }

    public void setLoadDetail(boolean isLoadDetail) {
        this.isLoadDetail = isLoadDetail;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb
                .append("id: ").append(id)
                .append(",name ").append(name)
                .append(",threadType ").append(threadType)
                .append(",numOfUnreadMessage ").append(numOfUnreadMessage)
                .append(",timeOfLast ").append(timeOfLast)
                .append(",draftMessage ").append(draftMessage)
                .append(",background ").append(background)
                .append(",dhVtt ").append(dhVtt)
                .append(",is admin ").append(isAdmin)
                .append(",stranger ").append(isStranger)
                .toString();
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setGroupAvatar(String groupAvatar) {
        // xu dung lai truong last show invite nen can check value cu ==0
        if ("0".equals(groupAvatar)) {
            this.groupAvatar = null;
        } else {
            this.groupAvatar = groupAvatar;
        }
    }

    public String getAvatarVerify(String content) {
        if (TextUtils.isEmpty(avatarVerify)) {
            avatarVerify = EncryptUtil.getVerify(content);
        }
        return avatarVerify;
    }

    public int getGroupClass() {
        return groupClass;
    }

    public void setGroupClass(int groupClass) {
        this.groupClass = groupClass;
    }

    public boolean isEncryptThread() {
        return threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && groupClass == ThreadMessageConstant.THREAD_ENCRYPT;
    }

    public void setEncryptThread(int state) {
        if (state == 1) groupClass = ThreadMessageConstant.THREAD_ENCRYPT;
        else groupClass = ThreadMessageConstant.GROUP_TYPE_NORMAL;
    }

    private String getStringFromListPhone(ArrayList<String> arrayNumbers) {
        if (arrayNumbers != null && arrayNumbers.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String number : arrayNumbers) {
                sb.append(number).append(" ");
            }
            int lengText = sb.length();
            sb.deleteCharAt(lengText - 1);
            return sb.toString();
        } else {
            return "";
        }
    }

    public boolean isLoadNonContact() {
        return isLoadNonContact;
    }

    public void setLoadNonContact(boolean isLoadNonContact) {
        this.isLoadNonContact = isLoadNonContact;
    }

    public boolean isForeUpdateBg() {
        return isForeUpdateBg;
    }

    public void setForeUpdateBg(boolean foreUpdateBg) {
        this.isForeUpdateBg = foreUpdateBg;
    }

    public boolean isHideHeaderSuggestSticker() {
        if (isStranger()) {
            return getAllMessages() != null && !getAllMessages().isEmpty();
        }
        return true;
    }

    public int getLastPositionVideo() {
        return lastPositionVideo;
    }

    public void setLastPositionVideo(int lastPositionVideo) {
        this.lastPositionVideo = lastPositionVideo;
    }

    public MediaModel getMediaModel() {
        return mediaModel;
    }

    public void setMediaModel(MediaModel mediaModel) {
        this.mediaModel = mediaModel;
    }

    public PinMessage getPinMessage() {
        return pinMessage;
    }

    public void setPinMessage(PinMessage pinMessage) {
        this.pinMessage = pinMessage;
    }

    public void setPinMessage(String jsonData) {
        pinMessage = new PinMessage();
        pinMessage.fromJson(jsonData);
    }

    public ArrayList<String> getListAllMemberIncludeAdmin(ApplicationController app) {
        if (listAllMember == null || listAllMember.isEmpty() || forceCalculatorAllMember) {
            if (phoneNumbers != null && !phoneNumbers.isEmpty()) {
                listAllMember = new ArrayList<>(phoneNumbers);
            } else {
                listAllMember = new ArrayList<>();
            }
            if (adminNumbers != null && !adminNumbers.isEmpty()) {
                for (String admin : adminNumbers) {
                    if (!listAllMember.contains(admin)) {
                        listAllMember.add(0, admin);
                    }
                }
            }
            if (!listAllMember.contains(app.getReengAccountBusiness().getJidNumber()))
                listAllMember.add(app.getReengAccountBusiness().getJidNumber());
            return listAllMember;
        } else
            return listAllMember;

    }

    public boolean isForceCalculatorAllMember() {
        return forceCalculatorAllMember;
    }

    public void setForceCalculatorAllMember(boolean forceCalculatorAllMember) {
        this.forceCalculatorAllMember = forceCalculatorAllMember;
        if (forceCalculatorAllMember) {
            listMemberAvatar = new ArrayList<>();
        }
    }

    public ArrayList<Object> getListMemberAvatar() {
        return listMemberAvatar;
    }

    public void setListMemberAvatar(ArrayList<Object> listMemberAvatar) {
        this.listMemberAvatar = listMemberAvatar;
    }

    public HashMap<String, Integer> getPollLastId() {
        return pollLastId;
    }

    public long getLastTimePinThread() {
        return lastTimePinThread;
    }

    public void setLastTimePinThread(long lastTimePinThread) {
        this.lastTimePinThread = lastTimePinThread;
    }

    public boolean isLastPin() {
        return isLastPin;
    }

    public void setLastPin(boolean lastPin) {
        isLastPin = lastPin;
    }

    public int getHiddenThread() {
        return hiddenThread;
    }

    public void setHiddenThread(int hiddenThread) {
        this.hiddenThread = hiddenThread;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (ThreadMessage.class != obj.getClass()) {
            return false;
        }
        ThreadMessage otherObject = (ThreadMessage) obj;
        int otherId = otherObject.getId();
        return otherId == id;
    }

    public boolean isPrivateOrEncrypt() {
        if (isEncryptThread()) return true;
        else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && isPrivateThread())
            return true;
        return false;

    }
}
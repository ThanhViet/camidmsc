package com.metfone.selfcare.database.constant;

/**
 * Created by sonnn00 on 7/17/2017.
 */

public class NoteMessageContant {
    public static final String TABLE = "note_message";
    public static final String ID = "id";
    public static final String THREAD_TYPE = "thread_type";
    public static final String THREAD_JID = "thread_jid";
    public static final String THREAD_NAME = "thread_name";
    public static final String THREAD_AVATAR = "thread_avatar";
    public static final String CONTENT = "content";
    public static final String STRANGER_ID = "stranger_id";
    public static final String TIMESTAMP = "timestamp";
    public static final int SIZE = 15;

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE +
            " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            THREAD_TYPE + " INTEGER, " +
            THREAD_JID + " TEXT, " +
            THREAD_NAME + " TEXT, " +
            THREAD_AVATAR + " TEXT, " +
            CONTENT + " TEXT, " +
            STRANGER_ID + " INTEGER, " +
            TIMESTAMP + " INTEGER)";

    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String SELECT_BY_ALLMODEL_ID_STATEMENT = "SELECT * FROM "
            + TABLE + " WHERE " + ID + " = ";
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;
    public static final String WHERE_ID = ID + " = ?";
    public static final String ORDER_BY = " ORDER BY " + TIMESTAMP + " DESC";
    public static final String LIMIT = " LIMIT " + SIZE + " OFFSET ";

    public static final String DELETE_ALL_DETAIL_STATEMENT = "DELETE FROM " + TABLE;
    //ALTER_COLUMN
//    public static final String ALTER_COLUMN_CRBT_CODE = "ALTER TABLE " + TABLE
//            + " ADD COLUMN " + CRBT_CODE + " TEXT;";
//    public static final String ALTER_COLUMN_CRBT_PRICE = "ALTER TABLE " + TABLE
//            + " ADD COLUMN " + CRBT_PRICE + " TEXT;";
}

package com.metfone.selfcare.database.constant;

/**
 * Created by toanvk2 on 25/03/2015.
 */
public class MediaConstant {
    public static final String TABLE = "media_table";
    public static final String SONG_ID = "song_id";
    public static final String NAME = "name";
    public static final String MEDIA_URL = "media_url";
    public static final String SINGER = "singer";
    public static final String IMAGE = "image_url";
    public static final String URL = "url";
    public static final String TYPE = "type";
    public static final String CRBT_CODE = "crbt_code";
    public static final String CRBT_PRICE = "crbt_price";

    public static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS " + TABLE +
            " (" + SONG_ID + " INTEGER, " +
            NAME + " TEXT, " +
            SINGER + " TEXT, " +
            IMAGE + " TEXT, " +
            MEDIA_URL + " TEXT, " +
            URL + " TEXT, " +
            TYPE + " INTEGER, " +
            CRBT_CODE + " TEXT, " +
            CRBT_PRICE + " TEXT)";

    public static final String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE;
    public static final String SELECT_ALL_STATEMENT = "SELECT * FROM " + TABLE;
    public static final String SELECT_MEDIA_BY_ID_STATEMENT = "SELECT * FROM "
            + TABLE + " WHERE " + SONG_ID + " = ";
    public static final String DELETE_ALL_STATEMENT = "DELETE FROM " + TABLE;
    public static final String WHERE_ID = SONG_ID + " = ?";
    //ALTER_COLUMN
    public static final String ALTER_COLUMN_CRBT_CODE = "ALTER TABLE " + TABLE
            + " ADD COLUMN " + CRBT_CODE + " TEXT;";
    public static final String ALTER_COLUMN_CRBT_PRICE = "ALTER TABLE " + TABLE
            + " ADD COLUMN " + CRBT_PRICE + " TEXT;";
}

package com.metfone.selfcare.database.datasource;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.LogConstant;
import com.metfone.selfcare.database.model.LogKQI;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/16/2017.
 */

public class LogDataSource {

    private static String TAG = LogDataSource.class.getSimpleName();
    private SQLiteDatabase databaseRead;
    private SQLiteDatabase databaseWrite;
    private static LogDataSource mInstance;
    private ApplicationController mApp;


    public static synchronized LogDataSource getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new LogDataSource(application);
        }
        return mInstance;
    }

    private LogDataSource(ApplicationController application) {
        mApp = application;
        databaseRead = application.getReengSQLiteHelper().getMyReadableDatabase();
        databaseWrite = application.getReengSQLiteHelper().getMyWritableDatabase();
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null)
            cursor.close();
    }

    public void insertLog(LogKQI model) {
        try {
            if (databaseWrite == null) {
                return;
            }
            ContentValues values = new ContentValues();
            values.put(LogConstant.CONTENT,  model.getContent());
            values.put(LogConstant.TYPE, model.getType());
            values.put(LogConstant.INFO, model.getInfo());
            values.put(LogConstant.COMMENT, model.getComment());
            databaseWrite.insert(LogConstant.TABLE, null, values);
        } catch (Exception e) {
            Log.e(TAG, "insertLog fail", e);
        }
    }

    // insert list item
    public void insertListLog(ArrayList<LogKQI> listLog) {
        if (listLog == null || listLog.isEmpty()) {
            return;
        }
        try {
            databaseWrite.beginTransaction();
            try {
                for (LogKQI model : listLog) {
                    ContentValues values = new ContentValues();
                    values.put(LogConstant.CONTENT, model.getContent());
                    values.put(LogConstant.TYPE, model.getType());
                    values.put(LogConstant.INFO, model.getInfo());
                    values.put(LogConstant.COMMENT, model.getComment());
                    databaseWrite.insert(LogConstant.TABLE, null, values);
                }
                databaseWrite.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, "Transaction", e);
            } finally {
                databaseWrite.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "insertListLog", e);
        }
    }

    public ArrayList<LogKQI> getAllLog() {
        Cursor cursor = null;
        ArrayList<LogKQI> listLog = null;
        try {
            if (databaseRead == null) {
                return null;
            }
            cursor = databaseRead.rawQuery(LogConstant.SELECT_ALL_STATEMENT, null);
            if (cursor != null && cursor.getCount() > 0) {
                listLog = new ArrayList<>();
                if (cursor.moveToFirst()) {
                    do {
                        String content = cursor.getString(1);
                        String type = cursor.getString(2);
                        String info = cursor.getString(3);
                        String comment = cursor.getString(4);

                        LogKQI logKQI = new LogKQI(type, content, info, comment);
                        listLog.add(logKQI);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllLog", e);
        } finally {
            closeCursor(cursor);
        }
        return listLog;
    }

    // del table
    public void deleteTable() {
        try {
            databaseWrite.execSQL(LogConstant.DELETE_ALL_STATEMENT);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllTable", e);
        }
    }
}

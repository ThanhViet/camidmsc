package com.metfone.selfcare.database.model.message;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant.Direction;
import com.metfone.selfcare.database.constant.ReengMessageConstant.MessageType;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Date;

/**
 * Created by toanvk2 on 20/05/2015.
 */
public class SoloShareLocationMessage extends ReengMessage {
    //      String filePath, String fileName,
    //      FileType fileType, MessageType messageType, int duration
    public SoloShareLocationMessage(ThreadMessage thread, String from, String to, String content,
                                    String latitude, String longitude) {
        super(thread.getThreadType(), MessageType.shareLocation);
        setSender(from);
        setReceiver(to);
        setThreadId(thread.getId());
        setFilePath(latitude);
        setImageUrl(longitude);
        setContent(content);
        setReadState(ReengMessageConstant.READ_STATE_READ);
        setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        setMessageType(MessageType.shareLocation);
        setDirection(Direction.send);
        Date date = new Date();
        setTime(date.getTime());
    }
}

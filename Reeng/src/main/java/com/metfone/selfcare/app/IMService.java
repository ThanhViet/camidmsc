package com.metfone.selfcare.app;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.broadcast.HeadsetPlugReceiver;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

public class IMService extends Service {
    private static final String TAG = "IMService";

    private ApplicationController mApplication;
    private static IMService mInstance;
    private LocalBinder mBinder = new LocalBinder();
    private HeadsetPlugReceiver headsetPlugReceiver;
//    private NetworkReceiver networkReceiver;

    public class LocalBinder extends Binder {
        public IMService getService() {
            return IMService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public static boolean isReady() {
        return mInstance != null;
    }

    public static IMService getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.sys(TAG, "IMService onCreate", true);
        mApplication = (ApplicationController) getApplication();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID, new NotificationCompat.Builder(this, Utilities.getChannelIdMusicPlayer(mApplication)).build());
        }
//        registerNetWorkReceiver();
        long beginTime = System.currentTimeMillis();
        mApplication.logDebugContent("IMService onCreate");
        mApplication.cancelNotification(Constants.MESSAGE.INCALL_NOTIF_ID); // in case of crash the icon is not removed

        initHeadsetPlugReceiver();
        initInstance(this);
        Log.sys(TAG, "[] create service take " + (System.currentTimeMillis() - beginTime) + " ms");
    }

    private static synchronized void initInstance(IMService im) {
        mInstance = im;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.sys(TAG, "IMService onStartCommand");
        mApplication.getPlayMusicController().actionNotification(intent);
        return super.onStartCommand(intent, flags, startId);
        //return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.sys(TAG, "IMService onDestroy", true);
        if (mApplication != null) {
            mApplication.logDebugContent("IMService onDestroy");
            mApplication.cancelNotification(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
            mApplication.cancelNotification(Constants.MESSAGE.INCALL_NOTIF_ID);
        }

        if (headsetPlugReceiver != null) {
            unregisterReceiver(headsetPlugReceiver);
        }
//        unRegisterNetWorkReceiver();
        initInstance(null);
        //sendBroadcast(new Intent("NeverKillMe"));
        super.onDestroy();
    }

    public static void stop(Context context) {
        if (context == null) return;
        context.stopService(new Intent(context, IMService.class));
    }

    @Override
    public void onLowMemory() {
        Log.sys(TAG, "IMService onLowMemory", true);
        if (mApplication != null)
            mApplication.logDebugContent("IMService onLowMemory");
        super.onLowMemory();
    }

    private void initHeadsetPlugReceiver() {
        headsetPlugReceiver = new HeadsetPlugReceiver();
        IntentFilter filter = new IntentFilter();
        for (String action : HeadsetPlugReceiver.getHeadphoneActions()) {
            filter.addAction(action);
        }
        registerReceiver(headsetPlugReceiver, filter);
    }

//    private void registerNetWorkReceiver() {
//        if (Version.hasN()) {
//            networkReceiver = new NetworkReceiver();
//            registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
//    }
//
//    private void unRegisterNetWorkReceiver() {
//        if (Version.hasN() && networkReceiver != null) {
//            unregisterReceiver(networkReceiver);
//        }
//    }

//    private static final String TAG = "IMService";
//    long[] vibrate = {300, 200, 100, 10};
//    long[] noVibraPattern = {0, 0, 0, 0};
//    //-----------------------------variables from Linphone------------------------------------------
//    private Notification mMsgNotif;
//    private NotificationManager mNotificationManager;
//    //----------------------------------------------------------------------------------------------
//    private ApplicationController mApplication;
//    private XMPPManager mXmppManager;
//    private static IMService mInstance;
//    private ReengAccountBusiness mReengAccountBusiness;
//    private static boolean isReconnectThreadRunning = false;
//    private boolean isCheckNetwork = true;
//    private Thread loadDataWaitThread;
//    //======================NOTIFY=============
//    private int smallIconID;
//    private Bitmap largeAvatarDefault;
//    //private Uri soundUri;
//    private int avatarSize;
//    private LocalBinder mBinder = new LocalBinder();
//    private int numberReconnectFail = 0;
//    private HeadsetPlugReceiver headsetPlugReceiver;
//    private NetworkReceiver networkReceiver;
//    private int currentThreadId = -1;
//
//    public class LocalBinder extends Binder {
//        public IMService getService() {
//            return IMService.this;
//        }
//    }
//
//    @Override
//    public IBinder onBind(Intent intent) {
//        return mBinder;
//    }
//
//    public Uri getCurrentSound() {
//        return SettingBusiness.getInstance(mApplication).getCurrentSoundUri();
//    }
//
//    public static boolean isReady() {
//        return mInstance != null;
//    }
//
//    public static IMService getInstance() {
//        return mInstance;
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        Log.sys(TAG, "IMService onCreate", true);
//        registerNetWorkReceiver();
//        long beginTime = System.currentTimeMillis();
//        mApplication = (ApplicationController) getApplication();
//        mApplication.logDebugContent("IMService onCreate");
//        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        cancelNotification(Constants.MESSAGE.INCALL_NOTIF_ID); // in case of crash the icon is not removed
//        // Retrieve methods to publish notification and keep Android
//        // from killing us and keep the audio quality high.
//        checkAppForegroundToConnectWhenStartService(); //
//        largeAvatarDefault = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable.ic_avatar_default);
//        smallIconID = R.drawable.ic_message_notify;
//        avatarSize = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
//        initHeadsetPlugReceiver();
//        initInstance(this);
//        Log.sys(TAG, "[] create service take " + (System.currentTimeMillis() - beginTime) + " ms");
//    }
//
//    private static synchronized void initInstance(IMService im) {
//        mInstance = im;
//    }
//
//    private void checkAppForegroundToConnectWhenStartService() {
//        if (!mApplication.getAppStateManager().isAppWentToBg() && mApplication.isDataReady()) {
//            Log.sys(TAG, "app is foreground --> call reconnect ");
//            connectByToken();
//        } else {
//            Log.sys(TAG, "app is background --> not reconnect: " + mApplication.getAppStateManager().isAppWentToBg() + " isDataReady: " + mApplication.isDataReady());
//            if (loadDataWaitThread == null) {
//                loadDataWaitThread = new LoadDataWaitThread();
//                loadDataWaitThread.setDaemon(true);
//                loadDataWaitThread.start();
//            }
//        }
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        Log.sys(TAG, "IMService onStartCommand");
//        mApplication.getPlayMusicController().actionNotification(intent);
//        return super.onStartCommand(intent, flags, startId);
//        //return START_STICKY;
//    }
//
//    @Override
//    public void onDestroy() {
//        Log.sys(TAG, "IMService onDestroy", true);
//        if (mApplication != null)
//            mApplication.logDebugContent("IMService onDestroy");
//        cancelNotification(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
//        cancelNotification(Constants.MESSAGE.INCALL_NOTIF_ID);
//        if (headsetPlugReceiver != null) {
//            unregisterReceiver(headsetPlugReceiver);
//        }
//        unRegisterNetWorkReceiver();
//        initInstance(null);
//        //sendBroadcast(new Intent("NeverKillMe"));
//        super.onDestroy();
//    }
//
//    /*@Override
//    public void onTaskRemoved(Intent rootIntent) {
//        Log.sys(TAG, "IMService onTaskRemoved", true);
//        sendBroadcast(new Intent("NeverKillMe"));
//        super.onTaskRemoved(rootIntent);
//    }*/
//
//    @Override
//    public void onLowMemory() {
//        Log.sys(TAG, "IMService onLowMemory", true);
//        if (mApplication != null)
//            mApplication.logDebugContent("IMService onLowMemory");
//        super.onLowMemory();
//    }
//
//    public void cancelNotification(int notificationId) {
//        if (mNotificationManager != null) {
//            mNotificationManager.cancel(notificationId);
//        }
//    }
//
//    public synchronized void notifyWrapper(int id, Notification notification) {
//        try {
//            if (mNotificationManager != null) {
//
////                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
////                {
////                    boolean enableSound = SettingBusiness.getInstance(mApplication).getPrefRingtone();
////                    boolean enableVibrate = SettingBusiness.getInstance(mApplication).getPrefVibrate();
////                    AudioAttributes att = new AudioAttributes.Builder()
////                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
////                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
////                            .build();
////
////                    int importance = NotificationManager.IMPORTANCE_HIGH;
////                    NotificationChannel notificationChannel = new NotificationChannel(id + "", "MOCHA", importance);
////                    notificationChannel.enableLights(true);
////                    notificationChannel.setLightColor(Color.RED);//Check set lai
////                    notificationChannel.setSound(enableSound ? SettingBusiness.getInstance(mApplication).getCurrentSoundUri() : null, att);
////                    notificationChannel.enableVibration(enableVibrate);
////                    notificationChannel.setVibrationPattern(enableVibrate ? vibrate : noVibraPattern);
////                    notificationChannel.setShowBadge(true);
//////                    notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
////                    mNotificationManager.createNotificationChannel(notificationChannel);
////                }
//
//                mNotificationManager.notify(id, notification);
//                Log.i(TAG, "notify -- im service" + id);
//            } else {
//                Log.i(TAG, "mNotificationManager not ready, discarding notification");
//            }
//        } catch (Exception e) {
//            Log.e(TAG, "Exception", e);
//        }
//    }
//
//    /**
//     * This is a wrapper around the new stopForeground method, using the older
//     * APIs if it is not available.
//     */
//    public void stopForegroundCompat(int id) {
//        cancelNotification(id);
//    }
//
//    /**
//     * goi ham nay trong cac truong hop
//     * 1. vao app ma ko co ket noi xmpp
//     * 2. auto reconnect khi reconnect fail
//     * 3. khoi tao service
//     * 4. gui message timeout
//     * 5. ConnectionClosed by server
//     * 6. ConnectionClosedOnError
//     * 7. Connectivity change
//     * 8. GCM new message
//     */
//    public synchronized void connectByToken() {
//        connectByToken(true);
//    }
//
//    public synchronized void connectByToken(boolean isCheckNetwork) {
//        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) {
//            Log.f(TAG, "connectByToken isProcessingChangeNumber");
//            return;
//        }
//        Log.sys(TAG, "connectByToken", true);
//        this.isCheckNetwork = isCheckNetwork;
//        // lan dau cai app chua dang nhap thi ko load contact, ko wait nua
//        if (mApplication.getReengAccountBusiness() != null &&
//                !mApplication.getReengAccountBusiness().isValidAccount()) {
//            Log.sys(TAG, "connectByToken  !isValidAccount", true);
//        } else if (!mApplication.isDataReady()) {//wait data load complete then login
//            Log.sys(TAG, "connectByToken  !isDataReady", true);
//            if (loadDataWaitThread == null) {
//                loadDataWaitThread = new LoadDataWaitThread();
//                loadDataWaitThread.setDaemon(true);
//                loadDataWaitThread.start();
//            }
//        } else {
//            connectByTokenWhenDataLoaded(isCheckNetwork);
//        }
//    }
//
//    private synchronized void connectByTokenWhenDataLoaded(final boolean isCheckNetwork) {
//        Log.sys(TAG, "connectByTokenWhenDataLoaded: " + NetworkHelper.isConnectInternet(mApplication), true);
//        Thread reconnectThread;
//        mXmppManager = mApplication.getXmppManager();
//        mReengAccountBusiness = mApplication.getReengAccountBusiness();
//        if (isCheckNetwork && !NetworkHelper.isConnectInternet(mApplication)) {
//            Log.sys(TAG, "network not available --> not reconnect", true);
//        } else if (mReengAccountBusiness.isValidRevision() && mXmppManager != null) {
//            if (mReengAccountBusiness.isValidAccount()) {
//                //neu dang chay hoac da xac thuc roi thi thoi
//                if (isReconnectThreadRunning || mXmppManager.isAuthenticated()) {
//                    Log.sys(TAG, "not connectByTokenWhenDataLoaded because " +
//                            " isReconnectThreadRunning: " + isReconnectThreadRunning +
//                            " isAuthenticated: " + mXmppManager.isAuthenticated(), true);
//                    return;
//                }
//                reconnectThread = new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
//                        long beginTime = System.currentTimeMillis();
//                        Log.sys(TAG, "[] Start A thread call connectByToken ");
//                        if (mXmppManager != null) {
//                            XMPPManager.notifyXMPPConnecting();
//                            mXmppManager.connectByToken(mApplication, mReengAccountBusiness.getJidNumber(),
//                                    mReengAccountBusiness.getToken(), mReengAccountBusiness.getRegionCode());
//                            long elapsedTime = (System.currentTimeMillis() - beginTime);
//                            Log.sys(TAG, "[] End the thread call connectByToken take " + elapsedTime + " ms");
//                        }
//                        isReconnectThreadRunning = false;
//                        checkConditionToAutoReconnect(isCheckNetwork);
//                    }
//                });
//                reconnectThread.setDaemon(true);
//                isReconnectThreadRunning = true;
//                reconnectThread.start();
//            } else {
//                Log.sys(TAG, "account not valid --> not reconnect", true);
//            }
//        } else {
//            Log.sys(TAG, "revision not valid --> not reconnect: " + (mXmppManager == null), true);
//        }
//    }
//
//    private void checkConditionToAutoReconnect(boolean isCheckNetwork) {
//        //neu reconnect ko thanh cong va co mang va so lan reconnect
//        // chua vuot qua nguong thi 10s sau tu dong reconnect lai
//        if (mApplication == null) return;
//        mXmppManager = mApplication.getXmppManager();
//        mReengAccountBusiness = mApplication.getReengAccountBusiness();
//        if (mXmppManager != null && mXmppManager.isAuthenticated()) {
//            numberReconnectFail = 0;//neu da xac thuc roi thi ko can nua
//        } else {
//            numberReconnectFail++;
//            if (mReengAccountBusiness.isValidRevision() && numberReconnectFail < 6) {
//                if (mXmppManager != null && !mXmppManager.isAuthenticated()) {
//                    // Wait 100 ms for processes to clean-up, then shutdown.
//                    try {
//                        Thread.sleep(100);
//                    } catch (Exception e) {
//                        Log.e(TAG, "Exception", e);
//                    }
//                    Log.sys(TAG, "AutoReconnect reconnect : " + numberReconnectFail, true);
//                    connectByToken(isCheckNetwork);
//                }
//            }
//        }
//    }
//
//    private class LoadDataWaitThread extends Thread {
//        public void run() {
//            Log.sys(TAG, "[] waiting for LoadData");
//            while (!mApplication.isDataReady()) {
//                try {
//                    sleep(150);
//                } catch (InterruptedException e) {
//                    Log.e(TAG, "Exception", e);
//                }
//            }
//            loadDataWaitThread = null;
//            connectByTokenWhenDataLoaded(isCheckNetwork);
//        }
//    }
//
//    /**
//     * set notification
//     */
//    public void setParamsNotify(final Intent onClickIntent, final String contentTitle,
//                                final String contentText, final int contentInfo,
//                                final boolean enableSound, final boolean enableVibrate,
//                                String number, int avatarType, final long notifyTime,
//                                final boolean enableTicker, String officialServerId,
//                                final boolean isMultiNotify, ThreadMessage threadMessage,
//                                final int idNotify) {
//        Log.i(TAG, "setParamsNotify: " + contentTitle + "--------" + contentText + "-------" + contentInfo);
//        currentThreadId = onClickIntent.getIntExtra(ThreadMessageConstant.THREAD_ID, -1);
//        Bitmap bitmapAvatar;
//        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
//        Bitmap avatarCache = null;
//        String avatarUrl = null;
//        onClickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        onClickIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        if (avatarType == ReengNotificationManager.AVATAR_FAKE_OFFICER) {
//            avatarUrl = null;
//            bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable.ic_thread_mocha);
//        } else if (avatarType == ReengNotificationManager.AVATAR_MULTI_THREAD) {
//            bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable.ic_avatar_default);
//        } else if (avatarType == ReengNotificationManager.AVATAR_GROUP) {//TODO change avatar default
//            if (threadMessage == null) {
//                bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(),
//                        R.drawable.ic_avatar_default);
//            } else if (TextUtils.isEmpty(threadMessage.getGroupAvatar())) {
//                String pathFile = ImageHelper.getAvatarGroupFilePath(threadMessage.getId());
//                //GroupAvatarHelper groupAvatarHelper = GroupAvatarHelper.getInstance(mApplication);
//                File fileGroupAvatar = new File(pathFile);
//                // khong check token khi ve notification, cu ve avatar cu, sau vao detail ve lai sau
//                if (fileGroupAvatar.exists()) {
//                    Log.i(TAG, "filepath: " + pathFile);
//                    try {
//                        bitmapAvatar = BitmapFactory.decodeFile(pathFile);
//                    } catch (Exception e) {
//                        Log.e(TAG, "Exception", e);
//                        bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(),
//                                R.drawable.ic_avatar_default);
//                    }
//                } else {
//                    bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(),
//                            R.drawable.ic_avatar_default);
//                }
//            } else {
//                bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(),
//                        R.drawable.ic_avatar_default);
//                avatarUrl = avatarBusiness.getGroupAvatarUrl(threadMessage, threadMessage.getGroupAvatar());
//            }
//        } else if (avatarType == ReengNotificationManager.AVATAR_SOLO) {
//            bitmapAvatar = largeAvatarDefault;
//            if (!TextUtils.isEmpty(number)) {
//                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(number);
//                if (phoneNumber != null && phoneNumber.getId() != null) {
//                    avatarUrl = avatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), number, avatarSize);
//                } else {
//                    StrangerPhoneNumber strangerPhoneNumber = mApplication.
//                            getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(number);
//                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(number);
//                    // check so non contact truoc
//                    if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
//                        avatarUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), number, avatarSize);
//                    } else if (strangerPhoneNumber != null) {
//                        if (strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType
//                                .other_app_stranger) {
//                            avatarUrl = strangerPhoneNumber.getFriendAvatarUrl();
//                        } else {
//                            avatarUrl = avatarBusiness.getAvatarUrl(strangerPhoneNumber.getFriendAvatarUrl(),
//                                    number, avatarSize);
//                        }
//                    } else {
//                        avatarUrl = null;
//                        if (onClickIntent.getIntExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, 0)
//                                == Constants.ONMEDIA.COMMENT) {
//                            bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable
//                                    .ic_thread_mocha);
//                        }
//                    }
//                }
//            } else {
//                avatarUrl = null;
//                bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable.ic_thread_mocha);
//            }
//        } else if (avatarType == ReengNotificationManager.AVATAR_OFFICER) {
//            bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable.ic_thread_mocha);
//            avatarUrl = mApplication.getOfficerBusiness().getOfficerAvatarByServerId(officialServerId);
//        } else {
//            bitmapAvatar = largeAvatarDefault;
//        }
//        // avatar by url
//        if (!TextUtils.isEmpty(avatarUrl)) {
//            avatarCache = avatarBusiness.getBitmapFromCache(avatarUrl);
//        }
//        if (avatarCache != null) {// co cache
//            bitmapAvatar = avatarCache;
//        } else if (!TextUtils.isEmpty(avatarUrl)) {// ko co cache va co duong dan
//            SimpleImageLoadingListener loadingImageListener = new SimpleImageLoadingListener() {
//
//                @Override
//                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                    super.onLoadingComplete(imageUri, view, loadedImage);
//                    Log.d(TAG, "draw notification load image ---> onLoadingComplete");
//
//                    if (loadedImage != null) {
//                        // load dduowcj avatar thi ve lai notify o che do silent
//                        int threadID = onClickIntent.getIntExtra(ThreadMessageConstant.THREAD_ID, -1);
//                        if (currentThreadId == threadID) {
//                            displayNotification(idNotify, onClickIntent, contentTitle, contentText,
//                                    contentInfo, false, false, loadedImage, notifyTime, enableTicker, isMultiNotify);
//                        }
//                    }
//                }
//
//                @Override
//                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    Log.d(TAG, "draw notification load image ---> onLoadingFailed");
//                    super.onLoadingFailed(imageUri, view, failReason);
//                    retryLoadAvatarAndDrawNotification(imageUri, onClickIntent, contentTitle,
//                            contentText, contentInfo, notifyTime, enableTicker, isMultiNotify, idNotify);
//                }
//
//                @Override
//                public void onLoadingCancelled(String imageUri, View view) {
//                    Log.d(TAG, "draw notification load image ---> onLoadingCancelled");
//                    super.onLoadingCancelled(imageUri, view);
//                    retryLoadAvatarAndDrawNotification(imageUri, onClickIntent, contentTitle,
//                            contentText, contentInfo, notifyTime, enableTicker, isMultiNotify, idNotify);
//                }
//            };
//            Log.d(TAG, "draw notification load image ---> loadAvatar: " + avatarUrl);
//            avatarBusiness.loadAvatar(avatarUrl, loadingImageListener, avatarSize);
//        }
//        // display
//        displayNotification(idNotify, onClickIntent, contentTitle, contentText, contentInfo,
//                enableSound, enableVibrate, bitmapAvatar, notifyTime, enableTicker, isMultiNotify);
//    }
//
//
//    public void drawNotifyWithAvatar(final Intent onClickIntent, final String contentTitle,
//                                     final String contentText,
//                                     final String avatar,
//                                     final int contentInfo,
//                                     final boolean enableSound, final boolean enableVibrate,
//                                     final long notifyTime,
//                                     final boolean enableTicker,
//                                     final int idNotify) {
//
//        Bitmap avatarCache = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable.ic_thread_mocha);
//        displayNotification(idNotify, onClickIntent, contentTitle, contentText, contentInfo,
//                enableSound, enableVibrate, avatarCache, notifyTime, enableTicker, false);
//
//
//        SimpleImageLoadingListener loadingImageListener = new SimpleImageLoadingListener() {
//
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                super.onLoadingComplete(imageUri, view, loadedImage);
//                Log.sys(TAG, "draw notification load image ---> onLoadingComplete");
//
//                if (loadedImage != null) {
//                    // load dduowcj avatar thi ve lai notify o che do silent
//                    int threadID = onClickIntent.getIntExtra(ThreadMessageConstant.THREAD_ID, -1);
//                    if (currentThreadId == threadID) {
//                        displayNotification(idNotify, onClickIntent, contentTitle, contentText,
//                                contentInfo, false, false, loadedImage, notifyTime, enableTicker, false);
//                    }
//                }
//            }
//
//            @Override
//            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                Log.sys(TAG, "draw notification load image ---> onLoadingFailed");
//                super.onLoadingFailed(imageUri, view, failReason);
//                retryLoadAvatarAndDrawNotification(imageUri, onClickIntent, contentTitle,
//                        contentText, contentInfo, notifyTime, enableTicker, false, idNotify);
//            }
//
//            @Override
//            public void onLoadingCancelled(String imageUri, View view) {
//                Log.sys(TAG, "draw notification load image ---> onLoadingCancelled");
//                super.onLoadingCancelled(imageUri, view);
//                retryLoadAvatarAndDrawNotification(imageUri, onClickIntent, contentTitle,
//                        contentText, contentInfo, notifyTime, enableTicker, false, idNotify);
//            }
//        };
//        Log.sys(TAG, "draw notification load image ---> loadAvatar: " + avatar);
//        mApplication.getAvatarBusiness().loadAvatar(avatar, loadingImageListener, avatarSize);
//
//
//
//    }
//
//    public void setParamsNotifyCall(final Intent onClickIntent, final String friendNumber, final int notifyId, int callType) {
//        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
//        String name;
//        final String msg = mApplication.getResources().getString(R.string.call_state_calling) + "...";
//        String avatarUrl;
//        Bitmap bitmapAvatar = null;
//        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendNumber);
//        if (phoneNumber != null && phoneNumber.getId() != null) {
//            name = phoneNumber.getName();
//            avatarUrl = avatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), friendNumber, avatarSize);
//        } else {
//            StrangerPhoneNumber strangerPhoneNumber = mApplication.
//                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(friendNumber);
//            NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(friendNumber);
//            // check so non contact truoc
//            if (strangerPhoneNumber != null) {
//                name = strangerPhoneNumber.getFriendName();
//                if (strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
//                    avatarUrl = strangerPhoneNumber.getFriendAvatarUrl();
//                } else {
//                    avatarUrl = avatarBusiness.getAvatarUrl(strangerPhoneNumber.getFriendAvatarUrl(), friendNumber,
//                            avatarSize);
//                }
//            } else {
//                name = friendNumber;
//                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
//                    avatarUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), friendNumber, avatarSize);
//                } else {
//                    avatarUrl = null;
//                }
//            }
//        }
//        if (!TextUtils.isEmpty(avatarUrl)) {
//            bitmapAvatar = avatarBusiness.getBitmapFromCache(avatarUrl);
//        }
//        setNotifyCall(onClickIntent, bitmapAvatar, name, msg, notifyId, callType);
//    }
//
//    private void setNotifyCall(Intent onClickIntent, Bitmap bitmapAvatar, String title, String content, int notifyId, int callType) {
//        PendingIntent pendingIntent;
//        pendingIntent = PendingIntent.getActivity(this,
//                1000, onClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        long notifyTime = System.currentTimeMillis();
//        int smallIcon;
//        if (callType == CallConstant.TYPE_CALLEE) {
//            smallIcon = R.drawable.ic_incall_notify;
//        } else {
//            smallIcon = R.drawable.ic_outcall_notify;
//        }
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(smallIcon)
//                .setAutoCancel(true)
//                .setWhen(notifyTime)
//                //.setLargeIcon(bitmapAvatar)
//                .setContentIntent(pendingIntent)
//                .setContentTitle(title)
//                .setContentText(content);
//        notificationBuilder.setSound(null);
//        notificationBuilder.setLights(Color.WHITE, 300, 1000);
//        //
//        mMsgNotif = notificationBuilder.build();
//        mMsgNotif.flags = Notification.FLAG_ONGOING_EVENT;
//        notifyWrapper(notifyId, mMsgNotif);
//    }
//
//    private void retryLoadAvatarAndDrawNotification(String avatarUrl, final Intent onClickIntent,
//                                                    final String contentTitle, final String contentText,
//                                                    final int contentInfo, final long notifyTime,
//                                                    final boolean enableTicker, final boolean isMultiNotify,
//                                                    final int idNotify) {
//        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
//        SimpleImageLoadingListener loadingImageListener = new SimpleImageLoadingListener() {
//
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                super.onLoadingComplete(imageUri, view, loadedImage);
//                Log.d(TAG, "draw notification load image ---> onLoadingComplete");
//                if (loadedImage != null) {
//                    // load dduowcj avatar thi ve lai notify o che do silent
//                    displayNotification(idNotify, onClickIntent, contentTitle, contentText,
//                            contentInfo, false, false, loadedImage, notifyTime, enableTicker, isMultiNotify);
//                }
//            }
//        };
//        Log.d(TAG, "draw notification load image ---> loadAvatar: " + avatarUrl);
//        avatarBusiness.loadAvatar(avatarUrl, loadingImageListener, avatarSize);
//    }
//
//    public void notifyMessageSendError(int threadId, final Intent onClickIntent, final String contentTitle,
//                                       final String contentText) {
//        Bitmap bitmapAvatar;
//        bitmapAvatar = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable.ic_thread_mocha);
//        displayNotification(threadId, onClickIntent, contentTitle, contentText, ReengNotificationManager
//                        .NOTIFY_NO_NUMBER,
//                false, false, bitmapAvatar, System.currentTimeMillis(), true, true);
//    }
//
//    /**
//     * draw notification
//     */
//    private void displayNotification(int notificationId, Intent onClickIntent, String contentTitle,
//                                     String contentText, int contentInfo, boolean enableSound,
//                                     boolean enableVibrate, Bitmap bitmapAvatar, long notifyTime,
//                                     boolean enableTicker, boolean isMultiNotify) {
//        displayNotificationNormal(notificationId, onClickIntent, contentTitle, contentText,
//                contentInfo, enableSound, enableVibrate, bitmapAvatar, notifyTime, enableTicker, isMultiNotify);
//    }
//
//    private void displayNotificationNormal(int notificationId, Intent onClickIntent, String contentTitle,
//                                           String contentText, int contentInfo, boolean enableSound,
//                                           boolean enableVibrate, Bitmap bitmapAvatar, long notifyTime,
//                                           boolean enableTicker, boolean isMultiNotify) {
//        PendingIntent pendingIntent;
//        if (isMultiNotify) {
//            Random r = new Random();
//            int requestCode = r.nextInt(100) + 1000;
//            pendingIntent = PendingIntent.getActivity(this,
//                    requestCode, onClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        } else {
//            pendingIntent = PendingIntent.getActivity(this,
//                    1000, onClickIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//        }
//        if (notifyTime == -1) {
//            Calendar currentCal = Calendar.getInstance();
//            notifyTime = currentCal.getTimeInMillis();
//        }
//        //TODO bigTextStyle
//        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//        bigTextStyle.setBigContentTitle(contentTitle);
//        bigTextStyle.bigText(contentText);
//        //
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(smallIconID)
//                .setAutoCancel(true)
//                .setWhen(notifyTime)
//                .setLargeIcon(bitmapAvatar)
//                .setContentIntent(pendingIntent)
//                .setContentTitle(contentTitle)
//                .setContentText(contentText)
//                .setStyle(bigTextStyle);
//        if (contentInfo != ReengNotificationManager.NOTIFY_NO_NUMBER) {
//            notificationBuilder.setNumber(contentInfo);
//        }
//        if (enableTicker) {
//            notificationBuilder.setTicker(contentTitle + ": " + contentText);
//        }
//        //
//        if (!enableSound) {
//            notificationBuilder.setSound(null);
//        } else {
//            notificationBuilder.setSound(getCurrentSound());
//        }
//        if (!enableVibrate) {
//            notificationBuilder.setVibrate(noVibraPattern);
//        } else {
//            notificationBuilder.setVibrate(vibrate);
//        }
//        if (enableSound && enableVibrate) {
//            notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
//        } else {
//            notificationBuilder.setPriority(NotificationCompat.PRIORITY_LOW);
//        }
//        notificationBuilder.setLights(Color.WHITE, 300, 1000);
//        //
//        mMsgNotif = notificationBuilder.build();
//        mMsgNotif.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
//        notifyWrapper(notificationId, mMsgNotif);
//    }
//
//    /**
//     * display notification image full
//     *
//     * @param bitmap
//     */
//    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//    public void displayNotifyPreviewImage(Intent clickIntent, Bitmap bitmap, String contentTitle,
//                                          String contentText, String bigContentTitle, String bigContentText) {
//        Bitmap bitmapLarger = BitmapFactory.decodeResource(mApplication.getResources(), R.drawable.ic_thread_mocha);
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
//                .setAutoCancel(true)
//                .setContentTitle(contentTitle)
//                .setSmallIcon(R.drawable.ic_message_notify)
//                .setLargeIcon(bitmapLarger)
//                .setContentText(contentText);
//        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
//        bigPicStyle.bigPicture(bitmap);
//        bigPicStyle.setBigContentTitle(bigContentTitle);
//        bigPicStyle.setSummaryText(bigContentText);
//        mBuilder.setStyle(bigPicStyle);
//        //stackBuilder
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addParentStack(HomeActivity.class);
//        stackBuilder.addNextIntent(clickIntent);
//        //pending intent
//        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//        mBuilder.setContentIntent(pendingIntent);
//        // mId allows you to update the notification later on.
//        Notification mNotification = mBuilder.build();
//        boolean enableSound = SettingBusiness.getInstance(mApplication).getPrefRingtone();
//        boolean enableVibrate = SettingBusiness.getInstance(mApplication).getPrefVibrate();
//        Log.d(TAG, "displayNotifyPreviewImage - enableSound: " + enableSound + " enableVibrate: " + enableVibrate);
//        if (!enableSound) {
//            mNotification.sound = null;
//        } else {
//            mNotification.sound = getCurrentSound();
//        }
//        if (!enableVibrate) {
//            mNotification.vibrate = noVibraPattern;
//        } else {
//            mNotification.vibrate = vibrate;
//        }
//        mNotification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
//        mNotification.ledARGB = Color.GREEN;
//        mNotification.ledOnMS = 300;
//        mNotification.ledOffMS = 1000;
//        notifyWrapper(Constants.NOTIFICATION.NOTIFY_OTHER, mNotification);
//        Log.i(TAG, "notify -- preview image");
//    }
//
//    private void initHeadsetPlugReceiver() {
//        headsetPlugReceiver = new HeadsetPlugReceiver();
//        IntentFilter filter = new IntentFilter();
//        for (String action : HeadsetPlugReceiver.getHeadphoneActions()) {
//            filter.addAction(action);
//        }
//        registerReceiver(headsetPlugReceiver, filter);
//    }
//
//    private void registerNetWorkReceiver() {
//        if (Version.hasN()) {
//            networkReceiver = new NetworkReceiver();
//            registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
//    }
//
//    private void unRegisterNetWorkReceiver() {
//        if (Version.hasN() && networkReceiver != null) {
//            unregisterReceiver(networkReceiver);
//        }
//    }
}
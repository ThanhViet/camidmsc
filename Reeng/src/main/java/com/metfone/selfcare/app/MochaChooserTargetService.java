package com.metfone.selfcare.app;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.service.chooser.ChooserTarget;
import android.service.chooser.ChooserTargetService;
import android.text.TextUtils;

import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 2/22/2017.
 */
@TargetApi(Build.VERSION_CODES.M)
public class MochaChooserTargetService extends ChooserTargetService {
    private static final String TAG = MochaChooserTargetService.class.getSimpleName();
    private int avatarSize;
    private Bitmap largeAvatarDefault, largeAvatarMocha;
    private ArrayList<ThreadMessage> listSupport = new ArrayList<>();
    private long t;


    @Override
    public List<ChooserTarget> onGetChooserTargets(ComponentName targetActivityName, IntentFilter matchedFilter) {
        if (!Version.hasM()) return null;
        t = System.currentTimeMillis();
        ApplicationController application = (ApplicationController) getApplicationContext();
        if (!application.isDataReady()) return null;
        CopyOnWriteArrayList<ThreadMessage> threadMessages = application.getMessageBusiness().getThreadMessageArrayList();
        if (threadMessages == null || threadMessages.isEmpty()) {
            return null;
        }
        listSupport = new ArrayList<>();
        for (ThreadMessage thread : threadMessages) {
            if (thread.getAllMessages() != null && !thread.getAllMessages().isEmpty()) {
                listSupport.add(thread);
            }
        }
        Collections.sort(listSupport, ComparatorHelper.getComparatorThreadMessageByLastTime());
        // return targets;
        return processTargets(application, listSupport);
    }

    private List<ChooserTarget> processTargets(ApplicationController application, ArrayList<ThreadMessage> threadMessages) {
        this.avatarSize = (int) application.getResources().getDimension(R.dimen.avatar_small_size);
        this.largeAvatarDefault = BitmapFactory.decodeResource(application.getResources(), R.drawable.ic_avatar_default);
        this.largeAvatarMocha = BitmapFactory.decodeResource(application.getResources(), R.drawable.ic_thread_mocha);
        List<ChooserTarget> targets = new ArrayList<>();
        for (ThreadMessage thread : threadMessages) {
            if (thread.isReadyShow(application.getMessageBusiness())) {
                targets.add(getTargetFromThread(application, thread));
            }
            if (targets.size() >= 4) break;
        }
        Log.d(TAG, "processTargets take: " + (System.currentTimeMillis() - t));
        return targets;
    }

    private ChooserTarget getTargetFromThread(ApplicationController application, ThreadMessage thread) {
        // Ranking score for this target between 0.0f and 1.0f
        ComponentName componentName = new ComponentName(getPackageName(), HomeActivity.class.getCanonicalName());
        Bundle extras = new Bundle();
        extras.putBoolean("from_chooser_target", true);
        extras.putInt("chooser_target_thread_id", thread.getId());
        ChooserTarget target = new ChooserTarget(thread.getThreadName(),
                Icon.createWithBitmap(getAvatarBitmap(application, thread)),
                new Random().nextFloat(),
                componentName,
                extras);
        return target;
    }

    private Bitmap getAvatarBitmap(ApplicationController application, final ThreadMessage threadMessage) {
        String avatarUrl = null;
        String groupAvatarFile = null;
        int threadType = threadMessage.getThreadType();
        AvatarBusiness avatarBusiness = application.getAvatarBusiness();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String friendNumber = threadMessage.getSoloNumber();
            avatarUrl = getFriendAvatarUrl(application, avatarBusiness, friendNumber);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            if (TextUtils.isEmpty(threadMessage.getGroupAvatar())) {
                groupAvatarFile = ImageHelper.getAvatarGroupFilePath(threadMessage.getId());
            } else {
                avatarUrl = avatarBusiness.getGroupAvatarUrl(threadMessage, threadMessage.getGroupAvatar());
            }
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            avatarUrl = application.getOfficerBusiness().getOfficerAvatarByServerId(threadMessage.getServerId());
        }
        // get bitmap
        if (TextUtils.isEmpty(avatarUrl)) {
            if (TextUtils.isEmpty(groupAvatarFile)) {
                if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    return largeAvatarDefault;
                }
            } else {
                try {
                    return BitmapFactory.decodeFile(groupAvatarFile);
                } catch (Exception e) {
                    Log.e(TAG,"Exception",e);
                }
            }
        } else {
            Bitmap avatarCache = avatarBusiness.getBitmapFromCache(avatarUrl, threadMessage.getId());
            if (avatarCache != null) {
                return avatarCache;
            }
        }
        return largeAvatarMocha;
    }

    private String getFriendAvatarUrl(ApplicationController application, AvatarBusiness avatarBusiness, String friendNumber) {
        String avatarUrl = null;
        PhoneNumber phoneNumber = application.getContactBusiness().getPhoneNumberFromNumber(friendNumber);
        if (phoneNumber != null && phoneNumber.getId() != null) {
            avatarUrl = avatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), friendNumber, avatarSize);
        } else {
            NonContact nonContact = application.getContactBusiness().getExistNonContact(friendNumber);
            if (nonContact != null) {
                avatarUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), friendNumber, avatarSize);
            } else {
                StrangerPhoneNumber strangerPhoneNumber = application.
                        getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(friendNumber);
                if (strangerPhoneNumber != null && strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                    avatarUrl = strangerPhoneNumber.getFriendAvatarUrl();
                } else if (strangerPhoneNumber != null) {
                    avatarUrl = avatarBusiness.getAvatarUrl(strangerPhoneNumber.getFriendAvatarUrl(), friendNumber, avatarSize);
                }
            }
        }
        return avatarUrl;
    }

    private boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }

    private Bitmap circleBitmap(Bitmap input) {
        Bitmap output = Bitmap.createBitmap(input.getWidth(), input.getHeight(), Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, input.getWidth(), input.getHeight());
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(0xff424242);
        canvas.drawCircle(input.getWidth() / 2, input.getHeight() / 2, input.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(input, rect, rect, paint);
        return output;
    }
}
package com.metfone.selfcare.app.dev;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDex;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.CrashUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.Utils;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
//import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
//import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler;
//import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.perf.FirebasePerformance;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.IMService;
import com.metfone.selfcare.broadcast.AlarmReceiver;
import com.metfone.selfcare.broadcast.HeadsetPlugReceiver;
import com.metfone.selfcare.broadcast.MiniReceiver;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.broadcast.NetworkReceiver;
import com.metfone.selfcare.broadcast.SmsObserver;
import com.metfone.selfcare.business.ApplicationLockManager;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.BlockContactBusiness;
import com.metfone.selfcare.business.CallBusiness;
import com.metfone.selfcare.business.CallHistoryHelper;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.business.KeengProfileBusiness;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.business.PubSubManager;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.business.TransferFileBusiness;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.datasource.ReengSQLiteHelper;
import com.metfone.selfcare.di.ApplicationComponent;
import com.metfone.selfcare.di.ApplicationModule;
import com.metfone.selfcare.di.DaggerApplicationComponent;
import com.metfone.selfcare.fragment.contact.HomeContactsFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.ConfigProperties;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.SignalStrengthHelper;
import com.metfone.selfcare.helper.TopExceptionHandler;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.helper.emoticon.EmoticonDecodeQueue;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.helper.message.ProcessSmsReceivedQueue;
import com.metfone.selfcare.listeners.ChangeUnreadMessageListener;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.KHAccountPoint;
import com.metfone.selfcare.module.home_kh.model.RankDefine;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.ui.recyclerview.PauseOnScrollListener;
import com.metfone.selfcare.ui.recyclerview.PauseOnScrollRecyclerViewListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.util.contactintergation.DeviceAccountManager;
import com.viettel.util.LogDebugHelper;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.Connection;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import iknow.android.utils.BaseUtils;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.metfone.selfcare.firebase.FireBaseHelper.FIREBASE_PROPERTY_SEND_SERVER;

//import com.squareup.leakcanary.LeakCanary;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by thaodv on 6/16/2014.
 */
public class ApplicationController extends Application {
    private static ApplicationController instance;

    public static ApplicationController getApp() {
        return instance;
    }
    private static final String TAG = ApplicationController.class.getSimpleName();
    private LoginBusiness loginBusiness;
    private MessageBusiness mMessageBusiness;
    private ReengAccountBusiness mAccountBusiness;
    private ContactBusiness mContactBusiness;
    private BlockContactBusiness mBlockContactBusiness;
    private AvatarBusiness mAvatarBusiness;
    private OfficerBusiness mOfficerBusiness;
    private ApplicationStateManager mAppStateManager;
    private ApplicationLockManager mAppLockManager;
    private ReloadDataThread mReloadDataThread;
    private LoadDataAfterLoginThread mLoadDataAfterLoginThread;
    private ReLoadContactAfterPermissionsResult mReLoadContactAfterPermissionsResult;
    private MusicBusiness mMusicBusiness;
    private ContentConfigBusiness mConfigBusiness;
    private StrangerBusiness mStrangerBusiness;
    private KeengProfileBusiness mKeengProfileBusiness;
    private FeedBusiness mFeedBusiness;
    private CallBusiness mCallBusiness;
    private ImageProfileBusiness mImageProfileBusiness;
    private TransferFileBusiness mTransferFileBusiness;
    private Resources mRes;
    private SmsObserver mSmsObserver;
    private boolean isFirstRequestPermission = false;
    private ProcessSmsReceivedQueue mProcessSmsReceivedQueue;
    private EmoticonDecodeQueue mQueueDecodeEmo;
    private static final String PROPERTY_ID = "UA-54264848-2";
    private double mDensity = 1;
    private int widthPixels;
    private int heightPixels;
    private SharedPreferences mPref;
    private AlarmReceiver mAlarmReceiver;
    private PowerManager.WakeLock wakeLock = null;
    private StickerBusiness stickerBusiness;
    private PhoneNumberUtil phoneUtil;
    private ReengSQLiteHelper mReengSQLiteHelper;
    private ConfigProperties distributionChannelProperties;
    private PlayMusicController mPlayMusicController;
    private FirebaseEventBusiness firebaseEventBusiness;
    // xmpp manager
    private XMPPManager mXmppManager;
    private boolean isSettingLanguage = false;
    protected String userAgent;
    private String configResolutionVideo = "auto"; //chat luong video

    private ChangeUnreadMessageListener changeUnreadMessageListener;

    private ApplicationComponent applicationComponent;

    private CamIdUserBusiness mCamIdUserBusiness;

    private boolean isParseNewMessage = false;
    private boolean isResize219 = false;
    private boolean loadedSubtabHomeMovie = false;
    private boolean loadedAdsMainTab = false;
    private boolean showedAdsMainTab = false;
    private BaseSlidingFragmentActivity mCurrentActivity = null;
    private RoundedCornersTransformation roundedCornersTransformation;
    private int gameLiveNumberAnswerCorrect = 0;
    AppEventsLogger logger;
    public boolean isPlayingMini = false;
    public boolean isEnableVolumeAutoPlay = false;
    public BaseSlidingFragmentActivity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(BaseSlidingFragmentActivity mCurrentActivity) {
        this.mCurrentActivity = mCurrentActivity;
    }

    public boolean isFirstRequestPermission() {
        return isFirstRequestPermission;
    }

    public void setFirstRequestPermission(boolean first) {
        isFirstRequestPermission = first;
    }

    private static ApplicationController mSelf;

    public static ApplicationController self() {
        return mSelf;
    }

    @Inject
    ListenerUtils listenerUtils;

    @Inject
    LogApi logApi;

    private Gson gson;
    private int padding;
    private int margin;
    private int round;
    private int totalVideosViewed = 0;

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate() {
        mSelf = this;
//        LogDebugHelper.getInstance(this);
        PopupHelper.getInstance().setContext(this);
        Log.setEnableLog(BuildConfig.DEBUG);

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);

        roundedCornersTransformation = new RoundedCornersTransformation(getRound(), 0);
        FirebaseApp.initializeApp(this);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver broadcastReceiver = new MiniReceiver();
        registerReceiver(broadcastReceiver, screenStateFilter);

        Log.d(TAG, "[perform] onCreate");
        long t = System.currentTimeMillis();
        //Version.enableStrictMode();
        mRes = getResources();
        // init database
//        universalImageLoader = ImageLoader.getInstance();
//        initUniversalLoader();
        Log.d(TAG, "[perform] init universal take: " + (System.currentTimeMillis() - t));
        t = System.currentTimeMillis();
        mReengSQLiteHelper = ReengSQLiteHelper.getInstance(this);
        Log.d(TAG, "[perform] init database take: " + (System.currentTimeMillis() - t));
        t = System.currentTimeMillis();
        //        initFont();
        super.onCreate();
        instance = this;
        registerNetWorkReceiver();
        initHeadsetPlugReceiver();
        mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        // TODO: [START] Cambodia version
        if (mPref.getInt(Constants.PREFERENCE.PREF_CONFIG_TAB_DEFAULT, -1) == -1) {
            mPref.edit().putInt(Constants.PREFERENCE.PREF_CONFIG_TAB_DEFAULT, TabHomeHelper.TAB_DEFAULT).apply();
        }
        // TODO: [END] Cambodia version
        isFirstRequestPermission = mPref.getInt(Constants.PREFERENCE.PREF_APP_VERSION, -1) == -1;
        if (!Config.Server.FREE_15_DAYS) {
            initGATracker();
            //enable when release
//            CrashlyticsCore core = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
//            Fabric.with(this, new Crashlytics.Builder().core(core).build());
            // FacebookSdk.sdkInitialize(ApplicationController.this);//initialize SDK before use
        }

        Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
        Log.d(TAG, "[perform] init GA and Facebook SDK take: " + (System.currentTimeMillis() - t));
        t = System.currentTimeMillis();
        createBusiness();
        Log.d(TAG, "[perform] create business  take: " + (System.currentTimeMillis() - t));
        t = System.currentTimeMillis();
        if (BuildConfig.IS_TIMOR) {
            settingLocal(mAccountBusiness.getSettingLanguage());
        }
//        UrlConfigHelper.getInstance(this);// init url config
        Log.d(TAG, "[perform] init url config take: " + (System.currentTimeMillis() - t));
        initData();
        mAlarmReceiver = new AlarmReceiver();
        stopAlarmPingPongWhenCreate();
        mDensity = mRes.getDisplayMetrics().density;
        getSizeScreen(this);
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "screen");
        wakeLock.setReferenceCounted(false);
        // register contact provider
        ContentObserverBusiness.getInstance(getApplicationContext()).registerContactObserver();
//        if (PermissionHelper.allowedPermission(this, Manifest.permission.READ_SMS))
//            registerSmsObserver();
        registerSmsOTPObserver();

        userAgent = Util.getUserAgent(this, "ExoPlayerDemo");
        if (mAccountBusiness.isEnableUploadLog()) {
            SignalStrengthHelper.getInstant(ApplicationController.this).startListener();
        }

//        LocaleManager.setLocale(this);

        /*if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);*/

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Utilities.getAppSignatures(this);
//        }
//        MochaShortcutManager.enableAllDynamicShortcut(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Log.d(TAG, "[perform] finish onCreate" + (System.currentTimeMillis() - t));

        if (FacebookSdk.isInitialized()) {
            logger = AppEventsLogger.newLogger(this);
            logFirstOpenCam();
        } else {
            (new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (FacebookSdk.isInitialized()) {
                        logger = AppEventsLogger.newLogger(ApplicationController.this);
                        logFirstOpenCam();
                    }
                }
            }, 800);
        }

        initEsport();
    }

    private void logFirstOpenCam() {
        boolean firstOpenCam = mPref.getBoolean(Constants.PREFERENCE.PREF_FIRST_OPEN_CAM, false);
        if (!firstOpenCam) {
            String rc = PhoneNumberHelper.getInstant().detectRegionCodeFromDevice(this);
            if ("KH".equalsIgnoreCase(rc)) {
                logEventCamFacebookSDKAndFirebase(
                        getResources().getString(R.string.c_first_open));
                mPref.edit().putBoolean(Constants.PREFERENCE.PREF_FIRST_OPEN_CAM, true).apply();
            }
        }
    }

    public int getPadding() {
        if (padding == 0) padding = Utilities.dpToPx(16);
        return padding;
    }

    public int getMargin() {
        if (margin == 0) margin = Utilities.dpToPx(8);
        return margin;
    }

    public int getRound() {
        if (round == 0) round = Utilities.dpToPx(10);
        return round;
    }

    public Gson getGson() {
        if (gson == null) gson = new GsonBuilder()
                .registerTypeAdapter(int.class, new EmptyStringToNumberTypeAdapter())
                .registerTypeAdapter(Integer.class, new EmptyStringToNumberTypeAdapter())
                .registerTypeAdapter(double.class, new EmptyStringToNumberTypeAdapter())
                .registerTypeAdapter(Double.class, new EmptyStringToNumberTypeAdapter())
                .create();
        return gson;
    }

    public LogApi getLogApi() {
        return logApi;
    }

    public ListenerUtils getListenerUtils() {
        return listenerUtils;
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (BuildConfig.IS_TIMOR) {
            String settingLang = mAccountBusiness.getSettingLanguageFromPref();
            String deviceLang = newConfig.locale.getLanguage();
            if (!TextUtils.isEmpty(settingLang) && !settingLang.equals(deviceLang)) {
                settingLocal(settingLang);
            }
        } else {
            String oldLanguage = mAccountBusiness.getCurrentLanguage();
            String currentLanguage = mAccountBusiness.getDeviceLanguage();
            XMPPManager.IqInfoFirstTime iqInfoFirstTime = XMPPManager.IqInfoFirstTime.NORMAL;
            if (!TextUtils.isEmpty(currentLanguage) && !currentLanguage.equals(oldLanguage)) {
                mAccountBusiness.setCurrentLanguage();
                mContactBusiness.initArrayListPhoneNumber();
            }
            if (mXmppManager != null && mXmppManager.isAuthenticated()) {
                mXmppManager.sendIQClientInfo(Connection.TOKEN_AUTH_NON_SASL, false, iqInfoFirstTime.VALUE);
            }
        }
        LocaleManager.setLocale(this);
    }

    public void startService() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(this, new Intent(this, IMService.class));
            } else {
                Intent intent = new Intent(this, IMService.class);
                startService(intent);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public void startIMService() {
//        Intent intent = new Intent(this, IMService.class);
//        startService(intent);
        checkAppForegroundToConnectWhenStartService();
    }

    /*public void initService(final IMConnection listener) {
        Intent intent = new Intent(this, IMService.class);
        bindService(intent, new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
                Log.d(TAG, "onServiceConnected : ");
                IMService.LocalBinder binder = (IMService.LocalBinder) service;
                ApplicationController.this.mIMService = (IMService) binder.getService();
                if (listener != null) listener.onConnected();
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
            }
        }, Context.BIND_AUTO_CREATE);
        // startService(new Intent(this, IMService.class));
    }*/

    protected void createBusiness() {
        // init app manager
        if (mXmppManager == null) {
            //mXmppManager=XMPPManager.getInstance(this);
            mXmppManager = new XMPPManager(this);
        }
        if (phoneUtil == null) {
            phoneUtil = PhoneNumberUtil.getInstance();
        }
        if (mAppStateManager == null) {
            mAppStateManager = new ApplicationStateManager(this);
        }
        if (mAppLockManager == null) {
            mAppLockManager = new ApplicationLockManager(this);
        }
        if (mAccountBusiness == null) {
            mAccountBusiness = new ReengAccountBusiness(this);
            mAccountBusiness.init(); //comment to avoid strict mode
        }
        if (mPlayMusicController == null) {
            mPlayMusicController = new PlayMusicController(this);
        }
        if (loginBusiness == null) {
            loginBusiness = new LoginBusiness(this);
        }
        if (mBlockContactBusiness == null) {
            mBlockContactBusiness = new BlockContactBusiness(this);
        }
        if (mConfigBusiness == null) {
            mConfigBusiness = new ContentConfigBusiness(this);
        }
        if (mOfficerBusiness == null) {
            mOfficerBusiness = new OfficerBusiness(this);
        }
        if (mMessageBusiness == null) {
            mMessageBusiness = new MessageBusiness(this);
        }
        if (stickerBusiness == null) {
            stickerBusiness = new StickerBusiness(this);
        }
        if (mContactBusiness == null) {
            mContactBusiness = new ContactBusiness(this);
        }
        if (mAvatarBusiness == null) {
            mAvatarBusiness = new AvatarBusiness(this);
        }
        if (mMusicBusiness == null) {
            mMusicBusiness = new MusicBusiness(this);
        }
        if (mStrangerBusiness == null) {
            mStrangerBusiness = new StrangerBusiness(this);
        }
        //init Business 25 ms
        if (mKeengProfileBusiness == null) {
            mKeengProfileBusiness = new KeengProfileBusiness(this);
        }
        if (mFeedBusiness == null) {
            mFeedBusiness = new FeedBusiness(this);
        }
        if (mImageProfileBusiness == null) {
            mImageProfileBusiness = new ImageProfileBusiness(this);
        }
        if (mCallBusiness == null) {
            mCallBusiness = new CallBusiness(this);
        }
        if (mTransferFileBusiness == null) {
            mTransferFileBusiness = new TransferFileBusiness(this);
        }
        if (mCamIdUserBusiness == null) {
            mCamIdUserBusiness = new CamIdUserBusiness(this);
        }
        if(firebaseEventBusiness == null){
            firebaseEventBusiness = new FirebaseEventBusiness(this);
        }
    }

    public void recreateBusiness() {
        //setting
        SettingBusiness.getInstance(this).init();
        // app state
        mAppStateManager = null;
        mAppStateManager = new ApplicationStateManager(this);
        // app lock
        mAppLockManager = null;
        mAppLockManager = new ApplicationLockManager(this);
        // block
        mBlockContactBusiness = null;
        mBlockContactBusiness = new BlockContactBusiness(this);
        //message
        mMessageBusiness = null;
        mMessageBusiness = new MessageBusiness(this);
        //sticker
        stickerBusiness = null;
        stickerBusiness = new StickerBusiness(this);
        //contact
        mContactBusiness = null;
        mContactBusiness = new ContactBusiness(this);
        // stranger
        mStrangerBusiness = null;
        mStrangerBusiness = new StrangerBusiness(this);
        //config
        mConfigBusiness = null;
        mConfigBusiness = new ContentConfigBusiness(this);
        // music
        mMusicBusiness = null;
        mMusicBusiness = new MusicBusiness(this);
        //keeng profile
        mKeengProfileBusiness = null;
        mKeengProfileBusiness = new KeengProfileBusiness(this);
        mFeedBusiness = null;
        mFeedBusiness = new FeedBusiness(this);
        //image profile
        mImageProfileBusiness = null;
        mImageProfileBusiness = new ImageProfileBusiness(this);
        //call
        mCallBusiness = null;
        mCallBusiness = new CallBusiness(this);
        CallHistoryHelper.getInstance().resetData();
        mPlayMusicController = null;
        mPlayMusicController = new PlayMusicController(this);
        if (mTransferFileBusiness == null) {
            mTransferFileBusiness = new TransferFileBusiness(this);
        }
        // xoa danh sach so subcribe
        PubSubManager pubSubManager = PubSubManager.getInstance(this);
        if (pubSubManager != null) {
            pubSubManager.clearSubscribeList();
            pubSubManager.clearSubscribeRoom();
        }
        initData();
    }

    @Override
    public void onLowMemory() {
        Log.d(TAG, "onLowMemory");
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        Log.d(TAG, "onTerminate");
        // chua biet huy dang ky o dau ca
        ContentObserverBusiness.getInstance(this).unRegisterContactObserver();
        unRegisterSmsObserver();
        super.onTerminate();
    }

    public ContactBusiness getContactBusiness() {
        return mContactBusiness;
    }

    public BlockContactBusiness getBlockContactBusiness() {
        return mBlockContactBusiness;
    }

    public LoginBusiness getLoginBusiness() {
        return loginBusiness;
    }

    public MessageBusiness getMessageBusiness() {
        return mMessageBusiness;
    }

    public void setReengAccountBusiness(ReengAccountBusiness mAccountBusiness) {
        this.mAccountBusiness = mAccountBusiness;
    }

    public ReengAccountBusiness getReengAccountBusiness() {
        return mAccountBusiness;
    }

    public AvatarBusiness getAvatarBusiness() {
        return mAvatarBusiness;
    }

    public ApplicationStateManager getAppStateManager() {
        return mAppStateManager;
    }

    public ApplicationLockManager getAppLockManager() {
        return mAppLockManager;
    }

    public OfficerBusiness getOfficerBusiness() {
        return mOfficerBusiness;
    }

    public MusicBusiness getMusicBusiness() {
        return mMusicBusiness;
    }

    public ContentConfigBusiness getConfigBusiness() {
        return mConfigBusiness;
    }

    public StickerBusiness getStickerBusiness() {
        return stickerBusiness;
    }

    public StrangerBusiness getStrangerBusiness() {
        return mStrangerBusiness;
    }

    public KeengProfileBusiness getKeengProfileBusiness() {
        return mKeengProfileBusiness;
    }

    public FeedBusiness getFeedBusiness() {
        return mFeedBusiness;
    }

    public ImageProfileBusiness getImageProfileBusiness() {
        return mImageProfileBusiness;
    }

    public PlayMusicController getPlayMusicController() {
        return mPlayMusicController;
    }

    public TransferFileBusiness getTransferFileBusiness() {
        return mTransferFileBusiness;
    }

    public CamIdUserBusiness getCamIdUserBusiness() {
        return mCamIdUserBusiness;
    }

    public FirebaseEventBusiness getFirebaseEventBusiness(){
        return firebaseEventBusiness;
    }

    private void initBusiness() {
        mConfigBusiness.init();
        ListenerHelper.getInstance().onConfigDataReadyChange();

        mBlockContactBusiness.init();
        mOfficerBusiness.init();
        mMessageBusiness.init();
        mContactBusiness.init();
        mMusicBusiness.init();
        stickerBusiness.init();
        mStrangerBusiness.init();
        mImageProfileBusiness.init();
    }

    public EmoticonDecodeQueue getQueueDecodeEmo() {
        if (mQueueDecodeEmo == null)
            mQueueDecodeEmo = new EmoticonDecodeQueue(this);
        return mQueueDecodeEmo;
    }

    public ProcessSmsReceivedQueue getProcessSmsQueue() {
        if (mProcessSmsReceivedQueue == null) {
            mProcessSmsReceivedQueue = new ProcessSmsReceivedQueue(this);
        }
        return mProcessSmsReceivedQueue;
    }

    public AlarmReceiver getAlarmReceiver() {
        return mAlarmReceiver;
    }

    public boolean isDataReady() {
        return !(!mContactBusiness.isContactReady() ||
                !mMessageBusiness.isMessageDataReady() ||
                !mBlockContactBusiness.isReady());
    }

    private void initData() {
        Log.d(TAG, "[perform] initData");
        if (!isDataReady()) {
            mReloadDataThread = null;
            mReloadDataThread = new ReloadDataThread();
            mReloadDataThread.setPriority(Thread.MAX_PRIORITY);
            mReloadDataThread.start();
        } else {
            // neu data da load xong thif check version code luon. khong thi cho khi load xong data
            checkVersionCode();
        }
    }

    private class ReloadDataThread extends Thread {
        @Override
        public void run() {
            long beginTime = System.currentTimeMillis();
            initBusiness();
            Log.i(TAG, "[perform] init business take " + (System.currentTimeMillis() - beginTime) + " ms");
            // block business
            mBlockContactBusiness.initBlockBusiness();
            //initUniversalLoader();
            // contact, load CONTACT khi da dang nhap, chua dang nhap thi ko goi init CONTACT
            boolean isNewAccount = false;
            //Log.i(TAG, "[perform] init universal take " + (System.currentTimeMillis() - beginTime) + " ms");
            beginTime = System.currentTimeMillis();
            mQueueDecodeEmo = new EmoticonDecodeQueue(ApplicationController.this);
            // mQueueCheckSpamStranger = new CheckSpamStrangerQueue(mInstance);
            /* chua dang nhap van load message (ko anh huong den load contact hay khong.
            th ng dung bi out ra dang nhap lai thi load message trc tranh th lap thread */
            if (!mMessageBusiness.isMessageDataReady()) {
                mMessageBusiness.loadAllThreadMessageOnFirstTime(null);
            }
            Log.i(TAG, "[perform] load message take " + (System.currentTimeMillis() - beginTime) + " ms");
            if (mAccountBusiness.isValidAccount()) {
                beginTime = System.currentTimeMillis();
                if (!mContactBusiness.isContactReady()) {
                    mContactBusiness.initContact();
                }
                Log.i(TAG, "[perform] load contact take " + (System.currentTimeMillis() - beginTime) + " ms");
                mMessageBusiness.updateThreadStrangerAfterLoadData();
                // neu da dang nhap thi moi goi init contact, CONTACT ok thi notify
                ListenerHelper.getInstance().onDataReadyChange();
            } else {
                isNewAccount = true;
            }
            // xyz method cu
            Log.i(TAG, "[perform] start xyzMethod");
            beginTime = System.currentTimeMillis();
            mContactBusiness.initNonContact();
            //load data sau khi notify data ready
            mMusicBusiness.updateStateAndSessionMusicAfterRestartApp();
            // officer
            mOfficerBusiness.initOfficerAccountList();
            // init media
            if (!mMusicBusiness.isDataReady()) {
                mMusicBusiness.initMedia();
            }
            stickerBusiness.loadAllSticker();
            checkVersionCode();
            initFolder();
            startIMService();
            if (!isNewAccount && !mContactBusiness.isSyncContact()) {// account cu va contact chua sync
                // neu db contact da duoc insert roi thi
                if (!mContactBusiness.isNewInsertDB()) {
                    mContactBusiness.syncContact();
                    mContactBusiness.initArrayListPhoneNumber();
                } else {
                    mContactBusiness.setNewInsertDB(false);
                }
                mMessageBusiness.updateThreadStrangerAfterLoadData();
                ListenerHelper.getInstance().onDataReadyChange();
            }
            //mCallBusiness.setLicense();
            stickerBusiness.getStickerCollectionFromServer(true);
            mConfigBusiness.getConfigFromServer(false);// debug always get config
            if (!mPref.getBoolean(Constants.PREFERENCE.PREF_GET_LIST_BLOCK, false)) {
                ContactRequestHelper.getInstance(ApplicationController.this).getBlockListV5();
            }
            // mOfficerBusiness.getListOfficerAccountFromSv();
//            ReengNotificationManager.clearMessagesNotification(ApplicationController.this, Constants.NOTIFICATION.NOTIFY_CALL);
            cancelNotification(Constants.NOTIFICATION.NOTIFY_CALL);
            Log.i(TAG, "[perform] stop xyzMethod - " + (System.currentTimeMillis() - beginTime) + " ms");
            mReloadDataThread = null;
        }
    }

    /**
     * load contact, message sau khi login bang code
     */
    public void loadDataAfterLogin() {
        mLoadDataAfterLoginThread = null;
        mLoadDataAfterLoginThread = new LoadDataAfterLoginThread();
        mLoadDataAfterLoginThread.setPriority(Thread.MAX_PRIORITY);
        mLoadDataAfterLoginThread.start();
    }

    public void reLoadContactAfterPermissionsResult() {
        mReLoadContactAfterPermissionsResult = null;
        mReLoadContactAfterPermissionsResult = new ReLoadContactAfterPermissionsResult();
        mReLoadContactAfterPermissionsResult.setPriority(Thread.MAX_PRIORITY);
        mReLoadContactAfterPermissionsResult.start();
        ContentObserverBusiness.getInstance(getApplicationContext()).registerContactObserver();
    }

    /**
     * load contact, message sau khi login bang code
     */
    private class LoadDataAfterLoginThread extends Thread {
        @Override
        public void run() {
            Log.i(TAG, "[perform] start LoadDataAfterLoginThread");
            long beginTime;
            beginTime = System.currentTimeMillis();
            if (!mContactBusiness.isContactReady()) {
                mContactBusiness.init();
                mContactBusiness.initContact();
                mContactBusiness.setInfoAllPhoneNumber();
            }
            Log.i(TAG, "[perform] load contact take " + (System.currentTimeMillis() - beginTime) + " ms");
            // chua dang nhap chua load contact thi cung ko load message nua
            beginTime = System.currentTimeMillis();
            if (!mMessageBusiness.isMessageDataReady()) {
                mMessageBusiness.init();
                mMessageBusiness.loadAllThreadMessageOnFirstTime(null);
            }
            Log.i(TAG, "[perform] load message take " + (System.currentTimeMillis() - beginTime) + " ms");
            mMessageBusiness.updateThreadStrangerAfterLoadData();
            ListenerHelper.getInstance().onDataReadyChange();
            mMusicBusiness.updateStateAndSessionMusicAfterRestartApp();
            //get config, sticker
            stickerBusiness.getStickerCollectionFromServer(true);
            mConfigBusiness.deleteAllKeyConfig();
            mConfigBusiness.getConfigFromServer(true);
            //  mOfficerBusiness.getListOfficerAccountFromSv();
            Log.i(TAG, "[perform] stop LoadDataAfterLoginThread - ");
            //  reloadTaskxxx();
            mLoadDataAfterLoginThread = null;
            ContactRequestHelper.getInstance(ApplicationController.this).getBlockListV5();
            ListenerHelper.getInstance().onDataReadyChange();
            EventBus.getDefault().post(new HomeContactsFragment.ReloadDataHomeContactEvent());
        }
    }

    private class ReLoadContactAfterPermissionsResult extends Thread {
        @Override
        public void run() {
            Log.i(TAG, "[perform] start ReLoadContactAfterPermissionsResult - ");
            while (!isDataReady()) {
                try {
                    sleep(200);
                } catch (InterruptedException e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            if (mAccountBusiness.isValidAccount()) {
                mContactBusiness.initContact();
                mMessageBusiness.updateThreadStrangerAfterLoadData();
                // neu da dang nhap thi moi goi init contact, CONTACT ok thi notify
                ListenerHelper.getInstance().onDataReadyChange();
                DeviceAccountManager.getInstance(ApplicationController.this).addAccountToDevice();

            }
            mReLoadContactAfterPermissionsResult = null;
        }
    }

    private void getSizeScreen(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        if (displaymetrics.widthPixels > displaymetrics.heightPixels) {
            // width>height (set nguoc lai)
            widthPixels = displaymetrics.heightPixels;
            heightPixels = displaymetrics.widthPixels;
        } else {
            widthPixels = displaymetrics.widthPixels;
            heightPixels = displaymetrics.heightPixels;
        }
    }

    public void initFolder() {
        // Output stream to write file
        File recordDir = new File(Config.Storage.REENG_STORAGE_FOLDER);
        if (!recordDir.exists()) {
            recordDir.mkdirs();
        }
        File profile = new File(Config.Storage.REENG_STORAGE_FOLDER
                + Config.Storage.PROFILE_PATH);
        if (!profile.exists()) {
            profile.mkdirs();
        }
        // check folder avatar
        File listAvatar = new File(Config.Storage.REENG_STORAGE_FOLDER
                + Config.Storage.LIST_AVATAR_PATH);
        if (!listAvatar.exists()) {
            listAvatar.mkdirs();
        }
        File imageFolder = new File(Config.Storage.REENG_STORAGE_FOLDER
                + Config.Storage.IMAGE_FOLDER);
        if (!imageFolder.exists()) {
            imageFolder.mkdirs();
        }
        imageFolder = new File(Config.Storage.REENG_STORAGE_FOLDER
                + Config.Storage.IMAGE_COMPRESSED_FOLDER);
        if (!imageFolder.exists()) {
            imageFolder.mkdirs();
        }
        File voiceFolder = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.VOICEMAIL_FOLDER);
        if (!voiceFolder.exists()) {
            voiceFolder.mkdirs();
        }
        //sticker
        File stickerFolder = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.STICKER_FOLDER);
        if (!stickerFolder.exists()) {
            stickerFolder.mkdirs();
        }
        //sharevideov2
        File videoFolder = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.VIDEO_FOLDER);
        if (!videoFolder.exists()) {
            videoFolder.mkdirs();
        }
        //cache
        File cacheFolder = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.CACHE_FOLDER);
        if (!cacheFolder.exists()) {
            cacheFolder.mkdirs();
        }
        // gif
        File gifFolder = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.GIF_FOLDER);
        if (!gifFolder.exists()) {
            gifFolder.mkdirs();
        }
        // downloads
        File downloadFolder = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.DOWNLOAD_FOLDER);
        if (!downloadFolder.exists()) {
            downloadFolder.mkdirs();
        }
        if (SettingBusiness.getInstance(getApplicationContext()).getPrefShowMedia()) {
            FileHelper.deleteNoMediaFile(this);
        } else {
            FileHelper.createNoMediaFile(this);
        }
    }

    public void initFolderFileDocument() {
        File documentFolder = new File(Config.Storage.FILE_DOCUMENT_MOCHA);
        if (!documentFolder.exists()) {
            documentFolder.mkdirs();
        }
    }

    public int getWidthPixels() {
        return widthPixels;
    }

    public int getHeightPixels() {
        return heightPixels;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = mRes.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = mRes.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public double getDensity() {
        return mDensity;
    }

    /*Cap nhat so tin nhan chua doc tren icon
    Neu =0 thi bo count
    */
    public void updateCountNotificationIcon() {
        int numberOfUnreadMessage = mMessageBusiness.getNumberOfUnreadMessage();
        Log.i(TAG, "updateCountNotificationIcon: " + numberOfUnreadMessage);
        ShortcutBadger.applyCount(ApplicationController.this, numberOfUnreadMessage);
        if (changeUnreadMessageListener != null) {
            changeUnreadMessageListener.onChangeUnreadMessage(numberOfUnreadMessage);
        }
    }

    public void removeCountNotificationIcon() {
        ShortcutBadger.applyCount(ApplicationController.this, 0);
    }

    public PauseOnScrollListener getPauseOnScrollListener(AbsListView.OnScrollListener scrollListener) {
        return new PauseOnScrollListener(this, false, false, scrollListener);
    }

    public PauseOnScrollRecyclerViewListener getPauseOnScrollRecyclerViewListener(RecyclerView.OnScrollListener scrollListener) {
        return new PauseOnScrollRecyclerViewListener(this, false, false, scrollListener);
    }

    private int checkScreenLayout() {
        if (mRes == null) mRes = getResources();
        return mRes.getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
    }

    public void wakeLockAcquire() {
        if (wakeLock != null && !wakeLock.isHeld()) {
            wakeLock.acquire();
            Log.i(TAG, "wakeLockAcquire");
        }
    }

    public void wakeLockRelease() {
        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
            Log.i(TAG, "wakeLockRelease");
        }
    }

    /**
     * kiem tra su thay doi version code
     */
    private void checkVersionCode() {
        int currentVersion = BuildConfig.VERSION_CODE;
        int oldVersion = mPref.getInt(Constants.PREFERENCE.PREF_APP_VERSION, -1);
        Log.i(TAG, "checkVersionCode oldVersion = " + oldVersion + " ; currentVersion = " + currentVersion);
        //ImageLoaderManager.getInstance(getApplicationContext()).clearDiskCacheBackgroundDefault();
        if (oldVersion == -1) {//cai moi
            mPref.edit().putInt(Constants.PREFERENCE.PREF_APP_VERSION, currentVersion).apply();
        } else if (oldVersion != currentVersion) {// co thay doi version app
            // co su thay doi tu version <18 len version >=18. clear cache sticker
            if (oldVersion < 18 || oldVersion < 22) {
                ImageLoaderManager.getInstance(getApplicationContext()).clearDiskCacheSticker(EmoticonUtils
                        .STICKER_DEFAULT_COLLECTION_ID);
            }
            // version 19 xoa cache bg default
            if (oldVersion < 19 || oldVersion < 1028) {
                Log.i(TAG, "clearDiskCacheBackgroundDefault");
                ImageLoaderManager.getInstance(getApplicationContext()).clearDiskCacheBackgroundDefault();
            }
            // version 147 them config on/off tab hot
            if (oldVersion < 149) {
                //reget config
                mPref.edit().putBoolean(Constants.PREFERENCE.CONFIG.PREF_FORCE_GET_CONFIG_NOT_DONE, true).apply();
            }
            mConfigBusiness.getConfigFromServer(true);
            //TODO xu ly cac truong hop tang version code khac o day
            // cap nhat version moi nhat vao pref
            mPref.edit().putInt(Constants.PREFERENCE.PREF_APP_VERSION, currentVersion).apply();
            if (mAccountBusiness != null && mAccountBusiness.isValidAccount() && !mAccountBusiness.isAnonymousLogin()) {
                mPref.edit().putBoolean(FIREBASE_PROPERTY_SEND_SERVER, false).apply();
            }

            if (oldVersion <= 1218)
                DeviceAccountManager.getInstance(this).removeAccount();

            mMessageBusiness.processUnknownMessage(
                    new MessageInteractionListener.LoadUnknownMessageAndReloadAllThread() {
                        @Override
                        public void onStartLoadData() {
                            Log.i(TAG, "onStartLoadData");
                            isParseNewMessage = true;
                        }

                        @Override
                        public void onLoadAllDone() {
                            Log.i(TAG, "onLoadAllDone");
                            isParseNewMessage = false;
                            ListenerHelper.getInstance().onUpdateMessageDone();
                        }
                    });

            if (mXmppManager != null && mXmppManager.isAuthenticated()) {
                mXmppManager.sendIQClientInfo(Connection.TOKEN_AUTH_NON_SASL, true,
                        XMPPManager.IqInfoFirstTime.AFTER_UPDATE.VALUE);
            } else {
                if (mXmppManager == null) mXmppManager = new XMPPManager(this);
                mXmppManager.setNeedSendIQUpdate(true);
            }
        }
    }

    public boolean isParseNewMessage() {
        return isParseNewMessage;
    }

    private void initGATracker() {

    }

    /**
     * thong ke ga
     */
    public void trackingEvent(String category, String action, String label) {

    }

    public void trackingEvent(int categoryId, int actionId, int labelId) {
        if (Config.Server.FREE_15_DAYS) return;
        trackingEvent(mRes.getString(categoryId), mRes.getString(actionId), mRes.getString(labelId));
    }

    public void trackingScreen(BaseSlidingFragmentActivity activity, String screenName) {

    }

    public void trackingException(String exceptionDescription, boolean isFatal) {

    }

    public void trackingEventFirebase(String key, String value, String eventName) {

    }

    //TODO chuyển từ GA sang FA ko thấy có hàm log speed nên tạm thời comment lại
    public void trackingSpeed(String category, String label, long speed) {
        if (Config.Server.FREE_15_DAYS) return;
        /*myTracker.send(new HitBuilders.TimingBuilder()
                .setCategory(category)
                .setVariable(NetworkHelper.getInstance().getConnectedType() + " vip: " + mAccountBusiness.isVip())
                .setLabel(label)
                .setValue(speed)
                .build());*/
    }

    public void logEventFacebookSDKAndFirebase(String name) {

    }

    public void logEventCamFacebookSDKAndFirebase(String name) {

    }

    private void stopAlarmPingPongWhenCreate() {
        if (mAlarmReceiver != null) {
            mAlarmReceiver.cancelAlarmManager(this, Constants.ALARM_MANAGER.MUSIC_PING_ID);
            mAlarmReceiver.cancelAlarmManager(this, Constants.ALARM_MANAGER.MUSIC_PONG_ID);
        }
    }

    public PhoneNumberUtil getPhoneUtil() {
        if (phoneUtil == null) {
            phoneUtil = PhoneNumberUtil.getInstance();
        }
        return phoneUtil;
    }

    public ReengSQLiteHelper getReengSQLiteHelper() {
        return mReengSQLiteHelper;
    }

    public XMPPManager getXmppManager() {
        return mXmppManager;
    }

    public ConfigProperties getConfigProperties() {
        if (distributionChannelProperties == null) {
            distributionChannelProperties = new ConfigProperties(this, "XNK");
        }
        return distributionChannelProperties;
    }

    public void settingLocal(String language) {
        Log.i(TAG, "settingLocal language: " + language);
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        if (Version.hasJellyBeanMR1()) {
            config.setLocale(locale);
        } else {
            config.locale = locale;
        }
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics
                ());
        getApplicationContext().getResources().updateConfiguration(config, getBaseContext().getResources()
                .getDisplayMetrics());
    }

    public void setSettingLanguage(boolean isSettingLanguage) {
        this.isSettingLanguage = isSettingLanguage;
    }

    public boolean isSettingLanguage() {
        return isSettingLanguage;
    }

    public void setChangeUnreadMessageListener(ChangeUnreadMessageListener changeUnreadMessageListener) {
        this.changeUnreadMessageListener = changeUnreadMessageListener;
    }

    private boolean isRegisterSms = false;

    public void registerSmsObserver() {
        try {
            if (isRegisterSms) return;
            mSmsObserver = new SmsObserver(new Handler(), this);
            getContentResolver().registerContentObserver(Uri.parse("content://sms"), true, mSmsObserver);
            isRegisterSms = true;
        } catch (SecurityException ex) {
            Log.e(TAG, "registerSmsObserver: " + ex);
        } catch (RuntimeException ex) {
            Log.e(TAG, "registerSmsObserver: " + ex);
        }
    }

    public void unRegisterSmsObserver() {
        try {
            isRegisterSms = false;
            if (mSmsObserver != null) {
                getContentResolver().unregisterContentObserver(mSmsObserver);
            }
        } catch (SecurityException ex) {
            Log.e(TAG, "unRegisterSmsObserver: " + ex);
        } catch (RuntimeException ex) {
            Log.e(TAG, "unRegisterSmsObserver: " + ex);
        }
    }

    public CallBusiness getCallBusiness() {
        return mCallBusiness;
    }

    public SharedPreferences getPref() {
        return mPref;
    }

    private void registerNetWorkReceiver() {
        if (Version.hasN()) {
            registerReceiver(new NetworkReceiver(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    public void logDebugContent(String content) {
        LogDebugHelper.getInstance(this).logDebugContent(content);
    }

    public void clearDBLogDebug() {
        LogDebugHelper.getInstance(this).clearDb();
    }

    //Them tu imservice qua hainv
    private static final long[] vibrate = {300, 200, 100, 10};
    private static final long[] noVibraPattern = {0, 0, 0, 0};
    private NotificationManager mNotificationManager;
    private HeadsetPlugReceiver headsetPlugReceiver;

    private void initHeadsetPlugReceiver() {
        headsetPlugReceiver = new HeadsetPlugReceiver();
        IntentFilter filter = new IntentFilter();
        for (String action : HeadsetPlugReceiver.getHeadphoneActions()) {
            filter.addAction(action);
        }
        registerReceiver(headsetPlugReceiver, filter);
    }

    public void deleteNotifyChannel(int type) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String channelId = getChannelIdFromCache(type);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);
        }
    }

    public void deleteAllNotifyChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            //Xoa nhung thang cua ban cu
            mNotificationManager.deleteNotificationChannel(Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG + "");
            mNotificationManager.deleteNotificationChannel(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID + "");
            mNotificationManager.deleteNotificationChannel(Constants.NOTIFICATION.NOTIFY_MESSAGE + "");
            mNotificationManager.deleteNotificationChannel(Constants.NOTIFICATION.NOTIFY_ONMEDIA + "");
            mNotificationManager.deleteNotificationChannel(Constants.NOTIFICATION.NOTIFY_OTHER + "");
            mNotificationManager.deleteNotificationChannel(Constants.NOTIFICATION.NOTIFY_CALL + "");
            mNotificationManager.deleteNotificationChannel(Constants.NOTIFICATION.NOTIFY_MOCHA_VIDEO + "");
            mNotificationManager.deleteNotificationChannel(Constants.NOTIFICATION.NOTIFY_DEEPLINK + "");

            //Xoa cho ban moi
            String channelId;

            channelId = getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);

            channelId = getChannelIdFromCache(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);

            channelId = getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_MESSAGE);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);

            channelId = getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_ONMEDIA);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);

            channelId = getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_OTHER);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);

            channelId = getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_CALL);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);

            channelId = getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_MOCHA_VIDEO);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);

            channelId = getChannelIdFromCache(Constants.NOTIFICATION.NOTIFY_DEEPLINK);
            if (!TextUtils.isEmpty(channelId))
                mNotificationManager.deleteNotificationChannel(channelId);

            genAndSaveAllChannelIdToCache();
        }
    }

    private String generateChannelIdFor(int type) {
        return type + "_" + System.currentTimeMillis();
    }

    private void genAndSaveAllChannelIdToCache() {
        SharedPrefs.getInstance().put(Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG + "", generateChannelIdFor(Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG));
        SharedPrefs.getInstance().put(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID + "", generateChannelIdFor(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID));
        SharedPrefs.getInstance().put(Constants.NOTIFICATION.NOTIFY_MESSAGE + "", generateChannelIdFor(Constants.NOTIFICATION.NOTIFY_MESSAGE));
        SharedPrefs.getInstance().put(Constants.NOTIFICATION.NOTIFY_ONMEDIA + "", generateChannelIdFor(Constants.NOTIFICATION.NOTIFY_ONMEDIA));
        SharedPrefs.getInstance().put(Constants.NOTIFICATION.NOTIFY_OTHER + "", generateChannelIdFor(Constants.NOTIFICATION.NOTIFY_OTHER));
        SharedPrefs.getInstance().put(Constants.NOTIFICATION.NOTIFY_CALL + "", generateChannelIdFor(Constants.NOTIFICATION.NOTIFY_CALL));
        SharedPrefs.getInstance().put(Constants.NOTIFICATION.NOTIFY_MOCHA_VIDEO + "", generateChannelIdFor(Constants.NOTIFICATION.NOTIFY_MOCHA_VIDEO));
        SharedPrefs.getInstance().put(Constants.NOTIFICATION.NOTIFY_DEEPLINK + "", generateChannelIdFor(Constants.NOTIFICATION.NOTIFY_DEEPLINK));
    }

    private String genAndSaveChannelIdToCache(int channelType) {
        String channelId = generateChannelIdFor(channelType);
        SharedPrefs.getInstance().put(channelType + "", channelId);
        return channelId;
    }

    public String getChannelIdFromCache(int type) {
//        return SharedPrefs.getInstance().get(type + "", String.class);
        String channelID = SharedPrefs.getInstance().get(type + "", String.class);
        if (TextUtils.isEmpty(channelID)) {
            channelID = genAndSaveChannelIdToCache(type);
        }

        return channelID;
    }

    public synchronized void notifyWrapper(int id, Notification notification, int channelType) {
        try {
            if (mNotificationManager != null) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    String channelName;
                    switch (channelType) {
                        case Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_FAKE_NEW_MSG;
                            break;
                        case Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.MUSIC_NOTIFICATION_ID;
                            break;
                        case Constants.NOTIFICATION.NOTIFY_MESSAGE:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_MESSAGE;
                            break;
                        case Constants.NOTIFICATION.NOTIFY_ONMEDIA:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_ONMEDIA;
                            break;
                        case Constants.NOTIFICATION.NOTIFY_OTHER:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_OTHER;
                            break;
                        case Constants.NOTIFICATION.NOTIFY_CALL:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_CALL;
                            break;
                        case Constants.NOTIFICATION.NOTIFY_CALL_HEAD_UP:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_HEAD_UP_CALL;
                            break;
                        case Constants.NOTIFICATION.NOTIFY_MOCHA_VIDEO:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_MOCHA_VIDEO;
                            break;
                        case Constants.NOTIFICATION.NOTIFY_DEEPLINK:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_DEEPLINK;
                            break;
                        default:
                            channelName = Constants.NOTIFICATION_CHANNEL_NAME.NOTIFY_OTHER;
                            break;
                    }

                    //Lay channel id tu cache
                    String channelID = getChannelIdFromCache(channelType);

                    boolean enableSound = SettingBusiness.getInstance(this).getPrefRingtone();
                    boolean enableVibrate = SettingBusiness.getInstance(this).getPrefVibrate();
                    AudioAttributes att = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                            .build();

                    int importance;

                    if (enableSound && enableVibrate) {
                        if (channelType == Constants.NOTIFICATION.NOTIFY_CALL || channelType == Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID)
                            importance = NotificationManager.IMPORTANCE_LOW;
                        else
                            importance = NotificationManager.IMPORTANCE_HIGH;
                    } else
                        importance = NotificationManager.IMPORTANCE_LOW;

                    NotificationChannel notificationChannel = new NotificationChannel(channelID, channelName, importance);
                    notificationChannel.enableLights(true);

                    if (channelType == Constants.NOTIFICATION.NOTIFY_CALL || channelType == Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID) {
                        notificationChannel.enableVibration(false);
                        notificationChannel.setSound(null, null);

                        notificationChannel.setShowBadge(false);
                        notificationChannel.setLightColor(Color.WHITE);
                    } else {
                        notificationChannel.enableVibration(enableVibrate);
                        notificationChannel.setVibrationPattern(enableVibrate ? vibrate : noVibraPattern);
                        notificationChannel.setSound(enableSound ? SettingBusiness.getInstance(this).getCurrentSoundUri() : null, att);

                        notificationChannel.setShowBadge(true);
                        notificationChannel.setLightColor(Color.RED);
                    }

//                    notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    mNotificationManager.createNotificationChannel(notificationChannel);
                }

                mNotificationManager.notify(id, notification);
                Log.i(TAG, "notify -- im service" + id);
            } else {
                Log.i(TAG, "mNotificationManager not ready, discarding notification");
                logDebugContent("notifyWrapper not ready, discarding notification");
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            logDebugContent("notifyWrapper Exception");
        }
    }

    public void cancelNotification(int notificationId) {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(notificationId);
        }
    }

    private ReengAccountBusiness mReengAccountBusiness;
    private static boolean isReconnectThreadRunning = false;
    private boolean isCheckNetwork = true;
    private Thread loadDataWaitThread;
    private int numberReconnectFail = 0;

    private void checkAppForegroundToConnectWhenStartService() {
        if (!getAppStateManager().isAppWentToBg() && isDataReady()) {
            Log.sys(TAG, "app is foreground --> call reconnect ");
            connectByToken();
        } else {
            Log.sys(TAG, "app is background --> not reconnect: " + getAppStateManager().isAppWentToBg() + " isDataReady: " + isDataReady());
            if (loadDataWaitThread == null) {
                loadDataWaitThread = new LoadDataWaitThread();
                loadDataWaitThread.setDaemon(true);
                loadDataWaitThread.start();
            }
        }
    }

    /**
     * goi ham nay trong cac truong hop
     * 1. vao app ma ko co ket noi xmpp
     * 2. auto reconnect khi reconnect fail
     * 3. khoi tao service
     * 4. gui message timeout
     * 5. ConnectionClosed by server
     * 6. ConnectionClosedOnError
     * 7. Connectivity change
     * 8. GCM new message
     */
    public synchronized void connectByToken() {
        connectByToken(true);
    }

    public synchronized void connectByToken(boolean isCheckNetwork) {
        if (getReengAccountBusiness().isAnonymousLogin()) return;
        if (getReengAccountBusiness().isProcessingChangeNumber()) {
            Log.f(TAG, "connectByToken isProcessingChangeNumber");
            return;
        }
        if (RestoreManager.isRestore()) {
            return;
        }
        Log.sys(TAG, "connectByToken", true);
        this.isCheckNetwork = isCheckNetwork;
        // lan dau cai app chua dang nhap thi ko load contact, ko wait nua
        if (getReengAccountBusiness() != null &&
                !getReengAccountBusiness().isValidAccount()) {
            Log.sys(TAG, "connectByToken  !isValidAccount", true);
        } else if (!isDataReady()) {//wait data load complete then login
            Log.sys(TAG, "connectByToken  !isDataReady", true);
            if (loadDataWaitThread == null) {
                loadDataWaitThread = new LoadDataWaitThread();
                loadDataWaitThread.setDaemon(true);
                loadDataWaitThread.start();
            }
        } else {
            connectByTokenWhenDataLoaded(isCheckNetwork);
        }
    }

    private synchronized void connectByTokenWhenDataLoaded(final boolean isCheckNetwork) {
        if (getReengAccountBusiness().isAnonymousLogin()) return;
        if (RestoreManager.isRestore()) {
            return;
        }
        Log.sys(TAG, "connectByTokenWhenDataLoaded: " + NetworkHelper.isConnectInternet(this), true);
        Thread reconnectThread;
        mXmppManager = getXmppManager();
        mReengAccountBusiness = getReengAccountBusiness();
        if (isCheckNetwork && !NetworkHelper.isConnectInternet(this)) {
            Log.sys(TAG, "network not available --> not reconnect", true);
        } else if (mReengAccountBusiness.isValidRevision() && mXmppManager != null) {
            if (mReengAccountBusiness.isValidAccount()) {
                //neu dang chay hoac da xac thuc roi thi thoi
                if (isReconnectThreadRunning || mXmppManager.isAuthenticated()) {
                    Log.sys(TAG, "not connectByTokenWhenDataLoaded because " +
                            " isReconnectThreadRunning: " + isReconnectThreadRunning +
                            " isAuthenticated: " + mXmppManager.isAuthenticated(), true);
                    return;
                }
                reconnectThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                        long beginTime = System.currentTimeMillis();
                        Log.sys(TAG, "[] Start A thread call connectByToken ");
                        if (mXmppManager != null) {
                            XMPPManager.notifyXMPPConnecting();
                            try{
                                mXmppManager.connectByToken(ApplicationController.this, mReengAccountBusiness.getJidNumber(),
                                        mReengAccountBusiness.getToken(), mReengAccountBusiness.getRegionCode(), mReengAccountBusiness.getCurrentAccount().getPreKey());
                                long elapsedTime = (System.currentTimeMillis() - beginTime);
                                Log.sys(TAG, "[] End the thread call connectByToken take " + elapsedTime + " ms");
                                isReconnectThreadRunning = false;
                                checkConditionToAutoReconnect(isCheckNetwork);
                            }catch (Exception ex){
                                android.util.Log.d(TAG, "run: " + ex.getMessage());
                            }


                        }

                    }
                });
                reconnectThread.setDaemon(true);
                isReconnectThreadRunning = true;
                reconnectThread.start();
            } else {
                Log.sys(TAG, "account not valid --> not reconnect", true);
            }
        } else {
            Log.sys(TAG, "revision not valid --> not reconnect: " + (mXmppManager == null), true);
        }
    }

    private void checkConditionToAutoReconnect(boolean isCheckNetwork) {
        //neu reconnect ko thanh cong va co mang va so lan reconnect
        // chua vuot qua nguong thi 10s sau tu dong reconnect lai
//        if (mApplication == null) return;
        mXmppManager = getXmppManager();
        mReengAccountBusiness = getReengAccountBusiness();
        if (mXmppManager != null && mXmppManager.isAuthenticated()) {
            numberReconnectFail = 0;//neu da xac thuc roi thi ko can nua
        } else {
            numberReconnectFail++;
            if (mReengAccountBusiness.isValidRevision() && numberReconnectFail < 6) {
                if (mXmppManager != null && !mXmppManager.isAuthenticated()) {
                    // Wait 100 ms for processes to clean-up, then shutdown.
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                    Log.sys(TAG, "AutoReconnect reconnect : " + numberReconnectFail, true);
                    connectByToken(isCheckNetwork);
                }
            }
        }
    }

    private class LoadDataWaitThread extends Thread {
        public void run() {
            Log.sys(TAG, "[] waiting for LoadData");
            while (!isDataReady()) {
                try {
                    sleep(150);
                } catch (InterruptedException e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            loadDataWaitThread = null;
            connectByTokenWhenDataLoaded(isCheckNetwork);
        }
    }

    public boolean isReady() {
        return true;
    }

    //Xu ly phan sms otp
    GoogleApiClient mCredentialsApiClient;
    MySMSBroadcastReceiver smsBroadcast;
    boolean isRegisterSmsOtp;

    public void setRegisterSmsOtp(boolean registerSmsOtp) {
        isRegisterSmsOtp = registerSmsOtp;
    }

    public void registerSmsOTPObserver() {
        try {
            if (isRegisterSmsOtp) return;

            if (mCredentialsApiClient == null) {
                mCredentialsApiClient = new GoogleApiClient.Builder(this)
                        .addApi(Auth.CREDENTIALS_API)
                        .build();
            }

//        requestPhoneNumberHint();
            startSMSListener();

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);

            registerReceiver(smsBroadcast, intentFilter);
            isRegisterSmsOtp = true;
        } catch (Exception e) {
            Log.e(TAG, "Error registerSmsOTPObserver");
        }
    }

    private void startSMSListener() {

        SmsRetrieverClient client = SmsRetriever.getClient(this);
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                Toast.makeText(LoginActivity.this, "SMS Retriever starts", Toast.LENGTH_LONG).show();

            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(LoginActivity.this, "Error", Toast.LENGTH_LONG).show();

            }
        });
    }

    public boolean isResize219() {
        return isResize219;
    }

    public void setResize219(boolean resize219) {
        isResize219 = resize219;
    }

    public boolean isLoadedSubtabHomeMovie() {
        return loadedSubtabHomeMovie;
    }

    public void setLoadedSubtabHomeMovie(boolean loadedSubtabHomeMovie) {
        this.loadedSubtabHomeMovie = loadedSubtabHomeMovie;
    }

    public int getTotalVideosViewed() {
        return totalVideosViewed;
    }

    public void setTotalVideosViewed() {
        this.totalVideosViewed++;
    }

    public boolean isLoadedAdsMainTab() {
        return loadedAdsMainTab;
    }

    public void setLoadedAdsMainTab(boolean loadedAdsMainTab) {
        this.loadedAdsMainTab = loadedAdsMainTab;
    }

    public boolean isShowedAdsMainTab() {
        return showedAdsMainTab;
    }

    public void setShowedAdsMainTab(boolean showedAdsMainTab) {
        this.showedAdsMainTab = showedAdsMainTab;
    }

    public RoundedCornersTransformation getRoundedCornersTransformation() {
        return roundedCornersTransformation;
    }

    public String getConfigResolutionVideo() {
        if (configResolutionVideo == null) configResolutionVideo = "";
        return configResolutionVideo;
    }

    public void setConfigResolutionVideo(String configResolutionVideo) {
        this.configResolutionVideo = configResolutionVideo;
    }

    public int getGameLiveNumberAnswerCorrect() {
        return gameLiveNumberAnswerCorrect;
    }

    public void setGameLiveNumberAnswerCorrect(int gameLiveNumberAnswerCorrect) {
        this.gameLiveNumberAnswerCorrect = gameLiveNumberAnswerCorrect;
    }

    public static class EmptyStringToNumberTypeAdapter extends TypeAdapter<Number> {
        @Override
        public void write(JsonWriter jsonWriter, Number number) throws IOException {
            if (number == null) {
                jsonWriter.nullValue();
                return;
            }
            jsonWriter.value(number);
        }

        @Override
        public Number read(JsonReader jsonReader) throws IOException {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }

            try {
                String value = jsonReader.nextString();
                if ("".equals(value)) {
                    return 0;
                }
                return NumberUtils.createNumber(value);
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }
    }

    private AccountRankDTO accountRankDTO;

    public AccountRankDTO getAccountRankDTO() {
        return accountRankDTO;
    }

    public void setAccountRankDTO(AccountRankDTO accountRankDTO) {
        this.accountRankDTO = accountRankDTO;
    }

    private List<RankDefine> listRank;

    public List<RankDefine> getListRank() {
        return listRank;
    }

    public void setListRank(List<RankDefine> listRank) {
        this.listRank = listRank;
    }

    private List<KHAccountPoint> listPoint;

    public List<KHAccountPoint> getListPoint() {
        return listPoint;
    }

    public void setListPoint(List<KHAccountPoint> listPoint) {
        this.listPoint = listPoint;
    }


    public FragmentTabHomeKh.LoginVia isLogin() {
//        String mochaToken = ApplicationController.self().getReengAccountBusiness().getToken();
        String openIDToken = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_ACCESS_TOKEN, String.class);
//        if (mochaToken == null) mochaToken = "";
        if (openIDToken == null) openIDToken = "";

        Log.e("TND", "TK openIDToken: " + openIDToken);
        if (openIDToken.isEmpty()) {
            return FragmentTabHomeKh.LoginVia.NOT;
        }
        return FragmentTabHomeKh.LoginVia.OPEN_ID;
    }

    private void initEsport() {
        Utils.init(this);
        CrashUtils.init();
        com.metfone.esport.util.Log.setEnableLog(BuildConfig.DEBUG);
        FirebasePerformance.getInstance().setPerformanceCollectionEnabled(true);
        BaseUtils.init(this);
//        try {
//            //todo init thu vien trimvideo
//            FFmpeg.getInstance(this).loadBinary(new FFmpegLoadBinaryResponseHandler() {
//                @Override
//                public void onStart() {
//                    com.metfone.esport.util.Log.d(TAG, "FFmpeg onStart");
//                }
//
//                @Override
//                public void onFinish() {
//                    com.metfone.esport.util.Log.d(TAG, "FFmpeg onFinish");
//                }
//
//                @Override
//                public void onFailure() {
//                    com.metfone.esport.util.Log.d(TAG, "FFmpeg onFailure");
//                }
//
//                @Override
//                public void onSuccess() {
//                    com.metfone.esport.util.Log.d(TAG, "FFmpeg onSuccess");
//                }
//
//            });
//        } catch (FFmpegNotSupportedException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        DeviceUtils.getDeviceInfoToLog();
    }

}

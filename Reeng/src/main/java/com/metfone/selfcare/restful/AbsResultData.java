/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.restful;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author gianglv3
 */
public class AbsResultData implements Serializable {

    @SerializedName("error")
    private ErrorMessage error;

    @SerializedName("code")
    private int code;

    public void setError(ErrorMessage error) {
        this.error = error;
    }

    public ErrorMessage getError() {
        return error;
    }

    public int getCode() {
        return code;
    }

    public boolean isWrongToken() {
        if (error != null) {

            return error.isWrongtoken();

        }
        return false;
    }
}
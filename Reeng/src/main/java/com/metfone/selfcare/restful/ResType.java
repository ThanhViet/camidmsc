package com.metfone.selfcare.restful;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ResType implements Serializable {
    @SerializedName("groups")
    @Expose
    private ArrayList<Groups> groups;

    public ResType() {
    }

    public void setGroups(ArrayList<Groups> groups) {
        this.groups = groups;
    }

    public ArrayList<Groups> getGroups() {
        return groups;
    }
}

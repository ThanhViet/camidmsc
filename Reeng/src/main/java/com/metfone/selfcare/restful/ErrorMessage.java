package com.metfone.selfcare.restful;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ErrorMessage implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private int VALID_TOKEN=400;
    public enum CODE {
        
        EXCEPTION(1),
    
        //nhom connection
        DB_CONNECT_TIME_OUT_EXCEPTION(100),

        //Nhom xu ly du lieu
        DATA_NULL_EXCEPTION(200),

        //Nhom loi request
        REQUEST_PARAM_EXCEPTION(300),
        REQUEST_TOO_MANY_EXCEPTION(301),
        
        //Nhom loi xac thuc
        APPLICATION_TOKEN_INVALID(400);
        
        private final int id;
        CODE(int id) {
            this.id = id;
        }

        public int getValue() {
            return id;
        }
    }
    
    @SerializedName("message")
    protected String message;
    
    @SerializedName("code")
    protected int code;

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
    
    public boolean isWrongtoken() {

        return code == VALID_TOKEN;
    }

    @Override
    public String toString() {
        return "ErrorMessage{[message = " + message + ", code=" + code + "}";
    }
    
    
    
}

package com.metfone.selfcare.restful;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Grouped implements Serializable {
    @SerializedName("type")
    @Expose
    private ResType type;

    public Grouped() {
    }

    public void setType(ResType type) {
        this.type = type;
    }

    public ResType getType() {
        return type;
    }
}

package com.metfone.selfcare.restful;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.SearchQuery;

import java.io.Serializable;
import java.util.ArrayList;

public class DocList implements Serializable {
    @SerializedName("docs")
    @Expose
    private ArrayList<SearchQuery> resSearchQuerys;

    public DocList() {
    }

    public void setResSearchQuerys(ArrayList<SearchQuery> resSearchQuerys) {
        this.resSearchQuerys = resSearchQuerys;
    }

    public ArrayList<SearchQuery> getResSearchQuerys() {
        return resSearchQuerys;
    }
}

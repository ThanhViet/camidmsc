package com.metfone.selfcare.restful;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;


public class ResfulSearchQueryObj implements Serializable {
    private static final long serialVersionUID = -43590256702591184L;

    @SerializedName("grouped")
    @Expose
    private Grouped grouped;

    public ResfulSearchQueryObj() {
    }

    public void setGrouped(Grouped grouped) {
        this.grouped = grouped;
    }

    public Grouped getGrouped() {
        return grouped;
    }
}
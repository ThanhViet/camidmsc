package com.metfone.selfcare.restful;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.OfficalAccountOnMedia;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedAction;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedContent;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.database.model.onmedia.RestAllNotifyModel;
import com.metfone.selfcare.database.model.onmedia.RestNewsHot;
import com.metfone.selfcare.database.model.onmedia.RestNewsHotDetail;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WSOnMedia extends BaseApi {

    public static final int NUMBER_PER_PAGE = 20;
    public static final String TAG_FEED_ONMEDIA = "TAG_FEED_ONMEDIA";
    public static final String TAG_FEED_COMMENT = "TAG_FEED_COMMENT";
    public static final String TAG_COMMENT = "TAG_COMMENT";
    public static final String TAG_GET_METADATA = "TAG_GET_METADATA";
    //    public static final String TAG_GET_TYPE = "TAG_GET_TYPE";
//    public static final String TAG_GET_SITE = "TAG_GET_SITE";
//    public static final String TAG_SET_SITE = "TAG_SET_SITE";
//    public static final String TAG_SET_TYPE = "TAG_SET_TYPE";
    public static final String TAG_GET_LIST_NOTIFY = "TAG_GET_LIST_NOTIFY";
    public static final String TAG_LOG_CLICK_LINK = "TAG_LOG_CLICK_LINK";
    public static final String TAG_GET_METADATA_WITH_ACTION = "TAG_GET_METADATA_WITH_ACTION";
    public static final String TAG_GET_LIKE_TITLE = "TAG_GET_LIKE_TITLE";
    public static final String TAG_FOLLOW_OFFICIAL = "TAG_FOLLOW_OFFICIAL";
    public static final String TAG_GET_PROFILE_OFFICIAL = "TAG_GET_PROFILE_OFFICIAL";
    //    public static final String TAG_LIKE_ALBUM_IMAGE = "TAG_LIKE_ALBUM_IMAGE";
//    public static final String TAG_GET_LIKE_ALBUM_IMAGE = "TAG_GET_LIKE_ALBUM_IMAGE";
    public static final String TAG_GET_CONTENT_DISCOVERY = "TAG_GET_CONTENT_DISCOVERY";
    public static final String TAG_UPLOAD_ALBUM_TO_ONMEDIA = "TAG_UPLOAD_ALBUM_TO_ONMEDIA";
    public static final String TAG_GET_IMAGE_DETAIL = "TAG_GET_IMAGE_DETAIL";
    //    public static final String TAG_GET_LIST_VIDEO_SIEUHAI = "TAG_GET_LIST_VIDEO_SIEUHAI";
    public static final String TAG_GET_LIST_SUGGEST_FRIEND = "TAG_GET_LIST_SUGGEST_FRIEND";
    //    public static final String TAG_GET_NUMBER_NOTIFY = "TAG_GET_NUMBER_NOTIFY";
    public static final String TAG_NEWS_HOT = "TAG_NEWS_HOT";
    public static final String TAG_NEWS_HOT_DETAIL = "TAG_NEWS_HOT_DETAIL";
    private static final String TAG = WSOnMedia.class.getSimpleName();
    private static final String TAG_LOG_VIEW_SIEUHAI = "TAG_LOG_VIEW_SIEUHAI";

    private String domain;

    public WSOnMedia(ApplicationController app) {
        super(app);
        domain = getDomainOnMedia();
    }

    //todo home time limne
    public void getFeedOnMedia(String lastRowId, int type
            , final OnMediaInterfaceListener.GetListFeedListener listener, final ErrorListener error) {
        final long startTime = System.currentTimeMillis();

        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_HOME_TIMELINE_V6);
        final String timeStamp = String.valueOf(startTime);
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(lastRowId)
                .append(NUMBER_PER_PAGE)
                .append(type)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_HOME_TIMELINE_V6);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("num", NUMBER_PER_PAGE);
        params.addParam("type", type);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));
        params.addParam("row_start", lastRowId);
        params.addParam("clientType", "Android");
        params.addParam("revApp", Config.REVISION);

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {

            @Override
            public void onResponse(String s) {
                long endTime = System.currentTimeMillis();
                String time = (endTime - startTime) + "";

                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    RestAllFeedsModel restAllFeedsModel = new Gson().fromJson(decryptData, RestAllFeedsModel.class);
                    listener.onGetListFeedDone(restAllFeedsModel);

                    LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_FEED, time,
                            timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                } else {
                    error.onErrorResponse(new ParseError());

                    LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_FEED, time,
                            timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                }
            }
        }, error);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_FEED_ONMEDIA, false);
    }

    //todo user timeline
    public void getUserTimeLine(String msisdn, String lastRowId
            , final OnMediaInterfaceListener.GetListFeedListener listener, final ErrorListener error) {
        final long startTime = System.currentTimeMillis();

        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_USER_TIMELINE_V3);
        final String timeStamp = String.valueOf(startTime);
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(msisdn).append(lastRowId)
                .append(NUMBER_PER_PAGE)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_USER_TIMELINE_V3);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("friend", msisdn);
        params.addParam("num", NUMBER_PER_PAGE);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));
        params.addParam("row_start", lastRowId);
        params.addParam("clientType", "Android");
        params.addParam("revApp", Config.REVISION);

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    long endTime = System.currentTimeMillis();
                    String time = (endTime - startTime) + "";

                    String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                    if (!TextUtils.isEmpty(decryptData)) {
                        RestAllFeedsModel restAllFeedsModel = new Gson().fromJson(decryptData, RestAllFeedsModel.class);
                        listener.onGetListFeedDone(restAllFeedsModel);

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_USER_TIMELINE,
                                time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                    } else {
                        error.onErrorResponse(new ParseError());

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_USER_TIMELINE,
                                time, timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                    }
                } catch (RuntimeException ex) {
                    Log.e(TAG, "Exception", ex);
                }
            }
        }, error);
        Log.i(TAG, "getUserTimeLine: " + params.toString());
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_FEED_ONMEDIA, false);
    }

    public void logAppV6(final String urlKey,//url ngoai
                         final String urlSubComment,//url cua comment, trong cmt cap 2 va like comment, like cmt cap 2
                         final FeedContent feedContent,//cua feed ngoai
                         final FeedModelOnMedia.ActionLogApp actionType,
                         final String commentMsg,
                         final String rowID,//rowid feed ngoai, like cai nao truyen rowid cua cai do
                         final String listTag,
                         final FeedModelOnMedia.ActionFrom postFrom,
                         Listener<String> response,
                         ErrorListener error) {
        if (TextUtils.isEmpty(urlKey)) {
            return;
        }
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_ACTION_APP_V6);

        String textPostFrom = "";
        if (postFrom != null) {
            textPostFrom = postFrom.name();
        }
        String contentString = "";
        if (feedContent != null) {
            contentString = gson.toJson(feedContent);
        }
        final String content = contentString;
        final String textPostFromFinal = textPostFrom;
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        final String userInfo = makeUserInfo(myAccount);
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(content)
                .append(urlKey)
                .append(actionType.name())
                .append(textPostFromFinal)
                .append(commentMsg)
                .append(rowID)
                .append(listTag)
                .append(userInfo)
                .append(urlSubComment)
                .append(myAccount.getToken())
                .append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, this.domain + Url.OnMedia.API_ONMEDIA_ACTION_APP_V6, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("content", content);
                params.put("url", urlKey);
                params.put("action_type", actionType.name());
                params.put("post_action_from", textPostFromFinal);
                params.put("status", commentMsg);
                params.put("row_id", rowID);
                params.put("tags", listTag);
                params.put("userinfo", userInfo);
                params.put("timestamp", timeStamp);
                params.put("url_sub_comment", urlSubComment);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_FEED_ONMEDIA, false);
    }

    private String makeUserInfo(ReengAccount myAccount) {
        try {
            JSONObject object = new JSONObject();
            object.put("msisdn", myAccount.getJidNumber());
            object.put("name", myAccount.getName());
            object.put("avatar", myAccount.getLastChangeAvatar());
            object.put("isMochaUser", 1);
            object.put("user_type", 0);
            object.put("revision", Config.REVISION);
            object.put("client_type", Constants.HTTP.CLIENT_TYPE_STRING);
            return object.toString();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return "";
        }
    }

    //todo get list comment
    public void getListComment(String urlKey, String lastRowId, final OnMediaInterfaceListener.GetListComment listener
            , String tag, final boolean isReplyComment) {
        final long startTime = System.currentTimeMillis();
        if (TextUtils.isEmpty(urlKey)) {
            return;
        }
//        final String urlAction = TextHelper.replaceUrl(urlKey);
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null || listener == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_COMMENT_V2);
        final String timeStamp = String.valueOf(startTime);
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(urlKey)
                .append(lastRowId)
                .append(NUMBER_PER_PAGE)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_COMMENT_V2);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("url", urlKey);
        params.addParam("num", NUMBER_PER_PAGE);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));
        params.addParam("row_start", lastRowId);

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                long endTime = System.currentTimeMillis();
                String time = (endTime - startTime) + "";

                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    parseResponseListComment(decryptData, listener, isReplyComment);

                    LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_COMMENT,
                            time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                } else {
                    listener.onGetListCommentError(-1, "");

                    LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_COMMENT,
                            time, timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                }
            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                long endTime = System.currentTimeMillis();
                String time = (endTime - startTime) + "";

                listener.onGetListCommentError(-1, "");

                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_COMMENT, time,
                        timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }
        });
        String tagCmt = (TextUtils.isEmpty(tag)) ? TAG_COMMENT : tag;
        VolleyHelper.getInstance(application).addRequestToQueue(req, tagCmt, false);
    }

    public void getListComment(String urlKey, String lastRowId,int limit, final OnMediaInterfaceListener.GetListComment listener
            , String tag, final boolean isReplyComment) {
        final long startTime = System.currentTimeMillis();
        if (TextUtils.isEmpty(urlKey)) {
            return;
        }
//        final String urlAction = TextHelper.replaceUrl(urlKey);
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null || listener == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_COMMENT_V2);
        final String timeStamp = String.valueOf(startTime);
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(urlKey)
                .append(lastRowId)
                .append(limit)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_COMMENT_V2);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("url", urlKey);
        params.addParam("num", limit);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));
        params.addParam("row_start", lastRowId);

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                long endTime = System.currentTimeMillis();
                String time = (endTime - startTime) + "";

                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    parseResponseListComment(decryptData, listener, isReplyComment);

                    LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_COMMENT,
                            time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                } else {
                    listener.onGetListCommentError(-1, "");

                    LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_COMMENT,
                            time, timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                }
            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                long endTime = System.currentTimeMillis();
                String time = (endTime - startTime) + "";

                listener.onGetListCommentError(-1, "");

                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.ONMEDIA_GET_COMMENT, time,
                        timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }
        });
        String tagCmt = (TextUtils.isEmpty(tag)) ? TAG_COMMENT : tag;
        VolleyHelper.getInstance(application).addRequestToQueue(req, tagCmt, false);
    }

    private void parseResponseListComment(String s, OnMediaInterfaceListener.GetListComment listener, boolean isReplyComment) {
        try {
            ArrayList<UserInfo> listUserLike = new ArrayList<>();
            ArrayList<FeedAction> listComment = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(s);
            int code = jsonObject.optInt("code");
            if (code == 200) {
                JSONArray jsonArrayCmt = jsonObject.optJSONArray("listComment");
                JSONArray jsonArrayLike = jsonObject.optJSONArray("userLike");
                for (int i = 0; i < jsonArrayLike.length(); i++) {
                    UserInfo userInfo;
                    if (jsonArrayLike.optJSONObject(i) != null) {
                        String userLike = jsonArrayLike.optJSONObject(i).toString();
                        userInfo = new Gson().fromJson(userLike, UserInfo.class);
                    } else {
                        userInfo = new UserInfo();
                    }
                    listUserLike.add(userInfo);
                }

                for (int i = 0; i < jsonArrayCmt.length(); i++) {
                    JSONObject js = jsonArrayCmt.optJSONObject(i);
                    getFeedActionFromJson(listComment, js, isReplyComment);
                }
                long timeSv = jsonObject.optLong("timeserver");
                FeedAction commentParent = null;
                if (isReplyComment) {
                    JSONObject jsCmtParent = jsonObject.optJSONObject("conmentParent");
                    commentParent = parseFeedAction(jsCmtParent, false);
                }
                listener.onGetListCommentSuccess(listComment, listUserLike, commentParent, timeSv);
            } else {
                listener.onGetListCommentError(-1, "");
            }

        } catch (Exception e) {
            Log.e(TAG, e);
            listener.onGetListCommentError(-1, "");
        }
    }

    private void getFeedActionFromJson(ArrayList<FeedAction> listComment, JSONObject js, boolean isReplyCmt) {

        FeedAction feedAction = parseFeedAction(js, isReplyCmt);
        listComment.add(feedAction);
        if (!isReplyCmt) {
            JSONArray jaSubCmt = js.optJSONArray("lstSubComment");
            if (jaSubCmt != null && jaSubCmt.length() > 0) {
                getFeedActionFromJson(listComment, jaSubCmt.optJSONObject(0), true);
            }
        }

    }

    private FeedAction parseFeedAction(JSONObject js, boolean isReplyCmt) {
        FeedAction feedAction = new FeedAction();
        String base64RowID = js.optString("base64RowID");
        long stamp = js.optLong("stamp");
        long currentTime = js.optLong("current_time");
        JSONObject jsUser = js.optJSONObject("userInfo");
        UserInfo us;
        if (jsUser != null) {
            us = new Gson().fromJson(jsUser.toString(), UserInfo.class);
        } else {
            us = new UserInfo();
        }
        String status = js.optString("status");
        JSONArray jaTag = js.optJSONArray("tags");
        ArrayList<TagMocha> listTag = new ArrayList<>();
        if (jaTag != null) {
            listTag = new Gson().fromJson(jaTag.toString(),
                    new TypeToken<ArrayList<TagMocha>>() {
                    }.getType());
            if (listTag != null && !listTag.isEmpty()) {
                for (TagMocha tag : listTag) {
                    String msisdn = tag.getMsisdn();
                    if (msisdn.equals(application.getReengAccountBusiness().getJidNumber())) {
                        tag.setContactName(application.getReengAccountBusiness().getUserName());
                    } else {
                        PhoneNumber phoneNumber = application.getContactBusiness().getPhoneNumberFromNumber(msisdn);
                        if (phoneNumber != null) {
                            tag.setContactName(phoneNumber.getName());
                        } else {
                            if (!TextUtils.isEmpty(tag.getName())) {
                                tag.setContactName(tag.getName());
                            } else if (!TextUtils.isEmpty(tag.getMsisdn())) {
                                tag.setContactName(Utilities.hidenPhoneNumber(tag.getMsisdn()));
                            } else {
                                tag.setContactName("***");
                            }
                        }
                    }
                }
            }
        }

        int numberCmt = js.optInt("number_comment");
        int numberLike = js.optInt("number_like");
        int isLike = js.optInt("is_like");

        feedAction.setBase64RowId(base64RowID);
        feedAction.setTimeStamp(stamp);
        feedAction.setTimeServer(currentTime);
        feedAction.setUserInfo(us);
        feedAction.setComment(status);
        feedAction.setListTag(listTag);
        feedAction.setNumberCmt(numberCmt);
        feedAction.setReplyCmt(isReplyCmt);
        feedAction.setNumberLike(numberLike);
        feedAction.setIsLike(isLike);

        return feedAction;
    }

    public void getLikeOrShare(String urlKey, int page, final OnMediaInterfaceListener.GetListFeedActionListener listener
            , final ErrorListener error, boolean isGetLike) {
        if (TextUtils.isEmpty(urlKey)) {
            return;
        }
//        final String urlAction = TextHelper.replaceUrl(urlKey);
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        String url;
        if (isGetLike) {
            //url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_LIKE_V2);
            url = this.domain + Url.OnMedia.API_ONMEDIA_GET_LIKE_V2;
        } else {
            //url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_SHARE_V2);
            url = this.domain + Url.OnMedia.API_ONMEDIA_GET_SHARE_V2;
        }
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + url + limit + offset + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(urlKey)
                .append(NUMBER_PER_PAGE)
                .append(page)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("url", urlKey);
        params.addParam("limit", NUMBER_PER_PAGE);
        params.addParam("offset", page);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    RestAllFeedAction restAllFeedAction = new Gson().fromJson(decryptData, RestAllFeedAction.class);
                    listener.onGetListFeedAction(restAllFeedAction);
                } else {
                    error.onErrorResponse(new ParseError());
                }
            }
        }, error);
        Log.i(TAG, "getLikeOrShare: " + params.toString());
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_COMMENT, false);
    }

    public void getMetaData(String urlKey, Listener<String> response, ErrorListener error, boolean needCancel) {
        if (TextUtils.isEmpty(urlKey)) {
            return;
        }
        final String urlAction = TextHelper.replaceUrl(urlKey);
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        if (needCancel) {
            VolleyHelper.getInstance(application).cancelPendingRequests(TAG_GET_METADATA);
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_METADATA);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + url + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(urlAction)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_METADATA);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("url", urlAction);
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", "Android");
        params.addParam("revision", Config.REVISION);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), response, error);
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_GET_METADATA, false);
    }

    public void reportViolation(FeedModelOnMedia feed, Listener<String> response, ErrorListener error) {
        if (feed == null) {
            return;
        }
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_REPORT_VIOLATION);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        final String feedString = new Gson().toJson(feed);
        //msisdn + feed + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(feedString)
                .append(myAccount.getToken())
                .append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, this.domain + Url.OnMedia.API_ONMEDIA_REPORT_VIOLATION, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("feed", feedString);
                params.put("timestamp", timeStamp);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG, false);
    }

    public void unFollowFriend(final String msisdn, Listener<String> response, ErrorListener error) {
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_UNFOLLOW);

        final String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + unfollower + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(msisdn)
                .append(myAccount.getToken())
                .append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, this.domain + Url.OnMedia.API_ONMEDIA_UNFOLLOW, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("unfollower", msisdn);
                params.put("timestamp", timeStamp);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG, false);
    }

    public void getListNotify(String lastRowId, final OnMediaInterfaceListener.GetListNotifyListener listener
            , final ErrorListener error) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(application)) {
            error.onErrorResponse(new NoConnectionError());
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_NOTIFY_V1);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + row_start + num + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(lastRowId).append(NUMBER_PER_PAGE)
                .append(application.getReengAccountBusiness().getCurrentLanguage())
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_NOTIFY_V1);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("num", NUMBER_PER_PAGE);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));
        params.addParam("timestamp", timeStamp);
        params.addParam("row_start", lastRowId);
        params.addParam("clientType", "Android");
        params.addParam("revApp", Config.REVISION);
        params.addParam("languageCode", application.getReengAccountBusiness().getCurrentLanguage());

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    RestAllNotifyModel restAllNotifyModel = new Gson().fromJson(decryptData, RestAllNotifyModel.class);
                    listener.onGetListNotifyDone(restAllNotifyModel);
                } else {
                    error.onErrorResponse(new ParseError());
                }
            }
        }, error);
        req.setShouldCache(true);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_GET_LIST_NOTIFY, false);
    }

    public void resetNotify(final String rowId, final String typeReset,
                            Listener<String> response,
                            ErrorListener error) {
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_RESET_NOTIFY_V1);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + row_id + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(rowId).append(typeReset)
                .append(myAccount.getToken())
                .append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, this.domain + Url.OnMedia.API_ONMEDIA_RESET_NOTIFY_V1, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("row_id", rowId);
                params.put("timestamp", timeStamp);
                params.put("type", typeReset);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG, false);
    }

    public void getNumberNotify(Listener<String> response, ErrorListener error) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_NUMBER_NOTIFY);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_NUMBER_NOTIFY);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), response, error);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG, false);
    }

    public void getFeedNotify(final OnMediaInterfaceListener.GetListFeedListener listener
            , final ErrorListener error, String rowId) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(application)) {
            error.onErrorResponse(new NoConnectionError());
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_DETAIL_NOTIFY_V1);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(rowId)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_DETAIL_NOTIFY_V1);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("timestamp", timeStamp);
        params.addParam("row_id", rowId);
        params.addParam("clientType", "Android");
        params.addParam("revApp", Config.REVISION);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (BuildConfig.DEBUG) Log.d(TAG, "getFeedNotify decryptData: " + decryptData);
                if (!TextUtils.isEmpty(decryptData)) {
                    RestAllFeedsModel restAllFeedsModel = new Gson().fromJson(decryptData, RestAllFeedsModel.class);
                    listener.onGetListFeedDone(restAllFeedsModel);
                } else {
                    error.onErrorResponse(new ParseError());
                }
            }
        }, error);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG, false);
    }

    public void getDetailUrl(final OnMediaInterfaceListener.GetListFeedListener listener
            , final ErrorListener error, String actionUrl, boolean fullData) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(application)) {
            if (error != null) error.onErrorResponse(new NoConnectionError());
            return;
        }
        new BaseApi(application).getDetailUrl(actionUrl, fullData, new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                RestAllFeedsModel model = new Gson().fromJson(data, RestAllFeedsModel.class);
                if (listener != null) listener.onGetListFeedDone(model);
            }

            @Override
            public void onFailure(String message) {
                if (error != null) error.onErrorResponse(new ParseError());
            }
        });
    }

    public void logClickLink(final String urlKey, final String type , String campId ,Listener<String> response, ErrorListener error) {
        if (TextUtils.isEmpty(urlKey)) {
            return;
        }
        final String urlWebview = TextHelper.replaceUrl(urlKey);
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        String domain = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_LOG_ACTION_MEDIA);
        String url;
        if (!TextUtils.isEmpty(domain) && !"-".equals(domain)) {
            url = domain + UrlConfigHelper.getInstance(application).getUrlByKey(Config.UrlEnum.ONMEDIA_LOG_CLICK_LINK);
        } else {
            //url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_LOG_CLICK_LINK);
            url = this.domain + Url.OnMedia.API_ONMEDIA_LOG_CLICK_LINK;
        }

        Log.i(TAG, "logClickLink: " + url+" campId "+ campId +" urlKey "+urlWebview);
        if(!TextUtils.isEmpty(campId)){
            AppStateProvider.getInstance().setCampId("");
        }

        final String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + urlWeb + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(urlWebview)
                .append(type)
                .append(myAccount.getToken())
                .append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, url, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("url", urlWebview);
                params.put("type", type);
                params.put("camp_id", campId);
                params.put("timestamp", timeStamp);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_LOG_CLICK_LINK, false);
    }

    public void getMetaDataWithAction(String urlKey, FeedModelOnMedia.ActionLogApp actionType
            , Listener<String> response, ErrorListener error) {
        if (TextUtils.isEmpty(urlKey)) {
            return;
        }
        final String urlAction = TextHelper.replaceUrl(urlKey);
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_GET_METADATA_WITH_ACTION);
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.GET_METADATA_WITH_ACTION);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + url + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(urlAction)
                .append(actionType.toString())
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_GET_METADATA_WITH_ACTION);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("url", urlAction);
        params.addParam("timestamp", timeStamp);
        params.addParam("action_type", actionType);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), response, error);
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_GET_METADATA_WITH_ACTION, false);
    }

    public void getLikeTitle(final String urlAction
            , final OnMediaInterfaceListener.GetListFeedActionListener listener, final ErrorListener error) {
        if (TextUtils.isEmpty(urlAction)) {
            return;
        }
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_GET_LIKE_TITLE);
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.GET_LIKE_TITLE_V2);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + url + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(urlAction)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_GET_LIKE_TITLE_V2);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("url", urlAction);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    RestAllFeedAction restAllFeedAction = new Gson().fromJson(decryptData, RestAllFeedAction.class);
                    listener.onGetListFeedAction(restAllFeedAction);
                } else {
                    error.onErrorResponse(new ParseError());
                }
            }
        }, error);
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_GET_LIKE_TITLE, false);
    }

    public void reduceTotalNotify(final String feedId, final String action
            , Listener<String> response, ErrorListener error) {
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.REDUCE_TOTAL_NOTIFY);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber()).append(feedId).append(action)
                .append(myAccount.getToken()).append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, this.domain + Url.OnMedia.API_REDUCE_TOTAL_NOTIFY, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("feedId", feedId);
                params.put("action_type", action);
                params.put("timestamp", timeStamp);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG, false);
    }

    public void followOfficial(final String officialId, final int status
            , Listener<String> response, ErrorListener error) {
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_FOLLOW_OFFICIAL);

        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(officialId)
                .append(status)
                .append(myAccount.getToken())
                .append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, this.domain + Url.OnMedia.API_ONMEDIA_FOLLOW_OFFICIAL, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("officalid", officialId);
                params.put("status", String.valueOf(status));
                params.put("timestamp", timeStamp);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_FOLLOW_OFFICIAL, false);
    }

    public void unfollowOfficialAndDelete(final String officialId, final String rowId
            , Listener<String> response, ErrorListener error) {
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_UNFOLLOW_OFFICIAL_TIMELINE);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(officialId)
                .append(rowId)
                .append(myAccount.getToken())
                .append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, this.domain + Url.OnMedia.API_ONMEDIA_UNFOLLOW_OFFICIAL_TIMELINE, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("officalid", officialId);
                params.put("row_id", String.valueOf(rowId));
                params.put("timestamp", timeStamp);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_FOLLOW_OFFICIAL, false);
    }

    public void getProfileOfficial(final String officialId, final String officalName, final String avatarUrl
            , final ContactRequestHelper.onResponseOfficialOnMediaInfoListener listener) {
        if (TextUtils.isEmpty(officialId) || listener == null) {
            return;
        }

        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_GET_PROFILE_OFFICIAL);
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_PROFILE_OFFICIAL);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + url + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(officialId)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_PROFILE_OFFICIAL);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("officalid", officialId);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String response) {
                int errorCode = -1;
                try {
                    JSONObject responseObject = new JSONObject(response);
                    if (responseObject.has(Constants.HTTP.REST_CODE)) {
                        errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                    }
                    if (errorCode == HTTPCode.E200_OK) {
                        OfficalAccountOnMedia official = new OfficalAccountOnMedia(officalName, officialId);
                        official.setUrlAvatar(avatarUrl);
                        if (responseObject.has(Constants.HTTP.CONTACT.ALBUMS)) {
                            official.setAlbumString(responseObject.getString(Constants.HTTP.CONTACT.ALBUMS));
                        }
                        if (responseObject.has(Constants.HTTP.CONTACT.COVER_IMAGE)) {
                            official.setUrlCover(responseObject.getString(Constants.HTTP.CONTACT.COVER_IMAGE));
                        }
                        if (responseObject.has(Constants.HTTP.CONTACT.FOLLOWER)) {
                            official.setFollower(responseObject.getLong(Constants.HTTP.CONTACT.FOLLOWER));
                        }
                        if (responseObject.has(Constants.HTTP.CONTACT.STATUS)) {
                            official.setStatus(responseObject.getString(Constants.HTTP.CONTACT.STATUS));
                        }
                        if (responseObject.has(Constants.HTTP.CONTACT.STATE_FOLLOWER)) {
                            int stateFollow = responseObject.getInt(Constants.HTTP.CONTACT.STATE_FOLLOWER);
                            if (stateFollow == OfficalAccountOnMedia.STATE_FOLLOW) {
                                official.setUserStateFollow(true);
                            } else {
                                official.setUserStateFollow(false);
                            }
                        }
                        if (responseObject.has(Constants.HTTP.CONTACT.CHAT_DETAIL)) {
                            JSONObject jsonChat = responseObject.getJSONObject(Constants.HTTP.CONTACT.CHAT_DETAIL);
                            if (jsonChat.has(Constants.ONMEDIA.OFFICIAL_CHAT_ID)
                                    && jsonChat.has(Constants.ONMEDIA.OFFICIAL_CHAT_NAME)
                                    && jsonChat.has(Constants.ONMEDIA.OFFICIAL_CHAT_AVATAR)) {
                                String id = jsonChat.getString(Constants.ONMEDIA.OFFICIAL_CHAT_ID);
                                String name = jsonChat.getString(Constants.ONMEDIA.OFFICIAL_CHAT_NAME);
                                String avatar = jsonChat.getString(Constants.ONMEDIA.OFFICIAL_CHAT_AVATAR);
                                official.setOfChatInfo(id, name, avatar);
                            }
                        }
                        if (responseObject.has(Constants.HTTP.CONTACT.IS_COVER_DEFAULT)) {
                            official.setIsCoverDefault(responseObject.getInt(Constants.HTTP.CONTACT.IS_COVER_DEFAULT));
                        }

                        listener.onResponse(official);
                    } else {
                        listener.onError(errorCode);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e);
                    listener.onError(-1);
                }
            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_GET_PROFILE_OFFICIAL, false);
    }

    public void getActivitiesOfficial(String officialId, int page
            , final OnMediaInterfaceListener.GetListFeedListener listener, final ErrorListener error) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_ACTIVITIES_OFFICIAL_V2);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(officialId)
                .append(page)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_ACTIVITIES_OFFICIAL_V2);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("officalid", officialId);
        params.addParam("page", page);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));
        params.addParam("clientType", "Android");
        params.addParam("revApp", Config.REVISION);

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    RestAllFeedsModel restAllFeedsModel = new Gson().fromJson(decryptData, RestAllFeedsModel.class);
                    listener.onGetListFeedDone(restAllFeedsModel);
                } else {
                    error.onErrorResponse(new ParseError());
                }
            }
        }, error);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_FEED_ONMEDIA, false);
    }

    //page start from 1
    public void getContentDiscovery(Listener<String> response, ErrorListener error, int page) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_GET_CONTENT_DISCOVERY);
        String url = UrlConfigHelper.getInstance(application).getUrlConfigOfFile(Config.UrlEnum.GET_LIST_VIDEO_DISCOVERY);
        ResfulString params = new ResfulString(url);
        params.addParam("page", page);

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), response, error);
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_GET_CONTENT_DISCOVERY, false);
    }

    public void uploadAlbumToOnMedia(ArrayList<String> listImage, final String typeImage
            , Listener<String> response, ErrorListener error) {
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_UPLOAD_ALBUM_TO_ONMEDIA);
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_UPLOAD_ALBUM);
        final String listImageString = gson.toJson(listImage);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(listImageString)
                .append(typeImage)
                .append(myAccount.getToken())
                .append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken());

        StringRequest request = new StringRequest(Request.Method.POST, this.domain + Url.OnMedia.API_ONMEDIA_UPLOAD_ALBUM, response, error) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("listImg", listImageString);
                params.put("timestamp", timeStamp);
                params.put("type", typeImage);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_UPLOAD_ALBUM_TO_ONMEDIA, false);
    }

    public void getImageDetail(String idImage, String jidFriend, String itemType
            , final OnMediaInterfaceListener.GetListFeedContentListener listener, final ErrorListener error) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_GET_IMAGE_DETAIL);
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_IMAGE_DETAIL_V3);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(jidFriend)
                .append(idImage)
                .append(itemType)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_IMAGE_DETAIL_V3);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("friend", jidFriend);
        params.addParam("type", itemType);
        params.addParam("timestamp", timeStamp);
        params.addParam("url", idImage);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    RestAllFeedContent restAllFeedContent = new Gson().fromJson(decryptData, RestAllFeedContent.class);
                    listener.onGetListFeedContent(restAllFeedContent);
                } else {
                    error.onErrorResponse(new ParseError());
                }
            }
        }, error);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_GET_IMAGE_DETAIL, false);
    }

    public void getNewsHotList(int page, Listener<RestNewsHot> response, ErrorListener error) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_NEWS_HOT);
        ResfulString params = new ResfulString("http://api.tinngan.vn/Tinngan.svc/getNewsHomeApp/" + page);

        GsonRequest<RestNewsHot> req = new GsonRequest<>(Request.Method.GET, params.toString()
                , RestNewsHot.class, null, response, error);
        req.setHeader("user", "tinngan");
        req.setHeader("password",
                "191f1f632d69180e6228d26849d34d081a3b8d8aa9197eba0f70530ffe698ba80108bfb075c43e82081e245ccb63f6a39107327b2c1d053469bdf4f09bc1e820");
        req.setShouldCache(true);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_NEWS_HOT, false);
    }

    public void getNewsHotDetail(String pid, String cid, String contentId
            , Listener<RestNewsHotDetail> response, ErrorListener error) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_NEWS_HOT_DETAIL);
        ResfulString params = new ResfulString("http://api.tinngan.vn/Tinngan.svc/getContent/" + pid + "/" + cid + "/" + contentId);

        GsonRequest<RestNewsHotDetail> req = new GsonRequest<>(Request.Method.GET, params.toString()
                , RestNewsHotDetail.class, null, response, error);
        req.setHeader("user", "tinngan");
        req.setHeader("password",
                "191f1f632d69180e6228d26849d34d081a3b8d8aa9197eba0f70530ffe698ba80108bfb075c43e82081e245ccb63f6a39107327b2c1d053469bdf4f09bc1e820");
        req.setShouldCache(true);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_NEWS_HOT_DETAIL, false);
    }

    public void getNewsHotRelate(int pid, int cid, int contentId, Listener<RestNewsHot> response, ErrorListener error) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_NEWS_HOT);
        ResfulString params = new ResfulString("http://api.tinngan.vn/Tinngan.svc/getNewsRelates/" + pid + "/" + cid
                + "/" + contentId);

        GsonRequest<RestNewsHot> req = new GsonRequest<>(
                Request.Method.GET, params.toString(), RestNewsHot.class,
                null, response, error);
        req.setHeader("user", "tinngan");
        req.setHeader("password",
                "191f1f632d69180e6228d26849d34d081a3b8d8aa9197eba0f70530ffe698ba80108bfb075c43e82081e245ccb63f6a39107327b2c1d053469bdf4f09bc1e820");
        req.setShouldCache(true);
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_NEWS_HOT, false);
    }

    public void getListSuggestFriend(final OnMediaInterfaceListener.GetListSuggestFriend listener, int page) {
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_GET_LIST_SUGGEST_FRIEND);
        //String url = UrlConfigHelper.getInstance(application).getUrlConfigOfOnMedia(Config.UrlEnum.ONMEDIA_GET_LIST_SUGGEST_FRIEND_V2);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + token + timestamp
        String platform = "Android";
        String languageCode = application.getReengAccountBusiness().getDeviceLanguage();
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(platform)
                .append(Config.REVISION)
                .append(page)
                .append(languageCode)
                .append(myAccount.getGender())
                .append(myAccount.getBirthdayString())
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(this.domain + Url.OnMedia.API_ONMEDIA_GET_LIST_SUGGEST_FRIEND_V2);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("timestamp", timeStamp);
        params.addParam("platform", platform);
        params.addParam("page", page);
        params.addParam("languageCode", languageCode);
        params.addParam("revision", Config.REVISION);
        params.addParam("gender", myAccount.getGender());
        params.addParam("birthdayStr", myAccount.getBirthdayString());
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(),
                new Listener<String>() {

                    @Override
                    public void onResponse(String s) {
                        handleResponseGetListSuggestFriend(s, listener);
                    }
                },
                new ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (listener != null) {
                            listener.onError(-3, volleyError.toString());
                        }
                    }
                });
        VolleyHelper.getInstance(application).addRequestToQueue(req, TAG_GET_LIST_SUGGEST_FRIEND, false);
    }

    private void handleResponseGetListSuggestFriend(String s, OnMediaInterfaceListener.GetListSuggestFriend listener) {
        if (listener == null) return;
        try {
            JSONObject jsonObject = new JSONObject(s);
            int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
            if (code == HTTPCode.E200_OK) {
                ArrayList<UserInfo> listUser = new ArrayList<>();
                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (TextUtils.isEmpty(decryptData)) {
                    listener.onError(-2, "");
                    return;
                }
                JSONObject jsonDescrypt = new JSONObject(decryptData);
                String title = jsonDescrypt.optString("title");
                JSONArray jsonArray = jsonDescrypt.optJSONArray("suggestionList");
                if (jsonArray != null && jsonArray.length() != 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jso = jsonArray.getJSONObject(i);
                        UserInfo userInfo = new UserInfo();
                        userInfo.setAvatar(jso.optString("avatar"));
                        userInfo.setName(jso.optString("name"));
                        userInfo.setStateMocha(jso.optInt("isMochaUser"));
                        userInfo.setUser_type(jso.optInt("user_type"));
                        userInfo.setMsisdn(jso.optString("msisdn"));
                        userInfo.setStatus(jso.optString("status"));
                        listUser.add(userInfo);
                    }
                    listener.onGetListSuggestFriendDone(listUser, title);
                } else {
                    listener.onError(-2, "");
                }

            } else {
                listener.onError(-2, "");
            }
        } catch (Exception e) {
            listener.onError(-1, e.toString());
            Log.e(TAG, e);
        }
    }

    public void logViewSieuHai(String videoId, String mediaUrl, Constants.SieuHai.SieuHaiEnum enumSieuHai) {
        if (TextUtils.isEmpty(videoId)) {
            Log.e(TAG, "empty video id");
            return;
        }
        if (TextUtils.isEmpty(mediaUrl)) {
            Log.e(TAG, "empty mediaUrl");
            return;
        }
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        String url = UrlConfigHelper.getInstance(application).getUrlByKey(Config.UrlEnum.LOG_VIEW_SIEUHAI);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("username", "974802c5178ab63eba02d8035ad3f6ab");
        params.addParam("password", "974802c5178ab63eba02d8035ad3f6ab");
        params.addParam("videoid", videoId);
        params.addParam("logtype", enumSieuHai.name());
        params.addParam("desc", mediaUrl);
        params.addParam("clientype", "android");
        params.addParam("channel", "mocha");

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "onResponse logViewSieuHai: " + s);
            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse logViewSieuHai: " + volleyError.toString());
            }
        });
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_LOG_VIEW_SIEUHAI, false);
    }

    public void getFeedById(String rowId
            , final OnMediaInterfaceListener.GetListFeedListener listener, final ErrorListener error) {
        if (TextUtils.isEmpty(rowId)) {
            return;
        }
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_FEED_COMMENT);
        String url = UrlConfigHelper.getInstance(application).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_GET_TIMELINE_BY_FEED_ID;
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + url + token + timestamp
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber())
                .append(rowId)
                .append(myAccount.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("row_id", rowId);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()));

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), new Listener<String>() {
            @Override
            public void onResponse(String s) {
                String decryptData = HttpHelper.decryptResponse(s, application.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    RestAllFeedsModel restAllFeedsModel = new Gson().fromJson(decryptData, RestAllFeedsModel.class);
                    listener.onGetListFeedDone(restAllFeedsModel);
                } else {
                    error.onErrorResponse(new ParseError());
                }
            }
        }, error);
        VolleyHelper.getInstance(application).addRequestToQueue(request, TAG_FEED_COMMENT, false);
    }

    public void logClickTabWap(String idTab, HttpCallBack listener) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String loginStatus = "0";
        if (!getReengAccountBusiness().isAnonymousLogin())
            loginStatus = "1";
        //md5=msisdn + idTab + clientType + revision + countryCode + loginStatus + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + idTab
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + getReengAccountBusiness().getRegionCode()
                + loginStatus
                + getReengAccountBusiness().getToken()
                + timeStamp, getReengAccountBusiness().getToken());
        Http.Builder builder = post(getDomainOnMedia(), Url.OnMedia.API_TAB_WAP_LOG_CLICK);
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putParameter("idTab", idTab);
        builder.putParameter("loginStatus", loginStatus);
        builder.putParameter(UUID, Utilities.getUuidApp());
        builder.putParameter(Parameter.LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter(Parameter.COUNTRY_CODE, getReengAccountBusiness().getRegionCode());
        builder.putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter(Parameter.REVISION, Config.REVISION);
        builder.putParameter(Parameter.TIMESTAMP, timeStamp);
        builder.putParameter(Parameter.SECURITY, security);
        builder.setTag(Url.OnMedia.API_TAB_WAP_LOG_CLICK);
        builder.withCallBack(listener);
        builder.execute();
    }

    public void logActionApp(final String urlKey,//url ngoai
                             final String urlSubComment,//url cua comment, trong cmt cap 2 va like comment, like cmt cap 2
                             final FeedContent feedContent,//cua feed ngoai
                             final FeedModelOnMedia.ActionLogApp actionType,
                             final String commentMsg,
                             final String rowID,//rowid feed ngoai, like cai nao truyen rowid cua cai do
                             final String listTag,
                             final FeedModelOnMedia.ActionFrom postFrom,
                             ApiCallbackV2<String> apiCallback) {
        if (TextUtils.isEmpty(urlKey)) {
            return;
        }
        final ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        if (!application.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        String textPostFrom = "";
        if (postFrom != null) {
            textPostFrom = postFrom.name();
        }
        String contentString = "";
        if (feedContent != null) {
            contentString = gson.toJson(feedContent);
        }
        final String content = contentString;
        final String textPostFromFinal = textPostFrom;
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        final String userInfo = makeUserInfo(myAccount);
        StringBuilder sb = new StringBuilder();
        sb.append(getReengAccountBusiness().getJidNumber())
                .append(content)
                .append(urlKey)
                .append(actionType.name())
                .append(textPostFromFinal)
                .append(commentMsg)
                .append(rowID)
                .append(listTag)
                .append(userInfo)
                .append(urlSubComment)
                .append(getReengAccountBusiness().getToken())
                .append(timeStamp);
        final String security = HttpHelper.encryptDataV2(application, sb.toString(), getReengAccountBusiness().getToken());

        Http.Builder builder = post(this.domain, Url.OnMedia.API_ONMEDIA_ACTION_APP_V6);
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putParameter("content", content);
        builder.putParameter("url", urlKey);
        builder.putParameter("action_type", actionType.name());
        builder.putParameter("post_action_from", textPostFromFinal);
        builder.putParameter("status", commentMsg);
        builder.putParameter("row_id", rowID);
        builder.putParameter("tags", listTag);
        builder.putParameter("userinfo", userInfo);
        builder.putParameter("url_sub_comment", urlSubComment);
        builder.putParameter(UUID, Utilities.getUuidApp());
        builder.putParameter(Parameter.LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter(Parameter.COUNTRY_CODE, getReengAccountBusiness().getRegionCode());
        builder.putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter(Parameter.REVISION, Config.REVISION);
        builder.putParameter(Parameter.TIMESTAMP, timeStamp);
        builder.putParameter(Parameter.SECURITY, security);
        builder.setTag(Url.OnMedia.API_ONMEDIA_ACTION_APP_V6);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject jsonObject = new JSONObject(data);
                int code = jsonObject.optInt("code");
                if (code == 0 || code == 200) {
                    if (apiCallback != null) apiCallback.onSuccess("", data);
                } else {
                    onFailure("");
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) apiCallback.onError(message);
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();

            }
        });
        builder.execute();
    }
}
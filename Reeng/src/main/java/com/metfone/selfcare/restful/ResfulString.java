package com.metfone.selfcare.restful;

import android.net.Uri;


/**
 * Build URl
 *
 * @author gianglv3
 */
public class ResfulString {
    private StringBuilder baseUrl;

    public ResfulString(String baseUrl, boolean gen) {
        super();
        if (gen) {
            this.baseUrl = new StringBuilder(baseUrl).append("?");
        } else {
            this.baseUrl = new StringBuilder(baseUrl);
        }
    }

    public void addParam(String key, Object value) {
        if(value==null){
            value = "";
        }
        baseUrl.append(key).append("=").append(Uri.encode(value.toString().trim())).append("&");
    }


    public ResfulString(String baseUrl) {
        if (!baseUrl.endsWith("?")) {
            this.baseUrl = new StringBuilder(baseUrl).append("?");
        }
    }


    @Override
    public String toString() {
        //   Log.i("ResfulString.toString()", "gianglv3----> "+encode);
        return baseUrl.toString().substring(0, baseUrl.length() - 1);
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.restful;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.MediaModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author huybq7
 */
public class RestTopModel extends AbsResultData implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @SerializedName("data")
    private ArrayList<MediaModel> data;

    public ArrayList<MediaModel> getData() {
        return data;
    }

    public void setData(ArrayList<MediaModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestTopModel [data=" + data + "] errro " + getError();
    }


}

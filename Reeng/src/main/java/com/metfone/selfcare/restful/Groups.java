package com.metfone.selfcare.restful;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Groups implements Serializable {
    @SerializedName("doclist")
    @Expose
    private DocList doclists;

    public Groups() {
    }

    public void setDoclists(DocList doclists) {
        this.doclists = doclists;
    }

    public DocList getDoclists() {
        return doclists;
    }
}

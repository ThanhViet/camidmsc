package com.metfone.selfcare.firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.service.ServiceRetrofit;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.WsSubmitComplaintMyMetfoneRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsUpdateDeviceTokenRequest;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQInfo;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

import static com.metfone.selfcare.network.metfoneplus.MetfonePlusClient.createBaseRequest;

/**
 * Created by thanhnt72 on 11/22/2016.
 */
public class FireBaseHelper {

    private static final String TAG = FireBaseHelper.class.getSimpleName();
    private static final long REG_TIME_RANGE = 2 * 7 * 24 * 60 * 60; //4 weeks in s
    public static final String FIREBASE_REG_ID = "firebase_reg_id";

    private static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String FIREBASE_PROPERTY_SEND_SERVER = "FIREBASE_PROPERTY_SEND_SERVER";
    private static final String FIREBASE_PROPERTY_TIME_SEND_SERVER = "FIREBASE_PROPERTY_TIME_SEND_SERVER";

    public static final String SENDER_ID = "364417924016";
    private boolean sentToServer = false;

    private ApplicationController mApp;
    private SharedPreferences mPref;
    private String regid;
    private static FireBaseHelper mInstance;

    public static synchronized FireBaseHelper getInstance(ApplicationController app) {
        if (mInstance == null) {
            mInstance = new FireBaseHelper(app);
        }
        return mInstance;
    }

    private FireBaseHelper(ApplicationController app) {
        mApp = app;
        mPref = mApp.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    public void checkServiceAndRegister(boolean forceRefresh) {
        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices()) {
            boolean regIdValid = checkRegIdValid();
            //chua co regId hoac app version thay doi
            if (!regIdValid || forceRefresh) {
                registerInBackground();
            } else {
                //kiem tra da gui thanh cong len server chua
                //neu chua gui thanh cong len server thi gui len server
                sentToServer = getSendingRegIdToServerResult(mApp);
                if (sentToServer) {
                    Log.i(TAG, "already sent to server :" + regid);
                } else {
                    sendRegistrationIdToBackend(regid);
                }
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }

    public boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(mApp);
        return result == ConnectionResult.SUCCESS;
    }

    public boolean checkRegIdValid() {
        regid = getRegistrationId(mApp);
        if (TextUtils.isEmpty(regid)) return false;
        long registrationTime = getSucceedTimeSentRegIdToServer();
        long difTime = System.currentTimeMillis() - registrationTime;
        difTime = difTime / 1000; //convert from ms to s
        Log.i(TAG, "diff time = " + difTime + " REG_TIME_RANGE = " + REG_TIME_RANGE);
        if (difTime > REG_TIME_RANGE) {
            Log.i(TAG, "regid not valid");
            return false;
        }
        Log.i(TAG, "regid is valid");
        return true;
    }

    private String getRegistrationId(Context context) {
        String registrationId = mPref.getString(FIREBASE_REG_ID, "");
        Log.f(TAG, "getRegistrationId: " + registrationId);
        LogDebugHelper.getInstance().logDebugContent("getRegistrationId: " + registrationId);
        if (TextUtils.isEmpty(registrationId)) {
            Log.i(TAG, "Registration not found.");
            return "";
        } else {
            Log.i(TAG, "regid = " + registrationId);
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = mPref.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            Log.e(TAG, "PackageManager.NameNotFoundException", e);
            return 0;
        }
    }

    /**
     * tra ve thoi gian ma gui reg id len server thanh cong
     *
     * @return
     */
    private long getSucceedTimeSentRegIdToServer() {
        return mPref.getLong(FIREBASE_PROPERTY_TIME_SEND_SERVER, -1);
    }

    private void registerInBackground() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            try {
                                if (task.isSuccessful() && task.getResult() != null) {
                                    regid = task.getResult().getToken();
                                    Log.f(TAG, "registerInBackground oncomplete: " + regid);
                                    handleSendRegId(regid);
                                }
                            } catch (Exception ex) {
                                Log.e(TAG, "registerInBackground Error: " + ex);
                            }

                        }
                    });

//                    regid = FirebaseInstanceId.getInstance().getToken();
//                    handleSendRegId(regid);
                } catch (Exception ex) {
                    Log.e(TAG, "Exception", ex);
                }
            }
        }).start();
    }

    public void handleSendRegId(String regid) {
        Log.f(TAG, "FCM handleSendRegId Token: " + regid);
        LogDebugHelper.getInstance().logDebugContent("FCM handleSendRegId Token: " + regid);
        if (TextUtils.isEmpty(regid)) return;
        sendRegistrationIdToBackend(regid);
        storeRegistrationId(mApp, regid);
        RxUtils.async(new CompositeDisposable(), ServiceRetrofit.getInstance().regDevice(regid, Build.MODEL, Build.VERSION.RELEASE, System.currentTimeMillis(), ServiceRetrofit.getDefaultParam()), new ICallBackResponse<Object>() {
            @Override
            public void onSuccess(Object response) {

            }

            @Override
            public void onFail(String error) {

            }
        });
    }

//    boolean sendingRegIdAnonymous;

    private void sendRegistrationIdToBackend(final String registrationId) {
        if (mApp.getReengAccountBusiness().isAnonymousLogin()
                || !mApp.getReengAccountBusiness().isValidAccount()) {
//            if (sendingRegIdAnonymous) return;
//            sendingRegIdAnonymous = true;
            mApp.getApplicationComponent().provideUserApi().setRegId(registrationId);
        } else {
            // Your implementation here.
            if (!mApp.getXmppManager().isAuthenticated())
                return; //ko ket noi toi xmpp server thi khong gui
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    IQInfo iqRegId = new IQInfo(IQInfo.NAME_SPACE_GCM);
                    iqRegId.setType(IQ.Type.SET);
                    iqRegId.addElements("clientType", "Android");
                    iqRegId.addElements("regId", registrationId);
                    try {
                        IQ result = mApp.getXmppManager().sendPacketThenWaitingResponse(iqRegId, false);
                        if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                            saveSendingRegIdToServerResult(mApp, true);
                        } else {
                            saveSendingRegIdToServerResult(mApp, false);
                        }
                    } catch (Exception e) {
                        saveSendingRegIdToServerResult(mApp, false);
                        Log.f(TAG, "Exception", e);
                    }
                }
            }).start();
        }
    }

    public void saveSendingRegIdToServerResult(Context context, boolean isSucceed) {
        Log.i(TAG, "sendRegistrationIdToBackend --> " + isSucceed);
        sentToServer = isSucceed;
        SharedPreferences.Editor editor = mPref.edit();
        editor.putBoolean(FIREBASE_PROPERTY_SEND_SERVER, isSucceed);
        editor.putLong(FIREBASE_PROPERTY_TIME_SEND_SERVER, System.currentTimeMillis());
        editor.apply();
//        sendingRegIdAnonymous = false;
    }

    private void storeRegistrationId(Context context, String regId) {
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(FIREBASE_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }

    /**
     * tra ve ket qua da gui thanh cong reg id len server chua
     *
     * @param context
     * @return
     */
    private boolean getSendingRegIdToServerResult(Context context) {
        return mPref.getBoolean(FIREBASE_PROPERTY_SEND_SERVER, false);
    }
}
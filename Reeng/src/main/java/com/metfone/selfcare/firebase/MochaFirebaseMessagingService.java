/**
 * Copyright Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.metfone.selfcare.firebase;

import android.content.Context;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.helper.home.HomeContactsHelper;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.notification.ReengNotificationManager;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Ping;
import org.json.JSONObject;

import java.util.Map;

public class MochaFirebaseMessagingService extends FirebaseMessagingService /*implements XMPPConnectivityChangeListener */ {

    private static final String TAG = MochaFirebaseMessagingService.class.getSimpleName();

    private static final String CODE_MSG = "201";
    private static final String CODE_NEW_USER = "202";
    private static final String CODE_SUGGEST_SMS = "203";
    private static final String CODE_ONMEDIA = "205";
    private static final String CODE_SHOW_NOTIFY_ONMEDIA = "206";
    private static final String CODE_ONMEDIA_STRANGER = "207";
    private static final String CODE_SOCIAL_FRIEND = "208";
    private static final String CODE_LIKE_ALBUM = "209";
    private static final String CODE_MOCHA_VIDEO = "210";
    private static final String CODE_RECONNECT = "211";
    private static final String CODE_MUSIC = "212";
    private static final String CODE_MOVIE = "213";
    private static final String CODE_NEWS = "214";
    private static final String CODE_DEEPLINK = "215";
    private static final String CODE_CAMID_NOTIFICATION = "216";

    /*private static final String CODE_DEEPLINK = "212";
    private static final String CODE_RICH_CONTENT = "213";*/

    ApplicationController mApp;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Handle data payload of FCM messages.
        if (remoteMessage == null || remoteMessage.getData() == null) return;
        mApp = (ApplicationController) getApplication();
        Map<String, String> mapData = remoteMessage.getData();
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "FCM Message Id: " + remoteMessage.getMessageId());
            Log.e(TAG, "onMessageReceived: " + mapData.toString());
        }
        SettingBusiness.getInstance(getApplication()).checkSettingNotiOn();

        String content = mapData.get("content");
        String notificationType = mapData.get("notificationType");
        Log.f(TAG, "content: " + content);
        if (!TextUtils.isEmpty(content)) {
            processGcmMessage(remoteMessage,content,true);
        }else if(!TextUtils.isEmpty(notificationType)){
            processGcmMessage(remoteMessage,new JSONObject(mapData).toString(),false);
        }
    }

    private static final String MOCHA_VIETTEL_TOPIC = "Mocha_Viettel";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.f(TAG, "onNewToken: " + s);
        if (mApp == null)
            mApp = (ApplicationController) getApplication();
        FireBaseHelper.getInstance(mApp).handleSendRegId(s);
        // Once a token is generated, we subscribe to topic.
//        FirebaseMessaging.getInstance().subscribeToTopic(MOCHA_VIETTEL_TOPIC);

        mApp.getCamIdUserBusiness().setFirebaseRefreshedToken(s);
    }

    private void processGcmMessage(RemoteMessage remoteMessage,String content,boolean isMocha) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(content);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        if (jsonObject == null) {
            Log.f(TAG, "convert content to json error");
        } else {
            ApplicationController application = (ApplicationController) getApplicationContext();
            if(isMocha){
                ReengAccountBusiness accountBusiness = application.getReengAccountBusiness();
                String myNumber = accountBusiness.getJidNumber();
                if (!accountBusiness.isValidAccount()) {
                    Log.e(TAG, "account not valid");
                } else {
                    String receiver = jsonObject.optString("receiver", null);
                    String code = jsonObject.optString("code", null);
                    if (!TextUtils.isEmpty(code) && receiver != null && receiver.equals(myNumber)) {
                        ProcessGcmAsyncTask asyncTask = new ProcessGcmAsyncTask(application, jsonObject, code,isMocha);
                        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }else{
                ProcessGcmAsyncTask asyncTask = new ProcessGcmAsyncTask(application,remoteMessage, jsonObject, CODE_CAMID_NOTIFICATION,isMocha);
                asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    /*@Override
    public void onXMPPConnected() {
        Log.f(TAG, "onXMPPConnected clear fake new MessagesNotification");
        mApp.logDebugContent("onXMPPConnected clear fake new MessagesNotification");
//        ReengNotificationManager.clearMessagesNotification(getApplication(), Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG);
        mApp.cancelNotification(Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG);
        XMPPManager.removeXMPPConnectivityChangeListener(this);
    }

    @Override
    public void onXMPPDisconnected() {
        Log.f(TAG, "onXMPPDisconnected");
        mApp.logDebugContent("onXMPPDisconnected");
        XMPPManager.removeXMPPConnectivityChangeListener(this);
        *//*ApplicationController application = (ApplicationController) getApplicationContext();
        showNotifyFakeNewMessage(application);*//*
    }

    @Override
    public void onXMPPConnecting() {
        Log.f(TAG, "onXMPPConnecting");
    }*/

    private class ProcessGcmAsyncTask extends AsyncTask<Void, Void, Void> {
        private boolean isMocha;
        private ApplicationController mApplication;
        private String code;
        private JSONObject mJsonObject;
        private RemoteMessage remoteMessage;
        private ReengNotificationManager notificationManager;

        public ProcessGcmAsyncTask(ApplicationController application, JSONObject jsonObject, String code,boolean isMocha) {
            this.mApplication = application;
            this.mJsonObject = jsonObject;
            this.code = code;
            this.isMocha = isMocha;
            notificationManager = ReengNotificationManager.getInstance(mApplication);
        }
        public ProcessGcmAsyncTask(ApplicationController application,RemoteMessage remoteMessage, JSONObject jsonObject, String code,boolean isMocha) {
            this.mApplication = application;
            this.remoteMessage = remoteMessage;
            this.mJsonObject = jsonObject;
            this.code = code;
            this.isMocha = isMocha;
            notificationManager = ReengNotificationManager.getInstance(mApplication);
        }

        @Override
        protected void onPreExecute() {
            if (CODE_MSG.equals(code)) {// push new message
                mApplication.logDebugContent("FCM: " + mJsonObject.toString());
                checkConnectionAndReconnect(mApplication, true);
            } else if (CODE_RECONNECT.equals(code)) {
                mApplication.logDebugContent("FCM: " + mJsonObject.toString());
                checkConnectionAndReconnect(mApplication, false);
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
//            while (!mApplication.isDataReady() || !IMService.isReady()) {
            if(isMocha){
                while (!mApplication.isDataReady() || !mApplication.isReady()) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            String content = mJsonObject.optString("content");
            if (CODE_NEW_USER.equals(code)) {
                String newUser = mJsonObject.optString("newuser");
                newUser = newUser.split("@")[0];
                mApplication.getContactBusiness().notifyNewUser(newUser, content, false);
            } else if (CODE_SUGGEST_SMS.equals(code)) {
                String friend = mJsonObject.optString("smsto");
                friend = friend.split("@")[0];
                mApplication.getContactBusiness().notifySuggestSms(friend, content);
            } else if (CODE_ONMEDIA.equals(code)) {
                String listFriendNumbers = mJsonObject.optString("msisdn");
                String rowFeedId = mJsonObject.optString("feed_id");
                String action = mJsonObject.optString("action");
                int numberNotify = mJsonObject.optInt("unread", -1);
                String urlSubCmt = mJsonObject.optString("url_sub_comment");
                boolean isReply = mJsonObject.optInt("comment_type", 0) == 1;
                String urlParent = mJsonObject.optString("url");
                String avatarUrl = mJsonObject.optString("attach_img");
                if (numberNotify != -1) {
                    mApplication.getFeedBusiness().setTotalNotify(numberNotify);
                }
                if (TextUtils.isEmpty(listFriendNumbers)) {
                    return;
                }
                String jidFirst = listFriendNumbers.split(",")[0];
                if (TextUtils.isEmpty(jidFirst)) {
                    return;
                }
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                if (myNumber != null && myNumber.equals(jidFirst)) {
                    return;
                }
                notificationManager.drawOnMediaNotification(mApplication,
                        content, jidFirst, rowFeedId, action, urlParent, urlSubCmt, isReply, avatarUrl);
            } else if (CODE_SHOW_NOTIFY_ONMEDIA.equals(code)) {
                if (TextUtils.isEmpty(content)) {
                    return;
                }
                notificationManager.drawOnMediaNotificationShowTab(mApplication, content);
            } else if (CODE_ONMEDIA_STRANGER.equals(code)) {
                String name = mJsonObject.optString("name", null);
                String rowFeedId = mJsonObject.optString("feed_id");
                if (TextUtils.isEmpty(name)) {
                    return;
                }
                notificationManager.drawOnMediaNotificationStranger(mApplication, content, name, rowFeedId);
            } else if (CODE_SOCIAL_FRIEND.equals(code)) {
                String friendNumber = mJsonObject.optString("msisdn");
                if (TextUtils.isEmpty(content) || TextUtils.isEmpty(friendNumber)) return;
                String name = mJsonObject.optString("name");
                int action = mJsonObject.optInt("action", 0);// 0 requested, 1 friend
                String avatar = mJsonObject.optString("avatar");
                String appId = mJsonObject.optString("appId");
                HomeContactsHelper.getInstance(mApplication).setForeUpdateBeRequest(true);
                notificationManager.drawSocialFriendNotify(mApplication, content, friendNumber, name, avatar, appId,
                        action);
            } else if (CODE_LIKE_ALBUM.equals(code)) {
                String friendNumber = mJsonObject.optString("msisdn");
                if (TextUtils.isEmpty(content) || TextUtils.isEmpty(friendNumber)) return;
                notificationManager.drawNotifyFriendLikeAlbum(mApplication, content, friendNumber);
            } else if (CODE_MOCHA_VIDEO.equals(code)) {
                String name = mJsonObject.optString("name", null);
                String urlVideo = mJsonObject.optString("url_video");
                String avatar = mJsonObject.optString("channelAvatar");
                String bigImg = mJsonObject.optString("big_img");
                String campId = mJsonObject.optString("camp_id");
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(urlVideo)) {
                    return;
                }
//                notificationManager.drawMochaVideoNotification(mApplication, content, name, urlVideo, avatar);
                notificationManager.drawSuperAppNotification(mApplication, content, name, urlVideo, avatar,
                        ReengNotificationManager.TYPE_VIDEO, bigImg, campId);
            } else if (CODE_MUSIC.equals(code)) {
                String name = mJsonObject.optString("name", null);
                String url = mJsonObject.optString("url");
                String avatar = mJsonObject.optString("avatar");
                String bigImg = mJsonObject.optString("big_img");
                String campId = mJsonObject.optString("camp_id");
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(url)) {
                    return;
                }
                notificationManager.drawSuperAppNotification(mApplication, content, name, url, avatar,
                        ReengNotificationManager.TYPE_MUSIC, bigImg, campId);
            } else if (CODE_MOVIE.equals(code)) {
                String name = mJsonObject.optString("name", null);
                String url = mJsonObject.optString("url");
                String avatar = mJsonObject.optString("avatar");
                String bigImg = mJsonObject.optString("big_img");
                String campId = mJsonObject.optString("camp_id");
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(url)) {
                    return;
                }
                notificationManager.drawSuperAppNotification(mApplication, content, name, url, avatar,
                        ReengNotificationManager.TYPE_MOVIE, bigImg, campId);
            } else if (CODE_NEWS.equals(code)) {
                String name = mJsonObject.optString("name", null);
                String url = mJsonObject.optString("url");
                String avatar = mJsonObject.optString("avatar");
                String bigImg = mJsonObject.optString("big_img");
                String campId = mJsonObject.optString("camp_id");
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(url)) {
                    return;
                }
                notificationManager.drawSuperAppNotification(mApplication, content, name, url, avatar,
                        ReengNotificationManager.TYPE_NEWS, bigImg, campId);
            } else if (CODE_DEEPLINK.equals(code)) {
                String name = mJsonObject.optString("name", null);
                String url = mJsonObject.optString("url");
                String avatar = mJsonObject.optString("avatar");
                String bigImg = mJsonObject.optString("big_img");
                String campId = mJsonObject.optString("camp_id");
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(url)) {
                    return;
                }
                notificationManager.drawSuperAppNotification(mApplication, content, name, url, avatar,
                        ReengNotificationManager.TYPE_DEEP_LINK, bigImg, campId);
            } else if (CODE_MSG.equals(code)) {
                //TODO test disable fake push
//                drawFakeMessageNotification();
            }else if(CODE_CAMID_NOTIFICATION.equals(code)){
                String type = mJsonObject.optString("notificationType");
                String actionType = mJsonObject.optString("actionType");
                String objectId = mJsonObject.optString("actionObjectId");
                String iconUrl = mJsonObject.optString("iconUrl");
                String featureImage = mJsonObject.optString("featureImage");
                String link = mJsonObject.optString("link");
                int isExchange = mJsonObject.optInt("isExchange",0);
                notificationManager.drawSuperAppNotificationCamId(
                        mApplication,
                        remoteMessage.getNotification().getBody(),
                        type,
                        actionType,
                        objectId,
                        isExchange,
                        remoteMessage.getNotification().getTitle(),
                        link,iconUrl,featureImage);
            }
        }

        /*private void drawFakeMessageNotification() {
            String sender = mJsonObject.optString("sender");
            String groupName = mJsonObject.optString("subject");
            String msgBody = mJsonObject.optString("msg_body");
            String type = mJsonObject.optString("type");
            String subType = mJsonObject.optString("subtype");
            String groupId = mJsonObject.optString("group_jid");
            String packetId = mJsonObject.optString("msgid");
            Log.f(TAG, "drawFakeMessageNotification sender: " + sender + " groupName: " + groupName + " msgBody: " + msgBody + " type: " + type
                    + " subType: " + subType + " groupId: " + groupId);
//            ThreadMessage mThreadMessage = null;
            ReengMessage mMessage = new ReengMessage();
            ReengMessageConstant.MessageType msgType = getMessageTypeFromData(subType);
            if (msgType == null) return;
            mMessage.setPacketId(packetId);
            if (TextUtils.isEmpty(msgBody)) {
                msgBody = mApplication.getResources().getString(R.string.you_have_new_msg);
            }
            mMessage.setContent(msgBody);
            mMessage.setSender(sender);
            mMessage.setMessageType(msgType);


            if (ReengMessagePacket.Type.chat.name().equals(type)) {
                if (TextUtils.isEmpty(sender) || mApplication.getBlockContactBusiness().isBlockNumber(sender)) {
                    Log.d(TAG, "block number");
                    return;
                } else {
//                    mThreadMessage = mApplication.getMessageBusiness().findExistingSoloThread(sender);
                }
            } else if (ReengMessagePacket.Type.groupchat.name().equals(type)) {
                *//*if (!TextUtils.isEmpty(groupId)) {
                    String serverId = groupId.split("@")[0];
                    mThreadMessage = mApplication.getMessageBusiness().findGroupThreadByServerId(serverId);
                }*//*
            } else if (ReengMessagePacket.Type.offical.name().equals(type)) {
                *//*if (!TextUtils.isEmpty(sender)) {
                    mThreadMessage = mApplication.getMessageBusiness().findOfficerThreadById(sender);
                }*//*
            } else {
                //ko lam gi neu ko phai 3 loai tin nhan nay
                return;
            }
            *//*String title, msg = "";
            if (mThreadMessage != null) {
                mMessage.setThreadId(mThreadMessage.getId());
                if (ReengMessagePacket.Type.groupchat.name().equals(type) &&
                        mMessage.getMessageType() != ReengMessageConstant.MessageType.notification) {
                    msg = mApplication.getMessageBusiness().getFriendNameOfGroup(mMessage.getSender()) + ": ";
                    if (TextUtils.isEmpty(groupName)) {
                        title = mApplication.getResources().getString(R.string.app_name);
                    } else {
                        title = groupName;
                    }
                } else {
                    title = mApplication.getResources().getString(R.string.app_name);
                }
                msg += mApplication.getMessageBusiness().getContentOfMessage(mMessage, mApplication.getResources(),mApplication);

            } else {
                title = mApplication.getResources().getString(R.string.app_name);
                msg = mApplication.getResources().getString(R.string.you_have_new_msg);
            }*//*
            String title = mApplication.getResources().getString(R.string.app_name);
            String msg = mApplication.getResources().getString(R.string.you_have_new_msg);
            mApplication.logDebugContent("Show fake new message");
            notificationManager.drawFakeMessagesNotification(mApplication, null, mMessage, title, msg);
        }*/

        /*private ReengMessageConstant.MessageType getMessageTypeFromData(String subType) {
            ReengMessagePacket.SubType type;
            try {
                type = ReengMessagePacket.SubType.fromString(subType);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                type = ReengMessagePacket.SubType.empty;
            }
            switch (type) {
                case text:
                    return ReengMessageConstant.MessageType.text;
                case file_2:
                    return ReengMessageConstant.MessageType.file;
                case image:
                    return ReengMessageConstant.MessageType.image;
                case voicemail:
                    return ReengMessageConstant.MessageType.voicemail;
                case contact:
                    return ReengMessageConstant.MessageType.shareContact;
                case sharevideov2:
                    return ReengMessageConstant.MessageType.shareVideo;
                case voicesticker:
                    return ReengMessageConstant.MessageType.voiceSticker;
                case greeting_voicesticker:
                    return ReengMessageConstant.MessageType.greeting_voicesticker;
                case location:
                    return ReengMessageConstant.MessageType.shareLocation;
                case transfer_money:
                    return ReengMessageConstant.MessageType.transferMoney;
                case notification:
                    return ReengMessageConstant.MessageType.notification;
                case deeplink:
                    return ReengMessageConstant.MessageType.deep_link;
                case gift:
                    return ReengMessageConstant.MessageType.gift;
                case image_link:
                    return ReengMessageConstant.MessageType.image_link;
                case advertise:
                    return ReengMessageConstant.MessageType.advertise;
                case music_invite:
                    return ReengMessageConstant.MessageType.inviteShareMusic;
                case watch_video:
                    return ReengMessageConstant.MessageType.watch_video;
                default:
                    //Crashlytics.logException(new Exception("getMessageTypeFromData: " + subType));
                    return null;
            }
        }*/
    }

    private boolean isChecking = false;
    private Thread mThreadChecking;

    private void checkConnectionAndReconnect(final ApplicationController mApplication, final boolean showNotify) {
        if (XMPPManager.getConnectionState() == Constants.CONNECTION_STATE.CONNECTING || isChecking) {
            Log.f(TAG, "[checking_connection] isChecking: " + isChecking);
            mApplication.logDebugContent("[checking_connection] isChecking: " + isChecking);
        } else {
            isChecking = true;
            XMPPManager xmppManager = mApplication.getXmppManager();
//            if (IMService.isReady() && xmppManager != null && xmppManager.isAuthenticated()) {
            if (mApplication.isReady() && xmppManager != null && xmppManager.isAuthenticated()) {
                Log.d(TAG, "[checking_connection] running");
                mThreadChecking = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        boolean isConnectFail = false;
                        final Ping pingPacket = new Ping(IQ.Type.GET);
                        try {
                            final IQ iq = mApplication.getXmppManager().sendPacketThenWaitingResponse(pingPacket, false);
                            isConnectFail = iq == null;
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            isConnectFail = true;
                        } finally {
                            if (isConnectFail) {
                                mApplication.getXmppManager().manualDisconnect();
                                Log.f(TAG, "[checking_connection] manualDisconnect and reconnect");
                                mApplication.logDebugContent("[checking_connection] manualDisconnect and reconnect");
                                reconnect(mApplication, showNotify);
                            } else {
                                mApplication.getXmppManager().sendAvailableAfterPingSuccess();
                                mApplication.logDebugContent("sendAvailableAfterPingSuccess");
                            }
                            isChecking = false;
                            mThreadChecking = null;
                        }
                    }
                });
                mThreadChecking.setPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                mThreadChecking.start();
            } else {
                mApplication.logDebugContent("Service not ready, reconnect");
                reconnect(mApplication, showNotify);
                isChecking = false;
            }
        }
    }

    private void showNotifyFakeNewMessage(ApplicationController mApplication) {
        Log.f(TAG, "showNotifyFakeNewMessage");
        String title = mApplication.getResources().getString(R.string.app_name);
        String msg = mApplication.getResources().getString(R.string.you_have_new_msg);
        mApplication.logDebugContent("Show fake new message");
        ReengNotificationManager.getInstance(mApplication).drawFakeMessagesNotification(mApplication, null, null, title, msg);
    }

    private void reconnect(ApplicationController mApplication, boolean showNotify) {
        if (showNotify)
            showNotifyFakeNewMessage(mApplication);
//        XMPPManager.addXMPPConnectivityChangeListener(this);
//        if (IMService.isReady()) {
        if (mApplication.isReady()) {
            boolean ignoringBattery = false;
            boolean powerSaveMode = false;
            boolean deviceIdleMode = false;
            boolean interactive = false;
            if (Version.hasM()) {
                PowerManager oPowerManager = (PowerManager) mApplication.getSystemService(Context.POWER_SERVICE);
                ignoringBattery = oPowerManager.isIgnoringBatteryOptimizations(mApplication.getPackageName());
                powerSaveMode = oPowerManager.isPowerSaveMode();
                deviceIdleMode = oPowerManager.isDeviceIdleMode();
                interactive = oPowerManager.isInteractive();
            }
            String contentToLog = "[checking_connection] isReady(): " + mApplication.isDataReady() + "--------ignoringBattery: " + ignoringBattery
                    + " powerSaveMode: " + powerSaveMode + " deviceIdleMode: " + deviceIdleMode
                    + " interactive: " + interactive + " isConnectInternet: " + NetworkHelper.isConnectInternet(mApplication);
            Log.f(TAG, contentToLog);
            mApplication.logDebugContent(contentToLog);
//            IMService.getInstance().connectByToken(false);
            mApplication.connectByToken(false);
        } else {
//            Intent intent = new Intent(mApplication.getApplicationContext(), IMService.class);
//            mApplication.startService(intent);

            mApplication.startIMService();

            isChecking = false;
            Log.f(TAG, "[checking_connection] startService");
            mApplication.logDebugContent("[checking_connection] startService");
        }
    }
}
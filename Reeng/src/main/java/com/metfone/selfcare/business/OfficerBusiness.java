package com.metfone.selfcare.business;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.datasource.OfficerDataSource;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.ListenerHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by toanvk2 on 11/26/14.
 */
public class OfficerBusiness {
    private static final String TAG = OfficerBusiness.class.getSimpleName();
    private ArrayList<OfficerAccount> mOfficerAccountList;
    private HashMap<String, OfficerAccount> hashMapOfficer;
    //    private static EventAppListener mEventAppListener;
    private ApplicationController mApplicationController;
    private OfficerDataSource mOfficerDataSource;
    private ArrayList<OfficerAccount> mStickyOfficerAccounts;// danh sach officer sticky tren danh ba
    private ArrayList<OfficerAccount> mOfficerAccountSv;// danh sach officer lay tu sv

    private ArrayList<OfficerAccount> mListRoomTab;// danh sach cac tab
    private ArrayList<String> mTabName;// ten cac tab

    public OfficerBusiness(ApplicationController app) {
        this.mApplicationController = app;
    }

    public void init() {
        mOfficerDataSource = OfficerDataSource.getInstance(mApplicationController);
    }

    public ArrayList<OfficerAccount> getStickyOfficerAccounts() {
        return mStickyOfficerAccounts;
    }

    public boolean isDataReady() {
        return !(mOfficerAccountList == null || hashMapOfficer == null);
    }

    public void initOfficerAccountList() {
        mOfficerAccountSv = new ArrayList<>();
        mStickyOfficerAccounts = new ArrayList<>();
        mOfficerAccountList = mOfficerDataSource.getAllOfficalAccount();
        if (mOfficerAccountList == null) {
            mOfficerAccountList = new ArrayList<>();
        }
        hashMapOfficer = new HashMap<>();
        for (OfficerAccount account : mOfficerAccountList) {
            hashMapOfficer.put(account.getServerId(), account);
            // lay danh sach officer sticky
            if (account.getState() == OfficerAccountConstant.OFFICER_STATE_STICKY) {
                mStickyOfficerAccounts.add(account);
            }
        }
    }

    public OfficerAccount getOfficerAccountByServerId(String serverId) {
        if (hashMapOfficer == null || hashMapOfficer.isEmpty() || serverId == null) {
            return null;
        }
        return hashMapOfficer.get(serverId);
    }

    public String getOfficerAvatarByServerId(String serverId) {
        OfficerAccount account = getOfficerAccountByServerId(serverId);
        if (account != null) {
            return account.getAvatarUrl();
        }
        return null;
    }

    private OfficerAccount insertOfficerAccount(String serverId, String name, String avatarUrl,
                                                int state, int type, int roomState) {
        OfficerAccount account = mOfficerDataSource.createAccount(serverId,
                name, avatarUrl, state, type, roomState);
        if (account.getId() >= 0) {
            mOfficerAccountList.add(account);
            if (hashMapOfficer == null) {
                hashMapOfficer = new HashMap<>();
            }
            hashMapOfficer.put(account.getServerId(), account);
            return account;
        }
        return null;
    }

    public void updateOfficerAccount(OfficerAccount account) {
        mOfficerDataSource.updateOfficalAccount(account);
    }

    private void removeOfficerAccount(OfficerAccount account) {
        mOfficerDataSource.deleteOfficerAccount(account.getId());
        mOfficerAccountList.remove(account);
        if (hashMapOfficer != null) {
            hashMapOfficer.remove(account.getServerId());
        }
    }

    public ArrayList<OfficerAccount> getOfficerAccountSv() {
        return mOfficerAccountSv;
    }

    public void setOfficerAccountSv(ArrayList<OfficerAccount> officerAccountSv) {
        this.mOfficerAccountSv = officerAccountSv;
    }

    public void insertOrUpdateOfficerAccount(String serverId, String name, String avatarUrl, int type, int roomState) {
        OfficerAccount account = getOfficerAccountByServerId(serverId);
        if (account == null) {// nhan ban tin officer thi khong sticky
            insertOfficerAccount(serverId, name, avatarUrl,
                    OfficerAccountConstant.OFFICER_STATE_NONE, type, roomState);
        } else {
            String oldName = account.getName();
            String oldUrl = account.getAvatarUrl();
            // avatar hoac name thay doi
            if ((!TextUtils.isEmpty(name) && !name.equals(oldName)) ||
                    (!TextUtils.isEmpty(avatarUrl) && !avatarUrl.equals(oldUrl))) {
                account.setName(name);
                account.setAvatarUrl(avatarUrl);
                updateOfficerAccount(account);
            }
        }
    }

    private void processConfigOfficerAccount(ArrayList<OfficerAccount> listStickyAccount) {
        if (listStickyAccount == null || listStickyAccount.isEmpty()) {
            return;
        }
        ArrayList<OfficerAccount> listInserts = new ArrayList<>();
        ArrayList<OfficerAccount> listUpdates = new ArrayList<>();
        for (OfficerAccount officerAccount : listStickyAccount) {
            OfficerAccount accountOld = getOfficerAccountByServerId(officerAccount.getServerId());
            if (accountOld != null) {// da ton tai, check xem co update khong
                String name = officerAccount.getName();
                String avatarUrl = officerAccount.getAvatarUrl();
                int state = officerAccount.getState();
                // avatar , name hoac state thay doi
                if ((!TextUtils.isEmpty(name) && !name.equals(accountOld.getName())) ||
                        (!TextUtils.isEmpty(avatarUrl) && !avatarUrl.equals(accountOld.getAvatarUrl())) ||
                        (state == OfficerAccountConstant.OFFICER_STATE_STICKY && state != accountOld.getState())) {
                    // update data
                    accountOld.setName(name);
                    accountOld.setAvatarUrl(avatarUrl);
                    accountOld.setState(state);
                    listUpdates.add(accountOld);
                }
            } else {// chua co, insert
                listInserts.add(officerAccount);
            }
        }
        boolean isUpdateStickyList = false;
        if (listUpdates != null && !listUpdates.isEmpty()) {
            mOfficerDataSource.updateListOfficerAccount(listUpdates);
            isUpdateStickyList = true;
        }
        if (listInserts != null && !listInserts.isEmpty()) {
            // them vao db de lay id
            mOfficerDataSource.insertListOfficerAccount(listInserts);
            // them vao mem
            mOfficerAccountList.addAll(listInserts);
            // insert hash map
            for (OfficerAccount account : listInserts) {
                hashMapOfficer.put(account.getServerId(), account);
            }
            isUpdateStickyList = true;
        }
        // init lai data
        if (isUpdateStickyList) {
            HashMap<String, OfficerAccount> hashMap = new HashMap<>();
            ArrayList<OfficerAccount> listSticky = new ArrayList<>();
            for (OfficerAccount account : mOfficerAccountList) {
                hashMap.put(account.getServerId(), account);
                // lay danh sach officer sticky
                if (account.getState() == OfficerAccountConstant.OFFICER_STATE_STICKY) {
                    listSticky.add(account);
                }
            }
            hashMapOfficer = hashMap;
            mStickyOfficerAccounts = listSticky;
            ListenerHelper.getInstance().onStickyOfficerAccountChange();
        }
    }

    public ArrayList<String> getTabName() {
        if (mTabName == null) {
            mTabName = new ArrayList<>();
        }
        return mTabName;
    }

    public void setmTabName(ArrayList<String> mTabName) {
        this.mTabName = mTabName;
    }

    public ArrayList<OfficerAccount> getmListRoomTab() {
        if (mListRoomTab == null) {
            mListRoomTab = new ArrayList<>();
        }
        return mListRoomTab;
    }

    public void setmListRoomTab(ArrayList<OfficerAccount> mListRoomTab) {
        this.mListRoomTab = mListRoomTab;
    }

    public interface RequestListRoomTAbResponseListener {
        void onResponse(ArrayList<OfficerAccount> list);

        void onError(int errorCode, String msg);
    }

    public interface RequestOfficerResponseListener {
        void onResponse(ArrayList<OfficerAccount> listRoom);

        void onError(int errorCode, String msg);
    }

    public interface RequestFollowRoomChatListener {
        //void onResponseFollowRoomChat(ArrayList<MediaModel> playList, int stateOnline, String contentPoster);
        void onResponseFollowRoomChat(ThreadMessage threadMessage);

        void onErrorFollow(int errorCode, String msg);
    }

    public interface RequestLeaveRoomChatListener {
        void onResponseLeaveRoomChat();

        void onErrorLeave(int errorCode, String msg);
    }

    public interface RequestReportRoomListener {
        void onResponse();

        void onError(int errorCode);
    }
}
package com.metfone.selfcare.business;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.Contact;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.fragment.setting.EditProfileFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.GroupAvatarHelper;
import com.metfone.selfcare.helper.GroupAvatarInfo;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.bumptech.glide.load.DecodeFormat.DEFAULT;

/**
 * Created by toanvk2 on 11/13/14.
 */
public class AvatarBusiness {
    private static final String TAG = AvatarBusiness.class.getSimpleName();
    private static final String AVATAR_URL = "https://graph.facebook.com/%s/picture?width=1000&height=1000";
    private static final String AVATAR_THUMB = "/api/thumbnail/download?v=%1$s&ac=%2$s&t=%3$s&u=%4$s";
    private static final String AVATAR_FULL = "/api/thumbnail/download-orginal?v=%1$s&ac=%2$s&t=%3$s&u=%4$s";
    private static final String FILE_PATH = "file://";
    private ApplicationController mApplication;
    private ContactBusiness mContactBusiness;
    private RequestOptions[] displayAvatarOptions = new RequestOptions[Constants.CONTACT
            .NUM_COLORS_AVATAR_DEFAULT];
    private RequestOptions displayAvatarFullScreenOptions;
    private RequestOptions displayAvatarRoomOptions;

    private RequestOptions displayAvatarOfficialOptions;
    private RequestOptions displayMusicOptions;
    private RequestOptions displayMyAvatarOptions;
    private RequestOptions displayGroupAvatarOptions;
    //    private DisplayImageOptions displayFeedsAvatarOptions;
    private RequestOptions displayLogoOtherAppOption;
    private RequestOptions displayImageOption;
    private RequestOptions displayStrangerMusicBannerOption;
    private RequestOptions displayWatchVideoOption;
    private Resources mRes;
    private String[] avatarDefaultColor = {
            "#93D0AC", "#FFED46", "#FFCC99", "#82D554", "#F38AA5",
            "#B98215", "#CB95C3", "#007DB6", "#01B199", "#f68d76"};
    // private Bitmap[] bitmaps = new Bitmap[Constants.CONTACT.COLOR_AVATAR_SIZE];

    private Drawable[] avatar = new Drawable[Constants.CONTACT.NUM_COLORS_AVATAR_DEFAULT];
    private Bitmap[] avatarBitmaps = new Bitmap[Constants.CONTACT.NUM_COLORS_AVATAR_DEFAULT];
    private HashSet<String> failAvatarInRooms = new HashSet<>();

    public AvatarBusiness(ApplicationController application) {
        this.mApplication = application;
        init();
    }

    public void init() {
        mContactBusiness = mApplication.getContactBusiness();
        mRes = mApplication.getResources();
        initData();
    }

    private void initData() {
        //initBitmaps();
        initAvatarColor();
        initDisplayImageOptions();
    }

    private void initDisplayImageOptions() {
        for (int i = 0; i < Constants.CONTACT.NUM_COLORS_AVATAR_DEFAULT; i++) {
            displayAvatarOptions[i] = new RequestOptions()
                    .placeholder(avatar[i])
                    .error(avatar[i])
                    .dontAnimate()
                    .dontTransform()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .format(DEFAULT)
                    .skipMemoryCache(false);
        }

        displayAvatarRoomOptions = new RequestOptions()
                .placeholder(avatar[3])
                .error(avatar[3])
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .format(DEFAULT)
                .skipMemoryCache(false);

        displayLogoOtherAppOption = new RequestOptions()
                .placeholder(R.drawable.ic_thread_mocha_gray)
                .error(R.drawable.ic_thread_mocha_gray)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);

        displayImageOption = new RequestOptions()
                .placeholder(R.color.gray)
                .error(R.color.gray)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);

        displayAvatarOfficialOptions = new RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        // avatar top song
        displayMusicOptions = new RequestOptions()
                .placeholder(R.drawable.ic_keeng)
                .error(R.drawable.ic_keeng)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        // my avatar
        displayMyAvatarOptions = new RequestOptions()
                .error(avatar[3])
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        displayGroupAvatarOptions = new RequestOptions()
                .placeholder(R.drawable.ic_avatar_default)
                .error(R.drawable.ic_avatar_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        /*displayFeedsAvatarOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_avatar_default)
                .showImageOnLoading(R.drawable.ic_avatar_default)
                .showImageOnFail(R.drawable.ic_avatar_default)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(false)
                .bitmapConfig(Constants.IMAGE_CACHE.BITMAP_CONFIG)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .build();*/

        displayAvatarFullScreenOptions = new RequestOptions()
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        displayStrangerMusicBannerOption = new RequestOptions()
                .placeholder(R.drawable.ic_stranger_music_wait)
                .error(R.drawable.ic_stranger_music_wait)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);

        displayWatchVideoOption = new RequestOptions()
                .placeholder(R.color.bg_onmedia_content_item)
                .error(R.color.bg_onmedia_content_item)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);

    }

    public void setContactAvatar(ImageView avatarImage, TextView avatarText, Contact contact, int size) {
        PhoneNumber numberReeng = contact.getPhoneNumberReeng();
        String lastChangeAvatar = null;
        String number = null;
        int gender = 1;
        if (numberReeng != null) {
            lastChangeAvatar = numberReeng.getLastChangeAvatar();
            number = numberReeng.getJidNumber();
            gender = numberReeng.getGender();
        }
        setAvatar(avatarImage, avatarText, "AA", number, lastChangeAvatar, size, gender, 4);
    }

    /**
     * @param mImgAvatar
     * @param threadMessage
     */
    public void setGroupThreadAvatar(ImageView mImgAvatar,
                                     ThreadMessage threadMessage) {
        if (TextUtils.isEmpty(threadMessage.getGroupAvatar())) {
            GroupAvatarHelper groupAvatarHelper = GroupAvatarHelper.getInstance(mApplication);
            int threadId = threadMessage.getId();
            String pathFile = ImageHelper.getAvatarGroupFilePath(threadId);
            File fileGroupAvatar = new File(pathFile);
            GroupAvatarInfo groupAvatarInfo = new GroupAvatarInfo(threadMessage);
            // lay ra tu sdcard
            if (fileGroupAvatar.exists()) { // da tao file
//                universalImageLoader.displayImage("file://" + pathFile, mImgAvatar, displayGroupAvatarOptions);
                Glide.with(mApplication).load("file://" + pathFile).apply(displayGroupAvatarOptions).into(mImgAvatar);
            } else { // tao request gen lai file
//                universalImageLoader.cancelDisplayTask(mImgAvatar);
                Glide.with(mApplication).clear(mImgAvatar);
                mImgAvatar.setImageResource(R.drawable.ic_avatar_default);
                groupAvatarInfo.setIsForceToGenerate(true);
            }
            // gen file
            int viewWidth = (int) mApplication.getResources().getDimension(R.dimen.item_thread_avatar_height);
            mImgAvatar.setTag(String.valueOf(threadId));
            groupAvatarInfo.setGroupSize(threadMessage.getPhoneNumbers().size() + 1); // me and friend
            groupAvatarInfo.setViewWith(viewWidth);
            groupAvatarInfo.setImgGroupAvatar(mImgAvatar);
            groupAvatarHelper.requestGenGroupAvatar(groupAvatarInfo);
        } else {
            setGroupAvatarUrl(mImgAvatar, threadMessage, threadMessage.getGroupAvatar());
        }
    }

    public void setGroupThreadAvatar(ImageView mImgAvatar, View rlAvatarGroup,
                                     ThreadMessage threadMessage) {
        if (TextUtils.isEmpty(threadMessage.getGroupAvatar())) {
            if (rlAvatarGroup != null) {
                View viewDouble = rlAvatarGroup.findViewById(R.id.llAvatarDouble);
                View viewTriple = rlAvatarGroup.findViewById(R.id.llAvatarTriple);
//            View viewQuadruple = rlAvatarGroup.findViewById(R.id.llAvatarQuadruple);

                int sizeListMember = threadMessage.getListAllMemberIncludeAdmin(mApplication).size();
                if (sizeListMember == 2) {
                    rlAvatarGroup.setVisibility(View.VISIBLE);
                    mImgAvatar.setVisibility(View.GONE);
                    viewDouble.setVisibility(View.VISIBLE);
                    viewTriple.setVisibility(View.GONE);
//                viewQuadruple.setVisibility(View.GONE);
                    setAvatarGroupDouble(viewDouble, getlistAvatarMember(threadMessage));
                } else if (sizeListMember >= 3) {
                    rlAvatarGroup.setVisibility(View.VISIBLE);
                    mImgAvatar.setVisibility(View.GONE);
                    viewDouble.setVisibility(View.GONE);
                    viewTriple.setVisibility(View.VISIBLE);
//                viewQuadruple.setVisibility(View.GONE);
                    setAvatarGroupTriple(viewTriple, getlistAvatarMember(threadMessage));
                }
            /*else if (sizeListMember == 4) {
                rlAvatarGroup.setVisibility(View.VISIBLE);
                mImgAvatar.setVisibility(View.GONE);
                viewDouble.setVisibility(View.GONE);
                viewTriple.setVisibility(View.GONE);
                viewQuadruple.setVisibility(View.VISIBLE);
                setAvatarGroupQuadruple(viewQuadruple, getlistAvatarMember(threadMessage), false);
            } else if (sizeListMember >= 5) {
                rlAvatarGroup.setVisibility(View.VISIBLE);
                mImgAvatar.setVisibility(View.GONE);
                viewDouble.setVisibility(View.GONE);
                viewTriple.setVisibility(View.GONE);
                viewQuadruple.setVisibility(View.VISIBLE);
                setAvatarGroupQuadruple(viewQuadruple, getlistAvatarMember(threadMessage), true);
            }*/
                else {
                    mImgAvatar.setVisibility(View.VISIBLE);
                    mImgAvatar.setImageResource(R.drawable.ic_avatar_default);
                    rlAvatarGroup.setVisibility(View.GONE);
                }
            } else {
                if (mImgAvatar != null) {
                    mImgAvatar.setVisibility(View.VISIBLE);
                    mImgAvatar.setImageResource(R.drawable.ic_avatar_default);
                }
            }
        } else {
            mImgAvatar.setVisibility(View.VISIBLE);
            setGroupAvatarUrl(mImgAvatar, threadMessage, threadMessage.getGroupAvatar());
        }
    }

    public ArrayList<Object> getlistAvatarMember(ThreadMessage threadMessage) {
        ArrayList<Object> listAvatarMember;
        if (threadMessage.isForceCalculatorAllMember()
                || threadMessage.getListMemberAvatar() == null
                || threadMessage.getListMemberAvatar().isEmpty()) {
            listAvatarMember = getListAvatarMember(threadMessage);
            threadMessage.setListMemberAvatar(listAvatarMember);
        } else {
            listAvatarMember = threadMessage.getListMemberAvatar();
        }
        return listAvatarMember;
    }

    private void setAvatarGroupDouble(View viewDouble, ArrayList<Object> listAvatarMember) {
        ImageView ivDouble1 = viewDouble.findViewById(R.id.ivDouble1);
        ImageView ivDouble2 = viewDouble.findViewById(R.id.ivDouble2);
        Object obj1, obj2;
        if (listAvatarMember == null || listAvatarMember.isEmpty()) {
            obj1 = obj2 = null;
        } else if (listAvatarMember.size() == 1) {
            obj1 = listAvatarMember.get(0);
            obj2 = null;
        } else {
            obj1 = listAvatarMember.get(0);
            obj2 = listAvatarMember.get(1);
        }
        setAvatarGroupItem(obj1, ivDouble1);
        setAvatarGroupItem(obj2, ivDouble2);
    }

    private void setAvatarGroupTriple(View viewTriple, ArrayList<Object> listAvatarMember) {
        ImageView ivTriple1 = viewTriple.findViewById(R.id.ivTriple1);
        ImageView ivTriple2 = viewTriple.findViewById(R.id.ivTriple2);
        ImageView ivTriple3 = viewTriple.findViewById(R.id.ivTriple3);
        Object obj1, obj2, obj3;
        if (listAvatarMember == null || listAvatarMember.isEmpty()) {
            obj1 = obj2 = obj3 = null;
        } else if (listAvatarMember.size() == 1) {
            obj1 = listAvatarMember.get(0);
            obj2 = null;
            obj3 = null;
        } else if (listAvatarMember.size() == 2) {
            obj1 = listAvatarMember.get(0);
            obj2 = listAvatarMember.get(1);
            obj3 = null;
        } else {
            obj1 = listAvatarMember.get(0);
            obj2 = listAvatarMember.get(1);
            obj3 = listAvatarMember.get(2);
        }

        setAvatarGroupItem(obj1, ivTriple1);
        setAvatarGroupItem(obj2, ivTriple2);
        setAvatarGroupItem(obj3, ivTriple3);
    }

    private void setAvatarGroupQuadruple(View viewQuadruple, ArrayList<Object> listAvatarMember, boolean overSize) {
        ImageView ivQuadruple1 = viewQuadruple.findViewById(R.id.ivQuadruple1);
        ImageView ivQuadruple2 = viewQuadruple.findViewById(R.id.ivQuadruple2);
        ImageView ivQuadruple3 = viewQuadruple.findViewById(R.id.ivQuadruple3);
        ImageView ivQuadruple4 = viewQuadruple.findViewById(R.id.ivQuadruple4);

        Object obj1, obj2, obj3, obj4;
        if (listAvatarMember == null || listAvatarMember.isEmpty()) {
            obj1 = obj2 = obj3 = obj4 = null;
        } else if (listAvatarMember.size() == 1) {
            obj1 = listAvatarMember.get(0);
            obj2 = null;
            obj3 = null;
            obj4 = null;
        } else if (listAvatarMember.size() == 2) {
            obj1 = listAvatarMember.get(0);
            obj2 = listAvatarMember.get(1);
            obj3 = null;
            obj4 = null;
        } else if (listAvatarMember.size() == 3) {
            obj1 = listAvatarMember.get(0);
            obj2 = listAvatarMember.get(1);
            obj3 = listAvatarMember.get(2);
            obj4 = null;
        } else {
            obj1 = listAvatarMember.get(0);
            obj2 = listAvatarMember.get(1);
            obj3 = listAvatarMember.get(2);
            obj4 = listAvatarMember.get(3);
        }

        setAvatarGroupItem(obj1, ivQuadruple1);
        setAvatarGroupItem(obj2, ivQuadruple2);
        setAvatarGroupItem(obj3, ivQuadruple3);
        setAvatarGroupItem(obj4, ivQuadruple4);
        View viewOverSize = viewQuadruple.findViewById(R.id.tvMoreAvatar);
        if (overSize)
            viewOverSize.setVisibility(View.VISIBLE);
        else
            viewOverSize.setVisibility(View.GONE);
    }

    public void setPhoto(ImageView imageView, String filePath){
        int size = (int) mRes.getDimension(R.dimen.avatar_home_menu_size);
        if (filePath != null) {
            RequestOptions options = new RequestOptions()
                    .dontAnimate()
                    .dontTransform()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .format(DEFAULT)
                    .skipMemoryCache(false).override(size, size);

            new GlideImageLoader(imageView, new GlideImageLoader.SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingFailed(GlideException e) {

                }

                @Override
                public void onLoadingComplete() {
                }
            }).load(FILE_PATH + filePath, options);
        }
    }

    private void setAvatarGroupItem(Object entry, ImageView imageView) {
        if (entry != null && entry instanceof String) {
            if (entry.equals("")) {
                Glide.with(mApplication).load(R.drawable.ic_avatar_default_no_background).into(imageView);
            } else {
                Glide.with(mApplication).load((String) entry).into(imageView);
            }
        } else {
            Glide.with(mApplication).load(R.drawable.ic_avatar_default_no_background).into(imageView);
        }
    }

    /*Lấy danh sách avatar member group để set vào imageview*/
    private ArrayList<Object> getListAvatarMember(ThreadMessage threadMessage) {
        ArrayList<Object> listAvatarMember = new ArrayList<>();
        CopyOnWriteArrayList<String> listNumber = new CopyOnWriteArrayList<>(threadMessage.getListAllMemberIncludeAdmin(mApplication));
        for (String phone : listNumber) {
            if (phone.equals(mApplication.getReengAccountBusiness().getJidNumber())) {
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                String avatarPath = account.getAvatarPath();
                if (TextUtils.isEmpty(avatarPath)) {
                    avatarPath = getAvatarUrl(account.getLastChangeAvatar(), account.getJidNumber(),
                            GroupAvatarHelper.getInstance(mApplication).getSize(), account.getAvatarVerify());
                } else {
                    avatarPath = FILE_PATH + avatarPath;
                }
                listAvatarMember.add(avatarPath);
            } else {
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(phone);
                String lAvatar;
                if (phoneNumber != null) {
                    lAvatar = phoneNumber.getLastChangeAvatar();
                    if (!TextUtils.isEmpty(lAvatar)) {
                        String url = mApplication.getAvatarBusiness().getAvatarUrl(lAvatar,
                                phoneNumber.getJidNumber(), GroupAvatarHelper.getInstance(mApplication).getSize());
                        listAvatarMember.add(url);
                    }
                } else {
                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(phone);
                    if (nonContact != null) {
                        lAvatar = nonContact.getLAvatar();
                        if (!TextUtils.isEmpty(lAvatar)) {
                            String url = mApplication.getAvatarBusiness().getAvatarUrl(lAvatar,
                                    nonContact.getJidNumber(), GroupAvatarHelper.getInstance(mApplication).getSize());
                            listAvatarMember.add(url);
                        }
                    }
                }
            }
            if (listAvatarMember.size() >= 3) break;
        }

        while (listAvatarMember.size() < 3 && listNumber.size() >= 3) {
            Log.i(TAG, "chua du 3 member");
            listAvatarMember.add(null);
        }
        /*//nếu size chưa đủ 4 add thêm avatar của mình
        if (listAvatarMember.size() < 4) {
            ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
            String avatarPath = account.getAvatarPath();
            if (TextUtils.isEmpty(avatarPath)) {
                avatarPath = getAvatarUrl(account.getLastChangeAvatar(), account.getJidNumber(),
                        GroupAvatarHelper.getInstance(mApplication).getSize(), account.getAvatarVerify());
            } else {
                avatarPath = FILE_PATH + avatarPath;
            }
            listAvatarMember.add(avatarPath);
        }*/

        //nếu vẫn chưa đủ 4 ảnh thì add thêm mấy cái ảnh default vào
        /*while (listAvatarMember.size() <= 3) {
            listAvatarMember.add(R.drawable.ic_avatar_default);
        }*/
        threadMessage.setForceCalculatorAllMember(false);
        return listAvatarMember;
    }

    public void setGroupAvatarUrl(ImageView mImgGroupAvatar, ThreadMessage threadMessage, String lAvatar) {
        /*int viewWidth = (int) mApplication.getResources().getDimension(R.dimen.item_thread_avatar_height);
        String verify = threadMessage.getAvatarVerify(mApplication.getReengAccountBusiness().getJidNumber() +
        threadMessage.getServerId());
        String url = getAvatarGroupUrl(threadMessage.getServerId(), lAvatar, viewWidth, verify);
        Log.d(TAG, "setGroupAvatarUrl: " + url);*/
//        universalImageLoader.displayImage(getGroupAvatarUrl(threadMessage, lAvatar), mImgGroupAvatar,
//                displayGroupAvatarOptions);
        Glide.with(mApplication).load(getGroupAvatarUrl(threadMessage, lAvatar)).apply(displayGroupAvatarOptions).into(mImgGroupAvatar);
    }

    public void loadGroupAvatarUrl(ThreadMessage threadMessage, String lAvatar, GlideImageLoader.ImageLoadingListener listener) {
        new GlideImageLoader(listener).loadBitmap(getGroupAvatarUrl(threadMessage, lAvatar));
//        universalImageLoader.loadImage(getGroupAvatarUrl(threadMessage, lAvatar), listener);
    }

    public String getGroupAvatarUrl(ThreadMessage threadMessage, String lAvatar) {
        int viewWidth = (int) mApplication.getResources().getDimension(R.dimen.item_thread_avatar_height);
        String verify = threadMessage.getAvatarVerify(mApplication.getReengAccountBusiness().getJidNumber() +
                threadMessage.getServerId());
        return getAvatarGroupUrl(threadMessage.getServerId(), lAvatar, viewWidth, verify);
    }

    /**
     * check gen avatar group file
     *
     * @param groupAvatarHelper
     * @param currentToken      token hien tai, neu khac token cu thi gen lai file
     * @param threadMessage     thread message can check
     * @param imageSize         size image can gen
     * @return boolean = true neu can gen lai
     */
    public boolean checkTokenGenAvatarGroup(GroupAvatarHelper groupAvatarHelper,
                                            String currentToken, ThreadMessage threadMessage, int imageSize) {
        String oldToken = threadMessage.getOldAvatarToken();
        int threadId = threadMessage.getId();
        if (oldToken == null) {
            oldToken = groupAvatarHelper.getTokenChangeGroupAvatar(threadId);
            threadMessage.setOldAvatarToken(oldToken);
        }
        if (oldToken == null || !oldToken.equals(currentToken)) {
            String pathFile = ImageHelper.getAvatarGroupFilePath(threadId);
            File fileGroupAvatar = new File(pathFile);
            //truoc da tao file thi xoa di
            if (fileGroupAvatar.exists()) {
                fileGroupAvatar.delete();
            }
            //clearAvatarCache(pathFile, imageSize);
            groupAvatarHelper.setTokenChangeGroupAvatar(threadId, currentToken);
            threadMessage.setOldAvatarToken(currentToken);
            return true;
        }
        return false;
    }

    public void setOfficialThreadAvatar(ImageView avatarImage, String avatarUrl, int size) {
        Log.d(TAG, "setOfficialThreadAvatar:avatarUrl " + avatarUrl);
        if (!TextUtils.isEmpty(avatarUrl)) {
            int sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small2_size);
//            universalImageLoader.displayImage(avatarUrl,
//                    new ImageViewAwareTargetSize(avatarImage, sizeAvatar, sizeAvatar), displayAvatarOfficialOptions);
            RequestOptions options = displayAvatarOfficialOptions.override(sizeAvatar, sizeAvatar);
            Glide.with(mApplication).load(avatarUrl).apply(options).into(avatarImage);
        } else {
            avatarImage.setImageResource(R.mipmap.ic_launcher);
//            universalImageLoader.cancelDisplayTask(avatarImage);
            Glide.with(mApplication).clear(avatarImage);
        }
    }

    public void setNoteMessageAvatar(ImageView imageView, String url) {
        if (!TextUtils.isEmpty(url)) {
            int sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small2_size);
//            universalImageLoader.displayImage(url,
//                    new ImageViewAwareTargetSize(imageView, sizeAvatar, sizeAvatar), displayAvatarOfficialOptions);
            RequestOptions options = displayAvatarOfficialOptions.override(sizeAvatar, sizeAvatar);
            Glide.with(mApplication).load(url).apply(options).into(imageView);
        } else {
            imageView.setImageResource(R.mipmap.ic_launcher);
//            universalImageLoader.cancelDisplayTask(imageView);
            Glide.with(mApplication).clear(imageView);
        }
    }

    public void setOfficialThreadAvatar(ImageView avatarImage, int size,
                                        String officerId, OfficerAccount officerAccount, boolean actionBar) {
        // lay offical account by offical id
        String avatarUrl;
        if (officerAccount != null) {
            avatarUrl = officerAccount.getAvatarUrl();
        } else {
            OfficerBusiness officerBusiness = mApplication.getOfficerBusiness();
            avatarUrl = officerBusiness.getOfficerAvatarByServerId(officerId);
        }
        if (!TextUtils.isEmpty(avatarUrl)) {
            int sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small2_size);
//            universalImageLoader.displayImage(avatarUrl,
//                    new ImageViewAwareTargetSize(avatarImage, sizeAvatar, sizeAvatar), displayAvatarOfficialOptions);
            RequestOptions options = displayAvatarOfficialOptions.override(sizeAvatar, sizeAvatar);
            Glide.with(mApplication).load(avatarUrl).apply(options).into(avatarImage);
        } else {
//            universalImageLoader.cancelDisplayTask(avatarImage);
            Glide.with(mApplication).clear(avatarImage);
            /* if (officerId != null && officerId.equals(NVLT_OFFICIAL_ID)) {// avatar mac dinh cho nvlt
                avatarImage.setImageResource(R.drawable.ic_official_nvlt);
            } else {*/
            if (actionBar) {
                avatarImage.setImageResource(R.mipmap.ic_launcher);
            } else {
                avatarImage.setImageResource(R.mipmap.ic_launcher);
            }
            //   }
        }
    }

    public void setPhoneNumberAvatar(ImageView avatarImage, TextView avatarText,
                                     PhoneNumber phoneNumber, int size) {
        String lastChangeAvatar = null;
        if (phoneNumber.isReeng()) {
            lastChangeAvatar = phoneNumber.getLastChangeAvatar();
        }
        String number = phoneNumber.getJidNumber();
        int gender = phoneNumber.getGender();
        int id;
        if (phoneNumber.getId() != null) {
           /* if (phoneNumber.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
                setAvatarSystemBlock(avatarImage, avatarText);
            } else {*///TODO so danh ba khong xu case nay
            id = Integer.parseInt(phoneNumber.getId());
            setAvatar(avatarImage, avatarText, phoneNumber.getAvatarName(), number, lastChangeAvatar, size, gender,
                    id);
            //}
        } else {
            setUnknownNumberAvatar(avatarImage, avatarText, number, size);
        }
    }

//    public void setPhoneNumberAvatar(ImageView avatarImage, TextView avatarText,
//                                     PhoneNumber phoneNumber, int size) {
//        String lastChangeAvatar = null;
//        if (phoneNumber.isReeng()) {
//            lastChangeAvatar = phoneNumber.getLastChangeAvatar();
//        }
//        String number = phoneNumber.getJidNumber();
//        int gender = phoneNumber.getGender();
//        int id;
//        if (phoneNumber.getId() != null) {
//           /* if (phoneNumber.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
//                setAvatarSystemBlock(avatarImage, avatarText);
//            } else {*///TODO so danh ba khong xu case nay
//            id = Integer.parseInt(phoneNumber.getId());
//            setAvatar(avatarImage, avatarText, phoneNumber.getAvatarName(), number, lastChangeAvatar, size, gender,
//                    id);
//            //}
//        } else {
//            setUnknownNumberAvatar(avatarImage, avatarText, number, size);
//        }
//    }

    public void setUnknownNumberAvatar(ImageView avatarImage, TextView avatarText, String number, int size) {
        Log.i(TAG, "setUnknownNumberAvatar: " + number);
        NonContact nonContact = mContactBusiness.getExistNonContact(number);
        setUnknownNumberAvatar(avatarImage, avatarText, nonContact, number, size);
    }

    public void setUnknownNumberAvatarGroup(ImageView avatarImage, TextView avatarText,
                                            String number, String lastAvatar, int size) {
        Log.i(TAG, "setUnknownNumberAvatarGroup: " + number);
        NonContact nonContact = mContactBusiness.getExistNonContact(number);
        setUnknownNumberAvatarGroup(avatarImage, avatarText, nonContact, number, lastAvatar, size);
    }

    private void setUnknownNumberAvatarGroup(ImageView avatarImage, TextView avatarText,
                                             NonContact nonContact, String number, String lastAvatar, int size) {
        if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
            setAvatarSystemBlock(avatarImage, avatarText);
        } else {
            String lAvatar = null;
            if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                lAvatar = nonContact.getLAvatar();
            } else
                lAvatar = lastAvatar;

            if (!TextUtils.isEmpty(lAvatar)) {
                String avatarString = number.substring(number.length() - 3);
                setAvatar(avatarImage, avatarText, avatarString, number, lAvatar, size, Constants.CONTACT.GENDER_MALE, 5
                );
            } else {
                if (number != null && number.length() >= 3) {
                    String avatarString = number.substring(number.length() - 3);
                    setAvatar(avatarImage, avatarText, avatarString, number, lAvatar, size, Constants.CONTACT
                            .GENDER_MALE, 5);
                }
            }
        }
    }

    public void setUnknownNumberAvatar(ImageView avatarImage, TextView avatarText,
                                       NonContact nonContact, String number, int size) {
        if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
            setAvatarSystemBlock(avatarImage, avatarText);
        } else {
            String lAvatar = null;
            if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                lAvatar = nonContact.getLAvatar();
            }
            if (number != null && number.length() >= 3) {
                String avatarString = number.substring(number.length() - 3);
                setAvatar(avatarImage, avatarText, avatarString, number, lAvatar, size, Constants.CONTACT
                        .GENDER_MALE, 5);
            } else if (number != null) {
                setAvatar(avatarImage, avatarText, number, number, lAvatar, size, Constants.CONTACT.GENDER_MALE, 5
                );
            }
        }
    }

    public void setUnknownNumberAvatar(ImageView avatarImage, TextView avatarText,
                                       String number, int size, GlideImageLoader.SimpleImageLoadingListener listener) {
        Log.i(TAG, "setUnknownNumberAvatar: " + number);
        NonContact nonContact = mContactBusiness.getExistNonContact(number);
        if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
            setAvatarSystemBlock(avatarImage, avatarText);
        } else {
            String lAvatar = null;
            if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                lAvatar = nonContact.getLAvatar();
            }
            if (number != null && number.length() >= 3) {
                String avatarString = number.substring(number.length() - 3);
                setAvatar(avatarImage, avatarText, avatarString, number, lAvatar, size, Constants.CONTACT
                        .GENDER_MALE, 5);
            } else if (number != null) {
                setAvatar(avatarImage, avatarText, number, number, lAvatar, size, Constants.CONTACT.GENDER_MALE, 5
                );
            }
        }
    }

    public void setMemberRoomChatAvatar(ImageView avatarImage, final TextView avatarText,
                                        final String friendJid, final String friendName,
                                        PhoneNumber phoneNumber, int size, String lAvatar) {
        if (phoneNumber != null) {
           /* if (avatarText != null) {
                avatarText.setVisibility(View.GONE);
            }*/
            setPhoneNumberAvatar(avatarImage, avatarText, phoneNumber, size);
        } else {
            // avatar da load fail 1 lan roi thi thoi ko load nua
            if (failAvatarInRooms != null && failAvatarInRooms.contains(friendJid)) {
                int color = getColorByPhoneNumber(friendJid);
                avatarImage.setImageBitmap(avatarBitmaps[color]);
                if (avatarText != null) {
                    avatarText.setVisibility(View.VISIBLE);
                    avatarText.setText(PhoneNumberHelper.getInstant().getAvatarNameFromName(friendName));
                }
//                universalImageLoader.cancelDisplayTask(avatarImage);
                Glide.with(mApplication).clear(avatarImage);
                return;
            }
            if (avatarText != null) {
                avatarText.setVisibility(View.GONE);
            }
            if (TextUtils.isEmpty(lAvatar)) lAvatar = "1";
            String url = getAvatarUrl(lAvatar, friendJid, size);
            RequestOptions options = displayAvatarRoomOptions.override(size, size);
            new GlideImageLoader(avatarImage, new GlideImageLoader.SimpleImageLoadingListener() {
                @Override
                public void onLoadingFailed(GlideException failReason) {
                    if (avatarText != null) {
                        avatarText.setVisibility(View.VISIBLE);
                        avatarText.setText(PhoneNumberHelper.getInstant().getAvatarNameFromName(friendName));
                    }
                    failAvatarInRooms.add(friendJid);
//                            super.onLoadingFailed(imageUri, view, failReason);
                }

                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingComplete() {
                    if (avatarText != null) {
                        avatarText.setVisibility(View.GONE);
                    }
//                            super.onLoadingComplete(imageUri, view, loadedImage);
                }
            }).load(url, options);

//            universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(avatarImage, size, size),
//                    displayAvatarRoomOptions, new GlideImageLoader.SimpleImageLoadingListener() {
//                        @Override
//                        public void onLoadingFailed(GlideException failReason) {
//                            if (avatarText != null) {
//                                avatarText.setVisibility(View.VISIBLE);
//                                avatarText.setText(PhoneNumberHelper.getInstant().getAvatarNameFromName(friendName));
//                            }
//                            failAvatarInRooms.add(friendJid);
////                            super.onLoadingFailed(imageUri, view, failReason);
//                        }
//
//                        @Override
//                        public void onLoadingStarted() {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete() {
//                            if (avatarText != null) {
//                                avatarText.setVisibility(View.GONE);
//                            }
////                            super.onLoadingComplete(imageUri, view, loadedImage);
//                        }
//                    });
           /* String avatarName = PhoneNumberHelper.getInstant().getAvatarNameFromName(friendName);
            int random = 7;
            if (friendJid != null && friendJid.length() > 2) {// lay avatar random
                random = TextHelper.parserIntFromString(friendJid.substring(friendJid.length() - 2), 1) % Constants
                .CONTACT.COLOR_AVATAR_SIZE;
            }
            if (avatarText != null) {
                avatarText.setVisibility(View.VISIBLE);
                avatarText.setText(avatarName);
            }
            universalImageLoader.cancelDisplayTask(avatarImage);
            avatarImage.setImageBitmap(bitmaps[random]);*/
        }
    }

    /**
     * set stranger avatar
     */
    public void setStrangerAvatar(ImageView avatarImage, TextView avatarText,
                                  StrangerPhoneNumber stranger, String friendJid,
                                  String friendName, String lAvatar, int size) {
        String avatarName;
        String url;
        int color = getColorByPhoneNumber(friendJid);
        NonContact nonContact = mContactBusiness.getExistNonContact(friendJid);
        if (stranger != null) {
            if (stranger.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                url = stranger.getFriendAvatarUrl();
            } else {
                String lastAvatar = null;
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                    lastAvatar = nonContact.getLAvatar();
                } else if (nonContact == null) {
                    lastAvatar = stranger.getFriendAvatarUrl();// chat voi nguoi la thi truong nay luu lastchange
                }
                url = getAvatarUrl(lastAvatar, friendJid, size);
            }
            avatarName = stranger.getFriendAvatarName();
        } else {// chi co so va ten
            avatarName = PhoneNumberHelper.getInstant().getAvatarNameFromName(friendName);
            if (!TextUtils.isEmpty(lAvatar)) {
                url = getAvatarUrl(lAvatar, friendJid, size);
            } else {
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                    url = getAvatarUrl(nonContact.getLAvatar(), friendJid, size);
                } else {
                    url = null;
                }
            }
        }
        //set avatar text
        if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
            setAvatarSystemBlock(avatarImage, avatarText);
        } else if (!TextUtils.isEmpty(url)) {
            if (avatarText != null) {
                avatarText.setVisibility(View.GONE);
            }
//            universalImageLoader.displayImage(url,
//                    new ImageViewAwareTargetSize(avatarImage, size, size), displayAvatarOptions[color]);
            RequestOptions options = displayAvatarOptions[color].override(size, size);
            Glide.with(mApplication).load(url).apply(options).into(avatarImage);
        } else {
            if (avatarText != null) {
                avatarText.setVisibility(View.VISIBLE);
                avatarText.setText(avatarName);
            }
//            universalImageLoader.cancelDisplayTask(avatarImage);
            Glide.with(mApplication).clear(avatarImage);
            avatarImage.setImageBitmap(avatarBitmaps[color]);
        }
    }

//    public void setStrangerAvatarWithListener(ImageView avatarImage, TextView avatarText,
//                                              StrangerPhoneNumber stranger, String friendJid,
//                                              String friendName, String lAvatar, int size, GlideImageLoader.SimpleImageLoadingListener
//                                                      listener) {
//        String avatarName;
//        String url;
//        int color = getColorByPhoneNumber(friendJid);
//        NonContact nonContact = mContactBusiness.getExistNonContact(friendJid);
//        if (stranger != null) {
//            if (stranger.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
//                url = stranger.getFriendAvatarUrl();
//            } else {
//                String lastAvatar = null;
//                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
//                    lastAvatar = nonContact.getLAvatar();
//                } else if (nonContact == null) {
//                    lastAvatar = stranger.getFriendAvatarUrl();// chat voi nguoi la thi truong nay luu lastchange
//                }
//                url = getAvatarUrl(lastAvatar, friendJid, size);
//            }
//            avatarName = stranger.getFriendAvatarName();
//        } else {// chi co so va ten
//            avatarName = PhoneNumberHelper.getInstant().getAvatarNameFromName(friendName);
//            if (!TextUtils.isEmpty(lAvatar)) {
//                url = getAvatarUrl(lAvatar, friendJid, size);
//            } else {
//                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
//                    url = getAvatarUrl(nonContact.getLAvatar(), friendJid, size);
//                } else {
//                    url = null;
//                }
//            }
//        }
//        //set avatar text
//        if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
//            setAvatarSystemBlock(avatarImage, avatarText);
//            listener.onLoadingComplete(null, avatarImage, null);
//        } else if (!TextUtils.isEmpty(url)) {
//            if (avatarText != null) {
//                avatarText.setVisibility(View.GONE);
//            }
//            universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(avatarImage, size, size),
//                    displayAvatarOptions[color], listener);
//        } else {
//            if (avatarText != null) {
//                avatarText.setVisibility(View.VISIBLE);
//                avatarText.setText(avatarName);
//            }
////            universalImageLoader.cancelDisplayTask(avatarImage);
//            Glide.with(mApplication).clear(avatarImage);
//            avatarImage.setImageBitmap(avatarBitmaps[color]);
//            listener.onLoadingComplete(null, avatarImage, null);
//        }
//    }

    public void setLogoOtherApp(ImageView imageView, String url) {
        if (!TextUtils.isEmpty(url)) {
//            universalImageLoader.displayImage(url, imageView, displayLogoOtherAppOption);
            Glide.with(mApplication).load(url).apply(displayLogoOtherAppOption).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.ic_thread_mocha_gray);
//            universalImageLoader.cancelDisplayTask(imageView);
            Glide.with(mApplication).clear(imageView);
        }
    }

    public void setBannerGameImage(ImageView imageView, String url) {
        if (!TextUtils.isEmpty(url)) {
//            universalImageLoader.displayImage(url, imageView, displayImageOption);
            Glide.with(mApplication).load(url).apply(displayImageOption).into(imageView);
        } else {
            imageView.setImageResource(R.color.gray_light);
//            universalImageLoader.cancelDisplayTask(imageView);
            Glide.with(mApplication).clear(imageView);
        }
    }

//    public void setMyAvatar(String uri, ImageViewAwareTargetSize targetSize) {
//        universalImageLoader.displayImage(uri, targetSize, displayMyAvatarOptions);
//        Glide.with(mApplication).load(url).apply(displayMyAvatarOptions).into(avatarImage);
//    }

    public void setMyAvatar(ImageView avatarImage, ReengAccount account) {
        int size = (int) mRes.getDimension(R.dimen.avatar_home_menu_size);
        String userNumber = account.getJidNumber();
        String lastChange = account.getLastChangeAvatar();
        if (userNumber != null && lastChange != null && lastChange.length() > 0) {
            String url = getAvatarUrl(lastChange, userNumber, size, account.getAvatarVerify());
            Log.i(TAG, "----------url avatar: " + url);
//            universalImageLoader.displayImage(url, avatarImage, displayMyAvatarOptions);
            Glide.with(mApplication).load(url).apply(displayMyAvatarOptions).into(avatarImage);
        } else {
            avatarImage.setImageResource(R.drawable.ic_avatar_default);
//            universalImageLoader.cancelDisplayTask(avatarImage);
            Glide.with(mApplication).clear(avatarImage);
        }
    }

//    public void setMyAvatarProfile(ImageView avatarImage, ReengAccount account) {
//        String avatarPath = account.getAvatarPath();
//        int size = (int) mRes.getDimension(R.dimen.avatar_contact_detail_height_2);
//        String userNumber = account.getJidNumber();
//        String lastChange = account.getLastChangeAvatar();
//        if (TextUtils.isEmpty(avatarPath)) {
//            String url = getAvatarUrl(lastChange, userNumber, size, account.getAvatarVerify());
//            displayAndSaveMyAvatar(url, avatarImage, null, size, lastChange, false);
//        } else {
//            File avatarFile = new File(avatarPath);
//            if (avatarFile.exists()) {
//                avatarPath = FILE_PATH + avatarPath;
////                universalImageLoader.displayImage(avatarPath,
////                        new ImageViewAwareTargetSize(avatarImage, size, size), displayMyAvatarOptions,
////                        null);
//                RequestOptions options = displayMyAvatarOptions.override(size, size);
//                Glide.with(mApplication).load(avatarPath).apply(options).into(avatarImage);
//            } else {
//                String url = getAvatarUrl(lastChange, userNumber, size, account.getAvatarVerify());
//                displayAndSaveMyAvatar(url, avatarImage, null, size, lastChange, false);
//            }
//        }
//
//    }

//    public void setMyAvatar(ImageView avatarImage, final TextView avatarText,
//                            final TextView avatarTextDefault, ReengAccount account, String filePath) {
//        setMyAvatar(avatarImage, avatarText, avatarTextDefault, account, filePath);
//    }

    public void setMyAvatar(ImageView avatarImage, final TextView avatarText,
                            final TextView avatarTextDefault, ReengAccount account,
                            String filePath) {
        if (account == null) {
            if (avatarText != null)
                avatarText.setVisibility(View.GONE);
            return;
        }
        int size = (int) mRes.getDimension(R.dimen.avatar_home_menu_size);
        if (filePath != null) {
            RequestOptions options = displayMyAvatarOptions.override(size, size);
            new GlideImageLoader(avatarImage, new GlideImageLoader.SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingFailed(GlideException e) {

                }

                @Override
                public void onLoadingComplete() {
                    if (avatarText != null)
                        avatarText.setVisibility(View.GONE);
//                            ((ImageView) view).setBorderWidth(0);
                    if (avatarTextDefault != null) {
                        avatarTextDefault.setVisibility(View.GONE);
                    }
//                            if (listener != null)
//                                listener.onLoadingComplete(imageUri, view, loadedImage);
//                            super.onLoadingComplete(imageUri, view, loadedImage);
                }
            }).load(FILE_PATH + filePath, options);

//            universalImageLoader.displayImage(FILE_PATH + filePath,
//                    new ImageViewAwareTargetSize(avatarImage, size, size), displayMyAvatarOptions,
//                    new GlideImageLoader.SimpleImageLoadingListener() {
//
//                        @Override
//                        public void onLoadingStarted() {
//
//                        }
//
//                        @Override
//                        public void onLoadingFailed(GlideException e) {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete() {
//                            if (avatarText != null)
//                                avatarText.setVisibility(View.GONE);
////                            ((ImageView) view).setBorderWidth(0);
//                            if (avatarTextDefault != null) {
//                                avatarTextDefault.setVisibility(View.GONE);
//                            }
////                            if (listener != null)
////                                listener.onLoadingComplete(imageUri, view, loadedImage);
////                            super.onLoadingComplete(imageUri, view, loadedImage);
//                        }
//                    });
        } else {
            String userNumber = account.getJidNumber();
            String lastChange = account.getLastChangeAvatar();
            if (userNumber != null && lastChange != null && lastChange.length() > 0) {
                if (avatarText != null)
                    avatarText.setVisibility(View.GONE);
                if (avatarTextDefault != null) {
                    avatarTextDefault.setVisibility(View.GONE);
                }
                String avatarPath = account.getAvatarPath();
                if (TextUtils.isEmpty(avatarPath)) {
                    String url = getAvatarUrl(lastChange, userNumber, size, account.getAvatarVerify());
                    displayAndSaveMyAvatar(url, avatarImage, avatarTextDefault, size, lastChange, true);
                } else {
                    File avatarFile = new File(avatarPath);
                    if (avatarFile.exists()) {
                        avatarPath = FILE_PATH + avatarPath;
                        RequestOptions options = displayMyAvatarOptions.override(size, size);
                        Glide.with(mApplication).load(avatarPath).apply(options).into(avatarImage);
//                        universalImageLoader.displayImage(avatarPath,
//                                new ImageViewAwareTargetSize(avatarImage, size, size), displayMyAvatarOptions,
//                                listener);
                    } else {
                        String url = getAvatarUrl(lastChange, userNumber, size, account.getAvatarVerify());
                        displayAndSaveMyAvatar(url, avatarImage, avatarTextDefault, size, lastChange, false);
                    }
                }
            } else {
                if (avatarText != null)
                    avatarText.setVisibility(View.GONE);
                if (avatarTextDefault != null) {
                    avatarTextDefault.setVisibility(View.GONE);
                }
                avatarImage.setImageResource(R.drawable.ic_avatar_default);
//                universalImageLoader.cancelDisplayTask(avatarImage);
                Glide.with(mApplication).clear(avatarImage);
//                if (listener != null) listener.onLoadingComplete("", avatarImage, null);
            }
        }
    }

//    private void displayAndSaveMyAvatar(final String url, ImageView avatarImage,
//                                        final TextView avatarTextDefault, int size, final String time,
//                                        final boolean isSaveDB) {
//        displayAndSaveMyAvatar(url, avatarImage, avatarTextDefault, size, time, isSaveDB);
//    }

    private void displayAndSaveMyAvatar(final String url, ImageView avatarImage,
                                        final TextView avatarTextDefault, int size, final String time,
                                        final boolean isSaveDB) {
        RequestOptions options = displayMyAvatarOptions.override(size, size);
        new GlideImageLoader(new GlideImageLoader.ImageLoadingListener() {

            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingFailed(String imageUri, GlideException e) {

            }

            @Override
            public void onLoadingComplete(Bitmap loadedImage) {
//                        ((ImageView) view).setBorderWidth(0);
                if (avatarTextDefault != null) {
                    avatarTextDefault.setVisibility(View.GONE);
                }
                saveMyAvatar(loadedImage, time, isSaveDB);
            }
        }).loadBitmap(url, options);
        try {
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.color.v5_avatar_default)
                    .error(R.color.v5_avatar_default)
                    .priority(Priority.HIGH)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .override(size, size)
                    .dontAnimate()
                    .dontTransform()
                    .fitCenter()
                    .clone();
            Glide.with(avatarImage.getContext())
                    .load(url)
                    .apply(requestOptions)
                    .into(avatarImage);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
//        universalImageLoader.displayImage(url,
//                new ImageViewAwareTargetSize(avatarImage, size, size), displayMyAvatarOptions,
//                new GlideImageLoader.SimpleImageLoadingListener() {
//
//                    @Override
//                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
////                        ((ImageView) view).setBorderWidth(0);
//                        if (avatarTextDefault != null) {
//                            avatarTextDefault.setVisibility(View.GONE);
//                        }
//                        saveMyAvatar(loadedImage, time, isSaveDB);
//                        if (listener != null) {
//                            listener.onLoadingComplete(imageUri, view, loadedImage);
//                        }
//                        super.onLoadingComplete(imageUri, view, loadedImage);
//                    }
//                });
    }

    public void saveMyAvatar(Bitmap bitmap, String time, boolean isSaveDB) {
       /* File fileForImage = new File(Config.Storage.REENG_STORAGE_FOLDER +
                Config.Storage.PROFILE_PATH, "avatar" + time + Constants.FILE.JPEG_FILE_SUFFIX);*/
        if (bitmap == null) {
            return;
        }
        File profile = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.PROFILE_PATH);
        if (!profile.exists()) {
            profile.mkdirs();
        }
        String filePath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.PROFILE_PATH +
                "/avatar" + time + Constants.FILE.JPEG_FILE_SUFFIX;
        ImageHelper.getInstance(mApplication).saveBitmapToPath(bitmap, filePath, Bitmap.CompressFormat.JPEG);
        if (isSaveDB) {
            ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
            account.setNeedUpload(false);
            account.setAvatarPath(filePath);
            mApplication.getReengAccountBusiness().updateReengAccount(account);
            ListenerHelper.getInstance().onAvatarChange(filePath);
        }
    }

    public void setMyAvatarFromGetUserInfo(ImageView avatarImage, final TextView avatarText,
                                           final TextView avatarTextDefault, ReengAccount account) {

        if (account == null) {
            avatarText.setVisibility(View.GONE);
            return;
        }
        int size = (int) mRes.getDimension(R.dimen.avatar_home_menu_size);
        String userNumber = account.getJidNumber();
        String lastChange = account.getLastChangeAvatar();
        if (userNumber != null && lastChange != null && lastChange.length() > 0) {
            avatarText.setVisibility(View.GONE);
            avatarTextDefault.setVisibility(View.GONE);
            String url = getAvatarUrl(lastChange, userNumber, size, account.getAvatarVerify());
            displayAndSaveMyAvatar(url, avatarImage, avatarTextDefault, size, lastChange, true);
        }
    }

    public void loadMyAvatar(final LoadMyAvatar listener) {
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (account == null || !account.isActive()) {
            listener.onResponse(null);
        } else {
            int size = mRes.getDimensionPixelOffset(R.dimen.avatar_call);
            String userNumber = account.getJidNumber();
            String avatarPath = account.getAvatarPath();
            String lastChange = account.getLastChangeAvatar();
            String url;
            if (userNumber == null) {
                url = null;
            } else if (!TextUtils.isEmpty(avatarPath) && new File(avatarPath).exists()) {
                if (new File(FILE_PATH + avatarPath).exists()) {
                    url = FILE_PATH + avatarPath;
                } else {
                    url = avatarPath;
                }
            } else if (lastChange != null && lastChange.length() > 0) {
                url = getAvatarUrl(lastChange, userNumber, size, account.getAvatarVerify());
            } else {
                url = null;
            }
            Log.d(TAG, "loadMyAvatar: " + url);
            if (url != null) {
                new GlideImageLoader(new GlideImageLoader.ImageLoadingListener() {

                    @Override
                    public void onLoadingStarted() {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, GlideException e) {
                        listener.onResponse(null);
                    }

                    @Override
                    public void onLoadingComplete(Bitmap loadedImage) {
                        listener.onResponse(loadedImage);
                    }
                }).loadBitmap(url, new RequestOptions().override(size, size));
//
//                universalImageLoader.loadImage(url, new ImageSize(size, size), new GlideImageLoader.ImageLoadingListener() {
//                    @Override
//                    public void onLoadingStarted() {
//
//                    }
//
//                    @Override
//                    public void onLoadingComplete(Bitmap loadedImage) {
//                        listener.onResponse(loadedImage);
//                    }
//
//                    @Override
//                    public void onLoadingFailed(String imageUri, GlideException e) {
//                        listener.onResponse(null);
//                    }
//                });
            } else {
                listener.onResponse(null);
            }
        }
    }

    private void setAvatarSystemBlock(ImageView avatarImage, TextView avatarText) {
        Log.i(TAG, "setAvatarSystemBlock");
//        universalImageLoader.cancelDisplayTask(avatarImage);
        Glide.with(mApplication).clear(avatarImage);
        avatarImage.setImageResource(R.drawable.ic_avatar_default);
        if (avatarText != null) {
            avatarText.setVisibility(View.GONE);
        }
    }

    private void setAvatar(ImageView avatarImage, TextView avatarText, String avatarName,
                           String number, String lastChange, int size, int gender, int id) {
        try {
            if (avatarText != null) {
                avatarText.setText(avatarName);
            }
            if (number != null && lastChange != null && lastChange.length() > 0) {
//                if (avatarText != null) {
//                    avatarText.setVisibility(View.GONE);
//                }
                size = 140;
                String url = getAvatarUrl(lastChange, number, size);
                RequestOptions options = displayAvatarOptions[getColorByPhoneNumber(number)].override(size, size);

                Picasso.with(avatarImage.getContext()).load(url).resize(size, size).
                        centerCrop().into(avatarImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        if (avatarText != null) {
                            avatarText.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError() {
                        int colorIdex = id % Constants.CONTACT.NUM_COLORS_AVATAR_DEFAULT;
                        avatarImage.setImageBitmap(avatarBitmaps[colorIdex]);
                        if (avatarText != null) {
                            avatarText.setVisibility(View.VISIBLE);
                        }
                    }
                });
            } else {
                int colorIdex = id % Constants.CONTACT.NUM_COLORS_AVATAR_DEFAULT;
                Glide.with(mApplication).clear(avatarImage);
                avatarImage.setImageBitmap(avatarBitmaps[colorIdex]);
                if (avatarText != null) {
                    avatarText.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "setAvatar", e);
        }
    }

//    public void setPopupFullAvatar(ImageView imageView, String url, TextView tvwError,
//                                   GlideImageLoader.SimpleImageLoadingListener listener) {
//        if (!TextUtils.isEmpty(url)) {
//            universalImageLoader.loadImage(url, displayAvatarFullScreenOptions, listener);
//        }
//    }

    public void loadAvatar(String url, GlideImageLoader.ImageLoadingListener listener, int avatarSize) {
        new GlideImageLoader(listener).loadBitmap(url, new RequestOptions().override(avatarSize, avatarSize));
//        universalImageLoader.loadImage(url, new ImageSize(avatarSize, avatarSize), listener);
    }

    void clearCacheAvatar(PhoneNumber phoneNumber) {
        int size = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
        String verify = phoneNumber.getAvatarVerify();
        String url = getAvatarUrl(phoneNumber.getLastChangeAvatar(), phoneNumber.getJidNumber(), size, verify);
//        universalImageLoader.clearDiskCache(url);
//        universalImageLoader.clearMemoryCache(url, new ImageSize(size, size));
    }

    public void setSongAvatar(ImageView songImage, String url) {
        //songImage.setImageResource(R.drawable.ic_keeng);
//        universalImageLoader.displayImage(url, songImage, displayMusicOptions);
        Glide.with(mApplication).load(url).apply(displayMusicOptions).into(songImage);
    }

    public void setStrangerMusicBanner(ImageView imageView, int resourceDefault) {
        String urlBanner = mApplication.getMusicBusiness().getStrangerMusicBannerUrl();
        if (TextUtils.isEmpty(urlBanner) || resourceDefault == R.drawable.ic_stranger_music_cancel) {
//            universalImageLoader.cancelDisplayTask(imageView);
            Glide.with(mApplication).clear(imageView);
            imageView.setImageResource(resourceDefault);
        } else {
//            universalImageLoader.displayImage(urlBanner, imageView, displayStrangerMusicBannerOption);
            Glide.with(mApplication).load(urlBanner).apply(displayStrangerMusicBannerOption).into(imageView);
        }
    }

    public Bitmap getBitmapFromCache(String url, int threadId) {
        return GroupAvatarHelper.getInstance(mApplication).getBitmapCache(threadId);
//        if (TextUtils.isEmpty(url)) return null;
//        List<Bitmap> bitmaps = MemoryCacheUtils.findCachedBitmapsForImageUri(url, universalImageLoader.getMemoryCache());
//        if (bitmaps != null && !bitmaps.isEmpty()) {
//            return bitmaps.get(0);
//        }
//        return null;
    }

    public String getAvatarUrl(String lastChangeAvatar, String number, int sizeImage, String verify) {
        if (TextUtils.isEmpty(lastChangeAvatar) || TextUtils.isEmpty(number)) {
            return "";
        }
        String numberEncode = HttpHelper.EncoderUrl(number);
        String url;
        int sizeThumb = mApplication.getResources().getDimensionPixelSize(R.dimen.avatar_small_size);
        if (sizeImage > sizeThumb) {
            url = UrlConfigHelper.getInstance(mApplication).getDomainImage() + AVATAR_FULL;
        } else {
            url = UrlConfigHelper.getInstance(mApplication).getDomainImage() + AVATAR_THUMB;
        }
        return String.format(url, verify, numberEncode, lastChangeAvatar,
                HttpHelper.EncoderUrl(mApplication.getReengAccountBusiness().getJidNumber()));
    }

    public String getAvatarUrl(String lastChangeAvatar, String number, int sizeImage) {
        String verify = EncryptUtil.getVerify(HttpHelper.EncoderUrl(number));
        return getAvatarUrl(lastChangeAvatar, number, sizeImage, verify);
    }

    private String getAvatarGroupUrl(String groupId, String lAvatar, int sizeImage, String verify) {
        if (TextUtils.isEmpty(groupId) || TextUtils.isEmpty(lAvatar)) {
            return "";
        }
        String numberEncode = HttpHelper.EncoderUrl(mApplication.getReengAccountBusiness().getJidNumber());
        String groupIdEncode = HttpHelper.EncoderUrl(groupId);
        String lAvatarEncode = HttpHelper.EncoderUrl(lAvatar);
        //sizeImage = sizeImage + sizeImage / 2;
        //String url = "http://pv.mocha.com.vn:8088/api/group/download??crop=center&w=%d&h=%d&v=%s&q=70&ac=%s&lavatar
        // =%s&groupid=%s";
        return String.format(
                UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.AVATAR_GROUP_DOWNLOAD),
                sizeImage, sizeImage, verify, numberEncode, lAvatarEncode, groupIdEncode);
        /*return String.format(
                url,
                sizeImage, sizeImage, verify, numberEncode, LAvatarEncode, groupIdEncode);*/
    }

    private void initAvatarColor() {
        for (int i = 0; i < Constants.CONTACT.NUM_COLORS_AVATAR_DEFAULT; i++) {
            int color = Color.parseColor(avatarDefaultColor[i]);
            int[] colors = {color, color};
            GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, colors);
            drawable.setSize(50, 50);
            avatar[i] = drawable;

            Bitmap bitmap = Bitmap.createBitmap(50, 50, Constants.IMAGE_CACHE.BITMAP_CONFIG);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            avatarBitmaps[i] = bitmap;
        }
    }

    public Bitmap getBitmapColorAvatar(String jid) {
        int color = getColorByPhoneNumber(jid);
        return avatarBitmaps[color];
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void downloadAndSaveAvatar(final ApplicationController mApplicationController, final
    BaseSlidingFragmentActivity activity, String userId) {
        String url = String.format(AVATAR_URL, userId);
        GlideImageLoader.ImageLoadingListener loadingImageListener = new GlideImageLoader.ImageLoadingListener() {

            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingFailed(String imageUri, GlideException e) {

            }

            @Override
            public void onLoadingComplete(Bitmap loadedImage) {
                if (loadedImage != null) {
                    // save file
                    SharedPreferences mPref = mApplicationController.getSharedPreferences(Constants.PREFERENCE
                            .PREF_DIR_NAME, Context.MODE_PRIVATE);
                    File fileAcc = new File(Config.Storage.REENG_STORAGE_FOLDER
                            + Config.Storage.PROFILE_PATH);
                    if (!fileAcc.exists()) {
                        fileAcc.mkdirs();
                    }
                    String avatarFilePath = Config.Storage.REENG_STORAGE_FOLDER +
                            Config.Storage.PROFILE_PATH + "avatar_facebook" + Constants.FILE.JPEG_FILE_SUFFIX;
                    mPref.edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_FACEBOOK, avatarFilePath).apply();
                    ImageHelper.getInstance(mApplicationController).
                            saveBitmapToPath(loadedImage, avatarFilePath, Bitmap.CompressFormat.JPEG);
                    //upload
                    if (activity instanceof LoginActivity) {
                        LoginActivity loginActivity = (LoginActivity) activity;
                        loginActivity.setAvatarPersonalInfo(avatarFilePath);
                    } else if (activity instanceof EditProfileFragment) {
                        EditProfileFragment settingAcitivity = (EditProfileFragment) activity;
                        settingAcitivity.setAvatarEditProfile(avatarFilePath);
                    }
                    /*avatarFilePath = MessageHelper.getPathOfCompressedFile(avatarFilePath, Config.Storage
                            .PROFILE_PATH, false);
                    ReengAccount account = mApplicationController.getReengAccountBusiness().getCurrentAccount();
                    account.setNeedUpload(true);
                    account.setAvatarPath(avatarFilePath);
                    mApplicationController.getReengAccountBusiness().updateReengAccount(account);
                    mApplication.getTransferFileBusiness().createChannel(avatarFilePath, uploadAvatarListener);*/
                    Log.d(TAG, "downloadAndSaveAvatar onCompleted: " + avatarFilePath);
                }
            }
        };
//        universalImageLoader.loadImage(url,
//                new ImageSize(Constants.FILE.AVATAR_MAX_SIZE, Constants.FILE.AVATAR_MAX_SIZE), loadingImageListener);
        new GlideImageLoader(loadingImageListener).loadBitmap(url,
                new RequestOptions().override(Constants.FILE.AVATAR_MAX_SIZE, Constants.FILE.AVATAR_MAX_SIZE));
    }

//    private UploadListener uploadAvatarListener = new UploadListener() {
//        @Override
//        public void onUploadStarted(UploadRequest uploadRequest) {
//
//        }
//
//        @Override
//        public void onUploadComplete(UploadRequest uploadRequest, String response) {
//            Log.i(TAG, "response: " + response);
//            String time = "";
//            try {
//                JSONObject object = new JSONObject(response);
//                int errorCode = -1;
//                if (object.has(Constants.HTTP.REST_CODE)) {
//                    errorCode = object.getInt(Constants.HTTP.REST_CODE);
//                }
//                if (errorCode == Constants.HTTP.CODE_SUCCESS) {
//                    if (object.has(Constants.HTTP.USER_INFOR.LAST_AVATAR)) {
//                        time = object.getString(Constants.HTTP.USER_INFOR.LAST_AVATAR);
//                    }
//                }
//            } catch (Exception e) {
//                Log.e(TAG, "JSONException", e);
//            }
//            if (!TextUtils.isEmpty(time)) {   // upload ok
//                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
//                account.setLastChangeAvatar(time);
//                account.setNeedUpload(false);
//                mApplication.getReengAccountBusiness().updateReengAccount(account);
//                ListenerHelper.getInstance().onAvatarChange(uploadRequest.getFilePath());
//            }
//        }
//
//        @Override
//        public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
//
//        }
//
//        @Override
//        public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress, long
//                speed) {
//
//        }
//    };

    public void setAvatarOnMedia(ImageView imageView, TextView avatarText,
                                 String url, String jidNumber, String friendName, int targetSize) {
        //set avatar text
        Log.d(TAG, "jidNumber" + jidNumber + " url: " + url);
        if (!TextUtils.isEmpty(url)) {
            if (avatarText != null) avatarText.setVisibility(View.GONE);
            int index = getColorByPhoneNumber(jidNumber);
//            universalImageLoader.displayImage(url,
//                    new ImageViewAwareTargetSize(imageView, targetSize, targetSize), displayAvatarOptions[index]);
            RequestOptions options = displayAvatarOptions[index].override(targetSize, targetSize);
            Glide.with(mApplication).load(url).apply(options).into(imageView);
        } else {
            setUnknownNumberAvatarOnMedia(imageView, avatarText, jidNumber, friendName, targetSize);
        }
    }

//    public void setAvatarOnMediaWithListener(ImageView imageView, TextView avatarText, String url, String
//            jidNumber, String friendName, int targetSize, GlideImageLoader.SimpleImageLoadingListener listener) {
//        //set avatar text
//        Log.d(TAG, "jidNumber" + jidNumber);
//        if (!TextUtils.isEmpty(url)) {
//            if (avatarText != null) avatarText.setVisibility(View.GONE);
//            int index = getColorByPhoneNumber(jidNumber);
//            RequestOptions options = displayAvatarOptions[index].override(targetSize, targetSize);
//
//            universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(imageView, targetSize, targetSize),
//                    displayAvatarOptions[index], listener);
//        } else {
//            setUnknownNumberAvatarOnMedia(imageView, avatarText, jidNumber, friendName, targetSize);
//        }
//    }

    private void setUnknownNumberAvatarOnMedia(ImageView avatarImage, TextView avatarText,
                                               String friendJid, String friendName, int size) {
        Log.i(TAG, "setUnknownNumberAvatarOnMedia");
        NonContact nonContact = mContactBusiness.getExistNonContact(friendJid);
        if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
            setAvatarSystemBlock(avatarImage, avatarText);
        } else {
            String avatarName = PhoneNumberHelper.getInstant().getAvatarNameFromName(friendName);
            String url;
            int color = getColorByPhoneNumber(friendJid);
            if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                url = getAvatarUrl(nonContact.getLAvatar(), friendJid, size);
            } else {
                url = null;
            }
            //set avatar text
            if (!TextUtils.isEmpty(url)) {
                if (avatarText != null) {
                    avatarText.setVisibility(View.GONE);
                }
                RequestOptions options = displayAvatarOptions[color].override(size, size);
                new GlideImageLoader(avatarImage, new GlideImageLoader.SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted() {

                    }

                    @Override
                    public void onLoadingFailed(GlideException e) {
                        if (avatarText != null) {
                            avatarText.setVisibility(View.VISIBLE);
                            avatarText.setText(avatarName);
                        }
                    }

                    @Override
                    public void onLoadingComplete() {
                        if (avatarText != null) {
                            avatarText.setVisibility(View.GONE);
                        }
                    }
                }).load(url, options);

//                universalImageLoader.displayImage(url,
//                        new ImageViewAwareTargetSize(avatarImage, size, size), displayAvatarOptions[color],
//                        new GlideImageLoader.SimpleImageLoadingListener() {
//                            @Override
//                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                                if (avatarText != null) {
//                                    avatarText.setVisibility(View.VISIBLE);
//                                    avatarText.setText(avatarName);
//                                }
//                                super.onLoadingFailed(imageUri, view, failReason);
//                            }
//
//                            @Override
//                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                                if (avatarText != null) {
//                                    avatarText.setVisibility(View.GONE);
//                                }
//                                super.onLoadingComplete(imageUri, view, loadedImage);
//                            }
//                        });
            } else {
                if (avatarText != null) {
                    avatarText.setVisibility(View.VISIBLE);
                    avatarText.setText(avatarName);
                }
                Log.i(TAG, "set vatar bitmap");
//                universalImageLoader.cancelDisplayTask(avatarImage);
                Glide.with(mApplication).clear(avatarImage);
                avatarImage.setImageBitmap(avatarBitmaps[color]);
            }
        }
    }

    private int getColorByPhoneNumber(String number) {
        if (TextUtils.isEmpty(number) || number.length() < 1) {
            return 3;
        } else {
            int phoneLen = number.length();
            return TextHelper.parserIntFromString("" + number.charAt(phoneLen - 1), 3) % Constants.CONTACT
                    .NUM_COLORS_AVATAR_DEFAULT;
        }
    }

    public void displayFullAvatar(String jidNumber, String lAvatar, String contactName, String strangerAvatarUrl,
                                  String avatarSmallUrl) {
        String avatarFullUrl;
        if (!TextUtils.isEmpty(strangerAvatarUrl)) {
            avatarFullUrl = strangerAvatarUrl;
        } else {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) mApplication.getSystemService(Context.WINDOW_SERVICE);
            if (wm != null) {
                wm.getDefaultDisplay().getMetrics(displaymetrics);
            }
            int sizeAvatar = displaymetrics.widthPixels - 40;
            if (TextUtils.isEmpty(lAvatar)) {
                avatarFullUrl = null;
            } else {
                avatarFullUrl = getAvatarUrl(lAvatar, jidNumber, sizeAvatar);
            }
        }
        if (!TextUtils.isEmpty(avatarFullUrl)) {
            ArrayList<ImageProfile> imageList = new ArrayList<>();
            ImageProfile item = new ImageProfile();
            item.setImageUrl(avatarFullUrl);
            if (!TextUtils.isEmpty(lAvatar)) {
                item.setIdServerString(lAvatar);
            } else {
                item.setIdServerString(strangerAvatarUrl);
            }
            item.setImageThumb(avatarSmallUrl);
            imageList.add(item);
            EventOnMediaHelper.showImageDetail(mApplication, contactName, jidNumber, imageList, 0,
                    FeedContent.ITEM_TYPE_PROFILE_AVATAR, -1, true);

            /*Intent intent = new Intent(mApplication, PreviewImageProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(PreviewImageProfileActivity.PARAM_LIST_IMAGE, imageList);
            intent.putExtra(PreviewImageProfileActivity.PARAM_CURRENT_IMAGE, 0);
            intent.putExtra(PreviewImageProfileActivity.PARAM_AVATAR_COVER, true);
            intent.putExtra(PreviewImageProfileActivity.PARAM_THREAD_NAME, contactName);
            intent.putExtra(PreviewImageProfileActivity.PARAM_PHONE_NUMBER, jidNumber);
            intent.putExtra(PreviewImageProfileActivity.PARAM_LAST_AVATAR, lAvatar);
            mApplication.startActivity(intent);*/
        }
    }

//    public void displayFullAvatarOfficial(String nameOfficial, String idOfficial, String avatarUrl) {
//        if (!TextUtils.isEmpty(avatarUrl)) {
//            ArrayList<ImageProfile> imageList = new ArrayList<>();
//            ImageProfile item = new ImageProfile();
//            item.setImageUrl(avatarUrl);
//            item.setIdServerString(avatarUrl);
//            item.setImageThumb(avatarUrl);
//            imageList.add(item);
//
//            EventOnMediaHelper.showImageDetail(mApplication, nameOfficial, idOfficial, imageList, 0,
//                    FeedContent.ITEM_TYPE_PROFILE_AVATAR, -1);
//
//
//            /*Intent intent = new Intent(mApplication, PreviewImageProfileActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intent.putExtra(PreviewImageProfileActivity.PARAM_LIST_IMAGE, imageList);
//            intent.putExtra(PreviewImageProfileActivity.PARAM_CURRENT_IMAGE, 0);
//            intent.putExtra(PreviewImageProfileActivity.PARAM_AVATAR_COVER, true);
//            intent.putExtra(PreviewImageProfileActivity.PARAM_THREAD_NAME, nameOfficial);
//            intent.putExtra(PreviewImageProfileActivity.PARAM_CONTACT_TYPE, Constants.CONTACT_TYPE.OFFICIAL);
//            mApplication.startActivity(intent);*/
//        }
//    }

    /**
     *
     */
    public void displayAvatarContact(Object object, ImageView avatarImage, TextView avatarText, int avatarSize) {
        String lastChangeAvatar = null;
        String strangerAvatar = null;
        String number;
        String avatarString = "";
        int idColor = 4;
        if (object == null) {
            number = "";
        } else if (object instanceof PhoneNumber) {
            PhoneNumber phoneNumber = (PhoneNumber) object;
            if (phoneNumber.isReeng()) {
                lastChangeAvatar = phoneNumber.getLastChangeAvatar();
            }
            number = phoneNumber.getJidNumber();
            if (phoneNumber.getId() != null) {
                idColor = Integer.parseInt(phoneNumber.getId());
                avatarString = phoneNumber.getAvatarName();
            } else {
                idColor = 5;
                NonContact nonContact = mContactBusiness.getExistNonContact(number);
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                    lastChangeAvatar = nonContact.getLAvatar();
                }
                if (number != null && number.length() >= 3) {
                    avatarString = number.substring(number.length() - 3);
                }
            }
        } else if (object instanceof NonContact) {
            NonContact nonContact = (NonContact) object;
            idColor = 5;
            number = nonContact.getJidNumber();
            if (nonContact.getState() == Constants.CONTACT.ACTIVE) {
                lastChangeAvatar = nonContact.getLAvatar();
            }
            if (number != null && number.length() >= 3) {
                avatarString = number.substring(number.length() - 3);
            }
        } else if (object instanceof StrangerPhoneNumber) {
            StrangerPhoneNumber strangerNumber = (StrangerPhoneNumber) object;
            number = strangerNumber.getPhoneNumber();
            if (strangerNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                strangerAvatar = strangerNumber.getFriendAvatarUrl();
            } else {
                NonContact nonContact = mContactBusiness.getExistNonContact(number);
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                    lastChangeAvatar = nonContact.getLAvatar();
                } else if (nonContact == null) {
                    lastChangeAvatar = strangerNumber.getFriendAvatarUrl();// chat voi nguoi la thi truong nay luu
                    // lastchange
                }
            }
            idColor = getColorByPhoneNumber(number);
            avatarString = strangerNumber.getFriendAvatarName();
        } else if (object instanceof String) {
            number = (String) object;
            PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
            if (phoneNumber != null) {
                if (phoneNumber.isReeng()) {
                    lastChangeAvatar = phoneNumber.getLastChangeAvatar();
                }
                idColor = Integer.parseInt(phoneNumber.getId());
                avatarString = phoneNumber.getAvatarName();
            } else {
                NonContact nonContact = mContactBusiness.getExistNonContact(number);
                idColor = 5;
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                    lastChangeAvatar = nonContact.getLAvatar();
                }
                if (number.length() >= 3) {
                    avatarString = number.substring(number.length() - 3);
                }
            }
        } else {
            number = "";
            idColor = 5;
        }
        if (!TextUtils.isEmpty(strangerAvatar)) {
//            universalImageLoader.displayImage(strangerAvatar,
//                    new ImageViewAwareTargetSize(avatarImage, avatarSize, avatarSize), displayAvatarOptions[4]);
            RequestOptions options = displayAvatarOptions[4].override(avatarSize, avatarSize);
            Glide.with(mApplication).load(strangerAvatar).apply(options).into(avatarImage);
        } else {
            setAvatar(avatarImage, avatarText, avatarString, number, lastChangeAvatar, avatarSize, 1, idColor);
        }
    }

    public void setThumbWatchVideo(ImageView songImage, String url) {
//        universalImageLoader.displayImage(url, songImage, displayWatchVideoOption);
        Glide.with(mApplication).load(url).apply(displayWatchVideoOption).into(songImage);
    }

    public Bitmap[] getAvatarBitmaps() {
        return avatarBitmaps;
    }

    public Drawable[] getAvatarDrawable() {
        return avatar;
    }

    public interface LoadMyAvatar {
        void onResponse(Bitmap bitmap);
    }
}
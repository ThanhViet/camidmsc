package com.metfone.selfcare.business;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.SoloTransferMoneyMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.util.Log;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thaodv on 6/24/2015.
 */
public class TransferMoneyBusiness {
    private static final String TAG = TransferMoneyBusiness.class.getSimpleName();
    private static TransferMoneyBusiness instance;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private MessageBusiness mMessageBusiness;
    private ReengAccount account;

    private TransferMoneyBusiness(ApplicationController app) {
        this.mApplication = app;
        mAccountBusiness = app.getReengAccountBusiness();
        mMessageBusiness = mApplication.getMessageBusiness();
    }

    public static synchronized TransferMoneyBusiness getInstance(ApplicationController app) {
        if (instance == null) {
            instance = new TransferMoneyBusiness(app);
        }
        return instance;
    }

    public void doGenOtpRequest(final RequestGenOtpCallback callback, final String receiver,
                                final String moneyAmount, final String transContent) {
        account = mAccountBusiness.getCurrentAccount();
        String url = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfFile(Config.UrlEnum.LIXI_GENOTP);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getCurrentAccount().getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        try {
                            TransferMoneyModel transferMoneyModel = new Gson().fromJson(decryptResponse, TransferMoneyModel.class);
                            if (transferMoneyModel.getCode() == HTTPCode.E200_OK) {
                                //Log.i(TAG, "requestId = " + transferMoneyModel.getRequestId());
                                callback.onSuccess(transferMoneyModel.getRequestId());
                            } else {
                                callback.onError(transferMoneyModel.getDesc());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            callback.onError(mApplication.getResources().getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                callback.onError(mApplication.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String content = "";
                if (!TextUtils.isEmpty(transContent)) {
                    content = transContent;
                }
                HashMap<String, String> params = new HashMap<>();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(receiver).
                        append(moneyAmount).
                        append(content).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("receiver", receiver);
                params.put("amount", moneyAmount);
                params.put("transContent", content);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                return params;
            }
        };

        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, "doGenOtpRequest", false);
    }

    public void doConfirmPayment(final RequestTransferCallback callback,
                                 final String requestId, final String otpSMS) {
        account = mAccountBusiness.getCurrentAccount();
        String url = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfFile(Config.UrlEnum.LIXI_TRANSFER_MONEY);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getCurrentAccount().getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        try {
                            TransferMoneyModel transferMoneyModel = new Gson().fromJson(decryptResponse, TransferMoneyModel.class);
                            if (transferMoneyModel.getCode() == HTTPCode.E200_OK) {
                                Log.i(TAG, "requestId = " + transferMoneyModel.getRequestId());
                                callback.onSuccess(String.valueOf(TimeHelper.getCurrentTime()),
                                        mApplication.getResources().getString(R.string.vnd));
                            } else {
                                callback.onError(transferMoneyModel.getDesc());
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            callback.onError(mApplication.getResources().getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                callback.onError(mApplication.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(requestId).append(otpSMS).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("requestId", requestId);
                params.put("otp", otpSMS);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                return params;
            }
        };

        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, "doConfirmPayment", false);

    }

    public long getMoneyAmountFromText(String input) {
        String longString = PhoneNumberHelper.getInstant().getStandardizedNumber(input);
        if (TextUtils.isEmpty(longString)) {
            return -1;
        }
        try {
            return Long.valueOf(longString);
        } catch (NumberFormatException e) {
            Log.e(TAG, "Exception", e);
            return -1;
        }
    }

    public void sendTransferMessage(ThreadMessage threadMessage, String from, String to,
                                    String amountMoney, String unitMoney, String transferTime) {
        SoloTransferMoneyMessage moneyMessage = new SoloTransferMoneyMessage(threadMessage,
                from, to, amountMoney, unitMoney, transferTime);
        moneyMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        mMessageBusiness.insertNewMessageBeforeSend(threadMessage,
                ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT, moneyMessage);
        mMessageBusiness.sendXMPPMessage(moneyMessage, threadMessage);
    }

    public void sendActionClick() {
        account = mAccountBusiness.getCurrentAccount();
        String url = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfFile(Config.UrlEnum.LIXI_CLICK);
        Log.d(TAG, "sendActionClick url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getCurrentAccount().getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        /*try {
                            TransferMoneyModel transferMoneyModel = new Gson().fromJson(decryptResponse, TransferMoneyModel.class);
                            if (transferMoneyModel.getCode() == HTTPCode.E200_OK) {
                                Log.i(TAG, "requestId = " + transferMoneyModel.getRequestId());
                                callback.onSuccess(String.valueOf(TimeHelper.getCurrentTime()),
                                        mApplication.getResources().getString(R.string.vnd));
                            } else {
                                callback.onError(transferMoneyModel.getDesc());
                            }
                        } catch (Exception e) {
                            Log.e(TAG,"Exception",e);
                        }*/
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                return params;
            }
        };

        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, "doLogPayment", false);
    }

    public interface RequestGenOtpCallback {
        void onSuccess(String requestId);

        void onError(String message);
    }

    public interface RequestTransferCallback {
        void onSuccess(String timeTransfer, String unitMoney);

        void onError(String message);
    }

    public class TransferMoneyModel implements Serializable {
        @SerializedName("code")
        private int code;

        @SerializedName("desc")
        private String desc;

        @SerializedName("requestId")
        private String requestId;

        @SerializedName("timetrans")
        private String timeTransfer;

        public int getCode() {
            return code;
        }

        public void setErrorCode(int code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getRequestId() {
            return requestId;
        }

        public void setRequestId(String requestId) {
            this.requestId = requestId;
        }

        public String getTimeTransfer() {
            return timeTransfer;
        }

        public void setTimeTransfer(String timeTransfer) {
            this.timeTransfer = timeTransfer;
        }
    }
}
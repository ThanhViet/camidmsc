package com.metfone.selfcare.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.HttpMethod;
import com.metfone.selfcare.common.api.http.HttpProgressCallBack;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.LocalSongInfo;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.fragment.setting.EditProfileFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.helper.message.ProgressFile;
import com.metfone.selfcare.network.file.DownloadListener;
import com.metfone.selfcare.network.file.DownloadManager;
import com.metfone.selfcare.network.file.DownloadRequest;
import com.metfone.selfcare.network.file.UploadListener;
import com.metfone.selfcare.network.file.UploadManager;
import com.metfone.selfcare.network.file.UploadRequest;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.viettel.util.LogDebugHelper;

import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import static com.metfone.selfcare.common.api.video.HttpUtils.createBuilder;

/**
 * Created by toanvk2 on 9/6/2016.
 */
public class TransferFileBusiness {
    private static final String TAG = TransferFileBusiness.class.getSimpleName();
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private ReengAccountBusiness mAccountBusiness;
    private DownloadManager mDownloadManager;
    private UploadManager mUploadManager;

    private SharedPreferences mPref;

    public TransferFileBusiness(ApplicationController app) {
        this.mApplication = app;
        mDownloadManager = new DownloadManager(mApplication);
        mUploadManager = new UploadManager(mApplication);
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    private void getBusiness() {
        mMessageBusiness = mApplication.getMessageBusiness();
        mAccountBusiness = mApplication.getReengAccountBusiness();
    }

    public DownloadManager getDownloadManager() {
        return mDownloadManager;
    }

    public UploadManager getUploadManager() {
        return mUploadManager;
    }

    public void cancelUpload(UploadRequest request) {
        if (request == null) return;
        mUploadManager.cancel(request.getUploadId());
    }

    public void cancelUploadFileMessage(ReengMessage message) {
        if (message == null) return;
        mUploadManager.cancel(message);
    }

    private DownloadListener downloadFileMessageListener = new DownloadListener() {

        long time;

        @Override
        public void onDownloadStarted(DownloadRequest request) {
            ListenerHelper.getInstance().onDownloadStated(request);
            time = System.currentTimeMillis();
            Object container = request.getContainer();
            if (container instanceof ReengMessage) {
                ((ReengMessage) container).setPlaying(true);
            }
        }

        @Override
        public void onDownloadComplete(DownloadRequest downloadRequest) {
            Object container = downloadRequest.getContainer();
            if (container instanceof ReengMessage) {
                ReengMessage message = (ReengMessage) container;
                message.setPlaying(false);
                mMessageBusiness = mApplication.getMessageBusiness();
                message.setFilePath(downloadRequest.getFilePath());
                if (message.getDirection() == ReengMessageConstant.Direction.send
                        && message.getMessageType() == ReengMessageConstant.MessageType.image
                        && !TextUtils.isEmpty(message.getFileId())
                        && !TextUtils.isEmpty(message.getDirectLinkMedia())) {

                } else {
                    message.setStatus(ReengMessageConstant.STATUS_RECEIVED);
                }
                if (SettingBusiness.getInstance(mApplication).getPrefShowMedia()) {
                    FileHelper.refreshGallery(mApplication, downloadRequest.getFilePath());
                }
                if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
                    message.setVideoContentUri(FileHelper.getVideoContentUri(mApplication, downloadRequest
                            .getFilePath()));
                }
                mMessageBusiness.updateAllFieldsOfMessage(message);
                mMessageBusiness.refreshThreadWithoutNewMessage(message.getThreadId());
                long deltaTime = System.currentTimeMillis() - time;
                LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.DOWNLOAD_IMAGE_CHAT,
                        String.valueOf(deltaTime), String.valueOf(time),
                        Constants.LogKQI.StateKQI.SUCCESS.getValue() + "|" + message.getPacketId());
            } else {
                Log.e(TAG, "download success not message file");
            }
            ListenerHelper.getInstance().onDownloadComplete(downloadRequest);
        }

        @Override
        public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
            Object container = downloadRequest.getContainer();
            if (container instanceof ReengMessage) {
                ReengMessage message = (ReengMessage) container;
                message.setPlaying(false);
                if (message.getDirection() == ReengMessageConstant.Direction.send
                        && message.getMessageType() == ReengMessageConstant.MessageType.image
                        && !TextUtils.isEmpty(message.getFileId())
                        && !TextUtils.isEmpty(message.getDirectLinkMedia())) {

                } else {
                    mMessageBusiness = mApplication.getMessageBusiness();
                    message.setStatus(ReengMessageConstant.STATUS_FAIL);
                    mMessageBusiness.updateAllFieldsOfMessage(message);
                    mMessageBusiness.refreshThreadWithoutNewMessage(message.getThreadId());
                }
                long deltaTime = System.currentTimeMillis() - time;
                LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.DOWNLOAD_IMAGE_CHAT,
                        String.valueOf(deltaTime), String.valueOf(time),
                        errorCode + "|" + message.getPacketId());
            } else {
                Log.e(TAG, "download fail not message file");
            }
        }

        @Override
        public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
            Log.d(TAG, "download progress: " + progress);
            ListenerHelper.getInstance().onDownloadProgress(downloadRequest, totalBytes, downloadedBytes, progress);
        }
    };

    /*private UploadListener uploadFileMessageListener = new UploadListener() {
        @Override
        public void onUploadStarted(UploadRequest uploadRequest) {
            Object container = uploadRequest.getContainer();
            if (container instanceof ReengMessage) {
                ReengMessage message = (ReengMessage) container;
                message.setStatus(ReengMessageConstant.STATUS_LOADING);
                mApplication.getMessageBusiness().refreshThreadWithoutNewMessage(message.getThreadId());
                ReengNotificationManager.getInstance(mApplication).drawNotificationProgress(message.getFileName());
                time = System.currentTimeMillis();
            }
        }

        @Override
        public void onUploadComplete(UploadRequest uploadRequest, String response) {
            Log.d(TAG, "upload complete: " + response);
            Object container = uploadRequest.getContainer();
            if (container instanceof ReengMessage) {
                try {
                    JSONObject object = new JSONObject(response);
                    int errorCode = object.optInt(Constants.HTTP.REST_ERROR_CODE, -1);
                    if (errorCode == Constants.HTTP.CODE_SUCCESS) {
                        mMessageBusiness = mApplication.getMessageBusiness();
                        String fileId = object.optString(Constants.HTTP.REST_DESC, null);
                        String videoThumb = object.optString(Constants.HTTP.REST_THUMB, null);
                        String directLink = object.optString("link", null);
                        ReengMessage message = (ReengMessage) container;
                        ThreadMessage threadMessage = mMessageBusiness.getThreadById(message.getThreadId());
                        if (threadMessage == null) return;
                        message.setImageUrl(videoThumb);
                        message.setFileId(fileId);
                        message.setDirectLinkMedia(directLink);
                        mMessageBusiness.sendXMPPMessage(message, threadMessage);
                        mMessageBusiness.updateAllFieldsOfMessage(message);
                        if (message.getMessageType() == ReengMessageConstant.MessageType.voicemail) {
                            message.setPlaying(false);
                        } else {
                            message.setPlaying(true);// progress fake upload image
                        }
                        mMessageBusiness.refreshThreadWithoutNewMessage(message.getThreadId());
                        if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
                            FileHelper.deleteFile(mApplication, uploadRequest.getFilePath());
                        }
                        ProgressFile progressFile = new ProgressFile(message, mApplication);
                        progressFile.showProgressSecond();
                        ReengNotificationManager.getInstance(mApplication).updateNotificationProgress("completed " +
                                "take: " + (System.currentTimeMillis() - time) + " ms", 100);
                    } else {
                        onUploadFailed(uploadRequest, -3, response);
                        ReengNotificationManager.getInstance(mApplication).updateNotificationProgress("upload fail: "
                                + errorCode, 0);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    onUploadFailed(uploadRequest, -3, response);
                    ReengNotificationManager.getInstance(mApplication).updateNotificationProgress(e.getMessage(), 0);
                }
            }
        }

        @Override
        public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
            Log.d(TAG, "upload failed: " + errorCode + " ," + errorMessage);
            Object container = uploadRequest.getContainer();
            if (container instanceof ReengMessage) {
                mMessageBusiness = mApplication.getMessageBusiness();
                ReengMessage message = (ReengMessage) container;
                message.setStatus(ReengMessageConstant.STATUS_FAIL);
                mMessageBusiness.updateAllFieldsOfMessage(message);
                mMessageBusiness.refreshThreadWithoutNewMessage(message.getThreadId());
                if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {// neu la video thi cho
                    // fail luon
                    FileHelper.deleteFile(mApplication, uploadRequest.getFilePath());
                }
                ReengNotificationManager.getInstance(mApplication).updateNotificationProgress(errorMessage, 0);
            }
        }

        @Override
        public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes,
                               int progress, long speed) {
            Log.d(TAG, "onProgress: " + progress + " speed: " + speed);
            //            String desc = "File length: " + totalBytes + " kb  --  speed: " + speed + " kb/s";
            //TODO luong upload mơi gọi hàm này liên tục, làm tràn stack update notification, tạm thời ko show
            // notification này nữa.
            //ReengNotificationManager.getInstance(mApplication).updateNotificationProgress(desc, progress);
            //Toast.makeText(mApplication, "upload file -> length: " + totalBytes + " speed: " + speed + "kb/s - " +
            // progress + "%", Toast.LENGTH_LONG).show();
        }
    };*/

    public void startDownloadMessageFile(ReengMessage message) {
        if (message == null) return;
        getBusiness();
        String userEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        String token = mAccountBusiness.getToken();
        if (TextUtils.isEmpty(message.getFileId()) && message.getSongId() != -1) {
            message.setFileId(String.valueOf(message.getSongId()));
            mMessageBusiness.updateAllFieldsOfMessage(message);
        }
        String downloadUrl;
        if (TextUtils.isEmpty(message.getDirectLinkMedia())) {
            downloadUrl = String.format(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfImage(Config.UrlEnum
                            .FILE_DOWNLOAD_URL),
                    userEncode, token, message.getFileId());
        } else if (message.getMessageType() == ReengMessageConstant.MessageType.image) {
            downloadUrl = UrlConfigHelper.getInstance(mApplication).getDomainImage() + message
                    .getDirectLinkMedia();
        } else {
            downloadUrl = UrlConfigHelper.getInstance(mApplication).getDomainFile() + message
                    .getDirectLinkMedia();
        }

        Log.d(TAG, "downloadUrl: " + downloadUrl);
        String filePath = getFilePathOfMessage(message);
        try {
            DownloadRequest request = new DownloadRequest(downloadUrl)
                    .setFilePath(filePath)
                    .setContainer(message)
                    .setDownloadListener(downloadFileMessageListener);
            mDownloadManager.add(request);
            if (message.getDirection() == ReengMessageConstant.Direction.received)
                message.setStatus(ReengMessageConstant.STATUS_LOADING);
            else
                message.setPlaying(true);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            if (message.getDirection() == ReengMessageConstant.Direction.received)
                message.setStatus(ReengMessageConstant.STATUS_FAIL);
            else
                message.setPlaying(false);
        }
        mMessageBusiness.updateAllFieldsOfMessage(message);
        mMessageBusiness.refreshThreadWithoutNewMessage(message.getThreadId());
    }

    private String getNewFilePath(ReengMessage message) {
        if (TextUtils.isEmpty(message.getFilePath())) return null;
        File file = new File(message.getFilePath());
        if (file.exists()) {
            if (message.getMessageType() == ReengMessageConstant.MessageType.image &&
                    file.length() > com.metfone.selfcare.helper.Constants.MESSAGE.MAX_TRANSFER_PHOTO_SIZE) {
                boolean isHD = SettingBusiness.getInstance(mApplication).getPrefEnableHDImage();
                String compressedFilePath = MessageHelper.getPathOfCompressedFile(
                        message.getFilePath(), Config.Storage.IMAGE_COMPRESSED_FOLDER, message.getPacketId(), isHD);
                if (compressedFilePath != null)
                    return compressedFilePath;
            }
            return message.getFilePath();
        }
        return null;
    }

    public void startUploadMessageFile(final ReengMessage message) {
        if (message.isUploading()) return;
        /*String newFile = getNewFilePath(message);
        if (TextUtils.isEmpty(newFile)) {
            onUploadImageChatFail(message, "", "");
            return;
        }*/
        message.setStatus(ReengMessageConstant.STATUS_LOADING);
        mApplication.getMessageBusiness().refreshThreadWithoutNewMessage(message.getThreadId());
//        ReengNotificationManager.getInstance(mApplication).drawNotificationProgress(message.getFileName());
        final long time = System.currentTimeMillis();

        getBusiness();
        String baseUrl = UrlConfigHelper.getInstance(mApplication).getDomainImage();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlByKey(Config.UrlEnum
                .FILE_UPLOAD_URL);

        String filePathOrigi = message.getFilePath();
        final File fileImage = MessageHelper.checkFile(filePathOrigi, MessageHelper.getNameFileImage(message.getPacketId()), mApplication);

        if (fileImage == null || !fileImage.exists()) {
            LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.UPLOAD_IMAGE_CHAT,
                    String.valueOf(0), String.valueOf(time),
                    Constants.LogKQI.StateKQI.UPLOAD_FILE_NOT_FOUND.getValue());
            onUploadImageChatFail(message, filePathOrigi, "");
            return;
        }
        message.setFilePathCompress(fileImage.getAbsolutePath());
        message.setUploading(true);
        createBuilder(mApplication)
                .setBaseUrl(baseUrl)
                .setUrl(url)
                .setMethod(HttpMethod.FILE)
                .putFile(Constants.HTTP.FILE.REST_UP_FILE, fileImage.getAbsolutePath())
                .putParameter(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                .putParameter(Constants.HTTP.FILE.REST_FILE_TYPE, message.getMessageType().toString())
                .putParameter(Constants.HTTP.REST_TOKEN, mAccountBusiness.getToken())
                .withCallBack(new HttpProgressCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        Log.d(TAG, "onUploadComplete: " + response + " time success: " + (System.currentTimeMillis() - time));
                        message.setUploading(false);
                        try {
                            JSONObject object = new JSONObject(response);
                            int errorCode = object.optInt(Constants.HTTP.REST_ERROR_CODE, -1);
                            if (errorCode == Constants.HTTP.CODE_SUCCESS) {
                                mMessageBusiness = mApplication.getMessageBusiness();
                                String fileId = object.optString(Constants.HTTP.REST_DESC, null);
                                String videoThumb = object.optString(Constants.HTTP.REST_THUMB, null);
                                String directLink = object.optString("link", null);
                                ThreadMessage threadMessage = mMessageBusiness.getThreadById(message.getThreadId());
                                if (threadMessage == null) return;
                                message.setImageUrl(videoThumb);
                                message.setFileId(fileId);
                                message.setDirectLinkMedia(directLink);
                                if (message.getMessageType() == ReengMessageConstant.MessageType.voicemail) {
                                    message.setPlaying(false);
                                } else {
                                    message.setPlaying(true);// progress fake upload image
                                }
                                if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
                                    FileHelper.deleteFile(mApplication, fileImage.getAbsolutePath());
                                } else if (message.getMessageType() == ReengMessageConstant.MessageType.image) {
                                    message.setDataResponseUpload(response);
                                    String currentFilePath = message.getFilePath();

                                    String path = Config.Storage.REENG_STORAGE_FOLDER
                                            + Config.Storage.CACHE_FOLDER + "/" + ImageHelper.IMAGE_NAME;
                                    if (currentFilePath.startsWith(path)) {
                                        FileHelper.deleteFile(mApplication, currentFilePath);
                                    } else {
                                        mMessageBusiness.addMessageImageDB(message, currentFilePath);
                                    }
                                    message.setFilePath(message.getFilePathCompress());
                                }
                                ProgressFile progressFile = new ProgressFile(message, mApplication);
                                progressFile.showProgressSecond();
                                /*ReengNotificationManager.getInstance(mApplication).updateNotificationProgress("completed " +
                                        "take: " + (System.currentTimeMillis() - time) + " ms", 100);*/
                                long deltatime = System.currentTimeMillis() - time;
                                LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.UPLOAD_IMAGE_CHAT,
                                        String.valueOf(deltatime), String.valueOf(time),
                                        Constants.LogKQI.StateKQI.SUCCESS.getValue(), true);
                                String device = Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL;
                                StringBuilder sb = new StringBuilder();
                                sb.append(deltatime);
                                sb.append("|ANDROID|");
                                sb.append(Config.REVISION);
                                sb.append("|").append(NetworkHelper.getNetworkSubType(mApplication)).append("|");
                                sb.append(device);
                                message.setKqiContent(sb.toString());

                                mMessageBusiness.sendXMPPMessage(message, threadMessage);
                                mMessageBusiness.updateAllFieldsOfMessage(message);
                                mMessageBusiness.refreshThreadWithoutNewMessage(message.getThreadId());

                            } else {
                                message.setUploading(false);
                                onUploadImageChatFail(message, fileImage.getAbsolutePath(), response);
                                /*ReengNotificationManager.getInstance(mApplication).updateNotificationProgress("upload fail: "
                                        + errorCode, 0);*/
                                LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.UPLOAD_IMAGE_CHAT,
                                        String.valueOf((System.currentTimeMillis() - time)), String.valueOf(time),
                                        String.valueOf(errorCode), true);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            message.setUploading(false);
                            onUploadImageChatFail(message, fileImage.getAbsolutePath(), response);
//                            ReengNotificationManager.getInstance(mApplication).updateNotificationProgress(e.getMessage(), 0);
                            LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.UPLOAD_IMAGE_CHAT,
                                    String.valueOf((System.currentTimeMillis() - time)), String.valueOf(time),
                                    Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue(), true);
                        }
                    }

                    @Override
                    public void onProgressUpdate(int position, int sum, int percentage) {
                        Log.d(TAG, "percentage: " + percentage);
                        //            String desc = "File length: " + totalBytes + " kb  --  speed: " + speed + " kb/s";
                        //TODO luong upload mơi gọi hàm này liên tục, làm tràn stack update notification, tạm thời ko show
                        // notification này nữa.
                        //ReengNotificationManager.getInstance(mApplication).updateNotificationProgress(desc, progress);
                        //Toast.makeText(mApplication, "upload file -> length: " + totalBytes + " speed: " + speed + "kb/s - " +
                        // progress + "%", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(String msg) {
                        message.setUploading(false);
                        onUploadImageChatFail(message, fileImage.getAbsolutePath(), msg);
                        LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.UPLOAD_IMAGE_CHAT,
                                String.valueOf((System.currentTimeMillis() - time)), String.valueOf(time),
                                Constants.LogKQI.StateKQI.UPLOAD_FILE_FAIL.getValue(), true);
                        super.onFailure(msg);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                })
                .execute();
    }

    private void onUploadImageChatFail(ReengMessage message, String oriPath, String msg) {
        if (message.getMessageType() == ReengMessageConstant.MessageType.image) return;
        mMessageBusiness = mApplication.getMessageBusiness();
        message.setStatus(ReengMessageConstant.STATUS_FAIL);
        mMessageBusiness.updateAllFieldsOfMessage(message);
        mMessageBusiness.refreshThreadWithoutNewMessage(message.getThreadId());
        if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {// neu la video thi cho
            // fail luon
            FileHelper.deleteFile(mApplication, oriPath);
        }
//        ReengNotificationManager.getInstance(mApplication).updateNotificationProgress(msg, 0);
    }

    private String getFilePathOfMessage(ReengMessage message) {
        String fileType = message.getFileType();
        StringBuilder fileSb = new StringBuilder(Config.Storage.REENG_STORAGE_FOLDER);
        if (fileType.equals(ReengMessagePacket.FileType.image.toString())) {
            fileSb.append(Config.Storage.IMAGE_FOLDER);
        } else if (fileType.equals(ReengMessagePacket.FileType.voicemail.toString())) {
            fileSb.append(Config.Storage.VOICEMAIL_FOLDER);
        } else if (fileType.equals(ReengMessagePacket.FileType.sharevideov2.toString())) {
            fileSb.append(Config.Storage.VIDEO_FOLDER);
        } else {
            return getFilePathOfFileMessage(message);
        }
        if (message.getMessageType() == ReengMessageConstant.MessageType.image) {
            fileSb.append("/").append(MessageHelper.getNameFileImage(message.getPacketId()));
            /*if (!TextUtils.isEmpty(message.getDirectLinkMedia())) {
            } else {
                fileSb.append("/").append(MessageHelper.getNameFileImage(message.getFileName()));
            }*/
        } else {
            fileSb.append("/").append(message.getFileName());
        }
        return fileSb.toString();
    }

    private String getFilePathOfFileMessage(ReengMessage message) {
        StringBuilder fileSb = new StringBuilder(Config.Storage.FILE_DOCUMENT_MOCHA);
        fileSb.append("/").append(MessageHelper.getUniqueDocFileName(message.getFileName(), message.getFileId()));
        return fileSb.toString();
       /* File recordDir = new File(Config.Storage.FILE_DOCUMENT_MOCHA);
        if (!recordDir.exists()) {
            recordDir.mkdirs();
        }
        String saveFileName = "received_" + message.getFileId() + ".jpg";
        File saveImage = new File(recordDir, saveFileName);*/
    }

    //=====================================================================
    public void startUploadLog(final boolean showToast) {
        Log.i(TAG, "startUploadLog");
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        LogDebugHelper.getInstance(mApplication).saveDataToFile(new LogDebugHelper.SaveFileListener() {
            @Override
            public void onSaveFileSuccess(String filePath) {
                if (showToast)
                    Toast.makeText(mApplication, "save file to: " + filePath, Toast.LENGTH_SHORT).show();
                Log.i(TAG, "onSaveFileSuccess: " + filePath);
                if (!TextUtils.isEmpty(filePath))
                    uploadLogDebug(filePath, showToast);
            }
        });
    }

    private void uploadLogDebug(String filePath, final boolean showToast) {
        Log.i(TAG, "uploadLogDebug: " + filePath);
        String uploadUrl = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .UPLOAD_LOG_DEBUG);

        UploadListener uploadLogDebugListener = new UploadListener() {
            @Override
            public void onUploadStarted(UploadRequest uploadRequest) {

            }

            @Override
            public void onUploadComplete(UploadRequest uploadRequest, String response) {
                Log.i(TAG, "onUploadComplete: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.optInt("errorCode", 0);
                    if (code == 200) {
                        if (showToast)
                            Toast.makeText(mApplication, "upload log success", Toast.LENGTH_SHORT).show();
                        mPref.edit().putLong(LogDebugHelper.PREF_UPLOAD_LOG_DEBUG_LAST_TIME, System.currentTimeMillis()).apply();
                        mApplication.clearDBLogDebug();
                        FileHelper.deleteFile(mApplication, uploadRequest.getFilePath());
                    } else {
                        if (showToast)
                            Toast.makeText(mApplication, "upload log failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onUploadComplete Exception", e);
                    if (showToast)
                        Toast.makeText(mApplication, "upload log Exception", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
                if (showToast)
                    Toast.makeText(mApplication, "upload log failed: " + errorMessage, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress, long
                    speed) {

            }
        };

        try {
            UploadRequest request = new UploadRequest(uploadUrl)
                    .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                    .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
                    .addCustomHeader(BaseApi.MOCHA_API, mAccountBusiness.getMochaApi())
                    .addCustomHeader(BaseApi.UUID, Utilities.getUuidApp())
                    .setFilePath(filePath)
                    .setContainer(filePath)
                    .setUploadListener(uploadLogDebugListener);
            mUploadManager.add(request);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    //==========================UPLOAD SONG/COVER/AVATAR/PROFILE============================================

    public UploadRequest uploadSong(UserInfo keengInfo, LocalSongInfo songInfo, UploadListener listener) {
        String uploadUrl = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfMedia2Keeng(Config.UrlKeengEnum
                .MEDIA_UPLOAD_SONG);
        //   String uploadUrl = "http://vip.medias4.cdn.keeng.vn:8089/uploadv4.php";
        try {
            UploadRequest request = new UploadRequest(uploadUrl)
                    .addParams(Constants.HTTP.KEENG_USER_ID, keengInfo.getId())
                    .addParams(Constants.HTTP.KEENG_SESSION_TOKEN_UPLOAD, keengInfo.getSessionToken())
                    .addParams(Constants.HTTP.KEENG_MEDIA_NAME, songInfo.getName())
                    .addParams(Constants.HTTP.KEENG_SINGER_NAME, songInfo.getSinger())
                    .setFormData(Constants.HTTP.FILE.REST_UP_MUSIC)
                    .setFilePath(songInfo.getPath())
                    .setContainer(songInfo)
                    .setUploadListener(listener);
            mUploadManager.add(request);
            return request;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public UploadRequest uploadAvatar(String filePath, UploadListener listener) {
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        String uploadUrl = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfFile(Config.UrlEnum.AVATAR_UPLOAD);
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
        try {
            UploadRequest request = new UploadRequest(uploadUrl)
                    .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                    .addParams(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime))
                    .addParams(Constants.HTTP.DATA_SECURITY, dataEncrypt)
                    .addCustomHeader(BaseApi.MOCHA_API, mAccountBusiness.getMochaApi())
                    .addCustomHeader(BaseApi.UUID, Utilities.getUuidApp())
                    .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
                    .setFilePath(filePath)
                    .setContainer(filePath)
                    .setUploadListener(listener);
            mUploadManager.add(request);
            return request;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public UploadRequest uploadAvatarGroup(String groupId, String filePath, UploadListener listener) {
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        String uploadUrl = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .AVATAR_GROUP_UPLOAD);
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(groupId).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());

        try {
            UploadRequest request = new UploadRequest(uploadUrl)
                    .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                    .addParams(Constants.HTTP.REST_GROUP_ID, groupId)
                    .addParams(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime))
                    .addParams(Constants.HTTP.DATA_SECURITY, dataEncrypt)
                    .addCustomHeader(BaseApi.MOCHA_API, mAccountBusiness.getMochaApi())
                    .addCustomHeader(BaseApi.UUID, Utilities.getUuidApp())
                    .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
                    .setFilePath(filePath)
                    .setContainer(filePath)
                    .setUploadListener(listener);
            mUploadManager.add(request);
            return request;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public UploadRequest uploadCover(String filePath, UploadListener listener) {
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        String uploadUrl = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfFile(Config.UrlEnum.IMAGE_COVER_UPLOAD_URL);
        try {
            UploadRequest request = new UploadRequest(uploadUrl)
                    .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                    .addParams(Constants.HTTP.FILE.REST_FILE_TYPE, ImageProfileConstant.TYPE_UPLOAD)
                    .addParams(Constants.HTTP.REST_TOKEN, mAccountBusiness.getToken())
                    .addCustomHeader(BaseApi.MOCHA_API, mAccountBusiness.getMochaApi())
                    .addCustomHeader(BaseApi.UUID, Utilities.getUuidApp())
                    .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
                    .setFilePath(filePath)
                    .setContainer(filePath)
                    .setUploadListener(listener);
            mUploadManager.add(request);
            return request;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public UploadRequest uploadCoverWhenConnected(String filePath) {
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        String uploadUrl = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfFile(Config.UrlEnum.IMAGE_COVER_UPLOAD_URL);
        try {
            UploadRequest request = new UploadRequest(uploadUrl)
                    .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                    .addParams(Constants.HTTP.FILE.REST_FILE_TYPE, ImageProfileConstant.TYPE_UPLOAD)
                    .addParams(Constants.HTTP.REST_TOKEN, mAccountBusiness.getToken())
                    .addCustomHeader(BaseApi.MOCHA_API, mAccountBusiness.getMochaApi())
                    .addCustomHeader(BaseApi.UUID, Utilities.getUuidApp())
                    .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
                    .setFilePath(filePath)
                    .setContainer(filePath)
                    .setUploadListener(uploadCoverWhenConnectedListener);
            mUploadManager.add(request);
            return request;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    private UploadListener uploadCoverWhenConnectedListener = new UploadListener() {
        @Override
        public void onUploadStarted(UploadRequest uploadRequest) {

        }

        @Override
        public void onUploadComplete(UploadRequest uploadRequest, String response) {
            Log.i(TAG, "response upload cover: " + response);
            try {
                JSONObject object = new JSONObject(response);
                int errorCode = -1;
                if (object.has(Constants.HTTP.REST_ERROR_CODE)) {
                    errorCode = object.getInt(Constants.HTTP.REST_ERROR_CODE);
                }
                if (errorCode == 200) {
                    ImageProfile cover = mApplication.getImageProfileBusiness().getImageCover();
                    String id = "";
                    if (cover == null) {
                        cover = new ImageProfile();
                        cover.setTypeImage(ImageProfileConstant.IMAGE_COVER);
                    }
                    if (object.has(Constants.HTTP.REST_LINK))
                        cover.setImageUrl(object.getString(Constants.HTTP.REST_LINK));
                    if (object.has(Constants.HTTP.REST_DESC)) {
                        id = object.getString(Constants.HTTP.REST_DESC);
                        cover.setIdServerString(id);
                    }
                    cover.setUpload(true);
                    cover.setTypeImage(ImageProfileConstant.IMAGE_COVER);
                    cover.setImagePathLocal(uploadRequest.getFilePath());
                    cover.setTime((new Date()).getTime());

                    ListenerHelper.getInstance().onCoverChange(uploadRequest.getFilePath());
                    mApplication.getImageProfileBusiness().insertOrUpdateCover(cover);
                    handleUploadAlbumToOnMedia(id);
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }

        @Override
        public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
            Log.e(TAG, "onUploadFailed cover code: " + errorMessage + " msg: " + errorMessage);
        }

        @Override
        public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress, long
                speed) {

        }
    };

    private void handleUploadAlbumToOnMedia(String idCover) {
        ArrayList<String> list = new ArrayList<>();
        list.add(idCover);
        WSOnMedia rest = new WSOnMedia(mApplication);
        rest.uploadAlbumToOnMedia(list, FeedContent.ITEM_TYPE_PROFILE_COVER, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "onResponse: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse: " + volleyError.toString());
            }
        });
    }

    private ArrayList<String> mListIdImageUpload;
    ArrayList<UploadRequest> listUploadPending;

    public void uploadAlbumImage(ArrayList<String> filePath, final UploadImageProfileListener
            uploadImageProfileListener) {
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        mListIdImageUpload = new ArrayList<>();
        listUploadPending = new ArrayList<>();
        FileHelper fileHelper = new FileHelper();

        final String uploadUrl = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfImage(Config.UrlEnum.IMAGE_PROFILE_UPLOAD_URL);
        for (int i = 0; i < filePath.size(); i++) {
            final int index = i;
            try {
                final UploadListener uploadListener = new UploadListener() {
                    @Override
                    public void onUploadStarted(UploadRequest uploadRequest) {

                    }

                    @Override
                    public void onUploadComplete(UploadRequest uploadRequest, String response) {
                        processResponse(response, uploadRequest, uploadImageProfileListener);
                    }

                    @Override
                    public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {

                    }

                    @Override
                    public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int
                            progress, long speed) {

                    }
                };

                MessageHelper.getPathOfCompressedFile(filePath.get(i), Config.Storage.IMAGE_COMPRESSED_FOLDER, false, new
                        FileHelper.CompressImageListener() {
                            @Override
                            public void onCompressSuccess(String filePath, String ratio) {
                                String timeStamp = String.valueOf(System.currentTimeMillis());
                                StringBuilder sb = new StringBuilder();
                                sb.append(mAccountBusiness.getJidNumber()).append(mAccountBusiness.getRegionCode()).
                                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                                        append(ImageProfileConstant.TYPE_UPLOAD).append(mAccountBusiness.getToken())
                                        .append
                                                (timeStamp);
                                UploadRequest request = new UploadRequest(uploadUrl)
                                        .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                                        .addParams(Constants.HTTP.FILE.REST_FILE_TYPE, ImageProfileConstant.TYPE_UPLOAD)
                                        .addParams(Constants.HTTP.REST_LANGUAGE_CODE, mApplication
                                                .getReengAccountBusiness()
                                                .getCurrentLanguage())
                                        .addParams(Constants.HTTP.REST_COUNTRY_CODE, mAccountBusiness.getRegionCode())
                                        .addParams(Constants.HTTP.TIME_STAMP, timeStamp)
                                        .addParams(Constants.HTTP.REST_RATIO, ratio)
                                        .addParams(Constants.HTTP.DATA_SECURITY,
                                                HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness
                                                        .getToken()))
                                        .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
                                        .setFilePath(filePath)
                                        .setContainer(filePath)
                                        .setUploadListener(uploadListener);
                                listUploadPending.add(request);
                                mUploadManager.add(listUploadPending.get(index));
                            }

                            @Override
                            public void onCompressFail() {
                                uploadImageProfileListener.onUploadFailed(402, "", 0);
                            }
                        });
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    private void processResponse(String responseStr, UploadRequest uploadRequest, UploadImageProfileListener
            uploadImageProfileListener) {
        try {
            JSONObject object = new JSONObject(responseStr);
            int errorCode;
            errorCode = object.optInt(Constants.HTTP.REST_ERROR_CODE);
            if (errorCode == 200) {
                String desc = object.optString(Constants.HTTP.REST_DESC);
                String imageLink = object.optString(Constants.HTTP.REST_LINK);
                ImageProfile imageProfile = new ImageProfile();
                imageProfile.setImagePathLocal(uploadRequest.getFilePath());
                imageProfile.setTypeImage(ImageProfileConstant.IMAGE_NORMAL);
                imageProfile.setImageUrl(imageLink);
                imageProfile.setUpload(true);
                imageProfile.setIdServerString(desc);
                imageProfile.setTime((new Date()).getTime());
                mApplication.getImageProfileBusiness().insertImageProfile(imageProfile);
                mListIdImageUpload.add(desc);
                Log.d(TAG, "upload image profile : " + imageProfile.getTypeImage());
                if (imageProfile.getTypeImage() == ImageProfileConstant.IMAGE_NORMAL) {//upload album
                    LuckyWheelHelper.getInstance(mApplication).doMission(Constants.LUCKY_WHEEL.ITEM_UPLOAD_ALBUM);
                }
                listUploadPending.remove(uploadRequest);
                if (listUploadPending.size() == 0) {
                    if (mListIdImageUpload.size() != 0) {
                        handleUploadAlbumToOnMedia();
                        uploadImageProfileListener.onUploadCompleted(mListIdImageUpload.size());
                    } else {
                        uploadImageProfileListener.onUploadFailed(-1, null, mListIdImageUpload.size());
                    }
                }
            } else if (errorCode == Constants.HTTP.CODE_UPLOAD_FAILED_EXCEEDED) {
                String mess = object.optString(Constants.HTTP.REST_DESC);
                uploadImageProfileListener.onUploadFailed(Constants.HTTP.CODE_UPLOAD_FAILED_EXCEEDED, mess,
                        mListIdImageUpload.size());
                removeAllRequestUploadImage();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            //fail 1 cai van tiep tuc upload neu con
            listUploadPending.remove(uploadRequest);
        }
    }

    private void handleUploadAlbumToOnMedia() {
        if (!mListIdImageUpload.isEmpty()) {
            WSOnMedia rest = new WSOnMedia(mApplication);
            rest.uploadAlbumToOnMedia(mListIdImageUpload, FeedContent.ITEM_TYPE_PROFILE_ALBUM, new Response
                    .Listener<String>() {
                @Override
                public void onResponse(String s) {
                    Log.i(TAG, "onResponse: " + s);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e(TAG, "onErrorResponse: " + volleyError.toString());
                }
            });
        }
    }

    private void removeAllRequestUploadImage() {
        if (listUploadPending != null && !listUploadPending.isEmpty()) {
            for (UploadRequest uploadRequest : listUploadPending) {
                uploadRequest.cancel();
            }
            listUploadPending.clear();
        }

    }

    private ArrayList<String> mListResponseImageSocial;
    ArrayList<UploadRequest> listUploadImgSocialPending;

    public void uploadImageSocialOnMedia(ArrayList<String> filePath, final UploadImageSocialOnMediaListener
            listener) {
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        mListResponseImageSocial = new ArrayList<>();
        listUploadImgSocialPending = new ArrayList<>();
        final String uploadUrl = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfImage(Config.UrlEnum.UPLOAD_IMAGE_SOCIAL_ONMEDIA);
        for (int i = 0; i < filePath.size(); i++) {
            final int index = i;
            try {
                final UploadListener uploadListener = new UploadListener() {
                    @Override
                    public void onUploadStarted(UploadRequest uploadRequest) {

                    }

                    @Override
                    public void onUploadComplete(UploadRequest uploadRequest, String response) {
                        handleUploadImageSocial(uploadRequest, response, listener);
//                        if (listener != null) listener.onUploadCompleted(response);
                    }

                    @Override
                    public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
//                        if (listener != null) listener.onUploadFailed(errorCode, errorMessage);
                    }

                    @Override
                    public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int
                            progress, long speed) {

                    }
                };

                MessageHelper.getPathOfCompressedFile(filePath.get(i), Config.Storage.IMAGE_COMPRESSED_FOLDER, false, new
                        FileHelper.CompressImageListener() {
                            @Override
                            public void onCompressSuccess(String filePath, String ratio) {
                                UploadRequest request = new UploadRequest(uploadUrl)
                                        .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                                        .addParams(Constants.HTTP.FILE.REST_FILE_TYPE, "image")
                                        .addParams(Constants.HTTP.REST_TOKEN, mAccountBusiness.getToken())
                                        .addParams(Constants.HTTP.REST_RATIO, ratio)
                                        .addCustomHeader(BaseApi.MOCHA_API, mAccountBusiness.getMochaApi())
                                        .addCustomHeader(BaseApi.UUID, Utilities.getUuidApp())
                                        .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
                                        .setFilePath(filePath)
                                        .setContainer(filePath)
                                        .setUploadListener(uploadListener);
                                listUploadImgSocialPending.add(request);
                                mUploadManager.add(listUploadImgSocialPending.get(index));
                            }

                            @Override
                            public void onCompressFail() {
                                if (listener != null)
                                    listener.onUploadFailed(402, "", new ArrayList<String>());
                            }
                        });
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    private void handleUploadImageSocial(UploadRequest uploadRequest, String response,
                                         UploadImageSocialOnMediaListener listener) {
        try {
            JSONObject object = new JSONObject(response);
            int errorCode;
            errorCode = object.optInt(Constants.HTTP.REST_ERROR_CODE);
            if (errorCode == 0) {
                mListResponseImageSocial.add(response);
                Log.d(TAG, "handleUploadImageSocial: " + response);
                listUploadImgSocialPending.remove(uploadRequest);
                if (listUploadImgSocialPending.size() == 0) {
                    if (mListResponseImageSocial.size() != 0) {
                        listener.onUploadCompleted(mListResponseImageSocial);
                    } else {
                        listener.onUploadFailed(-1, null, mListResponseImageSocial);
                    }
                }
            } else if (errorCode == Constants.HTTP.CODE_UPLOAD_FAILED_EXCEEDED) {
                String mess = object.optString(Constants.HTTP.REST_DESC);
                listener.onUploadFailed(Constants.HTTP.CODE_UPLOAD_FAILED_EXCEEDED, mess,
                        mListResponseImageSocial);
                removeAllRequestUploadImageSocial();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            //fail 1 cai van tiep tuc upload neu con
            listUploadImgSocialPending.remove(uploadRequest);
        }
    }

    private void removeAllRequestUploadImageSocial() {
        if (listUploadImgSocialPending != null && !listUploadImgSocialPending.isEmpty()) {
            for (UploadRequest uploadRequest : listUploadImgSocialPending) {
                uploadRequest.cancel();
            }
            listUploadImgSocialPending.clear();
        }

    }

    public UploadRequest uploadIdentityCard(String filePath, boolean isFrontCard,
                                            final EditProfileFragment.UploadIdentifyCardListener callBack) {
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        String compressedFilePath = MessageHelper.getPathOfCompressedFile(
                filePath, Config.Storage.IMAGE_COMPRESSED_FOLDER, "", true);
        if (TextUtils.isEmpty(compressedFilePath)) {
            if (callBack != null)
                callBack.onError(-1, mApplication.getResources().getString(R.string
                        .e601_error_but_undefined));
            return null;
        }
        final String uploadUrl = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfImage(Config.UrlEnum.AVNO_UPLOAD_IC);
        int uploadCardType = isFrontCard ? 1 : 2;
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(uploadCardType).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(mAccountBusiness.getToken()).
                append(currentTime);

        UploadListener listener = new UploadListener() {
            @Override
            public void onUploadStarted(UploadRequest uploadRequest) {

            }

            @Override
            public void onUploadComplete(UploadRequest uploadRequest, String response) {
                FileHelper.deleteFile(mApplication, uploadRequest.getFilePath());
                if (callBack == null) return;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.optInt("code");
                    if (code == 200) {
                        String link = jsonObject.optString("link");
                        if (!TextUtils.isEmpty(link)) {
                            callBack.onUploadSuccess(link);
                        } else {
                            callBack.onError(-1, mApplication.getResources().getString(R.string
                                    .e601_error_but_undefined));
                        }
                    } else {
                        callBack.onError(-1, mApplication.getResources().getString(R.string
                                .e601_error_but_undefined));
                    }
                } catch (Exception e) {
                    callBack.onError(-1, mApplication.getResources().getString(R.string
                            .e601_error_but_undefined));
                }
            }

            @Override
            public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
                if (callBack == null) return;
                callBack.onError(-1, mApplication.getResources().getString(R.string
                        .e601_error_but_undefined));
            }

            @Override
            public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress,
                                   long speed) {

            }
        };

        try {
            UploadRequest request = new UploadRequest(uploadUrl)
                    .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                    .addParams(Constants.HTTP.FILE.REST_FILE_TYPE, String.valueOf(uploadCardType))
                    .addParams(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                    .addParams(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime))
                    .addParams(Constants.HTTP.DATA_SECURITY,
                            HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness
                                    .getToken()))
                    .addCustomHeader(BaseApi.MOCHA_API, mAccountBusiness.getMochaApi())
                    .addCustomHeader(BaseApi.UUID, Utilities.getUuidApp())
                    .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
                    .setFilePath(compressedFilePath)
                    .setContainer(compressedFilePath)
                    .setUploadListener(listener);
            mUploadManager.add(request);
            return request;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public void startDownloadMessageImageSend(ReengMessage message) {
        if (message == null || message.getMessageType() != ReengMessageConstant.MessageType.image
                || TextUtils.isEmpty(message.getDirectLinkMedia())) return;
        getBusiness();
        if (TextUtils.isEmpty(message.getFileId()) && message.getSongId() != -1) {
            message.setFileId(String.valueOf(message.getSongId()));
            mMessageBusiness.updateAllFieldsOfMessage(message);
        }
        String downloadUrl = UrlConfigHelper.getInstance(mApplication).getDomainFile() + message.getDirectLinkMedia();
        Log.d(TAG, "downloadUrl: " + downloadUrl);
        String filePath = getFilePathOfMessage(message);
        try {
            DownloadRequest request = new DownloadRequest(downloadUrl)
                    .setFilePath(filePath)
                    .setContainer(message)
                    .setDownloadListener(downloadFileMessageListener);
            mDownloadManager.add(request);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        mMessageBusiness.updateAllFieldsOfMessage(message);
        mMessageBusiness.refreshThreadWithoutNewMessage(message.getThreadId());
    }

    public interface UploadImageSocialOnMediaListener {
        void onUploadCompleted(ArrayList<String> response);

        void onUploadFailed(int code, String msg, ArrayList<String> listSuccess);
    }

    public interface UploadImageProfileListener {
        void onUploadCompleted(int total);

        void onUploadFailed(int code, String mess, int total);
    }
}
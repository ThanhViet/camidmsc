package com.metfone.selfcare.business;

import com.metfone.selfcare.R;

/**
 * Created by ThanhNT on 9/17/2014.
 */
public class HTTPCode {
    //Code duoc sap xep theo thu tu tang dan
    //----------------1xx Code--------------------------
    public static final int E100_NOT_KEENG_USER = 100;
    //----------------2xx Code--------------------------
    public static final int E200_OK = 200; //200 E200_OK
    public static final int E201_OK = 201; //201 E200_SMS
    public static final int E202_OK = 202; //202 captcha

    //----------------3xx Code--------------------------

    //----------------4xx Code--------------------------
    public static final int E550_GENERATE_PASSWORD_OVER_LIMIT = 550; //qua so lan gui request sinh pass
    public static final int E403_GENOTP_OUT_OF_WHITELIST = 403;
    public static final int E404_NOT_FOUND = 404;
    public static final int E405_USER_INVAID = 405;
    //----------------5xx Code---------------------------
    public static final int E501_INTERNAL_SERVER_ERROR = 501;//loi xu ly cua server
    //----------------6xx Code---------------------------

    //----------------Auto detect phone number-----------
    public static final int E0_OK = 0;
    public static final int E_NOT_DETECT_DEVICE = -1;
    public static final int E_INTERNAL_SERVER_ERROR = -2;
    public static final int E_OUT_OF_WHITELIST = -3;

    //---------------Response info-----------------------
    public static final int E_TOKEN_INVALID = -2;
    public static final int E_TOKEN_OUT_OF_DATE = -3;
    public static final int E_SERVER_ERROR = 500;
    // stranger music
    public static final int E101_STRANGER_MUSIC_NO_PERMISSION = 101;
    public static final int E100_STRANGER_MUSIC_NOT_FOUND = 100;
    public static final int E205_STRANGER_MUSIC_ACCEPTED = 205;
    public static final int E206_STRANGER_MUSIC_TIME_OUT = 206;
    public static final int E0_STRANGER_MUSIC_CANCEL = 0;
    public static final int E401_NOT_AUTHEN = 401;
    //
    public static final int E206_CONTENT_SHARED = 206;

    /**
     * @param responeCode
     * @return string resource id of @responeCode
     */
    public static int getResourceOfCode(int responeCode) {
        int stringResourceId;
        switch (responeCode) {
            case E200_OK:
                stringResourceId = R.string.e200_ok;
                break;
            case E550_GENERATE_PASSWORD_OVER_LIMIT:
                stringResourceId = R.string.e550_password_over_limit;
                break;
            case E501_INTERNAL_SERVER_ERROR:
                stringResourceId = R.string.e500_internal_server_error;
                break;
            case E_INTERNAL_SERVER_ERROR:
                stringResourceId = R.string.e500_internal_server_error;
                break;
            case E405_USER_INVAID:
                stringResourceId = R.string.e405_user_invalid;
                break;
            default:
                stringResourceId = R.string.error_unidentify;
                break;
        }
        return stringResourceId;
    }
}

package com.metfone.selfcare.business;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.RoomChatRequestHelper;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Created by toanvk2 on 12/18/14.
 */
public class PubSubManager implements XMPPConnectivityChangeListener {
    private final String TAG = PubSubManager.class.getSimpleName();
    private static PubSubManager mInstance;
    private static final int SUBCRIBE_MAX_SIZE = 10;
    private LinkedList<String> listNumberSubscribe;
    private ApplicationController mApplication;
    private XMPPManager mXmppManager;
    // room chat
    private String subscribeRoomChatId;
    //stranger list
    private HashSet<String> mSubscribeStrangerRooms = new HashSet<>();
    private boolean isSubscribeStrangers = false;
    private boolean isSubscribeConnectionFeeds = false;
    // count down
    private CountDownTimer countDownUnSubStranger;
    private CountDownTimer countDownUnSubRoom;
    private CountDownTimer countDownUnSubConnectionFeeds;
    private PowerManager.WakeLock wakeLockKeepCountDown;
    private boolean isRunCountdownStrangers = false;
    private boolean isRunCountdownFeeds = false;

    public static synchronized PubSubManager getInstance(ApplicationController app) {
        if (mInstance == null) {
            mInstance = new PubSubManager(app);
        }
        return mInstance;
    }

    public PubSubManager(ApplicationController app) {
        mApplication = app;
        mXmppManager = mApplication.getXmppManager();
        listNumberSubscribe = new LinkedList<>();
        XMPPManager.addXMPPConnectivityChangeListener(this);
        PowerManager pm = (PowerManager) app.getSystemService(Context.POWER_SERVICE);
        wakeLockKeepCountDown = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Countdown");
    }

    public void clearSubscribeList() {
        if (listNumberSubscribe != null) {
            listNumberSubscribe.clear();
        } else {
            listNumberSubscribe = new LinkedList<>();
        }
    }

    public void pushToSubscribeList(ThreadMessage threadMessage) {
        Log.d(TAG, "begin pushToSubscribeList");
        if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            processSubscribeSoloThread(threadMessage);
        }
    }

    public void forceSubscribe(String jid) {
        Log.i(TAG, "forceSub:" + jid);
        if (TextUtils.isEmpty(jid)) {
            return;
        }
        // mat ket noi toi sv, khong subcribe
        if (!mXmppManager.isAuthenticated()) {
            return;
        }
        mXmppManager.sendPresenceSubscribe(jid, true);
        listNumberSubscribe.addFirst(jid);
    }

    private void processSubscribeSoloThread(ThreadMessage threadMessage) {
        mApplication.logDebugContent("processSubscribeSoloThread: " + threadMessage.getSoloNumber());
        String number = threadMessage.getSoloNumber();
        if (TextUtils.isEmpty(number)) {
            return;
        }
        // mat ket noi toi sv, khong subcribe
        if (!mXmppManager.isAuthenticated()) {
            mApplication.logDebugContent("!mXmppManager.isAuthenticated()");
            return;
        }
        if (listNumberSubscribe.contains(number)) {
            if (checkAndReSubscribe(number)) {
                Log.d(TAG, "checkAndReSubscribe");
                mXmppManager.sendPresenceSubscribe(number, true);
            }
            // neu nam o dau danh sach thi ko lam gi
            if (!listNumberSubscribe.getFirst().equals(number)) {
                Log.d(TAG, "remove list sub");
                listNumberSubscribe.remove(number);
            }
        } else {
            // chua co, kiem tra list neu over thi remove cuoi cung ra
            if (listNumberSubscribe.size() >= SUBCRIBE_MAX_SIZE) {
                String lastSubscribe = listNumberSubscribe.getLast();
                clearSubInfo(lastSubscribe);
                listNumberSubscribe.pollLast();
                mXmppManager.sendPresenceSubscribe(lastSubscribe, false);
            }
            Log.d(TAG, "send presence subcribe");
            clearSubInfo(number);
            mXmppManager.sendPresenceSubscribe(number, true);
        }
        // add lai len dau list
        listNumberSubscribe.addFirst(number);
        Log.d(TAG, "pushToSubscribeList: " + listNumberSubscribe);
    }

    private boolean checkAndReSubscribe(String number) {
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(number);
        if (phoneNumber != null) {
            return phoneNumber.isReeng() && phoneNumber.getLastSeen() == -1;
        } else {
            NonContact nonContact = contactBusiness.getExistNonContact(number);
            return nonContact != null && nonContact.isReeng() && nonContact.getLastSeen() == -1;
        }
    }

    private void clearSubInfo(String number) {
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(number);
        if (phoneNumber != null) {
            phoneNumber.setLastSeen(-1);
        } else {
            NonContact nonContact = contactBusiness.getExistNonContact(number);
            if (nonContact != null) {
                nonContact.setLastSeen(-1);
            }
        }
    }

    /**
     * @return true if packetId already exist
     */
    private boolean checkSubscribeExisted(String number) {
        return listNumberSubscribe.contains(number);
    }

    /////////////////////////////////////////////////
    ////////////subscribe room chat//////////////////
    /////////////////////////////////////////////////

    /**
     * clear subscribe khi mat ket noi hoac khi vua khoi tao
     */
    public void clearSubscribeRoom() {
        subscribeRoomChatId = null;
    }

    public void unSubscribeRoomChat(String roomId) {
        if (roomId != null) {
            if (mXmppManager != null && mXmppManager.isAuthenticated()) {
                mXmppManager.sendPresenceSubscribeRoomChat(roomId, -1, false, -1);
            }
            if (roomId.equals(subscribeRoomChatId))
                subscribeRoomChatId = null;
        }
    }

    public void unSubscribeCurrentRoom() {
        if (subscribeRoomChatId != null) {
            if (mXmppManager != null && mXmppManager.isAuthenticated()) {
                mXmppManager.sendPresenceSubscribeRoomChat(subscribeRoomChatId, -1, false, -1);
            }
            subscribeRoomChatId = null;
        }
    }

    public void subscribeRoomChat(ThreadMessage threadMessage) {
        //stop count down unsub
        stopCountDownUnSubscribeRoom();
        if (subscribeRoomChatId != null) {
            if (!subscribeRoomChatId.equals(threadMessage.getServerId())) {// dang sub room #, unsub va sub room moi
                if (mXmppManager != null && mXmppManager.isAuthenticated()) {
                    mXmppManager.sendPresenceSubscribeRoomChat(subscribeRoomChatId, -1, false, -1);
                }
                subscribeRoomChatId = null;//reset
            } else {// sub cung 1 room,  ko gui sub nua
                return;
            }
        }
        // TODO khong tu play nua cho ban tin tu sv
        // mApplication.getMusicBusiness().processJoinRoomChat(threadMessage);
        OfficerAccount accountRoomChat = mApplication.getOfficerBusiness().
                getOfficerAccountByServerId(threadMessage.getServerId());
        if (mXmppManager != null && mXmppManager.isAuthenticated()) {
            if (accountRoomChat != null) {
                mXmppManager.sendPresenceSubscribeRoomChat(threadMessage.getServerId(),
                        accountRoomChat.getJoinStateFromRoomState(), true, threadMessage.getId());
            }
            subscribeRoomChatId = threadMessage.getServerId();
        }
        // cap nhat trang thai follow vao db
        if (accountRoomChat != null && accountRoomChat.getRoomState() == OfficerAccountConstant.ROOM_STATE_NONE) {
            accountRoomChat.setRoomState(OfficerAccountConstant.ROOM_STATE_SUCCESS);
            mApplication.getOfficerBusiness().updateOfficerAccount(accountRoomChat);
        }
    }

    public void processSubscribeRoomChat(ThreadMessage threadMessage, OfficerAccount accountRoomChat,
                                         OfficerBusiness.RequestFollowRoomChatListener listener) {
        if (threadMessage.isExitRoomInfo()) {// da co thong tin roi
            subscribeRoomChat(threadMessage);
        } else if (accountRoomChat != null) {
            RoomChatRequestHelper.getInstance(mApplication).followRoomChat(threadMessage.getServerId(),
                    accountRoomChat.getName(), accountRoomChat.getAvatarUrl(),
                    accountRoomChat.getJoinStateFromRoomState(), listener);
        }
    }

    public boolean isExistSubRoomChat(String roomId) {
        return subscribeRoomChatId != null && subscribeRoomChatId.equals(roomId);
    }

    // stranger music
    public boolean isSubscribeStrangers() {
        return isSubscribeStrangers;
    }

    public boolean isSubscribeConnectionFeeds() {
        return isSubscribeConnectionFeeds;
    }

    public void processSubscribeStrangerMusic(ArrayList<StrangerMusic> strangerMusics) {
        //TODO khong dung nua
        /*if (strangerMusics == null || strangerMusics.isEmpty()) {
            return;
        }
        if (mSubscribeStrangerRooms == null) {
            mSubscribeStrangerRooms = new HashSet<String>();
        }
        ArrayList<String> newListSessionIds = new ArrayList<String>();
        for (StrangerMusic strangerMusic : strangerMusics) {
            if (strangerMusic.getState() != Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_ACCEPTED) {
                if (!mSubscribeStrangerRooms.contains(strangerMusic.getSessionId())) {
                    newListSessionIds.add(strangerMusic.getSessionId());
                }
            }
        }
        if (!newListSessionIds.isEmpty()) {
            subscribeStrangerMusic(newListSessionIds);
        }*/
    }

    public void unSubscribeStrangerMusic() {
        //TODO khong dung nua
        /*if (isSubscribeStrangers()) {
            mSubscribeStrangerRooms = new HashSet<String>();
            isSubscribeStrangers = false;
            if (mXmppManager == null || !NetworkHelper.isConnectInternet(mApplication)) {
                return;
            }
            //send presence unsubcribe
            mXmppManager.sendPresenceSubscribeStrangers(null, false);
        } else {
            mSubscribeStrangerRooms = new HashSet<String>();
            isSubscribeStrangers = false;
        }*/
    }

    private void subscribeStrangerMusic(ArrayList<String> roomSessionIds) {
        if (mXmppManager == null || !NetworkHelper.isConnectInternet(mApplication)) {
            return;
        }
        isSubscribeStrangers = true;
        mSubscribeStrangerRooms.addAll(roomSessionIds);
        //send presence subcribe
        mXmppManager.sendPresenceSubscribeStrangers(roomSessionIds, true);
    }

    ///////////////////////////////////////////
    ///////////CountDown///////////////////////
    ///////////////////////////////////////////
    private void acquireWakeLook() {
        if (wakeLockKeepCountDown != null && !wakeLockKeepCountDown.isHeld()) {
            wakeLockKeepCountDown.acquire();
        }
    }

    private void releaseWakeLook() {
        // stop tat ca count down moi release
        if ((wakeLockKeepCountDown != null && wakeLockKeepCountDown.isHeld()) &&
                (countDownUnSubRoom == null &&
                        countDownUnSubStranger == null &&
                        countDownUnSubConnectionFeeds == null)) {
            wakeLockKeepCountDown.release();
        }
    }

    public void startCountDownUnSubStranger() {
        if (countDownUnSubStranger != null && isRunCountdownStrangers) {
            return;// dang chay roi thi ko goi nua
        }
        acquireWakeLook();
        countDownUnSubStranger = new CountDownTimer(Constants.STRANGER_MUSIC.TIME_OUT_UN_SUB_STRANGER, Constants.STRANGER_MUSIC.TIME_OUT_UN_SUB_STRANGER) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "countDownUnSubStranger finish: ");
                unSubscribeStrangerMusic();// het 2 phut ko vao lai thi unSubscribe
                stopCountDownUnSubStranger();
            }
        };
        isRunCountdownStrangers = true;
        countDownUnSubStranger.start();
    }

    public void stopCountDownUnSubStranger() {
        if (countDownUnSubStranger != null) {
            countDownUnSubStranger.cancel();
            countDownUnSubStranger = null;
        }
        isRunCountdownStrangers = false;
        releaseWakeLook();
    }

    public void startCountDownUnSubRoomChat(ThreadMessage thread) {
        // khong xu ly neu thread thuong
        if (thread == null || thread.getThreadType() != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return;
        }
        // thread room thi start count down, qua 1 phut ko vao lai thi unsub
        acquireWakeLook();
        if (wakeLockKeepCountDown != null && !wakeLockKeepCountDown.isHeld()) {
            wakeLockKeepCountDown.acquire();
        }
        countDownUnSubRoom = new CountDownTimer(Constants.STRANGER_MUSIC.TIME_OUT_UN_SUB_ROOM_CHAT, Constants.STRANGER_MUSIC.TIME_OUT_UN_SUB_ROOM_CHAT) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "countDownUnSubRoom finish: ");
                // het 1 phut ko vao lai thi unSubscribe
                unSubscribeCurrentRoom();
                stopCountDownUnSubscribeRoom();
            }
        };
        countDownUnSubRoom.start();
    }

    public void stopCountDownUnSubscribeRoom() {
        if (countDownUnSubRoom != null) {
            countDownUnSubRoom.cancel();
            countDownUnSubRoom = null;
        }
        releaseWakeLook();
    }

    // feeds
    public void processSubscribeConnectionFeeds() {
        stopCountDownUnSubConnectionFeeds();// khi vao tab feeds thi stop countdown dong thoi xu ly sub
        if (isSubscribeConnectionFeeds) return;// dang sub roi thi ko sub nua
        if (mXmppManager == null || !NetworkHelper.isConnectInternet(mApplication)) {
            return;
        }
        isSubscribeConnectionFeeds = true;
        //send presence subcribe
        mXmppManager.sendPresenceSubscribeFeed(true);
    }

    public void unSubscribeConnectionFeeds() {
        if (isSubscribeConnectionFeeds()) {
            isSubscribeConnectionFeeds = false;
            if (mXmppManager == null || !NetworkHelper.isConnectInternet(mApplication)) {
                return;
            }
            //send presence subcribe
            mXmppManager.sendPresenceSubscribeFeed(false);
        }
        isSubscribeConnectionFeeds = false;
    }

    public void startCountDownUnSubConnectionFeeds() {
        if (countDownUnSubConnectionFeeds != null && isRunCountdownFeeds) {
            return;// dang chay roi thi ko goi nua
        }
        acquireWakeLook();
        countDownUnSubConnectionFeeds = new CountDownTimer(Constants.STRANGER_MUSIC.TIME_OUT_UN_SUB_CONNECTION_FEEDS, Constants.STRANGER_MUSIC.TIME_OUT_UN_SUB_CONNECTION_FEEDS) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "countDownUnSubConnectionFeeds finish: ");
                unSubscribeConnectionFeeds();// het 1 phut ko vao lai thi unSubscribe
                stopCountDownUnSubConnectionFeeds();
            }
        };
        isRunCountdownFeeds = true;
        countDownUnSubConnectionFeeds.start();
    }

    public void stopCountDownUnSubConnectionFeeds() {
        if (countDownUnSubConnectionFeeds != null) {
            countDownUnSubConnectionFeeds.cancel();
            countDownUnSubConnectionFeeds = null;
        }
        isRunCountdownFeeds = false;
        releaseWakeLook();
    }



    public void sendSubUnSubGame(String idGame, boolean isSub) {
        if (mXmppManager == null || !NetworkHelper.isConnectInternet(mApplication)) {
            return;
        }
        //send presence unsubcribe
        mXmppManager.sendPresenceSubscribeGameIQ(idGame, isSub);
    }

    @Override
    public void onXMPPConnected() {

    }

    @Override
    public void onXMPPDisconnected() {
        clearSubscribeList();
        clearSubscribeRoom();
    }

    @Override
    public void onXMPPConnecting() {

    }
}
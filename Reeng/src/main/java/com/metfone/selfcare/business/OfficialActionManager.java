package com.metfone.selfcare.business;

import android.os.AsyncTask;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.bot.Action;
import com.metfone.selfcare.database.model.bot.OfficialAction;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.fragment.message.ThreadDetailFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 1/17/2017.
 */
public class OfficialActionManager {
    private static final String TAG = OfficialActionManager.class.getSimpleName();
    private static final String REQUEST_ACTION = "request_action";
    private static OfficialActionManager mInstance;
    private ApplicationController mApplication;
    private ArrayList<OfficialAction> mOfficialActionActions = new ArrayList<>();
    private HashSet<String> mListRequestFail = new HashSet<>();
    private CopyOnWriteArrayList<OfficialActionCallback> mOfficialCallbacks = new CopyOnWriteArrayList<>();

    public static synchronized OfficialActionManager getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new OfficialActionManager(application);
        }
        return mInstance;
    }

    public OfficialActionManager(ApplicationController application) {
        this.mApplication = application;
    }

    public OfficialAction findOfficialAction(String officialId) {
        if (mOfficialActionActions.isEmpty() || TextUtils.isEmpty(officialId)) return null;
        for (OfficialAction officialActionAction : mOfficialActionActions) {
            if (officialId.equals(officialActionAction.getOfficialId())) {
                return officialActionAction;
            }
        }
        return null;
    }

    public boolean isRequestFailed(String officialId) {
        if (TextUtils.isEmpty(officialId)) {
            return false;
        } else if (!mListRequestFail.isEmpty()) {
            for (String item : mListRequestFail) {
                if (officialId.equals(item)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void checkAndGetDefaultOfficialAction(ThreadMessage threadMessage) {
        if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            String officialId = threadMessage.getServerId();
            OfficialAction officialAction = findOfficialAction(officialId);
            if (officialAction == null) {
                getActionDefault(officialId, threadMessage.getId());
            }
        }
    }

    public void handleClickOptionAction(String officialId, Action action, Fragment parentFragment) {
        OfficialAction officialAction = findOfficialAction(officialId);
        if (officialAction != null) {
            officialAction.setOptionRequesting(true);
            action.setSelected(true);
            if (parentFragment != null && parentFragment instanceof ThreadDetailFragment)
                ((ThreadDetailFragment) parentFragment).scrollListViewToBottom();
            onOfficialActionChanged(false, -1);
            // insert message fake
            insertFakeMessage(action.getTitle(), officialId);
            //send api
            sendActionKey(officialId, action.getKey(), false, -1);
        } else {
            Log.e(TAG, "why?");
        }
    }

    public boolean checkAndSendOfficialActionFromString(ThreadMessage threadMessage, String content) {
        if (TextUtils.isEmpty(content)) {
            return false;
        } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT && content
                .startsWith("@")) {
            OfficialAction officialAction = findOfficialAction(threadMessage.getServerId());
            if (officialAction != null) {
                String contentKey = content.substring(1);
                Log.d(TAG, "contentKey: " + contentKey);
                if (officialAction.isExistKeyword(contentKey)) {
                    sendActionKey(threadMessage.getServerId(), contentKey, true, threadMessage.getId());
                    insertFakeMessage(content, threadMessage.getServerId());
                    return true;
                }
            }
        }
        return false;
    }

    private void handleSendActionFail(String officialId) {
        OfficialAction officialAction = findOfficialAction(officialId);
        if (officialAction != null)
            officialAction.setOptionRequesting(false);
        onOfficialActionChanged(false, -1);
    }

    private void getActionDefault(final String officialId, final int threadId) {

        OfficerAccount officerAccount = mApplication.getOfficerBusiness().getOfficerAccountByServerId(officialId);
        if (officerAccount != null && !TextUtils.isEmpty(officerAccount.getServiceAction())) {
            String response = officerAccount.getServiceAction();
            Log.d(TAG, "getActionDefault in DB: " + response);
            onResponseGetAction(response, officialId, threadId, false);
        } else {
            VolleyHelper.getInstance(mApplication).cancelPendingRequests(officialId);
            ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
            long timeStamp = TimeHelper.getCurrentTime();
            StringBuilder sb = new StringBuilder(accountBusiness.getJidNumber()).append(officialId).
                    append(accountBusiness.getToken()).append(timeStamp);
            String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountBusiness.getToken());
            String url = String.format(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                            .FEEDBACK_GET_DEFAULT_V2),
                    HttpHelper.EncoderUrl(accountBusiness.getJidNumber()),
                    HttpHelper.EncoderUrl(officialId),
                    String.valueOf(timeStamp),
                    HttpHelper.EncoderUrl(encrypt));
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "getActionDefault: " + response);

                    onResponseGetAction(response, officialId, threadId,true);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e(TAG, "VolleyError", volleyError);
                    onOfficialActionFail(officialId, threadId);
                }
            });
            VolleyHelper.getInstance(mApplication).addRequestToQueue(request, officialId, false);
        }
    }

    private void onResponseGetAction(String response, String officialId, int threadId, boolean saveDB) {
        try {
            JSONObject object = new JSONObject(response);
            int code = object.optInt("code", -1);
            if (code == HTTPCode.E200_OK && object.has("data")) {
                JSONObject data = object.getJSONObject("data");
                ArrayList<String> keywords = parserListKeyword(data);
                ArrayList<Action> actions = parserListAction(data.optJSONArray("actions"));
                processReengMessageFromResponse(data.optJSONArray("messages"));
                //if (actions.isEmpty()) {//&& !keywords.isEmpty()
                OfficialAction officialAction = new OfficialAction(officialId, keywords, actions);
                mOfficialActionActions.add(officialAction);
                onOfficialActionChanged(false, -1);// get default ok -> refresh view
                mListRequestFail.remove(officialId);
                if (saveDB) {
                    OfficerAccount officerAccount = mApplication.getOfficerBusiness().getOfficerAccountByServerId(officialId);
                    if (officerAccount != null) {
                        officerAccount.setServiceAction(response);
                        mApplication.getOfficerBusiness().updateOfficerAccount(officerAccount);
                    }
                }
                        /* } else {
                            onOfficialActionFail(officialId, threadId);
                        }*/
            } else {
                onOfficialActionFail(officialId, threadId);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            onOfficialActionFail(officialId, threadId);
        }
    }

    private void sendActionKey(final String officialId, String actionKey,
                               final boolean fromMessageText, final int threadId) {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(REQUEST_ACTION);
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        long timeStamp = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder(accountBusiness.getJidNumber()).append(officialId).
                append(actionKey).append(accountBusiness.getToken()).append(timeStamp);
        String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountBusiness.getToken());
        String url = String.format(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                        .FEEDBACK_SEND_KEY_V2),
                HttpHelper.EncoderUrl(accountBusiness.getJidNumber()),
                HttpHelper.EncoderUrl(officialId),
                HttpHelper.EncoderUrl(actionKey),
                String.valueOf(timeStamp),
                HttpHelper.EncoderUrl(encrypt));
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "sendActionKey: " + response);
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.optInt("code", -1);
                    if (code == HTTPCode.E200_OK && object.has("data")) {
                        JSONObject jsonObjectData = object.optJSONObject("data");
                        ArrayList<Action> actions = parserListAction(jsonObjectData.optJSONArray("actions"));
                        processReengMessageFromResponse(jsonObjectData.optJSONArray("messages"));
                        OfficialAction officialAction = findOfficialAction(officialId);
                        if (officialAction != null) {
                            if (!actions.isEmpty()) {
                                // them action back o cuoi
                                Action actionBack = new Action();
                                actionBack.setActionBack(true);
                                actions.add(actionBack);
                                officialAction.setCurrentActions(actions);
                                officialAction.setOptionRequesting(false);
                                onOfficialActionChanged(fromMessageText, threadId);
                            } else {
                                handleSendActionFail(officialId);
                            }
                        } else {
                            Log.e(TAG, "why??????????");
                            handleSendActionFail(officialId);
                        }
                    } else {
                        handleSendActionFail(officialId);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    handleSendActionFail(officialId);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
                handleSendActionFail(officialId);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, REQUEST_ACTION, false);
    }

    private void processReengMessageFromResponse(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.length() == 0) return;
        ArrayList<String> listMsg = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            listMsg.add(jsonArray.optString(i));
        }
        ParserMessageAsyncTask parserMessageAsyncTask = new ParserMessageAsyncTask(listMsg);
        parserMessageAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private ArrayList<Action> parserListAction(JSONArray array) throws Exception {
        if (array != null && array.length() > 0) {
            ArrayList<Action> actions = new ArrayList<>();
            int size = array.length();
            for (int i = 0; i < size; i++) {
                actions.add(new Action(array.getJSONObject(i)));
            }
            return actions;
        }
        return new ArrayList<>();
    }

    private ArrayList<String> parserListKeyword(JSONObject data) throws Exception {
        JSONArray array = data.optJSONArray("keyword");
        if (array != null && array.length() > 0) {
            ArrayList<String> keywords = new ArrayList<>();
            int size = array.length();
            for (int i = 0; i < size; i++) {
                keywords.add(array.getString(i));
            }
            return keywords;
        }
        return new ArrayList<>();
    }

    private void insertFakeMessage(String content, String officialId) {
        ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateOfficerThread(officialId,
                "", "", OfficerAccountConstant.ONMEDIA_TYPE_NONE);
        SoloSendTextMessage message = new SoloSendTextMessage(threadMessage,
                mApplication.getReengAccountBusiness().getJidNumber(), officialId, content);
        message.setStatus(ReengMessageConstant.STATUS_DELIVERED);
        mApplication.getMessageBusiness().insertNewMessageToDB(threadMessage, message);
        mApplication.getMessageBusiness().notifyNewMessage(message, threadMessage);
    }

    public void addOfficialActionListener(OfficialActionCallback listener) {
        if (!mOfficialCallbacks.contains(listener)) {
            mOfficialCallbacks.add(listener);
        }
    }

    public void removeOfficialActionListener(OfficialActionCallback listener) {
        if (mOfficialCallbacks.contains(listener)) {
            mOfficialCallbacks.remove(listener);
        }
    }

    public void onOfficialActionChanged(boolean callFromMessageText, int threadId) {
        if (mOfficialCallbacks != null && !mOfficialCallbacks.isEmpty()) {
            for (OfficialActionCallback listener : mOfficialCallbacks) {
                if (callFromMessageText) {
                    listener.onShowOfficialAction(threadId);
                } else {
                    listener.onChangedOfficialAction();
                }
            }
        }
    }

    public void onOfficialActionFail(String officialId, int threadId) {
        mListRequestFail.add(officialId);
        if (mOfficialCallbacks != null && !mOfficialCallbacks.isEmpty()) {
            for (OfficialActionCallback listener : mOfficialCallbacks) {
                listener.onGetOfficialActionFail(threadId);
            }
        }
    }

    private class ParserMessageAsyncTask extends AsyncTask<String, Void, Void> {

        private ArrayList<String> listMsg;

        public ParserMessageAsyncTask(ArrayList<String> listMsg) {
            this.listMsg = listMsg;
        }

        @Override
        protected Void doInBackground(String... params) {
//            String input = params[0];
            try {
                XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                for (String s : listMsg) {
                    parser.setInput(new StringReader(s));
                    ReengMessagePacket packet = null;
                    int eventType = parser.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_TAG) {
                            if ("message".equals(parser.getName())) {
                                String subTypeString = parser.getAttributeValue("", "subtype");
                                ReengMessagePacket.SubType subType = ReengMessagePacket.SubType.fromString(subTypeString);
//                            String ns = parser.getAttributeValue("", "ns");
                                String external = parser.getAttributeValue("", "external");
                                packet = (ReengMessagePacket) PacketParserUtils.parseReengMessage(parser, subType,
                                        external, subTypeString);
                                packet.setNoStore(true);
                            }
                        }
                        eventType = parser.next();
                    }
                    if (packet == null) return null;
                    ReengMessagePacket.Type type = packet.getType();
                    MessageBusiness messageBusiness = mApplication.getMessageBusiness();
                    if (type == ReengMessagePacket.Type.chat) {
                        messageBusiness.getIncomingMessageProcessor().
                                processIncomingReengMessage(mApplication, packet);
                    } else if (type == ReengMessagePacket.Type.groupchat) {
                        messageBusiness.getIncomingMessageProcessor().
                                processIncomingGroupMessage(mApplication, packet);
                    } else if (type == ReengMessagePacket.Type.offical) {
                        messageBusiness.processIncomingOfficerMessage(mApplication, packet);
                    } else if (type == ReengMessagePacket.Type.roomchat) {
                        messageBusiness.getIncomingMessageProcessor().
                                processIncomingRoomMessage(mApplication, packet);
                    }
                }
            } catch (XmlPullParserException e) {
                Log.e(TAG, "XmlPullParserException", e);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            return null;
        }
    }

    public interface OfficialActionCallback {
        void onChangedOfficialAction();

        void onShowOfficialAction(int threadId);

        void onGetOfficialActionFail(int threadId);
    }
}
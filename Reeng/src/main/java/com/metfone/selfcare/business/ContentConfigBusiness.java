package com.metfone.selfcare.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.blankj.utilcode.util.StringUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.CommonApi;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.SubscriptionConfig;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.datasource.ConfigUserDataSource;
import com.metfone.selfcare.database.model.BannerMocha;
import com.metfone.selfcare.database.model.KeyValueModel;
import com.metfone.selfcare.database.model.MoreItemDeepLink;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.PinMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.PREFERENCE;
import com.metfone.selfcare.helper.Constants.PREFERENCE.CONFIG;
import com.metfone.selfcare.helper.Constants.PREF_DEFAULT;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.StrangerFilterHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.model.SuggestContent;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.model.setting.TabHomeDefault;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.tab_home.event.TabHomeEvent;
import com.metfone.selfcare.restful.StringRequest;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.viettel.util.LogDebugHelper;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.packet.ReengEventPacket;
import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by toanvk2 on 1/16/15.
 */
public class ContentConfigBusiness {
    private static final String TAG = ContentConfigBusiness.class.getSimpleName();
    private ApplicationController mApplication;
    private SharedPreferences mPref;
    private Resources mRes;
    private HashMap<String, String> hashMapContents;
    private HashMap<String, String> hashMapConfigUser;
    // key = key of config, value = default value
    private Map<String, String> mFilterConfig = new HashMap<>();
    private long lastTimeGetConfig = -1L;
    private ContactBusiness mContactBusiness;
    // banner config
    private ArrayList<BannerMocha> mListBanners;

    //media box
//    private ArrayList<MediaBoxItem> listMediaBox = new ArrayList<>();
//    private ArrayList<MediaBoxItem> listActiveMediaBox = new ArrayList<>();
    //    private ArrayList<BannerMocha> mListBannersCall;
    //private String apiKeyTranslate = null;
    private boolean isEnableCrbt = false;
    // location music
    private boolean isReady = false;
    private boolean isEnableOnmedia = false;
    private boolean isEnableInApp = false;
    private boolean isEnableLuckyWheel = false;
    private boolean isEnableQR = false;
    private boolean isEnableGuestBook = false;
    private boolean isEnableListGame = false;
    private boolean isEnableDiscovery = false;
    private String backgroundDefault;
    private boolean isUpdate = false;
    private MoreItemDeepLink moreBannerDeepLink = null;
    private SuggestContent suggestContent = null;
    private ArrayList<MoreItemDeepLink> listItemDeepLink = null;
    private boolean isShakeGame = false;
    private boolean isEnableLixi = false;
    private boolean isEnableSonTung83 = false;
    private ArrayList<PinMessage> listStickyBanner;
    private SubscriptionConfig subscriptionConfig = null;

    private ArrayList<TabHomeDefault> orderTabHomeConfig = new ArrayList<>();
    private ArrayList<ConfigTabHomeItem> listTabWap = new ArrayList<>();

    private boolean isEnableTabVideo = false;
    private boolean isEnableTabMusic = false;
    private boolean isEnableTabMovie = false;
    private boolean isEnableTabRewards = false;
    private boolean isEnableTabNews = false;
    private boolean isEnableTabTiin = false;
    private boolean isEnableTabSecurity = false;
    private boolean isEnableSpoint = false;
    private boolean isEnableBankplus = false;
    private boolean isEnableWatchVideoTogether = false;
    private boolean isEnableTabStranger = false;
    private boolean isEnableWakeUpFirebase = false;
    //private boolean isEnableItemEditContact = false;
    private boolean isEnableSmsNoInternet = false;
    private boolean isEnableSaving = false;
    private boolean isEnableMovieDrm = false;
    private boolean isEnableTabMyViettel = false;
    private boolean isEnableTabSelfcare = false;
    private boolean isEnableTabUMoney = false;
    // TODO CamID: home_tab_config
    // [start] CamID
    private boolean isEnableTabGame = false;
    private boolean isEnableTabCinema = false;
    private boolean isEnableTabESport = false;
    private boolean isEnableTabMetfone = false;
    private boolean isEnableTabContact = false;
    // [end]

    private HashMap<String, String> hashChangeNumberOld2New = new HashMap<>();
    private HashMap<String, String> hashChangeNumberNew2Old = new HashMap<>();
    private Pattern mPatternPrefixChangeNumb;
    private String regexPrefixChangeNumb;

    private ConfigUserDataSource configUserDataSource;

    private boolean isReceiveMediaBoxNews;
    private boolean isReceiveMediaBoxMovie;
    private boolean isReceiveMediaBoxMusic;
    private boolean isReceiveMediaBoxVideo;
    //    private boolean closeMediaBoxUntilHasNew;
    private boolean isEnableChannelUploadMoney = false;
    private boolean isEnableDataChallenge = false;

    private int timeSpointBonus = 0;

    public ContentConfigBusiness(ApplicationController appication) {
        mApplication = appication;
        //TODO khoi tao pref len dau
        mRes = mApplication.getResources();
        mPref = mApplication.getSharedPreferences(PREFERENCE.PREF_DIR_NAME,
                Context.MODE_PRIVATE);
        configUserDataSource = ConfigUserDataSource.getInstance(mApplication);
    }

    public void init() {
        mContactBusiness = mApplication.getContactBusiness();
        initConfigBanner();
        initFilterConfig();
        // get config tu pref lan dau khoi tao busuness
        getAllConfigFromPref();
        PhoneNumberHelper.getInstant().setSmsOutPrefixes(getContentConfigByKey(CONFIG.SMSOUT_PREFIX_AVAILABLE));
        initListStickyBanner();

        isReceiveMediaBoxMovie = mApplication.getPref().getBoolean(PREFERENCE.PREF_ENABLE_RECEIVE_MEDIA_BOX_MOVIE, true);
        isReceiveMediaBoxMusic = mApplication.getPref().getBoolean(PREFERENCE.PREF_ENABLE_RECEIVE_MEDIA_BOX_MUSIC, true);
        isReceiveMediaBoxVideo = mApplication.getPref().getBoolean(PREFERENCE.PREF_ENABLE_RECEIVE_MEDIA_BOX_VIDEO, true);
        isReceiveMediaBoxNews = mApplication.getPref().getBoolean(PREFERENCE.PREF_ENABLE_RECEIVE_MEDIA_BOX_NEWS, true);
//        closeMediaBoxUntilHasNew = mApplication.getPref().getBoolean(PREFERENCE.PREF_CLOSE_MEDIA_BOX_UNTIL_HAS_NEW, false);
//        getListMediaBoxSaved();
        isReady = true;
    }

//    private void getListMediaBoxSaved() {
//        if (!mApplication.getReengAccountBusiness().isValidAccount()) return;
//        String dataSave = mApplication.getPref().getString(PREFERENCE.PREF_CURRENT_LIST_MEDIA_BOX, "");
//        if (!TextUtils.isEmpty(dataSave)) {
//            listMediaBox = new Gson().fromJson(dataSave,
//                    new TypeToken<ArrayList<MediaBoxItem>>() {
//                    }.getType());
//            processListMediaBox(null);
//        }
//    }

    public boolean isReady() {
        return isReady;
    }

    /**
     * can lay config gi tu server thi khai bao o day
     */
    private void initFilterConfig() {
        // key = key of config, value = default value
        mFilterConfig.put(CONFIG.SMS2_NOTE_AVAILABLE, "");
        mFilterConfig.put(CONFIG.SMSOUT_PREFIX_AVAILABLE, "");
        // localpush music
        mFilterConfig.put(CONFIG.NVLT_ENABLE, "0");
        mFilterConfig.put(CONFIG.DEFAULT_STATUS_NOT_MOCHA, mRes.getString(R.string.status_non_mocha));
        // keeng
        mFilterConfig.put(CONFIG.PREF_DOMAIN_SERVICE_KEENG, "");
        mFilterConfig.put(CONFIG.PREF_DOMAIN_MEDIA2_KEENG, "");
        mFilterConfig.put(CONFIG.PREF_DOMAIN_IMAGE_KEENG, "");
        //version app
        mFilterConfig.put(CONFIG.VERSION_CODE_APP, String.valueOf(BuildConfig.VERSION_CODE));
        mFilterConfig.put(CONFIG.VERSION_NAME_APP, BuildConfig.VERSION_NAME);
        mFilterConfig.put(CONFIG.PING_INTERVAL, String.valueOf(Constants.ALARM_MANAGER.PING_TIMER));
        mFilterConfig.put(CONFIG.PING_INTERVAL_POWER_SAVE_MODE, String.valueOf(Constants.ALARM_MANAGER.PING_TIMER));
        //url service keeng
        mFilterConfig.put(CONFIG.PREF_URL_SERVICE_GET_SONG, PREF_DEFAULT.URL_SERVICE_GET_SONG_DEFAULT);
        mFilterConfig.put(CONFIG.PREF_URL_SERVICE_GET_TOP_SONG, PREF_DEFAULT.URL_SERVICE_GET_TOP_SONG_DEFAULT);
        mFilterConfig.put(CONFIG.PREF_URL_SERVICE_GET_FEEDS_KEENG, PREF_DEFAULT.URL_SERVICE_GET_FEEDS_KEENG_DEFAULT);
        mFilterConfig.put(CONFIG.PREF_URL_SERVICE_SEARCH_SONG, PREF_DEFAULT.URL_SERVICE_SEARCH_SONG_DEFAULT);
        mFilterConfig.put(CONFIG.PREF_URL_SERVICE_GET_ALBUM, PREF_DEFAULT.URL_SERVICE_GET_ALBUM_DEFAULT);
        mFilterConfig.put(CONFIG.PREF_URL_SERVICE_GET_SONG_UPLOAD, PREF_DEFAULT.URL_SERVICE_GET_SONG_UPLOAD_DEFAULT);
        mFilterConfig.put(CONFIG.PREF_URL_MEDIA2_SEARCH_SUGGESTION, PREF_DEFAULT.URL_MEDIA2_SEARCH_SUGGESTION_DEFAULT);
        mFilterConfig.put(CONFIG.PREF_URL_MEDIA_UPLOAD_SONG, PREF_DEFAULT.URL_MEDIA_UPLOAD_SONG_DEFAULT);
        //upload image profile
        mFilterConfig.put(CONFIG.IMAGE_PROFILE_MAX_SIZE, PREF_DEFAULT.PREF_IMAGE_PROFILE_MAX_SIZE);
        mFilterConfig.put(CONFIG.PREF_CRBT_ENABLE, PREF_DEFAULT.PREF_DEF_CRBT_ENABLE);
        mFilterConfig.put(CONFIG.PREF_CRBT_PRICE, PREF_DEFAULT.PREF_DEF_CRBT_PRICE);
        mFilterConfig.put(CONFIG.PREF_KEENG_PACKAGE, PREF_DEFAULT.PREF_DEF_KEENG_PACKAGE);
        //onmedia
        mFilterConfig.put(CONFIG.CONFIG_ONMEDIA_ON, "0");
//        mFilterConfig.put(CONFIG.REGISTER_VIP_BUTTON, mRes.getString(R.string.register_vip_button));
//        mFilterConfig.put(CONFIG.REGISTER_VIP_BANNER, mRes.getString(R.string.register_vip_banner));
//        mFilterConfig.put(CONFIG.REGISTER_VIP_CONFIRM, mRes.getString(R.string.register_vip_confirm));
//        mFilterConfig.put(CONFIG.UNREGISTER_VIP_CONFIRM, mRes.getString(R.string.msg_confirm_un_vip));
//        mFilterConfig.put(CONFIG.REGISTER_VIP_RECONFIRM, mRes.getString(R.string.msg_reconfirm_vip));
//        mFilterConfig.put(CONFIG.REGISTER_VIP_WC_CONFIRM, mRes.getString(R.string.register_vip_banner));
        //in app
        mFilterConfig.put(CONFIG.INAPP_ENABLE, "0");// mac dinh la off
        mFilterConfig.put(CONFIG.LUCKY_WHEEL_ENABLE, "0"); // mac dinh la off
        mFilterConfig.put(CONFIG.QR_SCAN_ENABLE, "0");
        mFilterConfig.put(CONFIG.GUEST_BOOK_ENABLE, "0");
        mFilterConfig.put(CONFIG.CALL_OUT_LABEL, mRes.getString(R.string.call_out_label));
        mFilterConfig.put(CONFIG.LISTGAME_ENABLE, "0");
        mFilterConfig.put(CONFIG.BANNER_CALL_HISTORY, "");
        mFilterConfig.put(CONFIG.DISCOVERY_ENABLE, "0");
        mFilterConfig.put(CONFIG.STRANGER_LOCATION_TIMEOUT, "24");
        mFilterConfig.put(CONFIG.BACKGROND_DEFAULT, "-");
        mFilterConfig.put(CONFIG.MORE_ITEM_DEEPLINK_V2, "-");
        mFilterConfig.put(CONFIG.MORE_ITEM_DEEPLINK_V3, "-");
        mFilterConfig.put(CONFIG.SHAKE_GAME_ON, "0");
        mFilterConfig.put(CONFIG.LIXI_ENABLE, "0");
        mFilterConfig.put(CONFIG.SONTUNG83_ENABLE, "0");
        mFilterConfig.put(CONFIG.AVNO_PAYMENT_WAPSITE, "");
        mFilterConfig.put(CONFIG.VIDEO_UPLOAD_USER, PREF_DEFAULT.PREF_DEF_VIDEO_UPLOAD_USER);
        mFilterConfig.put(CONFIG.VIDEO_UPLOAD_PASS, PREF_DEFAULT.PREF_DEF_VIDEO_UPLOAD_PASS);

        mFilterConfig.put(CONFIG.TAB_VIDEO_ENABLE, "0");
        mFilterConfig.put(CONFIG.SPOINT_ENABLE, "0");
        mFilterConfig.put(CONFIG.BANKPLUS_ENABLE, "0");
        mFilterConfig.put(CONFIG.WATCH_VIDEO_TOGETHER_ENABLE, "0");
        mFilterConfig.put(CONFIG.TAB_STRANGER_ENABLE, "0");
//        mFilterConfig.put(CONFIG.REGISTER_VIP_CMD, "");
//        mFilterConfig.put(CONFIG.REGISTER_VIP_CMD_CANCEL, "");
        mFilterConfig.put(CONFIG.SUGGEST_CONTENT_ENABLE, "");
        mFilterConfig.put(CONFIG.FIREBASE_WAKEUP_ENABLE, "");
        mFilterConfig.put(CONFIG.GET_LINK_LOCATION, "");
        mFilterConfig.put(CONFIG.WHITELIST_DEVICE, Constants.WHITE_LIST);
//        if (Config.Server.SERVER_TEST)
//            mFilterConfig.put(CONFIG.PREFIX_CHANGENUMBER_TEST, "-");
//        else
        mFilterConfig.put(CONFIG.PREFIX_CHANGENUMBER, "-");
        mFilterConfig.put(CONFIG.UPLOAD_IDCARD_ENABLE, "0");
        //mFilterConfig.put(CONFIG.MORE_ITEM_EDIT_CONTACT, "1");
        mFilterConfig.put(CONFIG.SMS_NO_INTERNET_ENABLE, "0");
        mFilterConfig.put(CONFIG.HOME_MOVIE_ENABLE, "1");
        mFilterConfig.put(CONFIG.HOME_MUSIC_ENABLE, "0");
        mFilterConfig.put(CONFIG.HOME_NEWS_ENABLE, "0");
        mFilterConfig.put(CONFIG.HOME_SELFCARE_ENABLE, "0");
        mFilterConfig.put(CONFIG.MORE_BANNER, "-");
        mFilterConfig.put(CONFIG.FACEBOOK_HASHTAG, "#mocha");
        mFilterConfig.put(CONFIG.VIP_SUBSCRIPTION, "-");
        mFilterConfig.put(CONFIG.DOMAIN_FREE_DATA, "vip.mocha.com.vn;pvvip.mocha.com.vn");
        mFilterConfig.put(CONFIG.DOMAIN_VIDEO_UPLOAD, "");
        mFilterConfig.put(CONFIG.DOMAIN_VIDEO_CAN_OPEN_PLAYER, "");
        mFilterConfig.put(CONFIG.SAVING_ENABLE, "0");
        mFilterConfig.put(CONFIG.HOME_SECURITY_ENABLE, "0");
        mFilterConfig.put(CONFIG.MOVIE_DRM_ENABLE, "0");
        mFilterConfig.put(CONFIG.HOME_TAB_CONFIG, "");
        mFilterConfig.put(CONFIG.VIDEO_INDEX_SHOW_ADS, "0");
        mFilterConfig.put(CONFIG.CHANNEL_VIDEO_UPLOAD_MONEY_ENABLE, "0");
        mFilterConfig.put(CONFIG.VIDEO_TIME_SKIP_AD, "0");
        mFilterConfig.put(CONFIG.API_LOCATION_GET, "");
        mFilterConfig.put(CONFIG.SECURE_LOCATION_GET, "");
        mFilterConfig.put(CONFIG.REGEX_MUSIC_SONG, "");
        mFilterConfig.put(CONFIG.REGEX_MUSIC_ALBUM, "");
        mFilterConfig.put(CONFIG.REGEX_MUSIC_VIDEO, "");
        mFilterConfig.put(CONFIG.REGEX_MUSIC_PLAYLIST, "");
        mFilterConfig.put(CONFIG.REGEX_MOVIES_DETAIL, "");
        mFilterConfig.put(CONFIG.REGEX_MOCHA_VIDEO, "");
        mFilterConfig.put(CONFIG.REGEX_MOCHA_CHANNEL, "");
        mFilterConfig.put(CONFIG.REGEX_NETNEWS_DETAIL, "");
        mFilterConfig.put(CONFIG.REGEX_TIIN_DETAIL, "");
        mFilterConfig.put(CONFIG.HOME_BANNER_THEME, "");
        mFilterConfig.put(CONFIG.HOME_TIIN_ENABLE, "0");
        mFilterConfig.put(CONFIG.VIDEO_SHOW_ADS_ON_FIRST, "0");
        mFilterConfig.put(CONFIG.FUNCTION_DATA_PACKAGES_TITLE, "");
        mFilterConfig.put(CONFIG.LOCATION_GOOGLE_MAP_URL, "");
        mFilterConfig.put(CONFIG.DOMAIN_KMOVIES_SHARING_ORIGINAL, "");
        mFilterConfig.put(CONFIG.DOMAIN_KMOVIES_SHARING_SWAP, "");
        mFilterConfig.put(CONFIG.DOMAIN_KMUSIC_SHARING_ORIGINAL, "");
        mFilterConfig.put(CONFIG.DOMAIN_KMUSIC_SHARING_SWAP, "");
        mFilterConfig.put(CONFIG.HOME_MY_VIETTEL_ENABLE, "0");
        mFilterConfig.put(CONFIG.MY_VIETTEL_MSG_SWITCH_TO_VIETTEL, "");
        mFilterConfig.put(CONFIG.MY_VIETTEL_MSG_NOT_REGISTERED_DATA, "");
        mFilterConfig.put(CONFIG.MY_VIETTEL_PACKAGE_NAME, "");
        mFilterConfig.put(CONFIG.MY_VIETTEL_LINK_SWITCH_TO_VIETTEL, "");
        mFilterConfig.put(CONFIG.MY_VIETTEL_LINK_CHARGING_HISTORY, "");
        mFilterConfig.put(CONFIG.MY_VIETTEL_LINK_VIETTEL_PLUS, "");
        mFilterConfig.put(CONFIG.MY_VIETTEL_LINK_SHOP, "");
        mFilterConfig.put(CONFIG.DOMAIN_LOG_ACTION_MEDIA, "");
        mFilterConfig.put(CONFIG.MY_VIETTEL_DATA_CHALLENGE_ENABLE, "");
        mFilterConfig.put(CONFIG.MY_VIETTEL_DATA_CHALLENGE_RULE, "");
        mFilterConfig.put(CONFIG.DOMAIN_VIDEO_GAME_STREAMING_API, PREF_DEFAULT.PREF_DEF_DOMAIN_VIDEO_GAME_STREAMING_API);
        mFilterConfig.put(CONFIG.DOMAIN_VIDEO_GAME_STREAMING_WS, PREF_DEFAULT.PREF_DEF_DOMAIN_VIDEO_GAME_STREAMING_WS);
        mFilterConfig.put(CONFIG.PUBLIC_KEY_VIDEO_GAME_STREAMING, PREF_DEFAULT.PREF_DEF_PUBLIC_KEY_VIDEO_GAME_STREAMING);
        mFilterConfig.put(CONFIG.DOMAIN_GAME_COUNT_DOWN, UrlConfigHelper.getInstance(mApplication).getDomainFile());
        mFilterConfig.put(CONFIG.DOMAIN_GAME_IQ, UrlConfigHelper.getInstance(mApplication).getDomainFile());
        mFilterConfig.put(CONFIG.DOMAIN_FAKE_MO, UrlConfigHelper.getInstance(mApplication).getDomainFile());
        mFilterConfig.put(CONFIG.CONFIG_LIVE_COMMENT, "");
        mFilterConfig.put(CONFIG.BAD_WORD_CONFIG, "");
        mFilterConfig.put(CONFIG.SENSITIVE_WORD_CONFIG, "");
        mFilterConfig.put(CONFIG.HOME_SEACH_HINT, "");
        mFilterConfig.put(CONFIG.SPOINT_ONLINE_BONUS, "0");
        mFilterConfig.put(CONFIG.VIDEO_TEXT_RUN_CONFIG, "");
        mFilterConfig.put(CONFIG.CONFIG_UMONEY, "0");
        mFilterConfig.put(CONFIG.DOMAIN_GAME_LUCKYWHEEL, getContentConfigByKey(CONFIG.DOMAIN_GAME_LUCKYWHEEL));

        // TODO CamID: home_tab_config
        // [start] CamID
        mFilterConfig.put(CONFIG.HOME_CINEMA_ENABLE, "0");
        mFilterConfig.put(CONFIG.HOME_ESPORT_ENABLE, "0");
        mFilterConfig.put(CONFIG.HOME_METFONE_ENABLE, "0");
        mFilterConfig.put(CONFIG.HOME_CONTACT_ENABLE, "0");
        // [end] CamID
    }

    /**
     * lay content config tu sharepref, neu chua luu thi lay trong resource
     */
    private void getAllConfigFromPref() {
        lastTimeGetConfig = mPref.getLong(CONFIG.TIMESTAMP_GET_CONFIG, -1L);
        hashMapContents = new HashMap<>();
        // get config from sharePref
        Set<String> keys = mFilterConfig.keySet();
        for (String key : keys) {
            String upgradeFeatures = mPref.getString(key, mFilterConfig.get(key));
            hashMapContents.put(key, upgradeFeatures);
        }

        //get Config from db uu tien
        hashMapConfigUser = new HashMap<>();
        ArrayList<KeyValueModel> listKeyValue = configUserDataSource.getAllConfigUser();
        if (listKeyValue != null && listKeyValue.size() > 0) {
            for (int i = 0; i < listKeyValue.size(); i++) {
                KeyValueModel keyValueModel = listKeyValue.get(i);
                hashMapConfigUser.put(keyValueModel.getKey(), keyValueModel.getValue());
                LogDebugHelper.getInstance(mApplication).logDebugContent("Config DB: " + keyValueModel.getKey() + " - " + keyValueModel.getValue());
            }
        }

        initState();
    }

    /**
     * lay content config tu api
     */

    public void getConfigFromServer(boolean isForceToGet) {

        getConfigFromServer(isForceToGet, false);
    }

    public void getConfigFromServer(boolean isForceToGet, final boolean firstTimeFromAnonymous) {
        if (isForceToGet) {     //bat phai update
            mPref.edit().putBoolean(CONFIG.PREF_FORCE_GET_CONFIG_NOT_DONE, true).apply();
            mPref.edit().putLong(CONFIG.TIMESTAMP_GET_CONFIG, 0).apply();
        } else {        //neu chua update thanh cong thi bat update
            if (mPref.getBoolean(CONFIG.PREF_FORCE_GET_CONFIG_NOT_DONE, false)) {
                isForceToGet = true;
            }
        }
        // neu thoi gian get config lan cuoi khong phai trong ngay hom nay thi moi get
        if (TimeHelper.checkTimeInDay(lastTimeGetConfig) && !isForceToGet) {
            Log.f(TAG, "getConfigFromServer: checkTimeInDay");
            return;
        }
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        if (accountBusiness.isValidAccount()
                && NetworkHelper.isConnectInternet(mApplication)) {
            // goi api get config
            String numberEncode = HttpHelper.EncoderUrl(accountBusiness.getJidNumber());
            long currentTime = System.currentTimeMillis();
            String language = accountBusiness.getDeviceLanguage();
            StringBuilder sb = new StringBuilder().
                    append(accountBusiness.getJidNumber()).
                    append(Constants.HTTP.CLIENT_TYPE_ANDROID).
                    append(language).
                    append(accountBusiness.getRegionCode()).
                    append(accountBusiness.getToken()).
                    append(currentTime);
            String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                    accountBusiness.getToken()));
            String url = String.format(
                    UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_CONTENT_CONFIG),
                    numberEncode,
                    Constants.HTTP.CLIENT_TYPE_ANDROID,
                    language,
                    accountBusiness.getRegionCode(),
                    String.valueOf(currentTime),
                    dataEncrypt);
            StringRequest request = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            String decryptResponse = HttpHelper.decryptResponse(response,
                                    mApplication.getReengAccountBusiness().getToken());
                            Log.f(TAG, "onResponse getConfig decrypt: " + decryptResponse);
                            int errorCode = -1;
                            if (StringUtils.isEmpty(decryptResponse)) {
                                return;
                            }
                            try {
                                JSONObject responseObject = new JSONObject(decryptResponse);
                                errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                                if (errorCode == HTTPCode.E200_OK) {
                                    mPref.edit().putBoolean(CONFIG.PREF_FORCE_GET_CONFIG_NOT_DONE, false).apply();
                                    parserJsonConfig(responseObject, firstTimeFromAnonymous);

                                    if (mApplication.getReengAccountBusiness() != null && !mApplication.getReengAccountBusiness().isAnonymousLogin())
                                        mApplication.getReengAccountBusiness().registerPreKey();
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception", e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String uuid = jsonObject.optString("uuid");
                                if (TextUtils.isEmpty(uuid))
                                    SharedPrefs.getInstance().remove(PREFERENCE.PREF_UUID_CONFIG);
                                else
                                    SharedPrefs.getInstance().put(PREFERENCE.PREF_UUID_CONFIG, uuid);
                            } catch (Exception e) {
                                Log.e(TAG, "Exception", e);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "VolleyError", error);
                }
            }
            );
//            {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    Map<String, String> params = new HashMap<>();
//                    String macAddress = CommonUtils.getMacAddress(mApplication);
//                    if (!TextUtils.isEmpty(macAddress))
//                        params.put("macAddress", macAddress);
//                    String imsi = CommonUtils.getIMSI(mApplication);
//                    if (!TextUtils.isEmpty(imsi))
//                        params.put("imsi", imsi);
//                    Log.i(TAG, "header: " + params.toString());
//                    return params;
//                }
//            };
            String macAddress = CommonUtils.getMacAddress(mApplication);
            if (!TextUtils.isEmpty(macAddress))
                request.setHeader("macAddress", macAddress);
            String imsi = CommonUtils.getIMSI(mApplication);
            if (!TextUtils.isEmpty(imsi))
                request.setHeader("imsi", imsi);

            VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
            //check xem cai nao da expired
            initConfigBanner();
        }
    }

    private void parserJsonConfig(JSONObject responseObject, boolean firstTimeFromAnonymous) throws JSONException {
        // thoi gian get config tu sv
        boolean enableOnMediaLastConfig = isEnableOnmedia;
        boolean enableTabVideoLastConfig = isEnableTabVideo;
        boolean enableTabMusicLastConfig = isEnableTabMusic;
        boolean enableTabMovieLastConfig = isEnableTabMovie;
        boolean enableTabNewsLastConfig = isEnableTabNews;
        boolean enableTabTiinLastConfig = isEnableTabTiin;
        boolean enableTabStrangerLastConfig = isEnableTabStranger;
        boolean enableTabSecurityLastConfig = isEnableTabSecurity;
        boolean enableSavingLastConfig = isEnableSaving;
        boolean enableTabMyViettelLastConfig = isEnableTabMyViettel;
        boolean enableTabSelfcareLastConfig = isEnableTabSelfcare;
        boolean enableDataChallengeLastConfig = isEnableDataChallenge;
        boolean enableDataUMoneyLastConfig = isEnableTabUMoney;
        // TODO CamID: home_tab_config
        // [start] CamID
        boolean enableDataGameLastConfig = isEnableTabGame;
        boolean enableDataCinemaLastConfig = isEnableTabCinema;
        boolean enableDataESportLastConfig = isEnableTabESport;
        boolean enableDataMetfoneLastConfig = isEnableTabMetfone;
        boolean enableDataContactLastConfig = isEnableTabContact;
        // [end] CamID

        lastTimeGetConfig = TimeHelper.getCurrentTime();
        mPref.edit().putLong(CONFIG.TIMESTAMP_GET_CONFIG, lastTimeGetConfig).apply();
        if (responseObject.has("config")) {
            JSONArray arrayConfig = responseObject.getJSONArray("config");
            if (arrayConfig != null && arrayConfig.length() > 0) {

                // TODO CamID: home_tab_config: append json
                // [start] CamID
                appendHomeTabConfigIntoConfig(arrayConfig);
                // [end] CamID

                int lengthArray = arrayConfig.length();
                for (int i = 0; i < lengthArray; i++) {
                    JSONObject object = arrayConfig.getJSONObject(i);
                    // get keys trong object, sau do lay values theo keys
                    // luu vao pref
                    Iterator iter = object.keys();
                    while (iter.hasNext()) {
                        String key = (String) iter.next();
                        String value = null;
                        try {
                            value = object.getString(key);
                        } catch (Exception jex) {
                            Log.e(TAG, "Exception", jex);
                        } finally {

                            // TODO CamID: home_tab_config: change json
                            // [start] CamID
                            if (CONFIG.HOME_TAB_CONFIG.equals(key)) {
                                value = changeHomeTabConfigV3Value(value);
                            }
                            // [end] CamID

                            setStringConfigToPref(key, value);

                            LogDebugHelper.getInstance(mApplication).logDebugContent("Config SV: " + key + " - " + value);
                            Log.i(TAG, key + " - " + value);
                        }
                    }
                }
                initState();
            }
        }
        //set lai SMS OUT Prefix khi load thanh cong config
        PhoneNumberHelper.getInstant().setSmsOutPrefixes(getContentConfigByKey(CONFIG.SMSOUT_PREFIX_AVAILABLE));
        if (mContactBusiness.isContactReady() && mContactBusiness.isInitArrayListReady()) {
            mContactBusiness.initArrayListPhoneNumber();
        }
        boolean enableOnMediaNewConfig = isEnableOnMedia();
        boolean enableTabVideoNewConfig = isEnableTabVideo();
        boolean enableTabMusicNewConfig = isEnableTabMusic();
        boolean enableTabMovieNewConfig = isEnableTabMovie();
        boolean enableTabNewsNewConfig = isEnableTabNews();
        boolean enableTabTiinNewConfig = isEnableTabTiin();
        boolean enableTabStrangerNewConfig = isEnableTabStranger();
        boolean enableTabSecurityNewConfig = isEnableTabSecurity();
        boolean enableSavingNewConfig = isEnableSaving();
        boolean enableTabMyViettelNewConfig = isEnableTabMyViettel();
        boolean enableTabSelfcareNewConfig = isEnableTabSelfCare();
        boolean enableDataChallengeNewConfig = isEnableDataChallenge();
        boolean enableDataUMoneyNewConfig = isEnableTabUMoney();

        // TODO CamID: home_tab_config
        // [start] CamID
        boolean enableTabGameNewConfig = isEnableTabGame();
        boolean enableTabCinemaNewConfig = isEnableTabCinema();
        boolean enableTabESportNewConfig = isEnableTabESport();
        boolean enableTabMetfoneNewConfig = isEnableTabMetfone();
        boolean enableTabContactNewConfig = isEnableTabContact();
        // [end] CamID

        if (enableOnMediaLastConfig != enableOnMediaNewConfig
                || enableTabVideoLastConfig != enableTabVideoNewConfig
                || enableTabStrangerLastConfig != enableTabStrangerNewConfig
                || enableTabMusicLastConfig != enableTabMusicNewConfig
                || enableTabMovieLastConfig != enableTabMovieNewConfig
                || enableTabNewsLastConfig != enableTabNewsNewConfig
                || enableTabTiinLastConfig != enableTabTiinNewConfig
                || enableTabSecurityLastConfig != enableTabSecurityNewConfig
                || enableSavingLastConfig != enableSavingNewConfig
                || enableTabMyViettelLastConfig != enableTabMyViettelNewConfig
                || enableTabSelfcareLastConfig != enableTabSelfcareNewConfig
                || enableDataChallengeLastConfig != enableDataChallengeNewConfig
                || enableDataUMoneyLastConfig != enableDataUMoneyNewConfig
                || enableDataGameLastConfig != enableTabGameNewConfig
                || enableDataCinemaLastConfig != enableTabCinemaNewConfig
                || enableDataESportLastConfig != enableTabESportNewConfig
                || enableDataMetfoneLastConfig != enableTabMetfoneNewConfig
                || enableDataContactLastConfig != enableTabContactNewConfig
        ) {
            Log.i(TAG, "onGetConfigTabChanged");
            ListenerHelper.getInstance().onConfigTabChanged(mApplication);
        } else if (firstTimeFromAnonymous)
            ListenerHelper.getInstance().onConfigTabChanged(mApplication);
    }

    private void setStringConfigToPref(String key, String content) {
        if (!TextUtils.isEmpty(content)) {
            mPref.edit().putString(key, content).apply();
            hashMapContents.put(key, content);
        }
        //set maximageupload
        String maxSize = getContentConfigByKey(Constants.PREFERENCE.CONFIG.IMAGE_PROFILE_MAX_SIZE);
        if (!TextUtils.isEmpty(maxSize)) {
            int maxAlbumImages = Integer.valueOf(maxSize);
            mApplication.getImageProfileBusiness().setMaxAlbumImages(maxAlbumImages);
        }
    }

    public String getContentConfigByKey(String key) {
        if (hashMapContents == null) {// truong hop loi goi  config lai
            getAllConfigFromPref();
        }

        //Check config cho tung user truoc
        if (hashMapConfigUser != null) {
            if (hashMapConfigUser.containsKey(key))
                return hashMapConfigUser.get(key);
        }

        return hashMapContents.get(key);
    }

    //////////////////////////
    //// config banner////////
    //////////////////////////
    public ArrayList<BannerMocha> getListBanners() {
        return mListBanners;
    }

    /*public ArrayList<BannerMocha> getListBannersCall() {
        return mListBannersCall;
    }*/

    public void processIncomingBannerMessage(ReengEventPacket receivedPacket) {
        boolean needNotifyBannerCall = false;
        String action = receivedPacket.getBannerAction();
        ArrayList<JSONObject> jsonBanners = receivedPacket.getBannerJson();
        ArrayList<BannerMocha> banners = new ArrayList<>();
//        ArrayList<BannerMocha> bannersCall = new ArrayList<>();
        if (TextUtils.isEmpty(action)) {//add or remove all
            if (jsonBanners != null && !jsonBanners.isEmpty()) {
                banners = new ArrayList<>();
                for (JSONObject item : jsonBanners) {
                    BannerMocha banner = new BannerMocha();
                    banner.parserFromJSON(item);
                    if (banner.isValid() && !banner.isExpired()) {
                        /*if (BannerMocha.TARGET_TAB_CALL.equals(banner.getTarget())) {
                            bannersCall.add(banner);
                            needNotifyBannerCall = true;
                        } else*/
                        banners.add(banner);
                    } else {
                        Log.d(TAG, "banner not valid or expired: " + banner.toString());
                    }
                }
            }

        } else if ("banner_off".equals(action) && jsonBanners != null) {// off list banner theo id
            banners = new ArrayList<>(mListBanners);
            for (JSONObject item : jsonBanners) {
                BannerMocha banner = findBannerByBannerId(item.optString("id", null), banners);
                if (banner != null) {
                    banners.remove(banner);
                }
            }

            /*bannersCall = new ArrayList<>(mListBannersCall);
            for (JSONObject item : jsonBanners) {
                BannerMocha banner = findBannerByBannerId(item.optString("id", null), bannersCall);
                if (banner != null) {
                    bannersCall.remove(banner);
                    needNotifyBannerCall = true;
                }
            }*/
        } else {
            Log.d(TAG, "ngoại lệ không xử lý");
        }

//        if (!needNotifyBannerCall) {
        mListBanners = banners;
        ListenerHelper.getInstance().onConfigBannerChanged(mListBanners);
        saveConfigBannerToPref(mListBanners, PREFERENCE.PREF_CONFIG_BANNER_CONTENT);
        /*} else {
            mListBannersCall = bannersCall;
            ListenerHelper.getInstance().onConfigBannerCallChanged(mListBannersCall);
            saveConfigBannerToPref(mListBannersCall, PREFERENCE.PREF_CONFIG_BANNER_CALL);
        }*/

    }

    private BannerMocha findBannerByBannerId(String bannerId, ArrayList<BannerMocha> list) {
        if (TextUtils.isEmpty(bannerId) || list == null || list.isEmpty()) {
            return null;
        }
        for (BannerMocha item : list) {
            if (bannerId.equals(item.getId())) {
                return item;
            }
        }
        return null;
    }

    public void saveConfigBannerToPref(ArrayList<BannerMocha> listBanners, String key) {
        if (listBanners == null || listBanners.isEmpty()) {
            mPref.edit().putString(key, "").apply();
        } else {
            try {
                Gson gson = new Gson();
                JsonArray jsonArray = new JsonArray();
                for (BannerMocha item : listBanners) {
                    jsonArray.add(new JsonParser().parse(gson.toJson(item)));
                }
                JsonObject jsonObject = new JsonObject();
                jsonObject.add("listBanner", jsonArray);
                mPref.edit().putString(key, jsonObject.toString()).apply();
            } catch (Exception e) {
                mPref.edit().putString(key, "").apply();
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public void initConfigBanner() {
        String contentPref = mPref.getString(PREFERENCE.PREF_CONFIG_BANNER_CONTENT, "");
        if (TextUtils.isEmpty(contentPref)) {
            mListBanners = null;
        } else {
            try {
                JsonElement jsonElement = new JsonParser().parse(contentPref);
                JsonObject object = jsonElement.getAsJsonObject();
                mListBanners = parserBannerMocha(object, PREFERENCE.PREF_CONFIG_BANNER_CONTENT);
                ListenerHelper.getInstance().onConfigBannerChanged(mListBanners);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                mListBanners = null;
            }
        }

        /*String contentCallPref = mPref.getString(PREFERENCE.PREF_CONFIG_BANNER_CALL, "");
        if (TextUtils.isEmpty(contentCallPref)) {
            mListBannersCall = null;
        } else {
            try {
                JsonElement jsonElement = new JsonParser().parse(contentPref);
                JsonObject object = jsonElement.getAsJsonObject();
                mListBannersCall = parserBannerMocha(object, PREFERENCE.PREF_CONFIG_BANNER_CALL);
                ListenerHelper.getInstance().onConfigBannerCallChanged(mListBannersCall);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                mListBannersCall = null;
            }
        }*/
        /*mListBanners = new ArrayList<>();
        BannerMocha bannerMocha = new BannerMocha("1", "tesst", "desc test", "icon", "link", 1, true, -1);
        mListBanners.add(bannerMocha);*/
    }

    private ArrayList<BannerMocha> parserBannerMocha(JsonObject object, String key) {
        ArrayList<BannerMocha> listBanners = new ArrayList<>();
        boolean isChanged = false;
        if (object.has("listBanner")) {
            JsonArray jsonArray = object.getAsJsonArray("listBanner");
            if (jsonArray != null && jsonArray.size() > 0) {
                int lengthArray = jsonArray.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    BannerMocha item = gson.fromJson(jsonArray.get(i), BannerMocha.class);
                    if (item.isExpired() || !item.isValid()) {
                        isChanged = true;
                    } else {
                        listBanners.add(item);
                    }
                }
            }
        }
        if (isChanged)// co banner expired thi save lai ds moi
            saveConfigBannerToPref(listBanners, key);
        return listBanners;
    }

    private void initState() {
        // crbtState
        isEnableCrbt = "1".equals(getContentConfigByKey(CONFIG.PREF_CRBT_ENABLE));
        // inAppEnable
        isEnableInApp = "1".equals(getContentConfigByKey(CONFIG.INAPP_ENABLE));
        isEnableLuckyWheel = "1".equals(getContentConfigByKey(CONFIG.LUCKY_WHEEL_ENABLE));
        isEnableQR = "1".equals(getContentConfigByKey(CONFIG.QR_SCAN_ENABLE));
        isEnableGuestBook = "1".equals(getContentConfigByKey(CONFIG.GUEST_BOOK_ENABLE));
        isEnableListGame = "1".equals(getContentConfigByKey(CONFIG.LISTGAME_ENABLE));
        isEnableDiscovery = "1".equals(getContentConfigByKey(CONFIG.DISCOVERY_ENABLE));
        isEnableOnmedia = "1".equals(getContentConfigByKey(CONFIG.CONFIG_ONMEDIA_ON));
        backgroundDefault = getContentConfigByKey(CONFIG.BACKGROND_DEFAULT);
        int version_code = TextHelper.parserIntFromString(getContentConfigByKey(CONFIG.VERSION_CODE_APP), BuildConfig.VERSION_CODE);
        isUpdate = BuildConfig.VERSION_CODE < version_code;
        isShakeGame = "1".equals(getContentConfigByKey(CONFIG.SHAKE_GAME_ON));
        isEnableLixi = "1".equals(getContentConfigByKey(CONFIG.LIXI_ENABLE));
        isEnableSonTung83 = "1".equals(getContentConfigByKey(CONFIG.SONTUNG83_ENABLE));
        isEnableTabVideo = "1".equals(getContentConfigByKey(CONFIG.TAB_VIDEO_ENABLE));
        isEnableSpoint = "1".equals(getContentConfigByKey(CONFIG.SPOINT_ENABLE));
        isEnableBankplus = "1".equals(getContentConfigByKey(CONFIG.BANKPLUS_ENABLE));
        isEnableWatchVideoTogether = "1".equals(getContentConfigByKey(CONFIG.WATCH_VIDEO_TOGETHER_ENABLE));
        isEnableTabStranger = "1".equals(getContentConfigByKey(CONFIG.TAB_STRANGER_ENABLE));
        isEnableWakeUpFirebase = "1".equals(getContentConfigByKey(CONFIG.FIREBASE_WAKEUP_ENABLE));
        isEnableTabUMoney = "1".equals(getContentConfigByKey(CONFIG.CONFIG_UMONEY));
//        if (Config.Server.SERVER_TEST)
//            regexPrefixChangeNumb = getContentConfigByKey(CONFIG.PREFIX_CHANGENUMBER_TEST);
//        else
        regexPrefixChangeNumb = getContentConfigByKey(CONFIG.PREFIX_CHANGENUMBER);
        mPatternPrefixChangeNumb = null;
        int lenght = PrefixChangeNumberHelper.PREFIX_NEW.length;
        for (int i = 0; i < lenght; i++) {
            hashChangeNumberOld2New.put(PrefixChangeNumberHelper.PREFIX_OLD[i], PrefixChangeNumberHelper.PREFIX_NEW[i]);
            hashChangeNumberNew2Old.put(PrefixChangeNumberHelper.PREFIX_NEW[i], PrefixChangeNumberHelper.PREFIX_OLD[i]);
        }
//        mApplication.getReengAccountBusiness().processChangeNumber();
        //isEnableItemEditContact = "1".equals(getContentConfigByKey(CONFIG.MORE_ITEM_EDIT_CONTACT));
        isEnableSmsNoInternet = "1".equals(getContentConfigByKey(CONFIG.SMS_NO_INTERNET_ENABLE));
        isEnableTabMovie = "1".equals(getContentConfigByKey(CONFIG.HOME_MOVIE_ENABLE));
        isEnableTabMusic = "1".equals(getContentConfigByKey(CONFIG.HOME_MUSIC_ENABLE));
        isEnableTabNews = "1".equals(getContentConfigByKey(CONFIG.HOME_NEWS_ENABLE));
        isEnableTabTiin = "1".equals(getContentConfigByKey(CONFIG.HOME_TIIN_ENABLE));
        isEnableSaving = "1".equals(getContentConfigByKey(CONFIG.SAVING_ENABLE));
        isEnableTabSecurity = "1".equals(getContentConfigByKey(CONFIG.HOME_SECURITY_ENABLE));
        isEnableMovieDrm = "1".equals(getContentConfigByKey(CONFIG.MOVIE_DRM_ENABLE));
        isEnableChannelUploadMoney = "1".equals(getContentConfigByKey(CONFIG.CHANNEL_VIDEO_UPLOAD_MONEY_ENABLE));
        isEnableTabMyViettel = "1".equals(getContentConfigByKey(CONFIG.HOME_MY_VIETTEL_ENABLE));
        isEnableTabSelfcare = "1".equals(getContentConfigByKey(CONFIG.HOME_SELFCARE_ENABLE));
        isEnableDataChallenge = "1".equals(getContentConfigByKey(CONFIG.MY_VIETTEL_DATA_CHALLENGE_ENABLE));
        isEnableTabUMoney = "1".equals(getContentConfigByKey(CONFIG.CONFIG_UMONEY));

        // TODO CamID: home_tab_config
        // [start] CamID
        isEnableTabGame = "1".equals(getContentConfigByKey(CONFIG.HOME_GAME_ENABLE));
        isEnableTabCinema = "1".equals(getContentConfigByKey(CONFIG.HOME_CINEMA_ENABLE));
        isEnableTabESport = "1".equals(getContentConfigByKey(CONFIG.HOME_ESPORT_ENABLE));
        isEnableTabMetfone = "1".equals(getContentConfigByKey(CONFIG.HOME_METFONE_ENABLE));
        isEnableTabContact = "1".equals(getContentConfigByKey(CONFIG.HOME_CONTACT_ENABLE));
        // [end] CamID

//        parserVipSubscription(getContentConfigByKey(CONFIG.VIP_SUBSCRIPTION));
//        parserListMoreItemDeepLink(getContentConfigByKey(CONFIG.MORE_ITEM_DEEPLINK_V3));
//        parserMoreBannerDeepLink(getContentConfigByKey(CONFIG.MORE_BANNER));
//        mApplication.getReengAccountBusiness().onMoreItemChanged();
//        //parse config tab de sau cung
//        parseConfigTab(getContentConfigByKey(CONFIG.HOME_TAB_CONFIG));
//        parseSuggestContent(getContentConfigByKey(CONFIG.SUGGEST_CONTENT_ENABLE));
//        LogDebugHelper.getInstance(mApplication).logDebugContent("initState: movie" + isEnableTabMovie + " video: " + isEnableTabVideo + " music: " + isEnableTabMusic + " news: " + isEnableTabNews + " tiin: " + isEnableTabTiin);
//        if (Utilities.notEmpty(getContentConfigByKey(CONFIG.API_LOCATION_GET))
//                && TextUtils.isEmpty(SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LOCATION_CLIENT_IP, String.class))) {
//            CommonApi.getInstance().getMyLocation();
//        }
//        TextHelper.getInstant().initPatternFromConfig(mApplication);
//        timeSpointBonus = Integer.parseInt(getContentConfigByKey(CONFIG.SPOINT_ONLINE_BONUS)) * 60 * 1000;
    }

    private void parseSuggestContent(String contentConfigByKey) {
        Log.i(TAG, "parseSuggestContent: " + contentConfigByKey);
        suggestContent = null;
        if (!TextUtils.isEmpty(contentConfigByKey) && !"-".equals(contentConfigByKey)) {
            try {
                JSONObject jsonObject = new JSONObject(contentConfigByKey);
                String title = jsonObject.optString("title");
                String img = jsonObject.optString("img");
                String deeplink = jsonObject.optString("url");
                int type = jsonObject.optInt("type");
                suggestContent = new SuggestContent(title, img, type, deeplink);
            } catch (Exception ex) {
                Log.e(TAG, "parseSuggestContent error", ex);
            }

        }
    }

    private void parserVipSubscription(String input) {
        subscriptionConfig = new SubscriptionConfig();
        String title;
        String confirm;
        String reconfirm;
        String cmd;
        String cmdCancel;
        String unVip;
        try {
            JSONObject js = new JSONObject(input);
            title = js.optString("title", mRes.getString(R.string.register_vip_button));
            confirm = js.optString("confirm", mRes.getString(R.string.register_vip_confirm));
            reconfirm = js.optString("reconfirm");
            cmd = js.optString("cmd");
            cmdCancel = js.optString("cmdCancel");
            unVip = js.optString("unVip", mRes.getString(R.string.msg_confirm_un_vip));
        } catch (Exception ex) {
            title = mRes.getString(R.string.register_vip_button);
            confirm = mRes.getString(R.string.register_vip_confirm);
            reconfirm = mRes.getString(R.string.register_vip_reconfirm);
            cmd = "";
            cmdCancel = "";
            unVip = mRes.getString(R.string.msg_confirm_un_vip);
        }

        subscriptionConfig.setTitle(title);
        subscriptionConfig.setConfirm(confirm);
        subscriptionConfig.setReconfirm(reconfirm);
        subscriptionConfig.setCmd(cmd);
        subscriptionConfig.setCmdCancel(cmdCancel);
        subscriptionConfig.setUnVip(unVip);
    }

    public boolean isEnableCrbt() {
        return isEnableCrbt;
    }

    public boolean isEnableInApp() {
        return isEnableInApp;
    }

    public boolean isEnableLuckyWheel() {
        return isEnableLuckyWheel;
    }

    public boolean isEnableQR() {
        return isEnableQR;
    }

    public boolean isEnableGuestBook() {
        return isEnableGuestBook;
    }

    public boolean isEnableListGame() {
        return isEnableListGame;
    }

    public boolean isEnableDiscovery() {
        return isEnableDiscovery;
    }

    public boolean isEnableOnMedia() {
        return isEnableOnmedia;
    }

    public String getBackgroundDefault() {
        //return "http://hlvip2.mocha.com.vn/hlmocha92/media3/banner/bg_default_newyear.jpg";
        //return "-";
        return backgroundDefault;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    private void parserMoreBannerDeepLink(String input) {
        //input="{\"icon\":\"http://hlvip2.mocha.com.vn/hlmocha92/media3/banner/ic_home_shake.png\",\"title\":\"Lắc để nhận quà\",\"deeplink\":\"mocha://shake\",\"isNew\":false}";
        //input = "{\"icon\":\"http://hlvip2.mocha.com.vn/hlmocha92/media3/banner/ic_home_sontung.png\",\"title\":\"Săn quà cùng Sơn Tùng\",\"deeplink\":\"mocha://sontung83\",\"isNew\":false}";
        Log.d(TAG, "[parserMoreBannerDeepLink] " + input);
        if (TextUtils.isEmpty(input) || "-".equals(input)) {
            moreBannerDeepLink = null;
        } else {
            try {
                JSONObject jsonObject = new JSONObject(input);
                moreBannerDeepLink = new MoreItemDeepLink(jsonObject.optString("img"),
                        "", jsonObject.optString("deeplink"), false);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                moreBannerDeepLink = null;
            }
        }
    }

    private void parserListMoreItemDeepLink(String input) {
        //input="{\"icon\":\"http://hlvip2.mocha.com.vn/hlmocha92/media3/banner/ic_home_shake.png\",\"title\":\"Lắc để nhận quà\",\"deeplink\":\"mocha://shake\",\"isNew\":false}";
        //input = "{\"icon\":\"http://hlvip2.mocha.com.vn/hlmocha92/media3/banner/ic_home_sontung.png\",\"title\":\"Săn quà cùng Sơn Tùng\",\"deeplink\":\"mocha://sontung83\",\"isNew\":false}";
        Log.d(TAG, "[check list more] " + input);
        if (TextUtils.isEmpty(input) || "-".equals(input)) {
            listItemDeepLink = null;
        } else {
            listItemDeepLink = new ArrayList<>();
            try {
                JSONArray jsonArray = new JSONArray(input);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject js = jsonArray.getJSONObject(i);
                    if (js != null) {
                        MoreItemDeepLink item = new MoreItemDeepLink(js.optString("icon"),
                                js.optString("title"),
                                js.optString("deeplink"),
                                js.optBoolean("isNew"));
                        listItemDeepLink.add(item);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                listItemDeepLink = null;
            }
        }
    }

    public MoreItemDeepLink getMoreBannerDeepLink() {
        return moreBannerDeepLink;
    }

    public MoreItemDeepLink getMoreItemDeepLink() {
        return null;
    }

    public ArrayList<MoreItemDeepLink> getListMoreItemDeepLink() {
        return listItemDeepLink;
    }

    public boolean isShakeGame() {
        return isShakeGame;
    }

    public boolean isEnableLixi() {
        return isEnableLixi;
    }

    public boolean isEnableSonTung83() {
        return isEnableSonTung83;
    }

    public boolean isEnableTabVideo() {
        return isEnableTabVideo;
    }

    public boolean isEnableTabMusic() {
        return isEnableTabMusic;
    }

    public boolean isEnableTabMovie() {
        return isEnableTabMovie;
    }

    public boolean isEnableTabRewards() {
        //TODO SEARCH_REWARDS logic luồng enable tab reward
        isEnableTabRewards = true;
        return isEnableTabRewards;
    }

    public boolean isEnableTabNews() {
        return isEnableTabNews;
    }

    public boolean isEnableTabTiin() {
        return isEnableTabTiin;
    }

    public boolean isEnableTabSecurity() {
        return isEnableTabSecurity;
    }

    public boolean isEnableTabStranger() {
        return isEnableTabStranger;
    }

    public boolean isEnableSpoint() {
        return isEnableSpoint;
    }

    public boolean isEnableBankplus() {
        return isEnableBankplus;
    }

    public boolean isEnableWatchVideoTogether() {
        return isEnableWatchVideoTogether;
    }

    public SuggestContent getSuggestContent() {
        return suggestContent;
    }

    public boolean isEnableWakeUpFirebase() {
        return isEnableWakeUpFirebase;
    }

//    public boolean isEnableItemEditContact() {
//        return isEnableItemEditContact;
//    }

    public boolean isEnableSmsNoInternet() {
        return isEnableSmsNoInternet;
    }

    public boolean isEnableSaving() {
        return isEnableSaving;
    }

    public boolean isEnableTabMyViettel() {
        return isEnableTabMyViettel;
    }

    public boolean isEnableTabSelfCare() {
        return isEnableTabSelfcare;
    }

    public boolean isEnableDataChallenge() {
        return isEnableDataChallenge;
    }

    public boolean isEnableTabUMoney() {
        return isEnableTabUMoney;
    }

    public boolean isEnableTabGame() {
        return isEnableTabGame;
    }

    public boolean isEnableTabCinema() {
        return isEnableTabCinema;
    }

    public boolean isEnableTabESport() {
        return isEnableTabESport;
    }

    public boolean isEnableTabMetfone() {
        return isEnableTabMetfone;
    }

    public boolean isEnableTabContact() {
        return isEnableTabContact;
    }

    public int getTimeSpointBonus() {
        return timeSpointBonus;
    }

    public SubscriptionConfig getSubscriptionConfig() {
        if (subscriptionConfig == null)
            parserVipSubscription(getContentConfigByKey(CONFIG.VIP_SUBSCRIPTION));
        return subscriptionConfig;
    }

    //Xu ly config theo tung user
    public void processSetConfig(ReengMessagePacket packet) {
        String key = packet.getKeyValueConfig().getKey();
        String value = packet.getKeyValueConfig().getValue();

        boolean enableOnMediaLastConfig = false;
        boolean enableTabVideoLastConfig = false;
        boolean enableTabStrangerLastConfig = false;
        boolean enableTabMusicLastConfig = false;
        boolean enableTabMovieLastConfig = false;
        boolean enableTabNewsLastConfig = false;
        boolean enableTabTiinLastConfig = false;
        boolean enableTabSecurityLastConfig = false;
        boolean enableSavingLastConfig = false;
        boolean enableTabSelcareLastConfig = false;

        //Check enable onmedia truoc day
        if (CONFIG.CONFIG_ONMEDIA_ON.equals(key)) {
            enableOnMediaLastConfig = isEnableOnMedia();
        } else if (CONFIG.TAB_VIDEO_ENABLE.equals(key)) {
            enableTabVideoLastConfig = isEnableTabVideo();
        } else if (CONFIG.TAB_STRANGER_ENABLE.equals(key)) {
            enableTabStrangerLastConfig = isEnableTabStranger();
        } else if (CONFIG.HOME_MOVIE_ENABLE.equals(key)) {
            enableTabMovieLastConfig = isEnableTabMovie();
        } else if (CONFIG.HOME_MUSIC_ENABLE.equals(key)) {
            enableTabMusicLastConfig = isEnableTabMusic();
        } else if (CONFIG.HOME_NEWS_ENABLE.equals(key)) {
            enableTabNewsLastConfig = isEnableTabNews();
        } else if (CONFIG.HOME_TIIN_ENABLE.equals(key)) {
            enableTabTiinLastConfig = isEnableTabTiin();
        } else if (CONFIG.HOME_SECURITY_ENABLE.equals(key)) {
            enableTabSecurityLastConfig = isEnableTabSecurity();
        } else if (CONFIG.SAVING_ENABLE.equals(key)) {
            enableSavingLastConfig = isEnableSaving();
        } else if (CONFIG.HOME_SELFCARE_ENABLE.equals(key)) {
            enableTabSelcareLastConfig = isEnableTabSelfCare();
        }

        if (configUserDataSource != null) {
            if (!TextUtils.isEmpty(value)) {
                if (hashMapConfigUser != null && hashMapConfigUser.containsKey(key))//Neu co trong db roi thi update
                {
                    configUserDataSource.updateConfigUser(new KeyValueModel(key, value));
                } else//Chua co thi insert config
                {
                    configUserDataSource.insertConfigUser(new KeyValueModel(key, value));
                }
                hashMapConfigUser.put(key, value);
            }
            initState();

            if ((CONFIG.CONFIG_ONMEDIA_ON.equals(key) && enableOnMediaLastConfig != isEnableOnMedia())
                    || (CONFIG.TAB_VIDEO_ENABLE.equals(key) && enableTabVideoLastConfig != isEnableTabVideo())
                    || (CONFIG.TAB_STRANGER_ENABLE.equals(key) && enableTabStrangerLastConfig != isEnableTabStranger())
                    || (CONFIG.HOME_MUSIC_ENABLE.equals(key) && enableTabMusicLastConfig != isEnableTabMusic())
                    || (CONFIG.HOME_MOVIE_ENABLE.equals(key) && enableTabMovieLastConfig != isEnableTabMovie())
                    || (CONFIG.HOME_NEWS_ENABLE.equals(key) && enableTabNewsLastConfig != isEnableTabNews())
                    || (CONFIG.HOME_TIIN_ENABLE.equals(key) && enableTabTiinLastConfig != isEnableTabTiin())
                    || (CONFIG.HOME_SECURITY_ENABLE.equals(key) && enableTabSecurityLastConfig != isEnableTabSecurity())
                    || (CONFIG.SAVING_ENABLE.equals(key) && enableSavingLastConfig != isEnableSaving())
                    || (CONFIG.HOME_SELFCARE_ENABLE.equals(key) && enableTabSelcareLastConfig != isEnableTabSelfCare())
                    || CONFIG.HOME_TAB_CONFIG.equals(key)
            ) {
                Log.i(TAG, "onConfigTabChanged");
                ListenerHelper.getInstance().onConfigTabChanged(mApplication);
            } else if (CONFIG.HOME_BANNER_THEME.equals(key)) {
                ListenerHelper.getInstance().onConfigBackgroundHeaderHomeChanged();
            } else if (CONFIG.REGEX_MUSIC_SONG.equals(key)
                    || CONFIG.REGEX_MUSIC_ALBUM.equals(key)
                    || CONFIG.REGEX_MUSIC_VIDEO.equals(key)
                    || CONFIG.REGEX_MUSIC_PLAYLIST.equals(key)
                    || CONFIG.REGEX_MOVIES_DETAIL.equals(key)
                    || CONFIG.REGEX_MOCHA_VIDEO.equals(key)
                    || CONFIG.REGEX_MOCHA_CHANNEL.equals(key)
                    || CONFIG.REGEX_NETNEWS_DETAIL.equals(key)
                    || CONFIG.REGEX_TIIN_DETAIL.equals(key)
            ) {
                TextHelper.getInstant().initPatternFromConfig(key, value);
            } else if (CONFIG.FUNCTION_DATA_PACKAGES_TITLE.equals(key)) {
                EventBus.getDefault().postSticky(new TabHomeEvent().setUpdateFeature(true));
            } else if (CONFIG.BAD_WORD_CONFIG.equals(key)) {
                TextHelper.getInstant().resetBadWordPatterns(mApplication, value);
            } else if (CONFIG.SENSITIVE_WORD_CONFIG.equals(key)) {
                TextHelper.getInstant().resetSensitiveWordPatterns(mApplication, value);
            }

        }
    }

    public void processDelConfig(ReengMessagePacket packet) {
        String key = packet.getKeyValueConfig().getKey();
        //String value = packet.getKeyValueConfig().getValue();

        boolean enableOnMediaLastConfig = false;
        boolean enableTabVideoLastConfig = false;
        boolean enableTabStrangerLastConfig = false;
        boolean enableTabMusicLastConfig = false;
        boolean enableTabMovieLastConfig = false;
        boolean enableTabNewsLastConfig = false;
        boolean enableTabTiinLastConfig = false;
        boolean enableTabSecurityLastConfig = false;
        boolean enableSavingLastConfig = false;
        //boolean enableMyViettelLastConfig = false;
        boolean enableSelfcareLastConfig = false;
        //boolean enableEditContact = isEnableItemEditContact;

        // TODO CamID: home_tab_config
        // [start] CamID
        boolean enableGameLastConfig = false;
        boolean enableCinemaLastConfig = false;
        boolean enableESportLastConfig = false;
        boolean enableMetfoneLastConfig = false;
        boolean enableContactLastConfig = false;
        // [end] CamID

        //Check enable onmedia truoc day
        if (CONFIG.CONFIG_ONMEDIA_ON.equals(key)) {
            enableOnMediaLastConfig = isEnableOnMedia();
        } else if (CONFIG.TAB_VIDEO_ENABLE.equals(key)) {
            enableTabVideoLastConfig = isEnableTabVideo();
        } else if (CONFIG.TAB_STRANGER_ENABLE.equals(key)) {
            enableTabStrangerLastConfig = isEnableTabStranger();
        } else if (CONFIG.HOME_MOVIE_ENABLE.equals(key)) {
            enableTabMovieLastConfig = isEnableTabMovie();
        } else if (CONFIG.HOME_MUSIC_ENABLE.equals(key)) {
            enableTabMusicLastConfig = isEnableTabMusic();
        } else if (CONFIG.HOME_NEWS_ENABLE.equals(key)) {
            enableTabNewsLastConfig = isEnableTabNews();
        } else if (CONFIG.HOME_TIIN_ENABLE.equals(key)) {
            enableTabTiinLastConfig = isEnableTabTiin();
        } else if (CONFIG.HOME_SECURITY_ENABLE.equals(key)) {
            enableTabSecurityLastConfig = isEnableTabSecurity();
        } else if (CONFIG.SAVING_ENABLE.equals(key)) {
            enableSavingLastConfig = isEnableSaving();
        } else if (CONFIG.HOME_SELFCARE_ENABLE.equals(key)) {
            enableSelfcareLastConfig = isEnableTabSelfCare();
        }

        // TODO CamID: home_tab_config
        // [start] CamID
        else if (CONFIG.HOME_GAME_ENABLE.equals(key)) {
            enableGameLastConfig = isEnableTabGame();
        }  else if (CONFIG.HOME_CINEMA_ENABLE.equals(key)) {
            enableCinemaLastConfig = isEnableTabCinema();
        } else if (CONFIG.HOME_ESPORT_ENABLE.equals(key)) {
            enableESportLastConfig = isEnableTabESport();
        } else if (CONFIG.HOME_METFONE_ENABLE.equals(key)) {
            enableMetfoneLastConfig = isEnableTabMetfone();
        } else if (CONFIG.HOME_CONTACT_ENABLE.equals(key)) {
            enableContactLastConfig = isEnableTabContact();
        }
        // [end] CamID

        if (configUserDataSource != null) {
            configUserDataSource.deleteConfigUserByKey(key);
            hashMapConfigUser.remove(key);
            initState();

            if ((key.equals(CONFIG.CONFIG_ONMEDIA_ON) && enableOnMediaLastConfig != isEnableOnMedia())
                    || (CONFIG.TAB_VIDEO_ENABLE.equals(key) && enableTabVideoLastConfig != isEnableTabVideo())
                    || (CONFIG.TAB_STRANGER_ENABLE.equals(key) && enableTabStrangerLastConfig != isEnableTabStranger())
                    || (CONFIG.HOME_MUSIC_ENABLE.equals(key) && enableTabMusicLastConfig != isEnableTabMusic())
                    || (CONFIG.HOME_MOVIE_ENABLE.equals(key) && enableTabMovieLastConfig != isEnableTabMovie())
                    || (CONFIG.HOME_NEWS_ENABLE.equals(key) && enableTabNewsLastConfig != isEnableTabNews())
                    || (CONFIG.HOME_TIIN_ENABLE.equals(key) && enableTabTiinLastConfig != isEnableTabTiin())
                    || (CONFIG.HOME_SECURITY_ENABLE.equals(key) && enableTabSecurityLastConfig != isEnableTabSecurity())
                    || (CONFIG.SAVING_ENABLE.equals(key) && enableSavingLastConfig != isEnableSaving())
                    || (CONFIG.HOME_SELFCARE_ENABLE.equals(key) && enableSelfcareLastConfig != isEnableTabSelfCare())
                    || CONFIG.HOME_TAB_CONFIG.equals(key)
                    || (CONFIG.HOME_GAME_ENABLE.equals(key) && enableGameLastConfig != isEnableTabGame())
                    || (CONFIG.HOME_CINEMA_ENABLE.equals(key) && enableCinemaLastConfig != isEnableTabCinema())
                    || (CONFIG.HOME_ESPORT_ENABLE.equals(key) && enableESportLastConfig != isEnableTabESport())
                    || (CONFIG.HOME_METFONE_ENABLE.equals(key) && enableMetfoneLastConfig != isEnableTabMetfone())
                    || (CONFIG.HOME_CONTACT_ENABLE.equals(key) && enableContactLastConfig != isEnableTabContact())
            ) {
                Log.i(TAG, "onConfigTabChanged");
                ListenerHelper.getInstance().onConfigTabChanged(mApplication);
            }
        }
    }

    private void initListStickyBanner() {
        String contentPref = mPref.getString(PREFERENCE.PREF_LIST_STICKY_BANNER, "");
        if (TextUtils.isEmpty(contentPref)) {
            listStickyBanner = null;
        } else {
            listStickyBanner = new Gson().fromJson(contentPref,
                    new TypeToken<List<PinMessage>>() {
                    }.getType());

        }
    }

    public void processConfigStickyBanner(ReengMessagePacket packet) {
        PinMessage pinMessage = getPinMessageFromPacket(packet);
        if (listStickyBanner == null)
            listStickyBanner = new ArrayList<>();
        if (!listStickyBanner.isEmpty()) {
            for (PinMessage sticky : listStickyBanner) {
                if (sticky.getThreadType() == pinMessage.getThreadType()) {
                    listStickyBanner.remove(sticky);
                    break;
                }
            }
        }
        listStickyBanner.add(pinMessage);
        mPref.edit().putString(PREFERENCE.PREF_LIST_STICKY_BANNER, new Gson().toJson(listStickyBanner)).apply();
        ListenerHelper.getInstance().onConfigStickyBannerChanged();
    }

    public void removeConfigStickyBanner(PinMessage pinMessage) {
        listStickyBanner.remove(pinMessage);
        mPref.edit().putString(PREFERENCE.PREF_LIST_STICKY_BANNER, new Gson().toJson(listStickyBanner)).apply();
        ListenerHelper.getInstance().onConfigStickyBannerChanged();
    }

    private PinMessage getPinMessageFromPacket(ReengMessagePacket packet) {
        PinMessage pinMessage = new PinMessage();
        pinMessage.setContent(packet.getBody());
        pinMessage.setImage(packet.getPinMsgImg());
        pinMessage.setType(PinMessage.TypePin.TYPE_SPECIAL_THREAD.VALUE);
        pinMessage.setTarget(packet.getPinMsgTarget());
        pinMessage.setThreadType(packet.getPinThreadType());
        pinMessage.setExpired(packet.getPinExpired());
        pinMessage.setTitle(packet.getPinMsgTitle());
        return pinMessage;
    }

    public PinMessage getStickyBannerFromThreadType(ThreadMessage threadMessage) {
        if (threadMessage == null)
            return null;
        if (!threadMessage.isStranger()
                && threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                && threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT)
            return null;
        if (listStickyBanner == null || listStickyBanner.isEmpty()) return null;
        for (PinMessage pinMessage : listStickyBanner) {
            int type = pinMessage.getThreadType();
            if (type == PinMessage.SpecialThread.THREAD_STRANGER.VALUE) {
                if (threadMessage.isStranger())
                    return pinMessage;
            } else if (type == PinMessage.SpecialThread.THREAD_CONTACT.VALUE) {
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT)
                    return pinMessage;
            } else if (type == PinMessage.SpecialThread.THREAD_GROUP.VALUE) {
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT)
                    return pinMessage;
            }
        }
        return null;
    }

    public HashMap<String, String> getHashChangeNumberOld2New() {
        return hashChangeNumberOld2New;
    }

    public boolean needChangePrefix(String jid) {
        if (TextUtils.isEmpty(jid)) return false;
        if (jid.length() != 11) return false;
        if (isDisableChangePrefix()) return false;
        if (mPatternPrefixChangeNumb == null)
            mPatternPrefixChangeNumb = Pattern.compile(regexPrefixChangeNumb);

        return mPatternPrefixChangeNumb.matcher(jid).find();
    }

    public boolean isDisableChangePrefix() {
        return TextUtils.isEmpty(regexPrefixChangeNumb) || "-".equals(regexPrefixChangeNumb);
    }

    public String getRegexPrefixChangeNumb() {
        return regexPrefixChangeNumb;
    }

    public HashMap<String, String> getHashChangeNumberNew2Old() {
        return hashChangeNumberNew2Old;
    }

    public void deleteAllKeyConfig() {
        SharedPreferences.Editor editor = mPref.edit();
        for (Map.Entry<String, String> entry : mFilterConfig.entrySet()) {
            String key = entry.getKey();
            editor.remove(key);
        }
        editor.remove(Constants.STRANGER_MUSIC.FILTER_LOCATION);
        editor.remove(Constants.STRANGER_MUSIC.FILTER_SEX);
        editor.remove(Constants.STRANGER_MUSIC.FILTER_AGE_MIN);
        editor.remove(Constants.STRANGER_MUSIC.FILTER_AGE_MAX);
        editor.remove(Constants.PREFERENCE.PREF_MY_STRANGER_LOCATION);
        editor.remove(Constants.PREFERENCE.PREF_LAST_UPDATE_STRANGER_LOCATION);
        editor.apply();
        mFilterConfig = new HashMap<>();
        init();
//        mApplication.getReengAccountBusiness().onMoreItemChanged();
        StrangerFilterHelper.getInstance(mApplication).reinit();
        Log.f(TAG, "deleteAllKeyConfig: stranger: " + isEnableTabStranger + " |onmedia: " + isEnableOnmedia + " |music: " + isEnableTabMusic + " |tiin: " + isEnableTabTiin + " |video: " + isEnableTabVideo + " |netnews: " + isEnableTabNews + " |movie: " + isEnableTabMovie);

    }

    long lastTimeRequestMediaBox;

//    public void requestListMediaBox() {
//        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) return;
//        if (lastTimeRequestMediaBox == 0)
//            lastTimeRequestMediaBox = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_TIME_GET_MEDIA_BOX, 0);
//        if ((System.currentTimeMillis() - lastTimeRequestMediaBox) < 60 * 60 * 1000) return;
//
//        CommonApi.getInstance().getListMediaBox(new ApiCallbackV2<ArrayList<MediaBoxItem>>() {
//            @Override
//            public void onSuccess(String response, ArrayList<MediaBoxItem> mediaBoxItems) throws JSONException {
//                lastTimeRequestMediaBox = System.currentTimeMillis();
//                mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LAST_TIME_GET_MEDIA_BOX, lastTimeRequestMediaBox).apply();
//                int countItemOld = 0;
//                String titleMediaBox = null;
//                if (!TextUtils.isEmpty(response)) {
//                    JSONObject jsonObject = new JSONObject(response);
//                    titleMediaBox = jsonObject.optString("header");
//                }
//
//                for (MediaBoxItem itemSv : mediaBoxItems) {
//                    boolean isNew = true;
//                    for (MediaBoxItem itemLocal : listMediaBox) {
//                        if (itemSv.getId() == itemLocal.getId()) {
//                            if (!itemLocal.isNewItem())
//                                isNew = false;
//                            countItemOld++;
//                            break;
//                        }
//                    }
//                    itemSv.setNewItem(isNew);
//                }
//                String lastTitleMediaBox = null;
//                if (listMediaBox != null && !listMediaBox.isEmpty()) {
//                    lastTitleMediaBox = listMediaBox.get(0).getTitleHeader();
//                }
//
//                if (countItemOld == mediaBoxItems.size()) {
//                    if (!TextUtils.isEmpty(titleMediaBox) && !titleMediaBox.equals(lastTitleMediaBox)) {
//                        //nếu title header có thay đổi sẽ set lại
//                    } else
//                        return;
//                } else setCloseMediaBoxUntilHasNew(false);
//
//                listMediaBox.clear();
//                listMediaBox = mediaBoxItems;
//                saveDataListMediaBox();
//                /*String titleMediaBox = null;
//                if (!TextUtils.isEmpty(response)) {
//                    JSONObject jsonObject = new JSONObject(response);
//                    titleMediaBox = jsonObject.optString("header");
//                }*/
//                if (titleMediaBox == null) titleMediaBox = "";
//                processListMediaBox(titleMediaBox);
//            }
//
//            @Override
//            public void onError(String s) {
//
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });
//    }

//    public void processListMediaBox(String titleHeader) {
//        /*listActiveMediaBox = new ArrayList<>();
//
//        for (int i = 0; i < listMediaBox.size(); i++) {
//            MediaBoxItem item = listMediaBox.get(i);
//            item.setFirstItem(false);
//            item.setLastItem(false);
//            if (isActiveItemMediaBox(item.getType())) {
//                listActiveMediaBox.add(item);
//            }
//        }
//        boolean hasNewItem = false;
//        for (int i = 0; i < listActiveMediaBox.size(); i++) {
//            MediaBoxItem item = listActiveMediaBox.get(i);
//            if (item.isNewItem()) hasNewItem = true;
//            if (i == 0) {
//                item.setFirstItem(true);
//                if (titleHeader != null)
//                    item.setTitleHeader(titleHeader);
//            }
//            if (i == listActiveMediaBox.size() - 1) item.setLastItem(true);
//        }
//        if (!hasNewItem) listActiveMediaBox = new ArrayList<>();
//        ArrayList<ThreadMessage> listThread = new ArrayList<>();
//        listThread.add(new ThreadMessage());
//        ListenerHelper.getInstance().onUpdateAllThread(new HashSet<ThreadMessage>(listThread));*/
//    }

//    private void saveDataListMediaBox() {
//        /*mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_CURRENT_LIST_MEDIA_BOX,
//                new Gson().toJson(listMediaBox)).apply();*/
//    }

//    private boolean isActiveItemMediaBox(String typeMediaBox) {
//        if (MediaBoxItem.ItemType.NEWS.equals(typeMediaBox)) {
//            return isReceiveMediaBoxNews;
//        } else if (MediaBoxItem.ItemType.VIDEO.equals(typeMediaBox)) {
//            return isReceiveMediaBoxVideo;
//        } else if (MediaBoxItem.ItemType.MOVIE.equals(typeMediaBox)) {
//            return isReceiveMediaBoxMovie;
//        } else if (MediaBoxItem.ItemType.MUSIC.equals(typeMediaBox)) {
//            return isReceiveMediaBoxMusic;
//        } else return MediaBoxItem.ItemType.DEEPLINK.equals(typeMediaBox);
//    }

//    public ArrayList<MediaBoxItem> getListActiveMediaBox() {
//        return listActiveMediaBox;
//    }

    public boolean isReceiveMediaBoxNews() {
        return isReceiveMediaBoxNews;
    }

    public void setReceiveMediaBoxNews(boolean receiveMediaBoxNews) {
        isReceiveMediaBoxNews = receiveMediaBoxNews;
        mApplication.getPref().edit().putBoolean(PREFERENCE.PREF_ENABLE_RECEIVE_MEDIA_BOX_NEWS, receiveMediaBoxNews).apply();
    }

    public boolean isReceiveMediaBoxMovie() {
        return isReceiveMediaBoxMovie;
    }

    public void setReceiveMediaBoxMovie(boolean receiveMediaBoxMovie) {
        isReceiveMediaBoxMovie = receiveMediaBoxMovie;
        mApplication.getPref().edit().putBoolean(PREFERENCE.PREF_ENABLE_RECEIVE_MEDIA_BOX_MOVIE, receiveMediaBoxMovie).apply();
    }

    public boolean isReceiveMediaBoxMusic() {
        return isReceiveMediaBoxMusic;
    }

    public void setReceiveMediaBoxMusic(boolean receiveMediaBoxMusic) {
        isReceiveMediaBoxMusic = receiveMediaBoxMusic;
        mApplication.getPref().edit().putBoolean(PREFERENCE.PREF_ENABLE_RECEIVE_MEDIA_BOX_MUSIC, receiveMediaBoxMusic).apply();
    }

    public boolean isReceiveMediaBoxVideo() {
        return isReceiveMediaBoxVideo;
    }

    public void setReceiveMediaBoxVideo(boolean receiveMediaBoxVideo) {
        isReceiveMediaBoxVideo = receiveMediaBoxVideo;
        mApplication.getPref().edit().putBoolean(PREFERENCE.PREF_ENABLE_RECEIVE_MEDIA_BOX_VIDEO, receiveMediaBoxVideo).apply();
    }

//    public boolean isCloseMediaBoxUntilHasNew() {
//        return closeMediaBoxUntilHasNew;
//    }
//
//    public void setCloseMediaBoxUntilHasNew(boolean closeMediaBoxUntilHasNew) {
//        this.closeMediaBoxUntilHasNew = closeMediaBoxUntilHasNew;
//        mApplication.getPref().edit().putBoolean(PREFERENCE.PREF_CLOSE_MEDIA_BOX_UNTIL_HAS_NEW, closeMediaBoxUntilHasNew).apply();
//        if (closeMediaBoxUntilHasNew) {
//            ArrayList<ThreadMessage> listThread = new ArrayList<>();
//            listThread.add(new ThreadMessage());
//            ListenerHelper.getInstance().onUpdateAllThread(new HashSet<ThreadMessage>(listThread));
//        }
//    }

//    public void saveItemMediaBoxUnNew(MediaBoxItem item) {
//        if (!item.isNewItem()) return;
//        boolean hasNewItem = false;
//        for (MediaBoxItem mediaBoxItem : listMediaBox) {
//            if (item.getId() == mediaBoxItem.getId()) {
//                mediaBoxItem.setNewItem(false);
//                saveDataListMediaBox();
//            }
//            if (mediaBoxItem.isNewItem()) hasNewItem = true;
//        }
//        if (!hasNewItem) {
//            listActiveMediaBox = new ArrayList<>();
//            ArrayList<ThreadMessage> listThread = new ArrayList<>();
//            listThread.add(new ThreadMessage());
//            ListenerHelper.getInstance().onUpdateAllThread(new HashSet<ThreadMessage>(listThread));
//        }
//    }

    private void parseConfigTab(String contentConfigByKey) {
        Log.i(TAG, "parseConfigTab: " + contentConfigByKey);
        orderTabHomeConfig.clear();
        listTabWap.clear();
        if (!TextUtils.isEmpty(contentConfigByKey) && !"-".equals(contentConfigByKey)) {
            try {
                JSONObject jsonObject = new JSONObject(contentConfigByKey);
                JSONArray jsa = jsonObject.optJSONArray("tabDefaultV2");
                if (jsa != null && jsa.length() > 0) {
                    for (int i = 0; i < (jsa.length() >= 3 ? 3 : jsa.length()); i++) {
                        JSONObject jso = jsa.getJSONObject(i);
                        TabHomeDefault tabHomeDefault = new TabHomeDefault(jso.optInt("type"), jso.optString("id"));
                        orderTabHomeConfig.add(tabHomeDefault);
                    }
                }
                Log.i(TAG, "size orderTabHomeConfig: " + orderTabHomeConfig.size());
                removeUnAvailableTabHome();

                JSONArray jsaWap = jsonObject.optJSONArray("tabWap");
                if (jsaWap != null && jsaWap.length() > 0) {
                    for (int i = 0; i < jsaWap.length(); i++) {
                        listTabWap.add(getTabWapFromJson(jsaWap.optJSONObject(i)));
                    }
                    Log.i(TAG, "listtabwap: " + listTabWap.size());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private ConfigTabHomeItem getTabWapFromJson(JSONObject jsonObject) {
        String name = jsonObject.optString("name");
        String desc = jsonObject.optString("desc");
        String link = jsonObject.optString("link");
        String img = jsonObject.optString("img");
        String id = jsonObject.optString("id");
        String color = jsonObject.optString("color");
        String imgAct = jsonObject.optString("imgAct");
        String imgMore = jsonObject.optString("imgMore");

        return new ConfigTabHomeItem(name, desc, link, img, id, color, imgAct, imgMore);
    }

    //concurrentmodificationexception: có thể thằng khác đang modify thằng orderTabHomeConfig
    private void removeUnAvailableTabHome() {
        try {
//            Iterator<Integer> indexItem = orderTabHomeConfig.iterator();
            ArrayList<TabHomeDefault> copy = new ArrayList<>();
            for (int i = 0; i < orderTabHomeConfig.size(); i++) {
                int index = orderTabHomeConfig.get(i).getTabIndex();
                TabHomeHelper.HomeTab homeTab = TabHomeHelper.getHomeTabFromIndex(index);
                if (homeTab == null || !TabHomeHelper.isTabAvailable(homeTab, mApplication)) {
                    Log.i(TAG, "homeTab: " + index + " null|| unavailable");
                } else {
                    Log.i(TAG, "add homeTab: " + index);
                    if (!copy.contains(orderTabHomeConfig.get(i)))
                        copy.add(orderTabHomeConfig.get(i));
                }
            }

            /*while (indexItem.hasNext()) {
                int index = indexItem.next(); // must be called before you can call i.remove()

                // Do something
                TabHomeHelper.HomeTab homeTab = TabHomeHelper.getHomeTabFromIndex(index);
                if (homeTab == null || !TabHomeHelper.isTabAvailable(homeTab, mApplication)) {
                    Log.i(TAG, "homeTab: " + index + " null|| unavailable");
                    indexItem.remove();
                } else
                    copy.add(index);
            }*/

            orderTabHomeConfig = copy;
            Log.i(TAG, "orderTabHomeConfig after remove: " + orderTabHomeConfig.size());
        } catch (Exception ex) {
            Log.e(TAG, "removeUnAvailableTabHome Exception: " + ex.toString());
        }
    }

    public ConfigTabHomeItem getConfigTabHomeWap(String id) {
        for (ConfigTabHomeItem item : listTabWap) {
            if (item.getId().equals(id)) return item;
        }
        return null;
    }

    public ArrayList<TabHomeDefault> getOrderTabHomeConfig() {
        return orderTabHomeConfig;
    }

    public ArrayList<ConfigTabHomeItem> getListTabWap() {
        return listTabWap;
    }

    public boolean isEnableMovieDrm() {
        return isEnableMovieDrm;
    }

    public boolean isEnableChannelUploadMoney() {
        return isEnableChannelUploadMoney;
    }

    public boolean isEnableVideoShowAdsOnFirst() {
        return "1".equals(getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_SHOW_ADS_ON_FIRST));
    }

    // TODO CamID: handle BottomNavigationBar for CAMID
    // [start]

    /**
     * Do hiện tại chưa thay đổi đc giá trị của đối tượng ```home.tab.config.v3``` trên CMS
     * nên sẽ hardcode phần này tạm thời để xử lý hiển thị của BottomNavigationBar
     * api lấy giá trị ```home.tab.config.v3``` : ReengBackendBiz/config/getConfig/v2.2
     * <p>
     * Tối đa là 5 tab, tất cả các tab có thể đổi vị trí cho nhau
     * - tab **Home** và **Chat** sẽ luôn xuất hiện
     * - tab **Cinema**, **eSport**, **Metfone+**, **Contact** có thể customize hide/show trong Setting
     */
    private String changeHomeTabConfigV3Value(String value) {
        List<Integer> indexHomeTab = new ArrayList<>();
        indexHomeTab.add(10); // cinema tab
        indexHomeTab.add(11); // esport tab
        indexHomeTab.add(12); // metfone+ tab
        indexHomeTab.add(13);
        try {
            JSONObject jsonObject = new JSONObject(value);
            JSONArray jsa = jsonObject.optJSONArray("tabDefaultV2");
            if (jsa != null && jsa.length() > 0) {
                int length = Math.min(jsa.length(), 3);
                for (int i = 0; i < length; i++) {
                    JSONObject jso = jsa.getJSONObject(i);
                    jso.put("type", indexHomeTab.get(i));
                }
            }
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Tab Cinema, eSport, metfone+, contact có thể bị ẩn đi cả ở BottomNavigationBar và trong Customize tab khi thực hiện cấu hình trên CMS
     * Do hiện tại chưa thể cấu hình được nên sẽ tạm thời sửa chuỗi json nhận được để cấu hình hiển thị các tab trên.
     * <p>
     * Giá trị home.....enable = 1 thì là tab đó sẽ được hiển thị
     * <p>
     * Tối đa là 5 tab, tất cả các tab có thể đổi vị trí cho nhau
     * - tab **Home** và **Chat** sẽ luôn xuất hiện
     * - tab **Cinema**, **eSport**, **Metfone+**, **Contact** có thể customize hide/show trong Setting
     */
    private void appendHomeTabConfigIntoConfig(JSONArray jsonArray) {
        try {
            JSONObject homeGamesEnableObject = new JSONObject();
            homeGamesEnableObject.put(CONFIG.HOME_GAME_ENABLE, "1");
            jsonArray.put(homeGamesEnableObject);

            JSONObject homeCinemaEnableObject = new JSONObject();
            homeCinemaEnableObject.put(CONFIG.HOME_CINEMA_ENABLE, "1");
            jsonArray.put(homeCinemaEnableObject);

            JSONObject homeESportEnableObject = new JSONObject();
            homeESportEnableObject.put(CONFIG.HOME_ESPORT_ENABLE, "1");
            jsonArray.put(homeESportEnableObject);

            JSONObject homeMetfoneEnableObject = new JSONObject();
            homeMetfoneEnableObject.put(CONFIG.HOME_METFONE_ENABLE, "1");
            jsonArray.put(homeMetfoneEnableObject);

            JSONObject homeContactEnableObject = new JSONObject();
            homeContactEnableObject.put(CONFIG.HOME_CONTACT_ENABLE, "1");
            jsonArray.put(homeContactEnableObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // [end] CamID

    public interface OnConfigBannerChangeListener {
        void onConfigBannerChanged(ArrayList<BannerMocha> listBanners);

        void onConfigBannerCallChanged(ArrayList<BannerMocha> listBannersCall);
    }

    public interface OnConfigChangeListener {
        void onConfigStickyBannerChanged();

        void onConfigTabChange();

        void onConfigBackgroundHeaderHomeChange();
    }

}
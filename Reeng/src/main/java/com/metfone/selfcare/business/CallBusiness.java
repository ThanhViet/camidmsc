package com.metfone.selfcare.business;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.stringee.StringeeCallListener;
import com.stringee.StringeeIceServer;
import com.stringee.StringeeStream;
import com.tbruyelle.rxpermissions2.Permission;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.MochaCallActivity;
import com.metfone.selfcare.activity.QuickMissCallActivity;
import com.metfone.selfcare.activity.QuickReplyActivity;
import com.metfone.selfcare.activity.RecallActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.CommonApi;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.call.CallQuality;
import com.metfone.selfcare.database.model.message.MochaCallMessage;
import com.metfone.selfcare.database.model.message.MochaCallOutMessage;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.call.CallBluetoothHelper;
import com.metfone.selfcare.helper.call.CallConstant;
import com.metfone.selfcare.helper.call.CallConstant.STATE;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.helper.message.PacketMessageId;
import com.metfone.selfcare.listeners.CallStateInterface;
import com.metfone.selfcare.listeners.GsmCallStateChange;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.notification.ReengNotificationManager;
import com.metfone.selfcare.ui.dialog.DialogCallOption;
import com.metfone.selfcare.ui.dialog.PermissionDialog;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.model.CallData;
import org.jivesoftware.smack.model.IceServer;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQCall;
import org.jivesoftware.smack.packet.ReengCallOutPacket;
import org.jivesoftware.smack.packet.ReengCallPacket;
import org.jivesoftware.smack.packet.ReengCallPacket.CallError;
import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.IS_REQUEST_PERMISSION_FOR_CALL;
import static com.metfone.selfcare.util.Utilities.isNeedShowHeadUpNotification;
import static com.metfone.selfcare.util.Utilities.isNotShowActivityCall;

/**
 * Created by toanvk2 on 7/25/2016.
 */
public class CallBusiness implements GsmCallStateChange, XMPPConnectivityChangeListener {
    private static final String TAG = CallBusiness.class.getSimpleName();
    private static final int PACKET_ID_LIST_MAX_SIZE = 2000;

    private static ArrayList<CallStateInterface> callStateListeners = new ArrayList<>();
    private ApplicationController mApplication;
    private Resources mRes;
    private Handler handler = new Handler(Looper.getMainLooper());
    private AudioManager audioManager;
    private int originalAudioMode;
    private boolean originalSpeaker;
    private boolean originalMute;
    private MediaPlayer mMediaPlayer;
    private Ringtone mRingtone;
    private Vibrator mVibrator;
    private String currentCallSession;
    private String codecAudio;
    private String codecVideo;
    private String myJidNumber;
    private String currentFriendJid;// jid de gui messsage
    private String currentCaller;
    private String currentCallee;
    private String friendOperator;
    private int usingDesktop;
    private int currentTypeCall;
    private int currentCallState = CallConstant.STATE.NON;
    private int candidateCount = 0;
    private long beginTimeConnect = 0;
    private boolean isCallOut = false;
    private boolean isCallIn = false;
    private boolean isStrangerConfide = false;
    private boolean isVideoCall = false;
    private int stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.NON;
    private boolean isEnableMyVideoCall = false;
    private boolean isEnableFriendVideoCall = false;
    private LinkedList<StringeeIceServer> currentIceServers = new LinkedList<>();
    private Timer mTimeoutTimer;
    private long startCallTime;
    private long timeoutCall;
    private int currentTimeCall = 0;
    private Timer mTimeCallTimer;
    private boolean isSpeaker = false;
    private boolean isMute = false;
    private Thread mThreadInitCall;
    private final LinkedList<CallData> sendCallDataQueue = new LinkedList<>();
    //
    private boolean isExistGsm = false;
    private boolean isExistCallActivity = false;
    private static ArrayList<CallQuality> callQualities = new ArrayList<>();

    private long deltaTimeConnect = 0;
    private boolean onlyAudio = false;

    private boolean isEnableRestartICE = false;
    private int iceTimeout;     //thư viện truyền vào là int nên để là int
    private long restartICEDelay;
    private long restartICEPeriod;
    private int restartICELoop;
    private long zeroBwEndCall;
    private long network2failedTime;
    private long timedis2recon;
    private long timeRestartBw;
    private long delayRestartOnFailed;

    private String bundlePolicy;
    private String rtcpMuxPolicy;
    private String iceTransportsType;
    private long fcallviafs;
    //use for callee
    private CallData callData;

    private boolean isStartedRestartICE;
    private LinkedList<String> listPacketIdOfReceivedMessage;

    public CallBusiness(ApplicationController app) {
        this.mApplication = app;
        this.mRes = mApplication.getResources();
        this.isExistCallActivity = false;
        this.audioManager = (AudioManager) mApplication.getSystemService(Context.AUDIO_SERVICE);
        this.mVibrator = (Vibrator) mApplication.getSystemService(Context.VIBRATOR_SERVICE);
//        ReengNotificationManager.clearMessagesNotification(mApplication, Constants.NOTIFICATION.NOTIFY_CALL);
        mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_CALL);
        listPacketIdOfReceivedMessage = new LinkedList<>();
    }

    public static void addCallStateListener(CallStateInterface listener) {
        Log.d(TAG, "addCallStateListener");
        if (!callStateListeners.contains(listener)) {
            callStateListeners.add(listener);
        }
    }

    public static void removeCallStateListener(CallStateInterface listener) {
        Log.d(TAG, "removeCallStateListener");
        if (callStateListeners.contains(listener)) {
            callStateListeners.remove(listener);
        }
    }

    private void notifyOnRinging() {
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onRinging();
            }
        }
    }

    private int sumState = 0;

    public void notifyOnConnectStateChange(int state) {
        Log.d(TAG, "notifyOnConnectStateChange: " + state);
        if (!isFcallviafs())
            stopMediaPlayer();
        if (!isCallOut && currentCallState == STATE.CONNECTED
                || (isCallOut && sumState >= (CallConstant.STATE.CALLEE_STARTED + STATE.CONNECTED)))
            return;
        currentCallState = state;
        if (isCallOut && getCurrentTypeCall() == CallConstant.TYPE_CALLER) {
            if (state == CallConstant.STATE.CALLEE_STARTED) {
                sumState += CallConstant.STATE.CALLEE_STARTED;
                long timeConnect = (System.currentTimeMillis() - deltaTimeConnect);
                Log.i(TAG, "--------------deltaTimeConnect: " + timeConnect);
                ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread
                        (currentFriendJid);
                sendCallOutKPIMessage(myJidNumber, currentFriendJid, ReengCallOutPacket.CallOutType.receive_status,
                        threadMessage,
                        currentCaller, currentCallee, null, null, timeConnect, isCallIn, false, null);
            }
        } else if (!isCallOut || isCallIn) {
            if (currentCallState == STATE.CONNECT) {
                deltaTimeConnect = System.currentTimeMillis();
            } else if (currentCallState == STATE.CONNECTED) {
                long timeConnect = (System.currentTimeMillis() - deltaTimeConnect);
                Log.i(TAG, "--------------deltaTimeConnect: " + timeConnect);
                ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread
                        (currentFriendJid);

                if (isCallIn) {
                    sendCallOutKPIMessage(myJidNumber, currentFriendJid, ReengCallOutPacket.CallOutType
                                    .receive_status, threadMessage,
                            currentCaller, currentCallee, null, null, timeConnect, isCallIn, false, null);
                } else {
                    sendCallKPIMessage(myJidNumber, currentFriendJid, threadMessage,
                            currentCaller, currentCallee, null, null, onlyAudio, timeConnect, false, null);
                }
            }
        }
        if (currentCallState == STATE.CONNECTED || ((isCallOut || isCallIn) && currentCallState == STATE.CALLEE_STARTED)) {
            // set lại audio mode khi connected cho chac, phòng th play media ringing làm sai mode
            if (currentCallState == STATE.CONNECTED) {
                sumState += CallConstant.STATE.CONNECTED;
            }
            if (!isCallOut || (isCallOut && sumState == (CallConstant.STATE.CONNECTED + STATE.CALLEE_STARTED)) || (isCallIn && sumState == CallConstant.STATE.CONNECTED)) {
                currentCallState = CallConstant.STATE.CONNECTED;
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
                CallBluetoothHelper.getInstance(mApplication).start();
                startTimerTimeCall();
            }

        } /*else if (currentCallState == STATE.CONNECT && currentCallState == STATE.CALLEE_STARTED){
            currentCallState = CallConstant.STATE.CONNECTED;
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            CallBluetoothHelper.getInstance(mApplication).start();
            startTimerTimeCall();
        }*/
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onConnectChange();
            }
        }
    }

    private void notifyOnCallEnd(boolean isBusy) {
        if (!callStateListeners.isEmpty()) {
            Log.d(TAG, "----> callEnd : " + callStateListeners.size());
            for (CallStateInterface listener : callStateListeners) {
                Log.d(TAG, "----> callEnd : for");
                if (isBusy) {
                    listener.onCallBusy();
                } else {
                    listener.onCallEnd();
                }
            }
        } else {
            Log.d(TAG, "----> no listener to callEnd : ");
            Log.f(TAG, "no listener to callEnd");
        }
    }

    private void notifyOnNotSupport(final String msg, boolean isToast) {
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onNotSupport(msg, isToast);
            }
        } else if (isToast) {
            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mApplication, msg, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void notifyOnTickTimeConnect(int timeCall) {
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onTickTimeConnect(timeCall);
            }
        }
    }

    private static void notifyOnQualityReported(long bandwidth) {
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onQualityReported(bandwidth);
            }
        }
    }

    public static void onAddStream(StringeeStream stringeeStream) {
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onAddStream(stringeeStream);
            }
        }
    }

    public static void onLocalStreamCreated(StringeeStream stringeeStream) {
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onLocalStreamCreated(stringeeStream);
            }
        }
    }

    public void onQualityReported(long audio, long video, long mPrevBytesAudioSent, long mPrevBytesVideoSent) {
        notifyOnQualityReported(audio);
        callQualities.add(new CallQuality(audio, video, mPrevBytesAudioSent, mPrevBytesVideoSent, getModeReport()));
        if (callQualities.size() >= 90) {
            handleLogCallQuality();
        }
    }

    public void onMissCall(final String incomingNumber) {
        Log.d(TAG, "onMissCall : " + incomingNumber);
        //TODO disable kich ban nay
        /*if (!NetworkHelper.isConnectInternet(mApplication)
                || !mApplication.isDataReady() || !mApplication.getReengAccountBusiness().isValidAccount()) {
            return;
        }
        if (TextUtils.isEmpty(incomingNumber) ||
                !mApplication.getReengAccountBusiness().isCallEnable() ||
                mApplication.getBlockContactBusiness().isBlockNumber(incomingNumber)) {// khong enable call, so bi block
            return;
        }
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(incomingNumber);
        if (phoneNumber == null || !phoneNumber.isReeng()) {//TODO ko insert miss call tu so vt khong dung nua, cha
        de lam gi
            Log.d(TAG, "onMissCall :  phone == null || !phone.isReeng");
            return;
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // insert message miss call
                ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(incomingNumber);
                insertMessageStateCall(thread, incomingNumber, 0, CallHistoryConstant.STATE_MISS, false, TimeHelper
                        .getCurrentTime(), ReengMessageConstant.MODE_GSM, false);
                // show quick miss call
                SettingBusiness mSettingBusiness = SettingBusiness.getInstance(mApplication);
                if (mSettingBusiness.getPrefMsgInPopup() && mSettingBusiness.getPrefPreviewMsg() &&
                        !mSettingBusiness.checkExistInSilentThread(thread.getId())) {
                    showQuickMissCallActivity(incomingNumber);
                }
            }
        }, 1000);*/// 1 số máy lúc miss call thì check app mocha dang ở foreground nên delay 1 chut
    }

    @Override
    public void onCallGsmStateChanged(int state, String incomingNumber) {
        /*Log.d(TAG, "onCallGsmStateChanged : " + state + " ,number: " + incomingNumber);
        if (isExistCall()) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                CallUtil.stopCall1(mApplication);
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK || state == TelephonyManager.CALL_STATE_IDLE) {
                if (currentCallState == STATE.CONNECTED) {
                    myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
                    ThreadMessage thread = mApplication.getMessageBusiness().findExistingSoloThread(currentFriendJid);
                    sendCallMessage(myJidNumber, currentFriendJid, STATE.KEEP_CALL, thread,
                            currentCaller, currentCallee, codecAudio, currentServer);
                    CallUtil.startCall1(mApplication, currentCallInfo);
                } else if (currentCallInfo.getMyPeerType() == CallInfo.PEER_TYPE_CALLER) {
                    CallUtil.startCall1(mApplication, currentCallInfo);
                }
            }
        }*/
        /*if (state == TelephonyManager.CALL_STATE_IDLE) {
            // nothing
        }*/
    }

    @Override
    public void onXMPPConnected() {
        Log.i(TAG, "onXMPPConnected");
        synchronized (sendCallDataQueue) {
            while (!sendCallDataQueue.isEmpty()) {
                CallData callData = sendCallDataQueue.get(0);
                sendCallDataQueue.remove(0);
                Log.i(TAG, "onXMPPConnected  --- sendMessageCallData");
                if (isCallOut) {
                    sendMessageCallOutData(callData);
                } else {
                    sendMessageCallData(callData, callData.isSdp());
                }
            }
        }
    }

    @Override
    public void onXMPPDisconnected() {
        Log.i(TAG, "onXMPPDisconnected");
        if (!sendCallDataQueue.isEmpty())
            sendCallDataQueue.clear();// disconnect mà chưa send het call data thi clear all
    }

    @Override
    public void onXMPPConnecting() {

    }

    public boolean isExistCall() {

        if (currentCallState == CallConstant.STATE.NON ||
                TextUtils.isEmpty(currentCallSession) ||
                TextUtils.isEmpty(currentFriendJid)) {
            return false;
        }
        return true;
    }

    public boolean isCallOut() {
        return isCallOut;
    }

    public boolean isModeVideoCall() {
        return isEnableMyVideoCall || isEnableFriendVideoCall;
    }

    public boolean isVideoCall() {
        return isVideoCall;
    }

    public boolean isSpeaker() {
        return isSpeaker;
    }

    public boolean isMute() {
        return isMute;
    }

    public int getCurrentTimeCall() {
        return currentTimeCall;
    }

    public int getCurrentTypeCall() {
        return currentTypeCall;
    }

    public int getCurrentCallState() {
        return currentCallState;
    }

    public boolean isStrangerConfide() {
        return isStrangerConfide;
    }

    // chuyen tu audio sang video lan dau can confirm

    public int getStateAudioVideo() {
        return stateAudioVideo;
    }

    public boolean isEnableMyVideoCall() {
        return isEnableMyVideoCall;
    }

    public boolean isEnableFriendVideoCall() {
        return isEnableFriendVideoCall;
    }

    public void setExistCallActivity(boolean isExistCallActivity) { // activity chưa finish();
        this.isExistCallActivity = isExistCallActivity;
    }

    private void resetCall() {
        XMPPManager.removeXMPPConnectivityChangeListener(this);
        this.isCallOut = false;
        this.isCallIn = false;
        //video call
        this.isVideoCall = false;
        this.stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.NON;
        this.isEnableMyVideoCall = false;
        this.isEnableFriendVideoCall = false;
        // stranger confider
        this.isStrangerConfide = false;
        this.currentCallState = STATE.NON;
        this.codecAudio = null;
        this.codecVideo = null;
        this.callData = null;
        // this.currentCallSession = null;
        // TODO chỉ cần set state call là được, tranh trường hợp 2 thằng cùng endcall gây crash
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                releaseAudioFocus();
                resetAudioManager();
                CallBluetoothHelper.getInstance(mApplication).stop();
            }
        }, 3000);
    }

    public void resetDataSetting() {
        sumState = 0;
        isEnableRestartICE = false;
        iceTimeout = 0;
        restartICEDelay = 0;
        restartICEPeriod = 0;
        restartICELoop = 0;
        zeroBwEndCall = 0;
        network2failedTime = 0;
        timedis2recon = 0;
        timeRestartBw = -1;
        delayRestartOnFailed = -1;

        bundlePolicy = "";
        rtcpMuxPolicy = "";
        iceTransportsType = "";

        fcallviafs = 0;
    }

    private void resetAudioManager() {
        audioManager.setMode(originalAudioMode);
        audioManager.setSpeakerphoneOn(originalSpeaker);
        audioManager.setMicrophoneMute(originalMute);
    }

    private boolean checkConnection() {
        if (NetworkHelper.isConnectInternet(mApplication)) {
            Log.f(TAG, "checkConnection isConnectInternet: true");
            if (mApplication.getXmppManager().isConnected()) {
                Log.f(TAG, "checkConnection getXmppManager isConnectInternet: true");
                return true;
            }
        }
        return false;
    }

    //TODO addlog
    private void initCall(String fromJid, String caller, String callee, int callType, boolean isCallOut, boolean
            isCallIn, boolean isVideoCall) {
        Log.d(TAG, "initCall: " + fromJid + " ,caller: " + caller + " ,callee: " + callee);
        originalAudioMode = audioManager.getMode();
        originalSpeaker = audioManager.isSpeakerphoneOn();
        originalMute = audioManager.isMicrophoneMute();
        callQualities = new ArrayList<>();
        this.isVideoCall = isVideoCall;
        stateAudioVideo = isVideoCall ? CallConstant.STATE_AUDIO_VIDEO.ACCEPTED : CallConstant.STATE_AUDIO_VIDEO.NON;
        isEnableMyVideoCall = isVideoCall;
        isEnableFriendVideoCall = isVideoCall;
        isSpeaker = isVideoCall && !isHeadset();// call video thì mở loa ngoài luôn
        isMute = false;
        audioManager.setSpeakerphoneOn(isSpeaker);
        audioManager.setMicrophoneMute(isMute);
        this.currentFriendJid = fromJid;
        this.currentCaller = caller;
        this.currentCallee = callee;
        this.currentTypeCall = callType;
        this.isCallOut = isCallOut;
        this.isCallIn = isCallIn;
        this.candidateHostGenerated = false;// reset callout state
        this.candidateSrflxGenerated = false;
        this.candidateGeneratedDone = false;
        this.candidateCallOutData = new ArrayList<>();
        this.sdpCallOutData = null;
        candidateCount = 0;
        beginTimeConnect = 0;
        currentTimeCall = 0;
        requestAudioFocus();
        //TODO test audio mode
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        XMPPManager.addXMPPConnectivityChangeListener(this);
        sendCallDataQueue.clear();
        isStartedRestartICE = false;
        friendOperator = mApplication.getMessageBusiness().getCPresenceByJid(fromJid).getOperatorPresence();
        usingDesktop = mApplication.getMessageBusiness().getCPresenceByJid(fromJid).getUsingDesktop();
    }

    public void handleAllowedPermissions(int requestCode, String friendJid, final CallData callData) {
        if (TextUtils.isEmpty(friendJid)) {
            handleDeclineCall(false);
            return;
        }
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(friendJid);
        if (requestCode == CallConstant.TYPE_CALLEE) {
            if (isExistCall() && (isCallOut || callData != null)) {
                Log.d(TAG, "start callee Allowed Permission: " + currentFriendJid);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        CallRtcHelper.getInstance(mApplication).startCall(currentTypeCall, currentIceServers,
                                isCallOut, isVideoCall, codecAudio, null, callData);
                    }
                });
            } else {
                Log.f(TAG, "start callee -->> sdpCallData null or !exist call");
                handleDeclineCall(false);
            }
        } else if (requestCode == CallConstant.TYPE_CALLER) {
            myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
            sendIQInitCall(thread, myJidNumber, friendJid);
        } else {
            handleDeclineCall(false);
        }
    }

    public void checkAndStartCallUnknownNumber(BaseSlidingFragmentActivity activity,
                                               String number, boolean isVideo, boolean isDeepLink) {
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(mApplication.getPhoneUtil(), number, mApplication.getReengAccountBusiness()
                        .getRegionCode());
        if (phoneNumberProtocol == null) {
            activity.showToast(mRes.getString(R.string.msg_not_phone_number), Toast.LENGTH_SHORT);
        } else {
            String jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (Utilities.notEmpty(jidNumber) && jidNumber.equals(myNumber)) {
                activity.showToast(mRes.getString(R.string.msg_not_call_me), Toast.LENGTH_SHORT);
            } else if (!PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(),
                    phoneNumberProtocol)) {
                activity.showToast(mRes.getString(R.string.msg_not_phone_number), Toast.LENGTH_SHORT);
            } else {
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(jidNumber);
                if (phoneNumber != null) {
                    if (isDeepLink) {
                        if (phoneNumber.getState() == Constants.CONTACT.ACTIVE) {
                            ThreadMessage threadMessage = mApplication.getMessageBusiness()
                                    .findExistingOrCreateNewThread(jidNumber);
                            handleStartCall(threadMessage, activity, false, false, false);
                        } else {
                            activity.showToast(mRes.getString(R.string.call_not_support), Toast.LENGTH_SHORT);
                        }
                    } else if (isVideo) {
                        if (phoneNumber.getState() == Constants.CONTACT.ACTIVE) {
                            ThreadMessage threadMessage = mApplication.getMessageBusiness()
                                    .findExistingOrCreateNewThread(jidNumber);
                            checkAndStartVideoCall(activity, threadMessage, false);
                        } else {
                            activity.showToast(mRes.getString(R.string.call_not_support), Toast.LENGTH_SHORT);
                        }
                    } else if ((phoneNumber.getState() == Constants.CONTACT.ACTIVE) ||
                            (mApplication.getReengAccountBusiness().isViettel() && phoneNumber.isViettel())) {
                        checkAndStartCall(activity, jidNumber);// show dialog call or toast error
                    } else {
                        /*InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, activity,
                                phoneNumber.getRawNumber(), jidNumber, false);*/
                        ThreadMessage threadMessage = mApplication.getMessageBusiness()
                                .findExistingOrCreateNewThread(jidNumber);
                        handleStartCall(threadMessage, activity, false, false, false);
                    }
                } else {
                    //comment if phone offline
                    checkInfoNumberAndCall(activity, jidNumber, isVideo, isDeepLink);
                }
            }
        }
    }

    private void checkInfoNumberAndCall(final BaseSlidingFragmentActivity activity, final String jidNumber,
                                        final boolean videoCall, final boolean isDeepLink) {
        ArrayList<String> listNumbers = new ArrayList<>();
        listNumbers.add(jidNumber);
        activity.showLoadingDialog(null, mRes.getString(R.string.waiting));
        ContactRequestHelper.getInstance(mApplication).getInfoContactsFromNumbers(listNumbers, new
                ContactRequestHelper.onResponseInfoContactsListener() {
                    @Override
                    public void onResponse(ArrayList<PhoneNumber> responses) {
                        activity.hideLoadingDialog();
                        ContactBusiness contactBusiness = mApplication.getContactBusiness();
                        String rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mApplication, jidNumber);
                        if (responses != null && !responses.isEmpty()) {// so dung mocha
                            PhoneNumber phoneNumber = responses.get(0);
                            //  cap nhat danh sach nonContact
                            contactBusiness.insertOrUpdateNonContact(phoneNumber, true, true);
                            if (isDeepLink) {
                                if (phoneNumber.getState() == Constants.CONTACT.ACTIVE) {
                                    ThreadMessage threadMessage = mApplication.getMessageBusiness()
                                            .findExistingOrCreateNewThread(jidNumber);
                                    handleStartCall(threadMessage, activity, false, false, false);
                                } else {
                                    activity.showToast(mRes.getString(R.string.call_not_support), Toast.LENGTH_SHORT);
                                }
                            } else if (videoCall) {
                                if (phoneNumber.getState() == Constants.CONTACT.ACTIVE) {
                                    ThreadMessage threadMessage = mApplication.getMessageBusiness()
                                            .findExistingOrCreateNewThread(jidNumber);
                                    checkAndStartVideoCall(activity, threadMessage, false);
                                } else {
                                    activity.showToast(mRes.getString(R.string.call_not_support), Toast.LENGTH_SHORT);
                                }
                            } else if ((phoneNumber != null && phoneNumber.getState() == Constants.CONTACT.ACTIVE) ||
                                    (mApplication.getReengAccountBusiness().isViettel()
                                            && phoneNumber != null && phoneNumber.isViettel())) {
                                checkAndStartCall(activity, jidNumber);// show dialog call or toast error
                            } else {
                                InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, activity,
                                        rawNumber, jidNumber, false);
                            }
                        } else if (videoCall || isDeepLink) {
                            activity.showToast(mRes.getString(R.string.call_not_support), Toast.LENGTH_SHORT);
                        } /*else if (mApplication.getReengAccountBusiness().isViettel() && PhoneNumberHelper
                                .getInstant()
                                .isViettelNumber(jidNumber)) {
                            PhoneNumber phoneNumber = new PhoneNumber();
                            phoneNumber.setJidNumber(jidNumber);
                            phoneNumber.setState(Constants.CONTACT.NONE);
                            contactBusiness.insertOrUpdateNonContact(phoneNumber, true);
                            checkAndStartCall(activity, jidNumber);// show dialog call or toast error
                        }*/ else {
                            InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, activity,
                                    rawNumber, jidNumber, false);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        activity.hideLoadingDialog();
                        activity.showToast(mRes.getString(R.string.e500_internal_server_error), Toast.LENGTH_SHORT);
                    }
                });
    }

    public void checkAndStartCall(BaseSlidingFragmentActivity activity, String friendNumber) {
        checkAndStartCall(activity, friendNumber, false);
    }

    public void checkAndStartCall(BaseSlidingFragmentActivity activity, String friendNumber, boolean fromThreadDetail) {
        //Check goi cho chinh minh
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(mApplication.getPhoneUtil(), friendNumber, mApplication.getReengAccountBusiness().getRegionCode());
        String jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (jidNumber != null && myNumber.equals(jidNumber)) {
            activity.showToast(mRes.getString(R.string.msg_not_call_me), Toast.LENGTH_SHORT);
            return;
        }

        ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(friendNumber);
        checkAndStartCall(activity, threadMessage, fromThreadDetail);
    }

    public void checkAndStartCall(final BaseSlidingFragmentActivity activity, final ThreadMessage threadMessage) {
        checkAndStartCall(activity, threadMessage, false);
    }

    public void checkAndStartCall(final BaseSlidingFragmentActivity activity, final ThreadMessage threadMessage, final boolean fromThreadDetail) {
        if (Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE) {
            // check permission before call
            final ArrayList<String> array = new ArrayList<>();
            if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.RECORD_AUDIO)) {
                array.add(Manifest.permission.RECORD_AUDIO);
            }
            //if (mCallBusiness.isModeVideoCall() && PermissionHelper.declinedPermission(mApplication, Manifest
            // .permission.CAMERA)) {
            // call thuong switch sang video dc nen can xin quyen luon
            if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.CAMERA)) {
                array.add(Manifest.permission.CAMERA);
            }
            if (array.size() > 0) {
                String[] permissions = new String[array.size()];
                permissions = array.toArray(permissions);
                mApplication.getPref().edit().putBoolean(IS_REQUEST_PERMISSION_FOR_CALL, true).commit();
                PermissionHelper.requestRxPermissions(activity, permissions, new PermissionHelper.ResultPermissionCallback() {
                    @Override
                    public void onPermissionGranted(Permission permission) {
                        checkAndStartCall(activity, threadMessage, fromThreadDetail);
                    }

                    @Override
                    public void onPermissionPermanentlyDenied(Permission permission) {
                        if (activity.isDialogShowing(PermissionDialog.TAG)) return;
                        PermissionDialog.newInstance(Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO, true, new PermissionDialog.CallBack() {
                            @Override
                            public void onPermissionAllowClick(boolean allow, int clickCount) {
                                if (allow) {
                                    //go to settings
                                    activity.dismissDialogFragment(PermissionDialog.TAG);
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                                    intent.setData(uri);
                                    activity.startActivity(intent);
                                    PermissionHelper.showDialogPermissionSettingIntro(activity, array, Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO);
                                }
                            }
                        }).show(activity.getSupportFragmentManager(), PermissionDialog.TAG);
                    }

                    @Override
                    public void onShouldShowRequestPermissionRationale(Permission permission) {
                        // doSomething later
                    }
                });
                return;
            }
        }

        if (threadMessage == null) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else if (threadMessage.isStranger() || threadMessage.getThreadType() != ThreadMessageConstant
                .TYPE_THREAD_PERSON_CHAT) {
            if (threadMessage.isStranger()) {
                StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(threadMessage.getSoloNumber());
                if (strangerPhoneNumber != null)
                    activity.showToast(String.format(activity.getString(R.string.only_make_call_friend), strangerPhoneNumber.getFriendName()));
                else
                    activity.showToast(R.string.e666_not_support_function);
            } else
                activity.showToast(R.string.e666_not_support_function);
        } else {
            if (mApplication.getReengAccountBusiness().isEnableCallOut()) {
                if (!mApplication.getReengAccountBusiness().isCallEnable() ||
                        checkCallOut(threadMessage.getSoloNumber())) {
                    handleStartCall(threadMessage, activity, true, false, fromThreadDetail);
                } else {
                    new DialogCallOption(activity, threadMessage, new PopupHelper.DialogCallOptionListener() {
                        @Override
                        public void onCallFree(ThreadMessage threadMessage) {
                            handleStartCall(threadMessage, activity, false, false, fromThreadDetail);
                        }

                        @Override
                        public void onCallOut(ThreadMessage threadMessage) {
                            handleStartCall(threadMessage, activity, true, false, fromThreadDetail);
                        }

                        @Override
                        public void onVideoCall(ThreadMessage threadMessage) {
                            handleStartCall(threadMessage, activity, false, true, fromThreadDetail);
                        }
                    }).show();
                }
            } else {
                if (!mApplication.getReengAccountBusiness().isCallEnable() ||
                        checkCallOut(threadMessage.getSoloNumber())) {
                    //activity.showToast(R.string.call_out_disable);
                    activity.showToast(R.string.e666_not_support_function);
                } else {
                    handleStartCall(threadMessage, activity, false, false, fromThreadDetail);
                }
            }
        }
    }

    public void createCallOutAfterSelectNumberFree(final BaseSlidingFragmentActivity activity, String msisdn) {
        ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(msisdn);
        if (mApplication.getReengAccountBusiness().isEnableCallOut()) {
            handleStartCall(threadMessage, activity, true, false, false);
        }
    }

    public void checkAndStartVideoCall(final BaseSlidingFragmentActivity activity, final PhoneNumber phoneNumber, final boolean fromThreadDetail) {
        // check permission before call
        if (Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE) {
            final ArrayList<String> array = new ArrayList<>();
            if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.RECORD_AUDIO)) {
                array.add(Manifest.permission.RECORD_AUDIO);
            }
            //if (mCallBusiness.isModeVideoCall() && PermissionHelper.declinedPermission(mApplication, Manifest
            // .permission.CAMERA)) {
            // call thuong switch sang video dc nen can xin quyen luon
            if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.CAMERA)) {
                array.add(Manifest.permission.CAMERA);
            }
            if (array.size() > 0) {
                String[] permissions = new String[array.size()];
                permissions = array.toArray(permissions);
                mApplication.getPref().edit().putBoolean(IS_REQUEST_PERMISSION_FOR_CALL, true).commit();
                PermissionHelper.requestRxPermissions(activity, permissions, new PermissionHelper.ResultPermissionCallback() {
                    @Override
                    public void onPermissionGranted(Permission permission) {
                        checkAndStartVideoCall(activity, phoneNumber, fromThreadDetail);
                    }

                    @Override
                    public void onPermissionPermanentlyDenied(Permission permission) {
                        if (activity.isDialogShowing(PermissionDialog.TAG)) return;
                        PermissionDialog.newInstance(Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO, true, new PermissionDialog.CallBack() {
                            @Override
                            public void onPermissionAllowClick(boolean allow, int clickCount) {
                                if (allow) {
                                    //go to settings
                                    activity.dismissDialogFragment(PermissionDialog.TAG);
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                                    intent.setData(uri);
                                    activity.startActivity(intent);
                                    PermissionHelper.showDialogPermissionSettingIntro(activity, array, Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO);
                                }
                            }
                        }).show(activity.getSupportFragmentManager(), PermissionDialog.TAG);
                    }

                    @Override
                    public void onShouldShowRequestPermissionRationale(Permission permission) {
                        // doSomething later
                    }
                });
                return;
            }
        }

        if (phoneNumber == null || phoneNumber.getJidNumber() == null) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else if (phoneNumber.isReeng()) {
            //Check goi cho chinh minh
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                    getPhoneNumberProtocol(mApplication.getPhoneUtil(), phoneNumber.getJidNumber(), mApplication.getReengAccountBusiness().getRegionCode());
            String jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (jidNumber != null && myNumber.equals(jidNumber)) {
                activity.showToast(mRes.getString(R.string.msg_not_call_me), Toast.LENGTH_SHORT);
                return;
            }

            ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(phoneNumber.getJidNumber());
            handleStartCall(threadMessage, activity, false, true, fromThreadDetail);
        } else {
            activity.showToast(R.string.e666_not_support_function);
        }
    }

    public void checkAndStartVideoCall(final BaseSlidingFragmentActivity activity, final ThreadMessage threadMessage, final boolean fromThreadDetail) {
        // check permission before call
        if (Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE) {
            final ArrayList<String> array = new ArrayList<>();
            if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.RECORD_AUDIO)) {
                array.add(Manifest.permission.RECORD_AUDIO);
            }
            //if (mCallBusiness.isModeVideoCall() && PermissionHelper.declinedPermission(mApplication, Manifest
            // .permission.CAMERA)) {
            // call thuong switch sang video dc nen can xin quyen luon
            if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.CAMERA)) {
                array.add(Manifest.permission.CAMERA);
            }
            if (array.size() > 0) {
                String[] permissions = new String[array.size()];
                permissions = array.toArray(permissions);

                PermissionHelper.requestRxPermissions(activity, permissions, new PermissionHelper.ResultPermissionCallback() {
                    @Override
                    public void onPermissionGranted(Permission permission) {
                        checkAndStartVideoCall(activity, threadMessage, fromThreadDetail);
                    }

                    @Override
                    public void onPermissionPermanentlyDenied(Permission permission) {
                        if (activity.isDialogShowing(PermissionDialog.TAG)) return;
                        PermissionDialog.newInstance(Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO, true, new PermissionDialog.CallBack() {
                            @Override
                            public void onPermissionAllowClick(boolean allow, int clickCount) {
                                if (allow) {
                                    //go to settings
                                    activity.dismissDialogFragment(PermissionDialog.TAG);
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                                    intent.setData(uri);
                                    activity.startActivity(intent);
                                    PermissionHelper.showDialogPermissionSettingIntro(activity, array, Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO);
                                }
                            }
                        }).show(activity.getSupportFragmentManager(), PermissionDialog.TAG);
                    }

                    @Override
                    public void onShouldShowRequestPermissionRationale(Permission permission) {
                        // doSomething later
                    }
                });
                return;
            }
        }

        if (threadMessage == null) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else if (threadMessage.isStranger() || threadMessage.getThreadType() != ThreadMessageConstant
                .TYPE_THREAD_PERSON_CHAT ||
                !mApplication.getReengAccountBusiness().isCallEnable() || checkCallOut(threadMessage.getSoloNumber())) {
            if (threadMessage.isStranger()) {
                StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(threadMessage.getSoloNumber());
                if (strangerPhoneNumber != null)
                    activity.showToast(String.format(activity.getString(R.string.only_make_call_friend), strangerPhoneNumber.getFriendName()));
                else
                    activity.showToast(R.string.e666_not_support_function);
            } else
                activity.showToast(R.string.e666_not_support_function);
        } else {
            handleStartCall(threadMessage, activity, false, true, fromThreadDetail);
        }
    }

    /*public void checkAndStartVideoCall(final BaseSlidingFragmentActivity activity,
                                       final ThreadMessage threadMessage, boolean finishActivity) {
        if (threadMessage == null) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else if (threadMessage.isStranger() || threadMessage.getThreadType() != ThreadMessageConstant
                .TYPE_THREAD_PERSON_CHAT ||
                !mApplication.getReengAccountBusiness().isCallEnable() || checkCallOut(threadMessage.getSoloNumber())) {
            activity.showToast(R.string.e666_not_support_function);
        } else {
            handleStartCall(threadMessage, activity, false, true, finishActivity);
        }
    }*/

    public void handleStartCall(final ThreadMessage threadMessage, final BaseSlidingFragmentActivity baseActivity, final boolean
            isCallOut, final boolean isVideoCall) {
        handleStartCall(threadMessage, baseActivity, isCallOut, isVideoCall, false);
    }

    public void handleStartCall(final ThreadMessage threadMessage, final BaseSlidingFragmentActivity baseActivity, final boolean
            isCallOut, final boolean isVideoCall, final boolean fromThreadDetail) {
        if (Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE) {
            //check permission
            final ArrayList<String> array = new ArrayList<>();
            if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.RECORD_AUDIO)) {
                array.add(Manifest.permission.RECORD_AUDIO);
            }
            //if (mCallBusiness.isModeVideoCall() && PermissionHelper.declinedPermission(mApplication, Manifest
            // .permission.CAMERA)) {
            // call thuong switch sang video dc nen can xin quyen luon
            if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.CAMERA)) {
                array.add(Manifest.permission.CAMERA);
            }
            if (array.size() > 0) {
                String[] permissions = new String[array.size()];
                permissions = array.toArray(permissions);

                PermissionHelper.requestRxPermissions(baseActivity, permissions, new PermissionHelper.ResultPermissionCallback() {
                    @Override
                    public void onPermissionGranted(Permission permission) {
                        handleStartCall(threadMessage, baseActivity, isCallOut, isVideoCall, fromThreadDetail);
                    }

                    @Override
                    public void onPermissionPermanentlyDenied(Permission permission) {
                        if (baseActivity.isDialogShowing(PermissionDialog.TAG)) return;
                        PermissionDialog.newInstance(Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO, true, new PermissionDialog.CallBack() {
                            @Override
                            public void onPermissionAllowClick(boolean allow, int clickCount) {
                                if (allow) {
                                    //go to settings
                                    baseActivity.dismissDialogFragment(PermissionDialog.TAG);
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", baseActivity.getPackageName(), null);
                                    intent.setData(uri);
                                    baseActivity.startActivity(intent);
                                    PermissionHelper.showDialogPermissionSettingIntro(baseActivity, array, Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO);
                                }
                            }
                        }).show(baseActivity.getSupportFragmentManager(), PermissionDialog.TAG);
                    }

                    @Override
                    public void onShouldShowRequestPermissionRationale(Permission permission) {
                        // doSomething later
                    }
                });
                return;
            }
        }
        String sessionCall = PacketMessageId.getInstance().genPacketId("chat", "call_rtc_2");
        handleStartCall(threadMessage, baseActivity, isCallOut, isVideoCall, false, sessionCall, fromThreadDetail);
    }

    /*public void handleStartCall(ThreadMessage threadMessage, BaseSlidingFragmentActivity baseActivity, boolean
            isCallOut, boolean isVideoCall, boolean finishActivity) {
        String sessionCall = PacketMessageId.getInstance().genPacketId("chat", "call_rtc_2");
        handleStartCall(threadMessage, baseActivity, isCallOut, isVideoCall, false, sessionCall, finishActivity);
    }*/

    public void handleStartStrangerConfide(ThreadMessage threadMessage, BaseSlidingFragmentActivity baseActivity,
                                           StrangerMusic stranger) {
        handleStartCall(threadMessage, baseActivity, isCallOut, isVideoCall, true, stranger.getSessionId(), false);
    }

    // thuc hien cuoc goi
    private void handleStartCall(ThreadMessage threadMessage, BaseSlidingFragmentActivity baseActivity, boolean
            isCallOut, boolean isVideoCall, boolean isConfider, String sessionCall, boolean fromThreadDetail) {
        synchronized (TAG) {
            if (!checkConnection()) {
                baseActivity.showToast(R.string.e604_error_connect_server);
                return;
            }
            if (isExistCall()) {
                baseActivity.showToast(R.string.e601_error_but_undefined);
                return;
            }
            String jidFriend = threadMessage.getSoloNumber();
            myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
            initCall(jidFriend, myJidNumber, jidFriend, CallConstant.TYPE_CALLER, isCallOut, false, isVideoCall);
            this.isStrangerConfide = isConfider;
            this.currentCallSession = sessionCall;
            this.currentCallState = CallConstant.STATE.TRYING;//100
            startTimerTimeout(CallConstant.CALL_TIME_OUT);// start trước, android 6 can xin quyen thi reset sau
            //navigate to caller view
            notifyCall(currentFriendJid, CallConstant.TYPE_CALLER, false);
            Intent intent = new Intent(mApplication, MochaCallActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(CallConstant.JIDNUMBER, currentFriendJid);
            bundle.putInt(CallConstant.TYPE_FRAGMENT, CallConstant.TYPE_CALLER);
            bundle.putBoolean(CallConstant.FIRST_START_ACTIVITY, true);
            intent.putExtras(bundle);
            baseActivity.startActivity(intent);
            ListenerHelper.getInstance().addGsmCallListener(this);
            EventBus.getDefault().post(new RecallActivity.RecallActivityEvent());

            if (fromThreadDetail && threadMessage.isHasNewMessage()) {
                mApplication.getMessageBusiness().markAllMessageIsOld(threadMessage, threadMessage.getId());
            }
        }
    }

    public void processSendCallData(CallData callData, boolean isSdp) {
        Log.i(TAG, "processSendCallData");
        if (!isExistCall()) {
            removeListenerWhenEndCall();
            return;
        }
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        synchronized (sendCallDataQueue) {
            //if (mApplication.getXmppManager().isAuthenticated() && sendCallDataQueue.isEmpty()) {
            if (mApplication.getXmppManager().isAuthenticated()) {//TODO ko cần đảm bảo thứ tự candidate nên send luôn
                Log.i(TAG, "processSendCallData --- sendMessageCallData");
                sendMessageCallData(callData, isSdp);
            } else {
                sendCallDataQueue.add(callData);
            }
        }
    }

    private void sendMessageCallData(CallData callData, boolean isSdp) {
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(currentFriendJid);
        if (isSdp && currentTypeCall == CallConstant.TYPE_CALLER) {
            String settingXML = getSettingXML();
            sendCallMessage(myJidNumber, currentFriendJid, null, thread, currentCaller, currentCallee, callData,
                    currentIceServers, settingXML);
        } else {
            sendCallMessage(myJidNumber, currentFriendJid, null, thread, currentCaller, currentCallee, callData, null);
        }
    }

    private String getSettingXML() {
        if (currentTypeCall == CallConstant.TYPE_CALLER && !isCallOut) {
            StringBuilder buf = new StringBuilder();
            buf.append("<setting")
                    .append(" restartICE=").append("\"").append(isEnableRestartICE ? "1" : "0").append("\"")
                    .append(" iceTimeout=").append("\"").append(iceTimeout).append("\"")
                    .append(" restartICEDelay=").append("\"").append(restartICEDelay).append("\"")
                    .append(" restartICEPeriod=").append("\"").append(restartICEPeriod).append("\"")
                    .append(" restartICELoop=").append("\"").append(restartICELoop).append("\"")
                    .append(" zeroBwEndCall=").append("\"").append(zeroBwEndCall).append("\"")
                    .append(" network2failedTime=").append("\"").append(network2failedTime).append("\"")
                    .append(" timedis2recon=").append("\"").append(timedis2recon).append("\"")
                    .append(" timeRestartBw=").append("\"").append(timeRestartBw).append("\"")
                    .append(" delayRestartOnFailed=").append("\"").append(delayRestartOnFailed).append("\"");
            if (!TextUtils.isEmpty(bundlePolicy))
                buf.append(" bundlePolicy=").append("\"").append(bundlePolicy).append("\"");
            if (!TextUtils.isEmpty(rtcpMuxPolicy))
                buf.append(" rtcpMuxPolicy=").append("\"").append(rtcpMuxPolicy).append("\"");
            if (!TextUtils.isEmpty(iceTransportsType))
                buf.append(" iceTransportsType=").append("\"").append(iceTransportsType).append("\"");

            buf.append(" />");
            return buf.toString();
        } else
            return null;

    }

    private void handleReceivedCallSdp(ReengCallPacket receivedPacket, ThreadMessage threadMessage, String fromNumber) {
        synchronized (TAG) {
            if (isCallee(receivedPacket)) {
                if ("end".equals(receivedPacket.getAttrStatus())) {
                    insertMessageStateCall(threadMessage, fromNumber, 0, CallHistoryConstant.STATE_MISS, false,
                            receivedPacket.getTimeSend(), ReengMessageConstant.MODE_IP_IP, receivedPacket
                                    .isCallConfide(), stateAudioVideo);
                } else if (isExistCall() || isExistCallActivity) {
                    int stateAudioVideoOfCall = receivedPacket.isVideoCall() ? CallConstant.STATE_AUDIO_VIDEO.ACCEPTED : CallConstant.STATE_AUDIO_VIDEO.NON;
                    sendCallMessageBusy(myJidNumber, fromNumber, threadMessage, receivedPacket);
                    insertMessageStateCall(threadMessage, fromNumber, 0, CallHistoryConstant.STATE_MISS, false,
                            receivedPacket.getTimeSend(), ReengMessageConstant.MODE_IP_IP, receivedPacket
                                    .isCallConfide(), stateAudioVideoOfCall);
                } else {// incoming call
                    MochaCallMessage inviteMessage = mapPacketToCallMessage(receivedPacket, threadMessage.getId(),
                            fromNumber, myJidNumber, receivedPacket.getSubType());
                    handleIncomingCall(inviteMessage, receivedPacket, threadMessage);
                }
            } else if (isExistCall()) {
                CallRtcHelper.getInstance(mApplication).addCallData(receivedPacket.getCallData(), isCallOut);
            } else {
                Log.e(TAG, "received call data sdp when !exist call");
            }
        }
    }

    private void handleReceivedCallCandidate(ReengCallPacket receivedPacket) {
        synchronized (TAG) {
            if (isExistCall()) {
                //if (isCallee(receivedPacket)) {
                if (currentCallState >= CallConstant.STATE.CONNECT) {
                    CallRtcHelper.getInstance(mApplication).addCallData(receivedPacket.getCallData(), isCallOut);
                } else {
                    CallRtcHelper.getInstance(mApplication).addCallDataToQueue(receivedPacket.getCallData());
                }
                /*} else {
                    CallRtcHelper.getInstance(mApplication).addCallData(receivedPacket.getCallData(), isCallOut);
                }*/
            } else {
                Log.e(TAG, "received call data candidate when !exist call");
            }
        }
    }

    //TODO
    private void handleReceivedCallRemoveCandidate(ReengCallPacket receivedPacket) {
        synchronized (TAG) {
            if (isExistCall()) {
                //if (isCallee(receivedPacket)) {
                if (currentCallState >= CallConstant.STATE.CONNECT) {
                    CallRtcHelper.getInstance(mApplication).removeCallData(receivedPacket.getCallData(), isCallOut);
                } else {
                    Log.i(TAG, "handleReceivedCallRemoveCandidate");
                    CallRtcHelper.getInstance(mApplication).removeCallDataToQueue(receivedPacket.getCallData());
                }
            } else {
                Log.e(TAG, "handleReceivedCallRemoveCandidate remove candidate when !exist call");
            }
        }
    }

    private void handleTryingCall(final ThreadMessage threadMessage, final ArrayList<IceServer> iceServers
            , final String codec, final String videoCodec) {
        synchronized (TAG) {
            if (!isExistCall()) {
                Log.i(TAG, "!ExistCall -> (chờ result mà ng dùng end luôn thì ko xử lý nữa)");
                return;
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    currentIceServers = CallRtcHelper.convertStringeeIceServers(iceServers);
                    codecAudio = codec;
                    codecVideo = videoCodec;
                    if (isCallOut) {
                        if (isFcallviafs()) startRingBackCall();
                        stopMediaPlayer();
                        sendMessageCallOutStart(threadMessage, threadMessage.getSoloNumber());
                    } else {
                        startRingBackCall();
                        CallRtcHelper.getInstance(mApplication).startCall(currentTypeCall, currentIceServers,
                                isCallOut, isVideoCall, codecAudio, codecVideo);
                    }

                }
            });
        }
    }

    // cuoc goi den
    private void handleIncomingCall(MochaCallMessage callMessage, ReengCallPacket callPacket, ThreadMessage
            threadMessage) {
        synchronized (TAG) {
            long timeOut = TimeHelper.getTimeOutIncomingCall(callPacket.getTimeSend(), callPacket.getTimeReceive());
            if (timeOut <= 500) {// het timeout cho cuoc goi
                insertMessageMissCall(threadMessage, callMessage.getSender(), false,
                        callMessage.getTime(), callPacket.isCallConfide(), ReengMessageConstant.MODE_IP_IP);
                return;
            }

            iceTimeout = callMessage.getIceTimeout() == 0
                    ? CallConstant.SETTING_CALL.ICE_TIME_OUT : callMessage.getIceTimeout();
            restartICEDelay = callMessage.getRestartICEDelay();
            restartICEPeriod = callMessage.getRestartICEPeriod() == 0
                    ? CallConstant.SETTING_CALL.RESTART_ICE_PERIOD : callMessage.getRestartICEPeriod();
            restartICELoop = callMessage.getRestartICELoop() == 0
                    ? CallConstant.SETTING_CALL.RESTART_ICE_LOOP : callMessage.getRestartICELoop();
            zeroBwEndCall = callMessage.getZeroBwEndCall() == 0
                    ? CallConstant.SETTING_CALL.ZERO_BW_END_CALL : callMessage.getZeroBwEndCall();
            network2failedTime = callMessage.getNetwork2failedTime();
            timedis2recon = callMessage.getTimedis2recon() == 0
                    ? CallConstant.SETTING_CALL.TIME_DIS_2_RECON : callMessage.getTimedis2recon();
            timeRestartBw = callMessage.getTimeRestartBw();
            delayRestartOnFailed = callMessage.getDelayRestartOnFailed();
            bundlePolicy = callMessage.getBundlePolicy();
            rtcpMuxPolicy = callMessage.getRtcpMuxPolicy();
            iceTransportsType = callMessage.getIceTransportsType();

            String jidFriend = threadMessage.getSoloNumber();
            myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
            // nhan cuoc goi call nen callOut = false
            initCall(jidFriend, callMessage.getCaller(), callMessage.getCallee(), CallConstant.TYPE_CALLEE, false,
                    false, callPacket.isVideoCall());
            this.isStrangerConfide = callPacket.isCallConfide();
            currentCallSession = callMessage.getCallSession();
            currentIceServers = CallRtcHelper.convertStringeeIceServers(callMessage.getIceServers());
            if (isStrangerConfide) {
                mApplication.getMusicBusiness().checkResetConfideSessionAndNotifyRefresh(callMessage.getCallSession());
            }
            // send ringing
            this.currentCallState = STATE.RINGING;//180
            ListenerHelper.getInstance().addGsmCallListener(this);
            sendMessageRinging(threadMessage, jidFriend);
            startTimerTimeout(timeOut);
            // start incoming screen
            currentCallState = CallConstant.STATE.CALLEE_STARTED;
            startRingIncomingCall();
            this.callData = callMessage.getCallData();
            handler.post(() -> notifyCall(currentFriendJid, CallConstant.TYPE_CALLEE, false, callMessage.getCallData()));
            // TODO hiện tại : samsung android chưa cấp quyền draw sẽ bị ẩn notify  khi kill app vừa bị kill
            //  . Tạm thời fix hiển thị lại 4s.
//            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q
//                    && !Settings.canDrawOverlays(mApplication.getApplicationContext())) {
//                handler.postDelayed(() -> {
//                    if (isExistCall()) {
//                        notifyCall(currentFriendJid, CallConstant.TYPE_CALLEE, isCallConnected(), callMessage.getCallData());
//                    }
//                }, 4000);
//            }
            Intent intent = new Intent(mApplication, MochaCallActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent
                    .FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_FROM_BACKGROUND);
            Bundle bundle = new Bundle();
            bundle.putString(CallConstant.JIDNUMBER, currentCaller);
            bundle.putInt(CallConstant.TYPE_FRAGMENT, CallConstant.TYPE_CALLEE);
            bundle.putParcelable(CallConstant.SDP_CALL_DATA, callMessage.getCallData());
            bundle.putBoolean(CallConstant.FIRST_START_ACTIVITY, true);
            intent.putExtras(bundle);
            mApplication.startActivity(intent);
            EventBus.getDefault().post(new RecallActivity.RecallActivityEvent());
        }
    }

    // gửi bản tin 180 ringing va start timer, can check permission android 6
    public void sendMessageRinging(ThreadMessage thread, String jidFriend) {
        sendCallMessage(myJidNumber, jidFriend, CallError.ringing, thread,
                currentCaller, currentCallee, null, null);
        notifyOnRinging();
    }

    public void sendMessageStateICE(StringeeCallListener.StringeeConnectionState state) {
        if (!isCaller() || !isEnableRestartICE || state == StringeeCallListener.StringeeConnectionState.CONNECTED)
            return;
        CallError callError = null;
        ReengCallOutPacket.CallStatus callStatus = null;
        if (state == StringeeCallListener.StringeeConnectionState.CHECKING) {
            callError = CallError.connecting;
            callStatus = ReengCallOutPacket.CallStatus.connecting;
        } else if (state == StringeeCallListener.StringeeConnectionState.FAILED) {
            callError = CallError.failed;
            callStatus = ReengCallOutPacket.CallStatus.failed;
        } else if (state == StringeeCallListener.StringeeConnectionState.DISCONNECTED) {
            callError = CallError.disconnect;
            callStatus = ReengCallOutPacket.CallStatus.disconnect;
        } else if (state == StringeeCallListener.StringeeConnectionState.CLOSED) {
            callError = CallError.close;
            callStatus = ReengCallOutPacket.CallStatus.close;
        } else if (state == StringeeCallListener.StringeeConnectionState.RESTARTICE_ONFAIL) {
            callError = CallError.restartICE;
            callStatus = ReengCallOutPacket.CallStatus.restartICE;
        } else if (state == StringeeCallListener.StringeeConnectionState.RESTARTICE_CONNECTED) {
            callError = CallError.restartSuccess;
            callStatus = ReengCallOutPacket.CallStatus.restartSuccess;
        }

        if (state == StringeeCallListener.StringeeConnectionState.RESTARTICE_CONNECTED
                || state == StringeeCallListener.StringeeConnectionState.CONNECTED) {
            if (audioManager != null) {
                Log.i(TAG, "-------------audioManager setSpeakerphoneOn " + isSpeaker);
                audioManager.setSpeakerphoneOn(isSpeaker);
            }
        }

        ThreadMessage thread = mApplication.getMessageBusiness().findExistingSoloThread(currentFriendJid);
        if (!isCallOut && callError != null) {
            sendCallKPIMessage(myJidNumber, currentFriendJid, thread,
                    currentCaller, currentCallee, null, null, onlyAudio, 0, isStartedRestartICE, callError);
        } else if (isCallOut && callStatus != null) {
            sendCallOutKPIMessage(myJidNumber, currentFriendJid, ReengCallOutPacket.CallOutType.receive_status,
                    thread, currentCaller, currentCallee, null, null, 0, isCallIn, isStartedRestartICE, callStatus);
        }
    }

    // minh tra loi cuoc goi
    public void handleAnswerCall(BaseSlidingFragmentActivity activity, boolean isOnlyAudio) {
        synchronized (TAG) {
            if (!checkConnection()) {
                activity.showToast(R.string.e604_error_connect_server);
                return;
            }
            if (!isExistCall()) {//???
                activity.showToast("end call", Toast.LENGTH_LONG);
                notifyCallEnd(false);
                return;
            }
            this.onlyAudio = isOnlyAudio;
            stopTimerTimeout();
            stopMediaPlayer();
            mVibrator.cancel();
            if (isCallOut) {
                processAnswerCallIn();
            } else {
                if (isOnlyAudio) resetVideoModeWhenAnswerOnlyAudio();
                audioManager.setSpeakerphoneOn(isSpeaker);
                //setSpeakerOn(false);
                ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread
                        (currentFriendJid);
                myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
                sendCallMessage(myJidNumber, currentFriendJid, CallError.connected, threadMessage,
                        currentCaller, currentCallee, null, null, isOnlyAudio, null);
                handler.post(() -> CallRtcHelper.getInstance(mApplication).putAllCallDataFromQueue()); // use handle to avoid field currentCall == null
                notifyOnConnectStateChange(STATE.CONNECT);
                beginTimeConnect = System.currentTimeMillis();
            }
            notifyCall(currentFriendJid, CallConstant.TYPE_CALLEE, true);
//            if(isStrangerConfide){
//                LuckyWheelHelper.getInstance(mApplication).doMission(Constants.LUCKY_WHEEL.ITEM_TALK_STRANGER);
//            }
        }
    }

    // ban be tra loi
    private void handleAcceptCall(boolean isOnlyAudio) {
        synchronized (TAG) {
            stopTimerTimeout();
            stopMediaPlayer();
            mVibrator.cancel();
            if (!isExistCall()) return;
            if (isOnlyAudio) {
                resetVideoModeWhenAnswerOnlyAudio();
            }
            CallRtcHelper.getInstance(mApplication).putAllCallDataFromQueue();
            // TODO: 5/22/2020 doMission call video ITEM_VIDEO_CALL
            if (isCallOut) {
                notifyOnConnectStateChange(STATE.CONNECTED);
                LuckyWheelHelper.getInstance(ApplicationController.self()).doMission(Constants.LUCKY_WHEEL.ITEM_CALL_OUT);
            } else {
                notifyOnConnectStateChange(STATE.CONNECT);
                LuckyWheelHelper.getInstance(ApplicationController.self()).doMission(Constants.LUCKY_WHEEL.ITEM_VIDEO_CALL);
            }
            beginTimeConnect = System.currentTimeMillis();
            notifyCall(currentFriendJid, CallConstant.TYPE_CALLER, true);
        }
    }

    // do chuong
    private void handleRinging() {
        synchronized (TAG) {
            if (!isExistCall()) return;
            this.currentCallState = STATE.RINGING;
            notifyOnRinging();
        }
    }

    // minh tu choi cuoc goi
    public void handleDeclineCall(boolean isBusy) {
        synchronized (TAG) {
            Log.f(TAG, "handleDeclineCall: " + currentFriendJid + " session: " + currentCallSession);
            removeListenerWhenEndCall();
            if (!isExistCall()) {
                notifyCallEnd(false);
                return;
            }
            startRingEndCall();
            myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
            ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(currentFriendJid);
            if (thread != null) {
                if (isCallOut) {
                    if (currentTypeCall == CallConstant.TYPE_CALLEE) {
                        declineCallIn(thread, isBusy);
                    } else {
                        declineCallOut(thread, isBusy);
                    }
                } else {
                    declineCall(thread, isBusy);
                }
            }
            handleLogCallState();
            handleLogCallQuality();
            notifyCallEnd(false);
        }
    }

    private void declineCall(ThreadMessage thread, boolean isBusy) {
        CallError callError;
        if (isBusy) {
            callError = CallError.busyCall;
        } else {
            callError = CallError.endCall;
        }
        sendCallMessage(myJidNumber, currentFriendJid, callError, thread,
                currentCaller, currentCallee, null, null);
        if (currentTimeCall > 0) {
            insertMessageEndCall(thread, isBusy);
        } else {
            insertMessageDeclineCall(thread);
        }
    }

    private void insertMessageDeclineCall(ThreadMessage thread) {
        boolean isSend;
        int stateCall;
        if (currentTypeCall == CallConstant.TYPE_CALLER) {// minh goi
            if (currentTimeCall > 0) {
                stateCall = CallHistoryConstant.STATE_OUT_GOING;
            } else {
                stateCall = CallHistoryConstant.STATE_CANCELLED;
            }
            isSend = true;
        } else {
            stateCall = CallHistoryConstant.STATE_REJECTED;
            isSend = false;
        }
        insertMessageStateCall(thread, currentFriendJid, currentTimeCall, stateCall, isSend, TimeHelper
                .getCurrentTime(), getChatMode(), isStrangerConfide, stateAudioVideo);
    }

    public void handleDeclinedPermission() {
        synchronized (TAG) {
            Log.d(TAG, "handleDeclinePermission: " + currentFriendJid);
            removeListenerWhenEndCall();
            if (!isExistCall()) {
                notifyCallEnd(false);
                return;
            }
            startRingEndCall();
            myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
            ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(currentFriendJid);
            if (thread != null) {
                ReengMessage messageNotify = mApplication.getMessageBusiness().
                        insertMessageNotify(mRes.getString(R.string.call_state_declined_permission),
                                currentFriendJid, myJidNumber, thread,
                                ReengMessageConstant.READ_STATE_READ, TimeHelper.getCurrentTime());
                mApplication.getMessageBusiness().notifyNewMessage(messageNotify, thread);
                // message end call
                if (isCallOut) {
                    sendCallOutMessage(myJidNumber, currentFriendJid, ReengCallOutPacket.CallOutType.stop_call,
                            null, thread, currentCaller, currentCallee, null, null);
                } else {
                    sendCallMessage(myJidNumber, currentFriendJid, CallError.endCall, thread, currentCaller,
                            currentCallee, null, null);
                }
            }
            notifyCallEnd(false);
        }
    }

    // ban be tu choi cuoc goi
    private void handleEndCall(boolean isBusy) {
        synchronized (TAG) {
            Log.d(TAG, "handleEndCall: " + currentFriendJid);
            removeListenerWhenEndCall();
            if (!isExistCall()) {
                notifyCallEnd(isBusy);
                return;
            }
            startRingBusyCall();
            myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
            ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(currentFriendJid);
            insertMessageEndCall(thread, isBusy);
            notifyCallEnd(isBusy);
            handleLogCallState();
            handleLogCallQuality();
        }
    }

    // khong tra loi
    private void handleNoAnswer() {
        synchronized (TAG) {
            stopTimerTimeout();
            removeListenerWhenEndCall();
            if (!isExistCall()) {
                notifyCallEnd(false);
                return;
            }
            startRingEndCall();
            boolean isSend = currentTypeCall == CallConstant.TYPE_CALLER;
            ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(currentFriendJid);
            insertMessageMissCall(thread, currentFriendJid, isSend, TimeHelper.getCurrentTime(), isStrangerConfide,
                    getChatMode());
            notifyCallEnd(false);
        }
    }

    private void handleKeepCall() {
       /* if (!isExistCall()) return;
        CallRtcHelper.getInstance(mApplication).stopCall();
        CallRtcHelper.getInstance(mApplication).startCall(currentTypeCall, currentIceServers);*/
    }

    private void handleLastSeen(ThreadMessage thread, String fromNumber, MochaCallMessage callMessage) {
        synchronized (TAG) {
            myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
            sendCallMessage(myJidNumber, fromNumber, CallError.endCall, thread, callMessage.getCaller(), callMessage
                    .getCallee(), null, null);
            removeListenerWhenEndCall();
            if (!isExistCall()) {
                notifyCallEnd(false);
                return;
            }
            startRingEndCall();
            insertMessageEndCall(thread, false);
            notifyCallEnd(false);
        }
    }

    private void handleNotSupport(String content, boolean isToast) {
        synchronized (TAG) {
            stopTimerTimeout();
            removeListenerWhenEndCall();
            if (isExistCall()) {
                resetCall();
            }
            notifyOnNotSupport(content, isToast);
        }
    }

    private void notifyCall(String jidCall, int callType, boolean isAnswered, CallData callData) {
        ReengNotificationManager notificationManager = ReengNotificationManager.getInstance(mApplication);
        notificationManager.drawNotifyCall(mApplication, jidCall, callType, isAnswered, callData, isNeedShowHeadUpNotification(mApplication));
        if (!isNotShowActivityCall(mApplication) && "samsung".equals(android.os.Build.MANUFACTURER)) {
            handler.postDelayed(() -> {
                if (isExistCall()) {
                    notifyCall(currentFriendJid, CallConstant.TYPE_CALLEE, isCallConnected(), this.callData);
                }
            }, 3000);
        }
    }

    private void notifyCall(String jidCall, int callType, boolean isAnswered) {
        ReengNotificationManager notificationManager = ReengNotificationManager.getInstance(mApplication);
        notificationManager.drawNotifyCall(mApplication, jidCall, callType, isAnswered);
    }

    public void dismissHeadUpNotification() {
        Log.e("dainv", "dismiss Notification");
        ReengNotificationManager reengNotificationManager = ReengNotificationManager.getInstance(mApplication);
        if (reengNotificationManager.isShowingHeadUpCallNotification()) {
            handler.postDelayed(() -> {
                mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_CALL_HEAD_UP);
            }, 800);
            reengNotificationManager.drawNotifyCall(mApplication, currentFriendJid, currentTypeCall, false, callData, false);
        }
//        notifyCall(currentFriendJid, currentTypeCall, isCallConnected());
    }

    private void removeListenerWhenEndCall() {
        if (currentCallState >= STATE.CONNECTED) {
            String logcall = CallRtcHelper.getInstance(mApplication).getLogCall();
            String callType;
            if (isFcallviafs()) callType = CommonApi.CALLTYPE.CALL_FS;
            else if (isVideoCall) callType = CommonApi.CALLTYPE.CALL_VIDEO;
            else if (isCallOut) {
                if (currentTypeCall == CallConstant.TYPE_CALLEE)
                    callType = CommonApi.CALLTYPE.CALL_IN;
                else
                    callType = CommonApi.CALLTYPE.CALL_OUT;
            } else
                callType = CommonApi.CALLTYPE.CALL_DATA;
            CommonApi.getInstance().uploadCallReport(logcall,
                    currentCaller,
                    currentCallee,
                    currentCallSession,
                    currentTypeCall == CallConstant.TYPE_CALLER,
                    callType);
        }

        stopTimerTimeCall();
        stopTimerTimeout();
        ListenerHelper.getInstance().removeGsmCallListener(this);
        handler.post(new Runnable() {
            @Override
            public void run() {
                CallRtcHelper.getInstance(mApplication).stopCall();
                stopMediaPlayer();
                mVibrator.cancel();
            }
        });
//        ReengNotificationManager.clearMessagesNotification(mApplication, Constants.NOTIFICATION.NOTIFY_CALL);
        if (ReengNotificationManager.getInstance(mApplication).isShowingHeadUpCallNotification()) {
            mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_CALL_HEAD_UP);
        }
        mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_CALL);
    }

    private void notifyCallEnd(boolean isBusy) {
        Log.d(TAG, "----> notifyCallEnd :");
        resetCall();
        Log.d(TAG, "----> notifyOnCallEnd :");
        notifyOnCallEnd(isBusy);

        // release
        if (CallRtcHelper.getInstance(mApplication).getLocalSurfaceRenderer() != null) {
            CallRtcHelper.getInstance(mApplication).getLocalSurfaceRenderer().release();
            CallRtcHelper.getInstance(mApplication).setLocalSurfaceRenderer(null);
        }
        if (CallRtcHelper.getInstance(mApplication).getRemoteSurfaceRenderer() != null) {
            CallRtcHelper.getInstance(mApplication).getRemoteSurfaceRenderer().release();
            CallRtcHelper.getInstance(mApplication).setRemoteSurfaceRenderer(null);
        }
    }

    private void startTimerTimeout(long timeOut) {
        this.startCallTime = System.currentTimeMillis();
        this.timeoutCall = timeOut;
        stopTimerTimeout();//stop truoc
        mTimeoutTimer = new Timer();
        TimerTask mTimeoutTimerTask = new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "mTimeoutTimerTask run: ");
                if (isExistCall()) {
                    if (currentCallState >= STATE.CONNECT) {
                        stopTimerTimeout();
                    } else if ((System.currentTimeMillis() - startCallTime >= timeoutCall) ||// quá time out
                            (currentTypeCall == CallConstant.TYPE_CALLEE &&
                                    currentCallState < CallConstant.STATE.CALLEE_STARTED &&
                                    System.currentTimeMillis() - startCallTime >= CallConstant.CALL_TIME_OUT_INVITE)) {// callee cho 30s từ khi nhận được call invite
                        removeListenerWhenEndCall();
                        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread
                                (currentFriendJid);
                        boolean isSend = currentTypeCall == CallConstant.TYPE_CALLER;
                        insertMessageMissCall(thread, currentFriendJid, isSend, TimeHelper.getCurrentTime(),
                                isStrangerConfide, getChatMode());
                        notifyCallEnd(false);
                    }
                } else if (mTimeoutTimer != null) {
                    mTimeoutTimer.cancel();
                    notifyCallEnd(false);
                }
            }
        };
        mTimeoutTimer.schedule(mTimeoutTimerTask, 0, 1000);
    }

    private void stopTimerTimeout() {
        if (mTimeoutTimer != null) {
            mTimeoutTimer.cancel();
            mTimeoutTimer = null;
        }
    }

    private void sendCallMessage(String from, String to, CallError callError, ThreadMessage mThreadMessage,
                                 String caller, String callee, CallData callData,
                                 LinkedList<StringeeIceServer> stringeeIceServers) {
        sendCallMessage(from, to, callError, mThreadMessage, caller, callee, callData, stringeeIceServers, false, null);
    }

    private void sendCallMessage(String from, String to, CallError callError, ThreadMessage mThreadMessage,
                                 String caller, String callee, CallData callData,
                                 LinkedList<StringeeIceServer> stringeeIceServers, String settingXML) {
        sendCallMessage(from, to, callError, mThreadMessage, caller, callee, callData, stringeeIceServers, false, settingXML);
    }

    private void sendCallMessage(String from, String to, CallError callError, ThreadMessage mThreadMessage,
                                 String caller, String callee, CallData callData,
                                 LinkedList<StringeeIceServer> stringeeIceServers, boolean isOnlyAudio,
                                 String settingXML) {
        if (mThreadMessage == null) {
            Log.e(TAG, "what???????????????????????????");
            return;
        }
        Log.i(TAG, "sendCallMessage");
        MochaCallMessage callMessage = new MochaCallMessage(from, to, mThreadMessage);
        callMessage.setCaller(caller);
        callMessage.setCallee(callee);
        callMessage.setCallError(callError);
        callMessage.setCallData(callData);
        callMessage.setSettingXML(settingXML);
        callMessage.setIceServers(CallRtcHelper.convertIceServers(stringeeIceServers));
        if (callError == CallError.invite) {
            currentCallSession = callMessage.getPacketId();
        }
        callMessage.setCallSession(currentCallSession);
        mApplication.getXmppManager().sendCallMessage(callMessage, mThreadMessage, isStrangerConfide, isVideoCall,
                isOnlyAudio);
    }

    private void sendCallKPIMessage(String from, String to, ThreadMessage mThreadMessage,
                                    String caller, String callee, CallData callData,
                                    LinkedList<StringeeIceServer> stringeeIceServers,
                                    boolean isOnlyAudio, long timeConnect, boolean isRestartICESuccess,
                                    CallError callError) {
        if (mThreadMessage == null) {
            Log.e(TAG, "what???????????????????????????");
            return;
        }
        Log.i(TAG, "sendCallMessage");
        MochaCallMessage callMessage = new MochaCallMessage(from, to, mThreadMessage);
        callMessage.setCaller(caller);
        callMessage.setCallee(callee);
        callMessage.setCallData(callData);
        callMessage.setTimeConnect(timeConnect);
        callMessage.setIceServers(CallRtcHelper.convertIceServers(stringeeIceServers));
        callMessage.setCallSession(currentCallSession);
        if (isRestartICESuccess && callError != null && callError == CallError.restartSuccess) {
            callMessage.setRestartICESuccess(true);
            setStartedRestartICE(false);
        }
        if (callError == null) {
            callMessage.setCallError(CallError.timeConnect);
        } else {
            callMessage.setCallError(callError);
        }
        if (callError == CallError.restartICE || callError == CallError.restartSuccess)
            callMessage.setRestartReason(CallRtcHelper.getInstance(mApplication).getRestartReason());
        mApplication.getXmppManager().sendCallMessage(callMessage, mThreadMessage, isStrangerConfide, isVideoCall,
                isOnlyAudio);
    }

    private void sendCallMessageBusy(String from, String to, ThreadMessage mThreadMessage, ReengCallPacket
            receivedPacket) {
        if (mThreadMessage == null) {
            Log.e(TAG, "what ? sendCallMessageBusy");
            return;
        }
        MochaCallMessage callMessage = new MochaCallMessage(from, to, mThreadMessage);
        callMessage.setCaller(receivedPacket.getCaller());
        callMessage.setCallee(receivedPacket.getCallee());
        callMessage.setCallError(CallError.busyCall);
        callMessage.setCallData(null);
        callMessage.setCallSession(receivedPacket.getCallSession());
        mApplication.getXmppManager().sendCallMessage(callMessage, mThreadMessage,
                receivedPacket.isCallConfide(), receivedPacket.isVideoCall(), false);
    }

    private void sendIQInitCall(final ThreadMessage threadMessage, final String caller, final String callee) {
        if (mThreadInitCall == null) {
            mThreadInitCall = new Thread() {
                @Override
                public void run() {
                    startTimerTimeout(CallConstant.CALL_TIME_OUT);
//                    notifyCall(callee, CallConstant.TYPE_CALLER, false);
                    String nameSpace;
                    if (mApplication.getReengAccountBusiness().isEnableFCallViaFS() && !isCallOut && !isVideoCall)
                        nameSpace = IQCall.nameSpaceCallViaFS;
                    else
                        nameSpace = isCallOut ? IQCall.nameSpaceCallOut : IQCall.nameSpaceFree;
                    IQCall iqCall = new IQCall(nameSpace);
                    iqCall.setType(IQ.Type.GET);
                    iqCall.setPacketID(currentCallSession);
                    iqCall.setCallSession(currentCallSession);
                    iqCall.setCaller(caller);
                    iqCall.setCallee(callee);
                    iqCall.setToOpr(friendOperator);
                    iqCall.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
                    iqCall.setVirtual(mApplication.getReengAccountBusiness().getAVNONumber());
                    iqCall.setVideo(isVideoCall);
                    iqCall.setLanguage(mApplication.getReengAccountBusiness().getCurrentLanguage());
                    iqCall.setCountry(mApplication.getReengAccountBusiness().getRegionCode());
                    iqCall.setPlatform("Android");
                    iqCall.setRevision(Config.REVISION);
                    iqCall.setUsingDesktop(usingDesktop);
                    iqCall.setCalloutGlobal(mApplication.getReengAccountBusiness().isCalloutGlobalEnable() ? 1 : 0);
                    iqCall.setCallout(mApplication.getReengAccountBusiness().getCallOutState());
                    iqCall.setOsVersion(Build.VERSION.RELEASE);
                    try {
                        IQCall result = (IQCall) mApplication.getXmppManager().sendPacketThenWaitingResponse(iqCall,
                                false);
                        if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                            Log.i(TAG, "init call success: " + result.toXML());
                            if (TextUtils.isEmpty(result.getErrorCode()) || "200".equals(result.getErrorCode())) {
                                if (!TextUtils.isEmpty(result.getCallee()))
                                    currentCallee = result.getCallee();
                                if (!TextUtils.isEmpty(result.getCaller()))
                                    currentCaller = result.getCaller();
                                handleTryingCall(threadMessage, result.getIceServers(), result.getCodecPrefs(), result.getCodecVideoPrefs());
                                isEnableRestartICE = result.isEnableRestartICE();
                                iceTimeout = result.getIceTimeout() == 0
                                        ? CallConstant.SETTING_CALL.ICE_TIME_OUT : result.getIceTimeout();
                                restartICEDelay = result.getRestartICEDelay();
                                restartICEPeriod = result.getRestartICEPeriod() == 0
                                        ? CallConstant.SETTING_CALL.RESTART_ICE_PERIOD : result.getRestartICEPeriod();
                                restartICELoop = result.getRestartICELoop() == 0
                                        ? CallConstant.SETTING_CALL.RESTART_ICE_LOOP : result.getRestartICELoop();
                                zeroBwEndCall = result.getZeroBwEndCall() == 0
                                        ? CallConstant.SETTING_CALL.ZERO_BW_END_CALL : result.getZeroBwEndCall();
                                network2failedTime = result.getNetwork2failedTime();
                                timedis2recon = result.getTimedis2recon() == 0
                                        ? CallConstant.SETTING_CALL.TIME_DIS_2_RECON : result.getTimedis2recon();
                                timeRestartBw = result.getTimeRestartBw();
                                delayRestartOnFailed = result.getDelayRestartOnFailed();
                                fcallviafs = result.getFcallviafs();
                                if (fcallviafs == 1) isCallOut = true;

                                bundlePolicy = result.getBundlePolicy();
                                rtcpMuxPolicy = result.getRtcpMuxPolicy();
                                iceTransportsType = result.getIceTransportsType();

                                Log.i(TAG, "-------setting:------ isEnableRestartICE: " + isEnableRestartICE()
                                        + " iceTimeout: " + getIceTimeout() + " restartICEDelay: " + getRestartICEDelay()
                                        + " restartICEPeriod: " + getRestartICEPeriod() + " restartICELoop: " + getRestartICELoop()
                                        + " zeroBwEndCall: " + getZeroBwEndCall() + " network2failedTime: " + getNetwork2failedTime()
                                        + " isValidRestartICE: " + isValidRestartICE());
                            } else {
                                String msg;
                                if ("401".equals(result.getErrorCode())) {
                                    msg = mRes.getString(R.string.e_call_401);
                                } else if ("404".equals(result.getErrorCode())) {
                                    msg = mRes.getString(R.string.e_call_404);
                                } else {
                                    msg = mRes.getString(R.string.e601_error_but_undefined);
                                    if (BuildConfig.DEBUG) {
                                        String content = "CallOut IQ init not define *** session = " +
                                                currentCallSession + " \nerrorCode = " + result.getErrorCode();
                                        handleNotDefine(content);
                                    }
                                }
                                handleNotSupport(msg, true);
                            }
                        } else {
                            Log.i(TAG, "init call fail ");
                            handleNotSupport(mRes.getString(R.string.e601_error_but_undefined), true);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        handleNotSupport(mRes.getString(R.string.e601_error_but_undefined), true);
                    } finally {
                        mThreadInitCall = null;
                    }
                }
            };
            mThreadInitCall.setPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mThreadInitCall.start();
        } else {
            Log.e(TAG, "error ?");
        }
    }

    public boolean isFcallviafs() {
        return fcallviafs == 1;
    }

    public void processIncomingCallOld(ReengCallPacket receivedPacket, ReengMessagePacket.Type type) {
        //Log.i(TAG, "process incoming call meessage: " + receivedPacket.toXML());
        int mThreadType;
        if (type == ReengMessagePacket.Type.chat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;
        } else if (type == ReengMessagePacket.Type.groupchat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT;
        } else if (type == ReengMessagePacket.Type.roomchat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT;
        } else {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT;
        }
        mApplication.getXmppManager().sendDeliverMessage(receivedPacket, mThreadType, false, false);
    }

    public void processIncomingCallMessage(ReengCallPacket receivedPacket, ReengMessagePacket.Type type) {
        String newNumbCallee = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(receivedPacket.getCallee());
        if (newNumbCallee != null) receivedPacket.setCallee(newNumbCallee);
        String newNumbCaller = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(receivedPacket.getCaller());
        if (newNumbCaller != null) receivedPacket.setCaller(newNumbCaller);

        //Log.i(TAG, "process incoming call meessage: " + receivedPacket.toXML());
        int mThreadType;
        if (type == ReengMessagePacket.Type.chat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;
        } else if (type == ReengMessagePacket.Type.groupchat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT;
        } else if (type == ReengMessagePacket.Type.roomchat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT;
        } else {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT;
        }
        mApplication.getXmppManager().sendDeliverMessage(receivedPacket, mThreadType, false, false);
        if (type != ReengMessagePacket.Type.chat) {
            return;
        }
        if (checkDuplicatePacket(receivedPacket.getPacketID())) {
            Log.f(TAG, "processIncomingCallMessage DuplicatePacket: " + receivedPacket
                    .getPacketID());
            return;
        }
        String fromNumber = receivedPacket.getFrom().split("@")[0]; // lay so dien thoai nguoi gui
        String newFrom = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(fromNumber);
        if (newFrom != null) fromNumber = newFrom;
        if (TextUtils.isEmpty(fromNumber) ||
                !mApplication.getReengAccountBusiness().isCallEnable() ||
                mApplication.getBlockContactBusiness().isBlockNumber(fromNumber)) {// khong enable call, so bi block
            return;
        }
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage threadMessage;
        if (!TextUtils.isEmpty(receivedPacket.getExternal())) {// external giong appId
            threadMessage = mApplication.getStrangerBusiness().
                    createMochaStrangerAndThread(fromNumber, receivedPacket.getNick(),
                            receivedPacket.getStrangerAvatar(), receivedPacket.getExternal(), false);
        } else {   // tao thread thuong
            threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(fromNumber);
        }
        if (receivedPacket.isSdp()) {// call data sdp
            handleReceivedCallSdp(receivedPacket, threadMessage, fromNumber);
        } else if (receivedPacket.isRemoveCandidate()) {
            handleReceivedCallRemoveCandidate(receivedPacket);
        } else if (receivedPacket.isSetLocalSDP()) {
            handleReceivedCallCandidate(receivedPacket);
        } else if (receivedPacket.getCallError() == null) {// call data candidate
            handleReceivedCallCandidate(receivedPacket);
        } else if (isExistCall() && checkCallSession(receivedPacket)) {
            MochaCallMessage callMessage = mapPacketToCallMessage(receivedPacket, threadMessage.getId(),
                    fromNumber, myJidNumber, receivedPacket.getSubType());
            switch (callMessage.getCallError()) {
                case ringing:
                    handleRinging();
                    break;
                case connected:
                    handleAcceptCall(receivedPacket.isOnlyAudio());
                    break;
                case reconnect:
                    handleKeepCall();
                    break;
                case timeOut:
                    handleNoAnswer();
                    break;
                case busyCall:
                    handleEndCall(true);
                    break;
                case endCall:
                    handleEndCall(false);
                    break;
                case lastSeen102:
                    handleLastSeen(threadMessage, fromNumber, callMessage);
                    break;
                case notSupport:
                    handleNotSupport(mRes.getString(R.string.e_call_404), true);
                    break;
                case videoEnable:
                    handleReceivedCallEnableVideo(true);
                    break;
                case videoDisable:
                    handleReceivedCallEnableVideo(false);
                    break;
                default:
                    break;
            }
        } else {
            Log.e(TAG, "received call error when !exist call ||  # sessionId ");
        }
    }

    private boolean checkCallSession(ReengCallPacket receivedPacket) {
        if (currentCallSession == null || receivedPacket.getCallSession() == null)
            return false;// không co session
        if (currentCallSession.equals(receivedPacket.getCallSession())) {
            return true;
        }
        return false;
    }

    private boolean isCallee(ReengCallPacket receivedPacket) {
        String caller = receivedPacket.getCaller();
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        return myJidNumber != null && !myJidNumber.equals(caller);
    }

    public boolean isCaller() {
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        return myJidNumber != null && myJidNumber.equals(currentCaller);
    }

    private MochaCallMessage mapPacketToCallMessage(ReengCallPacket packet, int threadId, String
            fromNumber, String userNumber, ReengMessagePacket.SubType subType) {
        MochaCallMessage callMessage = new MochaCallMessage();
        callMessage.setPacketId(packet.getPacketID()); // set packet id
        callMessage.setReceiver(userNumber);
        callMessage.setReadState(ReengMessageConstant.READ_STATE_UNREAD);
        callMessage.setThreadId(threadId);
        callMessage.setDirection(ReengMessageConstant.Direction.received);
        callMessage.setTime(packet.getTimeSend());

        callMessage.setEnableRestartICE(packet.isEnableRestartICE());
        callMessage.setIceTimeout(packet.getIceTimeout());
        callMessage.setRestartICEDelay(packet.getRestartICEDelay());
        callMessage.setRestartICEPeriod(packet.getRestartICEPeriod());
        callMessage.setRestartICELoop(packet.getRestartICELoop());
        callMessage.setZeroBwEndCall(packet.getZeroBwEndCall());
        callMessage.setNetwork2failedTime(packet.getNetwork2failedTime());
        callMessage.setTimedis2recon(packet.getTimedis2recon());
        callMessage.setTimeRestartBw(packet.getTimeRestartBw());
        callMessage.setDelayRestartOnFailed(packet.getDelayRestartOnFailed());

        callMessage.setBundlePolicy(packet.getBundlePolicy());
        callMessage.setRtcpMuxPolicy(packet.getRtcpMuxPolicy());
        callMessage.setIceTransportsType(packet.getIceTransportsType());

        ReengMessagePacket.Type type = packet.getType();
        if (type == ReengMessagePacket.Type.chat) {
            callMessage.setSender(fromNumber);
        } else if (type == ReengMessagePacket.Type.groupchat) {
            callMessage.setSender(packet.getSender());
        } else if (type == ReengMessagePacket.Type.offical) {
            callMessage.setSender(fromNumber);
        } else if (type == ReengMessagePacket.Type.roomchat) {
            callMessage.setSender(packet.getSender());
            callMessage.setSenderName(packet.getSenderName());
            callMessage.setSticky(packet.getStickyState());
            callMessage.setSenderAvatar(packet.getAvatarUrl());
        }
        callMessage.setMessageType(ReengMessageConstant.MessageType.call);
        switch (subType) {
            case call_rtc_2:
                callMessage.setStatus(ReengMessageConstant.STATUS_RECEIVED);
                callMessage.setCaller(packet.getCaller());
                callMessage.setCallee(packet.getCallee());
                callMessage.setCallError(packet.getCallError());
                callMessage.setCallData(packet.getCallData());
                callMessage.setIceServers(packet.getIceServers());
                callMessage.setCallSession(packet.getCallSession());
                break;

            default:
                break;
        }
        return callMessage;
    }

    /**
     * insert message state call
     *
     * @param thread
     * @param friendNumber
     * @param isSend
     * @param messageTime
     */
    private void insertMessageMissCall(ThreadMessage thread, String friendNumber,
                                       boolean isSend, long messageTime,
                                       boolean isConfide, int chatMode) {
        int stateCall;
//        if (isSend)
//            stateCall = CallHistoryConstant.STATE_CANCELLED;
//        else
        stateCall = CallHistoryConstant.STATE_MISS;// set always miss call
        insertMessageStateCall(thread, friendNumber, 0, stateCall, isSend, messageTime, chatMode, isConfide, stateAudioVideo);
    }

    private void insertMessageEndCall(ThreadMessage thread, boolean isBusy) {
        boolean isSend;
        int stateCall;
        if (currentTypeCall == CallConstant.TYPE_CALLER) {// minh goi
            if (currentCallState < STATE.CONNECTED) {
                if (isBusy) {
                    stateCall = CallHistoryConstant.STATE_BUSY;
                } else {
                    stateCall = CallHistoryConstant.STATE_REJECTED;
                }
            } else {
                stateCall = CallHistoryConstant.STATE_OUT_GOING;
            }
            isSend = true;
        } else {
            if (currentCallState < CallConstant.STATE.CONNECTED) {
                stateCall = CallHistoryConstant.STATE_MISS;
            } else {
                stateCall = CallHistoryConstant.STATE_IN_COMING;
            }
            isSend = false;
        }
        insertMessageStateCall(thread, currentFriendJid, currentTimeCall, stateCall, isSend, TimeHelper
                .getCurrentTime(), getChatMode(), isStrangerConfide, stateAudioVideo);
    }

    private void insertMessageStateCall(ThreadMessage thread, String friendNumber, int duration,
                                        int stateCall, boolean isSend, long messageTime, int chatMode, boolean
                                                isConfide, int stateAudioVideoOfCall) {
        Log.d(TAG, "insertMessageStateCall: " + stateAudioVideo);
        ReengMessageConstant.Direction direction;
        String sender;
        String received;
        int callState;
        if (isSend) {
            direction = ReengMessageConstant.Direction.send;
            sender = mApplication.getReengAccountBusiness().getJidNumber();
            received = friendNumber;
        } else {
            direction = ReengMessageConstant.Direction.received;
            sender = friendNumber;
            received = mApplication.getReengAccountBusiness().getJidNumber();
        }
        String content;
        if (stateCall == CallHistoryConstant.STATE_BUSY) {
            content = mRes.getString(R.string.call_state_busy);
        } else if (stateCall == CallHistoryConstant.STATE_CANCELLED) {
            content = mRes.getString(R.string.call_state_cancelled_call);
        } else if (stateCall == CallHistoryConstant.STATE_MISS) {
            content = mRes.getString(R.string.call_state_miss_call);
        } else {
            content = Utilities.secondsToTimer(duration);
        }
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        int readState;
        if (stateCall == CallHistoryConstant.STATE_MISS && direction == ReengMessageConstant.Direction.received) {
            readState = ReengMessageConstant.READ_STATE_UNREAD;
        } else {
            readState = ReengMessageConstant.READ_STATE_SENT_SEEN;
        }
        // !stranger, video call
        if (!isConfide && chatMode == ReengMessageConstant.MODE_IP_IP) {
            if (stateAudioVideoOfCall == CallConstant
                    .STATE_AUDIO_VIDEO.ACCEPTED) {
                chatMode = ReengMessageConstant.MODE_VIDEO_CALL;
                callState = CallHistoryConstant.CALL_VIDEO;
            } else {
                callState = CallHistoryConstant.CALL_FREE;
            }
        } else {
            callState = chatMode == ReengMessageConstant.MODE_IP_IP ? CallHistoryConstant.CALL_FREE :
                    CallHistoryConstant.CALL_OUT;
        }
        ReengMessage notifyCallMessage = messageBusiness.insertMessageCall(thread, content, received,
                sender, direction, stateCall, duration, messageTime, readState, chatMode, isConfide);
        messageBusiness.notifyNewMessage(notifyCallMessage, thread);
        // insert history
        if (!isStrangerConfide) {//TODO off tab call
//            if (mApplication.getConfigBusiness().isEnableTabCall())
            CallHistoryHelper.getInstance().insertNewCallHistoryDetail(friendNumber,
                    mApplication.getReengAccountBusiness().getJidNumber(), stateCall, callState,
                    isSend, messageTime, duration);
        }
        if (readState == ReengMessageConstant.READ_STATE_UNREAD) {
            thread.setNumOfUnreadMessage(thread.getNumOfUnreadMessage() + 1);
            ReengNotificationManager.getInstance(mApplication).drawMessagesNotification(mApplication,
                    thread, ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT, notifyCallMessage);
            mApplication.updateCountNotificationIcon();
            messageBusiness.updateThreadMessage(thread);
        }
    }

    private void startTimerTimeCall() {
        Log.d(TAG, "startTimerTimeCall");
        stopTimerTimeCall();
        currentTimeCall = 0;
        mTimeCallTimer = new Timer();
        TimerTask mTimeCallTimerTask = new TimerTask() {
            @Override
            public void run() {
                currentTimeCall++;
                Log.d(TAG, "TimerTask --- currentTimeCall: " + currentTimeCall);
                notifyOnTickTimeConnect(currentTimeCall);
                if (currentTimeCall % 2 == 0) {
                    CallRtcHelper.getInstance(mApplication).getReportQuality();
                }
            }
        };
        mTimeCallTimer.schedule(mTimeCallTimerTask, 1000, 1000);
        if (currentTypeCall == CallConstant.TYPE_CALLER) {
            mVibrator.vibrate(CallConstant.TIME_VIBRATOR);// connected
        }
    }

    private void stopTimerTimeCall() {
        if (mTimeCallTimer != null) {
            mTimeCallTimer.cancel();
        }
    }

    public void toggleMute() {
        isMute = !isMute;
        audioManager.setMicrophoneMute(isMute);
        CallRtcHelper.getInstance(mApplication).setMute();
    }

    public void toggleSpeaker() {
        isSpeaker = !isSpeaker;
        audioManager.setSpeakerphoneOn(isSpeaker);
    }

    private void setSpeaker(boolean isOn) {
        if (isOn && CallBluetoothHelper.getInstance(mApplication).isCurrentDisableSpeaker()) {
            // bluetooth thì ko change
            return;
        }
        if (isSpeaker != isOn) {
            isSpeaker = isOn;
            audioManager.setSpeakerphoneOn(isSpeaker);
        }
    }

    private void startRingIncomingCall() {
        stopMediaPlayer();
        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
            Log.f(TAG, "getRingerMode() == AudioManager.RINGER_MODE_NORMAL");
            handler.post(new Runnable() {
                @Override
                public void run() {
                    audioManager.setSpeakerphoneOn(true);
                    audioManager.setMode(AudioManager.MODE_RINGTONE);
                    try {
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                        mRingtone = RingtoneManager.getRingtone(mApplication, uri);
                        mRingtone.play();
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            });
            mVibrator.vibrate(CallConstant.getTimeVibratorCalling(), 0);
        } else if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
            Log.f(TAG, "getRingerMode() == AudioManager.RINGER_MODE_VIBRATE");
            mVibrator.vibrate(CallConstant.getTimeVibratorCalling(), 0);
        }
    }

    private void startRingBackCall() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                stopMediaPlayer();
                startMediaCall(R.raw.call_ringing, AudioManager.STREAM_VOICE_CALL, true, false);
            }
        });
    }

    private void startRingBusyCall() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "startRingBusyCall: ");
                stopMediaPlayer();
                startMediaCall(R.raw.call_busy, AudioManager.STREAM_MUSIC, false, true);
            }
        });
    }

    public void startRingReconnecting() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "startRingBusyCall: ");
                stopMediaPlayer();
                startMediaCall(R.raw.call_hold, AudioManager.STREAM_VOICE_CALL, true, false);
            }
        });
    }

    private void startRingEndCall() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                stopMediaPlayer();
                startMediaCall(R.raw.call_ended, AudioManager.STREAM_MUSIC, false, false);
            }
        });
    }

    private boolean isHeadset() {
        return audioManager.isWiredHeadsetOn() || audioManager.isBluetoothA2dpOn();
    }

    private void startMediaCall(int audioId, int streamType, boolean isLooping, boolean isSpeaker) {
        if (isExistGsm) {// dang co cuoc gọi gsm # thì khong bat am khi ket thuc cuoc goi ... tranh bat loa ngoai
            resetAudioManager();
        } else {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setAudioStreamType(streamType);
            mMediaPlayer.setVolume(1.0f, 1.0f);
            if (isSpeaker && !isHeadset()) {
                audioManager.setSpeakerphoneOn(isSpeaker);
            }
            if (isLooping) {
                mMediaPlayer.setLooping(true);
            } else {
                mMediaPlayer.setLooping(false);
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        stopMediaPlayer();
                        //resetAudioManager();
                    }
                });
            }
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    try {
                        mediaPlayer.start();
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            });
            mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Log.e(TAG, "onError" + what + " - " + extra);
                    stopMediaPlayer();
                    return false;
                }
            });
            AssetFileDescriptor afd = mRes.openRawResourceFd(audioId);
            try {
                mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                //mMediaPlayer.prepare();
                mMediaPlayer.prepareAsync();
                //mMediaPlayer.start();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                try {
                    afd.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
        }
    }

    private void stopMediaPlayer() {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
            if (mRingtone != null) {
                mRingtone.stop();
                mRingtone = null;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public boolean stopRingingWhenPressKeyVolumes() {
        if (currentTypeCall == CallConstant.TYPE_CALLEE) {
            if (currentCallState == STATE.RINGING || currentCallState == STATE.CALLEE_STARTED) {
                if (mVibrator != null) {
                    mVibrator.cancel();
                }
                stopMediaPlayer();
                return true;
            }
        }
        return false;
    }

    private void requestAudioFocus() {
        audioManager.requestAudioFocus(afChangeListener,
                // Use the music stream.
                AudioManager.STREAM_VOICE_CALL,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);
    }

    private void releaseAudioFocus() {
        audioManager.abandonAudioFocus(afChangeListener);
    }

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            Log.d(TAG, "OnAudioFocusChangeListener: " + focusChange);
            /*if (isExistCall()) {
                if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {// Pause playback
                    CallRtcHelper.getInstance(mApplication).stopCall();
                } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {   // Resume playback
                    if (currentCallState >= STATE.CONNECT) {
                        *//*myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
                        ThreadMessage thread = mApplication.getMessageBusiness().findExistingSoloThread
                        (currentFriendJid);
                        sendCallMessage(myJidNumber, currentFriendJid, CallError.reconnect, thread, currentCaller,
                        currentCallee, null);*//*
                        CallRtcHelper.getInstance(mApplication).startCall(currentTypeCall, currentIceServers);
                    } else if (currentTypeCall == CallConstant.TYPE_CALLER) {
                        CallRtcHelper.getInstance(mApplication).startCall(currentTypeCall, currentIceServers);
                    }
                }
            }*/
            /* else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                        audioManager.unregisterMediaButtonEventReceiver(RemoteControlReceiver);
                        audioManager.abandonAudioFocus(afChangeListener);
                        // Stop playback
            }*/
        }
    };

    public void showQuickMissCallActivity(String friendNumber) {
        ApplicationStateManager mApplicationStateManager = mApplication.getAppStateManager();
        SettingBusiness mSettingBusiness = SettingBusiness.getInstance(mApplication);
        String currentActivity = mApplicationStateManager.getCurrentActivity();
        boolean isScreenOn = mApplicationStateManager.isScreenOn();
        boolean isLocked = mApplicationStateManager.isScreenLocker();
        if (!mSettingBusiness.getPrefEnableUnlock() && isLocked) {
            return;
        }
        Log.i(TAG, "Check:----isActivityForResult(): " + mApplicationStateManager.isActivityForResult() + " , " +
                "isTakePhotoAndCrop: " + mApplicationStateManager.isTakePhotoAndCrop());
        if (mApplicationStateManager.isActivityForResult() || mApplicationStateManager.isTakePhotoAndCrop()) {
            return;// dang chụp anh, luu contact.
        }
        boolean isAppForeground = mApplicationStateManager.isAppForeground();
        Log.i(TAG, "Check:----isAppForeground: " + isAppForeground + " ,isScreenOn: " + isScreenOn + " ,isLocked: " +
                isLocked + " ,currentActivity: " + currentActivity);
        if (!isAppForeground || !isScreenOn
                || (isLocked && !currentActivity.equals(QuickMissCallActivity.class.getCanonicalName())
                && !currentActivity.equals(QuickReplyActivity.class.getCanonicalName()))) {// activity khac
            // quickreply thi moi hien thi
            Log.i(TAG, "Show QuickMissCall: ");
            mApplicationStateManager.setShowQuickReply(true);
            try {
                Intent mIntent = new Intent(mApplication, QuickMissCallActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent
                        .FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_FROM_BACKGROUND);
                mIntent.putExtra(QuickMissCallActivity.MISS_CALL_NUMBER, friendNumber);
                mIntent.putExtra(QuickMissCallActivity.MISS_CALL_TIME, TimeHelper.getCurrentTime());
                if (isLocked || !isScreenOn) {
                    mIntent.putExtra(QuickMissCallActivity.BG_TRANSPARENT, false);
                } else {
                    mIntent.putExtra(QuickMissCallActivity.BG_TRANSPARENT, true);
                }
                if (!isScreenOn) {
                    mIntent.putExtra(QuickMissCallActivity.SCREEN_OFF, true);
                } else {
                    mIntent.putExtra(QuickMissCallActivity.SCREEN_OFF, false);
                }
                mApplication.wakeLockAcquire();
                mApplication.startActivity(mIntent);
            } catch (Exception ex) {
                Log.e(TAG, "Exception", ex);
            }
        }
    }

    public void setExistGsm(boolean existGsm) {
        isExistGsm = existGsm;
    }

    ////////////////////////////////////////////////////////////////////
    /////////////////////////Call OUT //////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    private boolean candidateHostGenerated = false;
    private boolean candidateSrflxGenerated = false;
    private boolean candidateGeneratedDone = false;
    private ArrayList<String> candidateCallOutData = new ArrayList<>();
    private String sdpCallOutData;

    private int getChatMode() {
        if (isCallOut && !isFcallviafs()) {
            return ReengMessageConstant.MODE_GSM;
        }
        return ReengMessageConstant.MODE_IP_IP;
    }

    public void processIncomingCallOutMessage(ReengCallOutPacket
                                                      receivedPacket, ReengMessagePacket.Type type) {
        Log.i(TAG, "process incoming call out meessage: " + receivedPacket.toXML());
        int mThreadType;
        if (type == ReengMessagePacket.Type.chat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;
        } else if (type == ReengMessagePacket.Type.groupchat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT;
        } else if (type == ReengMessagePacket.Type.roomchat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT;
        } else {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT;
        }
        mApplication.getXmppManager().sendDeliverMessage(receivedPacket, mThreadType, false, false);
        if (type != ReengMessagePacket.Type.chat) {
            return;
        }
        String fromNumber = receivedPacket.getFrom().split("@")[0]; // lay so dien thoai nguoi gui
        if (TextUtils.isEmpty(fromNumber) ||
                !mApplication.getReengAccountBusiness().isCallEnable() ||
                mApplication.getBlockContactBusiness().isBlockNumber(fromNumber)) {// khong enable call, so bi block
            return;
        }
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage threadMessage;
        if (!TextUtils.isEmpty(receivedPacket.getExternal())) {// external giong appId
            threadMessage = mApplication.getStrangerBusiness().
                    createStrangerAndThreadAfterReceivedMessage(fromNumber,
                            receivedPacket.getNick(), receivedPacket.getExternal());
        } else {   // tao thread thuong
            threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(fromNumber);
        }
        if (isExistCall() && checkCallOutSession(receivedPacket)) {
            if (receivedPacket.getCallOutType() == ReengCallOutPacket.CallOutType.data_sdp ||
                    receivedPacket.getCallOutType() == ReengCallOutPacket.CallOutType.data_canditate) {
                handleReceivedCallOutData(receivedPacket, threadMessage, myJidNumber);
            } else if (receivedPacket.getCallOutType() == ReengCallOutPacket.CallOutType.receive_status) {
                handleReceivedCallOutStatus(receivedPacket, threadMessage, myJidNumber);
            } else if (receivedPacket.getCallOutType() == ReengCallOutPacket.CallOutType.stop_call) {
                /*MochaCallOutMessage callOutMessage = mapPacketToCallOutMessage(receivedPacket, threadMessage.getId(),
                        fromNumber, myJidNumber, receivedPacket.getSubType());*/
                handleEndCall(false);
            } else {

            }
        } else {
            Log.e(TAG, "received call error when !exist call ||  # sessionId ");
        }
    }

    private boolean checkCallOutSession(ReengCallOutPacket receivedPacket) {
        if (currentCallSession == null || receivedPacket.getCallSession() == null)
            return false;// không co session
        return currentCallSession.equals(receivedPacket.getCallSession());
    }

    private void handleReceivedCallOutStatus(ReengCallOutPacket receivedPacket, ThreadMessage threadMessage, String
            fromNumber) {
        Log.d(TAG, "handleReceivedCallOutStatus: " + receivedPacket.getCallStatus().getValue());
        switch (receivedPacket.getCallStatus()) {
            case session_fail:
                handleNotSupport(mRes.getString(R.string.e_call__1), false);
                break;
            case session_ok:
                CallRtcHelper.getInstance(mApplication).startCall(currentTypeCall, currentIceServers, isCallOut,
                        codecAudio, codecVideo);
                break;
            case trying:
                //CallRtcHelper.getInstance(mApplication).startCall(currentTypeCall, currentIceServers, isCallOut);
                break;
            case ringing:
            case ringing_183:
                handleRinging();
                break;
            case connected:
                handleAcceptCall(false);
                break;
            case timeOut:
                handleNoAnswer();
                break;
            case busyCall:
                handleEndCall(true);
                break;
            case endCall:
            case endCall_700:
                handleEndCall(false);
                break;
            default:
                if (BuildConfig.DEBUG) {
                    String content = "CallOut not define *** session = " + receivedPacket.getCallSession() + " " +
                            "\nerrorCode = " + receivedPacket.getCallStatusStr();
                    handleNotDefine(content);
                }
                break;
        }
    }

    private void handleReceivedCallOutData(ReengCallOutPacket receivedPacket, ThreadMessage threadMessage, String
            fromNumber) {
        synchronized (TAG) {
            String dataType;
            if (receivedPacket.getCallOutType() == ReengCallOutPacket.CallOutType.data_sdp) {
                dataType = "sdp";
                Log.i(TAG, "handleReceivedCallOutData sdp");
                deltaTimeConnect = System.currentTimeMillis();
            } else {
                dataType = "candidate";
            }
            CallData callData = new CallData(dataType, receivedPacket.getCallOutData());
            CallRtcHelper.getInstance(mApplication).addCallData(callData, isCallOut);
        }
    }

    public void processSendCallOutData(String input, boolean isSdp) {
        Log.i(TAG, "processSendCallOutData");
        if (!isExistCall()) return;
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (isSdp) {
            sdpCallOutData = input;
        } else {
            String[] candidaTe11 = input.split("\\s+");
            String type = candidaTe11[7];
            if ("host".equals(type)) {
                candidateHostGenerated = true;
            } else if ("srflx".equals(type)) {
                candidateSrflxGenerated = true;
                candidateCallOutData.add(input);
            }
        }
        // if (candidateHostGenerated && candidateSrflxGenerated && sdpCallOutData != null && !candidateGeneratedDone) {
        if (candidateSrflxGenerated && sdpCallOutData != null && !candidateGeneratedDone) {
            candidateGeneratedDone = true;
            Log.i(TAG, "processSendCallOutData done wait all call data");
            StringBuilder data = new StringBuilder();
            data.append(sdpCallOutData);
            for (String item : candidateCallOutData) {
                data.append("a=").append(item).append("\r\n");
            }
            CallData newCallData = new CallData();
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("sdp", data.toString());
                jsonObject.put("type", 0);
                newCallData = new CallData("sdp", jsonObject.toString());
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            Log.i(TAG, "processSendCallOutData append callData: " + newCallData.getData());
            synchronized (sendCallDataQueue) {
                //if (mApplication.getXmppManager().isAuthenticated() && sendCallDataQueue.isEmpty()) {
                if (mApplication.getXmppManager().isAuthenticated()) {//TODO ko cần đảm bảo thứ tự candidate nên send
                    // luôn
                    Log.i(TAG, "processSendCallOutData --- sendMessageCallData");
                    sendMessageCallOutData(newCallData);
                } else {
                    sendCallDataQueue.add(newCallData);
                }
            }
        }
    }

    private void sendMessageCallOutData(CallData callData) {
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(currentFriendJid);
        ReengCallOutPacket.CallOutType callOutType;
        if ("sdp".equals(callData.getType())) {
            callOutType = ReengCallOutPacket.CallOutType.data_sdp;
        } else {
            callOutType = ReengCallOutPacket.CallOutType.data_canditate;
        }
        sendCallOutMessage(myJidNumber, currentFriendJid, callOutType, null, thread,
                currentCaller, currentCallee, callData.getData(), currentIceServers);
    }

    public void sendMessageCallOutStart(ThreadMessage thread, String jidFriend) {
        sendCallOutMessage(myJidNumber, jidFriend, ReengCallOutPacket.CallOutType.invite,
                null, thread, currentCaller, currentCallee, null, null);
        notifyOnConnectStateChange(STATE.TRYING);
    }

    private void declineCallOut(ThreadMessage thread, boolean isBusy) {
        sendCallOutMessage(myJidNumber, currentFriendJid, ReengCallOutPacket.CallOutType.stop_call, null,
                thread, currentCaller, currentCallee, null, null);
        int stateCall;
        if (currentCallState < STATE.CONNECTED) {
            stateCall = CallHistoryConstant.STATE_CANCELLED;
        } else {
            stateCall = CallHistoryConstant.STATE_OUT_GOING;
        }
        insertMessageStateCall(thread, currentFriendJid, currentTimeCall, stateCall, true, TimeHelper
                .getCurrentTime(), getChatMode(), isStrangerConfide, stateAudioVideo);
    }

    private void sendCallOutMessage(String from, String to, ReengCallOutPacket.CallOutType callOutType,
                                    ReengCallOutPacket.CallStatus callStatus, ThreadMessage mThreadMessage,
                                    String caller, String callee, String callData,
                                    LinkedList<StringeeIceServer> stringeeIceServers) {
        if (mThreadMessage == null) {
            Log.e(TAG, "what??????????????????????????? call out");
            return;
        }
        Log.i(TAG, "sendCallOutMessage");
        boolean isCallIn = false;
        if (currentTypeCall == CallConstant.TYPE_CALLEE) {
            isCallIn = true;
        }
        MochaCallOutMessage callMessage = new MochaCallOutMessage(from, to, mThreadMessage, isCallIn);
        callMessage.setCaller(caller);
        callMessage.setCallee(callee);
        callMessage.setCallOutType(callOutType);
        callMessage.setCallStatus(callStatus);
        callMessage.setCallData(callData);
        callMessage.setIceServers(CallRtcHelper.convertIceServers(stringeeIceServers));
        callMessage.setCallSession(currentCallSession);
        callMessage.setEnableCallViaFS(isFcallviafs());
        mApplication.getXmppManager().sendCallOutMessage(callMessage, mThreadMessage);
    }

    private void sendCallOutKPIMessage(String from, String to, ReengCallOutPacket.CallOutType callOutType,
                                       ThreadMessage mThreadMessage, String caller, String callee, String callData,
                                       LinkedList<StringeeIceServer> stringeeIceServers, long timeConnect,
                                       boolean isCallIn, boolean isRestartICESuccess,
                                       ReengCallOutPacket.CallStatus callStatus) {
        if (mThreadMessage == null) {
            Log.e(TAG, "what??????????????????????????? call out");
            return;
        }
        Log.i(TAG, "sendCallOutMessage");
        MochaCallOutMessage callMessage = new MochaCallOutMessage(from, to, mThreadMessage, isCallIn);
        callMessage.setCaller(caller);
        callMessage.setCallee(callee);
        callMessage.setCallOutType(callOutType);
        callMessage.setTimeConnect(timeConnect);
        callMessage.setCallData(callData);
        callMessage.setIceServers(CallRtcHelper.convertIceServers(stringeeIceServers));
        callMessage.setCallSession(currentCallSession);
        callMessage.setEnableCallViaFS(isFcallviafs());
        if (callStatus == null) {
            callMessage.setCallStatus(ReengCallOutPacket.CallStatus.timeConnect);
        } else {
            callMessage.setCallStatus(callStatus);
        }
        if (isRestartICESuccess && callStatus == ReengCallOutPacket.CallStatus.restartSuccess) {
            callMessage.setRestartICESuccess(true);
            setStartedRestartICE(false);
        }
        if (callStatus == ReengCallOutPacket.CallStatus.restartICE || callStatus == ReengCallOutPacket.CallStatus.restartSuccess) {
            callMessage.setRestartReason(CallRtcHelper.getInstance(mApplication).getRestartReason());
        }
        mApplication.getXmppManager().sendCallOutMessage(callMessage, mThreadMessage);
    }

    private void handleNotDefine(String content) {
        Log.d(TAG, "handleNotDefine: " + content);
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(currentFriendJid);
        SoloSendTextMessage message = new SoloSendTextMessage(thread, myJidNumber, currentFriendJid, content);
        message.setStatus(ReengMessageConstant.STATUS_DELIVERED);
        mApplication.getMessageBusiness().insertNewMessageToDB(thread, message);
        mApplication.getMessageBusiness().notifyNewMessage(message, thread);
    }

    // ham check so chi co the ho tro call out // (chưa dùng mocha)
    private boolean checkCallOut(String numberFriend) {
        PhoneNumber phone = mApplication.getContactBusiness().getPhoneNumberFromNumber(numberFriend);
        if (phone != null && phone.isReeng()) {
            return false;
        } else {
            NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(numberFriend);
            if (nonContact != null && nonContact.isReeng()) {
                return false;
            }
        }
        return true;
    }

    public void countCandidate() {
        candidateCount++;
    }

    private void handleLogCallState() {
        Log.d(TAG, "handleLogCallState: " + currentCallState + " currentCallSession: " + currentCallSession);
        if (isCaller())
            if ((currentCallState < STATE.CONNECTED && currentCallState >= STATE.CONNECT)
                    || (isCallOut && sumState >= STATE.CONNECTED && sumState < (STATE.CALLEE_STARTED + STATE.CONNECTED))/*currentCallState < STATE.CONNECTED)*/) {
                if (BuildConfig.DEBUG) {
                    EventBus.getDefault().post(new MochaCallActivity.EventConnection(StringeeCallListener.StringeeConnectionState.LOG_CONNECTING));
                }
                ReportHelper.requestCallConnecting(mApplication, currentCaller, currentCallee,
                        currentCallSession, candidateCount, (System.currentTimeMillis() - beginTimeConnect));
            }
    }

    private int getModeReport() {
        if (isCallOut) {
            return 2;
        } else if (isModeVideoCall()) {
            return 1;
        }
        return 0;
    }

    private String getModeReportStr() {
        if (isCallOut) {
            if (currentTypeCall == CallConstant.TYPE_CALLEE) {
                return "call_in";
            } else {
                return "callout";
            }
        } else if (isModeVideoCall()) {
            return "video_call";
        }
        return "voice_call";
    }

    private void handleLogCallQuality() {
        int qualitiesSize = callQualities.size();
        if (qualitiesSize > 0) {
            String callType = getModeReportStr();
            StringBuilder sb = new StringBuilder();
            if (qualitiesSize > 1) {
                for (int i = 0; i < qualitiesSize - 1; i++) {
                    sb.append(callQualities.get(i).toReport()).append(",");
                }
            }
            sb.append(callQualities.get(qualitiesSize - 1).toReport());
            ReportHelper.reportCallQuality(mApplication, currentCaller, currentCallee,
                    currentCallSession, callType, sb.toString(),
                    currentTypeCall == CallConstant.TYPE_CALLER);
        }
        callQualities = new ArrayList<>();
    }

    // audio to video
    public void handleEnableVideo(boolean isEnable) {
        isEnableMyVideoCall = isEnable;
        // send messsage
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(currentFriendJid);
        CallError callError;
        if (isEnable) {// minh enable, cap nhat trang thai accept
            if (!isHeadset()) {
                setSpeaker(true);
            }
            callError = CallError.videoEnable;
            if (stateAudioVideo == CallConstant.STATE_AUDIO_VIDEO.NON) {
                stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.OUT_GOING;
            } else if (stateAudioVideo == CallConstant.STATE_AUDIO_VIDEO.IN_COMMING) {
                stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.ACCEPTED;
            }
        } else {// minh disable, cap nhat trang thai non
            callError = CallError.videoDisable;
            if (stateAudioVideo == CallConstant.STATE_AUDIO_VIDEO.IN_COMMING ||
                    stateAudioVideo == CallConstant.STATE_AUDIO_VIDEO.OUT_GOING) {
                stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.NON;
                isEnableFriendVideoCall = false;
            }
        }
        sendCallMessage(myJidNumber, currentFriendJid, callError, thread, currentCaller, currentCallee, null, null);
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onVideoStateChange();
            }
        }
        // enable(disable) stream
        CallRtcHelper.getInstance(mApplication).enableVideo(isEnableMyVideoCall);
    }

    private void handleReceivedCallEnableVideo(boolean isEnable) {
        isEnableFriendVideoCall = isEnable;
        if (isEnable) {// ban be dong y, cap nha trang thai accepted
            if (stateAudioVideo == CallConstant.STATE_AUDIO_VIDEO.NON) {
                stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.IN_COMMING;
            } else if (stateAudioVideo == CallConstant.STATE_AUDIO_VIDEO.OUT_GOING) {
                stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.ACCEPTED;
            }
        } else {// ban be tu choi, cap nhat trang thai non
            if (stateAudioVideo == CallConstant.STATE_AUDIO_VIDEO.OUT_GOING ||
                    stateAudioVideo == CallConstant.STATE_AUDIO_VIDEO.IN_COMMING) {
                stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.NON;
                isEnableMyVideoCall = false;
                CallRtcHelper.getInstance(mApplication).enableVideo(isEnableMyVideoCall);
            }
        }
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onVideoStateChange();
            }
        }
    }

    private void resetVideoModeWhenAnswerOnlyAudio() {
        isVideoCall = false;
        isEnableMyVideoCall = false;
        isEnableFriendVideoCall = false;
        stateAudioVideo = CallConstant.STATE_AUDIO_VIDEO.NON;
        setSpeaker(false);
        CallRtcHelper.getInstance(mApplication).enableVideo(isEnableMyVideoCall);
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onVideoStateChange();
            }
        }
    }

    public void headsetPlug(boolean isConnected) {
        Log.d(TAG, "headsetPlug: " + currentCallState + " isConnected: " + isConnected);
        if (isExistCall()) {
            boolean isChangeSpeaker = false;
            if (isConnected && isSpeaker) {
                if (currentCallState >= CallConstant.STATE.CONNECT || isModeVideoCall()) {
                    setSpeaker(false);
                    isChangeSpeaker = true;
                }
            } else if (!isConnected && isModeVideoCall()) {
                setSpeaker(true);
                isChangeSpeaker = true;
            }
            if (isChangeSpeaker) {
                updateSpeakerUI(false);
            }
            CallBluetoothHelper.getInstance(mApplication).updateStateWhenWiredHeadsetChanged(isConnected);
        } else {
            Log.d(TAG, "headsetPlug !isExistCall(): " + isConnected);
        }
    }

    public void onBluetoothChange(boolean isConnected) {
        if (isConnected) {
            setSpeaker(false);
        }
        updateSpeakerUI(isConnected);
    }

    private void updateSpeakerUI(boolean isDisable) {// disable button speaker when connect bluetooth
        if (!callStateListeners.isEmpty()) {
            for (CallStateInterface listener : callStateListeners) {
                listener.onSpeakerChanged(isDisable);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    /////////////////////////Call IN //////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    //private String sdpCallInData;

    public void processIncomingCallInMessage(ReengCallOutPacket receivedPacket, ReengMessagePacket.Type type) {
        Log.i(TAG, "process incoming call in meessage: " + receivedPacket.toXML());
        int mThreadType;
        if (type == ReengMessagePacket.Type.chat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;
        } else if (type == ReengMessagePacket.Type.groupchat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT;
        } else if (type == ReengMessagePacket.Type.roomchat) {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT;
        } else {
            mThreadType = ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT;
        }
        mApplication.getXmppManager().sendDeliverMessage(receivedPacket, mThreadType, false, false);
        if (type != ReengMessagePacket.Type.chat) {
            return;
        }
        String fromNumber = receivedPacket.getFrom().split("@")[0]; // lay so dien thoai nguoi gui
        if (TextUtils.isEmpty(fromNumber) ||
                !mApplication.getReengAccountBusiness().isCallEnable() ||
                mApplication.getBlockContactBusiness().isBlockNumber(fromNumber)) {// khong enable call, so bi block
            return;
        }
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage threadMessage;
        if (!TextUtils.isEmpty(receivedPacket.getExternal())) {// external giong appId
            threadMessage = mApplication.getStrangerBusiness().
                    createStrangerAndThreadAfterReceivedMessage(fromNumber,
                            receivedPacket.getNick(), receivedPacket.getExternal());
        } else {   // tao thread thuong
            threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(fromNumber);
        }
        if (receivedPacket.getCallOutType() == ReengCallOutPacket.CallOutType.data_sdp) {
            handleIncomingCallIn(receivedPacket, threadMessage, fromNumber);
        } else if (isExistCall() && checkCallOutSession(receivedPacket)) {
            handleReceivedCallOutStatus(receivedPacket, threadMessage, fromNumber);
        } else {
            Log.e(TAG, "received call in error when !exist call ||  # sessionId " + receivedPacket.toXML());
        }
    }

    private void handleIncomingCallIn(ReengCallOutPacket receivedPacket, ThreadMessage threadMessage, String
            fromNumber) {
        synchronized (TAG) {
            if ("end".equals(receivedPacket.getAttrStatus())) {
                insertMessageStateCall(threadMessage, fromNumber, 0, CallHistoryConstant.STATE_MISS, false,
                        receivedPacket.getTimeSend(), ReengMessageConstant.MODE_GSM, false, stateAudioVideo);
            } else if (isExistCall()) {
                sendCallInMessageStatus(myJidNumber, fromNumber, threadMessage,
                        receivedPacket.getCallSession(), ReengCallOutPacket.CallStatus.busyCall);
                insertMessageStateCall(threadMessage, fromNumber, 0, CallHistoryConstant.STATE_MISS, false,
                        receivedPacket.getTimeSend(), ReengMessageConstant.MODE_GSM, false, stateAudioVideo);
            } else {
                long timeOut = TimeHelper.getTimeOutIncomingCall(receivedPacket.getTimeSend(), receivedPacket
                        .getTimeReceive());
                if (timeOut <= 100) {// het timeout cho cuoc goi
                    insertMessageMissCall(threadMessage, fromNumber, false, receivedPacket.getTimeSend(), false,
                            ReengMessageConstant.MODE_GSM);
                    return;
                }
                // nhan cuoc goi call nen callOut = false
                initCall(fromNumber, receivedPacket.getCaller(), receivedPacket.getCallee(), CallConstant
                        .TYPE_CALLEE, true, true, false);
                this.isStrangerConfide = false;
                this.currentCallSession = receivedPacket.getCallSession();
                this.currentIceServers = CallRtcHelper.convertStringeeIceServers(receivedPacket.getIceServers());
                CallInData callInData = parserCallInData(receivedPacket.getCallOutData());
                if (callInData.getCanditateDatas() != null) {
                    for (CallData candidate : callInData.getCanditateDatas()) {
                        CallRtcHelper.getInstance(mApplication).addCallDataToQueue(candidate);
                    }
                }
                if (isStrangerConfide) {
                    mApplication.getMusicBusiness().checkResetConfideSessionAndNotifyRefresh(currentCallSession);
                }
                // send ringing
                this.currentCallState = STATE.RINGING;//180
                ListenerHelper.getInstance().addGsmCallListener(this);
                sendCallInMessageStatus(myJidNumber, fromNumber, threadMessage,
                        receivedPacket.getCallSession(), ReengCallOutPacket.CallStatus.ringing_183);
                startTimerTimeout(timeOut);
                // start incoming screen
                currentCallState = CallConstant.STATE.CALLEE_STARTED;
                startRingIncomingCall();
                Intent intent = new Intent(mApplication, MochaCallActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent
                        .FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_FROM_BACKGROUND);
                Bundle bundle = new Bundle();
                bundle.putString(CallConstant.JIDNUMBER, currentCaller);
                bundle.putInt(CallConstant.TYPE_FRAGMENT, CallConstant.TYPE_CALLEE);
                bundle.putParcelable(CallConstant.SDP_CALL_DATA, callInData.getSdpData());
                bundle.putBoolean(CallConstant.FIRST_START_ACTIVITY, true);
                intent.putExtras(bundle);
                mApplication.startActivity(intent);
                EventBus.getDefault().post(new RecallActivity.RecallActivityEvent());
            }
        }
    }

    private void processAnswerCallIn() {
        audioManager.setSpeakerphoneOn(isSpeaker);
        //ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread
        // (currentFriendJid);
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        Log.i(TAG, "processAddCallInData");
        notifyOnConnectStateChange(STATE.CONNECT);
        sendCallInAnswer();
        CallRtcHelper.getInstance(mApplication).putAllCallDataFromQueue();
        beginTimeConnect = System.currentTimeMillis();
    }

    private void sendCallInAnswer() {
        ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingSoloThread(currentFriendJid);
        if (threadMessage == null) {
            Log.e(TAG, "what ? sendCallMessage callIn status");
            return;
        }
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        sendCallInMessageStatus(myJidNumber, currentFriendJid, threadMessage, currentCallSession, ReengCallOutPacket
                .CallStatus.connected);
    }

    private void declineCallIn(ThreadMessage thread, boolean isBusy) {
        if (thread == null) {
            Log.e(TAG, "what ? sendCallMessage callIn decline");
            return;
        }
        ReengCallOutPacket.CallStatus status;
        if (isBusy || currentCallState < STATE.CONNECT) {
            status = ReengCallOutPacket.CallStatus.busyCall;
        } else {
            status = ReengCallOutPacket.CallStatus.endCall;
        }
        myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        insertMessageEndCall(thread, isBusy);
        sendCallInMessageStatus(myJidNumber, currentFriendJid, thread, currentCallSession, status);
    }

    private void sendCallInMessageStatus(String from, String to, ThreadMessage mThreadMessage,
                                         String session, ReengCallOutPacket.CallStatus status) {
        if (mThreadMessage == null) {
            Log.e(TAG, "what ? sendCallMessage callIn status");
            return;
        }
        // call in chi co chieu nhan
        MochaCallOutMessage callMessage = new MochaCallOutMessage(from, to, mThreadMessage, true);
        callMessage.setCaller(to);
        callMessage.setCallee(from);
        if (status != null && ReengCallOutPacket.CallStatus.containsEndCall(status)) {// gửi end call, type=5
            callMessage.setCallOutType(ReengCallOutPacket.CallOutType.stop_call);
        } else {
            callMessage.setCallOutType(ReengCallOutPacket.CallOutType.receive_status);
        }
        callMessage.setCallStatus(status);
        callMessage.setCallData(null);
        callMessage.setCallSession(session);
        mApplication.getXmppManager().sendCallOutMessage(callMessage, mThreadMessage);
    }

    private CallInData parserCallInData(String input) {
        Log.d(TAG, "parserCallInData: " + input);
        CallInData callInData = new CallInData();
        try {
            JSONObject object = new JSONObject(input);
            String callDataStr = object.optString("sdp", "");
            int sdpType = object.optInt("type", 0);
            final String[] lines = callDataStr.split("\\r?\\n");
            final List<String> sdpList = new ArrayList<>();
            for (String line : lines) {
                if (line.startsWith("a=candidate")) {
                    JSONObject candidateObject = new JSONObject();
                    candidateObject.put("sdpMid", "audio");//default
                    candidateObject.put("sdpMLineIndex", 0);
                    candidateObject.put("sdp", line.substring(2));
                    callInData.addCandidateData(new CallData("candidate", candidateObject.toString()));
                } else {
                    sdpList.add(line);
                }
            }
            StringBuilder buffer = new StringBuilder();
            Iterator<? extends CharSequence> iter = sdpList.iterator();
            //StringBuilder buffer = new StringBuilder(iter.next());
            if (iter.hasNext()) {
                while (iter.hasNext()) {
                    buffer.append(iter.next()).append("\r\n");
                }
            }
            JSONObject sdpObject = new JSONObject();
            sdpObject.put("sdp", buffer.toString());
            sdpObject.put("type", sdpType);
            callInData.setSdpData(new CallData("sdp", sdpObject.toString()));
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            callInData.setSdpData(new CallData("sdp", input));
        }
        //callInData.setSdpData(new CallData("sdp", input));
        Log.d(TAG, "parserCallInData ***: " + callInData.toString());
        return callInData;
    }

    private class CallInData {
        private CallData sdpData;
        private ArrayList<CallData> canditateDatas = new ArrayList<>();

        public CallData getSdpData() {
            return sdpData;
        }

        public void setSdpData(CallData sdpData) {
            this.sdpData = sdpData;
        }

        public ArrayList<CallData> getCanditateDatas() {
            return canditateDatas;
        }

        public void addCandidateData(CallData canditate) {
            this.canditateDatas.add(canditate);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            if (sdpData != null)
                sb.append("sdp: ").append(sdpData.toString()).append("/n");
            for (CallData candidate : canditateDatas) {
                sb.append("candidate: ").append(candidate.toString()).append("/n");
            }
            return sb.toString();
        }
    }

    public long getDeltaTimeConnect() {
        return deltaTimeConnect;
    }

    public boolean isEnableRestartICE() {
        return isEnableRestartICE;
    }

    public int getIceTimeout() {
        return iceTimeout;
    }

    public long getRestartICEDelay() {
        return restartICEDelay;
    }

    public long getRestartICEPeriod() {
        return restartICEPeriod;
    }

    public int getRestartICELoop() {
        return restartICELoop;
    }

    public long getZeroBwEndCall() {
        return zeroBwEndCall;
    }

    public long getNetwork2failedTime() {
        return network2failedTime;
    }

    public long getTimedis2recon() {
        return timedis2recon;
    }

    public boolean isValidRestartICE() {
        if (restartICEPeriod == 0 || restartICELoop == 0 || !isEnableRestartICE)
            return false;
        return true;
    }

    public void setStartedRestartICE(boolean startedRestartICE) {
        isStartedRestartICE = startedRestartICE;
    }

    public boolean isStartedRestartICE() {
        return isStartedRestartICE;
    }

    public long getTimeRestartBw() {
        return timeRestartBw;
    }

    public long getDelayRestartOnFailed() {
        return delayRestartOnFailed;
    }

    public String getBundlePolicy() {
        return bundlePolicy;
    }

    public void setBundlePolicy(String bundlePolicy) {
        this.bundlePolicy = bundlePolicy;
    }

    public String getRtcpMuxPolicy() {
        return rtcpMuxPolicy;
    }

    public void setRtcpMuxPolicy(String rtcpMuxPolicy) {
        this.rtcpMuxPolicy = rtcpMuxPolicy;
    }

    public String getIceTransportsType() {
        return iceTransportsType;
    }

    public void setIceTransportsType(String iceTransportsType) {
        this.iceTransportsType = iceTransportsType;
    }

    public String getCurrentFriendJid() {
        return currentFriendJid;
    }

    private void pushToPacketIdList(String packetId) {
        if (listPacketIdOfReceivedMessage.size() > PACKET_ID_LIST_MAX_SIZE) {
            listPacketIdOfReceivedMessage.pollFirst();
        }
        listPacketIdOfReceivedMessage.addLast(packetId);
    }

    /**
     * @param packetId
     * @return true if packetId already exist
     */
    private boolean checkPacketIdExisted(String packetId) {
        return listPacketIdOfReceivedMessage.contains(packetId);
    }

    protected boolean checkDuplicatePacket(String packetId) {
        if (packetId == null) return true;
        boolean isPacketIdExisted = checkPacketIdExisted(packetId);
        if (isPacketIdExisted) {
            Log.f(TAG, "call packetId " + packetId + " already exist --> duplicate message  --> not process");
            return true;
        }
        pushToPacketIdList(packetId);
        return false;
    }

    //dainv
    // restore state if other call app change state this mocha call
    public void onActivityResume() {
        if (isExistCall() && currentCallState >= STATE.CONNECT) {
            CallRtcHelper.getInstance(mApplication).onActivityResume();
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
//            if (isVideoCall) {
            if (!CallBluetoothHelper.getInstance(mApplication).isCurrentDisableSpeaker()) {
                if (!isHeadset()) {
                    audioManager.setSpeakerphoneOn(isSpeaker);
                }
            }
        }
    }

    public boolean isCallConnected() {
        return currentCallState >= STATE.CONNECT;
    }
}
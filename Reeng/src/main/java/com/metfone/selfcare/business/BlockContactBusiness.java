package com.metfone.selfcare.business;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.datasource.BlockDataSource;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/10/14.
 */
public class BlockContactBusiness {
    private static BlockContactBusiness mInstance;
    private ArrayList<String> listBlock = new ArrayList<>();
    private BlockDataSource mBlockDataSource;
    private ApplicationController mApplication;

//    public static BlockContactBusiness getInstance(Context context) {
//        if (mInstance == null) {
//            mInstance = new BlockContactBusiness(context);
//            mInstance.initBlockList();
//        }
//        return mInstance;
//    }

    public BlockContactBusiness(ApplicationController application) {
        this.mApplication = application;

    }

    public void init() {
        this.mBlockDataSource = BlockDataSource.getInstance(mApplication);
    }

    public ArrayList<String> getListBlock() {
        return listBlock;
    }

    public void setListBlock(ArrayList<String> mListBlock) {
        this.listBlock = mListBlock;
    }

    public boolean isReady() {
        return listBlock != null;
    }

    public void initBlockBusiness() {
        ArrayList<String> list = mBlockDataSource.getAllNumber();
        if (list == null) {
            setListBlock(new ArrayList<String>());
        } else {
            // remove duplicate number block
            /*HashSet<String> hashSetBlocks = new HashSet<>(list);
            setListBlock(new ArrayList<>(hashSetBlocks));*/
            setListBlock(list);
        }
    }

    public void updateListAfterBlockNumbers(ArrayList<String> numbers, boolean deleteAllColum) {
        if (listBlock == null) listBlock = new ArrayList<>();
        if (deleteAllColum) {
            mBlockDataSource.deleteAllTable();
            if (!listBlock.isEmpty()) {
                listBlock.clear();
            }
        }
        listBlock.addAll(numbers);
        mBlockDataSource.insertListNumber(numbers);
    }

    public void addBlockNumber(String number) {
        if (listBlock == null) listBlock = new ArrayList<>();
        listBlock.add(number);
        mBlockDataSource.insertNumber(number);
    }

    public void removeBlockNumber(String number) {
        String newNumber = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(number);
        String oldNumber = PrefixChangeNumberHelper.getInstant(mApplication).getOldNumber(number);
        if (newNumber != null) {
            listBlock.remove(newNumber);
            mBlockDataSource.deleteNumber(newNumber);
        }
        if (oldNumber != null) {
            listBlock.remove(oldNumber);
            mBlockDataSource.deleteNumber(oldNumber);
        }
        listBlock.remove(number);
        mBlockDataSource.deleteNumber(number);
    }

    public void blockNumbers(ArrayList<String> numbers) {
        //Remove numbers khong nam trong danh sach block moi
        for (String num : listBlock) {
            Log.d("blockNumbers", "current number" + num);
            if (!isContain(num, numbers)) {
                mBlockDataSource.deleteNumber(num);
            }
        }
        initBlockBusiness();
        for (String num : listBlock) {
            Log.d("blockNumbers", "number after delete" + num);
        }
        //cap nhat danh sach block moi
        for (String num : numbers) {
            if (!isBlockNumber(num)) {
                listBlock.add(num);
                mBlockDataSource.insertNumber(num);
            }
        }
        for (String num : listBlock) {
            Log.d("blockNumbers", "number after complete" + num);
        }
    }

    public boolean isContain(String number, ArrayList<String> list) {
        if (list == null || list.isEmpty()) {
            return false;
        }
        for (String item : list) {
            if (item.equals(number)) {
                return true;
            }
        }
        return false;
    }

    public boolean isBlockNumber(String number) {
        if (listBlock == null || listBlock.isEmpty()) {
            return false;
        } else {
            for (String item : listBlock) {
                if (item.equals(number)) {
                    return true;
                }
                String newNumb = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(item);
                if (newNumb != null) {
                    if (newNumb.equals(number)) return true;
                }

                String oldNumb = PrefixChangeNumberHelper.getInstant(mApplication).getOldNumber(item);
                if (oldNumb != null) {
                    if (oldNumb.equals(number)) return true;
                }
            }
            return false;
        }
    }
}
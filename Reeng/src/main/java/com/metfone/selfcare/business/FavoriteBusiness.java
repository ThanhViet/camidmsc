package com.metfone.selfcare.business;

import android.content.Context;

import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.network.camid.response.BaseResponse;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;

import java.util.ArrayList;

public class FavoriteBusiness {
    private final String TAG = "FavoriteBusiness";
    private static final String FAVORITE_COUNTRY = "favorite_country"; // save data of list favorite country
    private static final String FAVORITE_CATEGORY = "favorite_category"; // save data of list favorite category
    private static final String ADD_COUNTRY = "added_favorite_country"; // check added list favorite country to local
    private static final String ADD_CATEGORY = "added_favorite_category"; // check added list favorite to local
    private static final String SENT_COUNTRY = "sent_favorite_country"; // check sent list favorite country to server
    private static final String SENT_CATEGORY = "sent_favorite_category"; // check sent list favorite category to server
    private int totalFavoriteCategory = -1;
    private int totalFavoriteCountry = -1;

    private SharedPref sharedPref;
    private MovieApi movieApi;

    public FavoriteBusiness(Context context) {
        this.sharedPref = new SharedPref(context);
        movieApi = new MovieApi();
    }

    public ArrayList<String> getCategory() {
        return getStringArr(FAVORITE_CATEGORY);
    }

    public void setCategory(ArrayList<String> favoriteCategory) {
        setStringArray(FAVORITE_CATEGORY, favoriteCategory);
    }

    public ArrayList<String> getCountry() {
        return getStringArr(FAVORITE_COUNTRY);
    }

    public void setCountry(ArrayList<String> favoriteCountry) {
        setStringArray(FAVORITE_COUNTRY, favoriteCountry);
    }

    public boolean isAddedCountry() {
        return sharedPref.getBoolean(ADD_COUNTRY, false);
    }

    public void addedCountry() {
        sharedPref.putBoolean(ADD_COUNTRY, true);
    }

    public boolean isAddedCategory() {
        return sharedPref.getBoolean(ADD_CATEGORY, false);
    }

    public void addedCategory() {
        sharedPref.putBoolean(ADD_CATEGORY, true);
    }

    public boolean isSentCountry() {
        return sharedPref.getBoolean(SENT_COUNTRY, false);
    }

    public void setSentCountry() {
        sharedPref.putBoolean(SENT_COUNTRY, true);
    }

    public boolean isSentCategory() {
        return sharedPref.getBoolean(SENT_CATEGORY, false);
    }

    public void setSentCategory() {
        sharedPref.putBoolean(SENT_CATEGORY, true);
    }

    public void setStringArray(String key, ArrayList<String> data) {
        if (data != null && data.size() > 0) {
            String str = "";
            int size = data.size();
            for (int i = 0; i < size; i++) {
                str += data.get(i);
                if (i < (size - 1)) {
                    str += ",";
                }
            }
            sharedPref.putString(key, str);
        }
    }

    public ArrayList<String> getStringArr(String key) {
        ArrayList<String> result = new ArrayList<>();
        String data = sharedPref.getString(key, "");
        if (data != null && !data.isEmpty()) {
            String arr[] = data.split(",");
            if (arr != null && arr.length > 0) {
                for (String item : arr) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    public void checkAndSend() {
        if(UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT){
            Log.d(TAG,"Not login");
            return;
        }
        if (isAddedCategory() && !isSentCategory() && isAddedCountry() && !isSentCountry()) {
            sentFavorite();
        }
    }

    private void sentFavorite() {
        // check send favorite category to server
        ArrayList<String> categoryArr = getCategory();
        ArrayList<String> countryArr = getCountry();
        if (categoryArr != null && categoryArr.size() > 0 && isAddedCategory() && !isSentCountry()) {
            totalFavoriteCategory = categoryArr.size();
            for (String categoryId : categoryArr) {
                sendFavoriteCategory(categoryId);
            }
        } else {
            totalFavoriteCategory = 0;
        }
        // check send favorite country to server
        if (countryArr != null && countryArr.size() > 0 && isAddedCountry() && !isSentCountry()) {
            totalFavoriteCountry = countryArr.size();
            for (String categoryId : countryArr) {
                sendFavoriteCountry(categoryId);
            }
            totalFavoriteCountry = 0;
        }
    }

    private void sendFavoriteCategory(String categoryId) {
        movieApi.addFavoriteCategory(categoryId, new ApiCallbackV2<BaseResponse>() {
            @Override
            public void onSuccess(String msg, BaseResponse result) throws JSONException {
                totalFavoriteCategory--;
                if (totalFavoriteCategory == 0) {
                    setSentCategory();
                }
                Log.d(TAG, "sendFavoriteCategory  " + categoryId + " , message : " + result.getMessage());
            }

            @Override
            public void onError(String s) {
                totalFavoriteCategory = 0;
                Log.d(TAG, "sendFavoriteCategory " + categoryId + " , error : " + s);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void sendFavoriteCountry(String categoryId) {
        movieApi.addFavoriteCountry(categoryId, new ApiCallbackV2<BaseResponse>() {
            @Override
            public void onSuccess(String msg, BaseResponse result) throws JSONException {
                totalFavoriteCountry--;
                if (totalFavoriteCountry == 0) {
                    setSentCountry();
                }
                Log.d(TAG, "sendFavoriteCountry : " + categoryId + " , message : " + result.getMessage());
            }

            @Override
            public void onError(String s) {
                totalFavoriteCountry = 0;
                Log.d(TAG, "sendFavoriteCountry " + categoryId + " , error : " + s);
            }

            @Override
            public void onComplete() {

            }
        });
    }
}

package com.metfone.selfcare.business;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.metfone.esport.entity.event.UpdateTokenEvent;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.model.account.IdentifyProvider;
import com.metfone.selfcare.model.account.PhoneLinkedInfo;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.SignInData;
import com.metfone.selfcare.model.account.SignUpData;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.ContactTopup;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.model.camid.User;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_FIREBASE_REFRESHED_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IDENTIFY_PROVIDER;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_LIST_PHONE_SERVICE;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_PHONE_SERVICE;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_SEND_TOKEN_SUCCESS;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_SIGN_UP_INFO;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_WS_ISDN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_WS_SESSION_ID;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_WS_TOKEN;

public class CamIdUserBusiness {
    private static final String TAG = CamIdUserBusiness.class.getSimpleName();
    private ApplicationController mApplicationController;
    private SignUpData mSignUpData;
    private User mUser;
    private List<Service> mServices;
    private List<IdentifyProvider> mIdentityProviders;
    private SharedPrefs mSharedPrefs;
    private String mPhoneService = "";
    private String mCamIdToken = "";
    private String mMetfoneToken = "";
    private String mMetfoneSessionId = "";
    private String mMetfoneUsernameIsdn = "";
    private String mFirebaseRefreshedToken = "";
    private List<Store> mStores;
    private boolean mIsMetfoneTabSelected = false;
    private boolean mIsSignInUpSuccess = false;
    private int mPositionMetfoneTab = -1;
    private boolean mIsProcessingLoginSignUpMetfone = false;
    private boolean mAddOrSelectPhoneCalled = false;
    private ContactTopup mContactTopup = null;
    private boolean mForceUpdateMetfone = false;
    private boolean mIsProcessingLoginSignUpGame = false;
    private boolean mIsProcessingLoginSignUpGame2 = false;
    private int mPositionGameTab = -1;
    private String accountFTTH="";
    public CamIdUserBusiness(ApplicationController app) {
        this.mApplicationController = app;
        this.mSharedPrefs = SharedPrefs.getInstance();

        if (this.mSignUpData == null) {
            this.mSignUpData = new SignUpData();
        }

        if (this.mUser == null) {
            this.mUser = new User();
        }

        if (this.mServices == null) {
            this.mServices = new ArrayList<>();
        }

        if (this.mContactTopup == null) {
            this.mContactTopup = new ContactTopup();
        }
    }

    public SignUpData getSignUpData() {
        if (mSignUpData == null || mSignUpData.getAccess_token().isEmpty() || "".equals(mSignUpData.getAccess_token())) {
            String json = mSharedPrefs.get(PREF_SIGN_UP_INFO, String.class);
            Gson gson = new GsonBuilder().create();
            mSignUpData = gson.fromJson(json, SignUpData.class);
        }
        return mSignUpData;
    }

    public void setSignUpData(SignUpData signUpData) {
        this.mSignUpData = signUpData;
        String token = formatToken(signUpData.getAccess_token());
        this.mCamIdToken = token;
        this.mSignUpData.setAccess_token(token);
        this.mSharedPrefs.put(PREF_ACCESS_TOKEN, token);

        Gson gson = new Gson();
        String json = gson.toJson(mSignUpData);
        mSharedPrefs.put(PREF_SIGN_UP_INFO, json);
    }

    /**
     * Merge data of SignInData into SignUpData for getAccessToken
     *
     * @param signInData
     */
    public void setSignInData(SignInData signInData) {
        if (signInData == null) {
            return;
        }
        String token = formatToken(signInData.getAccess_token());
        this.mCamIdToken = token;
        this.mSignUpData.setAccess_token(token);
        this.mSignUpData.setExpires_in(signInData.getExpires_in());
        this.mSignUpData.setRefresh_token(signInData.getRefresh_token());
        this.mSharedPrefs.put(PREF_ACCESS_TOKEN, token);

        Gson gson = new Gson();
        String json = gson.toJson(mSignUpData);
        mSharedPrefs.put(PREF_SIGN_UP_INFO, json);
    }

    private String formatToken(String originalToken) {
        return "Bearer " + originalToken;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        this.mUser = user;
    }

    /**
     * Merge data of UserInfo into User
     *
     * @param userInfo
     */
    public void setUserInfo(UserInfo userInfo) {
        if (this.mUser == null) {
            this.mUser = new User();
        }

        if (userInfo != null) {
            mUser.setUsername(userInfo.getUsername());
            mUser.setEmail(userInfo.getEmail());
            mUser.setAddress(userInfo.getAddress());
            mUser.setGender(userInfo.getGender());
            mUser.setAvatar(userInfo.getAvatar());
            mUser.setVerified(userInfo.getVerified());
            mUser.setUserId(userInfo.getUser_id());
            mUser.setPhoneNumber(userInfo.getPhone_number());
            mUser.setFullName(userInfo.getFull_name());
            mUser.setDateOfBirth(userInfo.getDate_of_birth());
            mUser.setIdentityNumber(userInfo.getIdentity_number());
        }
    }

    public List<Service> getServices() {
        if (mServices == null || mServices.size() == 0) {
            String json = mSharedPrefs.get(PREF_LIST_PHONE_SERVICE, String.class);
            mServices = getObjectList(json, Service.class);
        }
        return mServices;
    }

    /**
     * Set services using {@link Service} model
     * @param services
     */
    public void setService(List<Service> services) {
        String jsonValue = convertObjectToJson(services);
        mSharedPrefs.put(PREF_LIST_PHONE_SERVICE, jsonValue);
        this.mServices = services;
        setPhoneService(services);
    }

    /**
     * Set services using {@link Services} model
     * @param servicesList
     */
    public void setServices(List<Services> servicesList) {
        mServices = new ArrayList<>();
        if (servicesList != null && servicesList.size() > 0) {
            for (Services s : servicesList) {
                Service service = new Service();
                service.setServiceId(s.getServiceId());
                service.setServiceCode(s.getServiceCode());
                service.setServiceName(s.getServiceName());

                if (s.getPhoneLinkedList() != null) {
                    List<PhoneLinkedInfo> phoneLinkedInfos = s.getPhoneLinkedList();
                    List<PhoneLinked> phoneLinkeds = new ArrayList<>();
                    for (PhoneLinkedInfo p : phoneLinkedInfos) {
                        PhoneLinked phoneLinked = new PhoneLinked();
                        try {
                            phoneLinked.setStatus(Integer.parseInt(p.getStatus()));
                        } catch (Exception e) {
                            Log.e(TAG, "setServices: ", e.fillInStackTrace());
                        }
                        if (p.getMetfonePlus() == 1 && service.getServiceCode().trim().equals("MOBILE")) {
                           setPhoneService(p.getPhoneNumber());
                        }
                        phoneLinked.setMetfonePlus(p.getMetfonePlus());
                        phoneLinked.setPhoneNumber(p.getPhoneNumber());
                        phoneLinked.setUserServiceId(p.getUserServiceId());
                        phoneLinkeds.add(phoneLinked);
                    }
                    service.setPhoneLinked(phoneLinkeds);
                    mServices.add(service);
                }
            }
        }

        String jsonValue = convertObjectToJson(mServices);
        mSharedPrefs.put(PREF_LIST_PHONE_SERVICE, jsonValue);

        setPhoneService(mServices);
    }

    public String getPhoneService() {
        if (mPhoneService == null || "".equals(mPhoneService)) {
            mPhoneService = SharedPrefs.getInstance().get(PREF_PHONE_SERVICE, String.class);
        }
        return mPhoneService;
    }

    public void setPhoneService(String phoneService) {
        mPhoneService = phoneService;
        SharedPrefs.getInstance().put(PREF_PHONE_SERVICE, phoneService);
    }

    public void setPhoneService(List<Service> services) {
        boolean containMetfonePlus1 = false;
        for (Service s : services) {
            List<PhoneLinked> phoneLinkeds = s.getPhoneLinked();
            for (int i = 0; i < phoneLinkeds.size(); i++) {
                PhoneLinked phoneLinked = phoneLinkeds.get(i);
                if (phoneLinked.getMetfonePlus() == 1) {
                  if(s.getServiceCode().trim().equals("MOBILE")) {
                      setPhoneService(phoneLinked.getPhoneNumber());
                  }
                    containMetfonePlus1 = true;
                    break;
                }
            }
        }

        if (!containMetfonePlus1) {
            setPhoneService("");
        }
    }

    public String getMetfoneToken() {
        if (mMetfoneToken == null || "".equals(mMetfoneToken)) {
            mMetfoneToken = mSharedPrefs.get(PREF_WS_TOKEN, String.class);
        }
        return mMetfoneToken;
    }

    public void setMetfoneToken(String wsToken) {
        mMetfoneToken = wsToken;
        mSharedPrefs.put(PREF_WS_TOKEN, mMetfoneToken);
    }

    public String getMetfoneSessionId() {
        if (mMetfoneSessionId == null || "".equals(mMetfoneSessionId)) {
            mMetfoneSessionId = mSharedPrefs.get(PREF_WS_SESSION_ID, String.class);
        }
        return mMetfoneSessionId;
    }

    public void setMetfoneSessionId(String wsSessionId) {
        mMetfoneSessionId = wsSessionId;
        mSharedPrefs.put(PREF_WS_SESSION_ID, wsSessionId);
    }

    public String getMetfoneUsernameIsdn() {
        if (mMetfoneUsernameIsdn == null || "".equals(mMetfoneUsernameIsdn)) {
            mMetfoneUsernameIsdn = mSharedPrefs.get(PREF_WS_ISDN, String.class);
        }
        return mMetfoneUsernameIsdn;
    }

    public String getAccountFTTH() {
        return accountFTTH;
    }

    public void setAccountFTTH(String accountFTTH) {
        this.accountFTTH = accountFTTH;
    }

    public void setMetfoneUsernameIsdn(String wsUsernameIsdn) {
        mMetfoneUsernameIsdn = wsUsernameIsdn;
        mSharedPrefs.put(PREF_WS_ISDN, wsUsernameIsdn);
    }

    public boolean isCamIdLogin() {
        return mSharedPrefs.get(PREF_IS_LOGIN, Boolean.class);
    }

    public String getCamIdToken() {
        if ("".equals(mCamIdToken)) {
            mCamIdToken = mSharedPrefs.get(PREF_ACCESS_TOKEN, String.class);
        }
        return mCamIdToken;
    }

    public String getFirebaseRefreshedToken() {
        if ("".equals(mFirebaseRefreshedToken)) {
            mFirebaseRefreshedToken = mSharedPrefs.get(PREF_FIREBASE_REFRESHED_TOKEN, String.class);
        }
        return mFirebaseRefreshedToken;
    }

    public void setFirebaseRefreshedToken(String mFirebaseRefreshedToken) {
        this.mFirebaseRefreshedToken = mFirebaseRefreshedToken;
        mSharedPrefs.put(PREF_FIREBASE_REFRESHED_TOKEN, mFirebaseRefreshedToken);
        mSharedPrefs.put(PREF_SEND_TOKEN_SUCCESS, false);
        EventBus.getDefault().postSticky(new UpdateTokenEvent(mFirebaseRefreshedToken));
    }

    public List<Store> getStores() {
        return mStores;
    }

    public void setStores(List<Store> mStores) {
        this.mStores = mStores;
    }

    public List<IdentifyProvider> getIdentityProviders() {
        if (mIdentityProviders == null) {
            String jsonObject = mSharedPrefs.get(PREF_IDENTIFY_PROVIDER, String.class);
            mIdentityProviders = getObjectList(jsonObject, IdentifyProvider.class);
        }
        return mIdentityProviders;
    }

    public void setIdentityProviders(List<IdentifyProvider> identityProviders) {
        String jsonValue = convertObjectToJson(identityProviders);
        mSharedPrefs.put(PREF_IDENTIFY_PROVIDER, jsonValue);
        this.mIdentityProviders = identityProviders;
    }

    public boolean isMetfoneTabSelected() {
        return mIsMetfoneTabSelected;
    }

    public void setIsMetfoneTabSelected(boolean isMetfoneTabSelected) {
        this.mIsMetfoneTabSelected = isMetfoneTabSelected;
    }

    public boolean isSignInUpSuccess() {
        return mIsSignInUpSuccess;
    }

    public void setIsSignInUpSuccess(boolean isSignInUpSuccess) {
        this.mIsSignInUpSuccess = isSignInUpSuccess;
    }

    public int getPositionMetfoneTab() {
        return mPositionMetfoneTab;
    }

    public void setPositionMetfoneTab(int positionMetfoneTab) {
        this.mPositionMetfoneTab = positionMetfoneTab;
    }

    public boolean isProcessingLoginSignUpMetfone() {
        return mIsProcessingLoginSignUpMetfone;
    }

    public void setProcessingLoginSignUpMetfone(boolean processingLoginSignUpMetfone) {
        this.mIsProcessingLoginSignUpMetfone = processingLoginSignUpMetfone;
    }

    public int getPositionGameTab() {
        return mPositionGameTab;
    }

    public void setPositionGameTab(int positionGameTab) {
        this.mPositionGameTab = positionGameTab;
    }

    public boolean isProcessingLoginSignUpGame() {
        return mIsProcessingLoginSignUpGame;
    }

    public boolean isProcessingLoginSignUpGame2() {
        return mIsProcessingLoginSignUpGame2;
    }

    public void setProcessingLoginSignUpGame(boolean processingLoginSignUpGame) {
        this.mIsProcessingLoginSignUpGame = processingLoginSignUpGame;
    }

    public void setProcessingLoginSignUpGame2(boolean processingLoginSignUpGame) {
        this.mIsProcessingLoginSignUpGame2 = processingLoginSignUpGame;
    }

    public boolean isAddOrSelectPhoneCalled() {
        return mAddOrSelectPhoneCalled;
    }

    public void setAddOrSelectPhoneCalled(boolean addOrSelectPhoneCalled) {
        this.mAddOrSelectPhoneCalled = addOrSelectPhoneCalled;
    }

    public ContactTopup getContactTopup() {
        if (mContactTopup == null) {
            mContactTopup = new ContactTopup();
        }
        return mContactTopup;
    }

    public void setContactTopup(ContactTopup contactTopup) {
        this.mContactTopup = contactTopup;
    }

    public boolean isForceUpdateMetfone() {
        return mForceUpdateMetfone;
    }

    public void setForceUpdateMetfone(boolean forceUpdateMetfone) {
        this.mForceUpdateMetfone = forceUpdateMetfone;
    }

    public static <T> List<T> getObjectList(String jsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson();
            JsonArray arry = new JsonParser().parse(jsonString).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                list.add(gson.fromJson(jsonElement, cls));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static <T> String convertObjectToJson(List<T> arr) {
        return new Gson().toJson(arr);
    }

    public void deleteAllInfo() {
        mSignUpData = null;
        mUser = null;
        mServices = new ArrayList<>();
        mIdentityProviders = new ArrayList<>();
        mPhoneService = "";
        mCamIdToken = "";
        mMetfoneToken = "";
        mMetfoneSessionId = "";
        mMetfoneUsernameIsdn = "";
        mFirebaseRefreshedToken = "";
        mStores = new ArrayList<>();
        mIsMetfoneTabSelected = false;
        mIsSignInUpSuccess = false;
        mPositionMetfoneTab = -1;
        mPositionGameTab = -1;
        mIsProcessingLoginSignUpMetfone = false;
        mIsProcessingLoginSignUpGame = false;
        mIsProcessingLoginSignUpGame2 = false;
        mContactTopup = null;
        mForceUpdateMetfone = false;
        if (mSharedPrefs != null) {
            this.mSharedPrefs.remove(PREF_ACCESS_TOKEN);
            this.mSharedPrefs.remove(PREF_SIGN_UP_INFO);
            this.mSharedPrefs.remove(PREF_PHONE_SERVICE);
            this.mSharedPrefs.remove(PREF_WS_TOKEN);
            this.mSharedPrefs.remove(PREF_WS_SESSION_ID);
            this.mSharedPrefs.remove(PREF_WS_ISDN);
            this.mSharedPrefs.remove(PREF_FIREBASE_REFRESHED_TOKEN);
        }
    }
}

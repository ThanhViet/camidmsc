package com.metfone.selfcare.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.onmedia.ContentDiscovery;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.NotificationModel;
import com.metfone.selfcare.database.model.onmedia.ObjectLikeTmp;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.database.model.onmedia.SieuHaiModel;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.jivesoftware.smack.packet.Presence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by thanhnt72 on 9/26/2015.
 */
public class FeedBusiness {
    private static final String TAG = FeedBusiness.class.getSimpleName();
    private static int TIME_COUNTDOWN_ACTION_LIKE = 3000;

    private ApplicationController mApplicationController;
    private ContactBusiness mContactBusiness;
    private SharedPreferences mPref;
    private ArrayList<FeedModelOnMedia> listFeed = new ArrayList<>();
    private ArrayList<FeedModelOnMedia> listFeedPresence = new ArrayList<>();
    private ArrayList<FeedModelOnMedia> listPost = new ArrayList<>();

    private HashMap<String, FeedModelOnMedia> hashMapFeedTmp = new HashMap<>();

    private ArrayList<NotificationModel> listNotification = new ArrayList<>();
    private ArrayList<FeedModelOnMedia> listFeedProfile = new ArrayList<>();
    private int totalNotify = 0;

    private String myPhone;
    private FeedBusinessInterface feedBusinessInterface;

    private FeedModelOnMedia feedPlayList, feedProfileProcess;
    private HashMap<String, ObjectLikeTmp> hashMapActionLike = new HashMap<>();
    private CountDownTimer timerActionLike;
    private boolean isRunningCountDown;
    private WSOnMedia rest;
    private boolean needToRefreshListNotify;
    private long deltaTimeServer;

    private ArrayList<ContentDiscovery> listContentDiscoveries = new ArrayList<>();
    private HashMap<String, ArrayList<ContentDiscovery>> hashMapDiscoveriesType = new HashMap<>();
    private ArrayList<Video> listSieuHai = new ArrayList<>();
//    private int indexCurrentPlaySieuHai = -1; //Neu ko phai la video tu sieu hai thi ko co index

    public FeedBusiness(ApplicationController app) {
        this.mApplicationController = app;
        mContactBusiness = mApplicationController.getContactBusiness();
        ReengAccount reengAccount = mApplicationController.getReengAccountBusiness().getCurrentAccount();
        if (reengAccount != null) {
            myPhone = reengAccount.getJidNumber();
        }
        mPref = mApplicationController.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                Context.MODE_PRIVATE);
        rest = new WSOnMedia(mApplicationController);
        initCountDown();
    }

    public void setListFeedToPref() {
        try {
            mPref.edit().putString(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, new Gson().toJson(listFeed)).apply();
        } catch (NoSuchMethodError ex) {

        }
    }

    public ArrayList<FeedModelOnMedia> getListFeedFromPref() {
        try {
            String str = mPref.getString(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, "");
            Log.i(TAG, "Cache: " + str);
            ArrayList<FeedModelOnMedia> listFeedCache = new Gson().fromJson(str,
                    new TypeToken<List<FeedModelOnMedia>>() {
                    }.getType());
            if (listFeedCache != null && !listFeedCache.isEmpty()) {
                listFeed = listFeedCache;
                return listFeed;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return new ArrayList<>();
    }

    public void setListNotifyToPref() {
        mPref.edit().putString(Constants.ONMEDIA.EXTRAS_NOTIFY_DATA, new Gson().toJson(listNotification)).apply();
    }

    public ArrayList<NotificationModel> getListNotifyFromPref() {
        try {
            String str = mPref.getString(Constants.ONMEDIA.EXTRAS_NOTIFY_DATA, "");
            Log.i(TAG, "Cache: " + str);
            ArrayList<NotificationModel> listNotifyCache = new Gson().fromJson(str,
                    new TypeToken<List<NotificationModel>>() {
                    }.getType());
            if (listNotifyCache != null && !listNotifyCache.isEmpty()) {
                listNotification = listNotifyCache;
                return listNotification;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return new ArrayList<>();
    }

    public ArrayList<FeedModelOnMedia> getListFeed() {
        return listFeed;
    }

    public void setListFeed(ArrayList<FeedModelOnMedia> listFeed) {
        this.listFeed.clear();
        this.listFeed.addAll(listFeed);
    }

    public ArrayList<FeedModelOnMedia> getListFeedAfterDeleteDuplicate() {
        ArrayList<FeedModelOnMedia> listTmp = listFeed;
        listTmp.addAll(0, listFeedPresence);
        deleteAndEditFeed(listTmp);
        listTmp = deleteDuplicateFeed(listTmp);
        return listTmp;
    }

    public void updateFeedAfterReceiverPresence(Presence presence) {
        Presence.Type type = presence.getType();
        if (type == Presence.Type.available) {
            String feedString = presence.getFeed();
            Log.i(TAG, "feedString: " + feedString);
            RestAllFeedsModel restPresence = new Gson().fromJson(feedString, RestAllFeedsModel.class);
            if (restPresence.getData() != null) {
                Log.i(TAG, "feed presence size: " + restPresence.getData().size());
                //xoa cac feed lap trong listpresent
//                ArrayList<FeedModelOnMedia> feedTmp = deleteDuplicateFeed(restPresence.getData());
//                Log.i(TAG, "feedTmp presence after deleteDuplicate size: " + feedTmp.size());
                //add toan bo feed presence moi vao listFeedPresence
                listFeedPresence.addAll(0, restPresence.getData());
                Log.i(TAG, "listFeedPresence after add: " + listFeedPresence.size());
                //xoa cac feed lap trong listpresence
//                listFeedPresence = deleteDuplicateFeed(listFeedPresence);
//                Log.i(TAG, " listFeedPresence after deleteDuplicateFeed: " + listFeedPresence.size());
                if (hasOnlyMyFeed()) {
                    Log.i(TAG, "hasOnlyMyFeed notify nowwww");
                    listFeed = getListFeedAfterDeleteDuplicate();
                    clearListFeedPresence();
                    notifyNewFeed(true, false);
                } else {
                    Log.i(TAG, "wtf");
                    notifyNewFeed(false, false);
                }
            }
        }
    }

    private boolean hasOnlyMyFeed() {
        boolean hasfeed = true;
        for (FeedModelOnMedia feed : listFeedPresence) {
            if (!feed.getUserInfo().getMsisdn().equals(myPhone)) {
                hasfeed = false;
            }
        }
        return hasfeed;
    }

    /*private void handleListPresence() {
        HashMap<String, FeedModelOnMedia> hashPresence = new HashMap<>();
        for (int i = 0; i < listFeedPresence.size() - 1; i++) {

        }
    }*/

    public void notifyNewFeed(boolean needNotify, boolean needToSetSelection) {
        ListenerHelper.getInstance().onNotifyFeedOnMedia(needNotify, needToSetSelection);
    }

    private void deleteAndEditFeed(ArrayList<FeedModelOnMedia> list) {
        ArrayList<FeedModelOnMedia> result = new ArrayList<>();
        HashMap<String, FeedModelOnMedia> hashMapFeed = new HashMap<>();
        for (FeedModelOnMedia item : list) {
            // If String is not in set, add it to the list and the set.
            if (!hashMapFeed.containsKey(item.getBase64RowId())) {
                result.add(item);
                hashMapFeed.put(item.getBase64RowId(), item);
            }
        }
        list = result;
        Collections.sort(list);

        /*ArrayList<FeedModelOnMedia> listDeleteEdit = new ArrayList<>();  //mang delete edit
        ArrayList<FeedModelOnMedia> listTmp = listFeed;
        for (int i = 0; i < listTmp.size() - 1; i++) {
            if (listDeleteEdit.contains(listTmp.get(i))) {
                //Gap thang nao ma giong nhau ve url va rowid thi xoa o list res;
                listFeed.remove(i);
            } else {
                if (listTmp.get(i).getActionType() == FeedModelOnMedia.ActionLogApp.DELETE
                        || listTmp.get(i).getActionType() == FeedModelOnMedia.ActionLogApp.EDIT) {
                    listDeleteEdit.add(listTmp.get(i));
                }
            }
        }
        Collections.sort(listFeed);*/
    }

    private ArrayList<FeedModelOnMedia> deleteDuplicateFeed(ArrayList<FeedModelOnMedia> data) {
        ArrayList<FeedModelOnMedia> result = new ArrayList<>();

        // Record encountered Strings in HashSet.
//        HashSet<FeedModelOnMedia> set = new HashSet<>();
        HashMap<String, FeedModelOnMedia> hashMap = new HashMap<>();

        // Loop over argument list.
        for (FeedModelOnMedia item : data) {
            // If String is not in set, add it to the list and the set.
            String urlKey = TextHelper.replaceUrl(item.getFeedContent().getUrl());
            if (!hashMap.containsKey(urlKey)
                    && item.getActionType() != FeedModelOnMedia.ActionLogApp.DELETE
                    && item.getActionType() != FeedModelOnMedia.ActionLogApp.UNLIKE) {
                result.add(item);
                hashMap.put(urlKey, item);
            }
        }
        return result;

        /*int size = data.size();
        // not using a method in the check also speeds up the execution
        // also i must be less that size-1 so that j doesn't
        // throw IndexOutOfBoundsException
        for (int i = 0; i < size - 1; i++) {
            // start from the next item after strings[i]
            // since the ones before are checked
            for (int j = i + 1; j < size; j++) {
                // no need for if ( i == j ) here
                String url1 = data.get(j).getFeedContent().getUrl();
                String url2 = data.get(i).getFeedContent().getUrl();
                if (!url1.equals(url2))
                    continue;
                data.remove(j);
                // decrease j because the array got re-indexed
                j--;
                // decrease the size of the array
                size--;
            } // for j
        } // for i

*//*        for (FeedModelOnMedia feed : data) {
            Log.i(TAG, "feed: " + feed.toString());
        }*//*
        return data;*/

    }

    /*private ArrayList<FeedModelOnMedia> deleteDuplicateTwoList(ArrayList<FeedModelOnMedia> data,
    ArrayList<FeedModelOnMedia> listCheck) {
        Log.i(TAG, "deleteDuplicateTwoList size listpost: " + listCheck.size());
        HashSet<FeedModelOnMedia> hashSetSever = new HashSet<FeedModelOnMedia>(data);
        HashSet<FeedModelOnMedia> hashSetPost = new HashSet<FeedModelOnMedia>(listCheck);
        hashSetPost.removeAll(hashSetSever);
        return new ArrayList<FeedModelOnMedia>(hashSetPost);
    }*/

    public ArrayList<FeedModelOnMedia> getListFeedPresence() {
        return listFeedPresence;
    }

    public void clearListFeedPresence() {
        listFeedPresence.clear();
    }

    public FeedModelOnMedia getFeedModelFromUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }
        url = TextHelper.replaceUrl(url);
        for (FeedModelOnMedia feed : listFeed) {
            FeedContent feedContent = feed.getFeedContent();
            if (feedContent != null) {
                if (TextHelper.replaceUrl(feedContent.getUrl()).equals(url)) {
                    return feed;
                }
            }
        }
        return null;
    }

    /*public int findModelOnMedia(FeedModelOnMedia feed) {
        String urlKey = feed.getFeedContent().getUrl();
        for (int i = 0; i < listFeed.size(); i++) {
            if (listFeed.get(i).getFeedContent().getUrl().equals(urlKey)) {
                return i;
            }
            *//*if (Arrays.equals(listFeed.get(i).getRowId(), feed.getRowId())) {
                return i;
            }*//*
        }
        return -1;
    }*/

    public ArrayList<FeedModelOnMedia> getListPost() {
        return listPost;
    }

    public void insertListPostToListFeed() {
        //TODO delete duplicate
        if (Utilities.notEmpty(listPost) && Utilities.notEmpty(listFeed)) {
            Log.i(TAG, "deleteDuplicateTwoList size listPost: " + listPost.size() + ", listFeed: " + listFeed.size());
            HashSet<FeedModelOnMedia> hashSetSever = new HashSet<>(listFeed);
            HashSet<FeedModelOnMedia> hashSetPost = new HashSet<>(listPost);
            hashSetPost.removeAll(hashSetSever);
            listPost = new ArrayList<>(hashSetPost);
            for (FeedModelOnMedia item : listPost) {
                if (item != null && Utilities.notEmpty(item.getBase64RowId())) {
                    listFeed.add(item);
                }
            }
//            listFeed.addAll(listPost);
            Collections.sort(listFeed);
            Log.i(TAG, "after deleteDuplicateTwoList size listPost: " + listPost.size() + ", listFeed: " + listFeed.size());
        }
    }

    public ArrayList<NotificationModel> getListNotification() {
        return listNotification;
    }

    public void setListNotification(ArrayList<NotificationModel> listNotification) {
        this.listNotification = listNotification;
    }

    public int getTotalNotify() {
        return totalNotify;
    }

    public void setTotalNotify(int totalNotify) {
        this.totalNotify = totalNotify;
        if (feedBusinessInterface != null) {
            feedBusinessInterface.onUpdateNotifyFeed(totalNotify);
        }
    }

    public synchronized void setFeedBusinessInterface(FeedBusinessInterface listener) {
        feedBusinessInterface = listener;
    }

    public FeedModelOnMedia getFeedFromRowId(String rowId) {
        if (TextUtils.isEmpty(rowId)) return null;
        for (FeedModelOnMedia feed : listFeed) {
            if (rowId.equals(feed.getBase64RowId())) {
                return feed;
            }
        }
        return null;
    }

    public FeedModelOnMedia getFeedPlayList() {
        return feedPlayList;
    }

    public void setFeedPlayList(FeedModelOnMedia feedPlayList) {
        this.feedPlayList = feedPlayList;
    }

    public FeedModelOnMedia getFeedProfileProcess() {
        return feedProfileProcess;
    }

    public void setFeedProfileProcess(FeedModelOnMedia feedProfileProcess) {
        this.feedProfileProcess = feedProfileProcess;
    }

    public ArrayList<FeedModelOnMedia> getListFeedProfile() {
        return listFeedProfile;
    }

    public void setListFeedProfile(ArrayList<FeedModelOnMedia> listFeedProfile) {
        this.listFeedProfile = listFeedProfile;
    }

    public FeedModelOnMedia getFeedProfileFromUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }
        for (FeedModelOnMedia feed : listFeedProfile) {
            if (feed.getFeedContent() != null && feed.getFeedContent().getUrl().equals(url)) {
                return feed;
            }
        }
        return null;
    }

    public FeedModelOnMedia getFeedProfileFromRowId(String rowId) {
        if (TextUtils.isEmpty(rowId)) {
            return null;
        }
        for (FeedModelOnMedia feed : listFeedProfile) {
            if (feed.getBase64RowId().equals(rowId)) {
                return feed;
            }
        }
        return null;
    }

    private void initCountDown() {
        timerActionLike = new CountDownTimer(TIME_COUNTDOWN_ACTION_LIKE, TIME_COUNTDOWN_ACTION_LIKE) {
            @Override
            public void onTick(long l) {
//                isRunningCountDown = true;
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "onfinish, sendactionlike");
                isRunningCountDown = false;
                sendActionLike();
            }
        };
    }

    private void sendActionLike() {
        if (hashMapActionLike == null) {
            hashMapActionLike = new HashMap<>();
            return;
        }
        for (Map.Entry<String, ObjectLikeTmp> entry : hashMapActionLike.entrySet()) {
            String url = entry.getKey();
            ObjectLikeTmp obj = entry.getValue();
            actionLikeFeed(url, obj.getRowId(), obj.getActionLogApp(), obj.getFeedContent());
        }
        hashMapActionLike.clear();
    }

    public void addUrlActionLike(String urlKey, String rowId, FeedModelOnMedia.ActionLogApp action, FeedContent feedContent) {
        Log.i(TAG, "addUrlActionLike: " + urlKey + " action: " + action);
        if (TextUtils.isEmpty(urlKey)) return;
        if (hashMapActionLike == null) {
            hashMapActionLike = new HashMap<>();
        }
        hashMapActionLike.remove(urlKey);
        hashMapActionLike.put(urlKey, new ObjectLikeTmp(rowId, action, feedContent));
        if (timerActionLike == null) {
            initCountDown();
        }
        if (isRunningCountDown) {
            timerActionLike.cancel();
        }
        isRunningCountDown = true;
        timerActionLike.start();
    }

    private void actionLikeFeed(String url, String rowId, final FeedModelOnMedia.ActionLogApp action, FeedContent feedContent) {
        Log.i(TAG, "actionLikeFeed: " + url + " action: " + action);
        rest.logAppV6(url, "", feedContent, action, "", rowId, "", null,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "action " + action.toString() + " response: " + response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
    }

    // rowID empty la reset all count notify
    public void setNotifySeen(String rowId) {
        rest.resetNotify(rowId, Constants.ONMEDIA.TYPE_RESET_NOTIFY.TYPE_ONMEDIA, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                com.metfone.selfcare.util.Log.i(TAG, "reset notify ok");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                com.metfone.selfcare.util.Log.i(TAG, "reset notify onErrorResponse");
            }
        });
    }

    public boolean isNeedToRefreshListNotify() {
        return needToRefreshListNotify;
    }

    public void setNeedToRefreshListNotify(boolean needToRefreshListNotify) {
        this.needToRefreshListNotify = needToRefreshListNotify;
    }

    public long getDeltaTimeServer() {
        if (deltaTimeServer == 0L) {
            deltaTimeServer = mPref.getLong(Constants.ONMEDIA.EXTRAS_DELTA_TIME, 0L);
        }
        return deltaTimeServer;
    }

    public void setDeltaTimeServer(long deltaTimeServer) {
        this.deltaTimeServer = deltaTimeServer;
        mPref.edit().putLong(Constants.ONMEDIA.EXTRAS_DELTA_TIME, deltaTimeServer).apply();
    }

    public ArrayList<FeedAction> preProcessFeedActionListTag(ArrayList<FeedAction> list) {
        if (list == null || list.isEmpty()) {
            return list;
        }
        for (FeedAction feedAction : list) {
            ArrayList<TagMocha> listTag = feedAction.getListTag();
            if (listTag != null && !listTag.isEmpty()) {
                for (TagMocha tag : listTag) {
                    String msisdn = tag.getMsisdn();
                    if (msisdn.equals(myPhone)) {
                        tag.setContactName(mApplicationController.getReengAccountBusiness().getUserName());
                    } else {
                        PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(msisdn);
                        if (phoneNumber != null) {
                            tag.setContactName(phoneNumber.getName());
                        } else {
                            if (!TextUtils.isEmpty(tag.getName())) {
                                tag.setContactName(tag.getName());
                            } else if (!TextUtils.isEmpty(tag.getMsisdn())) {
                                tag.setContactName(Utilities.hidenPhoneNumber(tag.getMsisdn()));
                            } else {
                                tag.setContactName("***");
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    public ArrayList<FeedModelOnMedia> preProcessListFeedModelListTag(ArrayList<FeedModelOnMedia> list) {
        if (list == null || list.isEmpty()) {
            return list;
        }
        for (FeedModelOnMedia feed : list) {
            ArrayList<TagMocha> listTag = feed.getListTag();
            if (listTag != null && !listTag.isEmpty()) {
                for (TagMocha tag : listTag) {
                    processTag(tag);
                }
            }
            /*if (feed.getListComment() != null && !feed.getListComment().isEmpty()
                    && feed.getListComment().get(0) != null) {
                ArrayList<TagMocha> listTagCmt = feed.getListComment().get(0).getListTag();
                if (listTagCmt != null && !listTagCmt.isEmpty()) {
                    for (TagMocha tag : listTagCmt) {
                        processTag(tag);
                    }
                }
            }*/
            if (feed.getFeedContent() != null && feed.getFeedContent().getContentListTag() != null &&
                    !feed.getFeedContent().getContentListTag().isEmpty()) {
                ArrayList<TagMocha> listContentTag = feed.getFeedContent().getContentListTag();
                for (TagMocha tag : listContentTag) {
                    processTag(tag);
                }
            }
        }
        return list;
    }

    public FeedModelOnMedia preProcessFeedModelListTag(FeedModelOnMedia feed) {
        ArrayList<TagMocha> listTag = feed.getListTag();
        if (listTag != null && !listTag.isEmpty()) {
            for (TagMocha tag : listTag) {
                processTag(tag);
            }
        }
        if (feed.getFeedContent() != null && feed.getFeedContent().getContentListTag() != null &&
                !feed.getFeedContent().getContentListTag().isEmpty()) {
            ArrayList<TagMocha> listContentTag = feed.getFeedContent().getContentListTag();
            for (TagMocha tag : listContentTag) {
                processTag(tag);
            }
        }
        return feed;
    }

    private void processTag(TagMocha tag) {
        String msisdn = tag.getMsisdn();
        if (!TextUtils.isEmpty(msisdn)) {
            if (msisdn.equals(myPhone)) {
                tag.setContactName(mApplicationController.getReengAccountBusiness().getUserName());
            } else {
                PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(msisdn);
                if (phoneNumber != null) {
                    tag.setContactName(phoneNumber.getName());
                } else {
                    if (!TextUtils.isEmpty(tag.getName())) {
                        tag.setContactName(tag.getName());
                    } else if (!TextUtils.isEmpty(tag.getMsisdn())) {
                        tag.setContactName(Utilities.hidenPhoneNumber(tag.getMsisdn()));
                    } else {
                        tag.setContactName("***");
                    }
                }
            }
        }
    }

    public static ArrayList<TagMocha> getListTagFromListPhoneNumber(ArrayList<PhoneNumber> listPhoneNumbers) {
        ArrayList<TagMocha> listTagToServer = new ArrayList<>();
        if (listPhoneNumbers != null && !listPhoneNumbers.isEmpty()) {
            for (PhoneNumber p : listPhoneNumbers) {
                String nameMocha = p.getNickName();
                if (TextUtils.isEmpty(nameMocha)) {
                    nameMocha = Utilities.hidenPhoneNumber(p.getJidNumber());
                }
                if (!TextUtils.isEmpty(nameMocha) && !TextUtils.isEmpty(p.getJidNumber())) {
                    TagMocha myTag = new TagMocha(TagsCompletionView.PREFIX_TAG + nameMocha,
                            nameMocha, p.getJidNumber());
                    myTag.setContactName(p.getName());
                    listTagToServer.add(myTag);
                }
            }
        }
        return listTagToServer;
    }

    public static String getTextTag(ArrayList<TagMocha> listTagToServer) {
        try {
            if (!listTagToServer.isEmpty()) {
                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(listTagToServer).getAsJsonArray();
                Log.i(TAG, "jsonarray: " + myCustomArray.toString());
                JsonObject jsonObject = new JsonObject();
                jsonObject.add("tags", myCustomArray);
                Log.i(TAG, "jsonobject: " + jsonObject.toString());
                return jsonObject.toString();
            } else {
                return "";
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
            return "";
        }
    }

    public static String getTextTagCopy(String text, ArrayList<TagMocha> listTag) {
        if (TextUtils.isEmpty(text) || (listTag != null && listTag.isEmpty())) {
            return text;
        }
        String textCopy = text;
        for (TagMocha tag : listTag) {
            textCopy = text.replaceFirst(Pattern.quote(tag.getTagName()), tag.getContactName());
        }
        return textCopy;
    }

    public void addFeedToHashMap(FeedModelOnMedia feed) {
        hashMapFeedTmp.remove(feed.getFeedContent().getUrl());
        hashMapFeedTmp.put(feed.getFeedContent().getUrl(), feed);
    }

    public FeedModelOnMedia getFeedFromHashMap(String url) {
        return hashMapFeedTmp.get(url);
    }

    public void clearHashMapFeed() {
        hashMapFeedTmp.clear();
    }

    public String generateUrlSocial() {
        String planUrl = "social_" + mApplicationController.getReengAccountBusiness().getJidNumber() + "_" + System
                .currentTimeMillis();
        return EncryptUtil.encryptMD5(planUrl);
    }

    public FeedModelOnMedia convertSieuHaiToFeedOnMedia(SieuHaiModel sieuHaiModel) {
        if (sieuHaiModel == null) return null;
        Log.i(TAG, "sieuHaiModel: " + sieuHaiModel);
        FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
        FeedContent feedContent = new FeedContent();
        feedContent.setUrl(sieuHaiModel.getLink());
        feedContent.setItemType(FeedContent.ITEM_TYPE_VIDEO);
        feedContent.setItemSubType("");
        feedContent.setSite(Constants.SieuHai.SITE_SIEUHAI);
        feedContent.setItemName(sieuHaiModel.getTitle());
        feedContent.setImageUrl(sieuHaiModel.getImageUrl());
        feedContent.setMediaUrl(sieuHaiModel.getVideoUrl());
        feedContent.setItemId(String.valueOf(sieuHaiModel.getId()));
        feedContent.setCountView(sieuHaiModel.getCountView());

        /*try {
            String site = HttpHelper.getDomainName(sieuHaiModel.getLink());
            feedContent.setSite(site);
        } catch (Exception e) {
            Log.e(TAG, "URISyntaxException", e);
        }*/
        feedContent.setSite("sieuhai.tv");
        feedModelOnMedia.setFeedContent(feedContent);
        return feedModelOnMedia;
    }

    public ArrayList<ContentDiscovery> getListContentDiscoveries() {
        return listContentDiscoveries;
    }

    public ArrayList<Video> getListSieuHai() {
        return listSieuHai;
    }

    public ArrayList<ContentDiscovery> getListDiscoverSameType(ContentDiscovery current) {
        if (current == null || hashMapDiscoveriesType.isEmpty() ||
                !hashMapDiscoveriesType.containsKey(current.getVideoType())) {
            return new ArrayList<>();
        } else {
            return hashMapDiscoveriesType.get(current.getVideoType());
        }
    }

    /*public int getIndexCurrentPlaySieuHai() {
        return indexCurrentPlaySieuHai;
    }

    public void setIndexCurrentPlaySieuHai(int indexCurrentPlaySieuHai) {
        this.indexCurrentPlaySieuHai = indexCurrentPlaySieuHai;
    }*/

    public void initHashmapDiscover() {
        hashMapDiscoveriesType = new HashMap<>();
        if (listContentDiscoveries == null || listContentDiscoveries.isEmpty()) return;
        int size = listContentDiscoveries.size();
        for (int i = 0; i < size; i++) {
            ContentDiscovery item = listContentDiscoveries.get(i);
            item.setPosition(i);
            ArrayList<ContentDiscovery> list;
            String key = item.getVideoType();
            if (hashMapDiscoveriesType.containsKey(key)) {
                list = hashMapDiscoveriesType.get(key);
            } else {
                list = new ArrayList<>();
                hashMapDiscoveriesType.put(key, list);
            }
            list.add(item);
        }
    }

    public boolean checkFeedToShowMenuCopy(FeedModelOnMedia feed) {
        if (feed == null) return false;
        FeedContent feedContent = feed.getFeedContent();
        if (feedContent == null) return false;
        String itemType = feedContent.getItemType();
        String itemSubType = feedContent.getItemSubType();
        return OnMediaHelper.isFeedViewAudio(feedContent)
                || FeedContent.ITEM_TYPE_NEWS.equals(itemType)
                || FeedContent.ITEM_TYPE_VIDEO.equals(itemType)
                || FeedContent.ITEM_TYPE_AUDIO.equals(itemType)
                || FeedContent.ITEM_TYPE_IMAGE.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) &&
                FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType));
    }

    public boolean checkSpamComment(String content, ArrayList<FeedAction> datas, String myPhoneNumber) {
        if (datas.size() < 1) return false;
        FeedAction firstComment = datas.get(0);
        String firstMsisdn = firstComment.getUserInfo().getMsisdn();
        return myPhoneNumber.equals(firstMsisdn) && firstComment.getComment().equals(content);
    }

    public boolean checkSpamReplyComment(String content, ArrayList<FeedAction> datas, String myPhoneNumber) {
        if (datas.size() < 2) return false;
        FeedAction firstComment = datas.get(1);
        String firstMsisdn = firstComment.getUserInfo().getMsisdn();
        return myPhoneNumber.equals(firstMsisdn) && firstComment.getComment().equals(content);
    }

    public interface FeedBusinessInterface {
        void onUpdateNotifyFeed(int totalNotify);
    }

}
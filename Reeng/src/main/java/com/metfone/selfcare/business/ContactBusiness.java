package com.metfone.selfcare.business;

import android.Manifest;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Process;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.text.TextUtils;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.datasource.ContactDataSource;
import com.metfone.selfcare.database.datasource.NonContactDataSource;
import com.metfone.selfcare.database.datasource.NumberDataSource;
import com.metfone.selfcare.database.model.Contact;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.SectionCharecter;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.GroupAvatarHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.notification.ReengNotificationManager;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.util.contactintergation.ContactsSyncAdapter;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQInfo;
import org.jivesoftware.smack.packet.Member;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;

/**
 * Created by toanvk2 on 6/26/14.
 */
public class ContactBusiness {
    //http://stackoverflow.com/questions/31319918/how-to-add-my-apps-connection-in-phonebook-contacts-as-whatsapp-and
    // -viber-does
    private static final String TAG = ContactBusiness.class.getSimpleName();
    private ArrayList<Contact> mListContactDB = new ArrayList<>();
    private ArrayList<Contact> mListContacts;
    private ArrayList<PhoneNumber> mListNumbers;
    private ArrayList<PhoneNumber> mListNumberAlls;
    private ArrayList<PhoneNumber> mListNumberFavorites;
    private ArrayList<PhoneNumber> mListNumberSupport;
    private ArrayList<PhoneNumber> mListNumberViettel;
    private ArrayList<PhoneNumber> mListNumberUseMocha = new ArrayList<>();
    private HashMap<String, ArrayList<PhoneNumber>> mHashmapPhone;
    private HashMap<String, PhoneNumber> mHashMap = new HashMap<>();
    private boolean isContactReady = false;
    // char section
    private ArrayList<SectionCharecter> mListSectionCharAlls;
    private Context mContext;
    private ApplicationController mApplication;
    private SharedPreferences mPref;
    private ReengAccountBusiness mAccountBusiness;
    private ContactDataSource mContactDataSource;
    private NumberDataSource mNumberDataSource;
    private boolean insertContact = false;
    private boolean syncContact = false;
    private boolean isArrayReady = false;
    private boolean isNewInsertDB = false;
    private boolean isInitArrayList = false;
    private int sizeContactSupport = -1;
    //non contact
    private NonContactDataSource mNonContactDataSource;
    private List<NonContact> mListNonContacts = new ArrayList<>();
    private PhoneNumberUtil mPhoneUtil;
    private PhoneNumber mPhoneNumber;

    public ContactBusiness(ApplicationController appication) {
        mApplication = appication;
        this.mContext = appication;
    }

    public void init() {
        mPhoneUtil = mApplication.getPhoneUtil();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mContactDataSource = ContactDataSource.getInstance(mApplication);
        mNumberDataSource = NumberDataSource.getInstance(mApplication);
        mNonContactDataSource = NonContactDataSource.getInstance(mApplication);
        mPref = mContext.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        // load noncontact tu db
        //initNonContact();
    }

    public ArrayList<PhoneNumber> getListNumberAlls() {
        synchronized (TAG) {
            return mListNumberAlls;
        }
    }

    public void setListNumberAll(ArrayList<PhoneNumber> list) {
        synchronized (TAG) {
            mListNumberAlls = list;
        }
    }

    public ArrayList<SectionCharecter> getListSectionCharAlls() {
        return mListSectionCharAlls;
    }

    public ArrayList<PhoneNumber> getListNumberFavorites() {
        return mListNumberFavorites;
    }

    public ArrayList<Contact> getListContacs() {
        return mListContacts;
    }

    public ArrayList<PhoneNumber> getListNumbers() {
        return mListNumbers;
    }

    public void setListContacs(ArrayList<Contact> listContact) {
        this.mListContacts = listContact;
    }

    public void setListNumbers(ArrayList<PhoneNumber> listNumbers) {
        this.mListNumbers = listNumbers;
    }

    public ArrayList<PhoneNumber> getListNumberSupport() {
        return mListNumberSupport;
    }

    public ArrayList<PhoneNumber> getListNumberViettel() {
        return mListNumberViettel;
    }

    public ArrayList<PhoneNumber> getListNumberUseMocha() {
        return mListNumberUseMocha;
    }

    public void setListNumberUseMocha(ArrayList<PhoneNumber> mListNumberUseMocha) {
        this.mListNumberUseMocha = mListNumberUseMocha;
    }

    private void setHashmapPhone(ArrayList<PhoneNumber> listPhoneNumbers, boolean isRegionSmsPrefixChange) {
        if (listPhoneNumbers == null) {
            this.mHashmapPhone = null;
        } else {
            HashMap<String, ArrayList<PhoneNumber>> hashmapPhone = new HashMap<>();
            for (PhoneNumber phone : listPhoneNumbers) {
                /*if (isRegionSmsPrefixChange) {
                    phone.setIsViettel(); // khoi tao lai
                }*/
                String key = phone.getJidNumber();
                if (!TextUtils.isEmpty(key)) {
                    ArrayList<PhoneNumber> list;
                    if (hashmapPhone.containsKey(key)) {
                        list = hashmapPhone.get(key);
                    } else {
                        list = new ArrayList<>();
                        hashmapPhone.put(key, list);
                    }
                    list.add(phone);
                }
            }
            this.mHashmapPhone = hashmapPhone;
        }
    }

    public boolean isInsertContact() {
        return insertContact;
    }

    public void setInsertContact(boolean insert) {
        this.insertContact = insert;
    }

    public boolean isSyncContact() {
        return syncContact;
    }

    public void setSyncContact(boolean isSync) {
        this.syncContact = isSync;
    }

    public boolean isNewInsertDB() {
        return isNewInsertDB;
    }

    public void setNewInsertDB(boolean isNewInsertDB) {
        this.isNewInsertDB = isNewInsertDB;
    }

    public int getSizeContactSupport() {
        return sizeContactSupport;
    }

    // close cursor
    private void closeCursor(Cursor cur) {
        if (cur != null) {
            cur.close();
        }
    }

    // get contact from cursor
    private Contact getContactFromCursor(Cursor cursor) {
        Contact contact = new Contact();
        String id = cursor.getString(cursor
                .getColumnIndex(ContactsContract.Contacts._ID));
        contact.setContactID(id);
        String name = cursor
                .getString(cursor
                        .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        contact.setName(name);
        int star = cursor.getInt(cursor
                .getColumnIndex(ContactsContract.Contacts.STARRED));
        contact.setFavorite(star);
        return contact;
    }

    // get all contact from phone
    public ArrayList<Contact> getListContactFromDevice() {
        ArrayList<Contact> listContacts = new ArrayList<>();
        Cursor cur = null;
        try {
            String[] projection = {ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.STARRED,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER};
            ContentResolver cr = mContext.getContentResolver();
            cur = cr.query(ContactsContract.Contacts.CONTENT_URI, projection,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER + " = 1",
                    null, null);
            if (cur != null && cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    Contact contact = getContactFromCursor(cur);
                    listContacts.add(contact);
                }
            }
        } catch (SecurityException e) {
            Log.e(TAG, "getListContactFromPhone", e);
        } catch (Exception e) {
            Log.e(TAG, "getListContactFromPhone", e);
        } finally {
            closeCursor(cur);
        }
        return listContacts;
    }

    // get Phone number from cursor
    private PhoneNumber getNumberFromCursor(Cursor cur, String regionCode) {
        PhoneNumber pNumber = new PhoneNumber();
        String nb = cur.getString(cur
                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        if (TextUtils.isEmpty(nb)) {// so rong
            return null;
        }
        Phonenumber.PhoneNumber phoneProtocol = PhoneNumberHelper.
                getInstant().getPhoneNumberProtocol(mPhoneUtil, nb, regionCode);
        if (PhoneNumberHelper.getInstant().isValidPhoneNumber(mPhoneUtil, phoneProtocol)) {// so hop le
            // lay jid
            String jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    mPhoneUtil.format(phoneProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
            pNumber.setJidNumber(jidNumber);
            // lay raw number
            /*String rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mPhoneUtil, phoneProtocol, regionCode);
            pNumber.setRawNumber(rawNumber);*/
            String id = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
            pNumber.setId(id);
            String contactId = cur.getString(cur
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            pNumber.setContactId(contactId);
            String name = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            /* if (TextUtils.isEmpty(name)) {
                name = rawNumber;
            }*/
            pNumber.setPhoneProtocol(phoneProtocol);
            pNumber.setName(name);
            pNumber.setFavorite(cur.getInt(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED)));
            return pNumber;
        } else {
            Log.i(TAG, "phone invalid: " + nb);
        }
        return null;
    }

    //get list phone number from cur
    public ArrayList<PhoneNumber> getListNumberFromCursor(Cursor cursor) {
        String regionCode = mAccountBusiness.getRegionCode();
        ArrayList<PhoneNumber> listNumbers = new ArrayList<>();
        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    // get number
                    PhoneNumber number = getNumberFromCursor(cursor, regionCode);
                    if (number != null) {
                        listNumbers.add(number);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getListNumberFromCursor", e);
        } finally {
            closeCursor(cursor);
        }
        return listNumbers;
    }

    private PhoneNumber getNumberFromCursor(Cursor cur, String regionCode, int phoneNumberIndex, int phoneIdIndex,
                                            int contactIdIndex, int displayNameIndex, int starredIndex) {
        PhoneNumber pNumber = new PhoneNumber();
        String nb = cur.getString(phoneNumberIndex);
        if (TextUtils.isEmpty(nb)) {// so rong
            return null;
        }
        Phonenumber.PhoneNumber phoneProtocol = PhoneNumberHelper.
                getInstant().getPhoneNumberProtocol(mPhoneUtil, nb, regionCode);
        if (PhoneNumberHelper.getInstant().isValidPhoneNumber(mPhoneUtil, phoneProtocol)) {// so hop le
            pNumber.setJidNumber(PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    mPhoneUtil.format(phoneProtocol, PhoneNumberUtil.PhoneNumberFormat.E164)));
            pNumber.setId(cur.getString(phoneIdIndex));
            pNumber.setContactId(cur.getString(contactIdIndex));
            pNumber.setPhoneProtocol(phoneProtocol);
            pNumber.setName(cur.getString(displayNameIndex));
            pNumber.setFavorite(cur.getInt(starredIndex));
            return pNumber;
        }
        return null;
    }

    public ArrayList<PhoneNumber> getListPhoneNumberFormListNumber(ArrayList<String> listNumber) {
        ArrayList<PhoneNumber> listPhoneNumber = new ArrayList<>();
        ArrayList<String> listDefaultNumber = new ArrayList<>();
        for (String number : listNumber) {
            PhoneNumber phoneNumber = getPhoneNumberFromNumber(number);
            if (phoneNumber != null) {
                /*boolean isAdd = true;
                for (PhoneNumber phoneNumber1 : listPhoneNumber) {//check neu da get contact roi thi bo qua
                    if (phoneNumber1.getContactId().equals(phoneNumber.getContactId())) {
                        isAdd = false;
                        break;
                    }
                }
                if (isAdd) {*/
                listPhoneNumber.add(phoneNumber);
                //}
            } else {
                StrangerPhoneNumber strangerNumber = mApplication.
                        getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(number);
                if (strangerNumber != null) {// stranger contact
                    PhoneNumber phone = new PhoneNumber();
                    phone.setJidNumber(number);
                    String rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mApplication, number);
                    phone.setRawNumber(rawNumber);
                    phone.setName(strangerNumber.getFriendName());
                    phone.setNameUnicode(TextHelper.getInstant().convertUnicodeToAscci(strangerNumber.getFriendName()));
                    phone.setContactId("-1");
                    phone.setId(null);
                    listPhoneNumber.add(phone);
                } else { //add new contact
                    listDefaultNumber.add(number);
                }
            }
        }
        if (listDefaultNumber.size() > 0) {
            listPhoneNumber.addAll(createListPhoneNumberDefault(listDefaultNumber));
        }
        return listPhoneNumber;
    }

    // get all phoneNumber in phone
    public ArrayList<PhoneNumber> getListNumberFromDevice() {
        ArrayList<PhoneNumber> listNumbers = new ArrayList<>();
        Cursor cur = null;
        try {
            ContentResolver cr = mContext.getContentResolver();
            String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone._ID,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.STARRED};
            cur = cr.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    projection, null, null, null);
            if (cur != null) {
                //get column indexes only once, just before the while cycle
                final int phoneNumberIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                final int phoneIdIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID);
                final int contactIdIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
                final int displayNameIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                final int starredIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED);
                while (cur.moveToNext()) {
                    PhoneNumber number = getNumberFromCursor(cur, mAccountBusiness.getRegionCode(),
                            phoneNumberIndex, phoneIdIndex, contactIdIndex, displayNameIndex, starredIndex);// get number
                    if (number != null) {
                        listNumbers.add(number);
                    }
                }
            }
        } catch (SecurityException e) {
            Log.e(TAG, "SecurityException", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cur);
        }
        return listNumbers;
    }

    public boolean isContactReady() {
        if (mListContacts == null || mListNumbers == null) {
            return false;
        } else // chua init contact xong
            return isContactReady;
    }

    // hashmap contact by id
    private HashMap<String, Contact> getHashMapContactById(ArrayList<Contact> listContacts) {
        HashMap<String, Contact> hashMapContact = new HashMap<>();
        // put new hashmap
        for (Contact contact : listContacts) {
            hashMapContact.put(contact.getContactId(), contact);
        }
        return hashMapContact;
    }

    private HashMap<String, PhoneNumber> getHashMapNumberById(ArrayList<PhoneNumber> listNumbers) {
        HashMap<String, PhoneNumber> hashMapNumber = new HashMap<>();
        for (PhoneNumber number : listNumbers) {
            hashMapNumber.put(number.getId(), number);
        }
        return hashMapNumber;
    }

    // hashmap number by contactId
    private HashMap<String, ArrayList<PhoneNumber>> getHashMapNumberByContactId(ArrayList<PhoneNumber> listNumbers) {
        HashMap<String, ArrayList<PhoneNumber>> hashMapNumber = new HashMap<>();
        // new hashmap phonenumber;
        for (PhoneNumber number : listNumbers) {
            ArrayList<PhoneNumber> list;
            if (hashMapNumber.containsKey(number.getContactId())) {
                list = hashMapNumber.get(number.getContactId());
            } else {
                list = new ArrayList<>();
                hashMapNumber.put(number.getContactId(), list);
            }
            list.add(number);
        }
        return hashMapNumber;
    }

    // merge danh sach contact voi danh sach so dien thoai
    private ArrayList<Contact> mergeContact(ArrayList<Contact> listContacts, ArrayList<PhoneNumber> listNumbers) {
        ArrayList<Contact> contacts = new ArrayList<>();
        // hashmap
        HashMap<String, Contact> hashMapContact = getHashMapContactById(listContacts);
        HashMap<String, ArrayList<PhoneNumber>> hashMapNumber = getHashMapNumberByContactId(listNumbers);
        // new array list
        for (Map.Entry<String, ArrayList<PhoneNumber>> entry : hashMapNumber
                .entrySet()) {
            String key = entry.getKey();
            if (hashMapContact.containsKey(key)) {
                Contact contact = hashMapContact.get(key);
                contact.setListNumbers(entry.getValue());
                contacts.add(contact);
            }
        }
        return contacts;
    }

    // sort number anphaB by nameContact
    public ArrayList<PhoneNumber> sortNumberByName(ArrayList<PhoneNumber> listNumbers) {
        ArrayList<PhoneNumber> numbers = new ArrayList<>();
        if (listNumbers == null || listNumbers.size() <= 0) {
            return numbers;
        }
        try {
            int sizeList = listNumbers.size();
            char ch;
            ch = listNumbers.get(0).getNameUnicode().charAt(0);
            if (ch >= '!' && ch < 'A' || ch > 'Z' && ch < 'a' || ch > 'z'
                    && ch <= '~') {
                ch = '#';
            }
            PhoneNumber index0 = new PhoneNumber(null, String.valueOf(ch).toUpperCase(), -2);
            numbers.add(index0); //TODO tam thời home contact có section "danh bạ" nên không add thêm section đầu nữa
            for (int i = 0; i < sizeList; i++) {
                PhoneNumber number = listNumbers.get(i);
                String name = number.getNameUnicode();
                if (name != null && name.length() > 0) {
                    char chAt = name.charAt(0);
                    if (chAt >= '!' && chAt < 'A' || chAt > 'Z'
                            && chAt < 'a' || chAt > 'z' && chAt <= '~') {
                        ch = '#';
                    } else if (chAt != ch && i > 0) {
                        ch = name.charAt(0);
                        PhoneNumber nb = new PhoneNumber(null, String.valueOf(ch).toUpperCase(), -2);
                        numbers.add(nb);
                    }
                }
                numbers.add(number);
            }
            return numbers;
        } catch (Exception e) {
            Log.e(TAG, "sortNumberByName", e);
            return numbers;
        }
    }

    public ArrayList<PhoneNumber> contactSearchList(String contentSearch, ArrayList<PhoneNumber> listPhoneNumbers) {
        contentSearch = TextHelper.getInstant().convertUnicodeToAscci(contentSearch.trim());
        if (contentSearch == null || contentSearch.length() <= 0) {
            return listPhoneNumbers;
        }
        String[] listTexts = contentSearch.split("\\s+");
        ArrayList<PhoneNumber> list;
        ArrayList<PhoneNumber> listSearch = new ArrayList<>(listPhoneNumbers);
        for (String item : listTexts) {
            if (item.trim().length() > 0) {
                list = new ArrayList<>();
                for (PhoneNumber phone : listSearch) {
                    if (phone.getContactId() != null) {
                        String name = phone.getNameUnicode();
                        String numberJid = phone.getJidNumber();
                        String rawNumber;
                        if (numberJid.startsWith("0")) {// so vn
                            rawNumber = "+84" + numberJid.substring(1);
                        } else {
                            rawNumber = phone.getRawNumber();
                        }
                        if (name.contains(item) || numberJid.contains(item) ||
                                (rawNumber != null && rawNumber.contains(item))) {
                            list.add(phone);
                        }
                    }
                }
                listSearch = list;
            }
        }
        if (!listSearch.isEmpty()) {
            ArrayList<PhoneNumber> others = new ArrayList<>();
            list = new ArrayList<>();
            for (PhoneNumber item : listSearch) {
                if (item.getNameUnicode() != null && item.getNameUnicode().contains(contentSearch)) {
                    list.add(item);
                } else {
                    others.add(item);
                }
            }
            listSearch = list;
            listSearch.addAll(others);
        }
        return listSearch;
    }

    public PhoneNumber getPhoneNumberFromNumber(String number) {
        if (mHashMap == null || mHashMap.isEmpty() || TextUtils.isEmpty(number)) {
            return null;
        }
        return mHashMap.get(number);
    }

    public ArrayList<SectionCharecter> getListSectionCharByListPhone(ArrayList<PhoneNumber> listNumbers) {
        ArrayList<SectionCharecter> sections = new ArrayList<>();
        if (listNumbers == null || listNumbers.isEmpty()) {
            return sections;
        }
        int size = listNumbers.size();
        for (int i = 0; i < size; i++) {
            if (listNumbers.get(i).getId() == null) {
                SectionCharecter section = new SectionCharecter();
                section.setCharAt(listNumbers.get(i).getName());
                section.setPosition(i);
                sections.add(section);
            }
        }
        return sections;
    }

    public ArrayList<PhoneNumber> getListPhoneNumberFromNumber(String number) {
        if (number == null || number.length() <= 0) {
            return null;
        }
        if (mListNumbers == null || mListNumbers.isEmpty()) {
            return null;
        }
        if (mHashmapPhone == null || mHashmapPhone.isEmpty()) {
            setHashmapPhone(mListNumbers, false);
        }
        return mHashmapPhone.get(number);
    }

    //get phone number obj from phonenumber id
    public PhoneNumber getPhoneNumberFromPhoneId(String phoneId) {
        if (phoneId == null || phoneId.length() <= 0) {
            return null;
        }
        ArrayList<PhoneNumber> phones = getListNumbers();
        if (phones == null || phones.isEmpty()) {
            return null;
        }
        for (PhoneNumber phone : phones) {
            if (phone.getId().equals(phoneId)) {
                return phone;
            }
        }
        return null;
    }

    //get contact obj from contact id
    public Contact getContactFromContactId(String contactId) {
        if (contactId == null || contactId.length() <= 0) {
            return null;
        }
        ArrayList<Contact> contacts = getListContacs();
        if (contacts == null || contacts.isEmpty()) {
            return null;
        }
        for (Contact contact : contacts) {
            if (contact.getContactId().equals(contactId)) {
                return contact;
            }
        }
        return null;
    }

    public Contact getContactFromIntentData(Intent data) {
        Contact contact = new Contact();
        Cursor curContact = null;
        Cursor curNumber = null;
        try {
            Uri contactUri = data.getData();
            String[] projectionContact = {ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.STARRED};
            String[] projectionNumber = {ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone._ID,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.STARRED};
            ContentResolver cr = mContext.getContentResolver();
            curContact = cr.query(contactUri,
                    projectionContact, null, null, null);
            if (curContact != null && curContact.getCount() > 0) {
                curContact.moveToFirst();
                contact = getContactFromCursor(curContact);
                String contactID = contact.getContactId();
                curNumber = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        projectionNumber,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{contactID}, null);
                ArrayList<PhoneNumber> numbers = getListNumberFromCursor(curNumber);
                contact.setListNumbers(numbers);
                closeCursor(curNumber);
            }
        } catch (SecurityException e) {
            Log.e(TAG, "SecurityException", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(curContact);
            closeCursor(curNumber);
        }
        return contact;
    }

    public ArrayList<PhoneNumber> createListPhoneNumberDefault(ArrayList<String> listString) {
        if (listString == null || listString.isEmpty()) {
            return null;
        } else {
            ArrayList<PhoneNumber> listNumbers = new ArrayList<>();
            for (String number : listString) {
                PhoneNumber phone = new PhoneNumber();
                phone.setJidNumber(number);
                String rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mApplication, number);
                phone.setRawNumber(rawNumber);
                phone.setName(rawNumber);
                phone.setNameUnicode(rawNumber);
                phone.setContactId("-1");
                phone.setId(null);
                listNumbers.add(phone);
            }
            return listNumbers;
        }
    }

    public synchronized Contact insertNewContactLastId() {
        Cursor cursor = null;
        try {
            setInsertContact(true);
            Contact contact = new Contact();
            String[] projectionContact = {ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.STARRED};
            String[] projectionNumber = {ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone._ID,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.STARRED};
            ContentResolver cr = mContext.getContentResolver();
            cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    projectionContact, null, null, ContactsContract.Contacts._ID);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToLast();
                contact = getContactFromCursor(cursor);
                String contactID = contact.getContactId();
                Cursor curNumber = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        projectionNumber,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{contactID}, null);
                ArrayList<PhoneNumber> numbers = getListNumberFromCursor(curNumber);
                // danh ba khong chua so dien thoai
                if (numbers == null || numbers.isEmpty()) {
                    closeCursor(cursor);
                    setInsertContact(false);
                    return null;
                }
                Contact dbContact = mContactDataSource.getContactFromId(contactID);
                // danh ba da ton tai trong CSDL, khong them danh ba moi
                if (dbContact != null) {
                    closeCursor(cursor);
                    setInsertContact(false);
                    return null;
                }
                // danh ba hop le
                contact.setListNumbers(numbers);
                getListContacs().add(contact);
                getListNumbers().addAll(numbers);
                ArrayList<PhoneNumber> listAddSV = new ArrayList<>();
                for (PhoneNumber itemNew : numbers) {
                    if (!mHashMap.containsKey(itemNew.getJidNumber())) {
                        NonContact oldNonContact = getExistNonContact(itemNew.getJidNumber());
                        if (oldNonContact != null) {
                            itemNew.setLastSeen(oldNonContact.getLastSeen());
                        }
                        listAddSV.add(itemNew);
                    } else {
                        PhoneNumber oldPhone = mHashMap.get(itemNew.getJidNumber());
                        updateValueNewPhoneFromOldPhone(itemNew, oldPhone);
                    }
                }
                updateRawNumberAndNameUnicode(numbers, mAccountBusiness.getRegionCode());
                for (PhoneNumber itemNew : numbers) {
                    mHashMap.put(itemNew.getJidNumber(), itemNew);
                }
                initArrayListPhoneNumber();
                // roster
                updateListInfoToServer(listAddSV, null);
            }
            setInsertContact(false);
            return contact;
        } catch (SecurityException e) {
            Log.e(TAG, "SecurityException", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            closeCursor(cursor);
        }
        return null;
    }

    public void insertContactToDB(Contact contact, ArrayList<PhoneNumber> numbers) {
        String regionCode = mAccountBusiness.getRegionCode();
        mContactDataSource.insertContact(contact);
        mNumberDataSource.insertListPhoneNumber(numbers, mPhoneUtil, regionCode);
    }

    public void updateRawNumberAndNameUnicode(ArrayList<PhoneNumber> list, String regionCode) {
        for (PhoneNumber number : list) {
            // lay raw number from phone protocol (insert vao db thi moi goi doan nay
            String rawNumber;
            Phonenumber.PhoneNumber phoneProtocol = number.getPhoneProtocol();
            if (phoneProtocol != null) {
                rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mPhoneUtil, phoneProtocol, regionCode);
            } else {
                rawNumber = number.getJidNumber();
            }
            number.setRawNumber(rawNumber);
            if (TextUtils.isEmpty(number.getName())) {
                number.setName(rawNumber);
            }
            number.setNameUnicode(TextHelper.getInstant().convertUnicodeToAscci(number.getName()));
        }
    }

    public Contact updateContactFromIntentData(Intent data, Contact oldContact) {
        if (oldContact == null) {
            return null;
        }
        ArrayList<PhoneNumber> oldNumbers = oldContact.getListNumbers();
        Contact contact = getContactFromIntentData(data);
        ArrayList<PhoneNumber> numbers = contact.getListNumbers();
        if (numbers == null || numbers.isEmpty()) {
            //xoa contact cu di
            deleteContact(oldContact.getContactId());
            oldContact = null;
        } else {
            ArrayList<PhoneNumber> newListNumbers = updateValueNewListFromOldList(oldNumbers, numbers);
            ArrayList<PhoneNumber> listDelete = new ArrayList<>(oldNumbers);
            listDelete.removeAll(numbers);
            ArrayList<PhoneNumber> listInsert = new ArrayList<>(numbers);
            listInsert.removeAll(oldNumbers);
            updateValueOldContactFromNewContact(oldContact, contact);
            updateRawNumberAndNameUnicode(newListNumbers, mAccountBusiness.getRegionCode());
            UpdateContactDBAsyncTask updateDatabase = new UpdateContactDBAsyncTask(oldContact, oldNumbers,
                    newListNumbers);
            updateDatabase.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            // list add
            ArrayList<PhoneNumber> listAddSV = new ArrayList<>();
            for (PhoneNumber itemNew : listInsert) {
                if (!mHashMap.containsKey(itemNew.getJidNumber())) {
                    listAddSV.add(itemNew);
                } else {
                    PhoneNumber oldPhone = mHashMap.get(itemNew.getJidNumber());
                    updateValueNewPhoneFromOldPhone(itemNew, oldPhone);
                }
            }
            // cap nhat obj
            removeListPhoneNumberByContactId(oldContact.getContactId(), getListNumbers());
            oldContact.setListNumbers(newListNumbers);
            getListNumbers().addAll(newListNumbers);
            if (!newListNumbers.isEmpty()) {
                for (PhoneNumber itemNew : newListNumbers) {
                    mHashMap.put(itemNew.getJidNumber(), itemNew);
                }
            }
            // roster
            ArrayList<PhoneNumber> listRemoveSV = new ArrayList<>();
            for (PhoneNumber itemRemove : listDelete) {
                if (!mHashMap.containsKey(itemRemove.getJidNumber())) {
                    listRemoveSV.add(itemRemove);
                }
            }
            updateListInfoToServer(listAddSV, listRemoveSV);
        }
        return oldContact;
    }

    public void updateContactEdit(PhoneNumber phoneNumber) {
        if (phoneNumber != null && Utilities.notEmpty(phoneNumber.getJidNumber())) {
            PhoneNumber temp = mHashMap.get(phoneNumber.getJidNumber());
            if (temp != null) {
                temp.setPreKey(phoneNumber.getPreKey());
                mHashMap.put(temp.getJidNumber(), temp);
            }
        }
    }

    private class UpdateContactDBAsyncTask extends AsyncTask<Void, Void, Void> {
        private Contact mOldContact;
        private ArrayList<PhoneNumber> mOldList;
        private ArrayList<PhoneNumber> mNewList;

        public UpdateContactDBAsyncTask(Contact oldContact, ArrayList<PhoneNumber> oldList, ArrayList<PhoneNumber>
                newList) {
            this.mOldContact = oldContact;
            this.mOldList = oldList;
            this.mNewList = newList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // cap nhat DB
            String regionCode = mAccountBusiness.getRegionCode();
            mNumberDataSource.deleteListPhoneNumber(mOldList);
            mNumberDataSource.insertListPhoneNumber(mNewList, mPhoneUtil, regionCode);
            mContactDataSource.updateContact(mOldContact);
            return null;
        }
    }

    // delcontact
    private void deleteContact(String contactId) {
        ArrayList<PhoneNumber> numbers = getListPhoneNumberByContactId(contactId, getListNumbers());
        Contact contact = getContactFromContactId(contactId);
        //xoa roster, khong con ton tai tren may moi xoa
        removeListPhoneNumberByContactId(contactId, getListNumbers());
        ArrayList<PhoneNumber> listRemoveSv = new ArrayList<>();
        if (numbers != null) {
            for (PhoneNumber itemRemove : numbers) {
                if (!mHashMap.containsKey(itemRemove.getJidNumber())) {
                    listRemoveSv.add(itemRemove);
                }
            }
        }
        updateListInfoToServer(null, listRemoveSv);
        //xo obj
        getListContacs().remove(contact);
        DeleteContactDBAsyncTask deleteDatabase = new DeleteContactDBAsyncTask(contactId);
        deleteDatabase.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class DeleteContactDBAsyncTask extends AsyncTask<Void, Void, Void> {
        private String contactId;

        public DeleteContactDBAsyncTask(String contactId) {
            this.contactId = contactId;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //xoa DB
            if (!TextUtils.isEmpty(contactId)) {
                mNumberDataSource.deleteListPhoneNumberByContactId(contactId);
                mContactDataSource.deleteContact(contactId);
            }
            return null;
        }
    }

    private void updateValueOldContactFromNewContact(Contact oldContact, Contact newContact) {
        oldContact.setName(newContact.getName());
        oldContact.setFavorite(newContact.getFavorite());
    }

    private void updateValueNewPhoneFromOldPhone(PhoneNumber newPhone, PhoneNumber oldPhone) {
        newPhone.setStatus(oldPhone.getStatus());
        newPhone.setLastChangeAvatar(oldPhone.getLastChangeAvatar());
        newPhone.setLastOnline(oldPhone.getLastOnline());
        newPhone.setState(oldPhone.getState());
        newPhone.setOnline(oldPhone.isOnline());
        newPhone.setLastSeen(oldPhone.getLastSeen());
        newPhone.setNewRegister(oldPhone.isNewRegister());
        newPhone.setAddRoster(oldPhone.getAddRoster());
        newPhone.setGender(oldPhone.getGender());
//        newPhone.setBirthDay(oldPhone.getBirthDay());
        newPhone.setRawNumber(oldPhone.getRawNumber());
        // gia tri moi lay tu new item
        newPhone.setAvatarName(newPhone.getName());
        newPhone.setNameUnicode(TextHelper.getInstant().convertUnicodeToAscci(newPhone.getName()));
        newPhone.setBirthdayString(oldPhone.getBirthdayString());
        newPhone.setCoverUrl(oldPhone.getCoverUrl());
        newPhone.setCoverId(oldPhone.getCoverId());
        newPhone.setAlbumString(oldPhone.getAlbumString());
        newPhone.setPermission(oldPhone.getPermission());
        newPhone.setNickName(oldPhone.getNickName());
        newPhone.setOperator(oldPhone.getOperator());
        newPhone.setPreKey(oldPhone.getPreKey());
    }

    //update value old list to new list
    private ArrayList<PhoneNumber> updateValueNewListFromOldList(ArrayList<PhoneNumber> oldList,
                                                                 ArrayList<PhoneNumber> newList) {
        ArrayList<PhoneNumber> numbers = new ArrayList<>();
        if (oldList == null || oldList.isEmpty() || newList == null || newList.isEmpty()) {
            return numbers;
        }
        HashMap<String, PhoneNumber> hashMapOldList = new HashMap<>();
        for (PhoneNumber phone : oldList) {
            hashMapOldList.put(phone.getJidNumber(), phone);
        }
        //
        HashMap<String, PhoneNumber> hashMapNewList = new HashMap<>();
        for (PhoneNumber phone : newList) {
            hashMapNewList.put(phone.getJidNumber(), phone);
        }
        //
        for (Map.Entry<String, PhoneNumber> entry : hashMapNewList
                .entrySet()) {
            String key = entry.getKey();
            PhoneNumber newValue = entry.getValue();
            if (hashMapOldList.containsKey(key)) {
                PhoneNumber oldValue = hashMapOldList.get(key);
                updateValueNewPhoneFromOldPhone(newValue, oldValue);
            }
            numbers.add(newValue);
        }
        return numbers;
    }

    public void updateE2EContact(String jidNumber, String preKey) {
        String number = jidNumber.split("@")[0].trim();
        if (TextUtils.isEmpty(number)) {
            return;
        }
        PhoneNumber phoneNumber = getPhoneNumberFromNumber(number);
        if (phoneNumber != null) {
            phoneNumber.setPreKey(preKey);

            /*//Update mem
            PhoneNumber temp = mHashMap.get(phoneNumber.getJidNumber());
            temp.setPreKey(phoneNumber.getPreKey());
            mHashMap.put(temp.getJidNumber(), temp);*/

            //Update database
            ArrayList<PhoneNumber> listUpdate = new ArrayList<>();
            listUpdate.add(phoneNumber);
            mNumberDataSource.updateListPhoneNumber(listUpdate, false);
        } else {
            NonContact nonContact = getExistNonContact(number);
            if (nonContact != null) {
                nonContact.setPreKey(preKey);
                updateNonContact(nonContact);
            }
        }
    }

    /**
     * cap nhat contacta thay doi v3,
     *
     * @param presence
     */
    public void updateContactAfterReceiverPresenceV3(Presence presence) {
        Log.d(TAG, "updateContactAfterReceiverPresenceV3");
        String contactInfo = presence.getContactInfo();
        if (TextUtils.isEmpty(contactInfo)) {
            sendIQAfterResponseContactInfo();
            return;
        }
        if (!isContactReady()) {
            return;
        }
        try {
            //get tu json
            JSONObject object = new JSONObject(contactInfo);
            if (object.has("contacts")) {
                JSONArray jsonArray = object.getJSONArray("contacts");
                ArrayList<PhoneNumber> listUpdate = new ArrayList<>();
                if (jsonArray != null && jsonArray.length() > 0) {
                    int lengthArray = jsonArray.length();
                    for (int i = 0; i < lengthArray; i++) {
                        ArrayList<PhoneNumber> listPhone = updatePhoneNumberV3(jsonArray.getJSONObject(i));
                        if (listPhone != null) {
                            listUpdate.addAll(listPhone);
                        }
                    }
                }
                if (!listUpdate.isEmpty()) {
                    ListenerHelper.getInstance().onPresenceChanged(listUpdate);
                }
                // refresh contact list
                initArrayListPhoneNumber();
                //update db
                mNumberDataSource.updateListPhoneNumber(listUpdate, false);
            }
            sendIQAfterResponseContactInfo();
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    private ArrayList<PhoneNumber> updatePhoneNumberV3(JSONObject jsonObject) throws JSONException {
        String jidNumber = null, status = null, birthdayString = null, lastChangeAvatar = null, nicName = null, operator = null, preKey = null;
        int desktop = 0;
        String cover = null, coverId = null, albums = null;
        int state = -1, gender = -1, permission = -1;
        if (jsonObject.has(Constants.HTTP.CONTACT.MSISDN)) {
            jidNumber = jsonObject.getString(Constants.HTTP.CONTACT.MSISDN);
        }
        if (jsonObject.has(Constants.HTTP.CONTACT.STATE)) {
            state = jsonObject.getInt(Constants.HTTP.CONTACT.STATE);
        }
        if (jsonObject.has(Constants.HTTP.CONTACT.STATUS)) {
            status = jsonObject.getString(Constants.HTTP.CONTACT.STATUS);
        }
        if (jsonObject.has(Constants.HTTP.CONTACT.GENDER)) {
            gender = jsonObject.getInt(Constants.HTTP.CONTACT.GENDER);
        }
        if (jsonObject.has(Constants.HTTP.CONTACT.BIRTHDAY_STRING)) {
            birthdayString = jsonObject.getString(Constants.HTTP.CONTACT.BIRTHDAY_STRING);
        }
        //avatar
        if (jsonObject.has(Constants.HTTP.CONTACT.LAST_AVATAR)) {
            String lAvatar = jsonObject.getString(Constants.HTTP.CONTACT.LAST_AVATAR);
            if (lAvatar != null && "0".equals(lAvatar)) {
                lastChangeAvatar = null;
            } else {
                lastChangeAvatar = lAvatar;
            }
        }
        if (jsonObject.has(Constants.HTTP.CONTACT.COVER_IMAGE)) {
            cover = jsonObject.getString(Constants.HTTP.CONTACT.COVER_IMAGE);
        }
        if (jsonObject.has(Constants.HTTP.CONTACT.COVER_ID)) {
            coverId = jsonObject.getString(Constants.HTTP.CONTACT.COVER_ID);
        }
        if (jsonObject.has(Constants.HTTP.CONTACT.ALBUMS)) {
            albums = jsonObject.getString(Constants.HTTP.CONTACT.ALBUMS);
        }
        if (jsonObject.has(Constants.HTTP.CONTACT.PERMISSION)) {
            permission = jsonObject.getInt(Constants.HTTP.CONTACT.PERMISSION);
        }
        if (jsonObject.has(Constants.HTTP.USER_INFOR.NAME)) {
            nicName = jsonObject.getString(Constants.HTTP.USER_INFOR.NAME);
        }

        if (jsonObject.has(Constants.HTTP.USER_INFOR.OPERATOR)) {
            operator = jsonObject.getString(Constants.HTTP.USER_INFOR.OPERATOR);
        }
        desktop = jsonObject.optInt("desktop");

        if (jsonObject.has(Constants.HTTP.USER_INFOR.PREKEY)) {
            preKey = jsonObject.getString(Constants.HTTP.USER_INFOR.PREKEY);
        }
        //update
        ArrayList<PhoneNumber> listPhones = getListPhoneNumberFromNumber(jidNumber);
        if (listPhones == null || listPhones.isEmpty()) {// khong tim thay trong danh ba
            return null;
        }
        for (PhoneNumber item : listPhones) {// kiem tra co gia tri moi set lai, vi sv ko tra full thong tin
            if (state != -1)
                item.setState(state);
            if (gender != -1) {
                item.setGender(gender);
            }
            if (!TextUtils.isEmpty(status)) {
                item.setStatus(status);
            }
            if (!TextUtils.isEmpty(lastChangeAvatar)) {
                item.setLastChangeAvatar(lastChangeAvatar);
            }
            if (!TextUtils.isEmpty(birthdayString)) {
                item.setBirthdayString(birthdayString);
            }
            if (!TextUtils.isEmpty(cover)) {
                item.setCoverUrl(cover);
            }
            if (!TextUtils.isEmpty(coverId)) {
                item.setCoverUrl(coverId);
            }
            if (!TextUtils.isEmpty(albums)) {
                item.setAlbumString(albums);
            }
            if (permission != -1) {
                item.setPermission(permission);
            }
            if (!TextUtils.isEmpty(nicName)) {
                item.setNickName(nicName);
            }
            if (!TextUtils.isEmpty(operator)) {
                item.setOperator(operator);
            }
            item.setUsingDesktop(desktop);
            if (!TextUtils.isEmpty(preKey)) {
                item.setPreKey(preKey);
            }
            Log.d(TAG, "updatePhoneNumberV3 item: " + item.toString());
        }
        return listPhones;
    }

    /**
     * cap nhat contact thay doi v1
     *
     * @param presence
     */
    public void updateContactAfterReceiverPresence(Presence presence) {
        String number = presence.getFrom();
        number = number.split("@")[0].trim();
        if (TextUtils.isEmpty(number)) {
            return;
        }
        String newUser = null;
        //update list obj
        ArrayList<PhoneNumber> listUpdate = getListPhoneNumberFromNumber(number);
        if (listUpdate == null || listUpdate.isEmpty()) {
            Log.d(TAG, "updateContactAfterReceiverPresence---> null");
            mApplication.logDebugContent("updateContactAfterReceiverPresence---> null: " + number);
            updateNonContactAfterReceivedPresence(number, presence);
        } else {
            // tim thay so trong danh ba
            boolean isStateChanged = false;
            boolean isDBChange = false;
            boolean isUpdateOprViaLastSeen = false;
            boolean isUpdatePreKey = false;
            PhoneNumber item = listUpdate.get(0);
            boolean isReeng = item.isReeng();
            Presence.Type type = presence.getType();
            Presence.SubType subType = presence.getSubType();
            if (type == Presence.Type.available) {
                if (!isReeng) {// moi dung reeng
                    item.setNewRegister(true);
                    //item.setState(Constants.CONTACT.ACTIVE);
                    isStateChanged = true;
                    newUser = number;
                }
                if (subType == Presence.SubType.change_status) {
                    item.setStatus(presence.getStatus());
                    isDBChange = true;
                } else if (subType == Presence.SubType.change_avatar) {
                    mApplication.getAvatarBusiness().clearCacheAvatar(item);// xoa cache cu
                    GroupAvatarHelper.getInstance(mApplication).changeInfoAvatarGroup(item.getJidNumber());
                    item.setLastChangeAvatar(presence.getLastAvatar());
                    isDBChange = true;
                } else if (subType == Presence.SubType.change_operator) {
                    item.setOperator(presence.getOperator());
                    item.setOperatorPresence(presence.getOperator());
                    isDBChange = true;
                } else if (subType == Presence.SubType.change_platform) {
                    item.setUsingDesktop(presence.getUsingDesktop());
                } else if (subType == Presence.SubType.remove_avatar) {
                    item.setLastChangeAvatar(null);
                    isDBChange = true;
                } else if (subType == Presence.SubType.invisible) {
                    item.setLastOnline(System.currentTimeMillis());
                    item.setOnline(false);
                } else if (subType == Presence.SubType.available ||
                        subType == Presence.SubType.client_info) {// subtype available online
                    item.setOnline(true);
                } else if (subType == Presence.SubType.new_user) {
                    item.setOnline(true);
                } else if (subType == Presence.SubType.change_permission) {
                    int permission = presence.getPermission();
                    if (permission != -1) {
                        item.setPermission(permission);
                    }
                } else if (subType == Presence.SubType.background) {
                    // thoi gian lech ko qua 1 ngay
                    long duration = TimeHelper.getSVDurationLastSeen(presence.getNowSv());
                    if (duration != 0) {
                        if (presence.getTimeStamp() == 0) {
                            item.setLastSeen(TimeHelper.getCurrentTime());
                        } else {
                            item.setLastSeen(presence.getTimeStamp() + duration);
                        }
                    } else {
                        item.setLastSeen(presence.getTimeStamp());
                    }
                    if (!TextUtils.isEmpty(presence.getOperator())) {
                        item.setOperator(presence.getOperator());
                        item.setOperatorPresence(presence.getOperator());
                    }
                    item.setUsingDesktop(presence.getUsingDesktop());
                    if (!TextUtils.isEmpty(presence.getStatePresence())) {
                        item.setStatePresence(Integer.valueOf(presence.getStatePresence()));
                    }
                    if (presence.getPreKey() != null) {
                        item.setPreKey(presence.getPreKey());
                    }
                } else if (subType == Presence.SubType.foreground) {
                    item.setLastSeen(0);
                    if (!TextUtils.isEmpty(presence.getOperator())) {
                        item.setOperator(presence.getOperator());
                        item.setOperatorPresence(presence.getOperator());
                    }
                    item.setUsingDesktop(presence.getUsingDesktop());
                    if (!TextUtils.isEmpty(presence.getStatePresence())) {
                        item.setStatePresence(Integer.valueOf(presence.getStatePresence()));
                    }
                    if (presence.getPreKey() != null) {
                        item.setPreKey(presence.getPreKey());
                    }
                }
            } else if (type == Presence.Type.unavailable) {
                if (subType == Presence.SubType.deactive) {
                    // deactive reset ve mac dinh
                    isStateChanged = true;
                    item.setState(Constants.CONTACT.DEACTIVE);
                    item.setLastChangeAvatar(null);
                    item.setStatus(null);
                    item.setNewRegister(false);
                    item.setGender(Constants.CONTACT.GENDER_MALE);
                } else {
                    if (!isReeng) {// moi dung reeng
                        item.setNewRegister(true);
                        item.setState(Constants.CONTACT.ACTIVE);
                        isStateChanged = true;
                        newUser = number;
                    }
                    item.setOnline(false);
                    long timeStamp = presence.getTimeStamp();
                    item.setLastOnline(timeStamp);
                    if (item.getLastSeen() == 0 || item.getLastSeen() == -1) {// truoc do dang seen (khong xac dinh)
                        if (TimeHelper.getSVDurationLastSeen(presence.getTimeStamp()) != 0) {
                            item.setLastSeen(TimeHelper.getCurrentTime());
                        } else {
                            item.setLastSeen(presence.getTimeStamp());
                        }
                    }
                }
            } else {// type # thi ko xu ly
                mApplication.logDebugContent("presence type khac: " + type.name());
                return;
            }
            // cache status emo
            mApplication.getQueueDecodeEmo().addTask(item.getStatus());
            //update listnumber
            boolean isReengTemp = item.isReeng();
            String operatorNew = item.getOperator();
            for (PhoneNumber phone : listUpdate) {
                if ((phone.isReeng() && !isReengTemp) || (!phone.isReeng() && isReengTemp)) {
                    isStateChanged = true;
                }
                phone.setLastChangeAvatar(item.getLastChangeAvatar());
                phone.setStatus(item.getStatus());
                phone.setState(item.getState());
                phone.setNewRegister(item.isNewRegister());
                phone.setOnline(item.isOnline());
                phone.setLastSeen(item.getLastSeen());
                phone.setLastOnline(item.getLastOnline());
                phone.setPermission(item.getPermission());
                if (!TextUtils.isEmpty(phone.getOperator()) && !phone.getOperator().equals(operatorNew)) {
                    phone.setOperator(operatorNew);
                    isUpdateOprViaLastSeen = true;
                }
                if (!TextUtils.isEmpty(phone.getPreKey()) && !phone.getPreKey().equals(item.getPreKey())) {
                    phone.setPreKey(item.getPreKey());
                    isUpdatePreKey = true;
                }
            }
            // refresh contact list
            if (isStateChanged) {
                initArrayListPhoneNumber();
            } /*else {
                ListenerHelper.getInstance().onPresenceChanged(item);
            }*///TODO notify cả trường hợp update roster
            ArrayList<PhoneNumber> p = new ArrayList<>();
            p.add(item);
            ListenerHelper.getInstance().onPresenceChanged(p);
            // draw notification
            // notifyPresence(item, presence); TODO không fake push notification khi, ban be change status,avatar nữa
            // . push nhận qua luồng onmedia
            //update db
            if (isDBChange || isStateChanged || isUpdateOprViaLastSeen || isUpdatePreKey) {// co thay doi du lieu can luu db moi update
                mNumberDataSource.updateListPhoneNumber(listUpdate, false);
            }
            // get infor nguoi dung moi
            if (newUser != null && newUser.length() > 0) {
                getInfoNewUser(newUser);
            }
        }
    }

    private void updateNonContactAfterReceivedPresence(String number, Presence presence) {
        NonContact nonContact = getExistNonContact(number);
        if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) return;
        PhoneNumber phoneNumber;
        if (nonContact != null) {
            phoneNumber = createPhoneNumberFromNonContact(nonContact);
        } else {
            phoneNumber = new PhoneNumber();
            phoneNumber.setJidNumber(number);
        }
        Presence.Type type = presence.getType();
        Presence.SubType subType = presence.getSubType();
        if (type == Presence.Type.available) {
            if (presence.getcState() == 0)
                phoneNumber.setState(Constants.CONTACT.NONE);
            else if (presence.getcState() == 1)
                phoneNumber.setState(Constants.CONTACT.ACTIVE);
            if (subType == Presence.SubType.change_status) {
                phoneNumber.setStatus(presence.getStatus());
            } else if (subType == Presence.SubType.change_avatar) {
                phoneNumber.setLastChangeAvatar(presence.getLastAvatar());
            } else if (subType == Presence.SubType.remove_avatar) {
                phoneNumber.setLastChangeAvatar(null);
            } else if (subType == Presence.SubType.invisible) {
                phoneNumber.setLastOnline(System.currentTimeMillis());
                phoneNumber.setOnline(false);
            } else if (subType == Presence.SubType.available ||
                    subType == Presence.SubType.client_info) {// subtype available online
                phoneNumber.setOnline(true);
            } else if (subType == Presence.SubType.new_user) {
                phoneNumber.setOnline(true);
            } else if (subType == Presence.SubType.background) {
                // thoi gian lech ko qua 1 ngay
                long duration = TimeHelper.getSVDurationLastSeen(presence.getNowSv());
                if (duration != 0) {
                    if (presence.getTimeStamp() == 0) {
                        phoneNumber.setLastSeen(TimeHelper.getCurrentTime());
                    } else {
                        phoneNumber.setLastSeen(presence.getTimeStamp() + duration);
                    }
                } else {
                    phoneNumber.setLastSeen(presence.getTimeStamp());
                }
                if (!TextUtils.isEmpty(presence.getOperator())) {
                    phoneNumber.setOperator(presence.getOperator());
                    phoneNumber.setOperatorPresence(presence.getOperator());
                }
                phoneNumber.setUsingDesktop(presence.getUsingDesktop());
                if (!TextUtils.isEmpty(presence.getStatePresence())) {
                    phoneNumber.setStatePresence(Integer.valueOf(presence.getStatePresence()));
                }
                if (presence.getPreKey() != null) {
                    phoneNumber.setPreKey(presence.getPreKey());
                }
            } else if (subType == Presence.SubType.foreground) {
                phoneNumber.setLastSeen(0);
                if (!TextUtils.isEmpty(presence.getOperator())) {
                    phoneNumber.setOperator(presence.getOperator());
                    phoneNumber.setOperatorPresence(presence.getOperator());
                }
                phoneNumber.setUsingDesktop(presence.getUsingDesktop());
                if (!TextUtils.isEmpty(presence.getStatePresence())) {
                    phoneNumber.setStatePresence(Integer.valueOf(presence.getStatePresence()));
                }
                if (presence.getPreKey() != null) {
                    phoneNumber.setPreKey(presence.getPreKey());
                }
            } else if (subType == Presence.SubType.change_operator) {
                phoneNumber.setOperator(presence.getOperator());
                phoneNumber.setOperatorPresence(presence.getOperator());
            } else if (subType == Presence.SubType.change_platform) {
                phoneNumber.setUsingDesktop(presence.getUsingDesktop());

                if (presence.getPreKey() != null) {
                    phoneNumber.setPreKey(presence.getPreKey());
                }
            }
            if (nonContact != null) {
                nonContact.setOperator(phoneNumber.getOperator());
                nonContact.setOperatorPresence(phoneNumber.getOperatorPresence());
                nonContact.setUsingDesktop(phoneNumber.getUsingDesktop());
                nonContact.setStatePresence(phoneNumber.getStatePresence());

                if (!TextUtils.isEmpty(phoneNumber.getPreKey())) {
                    nonContact.setPreKey(phoneNumber.getPreKey());
                }
            }
        } else if (type == Presence.Type.unavailable) {
            if (subType == Presence.SubType.deactive) {
                // deactive reset ve mac dinh
                phoneNumber.setState(Constants.CONTACT.DEACTIVE);
                phoneNumber.setLastChangeAvatar(null);
                phoneNumber.setStatus(null);
                phoneNumber.setGender(Constants.CONTACT.GENDER_MALE);
            } else {
                phoneNumber.setOnline(false);
                phoneNumber.setState(Constants.CONTACT.ACTIVE);
                long timeStamp = presence.getTimeStamp();
                phoneNumber.setLastOnline(timeStamp);
                if (phoneNumber.getLastSeen() == 0 || phoneNumber.getLastSeen() == -1) {// truoc do dang seen (khong
                    // xac dinh)
                    if (TimeHelper.getSVDurationLastSeen(presence.getTimeStamp()) != 0) {
                        phoneNumber.setLastSeen(TimeHelper.getCurrentTime());
                    } else {
                        phoneNumber.setLastSeen(presence.getTimeStamp());
                    }
                }
            }
        }
        // ve lai giao dien khi nhan dc response. cap nhat danh sach nonContact
        insertOrUpdateNonContact(phoneNumber, false);
        Log.d(TAG, "lastSeen: " + phoneNumber.getLastSeen() + " state: " + phoneNumber.getState());
        ArrayList<PhoneNumber> p = new ArrayList<>();
        p.add(phoneNumber);
        ListenerHelper.getInstance().onPresenceChanged(p);
    }

    private void notifyPresence(final PhoneNumber phoneNumber, Presence presence) {
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (myNumber != null && myNumber.equals(phoneNumber.getJidNumber())) {// nguoi change presence moi la chinh minh
            return;
        }
        BlockContactBusiness blockContactBusiness = mApplication.getBlockContactBusiness();
        if (blockContactBusiness.isBlockNumber(phoneNumber.getJidNumber())) {//neu da block roi thi ko lam gi
            return;
        }
        Resources res = mApplication.getResources();
        Presence.SubType subType = presence.getSubType();
        if (subType == Presence.SubType.change_avatar) {
            String message = String.format(mApplication.getResources().
                    getString(R.string.msg_change_avatar_notification), phoneNumber.getName());
            ReengNotificationManager.getInstance(mApplication).
                    drawPresenceNotification(mApplication, phoneNumber, message);
            /*AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
            int avatarSize = (int) res.getDimension(R.dimen.avatar_small_size);
            String avatarUrl = avatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), phoneNumber
            .getJidNumber(), avatarSize);
            if (!TextUtils.isEmpty(avatarUrl)) {
                SimpleImageLoadingListener loadingImageListener = new SimpleImageLoadingListener() {

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        Log.d(TAG, "updateContactAfterReceiverPresence---> onLoadingComplete");
                        if (loadedImage != null) {
                            Log.d(TAG, "updateContactAfterReceiverPresence---> onLoadingComplete bitmpa!=null ");
                            // load dduowcj avatar thi ve lai notify o che do silent
                            String message = String.format(mApplication.getResources().
                                    getString(R.string.msg_change_avatar_notification), phoneNumber.getName());
                            ReengNotificationManager.getInstance(mApplication).
                                    drawPresenceNotification(mApplication, phoneNumber, message);
                        }
                    }
                };
                Log.d(TAG, "updateContactAfterReceiverPresence---> loadAvatar " + avatarUrl);
                avatarBusiness.loadAvatar(avatarUrl, loadingImageListener, avatarSize);
            }*/
        } else if (subType == Presence.SubType.change_status) {
            if (!TextUtils.isEmpty(phoneNumber.getStatus())) {
                String msg = String.format(res.getString(R.string.msg_change_status_notification), phoneNumber
                        .getName());
                ReengNotificationManager.getInstance(mApplication).drawPresenceNotification(mApplication,
                        phoneNumber, msg);
            }
        }
    }

    // init contact when start main activity
    public synchronized void initContact() {
        logT("initContact > start");
        // load contact
        long beginTime = System.currentTimeMillis();
        ArrayList<Contact> listContacts = mContactDataSource.getAllContact();
        Log.d(TAG, "[] load all contact from db take: " + (System.currentTimeMillis() - beginTime) + " have: ");
        beginTime = System.currentTimeMillis();
        ArrayList<PhoneNumber> listNumbers = mNumberDataSource.getAllPhoneNumber();
        Log.d(TAG, "[] load all phone number from db take: " + (System.currentTimeMillis() - beginTime) + " have: ");
        // db null -- chay 1 lan khi cai moi app
        if (listContacts == null || listNumbers == null) {
            ArrayList<Contact> deviceContactList = getListContactFromDevice();
            ArrayList<PhoneNumber> devicePhoneList = getListNumberFromDevice();
            // device null
            if (deviceContactList == null || devicePhoneList == null) {
                setListContacs(new ArrayList<Contact>());
                setListNumbers(new ArrayList<PhoneNumber>());
                mListContactDB = new ArrayList<>();
                isContactReady = true;
                return;
            }
            // insert db
            String regionCode = mAccountBusiness.getRegionCode();
            deviceContactList = mergeContact(deviceContactList, devicePhoneList);
            mContactDataSource.insertListContact(deviceContactList);
            updateRawNumberAndNameUnicode(devicePhoneList, regionCode);// cap nhat ten va rawnumber trc khi insert
            mNumberDataSource.insertListPhoneNumber(devicePhoneList, mPhoneUtil, regionCode);

            // sync again contact
            syncContact();

            setListContacs(deviceContactList);
            mListContactDB = new ArrayList<>(deviceContactList);
            setListNumbers(devicePhoneList);
            initArrayListPhoneNumber();
            isNewInsertDB = true;
            isContactReady = true;
        } else {
            mListContactDB = new ArrayList<>(listContacts);
            // cache status emo
            for (PhoneNumber phoneNumber : listNumbers) {
                if (phoneNumber.isReeng() && phoneNumber.getStatus() != null) {
                    mApplication.getQueueDecodeEmo().addTask(phoneNumber.getStatus());
                }
            }
            // db not null.-- chay nhung lan sau
            ArrayList<Contact> listContactMerges = mergeContact(listContacts, listNumbers);
            setListContacs(listContactMerges);
            setListNumbers(listNumbers);
            initArrayListPhoneNumber();
            isContactReady = true;
        }
        logT("initContact > end");
    }

    /**
     * goi ham nay khi
     */
    public synchronized void syncContact() {
        logT("ContactBusiness > syncContact > start");
        Log.d(TAG, "syncContact: ");
        setSyncContact(true);
        if (!isContactReady()) {
            setSyncContact(false);
            return;
        }
        ArrayList<Contact> oldContactList = getListContacs();
        ArrayList<PhoneNumber> oldPhoneList = getListNumbers();
        long beginTime = System.currentTimeMillis();
        ArrayList<Contact> newContactList = getListContactFromDevice();
        ArrayList<PhoneNumber> newPhoneList = getListNumberFromDevice();
        Log.d(TAG, "[perform] load contact and phone from device take: " + (System.currentTimeMillis() - beginTime)
                + " - have:" + " ");
        newContactList = mergeContact(newContactList, newPhoneList);
        if (newContactList.isEmpty()) {
            //xoa db
            mContactDataSource.deleteAllTable();
            mNumberDataSource.deleteAllTable();
            setListContacs(new ArrayList<Contact>());
            setListNumbers(new ArrayList<PhoneNumber>());
            //update sv. replace = emptyTotalPointTabAdapter
            setInfoAllPhoneNumber();
            setSyncContact(false);
            return;
        }
        if (oldContactList == null || oldContactList.isEmpty() || oldPhoneList == null || oldPhoneList.isEmpty()) {
            //insert all
            String regionCode = mAccountBusiness.getRegionCode();
            mContactDataSource.insertListContact(newContactList);
            updateRawNumberAndNameUnicode(newPhoneList, regionCode);// raw number va ten trc khi insert
            mNumberDataSource.insertListPhoneNumber(newPhoneList, mPhoneUtil, regionCode);
            setListContacs(newContactList);
            setListNumbers(newPhoneList);
            setSyncContact(false);
            //update sv
            updateListInfoToServer(newPhoneList, null);
            return;
        }
        syncContactList(mListContactDB, newContactList);
        syncPhoneNumberList(oldPhoneList, newPhoneList);
        setSyncContact(false);
        logT("ContactBusiness > syncContact > end");
    }

    private void syncContactList(ArrayList<Contact> oldList, ArrayList<Contact> newList) {
        //HashMap<String, Contact> hashMapOldContact = getHashMapContactById(oldList);
        HashMap<String, Contact> hashMapOldContact = new HashMap<>();
        ArrayList<Contact> listDelete = new ArrayList<>();
        // put new hash map
        for (Contact contact : oldList) {
            if (hashMapOldContact.containsKey(contact.getContactId())) {
                // contact bi trung contact id thi xoa voi di
                listDelete.add(contact);
            } else {
                hashMapOldContact.put(contact.getContactId(), contact);
            }
        }
        HashMap<String, Contact> hashMapNewContact = getHashMapContactById(newList);
        ArrayList<Contact> list = new ArrayList<>();
        ArrayList<Contact> listUpdate = new ArrayList<>();
        ArrayList<Contact> listInsert = new ArrayList<>();
        // tim danh sach insert
        for (Map.Entry<String, Contact> entry : hashMapNewContact.entrySet()) {
            String key = entry.getKey();
            Contact newItem = entry.getValue();
            //co gia tri oldlist
            if (hashMapOldContact.containsKey(key)) {
                Contact oldItem = hashMapOldContact.get(key);
                if (!newItem.equalsValues(oldItem)) {
                    listUpdate.add(newItem);
                } else {
                    newItem.setNameUnicode(oldItem.getNameUnicode());
                }
            } else {
                listInsert.add(newItem);
            }
            list.add(newItem);
        }
        //list del
        for (Map.Entry<String, Contact> entry : hashMapOldContact.entrySet()) {
            String key = entry.getKey();
            Contact oldItem = entry.getValue();
            // khong co gia tri trong newlist
            if (!hashMapNewContact.containsKey(key)) {
                listDelete.add(oldItem);
            }
        }
        if (listDelete.size() >= 200) {// loi insert lap db, xoa het di insert lai
            mContactDataSource.deleteAllTable();
            mContactDataSource.insertListContact(list);
        } else {
            // update db
            mContactDataSource.updateListContact(listUpdate);
            mContactDataSource.deleteListContact(listDelete);
            mContactDataSource.insertListContact(listInsert);
        }
        // update obj
        setListContacs(list);
    }

    private void syncPhoneNumberList(ArrayList<PhoneNumber> oldList, ArrayList<PhoneNumber> newList) {
        Log.i(TAG, "syncPhoneNumberList");
        HashMap<String, PhoneNumber> hashMapOldNumber = getHashMapNumberById(oldList);
        HashMap<String, PhoneNumber> hashMapNewNumber = getHashMapNumberById(newList);
        ArrayList<PhoneNumber> list = new ArrayList<>();
        ArrayList<PhoneNumber> listUpdate = new ArrayList<>();
        ArrayList<PhoneNumber> listInsert = new ArrayList<>();
        ArrayList<PhoneNumber> listDelete = new ArrayList<>();
        ArrayList<PhoneNumber> listUpdateNewNumber = new ArrayList<>();
        HashMap<String, PhoneNumber> hashmap = new HashMap<>();
        for (Map.Entry<String, PhoneNumber> entry : hashMapNewNumber.entrySet()) {
            String key = entry.getKey();
            PhoneNumber newItem = entry.getValue();
            //co gia tri oldlist
            if (hashMapOldNumber.containsKey(key)) {
                PhoneNumber oldItem = hashMapOldNumber.get(key);
                // 2 obj cung so
                if (oldItem.getJidNumber() != null && oldItem.getJidNumber().equals(newItem.getJidNumber())) {
                    updateValueNewPhoneFromOldPhone(newItem, oldItem);
                    if (!newItem.equals(oldItem)) {// khac nhau 1 so truong, them vao list update
                        listUpdate.add(newItem);
                    }
                } else {// khac so
                    listUpdateNewNumber.add(newItem);
                }
            } else {
                listInsert.add(newItem);
            }
            list.add(newItem);
            hashmap.put(newItem.getJidNumber(), newItem);
        }
        // listdel
        HashMap<String, PhoneNumber> hashMapOld = new HashMap<>();
        for (Map.Entry<String, PhoneNumber> entry : hashMapOldNumber.entrySet()) {
            String key = entry.getKey();
            PhoneNumber oldItem = entry.getValue();
            // khong co gia tri trong newlist
            if (!hashMapNewNumber.containsKey(key)) {
                listDelete.add(oldItem);
            } else {
                hashMapOld.put(oldItem.getJidNumber(), oldItem);
            }
        }
        // merge list xoa (kiem tra so nao bi xoa that)
        ArrayList<PhoneNumber> listRemoveServer = new ArrayList<>();
        if (!listDelete.isEmpty()) {
            for (PhoneNumber removeItem : listDelete) {
                if (!hashmap.containsKey(removeItem.getJidNumber())) {
                    listRemoveServer.add(removeItem);
                } else {
                    hashMapOld.put(removeItem.getJidNumber(), removeItem);
                }
            }
        }
        // so sanh voi danh sach cu, neu ton tai danh sach cu thi ko add server, lay lai trang thai
        // merge lai du lieu (phong th lech phone id, so van vay)
        ArrayList<PhoneNumber> listAddServer = new ArrayList<>();
        if (!listUpdateNewNumber.isEmpty()) {
            for (PhoneNumber updateNewItem : listUpdateNewNumber) {
                if (!hashMapOld.containsKey(updateNewItem.getJidNumber())) {
                    listAddServer.add(updateNewItem);
                } else {// da co so nay thi lay lai trang thai set roster
                    PhoneNumber oldPhone = hashMapOld.get(updateNewItem.getJidNumber());
                    updateValueNewPhoneFromOldPhone(updateNewItem, oldPhone);
                }
            }
        }
        if (!listInsert.isEmpty()) {
            for (PhoneNumber insertItem : listInsert) {
                if (!hashMapOld.containsKey(insertItem.getJidNumber())) {
                    listAddServer.add(insertItem);
                } else {// da co so nay thi lay lai trang thai set roster
                    PhoneNumber oldPhone = hashMapOld.get(insertItem.getJidNumber());
                    updateValueNewPhoneFromOldPhone(insertItem, oldPhone);
                }
            }
        }
        //update db
        String regionCode = mAccountBusiness.getRegionCode();
        mNumberDataSource.updateListPhoneNumber(listUpdate, true);
        mNumberDataSource.updateListPhoneNumber(listUpdateNewNumber, true);
        mNumberDataSource.deleteListPhoneNumber(listDelete);
        updateRawNumberAndNameUnicode(list, regionCode);
        mNumberDataSource.insertListPhoneNumber(listInsert, mPhoneUtil, regionCode);
        // update obj
        this.mHashMap = hashmap;
        setListNumbers(list);
        // update number infor server
        //listInsert.addAll(listUpdate);
        updateListInfoToServer(listAddServer, listRemoveServer);
    }

    private ArrayList<PhoneNumber> getListPhoneNumberByContactId(String contactId, ArrayList<PhoneNumber>
            phoneNumbers) {
        if (phoneNumbers == null || phoneNumbers.isEmpty()) {
            return null;
        }
        ArrayList<PhoneNumber> list = new ArrayList<>();
        for (PhoneNumber item : phoneNumbers) {
            if (contactId.equals(item.getContactId())) {
                list.add(item);
            }
        }
        return list;
    }

    public void removeListPhoneNumberByContactId(String contactId, ArrayList<PhoneNumber> phoneNumbers) {
        if (phoneNumbers == null || phoneNumbers.isEmpty()) {
            return;
        }
        HashMap<String, PhoneNumber> hashmap = new HashMap<>();
        int size = phoneNumbers.size();
        int i = 0;
        for (; i < size; i++) {
            PhoneNumber phone = phoneNumbers.get(i);
            String key = phone.getJidNumber();
            if (phone.getContactId().equals(contactId)) {
                phoneNumbers.remove(i);
                i--;
                size--;
            } else {
                hashmap.put(key, phone);
            }
        }
        this.mHashMap = hashmap;
    }

    public String getListNameOfListNumber(ArrayList<String> numbers) {
        if (numbers == null || numbers.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(mApplication.getMessageBusiness().getFriendNameOfGroup(numbers.get(0)));
        int size = numbers.size();
        try {
            for (int i = 1; i < size; i++) {
                sb.append(", ").append(mApplication.getMessageBusiness().getFriendNameOfGroup(numbers.get(i)));
            }
        } catch (IndexOutOfBoundsException ex) {

        }

        return sb.toString();
    }

    public String getListNameOfListNumber(ArrayList<String> numbers, String myNumber, boolean isMe) {
        if (numbers == null || numbers.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (isMe) {
            numbers.remove(myNumber);
            numbers.add(0, myNumber);
            sb.append(mApplication.getResources().getString(R.string.you));
        } else {
            sb.append(mApplication.getMessageBusiness().getFriendNameOfGroup(numbers.get(0)));
        }
        int size = numbers.size();
        for (int i = 1; i < size; i++) {
            sb.append(", ").append(mApplication.getMessageBusiness().getFriendNameOfGroup(numbers.get(i)));
        }
        return sb.toString();
    }

    public String getLastSeenFromPhoneNumber(PhoneNumber phoneNumber) {
        Resources mResources;
        if (mApplication.getCurrentActivity() != null) {
            mResources = mApplication.getCurrentActivity().getResources();
        } else {
            mResources = mContext.getResources();
        }
        XMPPManager mXMPPManager = mApplication.getXmppManager();
        // check ket noi den server
        if (mXMPPManager.isConnected()) {
            // khong dung reeng
            if (!phoneNumber.isReeng()) {
                return getFakeLastSeen(mResources, phoneNumber, null);
            } else if (phoneNumber.getLastSeen() == 0) {// dung reeng va online
                return mResources.getString(R.string.online);
            } else if (phoneNumber.getLastSeen() == -1) {
                return null;
            } else {// dung reeng va offline  && phoneNumber.getLastOnline() > 0
                long timeSeen = TimeHelper.getCurrentTime() - phoneNumber.getLastSeen();
                if (timeSeen >= TimeHelper.INTERVAL && timeSeen <= TimeHelper.INTERVAL_MIN) {
                    return mResources.getString(R.string.offline_one_minute);
                } else if (isFakeLastSeenViettel(phoneNumber, timeSeen)) {// last_seen >30 days
                    return mResources.getString(R.string.fake_last_seen_month);
                } else {
                    return String.format(mResources.getString(R.string.offline),
                            TimeHelper.formatTimeLastSeen(phoneNumber.getLastSeen(), mResources, timeSeen));
                }
            }
        } else {
            return null;
        }
    }

    private boolean isFakeLastSeenViettel(PhoneNumber phoneNumber, long timeSeen) {
        if (mAccountBusiness.isViettel() && phoneNumber.isViettel()) {
            return timeSeen > TimeHelper.ONE_MONTH;
        }
        return false;
    }

    private String getFakeLastSeen(Resources res, PhoneNumber phoneNumber, String realLastSeen) {
        if (phoneNumber.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
            return null;
        } else if (mAccountBusiness.isViettel() && phoneNumber.isViettel()) {
            return res.getString(R.string.message_fake_lastseen);
            //return String.format(res.getString(R.string.offline), TimeHelper.formatTimeFakeLastSeen());
            //return res.getString(R.string.status_fake);
        } /*else if (phoneNumber.isReeng()) {
            return String.format(res.getString(R.string.offline), realLastSeen);
        }*/
        return null;
    }

    private ArrayList<PhoneNumber> removeDuplicatePhone(ArrayList<PhoneNumber> listPhone) {
        //remove duplicate
        HashSet<PhoneNumber> hashSetPhone = new HashSet<>(listPhone);
        return new ArrayList<>(hashSetPhone);
    }

    private void logT(String m) {
        Log.e("TVV-ShareAndGetMore", m);
    }

    //mocha
    public synchronized void initArrayListPhoneNumber() {
        Log.d(TAG, "initArrayListPhoneNumber");
        logT("ContactBusiness > initArrayListPhoneNumber > start");
        // neu dang init thi ko chay nua
        if (isInitArrayList) {
            return;
        }
        isInitArrayList = true;
        Log.d(TAG, "initArrayListPhoneNumber set true");
        ArrayList<PhoneNumber> listReeng = new ArrayList<>();
        ArrayList<PhoneNumber> listFavorite = new ArrayList<>();
        ArrayList<PhoneNumber> listAll = new ArrayList<>();
        ArrayList<PhoneNumber> listSupport = new ArrayList<>();
        ArrayList<PhoneNumber> listViettel = new ArrayList<>();
        // kiem tra so dang nhap co phai so vt khong
        boolean isUserViettel = false;
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (account != null && account.isActive()) {
            isUserViettel = mApplication.getReengAccountBusiness().isViettel();
        }
        setHashmapPhone(mListNumbers, true);
        if (getListContacs() == null || getListContacs().isEmpty() ||
                getListNumbers() == null || getListNumbers().isEmpty()) {
            mListNumberAlls = mListNumberFavorites = new ArrayList<>();
            isArrayReady = true;
            ListenerHelper.getInstance().onContactChanged();
            isInitArrayList = false;
            sizeContactSupport = 0;
            this.mListNumberUseMocha = new ArrayList<>();
            return;
        }
        long begin = System.currentTimeMillis();
        //remove duplicate
        ArrayList<PhoneNumber> listPhone = removeDuplicatePhone(mListNumbers);
        int sizeContact = listPhone.size();
        try {
            //sort
            Comparator<Object> comparator = ComparatorHelper.getComparatorNumberByName();
            Collections.sort(listPhone, comparator);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<String, PhoneNumber> hashmap = new HashMap<>();
        for (PhoneNumber phone : listPhone) {
            // so dung reeng add 1 ds, so con lai add 1 danh sach.
            if (phone.isReeng()) {
                listReeng.add(phone);
                listSupport.add(phone);
            } else if (isUserViettel && phone.isViettel()) {// ca so dang nhap va danh ba la so vt
                listSupport.add(phone);
            } else {
                listAll.add(phone);
            }
            if (phone.isViettel()) {
                listViettel.add(phone);
            }
            if (phone.getFavorite() == Constants.CONTACT.GROUP_FAVORITE) {
                listFavorite.add(phone);
            }
            // khoi tao hashmap phonenumber
            String key = phone.getJidNumber();
            hashmap.put(key, phone);
        }
        mHashMap = hashmap;
        //sort list. sap xep danh sach ko dung reeng la so vt truoc
        listSupport = sortNumberByName(listSupport);
        int sizeNoSupport = listAll.size();
        // danh sach reeng != empty, add item MOCHA
        sizeContactSupport = (sizeContact - sizeNoSupport);
        if (!listAll.isEmpty()) {
            PhoneNumber indexi = new PhoneNumber();
            String rowNoSupport = String.format(mContext.getResources().getString(R.string.row_not_support),
                    sizeNoSupport);
            indexi.setName(rowNoSupport);
            indexi.setContactId(null);
            indexi.setSectionType(-3);
            listAll.add(0, indexi);
        }
        // ghep voi list support
        listAll.addAll(0, listSupport);
        // danh sach favorite != empty, add item favorite
       /* if (!listFavorite.isEmpty()) {
            PhoneNumber indexf = new PhoneNumber();
            indexf.setName(mContext.getResources().getString(R.string.list_favorite));
            indexf.setContactId(null);
            listFavorite.add(0, indexf);
        }*/
        // lay list section
        mListSectionCharAlls = getListSectionCharByListPhone(listAll);
        setListNumberAll(listAll);
        this.mListNumberFavorites = listFavorite;
        this.mListNumberSupport = listSupport;
        this.mListNumberViettel = listViettel;
        this.mListNumberUseMocha = listReeng;
        isArrayReady = true;
        isInitArrayList = false;
        ListenerHelper.getInstance().initContactListComplate(sizeContactSupport);
        Log.d(TAG, "init arrayList take: " + (System.currentTimeMillis() - begin));
        ListenerHelper.getInstance().onContactChanged();

        logT("ContactBusiness > initArrayListPhoneNumber > mListNumberAlls: " + mListNumberAlls.size());
        logT("ContactBusiness > initArrayListPhoneNumber > end");
    }

    public synchronized void initFavoriteListPhoneNumber() {
        ArrayList<PhoneNumber> listFavorite = new ArrayList<>();
        for (PhoneNumber phone : mListNumbers) {
            if (phone.getContactId() != null && phone.getFavorite() == Constants.CONTACT.GROUP_FAVORITE) {
                listFavorite.add(phone);
            }
        }
        /*if (!listFavorite.isEmpty()) {
            PhoneNumber index0 = new PhoneNumber();
            index0.setName(mContext.getResources().getString(R.string.list_favorite));
            index0.setContactId(null);
            listFavorite.add(0, index0);
        }*/
        this.mListNumberFavorites = listFavorite;
    }

    public ArrayList<PhoneNumber> getListNonFavorite() {
        ArrayList<PhoneNumber> listNonFavorite = new ArrayList<>();
        if (getListContacs().isEmpty() || getListNumbers().isEmpty()) {
            return listNonFavorite;
        }
        //sort
        //        Comparator<Object> comparator = ComparatorHelper.getComparatorNumberByName();
        //        Collections.sort(mListNumbers, comparator);
        ArrayList<PhoneNumber> listReeng = new ArrayList<>();
        for (PhoneNumber phone : mListNumbers) {
            // so chua co trong yeu thich
            if (phone.getFavorite() == Constants.CONTACT.GROUP_NONE) {
                if (phone.isReeng()) {
                    listReeng.add(phone);
                } else {
                    listNonFavorite.add(phone);
                }
            }
        }
        //sort list. sap xep danh sach ko dung reeng truoc
        listNonFavorite = sortNumberByName(listNonFavorite);
        // danh sach reeng != empty, add item MOCHA
        if (!listReeng.isEmpty()) {
            PhoneNumber index0 = new PhoneNumber();
            index0.setName(mContext.getResources().getString(R.string.app_name).toUpperCase());
            index0.setContactId(null);
            listReeng.add(0, index0);
        }
        // ghep 2 danh sach lai voi nhau
        listNonFavorite.addAll(0, listReeng);
        return listNonFavorite;
    }

    public boolean isInitArrayListReady() {
        if (!isArrayReady) {
            return false;
        } else return !(mListNumberAlls == null || mListNumberFavorites == null);
    }

    public boolean deleteContactDBAndDevices(String contactId) {
        //xoa tren devices
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        ContentResolver cr = mContext.getContentResolver();
        ops.add(ContentProviderOperation
                .newDelete(ContactsContract.RawContacts.CONTENT_URI)
                .withSelection(
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                + " = ?", new String[]{contactId}
                ).build());
        try {
            cr.applyBatch(ContactsContract.AUTHORITY, ops);
            ops.clear();
            deleteContact(contactId);
            return true;
        } catch (OperationApplicationException e) {
            Log.e(TAG, "OperationApplicationException", e);
            return false;
        } catch (RemoteException e) {
            Log.e(TAG, "RemoteException", e);
            return false;
        }
    }

    // change favorite // neu add vao thi isFavorite=true
    public void changeFavorite(final String contactId, final boolean isFavorite) {
        Thread threadChangeFavorite = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    Contact contact = getContactFromContactId(contactId);
                    ContentObserverBusiness observerBusiness = ContentObserverBusiness.getInstance(mContext);
                    observerBusiness.setAction(Constants.ACTION.CHANGE_FAVORITE);
                    // update mem
                    int group = isFavorite ? 1 : 0;
                    contact.setFavorite(group);
                    ArrayList<PhoneNumber> listPhone = contact.getListNumbers();
                    if (listPhone != null && !listPhone.isEmpty()) {
                        for (PhoneNumber phone : listPhone) {
                            phone.setFavorite(group);
                        }
                    }
                    //update Db
                    mContactDataSource.updateContact(contact);
                    mNumberDataSource.updateListPhoneNumber(listPhone, false);
                    // update devices
                    ContentValues values = new ContentValues();
                    values.put(ContactsContract.Contacts.STARRED, isFavorite);
                    mContext.getContentResolver().update(ContactsContract.Contacts.CONTENT_URI, values,
                            "_ID=?", new String[]{contactId});
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
        });
        threadChangeFavorite.start();
    }

    /**
     * get list phone number from list member group
     *
     * @param listMembers
     * @return
     */
    public ArrayList<PhoneNumber> getListPhoneNumbersFromMemberGroup(ArrayList<String> listMembers) {
        ArrayList<String> listDefault = new ArrayList<>();
        if (listMembers == null || listMembers.isEmpty()) {
            return new ArrayList<>();
        }
        ArrayList<PhoneNumber> listPhoneNumbers = new ArrayList<>();
        for (String number : listMembers) {
            PhoneNumber phoneNumber = getPhoneNumberFromNumber(number);
            if (phoneNumber != null) {
                listPhoneNumbers.add(phoneNumber);
            } else {
                NonContact nonContact = getExistNonContact(number);
                if (nonContact != null) {
                    PhoneNumber p = createPhoneNumberFromNonContactForMember(nonContact);
                    p.setContactId("-100");
                    listPhoneNumbers.add(p);
                } else
                    listDefault.add(number);
            }
        }
        ArrayList<PhoneNumber> listPhoneDefault = createListPhoneNumberDefault(listDefault);
        if (listPhoneDefault != null) {
            listPhoneNumbers.addAll(listPhoneDefault);
        }
        //remove confilict
        HashSet<PhoneNumber> setNumbers = new HashSet<>(listPhoneNumbers);
        listPhoneNumbers = new ArrayList<>(setNumbers);
        // sort oder by name
        Collections.sort(listPhoneNumbers, ComparatorHelper.getComparatorNumberByName());
        return listPhoneNumbers;
    }

    public ArrayList<String> getListNumberUnknownFromMembersGroup(ArrayList<String> listMembers) {
        if (listMembers == null || listMembers.isEmpty()) {
            return null;
        }
        ArrayList<String> listDefault = new ArrayList<>();
        for (String number : listMembers) {
            PhoneNumber phoneNumber = getPhoneNumberFromNumber(number);
            if (phoneNumber == null) {
                listDefault.add(number);
            }
        }
        return listDefault;
    }

    //TODO fake noncontact khi group có số không lưu danh bạ
    public void updateStateNonContactAfterChangedGroup(ArrayList<Member> members) {
        if (members == null || members.isEmpty()) return;
        new AsyncTask<ArrayList<Member>, Void, Void>() {
            @Override
            protected Void doInBackground(ArrayList<Member>... arrayLists) {
                updateStateNonContactGroupMember(arrayLists[0]);
                return null;
            }
        }.execute(members);
    }

    private void updateStateNonContactGroupMember(@NonNull ArrayList<Member> members) {
        ArrayList<NonContact> listUpdateNonContacts = new ArrayList<>();
        ArrayList<NonContact> listInsertNonContacts = new ArrayList<>();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        for (Member member : members) {
            String jid = member.getJid();
            String newJid = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(jid);
            if (newJid != null) jid = newJid;
            if (myNumber != null && myNumber.equals(jid)) {
                continue;
            }
            PhoneNumber phoneNumber = getPhoneNumberFromNumber(jid);
            if (phoneNumber == null) {
                NonContact existNonContact = getExistNonContact(jid);
                if (existNonContact != null) { //non contact da co, update
                    if (!TextUtils.isEmpty(member.getNickName())) {
                        existNonContact.setNickName(member.getNickName());
                        listUpdateNonContacts.add(existNonContact);
                    }
                } else {
                    NonContact newNonContact = new NonContact();
                    newNonContact.setJidNumber(jid);
                    newNonContact.setState(Constants.CONTACT.ACTIVE);
                    newNonContact.setNickName(member.getNickName());
                    listInsertNonContacts.add(newNonContact);
                }
            }
        }
        Log.d(TAG, "updateStateNonContactGroupMember - insert: " + listInsertNonContacts.size() + " updateSize: " +
                listUpdateNonContacts.size());
        if (!listInsertNonContacts.isEmpty()) {
            synchronized (TAG) {
                mNonContactDataSource.insertListNonContact(listInsertNonContacts);
                mListNonContacts.addAll(listInsertNonContacts);
            }
        }
        if (!listUpdateNonContacts.isEmpty()) {
            mNonContactDataSource.updateListNonContact(listUpdateNonContacts);
        }
    }

    // cap nhat trang thai vao mem va db sau khi nhan dc response
    public void updateContactInfo(ArrayList<PhoneNumber> responseList) {
        // cho init data tren mem xong
        while (isInitArrayList) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                Log.e(TAG, "InterruptedException", e);
            }
        }
        ArrayList<PhoneNumber> phoneNumbers = getListNumbers();
        ArrayList<PhoneNumber> listUpdate = new ArrayList<>();
        if (phoneNumbers == null || phoneNumbers.isEmpty()) {
            return;
        }
        if (responseList == null || responseList.isEmpty()) {
            return;
        }

        if (mHashmapPhone == null || mHashmapPhone.isEmpty()) {
            setHashmapPhone(mListNumbers, false);
        }
        HashSet<PhoneNumber> response = new HashSet<>(responseList);
        for (PhoneNumber item : response) {
            String key = item.getJidNumber();
            if (mHashmapPhone.containsKey(key)) {
                ArrayList<PhoneNumber> listPhones = mHashmapPhone.get(key);
                //update mem
                ArrayList<PhoneNumber> listChanged = updateItemContactInfo(item, listPhones);
                if (listChanged != null && !listChanged.isEmpty()) {
                    listUpdate.addAll(listChanged);
                }
            }
        }
        //update db
        mNumberDataSource.updateListPhoneNumber(listUpdate, true);
        // refresh contact list
        if (!listUpdate.isEmpty()) {
            initArrayListPhoneNumber();
        } else {
            ListenerHelper.getInstance().onRosterChanged();
        }
    }

    // update obj sau khi parser json
    private ArrayList<PhoneNumber> updateItemContactInfo(PhoneNumber item,
                                                         ArrayList<PhoneNumber> phoneNumbers) {
        ArrayList<PhoneNumber> listUpdate = new ArrayList<>();
        int stateItem = item.getState();
        int genderItem = item.getGender();
        String statusItem = item.getStatus();
        String lastAvarItem = item.getLastChangeAvatar();
        String birthdayStringItem = item.getBirthdayString();
        int permissionItem = item.getPermission();
        String nickNameItem = item.getNickName();
        //Log.e(TAG, "item permission=" + permission + "; number=" + item.getJidNumber());
        long lastOnline = item.getLastOnline();
        long lastSeen = item.getLastSeen();
        boolean online = item.isOnline();
        String operator = item.getOperator();
        String preKey = item.getPreKey();
        for (PhoneNumber phone : phoneNumbers) {
            boolean isChange = false;
            int state = phone.getState();
            int gender = phone.getGender();
            String status = phone.getStatus();
            String lastAvar = phone.getLastChangeAvatar();
            String birthdayString = phone.getBirthdayString();
            int permission = phone.getPermission();
            String nickName = phone.getNickName();
            String opr = phone.getOperator();
            String key = phone.getPreKey();

            int addRoster = phone.getAddRoster();
            // last online chi cap nhat vao mem, khong cap nhat db
            if (lastOnline != -1) {
                phone.setLastOnline(lastOnline);
            }
            phone.setOnline(online);
            if (lastSeen != -1) {
                phone.setLastSeen(lastSeen);
            }
            //state. gender,birthday
            if (stateItem != state) {
                isChange = true;
                phone.setState(stateItem);
                // moi dung reeng
                if (stateItem == Constants.CONTACT.ACTIVE) {
                    phone.setNewRegister(true);
                }
            }
            if (genderItem != gender) {
                isChange = true;
                phone.setGender(genderItem);
            }
            /*            if (birthdayItem != birthday) {
                isChange = true;
                phone.setBirthDay(birthdayItem);
            }*/
            // co thay doi stt tu khong sang co || co sang khong || thay doi noi dung
            if ((statusItem != null && status == null) ||
                    (statusItem == null && status != null) ||
                    (statusItem != null && !statusItem.equals(status))) {
                phone.setStatus(statusItem);
                // cache status emo neu thay doi noi dung stt
                mApplication.getQueueDecodeEmo().addTask(item.getStatus());
                isChange = true;
            }
            //co thay doi lastchange avatar (giong status)
            if ((lastAvarItem != null && lastAvar == null) ||
                    (lastAvarItem == null && lastAvar != null) ||
                    (lastAvarItem != null && !lastAvarItem.equals(lastAvar))) {
                phone.setLastChangeAvatar(lastAvarItem);
                isChange = true;
            }
            if (birthdayStringItem != null && !birthdayStringItem.equals(birthdayString)) {
                isChange = true;
                phone.setBirthdayString(birthdayStringItem);
            }
            if (addRoster != 1) {
                isChange = true;
                phone.setAddRoster(1);
            }

            if (permissionItem != -1 && permissionItem != permission) {
                isChange = true;
                phone.setPermission(permissionItem);
            }
            if (nickNameItem != null && !nickNameItem.equals(nickName)) {
                isChange = true;
                phone.setNickName(nickNameItem);
            }
            if (operator != null && !operator.equals(opr)) {
                isChange = true;
                //Log.i(TAG, "updateItemContactInfo: phone " + phone.getJidNumber() + " operator: " + operator);
                phone.setOperator(operator);
            }
            if (preKey != null && !preKey.equals(key)) {
                isChange = true;
                phone.setPreKey(preKey);
            }
            // co trang thais thay doi can cap nhat DB
            if (isChange) {
                listUpdate.add(phone);
            }
        }
        return listUpdate;
    }

    // api lay thong tin nhung so co trang thai tren sv
    private void getInfoAllPhoneNumber() {
        checkUpdateInfo();
    }

    /**
     * api lay thong tin user moi su dung tren sv
     *
     * @param number so dt can lay trang thai
     */
    public void getInfoNewUser(String number) {
        ArrayList<String> listNumbers = new ArrayList<>();
        listNumbers.add(number);
        ContactRequestHelper.getInstance(mApplication).getInfoContactsFromNumbers(listNumbers, new
                ContactRequestHelper.onResponseInfoContactsListener() {
                    @Override
                    public void onResponse(ArrayList<PhoneNumber> responses) {
                        updateContactInfo(responses);
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.d(TAG, "Error get info new user: " + errorCode);
                    }
                });
    }

    /**
     * api lay thong tin 1 so tren sv
     *
     * @param number so dt can lay trang thai
     */
    public void getInfoNumber(final String number) {
        // neu da luu trang thai so nay roi thi se hien thi tam nen, sau do goi request
        NonContact nonContact = getExistNonContact(number);
        if (nonContact != null) {
            ArrayList<PhoneNumber> p = new ArrayList<>();
            p.add(createPhoneNumberFromNonContact(nonContact));
            ListenerHelper.getInstance().onPresenceChanged(p);
            //if (!nonContact.isTimeOutGetInfo() && nonContact.getStateFollow() != -2) {// đã lấy được state follow
            if (!nonContact.isTimeOutGetInfo()) {
                return;// chua qua time out thi ko get nua
            }
        }
        ArrayList<String> listNumbers = new ArrayList<>();
        listNumbers.add(number);
        ContactRequestHelper.getInstance(mApplication).getInfoContactsFromNumbers(listNumbers, new
                ContactRequestHelper.onResponseInfoContactsListener() {
                    @Override
                    public void onResponse(ArrayList<PhoneNumber> responses) {
                        if (responses != null && !responses.isEmpty()) {
                            PhoneNumber phoneNumber = responses.get(0);
                            // ve lai giao dien khi nhan dc response. cap nhat danh sach nonContact
                            insertOrUpdateNonContact(phoneNumber, true, true);
                            NonContact nonContact = getExistNonContact(phoneNumber.getJidNumber());
                            if (nonContact != null) {
                                Log.d(TAG, "lastSeen: " + nonContact.getLastSeen() + " state: " + nonContact.getState
                                        ());
                                ArrayList<PhoneNumber> p = new ArrayList<>();
                                p.add(createPhoneNumberFromNonContact(nonContact));
                                ListenerHelper.getInstance().onPresenceChanged(p);
                            }
                        } else {
                            PhoneNumber phoneNumber = new PhoneNumber();
                            phoneNumber.setJidNumber(number);
                            phoneNumber.setState(Constants.CONTACT.NONE);
                            insertOrUpdateNonContact(phoneNumber, true);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.d(TAG, "Error get info: " + errorCode);
                    }
                });
    }

    /**
     * api lay thong tin list so tren sv (lay trang thai list ban be trong group)
     *
     * @param listNumbers danh sach so can lay trang thai
     */
    public void getInfoListNumber(ArrayList<String> listNumbers, final ContactRequestHelper.onResponse callBack) {
        if (listNumbers == null || listNumbers.isEmpty()) {
            return;
        }
        ContactRequestHelper.getInstance(mApplication).getInfoContactsFromNumbers(listNumbers, new
                ContactRequestHelper.onResponseInfoContactsListener() {
                    @Override
                    public void onResponse(ArrayList<PhoneNumber> responses) {
                        if (responses != null && !responses.isEmpty()) {
                            Log.i(TAG, "---------S11--------- insertOrUpdateListNonContact");
                            insertOrUpdateListNonContact(responses);
                            // TODO co can phai notifi ra ko?
                            ListenerHelper.getInstance().onPresenceChanged(responses);
                        }
                        if (callBack != null) callBack.onResponse();
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.d(TAG, "Error get info list number: " + errorCode);
                    }
                });
    }

    private void updateStateRosterInMem() {
        if (mListNumbers != null) {
            for (PhoneNumber item : mListNumbers) {
                item.setAddRoster(1);
            }
        }
    }

    // api replace so tren sv
    public void setInfoAllPhoneNumber() {
        ContactRequestHelper.getInstance(mApplication).setInfoAllPhoneNumber(new ContactRequestHelper
                .onResponseSetContactListener() {
            @Override
            public void onResponse(ArrayList<PhoneNumber> responses) {
                updateStateRosterInMem();
                updateContactInfo(responses);
                mNumberDataSource.updateAllStateSendRoster();
                /*ArrayList<PhoneNumber> listPhone = getListNumberAlls();
                if (listPhone != null && !listPhone.isEmpty()) {
                    Log.i(TAG, "update list phonumber: " + listPhone.size());
                    ContactSyncHelper contactSyncHelper = new ContactSyncHelper(mApplication);
                    contactSyncHelper.setListPhone(listPhone);
                    contactSyncHelper.start();
                }*/
            }

            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "Error set all contact: " + errorCode);
            }
        });
    }

    // api them so len sv
    private void addInfoListPhoneNumber(final ArrayList<PhoneNumber> listPhone) {
        ContactRequestHelper.getInstance(mApplication).addInfoListPhoneNumber(listPhone, new ContactRequestHelper
                .onResponseContact() {
            @Override
            public void onResponse(ArrayList<PhoneNumber> responses) {
                updateStateRosterInMem();
                updateContactInfo(responses);
                ListenerHelper.getInstance().syncContactNotify();
                // cap nhat trang thai roster len server
                mNumberDataSource.updateListStateSendRoster(listPhone);
                handleSyncContactFromAddPhonenumber(listPhone);
            }

            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "Error add contact: " + errorCode);
            }
        });

        addOptionMochaToListContact(listPhone);
    }

    private void handleSyncContactFromAddPhonenumber(ArrayList<PhoneNumber> listPhone) {

        ArrayList<PhoneNumber> listUpdate = new ArrayList<>();
        for (PhoneNumber p : listPhone) {
            PhoneNumber phoneNumber = getPhoneNumberFromNumber(p.getJidNumber());
            if (phoneNumber != null) {
                listUpdate.add(phoneNumber);
            }
        }
        Log.i(TAG, "update list phonumber: " + listUpdate.size());
        /*ContactSyncHelper contactSyncHelper = new ContactSyncHelper(mApplication);
        contactSyncHelper.setListPhone(listUpdate);
        contactSyncHelper.start();*/
    }

    // api xoa so tren sv
    private void removeInfoListPhoneNumber(final ArrayList<String> listNumbers, final boolean deletePref) {
        ContactRequestHelper.getInstance(mApplication).removeInfoListPhoneNumber(listNumbers, new
                ContactRequestHelper.onResponseContact() {
                    @Override
                    public void onResponse(ArrayList<PhoneNumber> responses) {
                        if (deletePref) {
                            removeListSharePref();
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.d(TAG, "Error remove contact: " + errorCode);
                        if (!deletePref) {
                            addListRemoveFail(listNumbers);
                        }
                    }
                }
        );
    }

    /**
     * gui them, xoa so tren server
     *
     * @param listAdd    danh sach so duoc them
     * @param listRemove danh sach so bi xoa
     */
    private synchronized void updateListInfoToServer(ArrayList<PhoneNumber> listAdd, ArrayList<PhoneNumber>
            listRemove) {
        // truong hop chua login thanh cong ma chay vao sync contact thi ko goi api update contact
        if (!mAccountBusiness.isValidAccount()) {
            return;
        }
        Log.d(TAG, "updateListInfor: listAdd: ");
        //        HashSet<PhoneNumber> hashSetAll = new HashSet<PhoneNumber>(mListNumbers);
        if (listAdd != null && !listAdd.isEmpty()) {
            Log.d(TAG, "updateListInfor: listAdd: " + listAdd.size());
            addInfoListPhoneNumber(listAdd);
        }
        if (listRemove != null && !listRemove.isEmpty()) {
            Log.d(TAG, "updateListInfor: listRemove: != null");
            ArrayList<String> numbers = new ArrayList<>();
            for (PhoneNumber phone : listRemove) {
                numbers.add(phone.getJidNumber());
            }
            removeInfoListPhoneNumber(numbers, false);
        } else {
            Log.d(TAG, "updateListInfor: listRemove: == null");
        }
    }

    private Thread threadGetContact;

    public void queryPhoneNumberInfo() {
        Log.d(TAG, "get all info ");
        if (threadGetContact == null) {
            threadGetContact = new Thread() {
                @Override
                public void run() {
                    try {
                        while (isSyncContact()) {
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException e) {
                                Log.e(TAG, "InterruptedException", e);
                            }
                        }
                        /*long lastTimeGetInfo = mPref.getLong(Constants.PREFERENCE.PREF_CONTACT_LAST_TIME_GET_INFO,
                        -1);
                          if (TimeHelper.checkTimeOutForDurationTime(lastTimeGetInfo, TimeHelper
                          .TIME_OUT_GET_CONTACT)) {
                          thoi gian get contact chua < TIME_OUT_GET_CONTACT thi get contact*/
                        getInfoAllPhoneNumber();
                        //}
                        //checkUpdateInfo();
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    } finally {
                        threadGetContact = null;
                    }
                }
            };
            threadGetContact.start();
        }
    }

    // check so dien thoai chua add duoc len server, so chua xoa khi mat mang
    public void checkUpdateInfo() {
        Log.d(TAG, "check  update info ");
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        if (account == null || !account.isActive()) {
            return;
        }
        ArrayList<String> oldListRemove = getListRemoveFail();
        if (oldListRemove != null && !oldListRemove.isEmpty()) {
            if (oldListRemove.size() > 100) {//  error luu pref tu phien ban truoc
                removeListSharePref();
            } else {
                removeInfoListPhoneNumber(oldListRemove, true);
            }
        }
        ArrayList<PhoneNumber> listUnSendSever = mNumberDataSource.getPhoneNumberUnSendRoster();
        if (listUnSendSever != null && !listUnSendSever.isEmpty()) {
            addInfoListPhoneNumber(listUnSendSever);
        }
    }

    // set list delete fail SharePref
    private void addListRemoveFail(ArrayList<String> listRemoveFail) {
        HashSet<String> listInsert = new HashSet<>();
        // lay list fail cu
        ArrayList<String> oldList = getListRemoveFail();
        if (oldList != null && !oldList.isEmpty()) {
            listInsert.addAll(oldList);
        }
        if (listRemoveFail == null || listRemoveFail.isEmpty()) {
            return;
        } else {
            listInsert.addAll(listRemoveFail);
        }
        JSONArray jsonArray = new JSONArray();
        for (String item : listInsert) {
            jsonArray.put(item);
        }
        mPref.edit().putString(Constants.PREFERENCE.PREF_CONTACT_LIST_REMOVE_FAIL, jsonArray.toString()).apply();
    }

    // get list delete fail SharePref
    private ArrayList<String> getListRemoveFail() {
        String content = mPref.getString(Constants.PREFERENCE.PREF_CONTACT_LIST_REMOVE_FAIL, null);
        if (TextUtils.isEmpty(content)) {
            return null;
        }
        ArrayList<String> listFail = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(content);
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                String jidNumber = jsonArray.get(i).toString();
                listFail.add(jidNumber);
            }
            return listFail;
        } catch (Exception e) {
            Log.e(TAG, e);
            return null;
        }
    }

    // remove list delete fail SharePref
    private void removeListSharePref() {
        mPref.edit().putString(Constants.PREFERENCE.PREF_CONTACT_LIST_REMOVE_FAIL, "").apply();
    }

    /**
     * cac ham xu ly cho so chua luu danh ba
     */
    public NonContact createNonContactFromPhoneNumber(PhoneNumber phoneNumber) {
        return new NonContact(phoneNumber.getJidNumber(), phoneNumber.getState(),
                phoneNumber.getStatus(), phoneNumber.getLastChangeAvatar(),
                phoneNumber.getGender(), phoneNumber.getLastOnline(),
                phoneNumber.getLastSeen(), phoneNumber.getBirthdayString(),
                phoneNumber.getNickName(), phoneNumber.getStateFollow(), phoneNumber.getOperator(), phoneNumber.getPreKey());
    }

    public NonContact updateOldNonContactFromPhoneNumber(NonContact oldNonContact, PhoneNumber phoneNumber) {
        oldNonContact.setState(phoneNumber.getState());
        oldNonContact.setStatus(phoneNumber.getStatus());
        oldNonContact.setLAvatar(phoneNumber.getLastChangeAvatar());
        oldNonContact.setGender(phoneNumber.getGender());
        oldNonContact.setNickName(phoneNumber.getNickName());
        //        oldNonContact.setBirthDay(phoneNumber.getBirthDay());
        if (phoneNumber.getLastOnline() != -1) {
            oldNonContact.setLastOnline(phoneNumber.getLastOnline());
        }
        if (phoneNumber.getLastSeen() != -1) {
            oldNonContact.setLastSeen(phoneNumber.getLastSeen());
        }
        oldNonContact.setBirthDayString(phoneNumber.getBirthdayString());
        if (phoneNumber.getPermission() != -1) {
            oldNonContact.setPermission(phoneNumber.getPermission());
        }
        if (phoneNumber.getStateFollow() != -2) {
            oldNonContact.setStateFollow(phoneNumber.getStateFollow());
        }
        if (!TextUtils.isEmpty(phoneNumber.getOperator())) {
            oldNonContact.setOperator(phoneNumber.getOperator());
        }
        if (!TextUtils.isEmpty(phoneNumber.getPreKey())) {
            oldNonContact.setPreKey(phoneNumber.getPreKey());
        }
        return oldNonContact;
    }

    /**
     * @param nonContact
     * @return
     */
    public PhoneNumber createPhoneNumberFromNonContact(NonContact nonContact) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setJidNumber(nonContact.getJidNumber());
        phoneNumber.setName(nonContact.getRawNumber(mApplication));
        phoneNumber.setNameUnicode(nonContact.getRawNumber(mApplication));
        phoneNumber.setState(nonContact.getState());
        phoneNumber.setStatus(nonContact.getStatus());
        phoneNumber.setLastChangeAvatar(nonContact.getLAvatar());
        phoneNumber.setGender(nonContact.getGender());
        //        phoneNumber.setBirthDay(nonContact.getBirthDay());
        phoneNumber.setLastOnline(nonContact.getLastOnline());
        phoneNumber.setLastSeen(nonContact.getLastSeen());
        phoneNumber.setBirthdayString(nonContact.getBirthDayString());
        phoneNumber.setPermission(nonContact.getPermission());
        phoneNumber.setOperator(nonContact.getOperator());
        phoneNumber.setPreKey(nonContact.getPreKey());
        return phoneNumber;
    }

    public PhoneNumber createPhoneNumberFromNonContactForMember(NonContact nonContact) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setJidNumber(nonContact.getJidNumber());
//        phoneNumber.setName(nonContact.getRawNumber(mApplication));
//        phoneNumber.setNameUnicode(nonContact.getRawNumber(mApplication));
        phoneNumber.setName(nonContact.getNickName());
        phoneNumber.setNameUnicode(TextHelper.getInstant().convertUnicodeToAscci(nonContact.getNickName()));
        phoneNumber.setState(nonContact.getState());
        phoneNumber.setStatus(nonContact.getStatus());
        phoneNumber.setLastChangeAvatar(nonContact.getLAvatar());
        phoneNumber.setGender(nonContact.getGender());
        //        phoneNumber.setBirthDay(nonContact.getBirthDay());
        phoneNumber.setLastOnline(nonContact.getLastOnline());
        phoneNumber.setLastSeen(nonContact.getLastSeen());
        phoneNumber.setBirthdayString(nonContact.getBirthDayString());
        phoneNumber.setPermission(nonContact.getPermission());
        phoneNumber.setOperator(nonContact.getOperator());
        phoneNumber.setPreKey(nonContact.getPreKey());
        return phoneNumber;
    }

    public void initNonContact() {
        if (mListNonContacts == null) mListNonContacts = new ArrayList<>();
        /*synchronized (TAG) {
            mListNonContacts = mNonContactDataSource.getAllNonContact();
        }*/
    }

    public void insertOrUpdateNonContact(PhoneNumber phoneNumber, boolean updateLastTimeRequest) {
        insertOrUpdateNonContact(phoneNumber, updateLastTimeRequest, false);
    }

    public void insertOrUpdateNonContact(PhoneNumber phoneNumber, boolean updateLastTimeRequest, boolean isApi) {
        Log.i(TAG, "insertOrUpdateNonContact: " + phoneNumber.getJidNumber());
        String number = phoneNumber.getJidNumber();
        if (isApi && phoneNumber.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
            phoneNumber.setStatus(mContext.getResources().getString(R.string.system_block_status));
            phoneNumber.setName(mContext.getResources().getString(R.string.system_block_name));
            phoneNumber.setNickName(mContext.getResources().getString(R.string.system_block_name));
        }
        NonContact existNonContact = getExistNonContact(number);
        mApplication.getStrangerBusiness().updateStateBlockStrangerNumber(phoneNumber, existNonContact, isApi);
        if (existNonContact != null) { //non contact da co, update
            existNonContact = updateOldNonContactFromPhoneNumber(existNonContact, phoneNumber);
            if (updateLastTimeRequest)
                existNonContact.setTimeLastRequest(TimeHelper.getCurrentTime());
            mNonContactDataSource.updateNonContact(existNonContact);
        } else {
            insertNonContact(phoneNumber, updateLastTimeRequest);
        }
    }

    public void insertNonContact(PhoneNumber phoneNumber, boolean updateLastTimeRequest) {
        NonContact newNonContact = createNonContactFromPhoneNumber(phoneNumber);
        if (updateLastTimeRequest)
            newNonContact.setTimeLastRequest(TimeHelper.getCurrentTime());
        mNonContactDataSource.insertNonContact(newNonContact);
        synchronized (TAG) {
            mListNonContacts.add(newNonContact);
        }
    }

    public void insertOrUpdateListNonContact(ArrayList<PhoneNumber> phoneNumbers) {
        ArrayList<NonContact> listNonContactUpdates = new ArrayList<>();
        ArrayList<NonContact> listNonContactInserts = new ArrayList<>();
        for (PhoneNumber phoneNumber : phoneNumbers) {
//            phoneNumber.setIsViettel();
            String number = phoneNumber.getJidNumber();
            NonContact existNonContact = getExistNonContact(number);
            if (existNonContact != null) { //non contact da co, update
                existNonContact = updateOldNonContactFromPhoneNumber(existNonContact, phoneNumber);
                listNonContactUpdates.add(existNonContact);
            } else {
                NonContact newNonContact = createNonContactFromPhoneNumber(phoneNumber);
                listNonContactInserts.add(newNonContact);
            }
        }
        if (!listNonContactInserts.isEmpty()) {
            synchronized (TAG) {
                mNonContactDataSource.insertListNonContact(listNonContactInserts);
                mListNonContacts.addAll(listNonContactInserts);
            }
        }
        if (!listNonContactUpdates.isEmpty()) {
            mNonContactDataSource.updateListNonContact(listNonContactUpdates);
        }
    }

    public void updateListNonContact(ArrayList<NonContact> listNon) {
        if (!listNon.isEmpty()) {
            mNonContactDataSource.updateListNonContact(listNon);
        }
    }

    public void updateNonContact(NonContact nonContact) {
        mNonContactDataSource.updateNonContact(nonContact);
    }

    public NonContact getExistNonContact(String jidNumber) {
        synchronized (TAG) {
            if (TextUtils.isEmpty(jidNumber)) {
                return null;
            }
            for (NonContact nonContact : mListNonContacts) {
                if (jidNumber.equals(nonContact.getJidNumber())) {
                    return nonContact;
                }
            }
            // khong co tren mem thi check db
            NonContact dbContact = mNonContactDataSource.getNonContactFromNumber(jidNumber);
            if (dbContact != null) {
                Log.d(TAG, "getNonContactFromDB");
                mListNonContacts.add(dbContact);// add mem de lan sau ko phai doc db
                return dbContact;
            }
            return null;
        }
    }

    public void deleteNonContact(String jidNumber) {
        Log.d(TAG, "deleteNonContact");
        NonContact nonContact = getExistNonContact(jidNumber);
        if (nonContact != null) {
            mNonContactDataSource.deleteNonContact(nonContact);
            mListNonContacts.remove(nonContact);
        }
    }

    public void sendIQAfterResponseContactInfo() {
        Log.d(TAG, "sendIQAfterResponseContactInfo");
        XMPPManager mXmppManager = mApplication.getXmppManager();
        // khong co mang khong co ket noi xmpp
        if (mXmppManager == null) {
            return;
        }
        IQInfo iqResultContact = new IQInfo(IQInfo.NAME_SPACE_CONTACT);
        iqResultContact.setType(IQ.Type.SET);
        mXmppManager.sendXmppPacket(iqResultContact);
    }

    public PhoneNumber getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(PhoneNumber mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }

    public void notifyNewUser(String newUser, String msg, boolean isFake) {
        if (TextUtils.isEmpty(newUser)) {
            return;
        }
        SettingBusiness settingBusiness = SettingBusiness.getInstance(mApplication);
        // cai dat cho phep thong bao khi co nguoi dung moi
        if (settingBusiness != null && settingBusiness.getPrefSettingNotifyNewUser()) {
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (myNumber != null && myNumber.equals(newUser)) {// nguoi dung moi la chinh minh
                return;
            }
            if (mApplication.isDataReady()) {
                if (!isFake) {
                    getInfoNewUser(newUser);
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
                PhoneNumber phoneNumberNewUser = getPhoneNumberFromNumber(newUser);
                if (phoneNumberNewUser != null) {
                    ReengNotificationManager notificationManager = ReengNotificationManager.getInstance(mApplication);
                    if (msg.contains("%1$s")) {
                        msg = String.format(msg, phoneNumberNewUser.getName());
                    } else {
                        msg = phoneNumberNewUser.getName() + msg;
                    }
                    notificationManager.drawNewUserNotification(mApplication, phoneNumberNewUser, phoneNumberNewUser
                            .getJidNumber(), msg);
                }
            }
        }
    }

    public void notifySuggestSms(String friendJid, String msg) {
        if (TextUtils.isEmpty(friendJid) || TextUtils.isEmpty(msg)) return;
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (myNumber != null && myNumber.equals(friendJid)) {//la chinh minh
            return;
        }
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(mApplication.getPhoneUtil(),
                        friendJid, mApplication.getReengAccountBusiness().getRegionCode());
        if (phoneNumberProtocol != null) {
            friendJid = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
            if (PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(), phoneNumberProtocol)) {
                ReengNotificationManager notificationManager = ReengNotificationManager.getInstance(mApplication);
                notificationManager.drawSuggestSmsNotification(mApplication, friendJid, msg);
            }
        }
    }

    public void navigateToAddContact(BaseSlidingFragmentActivity activity, String number, String name) {
        if (PermissionHelper.declinedPermission(activity, Manifest.permission.WRITE_CONTACTS)) {
            PermissionHelper.requestPermissionWithGuide(activity,
                    Manifest.permission.WRITE_CONTACTS,
                    Constants.PERMISSION.PERMISSION_REQUEST_SAVE_CONTACT);
        } else {
            String numberSave = PhoneNumberHelper.getInstant().
                    formatNumberBeforeSaveContact(mApplication.getPhoneUtil(),
                            number, mApplication.getReengAccountBusiness().getRegionCode());
            // luu contact
            try {
                ContentObserverBusiness.getInstance(activity).setAction(Constants.ACTION.ADD_CONTACT);
                Intent addContactIntent = new Intent(Intent.ACTION_INSERT);
                addContactIntent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                if (!TextUtils.isEmpty(name)) {
                    addContactIntent.putExtra(ContactsContract.Intents.Insert.NAME, name);
                }
                if (!TextUtils.isEmpty(numberSave)) {
                    addContactIntent.putExtra(ContactsContract.Intents.Insert.PHONE, numberSave);
                }
                addContactIntent.putExtra("finishActivityOnSaveCompleted", true);
                activity.setActivityForResult(true);
                activity.setTakePhotoAndCrop(true);
                activity.startActivityForResult(addContactIntent, Constants.ACTION.ADD_CONTACT, true);
            } catch (Exception e) {
                activity.showToast(R.string.e667_device_not_support_function);
                Log.e(TAG, "Exception", e);
                ContentObserverBusiness.getInstance(activity).setAction(-1);
            }
        }
    }

    public int getCStateMessage(ThreadMessage thread) {
        if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            return getCStateMessage(thread.getSoloNumber());
        }
        return -1;
    }

    public int getCStateMessage(String jidNumber) {
        PhoneNumber phone = getPhoneNumberFromNumber(jidNumber);
        NonContact nonContact = null;
        if (phone == null) {
            nonContact = getExistNonContact(jidNumber);
        }
        return getCStateMessage(phone, nonContact);
    }

    public int getCStateMessage(PhoneNumber phoneNumber, NonContact nonContact) {
        int state = -1;
        if (phoneNumber != null) {
            if (phoneNumber.getState() == Constants.CONTACT.ACTIVE) {
                state = 1;
            } else {
                state = 0;
            }
        } else if (nonContact != null && nonContact.getTimeLastRequest() != -1) {
            if (nonContact.getState() == Constants.CONTACT.ACTIVE) {
                state = 1;
            } else {
                state = 0;
            }
        }
        return state;
    }

    public String getFriendBirthdayStr(String friendJid) {
        PhoneNumber mPhoneNumber = getPhoneNumberFromNumber(friendJid);
        if (mPhoneNumber != null) {
            return mPhoneNumber.getBirthdayString();
        } else {
            NonContact mNonContact = getExistNonContact(friendJid);
            if (mNonContact != null) {
                return mNonContact.getBirthDayString();
            }
        }
        return "";
    }

    public ArrayList<PhoneNumber> getRecentPhoneNumber() {
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        if (messageBusiness.getThreadMessageArrayList() == null ||
                messageBusiness.getThreadMessageArrayList().isEmpty()) {
            return null;
        } else {
            ContactBusiness contactBusiness = mApplication.getContactBusiness();
            ArrayList<ThreadMessage> listThreads = new ArrayList<>();
            ArrayList<PhoneNumber> numbers = new ArrayList<>();
            for (ThreadMessage threadMessage : messageBusiness.getThreadMessageArrayList()) {
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                        && threadMessage.isReadyShow(messageBusiness)) {
                    listThreads.add(threadMessage);
                }
            }
            if (!listThreads.isEmpty()) {
                Collections.sort(listThreads, ComparatorHelper.getComparatorThreadMessageByLastTime());
                int size = 0;
                for (ThreadMessage thread : listThreads) {
                    if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && !thread.isStranger()) {
                        PhoneNumber number = contactBusiness.getPhoneNumberFromNumber(thread.getSoloNumber());
                        if (number == null) {
                            NonContact nonContact = getExistNonContact(thread.getSoloNumber());
                            if (nonContact != null) {
                                if (nonContact.getState() != Constants.CONTACT.SYSTEM_BLOCK) {
                                    number = createPhoneNumberFromNonContact(nonContact);
                                    number.setContactId("-1");
                                }
                            } else {
                                number = new PhoneNumber();
                                number.setJidNumber(thread.getSoloNumber());
                                number.setName(thread.getSoloNumber());
                                number.setContactId("-1");
                            }
                        }
                        if (number != null) {
                            numbers.add(number);
                            size++;
                            if (size >= 5) {
                                break;
                            }
                        }
                    }
                }
            }
            return numbers;
        }
    }

    public void clearMemoryDataContact() {
        mListContacts = null;
        mListNumbers = null;
    }

    public void mergeContact() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                ContactsSyncAdapter contactsSyncAdapter = new ContactsSyncAdapter(mApplication, true);
                try {
                    contactsSyncAdapter.fetchAndSaveContacts(mApplication.getContentResolver());
                } catch (RemoteException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void addOptionMochaToListContact(final ArrayList<PhoneNumber> listNumber) {
        if (Config.Features.FLAG_SUPPORT_MERGE_CONTACT)
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... voids) {
                    ContactsSyncAdapter contactsSyncAdapter = new ContactsSyncAdapter(mApplication, true);
                    try {
                        for (PhoneNumber phoneNumber : listNumber)
                            contactsSyncAdapter.addNewContact(mApplication.getContentResolver(), phoneNumber.getContactId(), phoneNumber.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public static Bitmap retrieveContactPhoto(Context context, String number) {
        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        Bitmap photo = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.default_image);

        try {
            if(contactId != null) {
                InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactId)));

                if (inputStream != null) {
                    photo = BitmapFactory.decodeStream(inputStream);
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo;
    }
}
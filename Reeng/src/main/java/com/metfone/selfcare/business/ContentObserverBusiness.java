package com.metfone.selfcare.business;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.Contact;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 7/10/14.
 */
public class ContentObserverBusiness {
    private final String TAG = ContentObserverBusiness.class.getSimpleName();
    private static ContentObserverBusiness mInstance;
    private ContactContentObserver mContactContentObserver;
    private Context mContext;
    private int mAction = 0;
    private SyncContactAsyncTask mSyncAsyncTask;
    private InsertNewContactAsyncTask mInsertAsyncTask;
    private InitFavoriteListAsyncTask mInitAsyncTask;
    private ApplicationController mApplication;
    private ContactBusiness mContactBusiness;

    public static synchronized ContentObserverBusiness getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ContentObserverBusiness(context);
        }
        return mInstance;
    }

    public ContentObserverBusiness(Context context) {
        this.mContext = context;
        mApplication = (ApplicationController) mContext.getApplicationContext();
        mContactBusiness = mApplication.getContactBusiness();
    }

    public int getAction() {
        return mAction;
    }

    public void setAction(int action) {
        this.mAction = action;
    }

    /**
     * dang ky contentProvider
     *
     * @author toanvk2
     */
    public void registerContactObserver() {
        if (mContactContentObserver == null) {
            mContactContentObserver = new ContactContentObserver(new Handler());
            Log.i(TAG, "registerContactProvider---contactProvider == null");
        }
        // Uri uri2 = ContactsContract.Contacts.CONTENT_URI;
        if (PermissionHelper.allowedPermission(mApplication, Manifest.permission.WRITE_CONTACTS)) {
            mContext.getContentResolver().registerContentObserver(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, true, mContactContentObserver);
        }
        //content://com.android.contacts/data/phones
        //content://com.android.contacts/contacts
    }

    /**
     * huy dang ky contentProvider
     *
     * @author toanvk2
     */
    public void unRegisterContactObserver() {
        if (mContactContentObserver != null) {
            Log.i(TAG, "unRegisterContactProvider---contactProvider != null");
            mContext.getApplicationContext().getContentResolver()
                    .unregisterContentObserver(mContactContentObserver);
        }
    }

    /**
     * content observer
     */
    public class ContactContentObserver extends ContentObserver {
        private final String TAG = ContactContentObserver.class.getSimpleName();

        public ContactContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            //            if (Build.VERSION.SDK_INT < 16)
            //                onChangeContact(null);
            Log.d(TAG, " onChange selfChange: -> " + selfChange + " mAction = " + mAction);
            switch (mAction) {
                case Constants.ACTION.ADD_CONTACT:
                    insertNewContact();
                    break;
                case Constants.ACTION.EDIT_CONTACT:
                    break;
                case Constants.ACTION.DEL_CONTACT:
                    break;
                case Constants.ACTION.CHANGE_FAVORITE:
                    initFavoriteList();
                    break;
                default:
                    syncContact();
                    break;
            }
            super.onChange(selfChange);
        }

        // TODO đã để min sdk = 16 rồi, có thể tối ưu luồng contact change
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onChange(boolean selfChange, Uri uri) {
            //    onChangeContact(uri);
            super.onChange(selfChange, uri);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void insertNewContact() {
        if (!mContactBusiness.isInsertContact()) {
            if (mInsertAsyncTask != null) {
                mInsertAsyncTask.cancel(true);
                mInsertAsyncTask = null;
            }
            mInsertAsyncTask = new InsertNewContactAsyncTask();
            mInsertAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private void syncContact() {
        if (mContactBusiness.isContactReady() && !mContactBusiness.isSyncContact()) {
            if (mSyncAsyncTask != null) {
                mSyncAsyncTask.cancel(true);
                mSyncAsyncTask = null;
            }
            mSyncAsyncTask = new SyncContactAsyncTask();
            mSyncAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }


    private void initFavoriteList() {
        if (mInitAsyncTask != null) {
            mInitAsyncTask.cancel(true);
            mInitAsyncTask = null;
        }
        mInitAsyncTask = new InitFavoriteListAsyncTask();
        mInitAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class InsertNewContactAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Contact contactInsert;
            Log.d(TAG, "insertNewContactLastId->: ");
            contactInsert = mContactBusiness.insertNewContactLastId();
            if (contactInsert != null) {
                mApplication.getMessageBusiness().updateThreadStrangerAfterSyncContact();
                mContactBusiness.insertContactToDB(contactInsert, contactInsert.getListNumbers());
            }
            return null;
        }
    }

    private class SyncContactAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.d(TAG, " SyncContact sync: -> ");
            mContactBusiness.syncContact();
            mContactBusiness.initArrayListPhoneNumber();
            mApplication.getMessageBusiness().updateThreadStrangerAfterSyncContact();
            return null;
        }
    }

    private class InitFavoriteListAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            mContactBusiness.initFavoriteListPhoneNumber();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ListenerHelper.getInstance().onContactChanged();
            mAction = -1;
            super.onPostExecute(aVoid);
        }
    }
}
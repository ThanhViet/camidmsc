package com.metfone.selfcare.business;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.database.datasource.StickerDataSource;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerCollectionResponse;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.database.model.StoreInfo;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.network.fileTransfer.DownloadGifAsyncTask;
import com.metfone.selfcare.task.DownloadStickerAsynctask;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by thaodv on 10-Feb-15.
 */
public class StickerBusiness implements DownloadStickerAsynctask.DownloadStickerCallBack {
    private static final String TAG = StickerBusiness.class.getSimpleName();
    private static final int RECENT_MAX_SIZE = 20;
    private ApplicationController mApplicationController;
    private StickerDataSource mStickerDataSource;
    private List<StickerItem> allStickerItem;
    private HashMap<Integer, ArrayList<StickerItem>> hashMapStickerItem;// listSticker item bt collection id
    private List<StickerCollection> allStickerCollection; // tat ca collection trong db
    private List<StickerCollection> listStickerCollectionEnable;//tat cac collection chua delete, dung hien trong store
    private List<StickerCollection> listStickyStickerCollection;//cac collection duoc download hoac sticky va ko phai default
    private ArrayList<StickerItem> listRecentSticker = new ArrayList<>();
    private StickerCollection defaultCollection;
    private SharedPreferences mPref;
    private List<Integer> newStickerList;
    private long lastTimeGetCollectionInfo;
    private boolean isDownloadingDefaultCollection;
    private int failCount = 0;
    private HashSet<Integer> currentDownloadIds = new HashSet<>();

    public StickerBusiness(ApplicationController app) {
        mApplicationController = app;
        this.mPref = mApplicationController.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    public void init() {
        mStickerDataSource = StickerDataSource.getInstance(mApplicationController);
        lastTimeGetCollectionInfo = mPref.getLong(Constants.PREFERENCE.CONFIG.TIMESTAMP_GET_LIST_STICKER, -1L);
    }

    public void loadAllSticker() {
        allStickerCollection = mStickerDataSource.getAllStickerCollection();
        // init list item
        initStickerItem();
        // init recent stiker
        initStickerRecent();
        initListCollectionEnable();
        getNewStickerFromPref();
    }

    public List<StickerCollection> getListStickyStickerCollection() {
        return listStickyStickerCollection;
    }

    private void initListCollectionEnable() {
        List<StickerCollection> listEnables = new ArrayList<>();
        listStickyStickerCollection = new ArrayList<>();
        for (StickerCollection collection : allStickerCollection) {
            if (collection.isDefault() == 0) {
                if (collection.getCollectionState() != StickerConstant.COLLECTION_STATE_DELETED) {
                    if (collection.getCollectionState() == StickerConstant.COLLECTION_STATE_ENABLE ||
                            (collection.getCollectionState() == StickerConstant.COLLECTION_STATE_DISABLE && collection.isDownloaded())) {
                        listEnables.add(collection);
                        if (collection.isSticky() == 1 || collection.isDownloaded()) {
                            listStickyStickerCollection.add(collection);
                        }
                    }
                }
            } else {
                defaultCollection = collection;
            }
        }
        // sap xep bo sticky
        Collections.sort(listStickyStickerCollection, ComparatorHelper.getComparatorStickerCollectionByLastSticky());
        // sap xep bo trong store
        Collections.sort(listEnables, ComparatorHelper.getComparatorStickerCollectionByOrder());
        listStickerCollectionEnable = listEnables;
    }

    public List<StickerCollection> getAllEnabledStickerCollection() {
        return listStickerCollectionEnable;
    }

    public void updateStickerCollection(StickerCollection stickerCollection) {
        mStickerDataSource.updateStickerCollection(stickerCollection);
        allStickerCollection = mStickerDataSource.getAllStickerCollection();
        initListCollectionEnable();
    }

    public void updateStickerCollectionEnable(StickerCollection stickerCollection) {
        allStickerCollection = mStickerDataSource.getAllStickerCollection();
        initListCollectionEnable();
    }

    public List<StickerCollection> getAllDowloadedSticker() {
        List<StickerCollection> listSticker = new ArrayList<>();
        for (StickerCollection sticker : listStickerCollectionEnable) {
            if (sticker.isDownloaded()) {
                listSticker.add(sticker);
            }
        }
        return listSticker;
    }

    public List<StickerCollection> getAllNotDowloadSticker() {
        List<StickerCollection> listSticker = new ArrayList<>();
        for (StickerCollection sticker : listStickerCollectionEnable) {
            if (!sticker.isDownloaded() || sticker.isUpdateCollection()) {
                listSticker.add(sticker);
            }
        }
        return listSticker;
    }

    public ArrayList<StickerItem> getListRecentSticker() {
        return listRecentSticker;
    }

    public void getStickerCollectionFromServer(boolean isForceToGet) {
        if (isForceToGet) {     //bat phai update
            mPref.edit().putBoolean(Constants.PREFERENCE.CONFIG.PREF_FORCE_GET_STICKER_NOT_DONE, true).apply();
        } else {        //neu chua update thanh cong thi bat update
            if (mPref.getBoolean(Constants.PREFERENCE.CONFIG.PREF_FORCE_GET_STICKER_NOT_DONE, false)) {
                isForceToGet = true;
            }
        }
        if (TimeHelper.checkTimeInDay(lastTimeGetCollectionInfo) && !isForceToGet) {
            return;
        }
        ReengAccountBusiness accountBusiness = mApplicationController.getReengAccountBusiness();
        if (accountBusiness.isValidAccount() && NetworkHelper.isConnectInternet(mApplicationController)) {
            ReengAccount account = accountBusiness.getCurrentAccount();
            String numberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
            long currentTime = TimeHelper.getCurrentTime();
            String encrypt = HttpHelper.EncoderUrl(HttpHelper.encryptData(mApplicationController,
                    account.getJidNumber(), account.getToken(), currentTime));
            String url = String.format(UrlConfigHelper.getInstance(mApplicationController).
                            getUrlConfigOfFile(Config.UrlEnum.GET_ALL_STICKER), numberEncode, String.valueOf(currentTime),
                    encrypt, account.getRegionCode());
            Log.d(TAG, "url: " + url);
            StringRequest request = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d(TAG, "onResponse: " + response);
                                processGetCollectionResponse(response);
                            } catch (JsonSyntaxException e) {
                                Log.e(TAG, "JsonSyntaxException", e);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error:", error);
                }
            });
            VolleyHelper.getInstance(mApplicationController).addRequestToQueue(request, TAG, false);
        }
    }

    private void processGetCollectionResponse(String response) throws JsonSyntaxException {
        StickerCollectionResponse collectionResponse = new Gson().fromJson(response, StickerCollectionResponse.class);
        if (collectionResponse != null && HTTPCode.E200_OK == collectionResponse.getCode()) {
            mergeListCollection(allStickerCollection, collectionResponse.getStickerCollection());
            processHiddenAndRemovedCollections(collectionResponse.getStoreInfo());
            initListCollectionEnable();
            try {
                getNewStickerListFromJson(new JSONObject(response));
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    private void processHiddenAndRemovedCollections(StoreInfo storeInfo) {
        if (storeInfo == null) return;
        if (allStickerCollection == null || allStickerCollection.isEmpty()) {
            return;
        }
        ArrayList<Integer> hiddenCollections = storeInfo.getHiddenCollections();
        ArrayList<Integer> removedCollections = storeInfo.getRemovedCollections();
        ArrayList<StickerCollection> updateCollections = new ArrayList<>();
        for (StickerCollection collection : allStickerCollection) {
            if (collection == null) continue;
            if (removedCollections != null && removedCollections.contains(collection.getServerId())) {
                // bo sticker trc do chua chuyen sang delete
                if (collection.getCollectionState() != StickerConstant.COLLECTION_STATE_DELETED) {
                    collection.setCollectionState(StickerConstant.COLLECTION_STATE_DELETED);
                    updateCollections.add(collection);
                }
                // neu khong trong danh sach remove thi check ds hidden
            } else if (hiddenCollections != null && hiddenCollections.contains(collection.getServerId())) {
                if (collection.getCollectionState() != StickerConstant.COLLECTION_STATE_DISABLE) {
                    collection.setCollectionState(StickerConstant.COLLECTION_STATE_DISABLE);
                    updateCollections.add(collection);
                }
            } else {// neu khong co trong ca 2 danh sach tren thi set enable
                if (collection.getCollectionState() != StickerConstant.COLLECTION_STATE_ENABLE) {
                    collection.setCollectionState(StickerConstant.COLLECTION_STATE_ENABLE);
                    updateCollections.add(collection);
                }
            }
        }
        mStickerDataSource.updateListStickerCollection(updateCollections);
    }

    public StickerCollection getStickerCollectionById(long collectionId) {
        if (allStickerCollection == null || allStickerCollection.isEmpty()) {
            return null;
        }
        for (StickerCollection stickerCollection : allStickerCollection) {
            if (stickerCollection != null && stickerCollection.getServerId() == collectionId) {
                return stickerCollection;
            }
        }
        return null;
    }

    public void insertStickerCollection(StickerCollection stickerCollection) {
        mStickerDataSource.insertStickerCollection(stickerCollection);
        updateStickerCollectionEnable(stickerCollection);
    }

    // recent sticker
    private void initStickerRecent() {
//        int sizeRecentDefault = EmoticonUtils.RECENT_STICKER_DEFAULT.length;
        ArrayList<StickerItem> stickerRecentDefaults = new ArrayList<>();
        /*stickerRecentDefaults.addAll(Arrays.asList(EmoticonUtils.RECENT_STICKER_DEFAULT).
                subList(0, sizeRecentDefault));*/
        ArrayList<StickerItem> stickerRecentDatabases = getAllStickerRecentFromDB();
        ArrayList<StickerItem> listInValidates = new ArrayList<>();
        ArrayList<StickerItem> listValidates = new ArrayList<>();
        for (StickerItem item : stickerRecentDatabases) {
            if (item.getCollectionId() == -1 || item.getCollectionId() == 47 || item.getCollectionId() == 73) {
                listInValidates.add(item);
            } else {
                listValidates.add(item);
            }
        }
        if (!listInValidates.isEmpty()) {
            mStickerDataSource.removeListStickerRecent(listInValidates);
        }
        // neu DB trong thi lay luon danh sach mac dinh
        if (listValidates.isEmpty()) {
            listRecentSticker = stickerRecentDefaults;
        } else {
            listRecentSticker = new ArrayList<>();
            Collections.sort(listValidates, ComparatorHelper.getComparatorStickerRecent());
            // neu danh sach trong db >= max size thi cat tu 0 den maxsize -1 list db
            if (listValidates.size() >= RECENT_MAX_SIZE) {
                listRecentSticker.addAll(listValidates.subList(0, RECENT_MAX_SIZE));
            } else {// neu danh sach trong db < maxsize thi append them 1 so item default vao cuoi
                int sizeAppend = RECENT_MAX_SIZE - listValidates.size();
                // xoa bo nhung item giong nhau
                stickerRecentDefaults.removeAll(listValidates);
                if (!stickerRecentDefaults.isEmpty()) {
                    // so item can append nho hon so icon default thi cat du so item can thiet
                    if (stickerRecentDefaults.size() > sizeAppend) {
                        listRecentSticker.addAll(stickerRecentDefaults.subList(0, sizeAppend));
                    } else {// so item can them lon hon tong so item trong bo default thi add ca
                        listRecentSticker.addAll(stickerRecentDefaults);
                    }
                }
                listRecentSticker.addAll(0, listValidates);
            }
        }
    }

    private void insertStickerRecent(StickerItem item) {
        long id = mStickerDataSource.insertStickerRecent(item);
        if (id != -1) {
            item.setId(id);
        }
    }

    private StickerItem getExistStickerRecent(StickerItem newItem) {
        if (listRecentSticker == null || listRecentSticker.isEmpty()) {
            return null;
        }
        for (StickerItem item : listRecentSticker) {
            if (item.equals(newItem)) {
                return item;
            }
        }
        return null;
    }

    /**
     * cap nhat hoac insert stickerRecent
     *
     * @param itemId
     * @param collectionId
     * @return success
     */
    public boolean insertOrUpdateIfExistStickerRecent(int itemId, int collectionId) {
        if (collectionId == -1 || collectionId == 73 || collectionId == 47) {// bo default duoi client hoac bo sinh nhat thi ko them vao recent
            return false;
        }
        StickerItem newItem = new StickerItem();
        newItem.setCollectionId(collectionId);
        newItem.setItemId(itemId);
        // kiem tra xem sticker nay da co trong list recent chua
        StickerItem oldItem = getExistStickerRecent(newItem);
        // da co roi
        if (oldItem != null) {
            // truong hop old item la 1 item default chua luu db
            if (oldItem.getRecentTime() == -1) {
                oldItem.setRecentTime(TimeHelper.getCurrentTime());
                insertStickerRecent(oldItem);
            } else {// truong hop ko phai item default
                oldItem.setRecentTime(TimeHelper.getCurrentTime());
                mStickerDataSource.updateStickerRecent(oldItem);
            }
        } else {// chua co
            newItem.setRecentTime(TimeHelper.getCurrentTime());
            // insert db
            insertStickerRecent(newItem);
            // list recent full, xoa cu nhat, them cai moi
            if (listRecentSticker.size() >= RECENT_MAX_SIZE) {
                StickerItem itemRemove = listRecentSticker.get(listRecentSticker.size() - 1);
                // xoa db
                mStickerDataSource.removeStickerRecent(itemRemove);
                // xoa mem
                listRecentSticker.remove(itemRemove);
            }
            listRecentSticker.add(newItem);
        }
        Collections.sort(listRecentSticker, ComparatorHelper.getComparatorStickerRecent());
        return true;
    }


    // merge list get from server and list get from db
    public void mergeListCollection(List<StickerCollection> localLists, List<StickerCollection> serverLists) {
        if (localLists == null || localLists.isEmpty()) {
            //lan dau tien get sticker thanh cong
            if (serverLists != null && !serverLists.isEmpty()) {
                // insert all list server to mem and db
                allStickerCollection = serverLists;
                for (StickerCollection collection : allStickerCollection) {
                    // insert bo sticker moi co sticky thi set truong last sticky luon
                    if (collection.isSticky() == 1 && !collection.isDownloaded()) {
                        collection.setLastSticky(TimeHelper.getCurrentTime());
                    }
                    collection.setIsNew(true);
                    //do thoi gian server co the lech voi client nen client se dung thoi
                    //gian o server de danh dau lan cap nhat cuoi cung
                    collection.setLastLocalUpdate(collection.getLastServerUpdate() + 1);
                    collection.setLastInfoUpdate(collection.getLastInfoUpdate() + 1);
                    if (collection.isDefault() == 1) {
                        defaultCollection = collection;
                        startDownloadDefaultCollection();
                    }
                }
                mStickerDataSource.insertListStickerCollection(serverLists);
            }
        } else if (serverLists == null || serverLists.isEmpty()) {
            // danh dau disable sticker collection
            for (StickerCollection collection : localLists) {
                collection.setCollectionState(StickerConstant.COLLECTION_STATE_DISABLE);
            }
            mStickerDataSource.updateListStickerCollection(localLists);
        } else {
            //danh sach delete khong bao gom cac collection ma server tra ve
            HashSet<StickerCollection> hashSetDelete = new HashSet<>(localLists);
            HashSet<StickerCollection> hashSetServer = new HashSet<>(serverLists);
            hashSetDelete.removeAll(hashSetServer);
            //danh sach insert khong bao gom cac collection da co o local
            HashSet<StickerCollection> hashSetInsert = new HashSet<>(serverLists);
            HashSet<StickerCollection> hashSetLocal = new HashSet<>(localLists);
            hashSetInsert.removeAll(hashSetLocal);
            //danh sach update gom cac bo o local ma khong bi delete
            HashSet<StickerCollection> hashSetUpdate = new HashSet<>(serverLists);
            hashSetUpdate.removeAll(hashSetDelete);
            // cap nhat nhung sticker da bi disable
            //cai nao bi disable roi thi ko bat lai nua, chi co the vao store
            if (!hashSetUpdate.isEmpty()) {
                updatePropertiesOfCollections(hashSetUpdate);
            }
            // them moi collection sticker
            if (!hashSetInsert.isEmpty()) {
                List<StickerCollection> listInsert = new ArrayList<>(hashSetInsert);
                for (StickerCollection collection : listInsert) {
                    // insert bo sticker moi co sticky thi set truong last sticky luon
                    if (collection.isSticky() == 1 && !collection.isDownloaded()) {
                        collection.setLastSticky(TimeHelper.getCurrentTime());
                    }
                    collection.setIsNew(true);
                    collection.setLastLocalUpdate(collection.getLastServerUpdate() + 1);
                    collection.setLastInfoUpdate(collection.getLastInfoUpdate() + 1);
                }
                allStickerCollection.addAll(listInsert);
                mStickerDataSource.insertListStickerCollection(listInsert);
            }
        }
    }

    private void updatePropertiesOfCollections(HashSet<StickerCollection> hashSetUpdate) {
        HashMap<Integer, StickerCollection> stickerCollectionHashMap =
                new HashMap<>();
        for (StickerCollection collection : allStickerCollection) {
            stickerCollectionHashMap.put(collection.getServerId(), collection);
        }
        ArrayList<StickerCollection> collectionArrayList = new ArrayList<>();
        for (StickerCollection serverCollection : hashSetUpdate) {
            StickerCollection localCollection = stickerCollectionHashMap.get(serverCollection.getServerId());
            if (localCollection == null) continue;
            boolean needToUpdateAttribute = false, needToUpdateItem = false;
            if (isDifferentAttributeCollection(localCollection, serverCollection)) {
                copyAttribute(localCollection, serverCollection);
                needToUpdateAttribute = true;
            }
            if (localCollection.getLastLocalUpdate() < serverCollection.getLastServerUpdate()) {
                Log.i(TAG, "need to update item of collection " + localCollection.getServerId());
                //luu vao truong lastServerUpdate de so sanh voi lastLocalUpdate
                localCollection.setLastServerUpdate(serverCollection.getLastServerUpdate());
                needToUpdateItem = true;
            }
            if (localCollection.isDefault() == 1) {
                defaultCollection = localCollection;
                if (defaultCollection.isDownloaded()) {
                    if (needToUpdateItem) {
                        defaultCollection.setLastLocalUpdate(serverCollection.getLastServerUpdate() + 1);
                        startDownloadDefaultCollection();
                    }
                } else {
                    defaultCollection.setLastLocalUpdate(serverCollection.getLastServerUpdate() + 1);
                    startDownloadDefaultCollection();
                }
            }
            if (needToUpdateAttribute) {
                collectionArrayList.add(localCollection);
            } else if (needToUpdateItem && localCollection.isDefault() != 1) {
                collectionArrayList.add(localCollection);
            }
        }
        if (!collectionArrayList.isEmpty()) {
            mStickerDataSource.updateListStickerCollection(collectionArrayList);
        }
    }

    public StickerCollection getStickerCollectionFromStoreById(long collectionID) {
        if (allStickerCollection == null || allStickerCollection.isEmpty()) {
            return null;
        }
        for (StickerCollection stickerCollection : allStickerCollection) {
            if (stickerCollection != null && stickerCollection.getServerId() == collectionID) {
                return stickerCollection;
            }
        }
        return null;
    }

    private ArrayList<StickerItem> getAllStickerRecentFromDB() {
        return mStickerDataSource.getAllStickerRecent();
    }

    ///////////////////// sticker item ///////////////
    public void initStickerItem() {
        allStickerItem = mStickerDataSource.getAllStickerItem();
        hashMapStickerItem = new HashMap<>();
        if (allStickerItem != null && !allStickerItem.isEmpty()) {
            initHashMapStickerItem(allStickerItem);
        }
        // kiem tra xem co sticker item chua, chua co thi gen va insert db
        if (allStickerCollection != null && !allStickerCollection.isEmpty()) {
            List<StickerCollection> collectionNotExists = new ArrayList<>();
            for (StickerCollection stickerCollection : allStickerCollection) {
                // da download va chua
                if (stickerCollection.isDownloaded() &&
                        !hashMapStickerItem.containsKey(stickerCollection.getServerId())) {
                    collectionNotExists.add(stickerCollection);
                }
            }
            // neu co bo sticker da download ma chua insert list item vao db thi gen moi
            if (!collectionNotExists.isEmpty()) {
                createAndInsertStickerItemsByListCollection(collectionNotExists);
            }
        }
    }

    private void initHashMapStickerItem(List<StickerItem> stickerItems) {
        HashMap<Integer, ArrayList<StickerItem>> hashMap = new HashMap<>();
        for (StickerItem stickerItem : stickerItems) {
            ArrayList<StickerItem> list;
            int key = stickerItem.getCollectionId();
            if (hashMap.containsKey(key)) {
                list = hashMap.get(key);
            } else {
                list = new ArrayList<>();
                hashMap.put(key, list);
            }
            list.add(stickerItem);
        }
        hashMapStickerItem = hashMap;
    }

    /**
     * tu tao list item insert vao db
     *
     * @param collections
     */
    private void createAndInsertStickerItemsByListCollection(List<StickerCollection> collections) {
        List<StickerItem> listInsertStickerItem = new ArrayList<>();
        for (StickerCollection collection : collections) {
            for (int i = 1; i <= collection.getNumberSticker(); i++) {
                StickerItem stickerItem = new StickerItem();
                stickerItem.setItemId(i);
                stickerItem.setCollectionId(collection.getServerId());
                StringBuilder sb = new StringBuilder();
                sb.append(Config.Storage.REENG_STORAGE_FOLDER).append(Config.Storage.STICKER_FOLDER).
                        append(collection.getServerId()).append("/");
                stickerItem.setImagePath(sb.toString() + i + ".png");
                stickerItem.setVoicePath(sb.toString() + i + ".mp3");
                stickerItem.setDownloadImg(true);
                stickerItem.setDownloadVoice(true);
                listInsertStickerItem.add(stickerItem);
            }
        }
        // insert
        if (!listInsertStickerItem.isEmpty()) {
            // insert db lay id
            mStickerDataSource.insertListStickerItem(listInsertStickerItem);
            allStickerItem.addAll(listInsertStickerItem);
            initHashMapStickerItem(allStickerItem);
        }
    }

    public ArrayList<StickerItem> getListStickerItemByCollectionId(int collectionId) {
        if (hashMapStickerItem == null) {
            initHashMapStickerItem(allStickerItem);
        }
        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
            return new ArrayList<>(Arrays.asList(EmoticonUtils.getQuickReplyStickerDefault()));
        }
        ArrayList<StickerItem> listItems = hashMapStickerItem.get(collectionId);
        if (listItems != null && !listItems.isEmpty()) {
            /*HashSet<StickerItem> noConflictItems = new HashSet<StickerItem>(listItems);
            ArrayList<StickerItem> listNoConflict = new ArrayList<StickerItem>(noConflictItems);*/
            Collections.sort(listItems, ComparatorHelper.getComparatorStickerItem());
            return listItems;
        } else {
            return new ArrayList<>();
        }
    }

    public StickerItem getStickerItem(int collectionId, int itemId) {
        List<StickerItem> listStickerItem = hashMapStickerItem.get(collectionId);
        if (listStickerItem != null && !listStickerItem.isEmpty()) {
            for (StickerItem stickerItem : hashMapStickerItem.get(collectionId)) {
                if (stickerItem.getItemId() == itemId) {
                    return stickerItem;
                }
            }
        }
        return null;
    }

    public void insertNewStickerItem(StickerItem stickerItem) {
        mStickerDataSource.insertStickerItem(stickerItem);
        allStickerItem.add(stickerItem);
        initHashMapStickerItem(allStickerItem);
    }

    /**
     * cap nhat database
     *
     * @param stickerItem
     */
    public void updateStickerItem(StickerItem stickerItem) {
        mStickerDataSource.updateStickerItem(stickerItem);
    }

    public void deleteStickerCollection(StickerCollection collection) {
        // khong phai sticky, ko phai default
        if (collection.isSticky() != 1 && collection.isDefault() != 1) {
            mApplicationController.getStickerBusiness().removeCollectionId(collection.getServerId());
        }
        collection.setDownloaded(false);
        collection.setLastSticky(0);
        collection.setIsNew(true);
        updateStickerCollection(collection);
    }

    /**
     * update, insert sticker item
     *
     * @param collectionId
     * @param listStickerItems
     */
    private void processStickerInfoAfterGetFileConfig(int collectionId, ArrayList<StickerItem> listStickerItems) {
        if (collectionId < 0 || listStickerItems == null || listStickerItems.isEmpty()) {
            return;
        }
        ArrayList<StickerItem> listStickerItemsOld = hashMapStickerItem.get(collectionId);
        if (listStickerItemsOld == null || listStickerItemsOld.isEmpty()) {// chua co trong db, insert moi
            // insert db lay id
            mStickerDataSource.insertListStickerItem(listStickerItems);
            // tao lai du lieu
            allStickerItem.addAll(listStickerItems);
            initHashMapStickerItem(allStickerItem);
        } else {// so sanh de insert,delete
            HashSet<StickerItem> deleteItems = new HashSet<>(listStickerItemsOld);
            HashSet<StickerItem> newItems = new HashSet<>(listStickerItems);
            deleteItems.removeAll(newItems);              // lay list item khong con tren sv
            HashSet<StickerItem> oldItems = new HashSet<>(listStickerItemsOld);
            newItems.removeAll(oldItems);                 // lay list item co tren sv, khong co tren client.
            // xoa list
            if (!deleteItems.isEmpty()) {
                ArrayList<StickerItem> listDelete = new ArrayList<>(deleteItems);
                // xoa khoi mem
                allStickerItem.removeAll(listDelete);
                mStickerDataSource.deleteListStickerItem(listDelete);
            }
            // get lai tren mem
            ArrayList<StickerItem> listCurrentStickerItems = hashMapStickerItem.get(collectionId);
            if (listCurrentStickerItems != null && !listCurrentStickerItems.isEmpty()) {
                // update truong download = true
                ArrayList<StickerItem> listUpdates = new ArrayList<>();
                for (StickerItem item : listCurrentStickerItems) {
                    if (!item.isDownloadImg() || !item.isDownloadVoice()) {
                        item = updateStickerValues(item, listStickerItems);
                        listUpdates.add(item);
                    }
                }
                if (!listUpdates.isEmpty()) {
                    mStickerDataSource.updateListStickerItem(listUpdates);
                }
            }
            // insert new list
            if (!newItems.isEmpty()) {
                ArrayList<StickerItem> listInserts = new ArrayList<>(newItems);
                // them db
                mStickerDataSource.insertListStickerItem(listInserts);
                allStickerItem.addAll(listInserts);
            }
            // khoi tao lai hashmap
            initHashMapStickerItem(allStickerItem);
        }
    }

    private StickerItem updateStickerValues(StickerItem item, ArrayList<StickerItem> newLists) {
        if (newLists == null || newLists.isEmpty()) {
            item.setDownloadImg(false);
            item.setDownloadVoice(false);
        } else {
            for (StickerItem newItem : newLists) {
                if (newItem.getItemId() == item.getItemId()) {
                    item.setDownloadImg(true);
                    item.setDownloadVoice(true);
                    item.setImagePath(newItem.getImagePath());
                    item.setVoicePath(newItem.getVoicePath());
                }
            }
        }
        return item;
    }

    /**
     * doc file cau hinh
     *
     * @param collectionId
     */
    public void getStickerInfoFromFile(int collectionId) {
        try {
            // get file path
            StringBuilder sb = new StringBuilder();
            sb.append(Config.Storage.REENG_STORAGE_FOLDER).append(Config.Storage.STICKER_FOLDER).
                    append(collectionId).append("/");
            String stickerCollectionFilePath = sb.toString();
            File stickerCollectionFile = new File(stickerCollectionFilePath, "info.json");
            if (!stickerCollectionFile.exists()) {// file khong ton tai
                return;
            }
            FileInputStream stream = null;
            String content = null;
            try {
                stream = new FileInputStream(stickerCollectionFile);
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                content = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                if (stream != null) {
                    stream.close();
                }
            }
            Log.d(TAG, "get file content: " + content);
            JSONObject jsonObj = new JSONObject(content);
            // Getting data JSON Array nodes
            if (jsonObj.has(Constants.HTTP.STICKER.STICKER_COLLECTION_DATA)) {
                JSONArray jsonArrayStickerCollection = jsonObj.getJSONArray(Constants.HTTP.STICKER.STICKER_COLLECTION_DATA);
                if (jsonArrayStickerCollection != null && jsonArrayStickerCollection.length() > 0) {
                    JSONObject jsonStickerCollection = jsonArrayStickerCollection.getJSONObject(0);
                    StickerCollection stickerCollection = new StickerCollection();
                    stickerCollection.setDataFromJsonObject(jsonStickerCollection);
                    // list item
                    ArrayList<StickerItem> listStickerItems = new ArrayList<>();
                    JSONArray jsonArrayStickerItem = jsonStickerCollection.getJSONArray(Constants.HTTP.STICKER.STICKER_ITEM_DATA);
                    // get detail
                    if (jsonArrayStickerItem != null && jsonArrayStickerItem.length() > 0) {
                        int lengthArray = jsonArrayStickerItem.length();
                        for (int i = 0; i < lengthArray; i++) {
                            StickerItem stickerItem = new StickerItem();
                            stickerItem.setDataFromJsonObject(jsonArrayStickerItem.getJSONObject(i),
                                    collectionId, stickerCollectionFilePath);
                            stickerItem.setDownloadImg(true);
                            stickerItem.setDownloadVoice(true);
                            listStickerItems.add(stickerItem);
                        }
                    }
                    processStickerInfoAfterGetFileConfig(collectionId, listStickerItems);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public ArrayList<StickerItem> parserJsonSticker(String arrayJsonString) throws JSONException {
        if (TextUtils.isEmpty(arrayJsonString)) {
            return null;
        }
        JSONArray array = new JSONArray(arrayJsonString);
        ArrayList<StickerItem> stickerList = new ArrayList<>();
        if (array.length() > 0) {
            int lengthArray = array.length();
            for (int i = 0; i < lengthArray; i++) {
                JSONObject objectSticker = array.getJSONObject(i);
                StickerItem item = new StickerItem();
                item.setDataFromJsonObject(objectSticker);
                stickerList.add(item);
            }
        } else {
            return null;
        }
        return stickerList;
    }

    private void getNewStickerListFromJson(JSONObject jsonObject) throws Exception {
        JSONArray newStickerJsonArray = jsonObject.optJSONObject("storeInfo").optJSONArray("newCollections");
        newStickerList = new ArrayList<>();
        if (newStickerJsonArray == null || newStickerJsonArray.length() == 0) {
            mPref.edit().putString(Constants.PREFERENCE.PREF_STICKER_NEW_FROM_SERVER, "[]").apply();
        } else {
            //save in pref
            mPref.edit().putString(Constants.PREFERENCE.PREF_STICKER_NEW_FROM_SERVER, newStickerJsonArray.toString()).apply();
            int len = newStickerJsonArray.length();
            for (int i = 0; i < len; i++) {
                newStickerList.add(newStickerJsonArray.optInt(i));
            }
        }
    }

    private void getNewStickerFromPref() {
        String strNewSticker = mPref.getString(Constants.PREFERENCE.PREF_STICKER_NEW_FROM_SERVER, "[]");
        try {
            JSONArray jsonArray = new JSONArray(strNewSticker);
            int len = jsonArray.length();
            newStickerList = new ArrayList<>();
            for (int i = 0; i < len; i++) {
                newStickerList.add(jsonArray.optInt(i));
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public ArrayList<Integer> getStickersNew() {
        getNewStickerFromPref();
        ArrayList<Integer> listResult = (ArrayList) newStickerList;
        for (StickerCollection co : allStickerCollection) {
            if (isNewListContainId(co.getServerId()) && !co.isNew()) {
                int index = listResult.indexOf(co.getServerId());
                listResult.remove(index);
            }
        }
        return listResult;
    }

    private boolean isNewListContainId(int id) {
        for (Integer i : newStickerList) {
            if (i == id) return true;
        }
        return false;
    }

    private long isDbContainId(int id) {
        for (StickerCollection collection : allStickerCollection) {
            if (collection.getServerId() == id) return collection.getLocalId();
        }
        return -1;
    }

    public void insertOrUpdate(StickerCollection coll) {
        long localId = isDbContainId(coll.getServerId());
        if (localId != -1) {
            coll.setLocalId(localId);
            updateStickerCollection(coll);
        } else {
            if (!coll.isDownloaded())
                coll.setCollectionState(StickerConstant.COLLECTION_STATE_DISABLE);
            insertStickerCollection(coll);
        }
    }

    public StickerCollection getDefaultCollection() {
        return defaultCollection;
    }

    private void copyAttribute(StickerCollection oldCollection, StickerCollection newCollection) {
        // sticker dc sticky moi, chua down thi set truong last sticky
        if (oldCollection.isSticky() == 0 && newCollection.isSticky() == 1 && !oldCollection.isDownloaded()) {
            oldCollection.setLastSticky(TimeHelper.getCurrentTime());
        }
        oldCollection.setIsSticky(newCollection.isSticky());
        oldCollection.setIsDefault(newCollection.isDefault());
        oldCollection.setLastInfoUpdate(newCollection.getLastInfoUpdate() + 1);
        oldCollection.setLastServerUpdate(newCollection.getLastServerUpdate() + 1);
        oldCollection.setCollectionName(newCollection.getCollectionName());
        oldCollection.setCollectionPreviewPath(newCollection.getCollectionPreviewPath());
        oldCollection.setCollectionIconPath(newCollection.getCollectionIconPath());
        oldCollection.setNumberSticker(newCollection.getNumberSticker());
        oldCollection.setOrder(newCollection.getOrder());
    }

    private boolean isDifferentAttributeCollection(StickerCollection oldCollection, StickerCollection newCollection) {
        if (oldCollection.getLastServerUpdate() < newCollection.getLastServerUpdate()) {
            return true;
        } else if (oldCollection.getLastInfoUpdate() < newCollection.getLastInfoUpdate()) {
            return true;
        } else if (oldCollection.isSticky() != newCollection.isSticky()) {
            return true;
        } else if (oldCollection.isDefault() != newCollection.isDefault()) {
            return true;
        } else if (oldCollection.getNumberSticker() != newCollection.getNumberSticker()) {
            return true;
        } else if (newCollection.getCollectionName() != null &&
                !newCollection.getCollectionName().equals(oldCollection.getCollectionName())) {
            return true;
        } else if (newCollection.getCollectionPreviewPath() != null &&
                !newCollection.getCollectionPreviewPath().equals(oldCollection.getCollectionPreviewPath())) {
            return true;
        } else if (newCollection.getCollectionIconPath() != null &&
                !newCollection.getCollectionIconPath().equals(oldCollection.getCollectionIconPath())) {
            return true;
        } else return oldCollection.getOrder() != newCollection.getOrder();
    }

    @Override
    public void onPreExecute(StickerCollection stickerCollection) {
        Log.i(TAG, "download default sticker onPreExecute");
        isDownloadingDefaultCollection = true;
    }

    public boolean isDownloadingDefaultCollection() {
        return isDownloadingDefaultCollection;
    }

    @Override
    public void onPostExecute(Boolean isSucceed, StickerCollection stickerCollection) {
//        Log.i(TAG, "download default sticker onPostExecute");
//        mStickerDataSource.updateStickerCollection(defaultCollection);
        isDownloadingDefaultCollection = false;
        if (!isSucceed) {
            failCount++;
            if (failCount < 3) {
                startDownloadDefaultCollection();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void startDownloadDefaultCollection() {
        if (!NetworkHelper.isConnectInternet(mApplicationController)) {
            return;
        }
        DownloadStickerAsynctask downloadStickerAsynctask = new DownloadStickerAsynctask(mApplicationController,
                defaultCollection, false, this);
        downloadStickerAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onProgressUpdate(int progress) {
        Log.i(TAG, "download default sticker onProgressUpdate");
    }

    public void addCollectionId(int id) {
        if (currentDownloadIds == null) currentDownloadIds = new HashSet<>();
        currentDownloadIds.add(id);
    }

    public void removeCollectionId(int id) {
        if (currentDownloadIds != null && !currentDownloadIds.isEmpty()) {
            currentDownloadIds.remove(id);
        }
    }

    public int getPositionStickerWhenResume() {
        if (currentDownloadIds != null && !currentDownloadIds.isEmpty()) {
            currentDownloadIds = new HashSet<>();// reset
            return 3;// so 3 la vi tri sticker down moi nhat
        }
        return -1;
    }

    public String getGifFilePath(String filePath, String prefix) {
        StringBuilder sb = new StringBuilder();
        String fileName = FileHelper.getFileNameFromPath(filePath);
        sb.append(Config.Storage.REENG_STORAGE_FOLDER).append(Config.Storage.GIF_FOLDER)
                .append("/").append(fileName);//.append(".").append(prefix);//TODO sv trả về có extension file rồi
        return sb.toString();
    }

    public void checkAndDownLoadGifFile(ReengMessage message) {
        if (message.getStatus() == ReengMessageConstant.STATUS_NOT_LOAD ||
                message.getStatus() == ReengMessageConstant.STATUS_FAIL) {
            message.setStatus(ReengMessageConstant.STATUS_LOADING);
            DownloadGifAsyncTask downloadGifAsyncTask = new DownloadGifAsyncTask(mApplicationController, message);
            downloadGifAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
}
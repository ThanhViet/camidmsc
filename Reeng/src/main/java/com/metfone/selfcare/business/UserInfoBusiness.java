package com.metfone.selfcare.business;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.EnterAddPhoneNumberActivity;
import com.metfone.selfcare.activity.OTPAddPhoneLoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.account.IdentifyProvider;
import com.metfone.selfcare.model.account.PhoneLinkedInfo;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.backup_restore.RestoreModel;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.util.EnumUtils;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.github.mikephil.charting.charts.Chart.LOG_TAG;

public class UserInfoBusiness {

    public static final String DATE_FORMAT_2 = "dd/MM/yyyy";
    BaseMPConfirmDialog baseMPConfirmDialog;
    private SharedPref sharedPref;

    public UserInfoBusiness(Context context) {
        sharedPref = new SharedPref(context);
    }

    public static UserInfoBusiness instance;

    public static UserInfoBusiness getInstance(Context context) {
        if(instance == null){
            instance =new UserInfoBusiness(context);
        }
        return instance;
    }

    public static <T> List<T> getObjectList(String jsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson();
            JsonArray arry = new JsonParser().parse(jsonString).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                list.add(gson.fromJson(jsonElement, cls));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    ;

    public static <T> String convertObjectToJson(List<T> arr) {
        return new Gson().toJson(arr);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static ArrayList<ServicesModel> convertServicesToServiceModelList(List<Services> servicesList) {
        int endOfService = 0;
        ArrayList<ServicesModel> servicesModels = new ArrayList<>();
        for (Services services : servicesList) {
            List<PhoneLinkedInfo> phoneLinkedInfos = services.getPhoneLinkedList();
            if (phoneLinkedInfos == null)
                continue;
            for (PhoneLinkedInfo phoneLinkedInfo : phoneLinkedInfos) {
                ServicesModel model = new ServicesModel(services.getServiceId(), services.getServiceCode(),
                        services.getServiceName(), phoneLinkedInfo.getStatus(), phoneLinkedInfo.getMetfonePlus(), phoneLinkedInfo.getUserServiceId(), phoneLinkedInfo.getPhoneNumber(), false);
                servicesModels.add(model);
            }
            if (phoneLinkedInfos.size() == 0) {
                ServicesModel model = new ServicesModel(services.getServiceId(), services.getServiceCode(),
                        services.getServiceName(), null, 0, 0, null, true);
                servicesModels.add(model);
            }
            if (servicesModels.size() > 0 && servicesModels.size() > endOfService && phoneLinkedInfos.size() > 0) {
                servicesModels.get(endOfService).setHeader(true);
                endOfService += phoneLinkedInfos.size();
            }
        }

        return servicesModels;
    }

    public static void setTextNotNull(TextView textView, String text) {
        SpannableString spannableString = new SpannableString(text);
        ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
        spannableString.setSpan(foregroundSpan, spannableString.length() - 1, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);


    }

    public static void setTextBold(TextView textView, String text) {
        SpannableString spannableString = new SpannableString(text);
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        spannableString.setSpan(boldSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidPhoneNumber(CharSequence phone) {
        return phone != null && !(phone.length() < 8 || phone.length() > 14) && android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isPhoneNumberValid(
            String phoneNumber,
            String countryCode) {
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, countryCode);
            return phoneUtil.isValidNumber(numberProto);
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
        return false;
    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_2);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }
    public static String getDefaultDate() {
        String defaultDate = "03/02/1993";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_2);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = dateFormat.parse(defaultDate);
            return dateFormat.format(date);
        } catch (ParseException e) {
            Date date = Calendar.getInstance().getTime();
            return dateFormat.format(date);
        }
    }

    public static int getWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int toDp(int px, Context context) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, px, context.getResources().getDisplayMetrics()
        );
    }

    public static void setCursorColor(EditText editText, @ColorInt int color) {
        try {
            // Get the cursor resource id
            Field field = TextView.class.getDeclaredField("mCursorDrawableRes");
            field.setAccessible(true);
            int drawableResId = field.getInt(editText);

            // Get the drawable and set a color filter
            Drawable drawable = ContextCompat.getDrawable(editText.getContext(), drawableResId);
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
            Drawable[] drawables = {drawable, drawable};

            if (Build.VERSION.SDK_INT == 15) {
                // Get the editor
                Class<?> drawableFieldClass = TextView.class;
                // Set the drawables
                field = drawableFieldClass.getDeclaredField("mCursorDrawable");
                field.setAccessible(true);
                field.set(editText, drawables);

            } else {
                // Get the editor
                field = TextView.class.getDeclaredField("mEditor");
                field.setAccessible(true);
                Object editor = field.get(editText);
                // Set the drawables
                field = editor.getClass().getDeclaredField("mCursorDrawable");
                field.setAccessible(true);
                field.set(editor, drawables);
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "-> ", e);
        }
    }

    public static boolean checkEmptyPhoneNumber(Context context) {
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(context);
        UserInfo userInfo = userInfoBusiness.getUser();
        return TextUtils.isEmpty(userInfo.getPhone_number());
    }

    public static TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }
    public static final void setSystemBarTheme(final Activity pActivity, final boolean pIsDark) {
        // Fetch the current flags.
        final int lFlags = pActivity.getWindow().getDecorView().getSystemUiVisibility();
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        pActivity.getWindow().getDecorView().setSystemUiVisibility(pIsDark ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
    }

    public static Bitmap takeScreenShot(Activity activity) {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        int width = activity.getWindowManager().getDefaultDisplay().getWidth();
        int height = activity.getWindowManager().getDefaultDisplay().getHeight();

        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
        view.destroyDrawingCache();
        return b;
    }

    public static Bitmap takeScreenShotFull(Activity activity) {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();


        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
        view.destroyDrawingCache();
        return b;
    }

    public static Bitmap fastblur(Bitmap sentBitmap, int radius) {
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    public static EnumUtils.LoginVia isLogin() {
        String mochaToken = ApplicationController.self().getReengAccountBusiness().getToken();
        String openIDToken = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_ACCESS_TOKEN, String.class);
        if (mochaToken == null) mochaToken = "";
        if (openIDToken == null) openIDToken = "";
        if ((mochaToken.isEmpty() && openIDToken.isEmpty())) {
            return EnumUtils.LoginVia.NOT;
        } else if (mochaToken.isEmpty()) {
            return EnumUtils.LoginVia.OPEN_ID;
        } else {
            return EnumUtils.LoginVia.MOCHA_OPEN_ID;
        }
    }

    // Mark the argument as restricted to these enumerated types
    public void putLogInMode(@EnumUtils.LoginModeTypeDef int mode) {
        sharedPref.putInt(EnumUtils.LOGIN_TYPE, mode);
    }

    public int getLogInMode() {
        return sharedPref.getInt(EnumUtils.LOGIN_TYPE, EnumUtils.LoginModeTypeDef.PHONE_NUMBER);
    }

    public UserInfo getUser() {
        String json = sharedPref.getString(EnumUtils.USER_INFO, "");
        Gson gson = new GsonBuilder().create();
        if (TextUtils.isEmpty(json)) return new UserInfo();
        else return gson.fromJson(json, UserInfo.class);
    }

    public void setUser(UserInfo userInfo) {
        Gson gson = new Gson();
        String json = gson.toJson(userInfo);
        sharedPref.putString(EnumUtils.USER_INFO, json);
        ApplicationController.self().getCamIdUserBusiness().setUserInfo(userInfo);
    }

    public AccountRankDTO getAccountRankDTO() {
        String json = sharedPref.getString(EnumUtils.ACCOUNT_RANK_DTO, "");
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, AccountRankDTO.class);
    }

    public void setAccountRankDTO(AccountRankDTO userInfo) {
        Gson gson = new Gson();
        String json = gson.toJson(userInfo);
        sharedPref.putString(EnumUtils.ACCOUNT_RANK_DTO, json);
    }

    public String getOTPOldMochaResponseJson() {
        return sharedPref.getString(EnumUtils.OTP_OLD_MOCHA_RESPONSE, "");
    }

    public OTPOldMochaResponse getOTPOldMochaResponse() {
        String json = sharedPref.getString(EnumUtils.OTP_OLD_MOCHA_RESPONSE, "");
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, OTPOldMochaResponse.class);
    }

    public void setOTPOldMochaResponse(OTPOldMochaResponse response) {
        Gson gson = new Gson();
        String json = gson.toJson(response);
        sharedPref.putString(EnumUtils.OTP_OLD_MOCHA_RESPONSE, json);

    }

    public List<Services> getServiceList() {
        String json = sharedPref.getString(EnumUtils.SERVICE_LIST, "");
        return getObjectList(json, Services.class);
    }

    public void setServiceList(List<Services> serviceList) {
        String json = convertObjectToJson(serviceList);
        sharedPref.putString(EnumUtils.SERVICE_LIST, json);
        ApplicationController.self().getCamIdUserBusiness().setServices(serviceList);
    }

    public List<IdentifyProvider> getIdentifyProviderList() {
        String json = sharedPref.getString(EnumUtils.IDENTITY_PROVIDER_LIST, "");
        return getObjectList(json, IdentifyProvider.class);
    }

    public void setIdentifyProviderList(List<IdentifyProvider> identifyProviderList) {
        String json = convertObjectToJson(identifyProviderList);
        sharedPref.putString(EnumUtils.IDENTITY_PROVIDER_LIST, json);
        ApplicationController.self().getCamIdUserBusiness().setIdentityProviders(identifyProviderList);
    }

    public void clearCache() {
        com.metfone.selfcare.util.Log.i("TAG", "clear preferencesName \"" + sharedPref + "\"");
        if (sharedPref != null) {
            sharedPref.clear();
        }
    }

    public void remove(String key) {
        com.metfone.selfcare.util.Log.i("TAG", "remove key \"" + key + "\" of preferencesName \"" + sharedPref + "\"");
        if (sharedPref != null) {
            sharedPref.remove(key);
        }
    }

    public void showLoginDialog(BaseSlidingFragmentActivity activity, int titleId, int contentId) {
        baseMPConfirmDialog = new BaseMPConfirmDialog(titleId,
                contentId, R.string.sign_in, R.string.cancel,
                v -> {
                    //letdoit
                    NavigateActivityHelper.navigateToRegisterScreenActivity(activity, true);
                    if (baseMPConfirmDialog != null) {
                        baseMPConfirmDialog.dismiss();
                    }
                }, v -> {
            //back
            if (baseMPConfirmDialog != null) {
                baseMPConfirmDialog.dismiss();
            }
        });
        baseMPConfirmDialog.show(activity.getSupportFragmentManager(), null);
    }

    public void showAddPhoneDialog(BaseSlidingFragmentActivity activity,int titleId,int contentId){
        baseMPConfirmDialog = new BaseMPConfirmDialog(titleId,
                contentId, R.string.ok, R.string.cancel,
                v -> {
                    //letdoit
                    Intent intent = new Intent(activity,EnterAddPhoneNumberActivity.class);
                    intent.putExtra(EnumUtils.OBJECT_KEY, EnumUtils.PhoneNumberTypeDef.ADD_PHONE);
                    activity.startActivity(intent, false);
                    if (baseMPConfirmDialog != null) {
                        baseMPConfirmDialog.dismiss();
                    }
                }, v -> {
            //back
            if (baseMPConfirmDialog != null) {
                baseMPConfirmDialog.dismiss();
            }
        });
        baseMPConfirmDialog.show(activity.getSupportFragmentManager(), null);
    }

    public void autoBackUp(ApplicationController mApplication, DBImporter.RestoreProgressListener listener, BaseSlidingFragmentActivity activity) {
        Context context = mApplication.getApplicationContext();
        SharedPreferences mPref = context.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        if (NetworkHelper.isConnectInternet(context)) {
            if (Config.Features.FLAG_SUPPORT_BACKUP_MESSAGES && mPref != null && !mPref.getBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, false)) {
                RestoreManager.getBackupFileInfo(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        try {
                            Gson gson = new Gson();
                            RestoreModel restoreModel = gson.fromJson(data, RestoreModel.class);
                            if (restoreModel != null && restoreModel.getLstFile() != null && !restoreModel.getLstFile().isEmpty()) {
                                //restore
                                int lastIndex = 0;
                                RestoreModel.FileInfo fileInfo = restoreModel.getLstFile().get(lastIndex);
                                mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, data).apply();
                                if (NetworkHelper.isConnectInternet(context)) {
                                    RestoreManager.restoreMessages(listener, UrlConfigHelper.getInstance(mApplication).getDomainFile() + "/" + fileInfo.getPath());
                                } else {
                                    Toast.makeText(ApplicationController.self(), context.getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                //skip
                                RestoreManager.setRestoring(false);
                                NavigateActivityHelper.navigateToHomeScreenActivity(activity, false, true);
                            }
                        } catch (Exception e) {
                            //skip
                            RestoreManager.setRestoring(false);
                            NavigateActivityHelper.navigateToHomeScreenActivity(activity, false, true);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        RestoreManager.setRestoring(false);
                        com.metfone.selfcare.util.Log.i("TAG", "getBackupFileInfo: fail " + message);
                        NavigateActivityHelper.navigateToHomeScreenActivity(activity, false, true);
                    }
                });
            } else {
                RestoreManager.setRestoring(false);
                NavigateActivityHelper.navigateToHomeScreenActivity(activity, false, true);
            }
        } else {
            Toast.makeText(ApplicationController.self(), context.getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG).show();
        }
    }

    public static String getMaxLengthName(String str){
        if(str.length()>20){
            str  =  str.substring(0,19)+"...";
        }
        return  str;
    }
    public static String getCurrentLanguage(BaseSlidingFragmentActivity activity, ApplicationController mApplication){
        SharedPreferences mPref = activity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        String deviceLanguage = mApplication.getReengAccountBusiness().getDeviceLanguage();
        String mCurrentLanguageCode = mPref.getString(Constants.PREFERENCE.PREF_LANGUAGE_TRANSLATE_SELECTED, deviceLanguage);
        Log.d("TAG", "getCurrentLanguage: " + mCurrentLanguageCode);
        return  mCurrentLanguageCode;
    }

}

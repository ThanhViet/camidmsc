package com.metfone.selfcare.business;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChangeNumberActivity;
import com.metfone.selfcare.activity.QuickMissCallActivity;
import com.metfone.selfcare.activity.QuickReplyActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.SmsReceiver;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.EventMessageConstant;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.datasource.EventMessageDataSource;
import com.metfone.selfcare.database.datasource.MessageImageDataSource;
import com.metfone.selfcare.database.datasource.ReengMessageDataSource;
import com.metfone.selfcare.database.datasource.ThreadMessageDataSource;
import com.metfone.selfcare.database.model.EventMessage;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.CPresence;
import com.metfone.selfcare.database.model.message.MessageImageDB;
import com.metfone.selfcare.database.model.message.PinMessage;
import com.metfone.selfcare.database.model.message.ReengFileMessage;
import com.metfone.selfcare.database.model.message.ReplyMessage;
import com.metfone.selfcare.database.model.message.SoloSendImageMessage;
import com.metfone.selfcare.database.model.message.SoloSendShareContactMessage;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.database.model.message.SoloSendVoicemailMessage;
import com.metfone.selfcare.database.model.message.SoloShareLocationMessage;
import com.metfone.selfcare.database.model.message.SoloShareMusicMessage;
import com.metfone.selfcare.database.model.message.SoloShareVideoMessage;
import com.metfone.selfcare.database.model.message.SoloVoiceStickerMessage;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.fragment.message.ThreadDetailSettingFragment;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.GroupAvatarHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.MD5;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.TagHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.helper.message.BankPlusHelper;
import com.metfone.selfcare.helper.message.CountDownInviteManager;
import com.metfone.selfcare.helper.message.MessageConstants;
import com.metfone.selfcare.helper.message.PacketMessageId;
import com.metfone.selfcare.helper.message.SpamRoomManager;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.EventAppListener;
import com.metfone.selfcare.listeners.ForwardMessageListener;
import com.metfone.selfcare.listeners.LockRoomListener;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.listeners.SendNewMessageListener;
import com.metfone.selfcare.listeners.SmsReceiverListener;
import com.metfone.selfcare.module.backup_restore.BackupMessageModel;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.notification.MessageNotificationManager;
import com.metfone.selfcare.notification.ReengNotificationManager;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.util.contactintergation.DeviceAccountManager;
import com.viettel.util.LogDebugHelper;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.GSMMessage;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQGroup;
import org.jivesoftware.smack.packet.IQInfo;
import org.jivesoftware.smack.packet.Member;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.ReengEventPacket;
import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.jivesoftware.smack.packet.ReengMusicPacket;
import org.jivesoftware.smack.packet.VoicemailGSMResponseMessage;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.whispersystems.libsignal.state.PreKeyBundle;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thaodv on 7/1/2014.
 */
public class MessageBusiness implements SmsReceiverListener {
    private static final String TAG = MessageBusiness.class.getSimpleName();
    private static final int PACKET_ID_LIST_MAX_SIZE = 2000;
    private ReengMessageListener mReengMessageListenerThreadDetailInbox;
    private CopyOnWriteArrayList<ReengMessageListener> mReengMessageListenerArrayList = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ConfigGroupListener> mConfigGroupListenersArrayList = new CopyOnWriteArrayList<>();
    private EventAppListener mEventAppListener;
    private LockRoomListener mLockRoomListener;
    private ApplicationController mApplication;
    private CopyOnWriteArrayList<ThreadMessage> mThreadMessageArrayList;
    private ReengMessageDataSource mReengMessageDataSource;
    private ThreadMessageDataSource mThreadMessageDataSource;
    private EventMessageDataSource mEventMessageDataSource;
    private MessageImageDataSource mMessageImageDataSource;
    private BlockContactBusiness mBlockContactBusiness;
    private ReengAccountBusiness mAccountBusiness;
    private Resources mRes;
    private boolean isDataLoaded = false;
    private int gsmMessageErrorCode;
    private String myNumber;
    private ContactBusiness mContactBusiness;
    private MessageRetryManager mMessageRetryManager;
    private SettingBusiness mSettingBusiness;
    private LinkedList<String> listPacketIdOfReceivedMessage;
    private ApplicationStateManager mApplicationStateManager;
    private IncomingMessageProcessor mIncomingMessageProcessor;
    private OutgoingMessageProcessor mOutgoingMessageProcessor;
    private CopyOnWriteArrayList<ReengMessage> listMessageProcessInfo;
    private LogKQIHelper logContentHelper;
    private HashMap<String, PreKeyBundle> hashMapEncrypt = new HashMap<>();

//    ArrayList<NoteMessageItem> listNoteMessageItems = new ArrayList<>();

    private boolean isSendMsgQuickReply;
    private boolean isWaitInsertThread = false;
    private SharedPreferences mPref;
    private ArrayList<String> strangerNumberShowAlerts;
    private HashMap<Integer, Long> timeShowBannerLixi = new HashMap<>();

    public MessageBusiness(ApplicationController app) {
        this.mApplication = app;
    }

    public void init() {
        mReengMessageListenerArrayList = new CopyOnWriteArrayList<>();
        mConfigGroupListenersArrayList = new CopyOnWriteArrayList<>();
        mReengMessageDataSource = ReengMessageDataSource.getInstance(mApplication);
        mThreadMessageDataSource = ThreadMessageDataSource.getInstance(mApplication);
        mEventMessageDataSource = EventMessageDataSource.getInstance(mApplication);
        mMessageImageDataSource = MessageImageDataSource.getInstance(mApplication);
        mThreadMessageArrayList = new CopyOnWriteArrayList<>();
        this.mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        mBlockContactBusiness = mApplication.getBlockContactBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        isDataLoaded = false;
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        mRes = mApplication.getResources();
        mMessageRetryManager = mApplication.getXmppManager().getMessageRetryManager();
        listPacketIdOfReceivedMessage = new LinkedList<>();
        mApplicationStateManager = mApplication.getAppStateManager();
        mIncomingMessageProcessor = new IncomingMessageProcessor(this, mApplication);
        mOutgoingMessageProcessor = new OutgoingMessageProcessor(this, mApplication);
        logContentHelper = new LogKQIHelper(mApplication);
        initListMessageImageDB();
        try {
            SmsReceiver.removeSMSReceivedListener(this);
            SmsReceiver.addSMSReceivedListener(this);

            initListTimeShowBannerLixi();
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public void setReengMessageThreadDetaiInbox(ReengMessageListener listener) {
        this.mReengMessageListenerThreadDetailInbox = listener;
    }

    public synchronized void addReengMessageListener(ReengMessageListener reengMessageListener) {
        if (!mReengMessageListenerArrayList.contains(reengMessageListener))
            mReengMessageListenerArrayList.add(reengMessageListener);
    }

    public synchronized void removeReengMessageListener(ReengMessageListener reengMessageListener) {
        mReengMessageListenerArrayList.remove(reengMessageListener);
    }

    private void notifyMessageSentSuccessfully(int threadId) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            Log.i(TAG, "notifyMessageSentSuccessfully in thread " + threadId + " " + mReengMessageListenerArrayList
                    .size() + " listeners");
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.notifyMessageSentSuccessfully(threadId);
            }
        } else {
            Log.d(TAG, "notifyMessageSentSuccessfully but no listener to process");
        }
        if (mReengMessageListenerThreadDetailInbox != null) {
            mReengMessageListenerThreadDetailInbox.notifyMessageSentSuccessfully(threadId);
        }
    }

    public void refreshThreadWithoutNewMessage(int threadId) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.onRefreshMessage(threadId);
            }
        }
        if (mReengMessageListenerThreadDetailInbox != null) {
            mReengMessageListenerThreadDetailInbox.onRefreshMessage(threadId);
        }
    }

    private void notifyChangeStateAcceptStranger(String friendJid) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.onUpdateStateAcceptStranger(friendJid);
            }
        }
    }

    public void notifyNewOutgoingMessage() {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.notifyNewOutgoingMessage();
            }
        }
        if (mReengMessageListenerThreadDetailInbox != null) {
            mReengMessageListenerThreadDetailInbox.notifyNewOutgoingMessage();
        }
    }

    public void notifyDeleteThreadMessage(int mThreadId) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.onRefreshMessage(mThreadId);
            }
        }
        if (mReengMessageListenerThreadDetailInbox != null) {
            mReengMessageListenerThreadDetailInbox.onRefreshMessage(mThreadId);
        }
        GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(mThreadId);
    }

    private void notifySendGsmMessageError(ReengMessage reengMessage, XMPPResponseCode responseCode) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.onGSMSendMessageError(reengMessage, responseCode);
            }
        }
        if (mReengMessageListenerThreadDetailInbox != null) {
            mReengMessageListenerThreadDetailInbox.onGSMSendMessageError(reengMessage, responseCode);
        }
    }

    public void notifyStateRoomChanged() {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.onUpdateStateRoom();
            }
        }
    }

    public void onNonReengResponse(int threadId, ReengMessage reengMessage, boolean showAlert, String msgError) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                Log.i(TAG, "onNonReengResponse 2");
                listener.onNonReengResponse(threadId, reengMessage, showAlert, msgError);
            }
        }
    }

    public void notifyPinMessageUpdate(ThreadMessage thread) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.onBannerDeepLinkUpdate(null, thread);
            }
        } else {
            Log.d(TAG, "notifyUpdateMediaDetail Successfully but no listener to process");
        }
    }

    public void notifyUpdateMediaDetail(MediaModel mediaModel, int threadId) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.onUpdateMediaDetail(mediaModel, threadId);
            }
        } else {
            Log.d(TAG, "notifyUpdateMediaDetail Successfully but no listener to process");
        }
    }

    private void notifyLockRoomChat(ThreadMessage threadMessage, int status, long timeLock) {
        if (mLockRoomListener != null) {
            mLockRoomListener.notifyLockRoom(threadMessage.getId(), status, timeLock);
        }
    }

    public void notifySpamStranger(ThreadMessage threadMessage, String msg) {
        if (mLockRoomListener != null) {
            mLockRoomListener.notifySpamStranger(threadMessage.getId(), msg);
        }
    }

    public void addConfigGroupListener(ConfigGroupListener configGroupListener) {
        if (!mConfigGroupListenersArrayList.contains(configGroupListener))
            mConfigGroupListenersArrayList.add(configGroupListener);
    }

    public void removeConfigGroupListener(ConfigGroupListener configGroupListener) {
        mConfigGroupListenersArrayList.remove(configGroupListener);
    }

    public void notifyChangeGroupName(ThreadMessage threadMessage) {
        if (mConfigGroupListenersArrayList != null && !mConfigGroupListenersArrayList.isEmpty()) {
            for (ConfigGroupListener listener : mConfigGroupListenersArrayList) {
                listener.onConfigGroupChange(threadMessage, Constants.MESSAGE.CHANGE_GROUP_NAME);
            }
        }
    }

    public void notifyConfigRoomChange(String roomId, boolean unFollow) {
        if (mConfigGroupListenersArrayList != null && !mConfigGroupListenersArrayList.isEmpty()) {
            for (ConfigGroupListener listener : mConfigGroupListenersArrayList) {
                listener.onConfigRoomChange(roomId, unFollow);
            }
        }
    }

    public void notifyChangeGroupMember(ThreadMessage threadMessage) {
        if (mConfigGroupListenersArrayList != null && !mConfigGroupListenersArrayList.isEmpty()) {
            for (ConfigGroupListener listener : mConfigGroupListenersArrayList) {
                listener.onConfigGroupChange(threadMessage, Constants.MESSAGE.CHANGE_GROUP_MEMBER);
            }
        }
    }

    public void notifyChangeGroupPrivate(ThreadMessage threadMessage) {
        if (mConfigGroupListenersArrayList != null && !mConfigGroupListenersArrayList.isEmpty()) {
            for (ConfigGroupListener listener : mConfigGroupListenersArrayList) {
                listener.onConfigGroupChange(threadMessage, Constants.MESSAGE.CHANGE_GROUP_PRIVATE);
            }
        }
    }

    public void notifyChangeGroupAvatar(ThreadMessage threadMessage) {
        if (mConfigGroupListenersArrayList != null && !mConfigGroupListenersArrayList.isEmpty()) {
            for (ConfigGroupListener listener : mConfigGroupListenersArrayList) {
                listener.onConfigGroupChange(threadMessage, Constants.MESSAGE.CHANGE_GROUP_AVATAR);
            }
        }
    }

    public synchronized void addEventAppListener(EventAppListener listener) {
        mEventAppListener = listener;
    }

    public synchronized void removeEventAppListener() {
        mEventAppListener = null;
    }

    public synchronized void addLockRoomListener(LockRoomListener listener) {
        mLockRoomListener = listener;
    }

    public synchronized void removeLockRoomListener() {
        mLockRoomListener = null;
    }

    public void notifyForceUpdate(String msg, String link, boolean force) {
        if (mEventAppListener != null)
            mEventAppListener.notifyForceUpdate(msg, link, force);
    }

    public void notifyAuthenConflict() {
        /*ContactManager mContactManager = new ContactManager(mApplication);
        mContactManager.deleteALLAccount();*/
        DeviceAccountManager.getInstance(mApplication).removeAccount();
        if (mEventAppListener != null)
            mEventAppListener.notifyAuthenConflict();
    }

    public void notifyTypingMessage(ReengEventPacket receivedMessage) {
        String phoneNumber;
        if (receivedMessage.getType() == ReengMessagePacket.Type.chat) {
            phoneNumber = receivedMessage.getFrom().split("@")[0].trim();
            String newNumber = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(phoneNumber);
            if (newNumber != null) phoneNumber = newNumber;
            if (!mBlockContactBusiness.isBlockNumber(phoneNumber)) {
                if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
                    for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                        listener.onUpdateStateTyping(phoneNumber, null);
                    }
                }
            }
        } else if (receivedMessage.getType() == ReengMessagePacket.Type.groupchat) {
            phoneNumber = receivedMessage.getSender();
            String chatRoom = receivedMessage.getFrom().split("@")[0].trim();
            //tim thread chat tuong ung hoac tao moi neu chua co
            ThreadMessage mCorrespondingThread = findExistingOrCreateNewGroupThread(chatRoom);
            if (mCorrespondingThread != null) {
                if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
                    for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                        listener.onUpdateStateTyping(phoneNumber, mCorrespondingThread);
                    }
                }
            }
        }
    }

    public boolean isMessageDataReady() {
        return isDataLoaded;
    }

    public IncomingMessageProcessor getIncomingMessageProcessor() {
        return mIncomingMessageProcessor;
    }

    public OutgoingMessageProcessor getOutgoingMessageProcessor() {
        return mOutgoingMessageProcessor;
    }

    /**
     * load limited number of message for each thread for the first time
     */
    public void loadAllThreadMessageOnFirstTime(MessageInteractionListener.LoadUnknownMessageAndReloadAllThread listener) {
        long currentTime = System.currentTimeMillis();
        mThreadMessageArrayList = mThreadMessageDataSource.getAllThreadMessages(); //ham nay chi mat vai ms
        Log.d(TAG, "[perform] load all thread message take " + (System.currentTimeMillis() - currentTime) + " ms");
        currentTime = System.currentTimeMillis();
        ArrayList<ThreadMessage> listUpdateThreads = new ArrayList<>();
        ArrayList<ReengMessage> listUpdateMessages = new ArrayList<>();
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            // thread cu chua co value last msg id
            CopyOnWriteArrayList<ReengMessage> listMessages;
            if (threadMessage.getLastMessageId() == ThreadMessageConstant.LAST_MESSAGE_ID_NOT_SYNC) {
                int limitSize = Constants.MESSAGE.MESSAGE_LIMIT;
                if (threadMessage.getNumOfUnreadMessage() > limitSize) {
                    limitSize = threadMessage.getNumOfUnreadMessage();
                }
                listMessages = mReengMessageDataSource.getLimitMessagesOfThread_V2(threadMessage.getId(),
                        limitSize, this, -1);
                threadMessage.setLoadDetail(true);
                // cap nhat truong last message id
                int lastMessageId = ThreadMessageConstant.LAST_MESSAGE_ID_DEFAULT;
                if (listMessages != null && !listMessages.isEmpty()) {
                    lastMessageId = listMessages.get(listMessages.size() - 1).getId();
                }
                threadMessage.setLastMessageId(lastMessageId);
                listUpdateThreads.add(threadMessage);
            } else if (threadMessage.getLastMessageId() >= 0) {// da luu last msg id
                listMessages = mReengMessageDataSource.getLastMessagesOfThread(threadMessage.getLastMessageId(), this);
                threadMessage.setLoadDetail(false);// load 1 msg danh dau chua load detail de load sau
                if (!listMessages.isEmpty()) {
                    ReengMessage lastMessage = listMessages.get(listMessages.size() - 1);
                    if (lastMessage.getReadState() == ReengMessageConstant.READ_STATE_UNREAD &&
                            lastMessage.getExpired() != -1 && currentTime > lastMessage.getExpired()) {
                        lastMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
                        threadMessage.setNumOfUnreadMessage(threadMessage.getNumOfUnreadMessage() - 1);
                        listUpdateThreads.add(threadMessage);
                        listUpdateMessages.add(lastMessage);
                    }
                }
            } else {// ==-1 thread chua co message
                listMessages = new CopyOnWriteArrayList<>();
                threadMessage.setLoadDetail(true);
            }
            //set has new message
            if (threadMessage.getNumOfUnreadMessage() > 0) {
                threadMessage.setHasNewMessage(true);
            }
            threadMessage.setAllMessages(listMessages);
            //dua viec check len truoc vi co truong hop co tin nhan ve roi nhung chua co message de check duplicate
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// cap nhat trang
                // thai admin hay khong
                updateAdminGroup(threadMessage);
            }
        }
        if (!listUpdateThreads.isEmpty()) {
            mThreadMessageDataSource.updateListThreadMessageAfterChangeLastId(listUpdateThreads);
        }
        if (!listUpdateMessages.isEmpty()) {
            mReengMessageDataSource.updateListMessage(listUpdateMessages);
            mApplication.updateCountNotificationIcon();
        }
        Log.d(TAG, "[perform] load all message of list thread take " +
                (System.currentTimeMillis() - currentTime) + "" + " ms");
        currentTime = System.currentTimeMillis();
        ArrayList<String> listPacketIdCheckDuplicate = mReengMessageDataSource.getPacketIdCheckDuplicate(500);
        if (listPacketIdCheckDuplicate != null && !listPacketIdCheckDuplicate.isEmpty()) {
            for (String packetId : listPacketIdCheckDuplicate) {
                pushToPacketIdList(packetId);
            }
        }
        Log.d(TAG, "[perform] load all message check duplicate take " +
                (System.currentTimeMillis() - currentTime) + " ms");
        isDataLoaded = true;
        if (listener != null) listener.onLoadAllDone();
    }

    public void updateThreadStrangerAfterLoadData() {
        if (mThreadMessageArrayList == null || mThreadMessageArrayList.isEmpty()) {
            return;
        }
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            // check thread co phai thread lam quen khong
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                ArrayList<String> phoneNumbers = threadMessage.getPhoneNumbers();
                if (!phoneNumbers.isEmpty()) {
                    mApplication.getStrangerBusiness().
                            updateStateThreadStrangerAfterLoadDB(threadMessage, phoneNumbers.get(0));
                } else {
                    Log.i(TAG, "why?");
                }
            }
        }
    }

    public void updateThreadStrangerAfterSyncContact() {
        updateThreadStrangerAfterLoadData();
        refreshThreadWithoutNewMessage(-1);
    }

    public void updateAdminGroup(ThreadMessage threadMessage) {
        if (threadMessage == null) return;
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ArrayList<String> adminNumbers = threadMessage.getAdminNumbers();
        if (adminNumbers == null || adminNumbers.isEmpty()) {
            threadMessage.setIsAdmin(false);
        } else if (adminNumbers.contains(myNumber)) {
            // neu minh la admin thi set len vi tri dau tien
            adminNumbers.remove(myNumber);
            threadMessage.setIsAdmin(true);
            adminNumbers.add(0, myNumber);
        } else {
            threadMessage.setIsAdmin(false);
        }
    }

    /**
     * push vao listPacketId cac message nhan duoc
     * push vao waiting map cac tin nhan gui di can retry
     *
     * @param
     */
    // v2
    public void checkMessageWhenLoadFromDB(ReengMessage reengMessage) {
        ReengMessageConstant.Direction direction = reengMessage.getDirection();
        if (direction == ReengMessageConstant.Direction.send) {
            //tin nhan gui di
            //tin nhan nao la dang gui thi push vao waitingMap
            int status = reengMessage.getStatus();
            if (status == ReengMessageConstant.STATUS_LOADING ||
                    status == ReengMessageConstant.STATUS_NOT_SEND ||
                    status == ReengMessageConstant.STATUS_NOT_LOAD) {
                //neu da qua thoi gian
                if (TimeHelper.isPassedARangeTime(reengMessage.getTime(), Config.Message.RETRY_TIMEOUT)) {
                    reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
                    updateAllFieldsOfMessage(reengMessage);
                } else {
                    mMessageRetryManager.addToWaitingConfirmMap(reengMessage);
                    if (/*reengMessage.getMessageType() == ReengMessageConstant.MessageType.image ||
                     */reengMessage.getMessageType() == ReengMessageConstant.MessageType.voicemail) {
                        mApplication.getTransferFileBusiness().startUploadMessageFile(reengMessage);
                    }
                }
            } else if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.image
                    && TextUtils.isEmpty(reengMessage.getFilePath())
                    && !TextUtils.isEmpty(reengMessage.getDirectLinkMedia())
            ) {
                //bo sung luong tu dong download file tu server ve khi co link online va chua co file
                mApplication.getTransferFileBusiness().startDownloadMessageImageSend(reengMessage);
            }
        } else if (direction == ReengMessageConstant.Direction.received) {
            //tin nhan nhan duoc day vao listPacketIdReceived
            int status = reengMessage.getStatus();
            Log.d(TAG, "checkMessageWhenLoadFromDB: " + reengMessage.toString() + " status: " + status);
            if (status == ReengMessageConstant.STATUS_LOADING ||
                    status == ReengMessageConstant.STATUS_NOT_SEND ||
                    status == ReengMessageConstant.STATUS_NOT_LOAD) {
                // file not auto download
                if (TimeHelper.isPassedARangeTime(reengMessage.getTime(), Config.Message.RETRY_TIMEOUT)) {
                    if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.file) {
                        reengMessage.setStatus(ReengMessageConstant.STATUS_NOT_LOAD);
                    } else {
                        reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
                    }
                    updateAllFieldsOfMessage(reengMessage);
                } else if (reengMessage.getMessageType() != ReengMessageConstant.MessageType.file) {
                    mApplication.getTransferFileBusiness().startDownloadMessageFile(reengMessage);
                }
            }
        }
        // decode emoticon voi tin nhan text
        if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.text) {
            mApplication.getQueueDecodeEmo().addTask(reengMessage.getContent());
        }
    }

    public CopyOnWriteArrayList<ReengMessage> loadLimitMessage(int threadId, int limit, int firstOldMsgId) {
        return mReengMessageDataSource.getLimitMessagesOfThread_V2(threadId, limit, this, firstOldMsgId);

        /*CopyOnWriteArrayList<ReengMessage> moreMessagesOfThread = new CopyOnWriteArrayList<ReengMessage>();
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getId() == threadId) {
                moreMessagesOfThread = mReengMessageDataSource.getLimitMessagesOfThread_V2(threadId,
                        limit, this, firstOldMsgId);
                CopyOnWriteArrayList<ReengMessage> tmpListMessages = getListMessageNotSendSeen(threadMessage,
                moreMessagesOfThread);
                if (tmpListMessages != null && !tmpListMessages.isEmpty()) {
                    mApplication.getXmppManager().processSeenListMessage(tmpListMessages, threadMessage);
                    setSendMsgQuickReply(false);
                }
                break;
            }
        }
        return moreMessagesOfThread;*/
    }

    public CopyOnWriteArrayList<ThreadMessage> getThreadMessageArrayList() {
        return mThreadMessageArrayList;
    }

    public ArrayList<Integer> loadAllMessagesByThreadId(int threadId) {
        return mReengMessageDataSource.getAllMessagesIdsByThreadId(threadId);
    }

    public CopyOnWriteArrayList<ReengMessage> getMessagesWithinIds(int threadId, int startId, int endId) {
        return mReengMessageDataSource.getMessagesWithinIds(threadId, startId, endId);
    }

    public CopyOnWriteArrayList<ReengMessage> searchMessagesByTag(int threadId, String tag) {
        return mReengMessageDataSource.searchMessagesByTag(threadId, tag);
    }

    public ThreadMessage getThreadById(int threadId) {
        if (mThreadMessageArrayList == null) return null;
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getId() == threadId) {
                return threadMessage;
            }
        }
        return null;
    }

    public ReengMessage getMessageByMessageIdInThread(int messageId, ThreadMessage threadMessage) {
        if (threadMessage == null) {
            return null;
        }
        for (ReengMessage message : threadMessage.getAllMessages()) {
            if (message.getId() == messageId) {
                return message;
            }
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void updateThreadMessage(ThreadMessage threadMessage) {
        UpdateThreadMessageAsyncTask updateThread = new UpdateThreadMessageAsyncTask(threadMessage);
        updateThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void getInfoGroupIfNeeded(ThreadMessage mThreadMessage) {
        new MessageBusiness.GetGroupInfoAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mThreadMessage);
    }

    private class UpdateThreadMessageAsyncTask extends AsyncTask<Void, Void, Void> {
        private ThreadMessage threadMessage;

        public UpdateThreadMessageAsyncTask(ThreadMessage threadMessage) {
            this.threadMessage = threadMessage;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            updateThreadMessageDataSource(threadMessage);
            return null;
        }
    }

    private class UpdateMultiThreadMessageAsyncTask extends AsyncTask<Void, Void, Void> {
        private ArrayList<ThreadMessage> threadMessages;
        private ChangeNumberActivity.ChangeNumberMemberListener listener;

        public UpdateMultiThreadMessageAsyncTask(ArrayList<ThreadMessage> threadMessages, ChangeNumberActivity.ChangeNumberMemberListener activity) {
            this.threadMessages = threadMessages;
            this.listener = activity;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (ThreadMessage threadMessage : threadMessages)
                updateThreadMessageDataSource(threadMessage);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (listener != null) listener.onChangeNumberDone(true);
        }
    }

    public void updateThreadMessageDataSource(ThreadMessage threadMessage) {
        if (threadMessage != null) {
            mThreadMessageDataSource.updateThreadMessage(threadMessage);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void updateListThreadMessageBackground(ArrayList<ThreadMessage> listThreads) {
        UpdateListThreadBackgroundAsyncTask updateThread = new UpdateListThreadBackgroundAsyncTask(listThreads);
        updateThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class UpdateListThreadBackgroundAsyncTask extends AsyncTask<Void, Void, Void> {
        private ArrayList<ThreadMessage> listThreads;

        public UpdateListThreadBackgroundAsyncTask(ArrayList<ThreadMessage> listThreads) {
            this.listThreads = listThreads;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (listThreads != null) {
                mThreadMessageDataSource.updateListThreadMessageAfterChangeBackground(listThreads);
            }
            return null;
        }
    }

    public void insertThreadMessage(ThreadMessage threadMessage) {
        isWaitInsertThread = true;
        mThreadMessageDataSource.insertNewThreadMessage(threadMessage);
        threadMessage.setLoadMoreFinish(true);
        isWaitInsertThread = false;
        ListenerHelper.getInstance().onThreadsChanged();
    }

    public boolean checkAndUpdateBackground(ThreadMessage threadMessage, String background) {
        String oldBackground = threadMessage.getBackground();
        if (TextUtils.isEmpty(background)) {
            return false;
        } else if ((oldBackground == null) || (!oldBackground.equals(background))) {
            threadMessage.setBackground(background);
            updateThreadMessage(threadMessage);
            return true;
        }
        return false;
    }

    //for save draft
    public void updateAndNotifyThreadMessage(final ThreadMessage threadMessage) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                mThreadMessageDataSource.updateThreadMessage(threadMessage);
                refreshThreadWithoutNewMessage(threadMessage.getId());
            }
        }).start();
    }

    /**
     * get all message of a thread by threadId
     *
     * @param threadId id of thread
     * @return all message
     */
    public CopyOnWriteArrayList<ReengMessage> getMessagesByThreadId(int threadId) {
        ThreadMessage threadById = getThreadById(threadId);
        if (threadById == null) return null;
        else return threadById.getAllMessages();
    }

    public String getReceivedWhenSendMessage(ThreadMessage threadMessage, Activity activity) {
        if (threadMessage == null) {
            Toast.makeText(ApplicationController.self(), mRes.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
            return null;// thread chua biet
        }
        int mThreadType = threadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            return threadMessage.getSoloNumber();
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT
                || mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT
                || mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            if (TextUtils.isEmpty(threadMessage.getServerId())) {
                Toast.makeText(ApplicationController.self(), mRes.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
            }
            return threadMessage.getServerId();
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            return "broadcast" + threadMessage.getId();
        } else {
            Toast.makeText(ApplicationController.self(), mRes.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
            return null;// thread chua biet
        }
    }

    public void processResponseForNonReengUser(Packet receivedMessage) {
        if (checkDuplicatePacket(receivedMessage.getPacketID())) {
            return;
        }
        ReengMessage reengMessage = findMessageInMemByPacketId(receivedMessage.getPacketID());
        if (reengMessage == null) {
            //neu ko co trong memory
            reengMessage = mReengMessageDataSource.findMessageInDBByPacketId(receivedMessage.getPacketID());
            Log.i(TAG, "In DB " + reengMessage);
        }
        if (reengMessage == null) {
            //trong db ko con message nay nua
            return;
        }
        int oldStatus = reengMessage.getStatus();
        // chi cap nhat trang thai fail khi trang thai truoc do la not send, hoac loading
        if (oldStatus == ReengMessageConstant.STATUS_NOT_SEND ||
                oldStatus == ReengMessageConstant.STATUS_LOADING) {
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            updateAllFieldsOfMessage(reengMessage);
            boolean isGsmAvailable = false;
            refreshThreadWithoutNewMessage(reengMessage.getThreadId());
            onNonReengResponse(reengMessage.getThreadId(), reengMessage, isGsmAvailable, null);
        }
    }

    public ReengMessage findMessageInMemAndDBByPacketId(String packetId, ReengMessage reengMessage) {
        if (reengMessage == null) {
            //neu ko co trong map thi tim trong memory
            reengMessage = findMessageInMemByPacketId(packetId);
            Log.i(TAG, "in memory: " + reengMessage);
        }
        if (reengMessage == null) {
            //neu ko co trong memory
            reengMessage = mReengMessageDataSource.findMessageInDBByPacketId(packetId);
            Log.d(TAG, "In DB " + reengMessage);
        }
        if (reengMessage == null) {
            Log.d(TAG, "can not find message with packetid = " + packetId);
        }
        return reengMessage;
    }

    /**
     * xu ly ban tin confirm gui sms out thanh cong hay that bai
     *
     * @param receivedMessage
     * @param application
     */
    public void processResponseSmsOutPacket(ApplicationController application,
                                            ReengEventPacket receivedMessage, int threadType) {
        // gui lai ban tin confirm sau khi nhan dc deliver
        application.getXmppManager().sendDeliverMessage(receivedMessage, threadType, false, false);
        // xu ly luong cu
        String packetId = receivedMessage.getListIdOfEvent().get(0);
        Object object = mMessageRetryManager.removeFromWaitingConfirmMap(packetId);
        if (object == null) {

        } else if (!(object instanceof ReengMessage)) return;
        ReengMessage reengMessage = (ReengMessage) object;
        reengMessage = findMessageInMemAndDBByPacketId(packetId, reengMessage);
        if (reengMessage == null) {
            //trong db ko con message nay nua
            return;
        }
        String smsState = receivedMessage.getSmsState();
        int remain = TextHelper.parserIntFromString(receivedMessage.getSmsRemain(), -1);
        int threadId = reengMessage.getThreadId();
        ThreadMessage oldThread = findSoloThreadByThreadId(threadId);
        reengMessage.setChatMode(ReengMessageConstant.MODE_GSM);
        String notifyContent = null;
        if ("0".equals(smsState)) {
            reengMessage.setStatus(ReengMessageConstant.STATUS_DELIVERED);
            String smsNote;
            if (remain == 0) {// message =0
                //                smsNote = String.format(mRes.getString(R.string.msg_mocha_sms_remain), remain);
                smsNote = mRes.getString(R.string.msg_mocha_sms_remain);
                notifyContent = mRes.getString(R.string.msg_mocha_sms_rm_0);
            } else if (remain == -1) {// tru dc tien
                smsNote = mRes.getString(R.string.msg_mocha_sms);
            } else if (remain == -2) {  // gui voice mail.....
                smsNote = "";
            } else {
                //                smsNote = String.format(mRes.getString(R.string.msg_mocha_sms_remain), remain);
                smsNote = mRes.getString(R.string.msg_mocha_sms_remain);
            }
            if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic) {
                // stop timer. reset session. chuyen trang thai accepted
                if (reengMessage.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {
                    reengMessage.setMusicState(ReengMessageConstant.MUSIC_STATE_ACCEPTED);
                    reengMessage.setDuration(0);
                    CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(reengMessage);
                    mApplication.getMusicBusiness().resetSessionMusic();
                }
            } else if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.text) {
                reengMessage.setFileName(smsNote);
            } else {
                //TODO khong lam gì voi message # text
            }
            LuckyWheelHelper.getInstance(ApplicationController.self()).doMission(Constants.LUCKY_WHEEL.ITEM_SMS_OUT); // TODO:  gửi sms out
        } else if ("401".equals(smsState)) {// sms out loi ko tru dc tien
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            onNonReengResponse(threadId, reengMessage, false, mRes.getString(R.string.msg_mocha_sms_err_401));
        } else if ("-1".equals(smsState)) { // that bai do het tin free. chua co cs tru tien
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            onNonReengResponse(threadId, reengMessage, true, mRes.getString(R.string.msg_mocha_sms_err__1));
        } else if ("-10".equals(smsState)) {    // that bai do ko phai so vt
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            onNonReengResponse(threadId, reengMessage, false, mRes.getString(R.string.msg_mocha_sms_err__10));
        } else if ("-11".equals(smsState)) {    // that bai do tin nhan qua dai
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            onNonReengResponse(threadId, reengMessage, false, String.format(mRes.getString(R.string
                    .msg_mocha_sms_err__11), remain));
        } else if ("-3".equals(smsState)) {// hien message notify loi
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            notifyContent = receivedMessage.getBody();
            Log.d(TAG, "notifyContent: " + notifyContent);
        } else if ("-4".equals(smsState)) {// show toast noi dung loi
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            if (!TextUtils.isEmpty(receivedMessage.getBody())) {
                onNonReengResponse(threadId, reengMessage, false, receivedMessage.getBody());
            }
        } else if ("-5".equals(smsState)) {// chi cap nhat thanh fail, khong show thong bao loi
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            notifyContent = null;
        } else {                           // ma loi #
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            onNonReengResponse(threadId, reengMessage, false, mRes.getString(R.string.msg_mocha_sms_err_unknown));
        }
        if (!TextUtils.isEmpty(notifyContent)) {//content !=null thi show message notify
            ReengMessage messageNotify = insertMessageNotify(notifyContent, reengMessage.getReceiver(),
                    reengMessage.getSender(), oldThread, ReengMessageConstant.READ_STATE_READ, TimeHelper
                            .getCurrentTime());
            notifyNewMessage(messageNotify, oldThread);
        }
        refreshThreadWithoutNewMessage(reengMessage.getThreadId());
        updateAllFieldsOfMessage(reengMessage);
    }

    /**
     * xu ly ban tin confirm sv
     *
     * @param receivedPacket
     */
    public void processReceivedPacket(ApplicationController application, Packet receivedPacket) {
        long t = System.currentTimeMillis();
        application.getXmppManager().setLastTimeOfReceivedPacket(System.currentTimeMillis());
        String packetId = getPacketId(receivedPacket);
        Object object = mMessageRetryManager.removeFromWaitingConfirmMap(packetId);
        Log.d(TAG, "[perform] get packet from waiting map take: " + (System.currentTimeMillis() - t));
        ReengMessage reengMessage;
        if (object == null || (object instanceof ReengMessage)) {
            //neu ko co trong list waiting
            if (object != null) {
                reengMessage = findMessageInMemAndDBByPacketId(packetId, (ReengMessage) object);
            } else {
                reengMessage = findMessageInMemAndDBByPacketId(packetId, null);
            }
            if (reengMessage == null) {// ko con message nay nua
                mApplication.logDebugContent("cant find msg with id: " + packetId);
                Log.f(TAG, "cant find msg with id: " + packetId);
                return;
            }
            if (object != null) {// message dang doi received tren mem
                // moi log
                long differentTime = System.currentTimeMillis() - reengMessage.getTime();
                Log.f(TAG, "[perform] sent message take : " + differentTime + " packetId: " + packetId);
                // >10s thường rơi vào th phải retry lên ko log th này
                if (mAccountBusiness.isEnableUploadLog() && differentTime < 10000) {
                    LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.MESSAGE_SEND_RECEIVE, differentTime + "", t + "", "200", reengMessage.getPacketId());
                }
            }
            // chi cap nhat thanh da gui khi chua cap nhat fail
            if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic &&
                    reengMessage.getStatus() == ReengMessageConstant.STATUS_NOT_SEND) {
                // cung nghe group thi bo qua
                if (reengMessage.getMusicState() == ReengMessageConstant.MUSIC_STATE_GROUP ||
                        reengMessage.getMusicState() == ReengMessageConstant.MUSIC_STATE_ROOM ||
                        reengMessage.getMusicState() == ReengMessageConstant.MUSIC_STATE_REQUEST_CHANGE) {
                    Log.d(TAG, "cung nghe group");
                } else if (reengMessage.getMusicState() != ReengMessageConstant.MUSIC_STATE_ACCEPTED) {
                    // ban tin dong y cung nghe nguoi la de state la accepted
                    // huy timer cho receive ban tin invite
                    application.getMusicBusiness().stopTimerReceiveResponse();
                    reengMessage.setDuration(CountDownInviteManager.COUNT_DOWN_DURATION);
                    CountDownInviteManager.getInstance(mApplication).startCountDownMessage(reengMessage);
                    reengMessage.setMusicState(ReengMessageConstant.MUSIC_STATE_WAITING);
                    // ban tin received cua sv cho ms invite lay packet id lam sesion id luon
                    application.getMusicBusiness().onCreateSessionMusic(reengMessage.getImageUrl(),
                            reengMessage.getReceiver(), reengMessage.getSongModel(mApplication.getMusicBusiness()));
                    // cap nhat thread cuoi cung gui loi moi
                    application.getMusicBusiness().updatePrefSendInviteMusic(reengMessage.getThreadId());
                }
            }
            reengMessage.setStatus(ReengMessageConstant.STATUS_SENT);
            notifyMessageSentSuccessfully(reengMessage.getThreadId());
            updateAllFieldsOfMessage(reengMessage); //ham nay chiem nhieu thoi gian thuc hien nhat
        } else if (object instanceof ReengEventPacket) {
            //ban tin confirm cho ban tin deliver = 0, = 1 (seen 1 tin, seen list)
            ReengEventPacket reengEventPacket = (ReengEventPacket) object;
            ArrayList<ReengMessage> updateSentSeenStateMessageList = new ArrayList<>();
            for (String id : reengEventPacket.getListIdOfEvent()) {
                ReengMessage messageByPacketId = findMessageInMemAndDBByPacketId(id, null);
                if (messageByPacketId != null) {
                    Log.i(TAG, "sent seen succeed for message " + messageByPacketId);
                    messageByPacketId.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                    updateSentSeenStateMessageList.add(messageByPacketId);
                } else {
                    Log.f(TAG, "processReceived seen but no message have id " + id);
                    mApplication.logDebugContent("processReceived seen but no message have id " + id);
                }
            }
            if (!updateSentSeenStateMessageList.isEmpty())
                updateAllFieldsOfListMessage(updateSentSeenStateMessageList);
        }
    }

    /**
     * xu ly ban tin event
     *
     * @param receivedMessage
     * @param application
     */
    public void processDeliverReengPacket(ApplicationController application, ReengEventPacket receivedMessage) {
        //trong group chat, tin nhan minh gui di tu desktop, mobile nhan duoc se ko danh dau la tin nhan da nhan
        if (receivedMessage.getType() == ReengMessagePacket.Type.groupchat
                && receivedMessage.getSubType() == ReengMessagePacket.SubType.event
                && receivedMessage.getEventType() != ReengEventPacket.EventType.react) {
            String member = receivedMessage.getSender();
            if (application.getReengAccountBusiness().getJidNumber().equals(member)) return;
        }

        String from = receivedMessage.getFrom();
        int threadId = -1;
        ArrayList<ReengMessage> listReengMessageUpdate = new ArrayList<>();
        ArrayList<EventMessage> listEventMessageInsert = new ArrayList<>();
        for (String packetId : receivedMessage.getListIdOfEvent()) {
            Object object = mMessageRetryManager.removeFromWaitingConfirmMap(packetId);
            // khong phai ReengMessage thi bo qua, tiep tuc vong lap sau
            if (object != null && !(object instanceof ReengMessage)) {
                continue;
            }
            ReengMessage reengMessage = (ReengMessage) object;
            if (reengMessage == null) {
                reengMessage = findMessageInMemAndDBByPacketId(packetId, reengMessage);
            }
            if (reengMessage == null) {
                //trong db ko con message nay nua
                Log.d(TAG, "processDeliverReengPacket can not find message with packetid = " + packetId);
                application.logDebugContent("processDeliverReengPacket can not find message with packetid = " + packetId);
                continue;
            }
            // khong cap nhat neu ban tin la loai ban tin den
            if (reengMessage.getDirection() == ReengMessageConstant.Direction.received
                    && receivedMessage.getEventType() != ReengEventPacket.EventType.react
                    && TextUtils.isEmpty(receivedMessage.getToRef())
                    && !(receivedMessage.getType() == ReengMessagePacket.Type.groupchat
                    && mApplication.getReengAccountBusiness().getJidNumber().equals(receivedMessage.getSender()))) {
                continue;
            }
            int addReact = receivedMessage.getAddReact();
            int removeReact = receivedMessage.getRemoveReact();
            int seenState = receivedMessage.getSeenState();
            // them moi eventMessage, them moi ca khi tin nhan la tin gui di hoac tin nhan duoc
            EventMessage eventMessage = new EventMessage();
            eventMessage.setMessageId(reengMessage.getId());
            if (receivedMessage.getType() == ReengMessagePacket.Type.groupchat) {
                eventMessage.setSender(receivedMessage.getSender());
            } else {//chat 1-1
                if (!TextUtils.isEmpty(receivedMessage.getToRef())) {
                    from = receivedMessage.getToRef().split("@")[0].trim();
                } else {
                    from = from.split("@")[0].trim();
                }
                eventMessage.setSender(from);

            }
            eventMessage.setTime(receivedMessage.getTimeSend());

            if (receivedMessage.getEventType() != ReengEventPacket.EventType.react) {
                if (seenState == ReengMessageConstant.SEEN_FLAG_NOT_SEEING) {
                    eventMessage.setStatus(ReengMessageConstant.STATUS_DELIVERED);
                } else if (seenState == ReengMessageConstant.SEEN_FLAG_SEEING ||
                        seenState == ReengMessageConstant.SEEN_FLAG_LIST) {
                    eventMessage.setStatus(ReengMessageConstant.STATUS_SEEN);
                }
            }

            // them truong packet id va thread id
            eventMessage.setPacketId(reengMessage.getPacketId());
            eventMessage.setThreadId(reengMessage.getThreadId());

            if (receivedMessage.getEventType() == ReengEventPacket.EventType.react) {
                EventMessage eventRemove = getEventMessageReaction(reengMessage.getId(), packetId, eventMessage.getSender());
                if (addReact != -1) {
                    if (eventRemove != null) {
                        deleteEventReaction(reengMessage, eventRemove, eventRemove.getReaction());
                    }
                    //nếu tìm thấy event react rồi thì update vào event react cũ
                    eventMessage.setStatus(EventMessageConstant.EVENT_REACTION_STATE);
                    eventMessage.setReaction(addReact);
                    ArrayList<Integer> listReact = reengMessage.getListReaction();
                    if (listReact == null || listReact.size() != ReengMessageConstant.Reaction.DEFAULT.ordinal())
                        listReact = createArrayListReact();
                    int indexAdd = ReengMessageConstant.Reaction.fromInteger(addReact).ordinal();
                    if (indexAdd < ReengMessageConstant.Reaction.DEFAULT.ordinal()) {
                        int newValue = listReact.get(indexAdd) + 1;
                        listReact.set(indexAdd, newValue);
                    }
                    reengMessage.setListReaction(listReact);
                    listEventMessageInsert.add(eventMessage);

                    if (reengMessage.getDirection() == ReengMessageConstant.Direction.send) {
                        ThreadMessage threadMessage = findThreadByThreadId(reengMessage.getThreadId());
                        if (threadMessage != null) {
                            ReengNotificationManager.getInstance(application).drawMessagesReactionNotification
                                    (application, threadMessage, eventMessage.getSender());
                        }
                        mApplication.updateCountNotificationIcon();
                    }
                } else if (removeReact != -1) {
                    //tìm event message trong db xem có ko
                    if (eventRemove != null) {
                        deleteEventReaction(reengMessage, eventRemove, removeReact);
                    }
                }
            } else {
                listEventMessageInsert.add(eventMessage);
            }

            if (seenState == ReengMessageConstant.SEEN_FLAG_NOT_SEEING) {
                // cap nhat delivered khi message chua nhan dc seen
                if (reengMessage.getStatus() != ReengMessageConstant.STATUS_SEEN
                        && reengMessage.getStatus() != ReengMessageConstant.STATUS_RECEIVED) {
                    reengMessage.setTimeDelivered(System.currentTimeMillis());
                    reengMessage.setStatus(ReengMessageConstant.STATUS_DELIVERED);
                }
            } else if (seenState == ReengMessageConstant.SEEN_FLAG_SEEING) {
                if (listMessageProcessInfo != null && reengMessage.getTimeDelivered() == 0
                        && reengMessage.getStatus() != ReengMessageConstant.STATUS_SEEN) {
                    reengMessage.setTimeSeen(System.currentTimeMillis());
                    reengMessage.setIsForceShow(true);
                    listMessageProcessInfo.add(reengMessage);
                }
                reengMessage.setStatus(ReengMessageConstant.STATUS_SEEN);
            } else if (seenState == ReengMessageConstant.SEEN_FLAG_LIST) {
                if (listMessageProcessInfo != null
                        && reengMessage.getStatus() != ReengMessageConstant.STATUS_SEEN) {
                    reengMessage.setIsForceShow(true);
                    listMessageProcessInfo.add(reengMessage);
                }
                reengMessage.setTimeSeen(System.currentTimeMillis());
                reengMessage.setStatus(ReengMessageConstant.STATUS_SEEN);
            } else {
                Log.i(TAG, "need to process here");
            }
            // them message vao list update db
            listReengMessageUpdate.add(reengMessage);
            threadId = reengMessage.getThreadId();
        }
        // khi gui list id seen la th cung 1 thread, nen thread id giong nhau trong list
        if (threadId != -1) {
            refreshThreadWithoutNewMessage(threadId);

            if (receivedMessage.getSeenState() >= ReengMessageConstant.SEEN_FLAG_SEEING) {
                if (!TextUtils.isEmpty(receivedMessage.getToRef()) ||
                        (receivedMessage.getType() == ReengMessagePacket.Type.groupchat
                                && mApplication.getReengAccountBusiness().getJidNumber().equals(receivedMessage.getSender()))) {
                    ThreadMessage threadMessage = getThreadById(threadId);
                    if (threadMessage != null) {
                        threadMessage.setNumOfUnreadMessage(0);
                        updateThreadMessage(threadMessage);
                        MessageNotificationManager.getInstance(application).clearNotifyThreadMessage(threadMessage);
                        mApplication.updateCountNotificationIcon();
                    }
                }
            }

        }
        // cap nhat database message va insert list event_message.0
        updateAllFieldsOfListMessage(listReengMessageUpdate);
        //chi luu bang event message voi message 1-1 va group
        if (receivedMessage.getType() == ReengMessagePacket.Type.chat ||
                (receivedMessage.getType() == ReengMessagePacket.Type.groupchat
                        && !mApplication.getReengAccountBusiness().getJidNumber().equals(receivedMessage.getSender()))) {
            insertListEventMessageToDB(listEventMessageInsert);
        }
    }

    private void deleteEventReaction(ReengMessage reengMessage, EventMessage eventMessage, int removeReact) {
        ArrayList<Integer> listReact = reengMessage.getListReaction();
        if (listReact == null || listReact.size() != ReengMessageConstant.Reaction.DEFAULT.ordinal())
            listReact = createArrayListReact();
        int indexRemove = ReengMessageConstant.Reaction.fromInteger(removeReact).ordinal();
        if (indexRemove < ReengMessageConstant.Reaction.DEFAULT.ordinal()) {
            int newValue = listReact.get(indexRemove) - 1;
            if (newValue < 0) newValue = 0;
            listReact.set(indexRemove, newValue);
        }
        reengMessage.setListReaction(listReact);
        mEventMessageDataSource.deleteEventMessage(eventMessage);
    }

    /**
     * xu ly ban tin update version
     *
     * @param receivedMessage
     */
    public void processForceUpdate(ReengEventPacket receivedMessage) {
        String body = receivedMessage.getBody();
        String link = receivedMessage.getLink();
        boolean force = receivedMessage.isForce();
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        accountBusiness.setValidRevision(!force);
        accountBusiness.setMsgForceUpdate(body);
        accountBusiness.setUrlForceUpdate(link);
        // disconect
        if (force && mApplication.getXmppManager() != null) {
            mApplication.getXmppManager().manualDisconnect();
        }
        notifyForceUpdate(body, link, force);
    }

    private String getPacketId(Packet s) {
        String packetId = s.toXML().split("<id>")[1];
        packetId = packetId.split("</id>")[0].trim();
        Log.i(TAG, "packet Id = " + packetId);
        return packetId;
    }

    public ReengMessage findMessageInMemByPacketId(String packetID) {
        packetID = packetID.toLowerCase();
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            for (ReengMessage reengMessage : threadMessage.getAllMessages()) {
                if (reengMessage.getPacketId() != null && packetID.equals(reengMessage.getPacketId().toLowerCase())) {
                    return reengMessage;
                }
            }
        }
        return null;
    }

    /**
     * tim message invite (car gui va nhan)
     *
     * @param thread
     * @return reengMessage
     */
    public ReengMessage findLastMessageAllInviteMusic(ThreadMessage thread) {
        if (thread == null) {
            return null;
        }
        CopyOnWriteArrayList<ReengMessage> listMessage = thread.getAllMessages();
        if (listMessage != null) {
            for (int i = listMessage.size() - 1; i >= 0; i--) {
                ReengMessage message = listMessage.get(i);
                if (message.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic) {
                    return message;
                }
            }
        }
        return null;
    }

    /**
     * tim message invite (tin gui di)
     *
     * @param thread
     * @return reengMessage
     */
    public ReengMessage findLastMessageSendInviteMusic(ThreadMessage thread) {
        if (thread == null) {
            return null;
        }
        CopyOnWriteArrayList<ReengMessage> listMessage = thread.getAllMessages();
        if (listMessage != null) {
            for (int i = listMessage.size() - 1; i >= 0; i--) {
                ReengMessage message = listMessage.get(i);
                if (message.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic &&
                        message.getDirection() == ReengMessageConstant.Direction.send) {
                    return message;
                }
            }
        }
        return null;
    }

    /**
     * tim message invite theo session id
     *
     * @param sessionMusicId
     * @param threadMessage
     * @return
     */
    public ReengMessage findMessageInviteMusicBySessionMusicId(String sessionMusicId, ThreadMessage threadMessage) {
        if (threadMessage == null || threadMessage.getAllMessages() == null) {
            return null;
        }
        CopyOnWriteArrayList<ReengMessage> listMessage = threadMessage.getAllMessages();
        if (listMessage != null) {
            for (int i = 0; i < listMessage.size(); i++) {
                ReengMessage message = listMessage.get(i);
                if (message.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic &&
                        message.getImageUrl() != null && message.getImageUrl().equals(sessionMusicId)) {
                    return message;
                }
            }
        }
        return null;
    }

    /**
     * get name of thread message
     *
     * @param threadMessage
     * @return thread name
     */
    public String getThreadName(ThreadMessage threadMessage) {
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String numberFriend = threadMessage.getSoloNumber();
            threadMessage.setName(getFriendName(numberFriend));
        }
        return threadMessage.getThreadName();
    }

    /**
     * get friend name from number
     *
     * @param number
     * @return
     */
    public String getFriendName(String number) {
        if (TextUtils.isEmpty(number)) return "";
        mContactBusiness = mApplication.getContactBusiness();
        PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
        if (phoneNumber != null) {// so co trong danh ba tra ten trong danh ba
            return phoneNumber.getName();
        } else {
            StrangerPhoneNumber strangerPhoneNumber = mApplication.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(number);
            if (strangerPhoneNumber != null) { // neu so co trong bang stranger thi lay ten o day
                String name = strangerPhoneNumber.getFriendName();
                if (TextUtils.isEmpty(name)) {
                    return Utilities.hidenPhoneNumber(number);
                } else
                    return strangerPhoneNumber.getFriendName();
            } else {
                NonContact nonContact = mContactBusiness.getExistNonContact(number);
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
                    return nonContact.getNickName();
                }
            }
        }
        // nguoc lai tra ve so dt
        return PhoneNumberHelper.getInstant().getRawNumber(mApplication, number);
    }

    /**
     * get friend name from number
     *
     * @param number
     * @return
     */
    public String getFriendNameOfGroup(String number) {
        if (number.equals(mApplication.getReengAccountBusiness().getJidNumber()))
            return mRes.getString(R.string.you);
        mContactBusiness = mApplication.getContactBusiness();
        PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
        if (phoneNumber != null) {// so co trong danh ba tra ten trong danh ba
            return phoneNumber.getName();
        } else {
            NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(number);
            if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                return nonContact.getNickName();
            }
        }
        return PhoneNumberHelper.getInstant().getRawNumber(mApplication, number);
    }

    public String getFriendNameOfLixi(String number, boolean lowerCase) {
        if (TextUtils.isEmpty(number)) return "";
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (number.equals(myNumber)) {
            String myName = mRes.getString(R.string.non_you);
            if (lowerCase) myName = myName.toLowerCase();
            return myName;
        } else {
            return getFriendNameOfGroup(number);
        }
    }

    /**
     * get friend name from number
     *
     * @param number
     * @return
     */
    public String getFriendNameOfRoom(String number, String senderName, String roomName) {
        mContactBusiness = mApplication.getContactBusiness();
        PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
        if (phoneNumber != null) {// so co trong danh ba tra ten trong danh ba
            return phoneNumber.getName();
        } else {
            if (!TextUtils.isEmpty(senderName)) {
                return senderName;
            } else if (!TextUtils.isEmpty(roomName)) {
                return roomName;
            }
        }
        // nguoc lai tra ve so dt
        return Utilities.hidenPhoneNumber(PhoneNumberHelper.getInstant().getRawNumber(mApplication, number));
    }

    public void notifyReengMessage(ApplicationController application, ThreadMessage mCorrespondingThread
            , ReengMessage reengMessage, int threadType) {
        notifyReengMessage(application, mCorrespondingThread, reengMessage, threadType, false);

    }

    public void notifyReengMessage(ApplicationController application,
                                   ThreadMessage mCorrespondingThread,
                                   ReengMessage reengMessage, int threadType, boolean noNotify) {
        ReengNotificationManager mReengNotificationManager = ReengNotificationManager.getInstance(application);
        if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.restore) {
            ReengMessage message = findMessageInMemByPacketId(reengMessage.getFileName());
            Log.d(TAG, "In Memory " + reengMessage);
            if (message == null) {
                //neu ko co trong memory
                message = mReengMessageDataSource.findMessageInDBByPacketId(reengMessage.getFileName());
                Log.d(TAG, "In DB " + message);
            }
            if (message != null) {
                message.setMessageType(ReengMessageConstant.MessageType.restore);
                message.setReadState(ReengMessageConstant.READ_STATE_READ);
                message.setStatus(ReengMessageConstant.STATUS_SEEN);
                updateAllFieldsOfMessage(message);
                refreshThreadWithoutNewMessage(message.getThreadId());
            }
        } else if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.pin_message) {
            Log.i(TAG, "message pin");
            if (reengMessage.getSize() == PinMessage.ActionPin.ACTION_PIN.VALUE) {
                mCorrespondingThread.setPinMessage(new PinMessage(reengMessage.getSongId(),
                        reengMessage.getContent(), reengMessage.getFileName(), reengMessage.getImageUrl(),
                        reengMessage.getFilePath()));
            } else {
                mCorrespondingThread.setPinMessage("");
            }
            if (reengMessage.getSongId() == PinMessage.TypePin.TYPE_MESSAGE.VALUE) {
                this.insertNewMessageToDB(mCorrespondingThread, reengMessage);
                if (reengMessage.getDirection() != ReengMessageConstant.Direction.send)
                    mCorrespondingThread.setNumOfUnreadMessage(mCorrespondingThread.getNumOfUnreadMessage() + 1);
                notifyNewMessage(reengMessage, mCorrespondingThread);
            } else if (reengMessage.getSongId() == PinMessage.TypePin.TYPE_DEEPLINK.VALUE) {
                notifyNewPinMessage(reengMessage, mCorrespondingThread);
            }
            updateThreadMessage(mCorrespondingThread);
        } else {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                if (reengMessage.isSticky()) {
                    this.insertNewMessageToDB(mCorrespondingThread, reengMessage);
                }
                //insert message vao thread trong memory va database
            } else {
                //insert message vao thread trong memory va database
                long id = this.insertNewMessageToDB(mCorrespondingThread, reengMessage);
                mApplication.logDebugContent("insertNewMessageToDB id: " + id);
            }

            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.enable_e2e) {
                    mCorrespondingThread.setEncryptThread(reengMessage.getStateEnableE2E());
                    EventBus.getDefault().post(new ThreadDetailSettingFragment.ThreadDetailSettingEvent(mCorrespondingThread.getId(), reengMessage));
                } else if ((reengMessage.getMessageType() == ReengMessageConstant.MessageType.text
                        && !TextUtils.isEmpty(reengMessage.getPreKeyTmp()))) {
                    if (!mCorrespondingThread.isEncryptThread()) {
                        mCorrespondingThread.setEncryptThread(1);
                        EventBus.getDefault().post(new ThreadDetailSettingFragment.ThreadDetailSettingEvent(mCorrespondingThread.getId(), reengMessage));
                    }
                }
            }

            //------------------tang so tin nhan chua doc--------------------------
            if (reengMessage.getDirection() != ReengMessageConstant.Direction.send)
                mCorrespondingThread.setNumOfUnreadMessage(mCorrespondingThread.getNumOfUnreadMessage() + 1);
            //----------------------------------------------------------------------
            notifyNewMessage(reengMessage, mCorrespondingThread);
            ReengMessageConstant.MessageType type = reengMessage.getMessageType();
            if (type == ReengMessageConstant.MessageType.voicemail
                    // || type == ReengMessageConstant.MessageType.file//TODO file not auto download
                    || type == ReengMessageConstant.MessageType.image) {
                mApplication.getTransferFileBusiness().startDownloadMessageFile(reengMessage);
            } else if (type == ReengMessageConstant.MessageType.gift) {
                mApplication.getStickerBusiness().checkAndDownLoadGifFile(reengMessage);
            }
            //==========QuickReply========

            if (threadType != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                if (!noNotify)
                    if (reengMessage.getDirection() != ReengMessageConstant.Direction.send)
                        mReengNotificationManager.drawMessagesNotification(application, mCorrespondingThread,
                                threadType, reengMessage);
                mApplication.updateCountNotificationIcon();
            }
            updateThreadMessage(mCorrespondingThread);
            if (mApplication.getAppLockManager().isEnableSettingLockApp()) {
                return;
            }
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                    && mSettingBusiness.getPrefMsgInPopup() && mSettingBusiness.getPrefPreviewMsg()
                    && !mSettingBusiness.checkExistInSilentThread(mCorrespondingThread.getId())) {
                Log.d(TAG, "type====" + reengMessage.getMessageType().toString());
                if (type == ReengMessageConstant.MessageType.text
                        || type == ReengMessageConstant.MessageType.voicemail
                        || type == ReengMessageConstant.MessageType.file
                        || type == ReengMessageConstant.MessageType.image
                        || type == ReengMessageConstant.MessageType.shareVideo
                        || type == ReengMessageConstant.MessageType.shareContact
                        || type == ReengMessageConstant.MessageType.inviteShareMusic
                        || type == ReengMessageConstant.MessageType.voiceSticker
                        || type == ReengMessageConstant.MessageType.greeting_voicesticker
                        || type == ReengMessageConstant.MessageType.transferMoney
                        || type == ReengMessageConstant.MessageType.gift
                        || type == ReengMessageConstant.MessageType.watch_video) {
                    if (reengMessage.getDirection() != ReengMessageConstant.Direction.send)
                        showQuickReplyActivity(application.getApplicationContext(), reengMessage, mCorrespondingThread);
                }
            }
        }
    }

    /**
     * toanvk2
     *
     * @param application
     * @param receivedMessage
     */
    public void processIncomingOfficerMessage(ApplicationController application, ReengMessagePacket receivedMessage) {
        //gui ban tin confirm da nhan
        String officerId = receivedMessage.getFrom().split("@")[0];     //lay offical id
        String packetId = receivedMessage.getPacketID();
        String officerName = receivedMessage.getOfficalName();                 //lay ten offical account
        String avatarUrl = receivedMessage.getAvatarUrl();
        ThreadMessage mCorrespondingThread = application.getMessageBusiness().
                findExistingOrCreateOfficerThread(officerId, officerName, avatarUrl, OfficerAccountConstant
                        .ONMEDIA_TYPE_NONE);
        if (!receivedMessage.isNoStore()) {
            boolean isOnChatScreen = (mCorrespondingThread.getId() == ReengNotificationManager.getCurrentUIThreadId());
            SettingBusiness mSetting = SettingBusiness.getInstance(mApplication);
            boolean isSendSeen = mSetting.getPrefEnableSeen();
            //gui ban tin deliver truoc khi check duplicate
            mApplication.getXmppManager().sendDeliverMessage(receivedMessage, ThreadMessageConstant
                    .TYPE_THREAD_OFFICER_CHAT, isOnChatScreen, isSendSeen, receivedMessage.getIdCamp(), receivedMessage.getNameCamp());

            /*application.getXmppManager().sendConfirmDeliverMessage(packetId, officerId, null, ThreadMessageConstant
                    .TYPE_THREAD_OFFICER_CHAT, receivedMessage.getIdCamp());*/
        }
        // check dulicate ban tin
        if (checkDuplicatePacket(packetId)) {
            return;
        }
        mIncomingMessageProcessor.processIncomingOfficerMessage(application, receivedMessage, officerId, mCorrespondingThread);
    }

    /**
     * @param threadId
     */
    public ReengMessage createFakeMessageText(int threadId, String fromNumber, String userNumber, String body) {
        ReengMessage message = new ReengMessage();// khong gui mes nen ko can packetid chuan
        message.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
        message.setReceiver(userNumber);
        message.setReadState(ReengMessageConstant.READ_STATE_UNREAD);
        message.setThreadId(threadId);
        message.setDirection(ReengMessageConstant.Direction.received);
        // set time
        Date date = new Date();
        message.setTime(date.getTime());
        message.setSender(fromNumber);
        message.setMessageType(ReengMessageConstant.MessageType.text);
        message.setStatus(ReengMessageConstant.STATUS_RECEIVED);
        message.setContent(body);
        return message;
    }

    public ReengMessage createFakeMessageDeepLink(int threadId, String fromNumber, String userNumber
            , String body, String leftLabel, String leftAction, String rightLabel, String rightAction) {
        ReengMessage message = new ReengMessage();// khong gui mes nen ko can packetid chuan
        message.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
        message.setReceiver(userNumber);
        message.setReadState(ReengMessageConstant.READ_STATE_UNREAD);
        message.setThreadId(threadId);
        message.setDirection(ReengMessageConstant.Direction.received);
        Date date = new Date();// set time
        message.setTime(date.getTime());
        message.setSender(fromNumber);
        message.setMessageType(ReengMessageConstant.MessageType.deep_link);
        message.setContent(body);
        message.setDeepLinkLeftLabel(leftLabel);
        message.setDeepLinkLeftAction(leftAction);
        message.setDeepLinkRightLabel(rightLabel);
        message.setDeepLinkRightAction(rightAction);
        message.setFilePath(null);  // service type
        message.setDeepLinkInfo(false);  // convert sang json luu vao db
        message.setStatus(ReengMessageConstant.STATUS_RECEIVED);
        return message;
    }

    public boolean createAndSendMessageImage(BaseSlidingFragmentActivity activity
            , ThreadMessage threadMessage, ArrayList<String> paths, boolean isBoiDoi) {
        if (activity == null || threadMessage == null || Utilities.isEmpty(paths)) return false;
        int mThreadType = threadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return false;// room chat khong cho gui anh
        }
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        ArrayList<ReengMessage> reengMessages = new ArrayList<>();
        for (String filePath : paths) {
            SoloSendImageMessage imageMessage = new SoloSendImageMessage(threadMessage, myNumber, receiver, filePath);
            MessageImageDB msgImage = getMessageImageDB(filePath);
            if (msgImage != null) {
                Log.i(TAG, "msgImage: " + msgImage.toString());
                imageMessage.setVideoContentUri(msgImage.getRatio());
                try {
                    JSONObject object = new JSONObject(msgImage.getDataResponse());
                    String fileId = object.optString(Constants.HTTP.REST_DESC, null);
                    String videoThumb = object.optString(Constants.HTTP.REST_THUMB, null);
                    String directLink = object.optString("link", null);
                    imageMessage.setImageUrl(videoThumb);
                    imageMessage.setFileId(fileId);
                    imageMessage.setDirectLinkMedia(directLink);
                    reengMessages.add(imageMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                MessageHelper.setRatioForMessageImage(imageMessage, filePath);
                reengMessages.add(imageMessage);
            }
        }
        insertListNewMessageBeforeSend(threadMessage, reengMessages);
        for (ReengMessage message : reengMessages) {
            if (!TextUtils.isEmpty(message.getDirectLinkMedia())) {
                sendXMPPMessage(message, threadMessage);
            } else {
                mApplication.getTransferFileBusiness().startUploadMessageFile(message);
            }
        }
        return true;
    }

    public void createAndSendMessageFile(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , String path, String fileType, String fileName) {
        int mThreadType = threadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return;// room chat khong cho gui anh
        }
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        ArrayList<ReengMessage> reengMessages = new ArrayList<>();
        ReengFileMessage message = new ReengFileMessage(threadMessage, myNumber, receiver, path, fileType, fileName);
        reengMessages.add(message);
        mApplication.getTransferFileBusiness().startUploadMessageFile(message);
        insertListNewMessageBeforeSend(threadMessage, reengMessages);
    }

    public void createAndSendMessageInviteMusic(ThreadMessage threadMessage, MediaModel songModel
            , BaseSlidingFragmentActivity activity) {
        if (threadMessage == null || songModel == null) {
            activity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        int mThreadType = threadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT ||
                mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            return;// officer va broadcast khong cho gui invite music
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);

        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        MusicBusiness musicBusiness = mApplication.getMusicBusiness();
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloShareMusicMessage shareMusicMessage = new SoloShareMusicMessage(threadMessage, myNumber, receiver);
        shareMusicMessage.setPacketId(PacketMessageId.getInstance().genShareMusicPacketId(mThreadType,
                ReengMessagePacket.SubType.music_invite));
        // lay session la packet id luon (ban tin minh tao ra)
        shareMusicMessage.setImageUrl(shareMusicMessage.getPacketId());
        shareMusicMessage.setSongId(ReengMessage.SONG_ID_DEFAULT_NEW);
        shareMusicMessage.setSongModel(songModel);
        shareMusicMessage.setMessageType(ReengMessageConstant.MessageType.inviteShareMusic);
        String content = "";
        String contentBold = "";
        String threadName = getThreadName(threadMessage);
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            content = String.format(mRes.getString(R.string.invite_share_music_send),
                    threadName);
            contentBold = String.format(mRes.getString(R.string.invite_share_music_send),
                    TextHelper.textBoldWithHTML(threadName));
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            content = String.format(mRes.getString(R.string.invite_share_group_music),
                    mRes.getString(R.string.you), threadName);
            contentBold = String.format(mRes.getString(R.string.invite_share_group_music),
                    mRes.getString(R.string.you),
                    TextHelper.textBoldWithHTML(threadName));
            shareMusicMessage.setMusicState(ReengMessageConstant.MUSIC_STATE_ACCEPTED);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            content = String.format(mRes.getString(R.string.invite_share_room_music),
                    threadName);
            contentBold = String.format(mRes.getString(R.string.invite_share_room_music),
                    TextHelper.textBoldWithHTML(threadName));
            shareMusicMessage.setMusicState(ReengMessageConstant.MUSIC_STATE_ROOM);
        }
        shareMusicMessage.setContent(content);
        shareMusicMessage.setFileName(contentBold);
        if (insertNewMessageBeforeSend(threadMessage, mThreadType, shareMusicMessage)) {
            // tat media neu truoc do bi timeout
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                if (mApplication.getPlayMusicController() != null) {
                    mApplication.getPlayMusicController().closeNotifyMusicAndClearDataSong();
                }
                musicBusiness.startTimerReceiveResponse(ReengMusicPacket.MusicAction.invite,
                        shareMusicMessage.getPacketId(), Constants.ALARM_MANAGER.TIME_OUT_RECEIVE_INVITE_MUSIC);
            }
            //mApplication.getMusicBusiness().closeMusicAndClearDataSong();
            sendInviteMusic(shareMusicMessage, threadMessage);
        }
    }

    public void createAndSendMessageChangeMusic(ThreadMessage threadMessage, MediaModel songModel
            , BaseSlidingFragmentActivity activity, boolean isRequestChange) {
        if (songModel == null) {
            activity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        int mThreadType = threadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT ||
                mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            return;// officer va broadcast khong cho gui invite music
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloShareMusicMessage message = new SoloShareMusicMessage(threadMessage,
                myNumber, receiver);
        if (isRequestChange && mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            message.setMessageType(ReengMessageConstant.MessageType.inviteShareMusic);
            message.setPacketId(PacketMessageId.getInstance().genShareMusicPacketId(threadMessage.getThreadType(),
                    ReengMessagePacket.SubType.music_request_change));
            message.setMusicState(ReengMessageConstant.MUSIC_STATE_REQUEST_CHANGE);
            message.setContent(String.format(mRes.getString(R.string.friend_request_change_song),
                    mRes.getString(R.string.you)) + " \"" + songModel.getName() + "\"");
            message.setFileName(String.format(mRes.getString(R.string.friend_request_change_song),
                    TextHelper.textBoldWithHTML(mRes.getString(R.string.you))));
        } else {
            message.setMessageType(ReengMessageConstant.MessageType.actionShareMusic);
            message.setPacketId(PacketMessageId.getInstance().genShareMusicPacketId(threadMessage.getThreadType(),
                    ReengMessagePacket.SubType.music_action));
            message.setContent(String.format(mRes.getString(R.string.friend_change_song_v2),
                    mRes.getString(R.string.you)) + " \"" + songModel.getName() + "\"");
            message.setFileName(String.format(mRes.getString(R.string.friend_change_song_v2),
                    TextHelper.textBoldWithHTML(mRes.getString(R.string.you))));
        }
        message.setDirection(ReengMessageConstant.Direction.send);
        message.setStatus(ReengMessageConstant.STATUS_CHANGE_SONG);// set trang thai -1 de khong hien thi status dang
        // gui voi message chuyen bai
        message.setSongId(ReengMessage.SONG_ID_DEFAULT_NEW);
        message.setSongModel(songModel);
        message.setDuration(1);

        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            if (mApplication.getMusicBusiness().sendAndWaitConfirmChangeSong(activity, songModel, null)) {
                insertNewMessageToDB(threadMessage, message);
                notifyNewMessage(message, threadMessage);
                mApplication.getMusicBusiness().changeSong(songModel);
            } else {
                activity.showToast(R.string.e601_error_but_undefined);
            }
        } else {// group
            insertNewMessageToDB(threadMessage, message);
            mApplication.getXmppManager().sendMusicMessage(message, songModel, threadMessage);
            notifyNewMessage(message, threadMessage);
            if (!isRequestChange && mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                mApplication.getMusicBusiness().changeSong(songModel);
            }
        }
        mApplication.trackingEvent(R.string.ga_category_invite_music,
                R.string.ga_action_change_song, R.string.ga_action_change_song);
    }

    public ReengMessage createMessagePollAfterRequest(ThreadMessage threadMessage, PollObject poll
            , boolean isCreate, boolean isAdd) {
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        PollRequestHelper pollHelper = PollRequestHelper.getInstance(mApplication);
        String to;
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            to = threadMessage.getSoloNumber();
        } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            to = "broadcast" + threadMessage.getId();
        } else {
            to = threadMessage.getServerId();
        }
        ReengMessage message = new ReengMessage();// khong gui mes nen ko can packetid chuan
        message.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
        message.setReceiver(to);
        message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
        message.setThreadId(threadMessage.getId());
        message.setDirection(ReengMessageConstant.Direction.send);
        // set time
        Date date = new Date();
        message.setTime(date.getTime());
        message.setSender(myNumber);
        message.setStatus(ReengMessageConstant.STATUS_CHANGE_SONG);// khong hien thi icon deliver
        if (isCreate) {
            String contentCreate = String.format(mApplication.getResources().getString(R.string.poll_detail_creator), mApplication.getResources().getString(R.string.you));
            message.setMessageType(ReengMessageConstant.MessageType.poll_create);
            message.setContent(contentCreate);
        } else {
            message.setMessageType(ReengMessageConstant.MessageType.poll_action);
            message.setContent(pollHelper.getContentMessageActionPoll(poll, myNumber, true, isAdd));
        }
        message.setFileId(poll.getPollId());// luu thong tin pollId vao fileId
        //message.setDirectLinkMedia(voteDetail);//TODO nêu cần thiết thì lưu detail vào đây
        this.insertNewMessageToDB(threadMessage, message);//insert message vao thread trong database
        return message;
    }

    /**
     * tim thread 1-1 cua so phoneNumber
     *
     * @param phoneNumber
     * @return null neu chua ton tai
     */
    public ThreadMessage findExistingSoloThread(String phoneNumber) {
        ThreadMessage result = null;
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    threadMessage.getSoloNumber().equals(phoneNumber)) {
                result = threadMessage;
                break;
            }
        }
        return result;
    }

    public ThreadMessage findExistingOrCreateNewThread(String number) {
        return findExistingOrCreateNewThread(number, true);
    }

    public ThreadMessage findExistingOrCreateNewThread(String number, boolean isJoined) {
        if (TextUtils.isEmpty(number)) return null;
        mContactBusiness = mApplication.getContactBusiness();
        ThreadMessage mCorrespondingThread;
        //Kiểm tra số có phải chuyển đổi ko, nếu có đã có threadchat chưa
        String newNumb = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(number);
        if (newNumb != null) {
            mCorrespondingThread = findExistingSoloThread(newNumb);
            if (mCorrespondingThread != null) return mCorrespondingThread;
        }

        //tim trong memory
        mCorrespondingThread = findExistingSoloThread(number);
        //tim thread da ton tai
        if (mCorrespondingThread != null) {
            Log.i(TAG, "Da co threadchat: " + number);
            return mCorrespondingThread;
        }

        String oldNumber = PrefixChangeNumberHelper.getInstant(mApplication).getOldNumber(number);
        if (number.length() == 10) {        //kiểm tra xem đã tồn tại thread chat với số 11 số chưa, nếu có rồi thì lấy ra
            if (oldNumber != null) {
                mCorrespondingThread = findExistingSoloThread(oldNumber);
                if (mCorrespondingThread != null) {
                    Log.i(TAG, "da ton tai thread chat voi so cu: " + oldNumber);
                    return mCorrespondingThread;
                }
            }
        }
        //chua co thread 1-1 cua phoneNumber nay
        PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
        if (phoneNumber == null) {
            /*if (number.length() == 11) {
                String newNumberStranger = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(number);
                if (newNumberStranger != null) {
                    number = newNumberStranger;
                }
            }*/
            StrangerPhoneNumber strangerPhoneNumber = mApplication.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(number);
            if (strangerPhoneNumber != null) {// so chua luu, co trong bang stranger thi tao thread stranger
                if (strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                    return createNewThreadStrangerOtherApp(number, strangerPhoneNumber, isJoined);
                } else {
                    return createNewThreadStrangerMusic(number, strangerPhoneNumber, isJoined);
                }
            }
        }
        // ngoai le thi tra ve thread thuong
        return createNewThreadNormal(number);
    }

    private ThreadMessage createThreadSolo(String friendJid, String friendName) {
        ThreadMessage newThread = new ThreadMessage();
        newThread.setThreadType(ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT);
        newThread.addNumberToThread(friendJid);
        newThread.setName(friendName);
        newThread.setTimeOfLast(TimeHelper.getCurrentTime());
        newThread.setDhVtt(0);
        newThread.setState(ThreadMessageConstant.STATE_SHOW);
        newThread.setLastMessageId(ThreadMessageConstant.LAST_MESSAGE_ID_DEFAULT);
        return newThread;
    }

    /**
     * tao thread message 1-1 thuong
     *
     * @param number
     * @return
     */
    public ThreadMessage createNewThreadNormal(String number) {
        //chua co thread 1-1 cua phoneNumber nay
        ThreadMessage mCorrespondingThread = createThreadSolo(number, getFriendName(number));
        mCorrespondingThread.setJoined(true);
        insertThreadMessage(mCorrespondingThread);
        mThreadMessageArrayList.add(mCorrespondingThread);
        // them moi thread goi change adapter
        refreshThreadWithoutNewMessage(mCorrespondingThread.getId());
        // kiem tra so chua luu danh ba thi goi api get info
        mContactBusiness = mApplication.getContactBusiness();
        PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
        if (phoneNumber == null) {
            NonContact nonContact = mContactBusiness.getExistNonContact(number);
            if (nonContact == null) {
                mContactBusiness.getInfoNumber(number);
            }
        }
        return mCorrespondingThread;
    }

    /**
     * tao thread 1-1 lam quen
     *
     * @param number
     * @return threadMessage
     */
    public ThreadMessage createNewThreadStrangerOtherApp(String number, StrangerPhoneNumber strangerPhoneNumber, boolean isJoined) {
        //chua co thread 1-1 cua phoneNumber nay
        ThreadMessage mCorrespondingThread = createThreadSolo(number, strangerPhoneNumber.getFriendName());
        mCorrespondingThread.setJoined(isJoined);
        mCorrespondingThread.setStranger(true);
        mCorrespondingThread.setStrangerPhoneNumber(strangerPhoneNumber);
        insertThreadMessage(mCorrespondingThread);
        mThreadMessageArrayList.add(mCorrespondingThread);
        // them moi thread goi change adapter
        refreshThreadWithoutNewMessage(mCorrespondingThread.getId());
        return mCorrespondingThread;
    }

    /**
     * tao thread 1-1 cung nghe nguoi la
     *
     * @param number
     * @return thread
     */
    public ThreadMessage createNewThreadStrangerMusic(String number, StrangerPhoneNumber strangerPhoneNumber
            , boolean isJoined) {
        //chua co thread 1-1 cua phoneNumber nay
        ThreadMessage mCorrespondingThread = createThreadSolo(number, strangerPhoneNumber.getFriendName());
        mCorrespondingThread.setJoined(isJoined);
        mCorrespondingThread.setStranger(true);
        mCorrespondingThread.setStrangerPhoneNumber(strangerPhoneNumber);
        insertThreadMessage(mCorrespondingThread);
        mThreadMessageArrayList.add(mCorrespondingThread);
        // them moi thread goi change adapter
        refreshThreadWithoutNewMessage(mCorrespondingThread.getId());
        return mCorrespondingThread;
    }

    public ThreadMessage findExistingOrCreateNewGroupThread(String serverId) {
        ThreadMessage mCorrespondingThread = findGroupThreadByServerId(serverId); //tim thread hoac tao moi
        if (mCorrespondingThread != null) {
            updateStateShowGroup(mCorrespondingThread);
            return mCorrespondingThread; //neu co trong bo nho
        }
        // th tao moi thi set state show luon
        String groupNameDefault = mRes.getString(R.string.group_name_default);
        mCorrespondingThread = createThreadGroup(serverId, groupNameDefault,
                new ArrayList<Member>(), ThreadMessageConstant.STATE_NEED_GET_INFO, ThreadMessageConstant.GROUP_TYPE_NORMAL);
        new GetGroupInfoAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mCorrespondingThread);
        return mCorrespondingThread;
    }

    public ThreadMessage createNewGroupThread(String serverId) {
        String groupNameDefault = mRes.getString(R.string.group_name_default);
        ThreadMessage mCorrespondingThread = createThreadGroup(serverId, groupNameDefault,
                new ArrayList<Member>(), ThreadMessageConstant.STATE_NEED_GET_INFO, ThreadMessageConstant.GROUP_TYPE_NORMAL);
        updateThreadMessage(mCorrespondingThread);
//        new GetGroupInfoAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mCorrespondingThread);
        return mCorrespondingThread;
    }

    /**
     * @param officerId
     * @return
     */
    public ThreadMessage findExistingOrCreateOfficerThread(String officerId, String officerName
            , String avatarUrl, int onmediaType) {
        if (onmediaType < 0) onmediaType = OfficerAccountConstant.ONMEDIA_TYPE_NONE;
        OfficerBusiness officerBusiness = mApplication.getOfficerBusiness();
        // update bang offical
        officerBusiness.insertOrUpdateOfficerAccount(officerId,
                officerName, avatarUrl, OfficerAccountConstant.TYPE_OFFICER, onmediaType);
        //tim trong memory
        ThreadMessage mThread = findOfficerThreadById(officerId);
        //tim thread da ton tai
        if (mThread != null) {
            String oldName = getThreadName(mThread);
            // ten moi != null va khac ten cu
            if (!TextUtils.isEmpty(officerName) && !officerName.equals(oldName)) {
                mThread.setName(officerName);
                mThreadMessageDataSource.updateThreadMessage(mThread);
            }
            return mThread;
        }
        //chua co thread offical nay
        mThread = createThreadOfficerOrRoom(ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT,
                officerId, officerName, ThreadMessageConstant.STATE_SHOW);
        insertThreadMessage(mThread);
        mThreadMessageArrayList.add(mThread);
        refreshThreadWithoutNewMessage(mThread.getId());
        return mThread;
    }

    public ThreadMessage findExistingOrCreateOfficialThread() {
        String id = mRes.getString(R.string.officalId);
        String name = mRes.getString(R.string.officalName);
        String avatar = mRes.getString(R.string.officalAvatar);
        ThreadMessage thread = findExistingOrCreateOfficerThread(id, name, avatar, OfficerAccountConstant
                .ONMEDIA_TYPE_NONE);
        return thread;
    }

    /**
     * findExistingOrCreateStarRoomThread
     *
     * @param roomId
     * @param roomName
     * @return
     */
    public ThreadMessage findExistingOrCreateRoomThread(String roomId, String roomName
            , String avatarUrl, int roomState, int stateThread) {
        OfficerBusiness officerBusiness = mApplication.getOfficerBusiness();
        // update bang offical
        officerBusiness.insertOrUpdateOfficerAccount(roomId, roomName, avatarUrl,
                OfficerAccountConstant.TYPE_STAR_ROOM, roomState);
        //tim trong memory
        ThreadMessage mThread = findRoomThreadByRoomId(roomId);
        //tim thread da ton tai
        if (mThread != null) {
            String oldName = getThreadName(mThread);
            // ten moi != null va khac ten cu
            if (!TextUtils.isEmpty(roomName) && !roomName.equals(oldName)) {
                mThread.setName(roomName);
                mThreadMessageDataSource.updateThreadMessage(mThread);
            }
            return mThread;
        }
        //chua co thread room nay
        mThread = createThreadOfficerOrRoom(ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT, roomId,
                roomName, stateThread);
        insertThreadMessage(mThread);
        mThreadMessageArrayList.add(mThread);
        refreshThreadWithoutNewMessage(mThread.getId());
        return mThread;
    }

    private ThreadMessage createThreadOfficerOrRoom(int threadType, String threadServerId, String threadName, int
            stateShow) {
        ThreadMessage newThread = new ThreadMessage();
        newThread.setThreadType(threadType);
        newThread.setName(threadName);
        newThread.setServerId(threadServerId);
        newThread.setTimeOfLast(TimeHelper.getCurrentTime());
        newThread.setState(stateShow);
        newThread.setLastMessageId(ThreadMessageConstant.LAST_MESSAGE_ID_DEFAULT);
        return newThread;
    }

    private void updateStateShowGroup(ThreadMessage threadMessage) {
        if (threadMessage.getState() == ThreadMessageConstant.STATE_HIDE) {
            threadMessage.setState(ThreadMessageConstant.STATE_SHOW);
            mThreadMessageDataSource.updateThreadMessage(threadMessage);
        } else if (threadMessage.getState() == ThreadMessageConstant.STATE_PRIVATE_HIDE) {
            threadMessage.setState(ThreadMessageConstant.STATE_PRIVATE_SHOW);
            mThreadMessageDataSource.updateThreadMessage(threadMessage);
        }
    }

    /**
     * insert new message vao database
     *
     * @param threadMessage
     * @param reengMessage
     */
    public long insertNewMessageToDB(ThreadMessage threadMessage, ReengMessage reengMessage) {
        long t = System.currentTimeMillis();
        long id = mReengMessageDataSource.createMessage(reengMessage);
        reengMessage.setId((int) id);
        Log.d(TAG, "[perform] insert message to db take: " + (System.currentTimeMillis() - t));
        t = System.currentTimeMillis();
        threadMessage.setLastMessageId((int) id);
        mThreadMessageDataSource.updateThreadMessageAfterChangeLastId(threadMessage);
        Log.d(TAG, "[perform] update thread after insert message to db take: " + (System.currentTimeMillis() - t));
        return id;
    }

    /**
     * insert new list message to database
     *
     * @param reengMessages
     */
    private void insertListNewMessageToDB(ThreadMessage threadMessage, List<ReengMessage> reengMessages) {
        mReengMessageDataSource.insertNewListMessage(reengMessages);
        if (reengMessages != null && !reengMessages.isEmpty()) {
            ReengMessage lastMessage = reengMessages.get(reengMessages.size() - 1);
            threadMessage.setLastMessageId(lastMessage.getId());
            mThreadMessageDataSource.updateThreadMessageAfterChangeLastId(threadMessage);
        }

    }

    /**
     * @param threadMessage
     * @param reengMessage
     */
    public void insertNewMessageToMemory(ThreadMessage threadMessage, ReengMessage reengMessage) {
        reengMessage.setPlayedGif(false);
        //TODO crash khi thoat room nhac, co tn nhan den, check null
        if (threadMessage != null) {
            threadMessage.insertNewMessage(reengMessage);
        } else {
            mApplication.logDebugContent("insertNewMessageToMemory thread message null: " + reengMessage.getPacketId());
            Log.f(TAG, "insertNewMessageToMemory thread message null: " + reengMessage.getPacketId());
        }
    }

    /**
     * @param threadMessage
     * @param reengMessages
     */
    private void insertListNewMessageToMemory(ThreadMessage threadMessage, List<ReengMessage> reengMessages) {
        threadMessage.insertListMessage(reengMessages);
    }

    /**
     * xoa thread message
     */
    public void deleteThreadMessage(ThreadMessage mThreadMessage) {
        Log.d(TAG, "deleteThreadMessage");
        int threadId = mThreadMessage.getId();
        MusicBusiness musicBusiness = mApplication.getMusicBusiness();
        // neu xoa thread 1-1 ma co tin invite music dang dem nguoc thi stop countdown
        checkLeaveMusicWhenDelete(musicBusiness, mThreadMessage);
        mReengMessageDataSource.deleteListMessage(threadId);
        mThreadMessageDataSource.deleteThreadMessage(threadId);
        mThreadMessage.setAllMessages(null);
        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && mThreadMessage
                .isStranger()) {
            mApplication.getContactBusiness().deleteNonContact(mThreadMessage.getSoloNumber());
        }
        mThreadMessageArrayList.remove(mThreadMessage);
        notifyDeleteThreadMessage(mThreadMessage.getId());
        mApplication.updateCountNotificationIcon();
        MessageNotificationManager.getInstance(mApplication).clearNotifyThreadMessage(mThreadMessage);
        // xoa message event khi xoa thread
        mEventMessageDataSource.deleteEventMessageByThreadId(threadId);
        GroupAvatarHelper.getInstance(mApplication).deleteAvatarGroup(mThreadMessage);
        ListenerHelper.getInstance().onThreadsChanged();
    }

    /**
     * xoa all msg trong thread
     */
    public void deleteAllMsgGroupInThreadMessage(ThreadMessage mThreadMessage) {
        Log.d(TAG, "deleteThreadMessage");
        int threadId = mThreadMessage.getId();
        MusicBusiness musicBusiness = mApplication.getMusicBusiness();
        // neu xoa thread 1-1 ma co tin invite music dang dem nguoc thi stop countdown
        checkLeaveMusicWhenDelete(musicBusiness, mThreadMessage);
        mReengMessageDataSource.deleteListMessage(threadId);
        mThreadMessage.setAllMessages(null);
        mApplication.updateCountNotificationIcon();
        MessageNotificationManager.getInstance(mApplication).clearNotifyThreadMessage(mThreadMessage);
        // xoa message event khi xoa thread
        mEventMessageDataSource.deleteEventMessageByThreadId(threadId);
    }

    public void deleteListThreadMessage(ArrayList<ThreadMessage> listDelete) {
        MusicBusiness musicBusiness = mApplication.getMusicBusiness();
        if (!listDelete.isEmpty()) {
            mReengMessageDataSource.deleteListMessageOfListThread(listDelete);
            mEventMessageDataSource.deleteEventMessageOfListThread(listDelete);
            mThreadMessageDataSource.deleteListThreadMessage(listDelete);
            for (ThreadMessage threadMessage : listDelete) {
                checkLeaveMusicWhenDelete(musicBusiness, threadMessage);
                checkAndLeaveRoomChat(threadMessage);
                threadMessage.setAllMessages(null);
                GroupAvatarHelper.getInstance(mApplication).deleteAvatarGroup(threadMessage);
                //tam thoi xoa tung non contact 1, neu toi uu thi for het 1 luot lay list nonContacts can xoa roi xoa
                // 1 lan
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && threadMessage
                        .isStranger()) {
                    mApplication.getContactBusiness().deleteNonContact(threadMessage.getSoloNumber());
                }
                mThreadMessageArrayList.remove(threadMessage);
            }
            notifyDeleteThreadMessage(-1);
            mApplication.updateCountNotificationIcon();
            MessageNotificationManager.getInstance(mApplication).clearNotifyThreadMessages(listDelete);
        }
        ListenerHelper.getInstance().onThreadsChanged();
    }

    private void checkLeaveMusicWhenDelete(MusicBusiness musicBusiness, ThreadMessage threadMessage) {
        // neu xoa thread 1-1 ma co tin invite music dang dem nguoc thi stop countdown
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            ReengMessage lastInviteMessage = findLastMessageAllInviteMusic(threadMessage);
            if (lastInviteMessage != null && lastInviteMessage.getMusicState() == ReengMessageConstant
                    .MUSIC_STATE_WAITING) {
                CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(lastInviteMessage);
            }
            if (musicBusiness.isShowPlayer(threadMessage.getSoloNumber())) {
                musicBusiness.onLeaveMusic(false);
            }
        } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            if (musicBusiness.isShowPlayerGroup(threadMessage.getServerId())) {
                musicBusiness.onLeaveMusic(false);
            }
        }
    }

    private void checkAndLeaveRoomChat(ThreadMessage threadMessage) {
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            removePacketIdWhenDeleteRoom(threadMessage);
            mApplication.getMusicBusiness().processResponseLeaveRoom(threadMessage.getServerId(), false);
            try {
                IQInfo iqLeaveRoom = new IQInfo(IQInfo.NAME_SPACE_STAR_UN_FOLLOW);
                iqLeaveRoom.setType(IQ.Type.SET);
                iqLeaveRoom.setTo(threadMessage.getServerId() + Constants.XMPP.XMPP_ROOM_RESOUCE);
                mApplication.getXmppManager().sendPacketNoWaitResponse(iqLeaveRoom);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public void processMessageConfigGroup(ApplicationController application, ReengMessagePacket packet) {
        //gui ban tin confirm message config group
        String sender = packet.getSender();
        String serverId = packet.getFrom().split("@")[0];
        String packetId = packet.getPacketID();
        application.getXmppManager().sendConfirmDeliverMessage(packetId, serverId, sender, ThreadMessageConstant
                .TYPE_THREAD_GROUP_CHAT);
        // check dulicate ban tin
        if (checkDuplicatePacket(packetId)) {
            return;
        }
        mContactBusiness = mApplication.getContactBusiness();
        String groupName = mRes.getString(R.string.group_name_default);
        if (packet.getGroupName() != null) {
            groupName = packet.getGroupName();
        }
        myNumber = application.getReengAccountBusiness().getJidNumber();
        ReengMessagePacket.SubType subType = packet.getSubType();
        ThreadMessage threadMessage = null;
        String content = "";
        String groupAvatar = packet.getGroupAvatar();
        String notifyType = "";
        int groupClass = packet.getGroupClass();
        // lay ten nguoi gui
        String senderName = getFriendNameOfGroup(sender);
        // members
        ArrayList<Member> members = packet.getMembers();
        mApplication.getContactBusiness().updateStateNonContactAfterChangedGroup(members);
//        mContactBusiness.insertMemberGroup(members, myNumber);
        // su ly cac loai ban tin
        switch (subType) {
            // message notify config group
            case create:
                //nhan duoc ban tin thong bao tao nhom
                if (members == null || members.isEmpty()) return;
                content = String.format(mRes.getString(R.string.msg_noti_join_new_group), senderName);
                notifyType = MessageConstants.NOTIFY_TYPE.create.name();
                //truong hop tin nhan ve truoc tin tao nhom thi check trc
                threadMessage = findGroupThreadByServerId(serverId);// da co roi thi dung luon
                if (threadMessage == null) {
                    //create thread
                    threadMessage = createThreadGroup(serverId, groupName, members,
                            ThreadMessageConstant.STATE_HIDE, groupClass, packet.getGroupPrivate());
                }
                break;
            case invite:
                if (members == null || members.isEmpty()) return;
                ArrayList<String> memberInvite = new ArrayList<>();
                threadMessage = findGroupThreadByServerId(serverId);
                for (Member member : members) {
                    if (member.getJid() != null) {
                        String jid = member.getJid();
                        String newJid = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(jid);
                        if (newJid != null) jid = newJid;
                        if (!jid.equals(myNumber)) {
                            if (threadMessage != null)
                                threadMessage.addNumberToThread(jid);
                            memberInvite.add(jid);
                        }
                        if (threadMessage != null && member.isOwner()) {
                            threadMessage.addAdminToThread(jid);
                        }
                    }
                }
                if (threadMessage != null) {
                    updateAdminGroup(threadMessage);
                    mThreadMessageDataSource.updateThreadMessage(threadMessage);
                } else {// truong hop dang nhap tren may khac, sau co co tin moi them thanh vien (get info)
                    threadMessage = findExistingOrCreateNewGroupThread(serverId);
                }
                threadMessage.setLoadNonContact(false);//reset de load lai khi group thay doi
                threadMessage.setForceCalculatorAllMember(true);
                threadMessage.setGroupAvatar(groupAvatar);
                notifyType = MessageConstants.NOTIFY_TYPE.invite.name();
                content = String.format(mRes.getString(R.string.msg_noti_invite_old_member),
                        senderName, mContactBusiness.getListNameOfListNumber(memberInvite));
                notifyChangeGroupMember(threadMessage);//oldThread.setForceCalculatorAllMember(true);
                GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(threadMessage.getId());
                break;
            case join:
                if (members == null || members.isEmpty()) return;
                // so nay da roi nhom nay luc truoc hoac chua bao h tham gia nhom
                threadMessage = findGroupThreadByServerId(serverId);
                if (threadMessage != null) {
                    threadMessage.setJoined(true);
                    // xu ly thanh vien va admin
                    ArrayList<String> memberNumbers = new ArrayList<>();
                    ArrayList<String> adminNumbers = new ArrayList<>();
                    for (Member member : members) {
                        if (member.getJid() != null) {
                            if (!member.getJid().equals(myNumber)) {
                                memberNumbers.add(member.getJid());
                            }
                            if (member.isOwner()) {
                                adminNumbers.add(member.getJid());
                            }
                        }
                    }
                    threadMessage.setPhoneNumbers(memberNumbers);
                    threadMessage.setAdminNumbers(adminNumbers);
                    threadMessage.setName(groupName);
                    threadMessage.setGroupClass(groupClass);// group class tra ve trong messsage tao group, duoc moi
                    // vao group
                    updateAdminGroup(threadMessage);
                    threadMessage.setPrivateThread(packet.getGroupPrivate());
                    notifyChangeGroupMember(threadMessage);
                    mThreadMessageDataSource.updateThreadMessage(threadMessage);
                    threadMessage.setForceCalculatorAllMember(true);
                    GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(threadMessage.getId());
                } else {// chua co phai tao moi, hide
                    threadMessage = createThreadGroup(serverId, groupName, members, ThreadMessageConstant.STATE_HIDE,
                            groupClass, packet.getGroupPrivate());
                }
                threadMessage.setLoadNonContact(false);//reset de load lai khi group thay doi
                threadMessage.setGroupAvatar(groupAvatar);
                threadMessage.setState(ThreadMessageConstant.STATE_NEED_GET_INFO);
                notifyType = MessageConstants.NOTIFY_TYPE.join.name();
                content = String.format(mRes.getString(R.string.msg_noti_invite_new_member), senderName);
                //goi lai getconfig de xem co thay doi j ko
                new GetGroupInfoAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, threadMessage);
                break;
            case rename:
                notifyType = MessageConstants.NOTIFY_TYPE.rename.name();
                content = String.format(mRes.getString(R.string.msg_noti_rename_group), senderName, groupName);
                // find thread
                threadMessage = findExistingOrCreateNewGroupThread(serverId);
                if (threadMessage == null) {// ko tim thay why???????
                    return;
                }
                threadMessage.setName(groupName);
                //update db
                mThreadMessageDataSource.updateThreadMessage(threadMessage);
                notifyChangeGroupName(threadMessage);
                break;
            case leave:
                // find thread (neu ko con thread nay thi thoi ko xu ly nua
                threadMessage = findExistingOrCreateNewGroupThread(serverId);
                if (threadMessage == null) {// ko tim thay why???????
                    return;
                }
                if (sender != null && sender.equals(myNumber)) {
                    // truong hop qua thoi gian timeout ma ko phat sinh tinh nhan trong group, sv chu dong xoa
                    if (!TextUtils.isEmpty(packet.getBody())) {
                        content = packet.getBody();
                    } else {
                        content = mRes.getString(R.string.msg_noti_leave_timeout);
                    }
                    threadMessage.setJoined(false);
                    threadMessage.setPhoneNumbers(new ArrayList<String>());
                    threadMessage.setAdminNumbers(new ArrayList<String>());
                    updateAdminGroup(threadMessage);
                    mThreadMessageDataSource.updateThreadMessage(threadMessage);
                } else {
                    content = String.format(mRes.getString(R.string.msg_noti_leave_member), senderName);
                    threadMessage.removeNumberFromThread(sender);
                    threadMessage.removeAdminFromThread(sender);
                    mThreadMessageDataSource.updateThreadMessage(threadMessage);
                    updateAdminGroup(threadMessage);
                    notifyChangeGroupMember(threadMessage);
                }
                notifyType = MessageConstants.NOTIFY_TYPE.leave.name();
                threadMessage.setForceCalculatorAllMember(true);
                GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(threadMessage.getId());
                break;
            case makeAdmin:
                if (members == null || members.isEmpty()) return;
                threadMessage = findExistingOrCreateNewGroupThread(serverId);// tim hoac tao thread group
                boolean isMakeMe = false;
                boolean isMakeAdmin = false;
                // xu ly thanh vien va admin
                for (Member member : members) {
                    if (member.getJid() != null) {
                        String jid = member.getJid();
                        String newJid = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(jid);
                        if (newJid != null) jid = newJid;
                        if (!jid.equals(myNumber)) {
                            threadMessage.addNumberToThread(jid);
                        } else {
                            isMakeMe = true;
                        }
                        if (member.isOwner()) {
                            isMakeAdmin = true;
                            threadMessage.addAdminToThread(jid);
                        } else {
                            threadMessage.removeAdminFromThread(jid);
                        }
                    }
                }
                if (isMakeMe) {// danh sach make co so cua minh
                    if (isMakeAdmin) {
                        notifyType = MessageConstants.NOTIFY_TYPE.makeAdmin.name();
                        content = String.format(mRes.getString(R.string.msg_noti_make_admin),
                                senderName, mRes.getString(R.string.you).toLowerCase());
                    } else {
                        notifyType = MessageConstants.NOTIFY_TYPE.removeAdmin.name();
                        content = String.format(mRes.getString(R.string.msg_noti_make_member),
                                senderName, mRes.getString(R.string.you).toLowerCase());
                    }
                } else {
                    content = null;
                }
                updateAdminGroup(threadMessage);// cap nhat trang thai admin
                threadMessage.setPrivateThread(packet.getGroupPrivate());
                mThreadMessageDataSource.updateThreadMessage(threadMessage);
                notifyChangeGroupMember(threadMessage);
                break;
            case kick:
                if (members == null || members.isEmpty()) return;
                threadMessage = findExistingOrCreateNewGroupThread(serverId);
                String friendNumberKick = members.get(0).getJid();// kick chi thuc hien voi 1 thanh vien 1 lan
                String newNumberKick = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(friendNumberKick);
                if (newNumberKick != null) friendNumberKick = newNumberKick;
                String friendNameKick = mApplication.getMessageBusiness().getFriendNameOfGroup(friendNumberKick);
                if (myNumber != null && myNumber.equals(friendNumberKick)) {// chinh minh bi kick
                    content = String.format(mRes.getString(R.string.msg_noti_kick_member),
                            senderName, mRes.getString(R.string.you).toLowerCase());
                    // reset thread
                    threadMessage.setJoined(false);
                    threadMessage.setPhoneNumbers(new ArrayList<String>());
                    threadMessage.setAdminNumbers(new ArrayList<String>());
                } else {// ng # bi kick
                    content = String.format(mRes.getString(R.string.msg_noti_kick_member),
                            senderName, friendNameKick);
                    threadMessage.removeNumberFromThread(friendNumberKick);
                    threadMessage.removeAdminFromThread(friendNumberKick);
                }
                notifyType = MessageConstants.NOTIFY_TYPE.kick.name();
                updateAdminGroup(threadMessage);// cap nhat trang thai admin
                mThreadMessageDataSource.updateThreadMessage(threadMessage);
                notifyChangeGroupMember(threadMessage);
                threadMessage.setForceCalculatorAllMember(true);
                GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(threadMessage.getId());
                break;
            case groupPrivate:
                threadMessage = findExistingOrCreateNewGroupThread(serverId);
                threadMessage.setPrivateThread(packet.getGroupPrivate());
                if (threadMessage.isPrivateThread()) {
                    notifyType = MessageConstants.NOTIFY_TYPE.groupPrivate.name();
                    content = String.format(mRes.getString(R.string.msg_noti_set_private), senderName);
                } else {
                    notifyType = MessageConstants.NOTIFY_TYPE.groupPublic.name();
                    content = String.format(mRes.getString(R.string.msg_noti_unset_private), senderName);
                }
                mThreadMessageDataSource.updateThreadMessage(threadMessage);
                notifyChangeGroupPrivate(threadMessage);
                break;
            case groupAvatar:
                threadMessage = findExistingOrCreateNewGroupThread(serverId);
                notifyType = MessageConstants.NOTIFY_TYPE.changeAvatar.name();
                content = String.format(mRes.getString(R.string.msg_noti_change_avatar_group), senderName);
                threadMessage.setGroupAvatar(groupAvatar);
                mThreadMessageDataSource.updateThreadMessage(threadMessage);
                //
                notifyChangeGroupAvatar(threadMessage);
                GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(threadMessage.getId());
                break;
            default:
                break;
        }
        insertNewMessageNotifyGroup(application, threadMessage, packetId, serverId, myNumber,
                content, subType, packet.getTimeSend(), groupAvatar, notifyType, sender);
    }

    public ThreadMessage findOfficerThreadById(String officerId) {
        ThreadMessage result = null;
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT &&
                    threadMessage.getServerId().equals(officerId)) {
                result = threadMessage;
                break;
            }
        }
        return result;
    }

    /**
     * dung cho group va room chat
     *
     * @param chatRoomId
     * @return
     */
    public ThreadMessage findGroupThreadByServerId(String chatRoomId) {
        if (TextUtils.isEmpty(chatRoomId)) {
            return null;
        }
        ThreadMessage result = null;
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT
                    && chatRoomId.equals(threadMessage.getServerId())) {
                result = threadMessage;
                break;
            }
        }
        return result;
    }

    public ThreadMessage findRoomThreadByRoomId(String roomId) {
        ThreadMessage result = null;
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT &&
                    threadMessage.getServerId() != null && threadMessage.getServerId().equals(roomId)) {
                result = threadMessage;
                break;
            }
        }
        return result;
    }

    // thread chat
    private ThreadMessage findSoloThreadByThreadId(int threadId) {
        ThreadMessage result = null;
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    threadMessage.getId() == threadId) {
                result = threadMessage;
                break;
            }
        }
        return result;
    }

    public ThreadMessage findThreadByThreadId(int threadId) {
        ThreadMessage result = null;
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getId() == threadId) {
                result = threadMessage;
                break;
            }
        }
        return result;
    }

    /**
     * truoc khi gui tin nhan deu goi toi ham nay
     *
     * @param mThreadMessage
     * @param mThreadType
     * @param newMessage
     * @return gui message thanh cong hay khong
     */
    public boolean insertNewMessageBeforeSend(ThreadMessage mThreadMessage, int mThreadType, ReengMessage newMessage) {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            SpamRoomManager spamRoomManager = SpamRoomManager.getInstance(mApplication);
            long timeLock = spamRoomManager.getTimeLock();
            if (timeLock > 0) {// dang bi lock
                notifyLockRoomChat(mThreadMessage, 1, timeLock);
                Log.d(TAG, "lock room");
                return false;// khong hien message nay nua
            }
            int status = spamRoomManager.addMessageAfterSend(mThreadMessage, newMessage);
            if (status != -1) {
                notifyLockRoomChat(mThreadMessage, status, spamRoomManager.getMaxTimeLock(status));// vua bi lock
                Log.d(TAG, "start lock room");
                return false;
            }
        }/* else if (mThreadMessage != null &&
                mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                mThreadMessage.isStranger()) {
            String friendNumber = newMessage.getReceiver();
            boolean isSpam = false;
            if (newMessage.getMessageType() == ReengMessageConstant.MessageType.text) {
                isSpam = SpamRoomManager.getInstance(mApplication).checkSpamLinkTextThreadStranger(friendNumber,
                newMessage);
            } else if (newMessage.getMessageType() == ReengMessageConstant.MessageType.image) {
                isSpam = SpamRoomManager.getInstance(mApplication).checkSpamImageThreadStranger(mThreadMessage,
                friendNumber);
            }
            if (isSpam) {
                notifySpamStranger(mThreadMessage, mApplication.getResources().getString(R.string.spam_link_stranger));
                return false;
            }
        }*/
        mMessageRetryManager.addToWaitingConfirmMap(newMessage);
        if (newMessage.getDirection() == ReengMessageConstant.Direction.send) {
            //
            if (newMessage.getMessageType() == ReengMessageConstant.MessageType.text &&
                    TextUtils.isEmpty(newMessage.getReplyDetail())) {
                String largeEmo = EmoticonUtils.checkMessageLargeEmoticon(newMessage.getContent());
                if (!TextUtils.isEmpty(largeEmo)) {
                    newMessage.setContent(largeEmo);
                    newMessage.setFileId("1");
                }
            }
            insertNewMessageToMemory(mThreadMessage, newMessage);
            if (mThreadType != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                insertNewMessageToDB(mThreadMessage, newMessage); //insert DB truoc de lay messageId
            }
            // thread lam quen chua dong y (gui 1 tin nhan thi danh dau la dong y luon)
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && mThreadMessage.isStranger()) {
                StrangerPhoneNumber strangerPhoneNumber = mThreadMessage.getStrangerPhoneNumber();
                if (strangerPhoneNumber != null && strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber
                        .StrangerType.other_app_stranger &&
                        strangerPhoneNumber.getState() == StrangerPhoneNumber.StateAccept.not_accept) {
                    StrangerBusiness strangerBusiness = mApplication.getStrangerBusiness();
                    strangerPhoneNumber.setStateString(StrangerPhoneNumber.StateAccept.accepted.toString());
                    strangerBusiness.updateStrangerPhoneNumber(strangerPhoneNumber);
                    notifyChangeStateAcceptStranger(strangerPhoneNumber.getPhoneNumber());
                }
            }
            /*else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                updateStateShowGroup(mThreadMessage);// th gui dc tin nhan chung to group da dc show
            }*/

            if (newMessage.getMessageType() == ReengMessageConstant.MessageType.image
                    && TextUtils.isEmpty(newMessage.getFilePath())
                    && !TextUtils.isEmpty(newMessage.getDirectLinkMedia())
            ) {
                //todo bo sung luong tu dong download file tu server ve khi co link online va chua co file
                mApplication.getTransferFileBusiness().startDownloadMessageImageSend(newMessage);
            }
        }
        return true;
    }

    /**
     * @param mThreadMessage
     * @param newMessages
     */
    private void insertListNewMessageBeforeSend(ThreadMessage mThreadMessage, List<ReengMessage> newMessages) {
        for (ReengMessage newMessage : newMessages) {
            mMessageRetryManager.addToWaitingConfirmMap(newMessage);
        }
        insertListNewMessageToDB(mThreadMessage, newMessages); //insert DB truoc de lay messageId
        //neu la tin nhan text thi insert luon vao mem
        insertListNewMessageToMemory(mThreadMessage, newMessages);
    }

    public boolean sendXMPPMessage(ReengMessage newMessage, ThreadMessage threadMessage) {
        boolean isUpdateDb = false;
        if (newMessage.getStatus() != ReengMessageConstant.STATUS_LOADING) {
            Log.i(TAG, "sendXMPPMessage: " + newMessage.toString());
            newMessage.setStatus(ReengMessageConstant.STATUS_LOADING);
            isUpdateDb = true;
        }
        XMPPResponseCode responseCode = mApplication.getXmppManager().sendReengMessage(newMessage, threadMessage);
        //ko set trang thai fail luc gui di nua vi se de retry
        refreshThreadWithoutNewMessage(newMessage.getThreadId());
        if (isUpdateDb) {// co set lai status thi moi update db
            updateAllFieldsOfMessage(newMessage);
            Log.i(TAG, "sendXMPPMessage updateAllFieldsOfMessage: " + newMessage.toString());
        }
        //sau khi gui tin thi set lai la ko co new msg
        if (threadMessage.isHasNewMessage()) {
            markAllMessageIsOld(threadMessage, threadMessage.getId());
        }
        return responseCode.getCode() == XMPPCode.E200_OK;
    }

    /**
     * gui ban tin moi cung nghe
     *
     * @param newMessage
     */
    public void sendInviteMusic(ReengMessage newMessage, ThreadMessage threadMessage) {
        MusicBusiness mMusicBusiness = mApplication.getMusicBusiness();
        MediaModel songModel = newMessage.getSongModel(mMusicBusiness);
        if (songModel == null) return;
        if (threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mMusicBusiness.setCurrentSong(songModel);
        }
        mApplication.getXmppManager().sendMusicMessage(newMessage, songModel, threadMessage);
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            mMusicBusiness.startGroupMusic(threadMessage.getServerId(), newMessage);
        }
        LuckyWheelHelper.getInstance(mApplication).doMission(Constants.LUCKY_WHEEL.ITEM_LISTEN_STRANGER);
    }

    /**
     * update message to database
     *
     * @param messageHolder
     */
    public void updateAllFieldsOfMessage(ReengMessage messageHolder) {
        mReengMessageDataSource.updateMessage(messageHolder);
    }

    /**
     * update list message to database
     *
     * @param listReengMessage
     */
    public void updateAllFieldsOfListMessage(List<ReengMessage> listReengMessage) {
        mReengMessageDataSource.updateListMessage(listReengMessage);
    }

    /**
     * tra ve so tin nhan chua doc
     *
     * @return countUnreadMessage
     */
    public int getNumberOfUnreadMessage() {
        long t = System.currentTimeMillis();
        int result = 0;
        boolean hasUnreadHiddenThreadTmp = false;
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            //room chat khong notify
            if (threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && threadMessage.getHiddenThread() != 1) {
                result += threadMessage.getNumOfUnreadMessage();
            }
            if (threadMessage.getHiddenThread() == 1 && threadMessage.getNumOfUnreadMessage() > 0)
                hasUnreadHiddenThreadTmp = true;
        }
        this.hasUnreadHiddenThread = hasUnreadHiddenThreadTmp;
        Log.d(TAG, "[perform] getNumberOfUnreadMessage take: " + (System.currentTimeMillis() - t));
        return result;
    }

    private boolean hasUnreadHiddenThread = false;

    public boolean isHasUnreadHiddenThread() {
        return hasUnreadHiddenThread;
    }

    public ArrayList<ThreadMessage> getListUnreadThread() {
        ArrayList<ThreadMessage> listUnread = new ArrayList<>();
        for (ThreadMessage threadMessage : mThreadMessageArrayList) {
            if (threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT &&
                    threadMessage.getNumOfUnreadMessage() > 0) {
                listUnread.add(threadMessage);
            }
        }
        Collections.sort(listUnread, ComparatorHelper.getComparatorThreadMessageByLastTime());
        return listUnread;
    }

    public int getNumberUnreadMessageFromListUnreadThread(ArrayList<ThreadMessage> listUnreadThread) {
        int result = 0;
        for (ThreadMessage threadMessage : listUnreadThread) {
            result += threadMessage.getNumOfUnreadMessage();
        }
        return result;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void markAllMessageIsReadAndCheckSendSeen(ThreadMessage threadMessage, CopyOnWriteArrayList<ReengMessage>
            listMessage) {
        new UpdateUnreadAndSendSeenMessageTask(threadMessage, listMessage).
                executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public boolean checkNumberThreadNonContact(ThreadMessage threadMessage) {
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            mContactBusiness = mApplication.getContactBusiness();
            String number = threadMessage.getSoloNumber();
            PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
            return phoneNumber == null;
        } else {
            return false;
        }
    }

    public ArrayList<ReengMessage> getImageMessageOfThread(String threadId, boolean isDesc) {
        return mReengMessageDataSource.getImageMessageOfThread(threadId, isDesc);
    }

    /**
     * xu ly ban tin respone cho gsm message
     *
     * @param gsmMessage
     */
    public void processGsmResponse(Packet gsmMessage) {
        if (checkDuplicatePacket(gsmMessage.getPacketID())) return;
        Log.i(TAG, "" + gsmMessage.toXML());
        String packetID = gsmMessage.getPacketID();
        int errorCode = 0;
        if (gsmMessage instanceof GSMMessage) {
            errorCode = ((GSMMessage) gsmMessage).getErrorCode();
        } else if (gsmMessage instanceof VoicemailGSMResponseMessage) {
            errorCode = ((VoicemailGSMResponseMessage) gsmMessage).getErrorCode();
        }
        Log.i(TAG, "errorCode = " + errorCode);
        if (errorCode == 0) {
            Log.i(TAG, "no error");
            return;
        }
        //-----------------------update status
        ReengMessage reengMessage = findMessageInMemByPacketId(packetID);
        Log.d(TAG, "In Memory " + reengMessage);
        if (reengMessage == null) {
            //neu ko co trong memory
            reengMessage = mReengMessageDataSource.findMessageInDBByPacketId(packetID);
            Log.d(TAG, "In DB " + reengMessage);
        }
        if (reengMessage == null) {
            //trong db ko con message nay nua
            return;
        }
        reengMessage.setSent(false);
        reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
        //----------------------------show error---------------------------------
        XMPPResponseCode mXMPPResponseCode = new XMPPResponseCode();
        gsmMessageErrorCode = errorCode;
        Log.i(TAG, "gsmMessageErrorCode = " + gsmMessageErrorCode);
        if (gsmMessageErrorCode > 0) {
            if (gsmMessageErrorCode == GSMMessage.ERROR_OVER_TOTAL_MESSAGES) {
                mXMPPResponseCode.setCode(XMPPCode.E560_ERROR_OVER_TOTAL_MESSAGES);
                mXMPPResponseCode.setDescription(mApplication.getString(R.string.e560_error_over_total_message));
            } else if (gsmMessageErrorCode == GSMMessage.ERROR_OVER_24H_MESSAGES) {
                mXMPPResponseCode.setCode(XMPPCode.E561_ERROR_OVER_24H_MESSAGES);
                mXMPPResponseCode.setDescription(mApplication.getString(R.string.e561_error_over_24h_messages));
            } else if (gsmMessageErrorCode == XMPPCode.E404_GROUP_NO_LONGER_EXIST) {
                mXMPPResponseCode.setCode(XMPPCode.E666_ERROR_NOT_SUPPORT);
                mXMPPResponseCode.setDescription(mApplication.getString(R.string.e666_not_support_function));
            } else {
                mXMPPResponseCode.setCode(errorCode);
                mXMPPResponseCode.setDescription
                        (mApplication.getString(XMPPCode.getResourceOfCode(errorCode)));
            }
            //------------------------------
            refreshThreadWithoutNewMessage(reengMessage.getThreadId());
            notifySendGsmMessageError(reengMessage, mXMPPResponseCode);
        }
        updateAllFieldsOfMessage(reengMessage);
    }

    public void deleteAMessage(ThreadMessage threadMessage, ReengMessage reengMessage) {
        // xoa event message
        deleteEventMessageByMessageId(reengMessage.getId());
        mReengMessageDataSource.deleteAMessage(reengMessage.getId());
        // cap nhat last message
        mThreadMessageDataSource.updateThreadMessageAfterDeleteMessage(threadMessage);
        if (reengMessage.getDirection() == ReengMessageConstant.Direction.received &&
                (reengMessage.getMessageType() == ReengMessageConstant.MessageType.image ||
                        reengMessage.getMessageType() == ReengMessageConstant.MessageType.voicemail ||
                        reengMessage.getMessageType() == ReengMessageConstant.MessageType.shareVideo)) {
            FileHelper.deleteFile(mApplication, reengMessage.getFilePath());
        }
    }

    private ThreadMessage createThreadGroup(String serverId, String groupName, ArrayList<Member> members, int
            stateShow, int groupClass) {
        return createThreadGroup(serverId, groupName, members, stateShow, groupClass, 0);// nhom thuong
    }

    // group
    private ThreadMessage createThreadGroup(String serverId, String groupName, ArrayList<Member> members, int
            stateShow, int groupClass, int groupPrivate) {
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ArrayList<String> memberNumbers = new ArrayList<>();
        ArrayList<String> adminNumbers = new ArrayList<>();
        for (Member member : members) {
            if (member.getJid() != null) {
                String oldJid = member.getJid();
                String newJid = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(member.getJid());
                if (newJid != null) oldJid = newJid;
                if (!oldJid.equals(myNumber)) {
                    memberNumbers.add(oldJid);
                }
                if (member.isOwner()) {
                    adminNumbers.add(oldJid);
                }
            }
        }
        ThreadMessage newThread = new ThreadMessage();
        newThread.setThreadType(ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT);
        newThread.setPhoneNumbers(memberNumbers);
        newThread.setAdminNumbers(adminNumbers);
        newThread.setName(groupName);
        newThread.setServerId(serverId);
        newThread.setTimeOfLast(TimeHelper.getCurrentTime());
        //newThread.setState(stateShow);
        newThread.setState(ThreadMessageConstant.STATE_SHOW);
        newThread.setLastMessageId(ThreadMessageConstant.LAST_MESSAGE_ID_DEFAULT);
        newThread.setGroupClass(groupClass);
        newThread.setPrivateThread(groupPrivate);
        newThread.setState(stateShow);
        insertThreadMessage(newThread);
        updateAdminGroup(newThread);
        mThreadMessageArrayList.add(newThread);
        refreshThreadWithoutNewMessage(newThread.getId());
        return newThread;
    }

    public ThreadMessage createBroadcast(String serverId, String groupName, ArrayList<String> members
            , int stateShow, boolean onReceivedMessage) {
        ThreadMessage newThread = new ThreadMessage();
        newThread.setThreadType(ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT);
        newThread.setPhoneNumbers(members);
        newThread.setName(groupName);
        newThread.setServerId(serverId);
        newThread.setTimeOfLast(TimeHelper.getCurrentTime());
        newThread.setState(stateShow);
        newThread.setLastMessageId(ThreadMessageConstant.LAST_MESSAGE_ID_DEFAULT);
        insertThreadMessage(newThread);
        mThreadMessageArrayList.add(newThread);
        refreshThreadWithoutNewMessage(newThread.getId());
        return newThread;
    }

    public void renameBroadcast(String groupName, ThreadMessage oldThread) {
        mContactBusiness = mApplication.getContactBusiness();
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (TextUtils.isEmpty(groupName)) {
            groupName = mRes.getString(R.string.group_name_default);
        }
        oldThread.setName(groupName);
        mThreadMessageDataSource.updateThreadMessage(oldThread);
        // them message thong bao
        String content = String.format(mRes.getString(R.string.msg_noti_rename_broadcast_me), groupName);
        ReengMessage message = insertMessageNotify(content, myNumber,
                oldThread.getServerId(), oldThread, ReengMessageConstant.READ_STATE_READ, TimeHelper.getCurrentTime());
        notifyNewMessage(message, oldThread);
        notifyChangeGroupName(oldThread);
    }

    public void inviteBroadcast(ArrayList<String> members, ThreadMessage oldThread) {
        mContactBusiness = mApplication.getContactBusiness();
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ArrayList<String> successMember = new ArrayList<>();
        for (String member : members) {
            if (!oldThread.getPhoneNumbers().contains(member)) {
                oldThread.addNumberToThread(member);
                successMember.add(member);
            }
        }
        mThreadMessageDataSource.updateThreadMessage(oldThread);
        if (successMember.size() > 0) {
            String content = mContactBusiness.getListNameOfListNumber(successMember);
            content += " " + mApplication.getString(R.string.have_added_broadcast);
            ReengMessage message = insertMessageNotify(content, myNumber,
                    oldThread.getServerId(), oldThread, ReengMessageConstant.READ_STATE_READ, TimeHelper
                            .getCurrentTime());
            notifyNewMessage(message, oldThread);
        }
        notifyChangeGroupMember(oldThread);
    }

    public void removeBroadcast(ArrayList<String> members, ThreadMessage oldThread) {
        mContactBusiness = mApplication.getContactBusiness();
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        Log.e(TAG, "remove broadcast");
        ArrayList<String> successMember2 = new ArrayList<>();
        for (String member : members) {
            if (oldThread.getPhoneNumbers().contains(member)) {
                oldThread.removeNumberFromThread(member);
                successMember2.add(member);
            }
        }
        mThreadMessageDataSource.updateThreadMessage(oldThread);
        if (successMember2.size() > 0) {
            String content = mContactBusiness.getListNameOfListNumber(successMember2);
            content += " " + mApplication.getString(R.string.have_removed_broadcast);
            ReengMessage message2 = insertMessageNotify(content, myNumber,
                    oldThread.getServerId(), oldThread, ReengMessageConstant.READ_STATE_READ, TimeHelper
                            .getCurrentTime());
            notifyNewMessage(message2, oldThread);
        }
        notifyChangeGroupMember(oldThread);
    }

    public XMPPResponseCode createGroup(String groupName, ArrayList<String> numbers) {
        IQGroup packet = new IQGroup(IQGroup.GroupType.create, IQ.Type.SET, "create");
        packet.setGroupName(groupName);
        packet.setMembers(numbers);
        return sendIQGroupConfig(packet, null);
    }

    public XMPPResponseCode inviteGroup(ArrayList<String> numbers, ThreadMessage invitingGroup) {
        IQGroup packet = new IQGroup(IQGroup.GroupType.invite, IQ.Type.SET, invitingGroup.getServerId());
        packet.setMembers(numbers);
        return sendIQGroupConfig(packet, invitingGroup);
    }

    public XMPPResponseCode renameGroup(ThreadMessage renameThread, String groupName) {
        IQGroup packet = new IQGroup(IQGroup.GroupType.rename, IQ.Type.SET, renameThread.getServerId());
        packet.setGroupName(groupName);
        return sendIQGroupConfig(packet, renameThread);
    }

    public XMPPResponseCode leaveGroup(ThreadMessage threadMessage) {
        IQGroup packet = new IQGroup(IQGroup.GroupType.leave, IQ.Type.SET, threadMessage.getServerId());
        XMPPResponseCode result = sendIQGroupConfig(packet, threadMessage);
        int responseCode = result.getCode();
        // roi nhom thanh cong, nhom ko con ton tai, hoac ko con la thanh vien cua nhom
        if (responseCode == XMPPCode.E200_OK ||
                responseCode == XMPPCode.E404_GROUP_NO_LONGER_EXIST ||
                responseCode == XMPPCode.E415_NOT_ALLOW_LEAVE_ROOM ||
                responseCode == XMPPCode.E416_NOT_ALLOW_INVITE) {
            threadMessage.setJoined(false);
            threadMessage.setPhoneNumbers(new ArrayList<String>());
            threadMessage.setAdminNumbers(new ArrayList<String>());
            updateAdminGroup(threadMessage);
            mThreadMessageDataSource.updateThreadMessage(threadMessage);
            threadMessage.setForceCalculatorAllMember(true);
            GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(threadMessage.getId());
            // deleteThreadMessage(threadMessage);
        }
        return result;
    }

    // roi va xoa nhom
    public XMPPResponseCode leaveAndDeleteGroup(ThreadMessage threadMessage) {
        IQGroup packet = new IQGroup(IQGroup.GroupType.leave, IQ.Type.SET, threadMessage.getServerId());
        XMPPResponseCode result = sendIQGroupConfig(packet, threadMessage);
        int responseCode = result.getCode();
        // roi nhom thanh cong, nhom ko con ton tai, hoac ko con la thanh vien cua nhom
        if (responseCode == XMPPCode.E200_OK ||
                responseCode == XMPPCode.E404_GROUP_NO_LONGER_EXIST ||
                responseCode == XMPPCode.E415_NOT_ALLOW_LEAVE_ROOM ||
                responseCode == XMPPCode.E416_NOT_ALLOW_INVITE) {
            deleteThreadMessage(threadMessage);
        }
        return result;
    }

    public XMPPResponseCode getInfoGroup(ThreadMessage threadMessage) {
        IQGroup packet = new IQGroup(IQGroup.GroupType.config, IQ.Type.GET, threadMessage.getServerId());
        return sendIQGroupConfig(packet, threadMessage);
    }

    public XMPPResponseCode kickMemberGroup(String number, ThreadMessage threadGroup) {
        ArrayList<String> members = new ArrayList<>();
        members.add(number);
        IQGroup packet = new IQGroup(IQGroup.GroupType.kick, IQ.Type.SET, threadGroup.getServerId());
        packet.setMembers(members);
        return sendIQGroupConfig(packet, threadGroup);
    }

    /**
     * gui ban tin make admin hoac make member
     *
     * @param number
     * @param threadGroup
     * @param isMakeAdmin
     * @return
     */
    public XMPPResponseCode makeAdminGroup(String number, ThreadMessage threadGroup, boolean isMakeAdmin) {
        ArrayList<Member> members = new ArrayList<>();
        Member member = new Member();
        member.setJid(number);
        if (isMakeAdmin) {
            member.setRole(Member.Role.owner);
        } else {
            member.setRole(Member.Role.member);
        }
        members.add(member);
        IQGroup packet = new IQGroup(IQGroup.GroupType.makeAdmin, IQ.Type.SET, threadGroup.getServerId());
        packet.setMemberObjects(members);
        return sendIQGroupConfig(packet, threadGroup);
    }

    public XMPPResponseCode makeGroupPrivate(ThreadMessage threadGroup, boolean statePrivate) {
        IQGroup packet = new IQGroup(IQGroup.GroupType.groupPrivate, IQ.Type.SET, threadGroup.getServerId());
        packet.setGroupPrivate(statePrivate ? 1 : 0);
        return sendIQGroupConfig(packet, threadGroup);
    }

    /**
     * gui cac ban tin tao nhom, moi vao nhom, sua ten, roi nhom
     *
     * @param sendingPacket
     * @return
     */
    private XMPPResponseCode sendIQGroupConfig(IQGroup sendingPacket, ThreadMessage oldThread) {
        XMPPResponseCode mXMPPResponseCode = new XMPPResponseCode();
        try {
            mXMPPResponseCode.setContain(null);
            IQ mResponePacket = mApplication.getXmppManager().sendPacketIQGroupThenWaitingResponse(sendingPacket);
            if (mResponePacket != null) {
                //xu ly ban tin IQ tra ve
                IQ.Type type = mResponePacket.getType();
                if (type != null) {
                    if (type == IQ.Type.RESULT) {
                        if (oldThread != null && oldThread.getState() == ThreadMessageConstant.STATE_NEED_GET_INFO)
                            oldThread.setState(ThreadMessageConstant.STATE_SHOW);
                        processSuccessIQGroupConfig(mXMPPResponseCode, (IQGroup) mResponePacket, oldThread);
                    } else if (type == IQ.Type.ERROR) {
                        //neu type=error thi la ban tin bao loi
                        XMPPError error = mResponePacket.getError();
                        if (error != null) {
                            mXMPPResponseCode.setCode(error.getCode());
                            // nhom cu da co admin ma minh xin quyn admin thi co loi nay, cap nhat lai ds admin tren sv
                            if (error.getCode() == XMPPCode.E405_GROUP_ADMIN_EXIST) {
                                processErrorAdminExistGroup((IQGroup) mResponePacket, oldThread);
                            } else if (error.getCode() == XMPPCode.E416_NOT_ALLOW_INVITE) {
                                oldThread.setJoined(false);
                                oldThread.setPhoneNumbers(new ArrayList<String>());
                                oldThread.setAdminNumbers(new ArrayList<String>());
                                updateAdminGroup(oldThread);
                                mThreadMessageDataSource.updateThreadMessage(oldThread);
                                oldThread.setForceCalculatorAllMember(true);
                                GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(oldThread.getId());
                            }
                        } else {
                            mXMPPResponseCode.setCode(XMPPCode.E602_ERROR_NULL_TYPE);
                        }
                    } else {
                        //type == null -> loi khong xac dinh
                        mXMPPResponseCode.setCode(XMPPCode.E602_ERROR_NULL_TYPE);
                    }
                }
            } else {
                //responePacket = null -> chua ro nguyen nhan tai sao
                mXMPPResponseCode.setCode(XMPPCode.E603_ERROR_IQ_NO_RESPONE);
            }
        } catch (IllegalStateException e) {
            mXMPPResponseCode.setCode(XMPPCode.E490_ILLEGAL_STATE_EXCEPTION);
            Log.e(TAG, "IllegalStateException ", e);
        } catch (XMPPException e) {
            mXMPPResponseCode.setCode(XMPPCode.E604_ERROR_XMPP_EXCEPTION);
            Log.e(TAG, "XMPPException ", e);
        } catch (Exception e) {
            mXMPPResponseCode.setCode(XMPPCode.E601_ERROR_BUT_UNDEFINED);
            Log.e(TAG, "Exception ", e);
        }
        return mXMPPResponseCode;
    }

    /**
     * su ly ban in iq group
     *
     * @param mXMPPResponseCode
     * @param responsePacket
     * @param oldThread
     */
    private void processSuccessIQGroupConfig(XMPPResponseCode mXMPPResponseCode,
                                             IQGroup responsePacket, ThreadMessage oldThread) {
        if (checkDuplicatePacket(responsePacket.getPacketID())) {
            return;
        }
        mContactBusiness = mApplication.getContactBusiness();
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        mXMPPResponseCode.setCode(XMPPCode.E200_OK);
        String groupName = responsePacket.getGroupName();
        if (TextUtils.isEmpty(groupName)) {
            groupName = mRes.getString(R.string.group_name_default);
        }
        IQGroup.GroupType groupType = responsePacket.getGroupType();
        int groupClass = responsePacket.getGroupClass();
        mApplication.getContactBusiness().updateStateNonContactAfterChangedGroup(responsePacket.getMembers());
        switch (groupType) {
            case create:    // tao thread moi
                String groupId = responsePacket.getGroupJid();
                ThreadMessage threadMessage = createThreadGroup(groupId, groupName,
                        responsePacket.getMembers(), ThreadMessageConstant.STATE_SHOW, groupClass);
                mXMPPResponseCode.setContain(threadMessage);
                break;
            case invite:    // them thanh vien vao nhom, them message thong bao
                ArrayList<Member> listMember = responsePacket.getMemberObjects();
                ArrayList<String> successMember = new ArrayList<>();
                ArrayList<String> failureMember = new ArrayList<>();
                ArrayList<String> mOldMemberList = oldThread.getPhoneNumbers();
                for (Member member : listMember) {
                    final String jid = member.getJid();
                    final int code = member.getCode();
                    if (code == XMPPCode.E200_OK) {
                        //thanh cong
                        successMember.add(jid);
                        oldThread.addNumberToThread(jid);
                        oldThread.setForceCalculatorAllMember(true);
                        GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(oldThread.getId());
                    } else if (code == XMPPCode.E409_ALREADY_IN_GROUP) {// thanh vien da co trong nhom
                        if (!oldThread.getPhoneNumbers().contains(jid)) {
                            oldThread.addNumberToThread(jid);
                        }
                    } else {
                        //that bai
                        if (!mOldMemberList.contains(jid)) {
                            failureMember.add(jid);
                        }
                    }
                }
                mThreadMessageDataSource.updateThreadMessage(oldThread);
                if (failureMember.size() > 0) {
                    //tao tin nhan thong bao that bai
                    String contentFail = mContactBusiness.getListNameOfListNumber(failureMember);
                    contentFail += " " + mApplication.getString(R.string.can_not_join_group);
                    ReengMessage message = insertMessageNotify(contentFail, myNumber,
                            oldThread.getServerId(), oldThread,
                            ReengMessageConstant.READ_STATE_UNREAD, TimeHelper.getCurrentTime(),
                            MessageConstants.NOTIFY_TYPE.join.name());
                    notifyNewMessage(message, oldThread);
                }
                if (successMember.size() > 0) {
                    //tao tin nhan thong bao thanh cong
                    String contentSuccess = mContactBusiness.getListNameOfListNumber(successMember);
                    contentSuccess += " " + mApplication.getString(R.string.have_joined_group);
                    ReengMessage message = insertMessageNotify(contentSuccess, myNumber,
                            oldThread.getServerId(), oldThread,
                            ReengMessageConstant.READ_STATE_READ, TimeHelper.getCurrentTime(),
                            MessageConstants.NOTIFY_TYPE.join.name());
                    notifyNewMessage(message, oldThread);
                }
                mXMPPResponseCode.setContain(oldThread);
                break;
            case leave:
                // khong lam gi
                break;
            case rename:
                oldThread.setName(groupName);
                mThreadMessageDataSource.updateThreadMessage(oldThread);
                // them message thong bao
                String contentReName = String.format(mRes.getString(R.string.msg_noti_rename_group_me), groupName);
                ReengMessage message = insertMessageNotify(contentReName, myNumber,
                        oldThread.getServerId(), oldThread,
                        ReengMessageConstant.READ_STATE_READ, TimeHelper.getCurrentTime(),
                        MessageConstants.NOTIFY_TYPE.rename.name());
                notifyNewMessage(message, oldThread);
                notifyChangeGroupName(oldThread);
                break;
            case config:
                int dhvtt = responsePacket.getDhVtt();
                oldThread.setDhVtt(dhvtt);
                oldThread.setName(groupName);
                oldThread.setGroupClass(groupClass);// update khi get config group
                if (responsePacket.getMemberObjects() != null) {
                    ArrayList<String> memberNumbers = new ArrayList<>();
                    ArrayList<String> adminNumbers = new ArrayList<>();
                    for (Member member : responsePacket.getMemberObjects()) {
                        if (member.getJid() != null) {
                            if (!member.getJid().equals(myNumber)) {
                                memberNumbers.add(member.getJid());
                            }
                            if (member.isOwner()) {
                                adminNumbers.add(member.getJid());
                            }
                        }
                    }
                    oldThread.setPhoneNumbers(memberNumbers);
                    oldThread.setAdminNumbers(adminNumbers);
                    oldThread.setForceCalculatorAllMember(true);
                }
                updateAdminGroup(oldThread);
                oldThread.setPrivateThread(responsePacket.getGroupPrivate());
                oldThread.setGroupAvatar(responsePacket.getGroupAvatar());
                mThreadMessageDataSource.updateThreadMessage(oldThread);
                notifyChangeGroupMember(oldThread);
                GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(oldThread.getId());
                refreshThreadWithoutNewMessage(oldThread.getId());
                break;
            case kick:
                // thang # bi kick (iq ko the kick chinh minh)
                ArrayList<Member> kicks = responsePacket.getMemberObjects();
                if (kicks == null || kicks.isEmpty()) return;
                String kickNumber = kicks.get(0).getJid();
                String kickName = mApplication.getMessageBusiness().getFriendNameOfGroup(kickNumber);
                String contentKick = String.format(mRes.getString(R.string.msg_noti_kick_member),
                        mRes.getString(R.string.you), kickName);
                ReengMessage messageNotifyKick = insertMessageNotify(contentKick, myNumber,
                        oldThread.getServerId(), oldThread,
                        ReengMessageConstant.READ_STATE_READ, TimeHelper.getCurrentTime(),
                        MessageConstants.NOTIFY_TYPE.kick.name());
                notifyNewMessage(messageNotifyKick, oldThread);
                // update thread
                oldThread.removeNumberFromThread(kickNumber);
                oldThread.removeAdminFromThread(kickNumber);
                updateAdminGroup(oldThread);// cap nhat trang thai admin
                mThreadMessageDataSource.updateThreadMessage(oldThread);
                oldThread.setForceCalculatorAllMember(true);
                GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(oldThread.getId());
                notifyChangeGroupMember(oldThread);
                break;
            case makeAdmin:
                String typeNotify = groupType.name();
                ArrayList<Member> makes = responsePacket.getMemberObjects();
                if (makes == null || makes.isEmpty()) return;
                Member member = makes.get(0);// make admin chi thuc hien voi 1 thanh vien 1 lan
                String friendNumberMake = member.getJid();
                String friendNameMake = mApplication.getMessageBusiness().getFriendNameOfGroup(friendNumberMake);
                // ko phai so cua minh thi them vao danh sach thanh vien
                if (myNumber != null && !myNumber.equals(friendNumberMake)) {
                    oldThread.addNumberToThread(friendNumberMake);
                }
                String contentMake;
                if (member.isOwner()) {// them quyen admin
                    oldThread.addAdminToThread(friendNumberMake);
                    typeNotify = MessageConstants.NOTIFY_TYPE.makeAdmin.name();
                    if (myNumber != null && myNumber.equals(friendNumberMake)) {// chinh minh
                        contentMake = mRes.getString(R.string.msg_noti_make_admin_me);
                    } else {
                        contentMake = String.format(mRes.getString(R.string.msg_noti_make_admin),
                                mRes.getString(R.string.you), friendNameMake);
                    }
                } else {// xoa quyen admin thanh member
                    oldThread.removeAdminFromThread(friendNumberMake);
                    typeNotify = MessageConstants.NOTIFY_TYPE.removeAdmin.name();
                    if (myNumber != null && myNumber.equals(friendNumberMake)) {// chinh minh
                        contentMake = mRes.getString(R.string.msg_noti_make_member_me);
                    } else {
                        contentMake = String.format(mRes.getString(R.string.msg_noti_make_member),
                                mRes.getString(R.string.you), friendNameMake);
                    }
                }
                // insert message notify
                ReengMessage messageNotifyMake = insertMessageNotify(contentMake, myNumber,
                        oldThread.getServerId(), oldThread,
                        ReengMessageConstant.READ_STATE_READ, TimeHelper.getCurrentTime(), typeNotify);
                notifyNewMessage(messageNotifyMake, oldThread);
                // update thread
                updateAdminGroup(oldThread);// cap nhat trang thai admin
                mThreadMessageDataSource.updateThreadMessage(oldThread);
                notifyChangeGroupMember(oldThread);
                break;
            case groupPrivate:
                String typeNotify2 = groupType.name();
                int statePrivate = responsePacket.getGroupPrivate();
                oldThread.setPrivateThread(statePrivate);
                String contentPrivate;
                if (oldThread.isPrivateThread()) {
                    typeNotify2 = MessageConstants.NOTIFY_TYPE.groupPrivate.name();
                    contentPrivate = String.format(mRes.getString(R.string.msg_noti_set_private),
                            mRes.getString(R.string.you));
                } else {
                    typeNotify2 = MessageConstants.NOTIFY_TYPE.groupPublic.name();
                    contentPrivate = String.format(mRes.getString(R.string.msg_noti_unset_private),
                            mRes.getString(R.string.you));
                }
                ReengMessage messageNotifyPrivate = insertMessageNotify(contentPrivate, myNumber,
                        oldThread.getServerId(), oldThread,
                        ReengMessageConstant.READ_STATE_READ, TimeHelper.getCurrentTime(), typeNotify2);
                notifyNewMessage(messageNotifyPrivate, oldThread);
                mThreadMessageDataSource.updateThreadMessage(oldThread);
                notifyChangeGroupPrivate(oldThread);
                break;
            default:
                break;
        }
    }

    // xu ly ban tin error 405 khi minh xin quyen admin ma da co admin roi
    private void processErrorAdminExistGroup(IQGroup responsePacket, ThreadMessage oldThread) {
        if (checkDuplicatePacket(responsePacket.getPacketID())) {
            return;
        }
        mContactBusiness = mApplication.getContactBusiness();
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ArrayList<Member> members = responsePacket.getMemberObjects();
        if (members == null || members.isEmpty()) return;
        IQGroup.GroupType groupType = responsePacket.getGroupType();
        Log.d(TAG, "processErrorAdminExistGroup groupType: " + groupType);
        if (groupType == IQGroup.GroupType.makeAdmin) {
            for (Member member : members) {
                Log.d(TAG, "processErrorAdminExistGroup member: " + member.toXml());
                if (member.isOwner()) {
                    // khong phai la so chinh minh the add member
                    if (myNumber != null && !myNumber.equals(member.getJid())) {
                        oldThread.addNumberToThread(member.getJid());
                    }
                    oldThread.addAdminToThread(member.getJid());
                }
            }
            // update thread
            updateAdminGroup(oldThread);// cap nhat trang thai admin
            mThreadMessageDataSource.updateThreadMessage(oldThread);
            notifyChangeGroupMember(oldThread);
        }
    }

    /**
     * them message thong bao khi tao xong thread
     *
     * @param receiver
     * @param content
     * @param sender
     * @param threadMessage
     * @param readState
     * @return
     */
    public ReengMessage insertMessageNotify(String content, String receiver, String sender
            , ThreadMessage threadMessage, int readState, long messageTime) {
        EmoticonUtils.createCacheSpanned(mApplication, content);
        ReengMessage message = new ReengMessage();// msg ko gui di nen ko quan trong paketId
        message.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
        message.setReceiver(receiver);
        message.setSender(sender);
        message.setReadState(readState);
        message.setThreadId(threadMessage.getId());
        message.setDirection(ReengMessageConstant.Direction.received);
        message.setTime(messageTime);
        message.setStatus(ReengMessageConstant.STATUS_RECEIVED);
        message.setMessageType(ReengMessageConstant.MessageType.notification);
        message.setContent(content);
        //insert message vao thread trong database
        this.insertNewMessageToDB(threadMessage, message);
        return message;
    }

    public ReengMessage insertMessageNotify(String content, String receiver, String sender
            , ThreadMessage threadMessage, int readState, long messageTime, String typeNotify) {
        EmoticonUtils.createCacheSpanned(mApplication, content);
        ReengMessage message = new ReengMessage();// msg ko gui di nen ko quan trong paketId
        message.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
        message.setReceiver(receiver);
        message.setSender(sender);
        message.setReadState(readState);
        message.setThreadId(threadMessage.getId());
        message.setDirection(ReengMessageConstant.Direction.received);
        message.setTime(messageTime);
        message.setStatus(ReengMessageConstant.STATUS_RECEIVED);
        message.setMessageType(ReengMessageConstant.MessageType.notification);
        message.setContent(content);
        message.setFileName(typeNotify);
        //insert message vao thread trong database
        this.insertNewMessageToDB(threadMessage, message);
        return message;
    }

    /**
     * them message thong bao khi tao xong thread
     *
     * @param receiver
     * @param content
     * @param sender
     * @param threadMessage
     * @param stateCall
     * @return
     */
    public ReengMessage insertMessageCall(ThreadMessage threadMessage, String content, String receiver
            , String sender, ReengMessageConstant.Direction direction, int stateCall, int duration
            , long messageTime, int readState, int chatMode, boolean isConfide) {
        ReengMessage message = new ReengMessage();
        message.setMessageType(isConfide ? ReengMessageConstant.MessageType.talk_stranger : ReengMessageConstant
                .MessageType.call);
        message.setPacketId(PacketMessageId.getInstance().
                genMessagePacketId(threadMessage.getThreadType(), message.getMessageType().toString()));
        message.setThreadId(threadMessage.getId());
        message.setReceiver(receiver);
        message.setSender(sender);
        message.setReadState(readState);
        message.setStatus(ReengMessageConstant.STATUS_CHANGE_SONG);
        message.setDirection(direction);
        message.setTime(messageTime);
        message.setContent(content);
        message.setSongId(stateCall);// state call
        message.setDuration(duration);
        message.setChatMode(chatMode);
        //insert message vao thread trong database
        this.insertNewMessageToDB(threadMessage, message);
        return message;
    }

    public ReengMessage createMessageBankPlus(String sender, String receiver, ThreadMessage threadMessage
            , String amount, String desc, String id, String type, long messageTime) {
        ReengMessage message = new ReengMessage();
        message.setPacketId(PacketMessageId.getInstance().genMessagePacketId(threadMessage.getThreadType(),
                "bank_plus"));
        message.setReceiver(receiver);
        message.setSender(sender);
        message.setReadState(ReengMessageConstant.READ_STATE_READ);
        message.setThreadId(threadMessage.getId());
        message.setDirection(ReengMessageConstant.Direction.send);
        message.setTime(messageTime);
        message.setStatus(ReengMessageConstant.STATUS_LOADING);
        message.setMessageType(ReengMessageConstant.MessageType.bank_plus);
        message.setFilePath(type);
        message.setImageUrl(amount);
        message.setVideoContentUri(desc);
        message.setFileId(id);
        //message.setContent(content);
        return message;
    }

    /**
     * insert message notifi khi nhan duoc thong bao config group
     *
     * @param application
     * @param threadMessage
     * @param packetId
     * @param roomId
     * @param userNumber
     * @param content
     */

    public void insertNewMessageNotifyGroup(ApplicationController application, ThreadMessage threadMessage
            , String packetId, String roomId, String userNumber, String content
            , ReengMessagePacket.SubType subType, long time, String groupAvatar, String notifyType) {
        insertNewMessageNotifyGroup(application, threadMessage, packetId, roomId, userNumber, content, subType, time, groupAvatar, notifyType, null);
    }

    public void insertNewMessageNotifyGroup(ApplicationController application, ThreadMessage threadMessage
            , String packetId, String roomId, String userNumber, String content
            , ReengMessagePacket.SubType subType, long time, String groupAvatar, String notifyType
            , String sender) {
        if (TextUtils.isEmpty(content)) return;// content null thi ko insert notify

        boolean isFromDesktop = (!TextUtils.isEmpty(sender) && sender.equals(myNumber));
        EmoticonUtils.createCacheSpanned(mApplication, content);
        ReengNotificationManager mNotificationManager = ReengNotificationManager.getInstance(application);
        ReengMessage message = new ReengMessage();// mes khong gui len ko can gen packetid chuan
        //message.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
        message.setPacketId(packetId); // set packet id
        message.setReceiver(userNumber);
        message.setSender(roomId);
        message.setReadState(ReengMessageConstant.READ_STATE_UNREAD);
        message.setThreadId(threadMessage.getId());
        message.setDirection(ReengMessageConstant.Direction.received);
        message.setTime(time);
        message.setStatus(ReengMessageConstant.STATUS_RECEIVED);
        message.setMessageType(ReengMessageConstant.MessageType.notification);
        message.setContent(content);
        message.setFileName(notifyType);
        //xu ly doi voi truong hop tung ban
        int numberOfNewMessage = 1;
        switch (subType) {
            case create:
                break;
            case invite:
                break;
            case join:
                break;
            case rename:
                break;
            case leave:
                message.setReadState(ReengMessageConstant.READ_STATE_READ); //doi voi ban tin roi nhom thi ko can doc
                numberOfNewMessage = 0;
                break;
            case groupAvatar:
                message.setImageUrl(groupAvatar);
                numberOfNewMessage = 0;
                break;
            default:
                break;
        }
        // insert message vao thread trong database
        // truong hop create thi ko notify
        // truong hop join thi cung ko notify
        //TODO hien thi lai notification khi duoc moi vao nhom
        /*if (subType == ReengMessagePacket.SubType.create || (subType == ReengMessagePacket.SubType.join)) {
            //ko notify j ca
            return;
        } else {*/
        this.insertNewMessageToDB(threadMessage, message);
        //}
        //------------------tang so tin nhan chua doc--------------------------
        if (!isFromDesktop)
            threadMessage.setNumOfUnreadMessage(threadMessage.getNumOfUnreadMessage() + numberOfNewMessage);
        //----------------------------------------------------------------------
        notifyNewMessage(message, threadMessage);
        if (numberOfNewMessage > 0) {
            if (!isFromDesktop)
                mNotificationManager.drawMessagesNotification(application, threadMessage,
                        ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT, message);
        }
        if (!isFromDesktop)
            mApplication.updateCountNotificationIcon();
        updateThreadMessage(threadMessage);
    }

    private Handler handler;

    public synchronized void notifyNewMessage(ReengMessage message, ThreadMessage threadMessage) {
        boolean isOnChatScreen = (threadMessage.getId() == ReengNotificationManager.getCurrentUIThreadId());
        if (isOnChatScreen) {
            message.setNewMessage(false);
        } else if (message.getDirection() == ReengMessageConstant.Direction.received) {
            message.setNewMessage(true);
            threadMessage.setHasNewMessage(true);
        }
        /*insertNewMessageToMemory(threadMessage, message);// insert vào mem luon
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            //neu co man hinh nao dang listener new message thi se insert o trong UI Thread
            notifyNewIncomingMessage(message, threadMessage);
        }*/
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            //neu co man hinh nao dang listener new message thi se insert o trong UI Thread

            doNotifyNewMessage(threadMessage, message);

        } else {
            insertNewMessageToMemory(threadMessage, message);
            mApplication.logDebugContent("insertNewMessageToMemory mReengMessageListenerArrayList null empty: " + message.getPacketId());
            Log.f(TAG, "insertNewMessageToMemory mReengMessageListenerArrayList null empty: " + message.getPacketId());
        }
    }

    private void doNotifyNewMessage(final ThreadMessage threadMessage, final ReengMessage message) {
        if (handler == null) handler = new Handler(mApplication.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                insertNewMessageToMemory(threadMessage, message);
                for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                    listener.notifyNewIncomingMessage(message, threadMessage);
                }
                if (mReengMessageListenerThreadDetailInbox != null) {
                    mReengMessageListenerThreadDetailInbox.notifyNewIncomingMessage(message, threadMessage);
                }
            }
        });
    }

    private synchronized void notifyNewPinMessage(ReengMessage reengMessage, ThreadMessage mCorrespondingThread) {
        if (mReengMessageListenerArrayList != null && !mReengMessageListenerArrayList.isEmpty()) {
            //neu co man hinh nao dang listener new message thi se insert o trong UI Thread
            for (ReengMessageListener listener : mReengMessageListenerArrayList) {
                listener.onBannerDeepLinkUpdate(reengMessage, mCorrespondingThread);
            }
            if (mReengMessageListenerThreadDetailInbox != null) {
                mReengMessageListenerThreadDetailInbox.onBannerDeepLinkUpdate(reengMessage, mCorrespondingThread);
            }
        }
    }

    /*public void insertNewMessageSuggestShareMusic(ThreadMessage threadMessage, String from, String to, MediaModel
            mediaModel) {
        SuggestShareMusicMessage message = new SuggestShareMusicMessage(threadMessage,
                from, to, mediaModel);
        insertNewMessageToDB(threadMessage, message);
        threadMessage.setLastTimeShareMusic(TimeHelper.getCurrentTime());
        updateThreadMessage(threadMessage);
        // thong ke ga suggest music, label = song name
        mApplication.trackingEvent(mRes.getString(R.string.ga_category_remind),
                mRes.getString(R.string.ga_action_remind_suggest_music),
                mRes.getString(R.string.ga_label_remind_suggest_music));
        notifyNewMessage(message, threadMessage);
    }*/

    public void showQuickReplyActivity(Context context, ReengMessage newMessage, ThreadMessage thread) {
        String currentActivity = mApplicationStateManager.getCurrentActivity();
        boolean isScreenOn = mApplicationStateManager.isScreenOn();
        boolean isLocked = mApplicationStateManager.isScreenLocker();
        if (!mSettingBusiness.getPrefEnableUnlock() && isLocked) {
            return;
        }
        // dung mApplicationStateManager.isAppWentToBg(), khong check packetname nhu cu nua
        Log.i(TAG, "Check:----mApplicationStateManager.isAppWentToBg()" + mApplicationStateManager.isAppWentToBg());
        if (mApplicationStateManager.isActivityForResult() || mApplicationStateManager.isTakePhotoAndCrop()) {
            return;// dang chụp anh, luu contact.
        }
        boolean isAppForeground = mApplicationStateManager.isAppForeground();
        if (!isAppForeground || !isScreenOn
                || (isLocked && !currentActivity.equals(QuickReplyActivity.class.getCanonicalName())) &&
                !currentActivity.equals(QuickMissCallActivity.class.getCanonicalName())) {// khong activity quick nao
            // hien thi thì show
            Log.i(TAG, "Show QuickReplyActivity: " + newMessage);
            if (!thread.isLoadDetail() && thread.getNumOfUnreadMessage() > 0) {
                long t = System.currentTimeMillis();
                int firstMsgId = -1;
                if (thread.getAllMessages() != null && !thread.getAllMessages().isEmpty()) {
                    firstMsgId = thread.getAllMessages().get(0).getId();
                }
                thread.addMoreMessage(loadLimitMessage(thread.getId(), thread.getNumOfUnreadMessage(), firstMsgId));
                Log.i(TAG, "Show QuickReplyActivity load from DB take: " + (System.currentTimeMillis() - t));
            }
            mApplicationStateManager.setShowQuickReply(true);
            try {
                Intent mIntent = new Intent(context, QuickReplyActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent
                        .FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_FROM_BACKGROUND);
                mIntent.putExtra(QuickReplyActivity.REENG_MESSAGE, newMessage);
                if (isLocked || !isScreenOn) {
                    mIntent.putExtra(QuickReplyActivity.BG_TRANSPARENT, false);
                } else {
                    mIntent.putExtra(QuickReplyActivity.BG_TRANSPARENT, true);
                }
                if (!isScreenOn) {
                    mIntent.putExtra(QuickReplyActivity.SCREEN_OFF, true);
                } else {
                    mIntent.putExtra(QuickReplyActivity.SCREEN_OFF, false);
                }
                mApplication.wakeLockAcquire();
                context.startActivity(mIntent);
            } catch (Exception ex) {
                Log.e(TAG, "Exception", ex);
            }
        }
    }

    protected boolean checkDuplicatePacket(String packetId) {
        if (packetId == null) return true;
        boolean isPacketIdExisted = checkPacketIdExisted(packetId);
        if (isPacketIdExisted) {
            Log.i(TAG, "packetId " + packetId + " already exist --> duplicate message  --> not process");
            return true;
        }
        pushToPacketIdList(packetId);
        return false;
    }

    private void pushToPacketIdList(String packetId) {
        if (listPacketIdOfReceivedMessage.size() > PACKET_ID_LIST_MAX_SIZE) {
            listPacketIdOfReceivedMessage.pollFirst();
        }
        listPacketIdOfReceivedMessage.addLast(packetId);
    }

    public void removePacketIdWhenDeleteRoom(ThreadMessage threadMessage) {
        if (threadMessage == null || threadMessage.getAllMessages() == null) return;
        for (ReengMessage message : threadMessage.getAllMessages()) {
            if (TextUtils.isEmpty(message.getSender())) {//admin
                if (listPacketIdOfReceivedMessage != null)
                    listPacketIdOfReceivedMessage.remove(message.getPacketId());
            }
        }
    }

    /**
     * @param packetId
     * @return true if packetId already exist
     */
    private boolean checkPacketIdExisted(String packetId) {
        return listPacketIdOfReceivedMessage.contains(packetId);
    }

    private void clearAllListPacketId() {
        listPacketIdOfReceivedMessage.clear();
    }

    public void retrySendTextAndContactMessage(ReengMessage message, ThreadMessage mThreadMessage) {
        mMessageRetryManager.addToWaitingConfirmMap(message);
        message.setTime(System.currentTimeMillis()); //cap nhat thoi gian gui tin nhan la bay h de check timeout
        sendXMPPMessage(message, mThreadMessage);
    }

    @Override
    public void onSmsPasswordReceived(String password) {

    }

    @Override
    public void onSmsPasswordLixiReceived(String password) {

    }

    /**
     * @param smsSender
     * @param smsContent
     * @return true neu da xu ly trong app
     */
    @Override
    public boolean onSmsChatReceived(String smsSender, String smsContent) {
        return mIncomingMessageProcessor.processIncomingSMS(smsSender, smsContent);
    }

    public void sendTypingMessage(String to, int threadType) {
        mApplication.getXmppManager().sendTypingMessage(to, threadType);
    }

    public boolean isLastSendMsg(ReengMessage message) {
        ThreadMessage threadMessage = getThreadById(message.getThreadId());
        if (threadMessage == null) {
            return false;
        }
        CopyOnWriteArrayList<ReengMessage> allMessages = threadMessage.getAllMessages();
        if (allMessages == null || allMessages.isEmpty()) return false;
        ReengMessage lastMessage = allMessages.get(allMessages.size() - 1);
        return message.getPacketId() != null && message.getPacketId().equals(lastMessage.getPacketId());
    }

    public ReengMessage getLastVoiceStickerUnread(CopyOnWriteArrayList<ReengMessage> messageArrayList) {
        for (int i = messageArrayList.size() - 1; i >= 0; i--) {
            if (messageArrayList.get(i).getMessageType() == ReengMessageConstant.MessageType.voiceSticker
                    && messageArrayList.get(i).getReadState() == ReengMessageConstant.READ_STATE_UNREAD) {
                return messageArrayList.get(i);
            }
        }
        return null;
    }

    public ReengMessage getLastVideoMessage(CopyOnWriteArrayList<ReengMessage> messageArrayList) {
        for (int i = messageArrayList.size() - 1; i >= 0; i--) {
            if (messageArrayList.get(i).getMessageType() == ReengMessageConstant.MessageType.watch_video) {
                return messageArrayList.get(i);
            }
        }
        return null;
    }

    public String getContentOfMessage(ReengMessage message, Resources res, ApplicationController mApp) {
        //last message
        ReengMessageConstant.MessageType type = message.getMessageType();
        if (type == ReengMessageConstant.MessageType.text) {
            if (ReengMessageConstant.MESSAGE_ENCRYPTED.equals(message.getMessageEncrpyt())) {
                return res.getString(R.string.encrypt_message);
            }
            ArrayList<TagMocha> listTag = message.getListTagContent();
            return TagHelper.getTextTagCopy(message.getContent(), listTag, mApp);
        } else if (type == ReengMessageConstant.MessageType.notification) {
            if (message.getContent() == null) return "";
            return message.getContent();
        } else if (type == ReengMessageConstant.MessageType.file) {
            return res.getString(R.string.file_message);
        } else if (type == ReengMessageConstant.MessageType.image) {
            return res.getString(R.string.image_message);
        } else if (type == ReengMessageConstant.MessageType.voicemail) {
            return res.getString(R.string.voice_message);
        } else if (type == ReengMessageConstant.MessageType.shareContact) {
            return res.getString(R.string.person_chat_share_contact);
        } else if (type == ReengMessageConstant.MessageType.shareVideo) {
            return res.getString(R.string.person_chat_share_video);
        } else if (type == ReengMessageConstant.MessageType.voiceSticker) {
            return res.getString(R.string.voice_sticker_message);
        } else if (type == ReengMessageConstant.MessageType.suggestShareMusic) {
            return res.getString(R.string.msg_suggest_share_music);
        } else if (type == ReengMessageConstant.MessageType.inviteShareMusic) {
            if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {
                MediaModel songModel = message.getSongModel(mApplication.getMusicBusiness());
                if (songModel == null) {
                    return message.getContent();
                }
                String songName = songModel.getName();
                return String.format(mRes.getString(R.string.invite_share_music_last_content), songName);
            } else {
                return message.getContent();
            }
        } else if (type == ReengMessageConstant.MessageType.shareLocation) {
            return res.getString(R.string.msg_share_location);
        } else if (type == ReengMessageConstant.MessageType.restore) {
            return res.getString(R.string.message_restored);
        } else if (type == ReengMessageConstant.MessageType.transferMoney) {
            return res.getString(R.string.msg_transfer_money);
        } else if (type == ReengMessageConstant.MessageType.image_link) {
            return res.getString(R.string.image_message);
        } else if (type == ReengMessageConstant.MessageType.bank_plus) {
            return message.getContent();
        } else if (type == ReengMessageConstant.MessageType.lixi) {
            return res.getString(R.string.last_msg_lixi);
        } else if (type == ReengMessageConstant.MessageType.pin_message) {
            return res.getString(R.string.message_pin);
        } else if (type == ReengMessageConstant.MessageType.call) {
            return Utilities.getTextCallInfor(res, message.getDirection(), message.getChatMode(), (int) message.getSongId());

        } else if (type == ReengMessageConstant.MessageType.update_app) {
            return res.getString(R.string.message_content_update_app);
        } else {
            return message.getContent();
        }
    }

    private String getCallState(Resources res, ReengMessage message) {
        String stateStr;
        if (message.getDirection() == ReengMessageConstant.Direction.send) {
            if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                stateStr = res.getString(R.string.call_state_mocha);
            } else if (message.getChatMode() == ReengMessageConstant.MODE_VIDEO_CALL) {
                stateStr = res.getString(R.string.call_state_mocha_video);
            } else {
                stateStr = res.getString(R.string.call_state_mocha);
            }
            // content color
            if (message.getSongId() == CallHistoryConstant.STATE_MISS) {
//            mTvwContent.setText(mContext.getResources().getString(R.string.call_state_miss_call));
                stateStr = res.getString(R.string.call_state_mocha);
            } else {
                if (message.getSongId() == CallHistoryConstant.STATE_OUT_GOING) {
                    if (message.getDuration() > 0) {
                        //  mTvwDuration.setText(message.getContent());

                    }
                } else if (message.getSongId() == CallHistoryConstant.STATE_BUSY) {
                    stateStr = res.getString(R.string.call_state_mocha);
                } else if (message.getSongId() == CallHistoryConstant.STATE_CANCELLED) {
                    stateStr = res.getString(R.string.call_state_mocha);
                } else if (message.getSongId() == CallHistoryConstant.STATE_REJECTED) {
                    int id = message.getChatMode() == ReengMessageConstant.MODE_GSM ? R.string.call_state_mocha : R.string.call_state_mocha;
                    stateStr = res.getString(id);
                }
            }
        } else { // call receive
            if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                stateStr = res.getString(R.string.call_state_mocha); // receive no have callout :)))
            } else if (message.getChatMode() == ReengMessageConstant.MODE_VIDEO_CALL) {
                stateStr = res.getString(R.string.call_state_mocha);
            } else {

                stateStr = res.getString(R.string.call_state_mocha);
            }
            // content color
            if (message.getSongId() == CallHistoryConstant.STATE_MISS) {
                stateStr = res.getString(R.string.call_state_mocha);
            } else {
                if (message.getSongId() == CallHistoryConstant.STATE_IN_COMING) {
//                    mTvwContent.setText(stateStr);
//                    mTvwDuration.setText(message.getContent());
                } else if (message.getSongId() == CallHistoryConstant.STATE_BUSY) {
                    stateStr = res.getString(R.string.call_state_mocha); // receive no have call busy
                } else if (message.getSongId() == CallHistoryConstant.STATE_CANCELLED) {
                    stateStr = res.getString(R.string.call_state_mocha);// receive no have call CANCELLED
                } else if (message.getSongId() == CallHistoryConstant.STATE_REJECTED) {
                    stateStr = res.getString(R.string.call_state_mocha);
                } else {
//                    mTvwContent.setText(stateStr);
                }
            }
        }
        return stateStr;
    }

    public void setSendMsgQuickReply(boolean isQuickReply) {
        this.isSendMsgQuickReply = isQuickReply;
    }

    public boolean isWaitInsertThread() {
        return isWaitInsertThread;
    }

    private class UpdateUnreadAndSendSeenMessageTask extends AsyncTask<Void, Void, Void> {
        private CopyOnWriteArrayList<ReengMessage> listMessages;
        private ThreadMessage mThreadMessage;

        public UpdateUnreadAndSendSeenMessageTask(ThreadMessage threadMessage, CopyOnWriteArrayList<ReengMessage>
                listMessages) {
            this.mThreadMessage = threadMessage;
            this.listMessages = listMessages;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i(TAG, "UpdateUnreadAndSendSeenMessageTask doInBackground");
            // update db
            if (mThreadMessage == null || listMessages == null) return null;
            boolean isUpdateStatus;
            if (mThreadMessage.getNumOfUnreadMessage() > 0) {
                isUpdateStatus = true;
                mThreadMessage.setNumOfUnreadMessage(0);
                mThreadMessageDataSource.updateThreadMessage(mThreadMessage);
            } else {
                isUpdateStatus = false;
            }
            MessageNotificationManager.getInstance(mApplication).clearNotifyThreadMessage(mThreadMessage);
            // khong phai 1-1, group thi danh dau seen luon (khong gui ban tin)
            if (mThreadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT &&
                    mThreadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    mThreadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
                if (isUpdateStatus) {
                    for (ReengMessage message : listMessages) {
                        message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                    }
                    mReengMessageDataSource.markAllMessageIsReadState(mThreadMessage.getId(), ReengMessageConstant
                            .READ_STATE_SENT_SEEN);
                }
                return null;
            }
            //
            ArrayList<ReengMessage> listDbUpdate = new ArrayList<>();
            CopyOnWriteArrayList<ReengMessage> listSendSeen = new CopyOnWriteArrayList<>();
            boolean isSettingNotSendSeen = !mSettingBusiness.getPrefEnableSeen();
            for (ReengMessage message : listMessages) {
                if (message.getDirection().equals(ReengMessageConstant.Direction.received)) {
                    if (isSettingNotSendSeen) {
                        if (message.getReadState() != ReengMessageConstant.READ_STATE_SENT_SEEN) {
                            message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                            listDbUpdate.add(message);
                        }
                    } else if (message.getReadState() == ReengMessageConstant.READ_STATE_UNREAD) {
                        if (message.isTypeSendSeenMessage()) {
                            message.setReadState(ReengMessageConstant.READ_STATE_READ);
                            listSendSeen.add(message);
                        } else {// khong ho tro gui seen thi danh dau la seen luon de lan sau khong phai check
                            message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                        }
                        listDbUpdate.add(message);
                    } else if (message.getReadState() == ReengMessageConstant.READ_STATE_READ) {
                        if (message.isTypeSendSeenMessage()) {
                            listSendSeen.add(message);
                        } else {
                            message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                            listDbUpdate.add(message);
                        }
                    }
                } else if (message.getReadState() == ReengMessageConstant.READ_STATE_UNREAD) {
                    message.setReadState(ReengMessageConstant.READ_STATE_READ);
                    listDbUpdate.add(message);
                }
            }
            updateAllFieldsOfListMessage(listDbUpdate);
            if (!listSendSeen.isEmpty()) {
                mApplication.getXmppManager().processSeenListMessage(listSendSeen, mThreadMessage);
                setSendMsgQuickReply(false);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            refreshThreadWithoutNewMessage(mThreadMessage.getId());
            mApplication.updateCountNotificationIcon();
        }
    }

    /**
     * insert list event message to database
     *
     * @param listEventMessages
     */
    private void insertListEventMessageToDB(ArrayList<EventMessage> listEventMessages) {
        mEventMessageDataSource.insertListEventMessage(listEventMessages);
    }

    /**
     * get list event message from db
     *
     * @param reengMessage
     * @return listEventMessage
     */
    public ArrayList<EventMessage> getListEventMessageFromDBByReengMessage(ReengMessage reengMessage) {
        if (reengMessage == null) {
            return null;
        }
        // remove list conflict
        HashSet<EventMessage> eventMessages = new HashSet<>(
                mEventMessageDataSource.getListEventMessageFromMessageId(reengMessage.getId(), reengMessage
                        .getPacketId()));
        return new ArrayList<>(eventMessages);
    }

    public void deleteEventMessageByMessageId(long messageId) {
        mEventMessageDataSource.deleteEventMessageByMessageId(messageId);
    }

    public EventMessage getEventMessageReaction(long messageId, String packetId, String sender) {
        EventMessage eventMessage = mEventMessageDataSource.getListEventMessageReaction(messageId, packetId, sender);
        return eventMessage;
    }

    public String getLastPacketIdOfStickyRoom(int threadId) {
        ReengMessage message = mReengMessageDataSource.getLastMessageByThreadId(threadId);
        if (message != null) {
            return message.getPacketId();
        }
        return null;
    }

    public void updateSendInviteMessageTimeout(ReengMessage message) {
        if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {// gui qua sms out thi cap nhat thanh cong
            message.setMusicState(ReengMessageConstant.MUSIC_STATE_ACCEPTED);
        } else {
            String contentTimeOut;
            String contentBoldTimeOut;
            if (message.getDirection() == ReengMessageConstant.Direction.received) {
                contentTimeOut = mRes.getString(R.string.invite_share_music_time_out);
                contentBoldTimeOut = contentTimeOut;
            } else if (message.getDirection() == ReengMessageConstant.Direction.send) {
                String friendName = getFriendName(message.getReceiver());
                contentTimeOut = String.format(mRes.getString(R.string.invite_share_music_not_answer), friendName);
                contentBoldTimeOut = String.format(mRes.getString(R.string.invite_share_music_not_answer),
                        TextHelper.textBoldWithHTML(friendName));
                // reset current session id
                mApplication.getMusicBusiness().resetSessionMusic();
            } else {
                contentTimeOut = "";
                contentBoldTimeOut = "";
            }
            message.setContent(contentTimeOut);
            message.setFileName(contentBoldTimeOut);
            message.setMusicState(ReengMessageConstant.MUSIC_STATE_TIME_OUT);
        }
        message.setDuration(0);
        // update db
        updateAllFieldsOfMessage(message);
        mApplication.getMusicBusiness().updatePrefSendInviteMusic(-1);
        refreshThreadWithoutNewMessage(message.getThreadId());
    }

    public ArrayList<ThreadMessage> getListThreadGroup() {
        return getListThreadGroup(true);
    }

    public ArrayList<ThreadMessage> getListThreadGroup(boolean showHidden) {
        ArrayList<ThreadMessage> listGroups = new ArrayList<>();
        if (mThreadMessageArrayList != null && !mThreadMessageArrayList.isEmpty()) {
            for (ThreadMessage threadMessage : mThreadMessageArrayList) {
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT &&
                        threadMessage.isJoined()) {
                    if (!showHidden) {
                        if (threadMessage.getHiddenThread() != 1)
                            listGroups.add(threadMessage);
                    } else
                        listGroups.add(threadMessage);
                }
            }
        }
        return listGroups;
    }

    public ArrayList<ThreadMessage> getListThreadStranger() {
        ArrayList<ThreadMessage> listStranger = new ArrayList<>();
        if (mThreadMessageArrayList != null && !mThreadMessageArrayList.isEmpty()) {
            for (ThreadMessage threadMessage : mThreadMessageArrayList) {
                // lam quen qua keeng hoac tim ban nghe nhac
                if (threadMessage.isStranger()) {
                    listStranger.add(threadMessage);
                }
            }
        }
        return listStranger;
    }

    public CopyOnWriteArrayList<ReengMessage> getListMessageProcessInfo() {
        return listMessageProcessInfo;
    }

    public void initListMessageProcessInfo() {
        if (listMessageProcessInfo != null) {
            for (ReengMessage msg : listMessageProcessInfo) {
                msg.setIsForceShow(false);
            }
        }
        listMessageProcessInfo = new CopyOnWriteArrayList<>();
    }

    public void freeListMessageProcessInfo() {
        if (listMessageProcessInfo == null || listMessageProcessInfo.isEmpty()) {
            return;
        }
        for (ReengMessage msg : listMessageProcessInfo) {
            msg.setIsForceShow(false);
        }
        listMessageProcessInfo = null;
    }

    public void processEventMessage(ReengMessagePacket receivedMessage) {
        ArrayList<String> listKey = receivedMessage.getKeyConfig();
        if (listKey != null && listKey.size() > 0) {
            for (String mKey : listKey) {
                if (Constants.MESSAGE.CONFIG.KEY_CONFIG.equals(mKey)) {
                    mApplication.getConfigBusiness().getConfigFromServer(true);
                } else if (Constants.MESSAGE.CONFIG.KEY_STICKER.equals(mKey)) {
                    mApplication.getStickerBusiness().getStickerCollectionFromServer(true);
                } else if (Constants.MESSAGE.CONFIG.KEY_REGID.equals(mKey)) {
                    FireBaseHelper.getInstance(mApplication).checkServiceAndRegister(true);
                } else if (Constants.MESSAGE.CONFIG.KEY_CONTACT.equals(mKey)) {
                    handlerUpdateContact();
                } else if (Constants.MESSAGE.CONFIG.KEY_DEVICE_INFO.equals(mKey)) {
                    if (mApplication.getXmppManager() != null && mApplication.getXmppManager().isAuthenticated()) {
                        mApplication.getXmppManager().sendIQClientInfo(Connection.CODE_AUTH_NON_SASL,
                                false, XMPPManager.IqInfoFirstTime.NORMAL.VALUE);// bat
                        // buoc update
                    }
                } else if (Constants.MESSAGE.CONFIG.KEY_ENABLE_UPLOAD.equals(mKey)) {
                    mAccountBusiness.setEnableUploadLog(true);
                } else if (Constants.MESSAGE.CONFIG.KEY_DISABLE_UPLOAD.equals(mKey)) {
                    mAccountBusiness.setEnableUploadLog(false);
                } else if (Constants.MESSAGE.CONFIG.KEY_ENABLE_DEBUG.equals(mKey)) {
                    LogDebugHelper.getInstance(mApplication).setStateEnableLog(true);
                } else if (Constants.MESSAGE.CONFIG.KEY_DISABLE_DEBUG.equals(mKey)) {
                    LogDebugHelper.getInstance(mApplication).setStateEnableLog(false);
                } else if (Constants.MESSAGE.CONFIG.KEY_UPLOG_DEBUG.equals(mKey)) {
                    //TODO force upload log debug
                    if (LogDebugHelper.getInstance(mApplication).isEnableLog()) {
                        mPref.edit().putLong(LogDebugHelper.PREF_UPLOAD_LOG_DEBUG_LAST_TIME, 0L).apply();
                        mApplication.getTransferFileBusiness().startUploadLog(false);
                    }
                } else if (Constants.MESSAGE.CONFIG.KEY_UPLOG_KQI.equals(mKey)) {
                    if (!mApplication.getReengAccountBusiness().isEnableUploadLog()) return;
                    LogKQIHelper.getInstance(mApplication).uploadLogContent();
                } else if (Constants.MESSAGE.CONFIG.KEY_GROUP_CFG.equals(mKey)) {
                    String idGroup = receivedMessage.getIdGroupGetConfig();
                    if (!TextUtils.isEmpty(idGroup)) {
                        ThreadMessage threadMessage = findGroupThreadByServerId(idGroup);
                        if (threadMessage != null)
                            getInfoGroup(threadMessage);
                    }
                } else if (Constants.MESSAGE.CONFIG.KEY_CHANGE_NUMBER.equals(mKey)) {
                    processChangeNumberAllThread(receivedMessage.getOldUser(), receivedMessage.getNewUser(), null);
                } else if (Constants.MESSAGE.CONFIG.KEY_GET_BLOCK_LIST.equals(mKey)) {
                    processGetListBlock();
                } else if (Constants.MESSAGE.CONFIG.KEY_GET_USER_INFO.equals(mKey)) {
                    processGetUserInfo();
                } else if (Constants.MESSAGE.CONFIG.KEY_BACKUP_MSG.equals(mKey)) {
                    processBackUpMessage();
                } else if (Constants.MESSAGE.CONFIG.KEY_PRODUCT.equals(mKey)) {
                    mApplication.getReengAccountBusiness().setDev(false);
                } else if (Constants.MESSAGE.CONFIG.KEY_DEVELOPMENT.equals(mKey)) {
                    mApplication.getReengAccountBusiness().setDev(true);
                } else if (Constants.MESSAGE.CONFIG.KEY_RESEND_MSG.equals(mKey)) {
                    processResendMsgE2E(receivedMessage);
                }

                //TODO không dùng config này nữa
                /*else if (Constants.MESSAGE.CONFIG.KEY_LISTGAME.equals(mKey)) {
                        mApplication.getGameConfigBusiness().getListGameFromServer(true);
                } else if (Constants.MESSAGE.CONFIG.KEY_ENABLE_CALLOUT.equals(mKey)) {
                    mApplication.getCallBusiness().setEnableCallOut(true);
                } else if (Constants.MESSAGE.CONFIG.KEY_DISABLE_CALLOUT.equals(mKey)) {
                    mApplication.getCallBusiness().setEnableCallOut(false);
                }*/
            }
        }
    }

    private void processBackUpMessage() {
//        BackupManager.doBackup(null, 1);
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        ThreadMessage thread = messageBusiness.findExistingOrCreateOfficialThread();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ReengMessage reengMessage = messageBusiness.createFakeMessageDeepLink(thread.getId(),
                thread.getServerId(), myNumber, ApplicationController.self().getResources().getString(R.string.content_oa_sync_desktop),
                ApplicationController.self().getResources().getString(R.string.oa_start),
                "mocha://backup?syncdesktop=1",
                "",
                "");
        messageBusiness.notifyReengMessage(mApplication, thread, reengMessage, ThreadMessageConstant
                .TYPE_THREAD_OFFICER_CHAT);
    }

    private void processGetUserInfo() {
        final String lastAvatar = mApplication.getReengAccountBusiness().getLastChangeAvatar();
        ProfileRequestHelper.getInstance(mApplication).getUserInfo(mApplication.getReengAccountBusiness().getCurrentAccount(), new ProfileRequestHelper.onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
                if (!TextUtils.isEmpty(lastAvatar) && !lastAvatar.equals(account.getLastChangeAvatar())) {
                    mApplication.getReengAccountBusiness().getCurrentAccount().setAvatarPath("");
                    GroupAvatarHelper.getInstance(mApplication).clearBitmapCache();
                    CopyOnWriteArrayList<ThreadMessage> list = mApplication.getMessageBusiness().getThreadMessageArrayList();
                    for (ThreadMessage threadMessage : list) {
                        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                            threadMessage.setForceCalculatorAllMember(true);
                        }
                    }
                }
            }

            @Override
            public void onError(int errorCode) {

            }
        });
    }

    private void processGetListBlock() {
        ContactRequestHelper.getInstance(mApplication).getBlockListV5();
    }

    private void processResendMsgE2E(ReengMessagePacket receivedMessage) {
        //Cap nhat lai prekey
        mContactBusiness.updateE2EContact(receivedMessage.getFrom(), receivedMessage.getPreKey());

        //Lay danh sach cac message de gui lai
        String ids = receivedMessage.getIds();
        ReengMessage correctMsg = findMessageInMemAndDBByPacketId(ids, null);
        if (correctMsg != null) {
            try {
                int threadId = correctMsg.getThreadId();
                ThreadMessage threadMessage = getThreadById(threadId);
                if (threadMessage != null) {
                    ReengMessage newMessage = correctMsg.clone();
                    newMessage.setTargetPacketIdE2E(ids);
                    String packetId = PacketMessageId.getInstance().genMessagePacketId(
                            threadMessage.getThreadType(), newMessage.getMessageType().toString());
                    newMessage.setPacketId(packetId);
                    newMessage.setId(0);
                    //TODO add retry send msg resend
                    mApplication.getXmppManager().sendReengMessage(newMessage, threadMessage);
                }

            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }

    }

    public void processChangeNumberAllThread(String oldUser, String newUser
            , ChangeNumberActivity.ChangeNumberMemberListener listener) {
        //TODO processChangeNumberFriend
        //disconnect cho chắc ko bị các luồng thay đổi dữ liệu adapter
        mApplication.getXmppManager().manualDisconnect();
        ArrayList<ThreadMessage> allThreadMessage = getAllThreadMessages(false, true);
        ArrayList<ThreadMessage> listChange = new ArrayList<>();
        for (ThreadMessage threadMessage : allThreadMessage) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                boolean update = false;
                if (threadMessage.getPhoneNumbers() != null && threadMessage.getPhoneNumbers().contains(oldUser)) {
                    threadMessage.getPhoneNumbers().remove(oldUser);
                    threadMessage.getPhoneNumbers().add(newUser);
                    update = true;
                }
                if (threadMessage.getAdminNumbers() != null && threadMessage.getAdminNumbers().contains(oldUser)) {
                    threadMessage.getAdminNumbers().remove(oldUser);
                    threadMessage.getAdminNumbers().add(newUser);
                    update = true;
                }
                if (update) {
                    threadMessage.setSoloNumberAndNumberSearchGroup();
                    listChange.add(threadMessage);
                }
            }
        }
        updateMultiThreadMessages(listChange, listener);

    }

    private void updateMultiThreadMessages(ArrayList<ThreadMessage> listChange, ChangeNumberActivity.ChangeNumberMemberListener listener) {
        //TODO updateThreadMessages
        UpdateMultiThreadMessageAsyncTask updateMultiThread = new UpdateMultiThreadMessageAsyncTask(listChange, listener);
        updateMultiThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void handlerUpdateContact() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (!mContactBusiness.isContactReady()) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "InterruptedException", e);
                    }
                }
                mContactBusiness.initArrayListPhoneNumber();
                ContactRequestHelper.getInstance(mApplication).setInfoAllPhoneNumber(new ContactRequestHelper
                        .onResponseSetContactListener() {
                    @Override
                    public void onResponse(ArrayList<PhoneNumber> responses) {
                        mContactBusiness.updateContactInfo(responses);
                        ArrayList<PhoneNumber> listPhone = mContactBusiness.getListNumberAlls();
                        /*if (listPhone != null && !listPhone.isEmpty()) {
                            Log.i(TAG, "update list phonumber: " + listPhone.size());
                            ContactSyncHelper contactSyncHelper = new ContactSyncHelper(mApplication);
                            contactSyncHelper.setListPhone(listPhone);
                            contactSyncHelper.start();
                        }*/
                    }

                    @Override
                    public void onError(int errorCode) {

                    }
                });
            }
        };
        thread.start();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void markAllMessageIsOld(ThreadMessage mThreadMessage, int mThreadId) {
        new UpdateNewMessageTask(mThreadId, mThreadMessage).
                executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class UpdateNewMessageTask extends AsyncTask<Void, Void, Void> {
        int mThreadId;
        ThreadMessage mThreadMessage;

        public UpdateNewMessageTask(int threadId, ThreadMessage threadMessage) {
            this.mThreadId = threadId;
            this.mThreadMessage = threadMessage;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i(TAG, "UpdateUnreadMessageTask doInBackground");
            mThreadMessage.setHasNewMessage(false);
            CopyOnWriteArrayList<ReengMessage> listMessage = mThreadMessage.getAllMessages();
            for (ReengMessage reengMessage : listMessage) {
                if (reengMessage.getDirection().equals(ReengMessageConstant.Direction.received)) {
                    boolean isNewMessage = reengMessage.isNewMessage();
                    if (isNewMessage) {
                        reengMessage.setNewMessage(false);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            refreshThreadWithoutNewMessage(mThreadId);
        }
    }

    /**
     * tinh nang dich
     */
    public void translateMessageGoogle(final ReengMessage message, final String languageCode, int threadType) {
        String content = message.getContent();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GG_TRANSLATE);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(mAccountBusiness.getJidNumber()).append(languageCode).append(content)
                .append(mAccountBusiness.getToken()).append(timeStamp);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", mAccountBusiness.getJidNumber());
        params.addParam("target", languageCode);
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY,
                HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken()));
        params.addParam("q", content);

//        final String smartLinkFinal = smartLink;
        Log.d(TAG, "url: " + params.toString());
        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), new Response
                .Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "response : " + response);
                try {
                    JSONObject object = new JSONObject(response);
                    //int code = -1;
                    if (object.has("data")) {
                        JSONObject dataObject = object.getJSONObject("data");
                        if (dataObject.has("translations")) {
                            JSONArray arrayTranslate = dataObject.getJSONArray("translations");
                            if (arrayTranslate != null && arrayTranslate.length() > 0) {
                                JSONObject obj = arrayTranslate.getJSONObject(0);
                                if (obj.has("translatedText")) {
                                    String newContent = obj.getString("translatedText");
                                    newContent = TextHelper.fromHtml(newContent).toString();
                                    /*if (!TextUtils.isEmpty(smartLinkFinal)) {
                                        newContent = newContent + "\n" + smartLinkFinal;
                                    }*/
                                    Log.d(TAG, "newContent: " + newContent);
                                    processResponseTranslate(message, newContent, languageCode);
                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    Log.e(TAG, e);
                    processResponseTranslate(message, null, null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, "volleyError : ", volleyError);
                processResponseTranslate(message, null, null);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    private void processResponseTranslate(ReengMessage message, String newContent, String languageCode) {
        if (!TextUtils.isEmpty(newContent)) {
            message.setTextTranslated(newContent);
            message.setShowTranslate(true);
            message.setLanguageTarget(languageCode);
            refreshThreadWithoutNewMessage(message.getThreadId());
        } else {
            Log.i(TAG, "error processResponseTranslate");
        }
    }

    public boolean checkShowAlertStranger(ThreadMessage threadMessage) {
        if (threadMessage == null) {
            return false;
        } else if (threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            return false;
        } else if (!threadMessage.isStranger()) {
            return false;
        } else {// 1-1
            String friendJid = threadMessage.getSoloNumber();
            if (strangerNumberShowAlerts == null) {
                getStrangerNumberShowAlertFromPref();
            }
            if (strangerNumberShowAlerts.contains(friendJid)) {
                return false;
            } else {
                strangerNumberShowAlerts.add(friendJid);
                saveStrangerNumberShowAlertToPref();
                return true;
            }
        }
    }

    private void getStrangerNumberShowAlertFromPref() {
        String temp = mPref.getString(Constants.PREFERENCE.PREF_STRANGER_SHOW_ALERTS, "");
        ArrayList<String> numbersList = new ArrayList<>();
        if (!TextUtils.isEmpty(temp)) {
            String[] numbersArray = temp.split(",");
            for (String item : numbersArray) {
                if (item.length() > 0) {
                    numbersList.add(item);
                }
            }
        }
        strangerNumberShowAlerts = numbersList;
    }

    private void saveStrangerNumberShowAlertToPref() {
        String temp = "";
        if (strangerNumberShowAlerts != null && strangerNumberShowAlerts.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String number : strangerNumberShowAlerts) {
                sb.append(number).append(",");
            }
            int lengText = sb.length();
            sb.deleteCharAt(lengText - 1);
            temp = sb.toString();
        }
        mPref.edit().putString(Constants.PREFERENCE.PREF_STRANGER_SHOW_ALERTS, temp).apply();
    }

    public void updateJoinThreadStranger(ThreadMessage threadMessage) {
        threadMessage.setJoined(true);
        mThreadMessageDataSource.updateThreadMessage(threadMessage);
    }

    public void createAndSendMessageWatchVideo(ThreadMessage threadMessage, MediaModel mediaModel
            , BaseSlidingFragmentActivity activity) {
        if (threadMessage == null || mediaModel == null) {
            activity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        int mThreadType = threadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT ||
                mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            return;// officer va broadcast khong cho gui cung xem
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);

        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ReengMessage message = new ReengMessage(threadMessage.getThreadType(), ReengMessageConstant.MessageType
                .watch_video);
        message.setSender(myNumber);
        message.setMessageType(ReengMessageConstant.MessageType.watch_video);
        message.setReceiver(receiver);
        message.setThreadId(threadMessage.getId());
        message.setImageUrl(mediaModel.getImage());
        message.setSongId(ReengMessage.SONG_ID_DEFAULT_NEW);
        message.setReadState(ReengMessageConstant.READ_STATE_READ);
        message.setStatus(ReengMessageConstant.STATUS_LOADING);
        message.setSongModel(mediaModel);
        message.setDirection(ReengMessageConstant.Direction.send);
        Date date = new Date();
        message.setTime(date.getTime());

        String content = String.format(mRes.getString(R.string.invite_watch_video_send),
                threadMessage.getThreadName());
        String contentBold = String.format(mRes.getString(R.string.invite_watch_video_send),
                TextHelper.textBoldWithHTML(threadMessage.getThreadName()));
        message.setContent(content);
        message.setFileName(contentBold);
        String info;
        if (TextUtils.isEmpty(mediaModel.getSinger())) {
            info = mediaModel.getName();
        } else {
            info = mediaModel.getName() + " - " + mediaModel.getSinger();
        }
        message.setFilePath(info);
        if (insertNewMessageBeforeSend(threadMessage, mThreadType, message)) {
            mApplication.getXmppManager().sendWatchVideoMessage(message, mediaModel, threadMessage);
        }
    }

    public ReengMessage isAutoPlayMessageVideoFirstTime(CopyOnWriteArrayList<ReengMessage> messages
            , int first, int last) {
        int messageSize = messages.size();
        Log.d(TAG, "isAutoPlayMessageVideoFirstTime : " + messageSize + " -- " + first + " -- " + last);
        // index da được trừ đi header và footer listview, check thêm dk này tránh index < 0
        if (last < 0) last = 0;
        if (first < 0) first = 0;
        if (last >= messageSize) {
            last--;
        }
        if (last >= first) {
            for (int i = first; i <= last && i < messageSize; i++) {
                ReengMessage message = messages.get(i);
                if (isAutoPlayMessageVideo(message)) {
                    updateAllFieldsOfMessage(message);
                    return message;
                }
            }
        }
        return null;
    }

    public boolean isAutoPlayMessageVideo(ReengMessage message) {
        if (message.getMessageType() == ReengMessageConstant.MessageType.watch_video) {
            MediaModel mediaModel = new Gson().fromJson(message.getDirectLinkMedia(), MediaModel.class);
            if (mediaModel == null) {
                return false;
            } else {
                if (mediaModel.getAutoplayVideo() == 1) {// message cùng xem video có auto
                    mediaModel.setAutoplayVideo(0);
                    message.setSongModel(mediaModel);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean notifyWhenGroupNoExist(Activity activity, ThreadMessage threadMessage) {
        // neu nhom ko ton tai tren sv thi thong bao luon
        if (threadMessage == null) {
            return true;
        } else if (activity != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT
                && !threadMessage.isJoined()) {
            Toast.makeText(ApplicationController.self(), mRes.getString(R.string.e416_not_allow_invite), Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    public void markReadMessage(CopyOnWriteArrayList<ThreadMessage> threadMessages) {
        long t = System.currentTimeMillis();
        for (ThreadMessage thread : threadMessages) {
            if (thread.getNumOfUnreadMessage() > 0 && thread.getThreadType() != ThreadMessageConstant
                    .TYPE_THREAD_ROOM_CHAT) {
                Log.d(TAG, "markReadMessage thread: " + thread.getThreadName() + " - " + thread.getNumOfUnreadMessage
                        ());
                MessageNotificationManager.getInstance(mApplication).clearNotifyThreadMessage(thread);
                for (ReengMessage message : thread.getAllMessages()) {
                    if (message.getDirection() == ReengMessageConstant.Direction.received)
                        message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                }
                mReengMessageDataSource.markAllMessageIsReadState(thread.getId(),
                        ReengMessageConstant.READ_STATE_SENT_SEEN, thread.getNumOfUnreadMessage());
                /*mReengMessageDataSource.markAllMessageIsReadState(thread.getId(),
                        ReengMessageConstant.READ_STATE_SENT_SEEN);*/
                thread.setNumOfUnreadMessage(0);
                mThreadMessageDataSource.updateThreadMessage(thread);
            }
        }
        Log.d(TAG, "markReadMessage take: " + (System.currentTimeMillis() - t));
    }

    public boolean checkAndStartChatUnknownNumber(final BaseSlidingFragmentActivity activity, String number) {
        String region = mApplication.getReengAccountBusiness().getRegionCode();
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(mApplication.getPhoneUtil(), number, region);
        if (phoneNumberProtocol == null) {
            activity.showToast(mRes.getString(R.string.msg_not_phone_number), Toast.LENGTH_SHORT);
            return false;
        } else {
            final String jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (Utilities.notEmpty(jidNumber) && jidNumber.equals(myNumber)) {
                activity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
                return false;
            } else if (!PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(),
                    phoneNumberProtocol)) {
                activity.showToast(mRes.getString(R.string.msg_not_phone_number), Toast.LENGTH_SHORT);
                return false;
            } else {
                ArrayList<String> listNumbers = new ArrayList<>();
                listNumbers.add(jidNumber);
                final String rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mApplication, jidNumber);
                activity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                ContactRequestHelper.getInstance(mApplication).getInfoContactsFromNumbers(listNumbers, new
                        ContactRequestHelper.onResponseInfoContactsListener() {
                            @Override
                            public void onResponse(ArrayList<PhoneNumber> responses) {
                                activity.hideLoadingDialog();
                                ContactBusiness contactBusiness = mApplication.getContactBusiness();
                                boolean isViettel = mApplication.getReengAccountBusiness().isViettel();
                                if (responses != null && !responses.isEmpty()) {// so dung mocha
                                    PhoneNumber phoneNumber = responses.get(0);
                                    //  cap nhat danh sach nonContact
                                    contactBusiness.insertOrUpdateNonContact(phoneNumber, true, true);
                                    // TODO bỏ check so ko phải viettel de invite
                                    NavigateActivityHelper.navigateToChatDetail(activity,
                                            findExistingOrCreateNewThread(jidNumber));
                                    /*if ((phoneNumber != null && phoneNumber.getState() == Constants.CONTACT
                                            .ACTIVE) ||
                                            (isViettel && PhoneNumberHelper.getInstant().isViettelNumber
                                                    (jidNumber))) {
                                        NavigateActivityHelper.navigateToChatDetail(activity,
                                                findExistingOrCreateNewThread(jidNumber));
                                    } else {
                                        InviteFriendHelper.getInstance().showRecommendInviteFriendPopup
                                                (mApplication,
                                                        activity,
                                                        rawNumber, jidNumber, false);
                                    }*/

                                } else {
                                    // TODO bỏ check so ko phải viettel de invite
                                    PhoneNumber phoneNumber = new PhoneNumber();
                                    phoneNumber.setJidNumber(jidNumber);
                                    phoneNumber.setState(Constants.CONTACT.NONE);
                                    contactBusiness.insertOrUpdateNonContact(phoneNumber, true);
                                    ThreadMessage thread = findExistingOrCreateNewThread(jidNumber);
                                    thread.setIsReeng(false);
                                    NavigateActivityHelper.navigateToChatDetail(activity, thread);
                                    /*if (AVNOHelper.AVNO) {
                                        PhoneNumber phoneNumber = new PhoneNumber();
                                        phoneNumber.setJidNumber(jidNumber);
                                        phoneNumber.setState(Constants.CONTACT.NONE);
                                        contactBusiness.insertOrUpdateNonContact(phoneNumber, true);
                                        ThreadMessage thread = findExistingOrCreateNewThread(jidNumber);
                                        thread.setIsReeng(false);
                                        NavigateActivityHelper.navigateToChatDetail(activity, thread);
                                    } else {
                                        if (isViettel && PhoneNumberHelper.getInstant().isViettelNumber(jidNumber)) {
                                            PhoneNumber phoneNumber = new PhoneNumber();
                                            phoneNumber.setJidNumber(jidNumber);
                                            phoneNumber.setState(Constants.CONTACT.NONE);
                                            contactBusiness.insertOrUpdateNonContact(phoneNumber, true);
                                            ThreadMessage thread = findExistingOrCreateNewThread(jidNumber);
                                            thread.setIsReeng(false);
                                            NavigateActivityHelper.navigateToChatDetail(activity, thread);
                                        } else {
                                            InviteFriendHelper.getInstance().showRecommendInviteFriendPopup
                                                    (mApplication,
                                                            activity,
                                                            rawNumber, jidNumber, false);
                                        }
                                    }*/

                                }
                            }

                            @Override
                            public void onError(int errorCode) {
                                activity.hideLoadingDialog();
                                activity.showToast(mRes.getString(R.string.e500_internal_server_error), Toast
                                        .LENGTH_SHORT);
                            }
                        });
                return true;
            }
        }
    }

    public void checkAndStartForwardUnknownNumber(final BaseSlidingFragmentActivity activity
            , String number, final ReengMessage message) {
        String region = mApplication.getReengAccountBusiness().getRegionCode();
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(mApplication.getPhoneUtil(), number, region);
        if (phoneNumberProtocol == null) {
            activity.showToast(mRes.getString(R.string.msg_not_phone_number), Toast.LENGTH_SHORT);
        } else {
            final String jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (jidNumber != null && myNumber.equals(jidNumber)) {
                activity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
            } else if (!PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(),
                    phoneNumberProtocol)) {
                activity.showToast(mRes.getString(R.string.msg_not_phone_number), Toast.LENGTH_SHORT);
            } else {
                ArrayList<String> listNumbers = new ArrayList<>();
                listNumbers.add(jidNumber);
                final String rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mApplication, jidNumber);
                activity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                ContactRequestHelper.getInstance(mApplication).getInfoContactsFromNumbers(listNumbers, new
                        ContactRequestHelper.onResponseInfoContactsListener() {
                            @Override
                            public void onResponse(ArrayList<PhoneNumber> responses) {
                                activity.hideLoadingDialog();
                                ContactBusiness contactBusiness = mApplication.getContactBusiness();
                                boolean isViettel = mApplication.getReengAccountBusiness().isViettel();
                                if (responses != null && !responses.isEmpty()) {// so dung mocha
                                    PhoneNumber phoneNumber = responses.get(0);
                                    //  cap nhat danh sach nonContact
                                    contactBusiness.insertOrUpdateNonContact(phoneNumber, true, true);
                                    // TODO bỏ check so ko phải viettel de invite
                                    forwardMessageToThread(activity, findExistingOrCreateNewThread(jidNumber), message);
                                    /*NavigateActivityHelper.navigateToChatDetail(activity,
                                            findExistingOrCreateNewThread(jidNumber));*/

                                } else {
                                    // TODO bỏ check so ko phải viettel de invite
                                    PhoneNumber phoneNumber = new PhoneNumber();
                                    phoneNumber.setJidNumber(jidNumber);
                                    phoneNumber.setState(Constants.CONTACT.NONE);
                                    contactBusiness.insertOrUpdateNonContact(phoneNumber, true);
                                    ThreadMessage thread = findExistingOrCreateNewThread(jidNumber);
                                    thread.setIsReeng(false);
                                    forwardMessageToThread(activity, thread, message);
                                    /*NavigateActivityHelper.navigateToChatDetail(activity, thread);*/
                                }
                            }

                            @Override
                            public void onError(int errorCode) {
                                activity.hideLoadingDialog();
                                activity.showToast(mRes.getString(R.string.e500_internal_server_error), Toast
                                        .LENGTH_SHORT);
                            }
                        });
            }
        }
    }

    public void forwardMessageToThread(BaseSlidingFragmentActivity activity, ThreadMessage thread, ReengMessage message) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_MESSAGE, thread);
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, message);
        activity.setResult(Activity.RESULT_OK, returnIntent);
        activity.finish();
    }

    public void checkAndInsertMessageBannerLixi(CopyOnWriteArrayList<ReengMessage> listMsg, ThreadMessage threadMessage) {
        Log.d(TAG, "checkAndInsertMessageBannerLixi");
        if (threadMessage == null
                || threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT)
            return;

        CopyOnWriteArrayList<ReengMessage> allMessages = threadMessage.getAllMessages();
        if (allMessages != null && !allMessages.isEmpty()) {
            ReengMessage lastMessage = allMessages.get(allMessages.size() - 1);
            if (lastMessage.getMessageType() == ReengMessageConstant.MessageType.message_banner &&
                    "lixi".equals(lastMessage.getFilePath()))
                return;
        }
        if (BankPlusHelper.getInstance(mApplication).isShowLixi(threadMessage)) {
            if (hasShowBannerInDay(threadMessage)) return;
            String content = mRes.getString(R.string.last_msg_lixi);
            String receiver = mApplication.getReengAccountBusiness().getJidNumber();
            String sender;
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                sender = threadMessage.getSoloNumber();
            } else {
                sender = threadMessage.getServerId();
            }
            EmoticonUtils.createCacheSpanned(mApplication, content);
            ReengMessage message = new ReengMessage();// msg ko gui di nen ko quan trong paketId
            message.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
            message.setReceiver(receiver);
            message.setSender(sender);
            message.setFilePath("lixi");
            message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
            message.setThreadId(threadMessage.getId());
            message.setDirection(ReengMessageConstant.Direction.received);
            message.setTime(TimeHelper.getCurrentTime());
            message.setStatus(ReengMessageConstant.STATUS_RECEIVED);
            message.setMessageType(ReengMessageConstant.MessageType.message_banner);
            message.setContent(content);

//            deleteLastLixiMessage(listMsg, threadMessage);
            //insert message vao thread trong database
            this.insertNewMessageToDB(threadMessage, message);
            Log.d(TAG, "checkAndInsertMessageBannerLixi insert DB");
            mApplication.getMessageBusiness().notifyNewMessage(message, threadMessage);
            try {
                saveTimeShowBannerLixi(threadMessage);
            } catch (Exception e) {
                Log.e(TAG, e);
            }

        }
    }

    /*mMessageArrayList.remove(message);
                mMessageBusiness.deleteAMessage(mThreadMessage, message);*/
    private void deleteLastLixiMessage(CopyOnWriteArrayList<ReengMessage> listMsg, ThreadMessage threadMessage) {
        if (listMsg != null && threadMessage != null) {
            ReengMessage lastMsgBannerLixi = mReengMessageDataSource.getLastMessageBannerLixiByThreadId(threadMessage.getId());
            if (lastMsgBannerLixi != null) {
                deleteAMessage(threadMessage, lastMsgBannerLixi);
                if (listMsg.size() <= 0) return;
                for (int i = listMsg.size() - 1; i >= 0; i--) {
                    ReengMessage msg = listMsg.get(i);
                    if (msg.getId() == lastMsgBannerLixi.getId()) {
                        listMsg.remove(msg);
                        refreshThreadWithoutNewMessage(threadMessage.getId());
                        return;
                    }
                }
            }
        }
    }

    private boolean hasShowBannerInDay(ThreadMessage threadMessage) {
        if (timeShowBannerLixi.isEmpty()) return false;
        if (timeShowBannerLixi.containsKey(threadMessage.getId())) {
            long lastTimeShow = timeShowBannerLixi.get(threadMessage.getId());
            return TimeHelper.checkTimeInDay(lastTimeShow);
        }
        return false;
    }

    private void saveTimeShowBannerLixi(ThreadMessage threadMessage) throws JSONException {
        timeShowBannerLixi.put(threadMessage.getId(), System.currentTimeMillis());
        JSONArray jsonArray = new JSONArray();
        for (Map.Entry<Integer, Long> entry : timeShowBannerLixi.entrySet()) {
            int key = entry.getKey();
            long value = entry.getValue();
            JSONObject js = new JSONObject();
            js.put("id", key);
            js.put("time", value);
            jsonArray.put(js);
        }
        mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_TIME_SHOW_BANNER_LIXI, jsonArray.toString()).apply();
    }

    private void initListTimeShowBannerLixi() throws JSONException {

        String data = mApplication.getPref().getString(Constants.PREFERENCE.PREF_TIME_SHOW_BANNER_LIXI, "");
        if (!TextUtils.isEmpty(data)) {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject != null) {
                    int id = jsonObject.optInt("id", -1);
                    long time = jsonObject.optLong("time", 0);
                    if (id != -1) {
                        timeShowBannerLixi.put(id, time);
                    }
                }
            }
        }
    }

    public void sendPinMessage(ThreadMessage threadMessage, ReengMessage oldMsg
            , BaseSlidingFragmentActivity activity, boolean unPin) {
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        String sender = mAccountBusiness.getJidNumber();
        ReengMessage pinReengMsg = new ReengMessage();// msg ko gui di nen ko quan trong paketId
        pinReengMsg.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
        pinReengMsg.setReceiver(receiver);
        pinReengMsg.setSender(sender);
        pinReengMsg.setReadState(ReengMessageConstant.READ_STATE_READ);
        pinReengMsg.setStatus(ReengMessageConstant.STATUS_LOADING);
        pinReengMsg.setMessageType(ReengMessageConstant.MessageType.pin_message);
        pinReengMsg.setDirection(ReengMessageConstant.Direction.send);
        Date date = new Date();
        pinReengMsg.setTime(date.getTime());

        if (!unPin) {
            pinReengMsg.setContent(oldMsg.getContent());
            pinReengMsg.setSize(PinMessage.ActionPin.ACTION_PIN.VALUE);
            pinReengMsg.setSongId(PinMessage.TypePin.TYPE_MESSAGE.VALUE);
            pinReengMsg.setFileName(oldMsg.getSender());
            pinReengMsg.setFilePath(oldMsg.getPacketId());

            PinMessage pin = new PinMessage();
            pin.setContent(oldMsg.getContent());
            pin.setTarget(oldMsg.getPacketId());
            pin.setTitle(oldMsg.getSender());
            pin.setType(PinMessage.TypePin.TYPE_MESSAGE.VALUE);
            threadMessage.setPinMessage(pin);
        } else {
            pinReengMsg.setSize(PinMessage.ActionPin.ACTION_UNPIN.VALUE);
            pinReengMsg.setSongId(PinMessage.TypePin.TYPE_MESSAGE.VALUE);
            threadMessage.setPinMessage("");
        }
        //insert message
        insertNewMessageBeforeSend(threadMessage, threadMessage.getThreadType(), pinReengMsg);
        Log.d(TAG, "sendPinMessage insert DB");
        sendXMPPMessage(pinReengMsg, threadMessage);

        updateThreadMessage(threadMessage);
        refreshThreadWithoutNewMessage(threadMessage.getId());
    }

    public ReengMessage findLastPinMessage(ThreadMessage thread) {
        if (thread == null) {
            return null;
        }
        return mReengMessageDataSource.getLastPinMessageOfThread(String.valueOf(thread.getId()));
    }

    public boolean isMessageBannerDeepLink(ReengMessage message) {
        return message.getMessageType() == ReengMessageConstant.MessageType.pin_message && message.getSongId() ==
                PinMessage.TypePin.TYPE_DEEPLINK.VALUE;
    }

    public void processUnknownMessage(final MessageInteractionListener.LoadUnknownMessageAndReloadAllThread listener) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                ArrayList<ReengMessage> listMessage = mReengMessageDataSource.getListMessageUnknown();
                if (listMessage != null && !listMessage.isEmpty()) {
                    if (listener != null) listener.onStartLoadData();
                    for (ReengMessage message : listMessage) {
                        try {
                            XmlPullParserFactory factory = XmlPullParserFactory
                                    .newInstance();
                            factory.setNamespaceAware(true);
                            XmlPullParser parser = factory.newPullParser();
                            parser.setInput(new StringReader(message.getFilePath()));
                            ReengMessagePacket packet = PacketParserUtils.parseReengMessageUpdate(parser);

                            ReengMessagePacket.SubType subType = packet.getSubType();
                            ReengMessage reengMessage = getIncomingMessageProcessor().mapPacketToReengMessage(packet,
                                    message.getThreadId(), message.getSender(), myNumber, subType);
                            reengMessage.setId(message.getId());

                            updateAllFieldsOfMessage(reengMessage);
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                    loadAllThreadMessageOnFirstTime(listener);
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public ArrayList<ThreadMessage> getAllThreadMessages(boolean needSort, boolean getAllThreadGroup) {
        long beginTime = System.currentTimeMillis();
        try {
            ArrayList<ThreadMessage> listThreads = new ArrayList<>();
            ArrayList<ThreadMessage> listPins = new ArrayList<>();
            if (getThreadMessageArrayList() == null ||
                    getThreadMessageArrayList().isEmpty()) {
                return listThreads;
            }
            for (ThreadMessage threadMessage : getThreadMessageArrayList()) {
                if (getAllThreadGroup) {
                    if (threadMessage.isReadyShow()) {
                        listThreads.add(threadMessage);
                    }
                } else if (needSort && threadMessage.getLastTimePinThread() != 0) {
                    listPins.add(threadMessage);
                } else if (threadMessage.isReadyShow(this)) {
                    listThreads.add(threadMessage);
                }
            }
            if (!listThreads.isEmpty() && needSort) {
                Collections.sort(listThreads, ComparatorHelper.getComparatorThreadMessageByLastTime());
            }
            if (!listPins.isEmpty() && needSort) {
                Collections.sort(listPins, ComparatorHelper.getComparatorThreadMessageByLastTimePin());
                listThreads.addAll(0, listPins);
            }
            Log.i(TAG, "[] getAndSortThreadMessages take " + (System.currentTimeMillis() - beginTime) + " ms");
            return listThreads;
        } catch (Exception e) {// ArrayIndexOutOfBoundsException
            Log.e(TAG, "Exception", e);
        }
        return new ArrayList<>();
    }

    public class GetGroupInfoAsynctask extends AsyncTask<ThreadMessage, Void, Void> {
        @Override
        public Void doInBackground(ThreadMessage... threadMessages) {
            getInfoGroup(threadMessages[0]);
            return null;
        }

    }

    //==============================message image luu DB=====================
    private long TIME_EXPERID = 20 * 24 * 60 * 60;
    private HashMap<String, MessageImageDB> messageImageDB = new HashMap<>();

    private void initListMessageImageDB() {
        messageImageDB = mMessageImageDataSource.getAllMessageImageUpload();
    }

    public void addMessageImageDB(ReengMessage msg, String oriFilePath) {
        new AsyncTask<Object, Void, Void>() {

            @Override
            protected Void doInBackground(Object... msgs) {
                ReengMessage reengMessage = (ReengMessage) msgs[0];
                String filePath = (String) msgs[1];
                String digit = MD5.calculateMD5(new File(filePath));
                MessageImageDB msgImage = new MessageImageDB(filePath,
                        reengMessage.getFilePathCompress(), reengMessage.getDataResponseUpload(),
                        System.currentTimeMillis(), reengMessage.getVideoContentUri(), digit);
                long id = mMessageImageDataSource.insertMessageImageUpload(msgImage);
                msgImage.setId(id);
                messageImageDB.put(filePath, msgImage);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, msg, oriFilePath);

    }

    public MessageImageDB getMessageImageDB(String pathOri) {
        if (messageImageDB == null || messageImageDB.size() == 0 || TextUtils.isEmpty(pathOri))
            return null;
        if (messageImageDB.containsKey(pathOri)) {
            MessageImageDB msgDB = messageImageDB.get(pathOri);
            if (!MD5.checkMD5(msgDB.getDigit(), new File(pathOri))) {
                Log.f(TAG, "!MD5.checkMD5");
                mMessageImageDataSource.deleteMessageImage(msgDB.getId());
                messageImageDB.remove(pathOri);
                return null;
            }

            if ((System.currentTimeMillis() - msgDB.getTimeUpload()) >= TIME_EXPERID * 1000) {     // file upload đã quá lâu
                mMessageImageDataSource.deleteMessageImage(msgDB.getId());
                messageImageDB.remove(pathOri);
                return null;
            }
            if (TextUtils.isEmpty(msgDB.getPathNew())) return null;
            final File fileImage = new File(msgDB.getPathNew());
            if (fileImage == null || !fileImage.exists()) {         //file compress đã bị xóa
                mMessageImageDataSource.deleteMessageImage(msgDB.getId());
                messageImageDB.remove(pathOri);
                return null;
            }
            return messageImageDB.get(pathOri);
        }
        return null;
    }

    //get sender
    public void updateAllMessageOldPrefixNumber(final ThreadMessage threadMessage
            , final HashMap<String, String> hashMap
            , final PrefixChangeNumberHelper.UpdateThreadChat updateThreadChat) {
        /*ArrayList<ReengMessage> listMsgUpdate = mReengMessageDataSource.getListMessageChangeNumber(
                threadMessage.getId(), oldNumb, newNumb);*/
        new AsyncTask<Void, Void, Integer>() {

            @Override
            protected Integer doInBackground(Void... voids) {
                Log.i(TAG, "-------PREFIX-----updateAllMessageOldPrefixNumber");
                try {
                    CopyOnWriteArrayList<ReengMessage> listMsg = threadMessage.getAllMessages();
                    ArrayList<ReengMessage> listUpdate = new ArrayList<>();
                    CopyOnWriteArrayList<ReengMessage> listAllMsg = new CopyOnWriteArrayList<>();
                    for (ReengMessage message : listMsg) {
                        String sender = message.getSender();
                        if (hashMap.containsKey(sender)) {
                            message.setSender(hashMap.get(sender));
                            listUpdate.add(message);
                        }
                        listAllMsg.add(message);
                    }
                    if (!listUpdate.isEmpty()) {
                        updateAllFieldsOfListMessage(listUpdate);
//                        refreshView = 1;
                    }
                    //update cac tin nhan con lai trong db
                    for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();
                        int update = updateListMessageChangeNumberDB(threadMessage, key, value);
                        Log.i(TAG, "-------PREFIX-----update: " + threadMessage.getId() + " old: " + key + " new: " + value + " result: " + update);
                    }
                    threadMessage.setAllMessages(listAllMsg);
                } catch (Exception ex) {
                    Log.e(TAG, "-------PREFIX-----ex: " + ex.toString());

                }
                return 1;
            }

            @Override
            protected void onPostExecute(Integer integer) {
                super.onPostExecute(integer);
                Log.i(TAG, "-------PREFIX-----onPostExecute: " + integer);
                if (updateThreadChat != null)
                    updateThreadChat.onUpdateThreadChat(threadMessage.getId());
//                    refreshThreadWithoutNewMessage(threadMessage.getId());
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public int updateListMessageChangeNumberDB(ThreadMessage threadMessage, String oldNumb, String newNumb) {
        return mReengMessageDataSource.updateListMessageChangeNumber(threadMessage.getId(), oldNumb, newNumb);
    }

    public int updateListMessageChangeNumberDB(String oldNumb, String newNumb) {
        return mReengMessageDataSource.updateListMessageChangeNumber(oldNumb, newNumb);
    }

    public void addAllThreadMessageToMemory(ArrayList<ThreadMessage> threadMessages) {
        mThreadMessageArrayList.addAll(threadMessages);
    }

    public void addThreadMessageToMemory(ThreadMessage threadMessages) {
        mThreadMessageArrayList.add(threadMessages);
    }

    public String getPreKeyByJid(String jid) {
        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(jid);
        NonContact mNonContact = mContactBusiness.getExistNonContact(jid);
        if (mPhoneNumber != null) {
            return mPhoneNumber.getPreKey();
        } else if (mNonContact != null) {
            return mNonContact.getPreKey();
        }
        return null;
    }

    //Dùng cho vẽ giao diện
    public String getOperatorFriendByJid(String jid) {
        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(jid);
        NonContact mNonContact = mContactBusiness.getExistNonContact(jid);
        if (mPhoneNumber != null) {
            return mPhoneNumber.getOperator();
        } else if (mNonContact != null) {
            return mNonContact.getOperator();
        }
        return null;
    }

    //Dùng cho gửi bản tin lên sv
    public CPresence getCPresenceByJid(String jid) {
        CPresence cPresence = new CPresence();
        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(jid);
        NonContact mNonContact = mContactBusiness.getExistNonContact(jid);
        if (mPhoneNumber != null) {
            cPresence.setOperatorPresence(mPhoneNumber.getOperatorPresence());
            cPresence.setStatePresence(mPhoneNumber.getStatePresence());
            cPresence.setUsingDesktop(mPhoneNumber.getUsingDesktop());
        } else if (mNonContact != null) {
            cPresence.setOperatorPresence(mNonContact.getOperatorPresence());
            cPresence.setStatePresence(mNonContact.getStatePresence());
            cPresence.setUsingDesktop(mNonContact.getUsingDesktop());
        }
        return cPresence;
    }

    public int getStatePresenceFriend(PhoneNumber mPhoneNumber, NonContact mNonContact) {
        if (mPhoneNumber != null) {
            return mPhoneNumber.getStatePresence();
        } else if (mNonContact != null) {
            return mNonContact.getStatePresence();
        }
        return -1;
    }

    public void pinThreadMessage(ArrayList<ThreadMessage> threadMessages, boolean statePin) {
        for (ThreadMessage thread : threadMessages) {
            if (statePin) {
                thread.setLastTimePinThread(System.currentTimeMillis());
            } else {
                thread.setLastTimePinThread(0);
                thread.setLastPin(false);
            }
            mThreadMessageDataSource.updateThreadMessage(thread);
        }
    }

    public long getMessageNumbers() {
        return mReengMessageDataSource.getMessageNumber();
    }

    public CopyOnWriteArrayList<ReengMessage> getTextMessagesWithinIds(int threadId, int startId, int endId) {
        return mReengMessageDataSource.getTextMessagesWithinIds(threadId, startId, endId);
    }

    public CopyOnWriteArrayList<BackupMessageModel> getTextMessagesWithinIds(ThreadMessage threadMessage
            , int startId, int endId) {
        return mReengMessageDataSource.getTextMessagesWithinIds(threadMessage, startId, endId);
    }

    public void deleteAllMessages() {
        mReengMessageDataSource.deleteAllMessages();
    }

    public void insertRestoredThreadMessage(ThreadMessage threadMessage) {
        mThreadMessageDataSource.insertNewThreadMessage(threadMessage);
    }

    public long insertRestoreMessageList(List<BackupMessageModel> reengMessages, int threadId) {
        return mReengMessageDataSource.insertNewListMessage(reengMessages, threadId);
    }

    public void sendReactionMessage(BaseSlidingFragmentActivity activity, ReengMessage reengMessage
            , ReengMessageConstant.Reaction reaction, ThreadMessage mThreadMessage) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            activity.showToast(R.string.error_internet_disconnect);
            return;
        }

        if (!mApplication.getXmppManager().isAuthenticated()) {
            activity.showToast(R.string.e604_error_connect_server);
            return;
        }
        //tìm event message trong db xem có ko
        EventMessage eventMessage = new EventMessage();
        eventMessage.setMessageId(reengMessage.getId());
        eventMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        eventMessage.setTime(System.currentTimeMillis());
        eventMessage.setPacketId(reengMessage.getPacketId());
        eventMessage.setThreadId(reengMessage.getThreadId());
        eventMessage.setReaction(reaction.VALUE);
        eventMessage.setStatus(EventMessageConstant.EVENT_REACTION_STATE);

        ReengMessageConstant.Reaction reactAdd = null;
        ReengMessageConstant.Reaction reactRemove = null;

        EventMessage eventRemove = getEventMessageReaction(reengMessage.getId(),
                reengMessage.getPacketId(), mApplication.getReengAccountBusiness().getJidNumber());
        ArrayList<Integer> listReact = reengMessage.getListReaction();
        if (eventRemove != null) {
            if (listReact == null || listReact.size() != ReengMessageConstant.Reaction.DEFAULT.ordinal())
                listReact = createArrayListReact();
            int indexRemove = eventRemove.getReaction();
            if (indexRemove < ReengMessageConstant.Reaction.DEFAULT.ordinal()) {
                int newValue = listReact.get(indexRemove) - 1;
                if (newValue < 0) newValue = 0;
                listReact.set(indexRemove, newValue);
            }
            mEventMessageDataSource.deleteEventMessage(eventRemove);
            if (eventRemove.getReaction() == reaction.VALUE)
                reactRemove = reaction;
        }

        if (reactRemove == null) {
            if (listReact == null || listReact.size() != ReengMessageConstant.Reaction.DEFAULT.ordinal())
                listReact = createArrayListReact();
            int indexAdd = reaction.ordinal();
            if (indexAdd < ReengMessageConstant.Reaction.DEFAULT.ordinal()) {
                int newValue = listReact.get(indexAdd) + 1;
                listReact.set(indexAdd, newValue);
            }
            reactAdd = reaction;
            mEventMessageDataSource.insertEventMessage(eventMessage);
        }

        reengMessage.setListReaction(listReact);
        if (reactRemove != null) {
            reengMessage.setStateMyReaction(-1);
        }
        if (reactAdd != null)
            reengMessage.setStateMyReaction(reactAdd.VALUE);
        updateAllFieldsOfMessage(reengMessage);
        refreshThreadWithoutNewMessage(mThreadMessage.getId());

        mApplication.getXmppManager().sendReactionMessage(reengMessage, eventMessage, mThreadMessage,
                reactAdd, reactRemove);
    }

    private ArrayList<Integer> createArrayListReact() {
        ArrayList<Integer> listReact = new ArrayList<>();
        for (int i = 0; i < ReengMessageConstant.Reaction.DEFAULT.ordinal(); i++) {
            listReact.add(0);
        }
        return listReact;
    }

    public ArrayList<EventMessage> getListReaction(long mesId, String packetId) {
        return mEventMessageDataSource.getListEventMessageReactionFromMessageId(mesId, packetId);
    }

    public ArrayList<String> getListReactionSenderFromMessageId(long mesId, String packetId, int reaction) {
        return mEventMessageDataSource.getListReactionSenderFromMessageId(mesId, packetId, reaction);
    }

    public int getReactionByNumber(long mesId, String packetId, String number) {
        return mEventMessageDataSource.getReactionByNumber(mesId, packetId, number);
    }

    public void deleteAllHiddenThread(final BaseSlidingFragmentActivity activity) {
        //TODO deleteAllHiddenThread
        ArrayList<ThreadMessage> listDelete = new ArrayList<>();
        for (ThreadMessage threadMessage : getThreadMessageArrayList()) {
            if (threadMessage.getHiddenThread() == 1)
                listDelete.add(threadMessage);
        }
        if (!listDelete.isEmpty()) {
            new AsyncTask<ArrayList<ThreadMessage>, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    activity.showLoadingDialog("", R.string.loading);
                }

                @Override
                protected Void doInBackground(ArrayList<ThreadMessage>... voids) {
                    ArrayList<ThreadMessage> listThread = voids[0];
                    deleteListThreadMessage(listThread);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    activity.hideLoadingDialog();
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, listDelete);

        }
    }

    public HashMap<String, PreKeyBundle> getHashMapEncrypt() {
        return hashMapEncrypt;
    }

    public void setHashMapEncrypt(HashMap<String, PreKeyBundle> hashMapEncrypt) {
        this.hashMapEncrypt = hashMapEncrypt;
    }

    public void sendMessageEnableEncrypt(int state, ThreadMessage threadMessage) {
        threadMessage.setEncryptThread(state);
        ReengMessage reengMessage = new ReengMessage(threadMessage.getThreadType(), ReengMessageConstant.MessageType.enable_e2e);
        reengMessage.setStateEnableE2E(state);
        reengMessage.setMessageType(ReengMessageConstant.MessageType.enable_e2e);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(threadMessage.getSoloNumber());
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setStatus(ReengMessageConstant.STATUS_LOADING);
        Date date = new Date();
        reengMessage.setTime(date.getTime());
        String content = String.format(mRes.getString(state == 1
                ? R.string.e2e_enable_send : R.string.e2e_disable_send), threadMessage.getThreadName());
        reengMessage.setContent(content);

        insertNewMessageBeforeSend(threadMessage, threadMessage.getThreadType(), reengMessage);
        sendXMPPMessage(reengMessage, threadMessage);
        updateThreadMessage(threadMessage);
        refreshThreadWithoutNewMessage(threadMessage.getId());
    }

    public boolean sendMessageText(Activity activity, ThreadMessage threadMessage
            , String content, ReplyMessage replyMessage, ArrayList<TagMocha> mListTag, boolean isSendSMSout) {
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        if (OfficialActionManager.getInstance(mApplication).checkAndSendOfficialActionFromString(threadMessage, content)) {
            return false;// goi api official action
        }
        SoloSendTextMessage msg = new SoloSendTextMessage(threadMessage, mApplication.getReengAccountBusiness().getJidNumber(), receiver, content);
        msg.setReplyMessage(replyMessage);
        if (mListTag != null && !mListTag.isEmpty()) {
            String textTag = TagHelper.getTextTag(mListTag);
            Log.i(TAG, "textTag: " + textTag);
            msg.setTagContent(textTag);
            msg.setListTagContent(mListTag);
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && isSendSMSout) {
            msg.setChatMode(ReengMessageConstant.MODE_GSM);
        } else {
            msg.setChatMode(ReengMessageConstant.MODE_IP_IP);
        }
        if (insertNewMessageBeforeSend(threadMessage, threadType, msg)) {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    msg.setCState(newState);
                }
            }
            sendXMPPMessage(msg, threadMessage);
            return true;
        }
        return false;
    }

    public boolean sendForwardingMessage(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        if (activity == null || threadMessage == null || forwardingMessage == null) return false;
        ReengMessageConstant.MessageType messageType = forwardingMessage.getMessageType();
        if (messageType == ReengMessageConstant.MessageType.text) {
            // text
            return forwardText(activity, threadMessage, forwardingMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.image) {
            // image
            return forwardImage(activity, threadMessage, forwardingMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.voicemail) {
            // voice mail
            return forwardVoiceMail(activity, threadMessage, forwardingMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.file) {
            // file
            return forwardFile(activity, threadMessage, forwardingMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.shareContact) {
            // contact
            return forwardContact(activity, threadMessage, forwardingMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.voiceSticker) {
            // voice sticker
            return forwardVoiceSticker(activity, threadMessage, forwardingMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.shareVideo) {
            // video
            return forwardVideo(activity, threadMessage, forwardingMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.shareLocation) {
            // location
            return forwardLocation(activity, threadMessage, forwardingMessage, listener);
        }
        return false;
    }

    private boolean forwardText(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        ArrayList<TagMocha> listTagForward = forwardingMessage.getListTagContent();
        String content = TagHelper.getTextTagCopy(forwardingMessage.getContent(), listTagForward, mApplication);
        if (OfficialActionManager.getInstance(mApplication).checkAndSendOfficialActionFromString(threadMessage, content)) {
            return false;// goi api official action
        }
        SoloSendTextMessage msg = new SoloSendTextMessage(threadMessage, mApplication.getReengAccountBusiness().getJidNumber(), receiver, content);
        msg.setReplyMessage(null);
//        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && true) {
//            msg.setChatMode(ReengMessageConstant.MODE_GSM);
//        } else {
        msg.setChatMode(ReengMessageConstant.MODE_IP_IP);
//        }
        if (insertNewMessageBeforeSend(threadMessage, threadType, msg)) {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    msg.setCState(newState);
                }
            }
            sendXMPPMessage(msg, threadMessage);
            if (listener != null) listener.onForwardText(msg);
            return true;
        }
        return false;
    }

    private boolean forwardImage(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        String userNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloSendImageMessage msg = new SoloSendImageMessage(threadMessage, userNumber, receiver,
                forwardingMessage.getFilePath());
        msg.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (TextUtils.isEmpty(forwardingMessage.getFileId()) && forwardingMessage.getSongId() != -1) {
            msg.setFileId(String.valueOf(forwardingMessage.getSongId()));
        } else {
            msg.setFileId(forwardingMessage.getFileId());
        }
        msg.setDirectLinkMedia(forwardingMessage.getDirectLinkMedia());
        msg.setVideoContentUri(forwardingMessage.getVideoContentUri());
        if (insertNewMessageBeforeSend(threadMessage, threadType, msg)) {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    msg.setCState(newState);
                }
            }
            sendXMPPMessage(msg, threadMessage);
            if (listener != null) listener.onForwardImage(msg);
            return true;
        }
        return false;
    }

    private boolean forwardVideo(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        String userNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloShareVideoMessage msg = new SoloShareVideoMessage(threadMessage, userNumber, receiver,
                forwardingMessage.getFilePath(), forwardingMessage.getDuration(),
                forwardingMessage.getFileName(), forwardingMessage.getSize(),
                forwardingMessage.getVideoContentUri(), forwardingMessage.getImageUrl());
        msg.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (TextUtils.isEmpty(forwardingMessage.getFileId()) && forwardingMessage.getSongId() != -1) {
            msg.setFileId(String.valueOf(forwardingMessage.getSongId()));
        } else {
            msg.setFileId(forwardingMessage.getFileId());
        }
        msg.setDirectLinkMedia(forwardingMessage.getDirectLinkMedia());
        if (insertNewMessageBeforeSend(threadMessage, threadType, msg)) {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    msg.setCState(newState);
                }
            }
            sendXMPPMessage(msg, threadMessage);
            if (listener != null) listener.onForwardVideo(msg);
            return true;
        }
        return false;
    }

    private boolean forwardVoiceMail(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        String userNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloSendVoicemailMessage msg = new SoloSendVoicemailMessage(threadMessage, userNumber, receiver,
                forwardingMessage.getFilePath(), forwardingMessage.getDuration(), forwardingMessage.getFileName());
        msg.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (TextUtils.isEmpty(forwardingMessage.getFileId()) && forwardingMessage.getSongId() != -1) {
            msg.setFileId(String.valueOf(forwardingMessage.getSongId()));
        } else {
            msg.setFileId(forwardingMessage.getFileId());
        }
        msg.setDirectLinkMedia(forwardingMessage.getDirectLinkMedia());
        if (insertNewMessageBeforeSend(threadMessage, threadType, msg)) {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    msg.setCState(newState);
                }
            }
            sendXMPPMessage(msg, threadMessage);
            if (listener != null) listener.onForwardVoiceMail(msg);
            return true;
        }
        return false;
    }

    private boolean forwardContact(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return false;// room chat khong cho gui contact
        }
        String userNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloSendShareContactMessage msg =
                new SoloSendShareContactMessage(threadMessage, userNumber, receiver,
                        forwardingMessage.getContent(), forwardingMessage.getFileName());
        if (insertNewMessageBeforeSend(threadMessage, threadType, msg)) {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    msg.setCState(newState);
                }
            }
            sendXMPPMessage(msg, threadMessage);
            if (listener != null) listener.onForwardContact(msg);
            return true;
        }
        return false;
    }

    private boolean forwardVoiceSticker(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        int threadType = threadMessage.getThreadType();
        String userNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        SoloVoiceStickerMessage msg = new SoloVoiceStickerMessage(threadMessage,
                userNumber, receiver, TextHelper.parserIntFromString(forwardingMessage.getFileName(), -1), (int) forwardingMessage.getSongId());
        if (insertNewMessageBeforeSend(threadMessage, threadType, msg)) {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    msg.setCState(newState);
                }
            }
            sendXMPPMessage(msg, threadMessage);
            if (listener != null) listener.onForwardVoiceSticker(msg);
            return true;
        }
        return false;
    }

    private boolean forwardLocation(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return false;// room chat khong cho gui location
        }
        String userNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloShareLocationMessage msg = new SoloShareLocationMessage(threadMessage,
                userNumber, receiver, forwardingMessage.getContent(),
                forwardingMessage.getFilePath(), forwardingMessage.getImageUrl());
        msg.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (insertNewMessageBeforeSend(threadMessage, threadType, msg)) {
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    msg.setCState(newState);
                }
            }
            sendXMPPMessage(msg, threadMessage);
            if (listener != null) listener.onForwardLocation(msg);
            return true;
        }
        return false;
    }

    private boolean forwardFile(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage forwardingMessage, ForwardMessageListener listener) {
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return false;// room chat khong cho gui file
        }
        String userNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String filePath = forwardingMessage.getFilePath();
        String fileName = forwardingMessage.getFileName();
        String extension = FileHelper.getExtensionFile(forwardingMessage.getFilePath());
        String fileType = ReengMessageConstant.FileType.fromString(extension).toString();
        ArrayList<ReengMessage> reengMessages = new ArrayList<>();
        ReengFileMessage message = new ReengFileMessage(threadMessage, userNumber, receiver, filePath, fileType, fileName);
        reengMessages.add(message);
        mApplication.getTransferFileBusiness().startUploadMessageFile(message);
        insertListNewMessageBeforeSend(threadMessage, reengMessages);
        if (listener != null) listener.onForwardFile(message);
        return true;
    }

    public boolean sendNewMessage(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        ReengMessageConstant.MessageType messageType = reengMessage.getMessageType();
        if (messageType == ReengMessageConstant.MessageType.text) {
            // text
            return sendMsgText(activity, threadMessage, reengMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.image) {
            // image
            return sendMsgImage(activity, threadMessage, reengMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.voicemail) {
            // voice mail
            return sendMsgVoiceMail(activity, threadMessage, reengMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.file) {
            // file
            return sendMsgFile(activity, threadMessage, reengMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.shareContact) {
            // contact
            return sendMsgContact(activity, threadMessage, reengMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.voiceSticker) {
            // voice sticker
            return sendMsgVoiceSticker(activity, threadMessage, reengMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.shareVideo) {
            // video
            return sendMsgVideo(activity, threadMessage, reengMessage, listener);
        } else if (messageType == ReengMessageConstant.MessageType.shareLocation) {
            // location
            return sendMsgLocation(activity, threadMessage, reengMessage, listener);
        }
        return false;
    }

    private boolean sendMsgText(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        if (OfficialActionManager.getInstance(mApplication).checkAndSendOfficialActionFromString(threadMessage, reengMessage.getContent())) {
            return false;// goi api official action
        }
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(receiver);
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setPacketId(threadMessage.getThreadType(), reengMessage.getMessageType());
        reengMessage.setTime(new Date().getTime());
        reengMessage.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        if (insertNewMessageBeforeSend(threadMessage, threadMessage.getThreadType(), reengMessage)) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    reengMessage.setCState(newState);
                }
            }
            sendXMPPMessage(reengMessage, threadMessage);
            if (listener != null) listener.onSendText(reengMessage);
            return true;
        }
        return false;
    }

    private boolean sendMsgImage(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return false;// room chat khong cho gui anh
        }
        ArrayList<ReengMessage> reengMessages = new ArrayList<>();
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(receiver);
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setPacketId(threadMessage.getThreadType(), reengMessage.getMessageType());
        reengMessage.setTime(new Date().getTime());
        reengMessage.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        if (!TextUtils.isEmpty(reengMessage.getFilePath())) {
            //get file name from file path
            reengMessage.setFileName(MessageHelper.getNameFileImage(reengMessage.getPacketId()));
            MessageImageDB msgImage = getMessageImageDB(reengMessage.getFilePath());
            if (msgImage != null) {
                reengMessage.setVideoContentUri(msgImage.getRatio());
                try {
                    JSONObject object = new JSONObject(msgImage.getDataResponse());
                    String fileId = object.optString(Constants.HTTP.REST_DESC, null);
                    String videoThumb = object.optString(Constants.HTTP.REST_THUMB, null);
                    String directLink = object.optString("link", null);
                    reengMessage.setImageUrl(videoThumb);
                    reengMessage.setFileId(fileId);
                    reengMessage.setDirectLinkMedia(directLink);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                MessageHelper.setRatioForMessageImage(reengMessage, reengMessage.getFilePath());
            }
        }
        reengMessages.add(reengMessage);
        insertListNewMessageBeforeSend(threadMessage, reengMessages);
        for (ReengMessage message : reengMessages) {
            if (!TextUtils.isEmpty(message.getDirectLinkMedia())) {
                sendXMPPMessage(message, threadMessage);
            } else {
                mApplication.getTransferFileBusiness().startUploadMessageFile(message);
            }
        }
        if (listener != null) listener.onSendImage(reengMessage);
        return true;
    }

    private boolean sendMsgVoiceMail(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return false;// room chat khong cho gui voice mail
        }
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(receiver);
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setPacketId(threadMessage.getThreadType(), reengMessage.getMessageType());
        reengMessage.setTime(new Date().getTime());
        reengMessage.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        reengMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (insertNewMessageBeforeSend(threadMessage, threadType, reengMessage)) {
            mApplication.getTransferFileBusiness().startUploadMessageFile(reengMessage);
            if (listener != null) listener.onSendVoiceMail(reengMessage);
            return true;
        }
        return false;
    }

    private boolean sendMsgFile(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(receiver);
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setPacketId(threadMessage.getThreadType(), reengMessage.getMessageType());
        reengMessage.setTime(new Date().getTime());
        reengMessage.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        if (insertNewMessageBeforeSend(threadMessage, threadType, reengMessage)) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    reengMessage.setCState(newState);
                }
            }
            sendXMPPMessage(reengMessage, threadMessage);
            if (listener != null) listener.onSendFile(reengMessage);
            return true;
        }
        return false;
    }

    private boolean sendMsgContact(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return false;// room chat khong cho gui contacts
        }
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(receiver);
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setPacketId(threadMessage.getThreadType(), reengMessage.getMessageType());
        reengMessage.setTime(new Date().getTime());
        reengMessage.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        if (insertNewMessageBeforeSend(threadMessage, threadType, reengMessage)) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    reengMessage.setCState(newState);
                }
            }
            sendXMPPMessage(reengMessage, threadMessage);
            if (listener != null) listener.onSendContact(reengMessage);
            return true;
        }
        return false;
    }

    private boolean sendMsgVoiceSticker(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(receiver);
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setPacketId(threadMessage.getThreadType(), reengMessage.getMessageType());
        reengMessage.setTime(new Date().getTime());
        reengMessage.setStatus(ReengMessageConstant.STATUS_LOADING);
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        if (insertNewMessageBeforeSend(threadMessage, threadType, reengMessage)) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    reengMessage.setCState(newState);
                }
            }
            sendXMPPMessage(reengMessage, threadMessage);
            if (listener != null) listener.onSendVoiceSticker(reengMessage);
            return true;
        }
        return false;
    }

    private boolean sendMsgVideo(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT ||
                threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            activity.showToast(activity.getString(R.string.not_support), Toast.LENGTH_SHORT);
            return false;// room chat khong cho gui video
        }
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(receiver);
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setPacketId(threadMessage.getThreadType(), reengMessage.getMessageType());
        reengMessage.setTime(new Date().getTime());
        reengMessage.setStatus(ReengMessageConstant.STATUS_LOADING);
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        reengMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (insertNewMessageBeforeSend(threadMessage, threadType, reengMessage)) {
            mApplication.getTransferFileBusiness().startUploadMessageFile(reengMessage);
            if (listener != null) listener.onSendVideo(reengMessage);
            return true;
        }
        return false;
    }

    private boolean sendMsgLocation(BaseSlidingFragmentActivity activity, ThreadMessage threadMessage
            , ReengMessage reengMessage, SendNewMessageListener listener) {
        if (activity == null || threadMessage == null || reengMessage == null) return false;
        if (notifyWhenGroupNoExist(activity, threadMessage)) {
            return false;
        }
        String receiver = getReceivedWhenSendMessage(threadMessage, activity);
        if (TextUtils.isEmpty(receiver)) {
            return false;
        }
        int threadType = threadMessage.getThreadType();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return false;// room chat khong cho gui location
        }
        reengMessage.setSender(mApplication.getReengAccountBusiness().getJidNumber());
        reengMessage.setReceiver(receiver);
        reengMessage.setThreadId(threadMessage.getId());
        reengMessage.setPacketId(threadMessage.getThreadType(), reengMessage.getMessageType());
        reengMessage.setTime(new Date().getTime());
        reengMessage.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        reengMessage.setReadState(ReengMessageConstant.READ_STATE_READ);
        reengMessage.setDirection(ReengMessageConstant.Direction.send);
        reengMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (insertNewMessageBeforeSend(threadMessage, threadType, reengMessage)) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                int newState = getCPresenceByJid(receiver).getStatePresence();
                if (newState != -1) {
                    reengMessage.setCState(newState);
                }
            }
            sendXMPPMessage(reengMessage, threadMessage);
            if (listener != null) listener.onSendLocation(reengMessage);
            return true;
        }
        return false;
    }
}
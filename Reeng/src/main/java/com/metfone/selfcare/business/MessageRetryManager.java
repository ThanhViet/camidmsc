package com.metfone.selfcare.business;


import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.MessageImageDB;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.notification.ReengNotificationManager;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.ReengEventPacket;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thaodv on 05-Nov-14.
 */
public class MessageRetryManager implements XMPPConnectivityChangeListener {

    private static final String TAG = MessageRetryManager.class.getSimpleName();
    private static MessageRetryManager instance;
    //key la packet-id cua message
    private ConcurrentHashMap<String, Object> mMessageWaitingConfirmMap;
    private Timer mTimer;
    private RetryTimerTask mRetryTimerTask;
    private MessageBusiness mMessageBusiness;
    private ApplicationController mApplication;
    private XMPPManager mXmppManager;
    private ReengMessageConstant.MessageType messageType;
    private CopyOnWriteArrayList<ReengMessage> listMessageToRemove;
    private CopyOnWriteArrayList<ReengMessage> listMessageToRetry;
    private CopyOnWriteArrayList<Packet> listEventPacketToRetry;
    private ReengNotificationManager mReengNotificationManager;
    private boolean isNotCheckMin = false;

    private MessageRetryManager(ApplicationController app, XMPPManager xmppManager) {
        mMessageWaitingConfirmMap = new ConcurrentHashMap<>();
        //        mMessageSeenMap = new ConcurrentHashMap<String, Packet>();
        mApplication = app;
        mXmppManager = xmppManager;
        XMPPManager.addXMPPConnectivityChangeListener(this);
        listMessageToRemove = new CopyOnWriteArrayList<>();
        listMessageToRetry = new CopyOnWriteArrayList<>();
        listEventPacketToRetry = new CopyOnWriteArrayList<>();
        mReengNotificationManager = ReengNotificationManager.getInstance(app);
    }

    public static synchronized MessageRetryManager getInstance(ApplicationController app, XMPPManager xmppManager) {
        if (instance == null) {
            instance = new MessageRetryManager(app, xmppManager);
        }
        return instance;
    }

    public void addToWaitingConfirmMap(Object object) {
        if (object instanceof ReengMessage) {
            addReengMessageToWaitingConfirmMap((ReengMessage) object);
        } else if (object instanceof ReengEventPacket) {
            addToSendingSeenMap((Packet) object);
        }
    }

    /**
     * them vao map doi ban tin confirm de xac nhan la tin nhan da nhan duoc
     *
     * @param reengMessage
     */
    private void addReengMessageToWaitingConfirmMap(ReengMessage reengMessage) {
        messageType = reengMessage.getMessageType();
        if (messageType == ReengMessageConstant.MessageType.notification ||
                messageType == ReengMessageConstant.MessageType.file ||
                messageType == ReengMessageConstant.MessageType.voicemail ||
                messageType == ReengMessageConstant.MessageType.inviteShareMusic ||
                messageType == ReengMessageConstant.MessageType.actionShareMusic ||
                messageType == ReengMessageConstant.MessageType.shareVideo) {
            //tam thoi chua retry tin nhan file
            // message notify cung ko duoc retry
            return;
        }
        mMessageWaitingConfirmMap.put(reengMessage.getPacketId(), reengMessage);
        Log.d(TAG, "add mMessageWaitingConfirmMap: " + reengMessage.getPacketId());
        checkToStartRetryTimerTask(Config.Message.RETRY_DELAY);
    }

    public void addToSendingSeenMap(Packet seenPacket) {
        Log.i(TAG, "addToSendingSeenMap " + seenPacket.toXML());
        mMessageWaitingConfirmMap.put(seenPacket.getPacketID(), seenPacket);
    }

    public void checkToStartRetryTimerTask(int delayTime) {
        if (mRetryTimerTask == null) {
            Log.d(TAG, "start Timer & RetryTimerTask");
            mRetryTimerTask = new RetryTimerTask();
            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(mRetryTimerTask, delayTime, Config.Message.RETRY_PERIOD);
        }
    }

    /**
     * nhan duoc ban tin confirm received tu server --> remove
     *
     * @param packetId
     * @return
     */
    public Object removeFromWaitingConfirmMap(String packetId) {
        if(mMessageWaitingConfirmMap != null && mMessageWaitingConfirmMap.size() > 0)
        {
            Object object = mMessageWaitingConfirmMap.get(packetId);
            boolean removed = mMessageWaitingConfirmMap.remove(packetId, object);
            if(removed)
                Log.d(TAG, "removed " + removed + " - " + packetId + "size = " + mMessageWaitingConfirmMap.size() + object);
            cancelTimer();
            return object;
        }
        return null;
    }


    @Override
    public void onXMPPConnected() {
        //retry neu 1 trong 2 map co phan tu
        if (!mMessageWaitingConfirmMap.isEmpty()) {
            Log.i(TAG, "onXMPPConnected retry send message");
            if (mRetryTimerTask == null) {
                mRetryTimerTask = new RetryTimerTask();
            }
            isNotCheckMin = true;
            mRetryTimerTask.run();
        } else {
            Log.i(TAG, "onXMPPConnected don't need to retry send message");
        }
    }

    @Override
    public void onXMPPDisconnected() {

    }

    @Override
    public void onXMPPConnecting() {

    }


    private class RetryTimerTask extends TimerTask {

        @Override
        public void run() {
            if (!Config.Message.ENABLE_RETRY) return;
            long now = System.currentTimeMillis();
            Log.i(TAG, "RetryTimerTask running");
            listMessageToRemove.clear();
            listMessageToRetry.clear();
            listEventPacketToRetry.clear();
            // duyet cac phan tu trong map
            //so sanh create_time cua tung message voi thoi gian hien tai
            //neu qua thoi gian timeout thi chuyen trang thai message la error, remove khoi map
            //neu chua qua thoi gian timeout thi retry gui
            if (!mMessageWaitingConfirmMap.isEmpty()) {
                Log.i(TAG, "mMessageWaitingConfirmMap not null size: " + mMessageWaitingConfirmMap.size());
                for (Map.Entry<String, Object> entry : mMessageWaitingConfirmMap.entrySet()) {
                    Object object = entry.getValue();
                    if (object instanceof ReengMessage) {
                        ReengMessage reengMessage = (ReengMessage) object;
                        long passedTime = now - reengMessage.getTime();
                        Log.i(TAG, "passedTime " + passedTime);
                        if (passedTime > Config.Message.RETRY_TIMEOUT) {
                            //dua vao danh sach de chuyen thanh tin nhan loi va remove
                            listMessageToRemove.add(reengMessage);
                        } else {
                            Log.i(TAG, "isNotCheckMin " + isNotCheckMin);
                            if(isNotCheckMin)
                            {
                                listMessageToRetry.add(reengMessage);
                            }
                            else
                            {
                                if (passedTime >= Config.Message.RETRY_MIN_TIME) {
                                    //dua vao danh sach de retry
                                    listMessageToRetry.add(reengMessage);
                                }
                            }
                        }
                    } else if (object instanceof ReengEventPacket) {
                        ReengEventPacket reengEventPacket = (ReengEventPacket) object;
                        listEventPacketToRetry.add(reengEventPacket);
                    }
                }
                // xu ly sau khi duyet map
                if (!listMessageToRemove.isEmpty()) {
                    removeListMessageAndMarkError(listMessageToRemove);
                }
                if (!listMessageToRetry.isEmpty()) {
                    //can check co ket noi xmpp o day ko
                    if (mXmppManager.isAuthenticated()) {
                        resendListOutgoingMessage(listMessageToRetry);
                    } else {
                        Log.i(TAG, "have " + listMessageToRetry.size() + " messages to retry but not connect to server");
                    }
                }
                //---------------------neu co packet can retry thi retry-----------------
                if (!listEventPacketToRetry.isEmpty()) {
                    if (mXmppManager.isAuthenticated()) {
                        resendListSeenMessage(listEventPacketToRetry);
                    } else {
                        Log.i(TAG, "have " + listEventPacketToRetry.size() + " packet to retry but not connect to server");
                    }
                }
            }
            isNotCheckMin = false;
        }
    }

    private void removeListMessageAndMarkError(CopyOnWriteArrayList<ReengMessage> listMessageToRemove) {
        Log.d(TAG, "removeListMessageAndMarkError " + listMessageToRemove.size() + " messages");
        mMessageBusiness = mApplication.getMessageBusiness();
        if (mMessageBusiness == null) return;
        //danh dau la tin nhan loi
        //remove tin nhan khoi map
        for (ReengMessage reengMessage : listMessageToRemove) {
            removeFromWaitingConfirmMap(reengMessage.getPacketId());
            // cap nhat trang thai vao memory
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            // cap nhat trang thai vao db
            mMessageBusiness.updateAllFieldsOfMessage(reengMessage);
            mMessageBusiness.refreshThreadWithoutNewMessage(reengMessage.getThreadId());
        }
        //thong bao tin nhan ko gui dc
        HashMap<Integer, ReengMessage> distinctListMessageByThreadId = new HashMap<>();
        for (ReengMessage reengMessage : listMessageToRemove) {
            distinctListMessageByThreadId.put(reengMessage.getThreadId(), reengMessage);
        }
        for (Map.Entry<Integer, ReengMessage> entry : distinctListMessageByThreadId.entrySet()) {
            ReengMessage reengMessage = entry.getValue();
            ThreadMessage threadByThreadId = mMessageBusiness.findThreadByThreadId(reengMessage.getThreadId());
            if (threadByThreadId != null) {
                mReengNotificationManager.notifySendMessagesError(mApplication, threadByThreadId);
            }
        }
    }

    private void resendListOutgoingMessage(CopyOnWriteArrayList<ReengMessage> listMessageToRetry) {
        //retry gui tin nhan
        mMessageBusiness = mApplication.getMessageBusiness();
        if (mMessageBusiness == null) {
            return;
        }
        for (ReengMessage reengMessage : listMessageToRetry) {
            final ThreadMessage threadById = mMessageBusiness.getThreadById(reengMessage.getThreadId());
            if (threadById == null) {
                Log.d(TAG, "thread of message not found --> not retry --> remove");
                removeFromWaitingConfirmMap(reengMessage.getPacketId());
            } else {
                Log.i(TAG, "resend message  " + reengMessage);
                if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.image) {
                    Log.i(TAG, "retry message image");
                    MessageImageDB msgImage = mMessageBusiness.getMessageImageDB(reengMessage.getFilePath());
                    if (msgImage != null) {
                        Log.i(TAG, "msgImage: " + msgImage.toString());
                        reengMessage.setVideoContentUri(msgImage.getRatio());
                        try {
                            JSONObject object = new JSONObject(msgImage.getDataResponse());
                            String fileId = object.optString(Constants.HTTP.REST_DESC, null);
                            String videoThumb = object.optString(Constants.HTTP.REST_THUMB, null);
                            String directLink = object.optString("link", null);
                            reengMessage.setImageUrl(videoThumb);
                            reengMessage.setFileId(fileId);
                            reengMessage.setDirectLinkMedia(directLink);
                            mMessageBusiness.sendXMPPMessage(reengMessage, threadById);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        mApplication.getTransferFileBusiness().startUploadMessageFile(reengMessage);
                    }
                } else {
                    Log.i(TAG, "retry message text");
                    mMessageBusiness.sendXMPPMessage(reengMessage, threadById);
                }
            }
        }
    }

    private void resendListSeenMessage(CopyOnWriteArrayList<Packet> eventPacketArrayList) {
        Log.i(TAG, "resendListSeenMessage");
        mMessageBusiness = mApplication.getMessageBusiness();
        if (mMessageBusiness == null) return;
        if (!mXmppManager.isAuthenticated()) return;
        for (Packet entry : eventPacketArrayList) {
            try {
                mXmppManager.sendPacketNoWaitResponse(entry);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    private void cancelTimer() {
        if (mMessageWaitingConfirmMap.isEmpty()) {
            Log.d(TAG, "destroy Timer & RetryTimerTask");
            if (mRetryTimerTask != null) {
                mRetryTimerTask.cancel();
                mRetryTimerTask = null;
            }
            if (mTimer != null) {
                mTimer.cancel();
                mTimer = null;
            }
        }
    }
}
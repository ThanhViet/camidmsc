package com.metfone.selfcare.business;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.FirebaseEventConstant;
import com.metfone.selfcare.module.keeng.utils.SharedPref;

import java.util.Calendar;

public class FirebaseEventBusiness {
    private final FirebaseAnalytics firebaseAnalytics;
    private ApplicationController applicationController;
    public static int play_from_type = 0;
    public static String nameCategory = "";
    public static String idCategory = "";
    public static final int FROM_COUNTRY = 1;
    public static final int FROM_CATEGORY = 2;
    public static final int FROM_TOPIC = 3;

    public static int SIZE_TAB = 6;
    public static boolean[] activeTab = new boolean[SIZE_TAB]; // home,reward,esport,cinema,chat,metfone

    public static class TabLog {
        public final static int TAB_HOME = 0;
        public final static int TAB_REWARD = 1;
        public final static int TAB_ESPORT = 2;
        public final static int TAB_CINEMA = 3;
        public final static int TAB_CHAT = 4;
        public final static int TAB_METFONE = 5;

        public static final String ACTIVE_TAB = "active_tab_";
    }

    public FirebaseEventBusiness(ApplicationController applicationController) {
        this.firebaseAnalytics = FirebaseAnalytics.getInstance(applicationController);
        this.applicationController = applicationController;
        long startTimeDay = getStartOfDayInMillis();
        for (int i = 0; i < SIZE_TAB; i++) {
            long time = SharedPref.newInstance(applicationController).getLong(TabLog.ACTIVE_TAB + i, 0);
            activeTab[i] = time < startTimeDay ? false : true;
        }
    }

    public long getStartOfDayInMillis() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    private void logEvent(String name, Bundle bundle) {
        if (firebaseAnalytics != null) {
            firebaseAnalytics.logEvent(name, bundle);
        }
    }

    public void logSelectTab(String tabName) {
        Bundle bundle = new Bundle();
        bundle.putString("tab_name", tabName);
        logEvent(FirebaseEventConstant.EVENT_SELECT_TAB, bundle);
    }

    public void logIshare(boolean isSuccess, String share_price, String error_code) {
        Bundle bundle = new Bundle();
        if (isSuccess) {
            bundle.putString("share_price", share_price);
            logEvent(FirebaseEventConstant.EVENT_ISHARE_SUCCESS, bundle);
        } else {
            bundle.putString("error_code", error_code);
            logEvent(FirebaseEventConstant.EVENT_ISHARE_FAILURE, bundle);
        }
    }

    public void logOrderCurrentNumber(boolean isSuccess, String oldIsdn, String orderIsdn, String order_type, String error_code) {
        Bundle bundle = new Bundle();
        bundle.putString("oldIsdn", oldIsdn);
        bundle.putString("orderIsdn", orderIsdn);
        bundle.putString("order_type", order_type);
        if (isSuccess) {
            logEvent(FirebaseEventConstant.EVENT_CHANGENUMBER_KEEPSIM_SUCCESS, bundle);
        } else {
            bundle.putString("error_code", error_code);
            logEvent(FirebaseEventConstant.EVENT_CHANGENUMBER_KEEPSIM_FAILURE, bundle);
        }
    }

    public void logOrderNewSim(boolean isSuccess, String error_code) {
        Bundle bundle = new Bundle();
        if (isSuccess) {
            logEvent(FirebaseEventConstant.EVENT_ORDER_NEWSIM_SUCCESS, bundle);
        } else {
            bundle.putString("error_code", error_code);
            logEvent(FirebaseEventConstant.EVENT_ORDER_NEWSIM_FAILURE, bundle);
        }
    }

    public void logTopupQRCode(boolean isSuccess, String qr_code) {
        Bundle bundle = new Bundle();
        bundle.putString("QR_Code", qr_code);
        if (isSuccess) {
            logEvent(FirebaseEventConstant.EVENT_TOPUP_QRCODE_SUCCESS, bundle);
        } else {
            logEvent(FirebaseEventConstant.EVENT_TOPUP_QRCODE_FAILURE, bundle);
        }
    }

    public void logTopup(boolean isSuccess, String payment_info, String payment_method, String topup_amount, String error_code) {
        Bundle bundle = new Bundle();
        bundle.putString("payment_info", payment_info);
        bundle.putString("payment_method", payment_method);
        bundle.putString("topup_amount", topup_amount);
        if (isSuccess) {
            logEvent(FirebaseEventConstant.EVENT_TOPUP_SUCCESS, bundle);
        } else {
            bundle.putString("error_code", error_code);
            logEvent(FirebaseEventConstant.EVENT_TOPUP_FAILURE, bundle);
        }
    }

    public void logServicePack(String service_name) {
        Bundle bundle = new Bundle();
        bundle.putString("service_name", service_name);
        logEvent(FirebaseEventConstant.EVENT_SELECT_SERVICE_PACK, bundle);
    }

    public void logExchangeService(String service_name, String service_code) {
        Bundle bundle = new Bundle();
        bundle.putString("service_name", service_name);
        bundle.putString("service_code", service_code);
        logEvent(FirebaseEventConstant.EVENT_SELECT_EXCHANGE_SERVICE, bundle);
    }

    public void logRegisterService(String service_name, String service_code) {
        Bundle bundle = new Bundle();
        bundle.putString("service_name", service_name);
        bundle.putString("service_code", service_code);
        logEvent(FirebaseEventConstant.EVENT_SELECT_REGISTER_SERVICE, bundle);
    }

    public void logRegisterServiceResult(boolean isSuccess, String service_name, String service_code, String error_code) {
        Bundle bundle = new Bundle();
        bundle.putString("service_name", service_name);
        bundle.putString("service_code", service_code);
        if (isSuccess) {
            logEvent(FirebaseEventConstant.EVENT_REGISTER_SERVICE_SUCCESS, bundle);
        } else {
            bundle.putString("error_code", error_code);
            logEvent(FirebaseEventConstant.EVENT_REGISTER_SERVICE_FAILURE, bundle);
        }
    }

    public void logStopFilm(String filmId, String filmName, String stop_watch_film_time) {
        Bundle bundle = new Bundle();
        bundle.putString("film_id", filmId);
        bundle.putString("film_name", filmName);
        bundle.putString("stop_watch_film_time", stop_watch_film_time);
        logEvent(FirebaseEventConstant.EVENT_STOP_WATCH_FILM, bundle);
    }

    public void logPauseTrailer(String filmId, String filmName, String paused_time) {
        Bundle bundle = new Bundle();
        bundle.putString("film_id", filmId);
        bundle.putString("film_name", filmName);
        bundle.putString("paused_time", paused_time);
        logEvent(FirebaseEventConstant.EVENT_DETAIL_FILM_PAUSE_TRAILER, bundle);
    }

    public void logButtonFilmClick(String buttonName) {
        Bundle bundle = new Bundle();
        bundle.putString("paused_time", buttonName);
        logEvent(FirebaseEventConstant.EVENT_DETAIL_FILM_SELECT_BUTTON, bundle);
    }

    public void logRewardVisitPage(String reward_name, String reward_code, String reward_id) {
        Bundle bundle = new Bundle();
        bundle.putString("reward_name", reward_name);
        bundle.putString("reward_code", reward_code);
        bundle.putString("reward_id", reward_id);
        logEvent(FirebaseEventConstant.EVENT_REWARD_VISIT_PAGE, bundle);
    }

    public void logRewardRedeem(String reward_name, String reward_code, String reward_id) {
        Bundle bundle = new Bundle();
        bundle.putString("reward_name", reward_name);
        bundle.putString("reward_code", reward_code);
        bundle.putString("reward_id", reward_id);
        logEvent(FirebaseEventConstant.EVENT_REWARD_REDEEM, bundle);
    }

    public void logRewardQrCode(String reward_name, String reward_code, String reward_id) {
        Bundle bundle = new Bundle();
        bundle.putString("reward_name", reward_name);
        bundle.putString("reward_code", reward_code);
        bundle.putString("reward_id", reward_id);
        logEvent(FirebaseEventConstant.EVENT_REWARD_QRCODE, bundle);
    }

    public void logSubtitle(String subtitle_name, String subtitle_code) {
        Bundle bundle = new Bundle();
        bundle.putString("subtitle_code", subtitle_code);
        bundle.putString("subtitle_name", subtitle_name);
        logEvent(FirebaseEventConstant.EVENT_SELECT_SUBTITLE, bundle);
    }

    public void logOpenFilmCountry(String name) {
        Bundle bundle = new Bundle();
        bundle.putString("country_name", name);
        logEvent(FirebaseEventConstant.EVENT_OPEN_FILM_DETAIL_IN_COUNTRY, bundle);

        play_from_type = FROM_COUNTRY;
        nameCategory = name;
    }

    public void logOpenFilmCategory(String id, String name) {
        Bundle bundle = new Bundle();
        bundle.putString("category_id", id);
        bundle.putString("category_name", name);
        logEvent(FirebaseEventConstant.EVENT_OPEN_FILM_DETAIL_IN_CATEGORY, bundle);

        play_from_type = FROM_CATEGORY;
        nameCategory = name;
        idCategory = id;
    }

    public void logOpenFilmDetail(String filmId, String filmName) {
        Bundle bundle = new Bundle();
        bundle.putString("film_id", filmId);
        bundle.putString("film_name", filmName);
        logEvent(FirebaseEventConstant.EVENT_OPEN_FILM_DETAIL, bundle);
    }

    public void logWatchFilm(String filmId, String filmName) {
        Bundle bundle = new Bundle();
        bundle.putString("film_id", filmId);
        bundle.putString("film_name", filmName);
        logEvent(FirebaseEventConstant.EVENT_WATCH_FILM, bundle);
    }

    public void logOpenFilmTopic(String name) {
        Bundle bundle = new Bundle();
        bundle.putString("topic_name", name);
        logEvent(FirebaseEventConstant.EVENT_OPEN_FILM_DETAIL_IN_TOPIC, bundle);

        play_from_type = FROM_TOPIC;
        nameCategory = name;
    }

    public void logPlayFilmCountry(String name) {
        Bundle bundle = new Bundle();
        bundle.putString("country_name", name);
        logEvent(FirebaseEventConstant.EVENT_WATCH_FILM_IN_COUNTRY_GENRES, bundle);

        play_from_type = FROM_COUNTRY;
        nameCategory = name;
    }

    public void logPlayFilmCategory(String id, String name) {
        Bundle bundle = new Bundle();
        bundle.putString("category_id", id);
        bundle.putString("category_name", name);
        logEvent(FirebaseEventConstant.EVENT_WATCH_FILM_IN_CATEGORY_GENRES, bundle);

        play_from_type = FROM_CATEGORY;
        nameCategory = name;
        idCategory = id;
    }

    public void logPlayFilmTopic(String name) {
        Bundle bundle = new Bundle();
        bundle.putString("topic_name", name);
        logEvent(FirebaseEventConstant.EVENT_WATCH_FILM_IN_TOPIC, bundle);

        play_from_type = FROM_TOPIC;
        nameCategory = name;
    }

    public void logActiveTab(int indexTab) {
        if (indexTab < 0 || indexTab >= SIZE_TAB) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("indexTab", indexTab);
        logEvent(FirebaseEventConstant.EVENT_ACTIVE_TAB, bundle);

        activeTab[indexTab] = true;
        SharedPref.newInstance(applicationController).putLong(TabLog.ACTIVE_TAB + indexTab, System.currentTimeMillis());
    }

    public void logPlayEsport(String videoTitle, String videoId) {
        Bundle bundle = new Bundle();
        bundle.putString("video_title", videoTitle);
        bundle.putString("video_id", videoId);
        logEvent(FirebaseEventConstant.EVENT_WATCH_ESPORT, bundle);
    }

    public void logPopularEsport(String videoTitle, String videoId) {
        Bundle bundle = new Bundle();
        bundle.putString("video_title", videoTitle);
        bundle.putString("video_id", videoId);
        logEvent(FirebaseEventConstant.EVENT_WATCH_VIDEO_POPULAR_LIVE_ESPORT, bundle);
    }

    public void logPastBroadcastEsport(String videoTitle, String videoId) {
        Bundle bundle = new Bundle();
        bundle.putString("video_title", videoTitle);
        bundle.putString("video_id", videoId);
        logEvent(FirebaseEventConstant.EVENT_WATCH_PAST_BROADCAST_ESPORT, bundle);
    }

    public void logUploadedEsport(String videoTitle, String videoId) {
        Bundle bundle = new Bundle();
        bundle.putString("video_title", videoTitle);
        bundle.putString("video_id", videoId);
        logEvent(FirebaseEventConstant.EVENT_WATCH_UPLOADED_VIDEO_ESPORT, bundle);
    }

    public void logLiveEsport(String videoTitle, String videoId) {
        Bundle bundle = new Bundle();
        bundle.putString("video_title", videoTitle);
        bundle.putString("video_id", videoId);
        logEvent(FirebaseEventConstant.EVENT_WATCH_LIVE_VIDEO_ESPORT, bundle);
    }

    public void logOpenGame(String gameName) {
        Bundle bundle = new Bundle();
        bundle.putString("game_name", gameName);
        logEvent(FirebaseEventConstant.EVENT_OPEN_GAME, bundle);
    }
}

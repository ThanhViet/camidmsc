/*
package com.metfone.selfcare.business;

import com.stringee.StatsListener;
import com.stringee.StringeeCall;
import com.stringee.StringeeCallListener;
import com.stringee.StringeeIceCandidate;
import com.stringee.StringeeIceServer;
import com.stringee.StringeeSessionDescription;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.call.CallConstant;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.model.CallData;
import org.jivesoftware.smack.model.IceServer;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.StatsReport;

import java.util.ArrayList;
import java.util.LinkedList;

*/
/**
 * Created by toanvk2 on 10/6/2016. ..
 *//*


public class CallRtcHelperBAK implements StringeeCallListener, StatsListener {
    private static CallRtcHelperBAK mInstance;
    private ApplicationController mApplication;
    private static final String TAG = CallRtcHelperBAK.class.getSimpleName();
    public StringeeCall currentCall;
    private boolean isCallOut = false;
    private final LinkedList<CallData> callDataQueue = new LinkedList<>();

    public static synchronized CallRtcHelperBAK getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new CallRtcHelperBAK(application);
        }
        return mInstance;
    }

    private CallRtcHelperBAK(ApplicationController application) {
        this.mApplication = application;
    }

    public void startCall(int callType, LinkedList<StringeeIceServer> iceServers, boolean isCallOut) {
        */
/*PeerConnectionFactory.initializeAndroidGlobals(mApplication, true, true, true);
        PeerConnectionFactory peerConnectionFactory = new PeerConnectionFactory((PeerConnectionFactory.Options)null);
        PeerConnection pc = peerConnectionFactory.createPeerConnection(createRtcConfig(), createMediaConstraints(), new CandidateListener());
        MediaStream var3x = peerConnectionFactory.createLocalMediaStream("ARDAMS");
        MediaConstraints var5 = new MediaConstraints();
        AudioSource audioSource = peerConnectionFactory.createAudioSource(var5);
        AudioTrack localAudioTrack = peerConnectionFactory.createAudioTrack("ARDAMSa0", audioSource);
        localAudioTrack.setEnabled(true);
        var3x.addTrack(localAudioTrack);
        pc.addStream(var3x);*//*


        //stopCall();
        boolean isCaller = callType == CallConstant.TYPE_CALLER;
        Log.i(TAG, "start call : " + isCaller);
        if (iceServers != null) {
            for (StringeeIceServer server : iceServers) {
                Log.d(TAG, "start call, iceServer: " + server.toString());
            }
        }
       */
/* LinkedList<StringeeIceServer> iceServers1 = new LinkedList<>();
        iceServers1.add(new StringeeIceServer("stun:125.212.226.16:3478", "", ""));
        iceServers1.add(new StringeeIceServer("turn:125.212.226.16:3478?transport=udp", "hieuhd", "123456"));*//*


        // Create peer connection constraints.
        StringeeCall call = new StringeeCall(this);
        call.setIceServers(iceServers);
        //        call.setIceServers(iceServers1);
        call.init(mApplication);
        currentCall = call;
        this.isCallOut = isCallOut;
        call.startCall(isCaller, isCallOut);
        setMute();
    }

    */
/*private MediaConstraints createMediaConstraints() {
        MediaConstraints var1x = new MediaConstraints();
        var1x.optional.add(new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));
        return var1x;
    }

    private PeerConnection.RTCConfiguration createRtcConfig() {
        LinkedList var2x = createIceServers();
        PeerConnection.RTCConfiguration var4x;
        (var4x = new PeerConnection.RTCConfiguration(var2x)).bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE;
        var4x.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE;
        var4x.continualGatheringPolicy = PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY;
        var4x.tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.DISABLED;
        var4x.candidateNetworkPolicy = PeerConnection.CandidateNetworkPolicy.LOW_COST;
        return var4x;
    }

    private LinkedList<PeerConnection.IceServer> createIceServers() {
        LinkedList<PeerConnection.IceServer> iceServers1 = new LinkedList<>();
        iceServers1.add(new PeerConnection.IceServer("stun:125.212.226.16:3478", "", ""));
        iceServers1.add(new PeerConnection.IceServer("turn:125.212.226.16:3478?transport=udp", "hieuhd", "123456"));
        return iceServers1;
    }*//*


    public void startCall(int callTye, LinkedList<StringeeIceServer> iceServers, CallData sdpCallData, boolean isCallOut) {
        startCall(callTye, iceServers, isCallOut);
        try {
            JSONObject dataObject = new JSONObject(sdpCallData.getData());
            processSdp(dataObject, isCallOut);
        } catch (JSONException e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void stopCall() {
        Log.i(TAG, "stop call");
        if (currentCall == null) {
            Log.e(TAG, "Could not found any call");
            return;
        }
        currentCall.setListener(null);
        currentCall.stopCall();
        currentCall.cleanupSdk();
        currentCall = null;
        isCallOut = false;
        callDataQueue.clear();
        Log.i(TAG, "+++++++++++++++++++++++ Call is stopped");
    }

    public void startReportQuality() {
        currentCall.getStats(this);
    }

    public void setMute() {
        if (currentCall != null) {
            currentCall.setMicrophoneMute(mApplication.getCallBusiness().isMute());
        }
    }

    public void addCallDataToQueue(CallData callData) {
        callDataQueue.add(callData);
    }

    public void putAllCallDataFromQueue() {
        while (!callDataQueue.isEmpty()) {
            CallData callData = callDataQueue.get(0);
            callDataQueue.remove(0);
            addCallData(callData, isCallOut);
        }
    }

    public void addCallData(CallData callData, boolean isCallOut) {
        Log.i(TAG, "addCallData");
        try {
            JSONObject dataObject = new JSONObject(callData.getData());
            if (currentCall != null) {
                if ("sdp".equalsIgnoreCase(callData.getType())) {
                    processSdp(dataObject, isCallOut);
                } else if ("candidate".equalsIgnoreCase(callData.getType())) {
                    processCandidate(dataObject, isCallOut);
                }
            } else {
                Log.i(TAG, "===================== luu data vao queue");
                callDataQueue.add(callData);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Exception", e);
        }
    }

    private void processSdp(JSONObject data, boolean isCallOut) throws JSONException {
        int sdpType = data.getInt("type");
        String sdp = data.getString("sdp");
        StringeeSessionDescription.Type type = StringeeSessionDescription.Type.OFFER;
        if (sdpType == 0) {
            type = StringeeSessionDescription.Type.OFFER;
            Log.i("Stringee", "+++++++++++++++++++++++ sdp OFFER received");
        } else if (sdpType == 1) {
            type = StringeeSessionDescription.Type.PRANSWER;
        } else if (sdpType == 2) {
            Log.i(TAG, "+++++++++++++++++++++++ sdp ANSWER received");
            type = StringeeSessionDescription.Type.ANSWER;
        }
        StringeeSessionDescription sessionDescription = new StringeeSessionDescription(type, sdp);
        currentCall.setRemoteDescription(sessionDescription);
    }

    private void processCandidate(JSONObject data, boolean isCallOut) throws JSONException {
        Log.i(TAG, "+++++ co goi tin signaling ... candidate");
        String sdpMid = data.getString("sdpMid");
        int sdpMLineIndex = data.getInt("sdpMLineIndex");
        String sdp2;
        sdp2 = data.getString("sdp");
        StringeeIceCandidate iceCandidate = new StringeeIceCandidate(sdpMid, sdpMLineIndex, sdp2);
        currentCall.addIceCandidate(iceCandidate, isCallOut);

    }

    */
/**
     * add StringeeCallListener
     *//*


    @Override
    public void didCreateSDP(StringeeSessionDescription sessionDescription) {
        Log.f(TAG, "didCreateSDP : " + sessionDescription);
        if (isCallOut) {
            mApplication.getCallBusiness().processSendCallOutData(sessionDescription.description, true);//TODO send call out sdp data
        } else {
            try {
                JSONObject data = new JSONObject();
                data.put("sdp", sessionDescription.description);
                if (sessionDescription.type == StringeeSessionDescription.Type.OFFER) {
                    data.put("type", 0);
                } else if (sessionDescription.type == StringeeSessionDescription.Type.ANSWER) {
                    data.put("type", 2);
                }
                CallData callData = new CallData("sdp", data.toString());
                */
/*if (isCallOut) {
                    mApplication.getCallBusiness().processSendCallOutData(callData); //TODO send call out sdp data
                } else {
                }*//*

                mApplication.getCallBusiness().processSendCallData(callData, true);
            } catch (JSONException e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    @Override
    public void didGenerateIceCandidate(StringeeIceCandidate iceCandidate) {
        Log.f(TAG, "didGenerateIceCandidate : " + iceCandidate);
        if (isCallOut) {
            mApplication.getCallBusiness().processSendCallOutData(iceCandidate.sdp, false);//TODO send call out candidate data
        } else {
            try {
                JSONObject candidate = new JSONObject();
                candidate.put("sdpMid", iceCandidate.sdpMid);
                candidate.put("sdpMLineIndex", iceCandidate.sdpMLineIndex);
                candidate.put("sdp", iceCandidate.sdp);
                CallData callData = new CallData("candidate", candidate.toString());
                */
/*if (isCallOut) {
                    mApplication.getCallBusiness().processSendCallOutData(callData);//TODO send call out candidate data
                } else {
                }*//*

                mApplication.getCallBusiness().processSendCallData(callData, false);
            } catch (JSONException e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    @Override
    public void didChangeConnectionState(StringeeConnectionState state) {
        Log.f(TAG, "didChangeConnectionState: " + state);
        CallBusiness callBusiness = mApplication.getCallBusiness();
        if (state == StringeeConnectionState.CHECKING) {// connecting
            //callBusiness.notifyOnConnectStateChange(CallConstant.STATE.CONNECTING);
        } else if (state == StringeeConnectionState.CONNECTED) {// connected
            if (isCallOut) {
                callBusiness.notifyOnConnectStateChange(CallConstant.STATE.CALLEE_STARTED);
            } else {
                callBusiness.notifyOnConnectStateChange(CallConstant.STATE.CONNECTED);
            }
        }
    }



    @Override
    public void onComplete(StatsReport statsReport) {
        Log.d(TAG, "StatsReport complete: " + statsReport.toString());
    }

    public static LinkedList<StringeeIceServer> convertStringeeIceServers(ArrayList<IceServer> iceServers) {
        if (iceServers == null) return new LinkedList<>();
        LinkedList<StringeeIceServer> stringeeIceServers = new LinkedList<>();
        for (IceServer iceServer : iceServers) {
            stringeeIceServers.add(new StringeeIceServer(iceServer.getDomain(), iceServer.getUser(), iceServer.getCredential()));
        }
        return stringeeIceServers;
    }

    public static ArrayList<IceServer> convertIceServers(LinkedList<StringeeIceServer> stringeeIceServers) {
        ArrayList<IceServer> iceServers = new ArrayList<>();
        if (stringeeIceServers != null) {
            for (StringeeIceServer stringeeServer : stringeeIceServers) {
                iceServers.add(new IceServer(stringeeServer.username, stringeeServer.password, stringeeServer.uri));
            }
        }
        return iceServers;
    }
}*/

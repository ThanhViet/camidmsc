package com.metfone.selfcare.business;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.datasource.ImageProfileDataSource;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static com.bumptech.glide.load.DecodeFormat.DEFAULT;

/**
 * Created by thanhnt72 on 9/5/2015.
 */
public class ImageProfileBusiness {
    private static final String TAG = ImageProfileBusiness.class.getSimpleName();
    private static final int DEFAULT_MAX_IMAGE = 20;
    public static final String FILE_PATH = "file://";
    public static final String PATH_PROFILE_DEFAULT = "assets://profile/img_profile_default.png";
    private ImageProfileDataSource mImageProfileDataSource;
    private ArrayList<ImageProfile> imageProfileList = new ArrayList<>();
    private ApplicationController mApplication;
    private RequestOptions imageProfileOptions, imageCoverOptions, imageAvatarCallOptions;
    private ImageProfile imageCover;
    private int maxAlbumImages = DEFAULT_MAX_IMAGE;

    private Handler handler;

    public ImageProfileBusiness(ApplicationController application) {
        this.mApplication = application;
        Log.d(TAG, "ImageProfileBusiness");
        handler = new Handler(Looper.getMainLooper());
    }

    public void init() {
        mImageProfileDataSource = ImageProfileDataSource.getInstance(mApplication);
        initDisplayImageOptions();
        loadAllImageProfile();

        String maxSize = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG
                .IMAGE_PROFILE_MAX_SIZE);
        if (!TextUtils.isEmpty(maxSize)) {
            maxAlbumImages = Integer.valueOf(maxSize);
        }
    }

    public void setMaxAlbumImages(int maxAlbumImages) {
        this.maxAlbumImages = maxAlbumImages;
    }

    public void loadAllImageProfile() {
        Log.d(TAG, "loadAllImageProfile");
        imageProfileList = mImageProfileDataSource.getAllImageProfile();
        imageCover = mImageProfileDataSource.getImageCover();
    }

    public void insertImageProfile(ImageProfile imageProfile) {
        if (imageProfile != null
                && imageProfile.isUploaded()
                && imageProfileList.size() < maxAlbumImages) {
            Log.d(TAG, "insertImageProfile");
            mImageProfileDataSource.insertImageProfile(imageProfile);
            imageProfileList.add(0, imageProfile);
        }
    }

    public void insertListImageProfile(ArrayList<ImageProfile> listImages) {
        Log.d(TAG, "insertListImageProfile");
        imageProfileList.addAll(0, listImages);
        Collections.reverse(listImages);
        mImageProfileDataSource.insertListImageProfile(listImages);
    }

    public void updateImageProfile(ImageProfile imageProfile) {
        if (imageProfile != null
                && imageProfile.isUploaded()
                && imageProfileList.size() <= maxAlbumImages) {
            Log.d(TAG, "updateImageProfile");
            mImageProfileDataSource.updateImageProfile(imageProfile);
            // TODO test update data in arraylist
            for (int i = 0; i < imageProfileList.size(); i++) {
                if (imageProfile.getId() == imageProfileList.get(i).getId()) {
                    imageProfileList.set(i, imageProfile);
                    break;
                }
            }
            // imageProfileList.add(imageProfile);
        }
    }

    public void deleteImageProfile(ImageProfile imageProfile, int index) {
        Log.d(TAG, "deleteImageProfile");
        try {
            imageProfileList.remove(index);
            if (!TextUtils.isEmpty(imageProfile.getImagePathLocal())) {
                File file = new File(ImageProfileBusiness.FILE_PATH + imageProfile.getImagePathLocal());
                if (file.exists()) {
                    Log.d(TAG, "deleteImageProfile path=" + imageProfile.getImagePathLocal());
                    file.delete();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "delete file exception", e);
        }
        mImageProfileDataSource.deleteImageProfile(imageProfile);
    }

    public void insertOrUpdateCover(ImageProfile cover) {
        if (cover == null)
            return;

        if (cover.getId() == 0) {
            mImageProfileDataSource.insertImageProfile(cover);
        } else {
            mImageProfileDataSource.updateImageProfile(cover);
        }

        imageCover = mImageProfileDataSource.getImageCover();
    }

    public ArrayList<ImageProfile> getImageProfileList() {
        return imageProfileList;
    }

    public void deleteAllImageProfile() {
        mImageProfileDataSource.deleteAllTable();
        Log.d(TAG, "deleteAllImageProfile");
        imageProfileList = new ArrayList<>();
    }

    private void initDisplayImageOptions() {
        imageProfileOptions = new RequestOptions()
                .placeholder(R.color.bg_onmedia_content_item)
                .error(R.color.bg_onmedia_content_item)
                .dontAnimate()
                .dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);

        imageAvatarCallOptions = new RequestOptions()
                .placeholder(R.color.avatar_default)
                .error(R.color.avatar_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);

        imageCoverOptions = new RequestOptions()
                .placeholder(R.color.bg_onmedia_content_item)
                .error(R.color.bg_onmedia_content_item)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .format(DEFAULT)
                .skipMemoryCache(false);
    }

    public void displayImageProfile(final ImageView imageView, ImageProfile imageProfile, boolean isSaveDB) {
        if (imageProfile == null) {
            displayDefautImage(imageView);
            Log.d(TAG, "displayImageProfile imageProfile null");
            return;
        }

        DisplayMetrics displayMetrics = imageView.getContext().getResources().getDisplayMetrics();
        int width = (displayMetrics.widthPixels);
        width = width > 600 ? 600 : width;

        String imagePath = imageProfile.getImagePathLocal();
        Log.i(TAG, "displayImageProfile: imagePath: " + imagePath);
        long lastChange = (new Date()).getTime();
        if (TextUtils.isEmpty(imagePath)) {
            Log.i(TAG, "displayImageProfile--- path empty");
            displayAndSaveImageProfile(imageProfile, imageView, lastChange, isSaveDB, width);
        } else {
            File avatarFile = new File(imagePath);
            if (avatarFile.exists()) {
                imagePath = FILE_PATH + imagePath;
                Log.i(TAG, "displayImageProfile--- avatarFile.exists=" + avatarFile.getPath());
                if (imageProfile.getTypeImage() == ImageProfileConstant.IMAGE_COVER) {
                    Log.d(TAG, "displayImageProfile IMAGE_COVER=" + imagePath);
                    Glide.with(mApplication).load(imagePath).apply(imageCoverOptions).into(imageView);
//                    universalImageLoader.displayImage(imagePath, imageView, imageCoverOptions);
                } else {
//                    ImageViewAwareTargetSize targetSize = new ImageViewAwareTargetSize(imageView, width, width);
//                    universalImageLoader.displayImage(imagePath, targetSize, imageProfileOptions);
                    RequestOptions options = imageProfileOptions.override(width, width);
                    Glide.with(mApplication).load(imagePath).apply(options).into(imageView);
                }
            } else {
                Log.i(TAG, "displayImageProfile file not found id=" + imageProfile.getId()
                        + " ; url=" + imageProfile.getImagePathLocal());
                displayAndSaveImageProfile(imageProfile, imageView, lastChange, isSaveDB, width);
            }
        }
    }

    private void displayAndSaveImageProfile(final ImageProfile imageProfile, final ImageView imageView,
                                            final long time, final boolean isSaveDB, int width) {
        //load anh default luon
        if (imageProfile == null) {
            return;
        }
        int imageType = imageProfile.getTypeImage();
        if (imageType == ImageProfileConstant.IMAGE_COVER) {
            Log.d(TAG, "displayAndSaveImageProfile isSaveDB=" + isSaveDB + "; IMAGE_COVER=" + imageProfile
                    .getImageUrl());
            if (TextUtils.isEmpty(imageProfile.getImageUrl())) {
                // displayDefautCover(imageView);
                return;
            }
            new GlideImageLoader(new GlideImageLoader.ImageLoadingListener() {
                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingFailed(String imageUri, GlideException e) {

                }

                @Override
                public void onLoadingComplete(Bitmap loadedImage) {
                    if (loadedImage != null) {
                        if (isSaveDB) {
                            saveImageProfile(imageProfile, loadedImage, time);
                        }
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(loadedImage);
                            }
                        });

                    } else {
//                                displayDefautCover(imageView);
                    }
                }
            }).loadBitmap(imageProfile.getImageUrl(), imageCoverOptions);

//            universalImageLoader.displayImage(imageProfile.getImageUrl(), imageView, imageCoverOptions,
//                    new GlideImageLoader.ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted() {
//
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String imageUri, GlideException e) {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete(Bitmap loadedImage) {
//                            if (loadedImage != null) {
//                                if (isSaveDB) {
//                                    saveImageProfile(imageProfile, loadedImage, time);
//                                }
//                                imageView.setImageBitmap(loadedImage);
//                            } else {
////                                displayDefautCover(imageView);
//                            }
//                        }
//                    });
        } else {
            if (TextUtils.isEmpty(imageProfile.getImageUrl())) {
                displayDefautImage(imageView);
                return;
            }
            Log.d(TAG, "displayAndSaveImageProfile isSaveDB=" + isSaveDB + "; IMAGE_PROFILE=" + imageProfile
                    .getImageUrl());
//            ImageViewAwareTargetSize targetSize = new ImageViewAwareTargetSize(imageView, width, width);
            RequestOptions options = imageProfileOptions.override(width, width);
            new GlideImageLoader(new GlideImageLoader.ImageLoadingListener() {
                @Override
                public void onLoadingStarted() {
//                            Log.e(TAG, "onLoadingStarted");
                    imageView.setImageResource(R.drawable.ic_images_default);
                }

                @Override
                public void onLoadingFailed(String imageUri, GlideException e) {

                }

                @Override
                public void onLoadingComplete(Bitmap loadedImage) {
                    if (loadedImage != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(loadedImage);
                            }
                        });

                        if (isSaveDB) {
                            saveImageProfile(imageProfile, loadedImage, time);
                        }
//                                imageView.setImageBitmap(loadedImage);
                    }
                }
            }).loadBitmap(imageProfile.getImageUrl(), options);

//            universalImageLoader.displayImage(imageProfile.getImageUrl(), targetSize, imageProfileOptions,
//                    new GlideImageLoader.ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted() {
////                            Log.e(TAG, "onLoadingStarted");
//                            imageView.setImageResource(R.drawable.ic_images_default);
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String imageUri, GlideException e) {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete(Bitmap loadedImage) {
//                            if (loadedImage != null) {
//                                if (isSaveDB) {
//                                    saveImageProfile(imageProfile, loadedImage, time);
//                                }
////                                imageView.setImageBitmap(loadedImage);
//                            }
//                        }
//                    });
        }
    }

    private void saveImageProfile(ImageProfile imageProfile, Bitmap bitmap, long time) {
        String filePath;
        File profile = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.PROFILE_PATH);
        if (!profile.exists()) {
            profile.mkdirs();
        }

        if (imageProfile.getTypeImage() == ImageProfileConstant.IMAGE_COVER) {
            filePath = Config.Storage.REENG_STORAGE_FOLDER
//                    + Config.Storage.PROFILE_PATH + "/" + "image_cover.jpg";
                    + Config.Storage.PROFILE_PATH + "/" + "image_cover"
                    + "_" + time + Constants.FILE.JPEG_FILE_SUFFIX;
        } else {
            filePath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.PROFILE_PATH + "/" +
                    ImageHelper.IMAGE_NAME + "_" + imageProfile.getId() + "_" + time + Constants.FILE.JPEG_FILE_SUFFIX;
        }
        ImageHelper.getInstance(mApplication).saveBitmapToPath(bitmap, filePath, Bitmap.CompressFormat.JPEG);
        imageProfile.setImagePathLocal(filePath);
        imageProfile.setTime(time);
        updateImageProfile(imageProfile);
    }

    public void displayDefautImage(ImageView imageView) {
//        universalImageLoader.displayImage(PATH_PROFILE_DEFAULT, imageView, imageProfileOptions);
        Glide.with(mApplication).load(PATH_PROFILE_DEFAULT).apply(imageProfileOptions).into(imageView);
    }

    public void setImageProfileFriend(ImageView imageView, String url, int size) {
//        universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(imageView, size, size),
//                imageProfileOptions);
        RequestOptions options = imageProfileOptions.override(size, size);
        Glide.with(mApplication).load(url).apply(options).into(imageView);
    }

    /*public void displayImageProfileForFeed(ApplicationController mApp, ImageView imageView, float ratio, boolean
            isThumb, int padding, String url) {
        ImageViewAwareTargetSize imgTargetSize = FileHelper.caculateImageViewAware(mApp, imageView, ratio, isThumb,
                padding);
        universalImageLoader.displayImage(url, imgTargetSize, imageProfileOptions);
    }*/

    public void displayImageProfileForFeed(ImageView imgTargetSize, String url, int width, int height) {
//        universalImageLoader.displayImage(url, imgTargetSize, imageProfileOptions);
        RequestOptions options = imageProfileOptions.override(width, height);
        Glide.with(mApplication).load(url).apply(options).into(imgTargetSize);
    }

    public void displayImageSingleFeed(ImageView imgAware, String url, final ProgressLoading progressBar, int width, int height) {
//        SimpleImageLoadingListener simpleImageLoadingListener = new SimpleImageLoadingListener() {
//            @Override
//            public void onLoadingStarted(String imageUri, View view) {
//                super.onLoadingStarted(imageUri, view);
//                progressBar.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                super.onLoadingComplete(imageUri, view, loadedImage);
//                progressBar.setVisibility(View.GONE);
//            }
//        };
//        universalImageLoader.displayImage(url, imgAware, imageProfileOptions, simpleImageLoadingListener);

        new GlideImageLoader(imgAware, new
                GlideImageLoader.SimpleImageLoadingListener() {

                    @Override
                    public void onLoadingStarted() {
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingComplete() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingFailed(GlideException failReason) {
                        progressBar.setVisibility(View.GONE);
                    }
                }).load(url, imageProfileOptions);
    }

    public ImageProfile getImageCover() {
        if (imageCover == null) {
            ImageProfile imageProfile = new ImageProfile();
            imageProfile.setTypeImage(ImageProfileConstant.IMAGE_COVER);
            return imageProfile;
        }
        return imageCover;
    }

    public void displayImageProfileFriend(final ImageView imageView, ImageProfile imageProfile, boolean displayLarge) {
        if (imageProfile == null || TextUtils.isEmpty(imageProfile.getImageUrl())) {
            displayDefautImage(imageView);
            Log.d(TAG, "displayImageProfile imageProfile null");
            return;
        }
        int imageType = imageProfile.getTypeImage();
        if (imageType == ImageProfileConstant.IMAGE_COVER) {
//            universalImageLoader.displayImage(imageProfile.getImageUrl(), imageView, imageCoverOptions);
            Glide.with(mApplication).load(imageProfile.getImageUrl()).apply(imageCoverOptions).into(imageView);
        } else {
            String url;
            if (!displayLarge) {
                url = imageProfile.getImageThumb();
                if (TextUtils.isEmpty(url)) {
                    url = imageProfile.getImageUrl();
                }
                DisplayMetrics displayMetrics = imageView.getContext().getResources().getDisplayMetrics();
                int width = (displayMetrics.widthPixels) / 3;
                width = width > 300 ? 300 : width;
                //ko phai large thi la anh vuong nho
//                ImageViewAwareTargetSize targetSize = new ImageViewAwareTargetSize(imageView, width, width);
//                universalImageLoader.displayImage(url, targetSize, imageProfileOptions);
                RequestOptions options = imageProfileOptions.override(width, width);
                Glide.with(mApplication).load(url).apply(options).into(imageView);
            } else {
                url = imageProfile.getImageUrl();
//                universalImageLoader.displayImage(url, imageView, imageProfileOptions);
                Glide.with(mApplication).load(url).apply(imageProfileOptions).into(imageView);
            }
        }
    }

    public void displayImageProfileFriendWithThumb(final ImageView imageView, final ImageProfile imageProfile) {
        if (imageProfile == null || TextUtils.isEmpty(imageProfile.getImageUrl())) {
            displayDefautImage(imageView);
            Log.d(TAG, "displayImageProfile imageProfile null");
            return;
        }
        int imageType = imageProfile.getTypeImage();
        if (imageType == ImageProfileConstant.IMAGE_COVER) {
//            universalImageLoader.displayImage(imageProfile.getImageUrl(), imageView, imageCoverOptions);
            Glide.with(mApplication).load(imageProfile.getImageUrl()).apply(imageCoverOptions).into(imageView);
        } else {
            String urlThumb = imageProfile.getImageThumb();
            Log.i(TAG, "displayImageProfileFriendWithThumb: urlThumb" + urlThumb + " image: " + imageProfile.toString
                    ());
            if (!TextUtils.isEmpty(urlThumb)) {
                new GlideImageLoader(imageView, new
                        GlideImageLoader.SimpleImageLoadingListener() {

                            @Override
                            public void onLoadingStarted() {

                            }

                            @Override
                            public void onLoadingComplete() {
                                new Handler().post(() -> loadImageBigSize(imageView, imageProfile, false));
                            }

                            @Override
                            public void onLoadingFailed(GlideException failReason) {
                                new Handler().post(() -> loadImageBigSize(imageView, imageProfile, true));
                            }
                        }).load(urlThumb, imageProfileOptions);

//                universalImageLoader.displayImage(urlThumb, imageView, imageProfileOptions, );
            } else {
                new Handler().post(() -> loadImageBigSize(imageView, imageProfile, true));
            }
        }
    }

    private void loadImageBigSize(final ImageView imageView, ImageProfile imageProfile, boolean displayNow) {
        String url = imageProfile.getImageUrl();
        Log.i(TAG, "loadImageBigSize: url: " + url + " imageProfile: " + imageProfile.toString());
        if (!displayNow) {
            new GlideImageLoader(new GlideImageLoader.ImageLoadingListener() {
                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingFailed(String imageUri, GlideException e) {

                }

                @Override
                public void onLoadingComplete(Bitmap loadedImage) {
                    if (loadedImage != null && handler != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(loadedImage);
                            }
                        });

                    }
                }
            }).loadBitmap(url, imageProfileOptions);

//            universalImageLoader.loadImage(url, imageProfileOptions, new GlideImageLoader.ImageLoadingListener() {
//                @Override
//                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                    super.onLoadingComplete(imageUri, view, loadedImage);
//                    if (loadedImage != null) {
//                        imageView.setImageBitmap(loadedImage);
//                    }
//                }
//            });
        } else {
//            universalImageLoader.displayImage(url, imageView, imageProfileOptions);
            Glide.with(mApplication).load(url).apply(imageProfileOptions).into(imageView);
        }
    }

    public void displayAvatarCallImage(final ImageView imageView, String avatarUrl, GlideImageLoader.SimpleImageLoadingListener
            listener) {
        if (TextUtils.isEmpty(avatarUrl)) {
            if (listener != null)
                listener.onLoadingFailed(null);
        } else {
            new GlideImageLoader(imageView, listener).load(avatarUrl, imageAvatarCallOptions);
//            universalImageLoader.displayImage(avatarUrl, imageView, imageAvatarCallOptions, listener);
        }
    }

//    public Bitmap getBitmapFromCache(String url) {
//        List<Bitmap> bitmaps = MemoryCacheUtils.findCachedBitmapsForImageUri(url, universalImageLoader.getMemoryCache
//                ());
//        if (bitmaps != null && !bitmaps.isEmpty()) {
//            return bitmaps.get(0);
//        }
//        return null;
//    }

    public int getImageIndex(int totalImages, int imageViewIdx) {
        if (totalImages <= 3) {
            return imageViewIdx;
        } else if (totalImages == 5) {
            if (imageViewIdx < 3) return imageViewIdx + 2;
            else return imageViewIdx - 3;
        } else if (totalImages == 4) {
            if (imageViewIdx < 2) return imageViewIdx + 2;
            else return imageViewIdx - 3;
        }
        return -1;
    }

    public int getMaxAlbumImages() {
        return maxAlbumImages;
    }

    public int getCurrentAlbumSize() {
        return imageProfileList.size();
    }
}
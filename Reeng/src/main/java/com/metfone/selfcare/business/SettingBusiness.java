package com.metfone.selfcare.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.TextUtils;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.ConfigLocalized;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by toanvk2 on 7/15/14.
 */
public class SettingBusiness {
    private static SettingBusiness mInstance;
    private final String TAG = SettingBusiness.class.getSimpleName();
    private ArrayList<String> mListSilentThreadId;
    private ArrayList<String> mListSmartNotifyThreadId;
    private HashSet<Integer> mListAutoSmsout = new HashSet<>();
    private Context mContext;
    private SharedPreferences mPref;
    private boolean isPreviewMessage = true;
    private boolean isSettingOnNoti = true;
    private boolean isKeyboardSend;
    private boolean isSystemSound = false;
    private boolean isSystemSoundDefault = true;// dùng âm mặc định của máy hay ng dung da chon am #
    private boolean enableAutoSmsOut = true;
    private boolean enableReceiveSmsOut = true;
    private Uri notificationSoundUri = null;
    private Uri notificationDefaultUri;
    private int fontSize;

    public SettingBusiness(Context context) {
        this.mContext = context;
        init();
    }

    public static SettingBusiness getInstance(Context context) {
        if (mInstance == null) {
            synchronized (SettingBusiness.class) {
                if (mInstance == null) {
                    mInstance = new SettingBusiness(context);
                }
            }
        }
        return mInstance;
    }

    public void init() {
        if (mContext == null) return;
        this.mPref = mContext.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        setListSilentThreadId(getListThreadIdFromPref());
        setListSmartNotifyThreadId(getListThreadIdFromPref());
        isPreviewMessage = mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_PREVIEW_MSG,
                Constants.PREF_DEFAULT.PREF_DEF_PREVIEW_MSG);
        isSettingOnNoti = mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_ON_NOTI, Constants.PREF_DEFAULT.PREF_DEF_SETTING_ON_OFF_NOTI);
        isKeyboardSend = getPrefSetupKeyboardSend();
        enableAutoSmsOut = mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_AUTO_SMSOUT, true);
        enableReceiveSmsOut = mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_RECEIVE_SMSOUT, true);
        fontSize = mPref.getInt(Constants.PREFERENCE.PREF_SETTING_FONT_SIZE, Constants.FONT_SIZE.MEDIUM);
        initNotificationSound();
        checkSettingNotiOn();
    }

    private ArrayList<String> getListSilentThreadId() {
        return mListSilentThreadId;
    }

    private void setListSilentThreadId(ArrayList<String> threadIds) {
        this.mListSilentThreadId = threadIds;
    }

    public HashSet<Integer> getListAutoSmsout() {
        return mListAutoSmsout;
    }

    public void setListAutoSmsout(HashSet<Integer> mListAutoSmsout) {
        this.mListAutoSmsout = mListAutoSmsout;
    }

    private ArrayList<String> getListSmartNotifyThreadId() {
        return mListSmartNotifyThreadId;
    }

    private void setListSmartNotifyThreadId(ArrayList<String> threadIds) {
        this.mListSmartNotifyThreadId = threadIds;
    }

    public boolean checkExistInSmartNotifyThread(int threadId) {
        if (threadId < 0) {
            return false;
        }
        if (getListSmartNotifyThreadId() == null || getListSmartNotifyThreadId().isEmpty()) {
            return false;
        }
        for (String id : getListSmartNotifyThreadId()) {
            if (id.equals(String.valueOf(threadId))) {
                return true;
            }
        }
        return false;
    }

    public void updateSmartNotifyThread(int threadId, boolean isAdd) {
        if (isAdd) {
            addToListSmartNotifyThread(threadId);
            //            getListSmartNotifyThreadId().add(String.valueOf(threadId));
        } else {
            removeFromListSmartNotifyThread(threadId);
        }
        Log.d(TAG, "" + convertListThreadIdToString(mListSmartNotifyThreadId));
        setListThreadIdToPref(getListSilentThreadId());
    }

    private void removeFromListSmartNotifyThread(int threadId) {
        mListSmartNotifyThreadId.remove(String.valueOf(threadId));
    }

    private void addToListSmartNotifyThread(int threadId) {
        mListSmartNotifyThreadId.add(String.valueOf(threadId));
    }

    private String convertListThreadIdToString(ArrayList<String> threadIds) {
        JSONArray jsonArray = new JSONArray();
        for (String item : threadIds) {
            jsonArray.put(item);
        }
        return jsonArray.toString();
    }

    private String convertListThreadIdToString(HashSet<Integer> threadIds) {
        JSONArray jsonArray = new JSONArray();
        for (int item : threadIds) {
            jsonArray.put(item);
        }
        return jsonArray.toString();
    }

    private ArrayList<String> convertStringToListThreadId(String content) {
        if (content == null || content.length() <= 0) {
            return null;
        }
        try {
            ArrayList<String> listThread = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(content);
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                listThread.add(jsonArray.get(i).toString());
            }
            return listThread;
        } catch (Exception e) {
            Log.e(TAG, "convertStringToListThreadId", e);
            return null;
        }
    }

    private void setListThreadIdToPref(ArrayList<String> listThreadId) {
        String listString = convertListThreadIdToString(listThreadId);
        mPref.edit().putString(Constants.PREFERENCE.PREF_NO_NOTIF_LIST_ID, listString).apply();
    }

    private ArrayList<String> getListThreadIdFromPref() {
        String temp = mPref.getString(Constants.PREFERENCE.PREF_NO_NOTIF_LIST_ID, "");
        ArrayList<String> listThreadId = convertStringToListThreadId(temp);
        if (listThreadId == null) {
            return new ArrayList<>();
        }
        return listThreadId;
    }

    public boolean checkExistInSilentThread(int threadId) {
        if (threadId < 0) {
            return false;
        }
        if (getListSilentThreadId() == null || getListSilentThreadId().isEmpty()) {
            return false;
        }
        for (String id : getListSilentThreadId()) {
            if (id.equals(String.valueOf(threadId))) {
                return true;
            }
        }
        return false;
    }

    public void updateSilentThread(int threadId, boolean isAdd) {
        if (isAdd) {
            addToListSilentThread(threadId);
        } else {
            removeFromListSilentThread(threadId);
        }
        Log.d(TAG, "" + convertListThreadIdToString(mListSilentThreadId));
        setListThreadIdToPref(getListSilentThreadId());
    }

    private void removeFromListSilentThread(int threadId) {
        mListSilentThreadId.remove(String.valueOf(threadId));
    }

    private void addToListSilentThread(int threadId) {
        mListSilentThreadId.add(String.valueOf(threadId));
    }

    public boolean checkExistAutoSmsoutThread(int threadId) {

        if (threadId < 0) {
            return false;
        }
        if (mListAutoSmsout == null || mListAutoSmsout.isEmpty()) {
            return false;
        }
        if (mListAutoSmsout.contains(threadId)) return true;
        return false;
    }

    public void updateAutoSmsoutThread(int threadId, boolean isAdd) {
        if (isAdd) {
            addToListAutoSmsoutThread(threadId);
        } else {
            removeFromListAutoSmsoutThread(threadId);
        }
        setListAutoSmsoutThreadIdToPref(mListAutoSmsout);
    }

    private void setListAutoSmsoutThreadIdToPref(HashSet<Integer> mListAutoSmsout) {
        String listString = convertListThreadIdToString(mListAutoSmsout);
        mPref.edit().putString(Constants.PREFERENCE.PREF_THREAD_SMSOUT_LIST_ID, listString).apply();
    }

    private void removeFromListAutoSmsoutThread(int threadId) {
        mListAutoSmsout.remove(threadId);
    }

    private void addToListAutoSmsoutThread(int threadId) {
        mListAutoSmsout.add(threadId);
    }

    //cac ham get set pref setting
    public void setSettingPref(String key, boolean value) {
        mPref.edit().putBoolean(key, value).apply();
    }

    public boolean getPrefVibrate() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_VIBRATE, Constants.PREF_DEFAULT.PREF_DEF_VIBRATE);
    }

    public void setPrefVibrate(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_VIBRATE, value).apply();
    }

    public boolean getPrefRingtone() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_RINGTONE, Constants.PREF_DEFAULT.PREF_DEF_RINGTONE);
    }

    public void setPrefRingtone(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_RINGTONE, value).apply();
    }

    public boolean getPrefMsgInPopup() {
        return isSettingOnNoti && mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_MSG_IN_POPUP, Constants.PREF_DEFAULT
                .PREF_DEF_MSG_IN_POPUP);
    }

    public void setPrefMsgInPopup(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_MSG_IN_POPUP, value).apply();
    }

    public boolean getPrefEnableUnlock() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_ENABLE_UNLOCK, Constants.PREF_DEFAULT
                .PREF_DEF_ENABLE_UNLOCK);
    }

    public void setPrefEnableUnlock(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_ENABLE_UNLOCK, value).apply();
    }

    public boolean getPrefAutoPlaySticker() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_AUTO_PLAY_STICKER, true);
    }

    public void setPrefAutoPlaySticker(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_AUTO_PLAY_STICKER, value).apply();
    }

    public void setPrefShowMedia(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SHOW_MEDIA, value).apply();
        if (value) {
            FileHelper.deleteNoMediaFile(mContext);
        } else {
            FileHelper.createNoMediaFile(mContext);
        }
    }

    public void setPrefReplySms(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_REPLY_SMS, value).apply();
    }

    public boolean getPrefEnableSeen() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_ENABLE_SEEN, Constants.PREF_DEFAULT
                .PREF_DEF_ENABLE_SEEN);
    }

    public boolean getPrefBirthdayReminder() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_BIRTHDAY_REMINDER, Constants.PREF_DEFAULT
                .PREF_DEF_BIRTHDAY_REMINDER);
    }

    public boolean getPrefShowMedia() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SHOW_MEDIA, ConfigLocalized.PREF_DEF_SHOW_MEDIA);
    }

    public boolean getPrefReplySms() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_REPLY_SMS, ConfigLocalized.PREF_DEF_REPLY_SMS);
    }

    public void setPrefEnableSeen(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_ENABLE_SEEN, value).apply();
    }

    // TODO: [START] Cambodia version
    public void setPrefEnableWatchingCinemaActivity(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_WATCHING_CINEMA_ACTIVITY_ENABLE, value).apply();
    }

    public boolean getPrefEnableWatchingCinemaActivity() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_WATCHING_CINEMA_ACTIVITY_ENABLE,
                Constants.PREF_DEFAULT.PREF_DEF_ENABLE_WATCHING_CINEMA_ACTIVITY);
    }
    // TODO: [END] Cambodia version

    public void setPrefBirthdayReminder(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_BIRTHDAY_REMINDER, value).apply();
    }

    //image quality
    public void setPrefEnableHDImage(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_IMAGE_HD, value).apply();
    }

    public boolean getPrefEnableHDImage() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_IMAGE_HD, Constants.PREF_DEFAULT
                .PREF_DEF_IMAGE_HD);
    }

    //auto play music in room
    public void setPrefOffMusicInRoom(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_OFF_MUSIC_IN_ROOM, value).apply();
    }

    public boolean getPrefOffMusicInRoom() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_OFF_MUSIC_IN_ROOM, Constants.PREF_DEFAULT
                .PREF_DEF_PLAYMUSIC_IN_ROOM);
    }

    /**
     * Setting Notify New User
     *
     * @return
     */
    public boolean getPrefSettingNotifyNewUser() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_NOTIFY_NEW_FRIEND, Constants.PREF_DEFAULT
                .PREF_DEF_NOTIFY_NEW_FRIEND);
    }

    public void setPrefSettingNotifyNewUser(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_NOTIFY_NEW_FRIEND, value).apply();
    }

    public boolean getPrefPreviewMsg() {
        /*return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_PREVIEW_MSG, Constants.PREF_DEFAULT
                .PREF_DEF_PREVIEW_MSG);*/
        return isPreviewMessage;
    }

    public void setPrefPreviewMsg(boolean value) {
        isPreviewMessage = value;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_PREVIEW_MSG, value).apply();
    }

    public boolean getPrefChatWithStranger() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_ONMEDIA_CHAT_WITH_STRANGER,
                Constants.PREF_DEFAULT.PREF_DEF_ONMEDIA_CHAT_WITH_STRANGER);
    }

    public void setPrefChatWithStranger(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_ONMEDIA_CHAT_WITH_STRANGER, value).apply();
    }

    public boolean getPrefNotifyNewFeed() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_ONMEDIA_NOTIFY_NEW_FEED,
                Constants.PREF_DEFAULT.PREF_DEF_ONMEDIA_NOTIFY_NEW_FEED);
    }

    public void setPrefNotifyNewFeed(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_ONMEDIA_NOTIFY_NEW_FEED, value).apply();
    }

    public boolean getPrefSecureWeb() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_ONMEDIA_SECURE_WEB,
                Constants.PREF_DEFAULT.PREF_DEF_ONMEDIA_SECURE_WEB);
    }

    public void setPrefSecureWeb(boolean value) {
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_ONMEDIA_SECURE_WEB, value).apply();
    }

    /**
     * on/off keyboard send
     *
     * @return
     */
    private boolean getPrefSetupKeyboardSend() {
        return mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_SETUP_KEYBOARD_SEND, ConfigLocalized
                .PREF_DEF_SETUP_KEYBOARD_SEND);
    }

    public void setPrefSetupKeyboardSend(boolean value) {
        isKeyboardSend = value;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_SETUP_KEYBOARD_SEND, value).apply();
    }

    public boolean isSetupKeyboardSend() {
        return isKeyboardSend;
    }

    public void setPrefNotificationSound(Uri soundUri) {
        isSystemSoundDefault = false;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_NOTIFICATION_SOUND_DEFAULT, isSystemSoundDefault)
                .apply();
        notificationSoundUri = soundUri;
        if (soundUri == null) {
            mPref.edit().putString(Constants.PREFERENCE.PREF_SETTING_NOTIFICATION_SOUND, null).apply();
        } else {
            mPref.edit().putString(Constants.PREFERENCE.PREF_SETTING_NOTIFICATION_SOUND, soundUri.toString()).apply();
        }
    }

    public void setPrefSystemSound(boolean isSystem) {
        isSystemSound = isSystem;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_SYSTEM_SOUND, isSystemSound).apply();
    }

    public boolean isPrefSystemSound() {
        return isSystemSound;
    }

    public boolean isEnableAutoSmsOut() {
        return enableAutoSmsOut;
    }

    public void setPrefEnableAutoSmsOut(boolean enableAutoSmsOut) {
        this.enableAutoSmsOut = enableAutoSmsOut;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_AUTO_SMSOUT, enableAutoSmsOut).apply();
    }

    public boolean isEnableReceiveSmsOut() {
        return enableReceiveSmsOut;
    }

    public void setPrefEnableReceiveSmsOut(boolean enableReceiveSmsOut) {
        this.enableReceiveSmsOut = enableReceiveSmsOut;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_RECEIVE_SMSOUT, enableReceiveSmsOut).apply();
    }

    private void initNotificationSound() {
        //notificationDefaultUri = Uri.parse("android.resource://" + mContext.getPackageName() + "/" + com.viettel
        // .mocha.app.R.raw.receive_message);
        notificationDefaultUri = Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.receive_message);
        isSystemSound = mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_SYSTEM_SOUND, false);
        isSystemSoundDefault = mPref.getBoolean(Constants.PREFERENCE.PREF_SETTING_NOTIFICATION_SOUND_DEFAULT, true);
        String sound = mPref.getString(Constants.PREFERENCE.PREF_SETTING_NOTIFICATION_SOUND, null);//RingtoneManager
        // .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString()
        if (isSystemSoundDefault) {
            notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        } else {
            if (TextUtils.isEmpty(sound)) {
                notificationSoundUri = null;
            } else {
                notificationSoundUri = Uri.parse(sound);
            }
        }
    }

    public Uri getCurrentSoundUri() {
        if (isSystemSound) {
            return notificationSoundUri;
        } else {
            return notificationDefaultUri;
        }
    }

    public Uri getNotificationSoundUri() {
        return notificationSoundUri;
    }

    /*public Uri getNotificationDefaultUri() {
        return notificationDefaultUri;
    }*/

    public String getNotificationSoundName() {
        if (notificationSoundUri == null) {
            return mContext.getResources().getString(R.string.setting_select_sound_silent);
        } else {
            return RingtoneManager.getRingtone(mContext, notificationSoundUri).getTitle(mContext);
        }
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
        mPref.edit().putInt(Constants.PREFERENCE.PREF_SETTING_FONT_SIZE, fontSize).apply();
    }

    public int getFontSize() {
        return fontSize;
    }

    public long getTimeToStartSettingNoti() {
        if (isSettingOnNoti) return -1;
        long untilTime = getSettingUntilTime();
        if (untilTime == -1) {
            //đây là trường hợp đang on noti
            return -1l;
        }

        //tính toán thời gian còn lại để start work on lại notification setting
        long duration = untilTime - System.currentTimeMillis();
        if (duration < 0) {
            return 0;
        }
        return duration;
    }

    public long getSettingUntilTime() {
        return mPref.getLong(Constants.PREFERENCE.PREF_SETTING_ON_NOTI_UTIL_TIME, -1);
    }


    public void checkSettingNotiOn() {
        if (!Config.Features.FLAG_SUPPORT_ON_OFF_NOTIFICATION) {
            isSettingOnNoti = true;
            return;
        }
        if (!isSettingOnNoti) {
            long duration = getTimeToStartSettingNoti();
            if (duration <= 0) {
                setPrefSettingOnOffNoti(true, -1);
                return;
            }
        }
    }

    public void setPrefSettingOnOffNoti(boolean value, int position) {
        if (!Config.Features.FLAG_SUPPORT_ON_OFF_NOTIFICATION) return;
        isSettingOnNoti = value;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SETTING_ON_NOTI, value).apply();
        long currentTime = System.currentTimeMillis();
        switch (position) {
            case -1:
                mPref.edit().putLong(Constants.PREFERENCE.PREF_SETTING_ON_NOTI_UTIL_TIME, -1L).apply();
                break;

            case 0:
                mPref.edit().putLong(Constants.PREFERENCE.PREF_SETTING_ON_NOTI_UTIL_TIME, currentTime + Constants.PREF_DEFAULT.PREF_DEF_SETTING_NOTIFY_TIME_15M).apply();
                break;

            case 1:
                mPref.edit().putLong(Constants.PREFERENCE.PREF_SETTING_ON_NOTI_UTIL_TIME, currentTime + Constants.PREF_DEFAULT.PREF_DEF_SETTING_NOTIFY_TIME_1H).apply();
                break;

            case 2:
                mPref.edit().putLong(Constants.PREFERENCE.PREF_SETTING_ON_NOTI_UTIL_TIME, currentTime + Constants.PREF_DEFAULT.PREF_DEF_SETTING_NOTIFY_TIME_8H).apply();
                break;

            case 3:
                mPref.edit().putLong(Constants.PREFERENCE.PREF_SETTING_ON_NOTI_UTIL_TIME, currentTime + Constants.PREF_DEFAULT.PREF_DEF_SETTING_NOTIFY_TIME_24H).apply();
                break;

            default:
                mPref.edit().putLong(Constants.PREFERENCE.PREF_SETTING_ON_NOTI_UTIL_TIME, -1L).apply();
                break;
        }
    }

    public boolean getPrefSettingOnNoti() {
        if (!Config.Features.FLAG_SUPPORT_ON_OFF_NOTIFICATION) return true;
        return isSettingOnNoti;
    }

    public long getPrefSettingOnOffNotiTime(int pos) {
        if (mPref == null) return -1;
        if (pos < 0) return -1;
        if (pos == 0) return Constants.PREF_DEFAULT.PREF_DEF_SETTING_NOTIFY_TIME_15M;
        if (pos == 1) return Constants.PREF_DEFAULT.PREF_DEF_SETTING_NOTIFY_TIME_1H;
        if (pos == 2) return Constants.PREF_DEFAULT.PREF_DEF_SETTING_NOTIFY_TIME_8H;
        if (pos == 3) return Constants.PREF_DEFAULT.PREF_DEF_SETTING_NOTIFY_TIME_24H;
        return -1;
    }
}
package com.metfone.selfcare.business;

import android.content.res.Resources;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.datasource.StrangerDataSource;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.jivesoftware.smack.packet.ReengMusicPacket;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thaodv on 5/8/2015.
 */
public class StrangerBusiness {
    private static final String TAG = StrangerBusiness.class.getSimpleName();
    public static final String TAG_SOCIAL_REQUEST = "SOCIAL_REQUEST";
    private ApplicationController mApplication;
    private Resources mRes;
    private ReengAccountBusiness mAcountBusiness;
    private StrangerDataSource mStrangerDatasource;
    private List<StrangerPhoneNumber> allStrangerPhoneNumbers;
    private HashMap<String, StrangerPhoneNumber> mHashMapStrangers = new HashMap<>();

    public static final int TYPE_SOCIAL_FRIEND = 1;
    public static final int TYPE_SOCIAL_BE_FOLLOWED = 2;
    public static final int TYPE_SOCIAL_SUGGEST = 3;

    private ArrayList<SocialFriendInfo> mSocialsFriend = new ArrayList<>();
    private ArrayList<SocialFriendInfo> mSocialsBeFollowed = new ArrayList<>();
    private ArrayList<SocialFriendInfo> mSocialsSuggest = new ArrayList<>();
    private String pageFriend = "", pageBeFollowed = "", pageSuggest = "";

    public StrangerBusiness(ApplicationController application) {
        mApplication = application;
        mRes = mApplication.getResources();
    }

    public void init() {
        mStrangerDatasource = StrangerDataSource.getInstance(mApplication);
        loadAllStrangerPhoneNumber();
    }

    public void loadAllStrangerPhoneNumber() {
        allStrangerPhoneNumbers = mStrangerDatasource.getAllStrangerPhoneNumber();
        for (StrangerPhoneNumber item : allStrangerPhoneNumbers) {
            mHashMapStrangers.put(item.getPhoneNumber(), item);
        }
    }

    public List<StrangerPhoneNumber> getAllStrangerNumber() {
        return allStrangerPhoneNumbers;
    }

    public StrangerPhoneNumber getExistStrangerPhoneNumberFromNumber(String friendJid) {
        if (TextUtils.isEmpty(friendJid) || allStrangerPhoneNumbers == null ||
                allStrangerPhoneNumbers.isEmpty()) {
            return null;
        }
        return mHashMapStrangers.get(friendJid);
    }

    private boolean isExits(StrangerPhoneNumber number, ArrayList<StrangerPhoneNumber> list) {
        String friendJid = number.getPhoneNumber();
        if (TextUtils.isEmpty(friendJid) || list == null || list.isEmpty()) {
            return false;
        }
        for (StrangerPhoneNumber strangerPhoneNumber : list) {
            if (friendJid.equals(strangerPhoneNumber.getPhoneNumber())) {
                return true;
            }
        }
        return false;
    }

    //
    private void insertStrangerPhoneNumber(StrangerPhoneNumber strangerPhoneNumber) {
        Log.d(TAG, "insertStrangerPhoneNumber" + strangerPhoneNumber.getAppId());
        mStrangerDatasource.insertStrangerPhoneNumber(strangerPhoneNumber);
        allStrangerPhoneNumbers.add(strangerPhoneNumber);
        mHashMapStrangers.put(strangerPhoneNumber.getPhoneNumber(), strangerPhoneNumber);
    }

    public void updateStrangerPhoneNumber(StrangerPhoneNumber strangerPhoneNumber) {
        mStrangerDatasource.updateStrangerPhoneNumber(strangerPhoneNumber);
    }

    public void deleteStrangerPhoneNumber(StrangerPhoneNumber strangerPhoneNumber) {
        mStrangerDatasource.deleteStrangerPhoneNumber(strangerPhoneNumber);
        loadAllStrangerPhoneNumber();
    }

    /**
     * check message lam quen neu so chua co thread, hoac chua luu danh ba thi tao thread lam quen
     * insert them vao bang stranger, neu da co thi khong them vao bang stranger
     *
     * @param friendJid
     * @param reengMessagePacket
     * @return
     */
    public ThreadMessage processInviteFriend(String friendJid, ReengMessagePacket reengMessagePacket) {
        Log.d(TAG, "processInviteFriend");
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(friendJid);
        ThreadMessage threadSolo = messageBusiness.findExistingSoloThread(friendJid);
        if (threadSolo != null) {// thread da ton tai
            if (threadSolo.isStranger()) {
                if (threadSolo.getStrangerPhoneNumber() == null ||
                        threadSolo.getStrangerPhoneNumber().getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                    StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid, reengMessagePacket, false);
                    threadSolo.setStrangerPhoneNumber(strangerPhoneNumber);
                }
            } else if (threadSolo.getAllMessages() == null || threadSolo.getAllMessages().isEmpty()) {// thread thuong chua co message
                threadSolo.setStranger(true);
                StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid, reengMessagePacket, true);
                threadSolo.setStrangerPhoneNumber(strangerPhoneNumber);
                messageBusiness.updateThreadMessage(threadSolo);//cap nhat db khi doi state thread
            }
            return threadSolo;
        }
        if (phoneNumber != null) {// tao thread chat thuong khi luu danh ba roi
            return messageBusiness.createNewThreadNormal(friendJid);
        }
        Log.d(TAG, "thread da ton tai" + " khong co trong danh ba, chua co thread (danh dau la chua dong y)");
        // khong co trong danh ba, chua co thread (danh dau la chua dong y)
        StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid, reengMessagePacket, true);
        threadSolo = messageBusiness.createNewThreadStrangerOtherApp(friendJid, strangerPhoneNumber, false);// nhan loi moi lam quen tu app # show alert
        return threadSolo;
    }

    private boolean checkExistMessageSendFromThread(ThreadMessage threadMessage) {
        if (threadMessage == null ||
                threadMessage.getAllMessages() == null ||
                threadMessage.getAllMessages().isEmpty()) {// thread khong co message
            return false;
        } else {
            for (ReengMessage message : threadMessage.getAllMessages()) {
                if (message.getDirection() == ReengMessageConstant.Direction.send) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * check message lam quen thanh cong (chang khac khi gi cai invite friend, deu tao thread va cho chat luon)
     * spam nguoi dung, khong co cach nao tu choi loi moi lam quen
     *
     * @param friendJid
     * @param reengMessagePacket
     */
    public ThreadMessage processInviteSuccess(String friendJid, ReengMessagePacket reengMessagePacket) {
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(friendJid);
        ThreadMessage threadSolo = messageBusiness.findExistingSoloThread(friendJid);
        if (threadSolo != null) {
            if (threadSolo.isStranger()) {
                if (threadSolo.getStrangerPhoneNumber() == null ||
                        threadSolo.getStrangerPhoneNumber().getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                    StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid, reengMessagePacket, false);
                    threadSolo.setStrangerPhoneNumber(strangerPhoneNumber);
                }
            } else if (threadSolo.getAllMessages() == null || threadSolo.getAllMessages().isEmpty()) {// thread thuong chua co message
                threadSolo.setStranger(true);
                StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid, reengMessagePacket, false);// ngay 08/04 thay tu true sang false ??????
                threadSolo.setStrangerPhoneNumber(strangerPhoneNumber);
                messageBusiness.updateThreadMessage(threadSolo);//cap nhat db doi state
            }
            return threadSolo;
        }
        if (phoneNumber != null) {// so luu danh ba tao chat 1-1
            return messageBusiness.createNewThreadNormal(friendJid);
        }
        StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid, reengMessagePacket, false);
        threadSolo = messageBusiness.createNewThreadStrangerOtherApp(friendJid, strangerPhoneNumber, true);// di lam quen tren keeng khong show alert
        return threadSolo;
    }

    /**
     * insert or update stranger phone number
     *
     * @param friendJid
     * @param reengMessagePacket
     * @param isNotAccept
     * @return
     */
    private StrangerPhoneNumber createOrUpdateStrangerPhoneNumber(
            String friendJid, ReengMessagePacket reengMessagePacket, boolean isNotAccept) {
        // neu tao thread moi cho truong hop nhan dc lam quen thi update state la not accept, nguoc lai thi giu nguyen
        Log.d(TAG, "createOrUpdateStrangerPhoneNumber" + isNotAccept);
        String myName;
        String friendName;
        String friendAvatar;
        ReengMessagePacket.SubType subType = reengMessagePacket.getSubType();
        if (subType == ReengMessagePacket.SubType.invite_friend) {// minh duoc moi
            Log.d(TAG, "// minh duoc moi");
            myName = reengMessagePacket.getToName();
            friendName = reengMessagePacket.getFromName();
            friendAvatar = reengMessagePacket.getFromAvatar();
        } else {// ban tin bao minh moi thanh cong
            Log.d(TAG, "// ban tin bao minh moi thanh cong");
            myName = reengMessagePacket.getFromName();
            friendName = reengMessagePacket.getToName();
            friendAvatar = reengMessagePacket.getToAvatar();
        }
        String appId = reengMessagePacket.getAppId();
        return createOrUpdateStrangerPhoneNumber(friendJid,
                myName, friendName, friendAvatar, appId, isNotAccept);
    }

    /**
     * insert or update stranger phone number
     *
     * @param friendJid
     * @param strangerMusic
     * @return
     */
    private StrangerPhoneNumber createOrUpdateStrangerPhoneNumber(String friendJid, StrangerMusic strangerMusic, String appId) {
        // neu tao thread moi cho truong hop nhan dc lam quen thi update state la not accept, nguoc lai thi giu nguyen
        String myName = strangerMusic.getAcceptorName();
        String friendName = strangerMusic.getPosterName();
        String friendAvatar = strangerMusic.getPosterLastAvatar();
        return createOrUpdateStrangerPhoneNumber(friendJid, myName, friendName, friendAvatar, appId, false);
    }

    public StrangerPhoneNumber createOrUpdateStrangerPhoneNumber(String friendJid, String myName,
                                                                 String friendName, String friendAvatar,
                                                                 String appId, boolean isNotAccept) {
        // neu tao thread moi cho truong hop nhan dc lam quen thi update state la not accept, nguoc lai thi giu nguyen
        if (friendJid.length() == 11) {
            String newNumberStranger = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(friendJid);
            if (newNumberStranger != null) {
                friendJid = newNumberStranger;
            }
        }
        StrangerPhoneNumber strangerPhoneNumber = getExistStrangerPhoneNumberFromNumber(friendJid);
        if (TextUtils.isEmpty(friendName)) {
            friendName = Utilities.hidenPhoneNumber(friendJid);
        }
        if (strangerPhoneNumber == null) {
            Log.d(TAG, "strangerPhoneNumber == null");
            strangerPhoneNumber = new StrangerPhoneNumber();
            strangerPhoneNumber.setAppId(appId);
            strangerPhoneNumber.setPhoneNumber(friendJid);
            strangerPhoneNumber.setMyName(myName);
            strangerPhoneNumber.setFriendName(friendName);
            strangerPhoneNumber.setFriendAvatarUrl(friendAvatar);// avatar
            if (isNotAccept) {
                strangerPhoneNumber.setStateString(StrangerPhoneNumber.StateAccept.not_accept.toString());
            }
            insertStrangerPhoneNumber(strangerPhoneNumber);
        } else if (appId.equals(strangerPhoneNumber.getAppId())) {// cung app id cap nhat thong tin
            Log.d(TAG, "// cung app id cap nhat thong tin");
            if (!strangerPhoneNumber.equalsValues(myName, friendName, friendAvatar)) {
                strangerPhoneNumber.setMyName(myName);
                strangerPhoneNumber.setFriendName(friendName);
                strangerPhoneNumber.setFriendAvatarUrl(friendAvatar);// avatar
                updateStrangerPhoneNumber(strangerPhoneNumber);
            }
        }
        return strangerPhoneNumber;
    }

    public void updateStateThreadStrangerAfterLoadDB(ThreadMessage threadMessage, String friendJid) {
        if (threadMessage == null || TextUtils.isEmpty(friendJid)) {
            return;
        }
        // kiem tra neu so dien thoai co trong danh ba roi thi ko danh dau nua
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendJid);
        if (phoneNumber != null) {
            threadMessage.setStranger(false);
        } else {
            StrangerPhoneNumber strangerPhoneNumber = getExistStrangerPhoneNumberFromNumber(friendJid);
            if (strangerPhoneNumber != null) {
                threadMessage.setStranger(true);
                threadMessage.setStrangerPhoneNumber(strangerPhoneNumber);
            } else {
                threadMessage.setStranger(false);
            }
        }
    }

    public void updateStateBlockStrangerNumber(PhoneNumber newContact, NonContact oldContact, boolean isApi) {
        if (!isApi) return;
        int oldState = Constants.CONTACT.NONE;
        if (oldContact != null) {
            oldState = oldContact.getState();
        }
        StrangerPhoneNumber stranger = getExistStrangerPhoneNumberFromNumber(newContact.getJidNumber());
        if (stranger != null && oldState != newContact.getState() &&
                (newContact.getState() == Constants.CONTACT.ACTIVE || newContact.getState() == Constants.CONTACT.SYSTEM_BLOCK)) {
            stranger.setFriendName(newContact.getNickName());
            stranger.setFriendAvatarUrl(newContact.getLastChangeAvatar());// avatar
            updateStrangerPhoneNumber(stranger);
        }
    }

    /**
     * check message lam quen thanh cong (chang khac khi gi cai invite friend, deu tao thread va cho chat luon)
     * spam nguoi dung, khong co cach nao tu choi loi moi lam quen
     */
    public ThreadMessage processOutGoingStrangerMusic(String myName, String number, String name, String avatar, String strangerAppId) {
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        ThreadMessage threadSolo = messageBusiness.findExistingSoloThread(number);
        // check so luu danh ba
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(number);
        if (threadSolo != null) {
            if (threadSolo.isStranger()) {
                if (threadSolo.getStrangerPhoneNumber() == null ||
                        threadSolo.getStrangerPhoneNumber().getStrangerType() == StrangerPhoneNumber.StrangerType.music_stranger) {
                    StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(number, myName, name, avatar, strangerAppId, false);
                    threadSolo.setStrangerPhoneNumber(strangerPhoneNumber);
                }
            } else if (phoneNumber == null &&
                    (threadSolo.getAllMessages() == null || threadSolo.getAllMessages().isEmpty())) {// thread thuong chua co message
                threadSolo.setStranger(true);
                StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(number, myName, name, avatar, strangerAppId, false);
                threadSolo.setStrangerPhoneNumber(strangerPhoneNumber);
                messageBusiness.updateThreadMessage(threadSolo);//cap nhat db doi state cua thread
            }
            return threadSolo;
        }
        if (phoneNumber != null) {// so luu danh ba thi chat 1-1 luon
            threadSolo = messageBusiness.createNewThreadNormal(number);
            return threadSolo;
        }
        StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(number, myName, name, avatar, strangerAppId, false);
        threadSolo = messageBusiness.createNewThreadStrangerMusic(number, strangerPhoneNumber, true);// mời cùng nghe 1 bài ko show alert
        return threadSolo;
    }

    public ThreadMessage processOutGoingStrangerMusic(StrangerMusic strangerMusic, String strangerAppId) {
        return processOutGoingStrangerMusic(strangerMusic.getAcceptorName(), strangerMusic.getPosterJid(),
                strangerMusic.getPosterName(), strangerMusic.getPosterLastAvatar(), strangerAppId);
    }

    public ThreadMessage processOutGoingStrangerMusic(StrangerMusic strangerMusic) {
        return processOutGoingStrangerMusic(strangerMusic, Constants.CONTACT.STRANGER_MUSIC_ID);
    }

    /**
     * check message lam quen neu so chua co thread, hoac chua luu danh ba thi tao thread lam quen
     * insert them vao bang stranger, neu da co thi khong them vao bang stranger
     *
     * @param posterJid
     * @param reengMessagePacket
     * @return
     */
    public ThreadMessage processInComingStrangerMusic(String posterJid, ReengMusicPacket reengMessagePacket) {
        String friendName = reengMessagePacket.getNick();
        String friendAvatar = reengMessagePacket.getStrangerAvatar();
        String appId = Constants.CONTACT.STRANGER_MUSIC_ID;
        return createMochaStrangerAndThread(posterJid, friendName, friendAvatar, appId, true);
    }

    /**
     * tao moi stranger number and thread khi chat giau so
     *
     * @param friendJid
     * @param friendName
     * @param lAvatar
     * @return
     */
    public ThreadMessage createMochaStrangerAndThread(String friendJid, String friendName, String lAvatar, String appId, boolean isJoin) {
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        String myName = mApplication.getReengAccountBusiness().getUserName();
        if (TextUtils.isEmpty(appId)) appId = Constants.CONTACT.STRANGER_MOCHA_ID;
        // tim thread
        ThreadMessage threadSolo = messageBusiness.findExistingSoloThread(friendJid);
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(friendJid);
        if (threadSolo != null) {
            if (threadSolo.isStranger()) {
                if (threadSolo.getStrangerPhoneNumber() == null ||
                        threadSolo.getStrangerPhoneNumber().getStrangerType() == StrangerPhoneNumber.StrangerType.mocha_stranger) {
                    StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid,
                            myName, friendName, lAvatar, appId, false);
                    threadSolo.setStrangerPhoneNumber(strangerPhoneNumber);
                }
            } else if (phoneNumber == null &&
                    (threadSolo.getAllMessages() == null ||
                            threadSolo.getAllMessages().isEmpty())) {// thread thuong chua co message
                threadSolo.setStranger(true);
                StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid,
                        myName, friendName, lAvatar, appId, false);
                threadSolo.setStrangerPhoneNumber(strangerPhoneNumber);
                messageBusiness.updateThreadMessage(threadSolo);//cap nhat db doi state cua thread
            }
            return threadSolo;
        }
        if (phoneNumber != null) {// luu danh ba chat 1-1 luon
            threadSolo = messageBusiness.createNewThreadNormal(friendJid);
            return threadSolo;
        }
        StrangerPhoneNumber strangerPhoneNumber = createOrUpdateStrangerPhoneNumber(friendJid,
                myName, friendName, lAvatar, appId, false);
        if (TextUtils.isEmpty(lAvatar)) {
            requestGetAnonymous(friendJid, appId);
        }
        threadSolo = messageBusiness.createNewThreadStrangerMusic(friendJid, strangerPhoneNumber, isJoin);
        return threadSolo;
    }

    /**
     * tao stranger number, thread khi nhan duoc ban tin
     * co the external nhung khong co thread hay stranger number
     *
     * @param friendJid
     * @param friendName
     * @param appId
     * @return
     */
    public ThreadMessage createStrangerAndThreadAfterReceivedMessage(String friendJid, String friendName, String appId) {
        if ("makefriend".equals(appId)) {// ban cu thi lam quen van gui make_friend;
            appId = "keeng";
        } else if ("music_stranger".equals(appId)) {// ban cu thi nguoi la gui stranger_music
            appId = "music_stranger";
        }
        return createMochaStrangerAndThread(friendJid, friendName, null, appId, false);
    }

    public ThreadMessage createStrangerAndThreadAfterReceivedMessage(String friendJid,
                                                                     String friendName,
                                                                     String lAvatar,
                                                                     String appId) {
        if ("makefriend".equals(appId)) {// ban cu thi lam quen van gui make_friend;
            appId = "keeng";
        } else if ("music_stranger".equals(appId)) {// ban cu thi nguoi la gui stranger_music
            appId = "music_stranger";
        }
        return createMochaStrangerAndThread(friendJid, friendName, lAvatar, appId, false);
    }

    public void requestAcceptStranger(final String friendJid, final String myName,
                                      final onAcceptStrangeKeengListener listener) {
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.ACCEPT_STRANGER_KEENG);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: requestAcceptStranger: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_ERROR_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_ERROR_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                StrangerPhoneNumber strangerPhoneNumber = getExistStrangerPhoneNumberFromNumber(friendJid);
                                if (strangerPhoneNumber != null) {
                                    strangerPhoneNumber.setStateString(StrangerPhoneNumber.StateAccept.accepted.toString());
                                    updateStrangerPhoneNumber(strangerPhoneNumber);
                                }
                                listener.onResponse();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                HashMap<String, String> params = new HashMap<>();
                params.put("from", myNumber);
                params.put("from_name", myName);
                params.put("to", friendJid);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    private void requestGetAnonymous(final String friendJid, String appId) {
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!NetworkHelper.isConnectInternet(mApplication) || account == null || !account.isActive()) {
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
        String numberFriendEncode = HttpHelper.EncoderUrl(friendJid);
        String appIdEncode = HttpHelper.EncoderUrl(appId);
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(friendJid).append(appId).
                append(account.getToken()).
                append(currentTime);
        String encrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.ANONYMOUS_DETAIL),
                numberEncode,
                numberFriendEncode,
                appIdEncode,
                String.valueOf(currentTime),
                encrypt);
        Log.d(TAG, "requestGetAnonymous url: " + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String decryptResponse = HttpHelper.decryptResponse(response, mApplication.getReengAccountBusiness().getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                String friendName = responseObject.optString(Constants.HTTP.ANONYMOUS.TO_NAME, null);
                                String friendAvatar = responseObject.optString(Constants.HTTP.ANONYMOUS.TO_AVATAR, null);
                                String myName = responseObject.optString(Constants.HTTP.ANONYMOUS.FROM_NAME, null);
                                //update stranger
                                if (!TextUtils.isEmpty(friendName) && !TextUtils.isEmpty(myName)) {
                                    StrangerPhoneNumber strangerPhoneNumber =
                                            getExistStrangerPhoneNumberFromNumber(friendJid);
                                    if (strangerPhoneNumber != null) {
                                        strangerPhoneNumber.setFriendName(friendName);
                                        strangerPhoneNumber.setMyName(myName);
                                        strangerPhoneNumber.setFriendAvatarUrl(friendAvatar);
                                        updateStrangerPhoneNumber(strangerPhoneNumber);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    /////////////////////////////////Social friends tab////////////////////////////////////////
    private static final String SOCIAL_FRIENDS_LIMIT = "20";

    /**
     * api get social friends tab
     *
     * @param type
     * @return
     */
    public ArrayList<SocialFriendInfo> getSocialFriends(int type) {
        if (type == TYPE_SOCIAL_FRIEND) {
            return mSocialsFriend;
        } else if (type == TYPE_SOCIAL_BE_FOLLOWED) {
            return mSocialsBeFollowed;
        } else if (type == TYPE_SOCIAL_SUGGEST) {
            return mSocialsSuggest;
        } else {
            return new ArrayList<>();
        }
    }

    public void cancelSocialFriendRequest(final SocialFriendInfo socialFriendInfo, final int type, final onCancelSocialFriend listener) {
        mAcountBusiness = mApplication.getReengAccountBusiness();
        if (!mAcountBusiness.isValidAccount() || socialFriendInfo == null) {
            listener.onError(type, -1);
            return;
        }
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(type, -2);
            return;
        }
        //String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.SOCIAL_CANCEL_FRIEND_REQUEST);
        StringRequest request = new StringRequest(Request.Method.POST, UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_SOCIAL_CANCEL_FRIEND_REQUEST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "response: " + response);
                        int errorCode;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<SocialFriendInfo> list = getSocialFriends(type);
                                if (list != null) {
                                    list.remove(socialFriendInfo);
                                }
                                listener.onResponse(type, list);
                            } else {
                                listener.onError(type, errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(type, -1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(type, -1);
                Log.e(TAG, "VolleyError", error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String friendNumber = socialFriendInfo.getUserFriendJid();
                StringBuilder sb = new StringBuilder();
                sb.append(mAcountBusiness.getJidNumber()).
                        append(friendNumber).
                        append(mAcountBusiness.getToken()).
                        append(currentTime);
                String encryptData = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAcountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, mAcountBusiness.getJidNumber());
                params.put(Constants.HTTP.REST_OTHER_MSISDN, friendNumber);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encryptData);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    /**
     * get list social friends
     *
     * @param type
     * @param loadMore
     */
    public void getSocialFriendRequest(final int type, final boolean loadMore, final onSocialFriendsRequestListener listener) {
        mAcountBusiness = mApplication.getReengAccountBusiness();
        if (!mAcountBusiness.isValidAccount()) {
            listener.onError(type, -1, loadMore);
            return;
        }
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(type, -2, loadMore);
            return;
        }
        //VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SOCIAL_REQUEST);
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        String page = getPage(type, loadMore);
        Log.d(TAG, "getSocialFriendRequest page: " + page);
        sb.append(mAcountBusiness.getJidNumber()).
                append(SOCIAL_FRIENDS_LIMIT).
                append(page).
                append(mAcountBusiness.getToken()).
                append(currentTime);
        String encryptData = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), mAcountBusiness.getToken()));
        String url = getUrlGetSocialFriends(type, page, currentTime, encryptData);
        if (TextUtils.isEmpty(url)) {
            listener.onError(type, -1, loadMore);
            return;
        }
        Log.d(TAG, "getSocialFriendRequest url: " + loadMore + " --" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<SocialFriendInfo> socialFriends = parserSocialFriends(object, type);
                                initPhoneNumberBeforeLoadData(socialFriends);
                                String lastPage = "";
                                if (object.has("page")) {
                                    lastPage = object.get("page").getAsString();
                                }
                                processResponseGetSocial(socialFriends, lastPage, loadMore, type, listener);
                            } else {
                                listener.onError(type, errorCode, loadMore);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(type, -1, loadMore);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(type, -1, loadMore);
                Log.e(TAG, "VolleyError", error);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SOCIAL_REQUEST, false);
    }

    private String getPage(int type, boolean isLoadMore) {
        if (!isLoadMore) {// load page 0
            return "";
        } else if (type == TYPE_SOCIAL_FRIEND) {
            return pageFriend;
        } else if (type == TYPE_SOCIAL_BE_FOLLOWED) {
            return pageBeFollowed;
        } else if (type == TYPE_SOCIAL_SUGGEST) {
            return pageSuggest;
        }
        return "";
    }

    private String getUrlGetSocialFriends(int type, String page, long timestamp, String security) {
        String url = "";
        if (type == TYPE_SOCIAL_FRIEND) {
            //url = String.format(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.SOCIAL_GET_FRIENDS),
            url = String.format(UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_SOCIAL_GET_FRIENDS,
                    HttpHelper.EncoderUrl(mAcountBusiness.getJidNumber()),
                    SOCIAL_FRIENDS_LIMIT,
                    HttpHelper.EncoderUrl(page),
                    String.valueOf(timestamp),
                    security);
        } else if (type == TYPE_SOCIAL_BE_FOLLOWED) {
            //url = String.format(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.SOCIAL_GET_FRIEND_REQUESTS),
            url = String.format(UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_SOCIAL_GET_FRIEND_REQUESTS,
                    HttpHelper.EncoderUrl(mAcountBusiness.getJidNumber()),
                    SOCIAL_FRIENDS_LIMIT,
                    page,
                    String.valueOf(timestamp),
                    security);
        } else if (type == TYPE_SOCIAL_SUGGEST) {

        }
        return url;
    }

    private void processResponseGetSocial(ArrayList<SocialFriendInfo> socialFriendsResponse, String lastPage,
                                          boolean isLoadMore, int type, onSocialFriendsRequestListener listener) {
        if (socialFriendsResponse == null) socialFriendsResponse = new ArrayList<>();
        if (isLoadMore) {
            boolean isNoMore = false;
            if (socialFriendsResponse.isEmpty()) {
                isNoMore = true;
            }
            if (!isNoMore) {// van load more dc
                if (type == TYPE_SOCIAL_FRIEND) {
                    pageFriend = lastPage;
                    mSocialsFriend.addAll(socialFriendsResponse);
                } else if (type == TYPE_SOCIAL_BE_FOLLOWED) {
                    pageBeFollowed = lastPage;
                    mSocialsBeFollowed.addAll(socialFriendsResponse);
                } else {
                    pageSuggest = lastPage;
                    mSocialsSuggest.addAll(socialFriendsResponse);
                }
            }
            listener.onLoadMoreResponse(type, socialFriendsResponse, isNoMore);
        } else {
            if (type == TYPE_SOCIAL_FRIEND) {
                pageFriend = lastPage;
                mSocialsFriend = socialFriendsResponse;
            } else if (type == TYPE_SOCIAL_BE_FOLLOWED) {
                pageBeFollowed = lastPage;
                mSocialsBeFollowed = socialFriendsResponse;
            } else {
                pageSuggest = lastPage;
                mSocialsSuggest = socialFriendsResponse;
            }
            listener.onResponse(type, socialFriendsResponse);
        }
    }

    private ArrayList<SocialFriendInfo> parserSocialFriends(JsonObject object, int type) throws JsonSyntaxException {
        ArrayList<SocialFriendInfo> socialFriends = new ArrayList<>();
        if (object.has("data")) {
            JsonArray jsonArray = object.getAsJsonArray("data");
            if (jsonArray != null && jsonArray.size() > 0) {
                for (JsonElement element : jsonArray) {
                    JsonObject itemObject = element.getAsJsonObject();
                    SocialFriendInfo socialFriend = new SocialFriendInfo();
                    socialFriend.setJsonObject(itemObject);
                    socialFriend.setSocialType(type);
                    socialFriends.add(socialFriend);

                }
            }
        }
        return socialFriends;
    }

    public void initPhoneNumberBeforeLoadData(ArrayList<SocialFriendInfo> socialFriends) {
        if (socialFriends == null || socialFriends.isEmpty()) return;
        for (SocialFriendInfo socialFriend : socialFriends) {
            if (socialFriend.getUserType() == UserInfo.USER_ONMEDIA_NORMAL) {
                String jidNumber = socialFriend.getUserFriendJid();
                if (TextUtils.isEmpty(jidNumber)) {
                    socialFriend.setPhoneNumber(null);
                } else {
                    PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(jidNumber);
                    socialFriend.setPhoneNumber(phoneNumber);
                }
            } else {
                socialFriend.setPhoneNumber(null);
            }
        }
    }

    public void removeSocialBeRequested(SocialFriendInfo social) {
        String jid = social.getUserFriendJid();
        if (mSocialsBeFollowed != null && !TextUtils.isEmpty(jid)) {
            for (SocialFriendInfo item : mSocialsBeFollowed) {
                if (jid.equals(item.getUserFriendJid())) {
                    mSocialsBeFollowed.remove(item);
                    break;
                }
            }
        }
    }

    public interface onAcceptStrangeKeengListener {
        void onResponse();

        void onError(int errorCode);
    }

    public interface onDeleteListStranger {
        void onResponse(String response);

        void onError(int code);
    }

    public interface onSocialFriendsRequestListener {
        void onResponse(int type, ArrayList<SocialFriendInfo> list);

        void onLoadMoreResponse(int type, ArrayList<SocialFriendInfo> list, boolean isNoMore);

        void onError(int type, int code, boolean isLoadMore);
    }

    public interface onCancelSocialFriend {
        void onError(int type, int code);

        void onResponse(int type, ArrayList<SocialFriendInfo> list);
    }
}
package com.metfone.selfcare.business;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.datasource.CallHistoryDataSource;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.call.CallHistory;
import com.metfone.selfcare.database.model.call.CallHistoryDetail;
import com.metfone.selfcare.database.model.call.CallSubscription;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 10/13/2016.
 */

public class CallHistoryHelper {
    private static final String TAG = CallHistoryHelper.class.getSimpleName();
    private static final String TAG_SET_USER_FREE = "set_user_free";
    private static final String TAG_GET_REMAIN = "get_remain_call";
    private static CallHistoryHelper mInstance;
    private ApplicationController mApplication;
    private Resources mRes;
    private CallHistoryDataSource mCallHistoryDataSource;
    private CopyOnWriteArrayList<CallHistoryChangeListener> callHistoryReadiesListener = new CopyOnWriteArrayList<>();
    private LoadDataThread mLoadDataThread;
    private ArrayList<CallHistory> mCallHistories = new ArrayList<>();
    private boolean isLoading = false;
    private boolean isReady = false;

    public static synchronized CallHistoryHelper getInstance() {
        if (mInstance == null) {
            mInstance = new CallHistoryHelper(ApplicationController.self());
        }
        return mInstance;
    }

    private CallHistoryHelper(ApplicationController application) {
        this.mApplication = application;
        this.mCallHistoryDataSource = CallHistoryDataSource.getInstance(mApplication);
        this.mRes = mApplication.getResources();
        init();
    }

    public void resetData() {
        mCallHistories = new ArrayList<>();
    }

    public ArrayList<CallHistory> getCallHistories() {
        return mCallHistories;
    }

    public boolean isReady() {
        return isReady;
    }

    public void loadAllData() {
        Log.d(TAG, "[perform] initData");
        if (!isReady() && !isLoading) {
            mLoadDataThread = null;
            mLoadDataThread = new LoadDataThread();
            mLoadDataThread.setPriority(Thread.NORM_PRIORITY);
            mLoadDataThread.start();
        }
    }

    private class LoadDataThread extends Thread {
        @Override
        public void run() {
            long time = System.currentTimeMillis();
            isLoading = true;
            mCallHistories = mCallHistoryDataSource.getAllCallHistory();

            Log.i(TAG, "[perform] initData take: " + (System.currentTimeMillis() - time));
            notifyCallHistoryReady();
            isLoading = false;
            isReady = true;
            mLoadDataThread = null;
        }
    }

    private void init() {
        CallHistory history = mCallHistoryDataSource.getFirstCallHistory();
        if (history != null)
            mCallHistories.add(history);
    }

    private CallHistory getFirstCallHistory() {
        if (mCallHistories.isEmpty()) {
            return null;
        } else {
            return mCallHistories.get(0);
        }
    }

    public CallHistory getCallHistoryById(long historyId) {
        if (historyId < 0 || mCallHistories.isEmpty()) return null;
        for (CallHistory history : mCallHistories) {
            if (history.getId() == historyId) {
                return history;
            }
        }
        return null;
    }

    public ArrayList<CallHistoryDetail> getListHistoryDetailById(long historyId) {
        return mCallHistoryDataSource.getCallHistoryDetailByCallId(historyId);
    }

    private boolean differentCallHistory(CallHistory callHistory, CallHistoryDetail historyDetail) {
        if (callHistory.getFriendNumber() == null || !callHistory.getFriendNumber().equals(historyDetail
                .getFriendNumber()))
            return true;
        if (!TimeHelper.checkSameDay(callHistory.getCallTime(), historyDetail.getCallTime()))
            return true;
        if (callHistory.getCallState() == CallHistoryConstant.STATE_MISS && historyDetail.getCallState() !=
                CallHistoryConstant.STATE_MISS)
            return true;
        return callHistory.getCallState() != CallHistoryConstant.STATE_MISS && historyDetail.getCallState() ==
                CallHistoryConstant.STATE_MISS;
    }

    public void insertNewCallHistoryDetail(String friendNumber, String myNumber, int callState, int outState, boolean
            isSend, long time, int duration) {
        int direction = isSend ? CallHistoryConstant.DIRECTION_SEND : CallHistoryConstant.DIRECTION_RECEIVED;
        CallHistoryDetail historyDetail = new CallHistoryDetail(friendNumber, myNumber, callState, outState,
                direction, time, duration);
        new InsertHistoryAsyncTask().execute(historyDetail);
    }

    private class InsertHistoryAsyncTask extends AsyncTask<CallHistoryDetail, Void, Void> {

        @Override
        protected Void doInBackground(CallHistoryDetail... voids) {
            CallHistoryDetail historyDetail = voids[0];
            CallHistory firstHistory = getFirstCallHistory();
            if (firstHistory == null || differentCallHistory(firstHistory, historyDetail)) {
                firstHistory = new CallHistory(historyDetail, 1);
                mCallHistoryDataSource.insertCallHistory(firstHistory);
                if (isReady()) {
                    mCallHistories.add(0, firstHistory);
                } else if (isLoading) {
                    Log.d(TAG, "insert callHistory when loading data ??");
                }
            } else {
                firstHistory.updateValueFromHistoryDetail(historyDetail);
                mCallHistoryDataSource.updateCallHistory(firstHistory);
            }
            historyDetail.setCallId(firstHistory.getId());
            mCallHistoryDataSource.insertCallHistoryDetail(historyDetail);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            notifyCallHistoryChanged();
            super.onPostExecute(result);
        }
    }

    public void deleteCallHistory(CallHistory history) {
        if (history == null) return;
        mCallHistoryDataSource.deleteHistory(history);
        if (!mCallHistories.isEmpty()) {
            mCallHistories.remove(history);
        }
        notifyCallHistoryChanged();
    }

    /**
     * init Call History ready
     */
    public void addCallHistoryListener(CallHistoryChangeListener listener) {
        if (!callHistoryReadiesListener.contains(listener))
            callHistoryReadiesListener.add(listener);
    }

    public void removeCallHistoryListener(CallHistoryChangeListener listener) {
        callHistoryReadiesListener.remove(listener);
    }

    private void notifyCallHistoryReady() {
        if (callHistoryReadiesListener != null && !callHistoryReadiesListener.isEmpty()) {
            for (CallHistoryChangeListener listener : callHistoryReadiesListener) {
                listener.onReady();
            }
        }
    }

    private void notifyCallHistoryChanged() {
        if (!isReady()) return;
        if (callHistoryReadiesListener != null && !callHistoryReadiesListener.isEmpty()) {
            for (CallHistoryChangeListener listener : callHistoryReadiesListener) {
                listener.onDataChanged();
            }
        } else {
            Log.i(TAG, "no listener call historyChanged");
        }
    }

    public ArrayList<Object> searchCallHistory(String contentSearch, boolean isShowContacts) {
        if (!mApplication.isDataReady() || !isReady()) {
            return new ArrayList<>();
        }
        ArrayList<Object> listResult = new ArrayList<>();
        contentSearch = TextHelper.getInstant().convertUnicodeToAscci(contentSearch.trim());
        ArrayList<PhoneNumber> searchContacts;
        if (contentSearch.isEmpty()) {
            if (!mCallHistories.isEmpty()) {
                listResult.addAll(mCallHistories);
            }
            if (isShowContacts) {
                searchContacts = contactSearchListWithContentSearchEmpty(mCallHistories);
                if (!mCallHistories.isEmpty() &&
                        searchContacts != null && !searchContacts.isEmpty()) {
                    PhoneNumber index0 = new PhoneNumber();
                    index0.setName(mRes.getString(R.string.contact));
                    index0.setContactId(null);
                    searchContacts.add(0, index0);
                }
                listResult.addAll(searchContacts);
            }
        } else {
            ArrayList<CallHistory> searchHistories;
            String[] listTexts = contentSearch.split("\\s+");
            ArrayList<CallHistory> list;
            searchHistories = new ArrayList<>(mCallHistories);
            for (String item : listTexts) {
                if (item.trim().length() > 0) {
                    list = new ArrayList<>();
                    for (CallHistory history : searchHistories) {
                        if (isContainHistory(history, item)) {
                            list.add(history);
                        }
                    }
                    searchHistories = list;
                }
            }
            if (!searchHistories.isEmpty()) {
                ArrayList<CallHistory> others = new ArrayList<>();
                list = new ArrayList<>();
                for (CallHistory item : searchHistories) {
                    String name = TextHelper.getInstant().convertUnicodeToAscci(mApplication.getMessageBusiness()
                            .getFriendName(item.getFriendNumber()));
                    if (contentSearch != null && name != null && name.contains(contentSearch)) {
                        list.add(item);
                    } else {
                        others.add(item);
                    }
                }
                searchHistories = list;
                searchHistories.addAll(others);
            }
            //
            searchContacts = contactSearchList(listTexts, contentSearch, searchHistories);
            if (!searchHistories.isEmpty() && searchContacts != null && !searchContacts.isEmpty()) {
                PhoneNumber index0 = new PhoneNumber();
                index0.setName(mRes.getString(R.string.contact));
                index0.setContactId(null);
                searchContacts.add(0, index0);
            }
            listResult.addAll(searchHistories);
            listResult.addAll(searchContacts);
        }
        return listResult;
    }

    private boolean isContainHistory(CallHistory history, String itemSearch) {
        boolean isStranger;
        String friendNumber = history.getFriendNumber();
        String friendName;
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendNumber);
        if (phoneNumber != null) {// so co trong danh ba tra ten trong danh ba
            friendName = phoneNumber.getName();
            isStranger = false;
        } else {
            StrangerPhoneNumber strangerPhoneNumber = mApplication.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(friendNumber);
            if (strangerPhoneNumber != null) { // neu so co trong bang stranger thi lay ten o day
                if (TextUtils.isEmpty(strangerPhoneNumber.getFriendName())) {
                    friendName = Utilities.hidenPhoneNumber(friendNumber);
                } else {
                    friendName = strangerPhoneNumber.getFriendName();
                }
                isStranger = true;
            } else {
                friendName = PhoneNumberHelper.getInstant().getRawNumber(mApplication, friendNumber);
                isStranger = false;
            }
        }
        // nguoc lai tra ve so dt
        friendName = TextHelper.getInstant().convertUnicodeToAscci(friendName);
        if (isStranger && friendName != null && friendName.contains(itemSearch)) {
            return true;
        } else return friendName != null && friendName.contains(itemSearch) || friendNumber.contains(itemSearch);
    }

    // contact search list
    private ArrayList<PhoneNumber> contactSearchList(String[] listTexts, String contentSearch, ArrayList<CallHistory>
            searchHistories) {
        ArrayList<PhoneNumber> listContacts = mApplication.getContactBusiness().getListNumberAlls();
        if (listContacts == null || listContacts.isEmpty()) {
            return new ArrayList<>();
        }
        HashSet<String> hashSetHistoryNumber = new HashSet<>();
        for (CallHistory history : searchHistories) {
            hashSetHistoryNumber.add(history.getFriendNumber());
        }
        ArrayList<PhoneNumber> list;
        ArrayList<PhoneNumber> listSearch = new ArrayList<>(listContacts);
        for (String item : listTexts) {
            if (item.trim().length() > 0) {
                list = new ArrayList<>();
                for (PhoneNumber contact : listSearch) {
                    if (contact.getContactId() != null) {
                        String name = contact.getNameUnicode();
                        String numberJid = contact.getJidNumber();
                        String rawNumber;
                        if (numberJid.startsWith("0")) {// so vn
                            rawNumber = "+84" + numberJid.substring(1);
                        } else {
                            rawNumber = contact.getRawNumber();
                        }
                        if (!hashSetHistoryNumber.contains(numberJid) &&
                                ((name.contains(item) || numberJid.contains(item)) ||
                                        (rawNumber != null && rawNumber.contains(item)))) {
                            list.add(contact);
                        }
                    }
                }
                listSearch = list;
            }
        }
        if (!listSearch.isEmpty()) {
            ArrayList<PhoneNumber> others = new ArrayList<>();
            list = new ArrayList<>();
            for (PhoneNumber item : listSearch) {
                if (item.getNameUnicode() != null && contentSearch != null && item.getNameUnicode().contains
                        (contentSearch)) {
                    list.add(item);
                } else {
                    others.add(item);
                }
            }
            listSearch = list;
            listSearch.addAll(others);
        }
        return listSearch;
    }

    // lay danh sach neu content search trong
    private ArrayList<PhoneNumber> contactSearchListWithContentSearchEmpty(ArrayList<CallHistory> searchHistories) {
        ArrayList<PhoneNumber> listContacts = mApplication.getContactBusiness().getListNumberAlls();
        if (listContacts == null || listContacts.isEmpty()) {
            return new ArrayList<>();
        }
        HashSet<String> hashSetHistoryNumber = new HashSet<>();
        for (CallHistory history : searchHistories) {
            hashSetHistoryNumber.add(history.getFriendNumber());
        }
        ArrayList<PhoneNumber> list;
        ArrayList<PhoneNumber> listSearch = new ArrayList<>(listContacts);
        list = new ArrayList<>();
        for (PhoneNumber contact : listSearch) {
            if (contact.getContactId() != null) {
                String numberString = contact.getJidNumber();
                if (!hashSetHistoryNumber.contains(numberString)) {
                    list.add(contact);
                }
            }
        }
        listSearch = list;
        return listSearch;
    }

    public int getNameHistoryWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) mApplication.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        //return displaymetrics.widthPixels - (int) mRes.getDimension(R.dimen.call_div_size);
        return displaymetrics.widthPixels - (int) mRes.getDimension(R.dimen.call_div_size_enable_video);
    }

    /*public void requestSetCallOutFreeUser(final String friendJid, final String pkgId, final SetCallOutFreeUserListener listener) {
        if (!mApplication.getReengAccountBusiness().isValidAccount()) {
            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        StringRequest request = new StringRequest(Request.Method.POST, UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfFile(Config.UrlEnum.CALLOUT_SET_FREE),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestSetCallOutFreeUser" + response);
                        int errorCode;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            String desc = responseObject.optString("desc", mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                            } else {
                                listener.onError(desc);
                            }
                        } catch (JSONException e) {
                            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
                            Log.e(TAG, "Exception", e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                int callOut = mApplication.getReengAccountBusiness().getCallOutState();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(friendJid).
                        append(callOut).// TODO sv chưa chạy tron luong goi cuoc call out
                        append(mApplication.getReengAccountBusiness().getOperator()).
                        append(account.getToken()).
                        append(currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("contact", friendJid);
                params.put("callout", String.valueOf(callOut));
                params.put("currOperator", mApplication.getReengAccountBusiness().getOperator());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(mApplication, sb.toString(),
                        account.getToken()));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SET_USER_FREE, false);
    }*/

    public void requestSetCallOutFromCalloutGuide(final String friendJid, final String friendName, final String src,
                                                  final SetCallOutFreeUserListener listener) {
        if (!mApplication.getReengAccountBusiness().isValidAccount()) {
            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        StringRequest request = new StringRequest(Request.Method.POST, UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfFile(Config.UrlEnum.CALL_SUBSCRIPTION_SET_FREE_FROM_CALLOUTGUIDE),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestSetCallOutFromCalloutGuide" + response);
                        int errorCode;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            String desc = responseObject.optString("desc", mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                            } else {
                                listener.onError(desc);
                            }
                        } catch (Exception e) {
                            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
                            Log.e(TAG, e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                int callOut = mApplication.getReengAccountBusiness().getCallOutState();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(friendJid).
                        append(friendName).
                        append(callOut).// TODO sv chưa chạy tron luong goi cuoc call out
                        append(src).
                        append(mApplication.getReengAccountBusiness().getOperator()).
                        append(account.getToken()).
                        append(currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("contact", friendJid);
                params.put("callout", String.valueOf(callOut));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(mApplication, sb.toString(),
                        account.getToken()));
                params.put("currOperator", mApplication.getReengAccountBusiness().getOperator());
                params.put("src", src);
                params.put("contact_name", friendName);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SET_USER_FREE, false);
    }

    public void requestGetCallOutRemain(final GetCallOutRemainListener listener) {
        if (!mApplication.getReengAccountBusiness().isValidAccount()) {
            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_REMAIN);
        long timestamp = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        int callout = mApplication.getReengAccountBusiness().getCallOutState();
        String msisdn = mApplication.getReengAccountBusiness().getJidNumber();
        sb.append(msisdn).
                append(callout).// TODO sv chưa chạy tron luong goi cuoc call out
                append(mApplication.getReengAccountBusiness().getToken()).
                append(timestamp);
        String security = HttpHelper.encryptDataV2(mApplication, sb.toString(), mApplication.getReengAccountBusiness
                ().getToken());

        ResfulString paramsUrl = new ResfulString(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config
                .UrlEnum.CALL_SUBSCRIPTION_GET_REMAIN));
        paramsUrl.addParam(Constants.HTTP.REST_MSISDN, msisdn);
        paramsUrl.addParam("callout", callout);
        paramsUrl.addParam(Constants.HTTP.TIME_STAMP, String.valueOf(timestamp));
        paramsUrl.addParam(Constants.HTTP.DATA_SECURITY, security);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, paramsUrl.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "requestGetCallOutRemain onResponse : " + response);
                        int errorCode = -1;
                        try {
                            JsonObject object = new JsonParser().parse(response).getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            JsonObject dataObject = null;
                            if (object.has("data")) {
                                dataObject = object.getAsJsonObject("data");
                            }
                            if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                                CallSubscription item = new Gson().fromJson(dataObject, CallSubscription.class);
                                if (item != null) {
                                    listener.onSuccess(item);
                                } else {
                                    listener.onError(mRes.getString(R.string.e601_error_but_undefined));
                                }
                            } else {
                                listener.onError(mRes.getString(R.string.e601_error_but_undefined));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                        listener.onError(mRes.getString(R.string.e601_error_but_undefined));
                    }
                }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(stringRequest, TAG_GET_REMAIN, false);
    }

    public void getRemainTime() {
        if (!mApplication.getReengAccountBusiness().isValidAccount()) {
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_REMAIN);
        long timestamp = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        int callout = mApplication.getReengAccountBusiness().getCallOutState();
        String msisdn = mApplication.getReengAccountBusiness().getJidNumber();
        sb.append(mApplication.getReengAccountBusiness().getJidNumber())
                .append(callout)
                .append(Constants.HTTP.CLIENT_TYPE_STRING).append(Config.REVISION)
                .append(mApplication.getReengAccountBusiness().getCurrentLanguage())
                .append(mApplication.getReengAccountBusiness().getRegionCode())
                .append(mApplication.getReengAccountBusiness().getToken()).append(timestamp);
        String security = HttpHelper.encryptDataV2(mApplication, sb.toString(), mApplication.getReengAccountBusiness
                ().getToken());

        ResfulString paramsUrl = new ResfulString(UrlConfigHelper.getInstance(mApplication)
                .getDomainFile() + "/ReengBackendBiz/callout/getBalance");
        paramsUrl.addParam(Constants.HTTP.REST_MSISDN, msisdn);
        paramsUrl.addParam("callout", callout);
        paramsUrl.addParam("languageCode", mApplication.getReengAccountBusiness().getCurrentLanguage());
        paramsUrl.addParam("countryCode", mApplication.getReengAccountBusiness().getRegionCode());
        paramsUrl.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        paramsUrl.addParam("revision", Config.REVISION);
        paramsUrl.addParam(Constants.HTTP.TIME_STAMP, String.valueOf(timestamp));
        paramsUrl.addParam(Constants.HTTP.DATA_SECURITY, security);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, paramsUrl.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "requestRemainTime onResponse : " + response);
                        try {
                            JSONObject js = new JSONObject(response);
                            if (js.optInt("code", 0) == 200) {
                                String time = js.optString("balance", "");
                                if (!TextUtils.isEmpty(time)) {
                                    mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_TIME_CALLOUT_REMAIN, time).apply();
                                    TabMobileFragment.SubTabCallEvent event = new TabMobileFragment.SubTabCallEvent();
                                    event.setTimeRemain(time);
                                    EventBus.getDefault().post(event);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                    }
                }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(stringRequest, TAG_GET_REMAIN, false);
    }

    public interface CallHistoryChangeListener {
        void onReady();

        void onDataChanged();
    }

    public interface GetCallOutRemainListener {
        void onSuccess(CallSubscription callSubscription);

        void onError(String desc);
    }

    public interface SetCallOutFreeUserListener {
        void onSuccess(String desc);

        void onError(String desc);
    }
}
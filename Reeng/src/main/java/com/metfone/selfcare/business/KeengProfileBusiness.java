package com.metfone.selfcare.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

/**
 * Created by thaodv on 6/6/2015.
 */
public class KeengProfileBusiness {
    private static final String TAG = KeengProfileBusiness.class.getSimpleName();
    private ApplicationController mApp;
    private String keengUserId;
    private SharedPreferences mPref;
    private ReengAccountBusiness mReengAccountBusiness;
    private SettingBusiness mSettingBusiness;
    private UserInfo keengUserInfo;
    private long lastTimeGetInfo;


    public KeengProfileBusiness(ApplicationController application) {
        this.mApp = application;
        mReengAccountBusiness = mApp.getReengAccountBusiness();
        mSettingBusiness = SettingBusiness.getInstance(mApp);
        mPref = mApp.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                Context.MODE_PRIVATE);
        keengUserId = mPref.getString(Constants.PREFERENCE.PREF_KEENG_USER_ID, "");
    }

    public void doRequestGetMyKeengProfile(final KeengProfileRequestCallback callback, final boolean isUpload) {
        final long beginTime = System.currentTimeMillis();
        String numberEncode = HttpHelper.EncoderUrl(mReengAccountBusiness.getJidNumber());
        StringBuilder sb = new StringBuilder();
        sb.append(mReengAccountBusiness.getJidNumber()).append(mReengAccountBusiness.getToken()).append(beginTime);
        final String encrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApp, sb.toString(),
                mReengAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApp).getUrlConfigOfFile(Config.UrlEnum.GET_PROFILE_V2),
                numberEncode, beginTime, encrypt);
        Log.d(TAG, "getUserInfo url:" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int errorCode;
                        Log.i(TAG, "doRequestGetKeengProfile response" + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mApp.getReengAccountBusiness()
                                .getToken());
                        if (!TextUtils.isEmpty(decryptResponse)) {
                            Log.i(TAG, "doRequestGetKeengProfile onResponse: decrypt: " + decryptResponse);
                            try {
                                JSONObject responseObject = new JSONObject(decryptResponse);
                                errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                                if (errorCode == HTTPCode.E200_OK) {
                                    String data = responseObject.getString("data");
                                    UserInfo userInfo = new Gson().fromJson(data, UserInfo.class);
                                    keengUserInfo = userInfo;
                                    lastTimeGetInfo = TimeHelper.getCurrentTime();
                                    Log.i(TAG, "userInfo = " + userInfo);
                                    if (userInfo != null) {
                                        keengUserId = userInfo.getId();
                                        mPref.edit().putString(Constants.PREFERENCE.PREF_KEENG_USER_ID, keengUserId)
                                                .apply();
                                        if (callback != null) {
                                            callback.onRequestProfileSuccess(userInfo);
                                        }
                                    } else {
                                        if (callback != null) {
                                            callback.onRequestProfileFail(isUpload);
                                        }
                                    }
                                } else {
                                    if (callback != null) {
                                        callback.onRequestProfileFail(isUpload);
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception", e);
                                if (callback != null) {
                                    callback.onRequestProfileFail(isUpload);
                                }
                            }
                        } else
                        if (callback != null) {
                            callback.onRequestProfileFail(isUpload);
                        }
                    }
                }
                , new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"VolleyError",error);
                if (callback != null) {
                    callback.onRequestProfileFail(isUpload);
                }
            }
        });
        VolleyHelper.getInstance(mApp).addRequestToQueue(request, TAG, false);
    }

    public void logListenTogether(final MediaModel mediaModel) {
        if (mediaModel == null || !mediaModel.isValid()) return;// media khong hop le
//        handleLogListenTogether(mediaModel.getType(), mediaModel.getId());
//        keengUserId = "27367914";
        if (!TextUtils.isEmpty(keengUserId)) {
            handleLogListenTogether(mediaModel.getType(), mediaModel.getId());
        } else {
            doRequestGetMyKeengProfile(new KeengProfileRequestCallback() {
                @Override
                public void onRequestProfileSuccess(UserInfo userInfo) {
                    handleLogListenTogether(mediaModel.getType(), mediaModel.getId());
                }

                @Override
                public void onRequestProfileFail(boolean isUpload) {

                }
            }, false);
        }
    }

    private void handleLogListenTogether(final int itemType, final String itemId) {
        String url = UrlConfigHelper.getInstance(mApp).getUrlConfigOfFile(Config.UrlEnum.LOG_LISTEN_TOGETHER);
        //        String url = "http://pv.mocha.com.vn:8080/ReengBackendBiz/keeng/getLogTogether";
        String reqTime = String.valueOf(System.currentTimeMillis());
        String actionType = "1";
        String channel = "keeng";
        String password = "keeng@123";
        StringBuilder sb = new StringBuilder();
        ResfulString params = new ResfulString(url);
        params.addParam(Constants.HTTP.LOG_LISTEN.ACTION_TYPE, actionType);
        params.addParam(Constants.HTTP.LOG_LISTEN.ITEM_TYPE, String.valueOf(itemType));
        params.addParam(Constants.HTTP.LOG_LISTEN.ITEM_ID, itemId);
        params.addParam(Constants.HTTP.LOG_LISTEN.CHANNEL, channel);
        params.addParam(Constants.HTTP.LOG_LISTEN.USER_ID, keengUserId);
        params.addParam(Constants.HTTP.LOG_LISTEN.PASSWORD, password);
        params.addParam(Constants.HTTP.REST_MSISDN, mReengAccountBusiness.getJidNumber());
        params.addParam(Constants.HTTP.TIME_STAMP, reqTime);
        sb.append(actionType).append(itemType).append(itemId).append(channel).append(keengUserId).
                append(password).append(mReengAccountBusiness.getJidNumber()).
                append(mReengAccountBusiness.getToken()).append(reqTime);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.
                encryptDataV2(mApp, sb.toString(), mReengAccountBusiness.getToken()));
        Log.d(TAG, "handleLogListenTogether request------------------->");
        StringRequest req = new StringRequest(com.android.volley.Request.Method.GET, params.toString(), new Response
                .Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d(TAG, "handleLogListenTogether response -------------------> " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError: " + volleyError);
            }
        });
        Log.i(TAG, "url: " + params);
        VolleyHelper.getInstance(mApp).addRequestToQueue(req, TAG, false);
    }

    public long getLastTimeGetInfo() {
        return lastTimeGetInfo;
    }

    public void setLastTimeGetInfo(long lastTimeGetInfo) {
        this.lastTimeGetInfo = lastTimeGetInfo;
    }

    public UserInfo getKeengUserInfo() {
        return keengUserInfo;
    }

    public void setKeengUserInfo(UserInfo keengUserInfo) {
        this.keengUserInfo = keengUserInfo;
    }

    public interface KeengProfileRequestCallback {
        void onRequestProfileSuccess(UserInfo userInfo);

        void onRequestProfileFail(boolean isUpload);
    }
}
package com.metfone.selfcare.business;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Process;
import android.text.TextUtils;

import com.blankj.utilcode.util.SPUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.esport.common.Constant;
import com.metfone.esport.helper.AccountBusiness;
import com.metfone.selfcare.activity.SplashActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.datasource.BlockDataSource;
import com.metfone.selfcare.database.datasource.CallHistoryDataSource;
import com.metfone.selfcare.database.datasource.ConfigUserDataSource;
import com.metfone.selfcare.database.datasource.ContactDataSource;
import com.metfone.selfcare.database.datasource.EventMessageDataSource;
import com.metfone.selfcare.database.datasource.NonContactDataSource;
import com.metfone.selfcare.database.datasource.NoteMessageDataSource;
import com.metfone.selfcare.database.datasource.NumberDataSource;
import com.metfone.selfcare.database.datasource.OfficerDataSource;
import com.metfone.selfcare.database.datasource.ReengAccountDataSource;
import com.metfone.selfcare.database.datasource.ReengMessageDataSource;
import com.metfone.selfcare.database.datasource.StickerDataSource;
import com.metfone.selfcare.database.datasource.StrangerDataSource;
import com.metfone.selfcare.database.datasource.ThreadMessageDataSource;
import com.metfone.selfcare.database.datasource.TopSongDataSource;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.GroupAvatarHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.SignalStrengthHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.listeners.VipInfoChangeListener;
import com.metfone.selfcare.model.tab_video.AdsRegisterVip;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.keeng.widget.floatingView.MusicFloatingView;
import com.metfone.selfcare.module.libsignal.Base64;
import com.metfone.selfcare.module.libsignal.SignalEntity;
import com.metfone.selfcare.module.libsignal.SignalProtocolStore;
import com.metfone.selfcare.module.libsignal.SignalUtils;
import com.metfone.selfcare.module.onlinespoint.OnlineSpointHelper;
import com.metfone.selfcare.network.file.UploadListener;
import com.metfone.selfcare.network.file.UploadRequest;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.notification.MessageNotificationManager;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.contactintergation.DeviceAccountManager;
import com.viettel.util.ConvertHelper;
import com.viettel.util.LogDebugHelper;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQChangeNumber;
import org.jivesoftware.smack.packet.IQInfo;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONObject;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.state.PreKeyBundle;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ThaoDV on 6/6/14.
 */
public class ReengAccountBusiness {
    private static final String TAG = ReengAccountBusiness.class.getSimpleName();
    private CopyOnWriteArrayList<VipInfoChangeListener> vipInfoChangeListeners = new CopyOnWriteArrayList<>();
    private ReengAccount mCurrentAccount;
    private ApplicationController mApplication;
    private ReengAccountDataSource mReengAccountDataSource;
    private SharedPreferences mPref;
    private boolean isShowForceDialog = false;
    private boolean isValidRevision = true;
    private boolean isReady = false;
    private String msgForceUpdate;
    private String urlForceUpdate;
    private String countryCode;
    private String currentLanguage;
    private String settingLanguage;
    private int vipInfo;
    private boolean isVip = false;
    private boolean isCBNV = false;
    private int callEnable = -1;
    private int smsIn = -1;
    private int callOutState = 0;
    private boolean avnoEnable = false;
    private boolean tabCallEnable = false;
    private boolean isEnableCallOut = false;
    private boolean isEmulatorCam = false;
    private boolean isCalloutGlobalEnable = false;
    private String mochaApi = "";
    private String operator;
    private boolean isEnableChangeNumber;
    private int usingDesktop = 0;
    private int translatable = 0;
    private int fCallViaFS = 0;
    // location stranger
    private String locationId;
    private boolean e2eEnable;

    private String listGameIQPlay;

    private boolean isEnableUploadLog;
    private ArrayList<AdsRegisterVip> listAdsRegisterVip = new ArrayList<>();

    private boolean isDev;

    public ReengAccountBusiness(ApplicationController app) {
        mApplication = app;
        this.mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        isReady = false;
        this.settingLanguage = mPref.getString(Constants.PREFERENCE.PREF_SETTING_LANGUAGE_MOCHA, getDeviceLanguage());
    }

    private boolean isLoadDataDone = false;

    public void init() {
        mReengAccountDataSource = ReengAccountDataSource.getInstance(mApplication);
        // get account from database
        initAccountFromDatabase();
        initCountryCode();
        setCurrentLanguage();// set current khi khoi tao app
        this.vipInfo = mPref.getInt(Constants.PREFERENCE.PREF_MOCHA_USER_VIP_INFO, 0);
        this.isVip = vipInfo == 1;
        this.isCBNV = mPref.getBoolean(Constants.PREFERENCE.PREF_MOCHA_USER_CBNV, false);
        this.callEnable = mPref.getInt(Constants.PREFERENCE.PREF_MOCHA_ENABLE_CALL, -1);
        this.smsIn = mPref.getInt(Constants.PREFERENCE.PREF_MOCHA_ENABLE_SMS_IN, -1);
        this.callOutState = mPref.getInt(Constants.PREFERENCE.PREF_CALL_OUT_ENABLE_STATE, 0);
        this.locationId = mPref.getString(Constants.PREFERENCE.PREF_MY_STRANGER_LOCATION, "-1");
        this.avnoEnable = mPref.getBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_AVNO, false);
        this.isCalloutGlobalEnable = mPref.getBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_INTERNATIONAL, false);
        this.tabCallEnable = mPref.getBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_TAB_CALL, false);
        this.isEmulatorCam = mPref.getBoolean(Constants.PREFERENCE.PREF_MOCHA_EMULATOR_CAM, false);
        this.operator = mPref.getString(Constants.PREFERENCE.PREF_MOCHA_OPERATOR, "");
        this.listGameIQPlay = mPref.getString(Constants.PREFERENCE.PREF_LIST_GAME_IQ_PLAYED, "");
        this.isEnableChangeNumber = mPref.getBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_CHANGE_NUMBER, false);
        this.usingDesktop = mPref.getInt(Constants.PREFERENCE.PREF_MOCHA_USING_DESKTOP, 0);
        this.translatable = mPref.getInt(Constants.PREFERENCE.PREF_MOCHA_TRANSLATABLE, 0);
        this.isAnonymous = mPref.getBoolean(Constants.PREFERENCE.PREF_IS_ANONYMOUS_LOGIN, false);
        this.e2eEnable = mPref.getBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_E2E, false);
        this.isDev = mPref.getBoolean(Constants.PREFERENCE.PREF_IS_SERVER_TEST, false);
        isReady = true;
        //them state callout =-2 enable
        this.isEnableCallOut = callOutState != 0; // TODO sv chưa chạy
        // tron luong goi cuoc call
        // out
        //this.isEnableCallOut = callOutState == 1;
//        String rsaEncrypt = RSAEncrypt.getInStance(mApplication).encrypt(mApplication, getJidNumber());
//        Log.d(TAG, "rsaEncrypt: " + rsaEncrypt);
        this.isEnableUploadLog = mPref.getBoolean(Constants.PREFERENCE.PREF_ENABLE_UPLOAD_LOG, false);
        listAdsRegisterVip = new Gson().fromJson(mPref.getString(Constants.PREFERENCE.PREF_LIST_ADS_VIDEO_CATEGORY, ""),
                new TypeToken<List<AdsRegisterVip>>() {
                }.getType());
        isLoadDataDone = true;
        EventBus.getDefault().post(new SplashActivity.InitReengAccount());
    }

    public boolean isLoadDataDone() {
        return isLoadDataDone;
    }

    public boolean isDev() {
        return isDev;
    }

    public void setDev(boolean dev) {
        isDev = dev;
        mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_IS_SERVER_TEST, dev).apply();
    }

    public void setEnableUploadLog(boolean enable) {
        Log.i(TAG, "setEnableUploadLog: " + enable);
        isEnableUploadLog = enable;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_ENABLE_UPLOAD_LOG, enable).apply();
        if (enable) {
            SignalStrengthHelper.getInstant(mApplication).startListener();
        } else {
            SignalStrengthHelper.getInstant(mApplication).stopListener();
        }
    }

    public boolean isEnableUploadLog() {
        return isEnableUploadLog;
    }

    public boolean isReady() {
        return isReady;
    }

    public boolean isShowForceDialog() {
        return isShowForceDialog;
    }

    public ReengAccount getCurrentAccount() {
        if (!isReady) {
            initAccountFromDatabase();
            initCountryCode();
            isReady = true;
        }
        return mCurrentAccount;
    }

    public Phonenumber.PhoneNumber getPhoneNumberProtocol() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            return PhoneNumberHelper.getInstant().getPhoneNumberProtocol(mApplication.getPhoneUtil(),
                    account.getJidNumber(), account.getRegionCode());
        } else {
            return null;
        }
    }

    public void setShowForceDialog(boolean isShowForceDialog) {
        this.isShowForceDialog = isShowForceDialog;
    }

    public boolean isValidRevision() {
        return isValidRevision;
    }

    public void setValidRevision(boolean isValidRevision) {
        this.isValidRevision = isValidRevision;
    }

    public String getMsgForceUpdate() {
        return msgForceUpdate;
    }

    public void setMsgForceUpdate(String msgForceUpdate) {
        this.msgForceUpdate = msgForceUpdate;
    }

    public String getUrlForceUpdate() {
        return urlForceUpdate;
    }

    public void setUrlForceUpdate(String urlForceUpdate) {
        this.urlForceUpdate = urlForceUpdate;
    }

    public boolean isValidAccount() {
        getCurrentAccount();
        if (mCurrentAccount == null || !mCurrentAccount.isActive()) {
            return false;
        }
        return !TextUtils.isEmpty(mCurrentAccount.getToken());
    }

    public void createReengAccountBeforeLogin(Context context, String numberJid, String regionCode) {
        if (TextUtils.isEmpty(regionCode)) {
            regionCode = "VN";// mac dinh la vn
        }
        mCurrentAccount = new ReengAccount();
        mCurrentAccount.setNumberJid(numberJid);
        mCurrentAccount.setRegionCode(regionCode);
        mCurrentAccount.setActive(false);
        mCurrentAccount.setPermission(1);//00000001//show historyStranger, show birthday,
        initCountryCode();
        isReady = true;
        mReengAccountDataSource.createReengAccount(mCurrentAccount);
    }

    public void updateReengAccount(ReengAccount account) {
        mCurrentAccount = account;

        mReengAccountDataSource.updateReengAccount(account);
        initCountryCode();
    }

    public void initAccountFromDatabase() {
        mCurrentAccount = mReengAccountDataSource.getAccount();
        // co tai khoan ma truong region code null
        if (mCurrentAccount != null && TextUtils.isEmpty(mCurrentAccount.getRegionCode())) {
            mCurrentAccount.setRegionCode("VN");// mac dinh vn
            updateReengAccount(mCurrentAccount);// cap nhat db
        }
    }

    //get user number lay so de dang nhap
    public String getJidNumber() {
        if (mCurrentAccount != null) {
            return mCurrentAccount.getJidNumber();
        } else {
            return null;
        }
    }

    public String getUserName() {
        if (mCurrentAccount != null) {
            return mCurrentAccount.getName();
        } else {
            return null;
        }
    }

    public String getRegionCode() {
        if (mCurrentAccount != null) {
            if (!TextUtils.isEmpty(mCurrentAccount.getRegionCode())) {
                return mCurrentAccount.getRegionCode();
            } else {
                return "VN";// mac dinh la vn
            }
        } else {
            return "VN";// mac dinh la vn
        }
    }

    //get token
    public String getToken() {
        if (mCurrentAccount != null) {
            return mCurrentAccount.getToken();
        } else {
            return null;
        }
    }

    //get last changeAvatar
    public String getLastChangeAvatar() {
        if (mCurrentAccount != null) {
            return mCurrentAccount.getLastChangeAvatar();
        } else {
            return null;
        }
    }

    public void removeFileFromProfileDir() {
        File profileDir = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.IMAGE_COMPRESSED_FOLDER);
        File[] list = profileDir.listFiles();
        if (list == null)
            return;
        for (File fileName : list) {
            if (fileName.exists()) {
                fileName.delete();
            }
        }
    }

    public void removeFileExternalDir(Context context) {
        File profileDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File[] list = profileDir.listFiles();
        if (list == null)
            return;
        for (File fileName : list) {
            if (fileName.exists()) {
                fileName.delete();
            }
        }
    }

    public void deactivateAccount(Context context) throws XMPPException {
        //send packet deactive
        XMPPManager mXMPPManager = mApplication.getXmppManager();
        Presence presence = new Presence(Presence.Type.unavailable);
        presence.setStatus("unregister");
        presence.setSubType(Presence.SubType.deactive);
        mXMPPManager.sendPacketNoWaitResponse(presence);
        OnlineSpointHelper.getInstant(mApplication).restData();
        clearDataWhenDeactive(mApplication);
        clearPreference(context);
        lockAccount(mApplication, false);
        VideoService.stop(context);
        MusicFloatingView.stop(context);
        mApplication.getCamIdUserBusiness().deleteAllInfo();
        mApplication.getConfigBusiness().deleteAllKeyConfig();
        mApplication.getConfigBusiness().init();
        DeviceAccountManager.getInstance(context).removeAccount();
        mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_FIRST_OPEN, true).apply();
        mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_FIRST_OPEN_CAM, true).apply();
        SPUtils.getInstance().remove(Constant.PREF_USERNAME_ACCOUNT_ESPORT);
        SPUtils.getInstance().remove(Constant.PREF_PASSWORD_ACCOUNT_ESPORT);
        SPUtils.getInstance().remove(Constant.PREF_OTP_ACCOUNT_ESPORT);
        SPUtils.getInstance().remove(Constant.PREF_CACHE_AUTHEN_ESPORT);
        SPUtils.getInstance().remove(Constant.PREF_CACHE_ACCOUNT_ESPORT);
        SPUtils.getInstance().remove(Constant.PREF_CACHE_LIST_MY_CHANNEL_ESPORT);
        SPUtils.getInstance().remove(Constant.PREF_CACHE_CURRENT_CHANNEL_ESPORT);

        AccountBusiness.getInstance().clearAuthenInfo();

        android.util.Log.e(TAG, "deactivateAccount: " + ApplicationController.self().getReengAccountBusiness().getToken());
        if (mApplication != null && mApplication.getReengAccountBusiness() != null && mApplication.getReengAccountBusiness().getCurrentAccount() != null) {
            mApplication.getReengAccountBusiness().getCurrentAccount().setNumberJid("");
            mApplication.getReengAccountBusiness().getCurrentAccount().setToken("");
        }
        if (mReengAccountDataSource == null) {
            mReengAccountDataSource = ReengAccountDataSource.getInstance(mApplication);
        }
        mReengAccountDataSource.deleteReengAccount();
    }

    private void resetValue() {
        isVip = false;
        isCBNV = false;
        callEnable = -1;
        smsIn = -1;
        callOutState = 0;
        avnoEnable = false;
        tabCallEnable = false;
        isEnableCallOut = false;
        isEmulatorCam = false;
        isCalloutGlobalEnable = false;
        mochaApi = "";
        operator = "";
        isEnableChangeNumber = false;
        usingDesktop = 0;
        translatable = 0;
        e2eEnable = false;
    }

    /**
     * khoa account do sai token
     *
     * @param context
     */
    public void lockAccount(ApplicationController context, boolean isConfict) {
        if (mCurrentAccount == null) return;
        mCurrentAccount.setActive(false);
        mCurrentAccount.setToken("");
        mReengAccountDataSource.updateReengAccount(mCurrentAccount);
        if (!isConfict) {
            clearDataWhenDeactive(context);
            //clear mem cache
            mApplication.getMessageBusiness().init();
        }
        mApplication.deleteAllNotifyChannel();
        MessageNotificationManager.getInstance(mApplication).clearAllNotify();
        clearContactData(mApplication);
        clearPreference(context);
        GroupAvatarHelper.getInstance(mApplication).clearBitmapCache();
        mApplication.getXmppManager().setTokenForConfig("");
        resetValue();
    }


    private void clearDataWhenDeactive(ApplicationController application) {
        //        ko xoa bang account nua --> tranh truong hop deactive --> vao nhan dien
        //        ReengAccountDataSource raDataSource = ReengAccountDataSource.getInstance(context);
        //        raDataSource.deleteAllTable();

        application.getApplicationComponent().providesUtils().saveChannelInfo(new Channel());// clear cache channel
        ReengMessageDataSource rmDataSource = ReengMessageDataSource.getInstance(application);
        rmDataSource.deleteAllTable();
        ThreadMessageDataSource tmDataSource = ThreadMessageDataSource.getInstance(application);
        tmDataSource.deleteAllTable();
        EventMessageDataSource eventMessageDataSource = EventMessageDataSource.getInstance(application);
        eventMessageDataSource.deleteAllTable();
        //del contact
        clearContactData(application);
        // del block
        BlockDataSource blockDataSource = BlockDataSource.getInstance(application);
        blockDataSource.deleteAllTable();
        // del stranger contact
        StrangerDataSource strangerDataSource = StrangerDataSource.getInstance(application);
        strangerDataSource.deleteAllTable();
        // del noncontact
        NonContactDataSource nonContactDataSource = NonContactDataSource.getInstance(application);
        nonContactDataSource.deleteAllTable();
        //sticker recent
        StickerDataSource stickerDataSource = StickerDataSource.getInstance(application);
        stickerDataSource.deleteAllStickerRecent();
        stickerDataSource.deleteAllTable();
        //officer
        OfficerDataSource officerDataSource = OfficerDataSource.getInstance(application);
        officerDataSource.deleteAllTable();
        //top song
        TopSongDataSource topSongDataSource = TopSongDataSource.getInstance(application);
        topSongDataSource.deleteAllTable();
        // call history
        CallHistoryDataSource callHistoryDataSource = CallHistoryDataSource.getInstance(application);
        callHistoryDataSource.deleteAllTable();
        callHistoryDataSource.deleteAllDetailTable();

        NoteMessageDataSource noteMessageDataSource = NoteMessageDataSource.getInstance(application);
        noteMessageDataSource.deleteAllDetailTable();
        //clear image profile
        mApplication.getImageProfileBusiness().deleteAllImageProfile();
        //clear config user
        ConfigUserDataSource.getInstance(mApplication).deleteAllTable();
    }

    public void clearContactData(ApplicationController application) {
        ContactDataSource contactDataSource = ContactDataSource.getInstance(application);
        contactDataSource.deleteAllTable();
        NumberDataSource numberDataSource = NumberDataSource.getInstance(application);
        numberDataSource.deleteAllTable();
    }

    private void clearPreference(Context context) {
        try {
            SharedPreferences mPref = context.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context
                    .MODE_PRIVATE);
            SharedPreferences.Editor editor = mPref.edit();
            //get old config tab home
            String dataSaveConfigKh = mPref.getString(Constants.PREFERENCE.PREF_SAVE_LIST_CONFIG_TAB_HOME_KH, "");
            editor.clear().apply();
            SharedPref.newInstance(context).clear();
            com.metfone.selfcare.module.newdetails.utils.SharedPref.newInstance(context).clear();
            SharedPrefs.getInstance().clear();
            //set new config tab home
            editor.putString(Constants.PREFERENCE.PREF_SAVE_LIST_CONFIG_TAB_HOME_KH, dataSaveConfigKh).apply();
        } catch (Exception e) {
            Log.d(TAG, "clear preference" + e);
        }
    }

    public String getDeviceLanguage() {
        try {
            Locale localeDefault = Locale.getDefault();
            if (!TextUtils.isEmpty(localeDefault.getLanguage())) {
                return localeDefault.getLanguage();
            } else {
                return "vi";
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return "vi";
        }
    }

    /**
     * get country code user login
     *
     * @return
     */
    public String getCountryCode() {
        if (TextUtils.isEmpty(countryCode)) {
            initCountryCode();
        }
        return countryCode;
    }

    private void initCountryCode() {
        String regionCode = getRegionCode();
        if (TextUtils.isEmpty(regionCode)) {
            this.countryCode = "0";
        } else if ("VN".equals(regionCode)) {
            this.countryCode = "0";
        } else {
            int code = mApplication.getPhoneUtil().getCountryCodeForRegion(regionCode);
            this.countryCode = "+" + code;
        }
    }

    public boolean isViettel() {
        if (getCurrentAccount() != null) {
            if (!TextUtils.isEmpty(operator))
                return PhoneNumberHelper.isViettel(operator);
            else
                return PhoneNumberHelper.getInstant().isViettelNumber(getJidNumber());
        }
        return false;
    }

    public boolean isOperatorViettel() {
        if (getCurrentAccount() != null) {
            if (!TextUtils.isEmpty(operator))
                return PhoneNumberHelper.isViettel(operator);
        }
        return false;
    }

    public String getOperator() {
        if (operator == null) operator = "";
        return operator;
    }

    public boolean isHaiti() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            return "HT".equals(account.getRegionCode());
        } else {
            return false;
        }
    }

    public boolean isLaos() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            return "LA".equals(account.getRegionCode());
        } else {
            return false;
        }
    }

    public boolean isVietnam() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            return "VN".equals(account.getRegionCode());
        } else {
            return false;
        }
    }

    public boolean isTimorLeste() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            return "TL".equals(account.getRegionCode());
        } else {
            return false;
        }
    }

    public boolean isCambodia() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            return "KH".equals(account.getRegionCode());
        } else {
            return false;
        }
    }

    public String getCurrentLanguage() {
        return currentLanguage;
    }

    public void setCurrentLanguage() {
        this.currentLanguage = getDeviceLanguage();
    }

    public void updatePermissionBirthday(boolean isShow) {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            int permission = account.getPermission();
            if (isShow) {
                permission = permission | NumberConstant.PERMISSION_BIRTHDAY_ON;//00000001
            } else {
                permission = permission & ~NumberConstant.PERMISSION_BIRTHDAY_ON;//11111110
            }
            account.setPermission(permission);
            updateReengAccount(account);
        }
    }

    public void updatePreKey(String preKey) {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            account.setPreKey(preKey);
            updateReengAccount(account);
        }
    }

    public void updatePermissionHideStrangerHistory(boolean isHide) {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            int permission = account.getPermission();
            if (isHide) {
                permission = permission | NumberConstant.PERMISSION_HIDE_STRANGER_HISTORY;//00000010
            } else {
                permission = permission & ~NumberConstant.PERMISSION_HIDE_STRANGER_HISTORY;//11111101
            }
            account.setPermission(permission);
            updateReengAccount(account);
        }
    }

    public boolean isPermissionShowBirthday() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            if (account.getPermission() == -1) {
                return true;
            } else
                return (account.getPermission() & NumberConstant.PERMISSION_BIRTHDAY_ON) == NumberConstant
                        .PERMISSION_BIRTHDAY_ON;
        }
        return false;
    }

    public boolean isHideStrangerHistory() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            if (account.getPermission() == -1) {
                return false;
            } else
                return (account.getPermission() & NumberConstant.PERMISSION_HIDE_STRANGER_HISTORY) == NumberConstant
                        .PERMISSION_HIDE_STRANGER_HISTORY;
        }
        return false;
    }

   /* public int setPermission(int permissionInt, boolean isShowBirth, boolean isHideHistoryStranger) {
        Log.d(TAG, "setPermission: " + permissionInt + " isShowBirth: " + isShowBirth + " isHideHistoryStranger: " +
        isHideHistoryStranger);
        if (isShowBirth) {
            permissionInt = permissionInt | NumberConstant.PERMISSION_BIRTHDAY_ON;//00000001
        } else {
            //mark = ~mark & NumberConstant.PERMISSION_BIRTHDAY_OFF;
            permissionInt = permissionInt & ~NumberConstant.PERMISSION_BIRTHDAY_ON;//11111110
        }
        if (isHideHistoryStranger) {
            permissionInt = permissionInt | NumberConstant.PERMISSION_HIDE_HISTORY_STRANGER;//00000010
        } else {
            permissionInt = permissionInt & ~NumberConstant.PERMISSION_HIDE_HISTORY_STRANGER;//11111101
        }
        Log.d(TAG, "setPermission: END " + permissionInt);
        return permissionInt;
    }*/

    public void updateUserInfoFromPresence(Presence presence) {
        String from = presence.getFrom();
        from = from.split("@")[0].trim();
        if (TextUtils.isEmpty(from)) {
            return;
        }
        ReengAccount account = getCurrentAccount();
        if (account != null && from.equals(account.getJidNumber())) {// dung so cua minh
            if (presence.getUserName() != null) {
                account.setName(presence.getUserName());
            }
            if (presence.getUserBirthdayStr() != null) {
                account.setBirthdayString(presence.getUserBirthdayStr());
            }
            if (presence.getStatus() != null) {
                account.setStatus(presence.getStatus());
            }
            updateReengAccount(account);
        }
    }

    public void updateUserPackageInfo(Presence presence) {
        String from = presence.getFrom();
        from = from.split("@")[0].trim();
        if (TextUtils.isEmpty(from)) {
            return;
        }
        ReengAccount account = getCurrentAccount();
        if (account != null && from.equals(account.getJidNumber())) {// dung so cua minh
            setVipInfo(presence.getVipInfo());
        }
    }

    public boolean isDifferentCountry(String friendNumber) {
        if (TextUtils.isEmpty(friendNumber)) {
            return false;
        }
        String myCode = getCountryCode();
        return !friendNumber.startsWith(myCode);
    }

    public String getSettingLanguage() {
        return settingLanguage;
    }

    public String getSettingLanguageFromPref() {
        return mPref.getString(Constants.PREFERENCE.PREF_SETTING_LANGUAGE_MOCHA, getDeviceLanguage());
    }

    public void setSettingLanguage(String settingLanguage) {
        this.settingLanguage = settingLanguage;
        mPref.edit().putString(Constants.PREFERENCE.PREF_SETTING_LANGUAGE_MOCHA, settingLanguage).apply();
    }

    public boolean isVip() {
        return isVip;
    }

    public boolean isAvnoEnable() {
        return avnoEnable;
    }

    public boolean isTabCallEnable() {
        return tabCallEnable;
    }

    public void setVipInfo(int vipInfo) {
        Log.d(TAG, "setVipInfo----: " + vipInfo);
        this.vipInfo = vipInfo;
        this.isVip = vipInfo == 1;
        mPref.edit().putInt(Constants.PREFERENCE.PREF_MOCHA_USER_VIP_INFO, vipInfo).apply();
        onVipInfoChanged();
    }

    public boolean isCBNV() {
        return isCBNV;
    }

    public boolean isCallEnable() {
        return callEnable == 1;
    }

    public boolean isSmsInEnable() {// mac dinh ==-1 sẽ là on
        return smsIn == 1;
    }

    public void setSSL(int ssl) {
        Log.d(TAG, "ssl: " + ssl);
//        if (!Config.Server.SERVER_TEST)
        mPref.edit().putInt(Constants.PREFERENCE.PREF_MOCHA_ENABLE_SSL, ssl).apply();
    }

    public int getCallOutState() {
        return callOutState;
        //return -1;
    }

    public boolean isEnableCallOut() {
        return isEnableCallOut;
        //return true;
    }

    public String getMochaApi() {
        return mochaApi;
    }

    public void setConfigFromServer(int vipInfo, int cbnv, int call, int ssl, int smsIn, int callOut,
                                    String avnoNumber, String mochaApi, int avnoEnable, int tabCallEnable,
                                    int isCalloutGlobalEnable, String operator, int enableChangeNumber,
                                    int usingDesktop, int translatable, int fCallViaFS, int e2eEnable) {

        Log.d(TAG, "setConfigFromServer: " + vipInfo + " cbnv: " + cbnv
                + " call: " + call + " ssl: " + ssl + " " + "smsIn: " + smsIn + " avno: " + avnoNumber
                + " mochaapi: " + "" + mochaApi + " tabCallEnable: " + tabCallEnable
                + " isCalloutGlobalEnable: " + isCalloutGlobalEnable
                + " operator: " + operator
                + " enableChangeNumber: " + enableChangeNumber
                + " usingdesktop: " + usingDesktop
                + " translatable: " + translatable
                + " fCallViaFS: " + fCallViaFS);
        // co thông tin (!= -1) thì mới update
        SharedPreferences.Editor editor = mPref.edit();
        boolean updateVipInfo = false;
        if (vipInfo != -1) {
            this.vipInfo = vipInfo;
            this.isVip = vipInfo == 1;
            editor.putInt(Constants.PREFERENCE.PREF_MOCHA_USER_VIP_INFO, vipInfo);
            updateVipInfo = true;
        }
        if (cbnv != -1) {
            this.isCBNV = cbnv == 1;
            editor.putBoolean(Constants.PREFERENCE.PREF_MOCHA_USER_CBNV, isCBNV);
        }
        if (call != -1) {
            this.callEnable = call;
            editor.putInt(Constants.PREFERENCE.PREF_MOCHA_ENABLE_CALL, callEnable);
        }
        if (smsIn != -1) {
            this.smsIn = smsIn;
            editor.putInt(Constants.PREFERENCE.PREF_MOCHA_ENABLE_SMS_IN, smsIn);
        }
        if (ssl != -1) {
            setSSL(ssl);
        }
        if (this.callOutState != callOut) {
            this.callOutState = callOut;
            //them state callout =-2 enable
            this.isEnableCallOut = callOutState != 0; // TODO sv chưa chạy
            // tron luong goi cuoc call out
            //this.isEnableCallOut = callOutState == 1;
            updateVipInfo = true;
        }
        editor.putInt(Constants.PREFERENCE.PREF_CALL_OUT_ENABLE_STATE, callOut);

        if (avnoNumber == null || "-1".equals(avnoNumber)) avnoNumber = "";
        if (getCurrentAccount() != null &&
                !avnoNumber.equals(getAVNONumber())) {
            updateAVNONumber(avnoNumber);
        }
        if (!TextUtils.isEmpty(mochaApi)) {
            this.mochaApi = mochaApi;
        }
        if (avnoEnable != -1) {
            this.avnoEnable = avnoEnable == 1;
            editor.putBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_AVNO, this.avnoEnable);
            onAVNOChanged();
        }

        if (tabCallEnable != -1) {
            if ((tabCallEnable == 1) != this.tabCallEnable) {
                this.tabCallEnable = tabCallEnable == 1;
                editor.putBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_TAB_CALL, this.tabCallEnable);
                ListenerHelper.getInstance().onConfigTabChanged(mApplication);
            }
        }
        if (isCalloutGlobalEnable != -1) {
            this.isCalloutGlobalEnable = isCalloutGlobalEnable == 1;
            editor.putBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_INTERNATIONAL, this.isCalloutGlobalEnable);
        }

        if (!TextUtils.isEmpty(operator)) {
            this.operator = operator;
            editor.putString(Constants.PREFERENCE.PREF_MOCHA_OPERATOR, this.operator);
        }

        if (enableChangeNumber != -1) {
            this.isEnableChangeNumber = enableChangeNumber == 1;
            editor.putBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_CHANGE_NUMBER, this.isEnableChangeNumber);
        }

        if (usingDesktop != -1) {
            this.usingDesktop = usingDesktop;
            editor.putInt(Constants.PREFERENCE.PREF_MOCHA_USING_DESKTOP, usingDesktop);
        }

        if (translatable != -1) {
            this.translatable = translatable;
            editor.putInt(Constants.PREFERENCE.PREF_MOCHA_TRANSLATABLE, translatable);
        }

        if (fCallViaFS != -1) {
            this.fCallViaFS = fCallViaFS;
            editor.putInt(Constants.PREFERENCE.PREF_MOCHA_CALL_VIA_FS, fCallViaFS);
        }

        if (e2eEnable != -1) {
            this.e2eEnable = e2eEnable == 1;
            editor.putBoolean(Constants.PREFERENCE.PREF_MOCHA_ENABLE_E2E, this.e2eEnable);
        }

        editor.apply();
        if (updateVipInfo) {
            onVipInfoChanged();
        }
    }

    public void addVipInfoChangeListener(VipInfoChangeListener listener) {
        if (!vipInfoChangeListeners.contains(listener)) {
            vipInfoChangeListeners.add(listener);
        }
    }

    public void removeVipInfoChangeListener(VipInfoChangeListener listener) {
        vipInfoChangeListeners.remove(listener);
    }

    private void onVipInfoChanged() {
        if (vipInfoChangeListeners != null && !vipInfoChangeListeners.isEmpty()) {
            for (VipInfoChangeListener listener : vipInfoChangeListeners) {
                listener.onChange();
            }
        }
    }

    public void onMoreItemChanged() {
        if (vipInfoChangeListeners != null && !vipInfoChangeListeners.isEmpty()) {
            for (VipInfoChangeListener listener : vipInfoChangeListeners) {
                listener.onMoreConfigChange();
            }
        }
    }

    public void onAVNOChanged() {
        if (vipInfoChangeListeners != null && !vipInfoChangeListeners.isEmpty()) {
            for (VipInfoChangeListener listener : vipInfoChangeListeners) {
                listener.onAVNOChange();
            }
        }
    }

    public void processUploadAvatarTask(String filePath) {
        if (!TextUtils.isEmpty(filePath)) {
            boolean isHD = SettingBusiness.getInstance(mApplication).getPrefEnableHDImage();
            Log.i(TAG, "upload anh IMAGE_AVATAR sau khi crop");
            String fileCompressed = MessageHelper.getPathOfCompressedFile(filePath,
                    Config.Storage.PROFILE_PATH, "", isHD);
            //FileHelper.deleteFile(filePath);
            ReengAccount account = getCurrentAccount();
            if (account == null) return;
            account.setNeedUpload(true);
            account.setAvatarPath(fileCompressed);
            updateReengAccount(account);
            UploadListener uploadAvatarListener = new UploadListener() {
                @Override
                public void onUploadStarted(UploadRequest uploadRequest) {

                }

                @Override
                public void onUploadComplete(UploadRequest uploadRequest, String response) {
                    Log.i(TAG, "response: " + response);
                    String time = "";
                    try {
                        JSONObject object = new JSONObject(response);
                        int errorCode = -1;
                        if (object.has(Constants.HTTP.REST_CODE)) {
                            errorCode = object.getInt(Constants.HTTP.REST_CODE);
                        }
                        if (errorCode == Constants.HTTP.CODE_SUCCESS) {
                            if (object.has(Constants.HTTP.USER_INFOR.LAST_AVATAR)) {
                                time = object.getString(Constants.HTTP.USER_INFOR.LAST_AVATAR);
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "JSONException", e);
                    }
                    if (!TextUtils.isEmpty(time)) {   // upload ok
                        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                        account.setLastChangeAvatar(time);
                        account.setNeedUpload(false);
                        mApplication.getReengAccountBusiness().updateReengAccount(account);
                        GroupAvatarHelper.getInstance(mApplication).clearBitmapCache();
                        CopyOnWriteArrayList<ThreadMessage> list = mApplication.getMessageBusiness().getThreadMessageArrayList();
                        for (ThreadMessage threadMessage : list) {
                            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                                threadMessage.setForceCalculatorAllMember(true);
                            }
                        }
                        ListenerHelper.getInstance().onAvatarChange(uploadRequest.getFilePath());
                    }
                }

                @Override
                public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {

                }

                @Override
                public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int
                        progress, long speed) {

                }
            };
            mApplication.getTransferFileBusiness().uploadAvatar(fileCompressed, uploadAvatarListener);
        }
    }

    /**
     * get email from device
     *
     * @param context
     * @return
     */
    public String getEmail(Context context) {
        try {
            Account account = getAccount(context);
            if (account == null) {
                return null;
            } else {
                return account.name;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
    }

    //http://developer.android.com/intl/vi/guide/topics/security/permissions.html#normal-dangerous
    @SuppressWarnings({"MissingPermission"})
    private static Account getAccount(Context context) {
        if (PermissionHelper.declinedPermission(context, Manifest.permission.GET_ACCOUNTS)) {
            return null;
        }
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts != null && accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    public void updateDomainTask() {
        mApplication.getXmppManager().changeConfigXmpp();
        //PopupHelper.getInstance(mApplication).showDialogProgressChangeDomain();
        // reconnect
//        if (IMService.isReady()) {
        if (mApplication.isReady()) {
//            IMService.getInstance().connectByToken();
            mApplication.connectByToken();
        } else {
            Log.i(TAG, "IMService not ready -> start service");
//            mApplication.startService(new Intent(mApplication.getApplicationContext(), IMService.class));

            mApplication.startIMService();
        }
    }

    public String getLocationId() {
        return locationId;
    }

    public void updateLocationId(String id) {
        if (id != null && !id.equals(locationId)) {
            this.locationId = id;
            mPref.edit().putString(Constants.PREFERENCE.PREF_MY_STRANGER_LOCATION, id).apply();
        }
        updateLastTimeUpdateLocation();
    }

    public void updateLastTimeUpdateLocation() {
        mPref.edit().putLong(Constants.PREFERENCE.PREF_LAST_UPDATE_STRANGER_LOCATION, TimeHelper.getCurrentTime())
                .apply();
    }

    public void checkAndSendIqGetLocation() {
        Log.i(TAG, "checkAndSendIqGetLocation 1");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                Log.i(TAG, "checkAndSendIqGetLocation 2");
                if (checkTimeOutGetMyLocation()) {
                    Log.i(TAG, "checkAndSendIqGetLocation 3");
                    IQInfo iqRegId = new IQInfo(IQInfo.NAME_SPACE_LOCATION);
                    iqRegId.setType(IQ.Type.GET);
                    try {
                        IQInfo result = (IQInfo) mApplication.getXmppManager().sendPacketThenWaitingResponse(iqRegId,
                                false);
                        if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                            HashMap<String, String> elements = result.getElements();
                            if (elements != null && "200".equals(elements.get("error"))) {
                                Log.i(TAG, "updateLocationId");
                                updateLocationId(elements.get("locationId"));
                            } else
                                Log.i(TAG, "fail updateLocationId");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        Log.i(TAG, "fail updateLocationId Exception: ", e);
                    }
                } else
                    Log.i(TAG, "checkAndSendIqGetLocation check fail ");
            }
        }).start();
    }

    private boolean checkTimeOutGetMyLocation() {
        if (TextUtils.isEmpty(locationId)) {
            Log.i(TAG, "locationId Empty");
            return true;
        } else {
            long lastUpdate = mPref.getLong(Constants.PREFERENCE.PREF_LAST_UPDATE_STRANGER_LOCATION, -1);
            int configTimeOut = ConvertHelper.parserIntFromString(mApplication.getConfigBusiness().
                    getContentConfigByKey(Constants.PREFERENCE.CONFIG.STRANGER_LOCATION_TIMEOUT), 24);
            int duration = (int) ((TimeHelper.getCurrentTime() - lastUpdate) / TimeHelper.ONE_HOUR_IN_MILISECOND);
            Log.i(TAG, "checkTimeOutGetMyLocation: lastUpdate: " + lastUpdate + " timeout: " + configTimeOut + " duration: " + duration);
            return duration >= configTimeOut;
        }
    }

    //TODO update avno number vào database trong th đăng ký, đổi sim ảo
    public void updateAVNONumber(String avnoNumber) {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            account.setAvnoNumber(avnoNumber);
            updateReengAccount(account);
        }
    }

    public String getAVNONumber() {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            return account.getAvnoNumber();
        }
        return null;
    }

    public void updateAVNOIdenfityCardFront(String urlFront) {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            account.setAvnoICFront(urlFront);
            updateReengAccount(account);
        }
    }

    public void updateAVNOIdenfityCardBack(String urlBack) {
        ReengAccount account = getCurrentAccount();
        if (account != null) {
            account.setAvnoICBack(urlBack);
            updateReengAccount(account);
        }
    }

    public void uploadDataIfNeeded() {
        if (mCurrentAccount.getNeedUpload()
                && !TextUtils.isEmpty(mCurrentAccount.getAvatarPath())) {
            mApplication.getReengAccountBusiness().processUploadAvatarTask(mCurrentAccount.getAvatarPath());
        }
        ImageProfile imageCover = mApplication.getImageProfileBusiness().getImageCover();
        if (imageCover != null && !imageCover.isUploaded()
                && !TextUtils.isEmpty(imageCover.getImagePathLocal())) {
            mApplication.getTransferFileBusiness().uploadCoverWhenConnected(imageCover.getImagePathLocal());
        }

        if (LogDebugHelper.getInstance(mApplication).isEnableLog()) {
            long lastTime = mPref.getLong(LogDebugHelper.PREF_UPLOAD_LOG_DEBUG_LAST_TIME, 0l);
            if (TimeHelper.checkTimeInDay(lastTime)) {
                Log.i(TAG, "uploaded file logdebug");
            } else {
                mApplication.getTransferFileBusiness().startUploadLog(false);
            }
        }
    }

    public void setListAdsRegisterVip(ArrayList<AdsRegisterVip> list) {
        if (listAdsRegisterVip != null && !listAdsRegisterVip.isEmpty()) {
            for (AdsRegisterVip ads : list) {
                for (AdsRegisterVip adsLocal : listAdsRegisterVip) {
                    if (ads.getCategoryId().equals(adsLocal.getCategoryId())) {
                        ads.setCountClick(adsLocal.getCountClick());
                        ads.setDateLastClick(adsLocal.getDateLastClick());
                        // TODO: 5/18/2020 bổ sung
                        ads.setCountTimeShow(adsLocal.getCountTimeShow());
                        ads.setTimeFirstShow(adsLocal.getTimeFirstShow());
                    }
                }
            }
        }
        listAdsRegisterVip = list;
        mPref.edit().putString(Constants.PREFERENCE.PREF_LIST_ADS_VIDEO_CATEGORY, new Gson().toJson(listAdsRegisterVip)).apply();
    }

    public AdsRegisterVip showDialogAdsRegisterVip(String categoryId) {
        if (listAdsRegisterVip == null || listAdsRegisterVip.isEmpty()) return null;
        for (AdsRegisterVip ads : listAdsRegisterVip) {
            if (ads.getCategoryId().equals(categoryId)) {       //tìm đối tượng ads
                long lastClick = ads.getDateLastClick();
                int countClick;
                if (TimeHelper.checkTimeInDay(lastClick)) {     //nếu lastclick cùng ngày thì lấy số lượt click ra
                    countClick = ads.getCountClick();
                    if (countClick > ads.getNumberShowDialog())
                        return null;            //nếu >5 thì return null, ko phải làm j nữa
                } else {
                    countClick = 0;                             //nếu khác ngày thì reset về 0
                }
                ads.setCountClick(countClick + 1);
                ads.setDateLastClick(System.currentTimeMillis());
                mPref.edit().putString(Constants.PREFERENCE.PREF_LIST_ADS_VIDEO_CATEGORY, new Gson().toJson(listAdsRegisterVip)).apply();
                if (countClick == 0 || countClick == ads.getNumberShowDialog() - 1) {
                    // TODO: 5/18/2020 check 2 lần 7 ngày
                    long nowTime = System.currentTimeMillis();
                    long timeFisrtShow = ads.getTimeFirstShow();
                    boolean checkOneWeek = DateTimeUtils.checkDateTimeOneWeek(timeFisrtShow, nowTime);
                    if (checkOneWeek) {
                        ads.setCountTimeShow(0);
                    } else {
                        if (ads.getCountTimeShow() > 1) {
                            return null;
                        }
                    }
                    if (ads.getCountTimeShow() == 0) {
                        ads.setTimeFirstShow(nowTime);
                    }
                    ads.setCountTimeShow(ads.getCountTimeShow() + 1);
                    mPref.edit().putString(Constants.PREFERENCE.PREF_LIST_ADS_VIDEO_CATEGORY, new Gson().toJson(listAdsRegisterVip)).apply();
                    return ads;
                }
                return null;
            }
        }
        return null;
    }

    public AdsRegisterVip getAdsRegisterVipByCategory(String category) {
        if (listAdsRegisterVip == null || listAdsRegisterVip.isEmpty() || TextUtils.isEmpty(category))
            return null;
        else {
            for (AdsRegisterVip ads : listAdsRegisterVip) {
                if (category.equals(ads.getCategoryId())) return ads;
            }
        }
        return null;
    }

    private boolean isProcessingChangeNumber;

    public void processChangeNumber() {
        /*String listNumberChange = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.PREFIX_CHANGENUMBER);
        String myJid = getJidNumber();
        if (TextUtils.isEmpty(listNumberChange) || "-".equals(listNumberChange) || TextUtils.isEmpty(myJid)) {
            return;
        }
        String[] list = listNumberChange.split(",");
        int length = list.length;
        for (int i = 0; i < length; i++) {
            String number = list[i];
            if (myJid.startsWith(number)) {
                sendIQChangeNumber();
                return;
            }
        }*/
        Log.i(TAG, "processChangeNumber:" + isProcessingChangeNumber);
        if (isProcessingChangeNumber) return;
        String newNumber = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(getJidNumber());
        if (newNumber != null) {
            sendIQChangeNumber();
        }
    }

    private void sendIQChangeNumber() {
        isProcessingChangeNumber = true;
        //ArrayList<String>, Void, XMPPResponseCode

        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                IQChangeNumber iqChange = new IQChangeNumber();
                iqChange.setType(IQ.Type.GET);
                iqChange.setCurrentJid(getJidNumber());
                try {
                    IQChangeNumber result = (IQChangeNumber) mApplication.getXmppManager().sendPacketThenWaitingResponse(iqChange, false);
                    if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                        if (result.getErrorCode() == 200 && !TextUtils.isEmpty(result.getNewJid()) && !TextUtils.isEmpty(result.getOtp())) {
                            mApplication.getXmppManager().manualDisconnect();
                            LoginByCodeAsyncTask loginByCodeAsyncTask = new LoginByCodeAsyncTask(result.getOtp(), result.getNewJid(), getRegionCode());
                            loginByCodeAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            Log.i(TAG, "error sendIQChangeNumber: " + result.getErrorCode());
                            isProcessingChangeNumber = false;
                        }
                    } else {
                        Log.i(TAG, "error sendIQChangeNumber: " + result.getErrorCode());
                        isProcessingChangeNumber = false;
                    }
                } catch (Exception e) {
                    Log.e(TAG, "error sendIQChangeNumber");
                    Log.e(TAG, "Exception", e);
                    isProcessingChangeNumber = false;
                }
            }
        }).start();
    }

    public void setProcessingChangeNumber(boolean processingChangeNumber) {
        isProcessingChangeNumber = processingChangeNumber;
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        String jid;
        String regionCode;

        public LoginByCodeAsyncTask(String mPassword, String jid, String regionCode) {
            this.mPassword = mPassword;
            this.jid = jid;
            this.regionCode = regionCode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            LoginBusiness loginBusiness = mApplication.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(mApplication, jid,
                    mPassword, regionCode, false, Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            Log.i(TAG, "LoginByCodeAsyncTask changenumber responseCode: " + responseCode);
            try {

                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    Log.i(TAG, "E200_OK: " + responseCode);
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(jid);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);

                    //                    mApplication.registerPreKey();
                } else {
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            isProcessingChangeNumber = false;
        }
    }

    public boolean isProcessingChangeNumber() {
        return isProcessingChangeNumber;
    }

    public void setEmulatorCam(boolean emulatorCam) {
        this.isEmulatorCam = emulatorCam;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_MOCHA_EMULATOR_CAM, this.isEmulatorCam).apply();
    }

    public boolean isEmulatorCam() {
        return isEmulatorCam;
    }

    public boolean isCalloutGlobalEnable() {
//        return true;
        return isCalloutGlobalEnable;
    }

    public boolean isEnableChangeNumber() {
        return isEnableChangeNumber;
    }

    public int getUsingDesktop() {
        return usingDesktop;
    }

    public boolean isTranslatable() {
        return translatable == 1;
    }

    public boolean isEnableFCallViaFS() {
        return fCallViaFS == 1;
    }

    private static final String PREFIX_GAME_ID = "pf";

    public boolean isFirstTimePlayGameIQ(String idGame) {
        if (!TextUtils.isEmpty(listGameIQPlay)) {
            if (listGameIQPlay.contains(PREFIX_GAME_ID + idGame)) {
                return false;
            }
        }
        addListGameIQPlay(idGame);
        return true;
    }

    public void addListGameIQPlay(String idGamePlayed) {
        this.listGameIQPlay = listGameIQPlay + PREFIX_GAME_ID + idGamePlayed + ",";
        mPref.edit().putString(Constants.PREFERENCE.PREF_LIST_GAME_IQ_PLAYED, listGameIQPlay).apply();
    }

    private boolean isInProgressLoginFromAnonymous;
    private boolean isAnonymous = false;

    public boolean isAnonymousLogin() {
        if (mCurrentAccount == null) return false;
        return isAnonymous;
    }

    public void setAnonymous(boolean anonymous) {
        isAnonymous = anonymous;
        mPref.edit().putBoolean(Constants.PREFERENCE.PREF_IS_ANONYMOUS_LOGIN, anonymous).apply();
    }

    public boolean isInProgressLoginFromAnonymous() {
        return isInProgressLoginFromAnonymous;
    }

    public void setInProgressLoginFromAnonymous(boolean inProgressLoginFromAnonymous) {
        isInProgressLoginFromAnonymous = inProgressLoginFromAnonymous;
    }

    private SignalProtocolStore signalProtocolStore;
    private byte[] e2eSerialize;

    public SignalProtocolStore getSignalProtocolStore() {

        try {
            if (e2eSerialize == null) {
                String signalStoreBase64 = mPref.getString(Constants.KEY_SIGNAL_STORE, "");
                if (!TextUtils.isEmpty(signalStoreBase64)) {
                    byte[] serialize = Base64.decode(signalStoreBase64);
                    this.e2eSerialize = serialize;
                } else {
                    registerPreKey();
                }
            }
            signalProtocolStore = SignalProtocolStore.from(("PRE_" + getJidNumber()).hashCode(), ("SIGNED_" + getJidNumber()).hashCode(), e2eSerialize);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return signalProtocolStore;
    }

    public boolean isE2eEnable() {
        return e2eEnable;
    }

    public void registerPreKey() {
        try {
            String phoneNumber = getJidNumber();

            boolean isRegisterE2E = mPref.getBoolean(Constants.KEY_REGISTER_E2E, false);
            if (isRegisterE2E) return;
//            if (isRegisterE2E) {
//                if (signalProtocolStore == null) {
//                    //Lay signalEntity tu DB
//                    String signalStoreBase64 = mPref.getString(Constants.KEY_SIGNAL_STORE, "");
//
//                    Log.e(TAG, "signalStoreBase64: " + signalStoreBase64 + " | phone: " + getJidNumber());
//
//                    if (!TextUtils.isEmpty(signalStoreBase64)) {
//                        byte[] serialize = Base64.decode(signalStoreBase64);
//                        this.e2eSerialize = serialize;
//                        signalProtocolStore = SignalProtocolStore.from(("PRE_" + phoneNumber).hashCode(), ("SIGNED_" + phoneNumber).hashCode(), serialize);
//                        Log.i(TAG, "------ get key success");
//                    }
//                }
//                return;
//            }

            SignalEntity signalEntity = new SignalEntity(("PRE_" + phoneNumber).hashCode(), ("SIGNED_" + phoneNumber).hashCode(), phoneNumber);
            signalProtocolStore = signalEntity.getStore();
            final byte[] serialize = signalProtocolStore.serialize(("PRE_" + phoneNumber).hashCode(), ("SIGNED_" + phoneNumber).hashCode());
            this.e2eSerialize = serialize;
            Log.i(TAG, "------ gen key success");

            PreKeyBundle preKeyBundle = signalEntity.getPreKey();
            final String preKey = SignalUtils.preKeyBundle2String(preKeyBundle);

            if (!TextUtils.isEmpty(preKey)) {
                //TODO send key via xmpp
                if (!mApplication.getXmppManager().isAuthenticated())
                    return; //ko ket noi toi xmpp server thi khong gui
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                        IQInfo iqRegId = new IQInfo(IQInfo.NAME_SPACE_E2E);
                        iqRegId.setType(IQ.Type.SET);
                        iqRegId.addElements("clientType", "Android");
                        iqRegId.addElements("e2e_prekey", preKey);
                        try {
                            IQ result = mApplication.getXmppManager().sendPacketThenWaitingResponse(iqRegId, false);
                            if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                                Log.i(TAG, "--------- upload key success");
                                mPref.edit().putBoolean(Constants.KEY_REGISTER_E2E, true).apply();
                                //Luu DB serialize
                                mPref.edit().putString(Constants.KEY_SIGNAL_STORE, Base64.encodeBytes(serialize)).apply();

                                //Cap nhat prekey vao db
                                updatePreKey(preKey);
                            } else {
                                //do nothing
                            }
                        } catch (Exception e) {
                            //do nothing
                            Log.f(TAG, "Exception", e);
                        }
                    }
                }).start();

                //TODO send key via api
                /*String url = UrlConfigHelper.getInstance(mApplication).getConfigDomainFile() + "/ReengBackendBiz/e2e/setPreKeys";
                final long timestamp = TimeHelper.getCurrentTime();
                final String msisdn = getJidNumber();
                StringBuilder sb = new StringBuilder();
                sb.append(getJidNumber())
                        .append(msisdn)
                        .append(preKey)
                        .append(Constants.HTTP.CLIENT_TYPE_STRING)
                        .append(Config.REVISION)
                        .append(getToken())
                        .append(timestamp);
                final String security = HttpHelper.encryptDataV2(mApplication, sb.toString(), getToken());

                StringRequest request = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String data) {
                                try {
                                    JSONObject jsonObject = new JSONObject(data);
                                    int code = jsonObject.optInt("code");
                                    String desc = jsonObject.optString("desc");

                                    if (code == 200) {
                                        Log.i(TAG, "--------- upload key success");
                                        mPref.edit().putBoolean(Constants.KEY_REGISTER_E2E, true).apply();
                                        //Luu DB serialize
                                        mPref.edit().putString(Constants.KEY_SIGNAL_STORE, Base64.encodeBytes(serialize)).apply();

                                        //Cap nhat prekey vao db
                                        updatePreKey(preKey);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        HashMap<String, String> params = new HashMap<>();
                        params.put(Constants.HTTP.REST_MSISDN, msisdn);
                        params.put("prekey", preKey);
                        params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                        params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                        params.put(Constants.HTTP.TIME_STAMP, String.valueOf(timestamp));
                        params.put(Constants.HTTP.DATA_SECURITY, security);
                        return params;
                    }
                };
                VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
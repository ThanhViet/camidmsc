package com.metfone.selfcare.business;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.stringee.AudioStatsListener;
import com.stringee.CallConnectionListener;
import com.stringee.SignalingStatisticElement;
import com.stringee.StringeeCall;
import com.stringee.StringeeCallListener;
import com.stringee.StringeeConstant;
import com.stringee.StringeeIceCandidate;
import com.stringee.StringeeIceServer;
import com.stringee.StringeeSessionDescription;
import com.stringee.StringeeStream;
import com.metfone.selfcare.activity.MochaCallActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.call.CallConstant;
import com.metfone.selfcare.helper.call.SdpUtils;
import com.metfone.selfcare.listeners.NetworkConnectivityChangeListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.model.CallData;
import org.jivesoftware.smack.model.IceServer;
import org.jivesoftware.smack.packet.ReengCallPacket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.IceCandidate;
import org.webrtc.SurfaceViewRenderer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by toanvk2 on 10/6/2016. ..
 */

public class CallRtcHelper implements StringeeCallListener, AudioStatsListener,
        CallConnectionListener, NetworkConnectivityChangeListener {
    private static final String TAG = CallRtcHelper.class.getSimpleName();
    private static final String TAG_LOG = "check_call_connection";
    private static final String CODEC_FREE = "opus";
    private static final String CODEC_OUT = "PCMA";
    private static final String CODEC_VIDEO = "VP8";

    private static CallRtcHelper mInstance;
    private ApplicationController mApplication;
    private StringeeCall currentCall;
    private boolean isCallOut = false;
    private String audioCodec;
    private String videoCodec;
    private final LinkedList<CallData> callDataQueue = new LinkedList<>();
    private SurfaceViewRenderer localSurfaceRenderer, remoteSurfaceRenderer;
    // quality
    private double mPrevTimestamp = 0;
    private long mPrevBytesAudioRev = 0;
    private long mPrevBytesVideoRev = 0;
    private long mBwAudioReceived = 0, mBwVideoReceived = 0;
    //
    private long mPrevBytesAudioSent = 0;
    private long mPrevBytesVideoSent = 0;
    private long mBwAudioSent = 0, mBwVideoSent = 0;

//    private StringeeStream localStream;
//    private StringeeStream remoteStream;

    public static synchronized CallRtcHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new CallRtcHelper(application);
        }
        return mInstance;
    }

    private CallRtcHelper(ApplicationController application) {
        this.mApplication = application;
    }


    public void startCall(int callType, LinkedList<StringeeIceServer> iceServers,
                          boolean isCallOut, boolean isVideoCall, String codec, String codecVideo) {
        boolean isCaller = callType == CallConstant.TYPE_CALLER;
        Log.i(TAG, "start call : " + isCaller + " isCallOut: " + isCallOut + " isModeVideoCall: " + isVideoCall);
        LinkedList<StringeeIceServer> iceServers1 = new LinkedList<>();
        // iceServers1.add(new StringeeIceServer("stun:125.212.226.9:3478", "01689920034", "123456"));
        iceServers1.add(new StringeeIceServer("turn:171.255.193.153:3478?transport=udp", "01689920034", "123456"));
        // Create peer connection constraints.
        this.isCallOut = isCallOut;
        if (TextUtils.isEmpty(codec)) {// chưa có codec thì set gia trị default
            audioCodec = isCallOut ? CODEC_OUT : CODEC_FREE;
        } else {
            audioCodec = codec;
        }
        if (TextUtils.isEmpty(codecVideo)) {// chưa có codec thì set gia trị default
            videoCodec = CODEC_VIDEO;
        } else {
            videoCodec = codecVideo;
        }
        currentCall = new StringeeCall(this);
        //currentCall.setConnectionListener(this);
        currentCall.setPreferedAudioCodec(audioCodec);
        currentCall.setPreferedVideoCodec(videoCodec);
        currentCall.setIceServers(iceServers);
        //currentCall.setIceServers(iceServers1);
        currentCall.init(mApplication);
        CallBusiness callBusiness = mApplication.getCallBusiness();
        currentCall.startCall(isCaller, isCallOut, isVideoCall, StringeeConstant.VIDEO_QUALITY_HD,
                null, callBusiness.getIceTimeout(), callBusiness.getBundlePolicy(),
                callBusiness.getRtcpMuxPolicy(), callBusiness.getIceTransportsType());
        setMute();
        NetworkHelper.addNetworkConnectivityChangeListener(this);

        //
//        if(callType == CallConstant.TYPE_CALLEE) {
//            putAllCallDataFromQueue();
//        }
    }

    public void startCall(int callType, LinkedList<StringeeIceServer> iceServers, boolean isCallOut,
                          boolean isVideoCall, String codec, String codecVideo, CallData sdpCallData) {
        startCall(callType, iceServers, isCallOut, isVideoCall, codec, codecVideo);
        if (sdpCallData != null) {
            try {
                JSONObject dataObject = new JSONObject(sdpCallData.getData());
                processSdp(dataObject, isCallOut);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public void startCall(int callType, LinkedList<StringeeIceServer> iceServers, boolean isCallOut, String codec, String codecVideo) {
        startCall(callType, iceServers, isCallOut, false, codec, codecVideo);
    }

    public void stopCall() {
        Log.i(TAG, "stop call");
        isConnected = false;
        countRestartICEOnFail = 0;
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (currentCall == null) {
            Log.e(TAG, "Could not found any call");
            return;
        }
        currentCall.setListener(null);
        currentCall.stopCall();
        //currentCall.setConnectionListener(null);
        if (localSurfaceRenderer != null) localSurfaceRenderer.release();
        if (remoteSurfaceRenderer != null) remoteSurfaceRenderer.release();
        //currentCall.cleanupSdk();
        mPrevTimestamp = 0;
        mPrevBytesAudioRev = 0;
        mPrevBytesVideoRev = 0;
        mBwAudioReceived = 0;
        mBwVideoReceived = 0;

        mPrevBytesAudioSent = 0;
        mPrevBytesVideoSent = 0;
        mBwAudioSent = 0;
        mBwVideoSent = 0;
        currentCall = null;
        isCallOut = false;
        callDataQueue.clear();
        lastTimeNetworkChange = 0;
        restartReason = null;
//        localStream = null;
//        remoteStream = null;
        /*if (mHandler != null)
            mHandler.removeCallbacks(runnable);*/
    }

    public void restartICEOnFail() {
        //Log.i(TAG, "restartICEOnFail: " + NetworkHelper.isConnectInternet(mApplication) + " " + currentCall + " " + stateICEConnection.name());
        if (currentCall != null) {
            currentCall.addSignalingStatistics(new SignalingStatisticElement("restartICEOnFail", "restartICEOnFail: " + NetworkHelper.isConnectInternet(mApplication) + " " + currentCall + " " + stateICEConnection.name()));
        }

        if (NetworkHelper.isConnectInternet(mApplication) && currentCall != null
                && stateICEConnection == StringeeConnectionState.FAILED) {
            forceRestartICE();
        }
    }

    public void forceRestartICE() {
        CallBusiness callBusiness = mApplication.getCallBusiness();
        if (!callBusiness.isCaller()) return;
        Log.i(TAG, "forceRestartICE: ");
        if (currentCall != null)
            currentCall.restartICE();
        callBusiness.setStartedRestartICE(true);
        EventBus.getDefault().post(new MochaCallActivity.EventConnection(StringeeConnectionState.RESTARTICE_ONFAIL));
        callBusiness.sendMessageStateICE(StringeeConnectionState.RESTARTICE_ONFAIL);
    }

    public void setLocalSurfaceRenderer(SurfaceViewRenderer renderer) {
        this.localSurfaceRenderer = renderer;
    }

    public SurfaceViewRenderer getLocalSurfaceRenderer() {
        return localSurfaceRenderer;
    }

    public void setRemoteSurfaceRenderer(SurfaceViewRenderer renderer) {
        this.remoteSurfaceRenderer = renderer;
    }

    public SurfaceViewRenderer getRemoteSurfaceRenderer() {
        return remoteSurfaceRenderer;
    }

    public void getReportQuality() {
        Log.i(TAG, "getReportQuality");
        if (currentCall != null)
            currentCall.getStats(this);
    }

    public void setMute() {
        if (currentCall != null) {
            currentCall.setMicrophoneMute(mApplication.getCallBusiness().isMute());
        }
    }

    public void enableVideo(boolean isEnable) {
        if (currentCall != null) {
            currentCall.enableVideo(isEnable);
        }
    }

    public void renderStream(Context context, StringeeStream stream, boolean zOverlay) {
        if (currentCall != null) {
            currentCall.renderStream(stream, zOverlay);
        }
    }

    private boolean isFrontCamera = true;

    public void switchCamera(ImageView imageView) {
        if (currentCall != null) {
            currentCall.switchCamera(new CameraVideoCapturer.CameraSwitchHandler() {
                @Override
                public void onCameraSwitchDone(boolean isFrontCam) {
//                    isFrontCamera = isFrontCam;
                    if (getLocalSurfaceRenderer() != null) {
                        if (!isFrontCam) {
                            getLocalSurfaceRenderer().setMirror(false);
                            imageView.setImageResource(R.drawable.ic_call_camera_active);
                        } else {
                            getLocalSurfaceRenderer().setMirror(true);
                            imageView.setImageResource(R.drawable.ic_call_camera);
                        }

                    }
                }

                @Override
                public void onCameraSwitchError(String s) {

                }
            });
        }
    }

    public void addCallDataToQueue(CallData callData) {
        callDataQueue.add(callData);
    }

    public void putAllCallDataFromQueue() {
        while (!callDataQueue.isEmpty() && currentCall != null) {
            CallData callData = callDataQueue.get(0);
            callDataQueue.remove(0);
            addCallData(callData, isCallOut);
        }
    }

    public void addCallData(CallData callData, boolean isCallOut) {
        if (callData == null) return;
        Log.i(TAG, "addCallData: " + callData.getData());
        try {
            JSONObject dataObject = new JSONObject(callData.getData());
            if (currentCall != null) {
                if ("sdp".equalsIgnoreCase(callData.getType()) || "set-sdp".equalsIgnoreCase(callData.getType())) {
                    processSdp(dataObject, isCallOut);
                } else if ("candidate".equalsIgnoreCase(callData.getType())) {
                    processCandidate(dataObject, isCallOut);
                }
            } else {
                Log.i(TAG, "===================== luu data vao queue");
                callDataQueue.add(callData);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }


    private void processSdp(JSONObject data, boolean isCallOut) throws JSONException {
        int sdpType = data.getInt("type");
        String sdp = data.getString("sdp");
        StringeeSessionDescription.Type type = StringeeSessionDescription.Type.OFFER;
        if (sdpType == 0) {
            type = StringeeSessionDescription.Type.OFFER;
            Log.i("Stringee", "+++++++++++++++++++++++ sdp OFFER received");
        } else if (sdpType == 1) {
            type = StringeeSessionDescription.Type.PRANSWER;
        } else if (sdpType == 2) {
            Log.i(TAG, "+++++++++++++++++++++++ sdp ANSWER received");
            type = StringeeSessionDescription.Type.ANSWER;
        }
        StringeeSessionDescription sessionDescription = new StringeeSessionDescription(type, sdp);
        Log.f(TAG, TAG_LOG + " add callData SDP");
        currentCall.setRemoteDescription(sessionDescription);
    }

    private void processCandidate(JSONObject data, boolean isCallOut) throws JSONException {
        Log.i(TAG, "+++++ co goi tin signaling ... candidate");
        String sdpMid = data.getString("sdpMid");
        int sdpMLineIndex = data.getInt("sdpMLineIndex");
        String sdp2;
        sdp2 = data.getString("sdp");
        StringeeIceCandidate iceCandidate = new StringeeIceCandidate(sdpMid, sdpMLineIndex, sdp2);
        Log.f(TAG, TAG_LOG + " add callData Candidate");
        currentCall.addIceCandidate(iceCandidate, isCallOut);
    }

    //TODO addlog

    /**
     * add StringeeCallListener
     */
    @Override
    public void onSetSdpSuccess(boolean isSetLocalSDP, StringeeSessionDescription sessionDescription) {
        // Log.f(TAG, TAG_LOG + " didCreateSDP ");
        // Log.d(TAG, "didCreateSDP: " + sessionDescription);
        if (currentCall != null) {
            currentCall.addSignalingStatistics(new SignalingStatisticElement("onSetSdpSuccess", sessionDescription.description));
        }
        if (!mApplication.getCallBusiness().isExistCall()) {
            Log.i(TAG, "!ExistCall -> (chờ result mà ng dùng end luôn thì ko xử lý nữa)");
            return;
        }
        String sdpDescription = SdpUtils.preferCodec(sessionDescription.description, audioCodec, true);
        if (isCallOut) {
            if (!isSetLocalSDP) {
                mApplication.getCallBusiness().processSendCallOutData(sdpDescription, true);//TODO send call out sdp data
            }
        } else {
            try {
                JSONObject data = new JSONObject();
                data.put("sdp", sdpDescription);
                if (sessionDescription.type == StringeeSessionDescription.Type.OFFER) {
                    data.put("type", 0);
                } else if (sessionDescription.type == StringeeSessionDescription.Type.ANSWER) {
                    data.put("type", 2);
                }
                CallData callData = new CallData(isSetLocalSDP ? "set-sdp" : "sdp", data.toString());
                mApplication.getCallBusiness().processSendCallData(callData, true);

            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }


        }
    }

    //TODO addlog
    @Override
    public void onCreateIceCandidate(StringeeIceCandidate iceCandidate) {
        Log.d(TAG, "didGenerateIceCandidate: " + iceCandidate.sdp);
        Log.d(TAG, "didGenerateIceCandidate sdpMid: " + iceCandidate.sdpMid + " LineIndex: " + iceCandidate.sdpMLineIndex);
        mApplication.getCallBusiness().countCandidate();
        if (isCallOut) {
            mApplication.getCallBusiness().processSendCallOutData(iceCandidate.sdp, false);// TODO send call out
            // candidate data
        } else {
            String[] sdpA = iceCandidate.sdp.split(" ");

            String transport = sdpA[2];
            String candType = sdpA[7];
            if (transport.equals("udp") && candType.equals("relay")) {
                try {
                    JSONObject candidate = new JSONObject();
                    candidate.put("sdpMid", iceCandidate.sdpMid);
                    candidate.put("sdpMLineIndex", iceCandidate.sdpMLineIndex);
                    candidate.put("sdp", iceCandidate.sdp);
                    CallData callData = new CallData("candidate", candidate.toString());
                    Log.d("CHECK_CALL_DATA: ", candidate.toString());
                    mApplication.getCallBusiness().processSendCallData(callData, false);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            } else {
                Log.d(TAG, "candidate != relay");
            }
        }
    }

    private Timer timer;
    private boolean isConnected;
    private long lastTimeNetworkChange = 0;
    private StringeeConnectionState stateICEConnection;
    private int countRestartICEOnFail = 0;
    private Handler mHandler;
    private ReengCallPacket.RestartReason restartReason;

    //TODO addlog
    @Override
    public void onChangeConnectionState(StringeeConnectionState state) {
        try {
            stateICEConnection = state;
            if (BuildConfig.DEBUG) {
                EventBus.getDefault().post(new MochaCallActivity.EventConnection(state));
            }
            // Log.f(TAG, "didChangeConnectionState: " + state);
            if (currentCall != null) {
                currentCall.addSignalingStatistics(new SignalingStatisticElement("onChangeConnectionState", state.toString()));
            }

            final CallBusiness callBusiness = mApplication.getCallBusiness();
            if (restartReason != null && state == StringeeConnectionState.CONNECTED)
                callBusiness.sendMessageStateICE(StringeeConnectionState.RESTARTICE_CONNECTED);
            else
                callBusiness.sendMessageStateICE(state);

            if (state == StringeeConnectionState.CHECKING) {// connecting
                //callBusiness.notifyOnConnectStateChange(CallConstant.STATE.CONNECTING);
            } else if (state == StringeeConnectionState.CONNECTED) {// connected
                isConnected = true;
                countRestartICEOnFail = 0;
                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }

                if (isCallOut && callBusiness.getCurrentTypeCall() == CallConstant.TYPE_CALLER) {
                    Log.d(TAG, "----notifyOnConnectStateChange: " + state);
                    callBusiness.notifyOnConnectStateChange(CallConstant.STATE.CALLEE_STARTED);
                } else {
                    callBusiness.notifyOnConnectStateChange(CallConstant.STATE.CONNECTED);
                }
            } else if (state == StringeeConnectionState.FAILED && callBusiness.isEnableRestartICE()) {
                Log.i(TAG, "callee state: " + mApplication.getCallBusiness().getCurrentTypeCall());
                if (!isConnected) {     //nếu chưa connected bao giờ thì restartICE
                    restartReason = ReengCallPacket.RestartReason.NOT_CONNECTED;
                    restartICETimerOnFail();
                } else {
                    //nếu ko có thay đổi mạng từ trước đó 5s
                    if (callBusiness.getNetwork2failedTime() > 0 &&
                            (System.currentTimeMillis() - lastTimeNetworkChange > callBusiness.getNetwork2failedTime())) {
                        Log.i(TAG, "System.currentTimeMillis() - lastTimeNetworkChange > callBusiness.getNetwork2failedTime()");
                        restartReason = ReengCallPacket.RestartReason.BEFORE_FAILED_5s;
                        restartICETimerOnFail();
                        lastTimeNetworkChange = System.currentTimeMillis();
                    }
                    Log.i(TAG, "callBusiness.getDelayRestartOnFailed():" + callBusiness.getDelayRestartOnFailed());
                    if (callBusiness.getDelayRestartOnFailed() >= 0) {
                        final Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                timer.cancel();
                                restartReason = ReengCallPacket.RestartReason.AFTER_FAILED_5s;
                                restartICEOnFail();
                            }
                        }, callBusiness.getDelayRestartOnFailed());
//                        if (mHandler == null) mHandler = new Handler();
//                        mHandler.removeCallbacks(runnable);
//                        mHandler.postDelayed(runnable, callBusiness.getDelayRestartOnFailed());
                    }
                }

            }
        } catch (Exception e) {
            Log.e(TAG, "onChangeConnectionState: ", e);
        }
    }

    /*Runnable runnable = new Runnable() {
        @Override
        public void run() {
            restartReason = ReengCallPacket.RestartReason.AFTER_FAILED_5s;
            restartICEOnFail();
        }
    };*/

    public StringeeConnectionState getStateICEConnection() {
        return stateICEConnection;
    }

    private boolean isRunningRestart;


    private void restartICETimerOnFail() {
        if (isRunningRestart) return;
        final CallBusiness callBusiness = mApplication.getCallBusiness();
        if (!callBusiness.isCaller()) return;
//        Log.i(TAG, "-------setting:------ isEnableRestartICE: " + callBusiness.isEnableRestartICE()
//                + " iceTimeout: " + callBusiness.getIceTimeout() + " restartICEDelay: " + callBusiness.getRestartICEDelay()
//                + " restartICEPeriod: " + callBusiness.getRestartICEPeriod() + " restartICELoop: " + callBusiness.getRestartICELoop()
//                + " zeroBwEndCall: " + callBusiness.getZeroBwEndCall() + " network2failedTime: " + callBusiness.getNetwork2failedTime()
//                + " isValidRestartICE: " + callBusiness.isValidRestartICE());

        currentCall.addSignalingStatistics(new SignalingStatisticElement("restartICETimerOnFail", "-------setting:------ isEnableRestartICE: " + callBusiness.isEnableRestartICE()
                + " iceTimeout: " + callBusiness.getIceTimeout() + " restartICEDelay: " + callBusiness.getRestartICEDelay()
                + " restartICEPeriod: " + callBusiness.getRestartICEPeriod() + " restartICELoop: " + callBusiness.getRestartICELoop()
                + " zeroBwEndCall: " + callBusiness.getZeroBwEndCall() + " network2failedTime: " + callBusiness.getNetwork2failedTime()
                + " isValidRestartICE: " + callBusiness.isValidRestartICE()));
        if (currentCall != null && mApplication.getCallBusiness().getCurrentTypeCall() == CallConstant.TYPE_CALLER
                && callBusiness.isValidRestartICE()) {
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    restartICEOnFail();
                    countRestartICEOnFail++;
                    if (countRestartICEOnFail == callBusiness.getRestartICELoop()) {
                        isRunningRestart = false;
                        if (timer != null)
                            timer.cancel();
//                        mApplication.getCallBusiness().handleDeclineCall(false);
                        cancel();
                    }
                }
            };

            if (timer == null) {
                Log.i(TAG, "timer = null");
                timer = new Timer();
            } else {
                Log.i(TAG, "timer cancel");
                timer.cancel();
            }
            isRunningRestart = true;
            timer.scheduleAtFixedRate(timerTask, callBusiness.getRestartICEDelay(), callBusiness.getRestartICEPeriod());
        }
    }

    public void setRestartReason(ReengCallPacket.RestartReason restartReason) {
        this.restartReason = restartReason;
    }

    public ReengCallPacket.RestartReason getRestartReason() {
        return restartReason;
    }

    /*private void restartICETimerNetworkChange() {
        if (currentCall != null && mApplication.getCallBusiness().getCurrentTypeCall() == CallConstant.TYPE_CALLER
                && mApplication.getCallBusiness().isEnableRestartICE()) {
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    countRestartICEOnNetworkChange++;
                    if (stateICEConnection != StringeeConnectionState.FAILED) {
                        return;
                    }
                    if (countRestartICEOnNetworkChange == COUNT_RETRY + 1) {
                        timerNetworkChange.cancel();
                        mApplication.getCallBusiness().handleDeclineCall(false);
                    }
                    restartICEOnFail();
                }
            };

            if (timerNetworkChange == null) {
                Log.i(TAG, " timerNetworkChange null");
                timerNetworkChange = new Timer();
            } else {
                Log.i(TAG, "timerNetworkChange cancel");
                timerNetworkChange.cancel();
            }
            timerNetworkChange.schedule(timerTask, 5000, 5000);
        }
    }*/

    @Override
    public void onLocalStreamCreated(StringeeStream stringeeStream) {
//        localStream = stringeeStream;
        CallBusiness.onLocalStreamCreated(stringeeStream);
        if (currentCall != null) {
            currentCall.addSignalingStatistics(new SignalingStatisticElement("onLocalStreamCreated", ""));
        }

    }

    @Override
    public void onAddStream(StringeeStream stringeeStream) {
//        remoteStream = stringeeStream;
        CallBusiness.onAddStream(stringeeStream);
        if (currentCall != null) {
            currentCall.addSignalingStatistics(new SignalingStatisticElement("onAddStream", ""));
        }
    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
        Log.i(TAG, "onIceCandidatesRemoved");
        if (currentCall != null) {
            currentCall.addSignalingStatistics(new SignalingStatisticElement("onIceCandidatesRemoved", ""));
        }
        if (isCallOut) {
            Log.f(TAG, "drop remove-candidate callout ");
            return;
        }
        EventBus.getDefault().post(new MochaCallActivity.EventConnection(StringeeConnectionState.ICE_REMOVE));
        JSONArray jsa = new JSONArray();
        for (IceCandidate ice : iceCandidates) {

            try {

                JSONObject candidate = new JSONObject();
                candidate.put("sdpMid", ice.sdpMid);
                candidate.put("sdpMLineIndex", ice.sdpMLineIndex);
                candidate.put("sdp", ice.sdp);
                jsa.put(candidate);


            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        if (jsa.length() > 0) {
            CallData callData = new CallData("remove-candidate", jsa.toString());
            mApplication.getCallBusiness().processSendCallData(callData, false);
        }


    }

    @Override
    public void onAudioStats(StringeeStream.StringeeAudioStats stats) {
        double audioTimestamp = stats.timeStamp / 1000;
        //initialize values
        //mPrevTimestamp == 0
        if (Double.compare(mPrevTimestamp, 0) == 0) {
            mPrevTimestamp = audioTimestamp;
            mPrevBytesAudioRev = stats.audioBytesReceived;
            mPrevBytesVideoRev = stats.videoBytesReceived;
            mPrevBytesAudioSent = stats.audioBytesSent;
            mPrevBytesVideoSent = stats.videoBytesSent;
        } else {
            //calculate video bandwidth
            double duration = audioTimestamp - mPrevTimestamp;
            mBwAudioReceived = (long) ((8 * (stats.audioBytesReceived - mPrevBytesAudioRev)) / duration);
            mBwVideoReceived = (long) ((8 * (stats.videoBytesReceived - mPrevBytesVideoRev)) / duration);
            mBwAudioSent = (long) ((8 * (stats.audioBytesSent - mPrevBytesAudioSent)) / duration);
            mBwVideoSent = (long) ((8 * (stats.videoBytesSent - mPrevBytesVideoSent)) / duration);
            mPrevTimestamp = audioTimestamp;
            mPrevBytesAudioRev = stats.audioBytesReceived;
            mPrevBytesVideoRev = stats.videoBytesReceived;
            mPrevBytesAudioSent = stats.audioBytesSent;
            mPrevBytesVideoSent = stats.videoBytesSent;
            Log.d(TAG, "[check bandwidth]" + "\nAudioSent: " + mBwAudioSent + " (bps)" + " VideoSent: " + mBwVideoSent + " (bps)"
                    + "\nAudioRev: " + mBwAudioReceived + " (bps)" + " VideoRev: " + mBwVideoReceived + " (bps)");
            mApplication.getCallBusiness().onQualityReported(mBwAudioReceived, mBwVideoReceived, mPrevBytesAudioSent, mPrevBytesVideoSent);
        }
    }

    public static LinkedList<StringeeIceServer> convertStringeeIceServers(ArrayList<IceServer> iceServers) {
        if (iceServers == null) return new LinkedList<>();
        LinkedList<StringeeIceServer> stringeeIceServers = new LinkedList<>();
        for (IceServer iceServer : iceServers) {
            stringeeIceServers.add(new StringeeIceServer(iceServer.getDomain(), iceServer.getUser(), iceServer
                    .getCredential()));
        }
        return stringeeIceServers;
    }

    public static ArrayList<IceServer> convertIceServers(LinkedList<StringeeIceServer> stringeeIceServers) {
        ArrayList<IceServer> iceServers = new ArrayList<>();
        if (stringeeIceServers != null) {
            for (StringeeIceServer stringeeServer : stringeeIceServers) {
                iceServers.add(new IceServer(stringeeServer.username, stringeeServer.password, stringeeServer.uri));
            }
        }
        return iceServers;
    }

    @Override
    public void onSuccess(String result) {
        Log.f(TAG, TAG_LOG + " result ---> " + result);
    }

    @Override
    public void onError(String err) {
        Log.f(TAG, TAG_LOG + " err ---> " + err);
    }

    @Override
    public void onInfo(String info) {
        Log.f(TAG, TAG_LOG + " info ---> " + info);
    }

    @Override
    public void onConnectivityChanged(boolean isNetworkAvailable, int connectedType) {
        /*if (isNetworkAvailable) {
            restartICETimerOnFail(5000);
        }*/
        //Log.i("MochaCallActivity", "onConnectivityChanged: " + isNetworkAvailable);

        if (currentCall != null) {
            currentCall.addSignalingStatistics(new SignalingStatisticElement("onConnectivityChanged", String.format("connectedType: %s, isNetworkAvailable: %b", connectedType, isNetworkAvailable)));
        }

        lastTimeNetworkChange = System.currentTimeMillis();
    }


    public void removeCallData(CallData callData, boolean isCallOut) {
        Log.i(TAG, "removeCallData: " + callData.getData());
        try {
            JSONArray jsa = new JSONArray(callData.getData());

            StringeeIceCandidate[] stringeeIceCandidates = parseICECandidateArray(jsa);

            if (currentCall != null) {
                processRemoveCandidate(stringeeIceCandidates, isCallOut);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    private StringeeIceCandidate[] parseICECandidateArray(JSONArray jsa) {
        int length = jsa.length();
        StringeeIceCandidate[] stringeeIceCandidates = new StringeeIceCandidate[length];
        for (int i = 0; i < length; i++) {
            StringeeIceCandidate stringeeIceCandidate = StringeeIceCandidate.parseFromCallData(jsa.optJSONObject(i));
            stringeeIceCandidates[i] = stringeeIceCandidate;

        }
        return stringeeIceCandidates;
    }

    private void processRemoveCandidate(StringeeIceCandidate[] stringeeIceCandidates, boolean isCallOut) throws JSONException {
        Log.i(TAG, "+++++ processRemoveCandidate");
        currentCall.removeIceCandidate(stringeeIceCandidates, isCallOut);
    }

    private void processRemoveCandidateQueue(StringeeIceCandidate[] stringeeIceCandidates) {
//        ArrayList<Integer> listRemove = new ArrayList<>();
        Iterator<CallData> pItr = callDataQueue.iterator();
        while (pItr.hasNext()) {
            CallData callData = pItr.next();
            if (callData.isCandidate()) {
                try {
                    StringeeIceCandidate sic = StringeeIceCandidate.parseFromCallData(new JSONObject(callData.getData()));
                    for (int j = 0; j < stringeeIceCandidates.length; j++) {
                        if (StringeeIceCandidate.compare(sic, stringeeIceCandidates[j])) {
                            callDataQueue.remove(callData);
                            Log.i(TAG, "remove candidate");
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void removeCallDataToQueue(CallData callData) {
        Log.i(TAG, "removeCallDataToQueue: " + callData.getData());
        try {
            JSONArray jsa = new JSONArray(callData.getData());
            StringeeIceCandidate[] stringeeIceCandidates = parseICECandidateArray(jsa);
            if (!callDataQueue.isEmpty())
                processRemoveCandidateQueue(stringeeIceCandidates);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public String getLogCall() {
        try {
            if (StringeeCall.ENABLE_UPLOAD_CALL_REPORT
                    && currentCall != null
                    && currentCall.getWebrtcInternalDumpObj() != null
                    && !TextUtils.isEmpty(currentCall.getWebrtcInternalDumpObj().toString())) {
                return currentCall.getWebrtcInternalDumpObj().toString();
            }
        } catch (Exception ex) {

        }
        return null;
    }

    public void onActivityResume() {
        if (currentCall != null) {
            currentCall.onActivityResume();
        }
    }
    
}

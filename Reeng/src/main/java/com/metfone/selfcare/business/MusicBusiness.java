package com.metfone.selfcare.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.datasource.MediaDataSource;
import com.metfone.selfcare.database.datasource.TopSongDataSource;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.StrangerSticky;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.SoloShareMusicMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.HTTP.STRANGER_MUSIC;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.StrangerFilterHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.message.CountDownInviteManager;
import com.metfone.selfcare.helper.message.MessageConstants;
import com.metfone.selfcare.helper.message.PacketMessageId;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.module.keeng.widget.floatingView.MusicFloatingView;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.jivesoftware.smack.packet.ReengMusicPacket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import androidx.fragment.app.FragmentManager;

/**
 * Created by thaodv on 28-Nov-14.
 */
public class MusicBusiness extends BaseApi implements XMPPConnectivityChangeListener {
    private static final String TAG = MusicBusiness.class.getSimpleName();
    private static final String TAG_MUSIC_STRANGER_LIST = "TAG_MUSIC_STRANGER_LIST";
    private static final String TAG_CONFIDE_STRANGER_LIST = "TAG_CONFIDE_STRANGER_LIST";
    private static final String TAG_MUSIC_STRANGER_CHECK_NEWS = "TAG_MUSIC_STRANGER_CHECK_NEWS";
    private static final String TAG_CONFIDE_STRANGER_CHECK_NEWS = "TAG_CONFIDE_STRANGER_CHECK_NEWS";

    public static final String TAG_GET_LIST_WATCH_VIDEO = "TAG_GET_LIST_WATCH_VIDEO";
    private static final String TAG_GET_DETAIL_WATCH_VIDEO = "TAG_GET_DETAIL_WATCH_VIDEO";
    public static final String TAG_SEARCH_VIDEO = "TAG_SEARCH_VIDEO";
    private static ArrayList<OnActionMusicResponseListener> mActionMusicResponseListeners = new ArrayList<>();
    private static ArrayList<OnLoadDataResponseListener> mLoadDataResponseListeners = new ArrayList<>();
    private ApplicationController mApplication;
    private Resources mRes;
    private StrangerBusiness mStrangerBusiness;
    private ReengAccount account;
    //service nghe nhac
    private int currentPosterGroupId = -1;// id group khi post loi moi cung nghe nguoi la
    private String currentMusicSessionId; // session Cung nghe
    private String currentNumberFriend; // so dien thoai cua ban be dang nghe chung
    private String currentGroupId;// group id dang play nhac
    private MediaModel currentSong; // obj cua bai hat dang nghe chung
    private boolean isOnRoom = false;
    private long currentTimeStrangerMusic = -1;
    // top song
    private TopSongDataSource mTopSongDataSource;
    // media db
    private MediaDataSource mMediaDataSource;
    private ConcurrentHashMap<String, MediaModel> mCurrentHashMapMedia;
    // timer message
    private ConcurrentHashMap<String, ReengMusicPacket> mWaitingResponseMusicAction;
    private CountDownTimer timerReceiveResponse;
    private SharedPreferences mPref;

    private ArrayList<StrangerMusic> strangerMusics = new ArrayList<>();
    private ArrayList<StrangerMusic> newStrangerMusics = new ArrayList<>();
    private ArrayList<StrangerSticky> stickyStrangerMusics = new ArrayList<>();
    private ArrayList<StrangerSticky> newStickyStrangerMusics = new ArrayList<>();
    private String strangerMusicBannerUrl;
    private int mGroupFiler = -1;
    // tam su ng la
    private ArrayList<StrangerMusic> strangerConfides = new ArrayList<>();
    private ArrayList<StrangerMusic> newStrangerConfides = new ArrayList<>();
    private boolean isSelectedConfide = false;
    private String currentConfideSession = null;
    // cung nghe voi sao
    private String currentRoomId;
    private String currentRoomSessionId;
    private boolean isPlayList = false;
    private ArrayList<MediaModel> currentPlayList;
    private String descSongRoom;
    private String currentFriendNameAccepted;
    //
    private boolean listenAlbum = false;
    private ArrayList<MediaModel> listTopVideoDefault = new ArrayList<>();
    private boolean isPlayFromKeengMusic = false;

    public MusicBusiness(ApplicationController app) {
        super(app);
        this.mApplication = app;
        this.mRes = mApplication.getResources();
        long time = System.currentTimeMillis();
        this.mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        this.currentConfideSession = null;
        stopPingPong();
        Log.d(TAG, "[] pref take: " + (System.currentTimeMillis() - time));
    }

    public void init() {
        this.mTopSongDataSource = TopSongDataSource.getInstance(mApplication);
        this.mMediaDataSource = MediaDataSource.getInstance(mApplication);
        //startBindMediaService();
        XMPPManager.addXMPPConnectivityChangeListener(this);
        this.mWaitingResponseMusicAction = new ConcurrentHashMap<>();
    }

    public void setCurrentTimeStrangerMusic(long currentTimeStrangerMusic) {
        this.currentTimeStrangerMusic = currentTimeStrangerMusic;
    }

    public String getCurrentMusicSessionId() {
        return currentMusicSessionId;
    }

    public void setCurrentMusicSessionId(String currentMusicSessionId) {
        this.currentMusicSessionId = currentMusicSessionId;
    }

    public String getCurrentNumberFriend() {
        return currentNumberFriend;
    }

    public void setCurrentNumberFriend(String currentNumberFriend) {
        this.currentNumberFriend = currentNumberFriend;
    }

    public void setCurrentSong(MediaModel currentSong) {
        this.currentSong = currentSong;
    }

    public boolean isOnRoom() {
        return isOnRoom;
    }

    public String getStrangerMusicBannerUrl() {
        return strangerMusicBannerUrl;
    }

    public boolean isSelectedConfide() {
        return isSelectedConfide;
    }

    public void setSelectedConfide(boolean selectedConfide) {
        this.isSelectedConfide = selectedConfide;
    }

    public boolean isExistRoomConfide() {
        return !TextUtils.isEmpty(currentConfideSession);
    }

    public String getCurrentConfideSession() {
        return currentConfideSession;
    }

    public void resetRoomConfide() {
        this.currentConfideSession = null;
    }

    public void addMusicPacketToWaitingResponse(ReengMusicPacket musicPacket) {
        if (mWaitingResponseMusicAction == null) {
            mWaitingResponseMusicAction = new ConcurrentHashMap<>();
        }
        mWaitingResponseMusicAction.put(musicPacket.getPacketID(), musicPacket);
    }

    public ReengMusicPacket getMusicPacketFromListWaitingResponse(String packetId) {
        if (mWaitingResponseMusicAction == null || mWaitingResponseMusicAction.isEmpty()) {
            return null;
        }
        ReengMusicPacket musicPacket = mWaitingResponseMusicAction.get(packetId);
        if (musicPacket != null) {
            mWaitingResponseMusicAction.remove(packetId);
        }
        return musicPacket;
    }

    public void checkResetConfideSessionAndNotifyRefresh(String sessionId) {
        if (currentConfideSession != null && currentConfideSession.equals(sessionId)) {
            resetRoomConfide();
            notifyReloadStrangerData();
        }
    }

    /**
     * action music (change song, add song ....)
     *
     * @param listener
     */
    public static void addActionMusicResponseListener(OnActionMusicResponseListener listener) {
        if (!mActionMusicResponseListeners.contains(listener)) {
            mActionMusicResponseListeners.add(listener);
        }
    }

    public static void removeActionMusicResponseListener(OnActionMusicResponseListener listener) {
        if (mActionMusicResponseListeners.contains(listener)) {
            mActionMusicResponseListeners.remove(listener);
        }
    }

    private static void notifyActionMusicResponse(ReengMusicPacket packet) {
        if (mActionMusicResponseListeners != null && !mActionMusicResponseListeners.isEmpty()) {
            for (OnActionMusicResponseListener listener : mActionMusicResponseListeners) {
                listener.onActionResponse(packet);
            }
        }
    }

    private static void notifyActionMusicTimeout(ReengMusicPacket packet) {
        if (mActionMusicResponseListeners != null && !mActionMusicResponseListeners.isEmpty()) {
            for (OnActionMusicResponseListener listener : mActionMusicResponseListeners) {
                listener.onTimeOutRequest(packet);
            }
        }
    }

    /**
     * load stranger music callback
     *
     * @return
     */
    public static void addLoadDataResponseListener(OnLoadDataResponseListener listener) {
        if (!mLoadDataResponseListeners.contains(listener)) {
            mLoadDataResponseListeners.add(listener);
        }
    }

    public static void removeLoadDataResponseListener(OnLoadDataResponseListener listener) {
        if (mLoadDataResponseListeners.contains(listener)) {
            mLoadDataResponseListeners.remove(listener);
        }
    }

    private static void notifyStarting() {
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                listener.onStarting();
            }
        }
    }

    private static void notifyRefreshMusicDataResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerSticky> stickyStrangerMusics) {
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                listener.onRefreshDataMusicResponse(strangerMusics, stickyStrangerMusics);
            }
        }
    }

    private static void notifyRefreshConfideDataResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerSticky> stickyStrangerMusics) {
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                listener.onRefreshDataConfideResponse(strangerMusics, stickyStrangerMusics);
            }
        }
    }

    private void notifyFilterError(int errorCode) {
        strangerMusics = new ArrayList<>();
        stickyStrangerMusics = new ArrayList<>();
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                listener.onFilterDataError(errorCode);
            }
        }
    }

    private static void notifyNewRoomResponse(boolean isNew) {
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                listener.onNotifyNewRoomResponse(isNew);
            }
        }
    }

    private static void notifyRefreshData() {
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                listener.onRefreshDataWhenReceiverPresence();
            }
        }
    }

    private static void notifyLoadMoreDataResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerMusic>
            dataLoadMore, boolean isConfide) {
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            if (isConfide) {
                for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                    listener.onLoadMoreConfideResponse(strangerMusics, dataLoadMore);
                    Log.d(TAG, "notify display load More: " + TimeHelper.getCurrentTime());
                }
            } else {
                for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                    listener.onLoadMoreMusicResponse(strangerMusics, dataLoadMore);
                    Log.d(TAG, "notify display load More: " + TimeHelper.getCurrentTime());
                }
            }
        }
    }

    public static void notifyReloadStrangerData() {
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                listener.onReloadDataWhenEventMessage();
            }
        }
    }

    private static void notifyResponseError(int errorCode) {
        if (mLoadDataResponseListeners != null && !mLoadDataResponseListeners.isEmpty()) {
            for (OnLoadDataResponseListener listener : mLoadDataResponseListeners) {
                listener.onError(errorCode);
            }
        }
    }

    public boolean isListenAlbum() {
        return listenAlbum;
    }

    public void setListenAlbum(boolean listenAlbum) {
        this.listenAlbum = listenAlbum;
    }

    public String getCurrentGroupId() {
        return currentGroupId;
    }

    public void setCurrentGroupId(String currentGroupId) {
        this.currentGroupId = currentGroupId;
    }

    ////////***********************************///////
    public boolean isShowPlayer(String friendNumber) {
        if (currentNumberFriend != null && currentNumberFriend.equals(friendNumber)) {
            if (isOnRoom) {
                return true;
            }
        }
        return false;
    }

    public boolean isShowPlayerGroup(String groupId) {
        if (currentGroupId != null && currentGroupId.equals(groupId)) {
            if (isOnRoom) {
                return true;
            }
        }
        return false;
    }

    public boolean isShowPlayerRoom(String roomId) {
        if (currentRoomId != null && currentRoomId.equals(roomId)) {
            if (isOnRoom) {
                return true;
            }
        }
        return false;
    }

    public boolean isExistListenMusic() {
        if (mApplication.getPlayMusicController().isPlayFromFeed()) {
            return true;
        } else if (isPlayFromKeengMusic()) {
            return true;
        } else if (isExistListenerRoom()) {//dang danh dau cung nghe star
            return true;
        } else if (isExistListenerGroup()) {// dang danh dau cung nghe group
            return true;
        } else if (TextUtils.isEmpty(currentMusicSessionId)) {
            return false;
        } else if (TimeHelper.checkTimeOutStrangerMusic(currentTimeStrangerMusic)) {
            currentTimeStrangerMusic = -1;//reset session stranger music khi qua timeout
            currentMusicSessionId = null;
            return false;
        } else {
            return true;
        }
    }

    public boolean isExistListenerSolo() {
        if (!TextUtils.isEmpty(currentMusicSessionId)) {
            return !TextUtils.isEmpty(currentNumberFriend);
        }
        return false;
    }

    public boolean isExistListenerGroup() {
        return !TextUtils.isEmpty(currentGroupId);
    }

    public boolean isExistListenerRoom() {
        return !TextUtils.isEmpty(currentRoomId);
    }

    /**
     * kiem tra xem co dang danh dau cung nghe voi nguoi la khong
     * neu dang cung nghe (isExistListenMusic==true) va currentTimeStrangerMusic>0
     * (dang co loi moi cung nghe voi nguoi la).
     *
     * @return
     */
    public boolean isWaitingStrangerMusic() {
        return currentTimeStrangerMusic > 0;
    }

    public void resetSessionMusicAndKeepPlayer() {
        setCurrentMusicSessionId(null);
        currentTimeStrangerMusic = -1;
        currentSong = null;
    }

    /**
     * reset session music
     */
    public void resetSessionMusic() {
        Log.d(TAG, "resetSessionMusic");
        setCurrentMusicSessionId(null);
        setCurrentNumberFriend(null);
        setCurrentGroupId(null);
        setCurrentRoomId(null);
        setCurrentRoomSessionId(null);
        currentTimeStrangerMusic = -1;
        currentSong = null;
        isOnRoom = false;
    }

    public void resetSessionStranger() {
        setCurrentMusicSessionId(null);
        currentTimeStrangerMusic = -1;
    }

    public void closeMusic() {
        Log.i(TAG, "onCloseMusic click quit in notify");
        listenAlbum = false;
        onLeaveMusic(true);
        mApplication.getPlayMusicController().setStatePauseChangeSong(false);
        resetSessionMusic();

        //Tat floating view music
        setPlayFromKeengMusic(false);
        MusicFloatingView.stop(mApplication);
    }

    /**
     * tao session music khi nhan duoc receive cua ban in tinvite
     * hoac click dong y cung nghe
     *
     * @param songModel
     * @param sessionId
     * @param friendNumber
     */
    public void onCreateSessionMusic(String sessionId, String friendNumber, MediaModel songModel) {
        currentMusicSessionId = sessionId;
        currentNumberFriend = friendNumber;
        currentSong = songModel;
        currentTimeStrangerMusic = -1;
        currentGroupId = null;
        currentRoomId = null;
        currentRoomSessionId = null;
        isOnRoom = false;// chua play(luc nao goi ham play nhac thi set sau)
        listenAlbum = false;
        /*// cap nhat thoi gian lan cuoi cung nghe
        ThreadMessage currentThread = mApplication.getMessageBusiness().findExistingSoloThread(friendNumber);
        if (currentThread != null) {
            currentThread.setLastTimeShareMusic(TimeHelper.getCurrentTime());
            mApplication.getMessageBusiness().updateThreadMessage(currentThread);
        }*/
    }

    /**
     * play bai hat khi nguoi dung dong y, hoac nhan dc ban tin music ping
     *
     * @param sessionId
     */
    public void onStartMusic(String sessionId) {
        // dung session va chua play
        if (sessionId != null && sessionId.equals(currentMusicSessionId) && !isOnRoom) {
            isOnRoom = true;
            currentTimeStrangerMusic = -1;
            currentGroupId = null;
            startPlayingMusic();
            mApplication.getPlayMusicController().setStatePauseChangeSong(false);
        }
    }

    public void startGroupMusic(String groupId, ReengMessage message) {
        listenAlbum = false;
        setCurrentGroupId(groupId);
        setCurrentSong(message.getSongModel(this));
        isOnRoom = true;
        startPlayingMusic();
        currentTimeStrangerMusic = -1;
        currentMusicSessionId = null;
        currentNumberFriend = null;
        currentRoomId = null;
        currentRoomSessionId = null;
        mApplication.getPlayMusicController().setStatePauseChangeSong(false);
    }

    public void onAcceptAndPlayMusic(String sessionId, String friendNumber, MediaModel songModel) {
        onCreateSessionMusic(sessionId, friendNumber, songModel);
        isOnRoom = true;
        startPlayingMusic();
        currentTimeStrangerMusic = -1;
        mApplication.getPlayMusicController().setStatePauseChangeSong(false);
        startTimerPingMusic();
    }

    /**
     * gui thanh cong xoa message cu voi truong hop click vao moi lai
     *
     * @param mParentActivity
     * @param songModel
     * @param oldMessage
     */
    public boolean sendAndWaitConfirmChangeSong(BaseSlidingFragmentActivity mParentActivity,
                                                MediaModel songModel, ReengMessage oldMessage) {
        // kiem tra xem co ket noi khong
        XMPPManager xmppManager = mApplication.getXmppManager();
        String currentNumberInRoom = getCurrentNumberFriend();
        if (TextUtils.isEmpty(currentNumberInRoom)) {
            mParentActivity.showToast(R.string.music_room_no_longer_exist);
            return false;
        } else {
            if (NetworkHelper.isConnectInternet(mParentActivity) && xmppManager.isConnected()) {
                sendActionChangeSong(songModel);
                return true;
            } else {
                mParentActivity.showToast(R.string.no_connectivity_not_feature);
                return false;
            }
        }
    }

    public void sendActionChangeSong(MediaModel songModel) {
        ReengMusicPacket musicActionPacket = new ReengMusicPacket();
        musicActionPacket.setNoStore(true);
        musicActionPacket.setType(ReengMessagePacket.Type.chat);
        musicActionPacket.setSubType(ReengMessagePacket.SubType.music_action);
        musicActionPacket.setPacketID(PacketMessageId.getInstance().
                genPacketId(musicActionPacket.getType().toString(), musicActionPacket.getSubType().toString()));
        //musicActionPacket.setTo(currentNumberFriend + "@" + Constants.XMPP.XMPP_DOMAIN);
        musicActionPacket.setTo(currentNumberFriend + Constants.XMPP.XMPP_RESOUCE);
        musicActionPacket.setSessionId(currentMusicSessionId);
        musicActionPacket.setMusicAction(ReengMusicPacket.MusicAction.change);
        // thong tin bai hat
        if (songModel != null) {
            musicActionPacket.setSongId(songModel.getId());
            musicActionPacket.setSongName(songModel.getName());
            musicActionPacket.setSinger(songModel.getSinger());
            musicActionPacket.setSongUrl(songModel.getUrl());
            musicActionPacket.setMediaUrl(songModel.getMedia_url());
            musicActionPacket.setSongThumb(songModel.getImage());
            musicActionPacket.setCrbtCode(songModel.getCrbtCode());
            musicActionPacket.setCrbtPrice(songModel.getCrbtPrice());
        }
        // add waiting response
        addMusicPacketToWaitingResponse(musicActionPacket);
        // bat timer 5s check response.
        startTimerReceiveResponse(ReengMusicPacket.MusicAction.change,
                musicActionPacket.getPacketID(), Constants.ALARM_MANAGER.TIME_OUT_RESPONSE_ACTION_MUSIC);
        // gui ban tin
        mApplication.getXmppManager().sendXmppPacket(musicActionPacket);
    }

    /**
     * xu ly khi nhan duoc ban tin leave
     *
     * @param message
     */
    public void onReceivedLeaveMusic(ReengMessage message) {
        stopPingPong();
        // để play đến hết bài
        resetSessionMusicAndKeepPlayer();
        //resetSessionMusic();
    }

    /**
     * xu ly khi nhan duoc ban tin change song
     *
     * @param receivePacket
     * @param from
     */
    public void onReceivedChangeSong(ReengMusicPacket receivePacket, String from, MediaModel songModel) {
        // gui ban tin bao da nhan action chuyen bai
        if (receivePacket.getType() == ReengMessagePacket.Type.chat) {
            mApplication.getXmppManager().sendConfirmMusicActionPacket(from, receivePacket.getMusicAction(),
                    receivePacket.getPacketID(), currentMusicSessionId);
        }
        if (mApplication.getPlayMusicController().isPaused()) {
            mApplication.getPlayMusicController().setStatePauseChangeSong(true);
        } else {
            mApplication.getPlayMusicController().setStatePauseChangeSong(false);
        }
        changeSong(songModel);
    }

    /**
     * xu ly khi nhan duoc ban tin response music action
     *
     * @param receivePacket
     * @param from
     */
    public void onReceivedResponseAction(ReengMusicPacket receivePacket,
                                         String from, ThreadMessage threadMessage) {
        // tat loading va insert message chuyen bai
        if (from != null && from.equals(currentNumberFriend)) {// check so nguoi gui, check session tu incoming
            // process roi
            ReengMusicPacket waitingPacket = getMusicPacketFromListWaitingResponse(receivePacket.getResponsePacketId());
            if (waitingPacket != null) {
                notifyActionMusicResponse(waitingPacket);
                stopTimerReceiveResponse();
                //notify and draw message
                Log.i(TAG, "message action response " + waitingPacket.toString());
            }
        }
    }

    /**
     * show dialog action music timout
     *
     * @param musicPacket packet khong gui dc
     */
    public void showDialogTimeoutChangeSong(ReengMusicPacket musicPacket,
                                            BaseSlidingFragmentActivity activity,
                                            FragmentManager fragmentManager) {
        if (!isExistListenMusic() || !isOnRoom()) {// khong con cung nghe nua
            return;
        }
        Resources res = activity.getResources();
        String labelExit = res.getString(R.string.close);
        String labelRetry = res.getString(R.string.retry);
        String title = res.getString(R.string.note_title);
        String songName = musicPacket.getSongName();
        if (songName == null) {
            songName = "";
        }
        String msg = String.format(res.getString(R.string.timeout_change_song), songName);
        ClickListener.IconListener listener = new ClickListener.IconListener() {
            @Override
            public void onIconClickListener(View view, Object entry, int menuId) {
                Log.d(TAG, "showDialogTimeoutChangeSong click: " + menuId);
                if (!isExistListenMusic() || !isOnRoom()) {// khong con cung nghe nua
                    return;
                }
                ReengMusicPacket musicActionPacket = (ReengMusicPacket) entry;
                if (musicActionPacket != null && menuId == Constants.MENU.ACCEPT_RETRY_CHANGE_SONG) {
                    // add waiting response
                    addMusicPacketToWaitingResponse(musicActionPacket);
                    // bat timer 5s check response.
                    startTimerReceiveResponse(ReengMusicPacket.MusicAction.change,
                            musicActionPacket.getPacketID(), Constants.ALARM_MANAGER.TIME_OUT_RESPONSE_ACTION_MUSIC);
                    // gui ban tin
                    mApplication.getXmppManager().sendXmppPacket(musicActionPacket);
                }
            }
        };
        PopupHelper.getInstance().showDialogConfirm(activity, title,
                msg, labelRetry, labelExit, listener, musicPacket,
                Constants.MENU.ACCEPT_RETRY_CHANGE_SONG);
    }

    /**
     * huy cung nghe
     *
     * @param insertNotifyMessage
     */
    public void onLeaveMusic(boolean insertNotifyMessage) {
        // cap nhat last thread send invite
        updatePrefSendInviteMusic(-1);
        // dang ko trong cung nghe
        if (TextUtils.isEmpty(currentMusicSessionId) || TextUtils.isEmpty(currentNumberFriend)) {
            stopPingPong();
            resetSessionMusic();
            // close music
            mApplication.getPlayMusicController().closeNotifyMusicAndClearDataSong();
        } else {
            boolean isUpdateLastInvite = false;
            MessageBusiness messageBusiness = mApplication.getMessageBusiness();
            // loi moi chua dc dong y, tim va cap nhat trang thai
            ThreadMessage threadMessage = messageBusiness.findExistingSoloThread(currentNumberFriend);
            if (!isOnRoom && !TextUtils.isEmpty(currentNumberFriend)) {
                Resources res = mApplication.getResources();
                if (threadMessage != null) {
                    ReengMessage lastMessageInvite = messageBusiness.findLastMessageSendInviteMusic(threadMessage);
                    if (lastMessageInvite != null && lastMessageInvite.getMusicState() == ReengMessageConstant
                            .MUSIC_STATE_WAITING) {
                        lastMessageInvite.setDuration(0);
                        CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(lastMessageInvite);
                        lastMessageInvite.setMusicState(ReengMessageConstant.MUSIC_STATE_CANCEL);
                        lastMessageInvite.setContent(String.format(res.getString(R.string.invite_share_music_canceled),
                                res.getString(R.string.you)));
                        lastMessageInvite.setFileName(String.format(res.getString(R.string.invite_share_music_canceled),
                                TextHelper.textBoldWithHTML(res.getString(R.string.you))));
                        messageBusiness.refreshThreadWithoutNewMessage(lastMessageInvite.getThreadId());
                        // cap nhat db
                        messageBusiness.updateAllFieldsOfMessage(lastMessageInvite);
                        isUpdateLastInvite = true;
                    } else {
                        isUpdateLastInvite = false;
                    }
                } else {
                    isUpdateLastInvite = false;
                }
            }
            //ReengMessage message;
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (!isUpdateLastInvite && insertNotifyMessage) {
                insertMessageLeaveMusicBeforeSend(messageBusiness, myNumber);
            }
            //stop ping pong va gui ban tin leave
            stopPingPong();
            mApplication.getXmppManager().sendLeaveMusic(currentNumberFriend,
                    currentMusicSessionId, threadMessage);
            // nguoi dung chu dong thoat thi reset session, tat player
            resetSessionMusic();
            mApplication.getPlayMusicController().closeMusic();
        }
    }

    /**
     * huy cung nghe khi timeout ping pong
     */
    /**
     * huy cung nghe khi timeout ping pong
     */
    public void onTimeOutPingPong() {
        // dang ko trong cung nghe
        if (TextUtils.isEmpty(currentMusicSessionId) || TextUtils.isEmpty(currentNumberFriend)) {
            return;
        }
        MessageBusiness mMessageBusiness = mApplication.getMessageBusiness();
        ThreadMessage mThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(currentNumberFriend);
        if (!mThreadMessage.isStranger()) {// thread thuong thi van notify, nguoi la ko bao mat ket noi
            String content = String.format(mApplication.getResources().getString(R.string.unavaiable_music_room),
                    mMessageBusiness.getFriendName(currentNumberFriend));
            insertMessageNotifyMusic(mMessageBusiness, mThreadMessage, content);
        }
        //stop ping pong va gui ban tin leave
        stopPingPong();
        // play den het bai
        resetSessionMusicAndKeepPlayer();
        //resetSessionMusic();
    }

    /**
     * xy ly khi khong load duoc nhac tu keeng
     */
    public void onMediaPlayerTimeOut() {
        if (isExistListenerGroup() || isExistListenerRoom()) {
            resetSessionMusic();
            // close media
            mApplication.getPlayMusicController().closeMusic();
        } else {
            if (!TextUtils.isEmpty(currentMusicSessionId) && !TextUtils.isEmpty(currentNumberFriend)) {
                MessageBusiness mMessageBusiness = mApplication.getMessageBusiness();
                String content = mApplication.getResources().getString(R.string.play_music_time_out);
                ThreadMessage mThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(currentNumberFriend);
                insertMessageNotifyMusic(mMessageBusiness, mThreadMessage, content);
            }
            // stop ping pong va gui ban tin leave
            stopPingPong();
            // nguoi dung chu dong thoat thi reset session, tat player
            resetSessionMusic();
            // close media
            mApplication.getPlayMusicController().closeMusic();
        }
    }

    /**
     * tao message thong bao thoat cung nghe (nguoi dung chu dong thoat)
     *
     * @param mMessageBusiness
     */
    private ReengMessage insertMessageLeaveMusicBeforeSend(MessageBusiness mMessageBusiness, String myNumber) {
        Resources res = mApplication.getResources();
        ThreadMessage mThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(currentNumberFriend);
        ReengMessage messageNotify = new ReengMessage();
        messageNotify.setPacketId(PacketMessageId.getInstance().genMessagePacketIdDefault());
        messageNotify.setSender(myNumber);
        messageNotify.setReceiver(currentNumberFriend);
        messageNotify.setThreadId(mThreadMessage.getId());
        messageNotify.setReadState(ReengMessageConstant.READ_STATE_READ);
        messageNotify.setChatMode(ReengMessageConstant.MODE_IP_IP);
        messageNotify.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
        messageNotify.setDirection(ReengMessageConstant.Direction.send);
        messageNotify.setSize(1);
        Date date = new Date();
        messageNotify.setTime(date.getTime());
        messageNotify.setStatus(ReengMessageConstant.STATUS_RECEIVED);
        messageNotify.setMessageType(ReengMessageConstant.MessageType.notification);
        messageNotify.setFileName(MessageConstants.NOTIFY_TYPE.leaveMusic.name());
        messageNotify.setSongId(ReengMessage.SONG_ID_DEFAULT_NEW);
        messageNotify.setSongModel(currentSong);
        messageNotify.setContent(res.getString(R.string.left_music_room_you));
        mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage,
                ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT, messageNotify);
        return messageNotify;
    }

    /**
     * tao message thong bao thoat cung nghe (time out)
     *
     * @param mMessageBusiness
     */
    private void insertMessageNotifyMusic(MessageBusiness mMessageBusiness, ThreadMessage mThreadMessage, String
            content) {
        String myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloShareMusicMessage reengMessage = new SoloShareMusicMessage(mThreadMessage,
                currentNumberFriend, myJidNumber);
        // msg notify ko gui ko can chuan packetid
        reengMessage.setPacketId(PacketMessageId.getInstance().genShareMusicPacketId(mThreadMessage.getThreadType(),
                null));
        reengMessage.setStatus(ReengMessageConstant.STATUS_RECEIVED);
        reengMessage.setMessageType(ReengMessageConstant.MessageType.notification);
        reengMessage.setSongId(ReengMessage.SONG_ID_DEFAULT_NEW);
        reengMessage.setSongModel(currentSong);
        reengMessage.setContent(content);
        // notify
        mMessageBusiness.notifyReengMessage(mApplication, mThreadMessage,
                reengMessage, mThreadMessage.getThreadType());
    }

    /**
     * bat dau cung nghe (player)
     */
    public void startPlayingMusic() {
        if (currentSong != null && mApplication.getPlayMusicController() != null) {
            mApplication.getPlayMusicController().getPlayingListDetail(currentSong);
            mApplication.getPlayMusicController().setIsPlayFromFeed(false);
            // log feed
            mApplication.getKeengProfileBusiness().logListenTogether(currentSong);
        }
    }

    public void changeSong(MediaModel songModel) {
        currentSong = songModel;
        startPlayingMusic();
    }

    @Override
    public void onXMPPConnected() {
        //        resetRoom(); //truong hop build lai
    }

    @Override
    public void onXMPPDisconnected() {
        Log.d(TAG, "onXMPPDisconnected reset room");
        /*resetSessionMusic();
        if (mMediaService != null) mMediaService.pauseMusic();*/
    }

    @Override
    public void onXMPPConnecting() {

    }

    public ArrayList<MediaModel> getListTopSongFromDb() {
        if (mTopSongDataSource == null) {
            return new ArrayList<>();
        }
        return mTopSongDataSource.getListTopSong();
    }

    public void deleteTopSongFromDb() {
        mTopSongDataSource.deleteAllTable();
    }

    public void insertListTopSongToDb(ArrayList<MediaModel> list) {
        mTopSongDataSource.insertListTopSong(list);
    }

    /**
     * reset cung nghe khi deactive
     */
    public void clearSessionAndNotifyMusic() {
        Log.d(TAG, "reset session");
        resetSessionMusic();
        mApplication.getPlayMusicController().closeMusic();
    }

    public boolean isDataReady() {
        return mCurrentHashMapMedia != null;
    }

    /**
     * xu ly media model trong db
     */
    public synchronized void initMedia() {
        if (mCurrentHashMapMedia == null) {
            mCurrentHashMapMedia = new ConcurrentHashMap<>();
        }
    }

    /**
     * get media model by song id
     *
     * @param songId
     * @return mediaModel
     */
    public MediaModel getMediaModelBySongId(String songId) {
        initMedia();
        MediaModel model = mCurrentHashMapMedia.get(songId);
        if (model != null) {
            return model;
        }
        // ko co mem, load db
        MediaModel dbModel = mMediaDataSource.getMediaFromSongId(songId);
        if (dbModel != null) {
            mCurrentHashMapMedia.put(songId, dbModel);// put vao mem lan sau lay cho nhanh
            return dbModel;
        }
        return new MediaModel(songId);
    }

    /**
     * compare two mediaModel
     *
     * @param newModel
     * @param oldModel
     * @return
     */
    private boolean compareMediaModel(MediaModel newModel, MediaModel oldModel) {
        String oldName = oldModel.getName();
        String oldSinger = oldModel.getSinger();
        String oldMedia = oldModel.getMedia_url();
        String oldThumb = oldModel.getImage();
        String oldCrbtCode = oldModel.getCrbtCode();
        String oldCrbtPrice = oldModel.getCrbtPrice();
        //
        String newName = newModel.getName();
        String newSinger = newModel.getSinger();
        String newMedia = newModel.getMedia_url();
        String newThumb = newModel.getImage();
        String newCrbtCode = newModel.getCrbtCode();
        String newCrbtPrice = newModel.getCrbtPrice();
        if (newName != null && newName.equals(oldName)) {
            if (newSinger != null && newSinger.equals(oldSinger)) {
                if (newMedia != null && newMedia.equals(oldMedia)) {
                    if (newThumb != null && newThumb.equals(oldThumb)) {
                        if (newCrbtCode != null && newCrbtCode.equals(oldCrbtCode)) {
                            if (newCrbtPrice != null && newCrbtPrice.equals(oldCrbtPrice)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * ping, pong music
     */
    private int musicPingFailCount;
    private int musicPongFailCount;

    /**
     * den chu ky ping, gui ban tin ping, tang bien count fail ping
     */
    private void onReceiveMusicPingAlarm() {
        Log.d(TAG, "onReceivePingAlarm: count fail: > " + musicPingFailCount);
        if (musicPingFailCount > 4) { // time out
            onTimeOutPingPong();
        } else {
            mApplication.getXmppManager().sendMusicPingPongPacket(currentNumberFriend,
                    currentMusicSessionId, null, ReengMessagePacket.SubType.music_ping);
            musicPingFailCount++;
        }
    }

    /**
     * nhan dc pong thi reset bien count ping fail
     */
    public void onReceivePongPacket(String to, String sessionId) {
        musicPingFailCount = 0;
    }

    public void onReceivePongBusyPacket(String sessionId, ThreadMessage currentThread) {
       /* mApplication.trackingEvent(R.string.ga_category_invite_music,
                R.string.ga_action_interaction, R.string.ga_label_busy);*/
        // dang ko trong cung nghe
        if (TextUtils.isEmpty(currentMusicSessionId) || TextUtils.isEmpty(currentNumberFriend)) {
            return;
        }
        if (currentMusicSessionId.equals(sessionId)) {
            /*MessageBusiness mMessageBusiness = mApplication.getMessageBusiness();
            String content = String.format(mContext.getResources().getString(R.string.invite_share_music_not_answer),
                    mMessageBusiness.getFriendName(currentNumberFriend));
            insertMessageNotifyMusic(mMessageBusiness, currentThread, content);*/
            //stop ping pong va gui ban tin leave
            stopPingPong();
            // play den het bai
            resetSessionMusicAndKeepPlayer();
        }
    }

    /**
     * den chu ky 30 s tu dong tang count fail pong
     * khi nhan dc ping thi giam bien nay di
     */
    private void onReceiveMusicPongAlarm() {
        musicPongFailCount++;
        Log.d(TAG, "onReceivePongAlarm: count fail: > " + musicPongFailCount);
        if (musicPongFailCount > 4) {// time out
            onTimeOutPingPong();
        }
    }

    /**
     * tra lai ban tin pong khi nhan dc ping
     * reset lai count pong fail
     *
     * @param to
     * @param sessionId
     * @param status
     */
    public void onReceivePingPacket(String to, String sessionId, ReengMusicPacket.MusicStatus status) {
        mApplication.getXmppManager().sendMusicPingPongPacket(to, sessionId, status, ReengMessagePacket.SubType.music_pong);
        // truong hop dong y cung nghe, nhan dc ping tra lai pong va giam count fail
        if (status == ReengMusicPacket.MusicStatus.available) {
            musicPongFailCount = 0;
        }
    }

    public void stopPingPong() {
        Log.i(TAG, "stopPingPong");
        stopTimerPing();
        stopTimerPong();
        /*mApplication.getAlarmReceiver().cancelAlarmManager(mApplication, Constants.ALARM_MANAGER.MUSIC_PING_ID);
        mApplication.getAlarmReceiver().cancelAlarmManager(mApplication, Constants.ALARM_MANAGER.MUSIC_PONG_ID);*/
    }

    public void stopTimerReceiveResponse() {
        if (timerReceiveResponse != null) {
            timerReceiveResponse.cancel();
            timerReceiveResponse = null;
        }
    }

    public void startTimerReceiveResponse(final ReengMusicPacket.MusicAction musicAction, final String packetId, int
            timeout) {
        stopTimerReceiveResponse();
        Log.i(TAG, "startTimerReceiveResponse");
        timerReceiveResponse = new CountDownTimer(timeout, timeout) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "End timer ============================> " + packetId);
                if (musicAction == ReengMusicPacket.MusicAction.change) {// timeout ban tin chuyen bai ...
                    ReengMusicPacket waitingPacket = getMusicPacketFromListWaitingResponse(packetId);
                    if (waitingPacket != null) {
                        notifyActionMusicTimeout(waitingPacket);
                    }
                } else if (musicAction == ReengMusicPacket.MusicAction.invite) {// time out ban tin invite
                    updateMessageInviteFail(packetId);
                }
            }
        }.start();
    }

    /**
     * cap nhat khi khong nhan dc receive ban tin invite
     *
     * @param packetId
     */
    private void updateMessageInviteFail(String packetId) {
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        ReengMessage reengMessage = messageBusiness.findMessageInMemByPacketId(packetId);
        if (reengMessage == null) {
            //trong db ko con message nay nua
            return;
        }
        if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic ||
                reengMessage.getStatus() == ReengMessageConstant.STATUS_NOT_SEND) {
            ///
            CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(reengMessage);
            reengMessage.setMusicState(ReengMessageConstant.MUSIC_STATE_TIME_OUT);
            reengMessage.setContent(mApplication.getResources().getString(R.string.invite_share_music_not_send));
            reengMessage.setFileName(mApplication.getResources().getString(R.string.invite_share_music_not_send));
            reengMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
            messageBusiness.refreshThreadWithoutNewMessage(reengMessage.getThreadId());
            messageBusiness.updateAllFieldsOfMessage(reengMessage); //ham nay chiem nhieu thoi gian thuc hien nhat
        }
    }

    /**
     * huy loi moi cuoi cung neu click dong y 1 loi moi khac
     *
     * @param mCorrespondingThread
     */
    public void cancelLastMusicInviteMessage(ThreadMessage mCorrespondingThread) {
        if (mCorrespondingThread.getThreadType() != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            return;
        }
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        Resources res = mApplication.getResources();
        ReengMessage reengMessage = mApplication.getMessageBusiness().findLastMessageSendInviteMusic
                (mCorrespondingThread);
        // neu tin invite gui cuoi cung dang wait thi cap nhat thanh cancel
        if (reengMessage != null && reengMessage.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {
            updatePrefSendInviteMusic(-1);
            reengMessage.setDuration(0);
            CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(reengMessage);
            reengMessage.setMusicState(ReengMessageConstant.MUSIC_STATE_CANCEL);
            reengMessage.setContent(String.format(res.getString(R.string.invite_share_music_canceled),
                    res.getString(R.string.you)));
            reengMessage.setFileName(String.format(res.getString(R.string.invite_share_music_canceled),
                    TextHelper.textBoldWithHTML(res.getString(R.string.you))));
            messageBusiness.refreshThreadWithoutNewMessage(reengMessage.getThreadId());
            // cap nhat db
            messageBusiness.updateAllFieldsOfMessage(reengMessage);
            // gui ban tin leave
            // String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            mApplication.getXmppManager().sendLeaveMusic(currentNumberFriend, currentMusicSessionId,
                    mCorrespondingThread);
            // nguoi dung chu dong thoat thi reset session, tat player
            resetSessionMusic();
        }
    }

    /**
     * update session music khi kill app di vao lai
     */
    public void updateStateAndSessionMusicAfterRestartApp() {
        if (mPref == null) {
            mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        int lastThreadId = mPref.getInt(Constants.PREFERENCE.PREF_SEND_MUSIC_LAST_THREAD_ID, -1);
        if (lastThreadId != -1) {
            MessageBusiness messageBusiness = mApplication.getMessageBusiness();
            ThreadMessage threadMessage = messageBusiness.findThreadByThreadId(lastThreadId);
            if (threadMessage != null) {
                ReengMessage lastInviteMessage = messageBusiness.findLastMessageSendInviteMusic(threadMessage);
                if (lastInviteMessage != null &&
                        lastInviteMessage.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {
                    MediaModel songModel = lastInviteMessage.getSongModel(this);
                    long time = TimeHelper.getCurrentTime() - lastInviteMessage.getTime();
                    if (songModel == null || time > CountDownInviteManager.COUNT_DOWN_DURATION) {// qua thoi gian
                        // timeout
                        messageBusiness.updateSendInviteMessageTimeout(lastInviteMessage);
                        CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(lastInviteMessage);
                        if (!TextUtils.isEmpty(currentMusicSessionId)) {
                            resetSessionMusic();
                        }
                    } else {
                        lastInviteMessage.setDuration(CountDownInviteManager.COUNT_DOWN_DURATION - ((int) time));
                        CountDownInviteManager.getInstance(mApplication).startCountDownMessage(lastInviteMessage);
                        if (!isExistListenMusic()) {
                            currentMusicSessionId = lastInviteMessage.getPacketId();
                            currentNumberFriend = lastInviteMessage.getReceiver();
                            currentSong = songModel;
                            isOnRoom = false;
                            listenAlbum = false;
                        }
                    }
                    // update DB
                    messageBusiness.updateAllFieldsOfMessage(lastInviteMessage);
                }
            }
        }
    }

    // cap nhat pref last invite music
    public void updatePrefReceiveInviteMusic(ReengMusicPacket receivePacket) {
        if (mPref == null) {
            mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        long differenceTime = receivePacket.getTimeSend() - TimeHelper.getCurrentTime();
        mPref.edit().putLong(Constants.PREFERENCE.PREF_RECEIVE_MUSIC_DIFFERENCE_TIME, differenceTime).apply();
    }

    /**
     * lay thoi gian sau tinh toan do lech voi sv de tinh lai timeout tin invite
     *
     * @return DifferenceTime
     */
    public long getCurrentTimeAfterDifferenceTime() {
        if (mPref == null) {
            mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        long difference = mPref.getLong(Constants.PREFERENCE.PREF_RECEIVE_MUSIC_DIFFERENCE_TIME, 1);
        return TimeHelper.getCurrentTime() + difference;
    }

    /**
     * cap nhat thread cuoi cung chua tin nhan invite dang o trang thai wait
     *
     * @param threadId
     */
    public void updatePrefSendInviteMusic(int threadId) {
        if (mPref == null) {
            mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        mPref.edit().putInt(Constants.PREFERENCE.PREF_SEND_MUSIC_LAST_THREAD_ID, threadId).apply();
    }

    public ArrayList<StrangerMusic> getStrangerList() {
        return isSelectedConfide ? strangerConfides : strangerMusics;
    }

    public ArrayList<StrangerMusic> getStrangerConfides() {
        return strangerConfides;
    }

    public ArrayList<StrangerMusic> getStrangerMusics() {
        return strangerMusics;
    }

    public long getCurrentTimeStrangerMusic() {
        return currentTimeStrangerMusic;
    }

    public void resetStrangerList() {
        strangerConfides = new ArrayList<>();
        strangerMusics = new ArrayList<>();
    }

    public void setStrangerList(ArrayList<StrangerMusic> strangerList) {
        if (isSelectedConfide) {
            this.strangerConfides = strangerList;
        } else {
            this.strangerMusics = strangerList;
        }
    }

    public ArrayList<StrangerMusic> getNewStrangerList() {
        return isSelectedConfide ? newStrangerConfides : newStrangerMusics;
    }

    public ArrayList<StrangerMusic> getNewStrangerConfides() {
        return newStrangerConfides;
    }

    public ArrayList<StrangerMusic> getNewStrangerMusics() {
        return newStrangerMusics;
    }

    //sticky
    public ArrayList<StrangerSticky> getStickyStrangerMusics() {
        return stickyStrangerMusics;
    }

    public void setStickyStrangerMusics(ArrayList<StrangerSticky> stickyStrangerMusics) {
        this.stickyStrangerMusics = stickyStrangerMusics;
    }

    public ArrayList<StrangerSticky> getNewStickyStrangerMusics() {
        return newStickyStrangerMusics;
    }

    private String getUrlGetListStrangerMusic(String lastRoomId, boolean isConfide) {
        StrangerFilterHelper filterHelper = StrangerFilterHelper.getInstance(mApplication);
        String url;
        StringBuilder sb = new StringBuilder();
        long currentTime = TimeHelper.getCurrentTime();
        String numberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
        String regionCode = account.getRegionCode();
        String token = account.getToken();
        String groupFilter = String.valueOf(getGroupFiler());
        String state = "-1";
        String gender = String.valueOf(filterHelper.getFilterSex());
        String locationId = filterHelper.getFilterLocationId();
        String ageMin = String.valueOf(filterHelper.getFilterAgeMin());
        String ageMax = String.valueOf(filterHelper.getFilterAgeMax());
        int vip = mApplication.getReengAccountBusiness().isVip() ? 1 : 0;
        sb.append(account.getJidNumber()).
                append(regionCode).
                append(groupFilter).
                append(gender).
                append(state).
                append(lastRoomId).
                append(locationId).
                append(ageMin).
                append(ageMax).
                append(vip).
                append(mApplication.getReengAccountBusiness().getOperator()).
                append(token).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), token));
        String baseUrl = isConfide ? UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_STRANGER_CONFIDE) :
                UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_STRANGER_MUSIC);
        url = String.format(baseUrl,
                numberEncode,
                regionCode,
                groupFilter,
                gender,
                state,
                lastRoomId,
                HttpHelper.EncoderUrl(locationId),
                ageMin,
                ageMax,
                String.valueOf(vip),
                String.valueOf(currentTime),
                dataEncrypt,
                mApplication.getReengAccountBusiness().getOperator());
        Log.d(TAG, "getUrlGetListStrangerMusic: " + url);
        return url;
    }

    /**
     * stranger music
     *
     * @param isCheckNewRoom kiem tra notify new room
     * @param isSub          kiem tra co gui sub len sv hay khong
     */
    public void loadListStrangerMusic(final boolean isCheckNewRoom, final boolean isSub,
                                      final boolean isNewFilter, final boolean isConfide) {
        String tag;
        if (isCheckNewRoom) {
            tag = isConfide ? TAG_CONFIDE_STRANGER_CHECK_NEWS : TAG_MUSIC_STRANGER_CHECK_NEWS;
        } else {
            tag = isConfide ? TAG_CONFIDE_STRANGER_LIST : TAG_MUSIC_STRANGER_LIST;
        }
//        String tag = isCheckNewRoom ? TAG_STRANGER_CHECK_NEWS : TAG_STRANGER_LIST;
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(tag);
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!NetworkHelper.isConnectInternet(mApplication) || account == null || !account.isActive()
                || mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            if (isNewFilter) {
                notifyFilterError(-2);
            } else {
                notifyResponseError(-2);
            }
            return;
        }
        if (!isCheckNewRoom) {
            notifyStarting();
        }
        final long time = System.currentTimeMillis();
        String url = getUrlGetListStrangerMusic("-1", isConfide);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        long deltaTime = System.currentTimeMillis() - time;
                        String decryptResponse = HttpHelper.decryptResponse(response, account.getToken());
                        Log.d(TAG, "decryptResponse: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            String keyLog = isConfide ? Constants.LogKQI.STRANGER_GET_LIST_STRAGER_TALK : Constants.LogKQI.STRANGER_GET_LIST_STRAGER_MUSIC;
                            LogKQIHelper.getInstance(mApplication).saveLogKQI(keyLog,
                                    String.valueOf(deltaTime), String.valueOf(time), String.valueOf(errorCode));
                            if (errorCode == HTTPCode.E200_OK) {
                                if (responseObject.has(STRANGER_MUSIC.GROUP_ID)) {// cap nhat filter group id
                                    int filterGroupId = responseObject.getInt(STRANGER_MUSIC.GROUP_ID);
                                    Log.d(TAG, "filterGroupId: " + filterGroupId);
                                    updateGroupFilter(filterGroupId);
                                }
                                if (responseObject.has(STRANGER_MUSIC.STRANGER_BANNER)) {
                                    strangerMusicBannerUrl = responseObject.getString(STRANGER_MUSIC.STRANGER_BANNER);
                                } else {
                                    strangerMusicBannerUrl = "";
                                }
                                Log.d(TAG, "strangerMusicBannerUrl: " + strangerMusicBannerUrl);
                                ArrayList<StrangerMusic> strangers = parserStrangerMusic(responseObject, isConfide);
                                ArrayList<StrangerSticky> stickyStrangers = parserStickyStrangerMusic(responseObject);
                                if (isCheckNewRoom) {
                                    if (checkNotifyNewRoom(strangers, isConfide)) {
                                        if (isConfide) {
                                            newStrangerConfides = strangers;
                                        } else {
                                            newStrangerMusics = strangers;
                                        }
                                        newStickyStrangerMusics = stickyStrangers;
                                        notifyNewRoomResponse(true);
                                    } else {
                                        notifyNewRoomResponse(false);
                                    }
                                } else {
                                    if (isConfide) {
                                        strangerConfides = strangers;
                                    } else {
                                        strangerMusics = strangers;
                                    }
                                    stickyStrangerMusics = stickyStrangers;
                                    if (isConfide) {
                                        notifyRefreshConfideDataResponse(strangers, stickyStrangers);
                                    } else {
                                        notifyRefreshMusicDataResponse(strangers, stickyStrangers);
                                    }
                                }
                                /*if (isSub)
                                    PubSubManager.getInstance(mApplication).processSubscribeStrangerMusic(strangers);*/
                            } else {
                                if (isCheckNewRoom) {
                                    notifyNewRoomResponse(false);
                                } else if (isNewFilter) {
                                    notifyFilterError(errorCode);
                                } else {
                                    notifyResponseError(errorCode);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            if (isCheckNewRoom) {
                                notifyNewRoomResponse(false);
                            } else if (isNewFilter) {
                                notifyFilterError(errorCode);
                            } else {
                                notifyResponseError(errorCode);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                long deltaTime = System.currentTimeMillis() - time;
                String keyLog = isConfide ? Constants.LogKQI.STRANGER_GET_LIST_STRAGER_TALK : Constants.LogKQI.STRANGER_GET_LIST_STRAGER_MUSIC;
                LogKQIHelper.getInstance(mApplication).saveLogKQI(keyLog,
                        String.valueOf(deltaTime), String.valueOf(time), Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                if (isCheckNewRoom) {
                    notifyNewRoomResponse(false);
                } else if (isNewFilter) {
                    notifyFilterError(-1);
                } else {
                    notifyResponseError(-1);
                }
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, tag, false);
    }

    public void loadMoreStrangerMusic(long lastRoomId, final boolean isConfide) {
        String tag = isConfide ? TAG_CONFIDE_STRANGER_LIST : TAG_MUSIC_STRANGER_LIST;
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(tag);
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (lastRoomId == -1 || !NetworkHelper.isConnectInternet(mApplication) ||
                account == null || !account.isActive()) {
            notifyResponseError(-2);
            return;
        }
        String url = getUrlGetListStrangerMusic(String.valueOf(lastRoomId), isConfide);
        final long time = System.currentTimeMillis();
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        long deltaTime = System.currentTimeMillis() - time;
                        String decryptResponse = HttpHelper.decryptResponse(response, account.getToken());
                        Log.d(TAG, "decryptResponse: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            String keyLog = isConfide ? Constants.LogKQI.STRANGER_GET_LIST_STRAGER_TALK : Constants.LogKQI.STRANGER_GET_LIST_STRAGER_MUSIC;
                            LogKQIHelper.getInstance(mApplication).saveLogKQI(keyLog,
                                    String.valueOf(deltaTime), String.valueOf(time), String.valueOf(errorCode));
                            if (errorCode == HTTPCode.E200_OK) {
                                if (responseObject.has(STRANGER_MUSIC.GROUP_ID)) {// cap nhat filter group id
                                    int filterGroupId = responseObject.getInt(STRANGER_MUSIC.GROUP_ID);
                                    updateGroupFilter(filterGroupId);
                                }
                                if (responseObject.has(STRANGER_MUSIC.STRANGER_BANNER)) {
                                    strangerMusicBannerUrl = responseObject.getString(STRANGER_MUSIC.STRANGER_BANNER);
                                } else {
                                    strangerMusicBannerUrl = "";
                                }
                                ArrayList<StrangerMusic> listParser = parserStrangerMusic(responseObject, isConfide);
                                if (!listParser.isEmpty()) {
                                    if (isConfide) {
                                        strangerConfides.addAll(listParser);
                                    } else {
                                        strangerMusics.addAll(listParser);
                                    }
                                    //PubSubManager.getInstance(mApplication).processSubscribeStrangerMusic(listParser);
                                }
                                notifyLoadMoreDataResponse(isConfide ? strangerConfides : strangerMusics, listParser, isConfide);
                            } else {
                                notifyResponseError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            notifyResponseError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                long deltaTime = System.currentTimeMillis() - time;
                String keyLog = isConfide ? Constants.LogKQI.STRANGER_GET_LIST_STRAGER_TALK : Constants.LogKQI.STRANGER_GET_LIST_STRAGER_MUSIC;
                LogKQIHelper.getInstance(mApplication).saveLogKQI(keyLog,
                        String.valueOf(deltaTime), String.valueOf(time), Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                notifyResponseError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, tag, false);
    }

    public void createRoomStrangerMusic(final MediaModel songModel, final onCreateStrangerRoomListener listener) {
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!NetworkHelper.isConnectInternet(mApplication) || account == null || !account.isActive()) {
            listener.onError(-2);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.POST_STRANGER_MUSIC);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "response: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, account.getToken());
                        Log.d(TAG, "decryptResponse: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                StrangerMusic strangerMusic = new StrangerMusic();
                                strangerMusic.setTypeObj(Constants.STRANGER_MUSIC.TYPE_MUSIC);
                                strangerMusic.setPosterJid(account.getJidNumber());
                                strangerMusic.setPosterName(account.getName());
                                strangerMusic.setPosterGender(account.getGender());
                                strangerMusic.setPosterLastAvatar(account.getLastChangeAvatar());
                                strangerMusic.setPosterIsStar(0);// room thuong
                                //rom info
                                strangerMusic.setState(Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_WAIT);
                                if (responseObject.has(STRANGER_MUSIC.SESSION_ID)) {
                                    strangerMusic.setSessionId(responseObject.getString(STRANGER_MUSIC.SESSION_ID));
                                }
                                if (responseObject.has(STRANGER_MUSIC.GROUP_ID)) {// gui group id
                                    currentPosterGroupId = responseObject.getInt(STRANGER_MUSIC.GROUP_ID);
                                }
                                if (responseObject.has(STRANGER_MUSIC.ROOM_ID)) {
                                    strangerMusic.setRoomId(responseObject.getLong(STRANGER_MUSIC.ROOM_ID));
                                }
                                //song info
                                strangerMusic.setSongModel(songModel);
                                // create rom
                                updateListStrangerAfterCreateRoom(strangerMusic, false);
                                createSessionStrangerMusic(strangerMusic);
                                listener.onResponse(strangerMusics, false);
                                LuckyWheelHelper.getInstance(mApplication).doMission(Constants.LUCKY_WHEEL
                                        .ITEM_LISTEN_STRANGER);//post nghe ng lạ
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "JSONException:", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String posterInfo = createUserInfoParams(account);
                String songInfo = createSongInfoParams(songModel);
                int groupFiler = getGroupFiler();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(posterInfo).
                        append(songInfo).
                        append(account.getRegionCode()).
                        append(groupFiler).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(STRANGER_MUSIC.POSTER_INFO, posterInfo);
                params.put(STRANGER_MUSIC.SONG_INFO, songInfo);
                params.put(Constants.HTTP.REST_COUNTRY_CODE, account.getRegionCode());
                params.put(STRANGER_MUSIC.GROUP_ID, String.valueOf(groupFiler));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "param: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void createRoomStrangerConfide(final String status, final onCreateStrangerRoomListener listener) {
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!NetworkHelper.isConnectInternet(mApplication) || account == null || !account.isActive()) {
            listener.onError(-2);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.POST_STRANGER_CONFIDE);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "response: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, account.getToken());
                        Log.d(TAG, "decryptResponse: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                StrangerMusic strangerMusic = new StrangerMusic();
                                strangerMusic.setTypeObj(Constants.STRANGER_MUSIC.TYPE_CONFIDE);
                                strangerMusic.setPosterJid(account.getJidNumber());
                                strangerMusic.setPosterName(account.getName());
                                strangerMusic.setPosterGender(account.getGender());
                                strangerMusic.setPosterLastAvatar(account.getLastChangeAvatar());
                                strangerMusic.setPosterIsStar(0);// room thuong
                                //rom info
                                strangerMusic.setState(Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_WAIT);
                                if (responseObject.has(STRANGER_MUSIC.SESSION_ID)) {
                                    String sessionId = responseObject.getString(STRANGER_MUSIC.SESSION_ID);
                                    strangerMusic.setSessionId(sessionId);
                                    currentConfideSession = sessionId;
                                }
                                if (responseObject.has(STRANGER_MUSIC.GROUP_ID)) {// gui group id
                                    currentPosterGroupId = responseObject.getInt(STRANGER_MUSIC.GROUP_ID);
                                }
                                if (responseObject.has(STRANGER_MUSIC.ROOM_ID)) {
                                    strangerMusic.setRoomId(responseObject.getLong(STRANGER_MUSIC.ROOM_ID));
                                }
                                strangerMusic.setConfideStatus(status);
                                // create rom
                                updateListStrangerAfterCreateRoom(strangerMusic, true);
                                listener.onResponse(strangerConfides, true);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "JSONException:", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String posterInfo = createUserInfoParams(account);
                int groupFiler = getGroupFiler();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(posterInfo).
                        append(status).
                        append(account.getRegionCode()).
                        append(groupFiler).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(STRANGER_MUSIC.POSTER_INFO, posterInfo);
                params.put(STRANGER_MUSIC.CONFIDE_STATUS, status);
                params.put(Constants.HTTP.REST_COUNTRY_CODE, account.getRegionCode());
                params.put(STRANGER_MUSIC.GROUP_ID, String.valueOf(groupFiler));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "param: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void acceptRoomStranger(final StrangerMusic strangerMusic,
                                   final onAcceptStrangerRoomListener listener, final boolean isConfide) {
        //mStrangerMusicAccept = strangerMusic;
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!NetworkHelper.isConnectInternet(mApplication) || account == null || !account.isActive()) {
            listener.onError(-2, strangerMusic.getPosterName(), isConfide);
            return;
        }
        String url = isConfide ? UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.ACCEPT_STRANGER_CONFIDE) :
                UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.ACCEPT_STRANGER_MUSIC);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "response: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, account.getToken());
                        Log.d(TAG, "acceptRoomStranger decryptResponse: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                strangerMusic.setSessionId(responseObject.optString(STRANGER_MUSIC.SESSION_ID));
                                strangerMusic.setState(Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_ACCEPTED);
                                strangerMusic.setAcceptorJid(account.getJidNumber());
                                strangerMusic.setAcceptorName(account.getName());
                                strangerMusic.setAcceptorLastAvatar(account.getLastChangeAvatar());
                                //insertNewMediaModelFromStrangerMusic(mStrangerMusicAccept);
                                ThreadMessage threadMessage = isConfide ? processAcceptStrangerConfide(strangerMusic) : processAcceptedStrangerMusic(strangerMusic);
                                listener.onResponse(threadMessage, strangerMusic, isConfide);
                                if (!isConfide){
                                    LuckyWheelHelper.getInstance(mApplication).doMission(Constants.LUCKY_WHEEL.ITEM_LISTEN_STRANGER);//post nghe ng lạ
                                }else {
                                    // TODO: 5/22/2020 post tâm sự cùng người lạ
                                    LuckyWheelHelper.getInstance(mApplication).doMission(Constants.LUCKY_WHEEL.ITEM_TALK_STRANGER);
                                }
                            } else {
                                if (errorCode == HTTPCode.E100_STRANGER_MUSIC_NOT_FOUND || errorCode == HTTPCode.E0_STRANGER_MUSIC_CANCEL) {
                                    strangerMusics.remove(strangerMusic);
                                } else if (errorCode == HTTPCode.E205_STRANGER_MUSIC_ACCEPTED) {
                                    JSONObject item = responseObject.optJSONObject("roomInfo");
                                    if (item != null) {
                                        StrangerMusic info = new StrangerMusic();
                                        info.setJsonObject(item, isConfide);
                                        strangerMusic.setSessionId(responseObject.optString(STRANGER_MUSIC.SESSION_ID));
                                        strangerMusic.setState(Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_ACCEPTED);
                                        strangerMusic.setAcceptorJid(info.getAcceptorJid());
                                        strangerMusic.setAcceptorName(info.getAcceptorName());
                                        strangerMusic.setAcceptorLastAvatar(info.getAcceptorLastAvatar());
                                        strangerMusic.setAcceptorStatus(info.getAcceptorStatus());
                                        strangerMusic.setAcceptorBirthDay(info.getAcceptorBirthDay());
                                    }
                                }
                                listener.onError(errorCode, strangerMusic.getPosterName(), isConfide);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "JSONException:", e);
                            listener.onError(errorCode, strangerMusic.getPosterName(), isConfide);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, strangerMusic.getPosterName(), isConfide);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String accepterInfo = createUserInfoParams(account);
                int groupFiler = getGroupFiler();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(accepterInfo).
                        append(strangerMusic.getSessionId()).
                        append(account.getRegionCode()).
                        append(groupFiler).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(STRANGER_MUSIC.ACCEPTOR_INFO, accepterInfo);
                params.put(STRANGER_MUSIC.SESSION_ID, strangerMusic.getSessionId());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, account.getRegionCode());
                params.put(STRANGER_MUSIC.GROUP_ID, String.valueOf(groupFiler));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    // huy room khi moi nguoi khac
    public void cancelRoomStrangerRoom(final String sessionId, final onCancelStrangeRoomListener listener, final boolean isConfide) {
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!NetworkHelper.isConnectInternet(mApplication) || account == null || !account.isActive()) {
            listener.onError(-1);
            return;
        }
        String url = isConfide ? UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.CANCEL_STRANGER_CONFIDE) :
                UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.CANCEL_STRANGER_MUSIC);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "response: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, account.getToken());
                        Log.d(TAG, "decryptResponse: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            // xoa thanh cong hoac khong ton tai room nay thi deu xoa duoi client
                            if (errorCode == HTTPCode.E200_OK || errorCode == HTTPCode.E100_STRANGER_MUSIC_NOT_FOUND) {
                                resetRoomConfide();
                                resetSessionStranger();
                                updateListStrangerMusicAfterCancelRoom(account.getJidNumber(), isConfide);
                                if (listener != null) {
                                    listener.onResponse();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onError(errorCode);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            if (listener != null) {
                                listener.onError(errorCode);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                if (listener != null) {
                    listener.onError(-1);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(sessionId).
                        append(account.getRegionCode()).
                        append(currentPosterGroupId).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put("poster", account.getJidNumber());
                params.put(STRANGER_MUSIC.SESSION_ID, sessionId);
                params.put(Constants.HTTP.REST_COUNTRY_CODE, account.getRegionCode());
                params.put(STRANGER_MUSIC.GROUP_ID, String.valueOf(currentPosterGroupId));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "param: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void reInviteStrangerMusic(StrangerMusic strangerMusic, onReInviteListener listener) {
        //insertNewMediaModelFromStrangerMusic(strangerMusic);
        strangerMusic.setState(Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_TIME_OUT);
        strangerMusic.setAcceptorJid(account.getJidNumber());
        strangerMusic.setAcceptorName(account.getName());
        strangerMusic.setAcceptorLastAvatar(account.getLastChangeAvatar());
        strangerMusic.setAcceptorBirthDay(account.getBirthdayString());
        strangerMusic.setAcceptorGender(account.getGender());
        mStrangerBusiness = mApplication.getStrangerBusiness();
        ThreadMessage threadMessage = mStrangerBusiness.processOutGoingStrangerMusic(strangerMusic);
        insertMessageReinviteStrangeMusic(mApplication.getMessageBusiness(), threadMessage, strangerMusic);
        //play music and start ping
        currentTimeStrangerMusic = -1;
        listener.onResponse(threadMessage);
    }

    public String createUserInfoParams(ReengAccount account) {
        if (account == null || !account.isActive() || TextUtils.isEmpty(account.getToken())) {
            return null;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
            String name = account.getName();
            if (TextUtils.isEmpty(name)) {
                name = account.getJidNumber();
            }
            jsonObject.put(STRANGER_MUSIC.NAME, name);
            if (!TextUtils.isEmpty(account.getLastChangeAvatar())) {
                jsonObject.put(STRANGER_MUSIC.LAST_AVATAR, account.getLastChangeAvatar());
            }
            if (!TextUtils.isEmpty(account.getBirthdayString())) {
                jsonObject.put(STRANGER_MUSIC.BIRTHDAY, account.getBirthdayString());
            }
            if (!TextUtils.isEmpty(account.getStatus())) {
                jsonObject.put(STRANGER_MUSIC.STATUS, account.getStatus());
            }
            jsonObject.put(STRANGER_MUSIC.GENDER, account.getGender());
            //            int hideState = mApplication.getReengAccountBusiness().isHideStrangerHistory() ? 1 : 0;
            jsonObject.put(STRANGER_MUSIC.HIDE_STRANGER_HISTORY, 0);
            jsonObject.put(STRANGER_MUSIC.LOCATION_ID, mApplication.getReengAccountBusiness().getLocationId());
            String operator = mApplication.getReengAccountBusiness().getOperator();
            if (operator == null) operator = "";
            jsonObject.put(STRANGER_MUSIC.STRANGER_OPERATER, operator);
            return jsonObject.toString();
        } catch (Exception e) {
            Log.e(TAG, e);
            return null;
        }
    }

    private String createSongInfoParams(MediaModel songModel) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(STRANGER_MUSIC.SONG_ID, songModel.getId());
            jsonObject.put(STRANGER_MUSIC.SONG_NAME, songModel.getName());
            jsonObject.put(STRANGER_MUSIC.SONG_SINGER, songModel.getSinger());
            jsonObject.put(STRANGER_MUSIC.SONG_IMAGE, songModel.getImage());
            jsonObject.put(STRANGER_MUSIC.SONG_URL, songModel.getUrl());
            jsonObject.put(STRANGER_MUSIC.SONG_MEDIA_URL, songModel.getMedia_url());
            jsonObject.put(STRANGER_MUSIC.SONG_CRBT_CODE, songModel.getCrbtCode());
            jsonObject.put(STRANGER_MUSIC.SONG_CRBT_PRICE, songModel.getCrbtPrice());
            return jsonObject.toString();
        } catch (Exception e) {
            Log.e(TAG, e);
            return null;
        }
    }

    private ArrayList<StrangerMusic> parserStrangerMusic(JSONObject jsonObject, boolean isConfide) throws JSONException {
        ArrayList<StrangerMusic> strangerMusics = new ArrayList<>();
        if (jsonObject.has("listRoom")) {
            JSONArray jsonArray = jsonObject.getJSONArray("listRoom");
            int size = jsonArray.length();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    JSONObject item = jsonArray.getJSONObject(i);
                    StrangerMusic strangerMusic = new StrangerMusic();
                    strangerMusic.setJsonObject(item, isConfide);
                    strangerMusics.add(strangerMusic);
                }
            }
        }
        return strangerMusics;
    }

    /**
     * danh sach room star sticky
     *
     * @param jsonObject
     * @return
     * @throws JSONException
     */
    private ArrayList<StrangerSticky> parserStickyStrangerMusic(JSONObject jsonObject) throws JSONException {
        ArrayList<StrangerSticky> strangerStickys = new ArrayList<>();
        if (jsonObject.has("listStickyBanner")) {
            JSONArray jsonArray = jsonObject.getJSONArray("listStickyBanner");
            Log.d(TAG, "listStickyBanner" + jsonArray.toString());
            int size = jsonArray.length();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    JSONObject item = jsonArray.getJSONObject(i);
                    StrangerSticky strangerSticky = new StrangerSticky();
                    strangerSticky.setJsonObject(item);
                    strangerStickys.add(strangerSticky);
                }
            }
        }
        return strangerStickys;
    }


    private void createSessionStrangerMusic(StrangerMusic strangerMusic) {
        currentMusicSessionId = strangerMusic.getSessionId();
        currentSong = strangerMusic.getSongModel();
        currentTimeStrangerMusic = TimeHelper.getCurrentTime();
        isOnRoom = false;// chua play(luc nao goi ham play nhac thi set sau)
        listenAlbum = false;
    }

    private void updateListStrangerAfterCreateRoom(StrangerMusic newStrangerMusic, boolean isConfide) {
        ArrayList<StrangerMusic> list = isConfide ? strangerConfides : strangerMusics;
        if (list.isEmpty()) {
            list.add(newStrangerMusic);
        } else {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                StrangerMusic item = list.get(i);
                if (item.getPosterJid() != null &&
                        item.getPosterJid().equals(newStrangerMusic.getPosterJid())) {
                    list.remove(i);// xoa roi thoat vong for
                    break;
                }
            }
            list.add(0, newStrangerMusic);
        }
    }

    private void updateGroupFilter(int groupId) {
        Log.d(TAG, "updateGroupFilter: " + groupId);
        this.mGroupFiler = groupId;
    }

    private int getGroupFiler() {
        return mGroupFiler;
    }

    private ThreadMessage processAcceptedStrangerMusic(StrangerMusic strangerMusic) {
        Log.d(TAG, "processAcceptedStrangerMusic: " + strangerMusic.toString());
        mStrangerBusiness = mApplication.getStrangerBusiness();
        ThreadMessage threadMessage = mStrangerBusiness.processOutGoingStrangerMusic(strangerMusic);
        insertMessageAcceptedStrangeMusic(mApplication.getMessageBusiness(), threadMessage, strangerMusic);
        //play music and start ping
        currentTimeStrangerMusic = -1;
        String fromNumber = strangerMusic.getPosterJid();
        String newNumber = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(fromNumber);
        if (newNumber != null) fromNumber = newNumber;
        onAcceptAndPlayMusic(strangerMusic.getSessionId(), fromNumber, strangerMusic.getSongModel());
        return threadMessage;
    }

    private ThreadMessage processAcceptStrangerConfide(StrangerMusic strangerMusic) {
        Log.d(TAG, "processAcceptStrangerConfide: " + strangerMusic.toString());
        return mApplication.getStrangerBusiness().processOutGoingStrangerMusic(strangerMusic, Constants.CONTACT.STRANGER_CONFIDE_ID);
    }

    /**
     * tao message thong bao dong y cung nghe voi nguoi la
     *
     * @param mMessageBusiness
     */
    public void insertMessageAcceptedStrangeMusic(MessageBusiness mMessageBusiness,
                                                  ThreadMessage threadMessage, StrangerMusic strangerMusic) {
        Resources res = mApplication.getResources();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloShareMusicMessage soloShareMusicMessage = new SoloShareMusicMessage(threadMessage,
                myNumber, strangerMusic.getPosterJid());
        soloShareMusicMessage.setPacketId(PacketMessageId.getInstance().genShareMusicPacketId(threadMessage
                        .getThreadType(),
                ReengMessagePacket.SubType.music_stranger_accept));
        soloShareMusicMessage.setMessageType(ReengMessageConstant.MessageType.inviteShareMusic);
        soloShareMusicMessage.setDirection(ReengMessageConstant.Direction.send);
        soloShareMusicMessage.setStatus(ReengMessageConstant.STATUS_NOT_SEND);// set trang thai -1 de khong hien thi
        // status dang gui voi message chuyen bai
        soloShareMusicMessage.setSongId(ReengMessage.SONG_ID_DEFAULT_NEW);
        soloShareMusicMessage.setSongModel(strangerMusic.getSongModel());
        soloShareMusicMessage.setImageUrl(strangerMusic.getSessionId());
        soloShareMusicMessage.setDuration(0);
        CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(soloShareMusicMessage);
        soloShareMusicMessage.setMusicState(ReengMessageConstant.MUSIC_STATE_ACCEPTED);
        String content = String.format(res.getString(R.string.stranger_music_acceptor_accepted),
                strangerMusic.getPosterName());
        String contentBold = String.format(res.getString(R.string.stranger_music_acceptor_accepted),
                TextHelper.textBoldWithHTML(strangerMusic.getPosterName()));
        soloShareMusicMessage.setContent(content);
        soloShareMusicMessage.setFileName(contentBold);
        soloShareMusicMessage.setDuration(1);
        // notify message
        if (mMessageBusiness.insertNewMessageBeforeSend(threadMessage,
                ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT, soloShareMusicMessage)) {
            mMessageBusiness.refreshThreadWithoutNewMessage(soloShareMusicMessage.getThreadId());
            mApplication.getXmppManager().sendStrangerMusicMessage(soloShareMusicMessage, strangerMusic,
                    threadMessage, ReengMessagePacket.SubType.music_stranger_accept);
        }
    }

    /**
     * tao message thong bao dong y cung nghe voi nguoi la
     *
     * @param mMessageBusiness
     */
    public void insertMessageReinviteStrangeMusic(MessageBusiness mMessageBusiness,
                                                  ThreadMessage threadMessage, StrangerMusic strangerMusic) {
        Resources res = mApplication.getResources();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        SoloShareMusicMessage soloShareMusicMessage = new SoloShareMusicMessage(threadMessage,
                myNumber, strangerMusic.getPosterJid());
        // ban tin reinvite strange co gen packet id chuan
        soloShareMusicMessage.setPacketId(PacketMessageId.getInstance().genShareMusicPacketId(threadMessage
                        .getThreadType(),
                ReengMessagePacket.SubType.music_stranger_reinvite));
        soloShareMusicMessage.setMessageType(ReengMessageConstant.MessageType.inviteShareMusic);
        soloShareMusicMessage.setDirection(ReengMessageConstant.Direction.send);
        soloShareMusicMessage.setStatus(ReengMessageConstant.STATUS_NOT_SEND);// set trang thai -1 de khong hien thi
        // status dang gui voi message chuyen bai
        soloShareMusicMessage.setSongId(ReengMessage.SONG_ID_DEFAULT_NEW);
        soloShareMusicMessage.setSongModel(strangerMusic.getSongModel());
        //soloShareMusicMessage.setImageUrl(strangerMusic.getSessionId());
        soloShareMusicMessage.setImageUrl(soloShareMusicMessage.getPacketId());
        String content = String.format(res.getString(R.string.invite_share_music_send),
                strangerMusic.getPosterName());
        String contentBold = String.format(res.getString(R.string.invite_share_music_send),
                TextHelper.textBoldWithHTML(strangerMusic.getPosterName()));
        soloShareMusicMessage.setContent(content);
        soloShareMusicMessage.setFileName(contentBold);
        //soloShareMusicMessage.setDuration(1);
        // notify stri
        if (mMessageBusiness.insertNewMessageBeforeSend(threadMessage,
                ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT, soloShareMusicMessage)) {
            mMessageBusiness.refreshThreadWithoutNewMessage(soloShareMusicMessage.getThreadId());
            mApplication.getXmppManager().sendStrangerMusicMessage(soloShareMusicMessage, strangerMusic,
                    threadMessage, ReengMessagePacket.SubType.music_stranger_reinvite);
        }
    }

    public void updateListStrangerMusicAfterCancelRoom(String myNumber, boolean isConfide) {
        ArrayList<StrangerMusic> list = isConfide ? strangerConfides : strangerMusics;
        if (list != null && !list.isEmpty()) {
            for (StrangerMusic strangerMusic : list) {
                // xoa loi moi cua minh neu chua o trang thai dong y
                if (strangerMusic.getState() != Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_ACCEPTED &&
                        strangerMusic.getPosterJid() != null && strangerMusic.getPosterJid().equals(myNumber)) {
                    list.remove(strangerMusic);// xoa roi thoat vong for
                    break;
                }
            }
        }
    }

    public String getMessageErrorByErrorCode(int errorCode, String posterName, boolean isConfider) {
        Resources res = mApplication.getResources();
        String msg;
        switch (errorCode) {
            case HTTPCode.E100_STRANGER_MUSIC_NOT_FOUND:
                msg = res.getString(R.string.e100_error_stranger_music_not_found);
                break;
            case HTTPCode.E101_STRANGER_MUSIC_NO_PERMISSION:
                msg = res.getString(R.string.e101_error_stranger_music_no_permission);
                break;
            case HTTPCode.E205_STRANGER_MUSIC_ACCEPTED:
                if (posterName == null) {
                    msg = res.getString(R.string.e601_error_but_undefined);
                } else if (isConfider) {
                    msg = String.format(res.getString(R.string.e205_error_stranger_confider_accepted), posterName);
                } else {
                    msg = String.format(res.getString(R.string.e205_error_stranger_music_accepted), posterName);
                }
                break;
            case HTTPCode.E206_STRANGER_MUSIC_TIME_OUT:
                msg = res.getString(R.string.e206_error_stranger_music_time_out);
                break;
            case HTTPCode.E0_STRANGER_MUSIC_CANCEL:
                msg = res.getString(R.string.e0_error_stranger_music_cancel);
                break;
            case HTTPCode.E401_NOT_AUTHEN:
                msg = res.getString(R.string.e401_unauthorized);
                break;
            case -2:
                msg = res.getString(R.string.error_internet_disconnect);
                break;
            default:
                msg = res.getString(R.string.e601_error_but_undefined);
                break;
        }
        return msg;
    }

    private boolean checkNotifyNewRoom(ArrayList<StrangerMusic> newList, boolean isConfide) {
        ArrayList<StrangerMusic> oldList = isConfide ? strangerConfides : strangerMusics;
        if (newList == null || newList.isEmpty()) {
            return false;
        } else if (oldList == null || oldList.isEmpty()) {
            return true;
        } else {
            long firstOldId = oldList.get(0).getRoomId();
            int sizeNewItem = 0;
            for (StrangerMusic strangerMusic : newList) {
                if (strangerMusic.getRoomId() > firstOldId) {
                    sizeNewItem++;
                } else {//neu item <firstid thi thoat vong lap luon
                    break;
                }
            }
            if (sizeNewItem >= Constants.STRANGER_MUSIC.MIM_NEW_ROOM) {
                return true;
            }
        }
        return false;
    }

    /**
     * cap nhat thong tin room khi nhan dc presence
     *
     * @param presence
     */
    public void updateStrangerMusicAfterReceiverPresence(Presence presence, Presence.SubType subType) {
        String roomInfo = presence.getMusicInfo();
        if (TextUtils.isEmpty(roomInfo)) {
            return;
        }
        if (subType == Presence.SubType.music_info || subType == Presence.SubType.confide_accepted) {
            boolean isConfide = subType == Presence.SubType.confide_accepted;
            try {
                StrangerMusic strangerMusic = new StrangerMusic();
                JSONObject object = new JSONObject(roomInfo);
                if (object.has("info")) {
                    ArrayList<StrangerMusic> newList = isConfide ? newStrangerConfides : newStrangerMusics;
                    ArrayList<StrangerMusic> list = isConfide ? strangerConfides : strangerMusics;
                    JSONObject strangerObject = object.getJSONObject("info");
                    strangerMusic.setJsonObject(strangerObject, isConfide);
                    // cap nhat newStrangerMusics neu co
                    if (newList != null) {
                        for (StrangerMusic item : newList) {
                            if (item.getRoomId() == strangerMusic.getRoomId()) {
                                updateStranger(item, strangerMusic);
                                break;
                            }
                        }
                    }
                    // cap nhat strangerMusics neu co
                    if (list != null) {
                        for (StrangerMusic item : list) {
                            if (item.getRoomId() == strangerMusic.getRoomId()) {
                                updateStranger(item, strangerMusic);
                                // notify dataset change
                                notifyRefreshData();
                                break;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.d(TAG, "Exception", e);
            }
        }
    }

    private void updateStranger(StrangerMusic oldItem, StrangerMusic newItem) {
        oldItem.setAcceptorName(newItem.getAcceptorName());
        oldItem.setAcceptorJid(newItem.getAcceptorJid());
        oldItem.setAcceptorLastAvatar(newItem.getAcceptorLastAvatar());
        oldItem.setAcceptorGender(newItem.getAcceptorGender());
        oldItem.setAcceptorBirthDay(newItem.getAcceptorBirthDay());
        oldItem.setState(newItem.getState());
    }

    /**
     * cung nghe voi sao
     */
    public void setCurrentRoomId(String currentRoomId) {
        this.currentRoomId = currentRoomId;
    }

    public String getCurrentRoomId() {
        return currentRoomId;
    }

    public void setCurrentRoomSessionId(String currentRoomSessionId) {
        this.currentRoomSessionId = currentRoomSessionId;
    }

    public boolean isPlayList() {
        return isPlayList;
    }

    public String getDescSongRoom() {
        return descSongRoom;
    }

    private void leaveListenerSoloOrStrangerMusic() {
        if (isExistListenerSolo()) {// nghe 1-1
            onLeaveMusic(true);
        } else if (isWaitingStrangerMusic()) {// cho nguoi la
            cancelRoomStrangerRoom(getCurrentMusicSessionId(),
                    new onCancelStrangeRoomListener() {
                        @Override
                        public void onResponse() {

                        }

                        @Override
                        public void onError(int errorCode) {

                        }
                    }, false);
        }
    }

    /**
     * danh dau cung nghe room
     *
     * @param roomId
     * @param sessionId
     */
    public void createRoomMusic(String roomId, String sessionId) {
        leaveListenerSoloOrStrangerMusic();
        currentTimeStrangerMusic = -1;
        currentMusicSessionId = null;
        currentNumberFriend = null;
        currentGroupId = null;
        mApplication.getPlayMusicController().setStatePauseChangeSong(false);
        listenAlbum = false;
        setCurrentRoomId(roomId);
        setCurrentRoomSessionId(sessionId);
    }

    public void processResponseLeaveRoom(String roomId, boolean starUnFollow) {
        OfficerAccount accountRoomChat = mApplication.getOfficerBusiness().
                getOfficerAccountByServerId(roomId);
        if (accountRoomChat != null) {
            accountRoomChat.setRoomState(OfficerAccountConstant.ROOM_STATE_NONE);
            mApplication.getOfficerBusiness().updateOfficerAccount(accountRoomChat);
        }
        PubSubManager.getInstance(mApplication).unSubscribeRoomChat(roomId);
        //nhận được bản tin start_unfollow thì back khỏi màn hình chat và show toast thông báo
        if (starUnFollow) {
            mApplication.getMessageBusiness().notifyConfigRoomChange(roomId, true);
        }
        if (isExistListenerRoom() && currentRoomId.equals(roomId)) {// roi room dang cung nghe
            currentRoomId = null;
            clearSessionAndNotifyMusic();
        }
    }

    public void processJoinRoomChat(ThreadMessage threadMessage) {
        this.currentPlayList = threadMessage.getListSongStars();
        this.descSongRoom = threadMessage.getDescSongStar();
        if (threadMessage.getStateOnlineStar() == 0) {// play list "offline"
            processPlayRoomListMusic(threadMessage, currentPlayList);
        } else {         // online
            if (isPlayList() && isOnRoom) {// dang nghe play list, stop play list
                clearSessionAndNotifyMusic();
            }
            this.isPlayList = false;
        }
    }

    public void processChangeMemberFollow(Presence presence) {
        String roomId = presence.getFrom().split("@")[0].trim();
        int followStar = presence.getFollow();
        ThreadMessage threadMessage = mApplication.getMessageBusiness().findRoomThreadByRoomId(roomId);
        if (threadMessage != null) {
            threadMessage.setFollowStar(followStar);
            mApplication.getMessageBusiness().notifyConfigRoomChange(roomId, false);
        }
    }

    public void processChangeBackgroundRoom(Presence presence) {
        String roomId = presence.getFrom().split("@")[0].trim();
        String backgroundUrl = presence.getBackgroundRoom();
        ThreadMessage threadMessage = mApplication.getMessageBusiness().findRoomThreadByRoomId(roomId);
        if (threadMessage != null) {
            mApplication.getMessageBusiness().checkAndUpdateBackground(threadMessage, backgroundUrl);
            mApplication.getMessageBusiness().notifyConfigRoomChange(roomId, false);
        }
    }

    private void processPlayRoomListMusic(ThreadMessage threadMessage, ArrayList<MediaModel> listSongs) {
        if (listSongs == null || listSongs.isEmpty()) {// list song trong
            if (currentRoomId != null && isOnRoom) {// dang nghe trong room,  stop
                clearSessionAndNotifyMusic();
            }
            return;
        }
        // tat nhac
        if (SettingBusiness.getInstance(mApplication).getPrefOffMusicInRoom()) {
            return;
        }
        // dang nghe play list roi
        if (currentRoomId != null && currentRoomId.equals(threadMessage.getServerId()) && isPlayList) {
            return;
        }
        Resources res = mApplication.getResources();
        String msg;
        if (isExistListenerSolo()) {
            String friendSoloName = mApplication.getMessageBusiness().getFriendName(currentNumberFriend);
            msg = String.format(res.getString(R.string.confirm_music_solo_in_room_playlist),
                    friendSoloName, threadMessage.getThreadName());
            confirmPlayListMusicRoom(threadMessage, listSongs, msg);
        } else if (isWaitingStrangerMusic()) {
            msg = String.format(res.getString(R.string.confirm_music_stranger_in_room_playlist),
                    threadMessage.getThreadName());
            confirmPlayListMusicRoom(threadMessage, listSongs, msg);
        } else {// khong nghe 1-1 hoac nguoi la thi play luon
            playListMusicInRoom(threadMessage.getServerId(), listSongs);
        }
    }

    public String getCurrentFriendNameAccepted() {
        return currentFriendNameAccepted;
    }

    public void processPlayRoomMusic(ThreadMessage threadMessage, ReengMessage message) {
        MediaModel songModel = message.getSongModel(this);
        String sessionId = message.getImageUrl();
        if (songModel == null || TextUtils.isEmpty(sessionId)) {//khong hop le
            return;
        }
        //tat nhac
        if (SettingBusiness.getInstance(mApplication).getPrefOffMusicInRoom()) {
            return;
        }
        if (currentRoomSessionId != null && currentRoomSessionId.equals(sessionId)) {// cung 1 sessionId, ko play nua
            Log.d(TAG, "playRoomMusic: cung sessionId");// khong lam gif nua
        } else {
            Resources res = mApplication.getResources();
            String msg;
            if (isExistListenerSolo()) {// nghe 1-1
                String friendSoloName = mApplication.getMessageBusiness().getFriendName(currentNumberFriend);
                msg = String.format(res.getString(R.string.confirm_music_solo_in_room),
                        friendSoloName, threadMessage.getThreadName());
                confirmPlayMusicRoom(threadMessage, message, msg);
            } else if (isWaitingStrangerMusic()) {
                msg = String.format(res.getString(R.string.confirm_music_stranger_in_room),
                        threadMessage.getThreadName());
                confirmPlayMusicRoom(threadMessage, message, msg);
            } else {// khong nghe 1-1 hoac nguoi la thi play luon
                playMusicInRoom(threadMessage, message);
            }
        }
    }

    private void confirmPlayMusicRoom(final ThreadMessage threadMessage, ReengMessage message, String msg) {
        boolean isShowConfirm = PopupHelper.getInstance().checkAndShowDialogConfirmAnyWhere(null, msg,
                message,
                new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        try {
                            playMusicInRoom(threadMessage, (ReengMessage) result);
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                });
        if (!isShowConfirm) {
            playMusicInRoom(threadMessage, message);
        }
    }

    private void confirmPlayListMusicRoom(ThreadMessage threadMessage, ArrayList<MediaModel> listSongs, String msg) {
        final String roomId = threadMessage.getServerId();
        boolean isShowConfirm = PopupHelper.getInstance().checkAndShowDialogConfirmAnyWhere(null, msg,
                listSongs,
                new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        try {
                            playListMusicInRoom(roomId, (ArrayList<MediaModel>) result);
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                });
        if (!isShowConfirm) {
            playListMusicInRoom(roomId, listSongs);
        }
    }

    private void playMusicInRoom(ThreadMessage threadMessage, ReengMessage message) {
        MediaModel songModel = message.getSongModel(this);
        if (songModel == null) return;
        String sessionId = message.getImageUrl();
        createRoomMusic(threadMessage.getServerId(), sessionId);
        isOnRoom = true;
        isPlayList = false;
        currentFriendNameAccepted = message.getSenderName();
        setCurrentSong(songModel);
        if (mApplication.getPlayMusicController() != null) {
            mApplication.getPlayMusicController().getPlayingListDetail(songModel);
            mApplication.getPlayMusicController().setIsPlayFromFeed(false);
        }
        mApplication.getMessageBusiness().notifyReengMessage(mApplication,
                threadMessage, message, ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT);
    }

    private void playListMusicInRoom(String roomId, ArrayList<MediaModel> listSongs) {
        createRoomMusic(roomId, null);
        isOnRoom = true;
        isPlayList = true;
        currentRoomSessionId = null;//reset current session id cung nghe sao
        if (mApplication.getPlayMusicController() != null) {
            int songIndex = 0;
            /*int sizePlayList = listSongs.size();
            if (sizePlayList > 1) {
                Random rand = new Random();
                songIndex = rand.nextInt(sizePlayList - 1);
            }*/
            mApplication.getPlayMusicController().setPlayList(listSongs, songIndex);
            mApplication.getPlayMusicController().setStateRepeat(Constants.PLAY_MUSIC.REPEAT_All);
            mApplication.getPlayMusicController().setStateShuffle(Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
        }
    }

    public void stopEventRoomMusic(String roomId) {
        if (!TextUtils.isEmpty(roomId)) {
            ThreadMessage threadMessage = mApplication.getMessageBusiness().findRoomThreadByRoomId(roomId);
            if (threadMessage == null) {
                return;
            }
            threadMessage.setStateOnlineStar(0);
            mApplication.getMessageBusiness().notifyStateRoomChanged();
            // dang sub room nay. khong nghe play list
            if (PubSubManager.getInstance(mApplication).isExistSubRoomChat(roomId) && !isPlayList) {
                processPlayRoomListMusic(threadMessage, currentPlayList);
            } else if (roomId.equals(currentRoomId) && !isPlayList) {// khong sub, dang nghe cung sao room nay
                resetSessionMusic();
                // close music
                mApplication.getPlayMusicController().closeNotifyMusicAndClearDataSong();
            }
        }
    }

    public MediaModel getCurrentSong() {
        return currentSong;
    }


    /**
     * show confirm
     */
    public void showConfirmOrNavigateToSelectSong(final BaseSlidingFragmentActivity activity,
                                                  int type,
                                                  String toJid,
                                                  String toName,
                                                  final OnConfirmMusic callBack) {
        String myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (myJidNumber.equals(toJid)) {
            activity.showToast(R.string.invite_not_send_me);
            return;
        }
        if (!isExistListenMusic()) {
            callBack.onGotoSelect();
        } else if (isExistListenerRoom()) {
            clearSessionAndNotifyMusic();
            callBack.onGotoSelect();
        } else if (isExistListenerGroup()) {
            if (type == Constants.TYPE_MUSIC.TYPE_GROUP_CHAT && currentGroupId.equals(toJid)) {
                callBack.onGotoChange();
            } else {
                clearSessionAndNotifyMusic();
                callBack.onGotoSelect();
            }
        } else {// nghe 1-1, nguoi la
            String currentFriend = getCurrentNumberFriend();
            if (!TextUtils.isEmpty(currentFriend) && currentFriend.equals(toJid)) {
                if (isOnRoom()) {
                    callBack.onGotoChange();
                } else {
                    activity.showToast(String.format(mRes.getString(R.string.already_waiting_accept_music),
                            mApplication.getMessageBusiness().getFriendName(currentFriend)), Toast.LENGTH_LONG);
                }
            } else if (!TextUtils.isEmpty(currentFriend) || isWaitingStrangerMusic()) {
                String msg;
                if (!TextUtils.isEmpty(currentFriend)) {
                    String friendName = mApplication.getMessageBusiness().getFriendName(currentFriend);
                    if (type == Constants.TYPE_MUSIC.TYPE_ROOM_CHAT) {
                        msg = String.format(mRes.getString(R.string.confirm_music_solo_in_room_playlist), friendName,
                                toName);
                    } else if (type == Constants.TYPE_MUSIC.TYPE_STRANGER) {
                        msg = String.format(mRes.getString(R.string.stranger_music_warning_create_when_already_join),
                                friendName);
                    } else {
                        msg = String.format(mRes.getString(R.string.warning_accept_when_already_join), friendName,
                                toName);
                    }
                } else {
                    if (type == Constants.TYPE_MUSIC.TYPE_ROOM_CHAT) {
                        msg = String.format(mRes.getString(R.string.confirm_music_stranger_in_room_playlist), toName);
                    } else {
                        msg = String.format(mRes.getString(R.string.confirm_music_stranger_in_room), toName);
                    }
                }
                PopupHelper.getInstance().showDialogConfirm(activity,
                        "", msg, mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                        new ClickListener.IconListener() {
                            @Override
                            public void onIconClickListener(View view, Object entry, int menuId) {
                                if (menuId == 1) {
                                    if (isWaitingStrangerMusic()) {
                                        activity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                                        cancelRoomStrangerRoom(getCurrentMusicSessionId(),
                                                new onCancelStrangeRoomListener() {
                                                    @Override
                                                    public void onResponse() {
                                                        activity.hideLoadingDialog();
                                                        resetSessionMusic();
                                                        callBack.onGotoSelect();
                                                    }

                                                    @Override
                                                    public void onError(int errorCode) {
                                                        activity.hideLoadingDialog();
                                                        activity.showToast(getMessageErrorByErrorCode(errorCode, null, false),
                                                                Toast.LENGTH_LONG);
                                                    }
                                                }, false);
                                    } else {
                                        onLeaveMusic(true);
                                        callBack.onGotoSelect();
                                    }
                                }
                            }
                        },
                        -1, 1);
            } else {
                clearSessionAndNotifyMusic();
                callBack.onGotoSelect();
            }
        }
    }

    public void showDialogCancelStrangerRoom(final BaseSlidingFragmentActivity activity, final String sessionId,
                                             String msg, final boolean isConfide, final OnCancelStrangerRoom callBack) {
        DialogConfirm dialog = new DialogConfirm(activity, true)
                .setLabel(null)
                .setMessage(msg)
                .setNegativeLabel(mRes.getString(R.string.cancel))
                .setPositiveLabel(mRes.getString(R.string.ok))
                .setPositiveListener(new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        activity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                        cancelRoomStrangerRoom(sessionId,
                                new MusicBusiness.onCancelStrangeRoomListener() {
                                    @Override
                                    public void onResponse() {
                                        activity.hideLoadingDialog();
                                        if (callBack != null)
                                            callBack.onResponse();
                                    }

                                    @Override
                                    public void onError(int errorCode) {
                                        activity.hideLoadingDialog();
                                        activity.showToast(getMessageErrorByErrorCode(errorCode, null, isConfide), Toast.LENGTH_LONG);
                                    }
                                }, isConfide);
                    }
                });
        dialog.show();
    }

    public void clearStrangerHistory(final onCancelStrangeRoomListener listener, final boolean isNearYou) {
        if (!NetworkHelper.isConnectInternet(mApplication) || !mApplication.getReengAccountBusiness().isValidAccount()) {
            listener.onError(-1);
            return;
        }
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.DELETE_STRANGER_HISTORY);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                if (listener != null) {
                                    listener.onResponse();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onError(errorCode);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            if (listener != null) {
                                listener.onError(errorCode);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                if (listener != null) {
                    listener.onError(-1);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                int groupId;
                if (isNearYou) {
                    groupId = -1;
                } else {
                    groupId = getGroupFiler();
                }
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(account.getRegionCode()).
                        append(groupId).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put("posterMsisdn", account.getJidNumber());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, account.getRegionCode());
                params.put(STRANGER_MUSIC.GROUP_ID, String.valueOf(groupId));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    //////////////////////////// VIDEO///////////////////////////////
    public ArrayList<MediaModel> getListTopVideoDefault() {
        return listTopVideoDefault;
    }

    public void getListWatchVideo(final OnGetListWatchVideoListener listener, int page) {
        if (listTopVideoDefault == null || listTopVideoDefault.isEmpty()) {
            String s = mPref.getString(Constants.PREFERENCE.PREF_LIST_TOP_VIDEO, "");
            if (!TextUtils.isEmpty(s)) {
                listTopVideoDefault = parseJsonListVideo(s);
            }
        }
        if (!NetworkHelper.isConnectInternet(mApplication) || !mApplication.getReengAccountBusiness().isValidAccount()) {
            listener.onError(-1, "");
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_LIST_WATCH_VIDEO);
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_LIST_VIDEO_HOT);
        Log.i(TAG, "url getListWatchVideo: " + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                if (listener != null) {
                                    ArrayList<MediaModel> listVideo = parseJsonListVideo(response);
                                    if (!isOjectNull(listVideo)) {
                                        mPref.edit().putString(Constants.PREFERENCE.PREF_LIST_TOP_VIDEO, response)
                                                .apply();
                                        listTopVideoDefault = listVideo;
                                        listener.onResponse(listVideo);
                                    } else {
                                        if (!isOjectNull(listener)) {
                                            listener.onError(0, "");
                                        }
                                    }
                                }
                            } else {
                                if (listener != null) {
                                    listener.onError(errorCode, "");
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            if (listener != null) {
                                listener.onError(0, "");
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                if (listener != null) {
                    listener.onError(0, "");
                }
            }

        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_LIST_WATCH_VIDEO, false);
    }

    private boolean isOjectNull(Object obj) {
        return (obj == null);
    }

    private ArrayList<MediaModel> parseJsonListVideo(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.optJSONArray("lstVideo");
            ArrayList<MediaModel> listVideo = new ArrayList<>();
            if (jsonArray != null && jsonArray.length() > 0) {
                int lengthArray = jsonArray.length();
                for (int i = 0; i < lengthArray; i++) {
                    JSONObject objectVideo = jsonArray.getJSONObject(i);
                    MediaModel mediaModel = new MediaModel();
                    mediaModel.setType(MediaModel.TYPE_VIDEO);
                    mediaModel.setId(objectVideo.optString("id"));
                    mediaModel.setName(objectVideo.optString("name"));
//                    mediaModel.setSinger(objectVideo.optString("singer_name"));
                    mediaModel.setUrl(objectVideo.optString("link"));
//                    mediaModel.setMedia_url(objectVideo.optString("media_url"));
                    mediaModel.setImage(objectVideo.optString("image"));
                    listVideo.add(mediaModel);
                }
            }
            return listVideo;
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return null;
    }

    private ArrayList<MediaModel> parseJsonListVideoFromSearch(String response) {
        try {
            ArrayList<MediaModel> listVideo = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.optJSONArray(RESULT);
            if (jsonArray != null && jsonArray.length() > 0) {
                int lengthArray = jsonArray.length();
                for (int i = 0; i < lengthArray; i++) {
                    JSONObject objectVideo = jsonArray.getJSONObject(i);
                    MediaModel mediaModel = new MediaModel();
                    mediaModel.setType(MediaModel.TYPE_VIDEO);
                    mediaModel.setId(objectVideo.optString("id"));
                    mediaModel.setName(objectVideo.optString("name"));
                    mediaModel.setUrl(objectVideo.optString("link"));
                    mediaModel.setMedia_url(objectVideo.optString("original_path"));
                    mediaModel.setImage(objectVideo.optString("image_path"));
                    listVideo.add(mediaModel);
                }
                return listVideo;
            }

//            String grouped = jsonObject.optString("grouped");
//            if (!TextUtils.isEmpty(grouped)) {
//                JSONObject jsonGroup = new JSONObject(grouped);
//                String type = jsonGroup.optString("type");
//                if (!TextUtils.isEmpty(type)) {
//                    JSONObject jsonType = new JSONObject(type);
//                    JSONArray jsonArrayGroups = jsonType.optJSONArray("groups");
//                    if (jsonArrayGroups != null && jsonArrayGroups.length() > 0) {
//                        JSONObject jsonGroupFirst = jsonArrayGroups.getJSONObject(0);
//                        String doclist = jsonGroupFirst.optString("doclist");
//                        if (!TextUtils.isEmpty(doclist)) {
//                            JSONObject jsonDocList = new JSONObject(doclist);
//                            JSONArray jsonArray = jsonDocList.optJSONArray("docs");
//                            ArrayList<MediaModel> listVideo = new ArrayList<>();
//                            if (jsonArray != null && jsonArray.length() > 0) {
//                                int lengthArray = jsonArray.length();
//                                for (int i = 0; i < lengthArray; i++) {
//                                    JSONObject objectVideo = jsonArray.getJSONObject(i);
//                                    MediaModel mediaModel = new MediaModel();
//                                    mediaModel.setType(MediaModel.TYPE_VIDEO);
//                                    mediaModel.setId(objectVideo.optString("id"));
//                                    mediaModel.setName(objectVideo.optString("name"));
//                                    mediaModel.setUrl(objectVideo.optString("link"));
//                                    mediaModel.setImage(objectVideo.optString("image"));
//                                    listVideo.add(mediaModel);
//                                }
//                                return listVideo;
//                            }
//                        }
//                    }
//                }
//            }

        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return new ArrayList<>();
    }

    public void getDetailWatchVideo(final OnGetDetailWatchVideoListener listener, final MediaModel mediaModel) {
        if (!NetworkHelper.isConnectInternet(mApplication) || !mApplication.getReengAccountBusiness().isValidAccount()) {
            listener.onError(-1, "");
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_DETAIL_WATCH_VIDEO);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_DETAIL_VIDEO);
        url = String.format(url, mediaModel.getId(), HttpHelper.EncoderUrl(mApplication.getReengAccountBusiness()
                .getJidNumber()));
        Log.i(TAG, "url getDetailWatchVideo: " + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                if (listener != null && mediaModel != null) {
                                    mediaModel.setMedia_url(responseObject.optString("result"));
                                    if (!TextUtils.isEmpty(mediaModel.getMedia_url())) {
                                        listener.onResponse(mediaModel);
                                    } else {
                                        listener.onError(0, "");
                                    }
                                }
                            } else {
                                if (listener != null) {
                                    listener.onError(errorCode, "");
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            if (listener != null) {
                                listener.onError(0, "");
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                if (listener != null) {
                    listener.onError(0, "");
                }
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_DETAIL_WATCH_VIDEO, false);
    }

    public void getListSearchVideo(final OnGetListWatchVideoListener listener, String query) {
        if (!NetworkHelper.isConnectInternet(mApplication) || !mApplication.getReengAccountBusiness().isValidAccount()) {
            if (listener != null) {
                listener.onError(-1, "");
            }
            return;
        }
        final long startTime = System.currentTimeMillis();
        String timestamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String TAG_REQUEST = "TAG_SEARCH_VIDEO";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, VideoApi.SEARCH_VIDEO_V1);
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + query + 20 + 0
                + getReengAccountBusiness().getToken() + timestamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.SEARCH_VIDEO_V1)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("limit", "20")
                .putParameter("q", query)
                .putParameter("video_type", "")
                .putParameter("cateId", "")
                .putParameter("url", "")
                .putParameter("lastIdStr", "")
                .putParameter("source", "SEARCH")
                .putParameter("offset", "0")
                .putParameter("timestamp", timestamp)
                .putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage())
                .putParameter("countryCode", getReengAccountBusiness().getRegionCode())
                .putParameter("security", security)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter(Parameter.Http.NETWORK_TYPE, NetworkHelper.getNetworkSubType(application))
                .putParameter("vip", getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        Log.d(TAG, "response: " + data);
                        if (listener != null) {
                            try {
                                ArrayList<MediaModel> listVideo = parseJsonListVideoFromSearch(data);
                                listener.onResponse(listVideo);
                            } catch (Exception e) {
                                Log.e(TAG, "Exception", e);
                                listener.onError(0, "");
                            }
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (listener != null) {
                            listener.onError(0, "");
                        }
                    }
                }).execute();
//        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SEARCH_VIDEO);
//        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SEARCH_VIDEO);
//        url = String.format(url, HttpHelper.EncoderUrl(query));
//        Log.i(TAG, "url getListSearchVideo: " + url);
//        StringRequest request = new StringRequest(Request.Method.GET, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d(TAG, "response: " + response);
//                        if (listener != null) {
//                            try {
//                                ArrayList<MediaModel> listVideo = parseJsonListVideoFromSearch(response);
//                                listener.onResponse(listVideo);
//                            } catch (Exception e) {
//                                Log.e(TAG, "Exception", e);
//                                listener.onError(0, "");
//                            }
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "VolleyError", error);
//                if (listener != null) {
//                    listener.onError(0, "");
//                }
//            }
//        });
//        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SEARCH_VIDEO, false);
    }

    public void showConfirmCloseMusicAndPlayVideo(BaseSlidingFragmentActivity activity, PositiveListener<Object> listener, Object entry) {
        String msg = null;
        if (isExistListenMusic()) {
            String currentNumberInRoom = getCurrentNumberFriend();
            MessageBusiness mMessageBusiness = mApplication.getMessageBusiness();
            if (isWaitingStrangerMusic()) {
                msg = String.format(mRes.getString(R.string.warning_quit_music_and_play_onmedia),
                        mRes.getString(R.string.stranger));
            } else if (!TextUtils.isEmpty(currentNumberInRoom)) {
                msg = String.format(mRes.getString(R.string.warning_quit_music_and_play_onmedia),
                        mMessageBusiness.getFriendName(currentNumberInRoom));
            }
        }
        if (TextUtils.isEmpty(msg)) {
            listener.onPositive(entry);
        } else {
            new DialogConfirm(activity, true).setLabel(null).setMessage(msg).setEntry(entry).
                    setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok)).
                    setPositiveListener(listener).show();
        }
    }

    /**
     * ping pong music . chuyển từ alarm sang timer
     */
    private Timer mTimerPing, mTimerPong;
    private TimerTask mTimerTaskPing, mTimerTaskPong;

    private void stopTimerPing() {
        if (mTimerPing != null) {
            mTimerPing.cancel();
        }
    }

    private void stopTimerPong() {
        if (mTimerPong != null) {
            mTimerPong.cancel();
        }
    }

    private void startTimerPingMusic() {
        Log.d(TAG, "startTimerPingMusic");
        stopTimerPing();
        musicPingFailCount = 0;
        mTimerPing = new Timer();
        mTimerTaskPing = new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "TimerTaskPingMusic: ");
                onReceiveMusicPingAlarm();
            }
        };
        mTimerPing.schedule(mTimerTaskPing, 0, Constants.ALARM_MANAGER.MUSIC_PING_TIMER);
    }

    public void startTimerPongMusic() {
        Log.d(TAG, "startTimerPongMusic");
        stopTimerPong();
        musicPongFailCount = 0;
        mTimerPong = new Timer();
        mTimerTaskPong = new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "TimerTaskPongMusic: ");
                onReceiveMusicPongAlarm();
            }
        };
        mTimerPong.schedule(mTimerTaskPong, 0, Constants.ALARM_MANAGER.MUSIC_PING_TIMER);
    }

    public interface OnConfirmMusic {
        void onGotoSelect();

        void onGotoChange();
    }

    public interface OnActionMusicResponseListener {
        void onActionResponse(ReengMusicPacket packet);

        void onTimeOutRequest(ReengMusicPacket packet);
    }

    public interface OnLoadDataResponseListener {
        void onStarting();

        void onRefreshDataConfideResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerSticky> stickyStrangerMusics);

        void onRefreshDataMusicResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerSticky> stickyStrangerMusics);

        void onFilterDataError(int errorCode);

        void onNotifyNewRoomResponse(boolean isNew);

        void onLoadMoreConfideResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerMusic> dataLoadMore);

        void onLoadMoreMusicResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerMusic> dataLoadMore);

        void onReloadDataWhenEventMessage();// load lai du lieu khi nhan duoc response tu sv

        void onError(int errorCode);

        void onRefreshDataWhenReceiverPresence();
    }

    public interface onCancelStrangeRoomListener {
        void onResponse();

        void onError(int errorCode);
    }

    public interface onCreateStrangerRoomListener {
        void onResponse(ArrayList<StrangerMusic> strangerMusics, boolean isConfide);

        void onError(int errorCode);
    }

    public interface onAcceptStrangerRoomListener {
        void onResponse(ThreadMessage threadMessage, StrangerMusic strangerMusic, boolean isConfide);

        void onError(int errorCode, String posterName, boolean isConfider);
    }

    public interface onReInviteListener {
        void onResponse(ThreadMessage threadMessage);
    }

    public interface OnGetListWatchVideoListener {
        void onResponse(ArrayList<MediaModel> listVideo);

        void onError(int errorCode, String desc);
    }

    public interface OnGetDetailWatchVideoListener {
        void onResponse(MediaModel video);

        void onError(int errorCode, String desc);
    }

    public interface OnCancelStrangerRoom {
        void onResponse();
    }

    public boolean isPlayFromKeengMusic() {
        return isPlayFromKeengMusic;
    }

    public void setPlayFromKeengMusic(boolean playFromKeengMusic) {
        isPlayFromKeengMusic = playFromKeengMusic;
    }
}
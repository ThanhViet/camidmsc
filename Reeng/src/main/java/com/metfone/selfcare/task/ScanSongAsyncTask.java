package com.metfone.selfcare.task;

import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.LocalSongInfo;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tungt on 2/25/2016.
 */
public class ScanSongAsyncTask extends AsyncTask<Void, Integer, ArrayList<LocalSongInfo>> {
    private static final String TAG = ScanSongAsyncTask.class.getSimpleName();
    private String mp3Pattern = ".mp3";
    private int mTotalFileScanned = 0;

    private final String MEDIA_PATH = Environment.getExternalStorageDirectory().getPath() + "/";
    private final String ZING_MP3_PATH = MEDIA_PATH + "/Zing MP3/";
    private ArrayList<LocalSongInfo> mSongList;
    private ScanSongInterface mListener;
    private ApplicationController mApplication;

    public ScanSongAsyncTask(ScanSongInterface listener, ApplicationController application) {
        mSongList = new ArrayList<>();
        mListener = listener;
        this.mApplication = application;
    }

    @Override
    protected ArrayList<LocalSongInfo> doInBackground(Void... params) {
        publishProgress(0);
        return getPlayList();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (values[0] <= 0) {
            mListener.onScanStart();
        }
        mListener.onScanProgress(values[0]);
    }

    @Override
    protected void onPostExecute(ArrayList<LocalSongInfo> hashMaps) {
        super.onPostExecute(hashMaps);
        mListener.onScanFinish(mSongList);
    }

    /**
     * Function to read all mp3 files and store the details in
     * ArrayList
     */
    public ArrayList<LocalSongInfo> getPlayList() {
        Log.d(TAG, "getPlayList: " + MEDIA_PATH);
        long t = System.currentTimeMillis();

        List<String> listAudio = getAudioListFromDevice();
        if (listAudio != null && listAudio.size() > 0) {
            for (String audio : listAudio) {
                File file = new File(audio);
                if (!file.isHidden()) {//&& isFolderWithMusic(file.getPath())
                    addSongToList(file);
                }
            }
            if (ZING_MP3_PATH != null) {
                File home = new File(ZING_MP3_PATH);
                File[] listFiles = home.listFiles();
                if (listFiles != null && listFiles.length > 0) {
                    for (File file : listFiles) {
                        if (file.isDirectory() && !file.isHidden()) {//&& isFolderWithMusic(file.getPath())
                            scanDirectory(file);
                        } else {
                            addSongToList(file);
                        }
                    }
                }
            }
        } else {
            if (MEDIA_PATH != null) {
                File home = new File(MEDIA_PATH);
                File[] listFiles = home.listFiles();
                if (listFiles != null && listFiles.length > 0) {
                    for (File file : listFiles) {
                        if (file.isDirectory() && !file.isHidden()) {//&& isFolderWithMusic(file.getPath())
                            scanDirectory(file);
                        } else {
                            addSongToList(file);
                        }
                    }
                }
            }
        }
        Log.d(TAG, "getPlayList take: " + (System.currentTimeMillis() - t));
        // return songs list array
        return mSongList;
    }

    private void scanDirectory(File directory) {
        if (directory != null) {
            File[] listFiles = directory.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file : listFiles) {
                    if (file.isDirectory() && !file.isHidden()) {
                        scanDirectory(file);
                    } else {
                        addSongToList(file);
                    }

                }
            }
        }
    }

    private boolean isFolderWithMusic(String directoryPath) {
        Log.d(TAG, "isFolderWithMusic:-directoryPath: " + directoryPath);
        long t = System.currentTimeMillis();
        Cursor cursor;
        String selection;
        String[] projection = {MediaStore.Audio.Media.IS_MUSIC};
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        //Create query for searching media files in folder
        selection = MediaStore.Audio.Media.DATA + " like " + "'%" + directoryPath + "/%'";
        cursor = mApplication.getContentResolver().query(uri, projection, selection, null, null);
        if (cursor != null) {
            boolean isDataPresent;
            isDataPresent = cursor.moveToFirst();
            cursor.close();
            return isDataPresent;
        }
        Log.d(TAG, "isFolderWithMusic take: " + (System.currentTimeMillis() - t));
        return false;
    }

    private void addSongToList(File song) {
        if (song.getName().endsWith(mp3Pattern) && song.length() > 0) {
            try {
                String name = song.getName().substring(0, (song.getName().length() - 4));
                String artist;
                MediaMetadataRetriever mmr;

                mmr = new MediaMetadataRetriever();
                mmr.setDataSource(song.getCanonicalPath());
                //String albumName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                //String albumArtis = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST);
                //String author = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR);
                artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                    /*String writer = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_WRITER);
                    String album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                    String date = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DATE);
                    String genre = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);*/
                String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);

//                    Log.d(TAG, "title: " + ((title!= null) ? title : ""));
//                    Log.d(TAG, "albumName: " + (albumName != null ? albumName : ""));
//                    Log.d(TAG, "albumArtis: " + (albumArtis!= null ? albumArtis : ""));
//                    Log.d(TAG, "author: " + (author!= null ? author : ""));
//                    Log.d(TAG, "artist: " + (artist!= null ? artist : ""));
//                    Log.d(TAG, "writer: " + (writer!= null ? writer : ""));
//                    Log.d(TAG, "album: " + (album!= null ? album : ""));
//                    Log.d(TAG, "date: " + (date!= null ? date : ""));
//                    Log.d(TAG, "genre: "  + (genre!= null ? genre : ""));

                name = TextUtils.isEmpty(title) ? name : title;
                artist = TextUtils.isEmpty(artist) ? "" : artist;

                LocalSongInfo songInfo = new LocalSongInfo(
                        name // song name
                        , "",
                        song.getCanonicalPath(),
                        artist,
                        song.length());

                // Adding each song to SongList
                mSongList.add(songInfo);
                mTotalFileScanned += 1;
                publishProgress(mTotalFileScanned);

            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public interface ScanSongInterface {
        void onScanFinish(ArrayList<LocalSongInfo> array);

        void onScanStart();

        void onScanProgress(int i);
    }

    public List<String> getAudioListFromDevice() {
        Cursor c = null;
        try {
            if (mApplication == null) return null;
            final List<String> tempAudioList = new ArrayList<>();
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String[] projection = {MediaStore.Audio.Media.DATA};
            StringBuilder selection = new StringBuilder();
            selection.append("is_music=1 AND ").append(MediaStore.Audio.Media.DATA).append(" LIKE ? ");
            StringBuilder sb = new StringBuilder();
            sb.append(MediaStore.Audio.Media.DATA).append(" COLLATE LOCALIZED ASC");
            c = mApplication.getContentResolver().query(uri, projection, selection.toString(), new String[]{"%.mp3"}, sb.toString());
            if (c != null && !c.isClosed()) {
                while (c.moveToNext()) {
                    String path = c.getString(c.getColumnIndex(MediaStore.Audio.Media.DATA));
                    if (!TextUtils.isEmpty(path)) {
                        tempAudioList.add(path);
                    }
                }
            }
            return tempAudioList;
        } catch (Exception e) {
            Log.e(TAG, "getAudioListFromDevice happen an exception: ", e);
        } finally {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        }
        return null;
    }
}
package com.metfone.selfcare.task;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.sticker.StickerHelper;
import com.metfone.selfcare.util.Log;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by thaodv on 9/16/2015.
 */
public class DownloadStickerAsynctask extends AsyncTask<Void, Integer, Boolean> {
    private static final String TAG = DownloadStickerAsynctask.class.getSimpleName();
    private StickerCollection mStickerCollection;
    private DownloadStickerCallBack mCallback;
    private boolean isUpdate = false;
    private static final int READ_SIZE = 1024 * 32;
    private ApplicationController mApplicationController;
    private boolean isDownloadCancel = false;

    public DownloadStickerAsynctask(ApplicationController app, StickerCollection stickerCollection,
                                    boolean isUpdate, DownloadStickerCallBack callBack) {
        this.mCallback = callBack;
        this.mApplicationController = app;
        this.mStickerCollection = stickerCollection;
        this.isUpdate = isUpdate;
    }

    @Override
    protected void onPreExecute() {
        mCallback.onPreExecute(mStickerCollection);
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... stickerCollections) {
        if (mStickerCollection == null) {
            return false;
        }
        mStickerCollection.setIsDownloading(true);
        String url = StickerHelper.getUrlDownloadStickerPacket(mStickerCollection, mApplicationController);
        String zipFilePath = StickerHelper.createStickerZipFilePath(mStickerCollection);
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(zipFilePath)) {
            return false;
        }
        Log.d(TAG, "url: " + url + " filePath: " + zipFilePath);
        boolean result = false;
        // Defines a handle for the byte download stream
        InputStream byteStream = null;
        HttpURLConnection httpConn = null;
        try {
            // Opens an HTTP connection to the image's URL
            URL httpUrl = new URL(url);
            httpConn = (HttpURLConnection) httpUrl.openConnection();
            //  httpConn.setRequestProperty("Accept-Encoding", "gzip");
            httpConn.setConnectTimeout(Constants.HTTP.CONNECTION_TIMEOUT);
            httpConn.connect();
            // Before continuing, checks to see that the Thread
            // Gets the input stream
            int sizeOfFile = httpConn.getContentLength();
            byteStream = httpConn.getInputStream();
            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(byteStream,
                    READ_SIZE);
            OutputStream output = new FileOutputStream(zipFilePath);
            byte data[] = new byte[READ_SIZE];
            int sizeDownloaded = 0;
            int count;
            if (sizeOfFile <= 0) {
                publishProgress(-1);
            } else {
                publishProgress(0);
            }
            int progress = 0;
            while ((count = input.read(data)) != -1 && !isDownloadCancel) {
                sizeDownloaded += count;
                // writing data to file
                output.write(data, 0, count);
                int temp = (sizeDownloaded * 100) / sizeOfFile;
                // thay doi 3% moi cap nhat progress
                if (temp >= progress + 3) {
                    progress = temp;
                    publishProgress(progress);
                }
            }
            if (sizeOfFile > 0) {
                publishProgress(100);
            }
            // flushing output
            output.flush();
            // closing streams
            output.close();
            input.close();
            if (sizeDownloaded > 0) {
                // neu dung luong file download khop voi dau vao
                Log.i(TAG, "sizeOfFile: " + sizeOfFile + " sizeDownloaded: " + sizeDownloaded);
                // extra zip
                StickerHelper.extractZipFile(mApplicationController, mStickerCollection.getServerId(),
                        zipFilePath, StickerHelper.createSubFileSticker(mStickerCollection).getPath());
                result = true;
            } else {
                result = false;
            }
        } catch (MalformedURLException ex) {
            Log.e(TAG, "MalformedURLException", ex);
            result = false;
        }// If an IO error occurs, returns immediately
        catch (IOException e) {
            Log.e(TAG, "IOException", e);
            result = false;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            result = false;
        } finally {
            if (null != byteStream) {
                try {
                    byteStream.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            if (httpConn != null) {
                Log.i(TAG, " httpConn disconnected");
                httpConn.disconnect();
            }
        }
        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Log.d(TAG, "onProgressUpdate: " + values[0]);
        int progress = values[0];
        mCallback.onProgressUpdate(progress);
        super.onProgressUpdate(progress);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (result) {
            // update database
            mStickerCollection.setDownloaded(true);
            // download moi hoac khong phai bo sticky thi moi cap nhat truong nay
            if (!isUpdate && mStickerCollection.isSticky() != 1) {
                mStickerCollection.setLastSticky(TimeHelper.getCurrentTime());
                if (mStickerCollection.isDefault() != 1) {
                    mApplicationController.getStickerBusiness().addCollectionId(mStickerCollection.getServerId());
                }
            }
            mStickerCollection.setCollectionState(StickerConstant.COLLECTION_STATE_ENABLE);
            mStickerCollection.setLastLocalUpdate(mStickerCollection.getLastServerUpdate() + 1);
            mStickerCollection.setIsNew(false);
            mApplicationController.getStickerBusiness().updateStickerCollection(mStickerCollection);
        } else {
            mStickerCollection.setDownloaded(false);
            mStickerCollection.setIsNew(true);
        }
        mCallback.onPostExecute(result, mStickerCollection);
        mStickerCollection.setIsDownloading(false);
        super.onPostExecute(result);
    }

    public interface DownloadStickerCallBack {
        void onPreExecute(StickerCollection stickerCollection);

        void onPostExecute(Boolean result, StickerCollection stickerCollection);

        void onProgressUpdate(int progress);
    }
}
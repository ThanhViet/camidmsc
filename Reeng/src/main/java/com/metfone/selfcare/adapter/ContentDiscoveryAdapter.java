package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.ContentDiscovery;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 7/7/2016.
 */
public class ContentDiscoveryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ContentDiscoveryAdapter.class.getSimpleName();

    private ArrayList<ContentDiscovery> listData = new ArrayList<>();
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private RecyclerClickListener mRecyclerClickListener;
    int width, height;


    public ContentDiscoveryAdapter(ApplicationController app, ArrayList<ContentDiscovery> list, RecyclerClickListener
            mRecyclerClickListener) {
        this.mApplication = app;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = list;
        this.mRecyclerClickListener = mRecyclerClickListener;

        width = mApplication.getWidthPixels();
        height = mApplication.getHeightPixels();
        Log.i(TAG, "width: " + width + " height: " + height);
        float ratio = (float) width / (float) height;
        if (width > 600) {
            width = 600;
            height = Math.round(width / ratio);
            Log.i(TAG, "----width: " + width + " height: " + height);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        DiscoveryViewHolder holder;
        View view = mLayoutInflater.inflate(R.layout.holder_content_discovery, parent, false);
        holder = new DiscoveryViewHolder(view);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DiscoveryViewHolder officerHolder = (DiscoveryViewHolder) holder;
        officerHolder.setElement(listData.get(position));
        officerHolder.setViewClick(position, officerHolder);
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public void setListData(ArrayList<ContentDiscovery> mListData) {
        listData = mListData;
    }

    public ArrayList<ContentDiscovery> getListData() {
        return listData;
    }

    private class DiscoveryViewHolder extends BaseViewHolder {

        private AspectImageView mImgContent;
        private TextView mTvwTitle;

        public DiscoveryViewHolder(View itemView) {
            super(itemView);
            mImgContent = (AspectImageView) itemView.findViewById(R.id.img_thumb_content_discovery);
            mTvwTitle = (TextView) itemView.findViewById(R.id.tvw_title_content_discovery);
        }

        @Override
        public void setElement(Object obj) {
            final ContentDiscovery content = (ContentDiscovery) obj;
            mTvwTitle.setText(content.getTitle());

//            ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgContent, width,
//                    height);
            ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgContent, content.getImageUrl(), width, height);
            ViewCompat.setTransitionName(mImgContent, content.getImageUrl());
        }
    }

}

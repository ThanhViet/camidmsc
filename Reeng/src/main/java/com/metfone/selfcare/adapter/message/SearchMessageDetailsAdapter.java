package com.metfone.selfcare.adapter.message;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.ThreadDetailAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.message.MessageConstants;
import com.metfone.selfcare.holder.message.BaseMessageHolder;
import com.metfone.selfcare.holder.message.ReceivedCallHolder;
import com.metfone.selfcare.holder.message.ReceivedDeepLinkHolder;
import com.metfone.selfcare.holder.message.ReceivedImageHolder;
import com.metfone.selfcare.holder.message.ReceivedTextHolder;
import com.metfone.selfcare.holder.message.SearchMessageNotificationHolder;
import com.metfone.selfcare.holder.message.SendCallHolder;
import com.metfone.selfcare.holder.message.SendImageHolder;
import com.metfone.selfcare.holder.message.SendTextHolder;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.ui.roundview.RoundRelativeLayout;
import com.metfone.selfcare.util.Log;

/**
 * Created by huongnd38 on 17/10/2018.
 */

public class SearchMessageDetailsAdapter extends ThreadDetailAdapter {
    public static final String TAG = SearchMessageDetailsAdapter.class.getSimpleName();

    private int mResultPostion = -1;

    public SearchMessageDetailsAdapter(BaseSlidingFragmentActivity context, int threadType,
                                       SmartTextClickListener smartTextListener, MessageInteractionListener listener,
                                       TagMocha.OnClickTag onClickTag, int index) {
        super(context, threadType, smartTextListener, listener, onClickTag);
        mResultPostion = index;
    }

    @Override
    public int getItemViewType(int position) {
        ReengMessage message = (ReengMessage) getItem(position);
        ReengMessageConstant.MessageType messageType = message.getMessageType();
        ReengMessageConstant.Direction messageDirection = message.getDirection();
        if (messageType == ReengMessageConstant.MessageType.text) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_TEXT;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_TEXT;
            }
        } else if (messageType == ReengMessageConstant.MessageType.file) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.image) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_IMAGE;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_IMAGE;
            }
        } else if (messageType == ReengMessageConstant.MessageType.voicemail) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.shareContact) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.shareVideo) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.voiceSticker) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.notification ||
                messageType == ReengMessageConstant.MessageType.notification_fake_mo ||
                messageType == ReengMessageConstant.MessageType.poll_action) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.inviteShareMusic) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.actionShareMusic) {// chuyen bai, doi bai
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.suggestShareMusic) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.greeting_voicesticker) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.shareLocation) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.restore) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.transferMoney) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.event_follow_room ||
                messageType == ReengMessageConstant.MessageType.fake_mo ||
                messageType == ReengMessageConstant.MessageType.poll_create) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.crbt_gift) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.deep_link) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_DEEP_LINK;
        } else if (messageType == ReengMessageConstant.MessageType.gift) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.image_link) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.advertise) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.call ||
                messageType == ReengMessageConstant.MessageType.talk_stranger) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_CALL;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_CALL;
            }
        } else if (messageType == ReengMessageConstant.MessageType.watch_video) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.bank_plus) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.lixi) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.message_banner) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.pin_message) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.suggest_voice_sticker) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.update_app) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_DEEP_LINK;
        }
        return MessageConstants.TYPE_MESSAGE_NOTIFY;// tra ve default la notify
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        long t = System.currentTimeMillis();
        int type = getItemViewType(position);
        ReengMessage message = messages.get(position);
        BaseMessageHolder holder;
        switch (type) {
            case MessageConstants.TYPE_MESSAGE_SEND_TEXT:
                if (convertView == null) {
                    holder = new SendTextHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendTextHolder) convertView.getTag();
                }
                holder.setOnClickTag(onClickTag);
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_TEXT:
                if (convertView == null) {
                    holder = new ReceivedTextHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedTextHolder) convertView.getTag();
                }
                holder.setOnClickTag(onClickTag);
                break;

            case MessageConstants.TYPE_MESSAGE_SEND_IMAGE:
                if (convertView == null) {
                    holder = new SendImageHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendImageHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_IMAGE:
                if (convertView == null) {
                    holder = new ReceivedImageHolder(applicationController, mSmartTextListener, mThreadMessage);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedImageHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_NOTIFY:
                if (convertView == null) {
                    holder = new SearchMessageNotificationHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SearchMessageNotificationHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_RECEIVED_DEEP_LINK:
                if (convertView == null) {
                    holder = new ReceivedDeepLinkHolder(applicationController, mMessageInteractionListener,
                            mSmartTextListener);
                    ((ReceivedDeepLinkHolder) holder).setDisableButton(true);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedDeepLinkHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_SEND_CALL:
                if (convertView == null) {
                    holder = new SendCallHolder(applicationController, true);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendCallHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_CALL:
                if (convertView == null) {
                    holder = new ReceivedCallHolder(applicationController);
                    ((ReceivedCallHolder) holder).setDisableCallButton(true);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedCallHolder) convertView.getTag();
                }
                break;

            default:// dua het ve tin notify
                throw new IllegalArgumentException("" + type);

        }
        holder.setThreadType(mThreadType);
        holder.setMessageInteractionListener(mMessageInteractionListener);
        holder.setShowInfo(isShowInfoMsg);
        // +++++++++++++++call-before-set-element++++++++++++++++++++++++++++++++++++++++
        //---------------------------------------check same date
        if (position <= 0) {
            // neu la phan tu dau tien
            holder.setSameDateWithPrevious(false);
            holder.setDifferSenderWithPrevious(true);
            if (message.isNewMessage()) {
                holder.setFirstNewMessage(true);
            } else {
                holder.setFirstNewMessage(false);
            }
        } else {
            // kiem tra neu tin nay voi tin truoc do cach nhau 10p time_divide_thread_section
            // truong hop may nhan dc gui
            ReengMessage previousMessage = messages.get(position - 1);
            if ((message.getTime() - previousMessage.getTime()) > -TIME_DIVIDE_THREAD_SECTION &&
                    (message.getTime() - previousMessage.getTime()) < TIME_DIVIDE_THREAD_SECTION) {
                holder.setSameDateWithPrevious(true);
            } else {
                holder.setSameDateWithPrevious(false);
            }
            // message trc la notification, messag sau la thuong, thi hien avatar
            if (containsNotification(previousMessage.getMessageType()) &&
                    !containsNotification(message.getMessageType())) {
                holder.setDifferSenderWithPrevious(true);
            } else if (message.getSender().equals(previousMessage.getSender()) && holder.isSameDateWithPrevious()) {
                if (TextUtils.isEmpty(message.getSender())) {// admin room chat
                    if (message.getSenderName() != null && !message.getSenderName().equals(previousMessage
                            .getSenderName())) {
                        // neu sender name khac nhau thi tach row
                        holder.setDifferSenderWithPrevious(true);
                    } else {
                        holder.setDifferSenderWithPrevious(false);
                    }
                } else {
                    // kiem tra neu tin nay voi tin truoc cungf 1 nguoi gui
                    holder.setDifferSenderWithPrevious(false);
                }
            } else {
                holder.setDifferSenderWithPrevious(true);
            }
            if (!previousMessage.isNewMessage() && message.isNewMessage()) {
                holder.setFirstNewMessage(true);
            } else {
                holder.setFirstNewMessage(false);
            }
        }
        //-----------------------------------------
        if (position == (messages.size() - 1)) {
            // neu la phan tu cuoi cung
            holder.setDifferSenderWithAfter(true);
        } else {
            // kiem tra neu tin nay voi tin sau do la cung 1 nguoi gui
            if (message.getSender().equals(
                    messages.get(position + 1).getSender())) {
                holder.setDifferSenderWithAfter(false);
            } else {
                holder.setDifferSenderWithAfter(true);
            }
        }
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // initDownloadOrUpload(message, holder.getConvertView());
        holder.setElemnts(message);
        Log.d(TAG, "[perform] - threadDetailAdapter getView (type: " + type + ") take: "
                + (System.currentTimeMillis() - t));
        View view = holder.getConvertView();
        if (view != null) {
            View mIbnRetry = view.findViewById(R.id.message_retry);
            View mIbnReply = view.findViewById(R.id.message_reply);
            if (mIbnReply != null) {
                mIbnReply.setVisibility(View.GONE);
            }

            if (mIbnRetry != null) {
                mIbnRetry.setVisibility(View.GONE);
            }

            View messageLayout = view.findViewById(R.id.message_received_border_bgr);
            TextView txtMessage = view.findViewById(R.id.message_text_content);

            if (messageLayout != null) {
                if (messageLayout instanceof RoundRelativeLayout) {
                    txtMessage.setTextColor(applicationController.getResources().getColor(position == mResultPostion ? R.color.white : R.color.text_password));
                    ((RoundRelativeLayout) messageLayout).changeBackgroundColor(applicationController.getResources().getColor(position == mResultPostion ? R.color.setting_chat_text_color_red : R.color.bg_color_bubble_received));
                } else if (messageLayout instanceof RoundLinearLayout) {
                    txtMessage.setTextColor(applicationController.getResources().getColor(position == mResultPostion ? R.color.white : R.color.text_password));
                    ((RoundLinearLayout) messageLayout).changeBackgroundColor(applicationController.getResources().getColor(position == mResultPostion ? R.color.setting_chat_text_color_red : R.color.bg_color_bubble_received));
                }
            }

            View reactionLayout = view.findViewById(R.id.layout_reaction_parent);
            if (reactionLayout != null) {
                reactionLayout.setVisibility(View.GONE);
            }
        }

        return view;
    }

    public static boolean containsNotification(ReengMessageConstant.MessageType type) {
        return type != ReengMessageConstant.MessageType.text && type != ReengMessageConstant.MessageType.image && type != ReengMessageConstant.MessageType.call && type != ReengMessageConstant.MessageType.talk_stranger && type != ReengMessageConstant.MessageType.update_app;
    }

    public void setResultPostion(int postion) {
        mResultPostion = postion;
    }
}

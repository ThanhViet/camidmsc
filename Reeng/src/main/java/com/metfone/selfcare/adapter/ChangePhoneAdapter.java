package com.metfone.selfcare.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.account.UserInfo;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePhoneAdapter extends
    BaseAdapter<ChangePhoneAdapter.ChangePhoneViewHolder, ServicesModel> {
    UserInfoBusiness userInfoBusiness;
    private int lastSelectedPosition = -1;

    public ChangePhoneAdapter(Activity activity) {
        super(activity);
        userInfoBusiness = new UserInfoBusiness(activity);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @NonNull
    @Override
    public ChangePhoneAdapter.ChangePhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChangePhoneAdapter.ChangePhoneViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ChangePhoneAdapter.ChangePhoneViewHolder holder, int position) {
        holder.bindView(getItem(position), position);
    }

    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }

    public void setLastSelectedPosition(int lastSelectedPosition) {
        this.lastSelectedPosition = lastSelectedPosition;
        notifyDataSetChanged();
    }

    public class ChangePhoneViewHolder extends BaseViewHolder {
        @BindView(R.id.tvPhone)
        AppCompatTextView tvPhone;
        @BindView(R.id.rbSelect)
        ImageView rbSelect;
        @BindView(R.id.clContent)
        ConstraintLayout clContent;


        public ChangePhoneViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public ChangePhoneViewHolder(ViewGroup parent) {
            this(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_change_phone, parent, false));
        }

        public void bindView(ServicesModel model, int pos) {
            if (TextUtils.isEmpty(model.getPhone_number())) {
                clContent.setVisibility(View.GONE);
            }
            if(model.getPhone_number() != null){
                tvPhone.setText("0"+model.getPhone_number().substring(0,2)+" "+model.getPhone_number().substring(2));
            }
            if (lastSelectedPosition == pos) {
                rbSelect.setImageResource(R.drawable.icon_radio_red);
            } else {
                rbSelect.setImageResource(R.drawable.icon_radio_white);
            }

            clContent.setOnClickListener(v -> {
//                notifyItemChanged(lastSelectedPosition);
                lastSelectedPosition = getAdapterPosition();
                notifyDataSetChanged();
            });
        }

        @Override
        public void setElement(Object obj) {

        }
    }
}

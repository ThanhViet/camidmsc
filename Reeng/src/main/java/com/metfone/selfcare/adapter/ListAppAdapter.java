package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.MoreAppInteractHelper;
import com.metfone.selfcare.holder.AppInfo;
import com.metfone.selfcare.holder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tungt on 7/27/2015.
 */
public class ListAppAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ApplicationController mApp;
    private BaseSlidingFragmentActivity mActivity;
    private List<AppInfo> mListAppInfo = new ArrayList<>();
    private LayoutInflater inflater;

    public ListAppAdapter(ArrayList<AppInfo> mList, BaseSlidingFragmentActivity activity) {
        mActivity = activity;
        mApp = (ApplicationController) activity.getApplication();
        mListAppInfo = mList;
        inflater = LayoutInflater.from(this.mApp);

//        inflater = LayoutInflater.from(this.mApp);
    }

    public void setDataList(List<AppInfo> dataList) {
        this.mListAppInfo = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListAppViewHolder holder;
        View view = inflater.inflate(R.layout.holder_app_view, parent, false);
        holder = new ListAppViewHolder(mApp, view);
        /*if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);*/
        return holder;
//        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListAppViewHolder officerHolder = (ListAppViewHolder) holder;
        officerHolder.setElement(mListAppInfo.get(position));
    }

    @Override
    public int getItemCount() {
        return mListAppInfo.size();
    }

    private class ListAppViewHolder extends BaseViewHolder {
        private AppInfo mEntry;
        private Context mContext;
        private TextView mTvwTitle, mTvwDes;
        private ImageView img_thumb;
        private LinearLayout mRoot;

        public ListAppViewHolder(Context context, View itemView) {
            super(itemView);
            this.mContext = context;
//            mImgGame = (CircleImageView) itemView.findViewById(R.id.img_game_thumb);
//            mTvwTitle = (TextView) itemView.findViewById(R.id.tvw_title_game);
//            mTvwDes = (TextView) itemView.findViewById(R.id.tvw_des_game);
//            mTvwPlay = (TextView) itemView.findViewById(R.id.tvw_play_game);

            img_thumb = (ImageView) itemView.findViewById(R.id.img_app_avatar);
            mTvwTitle = (TextView) itemView.findViewById(R.id.tv_app_name);
            mTvwDes = (TextView) itemView.findViewById(R.id.tv_app_desc);
            mRoot = (LinearLayout) itemView.findViewById(R.id.root);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (AppInfo) obj;
//            mApplication.getAvatarBusiness().setLogoOtherApp(holder.img_thumb, item.getUrlIcon());
            mTvwTitle.setText(mEntry.getName());
            if (TextUtils.isEmpty(mEntry.getDesc())) {
                mTvwDes.setVisibility(View.GONE);
            } else {
                mTvwDes.setVisibility(View.VISIBLE);
                mTvwDes.setText(mEntry.getDesc());
            }
            ApplicationController application = (ApplicationController) mContext.getApplicationContext();
//            int size = (int) application.getResources().getDimension(R.dimen.size_image_intro_group);
//            mTvwTitle.setText(mEntry.getTitleGame());
//            mTvwDes.setText(mEntry.getDesGame());
            if (TextUtils.isEmpty(mEntry.getUrlIcon())) {
//                mImgGame.setImageResource(R.drawable.ic_accumulate_default);
//                application.getUniversalImageLoader().cancelDisplayTask(img_thumb);
                Glide.with(application).clear(img_thumb);
            } else {
//                application.getUniversalImageLoader().displayImage(mEntry.getUrlIcon(), new ImageViewAwareTargetSize(img_thumb));
                Glide.with(application).load(mEntry.getUrlIcon()).into(img_thumb);
            }
            mRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String appPackageName = mEntry.getPacketName();
                    MoreAppInteractHelper.openMoreApp(mActivity, appPackageName);
                }
            });
//            mTvwPlay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String schemaLink = mEntry.getLink();
//                    DeepLinkHelper.getInstance().openSchemaLink(mActivity, schemaLink);
//                }
//            });
//
//            mApp.getAvatarBusiness().setOfficialThreadAvatar(mImgGame, mEntry.getThumb(), 0);
        }
    }
}
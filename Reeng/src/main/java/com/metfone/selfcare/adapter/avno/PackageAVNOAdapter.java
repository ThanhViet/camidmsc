package com.metfone.selfcare.adapter.avno;

import android.content.Context;
import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 4/12/2018.
 */

public class PackageAVNOAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final String TAG = PackageAVNOAdapter.class.getSimpleName();

    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private ArrayList<PackageAVNO> listItem;
    private Resources mRes;
    private AVNOHelper.OnClickActionPackage listener;


    public PackageAVNOAdapter(ApplicationController application, ArrayList<PackageAVNO> listItem,
                              AVNOHelper.OnClickActionPackage listener) {
        this.mApplication = application;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listItem = listItem;
        this.mRes = mApplication.getResources();
        this.listener = listener;
    }

    public void setListItem(ArrayList<PackageAVNO> listItem) {
        this.listItem = listItem;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder;
        if (viewType == PackageAVNO.PACKAGE_TITLE ||
                viewType == PackageAVNO.PACKAGE_TITLE_SMS ||
                viewType == PackageAVNO.PACKAGE_TITLE_CALL) {
            View view = mLayoutInflater.inflate(R.layout.holder_avno_package_title, parent, false);
            holder = new PackageTitleHolder(view);
        } else if (viewType == PackageAVNO.PACKAGE_CHARGE_CALL || viewType == PackageAVNO.PACKAGE_CHARGE_SMS) {
            View view = mLayoutInflater.inflate(R.layout.holder_avno_price, parent, false);
            holder = new PackagePriceHolder(view);
        } else {
            View view = mLayoutInflater.inflate(R.layout.holder_avno_package, parent, false);
            holder = new PackageAVNOHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BaseViewHolder avno = (BaseViewHolder) holder;
        avno.setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }

    @Override
    public int getItemViewType(int position) {
        return listItem.get(position).getStatus();
    }

    public ArrayList<PackageAVNO> getListItem() {
        return listItem;
    }


    class PackageTitleHolder extends BaseViewHolder {

        private TextView mTvwTitle;
        private ImageView mImgCall;
        private ImageView mImgSms;

        public PackageTitleHolder(View itemView) {
            super(itemView);
            mTvwTitle = itemView.findViewById(R.id.tvw_title_pkg);
            mImgCall = itemView.findViewById(R.id.img_thumb_title_call);
            mImgSms = itemView.findViewById(R.id.img_thumb_title_sms);
        }

        @Override
        public void setElement(Object obj) {
            PackageAVNO packageAVNO = (PackageAVNO) obj;
            if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_TITLE) {
                mTvwTitle.setText(TextHelper.fromHtml(packageAVNO.getTitle()));
                mImgCall.setVisibility(View.GONE);
                mImgSms.setVisibility(View.GONE);
            } else if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_TITLE_CALL) {
                mImgCall.setVisibility(View.VISIBLE);
                mImgSms.setVisibility(View.GONE);
                mTvwTitle.setText(packageAVNO.getTitle());
            } else if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_TITLE_SMS) {
                mImgCall.setVisibility(View.GONE);
                mImgSms.setVisibility(View.VISIBLE);
                mTvwTitle.setText(packageAVNO.getTitle());
            } else {
                mImgCall.setVisibility(View.GONE);
                mImgSms.setVisibility(View.GONE);
                mTvwTitle.setText(packageAVNO.getTitle());
            }
        }
    }

    class PackageAVNOHolder extends BaseViewHolder {

        private TextView mTvwDesc;
        private TextView mTvwTitle;
        private RoundTextView mTvwAction;
        private CircleImageView mImgPackage;
        private TextView mTvwRegisterFreeNumb;

        public PackageAVNOHolder(View itemView) {
            super(itemView);
            mTvwDesc = itemView.findViewById(R.id.tvw_package_desc);
            mTvwTitle = itemView.findViewById(R.id.tvw_package_title);
            mTvwAction = itemView.findViewById(R.id.tvw_package_action);
            mImgPackage = itemView.findViewById(R.id.img_package_thumb);
            mTvwRegisterFreeNumb = itemView.findViewById(R.id.tvw_register_free_num);
        }

        @Override
        public void setElement(Object obj) {
            final PackageAVNO packageAVNO = (PackageAVNO) obj;
            mTvwDesc.setText(packageAVNO.getDesc());
            mTvwTitle.setText(packageAVNO.getTitle());
            if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_ACTIVE) {
                mTvwAction.setText(mRes.getString(R.string.cancel));
                mTvwAction.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.gray),
                        ContextCompat.getColor(mApplication, R.color.gray_light));
                mTvwAction.setStroke(ContextCompat.getColor(mApplication, R.color.gray), 0);
                mTvwAction.setTextColor(ContextCompat.getColor(mApplication, R.color.white));
            } else {
                mTvwAction.setText(mRes.getString(R.string.register));
                mTvwAction.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.white),
                        ContextCompat.getColor(mApplication, R.color.bg_mocha_focus));
                mTvwAction.setStroke(ContextCompat.getColor(mApplication, R.color.bg_mocha), 1);
                mTvwAction.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
            }
            mTvwAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClickActionPackage(packageAVNO);
                    }
                }
            });
            Glide.with(mApplication).load(packageAVNO.getImgUrl()).into(mImgPackage);

            if (packageAVNO.getStateFreeNumber()==1) {
                mTvwRegisterFreeNumb.setVisibility(View.VISIBLE);
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void updateDrawState(TextPaint ds) {
//                ds.setColor(ds.linkColor);    // you can use custom color
                        ds.setUnderlineText(false);    // this remove the underline
                    }

                    @Override
                    public void onClick(View textView) {
                        if(listener!=null){
                            listener.onClickRegisterFreeNumber(packageAVNO);
                        }
                    }
                };

                String textRegister = mRes.getString(R.string.avno_select_free_number);
                SpannableString spannableString = new SpannableString(textRegister);
                spannableString.setSpan(clickableSpan, 0, textRegister.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mApplication, R.color.blue)),
                        0, textRegister.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                mTvwRegisterFreeNumb.setText(spannableString);
                mTvwRegisterFreeNumb.setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                mTvwRegisterFreeNumb.setVisibility(View.GONE);
            }

        }
    }


    class PackagePriceHolder extends BaseViewHolder {

        private TextView mTvwTitle;
        private TextView mTvwDesc;
        private TextView mTvwPriceCall;
        private TextView mTvwPriceSms;

        public PackagePriceHolder(View itemView) {
            super(itemView);
            mTvwTitle = itemView.findViewById(R.id.tvw_pkg_price_title);
            mTvwDesc = itemView.findViewById(R.id.tvw_pkg_price_desc);
            mTvwPriceCall = itemView.findViewById(R.id.tvw_price_call);
            mTvwPriceSms = itemView.findViewById(R.id.tvw_price_sms);
        }

        @Override
        public void setElement(Object obj) {
            PackageAVNO packageAVNO = (PackageAVNO) obj;
            if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_CHARGE_CALL) {
                mTvwTitle.setText(packageAVNO.getTitle());
                mTvwDesc.setText(packageAVNO.getDesc());
                mTvwPriceCall.setVisibility(View.VISIBLE);
                mTvwPriceCall.setText(packageAVNO.getPrice());
                mTvwPriceSms.setVisibility(View.GONE);
            } else {
                mTvwTitle.setText(packageAVNO.getTitle());
                mTvwDesc.setText(packageAVNO.getDesc());
                mTvwPriceSms.setVisibility(View.VISIBLE);
                mTvwPriceSms.setText(packageAVNO.getPrice());
                mTvwPriceCall.setVisibility(View.GONE);
            }
        }
    }
}

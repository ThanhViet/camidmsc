package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.holder.guestbook.PagePreviewHolder;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/24/2017.
 */
public class BookPreviewAdapter extends PagerAdapter {
    private static final String TAG = BookPreviewAdapter.class.getSimpleName();
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Page> mListPages;

    public BookPreviewAdapter(Context context, ArrayList<Page> pages) {
        this.mContext = context;
        this.mListPages = pages;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListImage(ArrayList<Page> pages) {
        this.mListPages = pages;
    }

    @Override
    public int getCount() {
        if (mListPages == null) return 0;
        return mListPages.size();
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        Log.d(TAG, "instantiateItem");
        PagePreviewHolder holder = new PagePreviewHolder(mContext);
        holder.initHolder(container, null, position, mLayoutInflater);
        holder.setElemnts(mListPages.get(position));
        View itemView = holder.getConvertView();
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}
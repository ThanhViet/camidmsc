package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.metfone.selfcare.fragment.game.AccumulatePagerFragment;
import com.metfone.selfcare.helper.AccumulatePointHelper;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 11/30/2016.
 */
public class AccumulateTabAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = AccumulateTabAdapter.class.getSimpleName();
    private ArrayList<String> mListTitle = new ArrayList<>();
    private Context mContext;

    public AccumulateTabAdapter(FragmentManager fragmentManager, Context context, ArrayList<String> titleIds) {
        super(fragmentManager);
        this.mListTitle = titleIds;
        this.mContext = context;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mListTitle.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return AccumulatePagerFragment.newInstance(AccumulatePointHelper.TAB_TASK);
        } else if (position == 1) {
            return AccumulatePagerFragment.newInstance(AccumulatePointHelper.TAB_EXCHANGE);
        } else {
            return AccumulatePagerFragment.newInstance(AccumulatePointHelper.TAB_HISTORY);
        }
    }

    @Override
    public int getCount() {
        if (mListTitle == null) return 0;
        return mListTitle.size();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    /*public View getTabView(int position) {
        View mView = LayoutInflater.from(mApplication).inflate(R.layout.item_home_pager, null);
        View layoutBadge = mView.findViewById(R.id.layout_notify_hot);
        TextView textNumber = (TextView) mView.findViewById(R.id.text_number_unread_msg);
        Log.i(TAG, "getCustomTabView: " + position);
        if (position == 0) {
            if (unreadMsg <= 0) {
                layoutBadge.setVisibility(View.GONE);
            } else {
                Log.i(TAG, "unreadMsg: " + unreadMsg);
                layoutBadge.setVisibility(View.VISIBLE);
                textNumber.setText(TextHelper.getTextNumberNotify(unreadMsg));
            }
        } else if (position == posTabHot) {
            isShowingHot = needShowAlertTabHot();
            if (isShowingHot) {
                layoutBadge.setVisibility(View.VISIBLE);
                textNumber.setText("N");
            } else {
                layoutBadge.setVisibility(View.GONE);
            }
        } else {
            layoutBadge.setVisibility(View.GONE);
        }
        return mView;
    }*/
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.fragment.setting.ListRoomUtilitiesFragment;
import com.metfone.selfcare.helper.httprequest.RoomChatRequestHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by tungt on 12/30/2015.
 */
public class MusicRoomTabsAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = MusicRoomTabsAdapter.class.getSimpleName();
    private ArrayList<String> mListTitle = new ArrayList<>();
    private Context mContext;
    private ArrayList<OfficerAccount> mListRoomTab;

    public MusicRoomTabsAdapter(FragmentManager fragmentManager,
                                Context context,
                                ArrayList<String> titleIds,
                                ArrayList<OfficerAccount> listRoom
    ) {
        super(fragmentManager);
        this.mListTitle = titleIds;
        this.mContext = context;
        this.mListRoomTab = listRoom;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mListTitle.get(position);
    }

    @Override
    public int getCount() {
        return mListTitle.size();
    }

    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem: " + position);
        if (position == 0) {
            return ListRoomUtilitiesFragment.newInstance(RoomChatRequestHelper.TAB_TOPIC);
        } else if (position == 1) {
            return ListRoomUtilitiesFragment.newInstance(RoomChatRequestHelper.TAB_STAR);
        } else {
            return ListRoomUtilitiesFragment.newInstance(RoomChatRequestHelper.TAB_LOCATION);
        }
    }

    public View getTabView(int position) {
        View tab = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        int width = ((ApplicationController) mContext.getApplicationContext()).getWidthPixels() / 3;
        tab.setLayoutParams(new ViewGroup.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT));
        TextView tv = (TextView) tab.findViewById(R.id.custom_text);
        tv.setText(getPageTitle(position));
        return tab;
    }
}

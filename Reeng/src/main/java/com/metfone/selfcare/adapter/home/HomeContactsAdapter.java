package com.metfone.selfcare.adapter.home;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MochaFriendAccountWrapper;
import com.metfone.selfcare.database.model.OfficerAccountWrapper;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.DividerHolder;
import com.metfone.selfcare.holder.contact.GroupViewHolder;
import com.metfone.selfcare.holder.contact.OfficialViewWrapperHolder;
import com.metfone.selfcare.holder.contact.PhoneViewHolder;
import com.metfone.selfcare.holder.contact.SectionViewHolder;
import com.metfone.selfcare.holder.contact.SocialViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.ItemInvisibleHolder;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;
import com.metfone.selfcare.model.Divider;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/1/2017.
 */

public class HomeContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = HomeContactsAdapter.class.getSimpleName();

    private static final int TYPE_EMPTY = -1;
    private static final int TYPE_NUMBER = 0;
    private static final int TYPE_SECTION = 1;
    private static final int TYPE_OFFICIAL = 2;
    private static final int TYPE_SOCIAL = 3;
    private static final int TYPE_GROUP = 4;
    private static final int TYPE_SUGGEST = 5;
    private static final int TYPE_DIVIDER = 6;
    private ApplicationController mApplication;
    private ArrayList<Object> listData;
    private LayoutInflater inflater;
    private ContactsHolderCallBack mListener;
    private boolean isHideOption;
    private boolean isHideCall;
    private boolean showSync = false;

    private boolean showCallAVNO = false;
    private int typeSocial;

    public HomeContactsAdapter(ApplicationController context, ArrayList<Object> list) {
        this.mApplication = context;
        this.listData = list;
        this.isHideOption = false;
        inflater = LayoutInflater.from(mApplication);
    }

    public HomeContactsAdapter(ApplicationController context, ArrayList<Object> list, boolean isHideOption) {
        this.mApplication = context;
        this.listData = list;
        this.isHideOption = isHideOption;
        inflater = LayoutInflater.from(mApplication);
    }

    public HomeContactsAdapter(ApplicationController context, ArrayList<Object> list, boolean isHideOption, boolean isHideCall) {
        this.mApplication = context;
        this.listData = list;
        this.isHideOption = isHideOption;
        this.isHideCall = isHideCall;
        inflater = LayoutInflater.from(mApplication);
    }

    public void setShowCallAVNO(boolean showCallAVNO) {
        this.showCallAVNO = showCallAVNO;
    }

    public void setShowSync(boolean showSync) {
//        this.showSync = showSync;
    }

    public void setClickListener(ContactsHolderCallBack listener) {
        this.mListener = listener;
    }

    public void setData(ArrayList<Object> threads) {
        if (listData == null) {
            listData = new ArrayList<>();
        } else {
            listData.clear();
        }
        listData.addAll(threads);
    }

    public void setTypeSocial(int typeSocial) {
        this.typeSocial = typeSocial;
    }

    @Override
    public int getItemCount() {
        if (listData == null) return 0;
        return listData.size();
    }

    public Object getItem(int position) {
        if (position >= listData.size()) return null;
        return listData.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        Object obj = getItem(position);
        if (obj == null) return TYPE_EMPTY;
        if (obj instanceof PhoneNumber) {
            PhoneNumber phone = (PhoneNumber) obj;
            if (phone.getContactId() == null) {
                return TYPE_SECTION;
            } else {
                return TYPE_NUMBER;
            }
        } else if (obj instanceof MochaFriendAccountWrapper) {
            return TYPE_OFFICIAL;
        } else if (obj instanceof OfficerAccountWrapper) {
            return TYPE_OFFICIAL;
        } else if (obj instanceof SocialFriendInfo) {
            return TYPE_SOCIAL;
        } else if (obj instanceof UserInfo) {
            return TYPE_SUGGEST;
        } else if (obj instanceof Divider) {
            return TYPE_DIVIDER;
        } else {
            return TYPE_GROUP;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        BaseViewHolder holder;
        if (type == TYPE_EMPTY) {
            View viewUnknown = inflater.inflate(R.layout.holder_feed_default, parent, false);
            holder = new ItemInvisibleHolder(viewUnknown, mApplication);
        } else if (type == TYPE_SECTION) {
            View view = inflater.inflate(R.layout.holder_contact_section, parent, false);
            holder = new SectionViewHolder(view, mApplication, mListener, showSync, "nameContact");
        } else if (type == TYPE_DIVIDER) {
            View view = inflater.inflate(R.layout.holder_divider, parent, false);
            holder = new DividerHolder(view, mApplication);
        } else if (type == TYPE_NUMBER) {
            View view = inflater.inflate(R.layout.holder_contact_new, parent, false);
            holder = new PhoneViewHolder(view, mApplication, mListener, isHideOption, isHideCall);
            ((PhoneViewHolder) holder).setShowCallAVNO(showCallAVNO);
        } else if (type == TYPE_OFFICIAL) {
            View view = inflater.inflate(R.layout.holder_official_wrapper, parent, false);
            holder = new OfficialViewWrapperHolder(view, mApplication, mListener);
        } else if (type == TYPE_SOCIAL) {
            View view = inflater.inflate(R.layout.holder_social_friend, parent, false);
            holder = new SocialViewHolder(view, mApplication, typeSocial, mListener);
        } else if (type == TYPE_SUGGEST) {
            View view = inflater.inflate(R.layout.holder_social_friend, parent, false);
            holder = new SocialViewHolder(view, mApplication, typeSocial, mListener);
        } else {
            View threadMessageView = inflater.inflate(R.layout.holder_thread_layout, parent, false);
            holder = new GroupViewHolder(threadMessageView, mApplication, mListener);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        //int type = getItemViewType(position);
        Object item = getItem(position);
        if (item != null)
            ((BaseViewHolder) holder).setElement(item);
        /*if (type == TYPE_SECTION) {
            ((SectionViewHolder) holder).setElement(item);
        } else if (type == TYPE_NUMBER) {
            ((PhoneViewHolder) holder).setElement(item);
        } else if (type == TYPE_OFFICIAL) {
            ((OfficialViewHolder) holder).setElement(item);
        } else if (type == TYPE_SOCIAL) {

        } else {

        }*/
    }
}

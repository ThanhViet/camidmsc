package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.util.Log;

import java.util.List;

/**
 * Created by thaodv on 05-Jan-15.
 */
public class MoreStickerGridAdapter extends BaseEmoGridAdapter {
    private static final String TAG = MoreStickerGridAdapter.class.getSimpleName();
    private List<StickerItem> stickerItemList;


    public MoreStickerGridAdapter(Context context,
                                  KeyClickListener listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    /*    public void setStickerCollection(StickerCollection stickerCollection) {

    }*/

    public void setListStickerItem(List<StickerItem> listStickerItemByCollectionId) {
        stickerItemList = listStickerItemByCollectionId;
        setCount(stickerItemList.size());
        Log.i(TAG, "setListStickerItem " + count);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = getBaseView(position, convertView, parent);
        //float dimensionTextSize = this.mContext.getResources().getDimension(R.dimen.mocha_text_size_level_2_5);
        image.getLayoutParams().height = mContext.getResources().getDimensionPixelSize(R.dimen.sticker_grid_height);
        image.getLayoutParams().width = mContext.getResources().getDimensionPixelSize(R.dimen.sticker_grid_height);
        int paddingImage = mContext.getResources().getDimensionPixelSize(R.dimen.margin_more_content_5);
        image.setPadding(paddingImage, paddingImage, paddingImage, paddingImage);
        final StickerItem stickerItem = stickerItemList.get(position);
        ImageLoaderManager.getInstance(mContext).displayStickerImage(image,
                stickerItem.getCollectionId(), stickerItem.getItemId());
        emoLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mListener.stickerClicked(stickerItem);
            }
        });
//
        return v;
    }
}
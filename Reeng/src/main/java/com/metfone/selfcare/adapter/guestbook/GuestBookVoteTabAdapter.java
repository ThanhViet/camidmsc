package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.metfone.selfcare.fragment.guestbook.GuestBookVotesFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/25/2017.
 */
public class GuestBookVoteTabAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = GuestBookVoteTabAdapter.class.getSimpleName();
    private ArrayList<String> mListTitle = new ArrayList<>();
    private Context mContext;

    public GuestBookVoteTabAdapter(FragmentManager fragmentManager, Context context, ArrayList<String> titleIds) {
        super(fragmentManager);
        this.mListTitle = titleIds;
        this.mContext = context;
    }

    public void setListTitle(ArrayList<String> titles) {
        this.mListTitle = titles;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mListTitle.get(position);
    }

    @Override
    public int getCount() {
        return mListTitle.size();
    }

    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem: " + position);
        if (position == 0) {
            return GuestBookVotesFragment.newInstance(Constants.GUEST_BOOK.VOTE_TAB_WEEK);
        } else {
            return GuestBookVotesFragment.newInstance(Constants.GUEST_BOOK.VOTE_TAB_TOTAL);
        }
    }
}

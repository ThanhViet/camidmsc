package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.holder.contact.SearchUserViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 6/8/2016.
 */
public class SearchUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private ArrayList<PhoneNumber> listItem;
    private RecyclerClickListener mRecyclerClickListener;

    public SearchUserAdapter(ApplicationController application, ArrayList<PhoneNumber> listItem) {
        this.mApplication = application;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listItem = listItem;
    }

    public void setListUser(ArrayList<PhoneNumber> list) {
        this.listItem = list;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public Object getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        SearchUserViewHolder holder;
        View view = mLayoutInflater.inflate(R.layout.holder_officer_viewer, parent, false);
        holder = new SearchUserViewHolder(view, mApplication);
        if (mRecyclerClickListener != null) {
            holder.setRecyclerClickListener(mRecyclerClickListener);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SearchUserViewHolder searchUserHolder = (SearchUserViewHolder) holder;
        if (mRecyclerClickListener != null) {
            searchUserHolder.setViewClick(position, getItem(position));
        }
        searchUserHolder.setElement(getItem(position));
    }
}
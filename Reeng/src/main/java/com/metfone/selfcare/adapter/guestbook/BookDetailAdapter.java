package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.holder.guestbook.BookDetailPageHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/18/2017.
 */
public class BookDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private LayoutInflater inflater;
    private BookDetailPageHolder.OnPageEditListener mListener;
    private RecyclerClickListener mRecyclerClickListener;
    private ArrayList<Page> listItems;

    public BookDetailAdapter(Context context, ArrayList<Page> items,
                             BookDetailPageHolder.OnPageEditListener listener) {
        this.mContext = context;
        this.mListener = listener;
        this.inflater = LayoutInflater.from(mContext);
        this.listItems = items;
    }

    public void setListItems(ArrayList<Page> items) {
        this.listItems = items;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        if (listItems == null) {
            return 0;
        }
        return listItems.size();
    }

    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = inflater.inflate(R.layout.holder_guest_book_page_edit, parent, false);
        BookDetailPageHolder holder = new BookDetailPageHolder(view, mContext, mListener);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((BookDetailPageHolder) holder).setElement(getItem(position));
        if (mRecyclerClickListener != null)
            ((BookDetailPageHolder) holder).setViewClick(position, getItem(position));
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 05-Jan-15.
 */
public abstract class BaseEmoGridAdapter extends BaseAdapter {
    private static final String TAG = BaseEmoGridAdapter.class.getSimpleName();
    protected String[] paths;
    Context mContext;
    KeyClickListener mListener;
    ImageView image;
    View emoLayout;
    int count;
    public BaseEmoGridAdapter(){
        
    }
    public BaseEmoGridAdapter(String[] s) {
        paths = s;
        count = s.length;
    }

    @Override
    public int getCount() {
        Log.i(TAG, "count = " + count);
        return count;
    }

    public void setCount(int c){
        count = c;
    }
        
    @Override
    public Object getItem(int position) {
        return paths[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    protected View getBaseView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.emoticons_item, null);
        }
        image = (ImageView) v.findViewById(R.id.item);
        emoLayout = v.findViewById(R.id.layout_emo_item);

        return v;
    }



    public interface KeyClickListener {
        void emoClicked(String emoPath, String emoText, int emoIndex);
        void stickerClicked(StickerItem stickerItem);
    }
}

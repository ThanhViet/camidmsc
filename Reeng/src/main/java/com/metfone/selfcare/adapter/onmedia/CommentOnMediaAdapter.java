package com.metfone.selfcare.adapter.onmedia;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.holder.onmedia.OnMediaCommentHolder;
import com.metfone.selfcare.listeners.OnMediaCommentHolderListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/20/2017.
 */
public class CommentOnMediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = CommentOnMediaAdapter.class.getSimpleName();
    private ArrayList<FeedAction> datas = new ArrayList<>();
    private ApplicationController mApp;
    private OnMediaCommentHolderListener listener;
    private TagMocha.OnClickTag onClickTag;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;
    private boolean isReplyFragment;

    public CommentOnMediaAdapter(ApplicationController mApp, ArrayList<FeedAction> datas, OnMediaCommentHolderListener
            listener, TagMocha.OnClickTag onClickTag, RecyclerClickListener mRecyclerClickListener) {
        this.datas = datas;
        this.mApp = mApp;
        this.listener = listener;
        this.onClickTag = onClickTag;
        this.mRecyclerClickListener = mRecyclerClickListener;
        inflater = (LayoutInflater) mApp
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setReplyFragment(boolean replyFragment) {
        isReplyFragment = replyFragment;
    }

    public void setDatas(ArrayList<FeedAction> datas) {
        this.datas = datas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.holder_onmedia_comment_status, parent, false);
        OnMediaCommentHolder holder = new OnMediaCommentHolder(view, mApp);
        holder.setReplyFragment(isReplyFragment);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OnMediaCommentHolder onMediaCommentHolder = (OnMediaCommentHolder) holder;
        onMediaCommentHolder.setOnClickTag(onClickTag);
        onMediaCommentHolder.setOnMediaCommentHolderListener(listener);
        onMediaCommentHolder.setViewClick(position, getItem(position));
        onMediaCommentHolder.setElement(getItem(position));
        onMediaCommentHolder.setCurrentPos(position);
    }

    public Object getItem(int i) {
        return datas.get(i);
    }


    @Override
    public int getItemCount() {
        if (datas == null || datas.isEmpty())
            return 0;
        return datas.size();
    }
}

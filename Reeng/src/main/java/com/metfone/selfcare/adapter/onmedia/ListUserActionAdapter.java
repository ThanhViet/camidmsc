package com.metfone.selfcare.adapter.onmedia;

import android.content.Context;
import android.content.res.Resources;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/6/2017.
 */
public class ListUserActionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ListUserActionAdapter.class.getSimpleName();
    private ArrayList<UserInfo> listUserInfo = new ArrayList<>();
    private ApplicationController mApp;
    private AvatarBusiness mAvatarBusiness;
    private LayoutInflater mLayoutInflater;
    private int sizeAvatar;
    private OnMediaHolderListener listener;
    private Resources mRes;

    public ListUserActionAdapter(ApplicationController mApp) {
        this.mApp = mApp;
        this.mRes = mApp.getResources();
        this.mLayoutInflater = (LayoutInflater) mApp
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mAvatarBusiness = mApp.getAvatarBusiness();
        sizeAvatar = (int) mApp.getResources().getDimension(R.dimen.avatar_thumbnail_mini_size);
    }

    public void setListener(OnMediaHolderListener listener) {
        this.listener = listener;
    }

    public void setListUserInfo(ArrayList<UserInfo> listUserInfo) {
        this.listUserInfo = listUserInfo;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_avatar_mini, parent, false);
        ViewUserActionHolder viewUserActionHolder = new ViewUserActionHolder(view);
        return viewUserActionHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserInfo userInfo = listUserInfo.get(position);
        ((ViewUserActionHolder) holder).setElement(userInfo);
    }

    @Override
    public int getItemCount() {
        return listUserInfo.size();
    }

    class ViewUserActionHolder extends BaseViewHolder {

        private View mViewAvatar;
        private RoundedImageView mImgAvatar;
        private TextView mTvwAvatar;

        public ViewUserActionHolder(View itemView) {
            super(itemView);
            mViewAvatar = itemView.findViewById(R.id.layout_avatar_mini);
            mImgAvatar =  itemView.findViewById(R.id.img_onmedia_avatar_mini);
            mTvwAvatar = (TextView) itemView.findViewById(R.id.tvw_onmedia_avatar_mini);
        }

        @Override
        public void setElement(Object obj) {
            final UserInfo userInfo = (UserInfo) obj;
            String mFriendAvatarUrl;
            if (TextUtils.isEmpty(userInfo.getAvatar()) || "0".equals(userInfo.getAvatar())) {
                mFriendAvatarUrl = "";
            } else {
                mFriendAvatarUrl = mAvatarBusiness.getAvatarUrl(userInfo.getAvatar(), userInfo
                        .getMsisdn(), sizeAvatar);
            }
            mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar,
                    mFriendAvatarUrl, userInfo.getMsisdn(), userInfo.getName(), sizeAvatar);

            mViewAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickUser(userInfo);
                    }
                }
            });
        }
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.metfone.selfcare.database.model.LocalSongInfo;
import com.metfone.selfcare.holder.LocalSongHolder;

import java.util.ArrayList;

/**
 * Created by tungt on 2/25/16.
 */
public class LocalSongListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<LocalSongInfo> mSongInfoArrayList;
    private OnItemClicked mItemClickedListener;

    public LocalSongListAdapter(ArrayList<LocalSongInfo> listSong, Context context, OnItemClicked listener) {
        mSongInfoArrayList = listSong;
        mContext = context;
        mItemClickedListener = listener;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setSongArrayList(ArrayList<LocalSongInfo> mSongInfoArrayList) {
        this.mSongInfoArrayList = mSongInfoArrayList;
    }

    @Override
    public int getCount() {
        return mSongInfoArrayList!= null ? mSongInfoArrayList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mSongInfoArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LocalSongHolder localSongHolder;
        if(convertView == null){
            localSongHolder = new LocalSongHolder(mContext,mLayoutInflater,mItemClickedListener);
            localSongHolder.initView(parent);
            convertView = localSongHolder.getConvertView();
        }else{
            localSongHolder = (LocalSongHolder) convertView.getTag();
        }

        localSongHolder.showData(mSongInfoArrayList.get(position));
        return convertView;
    }

    public interface OnItemClicked{
        void onClicked(LocalSongInfo localSongInfo);
    }
}

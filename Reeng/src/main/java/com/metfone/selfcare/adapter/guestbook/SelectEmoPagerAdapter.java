package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.EmoCollection;
import com.metfone.selfcare.database.model.guestbook.EmoItem;
import com.metfone.selfcare.ui.viewpagerindicator.IconIndicator;
import com.metfone.selfcare.ui.viewpagerindicator.IconPagerAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class SelectEmoPagerAdapter extends PagerAdapter implements IconPagerAdapter {
    private Context mContext;
    private SelectEmoAdapter.OnEmoItemClick mListener;
    private ArrayList<EmoCollection> emoCollections;
    private ArrayList<IconIndicator> iconIndicators;

    public SelectEmoPagerAdapter(Context context, ArrayList<EmoCollection> collections,
                                 ArrayList<IconIndicator> indicators, SelectEmoAdapter.OnEmoItemClick listener) {
        this.mContext = context;
        this.emoCollections = collections;
        this.iconIndicators = indicators;
        this.mListener = listener;
    }

    public void setEmoCollection(ArrayList<EmoCollection> collections) {
        this.emoCollections = collections;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        if (emoCollections == null) return 0;
        return emoCollections.size();
    }

    @Override
    public IconIndicator getIconIndicator(int index) {
        Log.d("SelectEmoPagerAdapter", "getIconIndicator: " + index);
        return iconIndicators.get(index);
        /*EmoCollection emoCollection = emoCollections.get(index);
        String avatar = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(emoCollection.getAvatar());
        return new IconIndicator(avatar, avatar);*/
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        View stickerLayout = LayoutInflater.from(mContext).inflate(R.layout.emoticons_grid, null);
        LinearLayout layout_emoticons_grid = (LinearLayout) stickerLayout.findViewById(R.id.layout_emoticon_grid);
        GridView gridView = (GridView) stickerLayout.findViewById(R.id.emoticons_grid);
        gridView.setNumColumns(3);
        //sticker moi
        EmoCollection emoCollection = emoCollections.get(position);
        ArrayList<EmoItem> emoItems = emoCollection.getEmoItems();
        layout_emoticons_grid.setVisibility(View.VISIBLE);
        ApplicationController application = (ApplicationController) mContext.getApplicationContext();
        gridView.setOnScrollListener(application.getPauseOnScrollListener(null));
        SelectEmoAdapter adapter = new SelectEmoAdapter(mContext, emoItems, mListener);
        gridView.setAdapter(adapter);
        collection.addView(stickerLayout);
        return stickerLayout;
    }
}

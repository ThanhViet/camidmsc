package com.metfone.selfcare.adapter.dialog;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StrangerLocation;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/24/2017.
 */

public class FilterStrangerLocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = FilterStrangerLocationAdapter.class.getSimpleName();
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private ArrayList<StrangerLocation> listItem = new ArrayList<>();
    private RecyclerClickListener mRecyclerClickListener;

    public FilterStrangerLocationAdapter(ApplicationController application, ArrayList<StrangerLocation> listItem) {
        this.mApplication = application;
        this.mLayoutInflater = (LayoutInflater) mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listItem = listItem;
    }

    public void setListItems(ArrayList<StrangerLocation> items) {
        this.listItem = items;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public Object getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        Log.d(TAG, "onCreateViewHolder");
        BaseViewHolder holder;
        View view = mLayoutInflater.inflate(R.layout.holder_stranger_location, parent, false);
        holder = new ViewHolder(mApplication, view);
        holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + position);
        ((BaseViewHolder) holder).setViewClick(position, getItem(position));
        ((ViewHolder) holder).setElement(getItem(position));
    }

    private class ViewHolder extends BaseViewHolder {
        private StrangerLocation mEntry;
        private Context mContext;
        private AppCompatTextView mTvwName;
        private AppCompatImageView mImgSelected;

        public ViewHolder(Context context, View itemView) {
            super(itemView);
            this.mContext = context;
            mImgSelected =  itemView.findViewById(R.id.stranger_location_selected);
            mTvwName =  itemView.findViewById(R.id.stranger_location_name);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (StrangerLocation) obj;
            mTvwName.setText(mEntry.getName());
            if (mEntry.isSelected()) {
                mImgSelected.setVisibility(View.VISIBLE);
            } else {
                mImgSelected.setVisibility(View.INVISIBLE);
            }
        }
    }
}

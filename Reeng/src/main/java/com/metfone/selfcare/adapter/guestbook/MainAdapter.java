package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.holder.guestbook.MainAddBookHolder;
import com.metfone.selfcare.holder.guestbook.MainBookHolder;
import com.metfone.selfcare.holder.guestbook.MainDividerHolder;
import com.metfone.selfcare.holder.guestbook.MainPageHolder;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/19/2017.
 */
public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = MainAdapter.class.getSimpleName();
    private Context mContext;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;
    private ArrayList<Object> listItems;

    public MainAdapter(Context context, ArrayList<Object> items) {
        this(context);
        this.listItems = items;
    }

    public MainAdapter(Context context) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
    }

    public void setListItems(ArrayList<Object> items) {
        this.listItems = items;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        if (listItems == null) {
            return 0;
        }
        return listItems.size();
    }

    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof Book) {
            if (((Book) item).isEmpty()) {
                return Constants.GUEST_BOOK.HOLDER_MAIN_ADD_BOOK;
            } else {
                return Constants.GUEST_BOOK.HOLDER_MAIN_BOOK;
            }
        } else if (item instanceof Page) {
            return Constants.GUEST_BOOK.HOLDER_MAIN_PAGE;
        } else {
            return Constants.GUEST_BOOK.HOLDER_MAIN_DIVIDER;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        BaseViewHolder holder;
        if (type == Constants.GUEST_BOOK.HOLDER_MAIN_BOOK) {
            View view = inflater.inflate(R.layout.holder_guest_book_template, parent, false);
            holder = new MainBookHolder(view, mContext);
        } else if (type == Constants.GUEST_BOOK.HOLDER_MAIN_ADD_BOOK) {
            View view = inflater.inflate(R.layout.holder_guest_book_add_book, parent, false);
            holder = new MainAddBookHolder(view, mContext);
        } else if (type == Constants.GUEST_BOOK.HOLDER_MAIN_PAGE) {
            View view = inflater.inflate(R.layout.holder_guest_book_template, parent, false);
            holder = new MainPageHolder(view, mContext);
        } else {
            View view = inflater.inflate(R.layout.holder_guest_book_divider, parent, false);
            holder = new MainDividerHolder(view, mContext);
        }
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + position);
        /*if (getItemViewType(position)==Constants.GUEST_BOOK.HOLDER_MAIN_DIVIDER){
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }*/
        ((BaseViewHolder) holder).setElement(getItem(position));
        if (mRecyclerClickListener != null)
            ((BaseViewHolder) holder).setViewClick(position, getItem(position));
    }
}

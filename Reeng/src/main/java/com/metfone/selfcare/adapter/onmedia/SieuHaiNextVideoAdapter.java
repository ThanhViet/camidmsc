package com.metfone.selfcare.adapter.onmedia;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.holder.onmedia.feeds.SieuHaiVideoNextHolder;
import com.metfone.selfcare.listeners.SieuHaiNextVideoHolderListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by sonnn00 on 10/5/2017.
 */

public class SieuHaiNextVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = CommentOnMediaAdapter.class.getSimpleName();
    private ArrayList<FeedContent> datas = new ArrayList<>();
    private ApplicationController mApp;
    private SieuHaiNextVideoHolderListener listener;
    private TagMocha.OnClickTag onClickTag;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;

    public SieuHaiNextVideoAdapter(ApplicationController mApp, ArrayList<FeedContent> datas, SieuHaiNextVideoHolderListener
            listener, TagMocha.OnClickTag onClickTag, RecyclerClickListener mRecyclerClickListener) {
        this.datas = datas;
        this.mApp = mApp;
        this.listener = listener;
        this.onClickTag = onClickTag;
        this.mRecyclerClickListener = mRecyclerClickListener;
        inflater = (LayoutInflater) mApp
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setDatas(ArrayList<FeedContent> datas) {
        this.datas = datas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.holder_sieuhai_item_next_video, parent, false);
        SieuHaiVideoNextHolder holder = new SieuHaiVideoNextHolder(view, mApp);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SieuHaiVideoNextHolder onMediaCommentHolder = (SieuHaiVideoNextHolder) holder;
//        onMediaCommentHolder.setOnClickTag(onClickTag);
        onMediaCommentHolder.setOnSieuHaiHolderListener(listener);
//        onMediaCommentHolder.setViewClick(position, getItem(position));
        onMediaCommentHolder.setElement(getItem(position));
    }

    public Object getItem(int i) {
        return datas.get(i);
    }


    @Override
    public int getItemCount() {
        if (datas == null || datas.isEmpty())
            return 0;
        return datas.size();
    }
}
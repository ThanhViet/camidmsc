package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.holder.contact.BaseContactHolder;
import com.metfone.selfcare.holder.contact.OfficerAccountViewHolder;
import com.metfone.selfcare.holder.contact.PhoneNumberViewHolder;
import com.metfone.selfcare.holder.contact.SectionViewHolder;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/3/14.
 */
public class PhoneNumberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = PhoneNumberAdapter.class.getSimpleName();
    private LayoutInflater mLayoutInflater;
    private ApplicationController mApplication;
    private ArrayList<Object> mListObjects;
    private ClickListener.IconListener mCallBack;
    private int mType;
    private RecyclerClickListener mRecyclerClickListener;
    private boolean smallItem;
    private boolean officer;
    private ArrayList<String> mMemberThread = new ArrayList<>();

    public PhoneNumberAdapter(ApplicationController application, ArrayList<Object> listObjects,
                              ClickListener.IconListener callBack, int type, boolean officer) {
        this.mApplication = application;
        this.mListObjects = listObjects;
        this.mCallBack = callBack;
        this.mType = type;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    /**
     * new adapter phone number
     *
     * @param application
     * @param listPhoneNumbers
     * @param callBack
     * @param type
     * * @param officer
     */
    public PhoneNumberAdapter(ApplicationController application, ArrayList<PhoneNumber> listPhoneNumbers,
                              ClickListener.IconListener callBack, int type,  boolean officer, int typeScreen) {
        this.mApplication = application;
        this.mListObjects = new ArrayList<>();
        this.mListObjects.addAll(listPhoneNumbers);
        this.mCallBack = callBack;
        this.mType = type;
        this.officer = officer;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    /**
     * new adapter phone number
     *
     * @param application
     * @param listPhoneNumbers
     * @param callBack
     * @param type
     */
    public PhoneNumberAdapter(ApplicationController application, ArrayList<PhoneNumber> listPhoneNumbers,
                              ClickListener.IconListener callBack, int type) {
        this.mApplication = application;
        this.mListObjects = new ArrayList<>();
        this.mListObjects.addAll(listPhoneNumbers);
        this.mCallBack = callBack;
        this.mType = type;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListObjects(ArrayList<Object> listObjects) {
        this.mListObjects = listObjects;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setSmallItem(boolean smallItem) {
        this.smallItem = smallItem;
    }

    public void setMemberThread(ArrayList<String> datas) {
        if (datas == null) datas = new ArrayList<>();
        this.mMemberThread = datas;
    }

    /**
     * @param listPhoneNumbers
     */
    public void setListPhoneNumbers(ArrayList<PhoneNumber> listPhoneNumbers) {
        if (mListObjects == null) {
            mListObjects = new ArrayList<>();
        } else {
            mListObjects.clear();
        }
        if (listPhoneNumbers != null) mListObjects.addAll(listPhoneNumbers);
    }

    public void removeItem(int pos) {
        mListObjects.remove(pos);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos, getItemCount());
    }

    public void setType(int type) {
        mType = type;
    }

    @Override
    public int getItemCount() {
        if (mListObjects == null) return 0;
        return mListObjects.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Object getItem(int position) {
        return mListObjects.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        Object obj = getItem(position);
        if (obj instanceof PhoneNumber) {
            PhoneNumber phone = (PhoneNumber) obj;
            if (phone.getSectionType() == -4) {
                return NumberConstant.TYPE_HOLDER_DIVIDER;
            } else if (phone.getContactId() == null) {

                return NumberConstant.TYPE_HOLDER_SECTION;
            } else {
                return NumberConstant.TYPE_HOLDER_PHONE;
            }
        } else {
            return NumberConstant.TYPE_HOLDER_OFFICER;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        Log.d(TAG, "onCreateViewHolder: " + type);
        BaseContactHolder holder;
        if (type == NumberConstant.TYPE_HOLDER_PHONE) {
            View view = mLayoutInflater.inflate(R.layout.holder_contact_viewer, parent, false);
            holder = new PhoneNumberViewHolder(view, mApplication, mCallBack, smallItem);
            ((PhoneNumberViewHolder)holder).setSelectedList(mMemberThread);
        } else if (type == NumberConstant.TYPE_HOLDER_SECTION) {
            View view = mLayoutInflater.inflate(R.layout.holder_contact_section, parent, false);
            if (mType == Constants.CONTACT.CONTACT_VIEW_MEMBER_GROUP) {
                holder = new SectionViewHolder(view, mApplication, null, false, "settingGroup");
            } else if (mType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP || mType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST) {
                holder = new SectionViewHolder(view, mApplication, null, false, "createGroup");
            } else {
                holder = new SectionViewHolder(view, mApplication, null, false, "nameContact");
            }
        } else if (type == NumberConstant.TYPE_HOLDER_DIVIDER) {
            View view = mLayoutInflater.inflate(R.layout.holder_divider, parent, false);
            holder = new DividerHolder(view);
        } else {
            View view = mLayoutInflater.inflate(R.layout.holder_officer_viewer, parent, false);
            holder = new OfficerAccountViewHolder(view, mApplication, mCallBack);
        }
        holder.setType(mType);
        holder.setSizeList(getItemCount());
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object item = getItem(position);
        BaseContactHolder baseHolder = (BaseContactHolder) holder;
        if (mRecyclerClickListener != null)
            baseHolder.setViewClick(position, item);
        baseHolder.setElement(item);
    }

    public static class DividerHolder extends BaseContactHolder {

        DividerHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setElement(Object obj) {
        }
    }
}
package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.holder.NearYouHolder;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/3/2017.
 */

public class NearYouAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = StrangerMusicAdapter.class.getSimpleName();
    private Context mContext;
    private NearYouInterface mListener;
    private LayoutInflater infalter;
    private ArrayList<StrangerMusic> mStrangerMusics;

    public NearYouAdapter(Context context, ArrayList<StrangerMusic> strangerMusics, NearYouInterface listener) {
        this.mContext = context;
        this.infalter = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mStrangerMusics = strangerMusics;
        this.mListener = listener;
    }

    public void setDatas(ArrayList<StrangerMusic> strangerMusics) {
        this.mStrangerMusics = strangerMusics;
    }

    public Object getItem(int position) {
        return mStrangerMusics.get(position);
    }

    @Override
    public int getItemCount() {
        if (mStrangerMusics == null) return 0;
        return mStrangerMusics.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View convertView = infalter.inflate(R.layout.item_stranger_around_v5, parent, false);
        NearYouHolder nearYouHolder =  new NearYouHolder(convertView, mContext, mListener);
        return nearYouHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((NearYouHolder) holder).setElement(getItem(position));
    }

    public interface NearYouInterface {
        void onClickListen(StrangerMusic entry);

        void onClickPoster(StrangerMusic entry);

        void onClickMessage(StrangerMusic entry);
    }
}

package com.metfone.selfcare.adapter;

import android.content.res.Resources;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.bot.Action;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 1/17/2017.
 */
public class OfficialActionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = OfficialActionAdapter.class.getSimpleName();
    private ApplicationController mApplication;
    private ArrayList<Action> listData;
    private LayoutInflater inflater;
    private Resources mRes;
    private RecyclerClickListener mRecyclerClickListener;
    private boolean isRequesting = false;

    public OfficialActionAdapter(ApplicationController context) {
        this.mApplication = context;
        this.listData = new ArrayList<>();
        inflater = LayoutInflater.from(mApplication);
        mRes = mApplication.getResources();
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setActionList(ArrayList<Action> list) {
        if (list == null) list = new ArrayList<>();
        listData = list;
    }


    public void setRequesting(boolean requesting) {
        this.isRequesting = requesting;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public Action getItem(int position) {
        return listData.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View holderView = inflater.inflate(R.layout.holder_official_action, parent, false);
        OfficialActionHolder holder = new OfficialActionHolder(holderView, mApplication);
        holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder");
        ((OfficialActionHolder) holder).setViewClick(position, listData.get(position));
        ((OfficialActionHolder) holder).setElement(listData.get(position));
    }

    @Override
    public int getItemCount() {
        if (listData == null) return 0;
        return listData.size();
    }

    private class OfficialActionHolder extends BaseViewHolder {
        private final String TAG = OfficialActionHolder.class.getSimpleName();
        private ApplicationController mApplication;
        private Resources res;
        private Action mAction;
        private TextView mTvwTitle;

        public OfficialActionHolder(View rowView, ApplicationController application) {
            super(rowView);
            this.mApplication = application;
            this.res = application.getResources();
            mTvwTitle = (TextView) rowView.findViewById(R.id.holder_oa_action_title);
        }

        @Override
        public void setElement(Object obj) {
            mAction = (Action) obj;
            mTvwTitle.setSelected(mAction.isSelected());
            if (mAction.isActionBack()) {
                mTvwTitle.setText(res.getString(R.string.official_action_back));
            } else {
                mTvwTitle.setText(mAction.getTitle());
            }
        }
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.holder.onmedia.OnMediaUserLikeHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 29/09/2015.
 */
public class UserActionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private FeedUserInteractionListener mCallBack;
    private ArrayList<UserInfo> listData;
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private RecyclerClickListener mRecyclerClickListener;

    public UserActionAdapter(ApplicationController app, ArrayList<UserInfo> list,
                             FeedUserInteractionListener listener) {
        this.mApplication = app;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = list;
        this.mCallBack = listener;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setListData(ArrayList<UserInfo> list) {
        this.listData = list;
    }

    public Object getItem(int position) {
        return listData.get(position);
    }

    /*@Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }*/

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        OnMediaUserLikeHolder holder;
        View view = mLayoutInflater.inflate(R.layout.holder_onmedia_user_like, parent, false);
        holder = new OnMediaUserLikeHolder(view, mApplication, mCallBack);
        if (mRecyclerClickListener != null) {
            holder.setRecyclerClickListener(mRecyclerClickListener);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OnMediaUserLikeHolder onMediaUserLikeHolder = (OnMediaUserLikeHolder) holder;
        onMediaUserLikeHolder.setViewClick(position, getItem(position));
        onMediaUserLikeHolder.setElement(getItem(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public interface FeedUserInteractionListener {
        void onChatClick(UserInfo userInfo);

        void navigateToProfile(UserInfo userInfo);
    }
}
package com.metfone.selfcare.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.GameHTML5Info;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thanhnt72 on 5/29/2017.
 */
public class ListGameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAg = ListGameAdapter.class.getSimpleName();
    private ArrayList<GameHTML5Info> listGame = new ArrayList<>();
    private ApplicationController mApp;
    private BaseSlidingFragmentActivity mActivity;
    private LayoutInflater inflater;

    public ListGameAdapter(BaseSlidingFragmentActivity activity) {
        mActivity = activity;
        mApp = (ApplicationController) activity.getApplication();
        inflater = LayoutInflater.from(this.mApp);
    }

    public void setListGame(ArrayList<GameHTML5Info> list) {
        this.listGame = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListGameViewHolder holder;
        View view = inflater.inflate(R.layout.holder_row_game_title, parent, false);
        holder = new ListGameViewHolder(mApp, view);
        /*if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);*/
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListGameViewHolder officerHolder = (ListGameViewHolder) holder;
        officerHolder.setElement(listGame.get(position));
    }

    @Override
    public int getItemCount() {
        if (listGame == null) return 0;
        return listGame.size();
    }

    private class ListGameViewHolder extends BaseViewHolder {
        private GameHTML5Info mEntry;
        private Context mContext;
        private CircleImageView mImgGame;
        private TextView mTvwTitle, mTvwDes, mTvwPlay;

        public ListGameViewHolder(Context context, View itemView) {
            super(itemView);
            this.mContext = context;
            mImgGame = itemView.findViewById(R.id.img_game_thumb);
            mTvwTitle = itemView.findViewById(R.id.tvw_title_game);
            mTvwDes = itemView.findViewById(R.id.tvw_des_game);
            mTvwPlay = itemView.findViewById(R.id.tvw_play_game);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (GameHTML5Info) obj;
            ApplicationController application = (ApplicationController) mContext.getApplicationContext();
            int size = (int) application.getResources().getDimension(R.dimen.size_image_intro_group);
            mTvwTitle.setText(mEntry.getTitleGame());
            mTvwDes.setText(mEntry.getDesGame());
            if (TextUtils.isEmpty(mEntry.getThumb())) {
                mImgGame.setImageResource(R.drawable.ic_accumulate_default);
//                application.getUniversalImageLoader().cancelDisplayTask(mImgGame);
                Glide.with(ApplicationController.self()).clear(mImgGame);
            } else {
                Glide.with(application)
                        .load(mEntry.getThumb())
                        .into(mImgGame);
//                application.getUniversalImageLoader().displayImage(mEntry.getThumb(), new ImageViewAwareTargetSize(mImgGame, size, size));
            }
            mTvwPlay.setOnClickListener(v -> {
                if (mEntry != null) {
                    String schemaLink = mEntry.getLink();
                    DeepLinkHelper.getInstance().openSchemaLink(mActivity, schemaLink);
                }
            });

            mApp.getAvatarBusiness().setOfficialThreadAvatar(mImgGame, mEntry.getThumb(), 0);
        }
    }
}

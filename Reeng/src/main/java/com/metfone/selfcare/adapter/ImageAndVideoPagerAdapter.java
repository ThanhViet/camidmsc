package com.metfone.selfcare.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import androidx.viewpager.widget.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.MediaHolderFactory;
import com.metfone.selfcare.holder.MediaHolderFactory.MediaHolder;
import com.metfone.selfcare.ui.chatviews.ChatMediaView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thaodv on 6/3/2015.
 */
public class ImageAndVideoPagerAdapter extends PagerAdapter {
    private static final String TAG = ImageAndVideoPagerAdapter.class.getSimpleName();
    private static final float pageWidthPortrait = 0.75f;
    private static final float pageWidthLandscape = 0.4f;
    private Activity mActivity;
    private ArrayList<ImageInfo> imageInfoArrayList;
    private LayoutInflater inflater;
    //    private ImageView mImgContent;
    private ChatMediaView.OnMediaListener mMediaClickListener;

    public ImageAndVideoPagerAdapter(Activity activity, ArrayList<ImageInfo> arrayListMedia, ChatMediaView.OnMediaListener mediaListener) {
        this.mActivity = activity;
        this.imageInfoArrayList = arrayListMedia;
        this.mMediaClickListener = mediaListener;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return imageInfoArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public float getPageWidth(int position) {
        Log.d(TAG, "getPageWidth : " + position);
        int orientation = mActivity.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return pageWidthLandscape;
        } else {
            return pageWidthPortrait;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Log.d(TAG, "instantiateItem position = " + position);
        // Declare Variables
        if (inflater == null) {
            inflater = (LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        ImageInfo imageInfo = imageInfoArrayList.get(position);
        MediaHolder mediaHolder = MediaHolderFactory.createHolder(mActivity, inflater, container, imageInfo);
        setItemListener(mediaHolder, imageInfo);
        String imagePath = imageInfo.getImagePath();
        if (!TextUtils.isEmpty(imagePath)) {
            ImageLoaderManager.getInstance(mActivity).displayDetailOfMessage(imagePath, mediaHolder.mImgContent, imageInfo.isVideo());
        }
        // Add viewpager_item.xml to ViewPager
        container.addView(mediaHolder.itemView);
        return mediaHolder.itemView;
    }

    private void setItemListener(final MediaHolder mediaHolder, final ImageInfo imageInfo) {
        mediaHolder.mImgContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "imageContent onClick");
                boolean isSelectedBefore = imageInfo.isSelected();
                if (isSelectedBefore) {
                    //neu dang select ma click tiep thi deselected
                    imageInfo.setSelected(false);
                } else {
                    //neu dang deselected thi deselect all , sau do select this item
                    for (ImageInfo item : imageInfoArrayList) {
                        item.setSelected(false);
                    }
                    imageInfo.setSelected(true);
                }
                notifyDataSetChanged();
            }
        });

        mediaHolder.mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaHolder.mBtnSend.setVisibility(View.GONE);
                mediaHolder.maskLayout.setVisibility(View.GONE);
                if (imageInfo.isVideo()) {
                    mMediaClickListener.onSendVideo(imageInfo);
                } else {
                    mMediaClickListener.onSendImageMessage(imageInfo.getImagePath());
                }
            }
        });
        mediaHolder.mBtnPlayVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   Log.i(TAG, "onClick play Video " + imageInfo.getImagePath());
                MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        //khi complete thi hien thi lai nhu ban dau
                    }
                };
                mediaHolder.playVideo(onCompletionListener, (ApplicationController) mActivity.getApplicationContext());
            }
        });
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        Log.i(TAG, "destroyItem position = " + position);
        // Remove viewpager_item.xml from ViewPager
        container.removeView((RelativeLayout) object);
    }

}
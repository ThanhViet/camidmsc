package com.metfone.selfcare.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.musickeeng.TopSongFragment;
import com.metfone.selfcare.fragment.musickeeng.UploadSongFragment;

import java.util.ArrayList;

/**
 * Created by tungt on 3/31/2016.
 */
public class TabSongFragmentAdapter extends FragmentStatePagerAdapter {
    private ArrayList<String> mListTitle = new ArrayList<>();

    public TabSongFragmentAdapter(FragmentManager fm, ArrayList<String> mListTitle) {
        super(fm);
        this.mListTitle = mListTitle;
    }

    @Override
    public Fragment getItem(int position) {
        return (position == 0) ? TopSongFragment.newInstance() : UploadSongFragment.newInstance();
    }

    @Override
    public int getCount() {
        return mListTitle.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (mListTitle.get(position));
    }

    public View getTabView(LayoutInflater inflater) {
        View mView = inflater.inflate(R.layout.item_tab_custom, null);
        return mView;
    }
}
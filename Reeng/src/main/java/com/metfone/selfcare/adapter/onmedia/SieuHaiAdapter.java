package com.metfone.selfcare.adapter.onmedia;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.model.tab_video.Video;
import com.viettel.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 7/12/2017.
 */
public class SieuHaiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = SieuHaiAdapter.class.getSimpleName();

    private ApplicationController mApp;
    private Activity mActivity;
    private ArrayList<Video> listVideo;
    private LayoutInflater inflater;
    OnMediaInterfaceListener.OnClickItemSieuHai onClickItemSieuHai;
    private int width, height;
    private int itemWidth;

    public SieuHaiAdapter(Activity mActivity, ArrayList<Video> listVideo) {
        this.mActivity = mActivity;
        mApp = (ApplicationController) mActivity.getApplication();
        this.listVideo = listVideo;
        this.inflater = (LayoutInflater) mApp
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        width = Math.round(mApp.getWidthPixels() * 0.5f);
        if (width > 600) width = 600;
        height = Math.round(width * 9 / 16);
        itemWidth = Math.round(mApp.getWidthPixels() * 0.75f);
        Log.i(TAG, "width: " + width + "height: " + height);
    }

    public void setListVideo(ArrayList<Video> listVideo) {
        this.listVideo = listVideo;
    }

    public ArrayList<Video> getListVideo() {
        return listVideo;
    }

    public void setOnClickItemSieuHai(OnMediaInterfaceListener.OnClickItemSieuHai onClickItemSieuHai) {
        this.onClickItemSieuHai = onClickItemSieuHai;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_thumb_sieuhai, parent, false);
        ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
        layoutParams.width = itemWidth;
        itemView.setLayoutParams(layoutParams);
        SieuHaiHolder sieuHaiHolder = new SieuHaiHolder(itemView);
        return sieuHaiHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Video sieuHaiModel = listVideo.get(position);
        ((SieuHaiHolder) holder).setElement(sieuHaiModel, position);
    }

    @Override
    public int getItemCount() {
        return listVideo.size();
    }

    private class SieuHaiHolder extends BaseViewHolder {

        private View itemView;
        private ImageView mImgThumb;
        private TextView mTvwTitle;

        public SieuHaiHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            mImgThumb = (ImageView) itemView.findViewById(R.id.img_thumb_sieuhai);
            mTvwTitle = (TextView) itemView.findViewById(R.id.tvw_title_sieuhai);
        }

        @Override
        public void setElement(Object obj) {


        }

        public void setElement(Object obj, final int pos) {
            final Video sieuHaiModel = (Video) obj;
            mTvwTitle.setText(sieuHaiModel.getTitle());

            String imagePath = sieuHaiModel.getImagePath();
//            ImageViewAwareTargetSize imgTarget = new ImageViewAwareTargetSize(mImgThumb, width, height);
            ImageLoaderManager.getInstance(mApp).setImageFeeds(mImgThumb, imagePath, width, height);
//            ImageLoaderManager.getInstance(mActivity).displayImageSieuHai(imgTarget, imagePath);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickItemSieuHai != null) {
                        onClickItemSieuHai.onClickVideoSieuHai(sieuHaiModel, pos);
                    }
                }
            });
        }
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.AbsContentHolder;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/29/2016.
 */
public class PollItemDetailAdapter extends BaseAdapter {
    private ArrayList<String> listData;
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;

    public PollItemDetailAdapter(ApplicationController app, ArrayList<String> list) {
        this.mApplication = app;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = list;
    }

    public void setListData(ArrayList<String> list) {
        this.listData = list;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder(mApplication);
            holder.initHolder(viewGroup, convertView, position, mLayoutInflater);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.setElemnts(getItem(position));
        holder.setPosition(position);
        return holder.getConvertView();
    }

   /* public interface Listener {

        void onItemClick(PollItem item);
    }*/

    private class Holder extends AbsContentHolder {
        private ApplicationController mApplication;
        private String mEntry;
        private int position;
        private View convertView;
        private TextView mTvwContent;

        public Holder(ApplicationController app) {
            this.mApplication = app;
            setContext(app);
        }

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
            convertView = layoutInflater.inflate(R.layout.holder_vote_item_detail, parent, false);
            mTvwContent = (TextView) convertView.findViewById(R.id.holder_vote_content);
            convertView.setTag(this);
            setConvertView(convertView);
        }

        @Override
        public void setElemnts(Object obj) {
            this.mEntry = (String) obj;
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (myNumber.equals(mEntry)) {
                mTvwContent.setText(R.string.you);
            } else {
                String friendName = mApplication.getMessageBusiness().getFriendNameOfGroup(mEntry);
                mTvwContent.setText(friendName);
            }
        }
    }
}
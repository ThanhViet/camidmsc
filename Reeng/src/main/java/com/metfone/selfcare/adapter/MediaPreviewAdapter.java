package com.metfone.selfcare.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.helper.images.ImageLoaderManager;

import java.util.ArrayList;

/**
 * Created by ThanhNT72 on 5/30/2015.
 */
public class MediaPreviewAdapter extends ArrayAdapter<ImageInfo> {
    private LayoutInflater mInflater;
    public MediaPreviewAdapter(Context context, ArrayList<ImageInfo> objects) {
        super(context, R.layout.item_preview_media, objects);
        mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) {
            // Inflate the view since it does not exist
            convertView = mInflater.inflate(R.layout.item_preview_media, parent, false);
            holder = new Holder();

            holder.imageView = (ImageView) convertView.findViewById(R.id.item_image_preview);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        String filePath = getItem(position).getImagePath();

        if(!TextUtils.isEmpty(getItem(position).getImagePath())){
            ImageLoaderManager.getInstance(getContext()).displayDetailOfMessage(filePath, holder.imageView, false);
        }
        return convertView;
    }

    /**
     * View holder for the views we need access to
     */
    private static class Holder {
        public ImageView imageView;
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.NewsHotModel;
import com.metfone.selfcare.holder.NewsHotHolder;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/11/17.
 */

public class NewsHotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = NewsHotAdapter.class.getSimpleName();
    private Context mContext;
    private NewsHotInterface mListener;
    private LayoutInflater infalter;
    private ArrayList<NewsHotModel> datas;

    public NewsHotAdapter(Context context, ArrayList<NewsHotModel> datas, NewsHotInterface listener) {
        this.mContext = context;
        this.infalter = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.datas = datas;
        this.mListener = listener;
    }

    public void setDatas(ArrayList<NewsHotModel> NewsHotModels) {
        this.datas = NewsHotModels;
    }

    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public int getItemCount() {
        if (datas == null) return 0;
        return datas.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View convertView = infalter.inflate(R.layout.item_news_hot, parent, false);
        return new NewsHotHolder(convertView, mContext, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((NewsHotHolder) holder).setElement(getItem(position));
    }

    public interface NewsHotInterface {
        void onClickItem(NewsHotModel entry);
    }
}
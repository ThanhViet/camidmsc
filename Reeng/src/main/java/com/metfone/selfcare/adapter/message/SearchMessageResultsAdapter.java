package com.metfone.selfcare.adapter.message;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by huongnd38 on 10/11/2018.
 */

public class SearchMessageResultsAdapter extends BaseAdapter {
    private static final String TAG = SearchMessageResultsAdapter.class.getSimpleName();
    private ArrayList<ReengMessage> datas = new ArrayList<>();
    private LayoutInflater inflater;
    private ThreadMessage mThreadMessage;
    private Resources mRes;
    private Context mContext;
    private ApplicationController mApplication;
    private String mSearchTag = "";

    private SearchMessageClickListener mMessageInteractionListener;

    public SearchMessageResultsAdapter(Context context, SearchMessageClickListener listener) {
        inflater = LayoutInflater.from(context);
        mRes = context.getResources();
        this.mContext = context;
        mApplication = (ApplicationController) context.getApplicationContext();
        mMessageInteractionListener = listener;
    }

    public void setData(ArrayList<ReengMessage> data) {
        if (data != null) {
            datas = data;
        }
    }

    public void setSearchTag(String tag) {
        mSearchTag = tag;
    }

    public void setThreadMessage(ThreadMessage threadMessage) {
        mThreadMessage = threadMessage;
    }

    public void onDestroy() {
        mMessageInteractionListener = null;
    }

    @Override
    public int getCount() {
        if (datas != null) {
            return datas.size();
        }
        return 0;
    }

    @Override
    public ReengMessage getItem(int position) {
        if (position >= 0 && position < datas.size()) {
            return datas.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_search_message_result, parent, false);
            holder = new ViewHolder();
            holder.tvName = convertView.findViewById(R.id.thread_name);
            holder.tvLastTime = convertView.findViewById(R.id.thread_last_time);
            holder.tvContent = convertView.findViewById(R.id.thread_last_content);
            holder.imvAvatar = convertView.findViewById(R.id.message_row_receiver_avatar);
            holder.txtAvatar = convertView.findViewById(R.id.message_row_receiver_avatar_text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        setViewholderData(holder, getItem(position));
        return convertView;
    }

    private void setViewholderData(ViewHolder holder, ReengMessage messageModel) {
        if (holder != null && messageModel != null && mApplication != null) {
            String name = getSenderName(messageModel);
            holder.tvName.setText(name);
            holder.tvLastTime.setText("");
            holder.tvContent.setText(messageModel.getContent());
            setTitleTextView(holder.tvContent, messageModel.getContent(), mSearchTag);
            long timerOfMsg = messageModel.getTime();
            Date currentDate = new Date();
            long currentTime = currentDate.getTime();
            holder.tvLastTime.setVisibility(View.VISIBLE);
            holder.tvLastTime.setText(TimeHelper.formatCommonTime(timerOfMsg, currentTime, mRes));
            setFriendAvatar(holder.imvAvatar,  holder.txtAvatar, messageModel, mThreadMessage);
        }
    }

    private String getSenderName(ReengMessage message) {
        String name = message.getSender();
        if (!TextUtils.isEmpty(message.getSenderName())) {
            name = message.getSenderName();
        }
        try {
            if (message.getDirection() == ReengMessageConstant.Direction.received) {
                int threadType = mThreadMessage.getThreadType();
                if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                    name = mApplication.getMessageBusiness().getFriendNameOfRoom(message.getSender(), message.getSenderName(), mThreadMessage.getThreadName());
                } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(message.getSender());
                    if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                        name = phoneNumber.getName();
                    } else {                      //hien thi so dien thoai neu la group chat
                        NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(message.getSender());
                        if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                            name = message.getSender() + " (" + nonContact.getNickName() + ")";
                        } else {
                            if (!TextUtils.isEmpty(message.getSenderName())) {
                                name = message.getSender() + " (" + message.getSenderName() + ")";
                            } else
                                name = message.getSender();
                        }
                    }
                } else if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    String numberFriend = mThreadMessage.getSoloNumber();
                    String friendName = mApplication.getMessageBusiness().getFriendName(numberFriend);
                    if (!TextUtils.isEmpty(friendName)) {
                        name = friendName;
                    }
                }
            } else if (message.getDirection() == ReengMessageConstant.Direction.send) {
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                if (account != null && !TextUtils.isEmpty(account.getName())) {
                    name = account.getName();
                }
            }
        } catch (Exception e) {
        }
        return name;
    }

    protected void setFriendAvatar(CircleImageView mImgAvatar,TextView avatarText, final ReengMessage message, ThreadMessage threadMessage) {
        if (threadMessage == null || message == null || mContext == null) {
            return;
        }
        int size = (int) mContext.getResources().getDimension(R.dimen.avatar_small_size);
        final String number = message.getSender();
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(message.getSender());
        int mThreadType = mThreadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatar, size,
                    threadMessage.getServerId(), null, false);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            if (TextUtils.isEmpty(number)) {        //tin nhan tu admin rooms
                String avatarUrl;
                if (!TextUtils.isEmpty(message.getSenderAvatar())) {
                    avatarUrl = message.getSenderAvatar();
                } else {
                    avatarUrl = mApplication.getOfficerBusiness().getOfficerAvatarByServerId(threadMessage
                            .getServerId());
                }
                mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatar, avatarUrl, size);
            } else {
                //tin nhan tu thanh vien khac
                int sizeSmall = (int) mContext.getResources().getDimension(R.dimen.avatar_thumbnail_size);
                mApplication.getAvatarBusiness().setMemberRoomChatAvatar(mImgAvatar, avatarText, message.getSender(), message.getSenderName(), phoneNumber, sizeSmall, message.getSenderAvatar());
            }
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, avatarText, phoneNumber, size);
            } else {
                mApplication.getAvatarBusiness().setUnknownNumberAvatarGroup(mImgAvatar, avatarText, number, message.getSenderAvatar(), size);
            }
        } else {
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, avatarText, phoneNumber, size);
            } else {
                if (threadMessage.isStranger()) {
                    mApplication.getAvatarBusiness().setStrangerAvatar(mImgAvatar,
                            null, threadMessage.getStrangerPhoneNumber(),
                            message.getSender(), message.getSender(), null, size);
                } else {
                    mApplication.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatar, avatarText, number, size);
                }
            }
        }
        // click
        if (mThreadType != ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT && !TextUtils.isEmpty(number)) {
            mImgAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mMessageInteractionListener != null)
                        mMessageInteractionListener.onFriendAvatarClick(number, message.getSenderName());
                }
            });
        } else {
            mImgAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "khong lam gi");
                }
            });
        }
    }

    private static class ViewHolder {
        private TextView tvName;
        private TextView tvLastTime;
        private TextView tvContent;
        private CircleImageView imvAvatar;
        private TextView txtAvatar;

        public ViewHolder() {
        }
    }

    private void setTitleTextView(TextView textView, String content, String searchText) {
        if (TextUtils.isEmpty(content)) return;
        if (!TextUtils.isEmpty(searchText)) {
            int highlightStrLength = searchText.length();
            int index = content.toLowerCase().indexOf(searchText.toLowerCase());
            if (index >= 0 && index + highlightStrLength <= content.length()) {
                Spannable spannable = new SpannableStringBuilder(content);
                spannable.setSpan(new android.text.style.StyleSpan(Typeface.BOLD), index, index + highlightStrLength, /*Spannable.SPAN_EXCLUSIVE_EXCLUSIVE*/ 0);
                spannable.setSpan(new ForegroundColorSpan(mApplication.getResources().getColor(R.color.setting_chat_text_color_red)), index, index + highlightStrLength, /*Spannable.SPAN_EXCLUSIVE_EXCLUSIVE*/ 0);
                textView.setText(spannable);
            }
        } else {
            textView.setText(content);
        }
    }

    public interface SearchMessageClickListener {
        void onFriendAvatarClick(String numberJid, String friendName);
    }
}
package com.metfone.selfcare.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.activity.ChangeNumberMetfoneActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.view.CamIdTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddServiceAdapter extends BaseAdapter<AddServiceAdapter.AddPhoneViewHolder, String> {


    public AddServiceAdapter(Activity activity) {
        super(activity);
    }

    public AddServiceAdapter(Activity activity, ArrayList<String> phoneList) {
        super(activity, phoneList);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @NonNull
    @Override
    public AddPhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AddPhoneViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull AddPhoneViewHolder holder, int position) {
        holder.bindView(getItem(position));
    }


    public class AddPhoneViewHolder extends BaseViewHolder {
        @BindView(R.id.ivMark)
        ImageView ivMark;
        @BindView(R.id.tvPhone)
        CamIdTextView tvPhone;
        @BindView(R.id.tvDeletePhone)
        CamIdTextView tvDeletePhone;

        public AddPhoneViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public AddPhoneViewHolder(ViewGroup parent) {
            this(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_mobile, parent, false));
        }

        public void bindView(String str) {
            if (getAdapterPosition() == 0) {
                ivMark.setVisibility(View.GONE);
            }
            tvPhone.setText(str);
            tvDeletePhone.setOnClickListener(view -> {
                items.remove(getAdapterPosition());
                notifyItemRemoved(getAdapterPosition());
                notifyItemRangeChanged(getAdapterPosition(), getItemCount());
            });
            ivMark.setOnClickListener(view -> {
                ivMark.getContext().startActivity(new Intent(ivMark.getContext(), ChangeNumberMetfoneActivity.class));
            });
        }

        @Override
        public void setElement(Object obj) {

        }
    }
}

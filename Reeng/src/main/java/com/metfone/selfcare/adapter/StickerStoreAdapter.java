package com.metfone.selfcare.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.StickerActivity;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.fragment.sticker.StickerStoreFragment;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THANHNT72 on 5/7/2015.
 */
public class StickerStoreAdapter extends BaseAdapter {

    private StickerActivity mActivity;
    private StickerStoreFragment mFragment;
    private List<StickerCollection> mListSticker;
    private ArrayList<Integer> mNewStickerList;
    private LayoutInflater mLayoutInflater;
    private Resources mRes;
    private boolean checkScreen;

    public StickerStoreAdapter(StickerActivity activity,
                               List<StickerCollection> mList,
                               StickerStoreFragment mFragment,
                               ArrayList<Integer> newSticker, boolean checkScreenRatio) {
        this.mActivity = activity;
        this.mListSticker = mList;
        this.mFragment = mFragment;
        this.mNewStickerList = newSticker;
        this.mRes = mActivity.getResources();
        this.mLayoutInflater = (LayoutInflater) mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.checkScreen = checkScreenRatio;
    }

    public void setListNewSticker(ArrayList<Integer> newSticker) {
        if (newSticker == null) {
            newSticker = new ArrayList<>();
        }
        this.mNewStickerList = newSticker;
    }

    public void setListSticker(List<StickerCollection> collections) {
        if (collections == null) {
            this.mListSticker = new ArrayList<>();
        } else {
            this.mListSticker = collections;
        }
    }

    @Override
    public int getCount() {
        return mListSticker.size();
    }

    @Override
    public StickerCollection getItem(int i) {
        return mListSticker.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.holder_preview_sticker, null);
            holder = new ViewHolder();
            holder.stickerName = (TextView) view.findViewById(R.id.more_sticker_name);
            holder.stickerAmout = (TextView) view.findViewById(R.id.more_sticker_amount);
            holder.stickerAvatar = (ImageView) view.findViewById(R.id.more_sticker_avatar);
            holder.stickerDownload = (ImageView) view.findViewById(R.id.more_sticker_download_button);
            holder.tvNew = (TextView) view.findViewById(R.id.tvNew);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final StickerCollection collection = getItem(i);
        holder.stickerName.setText(collection.getCollectionName());
        if (!checkScreen){
            holder.stickerName.setMaxLines(1);
            holder.stickerName.setEllipsize(TextUtils.TruncateAt.END);
        }
        String amountString;
        if (collection.getNumberSticker() > 1) {
            amountString = collection.getNumberSticker() + " " + mRes.getString(R.string.stickers_text);
        } else {
            amountString = collection.getNumberSticker() + " " + mRes.getString(R.string.stickers_text_single);
        }
        holder.stickerAmout.setText(amountString);
        if (mNewStickerList.contains(collection.getServerId()) && !collection.isDownloaded()) {
            holder.tvNew.setVisibility(View.VISIBLE);
        } else {
            holder.tvNew.setVisibility(View.GONE);
        }
        setButtonByCollection(collection, holder);
        String fullAvatarLink = UrlConfigHelper.getInstance(mActivity).getDomainFile()
                + collection.getCollectionIconPath();
        ImageLoaderManager.getInstance(mActivity).
                displayStickerNetwork(holder.stickerAvatar, fullAvatarLink);
        holder.stickerDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (collection.isDownloaded()) {
                    if (collection.isUpdateCollection()) {
                        mFragment.downloadStickerCollection(collection, true);
                    } else {
                        mFragment.showDialogConfirmDelete(collection);
                    }
                } else {
                    mFragment.downloadStickerCollection(collection, false);
                }
            }
        });
        return view;
    }

    private void setButtonByCollection(StickerCollection stickerCollection, ViewHolder holder) {
        if (stickerCollection.isDownloaded()) {
            if (stickerCollection.isUpdateCollection()) {
                holder.stickerDownload.setBackgroundResource(R.drawable.download);
            } else {
                holder.stickerDownload.setBackgroundResource(R.drawable.remove_circle);
            }
        } else {
            holder.stickerDownload.setBackgroundResource(R.drawable.download);
        }
    }

    public static class ViewHolder {
        public TextView stickerName;
        public TextView stickerAmout;
        public ImageView stickerAvatar;
        public ImageView stickerDownload;
        public TextView tvNew;
    }
}
package com.metfone.selfcare.adapter.onmedia;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.NotificationModel;
import com.metfone.selfcare.holder.onmedia.NotificationOnMediaHolder;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.holder.onmedia.feeds.ItemInvisibleHolder;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/20/2017.
 */
public class NotificationOnMediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = NotificationOnMediaAdapter.class.getSimpleName();
    private static final int TYPE_NOTIFICATION = 1;
    private static final int TYPE_UNKNOWN = 0;
    private ArrayList<NotificationModel> listNotification = new ArrayList<>();
    private ApplicationController mApp;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;


    public NotificationOnMediaAdapter(ApplicationController mApp, ArrayList<NotificationModel> listNotification,
                                      RecyclerClickListener mRecyclerClickListener) {
        this.mApp = mApp;
        this.listNotification = listNotification;
        this.inflater = (LayoutInflater) mApp
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setListNotification(ArrayList<NotificationModel> listNotification) {
        this.listNotification = listNotification;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder;
        if (viewType == TYPE_NOTIFICATION) {
            View viewNotification = inflater.inflate(R.layout.holder_onmedia_notification, parent, false);
            baseViewHolder = new NotificationOnMediaHolder(viewNotification, mApp);
            if (mRecyclerClickListener != null)
                baseViewHolder.setRecyclerClickListener(mRecyclerClickListener);
        } else {
            View viewUnknown = inflater.inflate(R.layout.holder_feed_default, parent, false);
            baseViewHolder = new ItemInvisibleHolder(viewUnknown, mApp);
        }
        return baseViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        if (type == TYPE_NOTIFICATION) {
            ((NotificationOnMediaHolder) holder).setViewClick(position, getItem(position));
            ((NotificationOnMediaHolder) holder).setElement(getItem(position));
        }
    }

    @Override
    public int getItemCount() {
        if (listNotification == null || listNotification.isEmpty())
            return 0;
        else return listNotification.size();
    }

    public Object getItem(int i) {
        return listNotification.get(i);
    }

    @Override
    public int getItemViewType(int position) {
        NotificationModel notification = (NotificationModel) getItem(position);
        if (notification == null || notification.getContent() == null)
            return TYPE_UNKNOWN;
        else {
            String itemType = notification.getContent().getItemType();
            if (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType)
                    || FeedContent.ITEM_TYPE_VIDEO.equals(itemType)
                    || FeedContent.ITEM_TYPE_SONG.equals(itemType)
                    || FeedContent.ITEM_TYPE_ALBUM.equals(itemType)
                    || FeedContent.ITEM_TYPE_NEWS.equals(itemType)
                    || FeedContent.ITEM_TYPE_TOTAL.equals(itemType)
                    || FeedContent.ITEM_TYPE_PROFILE_ALBUM.equals(itemType)
                    || FeedContent.ITEM_TYPE_PROFILE_AVATAR.equals(itemType)
                    || FeedContent.ITEM_TYPE_PROFILE_COVER.equals(itemType)
                    || FeedContent.ITEM_TYPE_PROFILE_STATUS.equals(itemType)) {
                return TYPE_NOTIFICATION;
            } else {
                return TYPE_UNKNOWN;
            }
        }
    }
}

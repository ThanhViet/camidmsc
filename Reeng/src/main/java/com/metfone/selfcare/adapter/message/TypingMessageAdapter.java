package com.metfone.selfcare.adapter.message;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.SearchQuery;
import com.metfone.selfcare.database.model.message.TypingItem;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.message.typing.TypingItemHolder;
import com.metfone.selfcare.holder.message.typing.TypingSongHolder;
import com.metfone.selfcare.holder.message.typing.TypingVideoHolder;
import com.metfone.selfcare.holder.onmedia.feeds.ItemInvisibleHolder;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/27/2017.
 */

public class TypingMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_SONG = 1;
    private static final int TYPE_VIDEO = 2;
    private static final int TYPE_ITEM_TYPING = 3;
    private static final int TYPE_ITEM_NULL = 4;
    private ArrayList<Object> listItems = new ArrayList<>();
    private ApplicationController mApp;
    private LayoutInflater mLayoutInflater;
    private TypingMessageListener listener;

    public TypingMessageAdapter(ApplicationController mApp, TypingMessageListener listener) {
        this.mApp = mApp;
        this.mLayoutInflater = (LayoutInflater) mApp.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listener = listener;
    }

    public void setListItems(ArrayList<Object> list) {
        this.listItems = list;
    }

    @Override
    public int getItemCount() {
        if (listItems == null) return 0;
        return listItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object entry = listItems.get(position);
        if (entry instanceof SearchQuery) {
            return TYPE_SONG;
        } else if (entry instanceof MediaModel) {
            if (((MediaModel) entry).getType() == MediaModel.TYPE_BAI_HAT) {
                return TYPE_SONG;
            } else {
                return TYPE_VIDEO;
            }
        } else if (entry instanceof TypingItem) {
            return TYPE_ITEM_TYPING;
        } else {
            return TYPE_ITEM_NULL;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder;
        if (viewType == TYPE_SONG) {
            holder = new TypingSongHolder(mApp, mLayoutInflater.inflate(R.layout.holder_typing_song, parent, false), listener);
        } else if (viewType == TYPE_VIDEO) {
            holder = new TypingVideoHolder(mApp, mLayoutInflater.inflate(R.layout.holder_typing_video, parent, false), listener);
        } else if (viewType == TYPE_ITEM_TYPING) {
            holder = new TypingItemHolder(mApp, mLayoutInflater.inflate(R.layout.holder_typing_item, parent, false), listener);
        } else {
            holder = new ItemInvisibleHolder(mLayoutInflater.inflate(R.layout.holder_feed_default, parent, false), mApp);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((BaseViewHolder) holder).setElement(listItems.get(position));
    }

    public interface TypingMessageListener {
        void onSongClick(Object item);

        void onVideoClick(MediaModel model);

        void onItemTypeClick(TypingItem item);
    }
}

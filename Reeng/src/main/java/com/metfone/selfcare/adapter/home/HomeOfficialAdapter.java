package com.metfone.selfcare.adapter.home;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.contact.OfficialViewHolder;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 6/25/2018.
 */

public class HomeOfficialAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = HomeContactFavoritesAdapter.class.getSimpleName();

    private Context mApplication;
    private ArrayList<Object> listData;
    private LayoutInflater inflater;
    private ContactsHolderCallBack mListener;

    public HomeOfficialAdapter(Context context, ArrayList<Object> list, ContactsHolderCallBack listener) {
        this.mApplication = context;
        this.listData = list;
        this.mListener = listener;
        inflater = LayoutInflater.from(mApplication);
    }

    public void setData(ArrayList<Object> threads) {
        listData = threads;
    }

    @Override
    public int getItemCount() {
        if (listData == null) return 0;
        return listData.size();
    }

    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        View view = inflater.inflate(R.layout.holder_official_vertical, parent, false);
        return new OfficialViewHolder(view, mApplication, mListener, false);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Object item = getItem(position);
        if (item != null) {
            ((OfficialViewHolder) holder).setElement(item);
        }
    }
}

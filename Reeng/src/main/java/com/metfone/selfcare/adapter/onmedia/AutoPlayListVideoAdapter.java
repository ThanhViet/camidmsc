package com.metfone.selfcare.adapter.onmedia;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.AutoPlayVideoModel;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.onmedia.AutoPlayVideoHolder;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 1/20/2018.
 */

public class AutoPlayListVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ApplicationController mApplication;
    private OnItemClickListener mCallBack;
    private ArrayList<AutoPlayVideoModel> listDatas;

    public AutoPlayListVideoAdapter(ApplicationController application, ArrayList<AutoPlayVideoModel> list) {
        this.mApplication = application;
        this.listDatas = list;
    }

    public void setListData(ArrayList<AutoPlayVideoModel> list) {
        this.listDatas = list;
    }

    public void setCallBack(OnItemClickListener listener) {
        this.mCallBack = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_auto_play_video, parent, false);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        //layoutParams.height = view.getResources().getDisplayMetrics().widthPixels;
        return new AutoPlayVideoHolder(view, mApplication, mCallBack);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((BaseViewHolder) holder).setElement(getItem(position));
        ((AutoPlayVideoHolder) holder).setPosition(position);
    }

    @Override
    public int getItemCount() {
        return listDatas == null ? 0 : listDatas.size();
    }

    private AutoPlayVideoModel getItem(int position) {
        return listDatas.get(position);
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        ((AutoPlayVideoHolder) holder).test("onViewRecycled");
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        ((AutoPlayVideoHolder) holder).test("onViewDetachedFromWindow");
    }

    public interface OnItemClickListener {
        void onFullScreen();

        boolean onCompleted(int pos);
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.NewsHotDetailModel;
import com.metfone.selfcare.database.model.onmedia.NewsHotModel;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.VideoViewCustom;
import com.metfone.selfcare.ui.glide.ImageLoader;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/11/17.
 */

public class NewsHotDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = NewsHotAdapter.class.getSimpleName();
    private Context mContext;
    private NewsHotInterface mListener;
    private LayoutInflater infalter;
    private ArrayList<NewsHotDetailModel> datas = new ArrayList<>();
    private ArrayList<NewsHotModel> relateDatas = new ArrayList<>();
    private NewsHotModel newsHotModel;

    private final int TYPE_HEADER = 0;
    private final int TYPE_TEXT = 1;
    private final int TYPE_IMAGE = 2;
    private final int TYPE_VIDEO = 3;
    private final int TYPE_AUTHOR = 4;
    private final int TYPE_HEADER_RELATE = 10;
    private final int TYPE_RELATE = 11;
    private final int TYPE_NONE = -1;

    public NewsHotDetailAdapter(Context context, NewsHotModel newsHotModel, NewsHotInterface listener) {
        this.mContext = context;
        this.infalter = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.datas = newsHotModel.getBody();
        this.newsHotModel = newsHotModel;
        datas.clear();
        datas.addAll(newsHotModel.getShortContent());
        this.mListener = listener;
    }

    public void setDatas(ArrayList<NewsHotDetailModel> datas) {
        if(this.datas.size() == 0)
        {
            this.datas = datas;
            notifyItemInserted(0);
        }
        else
        {
            int position = this.datas.size();
            this.datas = datas;
            notifyItemInserted(position);
        }
    }

    public void setRelateDatas(ArrayList<NewsHotModel> relateDatas) {
        this.relateDatas = relateDatas;
    }

    public Object getItem(int position) {
        if(position == 0) //Header
        {
            return null;
        }
        else if(position > 0 && position < datas.size() + 1)
        {
            return datas.get(position - 1);
        }
        else if(position == datas.size() + 1)
        {
            return null;
        }
        else if(position > datas.size() + 1)
        {
            return relateDatas.get(position - (datas.size() + 2));
        }
        return null;
    }

    @Override
    public int getItemCount() {
        if (datas == null) return 0;
        if(relateDatas.size() == 0)
            return datas.size() + 1;
        else
        {
            if(relateDatas.size() > 0)
                return datas.size() + 1 + relateDatas.size() + 1;
            else
                return datas.size() + 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0) //Header
        {
            return TYPE_HEADER;
        }
        else if(position > 0 && position < datas.size() + 1)
        {
            NewsHotDetailModel model = datas.get(position - 1);
            return model.getType();
        }
        else if(position == datas.size() + 1)
        {
            return TYPE_HEADER_RELATE;
        }
        else if(position > datas.size() + 1)
        {
            return TYPE_RELATE;
        }
        return TYPE_NONE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View convertView;
        switch (type)
        {
            case TYPE_HEADER:
                convertView = infalter.inflate(R.layout.item_news_hot_header, parent, false);
                return new NewsHotDetailHeaderHolder(convertView, mContext);
            case TYPE_TEXT:
                convertView = infalter.inflate(R.layout.item_news_hot_text, parent, false);
                return new NewsHotDetailTextHolder(convertView, mContext);
            case TYPE_AUTHOR:
                convertView = infalter.inflate(R.layout.item_news_hot_author, parent, false);
                return new NewsHotDetailTextHolder(convertView, mContext);
            case TYPE_IMAGE:
                convertView = infalter.inflate(R.layout.item_news_hot_image, parent, false);
                return new NewsHotDetailImageHolder(convertView, mContext, mListener);
            case TYPE_VIDEO:
                convertView = infalter.inflate(R.layout.item_news_hot_video, parent, false);
                return new NewsHotDetailVideoHolder(convertView, mContext, mListener);
            case TYPE_HEADER_RELATE:
                convertView = infalter.inflate(R.layout.item_news_hot_relate_header, parent, false);
                return new NewsHotDetailRelateHeaderHolder(convertView, mContext);
            case TYPE_RELATE:
                convertView = infalter.inflate(R.layout.item_news_hot, parent, false);
                return new NewsHotDetailRelateHolder(convertView, mContext, mListener);
            default:
                convertView = infalter.inflate(R.layout.item_news_hot_text, parent, false);
                return new NewsHotDetailTextHolder(convertView, mContext);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof NewsHotDetailHeaderHolder)
        {
            ((NewsHotDetailHeaderHolder) holder).setElement(newsHotModel);
        }
        else if(holder instanceof NewsHotDetailTextHolder)
        {
            ((NewsHotDetailTextHolder) holder).setElement(getItem(position));
        }
        else if(holder instanceof NewsHotDetailImageHolder)
        {
            ((NewsHotDetailImageHolder) holder).setElement(getItem(position));
        }
        else if(holder instanceof NewsHotDetailVideoHolder)
        {
            ((NewsHotDetailVideoHolder) holder).setElement(getItem(position));
        }
        else if(holder instanceof NewsHotDetailRelateHolder)
        {
            ((NewsHotDetailRelateHolder) holder).setElement(getItem(position));
        }
    }

    public interface NewsHotInterface {
        void onClickImageItem(NewsHotDetailModel entry);
        void onClickVideoItem(NewsHotDetailModel entry);
        void onClickRelateItem(NewsHotModel entry);
    }

    public class NewsHotDetailTextHolder  extends BaseViewHolder {

        private Context mContext;
        private NewsHotDetailModel mEntry;
        private TextView tvContent;

        public NewsHotDetailTextHolder(View convertView, Context context) {
            super(convertView);
            this.mContext = context;

            tvContent = (TextView) convertView.findViewById(R.id.tvContent);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (NewsHotDetailModel) obj;
            initData();
        }

        private void initData() {
            if(mEntry != null)
                tvContent.setText(Html.fromHtml(mEntry.getContent()));
        }
    }

    public class NewsHotDetailImageHolder  extends BaseViewHolder {

        private Context mContext;
        private NewsHotDetailModel mEntry;
        private ImageView imvImage;
        private NewsHotDetailAdapter.NewsHotInterface listener;

        public NewsHotDetailImageHolder(View convertView, Context context, NewsHotDetailAdapter.NewsHotInterface listener) {
            super(convertView);
            this.mContext = context;
            this.listener = listener;
            imvImage = (ImageView) convertView.findViewById(R.id.imvImage);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (NewsHotDetailModel) obj;
            initData();
            setListener();
        }

        private void setListener() {
            if(getItemView() != null)
            {
                getItemView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onClickImageItem(mEntry);
                    }
                });
            }
        }

        private void initData() {
            if(mEntry == null) return;
            int widthPixels;
            DisplayMetrics displaymetrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(displaymetrics);
            if (displaymetrics.widthPixels > displaymetrics.heightPixels) {
                // width>height (set nguoc lai)
                widthPixels = displaymetrics.heightPixels;
            } else {
                widthPixels = displaymetrics.widthPixels;
            }

            float ratio = (float) mEntry.getHeight() / (float) mEntry.getWidth();
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imvImage.getLayoutParams();
            layoutParams.width = widthPixels - 2 * Utilities.dpToPixels(10, mContext.getResources());
            layoutParams.height = (int) (layoutParams.width * ratio);
            imvImage.setLayoutParams(layoutParams);

            ImageLoader.setImage(mContext, mEntry.getContent(), imvImage);
        }
    }

    public class NewsHotDetailVideoHolder  extends BaseViewHolder {

        private Context mContext;
        private NewsHotDetailModel mEntry;
        private VideoViewCustom videoView;
        private ImageView btnPlayPause;
        private View mViewProgress;
        private ImageView mImgThumb;
        private NewsHotDetailAdapter.NewsHotInterface listener;
        private Handler mHandler;
        private Runnable runnable = new Runnable() {
            @Override
            public void run() {
                btnPlayPause.setVisibility(View.GONE);
            }
        };

        public NewsHotDetailVideoHolder(View convertView, Context context, NewsHotDetailAdapter.NewsHotInterface listener) {
            super(convertView);
            this.mContext = context;
            this.listener = listener;
            videoView = (VideoViewCustom) convertView.findViewById(R.id.videoView);
            videoView.setScaleType(VideoViewCustom.ScaleType.CENTER_CROP);
            mImgThumb = (ImageView) convertView.findViewById(R.id.img_thumb_content_discovery);
            btnPlayPause = (ImageView) convertView.findViewById(R.id.btnPlayPause);
            mViewProgress = convertView.findViewById(R.id.view_progress_loading_video);
            mHandler = new Handler();

            //Setup size video
            int widthPixels;
            DisplayMetrics displaymetrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(displaymetrics);
            if (displaymetrics.widthPixels > displaymetrics.heightPixels) {
                // width>height (set nguoc lai)
                widthPixels = displaymetrics.heightPixels;
            } else {
                widthPixels = displaymetrics.widthPixels;
            }

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) videoView.getLayoutParams();
            layoutParams.width = widthPixels - 2 * Utilities.dpToPixels(10, mContext.getResources());
            layoutParams.height = (layoutParams.width * 9 / 16);
            videoView.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) mImgThumb.getLayoutParams();
            layoutParams1.width = layoutParams.width;
            layoutParams1.height = layoutParams.height;
            mImgThumb.setLayoutParams(layoutParams1);

            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) mViewProgress.getLayoutParams();
            layoutParams2.width = layoutParams.width;
            layoutParams2.height = layoutParams.height;
            mViewProgress.setLayoutParams(layoutParams2);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (NewsHotDetailModel) obj;
            initData();
            setListener();
        }

        private void setListener() {
            if(getItemView() != null)
            {
                getItemView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onClickVideoItem(mEntry);
                    }
                });


                btnPlayPause.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(videoView != null)
                        {
                            if(videoView.isPlaying())
                            {
                                //Pause
                                btnPlayPause.setImageResource(R.drawable.ic_news_play);
                                videoView.pause();
                                hideBtnPlay();
                            }
                            else
                            {
                                //play
                                btnPlayPause.setImageResource(R.drawable.ic_news_pause);
                                if(videoView.getCurrentState() == VideoViewCustom.STATE_IDLE)
                                {
                                    playVideo();
                                }
                                else
                                {
                                    videoView.start();
                                    hideBtnPlay();
                                }
                            }
                        }
                    }
                });

                videoView.setListener(new VideoViewCustom.MediaPlayerListener() {
                    @Override
                    public void onVideoPrepared() {
                        Log.i(TAG, "onVideoPrepared");
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mViewProgress.setVisibility(View.GONE);
                                mImgThumb.setVisibility(View.GONE);
                            }
                        }, 500);
                    }

                    @Override
                    public void onVideoEnd() {
                        Log.i(TAG, "onVideoEnd");

                    }

                    @Override
                    public void onBufferStart() {
                        mViewProgress.setVisibility(View.VISIBLE);
                        btnPlayPause.setVisibility(View.GONE);
                    }

                    @Override
                    public void onBufferEnd() {
                        mViewProgress.setVisibility(View.GONE);
                        hideBtnPlay();
                    }

                    @Override
                    public void onError() {

                    }
                });

                videoView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mViewProgress.getVisibility() == View.VISIBLE)
                            return;
                        if(btnPlayPause.getVisibility() == View.GONE)
                            hideBtnPlay();
                        else
                            btnPlayPause.setVisibility(View.GONE);
                    }
                });
            }
        }
        private void initData() {
            if(mEntry == null) return;

            mImgThumb.setVisibility(View.VISIBLE);
            ImageLoader.setImage(mContext, mEntry.getContent(), mImgThumb);
        }

        private void playVideo()
        {
            mViewProgress.setVisibility(View.VISIBLE);
            btnPlayPause.setVisibility(View.GONE);

            Uri videoUri = Uri.parse(mEntry.getMedia());
            videoView.setVideoURI(videoUri);
            videoView.setKeepScreenOn(true);
            videoView.start();
        }

        private void hideBtnPlay()
        {
            btnPlayPause.setVisibility(View.VISIBLE);
            mHandler.removeCallbacks(runnable);
            mHandler.postDelayed(runnable, 3000);
        }
    }

    public class NewsHotDetailHeaderHolder  extends BaseViewHolder {

        private Context mContext;
        private NewsHotModel newsHotModel;
        private TextView tvTitle;
        private TextView tvCategory;
        private TextView tvSapo;
        private TextView tvDate;
        private TextView tvLoading;

        public NewsHotDetailHeaderHolder(View convertView, Context context) {
            super(convertView);
            this.mContext = context;

            tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            tvSapo = (TextView) convertView.findViewById(R.id.tvSapo);
            tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            tvCategory = (TextView) convertView.findViewById(R.id.tvCategory);
            tvLoading = (TextView) convertView.findViewById(R.id.tvLoading);
        }

        @Override
        public void setElement(Object obj) {
            newsHotModel = (NewsHotModel) obj;
            initData();
        }

        private void initData() {
            if(newsHotModel == null) return;

            tvTitle.setText(Html.fromHtml(newsHotModel.getTitle()));
            tvCategory.setText(Html.fromHtml(newsHotModel.getCategory()));
            tvSapo.setText(Html.fromHtml(newsHotModel.getShapo()));
            tvDate.setText(Html.fromHtml(newsHotModel.getDatePub()));
            if(datas.size() > 0)
                tvLoading.setVisibility(View.GONE);
            else
                tvLoading.setVisibility(View.VISIBLE);
        }
    }

    public class NewsHotDetailRelateHeaderHolder  extends BaseViewHolder {

        private Context mContext;
        private NewsHotDetailModel mEntry;
        private TextView tvTitle;

        public NewsHotDetailRelateHeaderHolder(View convertView, Context context) {
            super(convertView);
            this.mContext = context;

            tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (NewsHotDetailModel) obj;
            initData();
        }

        private void initData() {
            if(mEntry == null) return;

            tvTitle.setText(Html.fromHtml(mEntry.getContent()));
        }
    }

    public class NewsHotDetailRelateHolder  extends BaseViewHolder {
        private NewsHotInterface mListener;
        private Context mContext;
        private NewsHotModel mEntry;
        private View parentView;
        private ImageView imvAvatar;
        private TextView tvTitle;
        private TextView tvCategory;

        public NewsHotDetailRelateHolder(View convertView, Context context, NewsHotInterface listener) {
            super(convertView);
            this.mContext = context;
            this.mListener = listener;
            //
            parentView = convertView.findViewById(R.id.holder_parent);
            imvAvatar = (ImageView) convertView.findViewById(R.id.imvImage);
            tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            tvCategory = (TextView) convertView.findViewById(R.id.tvCategory);

        }

        @Override
        public void setElement(Object obj) {
            mEntry = (NewsHotModel) obj;
            drawHolderDetail();
            setListener();
        }

        private void setListener() {
            if(parentView != null)
            {
                parentView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onClickRelateItem(mEntry);
                    }
                });
            }
        }

        private void drawHolderDetail() {
            if(mEntry == null) return;

            tvTitle.setText(mEntry.getTitle());
            tvCategory.setText(mEntry.getCategory());
            ImageLoader.setImage(mContext, mEntry.getImage169(), imvAvatar);
        }
    }
}
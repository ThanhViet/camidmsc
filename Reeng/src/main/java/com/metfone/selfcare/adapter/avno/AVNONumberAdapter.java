package com.metfone.selfcare.adapter.avno;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.NumberAVNO;
import com.metfone.selfcare.holder.AVNONumberHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 2/25/2018.
 */

public class AVNONumberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = AVNONumberAdapter.class.getSimpleName();

    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private ArrayList<NumberAVNO> listItem;
    private RecyclerClickListener mRecyclerClickListener;


    public AVNONumberAdapter(ApplicationController application, ArrayList<NumberAVNO> listItem) {
        this.mApplication = application;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listItem = listItem;
    }

    public void setListItem(ArrayList<NumberAVNO> listItem) {
        this.listItem = listItem;
    }

    public void addListItem(ArrayList<NumberAVNO> listItem) {
        this.listItem.addAll(listItem);
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AVNONumberHolder holder;
        View view = mLayoutInflater.inflate(R.layout.holder_avno_number, parent, false);
        holder = new AVNONumberHolder(view);
        if (mRecyclerClickListener != null) {
            holder.setRecyclerClickListener(mRecyclerClickListener);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AVNONumberHolder avno = (AVNONumberHolder) holder;
        if (mRecyclerClickListener != null) {
            avno.setViewClick(position, listItem.get(position));
        }
        avno.setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }

    public ArrayList<NumberAVNO> getListItem() {
        return listItem;
    }

}

package com.metfone.selfcare.adapter;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by thanhnt72 on 8/29/2018.
 */

public class GameIQWinnerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = GameIQWinnerAdapter.class.getSimpleName();
    private ArrayList<UserInfo> listWinner;
    private ApplicationController mApp;
    private Resources mRes;
    private RecyclerClickListener listener;


    public GameIQWinnerAdapter(ArrayList<UserInfo> listWinner, ApplicationController mApp) {
        this.listWinner = listWinner;
        this.mApp = mApp;
        this.mRes = mApp.getResources();
    }

    public void setListener(RecyclerClickListener listener) {
        this.listener = listener;
    }

    public void setListWinner(ArrayList<UserInfo> listWinner) {
        this.listWinner = listWinner;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mApp).inflate(R.layout.holder_winner_gameiq, parent, false);
        WinnerHolder winnerHolder = new WinnerHolder(view);
        if (listener != null) {
            winnerHolder.setRecyclerClickListener(listener);
        }
        return winnerHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((WinnerHolder) holder).setElement(listWinner.get(position));
        ((WinnerHolder) holder).setViewClick(position, listWinner.get(position));
    }

    @Override
    public int getItemCount() {
        if (listWinner == null || listWinner.isEmpty())
            return 0;
        return listWinner.size();
    }


    class WinnerHolder extends BaseViewHolder {
        @BindView(R.id.imgAvatar)
        CircleImageView imgAvatar;
        @BindView(R.id.tvAvatar)
        TextView tvAvatar;
        @BindView(R.id.flAvatar)
        FrameLayout flAvatar;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvPrize)
        TextView tvPrize;
        @BindView(R.id.rlViewHolder)
        RelativeLayout rlViewHolder;

        public WinnerHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setElement(Object obj) {
            UserInfo userInfo = (UserInfo) obj;
            setViewHolderContent(userInfo);
        }


        private void setViewHolderContent(UserInfo userInfo) {
            int sizeAvatar = (int) mRes.getDimension(R.dimen.avatar_small_size);
            if (userInfo != null) {
                String mFriendJid = userInfo.getMsisdn();
                String mFriendName = userInfo.getName();
                String myNumber = mApp.getReengAccountBusiness().getJidNumber();
                String myName = mApp.getReengAccountBusiness().getUserName();
                if (TextUtils.isEmpty(mFriendJid)) return;
                if (mFriendJid.equals(myNumber)) {
                    mFriendName = myName;
                    mApp.getAvatarBusiness().setMyAvatar(imgAvatar, tvAvatar, tvAvatar, mApp.getReengAccountBusiness().getCurrentAccount(), null);
                    tvName.setTextColor(ContextCompat.getColor(mApp, R.color.color_iq_game_green));
                } else {
                    tvName.setTextColor(ContextCompat.getColor(mApp, R.color.white));
                    PhoneNumber mFriendPhone = mApp.getContactBusiness().getPhoneNumberFromNumber(mFriendJid);
                    if (mFriendPhone != null) {// luu danh ba
                        mFriendName = mFriendPhone.getName();
                        mApp.getAvatarBusiness().setPhoneNumberAvatar(imgAvatar, tvAvatar, mFriendPhone, sizeAvatar);
                    } else {
                        if (TextUtils.isEmpty(mFriendName)) {
                            mFriendName = Utilities.hidenPhoneNumber(mFriendJid);
                        }

                        if (!"0".equals(userInfo.getAvatar())) {
                            String mFriendAvatarUrl = mApp.getAvatarBusiness().getAvatarUrl(userInfo.getAvatar(), mFriendJid, sizeAvatar);
                            mApp.getAvatarBusiness().setAvatarOnMedia(imgAvatar, tvAvatar, mFriendAvatarUrl, mFriendJid, mFriendName, sizeAvatar);
                        } else
                            imgAvatar.setImageResource(R.drawable.ic_avatar_default);
                    }
                }
                tvName.setText(mFriendName);
                tvPrize.setText(userInfo.getPrize());
            } else {
                tvName.setText("");
                tvPrize.setText("");
                imgAvatar.setImageResource(R.drawable.ic_avatar_default);
            }

        }
    }

}

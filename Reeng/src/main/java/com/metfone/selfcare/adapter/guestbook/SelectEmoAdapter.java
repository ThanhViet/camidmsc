package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.EmoItem;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class SelectEmoAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    private OnEmoItemClick mListener;
    private ArrayList<EmoItem> emoItems;

    public SelectEmoAdapter(Context context, ArrayList<EmoItem> emoItems, OnEmoItemClick listener) {
        this.mContext = context;
        this.emoItems = emoItems;
        this.mListener = listener;
        this.inflater = LayoutInflater.from(mContext);
    }

    public void setEmoItems(ArrayList<EmoItem> emoItems) {
        this.emoItems = emoItems;
    }

    @Override
    public int getCount() {
        if (emoItems == null) return 0;
        return emoItems.size();
    }

    @Override
    public Object getItem(int position) {
        return emoItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.guest_book_emo_item, parent, false);
            holder = new ViewHolder(convertView);
           /* holder.mImgEmo.getLayoutParams().height = mContext.getResources().
                    getDimensionPixelSize(R.dimen.sticker_grid_height);
            holder.mImgEmo.getLayoutParams().width = mContext.getResources()
                    .getDimensionPixelSize(R.dimen.sticker_grid_height);
            int paddingImage = mContext.getResources().getDimensionPixelSize(R.dimen.margin_more_content_5);
            holder.mImgEmo.setPadding(paddingImage, paddingImage, paddingImage, paddingImage);*/
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.setElement((EmoItem) getItem(position));
        return convertView;
    }

    private class ViewHolder {
        private View mImgParent;
        private ImageView mImgEmo;

        public ViewHolder(View rootView) {
            mImgParent = rootView.findViewById(R.id.layout_emo_item);
            mImgEmo = (ImageView) rootView.findViewById(R.id.item);
        }

        public void setElement(final EmoItem item) {
            mImgParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onEmoClick(item);
                }
            });
            String url = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(item.getPath());
            ImageLoaderManager.getInstance(mContext).displayGuestBookEmo(mImgEmo, url,
                    item.getWidth(), item.getHeight());
            /*if (color.isSelected()) {
                mImgSelected.setVisibility(View.VISIBLE);
            } else {
                mImgSelected.setVisibility(View.GONE);
            }*/
        }
    }

    public interface OnEmoItemClick {
        void onEmoClick(EmoItem item);
    }
}
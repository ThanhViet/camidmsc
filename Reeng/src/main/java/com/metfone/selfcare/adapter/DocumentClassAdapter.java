package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.DocumentClass;
import com.metfone.selfcare.holder.DocumentClassHolder;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by Toanvk2 on 5/6/2017.
 */

public class DocumentClassAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = DocumentClassAdapter.class.getSimpleName();
    private Context mContext;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;
    private ArrayList<DocumentClass> listItems;
    private DocumentClassHolder.OnDocumentListener mListener;

    public DocumentClassAdapter(Context context, ArrayList<DocumentClass> items, DocumentClassHolder.OnDocumentListener listener) {
        this(context);
        this.listItems = items;
        this.mListener = listener;
    }

    public DocumentClassAdapter(Context context) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
    }

    public void setListItems(ArrayList<DocumentClass> items) {
        this.listItems = items;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        if (listItems == null) {
            return 0;
        }
        return listItems.size();
    }

    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

   /* @Override
    public int getItemViewType(int position) {
        return 0;
    }*/

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        BaseViewHolder holder;
        View view = inflater.inflate(R.layout.holder_document_class, parent, false);
        holder = new DocumentClassHolder(view, mContext, mListener);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + position);
        ((BaseViewHolder) holder).setElement(getItem(position));
        if (mRecyclerClickListener != null)
            ((BaseViewHolder) holder).setViewClick(position, getItem(position));
    }
}

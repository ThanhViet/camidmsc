package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.call.CallHistory;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.call.CallConstant;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.call.CallHistoryHolder;
import com.metfone.selfcare.holder.contact.BaseContactHolder;
import com.metfone.selfcare.holder.contact.PhoneNumberViewHolder;
import com.metfone.selfcare.holder.contact.SectionViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 10/12/2016.
 */
public class CallHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = CallHistoryAdapter.class.getSimpleName();
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Object> listItem;
    private RecyclerClickListener mRecyclerClickListener;
    private HolderHistoryListener mHolderHistoryListener;
    private int nameWidth;

    public CallHistoryAdapter(ApplicationController application, ArrayList<Object> listItem) {
        this.mApplication = application;
        this.mLayoutInflater = (LayoutInflater) mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listItem = listItem;
    }

    public void setNameWidth(int nameWidth) {
        this.nameWidth = nameWidth;
    }

    public void setListData(ArrayList<Object> list) {
        this.listItem = list;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setHolderHistorylistener(HolderHistoryListener listener) {
        this.mHolderHistoryListener = listener;
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public Object getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof PhoneNumber) {
            PhoneNumber phone = (PhoneNumber) item;
            if (phone.getContactId() == null) {
                return CallConstant.HOLDER_SECTION;
            } else {
                return CallConstant.HOLDER_CONTACT;
            }
        }
        /*else if (item instanceof BannerMocha) {
            return CallConstant.HOLDER_CALL_BANNER;
        } */
        else {
            return CallConstant.HOLDER_HISTORY;
        }
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        Log.d(TAG, "onCreateViewHolder");
        BaseViewHolder holder;
        if (type == CallConstant.HOLDER_CONTACT) {
            View phoneNumberView = mLayoutInflater.inflate(R.layout.holder_contact_viewer, parent, false);
            holder = new PhoneNumberViewHolder(phoneNumberView, mApplication, null);
            ((BaseContactHolder) holder).setType(Constants.CONTACT.CONTACT_VIEW_THREAD_MESSAGE);
        } else if (type == CallConstant.HOLDER_SECTION) {
            View view = mLayoutInflater.inflate(R.layout.holder_contact_section, parent, false);
            holder = new SectionViewHolder(view, mApplication, null);
            ((BaseContactHolder) holder).setType(Constants.CONTACT.CONTACT_VIEW_THREAD_MESSAGE);
        }
        /*else if (type == CallConstant.HOLDER_CALL_BANNER) {
            View bannerView = mLayoutInflater.inflate(R.layout.holder_thread_banner_layout, parent, false);
            holder = new ThreadBannerViewHolder(bannerView, mApplication);
        } */
        else {
            View view = mLayoutInflater.inflate(R.layout.holder_call_history, parent, false);
            holder = new CallHistoryHolder(view, mApplication, mHolderHistoryListener);
        }
        if (mRecyclerClickListener != null) {
            holder.setRecyclerClickListener(mRecyclerClickListener);
        }
        return holder;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + position);
        int type = getItemViewType(position);
        if (type == CallConstant.HOLDER_HISTORY) {
            ((CallHistoryHolder) holder).setNameWidth(nameWidth);
        }
        ((BaseViewHolder) holder).setViewClick(position, getItem(position));
        ((BaseViewHolder) holder).setElement(getItem(position));
    }

    public interface HolderHistoryListener {
        void onActionCall(CallHistory history);

        void onActionVideo(CallHistory history);
    }
}
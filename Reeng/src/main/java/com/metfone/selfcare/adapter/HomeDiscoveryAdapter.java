package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.ContentDiscovery;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/7/17.
 */

public class HomeDiscoveryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private RecyclerClickListener mRecyclerClickListener;

    private ArrayList<ContentDiscovery> listData = new ArrayList<>();

    public HomeDiscoveryAdapter(ApplicationController app, ArrayList<ContentDiscovery> list, RecyclerClickListener
            mRecyclerClickListener) {
        this.mApplication = app;
        this.listData = list;
        this.mRecyclerClickListener = mRecyclerClickListener;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = mLayoutInflater.inflate(R.layout.holder_content_discovery, parent, false);

        DiscoveryViewHolder holder = new DiscoveryViewHolder(view);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        DiscoveryViewHolder discoveryViewHolder = (DiscoveryViewHolder) holder;
        discoveryViewHolder.setElement(listData.get(position));
        discoveryViewHolder.setViewClick(position, holder);
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    private class DiscoveryViewHolder extends BaseViewHolder {

        private AspectImageView mImgContent;
        private TextView mTvwTitle;

        public DiscoveryViewHolder(View itemView) {
            super(itemView);
            mImgContent = itemView.findViewById(R.id.img_thumb_content_discovery);
            mTvwTitle = itemView.findViewById(R.id.tvw_title_content_discovery);
        }

        @Override
        public void setElement(Object obj) {
            final ContentDiscovery content = (ContentDiscovery) obj;
            mTvwTitle.setText(content.getTitle());
            int pos = getAdapterPosition();
            int w, h;
            if ((pos - 7) % 9 == 0 || pos % 9 == 0) {
                w = (int) (mApplication.getWidthPixels() * 0.5);
            } else {
                w = (int) (mApplication.getWidthPixels() * 0.25);
            }
            h = (w * 16) / 9;
            //Glide.with(mApplication).load( content.getImageUrl()).into(mImgContent);
            ImageLoaderManager.getInstance(mApplication).setImageDiscovery(mImgContent, content.getImageUrl(), w, h);
            //ImageLoaderManager.getInstance(mApplication).setImageDiscovery(mImgContent, content.getImageUrl(), 0,0);
//            ImageLoader.setImage(mApplication.getApplicationContext(), content.getImageUrl(), mImgContent);
        }
    }

    public ArrayList<ContentDiscovery> getListData() {
        return listData;
    }

    public void setListData(ArrayList<ContentDiscovery> listData) {
        this.listData = listData;
    }

    public void addItem(ContentDiscovery contentDiscovery, int position) {
        listData.add(position, contentDiscovery);
        notifyItemInserted(position);
    }
}
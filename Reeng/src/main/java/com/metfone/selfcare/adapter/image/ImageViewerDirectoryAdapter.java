package com.metfone.selfcare.adapter.image;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.activity.ImageBrowserActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.IImageBrowser;

/**
 * Created by thanhnt72 on 10/3/2017.
 */
public class ImageViewerDirectoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ImageBrowserActivity mImageBrowserActivity;
    private LayoutInflater infalter;
    private IImageBrowser.SelectImageListener mSelectImageListener;
    private String action;
    private boolean isShowVideo = false;

    // dainv
    int maxItemSelected;

    public ImageViewerDirectoryAdapter(ImageBrowserActivity imageBrowserActivity, IImageBrowser.SelectImageListener
            selectImageListener) {
        mImageBrowserActivity = imageBrowserActivity;
        infalter = (LayoutInflater) imageBrowserActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.action = imageBrowserActivity.getAction();
        isShowVideo = imageBrowserActivity.getParamShowVideo();
        this.mSelectImageListener = selectImageListener;
        maxItemSelected = imageBrowserActivity.getMaxImages();

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = infalter.inflate(R.layout.item_image_viewer_v5, parent, false);
        ImageViewerDirectoryHolder holder = new ImageViewerDirectoryHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ImageViewerDirectoryHolder imageViewerDirectoryHolder = (ImageViewerDirectoryHolder) holder;
        imageViewerDirectoryHolder.setSelectedMax(maxItemSelected == mImageBrowserActivity.getCountSelected());
        imageViewerDirectoryHolder.setElement(mImageBrowserActivity.getListImage().get(position));
        if (position <= 2) {
            imageViewerDirectoryHolder.setPaddingTop(View.VISIBLE);
            imageViewerDirectoryHolder.setPaddingBottom(View.GONE);
        } else if (position >= getItemCount() - 3) {
            imageViewerDirectoryHolder.setPaddingTop(View.GONE);
            imageViewerDirectoryHolder.setPaddingBottom(View.VISIBLE);
        } else {
            imageViewerDirectoryHolder.setPaddingTop(View.GONE);
            imageViewerDirectoryHolder.setPaddingBottom(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        if (mImageBrowserActivity.getListImage() == null || mImageBrowserActivity.getListImage().isEmpty())
            return 0;
        else
            return mImageBrowserActivity.getListImage().size();
    }

    public int getMaxItemSelected() {
        return maxItemSelected;
    }

    public void setMaxItemSelected(int maxItemSelected) {
        this.maxItemSelected = maxItemSelected;
    }

    public class ImageViewerDirectoryHolder extends BaseViewHolder {

        private View convertView;
        private ImageView imgQueue;
        //        CheckBox cbxSelected;
        AppCompatImageView imgSelectedSimple;
        //        RelativeLayout mLayoutInfoMediaFile;
        AppCompatTextView tvwFileSizeInMB, tvIndexSelected;
        AppCompatTextView tvwFileDurationInSecond;
        View viewPaddingTop, viewPaddingBottom;
        private boolean isSelectedMax;

        public ImageViewerDirectoryHolder(View convertView) {
            super(convertView);
            this.convertView = convertView;
            imgQueue = convertView.findViewById(R.id.img_image);
//            cbxSelected =  convertView.findViewById(R.id.cbx_selected_img);
            imgSelectedSimple = convertView.findViewById(R.id.img_selected_simple_img);
//            mLayoutInfoMediaFile = convertView.findViewById(R.id.info_media);
            tvwFileSizeInMB = convertView.findViewById(R.id.txt_media_size);
            tvwFileDurationInSecond = convertView.findViewById(R.id.txt_media_duration);
            tvIndexSelected = convertView.findViewById(R.id.tvIndexSelect);
            viewPaddingBottom = convertView.findViewById(R.id.viewPaddingBottom);
            viewPaddingTop = convertView.findViewById(R.id.viewPaddingTop);
            if (maxItemSelected == 1) {
                tvIndexSelected.setVisibility(View.GONE);
            }
        }

        public void setSelectedMax(boolean selectedMax) {
            isSelectedMax = selectedMax;
        }

        @Override
        public void setElement(Object obj) {
            ImageInfo image = (ImageInfo) obj;
//            cbxSelected.setVisibility(View.VISIBLE);
            imgSelectedSimple.setVisibility(View.GONE);
            if (action.equals(ImageBrowserActivity.ACTION_SIMPLE_PICK)) {
                // Gone check box with simple pick
//                cbxSelected.setVisibility(View.GONE);
//                if (image.isSelected()) {
//                    // change icon
////                    imgSelectedSimple.setVisibility(View.VISIBLE);
//                    tvIndexSelected.setText(String.valueOf(image.getIndexSelect()));
//                } else {
////                    imgSelectedSimple.setVisibility(View.GONE);
//                    tvIndexSelected.setVisibility(View.GONE);
//                }
            }

            if (isSelectedMax && !image.isSelected()) {
                if (maxItemSelected > 1) {
                    imgQueue.setEnabled(false);
                }
                imgQueue.setSelected(image.isSelected());
            } else {
                imgQueue.setSelected(image.isSelected());
                if (maxItemSelected > 1) {
                    imgQueue.setEnabled(true);
                }
            }
            if (image.isSelected()) {
                // change icon
                if (maxItemSelected > 1) {
                    tvIndexSelected.setVisibility(View.VISIBLE);
                    tvIndexSelected.setText(String.valueOf(image.getIndexSelect()));
                }
            } else {
//                    imgSelectedSimple.setVisibility(View.GONE);
                tvIndexSelected.setVisibility(View.GONE);
            }
//            cbxSelected.setChecked(image.isSelected());
            String imagePath = image.getImagePath();
//        Log.i("ImageViewerAdapter", "setImage =" + imagePath);
            if (image.getVideoContentURI() != null && !TextUtils.isEmpty(image.getVideoContentURI())) {
//            Log.i("ImageViewerAdapter", "uri filepath: " + image.getVideoContentURI());
                ImageLoaderManager.getInstance(mImageBrowserActivity).displayThumbnailVideo(image.getVideoContentURI
                        (), imgQueue);
            } else {
                ImageLoaderManager.getInstance(mImageBrowserActivity).displayThumbleOfGallery(imagePath, imgQueue);
            }

            setListener(image);

            if (isShowVideo) {
//                mLayoutInfoMediaFile.setVisibility(View.VISIBLE);
                tvwFileDurationInSecond.setVisibility(View.VISIBLE);
                tvwFileSizeInMB.setVisibility(View.VISIBLE);
/*            holder.tvwFileSizeInMB.setText(
                    String.format(mImageBrowserActivity.getString(R.string.file_size_in_mb),
                            FileHelper.getSizeMediaFileInMB(image.getImagePath())));*/
                tvwFileSizeInMB.setText(String.format(mImageBrowserActivity.getString(R.string.file_size_in_mb),
                        image.getFileSize() / 1024.0 / 1024.0));
                long duration = image.getDurationInSecond();
                tvwFileDurationInSecond.setText(TimeHelper.getDuraionMediaFile(duration));
            } else {
//                mLayoutInfoMediaFile.setVisibility(View.GONE);
                tvwFileDurationInSecond.setVisibility(View.GONE);
                tvwFileSizeInMB.setVisibility(View.GONE);
            }
        }

        private void setListener(final ImageInfo image) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSelectImageListener != null) {
                        mSelectImageListener.onSelectImage(null, image, getAdapterPosition());
                    }
                }
            });

//            cbxSelected.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mSelectImageListener != null) {
//                        mSelectImageListener.onSelectImage(null, image);
//                    }
//                }
//            });
        }

        public ImageView getImgQueue() {
            return imgQueue;
        }

        public void setPaddingTop(int vis) {
            viewPaddingTop.setVisibility(vis);
        }

        public void setPaddingBottom(int vis) {
            viewPaddingBottom.setVisibility(vis);
        }
    }


}

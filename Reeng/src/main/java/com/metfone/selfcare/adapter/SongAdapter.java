package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.SongHolder;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;


public class SongAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Object> listObj;
    private Context mContext;
    private ClickListener.IconListener mCallBack;
    private RecyclerClickListener mRecyclerClickListener;

    public SongAdapter(Context context, ArrayList<Object> listObj, ClickListener.IconListener callBack) {
        this.listObj = listObj;
        this.mContext = context;
        this.mCallBack = callBack;
    }

    public void setListObject(ArrayList<Object> listObj) {
        this.listObj = listObj;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (listObj == null || listObj.isEmpty())
            return 0;
        return listObj.size();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.item_upload_music_v5, parent, false);
        SongHolder songHolder = new SongHolder(convertView, mContext, mCallBack);
        convertView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mRecyclerClickListener != null)
                mRecyclerClickListener.onClick(convertView, songHolder.getAdapterPosition(), listObj.get(songHolder.getAdapterPosition()));
            }
        });
//        songHolder.setRecyclerClickListener(mRecyclerClickListener);
        return songHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((SongHolder) holder).setElement(listObj.get(position));
        ((BaseViewHolder) holder).setViewClick(position, listObj.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

}
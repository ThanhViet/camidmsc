package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.BookVote;
import com.metfone.selfcare.holder.guestbook.BookVoteHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/26/2017.
 */
public class GuestBookVotesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private LayoutInflater inflater;
    private BookVoteHolder.OnBookVoteListener mListener;
    private RecyclerClickListener mRecyclerClickListener;
    private ArrayList<BookVote> listItems;

    public GuestBookVotesAdapter(Context context, ArrayList<BookVote> items,
                                 BookVoteHolder.OnBookVoteListener listener) {
        this.mContext = context;
        this.mListener = listener;
        this.inflater = LayoutInflater.from(mContext);
        this.listItems = items;
    }

    public void setListItems(ArrayList<BookVote> items) {
        this.listItems = items;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        if (listItems == null) {
            return 0;
        }
        return listItems.size();
    }

    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = inflater.inflate(R.layout.holder_guest_book_vote, parent, false);
        BookVoteHolder holder = new BookVoteHolder(view, mContext, mListener);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((BookVoteHolder) holder).setElement(getItem(position));
        if (mRecyclerClickListener != null)
            ((BookVoteHolder) holder).setViewClick(position, getItem(position));
    }
}

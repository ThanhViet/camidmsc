package com.metfone.selfcare.adapter;

import static com.metfone.selfcare.network.metfoneplus.MetfonePlusClient.LANGUAGE;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChangeNumberMetfoneActivity;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.AddPhoneViewHolder> {
    UserInfoBusiness userInfoBusiness;
    private OnItemServiceClickListener onItemClickListener;
    private ArrayList<ServicesModel> items;
    private Activity activity;

    public ServicesAdapter(Activity activity) {
        this.activity = activity;
        items = new ArrayList<>();
        userInfoBusiness = new UserInfoBusiness(activity);
    }

    public void setOnItemClickListener(OnItemServiceClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @NonNull
    @Override
    public AddPhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AddPhoneViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull AddPhoneViewHolder holder, int position) {
        holder.bindView(holder.getItemView(), getItem(position));
    }

    public ServicesModel getItem(int position) {
        return items.get(position);
    }

    public void submitList(ArrayList<ServicesModel> servicesModels) {
        items.clear();
        items.addAll(servicesModels);
        notifyDataSetChanged();

    }

    public Activity getActivity() {
        return activity;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface OnItemServiceClickListener {
        void onItemClickDelete(int position, int serviceId, String phoneNumber, String serviceName);

        void onItemUpdateInfo(int position, int serviceId, String phoneNumber, String serviceName);
    }

    public class AddPhoneViewHolder extends BaseViewHolder {
        @BindView(R.id.tvTitle)
        CamIdTextView tvTitle;
        @BindView(R.id.tvAddAnother)
        CamIdTextView tvAddAnother;
        @BindView(R.id.clHeader)
        ConstraintLayout clHeader;
        @BindView(R.id.clContent)
        ConstraintLayout clContent;
        @BindView(R.id.ivMark)
        ImageView ivMark;
        @BindView(R.id.tvPhone)
        AppCompatTextView tvPhone;
        @BindView(R.id.ivUpdate)
        ImageView ivUpdate;
        @BindView(R.id.ivDelete)
        ImageView ivDelete;

        public AddPhoneViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public AddPhoneViewHolder(ViewGroup parent) {
            this(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_services, parent, false));
        }

        public void bindView(View itemView, ServicesModel services) {
            ivDelete.setVisibility(View.GONE);
            String phoneNumber = null;
            if (userInfoBusiness.getUser() != null) {
                phoneNumber = userInfoBusiness.getUser().getPhone_number();
            }
            if (phoneNumber != null && services.getPhone_number() != null) {
                String phoneService = services.getPhone_number().substring(0, 2).concat(services.getPhone_number().substring(2));
                Log.d("TAG", "bindView: " + phoneNumber.concat(" ").concat(phoneService));
                if (!phoneService.equals(phoneNumber)) {
                    ivDelete.setVisibility(View.VISIBLE);
                } else {
                    ivDelete.setVisibility(View.GONE);
                }
            }
            if (services.getService_name().equals("Mobile") ) {
                ivUpdate.setVisibility(View.VISIBLE);
            } else {
                ivUpdate.setVisibility(View.GONE);
            }
            if (services.isHeader()) {
                clHeader.setVisibility(View.VISIBLE);
                if ("km".equals(LANGUAGE) && services.getService_name().equals("Mobile")) {
                    tvTitle.setText(itemView.getContext().getString(R.string.mobile_service));
                } else if ("en".equals(LANGUAGE) && services.getService_name().equals("Mobile")) {
                    tvTitle.setText(services.getService_name());
                } else {
                    tvTitle.setText(services.getService_name());
                }
                if (services.getPhone_number() == null) {
                    clContent.setVisibility(View.GONE);
                } else {
                    clContent.setVisibility(View.VISIBLE);
                }
            } else {
                clHeader.setVisibility(View.GONE);
            }


            ServicesModel servicesTmp = items.get(getAdapterPosition());
            if (servicesTmp.getService_id() == 2) {
                if (servicesTmp.getPhone_number() != null) {
                    tvAddAnother.setText(tvAddAnother.getContext().getResources().getString(R.string.add_another));
                } else {
                    tvAddAnother.setText(tvAddAnother.getContext().getResources().getString(R.string.add));
                }
            } else {
                tvAddAnother.setText(tvAddAnother.getContext().getResources().getString(R.string.add_another));
            }
            tvAddAnother.setOnClickListener(view -> {
                if (servicesTmp.getService_id() == 2) {
                    if (UserInfoBusiness.checkEmptyPhoneNumber(view.getContext())) {
                        NavigateActivityHelper.navigateToMetfoneAddNumber((BaseSlidingFragmentActivity) getActivity());
                    } else {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(view.getContext());
                        UserInfo userInfo = userInfoBusiness.getUser();
                        if (Utilities.isMetfoneNumber(userInfo.getPhone_number())) {
                            NavigateActivityHelper.navigateToAddAccountActivity((BaseSlidingFragmentActivity) getActivity(), servicesTmp.getService_id());
                        } else {
                            NavigateActivityHelper.navigateToMetfoneAddNumber((BaseSlidingFragmentActivity) getActivity());
                        }
                    }
                } else {
                    if (UserInfoBusiness.checkEmptyPhoneNumber(view.getContext())) {
                        NavigateActivityHelper.navigateAddLoginActivity((BaseSlidingFragmentActivity) getActivity());
                    } else {
                        NavigateActivityHelper.navigateToServiceScreenActivity((BaseSlidingFragmentActivity) getActivity(), servicesTmp.getService_id());
                    }
                }
            });
//            if (items != null && items.size() > 1){
//                if (items.get(1).getService_id() == 1 && getAdapterPosition() == 1) {
//                    ivMark.setVisibility(View.VISIBLE);
//                }else {
//                    ivMark.setVisibility(View.GONE);
//                }
//            }
            if (services.getPhone_number() != null && services.getPhone_number().length() > 2 && services.getService_id() != 2 && services.getMetfonePlus() == 1) {
                tvPhone.setText("0" + services.getPhone_number().substring(0, 2) + " " + services.getPhone_number().substring(2));
                ivMark.setVisibility(View.VISIBLE);
            } else if (services.getPhone_number() != null && services.getPhone_number().length() > 2) {
                ivMark.setVisibility(View.GONE);
                tvPhone.setText(services.getPhone_number().substring(0, 2) + " " + services.getPhone_number().substring(2));
            }

            ivUpdate.setOnClickListener(v -> onItemClickListener.onItemUpdateInfo(getAdapterPosition(), servicesTmp.getService_id(), servicesTmp.getPhone_number(), servicesTmp.getService_name()));
            ivDelete.setOnClickListener(v -> onItemClickListener.onItemClickDelete(getAdapterPosition(), servicesTmp.getService_id(), servicesTmp.getPhone_number(), servicesTmp.getService_name()));
            itemView.setOnClickListener(view -> {
                if (servicesTmp.getService_id() != 2) {
                    itemView.getContext().startActivity(new Intent(ivMark.getContext(), ChangeNumberMetfoneActivity.class));
                }
            });


        }

        @Override
        public void setElement(Object obj) {

        }
    }
}

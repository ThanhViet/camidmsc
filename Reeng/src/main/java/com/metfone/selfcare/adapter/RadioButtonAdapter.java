package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.holder.AbsContentHolder;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 3/11/2016.
 */
public class RadioButtonAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private ArrayList<ItemContextMenu> mItems = new ArrayList<>();
    private Context mContext;

    public RadioButtonAdapter(Context context) {
        mContext = context;
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // set list item
    public void setListItem(ArrayList<ItemContextMenu> list) {
        mItems = list;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        ItemContextMenu item = (ItemContextMenu) getItem(position);
        return item.getActionTag();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemContextMenu item = (ItemContextMenu) getItem(position);
        RadioButtonHolder holder;
        if (convertView == null) {
            holder = new RadioButtonHolder(mContext);
            holder.initHolder(parent, convertView, position, layoutInflater);
        } else {
            holder = (RadioButtonHolder) convertView.getTag();
        }
        holder.setElemnts(item);
        return holder.getConvertView();
    }

    private class RadioButtonHolder extends AbsContentHolder {
        private TextView mTvwContent;
        private ImageView mImgRadioButton;

        public RadioButtonHolder(Context context) {
            setContext(context);
        }

        @Override
        public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
            View convertView = layoutInflater.inflate(R.layout.holder_radio_button_menu, parent, false);
            mTvwContent = (TextView) convertView.findViewById(R.id.item_context_menu_text);
            mImgRadioButton = (ImageView) convertView.findViewById(R.id.item_context_menu_icon);
            convertView.setTag(this);
            setConvertView(convertView);
        }

        @Override
        public void setElemnts(Object obj) {
            ItemContextMenu item = (ItemContextMenu) obj;
            setViewHolder(item);
        }

        private void setViewHolder(ItemContextMenu item) {
            mTvwContent.setText(item.getText());
            mImgRadioButton.setSelected(item.isChecked());
            Log.d("RadioButtonAdapter", "setViewHolder: " + item.isChecked());

        }
    }
}

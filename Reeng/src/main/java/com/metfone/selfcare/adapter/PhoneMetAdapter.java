package com.metfone.selfcare.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.model.account.ServicesModel;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.metfone.selfcare.network.metfoneplus.MetfonePlusClient.LANGUAGE;

public class PhoneMetAdapter extends
        BaseAdapter<PhoneMetAdapter.PhoneMetViewHolder, ServicesModel> {
    private int lastSelectedPosition = 1;

    public PhoneMetAdapter(Activity activity) {
        super(activity);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @NonNull
    @Override
    public PhoneMetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PhoneMetViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull PhoneMetViewHolder holder, int position) {
        holder.bindView(getItem(position), position);
    }

    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }

    public void setLastSelectedPosition(int lastSelectedPosition) {
        this.lastSelectedPosition = lastSelectedPosition;
    }

    public class PhoneMetViewHolder extends BaseViewHolder {
        @BindView(R.id.tvMetfone)
        TextView tvMetfone;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.rbSelect)
        ImageView rbSelect;
        @BindView(R.id.clContent)
        ConstraintLayout clContent;

        public PhoneMetViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            inntView();
        }


        public PhoneMetViewHolder(ViewGroup parent) {
            this(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_metfone, parent, false));
        }

        public void bindView(ServicesModel services, int pos) {
            if (services.isHeader()) {
                tvTitle.setVisibility(View.VISIBLE);
                if (services.getPhone_number() == null) {
                    clContent.setVisibility(View.GONE);
                }
            } else {
                tvTitle.setVisibility(View.GONE);
            }
            if ("km".equals(LANGUAGE) && services.getService_name().equals("Mobile")){
                tvTitle.setText(tvTitle.getContext().getString(R.string.mobile_service));
            }else {
                tvTitle.setText(services.getService_name());
            }
            if (services.getPhone_number() != null && services.getPhone_number().length() > 2) {
                tvMetfone.setText((services.getService_id() == 2?"": "0") + services.getPhone_number().substring(0, 2) + " " + services.getPhone_number().substring(2));
            }
            //     rbSelect.setChecked(lastSelectedPosition == pos);
            if (lastSelectedPosition == pos) {
                rbSelect.setImageResource(R.drawable.icon_radio_red);
            } else {
                rbSelect.setImageResource(R.drawable.icon_radio_white);
            }

            clContent.setOnClickListener(v -> {
//                notifyItemChanged(lastSelectedPosition);
                lastSelectedPosition = getAdapterPosition();
                notifyDataSetChanged();
            });
        }

        private void inntView() {


        }


        @Override
        public void setElement(Object obj) {

        }
    }
}
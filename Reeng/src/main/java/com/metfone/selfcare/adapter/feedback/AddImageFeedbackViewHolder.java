package com.metfone.selfcare.adapter.feedback;

import android.view.View;

import com.metfone.selfcare.holder.BaseViewHolder;

public class AddImageFeedbackViewHolder extends BaseViewHolder {

    public AddImageFeedbackViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setElement(Object obj) {
        getItemView().getLayoutParams().height = getItemView().getWidth();
        getItemView().invalidate();
    }
}

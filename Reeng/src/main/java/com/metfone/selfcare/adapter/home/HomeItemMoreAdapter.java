package com.metfone.selfcare.adapter.home;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.model.home.ItemMoreHome;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 12/11/2018.
 */

public class HomeItemMoreAdapter extends RecyclerView.Adapter<HomeItemMoreAdapter.ItemMoreHolder> {

    private ApplicationController mApp;
    private ArrayList<ItemMoreHome> listItem = new ArrayList<>();
    private RecyclerClickListener listener;
    private boolean isFeature;

    public HomeItemMoreAdapter(ApplicationController mApp, ArrayList<ItemMoreHome> listItem, boolean isFeature) {
        this.mApp = mApp;
        this.listItem = listItem;
        this.isFeature = isFeature;
    }

    public void setListItem(ArrayList<ItemMoreHome> listItem) {
        this.listItem = listItem;
    }

    public void setListener(RecyclerClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ItemMoreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_more_home_holder, parent, false);
        ItemMoreHolder itemViewHolder = new ItemMoreHolder(view);
        if (listener != null) {
            itemViewHolder.setRecyclerClickListener(listener);
        }
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemMoreHolder holder, int position) {
        holder.setElement(listItem.get(position));
        holder.setViewClick(position, listItem.get(position));
    }

    @Override
    public int getItemCount() {
        return listItem == null ? 0 : listItem.size();
    }


    public class ItemMoreHolder extends BaseViewHolder {

        private CircleImageView ivItemHome;
        private TextView tvItemHome;

        public ItemMoreHolder(View itemView) {
            super(itemView);
            ivItemHome = itemView.findViewById(R.id.ivHomeItem);
            tvItemHome = itemView.findViewById(R.id.tvHomeItem);
            if (isFeature)
                tvItemHome.setLines(2);
            else
                tvItemHome.setLines(1);
        }

        @Override
        public void setElement(Object obj) {
            ItemMoreHome itemMoreHome = (ItemMoreHome) obj;
            tvItemHome.setText(itemMoreHome.getTitle());
            if (TextUtils.isEmpty(itemMoreHome.getImgUrl())) {
                Glide.with(mApp).load(itemMoreHome.getResId()).into(ivItemHome);
//                ivItemHome.setImageResource(itemMoreHome.getResId());
            } else {
                ImageLoaderManager.getInstance(mApp).displayImageBanner(itemMoreHome.getImgUrl(), ivItemHome,
                        itemMoreHome.getResId());
            }
        }
    }
}

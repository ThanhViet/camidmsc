package com.metfone.selfcare.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.TrackItem;
import com.metfone.selfcare.model.tab_video.Resolution;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.QualityVideoDialog;

import java.util.List;

public class QualityAdapter extends RecyclerView.Adapter<QualityAdapter.QualityHolder> {

    private List<TrackItem> mListResolution;
    private QualityVideoListener mQualityCallback;

    public QualityAdapter(List<TrackItem> listResolution, QualityVideoListener qualityVideoListener) {
        this.mListResolution = listResolution;
        this.mQualityCallback = qualityVideoListener;
    }

    @NonNull
    @Override
    public QualityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quanlity, parent, false);
        return new QualityHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QualityHolder holder, int position) {
        if(TrackItem.AUTO.equals(mListResolution.get(position).getLanguageCode())){
            holder.tvQuality.setText("Auto");
        }else{
            holder.tvQuality.setText(getNameResolution(mListResolution.get(position).getWidth(), mListResolution.get(position).getHeight()));
        }
    }

    @Override
    public int getItemCount() {
        return mListResolution == null ? 0 : mListResolution.size();
    }

    public class QualityHolder extends RecyclerView.ViewHolder {

        TextView tvQuality;

        public QualityHolder(@NonNull View itemView) {
            super(itemView);
            tvQuality = itemView.findViewById(R.id.tv_quanlity);
            itemView.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (mQualityCallback != null) {
                        mQualityCallback.onQualityVideo(mListResolution.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    private String getNameResolution(int width, int height) {
        if (height <= 480) {
            return height + "p";
        } else if (width == 1280 && height == 720) {
            return "720 HD";
        } else if (width == 1920 && height == 1080) {
            return "1080 FHD";
        } else if (width == 2560 && height == 1440) {
            return "1440 QHD";
        } else if (width == 2048 && height == 1080) {
            return "2k QHD";
        } else if (width == 3840 && height == 2160) {
            return "4k UHD";
        } else if (width == 7680 && height == 4320) {
            return "8K UHD";
        }
        return width + "x" + height;
    }

    public interface QualityVideoListener {
        void onQualityVideo(TrackItem item);
    }
}

package com.metfone.selfcare.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PollItem;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.fragment.message.PollDetailFragmentV2;
import com.metfone.selfcare.fragment.message.PollDetailFragmentV2.OnItemVoteListener;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.holder.onmedia.feeds.ItemInvisibleHolder;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.view.PollLayout;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by thanhnt72 on 10/30/2018.
 */

public class VoteAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> implements OnItemVoteListener {

    public static final String ADD_POLL = "ADD_POLL";
    private static final String TAG = VoteAdapter.class.getSimpleName();

    public static final int TYPE_GONE = -1;
    public static final int TYPE_POLL = 0;
    public static final int TYPE_ADD_POLL = 1;

    private @Nullable
    OnItemVoteListener onItemVoteListener;

    private PollObject pollObject;
    private ThreadMessage mThreadMessage;
    private boolean isPreview;
    private ApplicationController mApp;
    private int totalVote;

    public VoteAdapter(ApplicationController act) {
        super(act);
        mApp = act;
    }

    public void setPreview(boolean preview) {
        isPreview = preview;
    }

    public void setTotalVote(int totalVote) {
        this.totalVote = totalVote;
    }

    public int getTotalVote() {
        return totalVote;
    }

    public void setOnItemVoteListener(@Nullable OnItemVoteListener onItemVoteListener) {
        this.onItemVoteListener = onItemVoteListener;
    }

    public void setPollObject(PollObject pollObject) {
        this.pollObject = pollObject;
    }

    public void setThreadMessage(ThreadMessage mThreadMessage) {
        this.mThreadMessage = mThreadMessage;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = items.get(position);
        if (isPreview) {
//            if (position > 2) return TYPE_GONE;
            return TYPE_POLL;
        } else {
            if (Utilities.equals(ADD_POLL, item)) {
                return TYPE_ADD_POLL;
            } else {
                return TYPE_POLL;
            }
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_ADD_POLL:
                onShowAddClick(viewType);
//                return new VoteAdapter.AddVoteHolder(layoutInflater, parent);
            case TYPE_GONE:
                onShowAddClick(viewType);
                View viewUnknown = layoutInflater.inflate(R.layout.holder_feed_default, parent, false);
                return new ItemInvisibleHolder(viewUnknown, mApp);
            default:
                onShowAddClick(viewType);
                return new VoteAdapter.VoteHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemInvisibleHolder) {

        } else if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(items, position);
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (holder instanceof VoteAdapter.VoteHolder) {
            ((VoteAdapter.VoteHolder) holder).setOnItemVoteListener(this);
        } else if (holder instanceof VoteAdapter.AddVoteHolder) {
            ((VoteAdapter.AddVoteHolder) holder).setOnItemVoteListener(this);
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        if (holder instanceof VoteAdapter.VoteHolder) {
            ((VoteAdapter.VoteHolder) holder).setOnItemVoteListener(null);
        } else if (holder instanceof VoteAdapter.AddVoteHolder) {
            ((VoteAdapter.AddVoteHolder) holder).setOnItemVoteListener(null);
        }
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public void onItemVoteDetailClick(PollItem pollItem) {
        if (onItemVoteListener != null) {
            onItemVoteListener.onItemVoteDetailClick(pollItem);
        }
    }

    @Override
    public void onItemVoteChoseClick(PollItem pollItem) {
        if (onItemVoteListener != null) {
            onItemVoteListener.onItemVoteChoseClick(pollItem);
        }
    }

    @Override
    public void onItemVoteAddClick() {
        if (onItemVoteListener != null) {
            onItemVoteListener.onItemVoteAddClick();
        }
    }
    @Override
    public void onShowAddClick(int number) {
        if (onItemVoteListener != null) {
            onItemVoteListener.onShowAddClick(number);
        }
    }

    class AddVoteHolder extends ViewHolder {

        @BindView(R.id.root_item_poll_detail)
        LinearLayout rootItemPollDetail;

        private @Nullable
        OnItemVoteListener onItemVoteListener;

        AddVoteHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.item_poll_detail_add_vote_v2, parent, false));
        }

        void setOnItemVoteListener(@Nullable OnItemVoteListener onItemVoteListener) {
            this.onItemVoteListener = onItemVoteListener;
        }

        @OnClick(R.id.root_item_poll_detail)
        void onRootItemPollDetailClicked() {
            if (onItemVoteListener != null) {
                onItemVoteListener.onItemVoteAddClick();
            }
        }
    }

    class VoteHolder extends ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_checkbox)
        ImageView ivCheckbox;
        @BindView(R.id.pollLayout)
        PollLayout pollLayout;
        @BindView(R.id.root_item_poll_detail)
        LinearLayout rootItemPollDetail;

        private @Nullable
        PollItem pollItem;
        private @Nullable
        OnItemVoteListener onItemVoteListener;

        VoteHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.item_poll_detail_vote_v2, parent, false));
            rootItemPollDetail.setOnClickListener(this);
            pollLayout.getIvPollViewDetailVote().setOnClickListener(this);
            if (isPreview) {
                int minHeightPreview = mApp.getResources().getDimensionPixelOffset(R.dimen.button_small_height);
                pollLayout.getIvPollViewDetailVote().setVisibility(View.GONE);
                rootItemPollDetail.setMinimumHeight(minHeightPreview);
                pollLayout.getTvPollStatus().setVisibility(View.GONE);
                pollLayout.getViewRoot().changeBackgroundColor(mApp.getResources().getColor(R.color.white));
                pollLayout.getTvPollTitle().setTextColor(mApp.getResources().getColor(R.color.black));
                pollLayout.getTvPollTitle().setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApp, Constants.FONT_SIZE.LEVEL_1));
            } else {
                int minHeightPreview = mApp.getResources().getDimensionPixelOffset(R.dimen.videoIconSizeBar);
                rootItemPollDetail.setMinimumHeight(minHeightPreview);
                pollLayout.getTvPollStatus().setVisibility(View.VISIBLE);
                pollLayout.getTvPollTitle().setTextColor(mApp.getResources().getColor(R.color.white));
                pollLayout.getTvPollTitle().setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApp, Constants.FONT_SIZE.LEVEL_2));
            }
        }

        void setOnItemVoteListener(@Nullable OnItemVoteListener onItemVoteListener) {
            this.onItemVoteListener = onItemVoteListener;
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            Object item = items.get(position);
            if (item instanceof PollItem) {
                pollItem = (PollItem) item;
                updateCheck();
                updateProcess();
                if (isPreview) {
                    pollLayout.getIvPollViewDetailVote().setVisibility(View.GONE);
                    pollLayout.getTvPollStatus().setVisibility(View.GONE);
                } else if (totalVote == 0) {
                    pollLayout.getIvPollViewDetailVote().setVisibility(View.GONE);
                } else {
//                    pollLayout.getTvPollStatus().setVisibility(View.VISIBLE);
                    if (pollObject.getAnonymous() == 0) {
                        pollLayout.getIvPollViewDetailVote().setVisibility(View.VISIBLE);
                    } else {
                        pollLayout.getIvPollViewDetailVote().setVisibility(View.GONE);
                    }
                }
            }
        }

        void updateCheck() {
            if (isPreview) ivCheckbox.setVisibility(View.GONE);
            else {
                ivCheckbox.setVisibility(View.VISIBLE);
            }
            if (pollItem == null) return;
            if (pollItem.isSelected()) {
                ivCheckbox.setImageResource(R.drawable.ic_checkbox_selected);
            } else {
                ivCheckbox.setImageResource(R.drawable.ic_checkbox);
            }
        }

        void updateProcess() {
            if (pollItem == null || pollObject == null) return;

            Log.i(TAG, "totalvote: " + totalVote);
            pollLayout.setPollItem(pollItem, totalVote, isPreview);
        }

        @Override
        public void onClick(View v) {
            if (pollItem == null || onItemVoteListener == null) return;
            if (v == rootItemPollDetail) {
                if (pollObject.getClose() != 1)
                    onItemVoteListener.onItemVoteChoseClick(pollItem);
            } else if (v == pollLayout.getIvPollViewDetailVote()) {
                onItemVoteListener.onItemVoteDetailClick(pollItem);
            }
        }
    }


}

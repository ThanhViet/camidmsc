package com.metfone.selfcare.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV3;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class FilmAdapter extends BaseAdapterV3 {

    public static final int TYPE_FILM = 0;
    public static final int TYPE_SPACE = 1;

    @Nullable
    private OnItemFilmListener onItemFilmListener;

    public FilmAdapter(Activity act) {
        super(act);
    }

    public void setOnItemFilmListener(@Nullable OnItemFilmListener onItemFilmListener) {
        this.onItemFilmListener = onItemFilmListener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = items.get(position);
        if (item instanceof Movie) {
            return TYPE_FILM;
        } else if (Utilities.equals(item, TYPE_SPACE)) {
            return TYPE_SPACE;
        } else {
            return TYPE_LOAD_MORE;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FILM:
                return new FilmHolder(layoutInflater, parent);
            case TYPE_SPACE:
                return new SpaceHolder((int) (ApplicationController.self().getMargin() * 1.5f));
            default:
                return new LoadMoreHolder(layoutInflater, parent);
        }
    }

    private void onItemFilmClicked(@NonNull Movie movie) {
        if (onItemFilmListener != null) {
            onItemFilmListener.onItemFilmClicked(movie);
        }
    }

    public interface OnItemFilmListener {
        void onItemFilmClicked(@NonNull Movie movie);
    }

    static class FilmHolder extends ViewHolder {

        @BindView(R.id.iv_thumb)
        ImageView ivThumb;
        @BindView(R.id.tv_number)
        TextView tvNumber;
        @BindView(R.id.root_thumb)
        RelativeLayout rootThumb;
        @BindView(R.id.tv_title)
        TextView tvTitle;

        @Nullable
        Movie movie;

        FilmHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.holder_movie_normal, parent, false));
            int width = com.metfone.selfcare.module.keeng.utils.Utilities.getWidthPosterMovie();
            ViewGroup.LayoutParams layoutParams = rootThumb.getLayoutParams();
            layoutParams.width = width;
            layoutParams.height = (int) (width / Constants.RATIO_POSTER_MOVIE);
            rootThumb.setLayoutParams(layoutParams);
            rootThumb.requestLayout();
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            Object item = items.get(position);
            if (item instanceof Movie) {
                movie = (Movie) item;
                ImageBusiness.setPosterMovie(movie.getPosterPath(), ivThumb);
                tvTitle.setText(movie.getName());
                if (Utilities.notEmpty(movie.getLabelTxt())) {
                    tvNumber.setText(movie.getLabelTxt());
                    tvNumber.setVisibility(View.VISIBLE);
                } else {
                    tvNumber.setVisibility(View.GONE);
                }
            } else {
                movie = null;
            }
        }

        @OnClick(R.id.root_item_movie_update_base)
        public void onViewClicked() {
            if (!(baseAdapter instanceof FilmAdapter) || movie == null) return;
            ((FilmAdapter) baseAdapter).onItemFilmClicked(movie);
        }

    }
}

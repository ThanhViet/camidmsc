package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.contact.OfficialViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by THANHNT72 on 3/11/2017.
 */


public class OfficerListNewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<OfficerAccount> listOfficers;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;

    public OfficerListNewAdapter(Context context, ArrayList<OfficerAccount> listOfficers) {
        this.context = context;
        this.listOfficers = listOfficers;
        inflater = LayoutInflater.from(this.context);
    }

    public void setOfficerList(ArrayList<OfficerAccount> officers) {
        listOfficers = officers;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        if (listOfficers == null) {
            return 0;
        }
        return listOfficers.size();
    }

    public Object getItem(int position) {
        return listOfficers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (listOfficers.get(position).getOfficerType() == OfficerAccountConstant.OFFICER_TYPE_DIVIDER) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        BaseViewHolder holder;
        if (type == 1) {
            View view = inflater.inflate(R.layout.holder_fake_divider, parent, false);
            holder = new BaseViewHolder(view) {
                @Override
                public void setElement(Object obj) {
                    //do nothing
                }
            };
        } else {
            View view = inflater.inflate(R.layout.holder_official, parent, false);
            holder = new OfficialViewHolder(view, context, null, true);
            if (mRecyclerClickListener != null)
                holder.setRecyclerClickListener(mRecyclerClickListener);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (listOfficers.get(position).getOfficerType() == OfficerAccountConstant.OFFICER_TYPE_DIVIDER) {
            ((BaseViewHolder) holder).setElement(getItem(position));
        } else {
            OfficialViewHolder officerHolder = (OfficialViewHolder) holder;
            if (mRecyclerClickListener != null)
                officerHolder.setViewClick(position, getItem(position));
            officerHolder.setElement(getItem(position));
        }
    }
}
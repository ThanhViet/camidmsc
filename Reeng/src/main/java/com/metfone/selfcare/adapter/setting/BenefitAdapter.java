package com.metfone.selfcare.adapter.setting;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.ServicesAdapter;
import com.metfone.selfcare.model.account.BenefitModel;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.module.home_kh.api.response.GetRankingListDetailResponse;
import com.metfone.selfcare.module.home_kh.model.RankDefine;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BenefitAdapter extends RecyclerView.Adapter<BenefitAdapter.BenefitViewHolder>{
    private List<RankDefine.BenefitDetail> items;
    private Context context;
    public BenefitAdapter(List<RankDefine.BenefitDetail> items,Context context) {
        this.context = context;
        this.items = items;
    }
    @NonNull
    @Override
    public BenefitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_benefit_page_kh,parent,false);
        return new BenefitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BenefitViewHolder holder, int position) {
        holder.tvTitle.setText(items.get(position).getBenefitName());
        holder.tvDescription.setText(items.get(position).getDescription());
        Glide.with(context).load(items.get(position).getIcon()).into(holder.imgIcon);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class BenefitViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDescription;
        ImageView imgIcon;
        public BenefitViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.label);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            imgIcon = itemView.findViewById(R.id.imgIcon);
        }
    }
}

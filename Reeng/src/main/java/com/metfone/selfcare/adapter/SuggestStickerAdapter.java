package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by tungt on 2/25/2016.
 */
public class SuggestStickerAdapter extends PagerAdapter {
    private static final String TAG = SuggestStickerAdapter.class.getSimpleName();
    private RecentStickerGridAdapter recentStickerAdapter;

    private ArrayList<StickerItem> mStickersArray;
    private EmoticonsGridAdapter.KeyClickListener mListener;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private int mListSize = 0;
    private int mPageNum = 0;

    public SuggestStickerAdapter(ArrayList<StickerItem> listSticker,
                                 EmoticonsGridAdapter.KeyClickListener listener,
                                 Context context) {
        mContext = context;
        mListener = listener;
        mStickersArray = listSticker;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mListSize = mStickersArray.size();
        Log.d(TAG, "ListSize" + mListSize);
        mPageNum = (int) Math.ceil(mListSize/4);
    }

    @Override
    public int getCount() {
        return (mStickersArray != null)? mPageNum : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove item from ViewPager
        container.removeView((LinearLayout) object);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int pos) {
        Log.d(TAG, "Position: " + (pos) * 4 + ", " + Math.min((pos - 1) * 4 + 4 , mListSize ));

        ArrayList<StickerItem> stickerList = new ArrayList<>(mStickersArray.subList( (pos) * 4 , Math.min( (pos) * 4 + 4 , mListSize)));

        View stickerLayout = mLayoutInflater.inflate(R.layout.fragment_page_sticker, container, false);
        GridView gridView = (GridView) stickerLayout.findViewById(R.id.emoticons_grid);

        recentStickerAdapter = new RecentStickerGridAdapter(mContext, mListener, stickerList);

        gridView.setAdapter(recentStickerAdapter);
        container.addView(stickerLayout);
        return stickerLayout;
    }
}

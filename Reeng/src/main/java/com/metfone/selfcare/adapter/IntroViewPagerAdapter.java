package com.metfone.selfcare.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by tungt on 9/8/2015.
 */
public class IntroViewPagerAdapter extends PagerAdapter {

    private static final String TAG = IntroViewPagerAdapter.class.getSimpleName();

    private Context mContext;
    private String[] mTitles;
    private String[] mIntros;
    private String[] mImages;
    private ArrayList<Drawable> mDrawableList;

    private LayoutInflater mLayoutInflater;

    public IntroViewPagerAdapter(Context c, String[] titles, String[] intros, String[] images) {
        this.mContext = c;
        this.mTitles = titles;
        this.mIntros = intros;
        this.mImages = images;
        this.mDrawableList = new ArrayList<>(getCount());
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //
        getImage();
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int pos) {
        TextView title;
        TextView intro;
        ImageView image;

        View itemView = mLayoutInflater.inflate(R.layout.view_intro, container, false);
        title = (TextView) itemView.findViewById(R.id.text_title);
        intro = (TextView) itemView.findViewById(R.id.text_intro);
        image = (ImageView) itemView.findViewById(R.id.image);

        title.setText(mTitles[pos]);
        intro.setText(mIntros[pos]);
        image.setImageDrawable(mDrawableList.get(pos));

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove item from ViewPager
        container.removeView((LinearLayout) object);

    }

    private Drawable getDrawable(String filename) {
        // load image
        try {
            // get input stream
            InputStream ims = mContext.getAssets().open("intro/" + filename + ".jpg");
            // load image as Drawable
            return Drawable.createFromStream(ims, null);
        } catch (IOException ex) {
            Log.e(TAG, "IOException", ex);
            return null;
        }
    }

    private void getImage() {
        for (int i = 0; i < getCount(); i++) {
            mDrawableList.add(getDrawable(mImages[i]));
        }
    }

}

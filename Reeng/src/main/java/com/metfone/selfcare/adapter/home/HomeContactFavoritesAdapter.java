package com.metfone.selfcare.adapter.home;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.holder.contact.FavoriteHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/6/2017.
 */

public class HomeContactFavoritesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = HomeContactFavoritesAdapter.class.getSimpleName();

    private ApplicationController mApplication;
    private ArrayList<PhoneNumber> listData;
    private LayoutInflater inflater;
    private RecyclerClickListener mListener;

    public HomeContactFavoritesAdapter(ApplicationController context, ArrayList<PhoneNumber> list) {
        this.mApplication = context;
        this.listData = list;
        inflater = LayoutInflater.from(mApplication);
    }

    public void setRecyclerClickListener(RecyclerClickListener listener) {
        this.mListener = listener;
    }

    public void setData(ArrayList<PhoneNumber> threads) {
        listData = threads;
    }

    @Override
    public int getItemCount() {
        if (listData == null) return 0;
        return listData.size();
    }

    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = inflater.inflate(R.layout.holder_contact_favorite, parent, false);
        FavoriteHolder holder = new FavoriteHolder(view, mApplication);
        holder.setRecyclerClickListener(mListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object item = getItem(position);
        FavoriteHolder favoriteHolder = ((FavoriteHolder) holder);
        favoriteHolder.setElement(item);
        favoriteHolder.setViewClick(position, item);
    }
}

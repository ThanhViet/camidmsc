package com.metfone.selfcare.adapter;

import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/5/2017.
 */

public class AlbumProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = AlbumProfileAdapter.class.getSimpleName();
    private ArrayList<Object> mListImage = new ArrayList<>();
    private ApplicationController mApp;
    private boolean isMyProfile;
    private boolean isViewImageFromThread;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;
    private boolean isMySetting = false;
    public AlbumProfileAdapter(ApplicationController mApp, boolean isMyProfile) {
        this.mApp = mApp;
        this.isMyProfile = isMyProfile;
        inflater = (LayoutInflater) mApp.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isViewImageFromThread = true;
    }
    public AlbumProfileAdapter(ApplicationController mApp, boolean isMyProfile, boolean isMySetting) {
        this.mApp = mApp;
        this.isMyProfile = isMyProfile;
        inflater = (LayoutInflater) mApp.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isViewImageFromThread = true;
        this.isMySetting = isMySetting;
    }
    public void setListImageString(ArrayList<String> listImg) {
        this.mListImage = new ArrayList<>();
        this.mListImage.addAll(listImg);
    }

    public AlbumProfileAdapter(ArrayList<ImageProfile> mListImage,
                               ApplicationController mApp, boolean isMyProfile) {
        this.mListImage = new ArrayList<>();
        this.mListImage.addAll(mListImage);
        this.mApp = mApp;
        this.isMyProfile = isMyProfile;
        inflater = (LayoutInflater) mApp.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListImage(ArrayList<ImageProfile> mListImage) {
        this.mListImage = new ArrayList<>();
        this.mListImage.addAll(mListImage);
    }


    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (isMySetting) {
            view = inflater.inflate(R.layout.item_setting_image_profile, parent, false);
        }  else {
            view = inflater.inflate(R.layout.item_album_image_profile, parent, false);
        }

        AlbumProfileHolder albumProfileHolder = new AlbumProfileHolder(view);
        if (mRecyclerClickListener != null) {
            albumProfileHolder.setRecyclerClickListener(mRecyclerClickListener);
        }
        return albumProfileHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AlbumProfileHolder albumProfileHolder = (AlbumProfileHolder) holder;
        albumProfileHolder.setElement(mListImage.get(position));
        if (isMySetting && position <= 5) {
            if (isMySetting && position == 5) {
                ((AlbumProfileHolder) holder).mLinearLayout.setBackgroundColor(mApp.getResources().getColor(R.color.bg_image_setting));
                ((AlbumProfileHolder) holder).txtTotal.setText(String.valueOf("+" + (mListImage.size() - 5)));
                ((AlbumProfileHolder) holder).imageView.setVisibility(View.GONE);
                ((AlbumProfileHolder) holder).txtTotal.setVisibility(View.VISIBLE);
                ((AlbumProfileHolder) holder).cardView.setVisibility(View.GONE);
            } else {
                ((AlbumProfileHolder) holder).mLinearLayout.setBackgroundColor(mApp.getResources().getColor(R.color.white));
                ((AlbumProfileHolder) holder).imageView.setVisibility(View.VISIBLE);
                ((AlbumProfileHolder) holder).txtTotal.setVisibility(View.GONE);
                ((BaseViewHolder) holder).setViewClick(position, mListImage.get(position));
                ((AlbumProfileHolder) holder).cardView.setVisibility(View.VISIBLE);
            }
        }
        if (!isMySetting) {
            ((AlbumProfileHolder) holder).mLinearLayout.setBackgroundColor(mApp.getResources().getColor(R.color.white));
            ((AlbumProfileHolder) holder).imageView.setVisibility(View.VISIBLE);
            ((AlbumProfileHolder) holder).txtTotal.setVisibility(View.GONE);
            ((BaseViewHolder) holder).setViewClick(position, mListImage.get(position));
            ((AlbumProfileHolder) holder).cardView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (mListImage == null || mListImage.isEmpty()) {
            return 0;
        } else{
            if(isMySetting){
                if (mListImage.size() > 5) {
                    return 6;
                } else {
                    return mListImage.size();
                }
            } else {
                return mListImage.size();
            }
        }
    }

    class AlbumProfileHolder extends BaseViewHolder {

        private AspectImageView imageView;
        private TextView txtTotal;
        private LinearLayout mLinearLayout;
        private CardView cardView;

        public AlbumProfileHolder(View itemView) {
            super(itemView);
            imageView = (AspectImageView) itemView.findViewById(R.id.item);
            txtTotal = (TextView) itemView.findViewById(R.id.txtTotal);
            mLinearLayout = (LinearLayout) itemView.findViewById(R.id.layout_emo_item);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
        }

        @Override
        public void setElement(Object obj) {
            if(isViewImageFromThread){
                String filePath = (String) obj;
                ImageLoaderManager.getInstance(mApp).displayDetailOfMessage(filePath, imageView, false, false);
            } else {
                ImageProfile imageProfile = (ImageProfile) obj;
                if (isMyProfile) {
                    mApp.getImageProfileBusiness().displayImageProfile(imageView, imageProfile, true);
                } else {
                    mApp.getImageProfileBusiness().displayImageProfileFriend(imageView, imageProfile, false);
                }
            }
        }
    }
}

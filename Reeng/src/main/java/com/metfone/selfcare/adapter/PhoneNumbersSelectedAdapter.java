package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.fragment.contact.ChooseMultiNumberFragment;
import com.metfone.selfcare.listeners.SimpleSwipeListener;
import com.metfone.selfcare.ui.SwipeLayout;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

/**
 * Created by namnh40 on 6/5/2015.
 */
public class PhoneNumbersSelectedAdapter extends BaseSwipeAdapter {

    private static final String TAG = PhoneNumbersSelectedAdapter.class.getSimpleName();
    private ArrayList<String> mMemberThread = new ArrayList<>();

    private ArrayList<PhoneNumber> datas;
    private ArrayList<PhoneNumber> deleteableItems;
    private ChooseMultiNumberFragment mFragment;

    public PhoneNumbersSelectedAdapter(Context mContext, ChooseMultiNumberFragment fragment, ArrayList<PhoneNumber>
            phoneNumbers) {
        super(mContext);
        datas = phoneNumbers;
        deleteableItems = new ArrayList<>();
        mFragment = fragment;
    }

    public void setDatas(ArrayList<PhoneNumber> datas) {
        this.datas = datas;
    }

    public void setMemberThread(ArrayList<String> datas) {
        if (datas == null) datas = new ArrayList<>();
        this.mMemberThread = datas;
    }

    private static boolean itemIsDeletable(PhoneNumber item, PhoneNumber deletable) {
        return item.equals(deletable);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        return null;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        PhoneNumber phone = (PhoneNumber) getItem(position);
        if (convertView == null) {
            // Inflate the view since it does not exist
            convertView = mInflater.inflate(R.layout.item_phone_numbers_selected, parent, false);
            holder = new Holder();
            holder.avatar = convertView.findViewById(R.id.item_contact_view_avatar_circle);
            holder.title = convertView.findViewById(R.id.contact_avatar_text);
            holder.mTvContactName = convertView.findViewById(R.id.item_contact_view_name_text);
            holder.ivRemove = convertView.findViewById(R.id.ivRemove);
            holder.ivStatus = convertView.findViewById(R.id.ivStatus);

            SwipeLayout swipeLayout = convertView.findViewById(getSwipeLayoutResourceId(position));
            if (phone.isDisable()) {
                swipeLayout.setSwipeEnabled(false);
                holder.ivRemove.setVisibility(View.GONE);
            }
            swipeLayout.addSwipeListener(new SimpleSwipeListener() {
                @Override
                public void onOpen(SwipeLayout layout) {
                    // Toast.makeText(mContext, "slide", Toast.LENGTH_SHORT).show();
                    try {
                        boolean isDelete = mFragment.removePhoneNumberSelected(position);
                        if (isDelete)
                            delete(position);
                    } catch (ConcurrentModificationException ex) {
                        Log.e(TAG, "ConcurrentModificationException", ex);
                    } catch (Exception ex) {
                        Log.e(TAG, "Exception", ex);
                    }
                }
            });
            swipeLayout.setOnDoubleClickListener((layout, surface) -> mFragment.removePhoneNumberSelected(position));
            holder.ivRemove.setOnClickListener(view -> {
                try {
                    boolean isDelete = mFragment.removePhoneNumberSelected(position);
                    if (isDelete)
                        delete(position);
                } catch (ConcurrentModificationException ex) {
                    Log.e(TAG, "ConcurrentModificationException", ex);
                } catch (Exception ex) {
                    Log.e(TAG, "Exception", ex);
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        if (phone.isDisable() || mMemberThread.contains(phone.getJidNumber())) {
            holder.ivRemove.setVisibility(View.GONE);
        } else {
            holder.ivRemove.setVisibility(View.VISIBLE);
        }
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(holder.avatar, holder.title, phone, size);
        holder.mTvContactName.setText(phone.getName());
        if (!phone.getOnline()) {
            holder.ivStatus.setVisibility(View.GONE);
        } else {
            holder.ivStatus.setVisibility(View.VISIBLE);
        }
        try {
            checkIfItemHasBeenMarkedAsDeleted(phone);
        } catch (ConcurrentModificationException e) {
            Log.e(TAG, "ConcurrentModificationException:", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception:", e);
        }
        return convertView;
    }

    @Override
    public void fillValues(int position, View convertView) {
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        if (position >= datas.size()) return null;
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void delete(int position) {
        deleteableItems.add(datas.get(position));
        notifyDataSetChanged();
    }

    private void checkIfItemHasBeenMarkedAsDeleted(PhoneNumber item) {
        for (PhoneNumber deletable : deleteableItems) {
            deleteItemIfMarkedAsDeletable(item, deletable);
        }
    }

    private void deleteItemIfMarkedAsDeletable(PhoneNumber item, PhoneNumber deletable) {
        if (itemIsDeletable(item, deletable)) {
            datas.remove(item);
            deleteableItems.remove(item);
            notifyDataSetChanged();
        }
    }

    private static class Holder {

        public RoundedImageView avatar;
        public TextView title;
        public TextView mTvContactName;
        public ImageView ivRemove;
        public ImageView ivStatus;

    }

}

package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.ui.photoview.PhotoView;
import com.metfone.selfcare.ui.photoview.PhotoViewAttacher;

import java.util.ArrayList;


/**
 * Created by huybq7 on 2/9/2015.
 */
public class PreviewImageAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<String> mListImage;
    private boolean loadLocal;

    private ImageClickListener imageClickListener;

    public PreviewImageAdapter(Context context, ArrayList<String> listImage) {
        this.mContext = context;
        this.mListImage = listImage;
    }

    public void setLoadLocal(boolean loadLocal) {
        this.loadLocal = loadLocal;
    }

    public void setListImage(ArrayList<String> listImage) {
        this.mListImage = listImage;
    }

    public void setImageClickListener(ImageClickListener imageClickListener) {
        this.imageClickListener = imageClickListener;
    }

    @Override
    public int getCount() {
        return mListImage.size();
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        PhotoView photoView = new PhotoView(container.getContext());
        String imagePath = mListImage.get(position);
        photoView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        // Now just add PhotoView to ViewPager and return it
        container.addView(photoView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (!loadLocal) {
            imagePath = UrlConfigHelper.getInstance(mContext).getDomainImage() + imagePath;
        }
//            ImageLoaderManager.getInstance(mContext).displayDetailOfMessage(imagePath, photoView, false, true);
//        else
        Glide.with(mContext).load(imagePath).apply(new RequestOptions()
                .placeholder(R.drawable.ic_images_default)
                .error(R.drawable.ic_images_default))
                .into(photoView);
        photoView.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
            @Override
            public void onPhotoTap(View view, float v, float v1) {
                if (imageClickListener != null) {
                    imageClickListener.onClickImage();
                }
            }

            @Override
            public void onOutsidePhotoTap() {
                if (imageClickListener != null) {
                    imageClickListener.onClickImage();
                }
            }
        });
        return photoView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    public interface ImageClickListener {
        void onClickImage();
    }
}
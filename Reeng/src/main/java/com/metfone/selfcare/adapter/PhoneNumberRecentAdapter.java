package com.metfone.selfcare.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhoneNumberRecentAdapter extends RecyclerView.Adapter<PhoneNumberRecentAdapter.RecentPhoneHolder> {

    private List<PhoneNumber> mListRecent;
    private Context mContext;
    private ApplicationController mApplication;
    private Resources mRes;
    private IOnItemPhoneRecentClickListener mOnItemPhoneRecentClickListener;

    public interface IOnItemPhoneRecentClickListener{
        void onClick(int position, PhoneNumber item);
    }

    public void setOnItemPhoneRecentClickListener(IOnItemPhoneRecentClickListener mOnItemPhoneRecentClickListener) {
        this.mOnItemPhoneRecentClickListener = mOnItemPhoneRecentClickListener;
    }

    public PhoneNumberRecentAdapter(List<PhoneNumber> listRecent, Context context) {
        this.mListRecent = listRecent;
        this.mContext = context;
        mRes = mContext.getResources();
        mApplication = (ApplicationController) mContext.getApplicationContext();
    }
    @NonNull
    @Override
    public RecentPhoneHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.chat_item_phone_recent, parent, false);
        return new RecentPhoneHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentPhoneHolder holder, int position) {
        holder.mTvName.setText(mListRecent.get(position).getName());
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(holder.mRivAvatar, holder.mTvAvatar, mListRecent.get(position), size);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemPhoneRecentClickListener.onClick(position, mListRecent.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListRecent.size();
    }

    public class RecentPhoneHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_phone_recent_riv_avatar)
        RoundedImageView mRivAvatar;
        @BindView(R.id.item_phone_recent_tv_avatar)
        AppCompatTextView mTvAvatar;
        @BindView(R.id.item_phone_recent_tv_name)
        AppCompatTextView mTvName;
        public RecentPhoneHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}

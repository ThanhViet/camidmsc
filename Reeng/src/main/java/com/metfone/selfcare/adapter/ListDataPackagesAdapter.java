/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BoxDataPackagesHolder;
import com.metfone.selfcare.holder.DataPackageDetailHolder;
import com.metfone.selfcare.model.DataPackagesResponse;

import java.util.ArrayList;

public class ListDataPackagesAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    public static final int TYPE_BOX_PACKAGES = 1;
    public static final int TYPE_PACKAGE_INFO = 2;

    private String labelEnd;
    private DataPackageDetailHolder.OnBuyPackage listener;

    public ListDataPackagesAdapter(Activity activity, ArrayList<Object> list , DataPackageDetailHolder.OnBuyPackage listener) {
        super(activity, list);
        this.listener = listener;
    }

    public void setLabelEnd(String labelEnd) {
        this.labelEnd = labelEnd;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof DataPackagesResponse.ListDataPackages) return TYPE_BOX_PACKAGES;
        if (item instanceof DataPackagesResponse.DataPackageInfo) return TYPE_PACKAGE_INFO;
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_BOX_PACKAGES:
                return new BoxDataPackagesHolder(layoutInflater.inflate(R.layout.holder_box_data_packages, parent, false)
                        , activity, listener);

            case TYPE_PACKAGE_INFO:
                return new DataPackageDetailHolder(layoutInflater.inflate(R.layout.holder_data_package_detail, parent, false)
                        , activity, listener);
            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder instanceof BoxDataPackagesHolder) {
            ((BoxDataPackagesHolder) holder).bindData(getItem(position), position, (position + 1 == getItemCount() ? labelEnd : ""));
        } else if (holder instanceof DataPackageDetailHolder) {
            holder.bindData(getItem(position), position);
        }
    }
}

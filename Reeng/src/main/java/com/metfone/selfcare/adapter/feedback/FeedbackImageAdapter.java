package com.metfone.selfcare.adapter.feedback;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class FeedbackImageAdapter extends BaseAdapter<BaseViewHolder, String> {

    public static final int ADD_TYPE = 0;
    public static final int SELECTED_TYPE = 1;
    private static final int MAX_IMAGE = 5;

    private PublishSubject<Integer> addImageObservable;
    private PublishSubject<String> removeObservable;

    public FeedbackImageAdapter(Activity activity) {
        super(activity);
        addImageObservable = PublishSubject.create();
        removeObservable = PublishSubject.create();
        items = new ArrayList<>();

    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final BaseViewHolder viewHolder;
        switch (viewType) {
            case ADD_TYPE: {
                View view = layoutInflater.inflate(R.layout.item_add_image_feedback, parent, false);
                viewHolder = new AddImageFeedbackViewHolder(view);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addImageObservable.onNext(viewHolder.getAdapterPosition());
                    }
                });

                break;
            }
            case SELECTED_TYPE: {
                View view = layoutInflater.inflate(R.layout.item_selected_image_feedback, parent, false);
                viewHolder = new ImageSelectedFeedbackViewHolder(view);
                view.findViewById(R.id.icRemove).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        removeObservable.onNext(items.get(viewHolder.getAdapterPosition()));
                        removeImage(viewHolder.getAdapterPosition());
                    }
                });
                break;
            }
            default:
                viewHolder = null;
        }
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return (items.size() < MAX_IMAGE) ? items.size() + 1 : items.size();
    }

    public void addItems(List<String> listPath) {
        if (listPath == null || listPath.isEmpty()) {
            return;
        }
        int countAdded = 0;
        for (String path : listPath) {
            items.add(path);
            countAdded++;
            if (items.size() >= MAX_IMAGE) {
                notifyItemRemoved(items.size()); // remove select type
                break;
            }
        }
        notifyItemRangeInserted(items.size() - countAdded, countAdded);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        if (holder != null) {
            if (holder instanceof ImageSelectedFeedbackViewHolder) {
                holder.setElement(items.get(position));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == items.size() && items.size() < MAX_IMAGE) {
            return ADD_TYPE;
        } else {
            return SELECTED_TYPE;
        }
    }

    private void removeImage(int position) {
        items.remove(position);
        if (items.size() == MAX_IMAGE - 1) {
            notifyItemInserted(items.size());
        }
        notifyItemRemoved(position);
    }

    public Observable<Integer> getAddImageObservable() {
        return addImageObservable;
    }

    public Observable<String> getRemoveObservable() {
        return removeObservable;
    }

    public void removeObservable() {
        addImageObservable = null;
    }

}

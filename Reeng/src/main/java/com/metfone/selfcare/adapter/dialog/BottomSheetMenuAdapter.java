package com.metfone.selfcare.adapter.dialog;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.adapter.CallHistoryAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/7/2017.
 */

public class BottomSheetMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = CallHistoryAdapter.class.getSimpleName();
    private LayoutInflater mLayoutInflater;
    private ArrayList<ItemContextMenu> listItem;
    private RecyclerClickListener mRecyclerClickListener;

    public BottomSheetMenuAdapter(ArrayList<ItemContextMenu> listItem) {
        this.mLayoutInflater = (LayoutInflater) ApplicationController.self().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listItem = listItem;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        return (listItem != null) ? listItem.size() : 0;
    }

    public Object getItem(int position) {
        return (listItem != null && listItem.size() > position) ? listItem.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        Log.d(TAG, "onCreateViewHolder");
        BaseViewHolder holder;
        View view = mLayoutInflater.inflate(R.layout.holder_bottom_sheet_menu, parent, false);
        holder = new ViewHolder(view);
        holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + position);
        ((BaseViewHolder) holder).setViewClick(position, getItem(position));
        ((ViewHolder) holder).setElement(getItem(position));
    }

    private class ViewHolder extends BaseViewHolder {
        private ItemContextMenu mEntry;
        private TextView mTvwMessage;
        private AppCompatImageView mImgIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            mImgIcon = itemView.findViewById(R.id.bottom_sheet_holder_icon);
            mTvwMessage = itemView.findViewById(R.id.bottom_sheet_holder_text);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (ItemContextMenu) obj;
            mTvwMessage.setText(mEntry.getText());
            if (mEntry.getImageRes() != -1) {
                mImgIcon.setVisibility(View.VISIBLE);
                mImgIcon.setImageResource(mEntry.getImageRes());
            } else {
                mImgIcon.setVisibility(View.GONE);
            }
        }
    }
}
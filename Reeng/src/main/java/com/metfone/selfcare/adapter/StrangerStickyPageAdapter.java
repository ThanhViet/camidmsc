package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.database.model.StrangerSticky;
import com.metfone.selfcare.holder.StrangerStickyHolder;
import com.metfone.selfcare.listeners.StrangerStickyInteractionListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by tungt on 3/8/2016.
 */
public class StrangerStickyPageAdapter extends PagerAdapter {
    private static final String TAG = StrangerStickyPageAdapter.class.getSimpleName();
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<StrangerSticky> mStickyList;
    private StrangerStickyInteractionListener mListener;

    public StrangerStickyPageAdapter(Context mContext,
                                     ArrayList<StrangerSticky> stickyList,
                                     StrangerStickyInteractionListener listener) {
        this.mContext = mContext;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mStickyList = stickyList;
        mListener = listener;
    }

    public void setStrangerList(ArrayList<StrangerSticky> list) {
        mStickyList = list;
    }


    @Override
    public int getCount() {
        return mStickyList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int pos) {
        Log.d(TAG, "instantiateItem");
        StrangerStickyHolder holder = new StrangerStickyHolder(mContext, mListener);
        holder.initHolder(container, null, pos, mLayoutInflater);
        holder.setElemnts(mStickyList.get(pos));
        View itemView = holder.getConvertView();
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove item from ViewPager
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.metfone.selfcare.database.model.EventMessage;
import com.metfone.selfcare.holder.EventMessageHolder;

import java.util.ArrayList;


public class EventMessageAdapter extends BaseAdapter {
    private ArrayList<EventMessage> listEventMessages;
    private int threadType;
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public EventMessageAdapter(Context context, ArrayList<EventMessage> eventMessages, int threadType) {
        this.listEventMessages = eventMessages;
        this.threadType = threadType;
        this.mContext = context;
        this.mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListEventMessages(ArrayList<EventMessage> eventMessages) {
        this.listEventMessages = eventMessages;
    }

    @Override
    public int getCount() {
        return listEventMessages.size();
    }

    @Override
    public Object getItem(int position) {
        return listEventMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

//    @Override
//    public int getViewTypeCount() {
//        return 5;
//    }

//    @Override
//    public int getItemViewType(int position) {
//        return search.get(position).type;
//    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // get selected entry
        EventMessageHolder holder;
        if (convertView == null) {
            holder = new EventMessageHolder(mContext, threadType);
            holder.initHolder(parent, convertView, position, mLayoutInflater);
        } else {
            holder = (EventMessageHolder) convertView.getTag();
        }
        holder.setElemnts(getItem(position));
        return holder.getConvertView();
    }
}
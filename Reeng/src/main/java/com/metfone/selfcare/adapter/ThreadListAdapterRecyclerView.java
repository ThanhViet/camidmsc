package com.metfone.selfcare.adapter;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.BannerMocha;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.MediaBoxItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ads.AdsHolder;
import com.metfone.selfcare.helper.ads.AdsModel;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.MediaBoxHolder;
import com.metfone.selfcare.holder.ThreadBannerViewHolder;
import com.metfone.selfcare.holder.ThreadMessageViewHolder;
import com.metfone.selfcare.holder.contact.BaseContactHolder;
import com.metfone.selfcare.holder.contact.PhoneNumberViewHolder;
import com.metfone.selfcare.holder.contact.SectionViewHolder;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/27/2016.
 */

public class ThreadListAdapterRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = ThreadListAdapterRecyclerView.class.getSimpleName();

    private ApplicationController mApplication;
    private ArrayList<Object> listData;
    private LayoutInflater inflater;
    private int typeHolderContact;
    private ClickListener.IconListener mCallBack;
    private boolean isSelectMode = false;
    private Resources mRes;
    private RecyclerClickListener mRecyclerClickListener;
    private boolean showIconPin;
    private BaseSlidingFragmentActivity activity;

    public ThreadListAdapterRecyclerView(BaseSlidingFragmentActivity activity, int typeHolderContact,
                                         ClickListener.IconListener callBack) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplication();
        this.listData = new ArrayList<>();
        inflater = LayoutInflater.from(mApplication);
        this.typeHolderContact = typeHolderContact;
        this.mCallBack = callBack;
        mRes = mApplication.getResources();
    }

    public void setShowIconPin(boolean showIconPin) {
        this.showIconPin = showIconPin;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setThreadList(ArrayList<Object> threads) {
        if (threads.size() > 10) {
            AdsModel adsModel = new AdsModel(0);
            threads.add(5, adsModel);
        }
        listData = threads;
    }

    public ArrayList<Object> getListData() {
        return listData;
    }

    public void setSelectMode(boolean isDeleting) {
        this.isSelectMode = isDeleting;
    }

    public boolean getSelectMode() {
        return isSelectMode;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof ThreadMessage) {
            return Constants.MESSAGE.HOLDER_THREAD_MESSAGE;
        } else if (item instanceof AdsModel) {
            return Constants.MESSAGE.HOLDER_THREAD_ADS;
        } else if (item instanceof PhoneNumber) {
            PhoneNumber phone = (PhoneNumber) item;
            if (phone.getContactId() == null) {
                return Constants.MESSAGE.HOLDER_THREAD_SECTION;
            } else {
                return Constants.MESSAGE.HOLDER_THREAD_CONTACT;
            }
        } else if (item instanceof BannerMocha) {
            return Constants.MESSAGE.HOLDER_THREAD_BANNER;
        } else if (item instanceof MediaBoxItem) {
            return Constants.MESSAGE.HOLDER_THREAD_MEDIA_BOX;
        }
        return -1;
    }

    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        BaseViewHolder viewHolder;
        if (type == Constants.MESSAGE.HOLDER_THREAD_MESSAGE) {
            View threadMessageView = inflater.inflate(R.layout.holder_thread_layout, parent, false);
            viewHolder = new ThreadMessageViewHolder(threadMessageView, mApplication,this.activity);
            /*int height = mRes.getDimensionPixelOffset(R.dimen.item_thread_view_height);
            threadMessageView.setMinimumHeight(height);*/
        } else if (type == Constants.MESSAGE.HOLDER_THREAD_CONTACT) {
            View phoneNumberView = inflater.inflate(R.layout.holder_contact_viewer, parent, false);
            viewHolder = new PhoneNumberViewHolder(phoneNumberView, mApplication, mCallBack);
            ((BaseContactHolder) viewHolder).setType(typeHolderContact);
        } else if (type == Constants.MESSAGE.HOLDER_THREAD_SECTION) {
            View view = inflater.inflate(R.layout.holder_contact_section, parent, false);
            viewHolder = new SectionViewHolder(view, mApplication, null);
            ((BaseContactHolder) viewHolder).setType(typeHolderContact);
        } else if (type == Constants.MESSAGE.HOLDER_THREAD_MEDIA_BOX) {
            View view = inflater.inflate(R.layout.holder_media_box, parent, false);
            viewHolder = new MediaBoxHolder(view, activity);
        } else if (type == Constants.MESSAGE.HOLDER_THREAD_ADS) {
            View view = inflater.inflate(com.vtm.adslib.R.layout.holder_ads_banner, parent, false);
            viewHolder = new AdsHolder(view);
        } else {
            View bannerView = inflater.inflate(R.layout.holder_thread_banner_layout, parent, false);
            viewHolder = new ThreadBannerViewHolder(bannerView, mApplication);
        }
        viewHolder.setSelectMode(isSelectMode);
        viewHolder.setRecyclerClickListener(mRecyclerClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder");
        ((BaseViewHolder) holder).setViewClick(position, listData.get(position));
        ((BaseViewHolder) holder).setSelectMode(isSelectMode);
        if (holder instanceof ThreadMessageViewHolder) {
            ((ThreadMessageViewHolder) holder).setShowIconPin(showIconPin);
        }
        ((BaseViewHolder) holder).setElement(listData.get(position));
    }

    @Override
    public int getItemCount() {
        if (listData == null) return 0;
        return listData.size();
    }
}
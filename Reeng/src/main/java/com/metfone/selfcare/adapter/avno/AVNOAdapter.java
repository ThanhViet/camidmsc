package com.metfone.selfcare.adapter.avno;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.holder.BaseViewHolder;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 6/5/2019.
 */

public class AVNOAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private ArrayList<PackageAVNO> listItem;
    private Resources mRes;
    private AVNOHelper.PackageAVNOListener listener;

    public AVNOAdapter(BaseSlidingFragmentActivity activity, ArrayList<PackageAVNO> listItem,
                       AVNOHelper.PackageAVNOListener listener) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplication();
        this.listItem = listItem;
        this.mRes = mApplication.getResources();
        this.listener = listener;
    }

    public void setListItem(ArrayList<PackageAVNO> listItem) {
        this.listItem = listItem;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mApplication).inflate(R.layout.holder_avno,
                parent, false);
        AVNOHolder holder = new AVNOHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((AVNOHolder) holder).setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }

    private class AVNOHolder extends BaseViewHolder {

        private TextView tvTitle, tvMinutes, tvNumber, tvPrice;
        private TextView tvAction;
        private View llBgHolder;

        public AVNOHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitlePackage);
            tvMinutes = itemView.findViewById(R.id.tvMinutes);
            tvNumber = itemView.findViewById(R.id.tvNumber);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvAction = itemView.findViewById(R.id.tvActionPackage);
            llBgHolder = itemView.findViewById(R.id.llBgHolder);
        }

        @Override
        public void setElement(Object obj) {
            final PackageAVNO packageAVNO = (PackageAVNO) obj;
            tvTitle.setText(packageAVNO.getTitle());
            tvMinutes.setText(packageAVNO.getMinutes());
            tvNumber.setText(packageAVNO.getNumbInString());
            tvPrice.setText(packageAVNO.getPrice());
            if (packageAVNO.getDeeplinkItems() != null && packageAVNO.getDeeplinkItems().size() > 0) {
                String label = packageAVNO.getDeeplinkItems().get(0).getLabel();
                tvAction.setText(label);
            }
            /*if (packageAVNO.getInUse() == 1) {
                tvAction.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.white),
                        ContextCompat.getColor(mApplication, R.color.bg_mocha_focus));
                tvAction.setStroke(ContextCompat.getColor(mApplication, R.color.bg_mocha), 1);
                tvAction.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
            } else {
                tvAction.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.bg_mocha),
                        ContextCompat.getColor(mApplication, R.color.bg_mocha_focus));
                tvAction.setStroke(ContextCompat.getColor(mApplication, R.color.bg_mocha), 0);
                tvAction.setTextColor(ContextCompat.getColor(mApplication, R.color.white));
            }*/
            tvAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickAction(packageAVNO);
                }
            });

            switch (packageAVNO.getIndexColor()) {
                case 1:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail_1);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail_1);
                    break;
                case 2:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail_2);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail_2);
                    break;
                case 3:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail_3);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail_3);
                    break;
                case 4:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail_4);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail_4);
                    break;
                case 5:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail_5);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail_5);
                    break;
                case 6:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail_6);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail_6);
                    break;
                case 7:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail_7);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail_7);
                    break;
                case 8:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail_8);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail_8);
                    break;
                default:
                    llBgHolder.setBackgroundResource(R.drawable.bg_data_package_detail);
                    tvAction.setBackgroundResource(R.drawable.bg_button_data_package_detail);
                    break;
            }
        }
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.holder.AbsContentHolder;
import com.metfone.selfcare.holder.ThreadGroupHolder;
import com.metfone.selfcare.listeners.ClickListener;

import java.util.ArrayList;


public class GroupListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ThreadMessage> listThreads;
    private LayoutInflater inflater;
    private int typeHolderContact;
    private ClickListener.IconListener mCallBack;

    public GroupListAdapter(Context context, int typeHolderContact,
                            ClickListener.IconListener callBack, ArrayList<ThreadMessage> listThreads) {
        this.context = context;
        this.listThreads = listThreads;
        inflater = LayoutInflater.from(this.context);
        this.typeHolderContact = typeHolderContact;
        this.mCallBack = callBack;
    }

    public void setThreadList(ArrayList<ThreadMessage> threads) {
        listThreads = threads;
    }

    @Override
    public int getCount() {
        if (listThreads == null) {
            return 0;
        }
        return listThreads.size();
    }

    @Override
    public Object getItem(int position) {
        return listThreads.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return Constants.MESSAGE.HOLDER_THREAD_MESSAGE;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AbsContentHolder holder;
        //int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ThreadGroupHolder(context);
            holder.initHolder(parent, convertView, position, inflater);
        } else {
            holder = (ThreadGroupHolder) convertView.getTag();
        }
        holder.setElemnts(getItem(position));
        convertView = holder.getConvertView();
        return convertView;
    }
}
package com.metfone.selfcare.adapter.avno;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 7/26/2018.
 */

public class PackageAVNONewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = PackageAVNONewAdapter.class.getSimpleName();

    private ApplicationController mApplication;
    private ArrayList<PackageAVNO> listItem;
    private Resources mRes;
    private AVNOHelper.OnClickActionPackage listener;


    public PackageAVNONewAdapter(ApplicationController application, ArrayList<PackageAVNO> listItem,
                                 AVNOHelper.OnClickActionPackage listener) {
        this.mApplication = application;
        this.listItem = listItem;
        this.mRes = mApplication.getResources();
        this.listener = listener;
    }

    public void setListItem(ArrayList<PackageAVNO> listItem) {
        this.listItem = listItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder holder;
        if (viewType == 0) {
            View view = LayoutInflater.from(mApplication).inflate(R.layout.holder_avno_package_new, parent, false);
            holder = new PackageAVNOHolderNew(view);
        } else {
            View view = LayoutInflater.from(mApplication).inflate(R.layout.holder_avno_pay_as_go, parent, false);
            holder = new PayAsGoHolder(view);
        }
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        PackageAVNO packageAVNO = listItem.get(position);
        if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_ACTIVE || packageAVNO.getStatus() == PackageAVNO.PACKAGE_AVAILABLE)
            return 0;
        else return 1;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final int type = getItemViewType(position);
        if (type == 0) {
            PackageAVNOHolderNew avno = (PackageAVNOHolderNew) holder;
            avno.setElement(listItem.get(position));
        } else {
            PayAsGoHolder payAsGo = (PayAsGoHolder) holder;
            payAsGo.setElement(listItem.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }


    class PackageAVNOHolderNew extends BaseViewHolder {

        TextView tvTitlePackage;
        TextView tvDescPackage;
        RoundTextView btnPackage;
        ImageView ivThumbPrice;
        View content;

        public PackageAVNOHolderNew(View itemView) {
            super(itemView);
            tvTitlePackage = itemView.findViewById(R.id.tvTitlePackage);
            tvDescPackage = itemView.findViewById(R.id.tvDescPackage);
            btnPackage = itemView.findViewById(R.id.btnPackage);
            ivThumbPrice = itemView.findViewById(R.id.ivThumbPrice);
            content = itemView.findViewById(R.id.llContent);
        }

        @Override
        public void setElement(Object obj) {
            final PackageAVNO packageAVNO = (PackageAVNO) obj;
            tvTitlePackage.setText(packageAVNO.getTitle());
            tvDescPackage.setText(packageAVNO.getDesc());
            if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_ACTIVE) {
                btnPackage.setText(mRes.getString(R.string.cancel));
                btnPackage.setVisibility(View.VISIBLE);
                ivThumbPrice.setVisibility(View.GONE);
                btnPackage.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.gray),
                        ContextCompat.getColor(mApplication, R.color.gray_light));
            } else if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_AVAILABLE) {
                btnPackage.setText(mRes.getString(R.string.register));
                btnPackage.setVisibility(View.VISIBLE);
                ivThumbPrice.setVisibility(View.GONE);
                btnPackage.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.bg_mocha),
                        ContextCompat.getColor(mApplication, R.color.bg_mocha_focus));
            } else {
                btnPackage.setVisibility(View.INVISIBLE);
                ivThumbPrice.setVisibility(View.GONE);
            }
            btnPackage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickActionPackage(packageAVNO);
                    }
                }
            });

            content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickContentPackage(packageAVNO);
                    }
                }
            });

        }
    }

    class PayAsGoHolder extends BaseViewHolder {

        TextView tvTitlePayAsGo;
        TextView tvDescPayAsGo;
        TextView tvPrice;
        ImageView ivThumbPrice;

        public PayAsGoHolder(View itemView) {
            super(itemView);
            tvTitlePayAsGo = itemView.findViewById(R.id.tvTitlePayAsGo);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDescPayAsGo = itemView.findViewById(R.id.tvDesPayAsGo);
            ivThumbPrice = itemView.findViewById(R.id.ivThumbPrice);
        }

        @Override
        public void setElement(Object obj) {
            final PackageAVNO packageAVNO = (PackageAVNO) obj;
            if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_CHARGE_CALL) {
                drawView(packageAVNO);
                ivThumbPrice.setImageResource(R.drawable.ic_avno_price_call);
            } else if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_CHARGE_SMS) {
                drawView(packageAVNO);
                ivThumbPrice.setImageResource(R.drawable.ic_avno_price_sms);
            } else {
                ivThumbPrice.setVisibility(View.INVISIBLE);
                tvDescPayAsGo.setVisibility(View.INVISIBLE);
                tvPrice.setVisibility(View.INVISIBLE);
                tvTitlePayAsGo.setVisibility(View.INVISIBLE);
            }
        }

        private void drawView(PackageAVNO packageAVNO) {
            tvDescPayAsGo.setVisibility(View.VISIBLE);
            tvPrice.setVisibility(View.VISIBLE);
            tvTitlePayAsGo.setVisibility(View.VISIBLE);
            ivThumbPrice.setVisibility(View.VISIBLE);
            tvPrice.setText(packageAVNO.getPrice());
            tvTitlePayAsGo.setText(packageAVNO.getTitle());
            tvDescPayAsGo.setText(packageAVNO.getDesc());
        }
    }
}

package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Song;
import com.metfone.selfcare.holder.guestbook.SelectMusicHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/18/2017.
 */
public class SelectMusicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;
    private ArrayList<Song> listItems;

    public SelectMusicAdapter(Context context, ArrayList<Song> items) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
        this.listItems = items;
    }

    public void setListItems(ArrayList<Song> items) {
        this.listItems = items;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        if (listItems == null) {
            return 0;
        }
        return listItems.size();
    }

    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = inflater.inflate(R.layout.holder_song, parent, false);
        SelectMusicHolder holder = new SelectMusicHolder(view, mContext);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((SelectMusicHolder) holder).setElement(getItem(position));
        if (mRecyclerClickListener != null)
            ((SelectMusicHolder) holder).setViewClick(position, getItem(position));
    }
}

package com.metfone.selfcare.adapter.onmedia;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.holder.onmedia.feeds.ItemInvisibleHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedAudioViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedBannerViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedBaseViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedChannelViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedImageAlbumViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedImageSingleViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedMovieViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedNewsViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedSuggestFriendViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedTotalViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedVideoViewHolder;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/4/2016.
 */
public class OMFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = OMFeedAdapter.class.getSimpleName();

    private static final int TYPE_UNKNOW = 0;
    //    private static final int TYPE_SONG = 1;
//    private static final int TYPE_ALBUM = 2;
    private static final int TYPE_VIDEO = 3;
    private static final int TYPE_NEWS = 4;
    private static final int TYPE_TOTAL = 5;
    private static final int TYPE_IMAGE_SINGLE = 6;    //type profile album, nhung chi co 1 anh, type profile_avatar
    private static final int TYPE_IMAGE_ALBUM = 7;
    private static final int TYPE_SUGGEST_FRIEND = 8;
    private static final int TYPE_BANNER = 9;
    private static final int TYPE_MOVIE = 10;
    private static final int TYPE_AUDIO_MUSIC = 11;
    private static final int TYPE_CHANNEL = 12;
    private ApplicationController mApplication;
    private ArrayList<FeedModelOnMedia> listFeed;
    private LayoutInflater inflater;
    private OnMediaHolderListener listener;
    private TagMocha.OnClickTag onClickTag;

    private @Nullable
    BaseSlidingFragmentActivity activity;

    public OMFeedAdapter(ApplicationController app, ArrayList<FeedModelOnMedia> arrayList,
                         OnMediaHolderListener listener, TagMocha.OnClickTag onClickTag) {
        this.mApplication = app;
        this.listFeed = arrayList;
        this.listener = listener;
        this.onClickTag = onClickTag;
        this.inflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListFeed(ArrayList<FeedModelOnMedia> listFeed) {
        this.listFeed = listFeed;
    }

    public void setActivity(@Nullable BaseSlidingFragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public int getItemCount() {
        if (listFeed == null) return 0;
        return listFeed.size();
    }

    public Object getItem(int position) {
        if (listFeed != null && listFeed.size() > position && position >= 0)
            return listFeed.get(position);
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        Object object = getItem(position);
        if (object instanceof FeedModelOnMedia) {
            FeedModelOnMedia feedsModel = (FeedModelOnMedia) getItem(position);
            if (feedsModel != null && feedsModel.getFeedContent() != null) {
                String type = feedsModel.getFeedContent().getItemType();
                String subType = feedsModel.getFeedContent().getItemSubType();
                if (OnMediaHelper.isFeedViewVideo(feedsModel)) {
                    return TYPE_VIDEO;
                } else if (FeedContent.ITEM_TYPE_IMAGE.equals(type)
                        || FeedContent.ITEM_TYPE_NEWS.equals(type)
                        || FeedContent.ITEM_TYPE_AUDIO.equals(type)
                        || FeedContent.ITEM_TYPE_PROFILE_STATUS.equals(type)
                        || (FeedContent.ITEM_TYPE_SOCIAL.equals(type) &&
                        (FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(subType)
                                || FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(subType)))
                        ) {
                    return TYPE_NEWS;
                } else if (FeedContent.ITEM_TYPE_TOTAL.equals(type)) {
                    return TYPE_TOTAL;
                } else if (FeedContent.ITEM_TYPE_PROFILE_AVATAR.equals(type)
                        || FeedContent.ITEM_TYPE_PROFILE_COVER.equals(type)
                        || (FeedContent.ITEM_TYPE_PROFILE_ALBUM.equals(type) &&
                        feedsModel.getFeedContent().getListImage().size() == 1)
                        || (FeedContent.ITEM_TYPE_SOCIAL.equals(type) &&
                        FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(subType) &&
                        feedsModel.getFeedContent().getListImage().size() == 1))
                         {
                    return TYPE_IMAGE_SINGLE;
                } else if ((FeedContent.ITEM_TYPE_PROFILE_ALBUM.equals(type) &&
                        feedsModel.getFeedContent().getListImage().size() > 1)
                        || (FeedContent.ITEM_TYPE_SOCIAL.equals(type) &&
                        FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(subType) &&
                        feedsModel.getFeedContent().getListImage().size() > 1)
                        ) {
                    return TYPE_IMAGE_ALBUM;
                } else if (FeedContent.ITEM_TYPE_SUGGEST_FRIEND.equals(type)) {
                    return TYPE_SUGGEST_FRIEND;
                } else if (FeedContent.ITEM_TYPE_BANNER.equals(type)) {
                    return TYPE_BANNER;
                } else if (OnMediaHelper.isFeedViewMovie(feedsModel)) {
                    return TYPE_MOVIE;
                } else if (OnMediaHelper.isFeedViewAudio(feedsModel)) {
                    return TYPE_AUDIO_MUSIC;
                } else if (OnMediaHelper.isFeedViewChannel(feedsModel)) {
                    return TYPE_CHANNEL;
                }
            }
        }
        return TYPE_UNKNOW;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        OMFeedBaseViewHolder viewHolder;
        Log.i(TAG, "Create view holder viewType: " + viewType);
        switch (viewType) {
            case TYPE_VIDEO: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_video, parent, false);
                viewHolder = new OMFeedVideoViewHolder(view, mApplication, activity);
                viewHolder.initCommonView(view);
                viewHolder.initViewMediaHolder(view);
            }
            break;

            case TYPE_NEWS: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_news, parent, false);
                viewHolder = new OMFeedNewsViewHolder(view, mApplication);
                viewHolder.initCommonView(view);
                viewHolder.initViewMediaHolder(view);
            }
            break;

            case TYPE_TOTAL: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_total, parent, false);
                viewHolder = new OMFeedTotalViewHolder(view, mApplication);
            }
            break;

            case TYPE_IMAGE_SINGLE: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_image_single, parent, false);
                viewHolder = new OMFeedImageSingleViewHolder(view, mApplication);
                viewHolder.initCommonView(view);
                viewHolder.initViewMediaHolder(view);
            }
            break;

            case TYPE_IMAGE_ALBUM: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_image_album, parent, false);
                viewHolder = new OMFeedImageAlbumViewHolder(view, mApplication);
                viewHolder.initCommonView(view);
            }
            break;

            case TYPE_SUGGEST_FRIEND: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_suggest_friend, parent, false);
                viewHolder = new OMFeedSuggestFriendViewHolder(view, mApplication);
                viewHolder.initCommonView(view);
            }
            break;

            case TYPE_BANNER: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_banner, parent, false);
                viewHolder = new OMFeedBannerViewHolder(view, mApplication);
            }
            break;

            case TYPE_MOVIE: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_movie, parent, false);
                viewHolder = new OMFeedMovieViewHolder(view, mApplication, activity);
                viewHolder.initCommonView(view);
            }
            break;

            case TYPE_AUDIO_MUSIC: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_audio, parent, false);
                viewHolder = new OMFeedAudioViewHolder(view, mApplication, activity);
                viewHolder.initCommonView(view);
                viewHolder.initViewMediaHolder(view);
            }
            break;

            case TYPE_CHANNEL: {
                View view = inflater.inflate(R.layout.holder_onmedia_feeds_channel, parent, false);
                viewHolder = new OMFeedChannelViewHolder(view, mApplication, activity);
                viewHolder.initCommonView(view);
            }
            break;

            default: {
                View view = inflater.inflate(R.layout.holder_feed_default, parent, false);
                viewHolder = new ItemInvisibleHolder(view, mApplication);
                Log.i(TAG, "unknown type: ");
            }
            break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final int type = getItemViewType(position);
        FeedModelOnMedia feedModel = (FeedModelOnMedia) getItem(position);
        if (holder instanceof OMFeedBaseViewHolder) {
            OMFeedBaseViewHolder viewHolder = (OMFeedBaseViewHolder) holder;
            viewHolder.setOnClickTag(onClickTag);
            viewHolder.setOnMediaHolderListener(listener);
            switch (type) {
                case TYPE_VIDEO:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_NEWS:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_TOTAL:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_IMAGE_SINGLE:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_IMAGE_ALBUM:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_SUGGEST_FRIEND:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_BANNER:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_MOVIE:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_AUDIO_MUSIC:
                    viewHolder.setElement(feedModel);
                    break;
                case TYPE_CHANNEL:
                    viewHolder.setElement(feedModel);
                    break;
                default:
                    break;
            }
        }
    }
}

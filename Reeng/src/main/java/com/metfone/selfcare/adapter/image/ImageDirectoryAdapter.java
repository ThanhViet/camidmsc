package com.metfone.selfcare.adapter.image;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.ImageBrowserActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageDirectory;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.IImageBrowser;

/**
 * Created by thanhnt72 on 10/3/2017.
 */
public class ImageDirectoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ImageDirectoryAdapter.class.getSimpleName();
    private ImageBrowserActivity mImageBrowserActivity;
    private IImageBrowser.SelectImageDirectoryListener mSelectImageDirectoryListener;

    private LayoutInflater inflater;

    public ImageDirectoryAdapter(ImageBrowserActivity mImageBrowserActivity, IImageBrowser
            .SelectImageDirectoryListener mSelectImageDirectoryListener) {
        this.mImageBrowserActivity = mImageBrowserActivity;
        this.mSelectImageDirectoryListener = mSelectImageDirectoryListener;
        inflater = (LayoutInflater) mImageBrowserActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_album_v5, parent, false);
        ImageDirectoryHolder holder = new ImageDirectoryHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ImageDirectoryHolder imageDirectoryHolder = (ImageDirectoryHolder) holder;
        imageDirectoryHolder.setElement(mImageBrowserActivity.getImageDirectoryList().get(position));
    }

    @Override
    public int getItemCount() {
        if (mImageBrowserActivity.getImageDirectoryList() == null
                || mImageBrowserActivity.getImageDirectoryList().isEmpty())
            return 0;
        else return mImageBrowserActivity.getImageDirectoryList().size();
    }

    private class ImageDirectoryHolder extends BaseViewHolder {

        View itemView;
        AppCompatImageView imgFirstImage;
        AppCompatTextView directoryName;
        //        TextView numberImgSelected;
        AppCompatTextView numberImage;

        public ImageDirectoryHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            imgFirstImage =  itemView.findViewById(R.id.imgAlbum);
            directoryName = itemView.findViewById(R.id.tvName);
//            numberImgSelected = (TextView) itemView.findViewById(R.id.number_image_selected);
            numberImage = itemView.findViewById(R.id.tvCount);
        }

        @Override
        public void setElement(Object obj) {
            final ImageDirectory dir = (ImageDirectory) obj;
            directoryName.setText(dir.getParentName());
//            if (dir.getCountSelectedImage() > 0) {
//                numberImgSelected.setText("+" + dir.getCountSelectedImage());
//                numberImgSelected.setVisibility(View.VISIBLE);
//            } else {
//                numberImgSelected.setVisibility(View.INVISIBLE);
//            }
            numberImage.setText(dir.getListFile().size() + "");
            ImageInfo image = dir.getFirstImage();
            if (image != null) {
                String imagePath = image.getImagePath();
                if (image.getVideoContentURI() != null && !TextUtils.isEmpty(image.getVideoContentURI())) {
                    ImageLoaderManager.getInstance(mImageBrowserActivity).displayThumbnailVideo(image
                            .getVideoContentURI(), imgFirstImage);
                } else {
                    ImageLoaderManager.getInstance(mImageBrowserActivity).displayThumbleOfGallery(imagePath,
                            imgFirstImage);
                }
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSelectImageDirectoryListener != null) {
                        mSelectImageDirectoryListener.onSelectImageDirectory(dir, getAdapterPosition());
                    }
                }
            });
        }
    }
}

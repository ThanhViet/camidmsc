package com.metfone.selfcare.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.message.MessageConstants;
import com.metfone.selfcare.holder.message.BaseMessageHolder;
import com.metfone.selfcare.holder.message.MessageNotificationHolder;
import com.metfone.selfcare.holder.message.ReceivedActionMusicHolder;
import com.metfone.selfcare.holder.message.ReceivedBankplusHolder;
import com.metfone.selfcare.holder.message.ReceivedCallHolder;
import com.metfone.selfcare.holder.message.ReceivedCrbtGiftHolder;
import com.metfone.selfcare.holder.message.ReceivedDeepLinkHolder;
import com.metfone.selfcare.holder.message.ReceivedFileFullScreenHolder;
import com.metfone.selfcare.holder.message.ReceivedFileHolder;
import com.metfone.selfcare.holder.message.ReceivedGreetingStickerHolder;
import com.metfone.selfcare.holder.message.ReceivedImageHolder;
import com.metfone.selfcare.holder.message.ReceivedInviteMusicHolder;
import com.metfone.selfcare.holder.message.ReceivedSearchRankHolder;
import com.metfone.selfcare.holder.message.ReceivedShareContactHolder;
import com.metfone.selfcare.holder.message.ReceivedShareLocationHolder;
import com.metfone.selfcare.holder.message.ReceivedShareVideoHolder;
import com.metfone.selfcare.holder.message.ReceivedTextHolder;
import com.metfone.selfcare.holder.message.ReceivedTransferMoneyHolder;
import com.metfone.selfcare.holder.message.ReceivedVoiceStickerHolder;
import com.metfone.selfcare.holder.message.ReceivedVoicemailHolder;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

public class QuickReplyAdapter extends BaseAdapter {
    private static final long TIME_DIVIDE_THREAD_SECTION = 30 * 60 * 1000; //30 phut

    private static final String TAG = QuickReplyAdapter.class.getSimpleName();
    private ArrayList<ReengMessage> messages;
    private ApplicationController applicationController;
    private SmartTextClickListener mSmartTextListener;
    private MessageInteractionListener mMessageInteractionListener;
    //    private boolean isPreviewMessageEnable;
    private LayoutInflater inflater;

    public QuickReplyAdapter(ApplicationController context, ArrayList<ReengMessage> messages,
                             MessageInteractionListener listener) {
        applicationController = context;
//        isPreviewMessageEnable = mSettingBusiness.getPrefPreviewMsg();
        this.messages = messages;
//        mSmartTextListener = smartTextListener;
        mMessageInteractionListener = listener;
        inflater = LayoutInflater.from(applicationController);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public ReengMessage getItem(int position) {
        return messages.get(position);
    }

    @Override
    public int getCount() {
        /*if (!isPreviewMessageEnable) {
            return 1;
        }*/
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        ReengMessage message = getItem(position);
        ReengMessageConstant.MessageType mMessageType = message.getMessageType();
        if (mMessageType == ReengMessageConstant.MessageType.text)
            return MessageConstants.TYPE_MESSAGE_RECEIVED_TEXT;
        if (mMessageType == ReengMessageConstant.MessageType.voicemail)
            return MessageConstants.TYPE_MESSAGE_RECEIVED_VOICEMAIL;
        if (mMessageType == ReengMessageConstant.MessageType.file)
            return MessageConstants.TYPE_MESSAGE_RECEIVED_FILE;
        if (mMessageType == ReengMessageConstant.MessageType.image)
            return MessageConstants.TYPE_MESSAGE_RECEIVED_IMAGE;
        if (mMessageType == ReengMessageConstant.MessageType.shareContact)
            return MessageConstants.TYPE_MESSAGE_RECEIVED_SHARE_CONTACT;
        if (mMessageType == ReengMessageConstant.MessageType.shareVideo)
            return MessageConstants.TYPE_MESSAGE_RECEIVED_SHARE_VIDEO;
        if (mMessageType == ReengMessageConstant.MessageType.voiceSticker)
            return MessageConstants.TYPE_MESSAGE_RECEIVED_VOICE_STICKER;
        if (mMessageType == ReengMessageConstant.MessageType.inviteShareMusic) {
            if (message.getSize() == 0) {
                return MessageConstants.TYPE_MESSAGE_NOTIFY;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_INVITE_MUSIC_NEW;
            }
        }
        if (mMessageType == ReengMessageConstant.MessageType.actionShareMusic) {// chuyen bai, doi bai
            return MessageConstants.TYPE_MESSAGE_RECEIVED_ACTION_MUSIC;
        }
        if (mMessageType == ReengMessageConstant.MessageType.notification ||
                mMessageType == ReengMessageConstant.MessageType.notification_fake_mo ||
                mMessageType == ReengMessageConstant.MessageType.poll_action) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        }
        if (mMessageType == ReengMessageConstant.MessageType.voiceSticker) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_VOICE_STICKER;
        }
        if (mMessageType == ReengMessageConstant.MessageType.greeting_voicesticker) {
            return MessageConstants.TYPE_MESSAGE_GREETING_VOICE_STICKER;
        }
        if (mMessageType == ReengMessageConstant.MessageType.shareLocation) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_LOCATION;
        }
        if (mMessageType == ReengMessageConstant.MessageType.transferMoney) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_TRANSFER_MONEY;
        }
        if (mMessageType == ReengMessageConstant.MessageType.event_follow_room ||
                mMessageType == ReengMessageConstant.MessageType.fake_mo ||
                mMessageType == ReengMessageConstant.MessageType.poll_create) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_SEARCH_RANK;
        }
        if (mMessageType == ReengMessageConstant.MessageType.restore) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_RESTORE;
        }
        if (mMessageType == ReengMessageConstant.MessageType.crbt_gift) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_CRBT_GIFT;
        }
        if (mMessageType == ReengMessageConstant.MessageType.deep_link ||
                mMessageType == ReengMessageConstant.MessageType.luckywheel_help) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_DEEP_LINK;
        }
        if (mMessageType == ReengMessageConstant.MessageType.gift) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_VOICE_STICKER;
        }
        if (mMessageType == ReengMessageConstant.MessageType.image_link) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_FILE_LINK;
        }
        if (mMessageType == ReengMessageConstant.MessageType.call ||
                mMessageType == ReengMessageConstant.MessageType.talk_stranger) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_CALL;
        }
        if (mMessageType == ReengMessageConstant.MessageType.watch_video) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_TEXT;
        }
        if (mMessageType == ReengMessageConstant.MessageType.bank_plus) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_BPLUS;
        }
        return MessageConstants.TYPE_MESSAGE_NOTIFY;// tra ve default la notify
    }

    @Override
    public int getViewTypeCount() {
        return MessageConstants.TYPE_MESSAGE_MAX_COUNT;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ReengMessage message = getItem(position);
        int type = getItemViewType(position);
        BaseMessageHolder holder;
        switch (type) {
            case MessageConstants.TYPE_MESSAGE_RECEIVED_TEXT:
                if (convertView == null) {
                    holder = new ReceivedTextHolder(applicationController, mSmartTextListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedTextHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_VOICEMAIL:
                if (convertView == null) {
                    holder = new ReceivedVoicemailHolder(applicationController, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedVoicemailHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_IMAGE:
                if (convertView == null) {
                    holder = new ReceivedImageHolder(applicationController, mSmartTextListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedImageHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_FILE:
                if (convertView == null) {
                    holder = new ReceivedFileHolder(applicationController, mSmartTextListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedFileHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_RECEIVED_SHARE_CONTACT:
                if (convertView == null) {
                    holder = new ReceivedShareContactHolder(applicationController, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedShareContactHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_RECEIVED_SHARE_VIDEO:
                if (convertView == null) {
                    holder = new ReceivedShareVideoHolder(applicationController, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedShareVideoHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_INVITE_MUSIC_NEW:
                if (convertView == null) {
                    holder = new ReceivedInviteMusicHolder(applicationController, mMessageInteractionListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedInviteMusicHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_NOTIFY:
                if (convertView == null) {
                    holder = new MessageNotificationHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (MessageNotificationHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_VOICE_STICKER:
                if (convertView == null) {
                    holder = new ReceivedVoiceStickerHolder(applicationController, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedVoiceStickerHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_GREETING_VOICE_STICKER:
                if (convertView == null) {
                    holder = new ReceivedGreetingStickerHolder(applicationController, mMessageInteractionListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedGreetingStickerHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_ACTION_MUSIC:
                if (convertView == null) {
                    holder = new ReceivedActionMusicHolder(applicationController, mMessageInteractionListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedActionMusicHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_LOCATION:
                if (convertView == null) {
                    holder = new ReceivedShareLocationHolder(applicationController, mMessageInteractionListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedShareLocationHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_TRANSFER_MONEY:
                if (convertView == null) {
                    holder = new ReceivedTransferMoneyHolder(applicationController, mSmartTextListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedTransferMoneyHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_SEARCH_RANK:
                if (convertView == null) {
                    holder = new ReceivedSearchRankHolder(applicationController, mMessageInteractionListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedSearchRankHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_RESTORE:
                if (convertView == null) {
                    holder = new ReceivedTextHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedTextHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_CRBT_GIFT:
                if (convertView == null) {
                    holder = new ReceivedCrbtGiftHolder(applicationController, mMessageInteractionListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedCrbtGiftHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_DEEP_LINK:
                if (convertView == null) {
                    holder = new ReceivedDeepLinkHolder(applicationController, mMessageInteractionListener, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedDeepLinkHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_FILE_LINK:
                if (convertView == null) {
                    holder = new ReceivedFileFullScreenHolder(applicationController, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedFileFullScreenHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_CALL:
                if (convertView == null) {
                    holder = new ReceivedCallHolder(applicationController, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedCallHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_BPLUS:
                if (convertView == null) {
                    holder = new ReceivedBankplusHolder(applicationController, true);
                    holder.initHolder(parent, convertView, position, inflater);
                } else {
                    holder = (ReceivedBankplusHolder) convertView.getTag();
                }
                break;
            default:
                throw new IllegalArgumentException("" + type);
        }

        holder.setMessageInteractionListener(mMessageInteractionListener);
        /*processSameDateWithPrevious(position, message, holder);
        //-----------------------------------------
        processDifferSenderWithAfter(position, message, holder);
        holder.setElement(message);*/

        if (position <= 0) {
            // neu la phan tu dau tien
            holder.setSameDateWithPrevious(false);
            holder.setDifferSenderWithPrevious(true);
        } else {
            // kiem tra neu tin nay voi tin truoc do cach nhau 10p time_divide_thread_section
            // truong hop may nhan dc gui
            ReengMessage previousMessage = messages.get(position - 1);
            if ((message.getTime() - previousMessage.getTime()) > -TIME_DIVIDE_THREAD_SECTION &&
                    (message.getTime() - previousMessage.getTime()) < TIME_DIVIDE_THREAD_SECTION) {
                holder.setSameDateWithPrevious(true);
            } else {
                holder.setSameDateWithPrevious(false);
            }
            // message trc la notification, messag sau la thuong, thi hien avatar
            if (previousMessage.getMessageType() == ReengMessageConstant.MessageType.notification &&
                    message.getMessageType() != ReengMessageConstant.MessageType.notification) {
                holder.setDifferSenderWithPrevious(true);
            } else if (message.getSender().equals(previousMessage.getSender()) && holder.isSameDateWithPrevious()) {
                // kiem tra neu tin nay voi tin truoc cungf 1 nguoi gui
                holder.setDifferSenderWithPrevious(false);
            } else {
                holder.setDifferSenderWithPrevious(true);
            }
            if (!previousMessage.isNewMessage() && message.isNewMessage()) {

                holder.setFirstNewMessage(true);
            } else {
                holder.setFirstNewMessage(false);
            }
        }
        //-----------------------------------------
        if (position == (messages.size() - 1)) {
            // neu la phan tu cuoi cung
            holder.setDifferSenderWithAfter(true);
        } else {
            // kiem tra neu tin nay voi tin sau do la cung 1 nguoi gui
            if (message.getSender().equals(
                    messages.get(position + 1).getSender())) {
                holder.setDifferSenderWithAfter(false);
            } else {
                holder.setDifferSenderWithAfter(true);
            }
        }
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        initDownloadOrUpload(message, holder.getConvertView());
        holder.setElemnts(message);

//        bubbleView = initBubbleView(parent, position, message, type, bubbleView, inflater);

        /*if (isPreviewMessageEnable) {
            //neu duoc xem noi dung tin nhan thi hien thi view cua message holder
            bubbleView = initBubbleView(parent, position, message, type, bubbleView, inflater);
        } else {
            //neu khong duoc xem noi dung tin nhan thi hien thi dong text thong bao
            bubbleView = showSecretMessageView(inflater);
        }*/
        return holder.getConvertView();
    }


    private View showSecretMessageView(LayoutInflater mLayoutInflater) {
        View layout = mLayoutInflater.inflate(R.layout.quick_reply_secretview_in_pager, null);
        TextView tvwSecretMsg = (TextView) layout.findViewById(R.id.txt_secretview);
        tvwSecretMsg.setText(String.format(applicationController.getResources().getString(R.string.have_some_new_message), messages.size()));
        return layout;
    }

    public void setListItem(ArrayList<ReengMessage> listMessage) {
        this.messages = listMessage;
    }

    public void addItem(ReengMessage mReengMessage) {
        Log.i(TAG, "So phan tu " + messages.size());
        // check currentReengMessage in  messages

        boolean has = false;
        for (ReengMessage reengMessage : messages) {
            if (reengMessage.getId() == mReengMessage.getId()) {
                has = true;
                break;
            }
        }
        if (!has) {
            messages.add(mReengMessage);
            notifyDataSetChanged();
        } else {
            Log.d(TAG, "currentReengMessage has in message");
        }
    }
}

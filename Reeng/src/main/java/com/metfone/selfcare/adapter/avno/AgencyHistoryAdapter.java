package com.metfone.selfcare.adapter.avno;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.AgencyHistory;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/20/2019.
 */

public class AgencyHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private ArrayList<AgencyHistory> listItem;
    private Resources mRes;

    private AgencyHistoryListener listener;


    public AgencyHistoryAdapter(BaseSlidingFragmentActivity activity, ArrayList<AgencyHistory> listItem, AgencyHistoryListener listener) {
        this.activity = activity;
        mApplication = (ApplicationController) activity.getApplication();
        this.listItem = listItem;
        this.listener = listener;
    }


    public void setListItem(ArrayList<AgencyHistory> listItem) {
        this.listItem = listItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_agency_history,
                parent, false);
        AgencyHistoryHolder holder = new AgencyHistoryHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((AgencyHistoryHolder) holder).setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }


    private class AgencyHistoryHolder extends BaseViewHolder {

        TextView tvTime, tvName, tvAmount;
        RoundTextView btnSelect;
        CircleImageView ivTime;

        public AgencyHistoryHolder(View itemView) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvName = itemView.findViewById(R.id.tvName);
            tvAmount = itemView.findViewById(R.id.tvAmount);
            btnSelect = itemView.findViewById(R.id.btnSelect);
            ivTime = itemView.findViewById(R.id.ivTime);
        }

        @Override
        public void setElement(Object obj) {

            final AgencyHistory agencyHistory = (AgencyHistory) obj;
            tvTime.setText(agencyHistory.getTimeStamp());
            String name = agencyHistory.getMsisdn() + " (" + agencyHistory.getName() + ")";
            tvName.setText(name);
            String amount = agencyHistory.getAmount() + " " + agencyHistory.getUnit();
            tvAmount.setText(amount);

            btnSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) listener.onSelectHistory(agencyHistory);
                }
            });
            ivTime.setImageBitmap(mApplication.getAvatarBusiness().getBitmapColorAvatar(agencyHistory.getMsisdn()));

        }
    }

    public interface AgencyHistoryListener {
        public void onSelectHistory(AgencyHistory agencyHistory);
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.listeners.SwipeAdapterInterface;
import com.metfone.selfcare.listeners.SwipeManagerImplement;
import com.metfone.selfcare.listeners.SwipeManagerInterface;
import com.metfone.selfcare.ui.SwipeLayout;
import com.metfone.selfcare.util.Attributes;

import java.util.List;

/**
 * Created by namnh40 on 6/5/2015.
 */
public abstract class BaseSwipeAdapter extends BaseAdapter
        implements SwipeManagerInterface, SwipeAdapterInterface {

    protected SwipeManagerImplement mItemManger = new SwipeManagerImplement(this);
    protected LayoutInflater mInflater;
    protected ApplicationController mApplication;
    protected Resources mRes;
    protected Context mContext;

    public BaseSwipeAdapter(Context context) {
        super();
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRes = mContext.getResources();
        mApplication = (ApplicationController) mContext.getApplicationContext();
    }

    public abstract int getSwipeLayoutResourceId(int position);

    /**
     * generate a new view item.
     * Never setData SwipeListener or fill values here, every item has a chance to fill value or setData
     * listeners in fillValues.
     * to fill it in {@code fillValues} method.
     *
     * @param position
     * @param parent
     * @return
     */
    public abstract View generateView(int position, ViewGroup parent);

    /**
     * fill values or setData listeners to the view.
     *
     * @param position
     * @param convertView
     */
    public abstract void fillValues(int position, View convertView);

    @Override
    public void notifyDatasetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = generateView(position, parent);
        }
        mItemManger.bind(v, position);
        fillValues(position, v);
        return v;
    }

    @Override
    public void openItem(int position) {
        mItemManger.openItem(position);
    }

    @Override
    public void closeItem(int position) {
        mItemManger.closeItem(position);
    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {
        mItemManger.closeAllExcept(layout);
    }

    @Override
    public void closeAllItems() {
        mItemManger.closeAllItems();
    }

    @Override
    public List<Integer> getOpenItems() {
        return mItemManger.getOpenItems();
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return mItemManger.getOpenLayouts();
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {
        mItemManger.removeShownLayouts(layout);
    }

    @Override
    public boolean isOpen(int position) {
        return mItemManger.isOpen(position);
    }

    @Override
    public Attributes.Mode getMode() {
        return mItemManger.getMode();
    }

    @Override
    public void setMode(Attributes.Mode mode) {
        mItemManger.setMode(mode);
    }
}


package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 6/7/2017.
 */
public class ListImageOnMediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ListImageOnMediaAdapter.class.getSimpleName();
    private ArrayList<String> listImg = new ArrayList<>();
    private ApplicationController mApp;
    private LayoutInflater mLayoutInflater;
    private OnListImageChange listener;

    public ListImageOnMediaAdapter(ApplicationController mApp, OnListImageChange listener) {
        this.mApp = mApp;
        this.mLayoutInflater = (LayoutInflater) mApp
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listener = listener;
    }

    public void setListImg(ArrayList<String> listImg) {
        this.listImg = listImg;
    }

    public ArrayList<String> getListImg() {
        return listImg;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_image_horizontal, parent, false);
        ViewImageHolder viewImageHolder = new ViewImageHolder(view);
        return viewImageHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String filePath = listImg.get(position);
        ((ViewImageHolder) holder).setElement(filePath);
    }

    @Override
    public int getItemCount() {
        return listImg.size();
    }

    class ViewImageHolder extends BaseViewHolder {

        private ImageView mImgThumb;
        private ImageView mImgClose;

        public ViewImageHolder(View itemView) {
            super(itemView);
            mImgThumb = (ImageView) itemView.findViewById(R.id.img_thumb_onmedia);
            mImgClose = (ImageView) itemView.findViewById(R.id.img_remove_img);
        }

        @Override
        public void setElement(Object obj) {
            final String filePath = (String) obj;
            Log.i(TAG, "setElement: " + filePath);
            ImageLoaderManager.getInstance(mApp).displayThumbleOfGallery(filePath, mImgThumb);
            mImgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listImg.remove(filePath);
                    notifyDataSetChanged();
                    if (listener != null) {
                        listener.onRemoveImage();
                    }
                }
            });
        }
    }

    public interface OnListImageChange {
        void onRemoveImage();
    }
}


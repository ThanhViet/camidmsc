package com.metfone.selfcare.adapter.avno;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.ItemInfo;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 7/26/2018.
 */

public class ItemInfoAVNOAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ItemInfoAVNOAdapter.class.getSimpleName();

    private ApplicationController mApplication;
    private ArrayList<ItemInfo> listItem;
    private BaseSlidingFragmentActivity activity;


    public ItemInfoAVNOAdapter(BaseSlidingFragmentActivity activity, ArrayList<ItemInfo> listItem) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplication();
        this.listItem = listItem;
    }

    public void setListItem(ArrayList<ItemInfo> listItem) {
        this.listItem = listItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemInfoAVNOHolder holder;
        View view = LayoutInflater.from(mApplication).inflate(R.layout.holder_item_info_avno, parent, false);
        holder = new ItemInfoAVNOHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ItemInfoAVNOHolder avno = (ItemInfoAVNOHolder) holder;
        avno.setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }


    class ItemInfoAVNOHolder extends BaseViewHolder {

        private TextView mTvwTitle;
        private TextView mTvwDesc;
        private RoundTextView mBtnDeeplink;
        private View viewDivider;

        public ItemInfoAVNOHolder(View itemView) {
            super(itemView);
            mTvwTitle = itemView.findViewById(R.id.tvTitleInfo);
            mTvwDesc = itemView.findViewById(R.id.tvDescInfo);
            mBtnDeeplink = itemView.findViewById(R.id.btnDeeplink);
            viewDivider = itemView.findViewById(R.id.view_divider_info);
        }


        @Override
        public void setElement(Object obj) {
            final ItemInfo itemInfo = (ItemInfo) obj;
            mTvwTitle.setText(itemInfo.getTitle());
            mTvwDesc.setText(itemInfo.getDesc());
            if (!TextUtils.isEmpty(itemInfo.getDeeplink()) && !TextUtils.isEmpty(itemInfo.getDeeplinkLabel())) {
                mBtnDeeplink.setVisibility(View.VISIBLE);
                mBtnDeeplink.setText(itemInfo.getDeeplinkLabel());
            } else {
                mBtnDeeplink.setVisibility(View.GONE);
            }
            if (itemInfo.isLastItem()) {
                viewDivider.setVisibility(View.GONE);
            } else {
                viewDivider.setVisibility(View.VISIBLE);
            }
            mBtnDeeplink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, itemInfo.getDeeplink());
                }
            });
        }
    }
}

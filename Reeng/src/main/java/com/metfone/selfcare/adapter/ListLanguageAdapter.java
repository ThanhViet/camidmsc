package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.ui.PopupSingleChooseFragment;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 8/12/2015.
 */
public class ListLanguageAdapter extends BaseAdapter {
    private Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<Region> data;
    private ListlanguageListener listener;
    private PopupSingleChooseFragment.SingleChooseListener singleChooseListener;

    public ListLanguageAdapter(Context context, ArrayList<Region> data, ListlanguageListener listener) {
        ctx = context;
        this.data = data;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listener = listener;
    }

    public void setSingleChooseListener(PopupSingleChooseFragment.SingleChooseListener singleChooseListener) {
        this.singleChooseListener = singleChooseListener;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Region region = data.get(position);
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.holder_language, parent, false);
        }
        /*RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layout_language);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onCheckboxClick(position);
                }
            }
        });*/
        ((TextView) view.findViewById(R.id.txt_language)).setText(region.getRegionName());
        RadioButton checkbox = (RadioButton) view.findViewById(R.id.cb_language);
        checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onCheckboxClick(position);
                }
                if (singleChooseListener != null) {
                    singleChooseListener.onChoose(data.get(position));
                }
            }
        });

        if (region.isSelected()) {
            checkbox.setChecked(true);
        } else {
            checkbox.setChecked(false);
        }
        return view;
    }


    public interface ListlanguageListener {
        void onCheckboxClick(int position);
    }
}

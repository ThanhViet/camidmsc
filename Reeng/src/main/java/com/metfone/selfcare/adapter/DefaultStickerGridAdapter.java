package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.images.ImageLoaderManager;

/**
 * Created by thaodv on 05-Jan-15.
 */
public class DefaultStickerGridAdapter extends BaseEmoGridAdapter {

    public DefaultStickerGridAdapter(Context context,
                                     EmoticonsGridAdapter.KeyClickListener listener) {
        super(EmoticonUtils.getStickerImagePaths());
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = getBaseView(position, convertView, parent);
        //float dimensionTextSize = this.mContext.getResources().getDimension(R.dimen.mocha_text_size_level_2_5);
        image.getLayoutParams().height = mContext.getResources().getDimensionPixelSize(R.dimen.sticker_grid_height);
        image.getLayoutParams().width = mContext.getResources().getDimensionPixelSize(R.dimen.sticker_grid_height);
        int paddingImage = mContext.getResources().getDimensionPixelSize(R.dimen.margin_more_content_5);
        image.setPadding(paddingImage,paddingImage,paddingImage,paddingImage);
        final StickerItem stickerItem = EmoticonUtils.getGeneralStickerDefault()[position];
        ImageLoaderManager.getInstance(mContext).displayStickerImage(image,EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID,stickerItem.getItemId());
        emoLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mListener.stickerClicked(stickerItem);
            }
        });
//
        return v;
    }
}

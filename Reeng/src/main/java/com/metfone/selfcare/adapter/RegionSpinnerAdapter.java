package com.metfone.selfcare.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.metfone.selfcare.holder.RegionSpinnerHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toanvk2 on 26/05/2015.
 */
public class RegionSpinnerAdapter<T> extends BaseAdapter {
    private List<T> listRegions;
    public Resources mRes;
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public RegionSpinnerAdapter(Context context, ArrayList<T> listRegions) {
        //super(context, textViewResourceId, listRegions);
        this.mContext = context;
        this.listRegions = listRegions;
        //this.mRes = mContext.getResources();
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListRegions(List<T> listRegions) {
        this.listRegions = listRegions;
    }

    @Override
    public int getCount() {
        return listRegions.size();
    }

    @Override
    public Object getItem(int position) {
        return listRegions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        Object item = getItem(position);
        // get selected entry
        RegionSpinnerHolder holder;
        if (convertView == null) {
            holder = new RegionSpinnerHolder(mContext);
            holder.initHolder(parent, convertView, position, mLayoutInflater);
        } else {
            holder = (RegionSpinnerHolder) convertView.getTag();
        }
        holder.setElemnts(item);
        return holder.getConvertView();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Object item = getItem(position);
        // get selected entry
        RegionSpinnerHolder holder;
        if (convertView == null) {
            holder = new RegionSpinnerHolder(mContext);
            holder.initHolder(parent, convertView, position, mLayoutInflater);
        } else {
            holder = (RegionSpinnerHolder) convertView.getTag();
        }
        holder.setCustomView(item);
        return holder.getConvertView();
    }
}
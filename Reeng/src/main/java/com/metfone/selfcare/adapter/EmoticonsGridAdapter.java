package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.metfone.selfcare.helper.emoticon.EmoticonManager;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;

public class EmoticonsGridAdapter extends BaseEmoGridAdapter {

    public EmoticonsGridAdapter(Context context, KeyClickListener listener) {
        super(EmoticonUtils.getEmoticonKeys());
        this.mContext = context;
        this.mListener = listener;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = getBaseView(position, convertView, parent);
        //float dimensionTextSize = this.mContext.getResources().getDimension(R.dimen.mocha_text_size_level_2_5);
        image.setImageBitmap(EmoticonManager.getInstance(mContext).getImageOfEmoticon(paths[position] + ".png"));
        emoLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String emoText = EmoticonUtils.getEmoticonTexts()[position];
                mListener.emoClicked(paths[position], emoText, position);
            }
        });
        return v;
    }
}

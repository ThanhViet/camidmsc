package com.metfone.selfcare.adapter.home;

import android.content.Context;
import android.content.res.Resources;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.metfone.selfcare.adapter.onmedia.SuggestFriendAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/6/2017.
 */

public class HomeSocialSuggestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = SuggestFriendAdapter.class.getSimpleName();
    private ArrayList<UserInfo> listUserInfo = new ArrayList<>();
    private ApplicationController mApp;
    private AvatarBusiness mAvatarBusiness;
    private LayoutInflater mLayoutInflater;
    private int sizeAvatar;
    private SocialSuggestListener listener;
    private Resources mRes;

    public HomeSocialSuggestsAdapter(ApplicationController mApp) {
        this.mApp = mApp;
        this.mRes = mApp.getResources();
        this.mLayoutInflater = (LayoutInflater) mApp.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mAvatarBusiness = mApp.getAvatarBusiness();
        sizeAvatar = (int) mApp.getResources().getDimension(R.dimen.avatar_small_size);
    }

    public void setListener(SocialSuggestListener listener) {
        this.listener = listener;
    }

    public void setListUserInfo(ArrayList<UserInfo> listUserInfo) {
        if (this.listUserInfo == null) {
            this.listUserInfo = new ArrayList<>();
        } else {
            this.listUserInfo.clear();
        }
        this.listUserInfo.addAll(listUserInfo);
    }

    @Override
    public int getItemCount() {
        if (listUserInfo == null) return 0;
        return listUserInfo.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_suggest_friend, parent, false);
        SuggestFriendHolder viewSuggestFriendHolder = new SuggestFriendHolder(view, listener);
        return viewSuggestFriendHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserInfo userInfo = listUserInfo.get(position);
        ((SuggestFriendHolder) holder).setElement(userInfo);
    }

    public interface SocialSuggestListener {
        void onAvatarClick(UserInfo userInfo);

        void onAddFriendClick(UserInfo userInfo);
    }

    class SuggestFriendHolder extends BaseViewHolder {
        private SocialSuggestListener mListener;
        private View mViewAvatar;
        private CircleImageView mImgAvatar;
        private TextView mTvwAvatar;
        private Button mBtnAddFriend;

        public SuggestFriendHolder(View itemView, SocialSuggestListener listener) {
            super(itemView);
            this.mListener = listener;
            mViewAvatar = itemView.findViewById(R.id.layout_avatar);
            mImgAvatar = (CircleImageView) itemView.findViewById(R.id.img_onmedia_avatar);
            mTvwAvatar = (TextView) itemView.findViewById(R.id.tvw_onmedia_avatar);
            mBtnAddFriend = (Button) itemView.findViewById(R.id.btn_add_friend);
        }

        @Override
        public void setElement(Object obj) {
            final UserInfo userInfo = (UserInfo) obj;
            String mFriendAvatarUrl;
            if (TextUtils.isEmpty(userInfo.getAvatar()) || "0".equals(userInfo.getAvatar())) {
                mFriendAvatarUrl = "";
            } else {
                mFriendAvatarUrl = mAvatarBusiness.getAvatarUrl(userInfo.getAvatar(), userInfo
                        .getMsisdn(), sizeAvatar);
            }
            mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar,
                    mFriendAvatarUrl, userInfo.getMsisdn(), userInfo.getName(), sizeAvatar);

            if (userInfo.isAddFriend()) {
                mBtnAddFriend.setBackgroundResource(R.drawable.selector_rec_green);
                mBtnAddFriend.setText(mRes.getString(R.string.chat));
            } else {
                mBtnAddFriend.setBackgroundResource(R.drawable.selector_rec_purple);
                mBtnAddFriend.setText(mRes.getString(R.string.contact_follow_none));
            }

            mBtnAddFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onAddFriendClick(userInfo);
                    }
                }
            });

            mViewAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onAvatarClick(userInfo);
                    }
                }
            });
        }
    }
}

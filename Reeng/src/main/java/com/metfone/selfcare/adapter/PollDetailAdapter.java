package com.metfone.selfcare.adapter;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PollItem;
import com.metfone.selfcare.holder.AbsContentHolder;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/23/2016.
 */
public class PollDetailAdapter extends BaseAdapter {

    private static final String TAG = PollDetailAdapter.class.getSimpleName();
    private Listener mCallBack;
    private ArrayList<PollItem> listData;
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private int totalVote = 0;
    private Handler mHanlder;

    public PollDetailAdapter(ApplicationController app, ArrayList<PollItem> list,
                             Listener listener) {
        this.mApplication = app;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = list;
        this.mCallBack = listener;
        mHanlder = new Handler();

        for (PollItem item : list) {
            totalVote = totalVote + item.getTotalVoted();
        }
    }

    public void setListData(ArrayList<PollItem> list) {
        this.listData = list;
        totalVote = 0;
        for (PollItem item : list) {
            totalVote = totalVote + item.getTotalVoted();
        }
        Log.i(TAG, "totalVote: " + totalVote);
    }

    @Override
    public void notifyDataSetChanged() {
        totalVote = 0;
        for (PollItem item : listData) {
            totalVote = totalVote + item.getTotalVoted();
        }
        Log.i(TAG, "totalVote: " + totalVote);
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder(mApplication, mCallBack);
            holder.initHolder(viewGroup, convertView, position, mLayoutInflater);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.setElemnts(getItem(position));
        holder.setPosition(position);
        return holder.getConvertView();
    }

    public interface Listener {
        void onCheckBoxClick(PollItem item, View cbx);

        void onItemClick(PollItem item);
    }

    private class Holder extends AbsContentHolder {
        private Listener mCallBack;
        private PollItem mEntry;
        private int position;
        private View convertView, mViewContent, mViewNumber;
        private ImageView mImgCheckbox;
        private TextView mTvwContent, mTvwDesc, mTvwNumberSelected;
        private View mViewPercent, mViewLeftTop, mViewRightTop, mViewLeftBottom, mViewRightBottom;

        public Holder(ApplicationController app, PollDetailAdapter.Listener callBack) {
            setContext(app);
            this.mCallBack = callBack;
        }

        public void setPosition(int position) {
            this.position = position;
        }


        @Override
        public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
            convertView = layoutInflater.inflate(R.layout.holder_vote_detail, parent, false);
            mViewContent = convertView.findViewById(R.id.holder_vote_content_layout);
            mTvwContent = (TextView) convertView.findViewById(R.id.holder_vote_content);
            mImgCheckbox = (ImageView) convertView.findViewById(R.id.holder_vote_checkbox);
            mTvwDesc = (TextView) convertView.findViewById(R.id.holder_vote_desc);
            mViewNumber = convertView.findViewById(R.id.holder_vote_number_selected_layout);
            mTvwNumberSelected = (TextView) convertView.findViewById(R.id.holder_vote_number_selected);
            mViewPercent = convertView.findViewById(R.id.layout_draw_percent);
            mViewLeftTop = convertView.findViewById(R.id.view_left_top);
            mViewRightTop = convertView.findViewById(R.id.view_right_top);
            mViewLeftBottom = convertView.findViewById(R.id.view_left_bottom);
            mViewRightBottom = convertView.findViewById(R.id.view_right_bottom);
            convertView.setTag(this);
            setConvertView(convertView);
        }

        @Override
        public void setElemnts(Object obj) {
            this.mEntry = (PollItem) obj;
            mImgCheckbox.setSelected(mEntry.isSelected());
            if (TextUtils.isEmpty(mEntry.getDesc(mApplication))) {
                mTvwDesc.setVisibility(View.GONE);
            } else {
                mTvwDesc.setVisibility(View.VISIBLE);
                mTvwDesc.setText(mEntry.getDesc(mApplication));
            }
            if (mEntry.getTotalVoted() > 0) {
                mTvwNumberSelected.setText(String.valueOf(mEntry.getTotalVoted()));
                mViewNumber.setVisibility(View.VISIBLE);
            } else {
                mViewNumber.setVisibility(View.INVISIBLE);
            }
            mImgCheckbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallBack.onCheckBoxClick(mEntry, mImgCheckbox);
                }
            });
            mViewContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallBack.onCheckBoxClick(mEntry, mImgCheckbox);
                }
            });
            mViewNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallBack.onItemClick(mEntry);
                }
            });
            calculatorPercentVote(mEntry);
        }

        private void calculatorPercentVote(final PollItem item) {
            if (totalVote == 0 || item.getTotalVoted() == 0) {
                mViewPercent.setVisibility(View.GONE);
                mTvwContent.setText(mEntry.getTitle());
                return;
            }
            mHanlder.post(new Runnable() {
                @Override
                public void run() {
                    float percent = (float) item.getTotalVoted() / (float) totalVote;
                    float leftPercent = Math.round(percent * 100.0f) / 100.0f;
                    float rightPercent = 1.0f - leftPercent;

                    Log.i(TAG, "item: " + item.getTitle() + " percent: " + percent + " leftPercent: " + leftPercent + " rightPercent: " + rightPercent);

                    LinearLayout.LayoutParams paramsLeft = new LinearLayout.LayoutParams(
                            0, LinearLayout.LayoutParams.MATCH_PARENT, leftPercent);
//            paramsLeft.weight = leftPercent;
                    mViewLeftBottom.setLayoutParams(paramsLeft);
                    mViewLeftTop.setLayoutParams(paramsLeft);

                    LinearLayout.LayoutParams paramsRight = new LinearLayout.LayoutParams(
                            0, LinearLayout.LayoutParams.MATCH_PARENT, rightPercent);
//            paramsRight.weight = rightPercent;
                    mViewRightBottom.setLayoutParams(paramsRight);
                    mViewRightTop.setLayoutParams(paramsRight);
                    mViewPercent.setVisibility(View.VISIBLE);
                    int percentLeft = Math.round(leftPercent * 100);
                    String content = "(" + percentLeft + "%) " + mEntry.getTitle();
                    mTvwContent.setText(content);
                }
            });

        }
    }


}
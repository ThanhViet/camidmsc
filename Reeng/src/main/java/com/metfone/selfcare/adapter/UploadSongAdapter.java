package com.metfone.selfcare.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.LocalSongInfo;
import com.metfone.selfcare.holder.SongHolder;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by daipr on 4/17/2020
 */
public class UploadSongAdapter extends BaseAdapter<SongHolder, LocalSongInfo> {

    private PublishSubject<LocalSongInfo> publishSubject = PublishSubject.create();

    public UploadSongAdapter(Activity activity) {
        super(activity);
    }


    @NonNull
    @Override
    public SongHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext().getApplicationContext()).inflate(R.layout.item_upload_music_v5, parent, false);
        SongHolder songHolder = new SongHolder(view, parent.getContext(), null);
        view.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishSubject.onNext(items.get(songHolder.getAdapterPosition()));
            }
        });
        return songHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongHolder holder, int position) {
        holder.setElement(items.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

//    public void setItems(ArrayList<LocalSongInfo> items) {
//        localSongInfos.clear();
//        if (items != null) {
//            localSongInfos.addAll(items);
//        }
//        notifyDataSetChanged();
//    }


    @Override
    public void setItems(ArrayList<LocalSongInfo> items) {
        super.setItems(items);
        notifyDataSetChanged();
    }


    public Observable<LocalSongInfo> getItemClickObservable() {
        return publishSubject;
    }

}

package com.metfone.selfcare.adapter.avno;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.ItemInfo;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 6/6/2019.
 */

public class AVNOGiftAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private ArrayList<ItemInfo> listItem;
    private Resources mRes;
    private AVNOHelper.PackageAVNOListener listener;


    public AVNOGiftAdapter(BaseSlidingFragmentActivity activity, ArrayList<ItemInfo> listItem,
                           AVNOHelper.PackageAVNOListener listener) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplication();
        this.listItem = listItem;
        this.mRes = mApplication.getResources();
        this.listener = listener;
    }

    public void setListItem(ArrayList<ItemInfo> listItem) {
        this.listItem = listItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mApplication).inflate(R.layout.holder_avno_gift,
                parent, false);
        AVNOGiftHolder holder = new AVNOGiftHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((AVNOGiftHolder) holder).setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }

    private class AVNOGiftHolder extends BaseViewHolder {

        private TextView tvContent;
        private RoundTextView tvAction;

        public AVNOGiftHolder(View itemView) {
            super(itemView);
            tvContent = itemView.findViewById(R.id.tvContent);
            tvAction = itemView.findViewById(R.id.tvActionGift);
        }

        @Override
        public void setElement(Object obj) {
            final ItemInfo gift = (ItemInfo) obj;
            tvContent.setText(gift.getTitle());
            tvAction.setText(gift.getDeeplinkLabel());
            tvAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickGift(gift);
                }
            });
        }
    }
}

package com.metfone.selfcare.adapter;

import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.common.utils.image.ImageUtils;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.ui.photoview.PhotoView;
import com.metfone.selfcare.ui.photoview.PhotoViewAttacher;

import java.util.ArrayList;


/**
 * Created by huybq7 on 2/9/2015.
 */
public class PreviewImageProfileAdapter extends PagerAdapter {

    private ApplicationController mApp;
    private ArrayList<ImageProfile> mListImage;
    private ImageClickListener imageClickListener;
    private ImageProfileBusiness imageProfileBusiness;
    private boolean isViewMyImage;

    private ImageUtils imageUtils;
    private String domanFile;

    public PreviewImageProfileAdapter(ApplicationController context, ArrayList<ImageProfile> listImage) {
        this.mApp = context;
        this.imageUtils = context.getApplicationComponent().providesImageUtils();
        this.domanFile = UrlConfigHelper.getInstance(context).getDomainImage();
        this.mListImage = listImage;
        imageProfileBusiness = mApp.getImageProfileBusiness();
    }

    public void setViewMyImage(boolean viewMyImage) {
        isViewMyImage = viewMyImage;
    }

    public void setImageClickListener(ImageClickListener imageClickListener) {
        this.imageClickListener = imageClickListener;
    }

    public void setListImage(ArrayList<ImageProfile> mListImage) {
        this.mListImage = mListImage;
    }

    @Override
    public int getCount() {
        return mListImage.size();
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        final PhotoView photoView = new PhotoView(container.getContext());
        ImageProfile image = mListImage.get(position);
        // Now just add PhotoView to ViewPager and return it
        container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        /*if (image.getImageUrl() != null && image.getImageUrl().startsWith("http"))
            imageUtils.load(image.getImageUrl(), photoView);
        else if (image.getImageUrl() != null && image.getImageUrl().startsWith("file"))
            imageUtils.load(image.getImageUrl(), photoView);
        else imageUtils.load(domanFile + image.getImageUrl(), photoView);
//        if (isViewMyImage) {
//            imageProfileBusiness.displayImageProfile(photoView, image, false);
//        } else {
//            imageProfileBusiness.displayImageProfileFriendWithThumb(photoView, image);
        else imageUtils.load(domanFile + image.getImageUrl(), photoView);*/
        if (isViewMyImage) {
            imageProfileBusiness.displayImageProfile(photoView, image, false);
        } else
            imageProfileBusiness.displayImageProfileFriendWithThumb(photoView, image);
            /*if (TextUtils.isEmpty(image.getImageUrl())) {
                //imageProfileBusiness.displayImageProfile(photoView, image, isSaveDb);
                imageProfileBusiness.displayImageProfileFriend(photoView, image, true);
            } else {
                AvatarBusiness avatarBusiness = mApp.getAvatarBusiness();
           *//* if (!TextUtils.isEmpty(image.getAvatarSmallUrl())) {
                avatarBusiness.displayImageLarger(image.getAvatarSmallUrl(), photoView);
            }*//*
                avatarBusiness.displayImageLarger(image.getImageThumb(), photoView);
                avatarBusiness.setPopupFullAvatar(photoView, image.getImageUrl(), null, new
                SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (loadedImage != null) {
                            photoView.setImageBitmap(loadedImage);
                        }
                        super.onLoadingComplete(imageUri, view, loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        photoView.setVisibility(View.INVISIBLE);
                        super.onLoadingCancelled(imageUri, view);
                    }
                });
            }*/
//        }
        photoView.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
            @Override
            public void onPhotoTap(View view, float v, float v1) {
                if (imageClickListener != null) {
                    imageClickListener.onClickImage();
                }
            }

            @Override
            public void onOutsidePhotoTap() {
                if (imageClickListener != null) {
                    imageClickListener.onClickImage();
                }
            }
        });

        return photoView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public interface ImageClickListener {
        void onClickImage();
    }

}
package com.metfone.selfcare.adapter;


import android.content.Context;
import android.graphics.Typeface;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LayoutHelper;
import com.metfone.selfcare.holder.StrangerAroundHolder;
import com.metfone.selfcare.holder.StrangerMusicHolder;
import com.metfone.selfcare.listeners.StrangerMusicInteractionListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 11/6/2015.
 */
public class StrangerMusicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private StrangerMusicInteractionListener mListener;
    private LayoutInflater inflater;
    private ArrayList<StrangerMusic> mStrangerMusics;
    private int holderHeight, contentMargin, headphoneWidth, starHeight, fraHeight, fraWidth, starMargin;
    private int sizeAvatar, sizeViewGroupAvatar;
    private Typeface typefaceRegular;
    private Typeface typefaceMedium;

    public StrangerMusicAdapter(Context context, ArrayList<StrangerMusic> strangerMusics,
                                StrangerMusicInteractionListener listener) {
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mStrangerMusics = strangerMusics;
        this.mListener = listener;
//        this.sizeAvatar = (int) mContext.getResources().getDimension(R.dimen.avatar_small_size);
//        initLayoutParams();
        typefaceRegular = ResourcesCompat.getFont(context, R.font.sf_ui_text_regular);
        typefaceMedium = ResourcesCompat.getFont(context, R.font.sf_ui_text_medium);
        caculateWidthAvartarStranger();
    }

    public void setStrangerStrangers(ArrayList<StrangerMusic> strangerMusics) {
        this.mStrangerMusics = strangerMusics;
    }

    public Object getItem(int position) {
        return mStrangerMusics.get(position);
    }

    @Override
    public int getItemCount() {
        if (mStrangerMusics == null) return 0;
        return mStrangerMusics.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        StrangerMusic obj = (StrangerMusic) getItem(position);
        if(obj.getTypeObj() == Constants.STRANGER_MUSIC.TYPE_AROUND){
            return Constants.STRANGER_MUSIC.TYPE_AROUND;
        }else {
            return Constants.STRANGER_MUSIC.TYPE_MUSIC;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        Log.e("Debug", "viewtype : " + type);
        if (type == Constants.STRANGER_MUSIC.TYPE_AROUND) {
            View convertView = inflater.inflate(R.layout.item_stranger_around_v5, parent, false);
            return new StrangerAroundHolder(convertView, mContext, mListener);
        } else {
            View convertView = inflater.inflate(R.layout.item_stranger_music_v5, parent, false);
            /*StrangerMusicHolder strangerHolder = new StrangerMusicHolder(convertView, mContext, mListener);
            strangerHolder.setLayoutParams(holderHeight, contentMargin, headphoneWidth,
                    starHeight, fraHeight, fraWidth, starMargin, sizeAvatar);
            return strangerHolder;*/
            return new StrangerMusicHolder(convertView, mContext, mListener, typefaceRegular, typefaceMedium, sizeViewGroupAvatar);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        if (type == Constants.STRANGER_MUSIC.TYPE_AROUND) {
            ((StrangerAroundHolder) holder).setElement(getItem(position));
        } else {
            ((StrangerMusicHolder) holder).setElement(getItem(position));
//            ((StrangerMusicHolder) holder).setLayoutParams(holderHeight, contentMargin,
//                    headphoneWidth, starHeight, fraHeight, fraWidth, starMargin, sizeAvatar);
        }
    }


    public void initLayoutParams() {
        holderHeight = LayoutHelper.getHeightLayoutStrangerMusic(mContext.getResources());
        contentMargin = holderHeight / 8;
        headphoneWidth = holderHeight + (contentMargin * 16) / 10;
        starHeight = holderHeight / 5;
        fraHeight = holderHeight + contentMargin / 2;
        fraWidth = holderHeight * 2;
        starMargin = (contentMargin * 3) / 2;
    }

    private void caculateWidthAvartarStranger(){
        int widthScreen = mContext.getResources().getDisplayMetrics().widthPixels;
        int space = (int) mContext.getResources().getDimension(R.dimen.v5_spacing_normal);
        int holderPadding = (int) mContext.getResources().getDimension(R.dimen.v5_margin_normal);
        int widthHolder = widthScreen - space *3;
        int widthViewGroupAvatarMax = widthHolder - holderPadding *2;
        int avatarSizeOrigin = (int) mContext.getResources().getDimension(R.dimen.v5_avatar_stranger_music);
        int spaceOverlappingTwoAvatar = space;
        if((avatarSizeOrigin*2 - spaceOverlappingTwoAvatar) > widthViewGroupAvatarMax){
            sizeViewGroupAvatar = widthViewGroupAvatarMax;
        }else {
            sizeViewGroupAvatar = avatarSizeOrigin*2 - spaceOverlappingTwoAvatar; // vùng chống lấn sẽ thay đổi..// không biết có to quá không
        }
    }
}
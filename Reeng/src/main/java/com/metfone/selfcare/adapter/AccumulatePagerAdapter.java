package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.holder.game.AccumulatePointItemHolder;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 11/30/2016.
 */
public class AccumulatePagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater mLayoutInflater;
    private ItemListener mListener;
    private ApplicationController mApplication;
    private ArrayList<AccumulatePointItem> items;

    public AccumulatePagerAdapter(ApplicationController application, ArrayList<AccumulatePointItem> list) {
        this.mApplication = application;
        this.items = list;
        this.mLayoutInflater = (LayoutInflater) mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListItem(ArrayList<AccumulatePointItem> items) {
        this.items = items;
    }

    public void setListener(ItemListener listener) {
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        if (items == null) return 0;
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public AccumulatePointItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AccumulatePointItemHolder holder;
        View view = mLayoutInflater.inflate(R.layout.holder_accumulate_item, parent, false);
        holder = new AccumulatePointItemHolder(view, mApplication, mListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AccumulatePointItemHolder videoHolder = (AccumulatePointItemHolder) holder;
        videoHolder.setElement(getItem(position));
    }

    public interface ItemListener {
        void onDeepLinkClick(AccumulatePointItem item);

        void onExchangeClick(AccumulatePointItem item);

        void onInstallAppClick(AccumulatePointItem item);
    }
}
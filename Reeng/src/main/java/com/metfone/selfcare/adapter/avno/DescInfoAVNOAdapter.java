package com.metfone.selfcare.adapter.avno;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 4/12/2018.
 */

public class DescInfoAVNOAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = DescInfoAVNOAdapter.class.getSimpleName();

    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> listItem;


    public DescInfoAVNOAdapter(ApplicationController application, ArrayList<String> listItem) {
        this.mApplication = application;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listItem = listItem;
    }

    public void setListItem(ArrayList<String> listItem) {
        this.listItem = listItem;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        DescInfoAVNOHolder holder;
        View view = mLayoutInflater.inflate(R.layout.holder_avno_desc_info, parent, false);
        holder = new DescInfoAVNOHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DescInfoAVNOHolder avno = (DescInfoAVNOHolder) holder;
        avno.setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }

    public ArrayList<String> getListItem() {
        return listItem;
    }

    class DescInfoAVNOHolder extends BaseViewHolder {

        private TextView mTvwDesc;

        public DescInfoAVNOHolder(View itemView) {
            super(itemView);
            mTvwDesc = itemView.findViewById(R.id.tvw_desc_info_avno);
        }

        @Override
        public void setElement(Object obj) {
            String desc = (String) obj;
            mTvwDesc.setText(desc);
        }
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.model.call.CallHistoryDetail;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by toanvk2 on 10/14/2016.
 */

public class CallHistoryDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;
    private ArrayList<CallHistoryDetail> listItem;

    public CallHistoryDetailAdapter(ApplicationController application, ArrayList<CallHistoryDetail> listItem) {
        this.mApplication = application;
        this.mLayoutInflater = (LayoutInflater) mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listItem = listItem;
    }

    public void setListData(ArrayList<CallHistoryDetail> list) {
        this.listItem = list;
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public Object getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        View view = mLayoutInflater.inflate(R.layout.holder_call_history_detail, parent, false);
        return new CallHistoryDetailHolder(view, mApplication);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((CallHistoryDetailHolder) holder).setElement(getItem(position));
    }

    private class CallHistoryDetailHolder extends BaseViewHolder {
        private Context mContext;
        private TextView mTvwHours, mTvwStatus, mTvwDuration;
        private ImageView ivStatus;

        CallHistoryDetailHolder(View convertView, Context context) {
            super(convertView);
            this.mContext = context;
            mTvwHours = convertView.findViewById(R.id.holder_call_history_detail_hours);
            mTvwStatus = convertView.findViewById(R.id.holder_call_history_detail_status);
            mTvwDuration = convertView.findViewById(R.id.holder_call_history_detail_duration);
            ivStatus = convertView.findViewById(R.id.holder_call_history_status);
        }

        @Override
        public void setElement(Object obj) {
            CallHistoryDetail mEntry = (CallHistoryDetail) obj;
            Resources res = mContext.getResources();
            mTvwHours.setText(TimeHelper.formatSeparatorTimeOfHistoryDetail(mEntry.getCallTime(), res));
            mTvwStatus.setText(Utilities.getTextCallInfor(res, mEntry.getCallType()));
            ivStatus.setImageResource(Utilities.getIconCallInfor(mEntry.getCallDirection(), mEntry.getCallState()));
//            ivStatus.setColorFilter(ContextCompat.getColor(mContext, R.color.text_last_message));
            if (mEntry.getCallState() == CallHistoryConstant.STATE_MISS) {
                ivStatus.setImageResource(R.drawable.ic_call_in);
                mTvwHours.setTextColor(ContextCompat.getColor(mContext,R.color.red));
            }
            if (mEntry.getCallState() == CallHistoryConstant.STATE_BUSY ||
                    mEntry.getCallState() == CallHistoryConstant.STATE_CANCELLED ||
                    mEntry.getCallState() == CallHistoryConstant.STATE_MISS) {
                mTvwDuration.setVisibility(View.INVISIBLE);
            } else {
                mTvwDuration.setVisibility(View.VISIBLE);
                mTvwDuration.setText(Utilities.secondsToTimerText(mEntry.getDuration()));
            }
        }
    }
}
package com.metfone.selfcare.adapter.feedback;

import androidx.appcompat.widget.AppCompatImageView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.hoanganhtuan95ptit.upload.view.CircleProgress;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

public class ImageSelectedFeedbackViewHolder extends BaseViewHolder {
    private RoundedImageView imageView;
    private AppCompatImageView icRemove;
    private CircleProgress circleProgress;

    public ImageSelectedFeedbackViewHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.img);
        icRemove = itemView.findViewById(R.id.icRemove);
        circleProgress = itemView.findViewById(R.id.circleProgress);
    }

    @Override
    public void setElement(Object obj) {
        Glide.with(getItemView())
                .load((String) obj)
                .into(imageView);
    }

    public void uploadProgress(int progress) {
        if(progress == 0){
            circleProgress.setProgress(progress);
        }else {
            circleProgress.setSmoothProgress(progress);
        }
        if (progress >= 100 || progress < 0) {
            circleProgress.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(circleProgress != null) {
                        circleProgress.setVisibility(View.GONE);
                        circleProgress.resetProgress();//reset
                    }
                }
            }, 500);
        } else {
            if (circleProgress.getVisibility() == View.GONE) {
                circleProgress.setVisibility(View.VISIBLE);
            }
        }
    }
}

package com.metfone.selfcare.adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import androidx.viewpager.widget.PagerAdapter;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.StickerActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.viewpagerindicator.IconIndicator;
import com.metfone.selfcare.ui.viewpagerindicator.IconPagerAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

public class EmoticonsPagerAdapter extends PagerAdapter implements IconPagerAdapter {
    private static final String TAG = EmoticonsPagerAdapter.class.getSimpleName();
    private static final int PAGE_SIZE_DEFAULT = 3;
    private static final int RECENT_STICKER_TAB_POSITION = 0;
    private static final int DEFAULT_STICKER_TAB_POSITION = 1;
    private static final int EMOJI_TAB_POSITION = 2;
    private Activity mActivity;
    private EmoticonsGridAdapter.KeyClickListener mListener;
    private ArrayList<IconIndicator> icons;
    boolean isThreadChat;
    private List<StickerCollection> stickerCollections;
    private ApplicationController mApplicationController;
    private StickerBusiness stickerBusiness;
    private Resources mRes;
    private RecentStickerGridAdapter recentStickerAdapter;
    private View stickerRecentLayout;
    private EditText mEdtContent;
    private int recentStickerTabPosition, defaultStickerTabPosition, emojiTabPosition, pageSizeDefault;
    private Button mBtnDownload;

    public EmoticonsPagerAdapter(Activity activity, EmoticonsGridAdapter.KeyClickListener listener,
                                 ArrayList<IconIndicator> icons, boolean isThreadChat,
                                 List<StickerCollection> stickerCollections, EditText pEdtContent) {
        this.mActivity = activity;
        this.mListener = listener;
        this.icons = icons;
        this.isThreadChat = isThreadChat;
        this.stickerCollections = stickerCollections;
        this.mRes = mActivity.getResources();
        this.mEdtContent = pEdtContent;
        mApplicationController = (ApplicationController) mActivity.getApplicationContext();
        stickerBusiness = mApplicationController.getStickerBusiness();
        setTabPositions();
    }

    public void setStickerCollections(List<StickerCollection> stickerCollections) {
        this.stickerCollections = stickerCollections;
    }

    public void setIcons(ArrayList<IconIndicator> icons) {
        this.icons = icons;
    }

    private void setTabPositions() {
        recentStickerTabPosition = RECENT_STICKER_TAB_POSITION;
        defaultStickerTabPosition = DEFAULT_STICKER_TAB_POSITION;
        emojiTabPosition = EMOJI_TAB_POSITION;
        pageSizeDefault = PAGE_SIZE_DEFAULT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        if (isThreadChat) {
            return icons.size();
        } else {
            return 1;
        }
    }

    @Override
    public IconIndicator getIconIndicator(int index) {
        if (isThreadChat) {
            return icons.get(index);
        } else {
            return null;
        }
    }

    private View.OnClickListener onDeleteClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
                    0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
            mEdtContent.dispatchKeyEvent(event);
        }
    };

    private View.OnLongClickListener onDeleteLongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
                    0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
            mEdtContent.dispatchKeyEvent(event);
            return true;
        }
    };

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        View stickerLayout = mActivity.getLayoutInflater().inflate(
                R.layout.emoticons_grid, null);
        LinearLayout layout_emoticons_grid = (LinearLayout) stickerLayout.findViewById(R.id.layout_emoticon_grid);
        ImageButton btnDeleteEmo = (ImageButton) stickerLayout.findViewById(R.id.btn_delete_emoticon);
        GridView gridView = (GridView) stickerLayout.findViewById(R.id.emoticons_grid);
        btnDeleteEmo.setOnClickListener(onDeleteClick);
        btnDeleteEmo.setLongClickable(true);
        btnDeleteEmo.setOnLongClickListener(onDeleteLongClick);
        View stickerDownloadView = stickerLayout.findViewById(R.id.more_sticker_download);
        mBtnDownload = (Button) stickerDownloadView.findViewById(R.id.more_sticker_download_button);

        TextView tvwEmpty = (TextView) stickerLayout.findViewById(R.id.text_note_empty);
        BaseEmoGridAdapter adapter;
        if (!isThreadChat) {
            adapter = new EmoticonsGridAdapter(mActivity.getApplicationContext(), mListener);
            btnDeleteEmo.setVisibility(View.VISIBLE);
            gridView.setAdapter(adapter);
            layout_emoticons_grid.setVisibility(View.VISIBLE);
            stickerDownloadView.setVisibility(View.GONE);
            tvwEmpty.setVisibility(View.GONE);
        } else {
            if (position == recentStickerTabPosition) {
                stickerRecentLayout = stickerLayout;
//                gridView.setVisibility(View.VISIBLE);
                stickerDownloadView.setVisibility(View.GONE);
                gridView.setNumColumns(4);
                if (recentStickerAdapter == null) {
                    recentStickerAdapter = new RecentStickerGridAdapter(mActivity.getApplicationContext(),
                            mListener, mApplicationController.getStickerBusiness().getListRecentSticker());
                }
                if (mApplicationController.getStickerBusiness().getListRecentSticker().isEmpty()) {
                    layout_emoticons_grid.setVisibility(View.GONE);
                    tvwEmpty.setVisibility(View.VISIBLE);
                } else {
                    tvwEmpty.setVisibility(View.GONE);
                    layout_emoticons_grid.setVisibility(View.VISIBLE);
                }
                adapter = recentStickerAdapter;
                gridView.setAdapter(adapter);
            } else if (position == emojiTabPosition) {
                layout_emoticons_grid.setVisibility(View.VISIBLE);
                btnDeleteEmo.setVisibility(View.VISIBLE);
                //  gridView.setVisibility(View.VISIBLE);
                stickerDownloadView.setVisibility(View.GONE);
                tvwEmpty.setVisibility(View.GONE);
                gridView.setNumColumns(5);
                adapter = new EmoticonsGridAdapter(
                        mActivity.getApplicationContext(),
                        mListener);
                gridView.setAdapter(adapter);
            } else {
                btnDeleteEmo.setVisibility(View.GONE);
                tvwEmpty.setVisibility(View.GONE);
                gridView.setNumColumns(4);
                //sticker moi
                StickerCollection stickerCollection;
                ArrayList<StickerItem> stickerItems;
                if (position == defaultStickerTabPosition) {
                    stickerCollection = stickerBusiness.getDefaultCollection();
                    if (stickerCollection == null || !stickerCollection.isDownloaded()) {
                        //cai ban cu nang cap len ban moi --> chua get config --> fake default collection
                        stickerCollection = new StickerCollection();
                        stickerCollection.setServerId(EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID);
                        stickerCollection.setIsDefault(1);
                        stickerCollection.setDownloaded(true);
                    }
                } else {
                    stickerCollection = stickerCollections.get(position - pageSizeDefault);
                }
                if (stickerCollection.isDownloaded()) {
                    //neu da download sticker thi hien thi trong gridView
                    layout_emoticons_grid.setVisibility(View.VISIBLE);
                    stickerDownloadView.setVisibility(View.GONE);
                    adapter = new MoreStickerGridAdapter(mApplicationController, mListener);
                    int collectionId = stickerCollection.getServerId();
                    stickerItems = stickerBusiness.getListStickerItemByCollectionId(collectionId);
                    ((MoreStickerGridAdapter) adapter).setListStickerItem(stickerItems);
                    gridView.setAdapter(adapter);
                } else {
                    //chua download sticker
                    layout_emoticons_grid.setVisibility(View.GONE);
                    stickerDownloadView.setVisibility(View.VISIBLE);
                    mBtnDownload.setVisibility(View.VISIBLE);
                    attachAndInitData(stickerCollection, stickerDownloadView);
                    if (stickerCollection.isDownloading()) {
                        //neu dang download thi hien thi downloading
                        mBtnDownload.setText(R.string.downloading);
                        mBtnDownload.setEnabled(false);

                    } else {
                        mBtnDownload.setText(R.string.free_download);
                        mBtnDownload.setEnabled(true);
                    }
                }
            }
        }
        collection.addView(stickerLayout);
        return stickerLayout;
    }

    private void attachAndInitData(final StickerCollection stickerCollection, View stickerDownloadView) {
        TextView txtCollectionName = (TextView) stickerDownloadView.findViewById(R.id.more_sticker_name);
        TextView txtCollectionAmount = (TextView) stickerDownloadView.findViewById(R.id.more_sticker_amount);
        ImageView imgCollectionAvatar = (ImageView) stickerDownloadView.findViewById(R.id.more_sticker_avatar);
        Button btnCollectionDownload = (Button) stickerDownloadView.findViewById(R.id.more_sticker_download_button);        //
        txtCollectionName.setText(stickerCollection.getCollectionName());

        //Store sticker
        if (stickerCollection.isStickerStore()) {
            txtCollectionAmount.setVisibility(View.VISIBLE);
            txtCollectionAmount.setText(String.format(mRes.getString(R.string.number_sticker_collection), stickerCollection.getNumberSticker()));
            imgCollectionAvatar.setImageResource(R.drawable.ic_sticker_default);
            btnCollectionDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mActivity.getApplicationContext(), StickerActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mActivity.startActivityForResult(intent, Constants.ACTION.SET_DOWNLOAD_STICKER);
                }
            });
        } else {
            txtCollectionAmount.setVisibility(View.VISIBLE);
            String amountString;
            if (stickerCollection.getNumberSticker() > 1) {
                amountString = stickerCollection.getNumberSticker() + " " + mRes.getString(R.string.stickers_text);
            } else {
                amountString = stickerCollection.getNumberSticker() + " " + mRes.getString(R.string.stickers_text_single);
            }
            txtCollectionAmount.setText(amountString);
            //txtCollectionAmount.setText(stickerCollection.getNumberSticker() + " " + mActivity.getString(R.string.stickers_text));
            // domain file
            String fullAvatarLink = UrlConfigHelper.getInstance(mActivity).getDomainFile()
                    + stickerCollection.getCollectionIconPath();
            Log.i(TAG, "fullAvatarLink " + fullAvatarLink);
            ImageLoaderManager.getInstance(mActivity).
                    displayThumbnailOfImagePath(fullAvatarLink, imgCollectionAvatar);
            btnCollectionDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity.getApplicationContext(), StickerActivity.class);
                    intent.putExtra(StickerActivity.PARAM_STICKER_COLLECTION_ID, stickerCollection.getServerId());
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mActivity.startActivityForResult(intent, Constants.ACTION.SET_DOWNLOAD_STICKER);
                }
            });
        }
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void notifyChangeRecentAdapter(ArrayList<StickerItem> listItems) {
        if (listItems != null && recentStickerAdapter != null) {
            recentStickerAdapter.setListStickerItem(listItems);
            recentStickerAdapter.notifyDataSetChanged();
            if (listItems.size() < 3 && stickerRecentLayout != null) {
                LinearLayout layout_emoticons_grid = (LinearLayout) stickerRecentLayout.findViewById(R.id.layout_emoticon_grid);
                TextView tvwEmpty = (TextView) stickerRecentLayout.findViewById(R.id.text_note_empty);
                if (listItems.isEmpty()) {
                    layout_emoticons_grid.setVisibility(View.GONE);
                    tvwEmpty.setVisibility(View.VISIBLE);
                } else {
                    tvwEmpty.setVisibility(View.GONE);
                    layout_emoticons_grid.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
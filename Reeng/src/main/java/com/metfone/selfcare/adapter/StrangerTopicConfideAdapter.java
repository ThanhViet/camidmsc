package com.metfone.selfcare.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.TopicConfide;
import com.metfone.selfcare.holder.BaseViewHolder;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/21/2017.
 */

public class StrangerTopicConfideAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = StrangerTopicConfideAdapter.class.getSimpleName();
    private ArrayList<TopicConfide> listTopic = new ArrayList<>();
    private BaseSlidingFragmentActivity mActivity;
    private LayoutInflater inflater;
    private CallBack mCallBack;

    public StrangerTopicConfideAdapter(BaseSlidingFragmentActivity activity, ArrayList<TopicConfide> list, CallBack listener) {
        this.mActivity = activity;
        this.inflater = LayoutInflater.from(mActivity);
        this.listTopic = list;
        this.mCallBack = listener;
    }

    public void setListTopic(ArrayList<TopicConfide> list) {
        this.listTopic = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TopicViewHolder holder;
        View view = inflater.inflate(R.layout.holder_stranger_topic, parent, false);
        holder = new TopicViewHolder(mActivity, view, mCallBack);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TopicViewHolder topicViewHolder = (TopicViewHolder) holder;
        topicViewHolder.setElement(listTopic.get(position));
    }

    @Override
    public int getItemCount() {
        if (listTopic == null) return 0;
        return listTopic.size();
    }

    private class TopicViewHolder extends BaseViewHolder {
        private TopicConfide mEntry;
        private Context mContext;
        private TextView mTvwMessage;
        private CallBack mCallBack;
        private View rootView;

        public TopicViewHolder(Context context, View itemView, CallBack listener) {
            super(itemView);
            rootView = itemView;
            this.mContext = context;
            this.mCallBack = listener;
            mTvwMessage = (TextView) itemView.findViewById(R.id.holder_stranger_topic_message);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (TopicConfide) obj;
            mTvwMessage.setText(mEntry.getMessage());
            getItemView().setSelected(mEntry.isSelected());
            getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallBack.onClick(getAdapterPosition());
                    rootView.setSelected(true);
                    mTvwMessage.setSelected(true);
                }
            });
        }

    }

    public interface CallBack {
        void onClick(int  position);
    }
}
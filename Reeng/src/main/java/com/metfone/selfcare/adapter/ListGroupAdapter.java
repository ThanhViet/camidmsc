package com.metfone.selfcare.adapter;

import android.content.res.Resources;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.ThreadGroup;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 3/24/2018.
 */

public class ListGroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ApplicationController mApplication;
    private AvatarBusiness avatarBusiness;
    private ArrayList<ThreadMessage> listData;
    private LayoutInflater inflater;
    private Resources mRes;
    private RecyclerClickListener mRecyclerClickListener;

    public ListGroupAdapter(ApplicationController context) {
        this.mApplication = context;
        this.listData = new ArrayList<>();
        inflater = LayoutInflater.from(mApplication);
        mRes = mApplication.getResources();
        avatarBusiness = mApplication.getAvatarBusiness();
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setListData(ArrayList<ThreadGroup> threads) {
        if (listData == null) {
            listData = new ArrayList<>();
        } else {
            listData.clear();
        }
        listData.addAll(threads);
    }

    public void addListData(ArrayList<ThreadMessage> threads) {
        listData.addAll(threads);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View threadMessageView = inflater.inflate(R.layout.holder_contact_viewer, parent, false);
        ListGroupViewHolder viewHolder = new ListGroupViewHolder(threadMessageView);
        viewHolder.setRecyclerClickListener(mRecyclerClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((BaseViewHolder) holder).setViewClick(position, listData.get(position));
        ((BaseViewHolder) holder).setElement(listData.get(position));
    }

    @Override
    public int getItemCount() {
        if (listData == null) return 0;
        return listData.size();
    }


    class ListGroupViewHolder extends BaseViewHolder {

        private ThreadMessage mThreadMessage;
        private CircleImageView mImgThreadAvatar;
        private TextView mTvwThreadName;


        public ListGroupViewHolder(View rowView) {
            super(rowView);
            mImgThreadAvatar = rowView.findViewById(R.id.item_contact_view_avatar_circle);
            mTvwThreadName = rowView.findViewById(R.id.item_contact_view_name_text);

            ImageView mImgMocha = rowView.findViewById(R.id.item_contact_view_avatar_state);
            mImgMocha.setVisibility(View.GONE);
            View viewStatus = rowView.findViewById(R.id.item_contact_view_status_text);
            viewStatus.setVisibility(View.GONE);


            /*TextView mTvwAvatar = rowView.findViewById(R.id.contact_avatar_text);
            ImageView mImgStatus = rowView.findViewById(R.id.thread_last_status);
            TextView mTvwThreadType = rowView.findViewById(R.id.thread_holder_type);
            TextView mTvwUnreadMsg = rowView.findViewById(R.id.thread_number_unread);
            ImageView mImgSong = rowView.findViewById(R.id.thread_song_ic);
            EllipsisTextView mTvwLastContent = rowView.findViewById(R.id.thread_last_content);
            TextView mTvwLastTime = rowView.findViewById(R.id.thread_last_time);
            ImageView mImgWatchVideo = rowView.findViewById(R.id.thread_watch_video);
            mImgStatus.setVisibility(View.GONE);
            mTvwThreadType.setVisibility(View.GONE);
            mTvwUnreadMsg.setVisibility(View.GONE);
            mImgSong.setVisibility(View.GONE);
            mTvwLastContent.setVisibility(View.GONE);
            mTvwLastTime.setVisibility(View.GONE);
            mImgWatchVideo.setVisibility(View.GONE);
            mTvwAvatar.setVisibility(View.GONE);        // Text*/
        }

        @Override
        public void setElement(Object obj) {
            mThreadMessage = (ThreadMessage) obj;
            if (TextUtils.isEmpty(mThreadMessage.getGroupAvatar())) {
                mImgThreadAvatar.setImageResource(R.drawable.ic_avatar_default);
            } else {
                avatarBusiness.setGroupAvatarUrl(mImgThreadAvatar, mThreadMessage, mThreadMessage.getGroupAvatar());
            }
            mTvwThreadName.setText(mThreadMessage.getThreadName());
        }
    }
}

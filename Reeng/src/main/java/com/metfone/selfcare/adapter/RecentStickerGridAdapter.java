package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thaodv on 05-Jan-15.
 */
public class RecentStickerGridAdapter extends BaseEmoGridAdapter {
    private static final String TAG = RecentStickerGridAdapter.class.getSimpleName();
    private ArrayList<StickerItem> stickerItemList;

    public RecentStickerGridAdapter(Context context,
                                    KeyClickListener listener, ArrayList<StickerItem> listItem) {
        this.mContext = context;
        this.mListener = listener;
        this.stickerItemList = listItem;
        setCount(stickerItemList.size());
        Log.i(TAG, "setListStickerItem " + count);
    }


    public void setListStickerItem(ArrayList<StickerItem> listItem) {
        this.stickerItemList = listItem;
        setCount(stickerItemList.size());
        Log.i(TAG, "setListStickerItem " + count);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = getBaseView(position, convertView, parent);
        final StickerItem stickerItem = stickerItemList.get(position);
        image.getLayoutParams().height = mContext.getResources().getDimensionPixelSize(R.dimen.sticker_grid_height);
        image.getLayoutParams().width = mContext.getResources().getDimensionPixelSize(R.dimen.sticker_grid_height);
        int paddingImage = mContext.getResources().getDimensionPixelSize(R.dimen.margin_more_content_5);
        image.setPadding(paddingImage, paddingImage, paddingImage, paddingImage);
        // show detail sticker by sticker item
        ImageLoaderManager.getInstance(mContext).displayStickerImage(image,
                stickerItem.getCollectionId(), stickerItem.getItemId());
        emoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.stickerClicked(stickerItem);
            }
        });
        return v;
    }
}

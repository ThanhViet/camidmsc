package com.metfone.selfcare.adapter.onmedia;

import android.content.Context;
import android.content.res.Resources;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/5/2017.
 */
public class SuggestFriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = SuggestFriendAdapter.class.getSimpleName();
    private ArrayList<UserInfo> listUserInfo = new ArrayList<>();
    private ApplicationController mApp;
    private AvatarBusiness mAvatarBusiness;
    private LayoutInflater mLayoutInflater;
    private int sizeAvatar;
    private OnMediaHolderListener listener;
    private Resources mRes;

    public SuggestFriendAdapter(ApplicationController mApp) {
        this.mApp = mApp;
        this.mRes = mApp.getResources();
        this.mLayoutInflater = (LayoutInflater) mApp
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mAvatarBusiness = mApp.getAvatarBusiness();
        sizeAvatar = (int) mApp.getResources().getDimension(R.dimen.avatar_small_size);
    }

    public void setListener(OnMediaHolderListener listener) {
        this.listener = listener;
    }

    public void setListUserInfo(ArrayList<UserInfo> listUserInfo) {
        this.listUserInfo = listUserInfo;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_suggest_friend, parent, false);
        ViewSuggestFriendHolder viewSuggestFriendHolder = new ViewSuggestFriendHolder(view);
        return viewSuggestFriendHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserInfo userInfo = listUserInfo.get(position);
        ((ViewSuggestFriendHolder) holder).setElement(userInfo);
    }

    @Override
    public int getItemCount() {
        return listUserInfo.size();
    }

    class ViewSuggestFriendHolder extends BaseViewHolder {

        private View mViewAvatar;
        private ImageView mImgAvatar;
        private TextView mTvwAvatar;
        private RoundTextView mBtnAddFriend;
        private AppCompatTextView tvFriendName;

        public ViewSuggestFriendHolder(View itemView) {
            super(itemView);
            mViewAvatar = itemView.findViewById(R.id.layout_avatar);
            mImgAvatar = itemView.findViewById(R.id.img_onmedia_avatar);
            mTvwAvatar = (TextView) itemView.findViewById(R.id.tvw_onmedia_avatar);
            mBtnAddFriend =  itemView.findViewById(R.id.btn_add_friend);
            tvFriendName = itemView.findViewById(R.id.tvNameFriend);
        }

        @Override
        public void setElement(Object obj) {
            final UserInfo userInfo = (UserInfo) obj;
            String mFriendAvatarUrl;
            if (TextUtils.isEmpty(userInfo.getAvatar()) || "0".equals(userInfo.getAvatar())) {
                mFriendAvatarUrl = "";
            } else {
                mFriendAvatarUrl = mAvatarBusiness.getAvatarUrl(userInfo.getAvatar(), userInfo
                        .getMsisdn(), sizeAvatar);
            }
            mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar,
                    mFriendAvatarUrl, userInfo.getMsisdn(), userInfo.getName(), sizeAvatar);
            tvFriendName.setText(userInfo.getName());

            if (userInfo.isAddFriend()) {
                mBtnAddFriend.setBackgroundResource(R.drawable.selector_rec_green);
                mBtnAddFriend.setText(mRes.getString(R.string.chat));
            } else {
                mBtnAddFriend.setBackgroundResource(R.drawable.selector_rec_purple);
                mBtnAddFriend.setText(mRes.getString(R.string.contact_follow_none));
            }

            mBtnAddFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickSuggestFriend(userInfo);
                    }
                    if (!userInfo.isAddFriend()) {
                        userInfo.setAddFriend(true);
                        mBtnAddFriend.setBackgroundResource(R.drawable.selector_rec_green);
                        mBtnAddFriend.setText(mRes.getString(R.string.chat));
                    }
                }
            });

            mViewAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickUser(userInfo);
                    }
                }
            });
        }
    }
}

package com.metfone.selfcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.AbsContentHolder;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/23/2016.
 */
public class PollCreateAdapter extends BaseAdapter {
    private Listener mCallBack;
    private ArrayList<String> listData;
    private ApplicationController mApplication;
    private LayoutInflater mLayoutInflater;

    public PollCreateAdapter(ApplicationController app, ArrayList<String> list,
                             Listener listener) {
        this.mApplication = app;
        this.mLayoutInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = list;
        this.mCallBack = listener;
    }

    public void setListData(ArrayList<String> list) {
        this.listData = list;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        PollHolder holder;
        if (convertView == null) {
            holder = new PollHolder(mApplication, mCallBack);
            holder.initHolder(viewGroup, convertView, position, mLayoutInflater);
        } else {
            holder = (PollHolder) convertView.getTag();
        }
        holder.setElemnts(getItem(position));
        holder.setPosition(position);
        return holder.getConvertView();
    }

    public interface Listener {
        void onClearClick(int position, String item);
    }

    private class PollHolder extends AbsContentHolder {
        private Listener mCallBack;
        private String mEntry;
        private int position;
        private ImageView mImgClear;
        private View convertView;
        private TextView mTvwContent;

        public PollHolder(ApplicationController app, PollCreateAdapter.Listener callBack) {
            setContext(app);
            this.mCallBack = callBack;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
            convertView = layoutInflater.inflate(R.layout.holder_vote_create, parent, false);
            mTvwContent = (TextView) convertView.findViewById(R.id.holder_vote_content);
            mImgClear = (ImageView) convertView.findViewById(R.id.holder_vote_clear);
            convertView.setTag(this);
            setConvertView(convertView);
        }

        @Override
        public void setElemnts(Object obj) {
            this.mEntry = (String) obj;
            mTvwContent.setText(mEntry);
            mImgClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallBack.onClearClick(position, mEntry);
                }
            });
        }
    }
}
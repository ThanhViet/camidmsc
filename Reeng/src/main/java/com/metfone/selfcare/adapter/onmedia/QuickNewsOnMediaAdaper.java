package com.metfone.selfcare.adapter.onmedia;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.onmedia.QuickNewsOnMedia;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 6/12/2017.
 */
public class QuickNewsOnMediaAdaper extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = QuickNewsOnMediaAdaper.class.getSimpleName();
    private ArrayList<QuickNewsOnMedia> listQuickNews = new ArrayList<>();
    private ApplicationController mApp;
    private BaseSlidingFragmentActivity mActivity;
    private AvatarBusiness mAvatarBusiness;
    private LayoutInflater mLayoutInflater;
    private int sizeThumb;

    public QuickNewsOnMediaAdaper(BaseSlidingFragmentActivity mActivity) {
        this.mApp = (ApplicationController) mActivity.getApplication();
        this.mActivity = mActivity;
        this.mLayoutInflater = (LayoutInflater) mApp
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mAvatarBusiness = mApp.getAvatarBusiness();
        sizeThumb = mApp.getResources().getDimensionPixelSize(R.dimen.height_item_share_like);
    }

    public void setListQuickNews(ArrayList<QuickNewsOnMedia> listQuickNews) {
        this.listQuickNews = listQuickNews;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_om_quick_news, parent, false);
        ViewQuickNewsHolder viewQuickNewsHolder = new ViewQuickNewsHolder(view);
        return viewQuickNewsHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        QuickNewsOnMedia quickNewsOnMedia = listQuickNews.get(position);
        ((ViewQuickNewsHolder) holder).setElement(quickNewsOnMedia);
    }

    @Override
    public int getItemCount() {
        return listQuickNews.size();
    }

    class ViewQuickNewsHolder extends BaseViewHolder {

        private View viewQuickNews;
        private TextView mTvwTitle;
        private CircleImageView mImgThumb;

        public ViewQuickNewsHolder(View itemView) {
            super(itemView);
            viewQuickNews = itemView.findViewById(R.id.view_quick_news);
            mTvwTitle = (TextView) itemView.findViewById(R.id.tvw_title_quick_news);
            mImgThumb = (CircleImageView) itemView.findViewById(R.id.img_thumb_quick_news);
        }

        @Override
        public void setElement(Object obj) {
            final QuickNewsOnMedia quickNewsOnMedia = (QuickNewsOnMedia) obj;
            Log.i(TAG, "setElement: " + quickNewsOnMedia.toString());
            ImageLoaderManager.getInstance(mActivity).setImageFeeds(mImgThumb, quickNewsOnMedia.getThumb(),
                    sizeThumb, sizeThumb);
            mTvwTitle.setText(quickNewsOnMedia.getTitle());
            viewQuickNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String schemaLink = quickNewsOnMedia.getDeepLink();
                    DeepLinkHelper.getInstance().openSchemaLink(mActivity, schemaLink);
                }
            });
        }
    }
}

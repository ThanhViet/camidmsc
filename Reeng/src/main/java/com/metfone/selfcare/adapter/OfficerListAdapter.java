package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.holder.contact.OfficerAccountViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;


public class OfficerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<OfficerAccount> listOfficers;
    private LayoutInflater inflater;
    private int typeHolderContact;
    private RecyclerClickListener mRecyclerClickListener;

    public OfficerListAdapter(Context context, int typeHolderContact, ArrayList<OfficerAccount> listOfficers) {
        this.context = context;
        this.listOfficers = listOfficers;
        inflater = LayoutInflater.from(this.context);
        this.typeHolderContact = typeHolderContact;
    }

    public void setOfficerList(ArrayList<OfficerAccount> officers) {
        listOfficers = officers;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        if (listOfficers == null) {
            return 0;
        }
        return listOfficers.size();
    }

    public Object getItem(int position) {
        return listOfficers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        OfficerAccountViewHolder holder;
        View view = inflater.inflate(R.layout.holder_officer_viewer, parent, false);
        holder = new OfficerAccountViewHolder(view, context, null);
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OfficerAccountViewHolder officerHolder = (OfficerAccountViewHolder) holder;
        if (mRecyclerClickListener != null)
            officerHolder.setViewClick(position, getItem(position));
        officerHolder.setElement(getItem(position));
    }
}
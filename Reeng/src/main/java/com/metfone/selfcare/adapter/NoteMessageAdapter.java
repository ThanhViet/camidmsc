package com.metfone.selfcare.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.message.NoteMessageItem;
import com.metfone.selfcare.fragment.setting.NoteMessageFragment;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.RecyclerViewItemClickListener;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.imageview.CircleImageView;

import java.io.File;
import java.util.List;

import static com.bumptech.glide.load.DecodeFormat.DEFAULT;

/**
 * Created by sonnn00 on 7/17/2017.
 */

public class NoteMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<NoteMessageItem> dataList;
    private ApplicationController mApplication;
    private LayoutInflater mInflater;
    private NoteMessageFragment fragment;
    private RecyclerViewItemClickListener mClickListener;
    private int viewTypeItem = 0;
    private int viewTypeFooter = 1;
    public boolean isLoading;
    private RequestOptions options;

    public NoteMessageAdapter(NoteMessageFragment fragment, List<NoteMessageItem> dataList, ApplicationController application, RecyclerViewItemClickListener clickListener) {
        this.dataList = dataList;
        this.mApplication = application;
        this.fragment = fragment;
        this.mInflater = (LayoutInflater) mApplication
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListener = clickListener;
        options = new RequestOptions()
                .placeholder(R.drawable.ic_avatar_default)
                .error(R.drawable.ic_avatar_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
    }

    public void setRecyclerClickListener(RecyclerViewItemClickListener mRecyclerClickListener) {
        this.mClickListener = mRecyclerClickListener;
    }

    public void setDataList(List<NoteMessageItem> dataList) {
        this.dataList = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        if (viewType == viewTypeItem) {
//            NoteMessageAdapter.ListNoteViewHolder holder;
            View view = mInflater.inflate(R.layout.holder_note_message, parent, false);

            holder = new NoteMessageAdapter.ListNoteViewHolder(mApplication, view, mClickListener);
        } else {
            View view = LayoutInflater.from(fragment.getContext()).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(view);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ListNoteViewHolder) {
            NoteMessageAdapter.ListNoteViewHolder officerHolder = (NoteMessageAdapter.ListNoteViewHolder) holder;
            officerHolder.setElement(dataList.get(position));
        } else {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            if (isLoading) {
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
            } else {
                loadingViewHolder.progressBar.setVisibility(View.GONE);

            }
        }

    }

    public void setLoaded() {
        isLoading = false;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == dataList.size()) {
            return viewTypeFooter;
        } else {
            return viewTypeItem;
        }
//        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return dataList.size() + 1;
    }


    private class ListNoteViewHolder extends BaseViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private RecyclerViewItemClickListener mClickListener;
        private ApplicationController application;
        private NoteMessageItem item;
        private Context mContext;
        private LinearLayout linear_content, image_thumnail_ll;
        private CircleImageView img_thumb;
        private TextView text_content;
        private TextView text_time;
        private TextView text_date;

        public ListNoteViewHolder(Context context, View itemView, RecyclerViewItemClickListener mClickListener) {
            super(itemView);
            this.mContext = context;
            this.application = (ApplicationController) mContext.getApplicationContext();
            img_thumb = itemView.findViewById(R.id.image_thumnail);
            text_content = itemView.findViewById(R.id.tv_content);
            text_time = itemView.findViewById(R.id.tv_time);
            text_date = itemView.findViewById(R.id.tv_date);
            linear_content = itemView.findViewById(R.id.linear_content);
            image_thumnail_ll = itemView.findViewById(R.id.image_thumnail_ll);
            this.mClickListener = mClickListener;

        }

        @Override
        public void setElement(final Object obj) {
            item = (NoteMessageItem) obj;

            if (TextUtils.isEmpty(item.getThread_avatar()) || (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT)) {
                img_thumb.setImageResource(R.drawable.ic_avatar_default);
//                application.getUniversalImageLoader().cancelDisplayTask(img_thumb);
                Glide.with(mApplication).clear(img_thumb);
            } else {
                if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    Glide.with(mApplication)
                            .load(item.getThread_avatar())
                            .apply(options)
                            .into(img_thumb);
//                    application.getUniversalImageLoader().displayImage(item.getThread_avatar(), new ImageViewAwareTargetSize(img_thumb), displayGroupAvatarOptions);

                } else if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
                    File fileGroupAvatar = new File(item.getThread_avatar());
                    if (fileGroupAvatar.exists()) {
                        Glide.with(mApplication)
                                .load("file://" + item.getThread_avatar())
                                .apply(options)
                                .into(img_thumb);

//                        application.getUniversalImageLoader().displayImage("file://" + item.getThread_avatar(), img_thumb, displayGroupAvatarOptions);
                    } else {
                        if (item.getThread_avatar().contains("http://")) {
                            Glide.with(mApplication)
                                    .load(item.getThread_avatar())
                                    .apply(options)
                                    .into(img_thumb);

//                            application.getUniversalImageLoader().displayImage(item.getThread_avatar(), new ImageViewAwareTargetSize(img_thumb), displayGroupAvatarOptions);
                        } else {
                            img_thumb.setImageResource(R.drawable.ic_avatar_default);
//                            application.getUniversalImageLoader().cancelDisplayTask(img_thumb);
                            Glide.with(mApplication).clear(img_thumb);
                        }
                    }

                } else if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {//chat room
                    application.getAvatarBusiness().setNoteMessageAvatar(img_thumb, item.getThread_avatar());


                } else if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {    // official
                    Glide.with(mApplication)
                            .load(item.getThread_avatar())
                            .apply(options)
                            .into(img_thumb);
//                    application.getUniversalImageLoader().displayImage(item.getThread_avatar(), new ImageViewAwareTargetSize(img_thumb), displayGroupAvatarOptions);

                }
            }

            text_content.setText(item.getContent());
            text_time.setText(TimeHelper.formatTimeInDay(item.getTimestamp()));
            text_date.setText(TimeHelper.getDateOfMessage(item.getTimestamp()));

            linear_content.setOnClickListener(this);
            linear_content.setOnLongClickListener(this);
            image_thumnail_ll.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linear_content:
                    mClickListener.onClick(item);
                    break;
                case R.id.image_thumnail_ll:
                    mClickListener.onAvatarClick(item);
                    break;
            }
        }


        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()) {
                case R.id.linear_content:
                    mClickListener.onLongClick(item);
                    break;
            }
            return false;
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressLoading progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar1);

        }
    }
}
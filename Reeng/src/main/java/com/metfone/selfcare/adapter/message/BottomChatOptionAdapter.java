package com.metfone.selfcare.adapter.message;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/29/2017.
 */

public class BottomChatOptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ItemContextMenu> listItems = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private RecyclerClickListener listener;

    public BottomChatOptionAdapter(ApplicationController application, RecyclerClickListener listener) {
        this.mLayoutInflater = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listener = listener;
    }

    public void setListItems(ArrayList<ItemContextMenu> list) {
        this.listItems = list;
    }

    @Override
    public int getItemCount() {
        if (listItems == null) return 0;
        return listItems.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = new ViewHolder(mLayoutInflater.inflate(R.layout.holder_bottom_chat_option, parent, false));
        holder.setRecyclerClickListener(listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((BaseViewHolder) holder).setElement(listItems.get(position));
    }


    private class ViewHolder extends BaseViewHolder {
        private ItemContextMenu mEntry;
        private ImageView mImgIcon;
        private TextView mTvwLabel;

        public ViewHolder(View itemView) {
            super(itemView);
            mImgIcon = (ImageView) itemView.findViewById(R.id.holder_bottom_chat_option_icon);
            mTvwLabel = (TextView) itemView.findViewById(R.id.holder_bottom_chat_option_label);
        }

        @Override
        public void setElement(Object obj) {
            mEntry = (ItemContextMenu) obj;
            mImgIcon.setImageResource(mEntry.getImageRes());
            mTvwLabel.setText(mEntry.getText());
            setViewClick(-1, obj);
        }
    }
}

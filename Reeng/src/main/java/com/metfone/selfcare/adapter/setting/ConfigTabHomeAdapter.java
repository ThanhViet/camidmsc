package com.metfone.selfcare.adapter.setting;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.setting.ConfigTabHomeFragment;
import com.metfone.selfcare.helper.MochaShortcutManager;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.ItemInvisibleHolder;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.ui.recyclerview.drag.ItemTouchHelperAdapter;
import com.metfone.selfcare.ui.recyclerview.drag.ItemTouchHelperViewHolder;
import com.metfone.selfcare.ui.recyclerview.drag.OnStartDragListener;
import com.metfone.selfcare.v5.widget.SwitchButton;

import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thanhnt72 on 12/11/2018.
 */

public class ConfigTabHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements ItemTouchHelperAdapter {

    private static final int TYPE_INVISIBLE = 0;
    private static final int TYPE_VISIBLE = 1;


    CopyOnWriteArrayList<ConfigTabHomeItem> listItem = new CopyOnWriteArrayList<>();
    private ApplicationController mApp;
    private final OnStartDragListener mDragStartListener;
    ConfigTabHomeFragment.ConfigTabHomeListener listener;

    public ConfigTabHomeAdapter(CopyOnWriteArrayList<ConfigTabHomeItem> listItem, ApplicationController mApp,
                                OnStartDragListener dragStartListener, ConfigTabHomeFragment.ConfigTabHomeListener listener) {
        this.listItem = listItem;
        this.mApp = mApp;
        mDragStartListener = dragStartListener;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        ConfigTabHomeItem item = listItem.get(position);
        if (item.getState() == ConfigTabHomeItem.STATE_UNAVAILABLE)
            return TYPE_INVISIBLE;
        return TYPE_VISIBLE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_INVISIBLE) {
            View viewUnknown = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_feed_default, parent, false);
            ItemInvisibleHolder viewHolder = new ItemInvisibleHolder(viewUnknown, mApp);
            return viewHolder;
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_config_tab_home, parent, false);
            ConfigTabHomeHolder holder = new ConfigTabHomeHolder(view);
            return holder;

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BaseViewHolder baseHolder = (BaseViewHolder) holder;
        baseHolder.setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        return listItem == null ? 0 : listItem.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(listItem, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        listener.onMoveItem(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        listItem.remove(position);
        notifyItemRemoved(position);
    }

    public class ConfigTabHomeHolder extends BaseViewHolder implements
            ItemTouchHelperViewHolder {

        private AppCompatImageView ivHandler, btnAddShortCut;
        private SwitchButton switchButtonSelect;
        private TextView tvTitle, tvDesc;
        private ConfigTabHomeItem item;

        public ConfigTabHomeHolder(View itemView) {
            super(itemView);
            ivHandler = itemView.findViewById(R.id.ivHandler);
            switchButtonSelect = itemView.findViewById(R.id.ivSelected);
            btnAddShortCut = itemView.findViewById(R.id.btnAddShortCut);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDesc = itemView.findViewById(R.id.tvDesc);
        }

        @Override
        public void setElement(Object obj) {
             item = (ConfigTabHomeItem) obj;
            if (item.getHomeTab() == TabHomeHelper.HomeTab.tab_wap) {
                tvTitle.setText(item.getName());
                tvDesc.setText(item.getDesc());
            } else {
                tvTitle.setText(TabHomeHelper.getNameTabHome(item.getHomeTab(), mApp));
                tvDesc.setText(TabHomeHelper.getDescTabHome(item.getHomeTab(), mApp));
            }
            setViewSelect(item);

            ivHandler.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    onMyTouch(event);
                    return false;
                }
            });
            switchButtonSelect.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        boolean isSuccess = listener.onChangeStateItem(getAdapterPosition());
                        if(isSuccess) {
                            setViewSelect(item);
                        }
                    }
                    return true;
                }
            });
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                btnAddShortCut.setVisibility(View.VISIBLE);
                btnAddShortCut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.addShortcut(item, getAdapterPosition());
                    }
                });
                if(MochaShortcutManager.checkShortcutAdded(MochaShortcutManager.convertShortcutId(item.getHomeTab()),mApp)){
                    btnAddShortCut.setImageResource(R.drawable.ic_v5_star_primary);
                }else {
                    btnAddShortCut.setImageResource(R.drawable.ic_v5_star_setting);
                }
            } else {
                btnAddShortCut.setVisibility(View.GONE);
            }
        }

        private void setViewSelect(ConfigTabHomeItem item) {
            if (item.getState() == ConfigTabHomeItem.STATE_ACTIVE)
                switchButtonSelect.setChecked(true);
            else
                switchButtonSelect.setChecked(false);
        }

        private void onMyTouch(MotionEvent event) {
            if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                mDragStartListener.onStartDrag(this);
            }
        }

        @Override
        public void onItemSelected() {

        }

        @Override
        public void onItemClear() {

        }
        public void setIconShortcut(){
            if(MochaShortcutManager.checkShortcutAdded(MochaShortcutManager.convertShortcutId(item.getHomeTab()),mApp)){
                btnAddShortCut.setImageResource(R.drawable.ic_v5_star_primary);
            }else {
                btnAddShortCut.setImageResource(R.drawable.ic_v5_star_setting);
            }
        }
    }

}

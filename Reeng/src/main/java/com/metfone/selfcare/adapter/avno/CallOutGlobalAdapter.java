package com.metfone.selfcare.adapter.avno;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.DeeplinkItem;
import com.metfone.selfcare.database.model.avno.FakeMOItem;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by thanhnt72 on 9/13/2018.
 */

public class CallOutGlobalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = CallOutGlobalAdapter.class.getSimpleName();

    private static final int TYPE_MORE = 0;
    private static final int TYPE_PACKAGE = 1;

    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private ArrayList<PackageAVNO> listItem;
    private Resources mRes;
    private AVNOHelper.PackageListener listener;

    public CallOutGlobalAdapter(BaseSlidingFragmentActivity activity, ArrayList<PackageAVNO> listItem,
                                AVNOHelper.PackageListener listener) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplication();
        this.listItem = listItem;
        this.mRes = mApplication.getResources();
        this.listener = listener;
    }

    public void setListItem(ArrayList<PackageAVNO> listItem) {
        this.listItem = listItem;
    }

    @Override
    public int getItemViewType(int position) {
        PackageAVNO packageAVNO = listItem.get(position);
        if (packageAVNO.getId() == -1) {
            return TYPE_MORE;
        } else
            return TYPE_PACKAGE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        BaseViewHolder holder;
        if (viewType == TYPE_PACKAGE) {
            View view = LayoutInflater.from(mApplication).inflate(R.layout.holder_avno_package_international, parent, false);
            holder = new ViewHolder(view);
        } else
            holder = new ViewMoreHolder(LayoutInflater.from(mApplication).inflate(R.layout.holder_avno_more, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_PACKAGE)
            ((ViewHolder) holder).setElement(listItem.get(position));
        else
            ((ViewMoreHolder) holder).setElement(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem == null || listItem.isEmpty())
            return 0;
        return listItem.size();
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvTitlePackage)
        TextView tvTitlePackage;
        @BindView(R.id.ivPackage)
        ImageView ivPackage;
        @BindView(R.id.tvContentPackage)
        TextView tvContentPackage;
        @BindView(R.id.tvAction)
        RoundTextView tvDeeplinkFirst;
        @BindView(R.id.tvDeeplink)
        RoundTextView tvDeeplinkSecond;
        @BindView(R.id.tvIsActive)
        TextView tvIsActive;
        @BindView(R.id.rlDeeplink)
        RelativeLayout rlDeeplinkSecond;
        @BindView(R.id.rlAction)
        RelativeLayout rlDeeplinkFirst;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            tvContentPackage.setMovementMethod(LinkMovementMethod.getInstance());
        }

        @Override
        public void setElement(Object obj) {
            final PackageAVNO packageAVNO = (PackageAVNO) obj;
            tvTitlePackage.setText(packageAVNO.getTitle());
            Glide.with(mApplication)
                    .load(packageAVNO.getImgUrl())
                    .apply(new RequestOptions().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher))
                    .into(ivPackage);

            setTextViewHTML(tvContentPackage, packageAVNO.getDesc());

            setViewButton(packageAVNO);
        }

        private void setViewButton(PackageAVNO packageAVNO) {
            int remain = 2;
            rlDeeplinkFirst.setVisibility(View.GONE);
            rlDeeplinkSecond.setVisibility(View.GONE);
            if (packageAVNO.getDeeplinkItems() != null && !packageAVNO.getDeeplinkItems().isEmpty()) {
                DeeplinkItem deeplinkItemFirst = packageAVNO.getDeeplinkItems().get(0);
                setDetailButtonDeepLink(rlDeeplinkFirst, tvDeeplinkFirst, deeplinkItemFirst);
                remain--;

                //nếu size =2 thì set nốt vào nút 2
                if (packageAVNO.getDeeplinkItems().size() > 1) {
                    final DeeplinkItem deeplinkItemSecond = packageAVNO.getDeeplinkItems().get(1);
                    setDetailButtonDeepLink(rlDeeplinkSecond, tvDeeplinkSecond, deeplinkItemSecond);
                    remain--;
                }
            }

            //nếu set nút chưa đủ 2 và list fakemo > 0
            if (remain > 0 && packageAVNO.getFakeMOItems() != null && !packageAVNO.getFakeMOItems().isEmpty()) {
                FakeMOItem fakeMOItem = packageAVNO.getFakeMOItems().get(0);
                if (remain == 2) {     //set vào nút 1
                    setDetailButtonFakeMO(rlDeeplinkFirst, tvDeeplinkFirst, fakeMOItem);
                    remain--;
                } else if (remain == 1) {   //set vào nút 2
                    setDetailButtonFakeMO(rlDeeplinkSecond, tvDeeplinkSecond, fakeMOItem);
                    remain--;
                }

                //chắc chắn nếu còn chỉ có nút 2
                if (remain > 0 && packageAVNO.getFakeMOItems().size() > 1) {
                    FakeMOItem fakeMOItemSecond = packageAVNO.getFakeMOItems().get(1);
                    setDetailButtonFakeMO(rlDeeplinkSecond, tvDeeplinkSecond, fakeMOItemSecond);
                }

            }

            if (packageAVNO.getInUse() == 1) {
                tvIsActive.setVisibility(View.VISIBLE);
            } else {
                tvIsActive.setVisibility(View.GONE);
            }


        }

        private void setDetailButtonDeepLink(View parent, RoundTextView textView, final DeeplinkItem deeplinkItem) {
            parent.setVisibility(View.VISIBLE);
            textView.setText(deeplinkItem.getLabel());
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickDeeplink(deeplinkItem);
                    }
                }
            });
            setBackgroudButton(textView, deeplinkItem.getStateButton());
        }

        private void setDetailButtonFakeMO(View parent, RoundTextView textView, final FakeMOItem fakeMOItem) {
            parent.setVisibility(View.VISIBLE);
            textView.setText(fakeMOItem.getLabel());
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickFakeMO(fakeMOItem);
                    }
                }
            });
            setBackgroudButton(textView, fakeMOItem.getStateButton());
        }

        private void setBackgroudButton(RoundTextView textView, int stateButton) {
            switch (stateButton) {
                case 1:
                    textView.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.white),
                            ContextCompat.getColor(mApplication, R.color.bg_mocha_focus));
                    textView.setStroke(ContextCompat.getColor(mApplication, R.color.bg_mocha), 1);
                    textView.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
                    textView.setEnabled(true);
                    break;

                case 2:
                    textView.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.gray_light),
                            ContextCompat.getColor(mApplication, R.color.bg_mocha_focus));
                    textView.setStroke(ContextCompat.getColor(mApplication, R.color.gray_light), 0);
                    textView.setTextColor(ContextCompat.getColor(mApplication, R.color.color_text_singer));
                    textView.setEnabled(false);
                    break;

                default:
                    textView.setBackgroundColorAndPress(ContextCompat.getColor(mApplication, R.color.bg_mocha),
                            ContextCompat.getColor(mApplication, R.color.bg_mocha_focus));
                    textView.setStroke(ContextCompat.getColor(mApplication, R.color.bg_mocha), 0);
                    textView.setTextColor(ContextCompat.getColor(mApplication, R.color.white));
                    textView.setEnabled(true);
                    break;
            }
        }
    }


    public class ViewMoreHolder extends BaseViewHolder {

        @BindView(R.id.tvTitlePackage)
        TextView tvTitlePackage;
        @BindView(R.id.ivPackage)
        ImageView ivPackage;
        @BindView(R.id.tvContentPackage)
        TextView tvContentPackage;

        public ViewMoreHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvContentPackage.setMovementMethod(LinkMovementMethod.getInstance());
        }

        @Override
        public void setElement(Object obj) {
            final PackageAVNO packageAVNO = (PackageAVNO) obj;
            tvTitlePackage.setText(packageAVNO.getTitle());
            Glide.with(mApplication)
                    .load(packageAVNO.getImgUrl())
                    .apply(new RequestOptions().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher))
                    .into(ivPackage);
            setTextViewHTML(tvContentPackage, packageAVNO.getDesc());
        }




    }

    private void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
    }

    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                // Do something with span.getURL() to handle the link click...
                String deeplink = span.getURL();
                DeepLinkHelper.getInstance().openSchemaLink(activity, deeplink);
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

}

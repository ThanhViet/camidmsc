package com.metfone.selfcare.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.message.MessageConstants;
import com.metfone.selfcare.holder.message.AdvertiseHolder;
import com.metfone.selfcare.holder.message.BannerMessageHolder;
import com.metfone.selfcare.holder.message.BaseMessageHolder;
import com.metfone.selfcare.holder.message.MessageNotificationHolder;
import com.metfone.selfcare.holder.message.PollHolder;
import com.metfone.selfcare.holder.message.ReceivedActionMusicHolder;
import com.metfone.selfcare.holder.message.ReceivedBankplusHolder;
import com.metfone.selfcare.holder.message.ReceivedCallHolder;
import com.metfone.selfcare.holder.message.ReceivedCrbtGiftHolder;
import com.metfone.selfcare.holder.message.ReceivedDeepLinkHolder;
import com.metfone.selfcare.holder.message.ReceivedFileFullScreenHolder;
import com.metfone.selfcare.holder.message.ReceivedFileHolder;
import com.metfone.selfcare.holder.message.ReceivedGiftLixiHolder;
import com.metfone.selfcare.holder.message.ReceivedGreetingStickerHolder;
import com.metfone.selfcare.holder.message.ReceivedImageHolder;
import com.metfone.selfcare.holder.message.ReceivedInviteMusicHolder;
import com.metfone.selfcare.holder.message.ReceivedSearchRankHolder;
import com.metfone.selfcare.holder.message.ReceivedShareContactHolder;
import com.metfone.selfcare.holder.message.ReceivedShareLocationHolder;
import com.metfone.selfcare.holder.message.ReceivedShareVideoHolder;
import com.metfone.selfcare.holder.message.ReceivedSuggestVoiceStickerHolder;
import com.metfone.selfcare.holder.message.ReceivedTextHolder;
import com.metfone.selfcare.holder.message.ReceivedTransferMoneyHolder;
import com.metfone.selfcare.holder.message.ReceivedVoiceStickerHolder;
import com.metfone.selfcare.holder.message.ReceivedVoicemailHolder;
import com.metfone.selfcare.holder.message.ReceivedWatchVideoHolder;
import com.metfone.selfcare.holder.message.SendActionMusicHolder;
import com.metfone.selfcare.holder.message.SendBankplusHolder;
import com.metfone.selfcare.holder.message.SendCallHolder;
import com.metfone.selfcare.holder.message.SendFileMessageHolder;
import com.metfone.selfcare.holder.message.SendImageHolder;
import com.metfone.selfcare.holder.message.SendInviteMusicHolder;
import com.metfone.selfcare.holder.message.SendSearchRankHolder;
import com.metfone.selfcare.holder.message.SendShareContactHolder;
import com.metfone.selfcare.holder.message.SendShareLocationHolder;
import com.metfone.selfcare.holder.message.SendShareVideoHolder;
import com.metfone.selfcare.holder.message.SendTextHolder;
import com.metfone.selfcare.holder.message.SendTransferMoneyHolder;
import com.metfone.selfcare.holder.message.SendVoiceStickerHolder;
import com.metfone.selfcare.holder.message.SendVoicemailHolder;
import com.metfone.selfcare.holder.message.SendWatchVideoHolder;
import com.metfone.selfcare.holder.message.SuggestShareMusicHolder;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.util.Log;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class ThreadDetailAdapter extends BaseAdapter {
    private static final String TAG = ThreadDetailAdapter.class.getSimpleName();
    public static final long TIME_DIVIDE_THREAD_SECTION = 30 * 60 * 1000; //30 phut
    public CopyOnWriteArrayList<ReengMessage> messages;
    public ApplicationController applicationController;
    public int mThreadType;
    public SmartTextClickListener mSmartTextListener;
    public MessageInteractionListener mMessageInteractionListener;
    public LayoutInflater layoutInflater;
    public boolean isShowInfoMsg = false;
    public ThreadMessage mThreadMessage;
    private int mLastMessageOfOthersPostion = -1;

    public TagMocha.OnClickTag onClickTag;
    private boolean hasNewMessage;

    public ThreadDetailAdapter(BaseSlidingFragmentActivity activity, int threadType,
                               SmartTextClickListener smartTextListener, MessageInteractionListener listener,
                               TagMocha.OnClickTag onClickTag) {
        mThreadType = threadType;
        layoutInflater = LayoutInflater.from(activity);
        applicationController = (ApplicationController) activity.getApplication();
        mSmartTextListener = smartTextListener;
        mMessageInteractionListener = listener;
        this.onClickTag = onClickTag;
    }

    public void clearNewMessage() {
        if (hasNewMessage) {
            for (ReengMessage msg : messages) {
                msg.setNewMessage(false);
            }
            notifyDataSetChanged();
        }

    }

    public CopyOnWriteArrayList<ReengMessage> getMessages() {
        return messages;
    }

    public void setMessages(CopyOnWriteArrayList<ReengMessage> messages) {
        this.messages = messages;
        //this.messages = new CopyOnWriteArrayList<>();
    }

    public void setThreadMessage(ThreadMessage threadMessage) {
        this.mThreadMessage = threadMessage;
    }

    @Override
    public int getCount() {
        if (messages == null) return 0;
        else
            return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        ReengMessage message = (ReengMessage) getItem(position);
        ReengMessageConstant.MessageType messageType = message.getMessageType();
        ReengMessageConstant.Direction messageDirection = message.getDirection();
        if (messageType == ReengMessageConstant.MessageType.text) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_TEXT;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_TEXT;
            }
        } else if (messageType == ReengMessageConstant.MessageType.file) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_FILE;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_FILE;
            }
        } else if (messageType == ReengMessageConstant.MessageType.image) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_IMAGE;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_IMAGE;
            }
        } else if (messageType == ReengMessageConstant.MessageType.voicemail) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_VOICEMAIL;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_VOICEMAIL;
            }
        } else if (messageType == ReengMessageConstant.MessageType.shareContact) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_SHARE_CONTACT;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_SHARE_CONTACT;
            }
        } else if (messageType == ReengMessageConstant.MessageType.shareVideo) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_SHARE_VIDEO;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_SHARE_VIDEO;
            }
        } else if (messageType == ReengMessageConstant.MessageType.voiceSticker) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_VOICE_STICKER;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_VOICE_STICKER;
            }
        } else if (messageType == ReengMessageConstant.MessageType.notification ||
                messageType == ReengMessageConstant.MessageType.notification_fake_mo
                ) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.poll_action ||
                messageType == ReengMessageConstant.MessageType.poll_create) {
            if (mThreadMessage != null) {
                HashMap<String, Integer> hashMap = mThreadMessage.getPollLastId();
                if (hashMap.containsKey(message.getFileId())) {
                    int lastId = hashMap.get(message.getFileId());
                    if (lastId < message.getId())
                        hashMap.put(message.getFileId(), message.getId());
                } else {
                    hashMap.put(message.getFileId(), message.getId());
                }
            }
            return MessageConstants.TYPE_MESSAGE_POLL;
        } else if (messageType == ReengMessageConstant.MessageType.inviteShareMusic) {
            if (message.getSize() == 0) {
                return MessageConstants.TYPE_MESSAGE_NOTIFY;
            } else if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_INVITE_MUSIC;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_INVITE_MUSIC_NEW;
            }
        } else if (messageType == ReengMessageConstant.MessageType.actionShareMusic) {// chuyen bai, doi bai
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_ACTION_MUSIC;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_ACTION_MUSIC;
            }
        } else if (messageType == ReengMessageConstant.MessageType.suggestShareMusic) {
            return MessageConstants.TYPE_MESSAGE_SUGGEST_SHARE_MUSIC;
        } else if (messageType == ReengMessageConstant.MessageType.greeting_voicesticker) {
            return MessageConstants.TYPE_MESSAGE_GREETING_VOICE_STICKER;
        } else if (messageType == ReengMessageConstant.MessageType.shareLocation) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_LOCATION;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_LOCATION;
            }
        } else if (messageType == ReengMessageConstant.MessageType.restore) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_RESTORE;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_RESTORE;
            }
        } else if (messageType == ReengMessageConstant.MessageType.transferMoney) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_TRANSFER_MONEY;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_TRANSFER_MONEY;
            }
        } else if (messageType == ReengMessageConstant.MessageType.event_follow_room ||
                messageType == ReengMessageConstant.MessageType.fake_mo) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_SEARCH_RANK;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_SEARCH_RANK;
            }
        } else if (messageType == ReengMessageConstant.MessageType.crbt_gift) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_CRBT_GIFT;
        } else if (messageType == ReengMessageConstant.MessageType.deep_link ||
                messageType == ReengMessageConstant.MessageType.luckywheel_help) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_DEEP_LINK;
        } else if (messageType == ReengMessageConstant.MessageType.gift) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_VOICE_STICKER;
        } else if (messageType == ReengMessageConstant.MessageType.image_link) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_FILE_LINK;
        } else if (messageType == ReengMessageConstant.MessageType.advertise) {
            return MessageConstants.TYPE_MESSAGE_ADVERTISE;
        } else if (messageType == ReengMessageConstant.MessageType.call ||
                messageType == ReengMessageConstant.MessageType.talk_stranger) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_CALL;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_CALL;
            }
        } else if (messageType == ReengMessageConstant.MessageType.watch_video) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_WATCH_VIDEO;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_WATCH_VIDEO;
            }
        } else if (messageType == ReengMessageConstant.MessageType.bank_plus) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_SEND_BPLUS;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_BPLUS;
            }
        } else if (messageType == ReengMessageConstant.MessageType.lixi) {
            if (messageDirection == ReengMessageConstant.Direction.send) {
                return MessageConstants.TYPE_MESSAGE_NOTIFY;
            } else {
                return MessageConstants.TYPE_MESSAGE_RECEIVED_GIFT_LIXI;
            }
        } else if (messageType == ReengMessageConstant.MessageType.message_banner) {
            return MessageConstants.TYPE_MESSAGE_BANNER;
        } else if (messageType == ReengMessageConstant.MessageType.pin_message) {
            return MessageConstants.TYPE_MESSAGE_NOTIFY;
        } else if (messageType == ReengMessageConstant.MessageType.suggest_voice_sticker) {
            return MessageConstants.TYPE_MESSAGE_SUGGEST_VOICESTICKER;
        } else if (messageType == ReengMessageConstant.MessageType.update_app) {
            return MessageConstants.TYPE_MESSAGE_RECEIVED_DEEP_LINK;
        }
        return MessageConstants.TYPE_MESSAGE_NOTIFY;// tra ve default la notify
    }

    @Override
    /**
     * @author cngp_thaodv
     */
    public int getViewTypeCount() {
        return MessageConstants.TYPE_MESSAGE_MAX_COUNT;
    }

    @Override
    /**
     * @author cngp_thaodv
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        long t = System.currentTimeMillis();
        int type = getItemViewType(position);
        ReengMessage message = messages.get(position);
        BaseMessageHolder holder;
        switch (type) {
            case MessageConstants.TYPE_MESSAGE_SEND_TEXT:
                if (convertView == null) {
                    holder = new SendTextHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendTextHolder) convertView.getTag();
                }
                holder.setOnClickTag(onClickTag);
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_TEXT:
                if (convertView == null) {
                    holder = new ReceivedTextHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedTextHolder) convertView.getTag();
                }
                holder.setOnClickTag(onClickTag);
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_VOICEMAIL:
                if (convertView == null) {
                    holder = new SendVoicemailHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendVoicemailHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_RECEIVED_VOICEMAIL:
                if (convertView == null) {
                    holder = new ReceivedVoicemailHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedVoicemailHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_IMAGE:
                if (convertView == null) {
                    holder = new SendImageHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendImageHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_IMAGE:
                if (convertView == null) {
                    holder = new ReceivedImageHolder(applicationController, mSmartTextListener, mThreadMessage);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedImageHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_FILE:
                if (convertView == null) {
                    holder = new SendFileMessageHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendFileMessageHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_RECEIVED_FILE:
                if (convertView == null) {
                    holder = new ReceivedFileHolder(applicationController, mSmartTextListener, mThreadMessage);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedFileHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_SEND_SHARE_CONTACT:
                if (convertView == null) {
                    holder = new SendShareContactHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendShareContactHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_SHARE_CONTACT:
                if (convertView == null) {
                    holder = new ReceivedShareContactHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedShareContactHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_SHARE_VIDEO:
                if (convertView == null) {
                    holder = new SendShareVideoHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendShareVideoHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_RECEIVED_SHARE_VIDEO:
                if (convertView == null) {
                    holder = new ReceivedShareVideoHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedShareVideoHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_VOICE_STICKER:
                if (convertView == null) {
                    holder = new SendVoiceStickerHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendVoiceStickerHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_RECEIVED_VOICE_STICKER:
                if (convertView == null) {
                    holder = new ReceivedVoiceStickerHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedVoiceStickerHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_GREETING_VOICE_STICKER:
                if (convertView == null) {
                    holder = new ReceivedGreetingStickerHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedGreetingStickerHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_NOTIFY:
                if (convertView == null) {
                    holder = new MessageNotificationHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (MessageNotificationHolder) convertView.getTag();
                }
                break;

            case MessageConstants.TYPE_MESSAGE_SEND_INVITE_MUSIC:
                if (convertView == null) {
                    holder = new SendInviteMusicHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendInviteMusicHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_INVITE_MUSIC_NEW:
                if (convertView == null) {
                    holder = new ReceivedInviteMusicHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedInviteMusicHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SUGGEST_SHARE_MUSIC:
                if (convertView == null) {
                    holder = new SuggestShareMusicHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SuggestShareMusicHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_ACTION_MUSIC:
                if (convertView == null) {
                    holder = new SendActionMusicHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendActionMusicHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_ACTION_MUSIC:
                if (convertView == null) {
                    holder = new ReceivedActionMusicHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedActionMusicHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_LOCATION:
                if (convertView == null) {
                    holder = new SendShareLocationHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendShareLocationHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_LOCATION:
                if (convertView == null) {
                    holder = new ReceivedShareLocationHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedShareLocationHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_RESTORE:
                if (convertView == null) {
                    holder = new SendTextHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendTextHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_RESTORE:
                if (convertView == null) {
                    holder = new ReceivedTextHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedTextHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_TRANSFER_MONEY:
                if (convertView == null) {
                    holder = new SendTransferMoneyHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendTransferMoneyHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_TRANSFER_MONEY:
                if (convertView == null) {
                    holder = new ReceivedTransferMoneyHolder(applicationController, mSmartTextListener, false);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedTransferMoneyHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_SEARCH_RANK:
                if (convertView == null) {
                    holder = new SendSearchRankHolder(applicationController, mMessageInteractionListener,
                            mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendSearchRankHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_SEARCH_RANK:
                if (convertView == null) {
                    holder = new ReceivedSearchRankHolder(applicationController, mMessageInteractionListener,
                            mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedSearchRankHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_CRBT_GIFT:
                if (convertView == null) {
                    holder = new ReceivedCrbtGiftHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedCrbtGiftHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_DEEP_LINK:
                if (convertView == null) {
                    holder = new ReceivedDeepLinkHolder(applicationController, mMessageInteractionListener,
                            mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedDeepLinkHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_FILE_LINK:
                if (convertView == null) {
                    holder = new ReceivedFileFullScreenHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedFileFullScreenHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_ADVERTISE:
                if (convertView == null) {
                    holder = new AdvertiseHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (AdvertiseHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_CALL:
                if (convertView == null) {
                    holder = new SendCallHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendCallHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_CALL:
                if (convertView == null) {
                    holder = new ReceivedCallHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedCallHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_WATCH_VIDEO:
                if (convertView == null) {
                    holder = new SendWatchVideoHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendWatchVideoHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_WATCH_VIDEO:
                if (convertView == null) {
                    holder = new ReceivedWatchVideoHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedWatchVideoHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SEND_BPLUS:
                if (convertView == null) {
                    holder = new SendBankplusHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (SendBankplusHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_BPLUS:
                if (convertView == null) {
                    holder = new ReceivedBankplusHolder(applicationController);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedBankplusHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_RECEIVED_GIFT_LIXI:
                if (convertView == null) {
                    holder = new ReceivedGiftLixiHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedGiftLixiHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_BANNER:
                if (convertView == null) {
                    holder = new BannerMessageHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (BannerMessageHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_SUGGEST_VOICESTICKER:
                if (convertView == null) {
                    holder = new ReceivedSuggestVoiceStickerHolder(applicationController, mMessageInteractionListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (ReceivedSuggestVoiceStickerHolder) convertView.getTag();
                }
                break;
            case MessageConstants.TYPE_MESSAGE_POLL:
                if (convertView == null) {
                    holder = new PollHolder(applicationController, mSmartTextListener);
                    holder.initHolder(parent, convertView, position, layoutInflater);
                } else {
                    holder = (PollHolder) convertView.getTag();
                }
                break;
            default:// dua het ve tin notify
                throw new IllegalArgumentException("" + type);

        }
        holder.setThreadType(mThreadType);
        holder.setMessageInteractionListener(mMessageInteractionListener);
        holder.setShowInfo(isShowInfoMsg);
        // +++++++++++++++call-before-set-element++++++++++++++++++++++++++++++++++++++++
        //---------------------------------------check same date
        if (position <= 0) {
            // neu la phan tu dau tien
            holder.setSameDateWithPrevious(false);
            holder.setDifferSenderWithPrevious(true);
            if (message.isNewMessage()) {
                hasNewMessage = true;
                holder.setFirstNewMessage(true);
            } else {
                holder.setFirstNewMessage(false);
            }
        } else {
            // kiem tra neu tin nay voi tin truoc do cach nhau 10p time_divide_thread_section
            // truong hop may nhan dc gui
            ReengMessage previousMessage = messages.get(position - 1);
            if ((message.getTime() - previousMessage.getTime()) > -TIME_DIVIDE_THREAD_SECTION &&
                    (message.getTime() - previousMessage.getTime()) < TIME_DIVIDE_THREAD_SECTION) {
                holder.setSameDateWithPrevious(true);
            } else {
                holder.setSameDateWithPrevious(false);
            }
            // message trc la notification, messag sau la thuong, thi hien avatar
            if (ReengMessageConstant.MessageType.containsNotification(previousMessage.getMessageType()) &&
                    !ReengMessageConstant.MessageType.containsNotification(message.getMessageType())) {
                holder.setDifferSenderWithPrevious(true);
            } else if (message.getSender().equals(previousMessage.getSender()) && holder.isSameDateWithPrevious()) {
                if (TextUtils.isEmpty(message.getSender())) {// admin room chat
                    if (message.getSenderName() != null && !message.getSenderName().equals(previousMessage
                            .getSenderName())) {
                        // neu sender name khac nhau thi tach row
                        holder.setDifferSenderWithPrevious(true);
                    } else {
                        holder.setDifferSenderWithPrevious(false);
                    }
                } else {
                    // kiem tra neu tin nay voi tin truoc cungf 1 nguoi gui
                    holder.setDifferSenderWithPrevious(false);
                }
            } else {
                holder.setDifferSenderWithPrevious(true);
            }
            if (!previousMessage.isNewMessage() && message.isNewMessage()) {
                holder.setFirstNewMessage(true);
                hasNewMessage = true;
            } else {
                holder.setFirstNewMessage(false);
            }
        }
        //-----------------------------------------
        if (position == (messages.size() - 1)) {
            // neu la phan tu cuoi cung
            holder.setDifferSenderWithAfter(true);
            holder.setIsLastMessage(true);
        } else {
            // kiem tra neu tin nay voi tin sau do la cung 1 nguoi gui
            if (message.getSender().equals(
                    messages.get(position + 1).getSender())) {
                holder.setDifferSenderWithAfter(false);
            } else {
                holder.setDifferSenderWithAfter(true);
            }
        }

        if (position == mLastMessageOfOthersPostion) {
            holder.setLastMessageOfOthers(true);
        } else {
            holder.setLastMessageOfOthers(false);
        }

        if (message.getDirection() == ReengMessageConstant.Direction.send) {
            if ((position + 1) < messages.size() && messages.get(position + 1).getDirection() == ReengMessageConstant.Direction.send && messages.get(position + 1).getStatus() != ReengMessageConstant.STATUS_SEEN) {
                holder.setLastSeenBlock(true);
            } else {
                holder.setLastSeenBlock(false);
            }
        }

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // initDownloadOrUpload(message, holder.getConvertView());
        holder.setElemnts(message);
//        Log.d(TAG, "[perform] - threadDetailAdapter getView (type: " + type + ") take: "
//                + (System.currentTimeMillis() - t));
        return holder.getConvertView();
    }

    @Override
    public void notifyDataSetChanged() {
        Log.d(TAG, "notifyDataSetChanged");
        mLastMessageOfOthersPostion = findLastMessageOfOthers();
        super.notifyDataSetChanged();
    }

    public void setShowInfoMsg(boolean isShow) {
        this.isShowInfoMsg = isShow;
    }

    private int findLastMessageOfOthers() {
        if (messages == null || messages.isEmpty()) return -1;
        try {
            String myNumber = applicationController.getReengAccountBusiness().getJidNumber();
            int size = messages.size();
            for (int i = size - 1; i >= 0; i--) {
                ReengMessage message = messages.get(i);
                if (message.getMessageType() == ReengMessageConstant.MessageType.notification
                        || message.getMessageType() == ReengMessageConstant.MessageType.notification_fake_mo)
                    continue;
                if (!message.getSender().equals(myNumber)) {
                    return i;
                }
            }
        } catch (Exception e) {
        }
        return -1;
    }


}
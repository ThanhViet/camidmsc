package com.metfone.selfcare.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 1/25/2018.
 */

public class ListImageLixiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ListImageLixiAdapter.class.getSimpleName();
    private ApplicationController mApp;
    private ArrayList<String> listImage;
    private RecyclerClickListener clickListener;
    private LayoutInflater inflater;

    public ListImageLixiAdapter(ApplicationController mApp, RecyclerClickListener clickListener) {
        this.mApp = mApp;
        this.clickListener = clickListener;
        inflater = LayoutInflater.from(mApp);
    }

    public void setListImage(ArrayList<String> listImage) {
        this.listImage = listImage;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View threadMessageView = inflater.inflate(R.layout.item_image_horizontal, parent, false);
        ListImageLixiViewHolder viewHolder = new ListImageLixiViewHolder(threadMessageView);
        viewHolder.setRecyclerClickListener(clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String img = listImage.get(position);
        ((ListImageLixiViewHolder) holder).setViewClick(position, listImage.get(position));
        ((ListImageLixiViewHolder) holder).setElement(img);

    }

    @Override
    public int getItemCount() {
        if (listImage == null || listImage.isEmpty())
            return 0;
        else
            return listImage.size();
    }

    public class ListImageLixiViewHolder extends BaseViewHolder {

        ImageView mImgThumb;
        public ListImageLixiViewHolder(View itemView) {
            super(itemView);
            mImgThumb = (ImageView) itemView.findViewById(R.id.img_thumb_onmedia);
            ImageView mimgClose = (ImageView) itemView.findViewById(R.id.img_remove_img);
            mimgClose.setVisibility(View.GONE);
        }

        @Override
        public void setElement(Object obj) {
            String url = (String) obj;
            ImageLoaderManager.getInstance(mApp).displayStickerNetwork(mImgThumb,url);
        }
    }
}

package com.metfone.selfcare.adapter.guestbook;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Background;
import com.metfone.selfcare.database.model.guestbook.Template;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.holder.guestbook.SelectBackgroundHolder;
import com.metfone.selfcare.holder.guestbook.TemplateHolder;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/14/2017.
 */
public class TemplateAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private LayoutInflater inflater;
    private RecyclerClickListener mRecyclerClickListener;
    private ArrayList<Object> listItems;

    public TemplateAdapter(Context context, ArrayList<Object> items) {
        this(context);
        this.listItems = items;
    }

    public TemplateAdapter(Context context) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
    }

    public void setListTemplates(ArrayList<Template> templates) {
        this.listItems = new ArrayList<>();
        listItems.addAll(templates);
    }

    public void setListBackgrounds(ArrayList<Background> backgrounds) {
        this.listItems = new ArrayList<>();
        listItems.addAll(backgrounds);
    }

    public void setListItems(ArrayList<Object> items) {
        this.listItems = items;
    }

    public void setRecyclerClickListener(RecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public int getItemCount() {
        if (listItems == null) {
            return 0;
        }
        return listItems.size();
    }

    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (listItems.get(position) instanceof Template) {
            return Constants.GUEST_BOOK.HOLDER_TEMPLATE;
        } else {
            return Constants.GUEST_BOOK.HOLDER_BACKGROUND;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        BaseViewHolder holder;
        if (type == Constants.GUEST_BOOK.HOLDER_TEMPLATE) {
            View view = inflater.inflate(R.layout.holder_guest_book_template, parent, false);
            holder = new TemplateHolder(view, mContext);
        } else {
            View view = inflater.inflate(R.layout.holder_guest_book_background, parent, false);
            holder = new SelectBackgroundHolder(view, mContext);
        }
        if (mRecyclerClickListener != null)
            holder.setRecyclerClickListener(mRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((BaseViewHolder) holder).setElement(getItem(position));
        if (mRecyclerClickListener != null)
            ((BaseViewHolder) holder).setViewClick(position, getItem(position));
    }
}
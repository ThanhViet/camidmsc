package com.metfone.selfcare.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV3;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class EpisodeAdapter extends BaseAdapterV3 {

    @Nullable
    private OnItemEpisodeListener onItemEpisodeListener;

    public EpisodeAdapter(Activity act) {
        super(act);
    }

    public void setOnItemEpisodeListener(@Nullable OnItemEpisodeListener onItemEpisodeListener) {
        this.onItemEpisodeListener = onItemEpisodeListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EpisodeHolder(layoutInflater, parent);
    }

    private void onItemEpisodeClicked(@NonNull Video video) {
        if (onItemEpisodeListener != null) {
            onItemEpisodeListener.onItemEpisodeClicked(video);
        }
    }

    static class EpisodeHolder extends BaseAdapterV3.ViewHolder {

        @BindView(R.id.tv_episode)
        TextView tvEpisode;
        @BindView(R.id.iv_cover)
        AppCompatImageView ivCover;
        @BindView(R.id.tvTitleVideo)
        AppCompatTextView tvTitleVideo;
        @BindView(R.id.tvDesVideo)
        AppCompatTextView tvDesVideo;
        @BindView(R.id.tvDuration)
        AppCompatTextView tvDuration;
        @BindView(R.id.viewRoot)
        ConstraintLayout viewRoot;
        @BindView(R.id.progress_bar)
        ProgressBar progressCurrent;

        @Nullable
        Video video;

        EpisodeHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.item_episode, parent, false));
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            Object item = items.get(position);
            if (item instanceof Video) {
                video = (Video) item;
                tvDesVideo.setText(video.getDescription());
                tvTitleVideo.setText(video.getTitle());
                if(video.getDurationMinutes() > 60){
                    tvDuration.setText(video.getDurationMinutes() / 60 + "h " + video.getDurationMinutes() % 60 + "m");
                }else{
                    tvDuration.setText(video.getDurationMinutes() + "m");
                }
                ImageBusiness.setCoverMovie(video.getImagePath(), ivCover);
//                tvEpisode.setText(video.getChapter());
                tvEpisode.setBackgroundResource(video.isPlaying() ?
                        R.drawable.xml_background_episode_press :
                        R.drawable.xml_background_episode);
//                viewRoot.setBackgroundResource(video.isPlaying()?R.color.cinema_white_opacity_50:R.color.color_App_new);
                tvEpisode.setTextColor(video.isPlaying() ?
                        App.getInstance().getBaseContext().getResources().getColor(R.color.white) :
                        App.getInstance().getBaseContext().getResources().getColor(R.color.videoV5TextColor));
                if(!StringUtils.isEmpty(video.getTimeWatched())){
                    try{
                        long time = Long.parseLong(video.getTimeWatched());
                        if(time > 0){
                            int progress = (int) ((time/60000)*100/video.getDurationMinutes());
                            progressCurrent.setVisibility(View.VISIBLE);
                            progressCurrent.setProgress(progress);
                        }
                    }catch (Exception e){

                    }
                }else{
                    progressCurrent.setVisibility(View.INVISIBLE);
                }
            } else {
                video = null;
            }
        }

        @OnClick(R.id.viewRoot)
        public void onViewClicked() {
            if (!(baseAdapter instanceof EpisodeAdapter) || video == null) return;
            ((EpisodeAdapter) baseAdapter).onItemEpisodeClicked(video);
        }

    }

    public interface OnItemEpisodeListener {
        void onItemEpisodeClicked(@NonNull Video video);
    }

}

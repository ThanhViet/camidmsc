package com.metfone.selfcare.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.metfone.selfcare.R;

public class FontUtils {
    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";
    public static final int CIF_WEIGHT_DEFAULT = 400;

    public static void applyCustomFont(TextView customFontTextView, Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CamIdFont,
                0, 0);

        // check if a special textStyle was used (e.g. extra bold)
        int sfpWeight = attributeArray.getInt(R.styleable.CamIdFont_cifWeight, CIF_WEIGHT_DEFAULT);

        Typeface customFont = selectTypeface(context, sfpWeight);
        customFontTextView.setTypeface(customFont);

        attributeArray.recycle();
    }

    private static Typeface selectTypeface(Context context, int sfpWeight) {
            /*
            information about the TextView textStyle:
            http://developer.android.com/reference/android/R.styleable.html#TextView_textStyle
            */
        switch (sfpWeight) {
            case Constants.MEDIUM:
                return ResourcesCompat.getFont(context, R.font.font_sf_pro_text_medium);
            case Constants.SEMI_BOLD:
                return ResourcesCompat.getFont(context, R.font.font_sf_pro_text_semibold);
            case Constants.REGULAR:
            default:
                return ResourcesCompat.getFont(context, R.font.font_sf_pro_text_regular);
        }
    }

    public interface Constants {
        int REGULAR = 400;
        int MEDIUM = 500;
        int SEMI_BOLD = 600;
    }
}

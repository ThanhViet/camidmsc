package com.metfone.selfcare.util.contact;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.PhoneNumber;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 12/5/2017.
 */

public class ContactSyncHelper extends Thread {

    private static final String TAG = ContactSyncHelper.class.getSimpleName();
    private ApplicationController mApp;

    private ContactManager mContactManager;
    private ArrayList<PhoneNumber> listPhone;

    public ContactSyncHelper(ApplicationController mApp) {
        this.mApp = mApp;
        mContactManager = new ContactManager(mApp);
    }

    @Override
    public void run() {
        /*if (Version.hasN()) {
            Log.i(TAG, "start contactsync");
            String accountName = mApp.getReengAccountBusiness().getJidNumber();
            mContactManager.createAccount(accountName);
            if (!ContactManager.isExistAccount(mApp, accountName)) {
                Log.i(TAG, "ko tao dc account");
                return;
            }
            mContactManager.syncContacts(accountName, listPhone);
        }*/
    }

    public void setListPhone(ArrayList<PhoneNumber> listPhone) {
        this.listPhone = listPhone;
    }

}

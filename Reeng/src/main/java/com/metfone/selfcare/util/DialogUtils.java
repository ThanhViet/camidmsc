/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.util;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.dialog.BottomSheetMenuAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.database.constant.VideoConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.ui.dialog.BottomSheetDialog;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.tabvideo.activity.uploadVideo.UploadVideoActivity;

import java.util.ArrayList;

import static com.metfone.selfcare.helper.Constants.TabVideo.UPLOAD_LIBRARY;

public class DialogUtils {

    public static void showOptionVideoItem(BaseSlidingFragmentActivity activity, final Video item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
//        itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_ADD_FAVORITE);
//        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.save), R.drawable.ic_add_option, null, Constants.MENU.MENU_SAVE_VIDEO);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.add_later), R.drawable.ic_movie_later_option, null, Constants.MENU.MENU_ADD_LATER);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_video, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionLibraryVideoItem(BaseSlidingFragmentActivity activity, final Video item, String type
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(
                type.equals(VideoConstant.Type.SAVED.VALUE) ? activity.getString(R.string.video_remove_from_saved_list) : activity.getString(R.string.video_remove_from_watch_later_list),
                R.drawable.ic_del_option, null,
                type.equals(VideoConstant.Type.SAVED.VALUE) ? Constants.MENU.MENU_REMOVE_SAVED : Constants.MENU.MENU_REMOVE_WATCHED);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_video, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionMusicItem(BaseSlidingFragmentActivity activity, final AllModel item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_ADD_FAVORITE);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_music, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionMusicSongItem(BaseSlidingFragmentActivity activity, final Object item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_ADD_FAVORITE);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_music, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionMusicPlaylistItem(BaseSlidingFragmentActivity activity, final Object item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_ADD_FAVORITE);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_music, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionMusicVideoItem(BaseSlidingFragmentActivity activity, final Object item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_ADD_FAVORITE);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_music, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionMovieItem(BaseSlidingFragmentActivity activity, final Movie item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        if (ApplicationController.self().getReengAccountBusiness().isVietnam()) {
            itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_ADD_FAVORITE);
            data.add(itemMenu);
            itemMenu = new ItemContextMenu(activity.getString(R.string.add_later), R.drawable.ic_movie_later_option, null, Constants.MENU.MENU_ADD_LATER);
            data.add(itemMenu);
        }
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionVideoMovieItem(BaseSlidingFragmentActivity activity, final Movie item
            , final OnClickMoreItemListener listener) {
        //Hiển thị riêng dialog này cho tab video. Sau khi APi sửa sẽ xóa sau
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionMovieWatched(BaseSlidingFragmentActivity activity, final Object item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.remove_watched), R.drawable.ic_del_option, null, Constants.MENU.MENU_REMOVE_WATCHED);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionMovieLater(BaseSlidingFragmentActivity activity, final Object item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.remove_later), R.drawable.ic_del_option, null, Constants.MENU.MENU_REMOVE_LATER);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionMovieLiked(BaseSlidingFragmentActivity activity, final Object item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.remove_liked), R.drawable.ic_del_option, null, Constants.MENU.MENU_REMOVE_FAVORITE);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionNewsItem(BaseSlidingFragmentActivity activity, final NewsModel item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
//        if (item.isLike()) {
//            itemMenu = new ItemContextMenu(activity.getString(R.string.del_favorite), R.drawable.ic_v5_heart_active, null, Constants.MENU.MENU_ADD_FAVORITE);
//            data.add(itemMenu);
//        } else {
//            itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_v5_heart_normal, null, Constants.MENU.MENU_ADD_FAVORITE);
//            data.add(itemMenu);
//        }
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);
        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionTiinItem(BaseSlidingFragmentActivity activity, final TiinModel item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
//        if (item.isLike()) {
//            itemMenu = new ItemContextMenu(activity.getString(R.string.del_favorite), R.drawable.ic_v5_heart_active, null, Constants.MENU.MENU_ADD_FAVORITE);
//            data.add(itemMenu);
//        } else {
//            itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_v5_heart_normal, null, Constants.MENU.MENU_ADD_FAVORITE);
//            data.add(itemMenu);
//        }
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionComicItem(BaseSlidingFragmentActivity activity, final Object item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_ADD_FAVORITE);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionChannelItem(BaseSlidingFragmentActivity activity, final Object item
            , final OnClickMoreItemListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.share), R.drawable.ic_share_option, null, Constants.MENU.MENU_SHARE_LINK);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.add_favorite), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_ADD_FAVORITE);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null) listener.onClickMoreItem(item, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void onClickUpload(BaseSlidingFragmentActivity activity) {
        UploadVideoActivity.start(activity, UPLOAD_LIBRARY);
//        BottomSheetDialog dialogUpload = new BottomSheetDialog(activity);
//        ArrayList<ItemContextMenu> dialogUploadItems = new ArrayList<>();
//        ItemContextMenu item;
//
//        item = new ItemContextMenu(activity.getString(R.string.channel_upload_youtube), R.drawable.ic_upload_youtube_option, null, Constants.MENU.MENU_VIDEO_YOUTUBE);
//        dialogUploadItems.add(item);
//
//        item = new ItemContextMenu(activity.getString(R.string.channel_upload_local), R.drawable.ic_upload_file_option, null, Constants.MENU.MENU_VIDEO_LIBRARY);
//        dialogUploadItems.add(item);
//
//        item = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
//        dialogUploadItems.add(item);
//
//        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_video, null, false);
//        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
//        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(dialogUploadItems);
//        adapter.setRecyclerClickListener(new RecyclerClickListener() {
//            @Override
//            public void onClick(View v, int pos, Object object) {
//                if (object instanceof ItemContextMenu) {
//                    ItemContextMenu item = (ItemContextMenu) object;
//                    switch (item.getActionTag()) {
//                        case Constants.MENU.MENU_VIDEO_YOUTUBE:
//                            UploadVideoActivity.start(activity, UPLOAD_YOUTUBE);
//                            break;
//                        case Constants.MENU.MENU_VIDEO_LIBRARY:
//                            UploadVideoActivity.start(activity, UPLOAD_LIBRARY);
//                            break;
//                        default:
//                            break;
//                    }
//                }
//                dialogUpload.dismiss();
//            }
//
//            @Override
//            public void onLongClick(View v, int pos, Object object) {
//
//            }
//        });
//        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
//        dialogUpload.setContentView(sheetView);
//        dialogUpload.show();
    }

    public static void showOptionUpdateImage(BaseSlidingFragmentActivity activity, final ClickListener.IconListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.channel_new_image), R.drawable.ic_menu_camera, null, Constants.MENU.CAPTURE_IMAGE);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.channel_upload_image), R.drawable.ic_setting_image, null, Constants.MENU.SELECT_GALLERY);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_video, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null)
                        listener.onIconClickListener(null, null, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    public static void showOptionTabMobile(BaseSlidingFragmentActivity activity, final ClickListener.IconListener listener) {
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        itemMenu = new ItemContextMenu(activity.getString(R.string.menu_mark_read_all), R.drawable.ic_mark_all_read, null, Constants.MENU.MENU_MARK_READ);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.menu_call_history), R.drawable.ic_clock, null, Constants.MENU.MENU_HISTORY);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.create_new_group), R.drawable.ic_group, null, Constants.MENU.MENU_CREATE_GROUP);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.title_new_call), R.drawable.ic_new_call, null, Constants.MENU.MENU_CALL);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.new_message), R.drawable.ic_send, null, Constants.MENU.MENU_MESS);
        data.add(itemMenu);
        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_more_tab_video, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);

        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null)
                        listener.onIconClickListener(null, null, itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {

        });
        dialog.show();
    }

    @SuppressLint("SourceLockedOrientationActivity")
    public static BottomSheetDialog showSpeedDialog(BaseSlidingFragmentActivity activity, final float currentSpeed
            , final OnClickMoreItemListener listener) {
        if (activity == null || activity.isFinishing()) return null;
        ArrayList<ItemContextMenu> data = new ArrayList<>();
        ItemContextMenu itemMenu;
        String title;
        float speed;
        int resIcon;

        speed = 0.25f;
        title = "0.25x";
        resIcon = currentSpeed == speed ? R.drawable.ic_circle_selected : R.drawable.ic_circle_unselected;
        itemMenu = new ItemContextMenu(title, resIcon, speed, Constants.MENU.MENU_SETTING);
        data.add(itemMenu);

        speed = 0.5f;
        title = "0.5x";
        resIcon = currentSpeed == speed ? R.drawable.ic_circle_selected : R.drawable.ic_circle_unselected;
        itemMenu = new ItemContextMenu(title, resIcon, speed, Constants.MENU.MENU_SETTING);
        data.add(itemMenu);

        speed = 0.75f;
        title = "0.75x";
        resIcon = currentSpeed == speed ? R.drawable.ic_circle_selected : R.drawable.ic_circle_unselected;
        itemMenu = new ItemContextMenu(title, resIcon, speed, Constants.MENU.MENU_SETTING);
        data.add(itemMenu);

        speed = 1.0f;
        title = activity.getString(R.string.normal);
        resIcon = currentSpeed == speed ? R.drawable.ic_circle_selected : R.drawable.ic_circle_unselected;
        itemMenu = new ItemContextMenu(title, resIcon, speed, Constants.MENU.MENU_SETTING);
        data.add(itemMenu);

        speed = 1.25f;
        title = "1.25x";
        resIcon = currentSpeed == speed ? R.drawable.ic_circle_selected : R.drawable.ic_circle_unselected;
        itemMenu = new ItemContextMenu(title, resIcon, speed, Constants.MENU.MENU_SETTING);
        data.add(itemMenu);

        speed = 1.5f;
        title = "1.5x";
        resIcon = currentSpeed == speed ? R.drawable.ic_circle_selected : R.drawable.ic_circle_unselected;
        itemMenu = new ItemContextMenu(title, resIcon, speed, Constants.MENU.MENU_SETTING);
        data.add(itemMenu);

        speed = 2.0f;
        title = "2.0x";
        resIcon = currentSpeed == speed ? R.drawable.ic_circle_selected : R.drawable.ic_circle_unselected;
        itemMenu = new ItemContextMenu(title, resIcon, speed, Constants.MENU.MENU_SETTING);
        data.add(itemMenu);

        itemMenu = new ItemContextMenu(activity.getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        data.add(itemMenu);

        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_playback_speedd_video, null, false);
        if (ScreenManager.isLandscape(activity)) {
            try {
                int width = ScreenManager.getWidth(activity);
                int height = ScreenManager.getHeight(activity);
                sheetView.getLayoutParams().width = Math.min(width, height);
                sheetView.requestLayout();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(data);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu itemMenu = (ItemContextMenu) object;
                    if (listener != null)
                        listener.onClickMoreItem(itemMenu.getObj(), itemMenu.getActionTag());
                }
                dialog.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialog.setContentView(sheetView);
        dialog.setOnShowListener(dialogInterface -> {
            final BottomSheetBehavior behavior = dialog.getBottomSheetBehavior();
            if (behavior != null) {
                behavior.setPeekHeight(ScreenManager.getHeight(activity));
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                    }
                });
            }
        });
        dialog.show();
        return dialog;
    }

}

package com.metfone.selfcare.util.contactintergation;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.HashSet;

public class ContactsSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = ContactsSyncAdapter.class.getSimpleName();
    ApplicationController app;

    public ContactsSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        app = ApplicationController.self();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras,
                              String authority, ContentProviderClient provider,
                              SyncResult syncResult) {
        Log.i(TAG, "onPerformSync");

        try {
            fetchAndSaveContacts(provider);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchAndSaveContacts(ContentProviderClient provider) throws RemoteException {
        final Cursor idCursor = provider.query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);

        if (idCursor != null && idCursor.moveToFirst()) {
            // add contact ids and names to the list
            do {
                final String id = idCursor.getString(idCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                final String name = idCursor.getString(idCursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));

                if (!contactExists(id)) {
                    // query all contact numbers corresponding to current id
                    final Cursor phoneCursor = provider
                            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                                    new String[]{id}, null);

                    if (phoneCursor != null && phoneCursor.moveToFirst()) {
                        HashSet<String> listNumb = new HashSet<>();
                        // add contact number to the mNumbers list
                        do {
                            String number = phoneCursor.getString(phoneCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if (!TextUtils.isEmpty(number)) {
                                number = number.replaceAll(" ", "");
                                Log.f(TAG, "fetchAndSaveContacts id: " + id + " name: " + name + " number: " + number);
                                if (!listNumb.contains(number)) {
                                    listNumb.add(number);
                                    addNewRawContact(name, number);
                                }
                            }
                        } while (phoneCursor.moveToNext());
                        phoneCursor.close();
                    }
                }
            } while (idCursor.moveToNext());
            idCursor.close();
        }
    }

    public void fetchAndSaveContacts(ContentResolver provider) throws RemoteException {
        final Cursor idCursor = provider.query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);

        if (idCursor != null && idCursor.moveToFirst()) {
            // add contact ids and names to the list
            do {
                final String id = idCursor.getString(idCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                final String name = idCursor.getString(idCursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));

                if (!contactExists(id)) {
                    // query all contact numbers corresponding to current id
                    final Cursor phoneCursor = provider
                            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                                    new String[]{id}, null);

                    if (phoneCursor != null && phoneCursor.moveToFirst()) {
                        HashSet<String> listNumb = new HashSet<>();
                        // add contact number to the mNumbers list
                        do {
                            String number = phoneCursor.getString(phoneCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if (!TextUtils.isEmpty(number)) {
                                number = number.replaceAll(" ", "");
                                Log.f(TAG, "fetchAndSaveContacts id: " + id + " name: " + name + " number: " + number);
                                if (!listNumb.contains(number)) {
                                    listNumb.add(number);
                                    addNewRawContact(name, number);
                                }
                            }
                        } while (phoneCursor.moveToNext());
                        phoneCursor.close();
                    }
                }
            } while (idCursor.moveToNext());
            idCursor.close();
            Log.i(TAG, "merger contact done ------------------DONE-------------------");
        }
    }

    private boolean contactExists(@NonNull String contactId) {

        final String[] projection = new String[]{ContactsContract.RawContacts._ID};
        final String selection = ContactsContract.RawContacts.CONTACT_ID + " = ? AND " +
                ContactsContract.RawContacts.ACCOUNT_TYPE + " = ?";
        final String[] selectionArgs = new String[]{contactId, Constants.ACCOUNT_TYPE};
        boolean contactExists = false;

        try (Cursor cursor = getContext().getContentResolver().query(
                ContactsContract.RawContacts.CONTENT_URI, projection, selection, selectionArgs, null)) {

            if (cursor != null) {
                contactExists = cursor.getCount() > 0;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return contactExists;
    }

    private void addNewRawContact(String name, String number) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();

        final Context context = getContext();

        // insert account name and account type
        ops.add(ContentProviderOperation
                .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.RawContacts.CONTENT_URI))
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, Constants.ACCOUNT_NAME)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE)
                .withValue(ContactsContract.RawContacts.AGGREGATION_MODE,
                        ContactsContract.RawContacts.AGGREGATION_MODE_DEFAULT)
                .build());

        // insert contact number
        ops.add(ContentProviderOperation
                .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.Data.CONTENT_URI))
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number)
                .build());

        // insert contact name
        ops.add(ContentProviderOperation
                .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.Data.CONTENT_URI))
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                .build());

        // insert mime-type data
        ops.add(ContentProviderOperation
                .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.Data.CONTENT_URI))
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, Constants.MIME_MOCHA_CHAT)
                .withValue(ContactsContract.Data.DATA1, number)
                .withValue(ContactsContract.Data.DATA2, context.getString(R.string.app_name))
                .withValue(ContactsContract.Data.DATA3, "Chat (" + number + ")")
                .build());

        ops.add(ContentProviderOperation
                .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.Data.CONTENT_URI))
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, Constants.MIME_MOCHA_CALL)
                .withValue(ContactsContract.Data.DATA1, number)
                .withValue(ContactsContract.Data.DATA2, context.getString(R.string.app_name))
                .withValue(ContactsContract.Data.DATA3, "Call (" + number + ")")
                .build());

        try {
            context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            Log.i(TAG, "addNewRawContact done: " + name + " " + number);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "addNewRawContact exception ", e);
        }
    }

    public void addNewContact(ContentResolver provider, String id, String name) {
        if (!contactExists(id)) {
            // query all contact numbers corresponding to current id
            final Cursor phoneCursor = provider
                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                            new String[]{id}, null);

            if (phoneCursor != null && phoneCursor.moveToFirst()) {
                HashSet<String> listNumb = new HashSet<>();
                // add contact number to the mNumbers list
                do {
                    String number = phoneCursor.getString(phoneCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    if (!TextUtils.isEmpty(number)) {
                        number = number.replaceAll(" ", "");
                        Log.f(TAG, "addNewContact id: " + id + " name: " + name + " number: " + number);
                        if (!listNumb.contains(number)) {
                            listNumb.add(number);
                            addNewRawContact(name, number);
                        }
                    }
                } while (phoneCursor.moveToNext());
                phoneCursor.close();
            }
        }
    }

    @NonNull
    private Uri addCallerIsSyncAdapterParameter(@NonNull Uri uri) {
        return uri.buildUpon()
                .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true")
                .build();
    }
}

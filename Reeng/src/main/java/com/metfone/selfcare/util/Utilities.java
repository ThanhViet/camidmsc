package com.metfone.selfcare.util;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.location.LocationManagerCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.esport.common.Common;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.WebViewNewActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.common.api.video.callback.OnChannelInfoCallback;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.model.home.AdsMainTab;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.network.restpaser.RestModel;
import com.metfone.selfcare.module.keeng.network.restpaser.RestPlaylist;
import com.metfone.selfcare.module.keeng.utils.DialogLoadData;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.response.NewsContentResponse;
import com.metfone.selfcare.module.saving.model.SavingStatistics;
import com.metfone.selfcare.module.search.activity.SearchAllActivity;
import com.metfone.selfcare.module.spoint.network.model.StatisticalModel;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.TiinContentResponse;
import com.metfone.selfcare.restful.GsonRequest;
import com.metfone.selfcare.restful.StringRequest;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;

import org.json.JSONException;

import java.io.File;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.itangqi.waveloadingview.WaveLoadingView;

@SuppressLint("DefaultLocale")
public class Utilities {
    private static final String TAG = Utilities.class.getSimpleName();
    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    private static final String RESULT_REGEX = "[-|+|:|.|_|+|;|'|$|@||/]";
    private static final String PARAM_APPEND_UUID = "uuid=";
    private static final String PARAM_APPEND_MCUID = "mcuid=";
    private static final String PARAM_APPEND_MCAPP = "mcapp=";

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    public static int dpToPixels(int dp, Resources res) {
        return (int) (res.getDisplayMetrics().density * dp + 0.5f);
    }

    /*
     * Convert string
     */
    public static String convert(String org) {
        // convert to VNese no sign. @haidh 2008
        char[] arrChar = org.toCharArray();
        char[] result = new char[arrChar.length];
        for (int i = 0; i < arrChar.length; i++) {
            switch (arrChar[i]) {
                case '\u00E1':
                case '\u00E0':
                case '\u1EA3':
                case '\u00E3':
                case '\u1EA1':
                case '\u0103':
                case '\u1EAF':
                case '\u1EB1':
                case '\u1EB3':
                case '\u1EB5':
                case '\u1EB7':
                case '\u00E2':
                case '\u1EA5':
                case '\u1EA7':
                case '\u1EA9':
                case '\u1EAB':
                case '\u1EAD':
                case '\u0203':
                case '\u01CE': {
                    result[i] = 'a';
                    break;
                }
                case '\u00E9':
                case '\u00E8':
                case '\u1EBB':
                case '\u1EBD':
                case '\u1EB9':
                case '\u00EA':
                case '\u1EBF':
                case '\u1EC1':
                case '\u1EC3':
                case '\u1EC5':
                case '\u1EC7':
                case '\u0207': {
                    result[i] = 'e';
                    break;
                }
                case '\u00ED':
                case '\u00EC':
                case '\u1EC9':
                case '\u0129':
                case '\u1ECB': {
                    result[i] = 'i';
                    break;
                }
                case '\u00F3':
                case '\u00F2':
                case '\u1ECF':
                case '\u00F5':
                case '\u1ECD':
                case '\u00F4':
                case '\u1ED1':
                case '\u1ED3':
                case '\u1ED5':
                case '\u1ED7':
                case '\u1ED9':
                case '\u01A1':
                case '\u1EDB':
                case '\u1EDD':
                case '\u1EDF':
                case '\u1EE1':
                case '\u1EE3':
                case '\u020F': {
                    result[i] = 'o';
                    break;
                }
                case '\u00FA':
                case '\u00F9':
                case '\u1EE7':
                case '\u0169':
                case '\u1EE5':
                case '\u01B0':
                case '\u1EE9':
                case '\u1EEB':
                case '\u1EED':
                case '\u1EEF':
                case '\u1EF1': {
                    result[i] = 'u';
                    break;
                }
                case '\u00FD':
                case '\u1EF3':
                case '\u1EF7':
                case '\u1EF9':
                case '\u1EF5': {
                    result[i] = 'y';
                    break;
                }
                case '\u0111': {
                    result[i] = 'd';
                    break;
                }
                case '\u00C1':
                case '\u00C0':
                case '\u1EA2':
                case '\u00C3':
                case '\u1EA0':
                case '\u0102':
                case '\u1EAE':
                case '\u1EB0':
                case '\u1EB2':
                case '\u1EB4':
                case '\u1EB6':
                case '\u00C2':
                case '\u1EA4':
                case '\u1EA6':
                case '\u1EA8':
                case '\u1EAA':
                case '\u1EAC':
                case '\u0202':
                case '\u01CD': {
                    result[i] = 'A';
                    break;
                }
                case '\u00C9':
                case '\u00C8':
                case '\u1EBA':
                case '\u1EBC':
                case '\u1EB8':
                case '\u00CA':
                case '\u1EBE':
                case '\u1EC0':
                case '\u1EC2':
                case '\u1EC4':
                case '\u1EC6':
                case '\u0206': {
                    result[i] = 'E';
                    break;
                }
                case '\u00CD':
                case '\u00CC':
                case '\u1EC8':
                case '\u0128':
                case '\u1ECA': {
                    result[i] = 'I';
                    break;
                }
                case '\u00D3':
                case '\u00D2':
                case '\u1ECE':
                case '\u00D5':
                case '\u1ECC':
                case '\u00D4':
                case '\u1ED0':
                case '\u1ED2':
                case '\u1ED4':
                case '\u1ED6':
                case '\u1ED8':
                case '\u01A0':
                case '\u1EDA':
                case '\u1EDC':
                case '\u1EDE':
                case '\u1EE0':
                case '\u1EE2':
                case '\u020E': {
                    result[i] = 'O';
                    break;
                }
                case '\u00DA':
                case '\u00D9':
                case '\u1EE6':
                case '\u0168':
                case '\u1EE4':
                case '\u01AF':
                case '\u1EE8':
                case '\u1EEA':
                case '\u1EEC':
                case '\u1EEE':
                case '\u1EF0': {
                    result[i] = 'U';
                    break;
                }

                case '\u00DD':
                case '\u1EF2':
                case '\u1EF6':
                case '\u1EF8':
                case '\u1EF4': {
                    result[i] = 'Y';
                    break;
                }
                case '\u0110':
                case '\u00D0':
                case '\u0089': {
                    result[i] = 'D';
                    break;
                }
                default:
                    result[i] = arrChar[i];
            }
        }
        return new String(result);
    }

    public static String fixPhoneNumbTo855(String str) {
        if (str == null || "".equals(str) || str.length() < 3)
            return "";
        /*String x = "0123456789";
        for (int i = 0; i < str.length(); i++) {
            if (x.indexOf("" + str.charAt(i)) < 0) {
                str = str.replace("" + str.charAt(i), "");
                i--;
            }
        }*/
        if (str.startsWith("+")) {
            str = str.substring(1);
        } else if (str.startsWith("0855")) {
            str = str.substring(1);
        } else if (str.startsWith("0")) {
            str = "855" + str.substring(1);
        } else if (!str.startsWith("855")) {
            str = "855" + str;
        }

        return str.trim();
    }

    public static boolean isMetPhone(String numberPhone) {
        String numTo855 = fixPhoneNumbTo855(numberPhone);
        return numTo855.contains("85560")
                || numTo855.contains("85590")
                || numTo855.contains("85566")
                || numTo855.contains("85567")
                || numTo855.contains("85568")
                || numTo855.contains("85531")
                || numTo855.contains("85597")
                || numTo855.contains("85588")
                || numTo855.contains("85571");
    }

    public static String fixPhoneNumb(String str) {
        String fixPhoneNumbTo855 = fixPhoneNumbTo855(str);
        if (fixPhoneNumbTo855.length() < 4) {
            return "";
        }
        return fixPhoneNumbTo855.substring(3);
    }

    /**
     * Function to change progress to timer
     *
     * @param progress      -
     * @param totalDuration returns current duration in milliseconds
     */
    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration;
        totalDuration = totalDuration / Constants.ONMEDIA.ONE_SECOND;
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);
        return currentDuration * Constants.ONMEDIA.ONE_SECOND;
    }

    /**
     * Function to convert milliseconds time to Timer Format
     * Hours:Minutes:Seconds
     */
    public static String milliSecondsToTimer(long milliseconds) {
        // Convert total duration into time
        /*int hours = (int) (milliseconds / (Constants.ONMEDIA.ONE_SECOND * 60 * 60));
        int minutes = (int) (milliseconds % (Constants.ONMEDIA.ONE_SECOND * 60 * 60))
                / (Constants.ONMEDIA.ONE_SECOND * 60);
        int seconds = (int) ((milliseconds % (Constants.ONMEDIA.ONE_SECOND * 60 * 60))
                % (Constants.ONMEDIA.ONE_SECOND * 60) / Constants.ONMEDIA.ONE_SECOND);
        StringBuilder sb = new StringBuilder();
        if (hours > 0) {
            sb.append(twoDigit(hours)).append(':');
        }
        sb.append(twoDigit(minutes)).append(':');
        sb.append(twoDigit(seconds));
        return sb.toString();*/
        return secondsToTimer((int) milliseconds / Constants.ONMEDIA.ONE_SECOND);
    }

    public static String secondsToTimer(int allSeconds) {
        // Convert total duration into time
        int hours = (allSeconds / (60 * 60));
        int minutes = (allSeconds % (60 * 60)) / (60);
        int seconds = allSeconds - (hours * 3600 + minutes * 60);
        StringBuilder sb = new StringBuilder();
        if (hours > 0) {
            sb.append(twoDigit(hours)).append(':');
        }
        sb.append(twoDigit(minutes)).append(':');
        sb.append(twoDigit(seconds));
        return sb.toString();
    }

    public static String secondsToTimerText(int allSeconds) {
        int hours = (allSeconds / (60 * 60));
        int minutes = (allSeconds % (60 * 60)) / (60);
        int seconds = allSeconds - (hours * 3600 + minutes * 60);
//        StringBuilder sb = new StringBuilder();
//        if (hours > 0) {
//            sb.append(twoDigit(hours)).append(ApplicationController.self().getString(R.string.hour));
//        }
//        if (minutes > 0) {
//            sb.append(twoDigit(minutes)).append(ApplicationController.self().getString(R.string.minues));
//        }
//        sb.append(twoDigit(seconds)).append(ApplicationController.self().getString(R.string.second));
//        return sb.toString();
        String timeCall = null;
        if (allSeconds < 60 * 60) {
            timeCall = String.format("%02d:%02d", minutes, seconds);
        } else {
            timeCall = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }
        return timeCall;
    }

    static public String twoDigit(int d) {
        NumberFormat formatter = new DecimalFormat("#00");
        return formatter.format(d);
    }

    public static int getProgressPercentage(long currentDuration,
                                            long totalDuration) {
        Double percentage;
        long currentSeconds = (int) (currentDuration / Constants.ONMEDIA.ONE_SECOND);
        long totalSeconds = (int) (totalDuration / Constants.ONMEDIA.ONE_SECOND);
        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;
        // return percentage
        return percentage.intValue();
    }

    private static boolean isPhoneNumberStartWith0(String phoneNumber) {
        String startString = phoneNumber.substring(0, 1);
        return "0".equals(startString);
    }

    private static boolean isPhoneNumberStartWith84(String phoneNumber) {
        String startString = phoneNumber.substring(0, 2);
        Log.i(TAG, "startString: " + startString);
        return "84".equals(startString);
    }

    public static String convertPhoneNumberFromOnMedia(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return "";
        }
        if (isPhoneNumberStartWith0(phoneNumber)) {
            return phoneNumber;
        }
        if (isPhoneNumberStartWith84(phoneNumber)) {
            phoneNumber = phoneNumber.substring(2);
            return "0" + phoneNumber;
        } else {
            return "+" + phoneNumber;
        }
    }

    public static String hidenPhoneNumber(String sdt) {
        if (TextUtils.isEmpty(sdt)) {
            return "";
        }
        int leng = sdt.length();
        StringBuilder mPhone = new StringBuilder(sdt);
        if (leng >= 6) {
//            mPhone.replace(leng - 6, leng - 4, "***");  //ma hoa thanh dang 0984***398
            mPhone.replace(leng - 6, leng, "******");  //ma hoa thanh dang 0984******
        } else if (leng < 6 && leng >= 3) {
            mPhone.replace(leng - 3, leng, "***");
        } else {
            mPhone = new StringBuilder("***");
        }
        return mPhone.toString();
    }

    public static String shortenLongNumber(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here  //cai nay ko can
        if (value == Long.MIN_VALUE) return shortenLongNumber(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + shortenLongNumber(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;


        /*if (value <= 0) return "0";
        if (value < 1000) return Long.toString(value);

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / divideBy;
        double withDecimals = (value * 1.0) / divideBy;
        boolean hasDecimal = (Double.compare(withDecimals, Math.floor(withDecimals)) != 0);
        return !hasDecimal ? truncated + suffix : String.format("%.1f", withDecimals) + suffix;*/
    }

    public static int dpToPixel(int resId, Resources r) {
//        int dp = (int) r.getDimension(resId);

        DisplayMetrics displayMetrics = r.getDisplayMetrics();
        int dp = (int) (r.getDimension(resId) / displayMetrics.density);
        float px = dp * (displayMetrics.densityDpi) / 160.0F;
        return Math.round(px);
    }

    public static int dpToPx(float dp) {
        DisplayMetrics displayMetrics = ApplicationController.self().getResources().getDisplayMetrics();
        float px = dp * (displayMetrics.densityDpi) / 160.0F;
        return Math.round(px);
    }

    public static boolean needGuideShow(long checkShow) {
        if (checkShow == Constants.INTRO.INTRO_NOT_SHOW_YET) {
            return true;
        } else if (checkShow == Constants.INTRO.INTRO_HAS_FINISH_SHOW) {
            return false;
        } else if (checkShow >= 0) {
            long deltaTime = System.currentTimeMillis() - checkShow;
            return deltaTime >= 7 * 24 * 60 * 60 * 1000;
        }
        return false;
    }

    public static String formatUrl(String currentUrl) {
        if (notEmpty(currentUrl)) {
            if (currentUrl.startsWith("Http://")) {
                currentUrl = currentUrl.replaceFirst("Http://", "http://");
            } else if (currentUrl.startsWith("Https://")) {
                currentUrl = currentUrl.replaceFirst("Https://", "https://");
            } else if (!currentUrl.startsWith("http")) {
                currentUrl = "http://" + currentUrl;
            }
            currentUrl = appendParamsForUrl(currentUrl);
        } else {
            currentUrl = "";
        }
        return currentUrl;
    }

    public static String getMyPhoneNumberKeeng(ApplicationController mApp) {
        String jid = mApp.getReengAccountBusiness().getJidNumber();
        if (mApp.getReengAccountBusiness().isVietnam()) {
            jid = jid.substring(1);
            Log.i(TAG, "jid: " + jid);
        }
        return jid;
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        if (TextUtils.isEmpty(packageName)) return false;
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * tính số kích thước của cac tab
     *
     * @param context     context
     * @param screenWidth screenWidth
     * @param noOfTabs    noOfTabs
     * @param scrollable  scrollable
     * @return
     */
    public static int[] getMeasurementsForShiftingMode(Context context, int screenWidth, int noOfTabs, boolean scrollable) {

        int[] result = new int[2];

        int minWidth = (int) context.getResources().getDimension(R.dimen.shifting_min_width_inactive);
        int maxWidth = (int) context.getResources().getDimension(R.dimen.shifting_max_width_inactive);

        double minPossibleWidth = minWidth * (noOfTabs + 0.5);
        double maxPossibleWidth = maxWidth * (noOfTabs + 0.75);
        int itemWidth;
        int itemActiveWidth;

        if (screenWidth < minPossibleWidth) {
            if (scrollable) {
                itemWidth = minWidth;
                itemActiveWidth = (int) (minWidth * 1.5);
            } else {
                itemWidth = (int) (screenWidth / (noOfTabs + 0.5));
                itemActiveWidth = (int) (itemWidth * 1.5);
            }
        } else if (screenWidth > maxPossibleWidth) {
            itemWidth = maxWidth;
            itemActiveWidth = (int) (itemWidth * 1.75);
        } else {
            double minPossibleWidth1 = minWidth * (noOfTabs + 0.625);
            double minPossibleWidth2 = minWidth * (noOfTabs + 0.75);
            itemWidth = (int) (screenWidth / (noOfTabs + 0.5));
            itemActiveWidth = (int) (itemWidth * 1.5);
            if (screenWidth > minPossibleWidth1) {
                itemWidth = (int) (screenWidth / (noOfTabs + 0.625));
                itemActiveWidth = (int) (itemWidth * 1.625);
                if (screenWidth > minPossibleWidth2) {
                    itemWidth = (int) (screenWidth / (noOfTabs + 0.75));
                    itemActiveWidth = (int) (itemWidth * 1.75);
                }
            }
        }

        result[0] = itemWidth;
        result[1] = itemActiveWidth;

        return result;
    }

    /**
     * kiểm tra thiết bị có nằm trong danh sách đặc biệt không
     *
     * @return true or false
     */
    public static boolean isFromWhileList() {
        ApplicationController mApp = ApplicationController.self();
        if (mApp == null) return false;
        String whiteList = mApp.getPref().getString(Constants.PREFERENCE.CONFIG.WHITELIST_DEVICE, Constants.WHITE_LIST);
        if (!TextUtils.isEmpty(whiteList)) {
            whiteList = whiteList.toUpperCase();

            return whiteList.contains(Build.MANUFACTURER.toUpperCase())
                    || whiteList.contains(Build.BRAND.toUpperCase())
                    || whiteList.contains(Build.MODEL.toUpperCase())
                    || whiteList.contains("*ALL*");
        }
        return false;
    }

    public static String convertQuery(String keySearch) {
        if (TextUtils.isEmpty(keySearch))
            keySearch = "";
        else {
            try {
                Pattern pattern = Pattern.compile(RESULT_REGEX);
                Matcher matcher = pattern.matcher(keySearch);
                if (matcher.find()) {
                    keySearch = matcher.replaceAll(" ");
                }
            } catch (Exception e) {
                Log.e(TAG, "convertQuery", e);
            }
        }
        return keySearch.trim();
    }

    public static void openLink(BaseSlidingFragmentActivity activity, String content) {
        ApplicationController application = ApplicationController.self();
        if (application == null || activity == null) return;
        if (!URLUtil.isValidUrl(content)) return;
        if (content.startsWith("http") && (content.contains("keeng.vn") || content.contains("facebook.com"))) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(content));
            activity.startActivity(i);
        } else if (application.getConfigBusiness().isEnableTabVideo()) {
            if (TextHelper.getInstant().isLinkMochaVideo(content)) { //lay dc id thi la mocha video
//                DeepLinkHelper.getInstance().handleSchemaMochaVideoWeb(application, activity, content);
            } else if (TextHelper.getInstant().isLinkMochaChannel(content)) {
                String idChannel = TextHelper.getInstant().getMochaChannelId(content);
                if (TextUtils.isEmpty(idChannel)) {
                    UrlConfigHelper.getInstance(activity).gotoWebViewOnMedia(application, activity, content);
                }
//                else
//                    DeepLinkHelper.getInstance().handleSchemaChannelVideo(application, activity, idChannel);
            } else
                UrlConfigHelper.getInstance(activity).gotoWebViewOnMedia(application, activity, content);
        } else {
            UrlConfigHelper.getInstance(activity).gotoWebViewOnMedia(application, activity, content);
        }
    }

    public static void openApp(BaseSlidingFragmentActivity activity, String packageStr) {
        try {
            Intent intent = activity.getPackageManager().getLaunchIntentForPackage(packageStr);
            if (intent == null) {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=" + packageStr));
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageStr)));
        }
    }

    public static boolean equals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }

    public static <T> T getFistItem(List<T> list) {
        return list.get(0);
    }

    public static <T> T getLastItem(List<T> list) {
        return list.get(list.size() - 1);
    }

    public static <T> boolean notEmpty(List<T> list) {
        return !isEmpty(list);
    }

    public static <T> boolean isEmpty(List<T> list) {
        return list == null || list.isEmpty();
    }

    public static boolean notEmpty(String text) {
        return !isEmpty(text);
    }

    public static boolean isEmpty(String text) {
        return TextUtils.isEmpty(text);
    }

    public static <T> boolean notNull(WeakReference<T> reference) {
        return reference != null && reference.get() != null;
    }

    public static void shareLink(BaseSlidingFragmentActivity activity, String link) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_TEXT, link);
        activity.startActivity(Intent.createChooser(share, activity.getResources().getString(R.string.title_share_video_other)));
    }

    public static void drawChartSaving(Context context, SavingStatistics item, PieChart chart, boolean drawCenterText) {
        if (context != null && item != null && chart != null) {
            //Sử dụng %
            chart.setUsePercentValues(false);
            //ẩn tên của biểu đồ
            chart.getDescription().setEnabled(false);

            //ẩn - hiện label của entry trên vòng tròn biểu đồ: setDrawSliceText hoặc setDrawEntryLabels
            chart.setDrawEntryLabels(false);
            //set bán kính cho hình tròn ở tâm biểu đồ (theo % của cả biểu đồ)
            chart.setHoleRadius(70f);
            //cho phép click lên từng phần của biểu đồ
            chart.setHighlightPerTapEnabled(false);
            //cho phép xoay biểu đồ
            chart.setRotationEnabled(false);
            chart.setTransparentCircleRadius(30f);
            chart.setTransparentCircleColor(Color.GRAY);
            chart.setDrawSlicesUnderHole(false);

            Legend chartLegend = chart.getLegend();
            chartLegend.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
            chartLegend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
            chartLegend.setOrientation(Legend.LegendOrientation.VERTICAL);
            chartLegend.setDrawInside(false);
            chartLegend.setEnabled(false);

            ArrayList<PieEntry> entries = new ArrayList<>();
            ArrayList<Integer> colors = new ArrayList<>();
            long totalSaving = item.getTotalSavingInt();
            if (totalSaving > 0) {
                if (drawCenterText) {
                    //cho phép vẽ text vào chính giữa biểu đồ
                    chart.setDrawCenterText(true);
                    //set text ở giữa biểu đồ
                    if (!TextUtils.isEmpty(item.getTotalSaving())) {
                        SpannableString s = new SpannableString(item.getTotalSaving());
                        s.setSpan(new RelativeSizeSpan(1.0f), 0, s.length(), 0);
                        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
                        s.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.bg_mocha)), 0, s.length(), 0);
                        chart.setCenterText(s);
                    }
                } else {
                    chart.setDrawCenterText(false);
                }
                if (item.getSmsOutSavingInt() > 0) {
//                    PieEntry pieEntry = new PieEntry(item.getSmsOutSavingInt() * 100 / totalSaving, "");
                    PieEntry pieEntry = new PieEntry(item.getSmsOutSavingInt(), "");
                    entries.add(pieEntry);
                    colors.add(Color.rgb(102, 51, 204)); // màu sms out
                }
                if (item.getCallOutSavingInt() > 0) {
//                    PieEntry pieEntry = new PieEntry(item.getCallOutSavingInt() * 100 / totalSaving, "");
                    PieEntry pieEntry = new PieEntry(item.getCallOutSavingInt(), "");
                    entries.add(pieEntry);
                    colors.add(Color.rgb(255, 102, 51)); //màu call out
                }
                if (item.getDataSavingInt() > 0) {
//                    PieEntry pieEntry = new PieEntry(item.getDataSavingInt() * 100 / totalSaving, "");
                    PieEntry pieEntry = new PieEntry(item.getDataSavingInt(), "");
                    entries.add(pieEntry);
                    colors.add(Color.rgb(117, 214, 113)); //màu data
                }
            }
            PieDataSet dataSet = new PieDataSet(entries, "");
            dataSet.setColors(colors);
            //ẩn - hiện giá trị % trên vòng tròn biểu đồ
            dataSet.setDrawValues(false);
            PieData data = new PieData(dataSet);
            data.setHighlightEnabled(false);
            chart.setData(data);
            // undo all highlights
            chart.highlightValues(null);
            chart.invalidate();
            if (item.isShowWithAnimation()) {
                item.setShowWithAnimation(false);
                chart.animateXY(1400, 1400);
            }
        }
    }

    public static void drawChartSpoint(Context context, StatisticalModel item, PieChart chart, boolean drawCenterText) {
        if (context != null && item != null && chart != null) {
            //Sử dụng %
            chart.setUsePercentValues(false);
            //ẩn tên của biểu đồ
            chart.getDescription().setEnabled(false);

            //ẩn - hiện label của entry trên vòng tròn biểu đồ: setDrawSliceText hoặc setDrawEntryLabels
            chart.setDrawEntryLabels(false);
            //set bán kính cho hình tròn ở tâm biểu đồ (theo % của cả biểu đồ)
            chart.setHoleRadius(90f);
            //cho phép click lên từng phần của biểu đồ
            chart.setHighlightPerTapEnabled(false);
            //cho phép xoay biểu đồ
            chart.setRotationEnabled(false);
            chart.setTransparentCircleRadius(20f);
            chart.setTransparentCircleColor(Color.GRAY);
            chart.setDrawSlicesUnderHole(false);

            Legend chartLegend = chart.getLegend();
            chartLegend.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
            chartLegend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
            chartLegend.setOrientation(Legend.LegendOrientation.VERTICAL);
            chartLegend.setDrawInside(false);
            chartLegend.setEnabled(false);

            ArrayList<PieEntry> entries = new ArrayList<>();
            ArrayList<Integer> colors = new ArrayList<>();
            long totalSaving = item.getTotalSpointInt();
            if (totalSaving > 0) {
                if (drawCenterText) {
                    //cho phép vẽ text vào chính giữa biểu đồ
                    chart.setDrawCenterText(true);
                    //set text ở giữa biểu đồ
                    if (!TextUtils.isEmpty(item.getTotalSpointString())) {
                        SpannableString s = new SpannableString(item.getTotalSpointString());
                        s.setSpan(new RelativeSizeSpan(1.0f), 0, s.length(), 0);
                        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
//                        s.setSpan(new AbsoluteSizeSpan(16, true), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        s.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.v5_text)), 0, s.length(), 0);
                        chart.setCenterTextSize(16f);
                        chart.setCenterText(s);

                    }
                } else {
                    chart.setDrawCenterText(false);
                }
                if (item.getSpoint() > 0) {
//                    PieEntry pieEntry = new PieEntry(item.getSmsOutSavingInt() * 100 / totalSaving, "");
                    PieEntry pieEntry = new PieEntry(item.getSpoint(), "");
                    entries.add(pieEntry);
                    colors.add(Color.rgb(102, 51, 204)); // màu sms out
                }
                if (item.getSpointPromotion() > 0) {
//                    PieEntry pieEntry = new PieEntry(item.getCallOutSavingInt() * 100 / totalSaving, "");
                    PieEntry pieEntry = new PieEntry(item.getSpointPromotion(), "");
                    entries.add(pieEntry);
                    colors.add(Color.rgb(255, 102, 51)); //màu call out
                }
            }
            PieDataSet dataSet = new PieDataSet(entries, "");
            dataSet.setColors(colors);
            //ẩn - hiện giá trị % trên vòng tròn biểu đồ
            dataSet.setDrawValues(false);
            PieData data = new PieData(dataSet);
            data.setHighlightEnabled(false);
            chart.setData(data);
            // undo all highlights
            chart.highlightValues(null);
            chart.invalidate();
//            if (item.isShowWithAnimation()) {
//                item.setShowWithAnimation(false);
//                chart.animateXY(1400, 1400);
//            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<String> getAppSignatures(Context context) {
        ArrayList<String> appCodes = new ArrayList<>();

        try {
            // Get all package signatures for the current package
            String packageName = context.getPackageName();
            PackageManager packageManager = context.getPackageManager();
            Signature[] signatures = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;

            // For each signature create a compatible hash
            for (Signature signature : signatures) {
                String hash = getKeyHash(context, packageName, signature.toCharsString());
                if (hash != null) appCodes.add(String.format("%s", hash));

            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Unable to find package to obtain hash.", e);
        }
        return appCodes;
    }

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static String getKeyHash(Context context, String packageName, String signature) {
        String appInfo = packageName + " " + signature;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(appInfo.getBytes(StandardCharsets.UTF_8));
            byte[] hashSignature = messageDigest.digest();

            // truncated into NUM_HASHED_BYTES
            hashSignature = Arrays.copyOfRange(hashSignature, 0, 9);

            // encode into Base64
            String base64Hash = Base64.encodeToString(hashSignature, Base64.NO_PADDING | Base64.NO_WRAP);
            base64Hash = base64Hash.substring(0, 11);

            Log.e(TAG, String.format("pkg: %s -- hash: %s", packageName, base64Hash));
            ToastUtils.makeText(context, "Key hash: " + base64Hash);
            return base64Hash;

        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "hash:NoSuchAlgorithm", e);
        }
        return null;
    }

    public static String getTotalView(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here  //cai nay ko can
        if (value == Long.MIN_VALUE) return shortenLongNumber(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + shortenLongNumber(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();
        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public static void addDefaultParamsRequestVolley(Request request) {
        try {
            Map<String, String> body = null;
            Map<String, String> header = null;
            if (request instanceof StringRequest) {
                try {
                    StringRequest stringRequest = (StringRequest) request;
                    body = stringRequest.getParams();
                    header = stringRequest.getHeaders();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (request instanceof GsonRequest) {
                try {
                    GsonRequest gsonRequest = (GsonRequest) request;
                    body = gsonRequest.getParams();
                    header = gsonRequest.getHeaders();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            addDefaultParamsRequest(body);
            addDefaultHeadersRequest(header);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addDefaultParamsRequest(Map<String, String> body) {
        try {
            if (body != null) {
                if (body.get("clientType") == null) {
                    Log.i(TAG, "body không có clientType nên bổ sung clientType: " + Constants.HTTP.CLIENT_TYPE_STRING);
                    body.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                } else {
                    Log.i(TAG, "body đã có clientType");
                }
                if (body.get("revision") == null) {
                    Log.i(TAG, "body không có revision nên bổ sung revision: " + Config.REVISION);
                    body.put("revision", Config.REVISION);
                } else {
                    Log.i(TAG, "body đã có revision");
                }
                if (body.get("Platform") == null) {
                    Log.i(TAG, "body không có Platform nên bổ sung Platform: " + Constants.HTTP.PLATFORM);
                    body.put("Platform", Constants.HTTP.PLATFORM);
                } else {
                    Log.i(TAG, "body đã có Platform");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addDefaultHeadersRequest(Map<String, String> header) {
        try {
            if (header != null) {
                if (header.get("Media-IP") == null) {
                    String ipAddress = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LOCATION_CLIENT_IP, String.class);
                    Log.i(TAG, "header không có Media-IP nên bổ sung ipAddress: " + ipAddress);
                    if (notEmpty(ipAddress))
                        header.put("Media-IP", ipAddress);
                } else {
                    Log.i(TAG, "header đã có Media-IP");
                }

                if (header.get("Media-CC") == null) {
                    String countryCode = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LOCATION_CLIENT_COUNTRY_CODE, String.class);
                    Log.i(TAG, "header không có Media-CC nên bổ sung countryCode: " + countryCode);
                    if (notEmpty(countryCode))
                        header.put("Media-CC", countryCode);
                } else {
                    Log.i(TAG, "header đã có Media-CC");
                }
                if (header.get("uuid") == null) {
                    String uuid = getUuidApp();
                    Log.i(TAG, "header không có uuid nên bổ sung uuid: " + uuid);
                    if (notEmpty(uuid))
                        header.put("uuid", uuid);
                } else {
                    Log.i(TAG, "header đã có uuid");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showAdsMainTab(final ApplicationController application
            , final BaseSlidingFragmentActivity activity, final String currentTab, String tabId) {
        if (application == null || activity == null) return;
        long lastTime = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LAST_TIME_SHOW_ADS_MAIN_TAB, Long.class);
        if (TimeHelper.checkTimeInDay(lastTime)) {
            return;
        } else {
            SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_INDEX_SHOW_ADS_MAIN_TAB);
            application.setShowedAdsMainTab(false);
        }
        if (!application.isShowedAdsMainTab()) {
            String indexTmp = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_INDEX_SHOW_ADS_MAIN_TAB, String.class);
            boolean canShowAds = Utilities.isEmpty(indexTmp);
            if (canShowAds) {
                ArrayList<AdsMainTab> list = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LIST_ADS_MAIN_TAB, new TypeToken<ArrayList<AdsMainTab>>() {
                }.getType());
                if (Utilities.notEmpty(list)) {
                    AdsMainTab item = null;
                    for (AdsMainTab tmp : list) {
                        if (tmp != null && tmp.isEnable() && currentTab.equals(tmp.getTabType())
                                && (!Constants.MAIN_TAB_WAP.equalsIgnoreCase(currentTab)
                                || (Constants.MAIN_TAB_WAP.equalsIgnoreCase(currentTab) && notEmpty(tabId) && tabId.equals(tmp.getTabId())))) {
                            item = tmp;
                            break;
                        }
                    }
                    if (item != null) {
                        application.setShowedAdsMainTab(true);
                        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LAST_TIME_SHOW_ADS_MAIN_TAB, System.currentTimeMillis());
                        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_INDEX_SHOW_ADS_MAIN_TAB, indexTmp + currentTab + "|");
                        ReportHelper.showPopupOnMainTab(application, activity, item, currentTab);
                    }
                }
            }
        }
    }

    private static void gotoSearchAll(BaseSlidingFragmentActivity activity, int tabId) {
        if (activity == null) return;
        Intent intent = new Intent(activity, SearchAllActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SEARCH_ALL);
        intent.putExtra(Constants.KEY_POSITION, tabId);
        activity.startActivity(intent);
    }

    public static void openSearch(BaseSlidingFragmentActivity activity, int tabId) {
        if (activity == null) return;
        gotoSearchAll(activity, tabId);
    }

    /**
     * @return : true nếu có xử lý
     * false nếu không xử lý gì
     **/
    public static boolean processOpenLink(final ApplicationController application
            , final BaseSlidingFragmentActivity activity, String link) {
        Log.i(TAG, "processOpenLink: " + link);
        if (application != null && activity != null && !activity.isFinishing() && notEmpty(link)) {
            final String url = link.trim();
            if (TextHelper.getInstant().isLinkMusicSong(url) && application.getConfigBusiness().isEnableTabMusic()) {
                String itemId = TextHelper.getInstant().getIdentifyOfMusicSong(url);
                if (notEmpty(itemId)) {
                    final DialogLoadData dialog = new DialogLoadData(activity, false);
                    if (!dialog.isShowing())
                        dialog.show();
                    new KeengApi().getSong(0, itemId, new ApiCallbackV2<RestModel>() {
                        @Override
                        public void onSuccess(String msg, RestModel result) throws JSONException {
                            if (dialog != null) dialog.dismiss();
                            if (result != null && result.getData() != null) {
                                activity.playMusicSong(result.getData(), true);
                            } else {
                                openWebView(application, activity, url);
                            }
                        }

                        @Override
                        public void onError(String s) {
                            if (dialog != null) dialog.dismiss();
                            openWebView(application, activity, url);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
                    return true;
                }
            } else if (TextHelper.getInstant().isLinkMusicAlbum(url) && application.getConfigBusiness().isEnableTabMusic()) {
                String itemId = TextHelper.getInstant().getIdentifyOfMusicAlbum(url);
                if (notEmpty(itemId)) {
                    final DialogLoadData dialog = new DialogLoadData(activity, false);
                    if (!dialog.isShowing())
                        dialog.show();
                    new KeengApi().getAlbum(0, itemId, new ApiCallbackV2<RestModel>() {
                        @Override
                        public void onSuccess(String msg, RestModel result) throws JSONException {
                            if (dialog != null) dialog.dismiss();
                            if (result != null && result.getData() != null) {
                                activity.gotoAlbumDetail(result.getData());
                            } else {
                                openWebView(application, activity, url);
                            }
                        }

                        @Override
                        public void onError(String s) {
                            if (dialog != null) dialog.dismiss();
                            openWebView(application, activity, url);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
                    return true;
                }
            } else if (TextHelper.getInstant().isLinkMusicVideo(url) && application.getConfigBusiness().isEnableTabMusic()) {
                String itemId = TextHelper.getInstant().getIdentifyOfMusicVideo(url);
                if (notEmpty(itemId)) {
                    final DialogLoadData dialog = new DialogLoadData(activity, false);
                    if (!dialog.isShowing())
                        dialog.show();
                    new KeengApi().getVideo(0, itemId, new ApiCallbackV2<RestModel>() {
                        @Override
                        public void onSuccess(String msg, RestModel result) throws JSONException {
                            if (dialog != null) dialog.dismiss();
                            if (result != null && result.getData() != null) {
                                activity.setMediaToPlayVideo(result.getData());
                            } else {
                                openWebView(application, activity, url);
                            }
                        }

                        @Override
                        public void onError(String s) {
                            if (dialog != null) dialog.dismiss();
                            openWebView(application, activity, url);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
                    return true;
                }
            } else if (TextHelper.getInstant().isLinkMusicPlaylist(url) && application.getConfigBusiness().isEnableTabMusic()) {
                String itemId = TextHelper.getInstant().getIdentifyOfMusicPlaylist(url);
                if (notEmpty(itemId)) {
                    final DialogLoadData dialog = new DialogLoadData(activity, false);
                    if (!dialog.isShowing())
                        dialog.show();
                    new KeengApi().getPlaylist(0, itemId, new ApiCallbackV2<RestPlaylist>() {
                        @Override
                        public void onSuccess(String msg, RestPlaylist result) throws JSONException {
                            if (dialog != null) dialog.dismiss();
                            if (result != null && result.getData() != null) {
                                activity.gotoPlaylistDetail(result.getData());
                            } else {
                                openWebView(application, activity, url);
                            }
                        }

                        @Override
                        public void onError(String s) {
                            if (dialog != null) dialog.dismiss();
                            openWebView(application, activity, url);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
                    return true;
                }
            } else if (TextHelper.getInstant().isLinkMoviesDetail(url) && application.getConfigBusiness().isEnableTabMovie()) {
                String itemId = TextHelper.getInstant().getMoviesId(url);
                if (notEmpty(itemId)) {
                    final DialogLoadData dialog = new DialogLoadData(activity, false);
                    if (!dialog.isShowing())
                        dialog.show();
                    new MovieApi().getMovieDetail(itemId, false, new ApiCallbackV2<Movie>() {

                        @Override
                        public void onSuccess(String msg, Movie result) throws JSONException {
                            if (dialog != null) dialog.dismiss();
                            if (result != null) {
                                VideoPlayerActivity.start(activity, result, "", false);
                                //activity.playMovies(result);
                            } else {
                                openWebView(application, activity, url);
                            }
                        }

                        @Override
                        public void onError(String s) {
                            if (dialog != null) dialog.dismiss();
                            openWebView(application, activity, url);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
                    return true;
                }
            } else if (TextHelper.getInstant().isLinkMochaVideo(url) && application.getConfigBusiness().isEnableTabVideo()) {
                final DialogLoadData dialog = new DialogLoadData(activity, false);
                if (!dialog.isShowing())
                    dialog.show();
                application.getApplicationComponent().providerVideoApi().getInfoVideoFromUrl(url, new ApiCallbackV2<Video>() {
                    @Override
                    public void onSuccess(String lastId, Video video) throws JSONException {
                        if (dialog != null) dialog.dismiss();
                        application.getApplicationComponent().providesUtils().openVideoDetail(activity, video);
                    }

                    @Override
                    public void onError(String s) {
                        if (dialog != null) dialog.dismiss();
                        openWebView(application, activity, url);
                    }

                    @Override
                    public void onComplete() {

                    }

                });
                return true;
            } else if (TextHelper.getInstant().isLinkMochaChannel(url) && application.getConfigBusiness().isEnableTabVideo()) {
                String itemId = TextHelper.getInstant().getMochaChannelId(url);
                if (notEmpty(itemId)) {
                    final DialogLoadData dialog = new DialogLoadData(activity, false);
                    if (!dialog.isShowing())
                        dialog.show();
                    application.getApplicationComponent().providerChannelApi().getChannelInfo(itemId, new OnChannelInfoCallback() {
                        @Override
                        public void onGetChannelInfoSuccess(Channel channel) {
                            if (dialog != null) dialog.dismiss();
                            if (channel != null) {
                                application.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
                            } else {
                                openWebView(application, activity, url);
                            }
                        }

                        @Override
                        public void onGetChannelInfoError(String s) {
                            if (dialog != null) dialog.dismiss();
                            openWebView(application, activity, url);
                        }

                        @Override
                        public void onGetChannelInfoComplete() {

                        }
                    });
                    return true;
                }
            } else if (TextHelper.getInstant().isLinkNetNewsDetail(url) && application.getConfigBusiness().isEnableTabNews()) {
                final DialogLoadData dialog = new DialogLoadData(activity, false);
                if (!dialog.isShowing())
                    dialog.show();
                new NetNewsApi(application).getNewsDetail(url, new ApiCallbackV2<NewsContentResponse>() {

                    @Override
                    public void onSuccess(String msg, NewsContentResponse result) throws JSONException {
                        if (dialog != null) dialog.dismiss();
                        if (result != null && result.getData() != null) {
                            CommonUtils.readDetailNews(activity, result.getData(), true);
                        } else {
                            openWebView(application, activity, url);
                        }
                    }

                    @Override
                    public void onError(String s) {
                        if (dialog != null) dialog.dismiss();
                        openWebView(application, activity, url);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
                return true;
            } else if (TextHelper.getInstant().isLinkTiinDetail(url) && application.getConfigBusiness().isEnableTabTiin()) {
                final DialogLoadData dialog = new DialogLoadData(activity, false);
                if (!dialog.isShowing())
                    dialog.show();
                new TiinApi(application).getDetailByUrl(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        if (dialog != null) dialog.dismiss();
                        Gson gson = new Gson();
                        TiinContentResponse result = gson.fromJson(data, TiinContentResponse.class);
                        if (result != null && result.getData() != null) {
                            CommonUtils.readDetailTiin(activity, result.getData(), true);
                        } else {
                            openWebView(application, activity, url);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (dialog != null) dialog.dismiss();
                        openWebView(application, activity, url);
                    }
                }, url);
                return true;
            }

            if (URLUtil.isValidUrl(url)) {
                openWebView(application, activity, url);
                return true;
            }
        }
        return false;
    }

    public static void openWebView(ApplicationController application, BaseSlidingFragmentActivity activity, String url) {
        if (application != null && activity != null && notEmpty(url)) {
            UrlConfigHelper.gotoWebViewOnMedia(application, activity, url);
        }
    }

    public static void logClickNews(NewsModel newsModel) {
        new WSOnMedia(ApplicationController.self()).logClickLink(newsModel.getUrl(), Constants.LOG_SOURCE_TYPE.TYPE_NEWS, AppStateProvider.getInstance().getCampId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "callApiLogView response: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
            }
        });
    }

    public static void logClickPush(String url, String campid) {
        new WSOnMedia(ApplicationController.self()).logClickLink(url, Constants.LOG_SOURCE_TYPE.TYPE_PUSH, campid, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "callApiLogView response: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
            }
        });
    }

    public static String getChannelIdMusicPlayer(Context context) {
        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = ApplicationController.self().getChannelIdFromCache(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
            //TODO voi target SDK 27 RemoteServiceException: Bad notification for startForeground: RuntimeException: invalid channel for service notification
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(channelId);
            if (notificationChannel != null)
                notificationManager.deleteNotificationChannel(channelId);
            notificationChannel = new NotificationChannel(channelId, context.getString(R.string.app_name)
                    , NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setLightColor(Color.WHITE);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.deleteNotificationChannel(channelId);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        return channelId;
    }

    public static int getWidthVideoEpisode() {
        return (int) ((ApplicationController.self().getHeightPixels() - ApplicationController.self().getRound() * 3) / 3.5);
    }

    public static void setSizeFrameVideo(Activity activity, FrameLayout frController, double aspectRatio) {
        if (activity == null || frController == null) return;
        try {
            if (Double.isNaN(aspectRatio))
                aspectRatio = 16 / 9f;
            ViewGroup.LayoutParams layoutParams = frController.getLayoutParams();
            int width = ScreenManager.getWidth(activity);
            int height = ScreenManager.getHeight(activity);
            if (height < width)
                width = height;
            layoutParams.width = width;
            layoutParams.height = (int) (width / aspectRatio);
            frController.setLayoutParams(layoutParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getUuidConfig() {
        String uuid = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_UUID_CONFIG, String.class);
        Log.i(TAG, "getUuidConfig uuid: " + uuid);
        if (uuid == null) uuid = "";
        return uuid;
    }

    public static String getUuidApp() {
        String uuid = null;
        try {
            uuid = Settings.Secure.getString(ApplicationController.self().getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (SecurityException e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
        }
        //Log.i(TAG, "getUuidApp uuid: " + uuid);
        if (uuid == null) uuid = "";
        return uuid;
    }

    private static String appendParamsForUrl(String url) {
        if (notEmpty(url)) {
            // uuid is param get data from API getConfig
            String value = Utilities.getUuidConfig();
            if (notEmpty(value)) {
                try {
                    if (!url.contains(PARAM_APPEND_UUID)) {
                        if (url.contains("?")) {
                            url += "&" + PARAM_APPEND_UUID + Uri.encode(value);
                        } else {
                            url += "?" + PARAM_APPEND_UUID + Uri.encode(value);
                        }
                    }
                } catch (Exception e) {
                }
            }

            // mcuid is MD5 of msisdn
            value = EncryptUtil.md5(ApplicationController.self().getReengAccountBusiness().getJidNumber());
            if (notEmpty(value)) {
                try {
                    if (!url.contains(PARAM_APPEND_MCUID)) {
                        if (url.contains("?")) {
                            url += "&" + PARAM_APPEND_MCUID + Uri.encode(value);
                        } else {
                            url += "?" + PARAM_APPEND_MCUID + Uri.encode(value);
                        }
                    }
                } catch (Exception e) {
                }
            }

            // mcapp is source from Mocha
            value = EncryptUtil.md5(ApplicationController.self().getReengAccountBusiness().getJidNumber());
            if (notEmpty(value)) {
                try {
                    if (!url.contains(PARAM_APPEND_MCAPP)) {
                        if (url.contains("?")) {
                            url += "&" + PARAM_APPEND_MCAPP + "mocha";
                        } else {
                            url += "?" + PARAM_APPEND_MCAPP + "mocha";
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
        return url;
    }

    public static boolean processOnShouldOverrideUrlLoading(BaseSlidingFragmentActivity activity
            , WebView webView, String url) {
        if (activity == null || webView == null || isEmpty(url)) return false;
        Log.d(TAG, "processOnShouldOverrideUrlLoading url: " + url);
        if (url.startsWith("market://")
                || url.startsWith("vnd:youtube")
                || url.startsWith("tel:")
                || url.startsWith("mailto:")
                || url.startsWith("https://www.facebook.com")
                || url.startsWith("https://m.facebook.com")
                || url.startsWith("sms:")
                || url.startsWith("smsto:")
                || url.startsWith("mms:")
                || url.startsWith("mmsto:")) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else if (url.startsWith("intent://")) {
            try {
                Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                if (intent != null) {
                    webView.stopLoading();
                    PackageManager packageManager = activity.getPackageManager();
                    ResolveInfo info = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
                    if (info != null) {
                        activity.startActivity(intent);
                    } else {
                        String fallbackUrl = intent.getStringExtra("http://mocha.com.vn");
                        Log.i(TAG, "WebView.loadUrl: " + fallbackUrl);
                        webView.loadUrl(fallbackUrl);
                    }
                    return true;
                } else return false;
            } catch (Exception e) {
                return false;
            }
        } else if (url.startsWith("mocha://")) {
            try {
                DeepLinkHelper.getInstance().openSchemaLink(activity, url);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    public static String getTextCallInfor(Resources res, int callMode) {
        if (callMode == ReengMessageConstant.MODE_GSM) {
            return res.getString(R.string.title_mocha_callout);
        } else if (callMode == ReengMessageConstant.MODE_VIDEO_CALL) {
            return res.getString(R.string.call_state_mocha_video);
        } else {
            return res.getString(R.string.call_state_mocha);
        }
    }

    public static String getTextCallInfor(Resources res, int direction, int callMode,
                                          int callState) {
        ReengMessageConstant.Direction directionEnum;
        if (direction == CallHistoryConstant.DIRECTION_SEND) {
            directionEnum = ReengMessageConstant.Direction.send;
        } else {
            directionEnum = ReengMessageConstant.Direction.received;

        }
        return getTextCallInfor(res, directionEnum, callMode, callState);
    }

    public static int getIconCallInfor(int direction, int callState) {
        if (callState == CallHistoryConstant.STATE_MISS) {
            return R.drawable.ic_call_in;
        }
        if (direction == CallHistoryConstant.DIRECTION_SEND) {
            return R.drawable.ic_call_out;
        }
        return R.drawable.ic_call_in;
    }

    public static String getTextCallInfor(Resources res, ReengMessageConstant.Direction direction, int callMode,
                                          int callState) {
        String stateStr;
        if (direction == ReengMessageConstant.Direction.send) {
            if (callMode == ReengMessageConstant.MODE_GSM) {
                stateStr = res.getString(R.string.call_state_mocha);
            } else if (callMode == ReengMessageConstant.MODE_VIDEO_CALL) {
                stateStr = res.getString(R.string.call_state_mocha);
            } else {
                stateStr = res.getString(R.string.call_state_mocha);
            }
            // content color
            if (callState == CallHistoryConstant.STATE_MISS) {
//            mTvwContent.setText(mContext.getResources().getString(R.string.call_state_miss_call));
                stateStr = res.getString(R.string.call_state_mocha);
            } else {
                if (callState == CallHistoryConstant.STATE_OUT_GOING) {
                } else if (callState == CallHistoryConstant.STATE_BUSY) {
                    stateStr = res.getString(R.string.call_state_mocha);
                } else if (callState == CallHistoryConstant.STATE_CANCELLED) {
                    stateStr = res.getString(R.string.call_state_mocha);
                } else if (callState == CallHistoryConstant.STATE_REJECTED) {
                    int id = callMode == ReengMessageConstant.MODE_GSM ? R.string.call_state_mocha : R.string.call_state_mocha;
                    stateStr = res.getString(id);
                }
            }
        } else { // call receive
            if (callMode == ReengMessageConstant.MODE_GSM) {
                stateStr = res.getString(R.string.call_state_mocha); // receive no have callout :)))
            } else if (callMode == ReengMessageConstant.MODE_VIDEO_CALL) {
                stateStr = res.getString(R.string.call_state_mocha);
            } else {

                stateStr = res.getString(R.string.call_state_mocha);
            }
            // content color
            if (callState == CallHistoryConstant.STATE_MISS) {
                stateStr = res.getString(R.string.call_state_mocha);
            } else {
                if (callState == CallHistoryConstant.STATE_IN_COMING) {
//                    mTvwContent.setText(stateStr);
//                    mTvwDuration.setText(message.getContent());
                } else if (callState == CallHistoryConstant.STATE_BUSY) {
                    stateStr = res.getString(R.string.call_state_mocha); // receive no have call busy
                } else if (callState == CallHistoryConstant.STATE_CANCELLED) {
                    stateStr = res.getString(R.string.call_state_mocha);// receive no have call CANCELLED
                } else if (callState == CallHistoryConstant.STATE_REJECTED) {
                    stateStr = res.getString(R.string.call_state_mocha);
                } else {
//                    mTvwContent.setText(stateStr);
                }
            }
        }
        return stateStr;
    }

    public static void gotoWebView(ApplicationController application, BaseSlidingFragmentActivity activity
            , String url, String title, String orientation, String fullscreen, String onmedia
            , String close, String expand) {
        if (application == null || activity == null || activity.isFinishing()) return;
        if (TextUtils.isEmpty(url)) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else {
            Intent intent = new Intent(application, WebViewNewActivity.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_TITLE, title);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_ORIENTATION, orientation);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_FULLSCREEN, "1".equals(fullscreen));
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_ON_MEDIA, "1".equals(onmedia));
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_CLOSE, close);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_EXPAND, expand);
            activity.startActivity(intent, true);
        }
    }

    public static void gotoWebViewFullScreen(ApplicationController application, BaseSlidingFragmentActivity activity
            , String url, String orientation, String close, String expand, boolean confirmBack) {
        if (application == null || activity == null || activity.isFinishing()) return;
        if (TextUtils.isEmpty(url)) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else {
            Intent intent = new Intent(application, WebViewNewActivity.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_FULLSCREEN, true);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_ORIENTATION, orientation);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_CLOSE, close);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_EXPAND, expand);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_CONFIRM_BACK, confirmBack);
            activity.startActivity(intent, true);
        }
    }

    public static String getOriginalUrl(String url) {
        if (notEmpty(url)) {

            //remove append uuid
            String value = Utilities.getUuidConfig();
            if (notEmpty(value)) {
                try {
                    value = PARAM_APPEND_UUID + Uri.encode(value);
                    if (url.contains(value)) {
                        String tmp = "?" + value;
                        if (url.endsWith(tmp)) url = url.replace(tmp, "");
                        tmp = "&" + value;
                        if (url.endsWith(tmp)) url = url.replace(tmp, "");
                        tmp = "?" + value + "&";
                        if (url.contains(tmp)) url = url.replace(tmp, "?");
                        tmp = "&" + value + "&";
                        if (url.contains(tmp)) url = url.replace(tmp, "&");
                    }
                } catch (Exception e) {
                }
            }

            //remove append mcuid
            value = EncryptUtil.md5(ApplicationController.self().getReengAccountBusiness().getJidNumber());
            if (notEmpty(value)) {
                try {
                    value = PARAM_APPEND_MCUID + Uri.encode(value);
                    if (url.contains(value)) {
                        String tmp = "?" + value;
                        if (url.endsWith(tmp)) url = url.replace(tmp, "");
                        tmp = "&" + value;
                        if (url.endsWith(tmp)) url = url.replace(tmp, "");
                        tmp = "?" + value + "&";
                        if (url.contains(tmp)) url = url.replace(tmp, "?");
                        tmp = "&" + value + "&";
                        if (url.contains(tmp)) url = url.replace(tmp, "&");
                    }
                } catch (Exception e) {
                }
            }

            //remove append mcapp
            try {
                value = PARAM_APPEND_MCAPP + "mocha";
                if (url.contains(value)) {
                    String tmp = "?" + value;
                    if (url.endsWith(tmp)) url = url.replace(tmp, "");
                    tmp = "&" + value;
                    if (url.endsWith(tmp)) url = url.replace(tmp, "");
                    tmp = "?" + value + "&";
                    if (url.contains(tmp)) url = url.replace(tmp, "?");
                    tmp = "&" + value + "&";
                    if (url.contains(tmp)) url = url.replace(tmp, "&");
                }
            } catch (Exception e) {
            }

        } else {
            url = "";
        }
        return url;
    }

    public static void setMargins(View view, int marginLeft, int marginTop, int marginRight, int marginBottom) {
        if (view != null && view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(marginLeft, marginTop, marginRight, marginBottom);
            view.requestLayout();
        }
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
//        bm.recycle();
        return resizedBitmap;
    }

    public static boolean isNotShowActivityCall(Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
                && !Settings.canDrawOverlays(context);
    }

    public static boolean isNeedShowHeadUpNotification(ApplicationController applicationController) {
        return isNotShowActivityCall(applicationController.getApplicationContext())
                && (applicationController.getAppStateManager().isAppWentToBg() || applicationController.getAppStateManager().isScreenLocker());
    }

    // dainv
    // header of retrofit http can not using  non-ASCII characters
    // return "true", it not have non-ASCII characters
    // return "false", path is null, false
    //else return path of file copy
    public static String checkAndGetNewPath(String path, Context context, boolean isCopyFile) {
        if (path == null) {
            return "false";
        }
//        String nameFile = path.lastIndexOf()
//        \/:*?”<>|
        Pattern specialPattern = Pattern.compile("[\\:*?”<>|]");
        boolean isInValidPath = false;
        if (specialPattern.matcher(path).find()) {
            isInValidPath = true;
        }
        if (!isInValidPath) {
            for (int i = 0, length = path.length(); i < length; i++) {
                char c = path.charAt(i);
                if ((c <= '\u001f' && c != '\t') || c >= '\u007f') {
                    isInValidPath = true;
                    break;
                }
            }
        }
        if (isInValidPath) {
            if (isCopyFile) {
                return copyFile(path, context);
            } else {
                return "false";
            }
        }
        return "true"; // no have non acill
    }

    private static String copyFile(String path, Context context) {
        String newPath;
        String extension = "";
        int j = path.lastIndexOf('.');
        if (j > 0) {
            extension = path.substring(j + 1);
            newPath = context.getCacheDir().getPath()
                    + "/" + System.currentTimeMillis() + "." + extension;
            File newCopyFile = FileHelper.copyFile(path, newPath);
            if (newCopyFile != null) {
                return newPath;
            }

        }
        return "false";
    }

    public static void insertPhoneContact(String displayName, String number, Context context) {
        /*context is App or Activity*/
        ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
        int contactIndex = operations.size();
        //Newly Inserted contact
        // A raw contact will be inserted ContactsContract.RawContacts table in contacts database.
        operations.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)//Step1
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        //Display name will be inserted in ContactsContract.Data table
        operations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)//Step2
                .withValueBackReference(ContactsContract.RawContacts.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(ContactsContract.RawContacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, displayName)
                .build());

        //Mobile number will be inserted in ContactsContract.Data table
        operations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)//Step 3
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(ContactsContract.RawContacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());
        boolean isSuccess = false;
        try {
            // We will do batch operation to insert all above data
            //Contains the output of the app of a ContentProviderOperation.
            //It is sure to have exactly one of uri or count set
            ContentProviderResult[] contentProvider = context.getContentResolver()
                    .applyBatch(ContactsContract.AUTHORITY, operations); //apply above data insertion into contacts list
            Log.e("insertPhoneContact", "result: " + contentProvider);
            isSuccess = contentProvider != null && contentProvider.length > 0;
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("insertPhoneContact displayName: " + displayName + ", number:  " + number + ", isSuccess: " + isSuccess);
    }

    public static Bitmap getImageCaptche(String str) {
        Bitmap bitmap = null;
        byte[] data = Base64.decode(str, Base64.DEFAULT);
        bitmap = BitmapFactory.decodeByteArray(data,
                0, data.length);
        return bitmap;
    }

    /**
     * Format phone to: 0xx xxxxxxxx
     *
     * @param phone
     * @return
     */
    public static String formatPhoneNumberCambodia(String phone) {
        String defaultRegion = "KH";
        StringBuilder number = new StringBuilder();
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phone, defaultRegion);
            if (phoneUtil.isValidNumber(numberProto)) {
                String nationalPhone = Utilities.getNationalPhoneNumber(phone, defaultRegion);
                nationalPhone = "0" + nationalPhone;
                String subPhoneFirst = nationalPhone.substring(0, 3);
                String subPhoneLast = nationalPhone.substring(3);
                number.append(subPhoneFirst)
                        .append(" ")
                        .append(subPhoneLast);
            }
        } catch (NumberParseException e) {
            android.util.Log.e(TAG, "formatPhoneNumberCambodia: ", e);
            number.append("");
        }
        return number.toString();
    }

    public static void animateExpand(ImageView arrow) {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.startAnimation(rotate);
    }

    public static void animateCollapse(ImageView arrow) {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.startAnimation(rotate);
    }

    /**
     * Check email validate
     *
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static boolean isLocationEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return LocationManagerCompat.isLocationEnabled(locationManager);
    }

    /**
     * Helper method to interpolate colors
     * <p>
     * See more: https://github.com/ogasimli/BottomSheetDemo
     *
     * @param fraction
     * @param startValue
     * @param endValue
     * @return
     */
    public static int interpolateColor(float fraction, int startValue, int endValue) {
        int startA = (startValue >> 24) & 0xff;
        int startR = (startValue >> 16) & 0xff;
        int startG = (startValue >> 8) & 0xff;
        int startB = startValue & 0xff;
        int endA = (endValue >> 24) & 0xff;
        int endR = (endValue >> 16) & 0xff;
        int endG = (endValue >> 8) & 0xff;
        int endB = endValue & 0xff;
        return ((startA + (int) (fraction * (endA - startA))) << 24) |
                ((startR + (int) (fraction * (endR - startR))) << 16) |
                ((startG + (int) (fraction * (endG - startG))) << 8) |
                ((startB + (int) (fraction * (endB - startB))));
    }

    public static void adaptViewForInserts(View view) {
        if (view == null) {
            android.util.Log.e(TAG, "adaptViewForInserts: view null");
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            // Prepare original top padding of the view
            int originalTopPadding = view.getPaddingTop();
            view.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                @Override
                public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                    // Update view's top padding to accommodate system window top inset
                    int systemTopPadding = windowInsets.getSystemWindowInsetTop() + originalTopPadding;
                    view.setPadding(0, systemTopPadding, 0, 0);
                    return windowInsets;
                }
            });
        }
    }

    public static void adaptViewForInsertBottom(View view) {
        if (view == null) {
            android.util.Log.e(TAG, "adaptViewForInserts: view null");
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            // Prepare original bottom padding of the view
            int originalBottomPadding = view.getPaddingBottom();
            view.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                @Override
                public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                    // Update view's bottom padding to accommodate system window bottom inset
                    int systemBottomPadding = windowInsets.getSystemWindowInsetBottom() + originalBottomPadding;
                    view.setPadding(0, 0, 0, systemBottomPadding);
                    return windowInsets;
                }
            });
        }
    }

    public static int getHeightNavigationDevice(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static void adaptViewForInsertBottomKeepTop(View view) {
        if (view == null) {
            android.util.Log.e(TAG, "adaptViewForInserts: view null");
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            // Prepare original bottom padding of the view
            int originalBottomPadding = view.getPaddingBottom();
            view.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                @Override
                public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                    // Update view's bottom padding to accommodate system window bottom inset
                    int systemBottomPadding = windowInsets.getSystemWindowInsetBottom() + originalBottomPadding;
                    view.setPadding(0, view.getPaddingTop(), 0, systemBottomPadding);
                    return windowInsets;
                }
            });
        }
    }

    public static void adaptViewForInsertPeekHeight(View view, BottomSheetBehavior bottomSheetBehavior) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            // Prepare original top padding of the view
            int bottomSheetOriginalPeekHeight = bottomSheetBehavior.getPeekHeight();
            view.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                @Override
                public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        // If Q, update peek height according to gesture inset bottom
                        Insets gestureInsets = windowInsets.getSystemGestureInsets();
                        bottomSheetBehavior.setPeekHeight(bottomSheetOriginalPeekHeight + gestureInsets.bottom);
                    } else {
                        // If not Q, update peek height according to system window inset bottom
                        bottomSheetBehavior.setPeekHeight(bottomSheetOriginalPeekHeight + windowInsets.getSystemWindowInsetBottom());
                    }
                    return windowInsets;
                }
            });
        }
    }

    public static ValueAnimator changeGradientDrawableColor(GradientDrawable view, int duration, int initialColor, int finalColor) {
        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();
                int blended = interpolateColor(position, initialColor, finalColor);
                // Apply blended color to the view.
                view.setColor(blended);
            }
        });
        anim.setDuration(2000).start();
        return anim;
    }

    public static ValueAnimator changeWaveLoadingColor(WaveLoadingView view, int duration, int initialColor, int finalColor) {
        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();
                int blended = interpolateColor(position, initialColor, finalColor);

                // Apply blended color to the view.
                view.setWaveColor(blended);
            }
        });

        anim.setDuration(2000).start();
        return anim;
    }

    private static void setNavigationBarTransparent(Activity activity, int color) {
        if (activity == null || activity.isFinishing()) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setNavigationBarColor(ContextCompat.getColor(activity, color));
        }
    }

    public static String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        return String.valueOf(calendar.getTimeInMillis());
    }

    public static String getTimeToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return String.valueOf(calendar.getTimeInMillis());
    }

    public static String getTime7daysPrev() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 7);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return String.valueOf(calendar.getTimeInMillis());
    }

    public static String getTime30daysPrev() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 30);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return String.valueOf(calendar.getTimeInMillis());
    }

    public static void setSystemUiVisibility(Activity activity) {
        if (activity == null || activity.isFinishing()) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
        } else {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
            }
        }
    }

    public static void setSystemUiVisibilityHideNavigation(Activity activity) {
        if (activity == null || activity.isFinishing()) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
        } else {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
            }
        }
    }


    public static void setSystemUiVisibilityHideNavigation(Activity activity, int colorBar) {
        if (activity == null || activity.isFinishing()) return;
        Log.e("setSystemUiVisibilityHideNavigation", activity.getClass().getSimpleName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, colorBar));
            setNavigationBarTransparent(activity, colorBar);
            Common.setFullStatusBar(activity);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, colorBar));
            }
        }
    }
//
//    public static void setSystemUiVisibilityHideNavigation(Activity activity) {
//        if (activity == null || activity.isFinishing()) return;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
//        } else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//                activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity,android.R.color.transparent));
//            }
//        }
//    }

    public static boolean hasNavBar(Resources resources) {
        //Emulator
        if (Build.FINGERPRINT.startsWith("generic"))
            return true;

        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        return id > 0 && resources.getBoolean(id);
    }

    public static int navHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static String getNationalPhoneNumber(String phoneNumber, String countryCode) {
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, countryCode);
            return String.valueOf(numberProto.getNationalNumber());
        } catch (NumberParseException e) {
            android.util.Log.e(TAG, "getNationalPhoneNumber: - NumberParseException was thrown: " + e.toString());
        }
        return "";
    }

    public static boolean isPackageInstalled(Context c, String targetPackage) {
        PackageManager pm = c.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public static int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Point point = new Point();
        assert wm != null;
        wm.getDefaultDisplay().getSize(point);
        return point.y;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(float dp) {
        return (int) (dp * ApplicationController.self().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @return A float value to represent dp equivalent to px value
     */
    public static int convertPixelsToDp(float px) {
        return (int) (px / ApplicationController.self().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * This method return height of android navigator
     *
     * @param activity Activity
     */
    public static int getNavigationBarHeight(Activity activity) {
        Rect rectangle = new Rect();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
        activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        return displayMetrics.heightPixels - (rectangle.top + rectangle.height());
    }

    public static boolean isMetfoneNumber(String number) {
        String reg = "(00855|855|0|)((7546)\\d{5}|(60|66|67|68|90)\\d{6}|(31|71|88|97)\\d{7})";
        Pattern p = Pattern.compile(reg);
        return p.matcher(number).matches();
    }

    public static boolean isPhoneNumber(String number) {
        String reg = "[+]*[0-9]{9,13}$";
        Pattern p = Pattern.compile(reg);
        return p.matcher(number).matches();
    }

    public static String formatDouble(double value) {
        DecimalFormat df = new DecimalFormat("###.##");
        return df.format(value);
    }

    public static void reCalculateDimensionAnchorView(View anchorView, View screen, View originalView, View frame) {
        anchorView.post(() -> {
            int heightScreen = screen.getMeasuredHeight();
            int heightImageOriginalSize = originalView.getMeasuredHeight();
            int heightAnchorPointForFrame = frame.getMeasuredHeight();
            anchorView.getLayoutParams().height =
                    heightAnchorPointForFrame * heightScreen / heightImageOriginalSize;
            anchorView.requestLayout();
        });
    }

    public static String addRequire(String text, String color) {
        String input = "<font color=" + color + ">" + "*" + "</font>";
        return text + input;
    }
}
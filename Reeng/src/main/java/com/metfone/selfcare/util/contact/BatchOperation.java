package com.metfone.selfcare.util.contact;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.provider.ContactsContract;

import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * This class handles execution of batch mOperations on Contacts provider.
 */
public class BatchOperation {

	private static final String TAG = "BatchOperation";

	private final ContentResolver mResolver;

	ArrayList<ContentProviderOperation> mOperations;

	public BatchOperation(Context context, ContentResolver resolver) {
		mResolver = context.getContentResolver();
		mOperations = new ArrayList<ContentProviderOperation>();
	}

	public int size() {
		return mOperations.size();
	}

	public void add(ContentProviderOperation contentProviderOperation) {
		mOperations.add(contentProviderOperation);
	}

	public void execute() {

		if (mOperations.size() == 0) {
			return;
		}

		try {
			// Apply the mOperations to the content provider
			mResolver.applyBatch(ContactsContract.AUTHORITY, mOperations);
		} catch (final OperationApplicationException e1) {
			Log.e(TAG, "storing contact data failed", e1);
		} catch (final RemoteException e2) {
			Log.e(TAG, "storing contact data failed", e2);
		}
		mOperations.clear();
	}
}

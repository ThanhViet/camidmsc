package com.metfone.selfcare.util;

import android.content.res.Resources;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.easyvideoplayer.Util;
import com.metfone.selfcare.ui.imageview.CircleImageView;

/**
 * Created by vtsoft on 11/27/2014.
 */
public class ControlMedia {
    private static String TAG = ControlMedia.class.getSimpleName();
    private View mRoot, mViewPlay;
    private ImageView btnPlay;
    private ImageView btnClose;
    private ImageView btnNext;
    private TextView tvName;
    private TextView tvDuration;
    private CircleImageView mImgThumb;
    private SeekBar mSeekbar;
    private ProgressLoading mProgresLoading;
    private OnStateMusicBarChange mStateMusicBarListener;
    private boolean isProvisionalHiddenMusicBar = false;
    private ApplicationController mApplication;
    private MusicBusiness mMusicBusiness;

    //
    private final int TIME_HIDDEN_MUSIC_BAR = 3000;

    private CountDownTimer mCountDownTimer = new CountDownTimer(TIME_HIDDEN_MUSIC_BAR, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            isProvisionalHiddenMusicBar = true;
            mRoot.setVisibility(View.GONE);
            mStateMusicBarListener.onShowMusicBar(false);
        }
    };

    public ControlMedia(View v, ApplicationController application) {
        mRoot = v;
        mViewPlay = v.findViewById(R.id.layout_play);
        btnPlay = (ImageView) v.findViewById(R.id.media_play);
        btnClose = (ImageView) v.findViewById(R.id.media_close);
        btnNext = (ImageView) v.findViewById(R.id.media_next);
        tvName = (TextView) v.findViewById(R.id.media_name);
        tvDuration = (TextView) v.findViewById(R.id.media_duration);
        mSeekbar = (SeekBar) v.findViewById(R.id.media_buffer);
        mSeekbar.setPadding(0, 0, 0, 0);
        mImgThumb = (CircleImageView) v.findViewById(R.id.media_thumb);
        mProgresLoading = (ProgressLoading) v.findViewById(R.id.media_loading);
        isProvisionalHiddenMusicBar = false;
        this.mApplication = application;
        mMusicBusiness = mApplication.getMusicBusiness();
        // setStateMediaPlayer(true);
    }

    /*public void updateSongDesc(String songDesc) {
        if (mMusicBusiness != null &&
                mMusicBusiness.isExistListenerRoom() &&
                !mMusicBusiness.isPlayList()) {
            tvSinger.setText(songDesc);
            tvSinger.setSelected(true);
        }
    }*/

    public void setInfo(MediaModel song, int stateMedia, ThreadMessage threadMessage) {
        Log.d(TAG, "setInfo " + stateMedia);
        if (song == null) {
            return;
        }
        Resources res = mApplication.getResources();
        int resIconNext;
        String songTitle;
        String songDesc;
        // nghe room
        if (mMusicBusiness == null) {
            resIconNext = R.drawable.ic_voicemail_next;
            songTitle = song.getName();
            songDesc = song.getSinger();
        } else if (mMusicBusiness.isExistListenerRoom()) {
            if (mMusicBusiness.isPlayList()) {
                songTitle = song.getName();
                songDesc = song.getSinger();
            } else {
                if (threadMessage != null) {
                    String acceptedName = mMusicBusiness.getCurrentFriendNameAccepted();
                    if (TextUtils.isEmpty(acceptedName)) {
                        acceptedName = threadMessage.getThreadName();
                    }
                    songTitle = String.format(res.getString(R.string.desc_song_room), acceptedName, song.getName());
                } else {
                    songTitle = song.getName();
                }
                songDesc = mMusicBusiness.getDescSongRoom();
            }
            resIconNext = R.drawable.ic_voicemail_next;
        } else {// nghe 1-1,group,stranger
            resIconNext = R.drawable.ic_voicemail_change;
            songTitle = song.getName();
            songDesc = song.getSinger();
        }
        btnNext.setImageResource(resIconNext);
        // tvName.setText(songTitle);
        tvName.setText(TextHelper.fromHtml(String.format(res.getString(R.string.media_title), songTitle, songDesc)));
        tvName.setSelected(true);
        /*tvSinger.setText(songDesc);
        tvSinger.setSelected(true);*/
        mApplication.getAvatarBusiness().setSongAvatar(mImgThumb, song.getImage());
        setStateMediaPlayer(stateMedia);
        setProgress(mApplication.getPlayMusicController().getCurrentProgress(),
                mApplication.getPlayMusicController().getCurrentMediaPosition());
    }

    private void drawStateAction(boolean isLoading) {
        if (isLoading) {
            btnNext.setVisibility(View.GONE);
            mViewPlay.setVisibility(View.VISIBLE);
            mProgresLoading.setVisibility(View.VISIBLE);
            btnPlay.setVisibility(View.INVISIBLE);
        } else {
            mProgresLoading.setVisibility(View.INVISIBLE);
            btnPlay.setVisibility(View.VISIBLE);
            if (mMusicBusiness == null) {
                btnNext.setVisibility(View.GONE);
                mViewPlay.setVisibility(View.VISIBLE);
            } else if (mMusicBusiness.isExistListenerRoom() && mMusicBusiness.isPlayList()) {
                btnNext.setVisibility(View.VISIBLE);
                mViewPlay.setVisibility(View.GONE);
            } else if (mMusicBusiness.isExistListenerSolo()) {
                btnNext.setVisibility(View.VISIBLE);
                mViewPlay.setVisibility(View.GONE);
            } else {
                btnNext.setVisibility(View.GONE);
                mViewPlay.setVisibility(View.VISIBLE);
            }
        }
    }

    public void setProgress(int progress, int currentPosition) {
        mSeekbar.setProgress(progress);
        tvDuration.setText(Util.getDurationString(currentPosition, false));
    }

    public void showMediaPlayer(boolean show) {
        if (show) {
            mRoot.setVisibility(View.VISIBLE);
            // start timer
            int heightScreen = mApplication.getHeightPixels();
            double density = mApplication.getDensity();
            // density < 1.5 and device width, height < xxx
            if (!mApplication.getMusicBusiness().isExistListenerRoom() &&
                    density < 1.5 && heightScreen < 400) {// nghe trong room thi ko tu an
                // Log.d("----------------density ", "" + density);
                mCountDownTimer.start();
            }
            isProvisionalHiddenMusicBar = false;
        } else {
            mRoot.setVisibility(View.GONE);
            mCountDownTimer.cancel();
            isProvisionalHiddenMusicBar = true;
        }
        mStateMusicBarListener.onShowMusicBar(show);
    }

    public int getVisibility() {
        return mRoot.getVisibility();
    }

    public void setOnClick(View.OnClickListener onClickListener) {
        btnPlay.setOnClickListener(onClickListener);
        btnClose.setOnClickListener(onClickListener);
        btnNext.setOnClickListener(onClickListener);
    }

    public void setStateMediaPlayer(int stateMediaPlayer) {
        switch (stateMediaPlayer) {
            case Constants.PLAY_MUSIC.PLAYING_NONE:
            case Constants.PLAY_MUSIC.PLAYING_GET_INFO:
            case Constants.PLAY_MUSIC.PLAYING_PREPARING:
            case Constants.PLAY_MUSIC.PLAYING_RETRIEVING:
                drawStateAction(true);
                setProgress(0, 0);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PLAYING:
                drawStateAction(false);
                btnPlay.setImageResource(R.drawable.ic_voicemail_pause);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PAUSED:
                drawStateAction(false);
                mViewPlay.setVisibility(View.VISIBLE);
                btnNext.setVisibility(View.GONE);
                btnPlay.setImageResource(R.drawable.ic_voicemail_play);
                break;
            case Constants.PLAY_MUSIC.PLAYING_BUFFERING_START:
            case Constants.PLAY_MUSIC.PLAYING_BUFFERING_END:

                break;
            default:
                drawStateAction(true);
                setProgress(0, 0);
                break;
        }
    }

    public void setStateMusicBarListener(OnStateMusicBarChange listener) {
        this.mStateMusicBarListener = listener;
    }

    public boolean isProvisionalHiddenMusicBar() {
        return isProvisionalHiddenMusicBar;
    }

    public interface OnStateMusicBarChange {
        void onShowMusicBar(boolean isShow);
    }
}

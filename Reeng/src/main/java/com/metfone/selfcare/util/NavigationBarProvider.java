/**
 * Copyright (C) 2016 Samsung Electronics Co., Ltd. All rights reserved.
 * <p/>
 * Mobile Communication Division,
 * Digital Media & Communications Business, Samsung Electronics Co., Ltd.
 * <p/>
 * This software and its documentation are confidential and proprietary
 * information of Samsung Electronics Co., Ltd.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to
 * any electronic medium or machine-readable form without the prior written
 * consent of Samsung Electronics.
 * <p/>
 * Samsung Electronics makes no representations with respect to the contents,
 * and assumes no responsibility for any errors that might appear in the
 * software and documents. This publication and the contents hereof are subject
 * to change without notice.
 */

package com.metfone.selfcare.util;

import android.content.Context;
import android.content.res.Resources;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.ViewConfiguration;

public class NavigationBarProvider {
    private static final String TAG = "NavigationBarProvider";

    private static volatile NavigationBarProvider mInstance = null;

    private NavigationBarProvider() {
    }

    public static NavigationBarProvider getInstance() {
        if (mInstance == null) {
            synchronized (NavigationBarProvider.class) {
                if (mInstance == null) {
                    mInstance = new NavigationBarProvider();
                }
            }
        }
        return mInstance;
    }

    public int getNavigationBarHeight(Context context) {
        int ret = 0;
        if (!hasNavigationBar(context)) {
            return ret;
        }
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            ret = resources.getDimensionPixelSize(resourceId);
        }
        Log.i(TAG,"getNavigationBarHeight ret : " + ret);
        return ret;
    }

    private boolean hasNavigationBar(Context context) {
        boolean ret = false;

        // Check resources
        Resources resources = context.getResources();
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        if (id > 0) {
            ret = resources.getBoolean(id);
        }

        // Check keys
        if (!ret) {
            boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            ret = !hasMenuKey && !hasBackKey;
        }

        Log.i(TAG,"hasNavigationBar ret : " + ret);
        return ret;
    }

}
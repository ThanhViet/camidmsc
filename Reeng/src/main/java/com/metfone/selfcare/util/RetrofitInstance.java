package com.metfone.selfcare.util;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.network.metfoneplus.MetfonePlusClient.LANGUAGE;

import com.google.gson.Gson;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.InvitedCodeRequest;
import com.metfone.selfcare.model.account.InvitedCodeResponse;
import com.metfone.selfcare.model.addAccount.AddAccountFtthUserResponse;
import com.metfone.selfcare.model.addAccount.AddAccountSendIdRequest;
import com.metfone.selfcare.model.addAccount.AddAccountSendIdResponse;
import com.metfone.selfcare.model.addAccount.AddAccountVerifyIdRequest;
import com.metfone.selfcare.module.movienew.model.InsertLogWatchResponse;
import com.metfone.selfcare.module.movienew.model.RateResponse;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.request.AddServiceRequest;
import com.metfone.selfcare.network.camid.request.CheckPhoneRequest;
import com.metfone.selfcare.network.camid.request.GenerateOtpRequest;
import com.metfone.selfcare.network.camid.request.RefreshTokenRequest;
import com.metfone.selfcare.network.camid.request.UpdateMetfoneRequest;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.network.camid.response.CheckCarrierResponse;
import com.metfone.selfcare.network.camid.response.RefreshTokenResponse;
import com.metfone.selfcare.network.camid.response.ServiceResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    private static final String OTP_DEFAULT = "112233";
    public static Retrofit mRetrofitInstance;
    //    public static String BASE_URL = "http://123.31.45.201:8856/";
    // public static String BASE_URL = "http://103.27.237.84:8856/";
    // public static String BASE_URL = "http://103.27.237.84:8887/";
//    public static String BASE_URL = "http://123.31.45.218:8856/";
    public static String BASE_URL = "https://openid.metfone.com.kh/";
    public static CamIdUserBusiness mCamIdUserBusiness;
    ApiService service = getInstance().create(ApiService.class);

    public static Retrofit getInstance() {
        if (mRetrofitInstance == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }
            client.interceptors().add(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request.Builder requestBuilder = chain.request().newBuilder();
//                    requestBuilder.addHeader("Authorization", MovieApi.TOKEN);
                    requestBuilder.addHeader("language", LANGUAGE);
                    requestBuilder.addHeader("Content-Type", "application/json");
                    return chain.proceed(requestBuilder.build());
                }
            });
            client.connectTimeout(60, TimeUnit.SECONDS);
            client.readTimeout(60, TimeUnit.SECONDS);
            client.writeTimeout(60, TimeUnit.SECONDS);
            mRetrofitInstance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client.build())
                    .build();
        }

        return mRetrofitInstance;
    }

    public void checkPhone(String phone, ApiCallback<BodyResponse> callback) {
        CheckPhoneRequest checkPhoneRequest = new CheckPhoneRequest();
        CheckPhoneRequest.Request request = checkPhoneRequest.new Request();
        request.setPhoneNumber(phone);
        checkPhoneRequest.setWsRequest(request);

        String json = new Gson().toJson(checkPhoneRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        ApiService service = getInstance().create(ApiService.class);
        Call<BodyResponse> call = service.checkPhone(requestBody);
        call.enqueue(new Callback<BodyResponse>() {
            @Override
            public void onResponse(Call<BodyResponse> call, Response<BodyResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<BodyResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void generateOtp(String phone, ApiCallback<BodyResponse> callback) {
        GenerateOtpRequest generateOtpRequest = new GenerateOtpRequest();
        GenerateOtpRequest.Request request = generateOtpRequest.new Request();
        request.setPhoneNumber(phone);
        generateOtpRequest.setWsRequest(request);

        String json = new Gson().toJson(generateOtpRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        ApiService service = getInstance().create(ApiService.class);
        Call<BodyResponse> call = service.generateOtp(requestBody);
        call.enqueue(new Callback<BodyResponse>() {
            @Override
            public void onResponse(Call<BodyResponse> call, Response<BodyResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<BodyResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }


    public void addService(String phone, String otp, int serviceId, ApiCallback<AddServiceResponse> callback) {
        AddServiceRequest addServiceRequest = new AddServiceRequest();
        AddServiceRequest.Request request = addServiceRequest.new Request();
        request.setPhoneNumber(phone);
        request.setOtp(otp);
        request.setServiceId(serviceId);
        addServiceRequest.setWsRequest(request);

        String json = new Gson().toJson(addServiceRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        String token = mCamIdUserBusiness.getCamIdToken();

        ApiService service = getInstance().create(ApiService.class);
        Call<AddServiceResponse> call = service.addService(token, requestBody);
        call.enqueue(new Callback<AddServiceResponse>() {
            @Override
            public void onResponse(Call<AddServiceResponse> call, Response<AddServiceResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<AddServiceResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void addServiceProfile(String phone, String otp, int serviceId, ApiCallback<GetUserResponse> callback) {
        AddServiceRequest addServiceRequest = new AddServiceRequest();
        AddServiceRequest.Request request = addServiceRequest.new Request();
        request.setPhoneNumber(phone);
        request.setOtp(otp);
        request.setServiceId(serviceId);
        addServiceRequest.setWsRequest(request);

        String json = new Gson().toJson(addServiceRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);

        ApiService service = getInstance().create(ApiService.class);
        Call<GetUserResponse> call = service.addServiceProfile(token, requestBody);
        call.enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void getUserInfo(ApiCallback<AddServiceResponse> callback) {
        ApiService service = getInstance().create(ApiService.class);
        String token = mCamIdUserBusiness.getCamIdToken();
        Call<AddServiceResponse> call = service.getUserInfo(token);
        call.enqueue(new Callback<AddServiceResponse>() {
            @Override
            public void onResponse(Call<AddServiceResponse> call, Response<AddServiceResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<AddServiceResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    /**
     * Update 1 số điện thoại thành số service
     * Khi add số
     * - nếu số đã có trong ```service``` truyền user_service_id
     * - nếu không thì truyền ```phone_number``` và ```otp```
     * <p>
     * Khi gọi hàm nếu giá trị nào không cần, hãy set tham số là null
     *
     * @param phone         new phone number not on services
     * @param otp           otp of that phone
     * @param userServiceId user service id of the phone number in services
     * @param callback
     */
    public void updateMetfone(String phone, String otp, int userServiceId, ApiCallback<AddServiceResponse> callback) {
        UpdateMetfoneRequest updateMetfoneRequest = new UpdateMetfoneRequest();
        UpdateMetfoneRequest.Request request = updateMetfoneRequest.new Request();
        request.setPhoneNumber(null);
        request.setOtp(null);
        request.setUserServiceId(userServiceId);

        updateMetfoneRequest.setWsRequest(request);

        String json = new Gson().toJson(updateMetfoneRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        String token = mCamIdUserBusiness.getCamIdToken();

        ApiService service = getInstance().create(ApiService.class);
        Call<AddServiceResponse> call = service.updateMetfone(token, requestBody);
        call.enqueue(new Callback<AddServiceResponse>() {
            @Override
            public void onResponse(Call<AddServiceResponse> call, Response<AddServiceResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<AddServiceResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void updateMetfoneProfile(String phone, String otp, int userServiceId, ApiCallback<GetUserResponse> callback) {
        UpdateMetfoneRequest updateMetfoneRequest = new UpdateMetfoneRequest();
        UpdateMetfoneRequest.Request request = updateMetfoneRequest.new Request();
        request.setPhoneNumber(phone);
        request.setOtp(otp);
        request.setUserServiceId(userServiceId);

        updateMetfoneRequest.setWsRequest(request);

        String json = new Gson().toJson(updateMetfoneRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

//        String token = TOKEN;
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);

        com.metfone.selfcare.network.ApiService service = getInstance().create(com.metfone.selfcare.network.ApiService.class);
        Call<GetUserResponse> call = service.updateMetfoneProfile(token, requestBody);
        call.enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }


    public void getService(ApiCallback<ServiceResponse> callback) {
        ApiService service = getInstance().create(ApiService.class);
        String token = mCamIdUserBusiness.getCamIdToken();
        Call<ServiceResponse> call = service.getService(token);
        call.enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void refreshToken(String token, ApiCallback<RefreshTokenResponse> callback) {
        RefreshTokenRequest refreshTokenRequest = new RefreshTokenRequest();
        RefreshTokenRequest.Request request = refreshTokenRequest.new Request();
        request.setRefresh_token(token);
        refreshTokenRequest.setWsRequest(request);
        String json = new Gson().toJson(refreshTokenRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

//        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);

        ApiService service = getInstance().create(ApiService.class);
        Call<RefreshTokenResponse> call = service.refreshToken(requestBody);
        call.enqueue(new Callback<RefreshTokenResponse>() {
            @Override
            public void onResponse(Call<RefreshTokenResponse> call, Response<RefreshTokenResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<RefreshTokenResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    /**
     * insert thông tin các video đc pause
     *
     * @param id_film
     * @param msisdn
     * @param time_seek
     * @param callback
     */
    public void insertLogWatch(String id_film, String msisdn, int ref_id, String time_seek, ApiCallback<InsertLogWatchResponse> callback) {
        service.insertLogWatch(id_film, msisdn, String.valueOf(ref_id), time_seek).enqueue(new Callback<InsertLogWatchResponse>() {
            @Override
            public void onResponse(Call<InsertLogWatchResponse> call, Response<InsertLogWatchResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<InsertLogWatchResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void rateVideo(String filmid, String msisdn, int ref_id, int rate, ApiCallback<RateResponse> callback) {
        service.rateVideo(filmid, msisdn, String.valueOf(ref_id), rate).enqueue(new Callback<RateResponse>() {
            @Override
            public void onResponse(Call<RateResponse> call, Response<RateResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<RateResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void checkCarrier(String phone, ApiCallback<CheckCarrierResponse> callback) {
        CheckPhoneRequest checkPhoneRequest = new CheckPhoneRequest();
        CheckPhoneRequest.Request request = checkPhoneRequest.new Request();
        request.setPhoneNumber(phone);
        checkPhoneRequest.setWsRequest(request);

        String json = new Gson().toJson(checkPhoneRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        ApiService service = getInstance().create(ApiService.class);
        Call<CheckCarrierResponse> call = service.checkCarrier(requestBody);
        call.enqueue(new Callback<CheckCarrierResponse>() {
            @Override
            public void onResponse(Call<CheckCarrierResponse> call, Response<CheckCarrierResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<CheckCarrierResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void invitedCode(String code, ApiCallback<InvitedCodeResponse> callback) {
        InvitedCodeRequest invitedCodeRequest = new InvitedCodeRequest();
        InvitedCodeRequest.Request request = invitedCodeRequest.new Request();
        request.setCode(code);
        invitedCodeRequest.setWsRequest(request);

        String json = new Gson().toJson(invitedCodeRequest);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);

        Call<InvitedCodeResponse> call = service.invitedCode(token, requestBody);
        call.enqueue(new Callback<InvitedCodeResponse>() {
            @Override
            public void onResponse(Call<InvitedCodeResponse> call, Response<InvitedCodeResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<InvitedCodeResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void sendId(String phone, ApiCallback<BaseResponse<AddAccountSendIdResponse>> callback) {
        AddAccountSendIdRequest request = new AddAccountSendIdRequest(phone);
        ApiService service = getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        Call<BaseResponse<AddAccountSendIdResponse>> call = service.addAccountSendId(token, request);
        call.enqueue(new Callback<BaseResponse<AddAccountSendIdResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<AddAccountSendIdResponse>> call,
                                   Response<BaseResponse<AddAccountSendIdResponse>> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<BaseResponse<AddAccountSendIdResponse>> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void verifyId(String phone, String accountID, ApiCallback<BaseResponse<AddAccountSendIdResponse>> callback) {
        AddAccountVerifyIdRequest request = new AddAccountVerifyIdRequest(phone, accountID);
        ApiService service = getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        Call<BaseResponse<AddAccountSendIdResponse>> call = service.addAccountVerifyId(token, request);
        call.enqueue(new Callback<BaseResponse<AddAccountSendIdResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<AddAccountSendIdResponse>> call,
                                   Response<BaseResponse<AddAccountSendIdResponse>> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<BaseResponse<AddAccountSendIdResponse>> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

    public void getFtthAccount(ApiCallback<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> callback) {
        ApiService service = getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        Call<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> call = service.getAccountFtth(token);
        call.enqueue(new Callback<BaseResponse<ArrayList<AddAccountFtthUserResponse>>>() {
            @Override
            public void onResponse(Call<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> call,
                                   Response<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> call, Throwable t) {
                callback.onError(t);
            }
        });
    }
}

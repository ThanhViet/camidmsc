package com.metfone.selfcare.util;

public class OpenIDErrorCode {
    public static final String ERROR_CODE_00 = "00";
    public static final String ERROR_CODE_01 = "1";
    public static final String ERROR_CODE_42 = "42";
    public static final String ERROR_CODE_49 = "49";
    public static final String ERROR_CODE_68 = "68";
}

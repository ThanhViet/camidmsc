package com.metfone.selfcare.util;

import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.camid.UserOldResponse;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.BaseDataRequestMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.HistoryRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TopUpQrCodeRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TransMobileRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.BaseResponseMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.HistoryResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TopUpQrCodeResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TransMobileResponse;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.request.AddServiceRequest;
import com.metfone.selfcare.network.camid.request.CheckPhoneRequest;
import com.metfone.selfcare.network.camid.request.GenerateOtpRequest;
import com.metfone.selfcare.network.camid.request.UpdateMetfoneRequest;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.network.camid.response.ServiceResponse;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;

public class RetrofitMochaInstance {
    public static Retrofit mRetrofitInstance;
    public static String BASE_URL = "http://hlvip2.mocha.com.vn:80/";
    ApiService service = getInstance().create(ApiService.class);
    public static CamIdUserBusiness mCamIdUserBusiness;

    public static Retrofit getInstance() {
        if (mRetrofitInstance == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }

            mRetrofitInstance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .build();
        }

        return mRetrofitInstance;
    }

    public void getOtpOldMocha(String token, String username, String regionCode, ApiCallback<OTPOldMochaResponse> callback) {
        String version = BuildConfig.VERSION_NAME;
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.HTTP.REST_USER_NAME, username);
        params.put(Constants.HTTP.REST_COUNTRY_CODE, regionCode);
        params.put("token", token);
        params.put("platform", "Android");
        params.put("os_version", Build.VERSION.RELEASE);
        params.put("device", Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL);
        params.put("revision", Config.REVISION);
        params.put("version", version);
        params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        Call<OTPOldMochaResponse> call = service.generateOldMochaOtp(params);
        call.enqueue(new Callback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Call<OTPOldMochaResponse> call, Response<OTPOldMochaResponse> response) {
                callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<OTPOldMochaResponse> call, Throwable t) {
                callback.onError(t);
            }
        });
    }

}

package com.metfone.selfcare.util.contact;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Class for managing contacts sync related mOperations
 */
@SuppressLint("NewApi")
public class ContactManager {

    private static final String TAG = ContactManager.class.getSimpleName();

    private ApplicationController mApp;
    private boolean isAccountViettel;


    public ContactManager(ApplicationController mApp) {
        this.mApp = mApp;
        isAccountViettel = mApp.getReengAccountBusiness().isViettel();
    }

    /**
     * Add account for sync contacts
     *
     * @param accountName account name
     */
    public void createAccount(String accountName) {
        if (!Version.hasN()) {
            return;
        }
        if (TextUtils.isEmpty(accountName)) {
            return;
        }
        if (!isExistAccount(mApp, accountName)) {

            Account cocoAccount = new Account(accountName,
                    SyncAdapterColumns.ACCOUNT_TYPE);
            AccountManager.get(mApp).addAccountExplicitly(cocoAccount,
                    null, null);

            ContentResolver.setSyncAutomatically(cocoAccount,
                    "com.android.contacts", true);
            ContentResolver.setMasterSyncAutomatically(true);
        }
    }

    public void deleteALLAccount(Activity activity) {
        if (!Version.hasN()) {
            return;
        }
        if (null == activity) {
            return;
        }
        try {
            AccountManager accountManager = AccountManager.get(activity);
            Account[] accounts = accountManager
                    .getAccountsByType(SyncAdapterColumns.ACCOUNT_TYPE);
            if (null != accounts && accounts.length > 0) {
                for (Account account : accounts) {
                    if ((null != account) && !TextUtils.isEmpty(account.name)) {
                        AccountManager.get(activity).removeAccount(
                                new Account(account.name, SyncAdapterColumns.ACCOUNT_TYPE), activity, new
                                        AccountManagerCallback<Bundle>() {
                                            @Override
                                            public void run(AccountManagerFuture<Bundle> future) {
                                                Log.i(TAG, " delete account callback: " + future.toString());
                                            }
                                        }, null);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception deleteallaccount", e);
        }
    }

    public void deleteALLAccount() {
        if (!Version.hasN()) {
            return;
        }
        try {
            AccountManager accountManager = AccountManager.get(mApp);
            Account[] accounts = accountManager
                    .getAccountsByType(SyncAdapterColumns.ACCOUNT_TYPE);
            if (null != accounts && accounts.length > 0) {
                for (Account account : accounts) {
                    if ((null != account) && !TextUtils.isEmpty(account.name)) {
                        AccountManager.get(mApp).removeAccountExplicitly(
                                new Account(account.name, SyncAdapterColumns.ACCOUNT_TYPE));
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception deleteallaccount", ex);
        }

    }

    /**
     * check whether exist account by account name and type
     *
     * @param context     context
     * @param accountName account name
     * @return whether exist account
     */
    public static boolean isExistAccount(Context context, String accountName) {
        if (!Version.hasN()) {
            return false;
        }
        boolean isExist = false;

        if (null == context || TextUtils.isEmpty(accountName)) {
            return isExist;
        }

        AccountManager accountManager = AccountManager.get(context);

        Account[] accounts = accountManager
                .getAccountsByType(SyncAdapterColumns.ACCOUNT_TYPE);

        if (null != accounts && accounts.length > 0) {
            for (Account account : accounts) {
                if ((null != account) && (accountName.equals(account.name))) {
                    isExist = true;
                    break;
                }
            }
        }

        return isExist;
    }


    public void syncContacts(String accountName, List<PhoneNumber> users) {
        HashSet<String> hashSetJid = new HashSet<>();
        Log.i(TAG, "syncContacts");
        if (TextUtils.isEmpty(accountName)) {
            Log.i(TAG, "syncContacts parameter is null");
            return;
        }

        if (null == users || users.size() == 0) {
            Log.i(TAG, "syncContacts users is null");
            return;
        }

        final ContentResolver resolver = mApp.getContentResolver();

        final BatchOperation batchOperation = new BatchOperation(mApp,
                resolver);
        for (final PhoneNumber user : users) {
            Log.i(TAG, "user: " + user.toString());
            if (!TextUtils.isEmpty(user.getJidNumber()) && !TextUtils.isEmpty(user.getContactId())) {
                if (!hashSetJid.contains(user.getContactId())) {
                    long rawContactIdActionMocha = getRawContact(resolver, accountName,
                            user.getContactId());
                    if (-1 == rawContactIdActionMocha) {
                    } else {
                        deleteContact(mApp, accountName, user);
                    }
                    addContact(mApp, accountName, user, batchOperation);
                    hashSetJid.add(user.getContactId());
                    if (batchOperation.size() >= 50) {
                        batchOperation.execute();
                    }
                }
            }

        }

        batchOperation.execute();
    }

    public static synchronized void deleteContacts(Context context,
                                                   String accountName, List<PhoneNumber> users) {
        if (!Version.hasN()) {
            return;
        }
        if (null == context || TextUtils.isEmpty(accountName)) {
            Log.i(TAG, "deleteContacts parameter is null");
            return;
        }

        if (null == users || users.size() == 0) {
            Log.i(TAG, "deleteContacts users is null");
            return;
        }

        final ContentResolver resolver = context.getContentResolver();

        final BatchOperation batchOperation = new BatchOperation(context,
                resolver);

        for (final PhoneNumber user : users) {
            long rawContactId = getRawContact(resolver, accountName,
                    user.getJidNumber());
            deleteContact(context, rawContactId, batchOperation);

            if (batchOperation.size() >= 50) {
                batchOperation.execute();
            }
        }

        batchOperation.execute();
    }

    public void deleteContact(Context context, String accountName, PhoneNumber phoneNumber) {
        /*final ContentResolver resolver = context.getContentResolver();
        final BatchOperation batchOperation = new BatchOperation(context,
                resolver);
        long rawContactId = getRawContact(resolver, accountName,
                phoneNumber.getJidNumber());

        deleteContact(context, rawContactId, batchOperation);
        batchOperation.execute();*/
        if (!Version.hasN()) {
            return;
        }
        final ContentResolver resolver = context.getContentResolver();

        final Cursor c = resolver.query(ContactsContract.Data.CONTENT_URI,
                new String[]{Data.RAW_CONTACT_ID},
                "(account_type NOT NULL AND account_type =? ) AND (account_name NOT NULL AND account_name =? " +
                        ") AND "
                        + Data.CONTACT_ID + "=?", new String[]{SyncAdapterColumns.ACCOUNT_TYPE, accountName,
                        phoneNumber.getContactId()}, null);

        try {
            if (c.moveToFirst()) {
                long rawContactId = c.getLong(c
                        .getColumnIndex(Data.RAW_CONTACT_ID));
                Log.d(TAG, "rawContactId: " + rawContactId);
//                deleteContact(context, contactIds.mRawContactsId, batchOperation);

                ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
//                int rawContactInsertIndex = ops.size();
                ops.add(ContentProviderOperation.newDelete(RawContacts.CONTENT_URI)
                        .withSelection(RawContacts._ID + " = ?", new String[]{String.valueOf(rawContactId)})
                        .build());
                resolver.applyBatch(ContactsContract.AUTHORITY, ops);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            if (c != null) {
                c.close();
            }
        }


    }


    /**
     * Adds a single contact to the platform contacts provider.
     *
     * @param context     the Activity context
     * @param accountName the account the contact belongs to
     * @param user        the sample User object
     */
    private void addContact(Context context, String accountName,
                            PhoneNumber user, BatchOperation batchOperation) {
        // Put the data in the contacts provider


//                .addPhone(user.getJidNumber(), Phone.TYPE_MOBILE)
                /*.addChatAction(context, user)
                .addVoicecallAction(context, user)
//                .addCallOutAction(context, user)
                .addVideocallAction(context, user)
                .addAggExceptions();*/
        if (user.isReeng()) {
            ContactOperations contactOp = ContactOperations.createNewContact(
                    context, user.getJidNumber(), accountName, batchOperation);
            contactOp.addName(user.getName())
//                    .addPhone(user.getJidNumber(), ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .addChatAction(context, user)
                    .addVoicecallAction(context, user)
                    .addVideocallAction(context, user)
                    .addAggExceptions();

        } else {
            if (user.isViettel()) {
                if (isAccountViettel) {
                    ContactOperations contactOp = ContactOperations.createNewContact(
                            context, user.getJidNumber(), accountName, batchOperation);
                    contactOp.addName(user.getName())
                            .addChatAction(context, user)
                            .addVoicecallAction(context, user)
                            .addAggExceptions();
                } else {
                    Log.i(TAG, "jid: " + user.getJidNumber() + "!mocha, isViettel, !isAccountViettel");
                }

            } else {
                Log.i(TAG, "jid: " + user.getJidNumber() + "!mocha va !isViettel");
            }

        }


    }

    /**
     * Deletes a contact from the platform contacts provider.
     *
     * @param context      the Authenticator Activity context
     * @param rawContactId the unique Id for this rawContact in contacts provider
     */
    private static void deleteContact(Context context, long rawContactId,
                                      BatchOperation batchOperation) {
        if (!Version.hasN()) {
            return;
        }
        batchOperation.add(ContactOperations.newDeleteCpo(
                ContentUris.withAppendedId(RawContacts.CONTENT_URI,
                        rawContactId), true).build());
    }


    private static long getRawContact(ContentResolver resolver,
                                      String accountName, String contactId) {
        long rawContactsId = -1;

        final Cursor c = resolver.query(Data.CONTENT_URI,
                new String[]{RawContacts._ID}, RawContacts.ACCOUNT_TYPE
                        + "=?" + " AND " + RawContacts.ACCOUNT_NAME + "=?"
                        + " AND " + Data.DATA4 + "=?",
                new String[]{SyncAdapterColumns.ACCOUNT_TYPE, accountName,
                        contactId}, null);

        try {
            if (c.moveToFirst()) {
                rawContactsId = c.getLong((c.getColumnIndex(RawContacts._ID)));
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return rawContactsId;
    }
}

package com.metfone.selfcare.util;

public interface IFragmentBackPress {
    boolean onFragmentBackPressed();
}

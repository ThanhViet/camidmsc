package com.metfone.selfcare.util.contact;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.AggregationExceptions;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.text.TextUtils;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;

/**
 * Helper class for storing data in the platform content providers.
 */
@SuppressLint("NewApi")
public class ContactOperations {

    private final ContentValues mValues;
    private ContentProviderOperation.Builder mBuilder;
    private final BatchOperation mBatchOperation;
    private boolean mYield;
    private long mRawContactId;
    private int mBackReference;
    private boolean mIsNewContact;

    /**
     * Returns an instance of ContactOperations instance for adding new contact
     * to the platform contacts provider.
     *
     * @param context     the Activity context
     * @param accountName the user name of the current login
     * @return instance of ContactOperations
     */
    public static ContactOperations createNewContact(Context context,
                                                     String jid, String accountName, BatchOperation batchOperation) {
        return new ContactOperations(context, jid, accountName,
                batchOperation);
    }

    public ContactOperations(Context context, String jid, String accountName,
                             BatchOperation batchOperation) {
        this(context, batchOperation);
        mBackReference = mBatchOperation.size();
        mIsNewContact = true;

        mValues.put(RawContacts.ACCOUNT_TYPE, SyncAdapterColumns.ACCOUNT_TYPE);
        mValues.put(RawContacts.ACCOUNT_NAME, accountName);
        mBuilder = newInsertCpo(RawContacts.CONTENT_URI, true).withValues(
                mValues);
        mBatchOperation.add(mBuilder.build());
    }

    public ContactOperations(Context context, BatchOperation batchOperation) {
        mValues = new ContentValues();
        mYield = true;
        mBatchOperation = batchOperation;
    }

    /**
     * Adds a contact name
     *
     * @return instance of ContactOperations
     */
    public ContactOperations addName(String displayName) {
        mValues.clear();
        if (!TextUtils.isEmpty(displayName)) {
            mValues.put(StructuredName.DISPLAY_NAME, displayName);
            mValues.put(StructuredName.MIMETYPE,
                    StructuredName.CONTENT_ITEM_TYPE);
        }
        if (mValues.size() > 0) {
            addInsertOp();
        }
        return this;
    }

    /**
     * Adds a phone number
     *
     * @param phone     new phone number for the contact
     * @param phoneType the type: cell, home, etc.
     * @return instance of ContactOperations
     */
    public ContactOperations addPhone(String phone, int phoneType) {
        mValues.clear();
        if (!TextUtils.isEmpty(phone)) {
            mValues.put(Phone.NUMBER, phone);
            mValues.put(Phone.TYPE, phoneType);
            mValues.put(Phone.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
            addInsertOp();
        }
        return this;
    }


    /**
     * Adds a profile call action
     *
     * @return instance of ContactOperations
     */
    public ContactOperations addChatAction(Context context, PhoneNumber user) {
        mValues.clear();
        if (null != user) {
            mValues.put(Data.DATA1, user.getJidNumber());
            mValues.put(Data.DATA2, context.getResources().getString(R.string.app_name));
            mValues.put(Data.DATA3, joinUserId2Label(context.getResources().getString(R.string.message), user.getJidNumber()));
            mValues.put(Data.DATA4, user.getContactId());
            mValues.put(Data.DATA5, user.getId());
            mValues.put(Data.MIMETYPE, SyncAdapterColumns.MIME_MOCHA_CHAT);
            addInsertOp();
        }
        return this;
    }

    /**
     * Adds a profile call action
     *
     * @return instance of ContactOperations
     */
    public ContactOperations addVideocallAction(Context context, PhoneNumber user) {
        mValues.clear();
        if (null != user) {
            mValues.put(Data.DATA1, user.getJidNumber());
            mValues.put(Data.DATA2, context.getResources().getString(R.string.app_name));
            mValues.put(Data.DATA3, joinUserId2Label(context.getResources().getString(R.string.video), user.getJidNumber()));
            mValues.put(Data.DATA4, user.getContactId());
            mValues.put(Data.DATA5, user.getId());
            mValues.put(Data.MIMETYPE, SyncAdapterColumns.MIME_MOCHA_CALL_VIDEO);
            addInsertOp();
        }
        return this;
    }

    /**
     * Adds a profile call action
     *
     * @return instance of ContactOperations
     */
    public ContactOperations addVoicecallAction(Context context, PhoneNumber user) {
        mValues.clear();
        if (null != user) {
            mValues.put(Data.DATA1, user.getJidNumber());
            mValues.put(Data.DATA2, context.getResources().getString(R.string.app_name));
            mValues.put(Data.DATA3, joinUserId2Label(context.getResources().getString(R.string.call), user.getJidNumber()));
            mValues.put(Data.DATA4, user.getContactId());
            mValues.put(Data.DATA5, user.getId());
            mValues.put(Data.MIMETYPE, SyncAdapterColumns.MIME_MOCHA_CALL);
            addInsertOp();
        }
        return this;
    }

    /*public ContactOperations addCallOutAction(Context context, PhoneNumber user) {
        mValues.clear();
        if (null != user) {
            mValues.put(Data.DATA1, user.getJidNumber());
            mValues.put(Data.DATA2, context.getResources().getString(R.string.app_name));
            mValues.put(Data.DATA3, joinUserId2Label(context.getResources().getString(R.string.call_option_out), user.getJidNumber()));
            mValues.put(Data.DATA4, user.getContactId());
            mValues.put(Data.DATA5, user.getId());
            mValues.put(Data.MIMETYPE, SyncAdapterColumns.MIME_MOCHA_CALL_OUT);
            addInsertOp();
        }
        return this;
    }*/

    /**
     * Application contacts keep together with local contacts
     *

     * @return this
     */
    public ContactOperations addAggExceptions() {
        mBatchOperation.add(ContentProviderOperation
                .newUpdate(AggregationExceptions.CONTENT_URI)
                .withYieldAllowed(false)
                .withValue(AggregationExceptions.TYPE,
                        AggregationExceptions.TYPE_KEEP_TOGETHER)
                /*.withValue(AggregationExceptions.RAW_CONTACT_ID1,
                        localRawContactId)*/
                .withValueBackReference(AggregationExceptions.RAW_CONTACT_ID2,
                        mBackReference).build());
        return this;
    }

    /**
     * Adds an insert operation into the batch
     */
    private void addInsertOp() {
        if (!mIsNewContact) {
            mValues.put(Phone.RAW_CONTACT_ID, mRawContactId);
        }
        mBuilder = newInsertCpo(
                addCallerIsSyncAdapterParameter(Data.CONTENT_URI), mYield);
        mBuilder.withValues(mValues);
        if (mIsNewContact) {
            mBuilder.withValueBackReference(Data.RAW_CONTACT_ID, mBackReference);
        }
        mYield = false;
        mBatchOperation.add(mBuilder.build());
    }

    private static ContentProviderOperation.Builder newInsertCpo(Uri uri,
                                                                 boolean yield) {
        return ContentProviderOperation.newInsert(
                addCallerIsSyncAdapterParameter(uri)).withYieldAllowed(yield);
    }

    public static ContentProviderOperation.Builder newDeleteCpo(Uri uri,
                                                                boolean yield) {
        return ContentProviderOperation.newDelete(
                addCallerIsSyncAdapterParameter(uri)).withYieldAllowed(yield);
    }

    private static Uri addCallerIsSyncAdapterParameter(Uri uri) {
        return uri
                .buildUpon()
                .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER,
                        "true").build();
    }

    private static String joinUserId2Label(String label, long userId) {
        return label + " (+" + userId + ")";

    }

    private static String joinUserId2Label(String label, String jid) {
        return label + " (" + jid + ")";

    }
}

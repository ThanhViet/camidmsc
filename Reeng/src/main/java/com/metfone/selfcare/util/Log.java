package com.metfone.selfcare.util;

public class Log {

    public static void setEnableLog(boolean enable) {
        com.viettel.util.Log.ENABLE_LOG = true;
        new Thread(com.viettel.util.Log::initialize);
    }

    public static void i(String tag, String string) {
        com.viettel.util.Log.i(tag, string);
    }

    public static void i(String tag, String string, Throwable throwable) {
        com.viettel.util.Log.i(tag, string, throwable);
    }

    public static void e(String tag, String string) {
        com.viettel.util.Log.e(tag, string);
    }

    public static void e(String tag, String string, Throwable throwable) {
        com.viettel.util.Log.e(tag, string, throwable);
    }

    public static void e(String tag, Throwable throwable) {
        if (throwable != null)
            com.viettel.util.Log.e(tag, throwable.getMessage(), throwable);
    }

    public static void d(String tag, String string) {
        com.viettel.util.Log.d(tag, string);
    }

    public static void d(String tag, String string, Throwable throwable) {
        com.viettel.util.Log.d(tag, string, throwable);
    }

    public static void v(String tag, String string) {
        com.viettel.util.Log.v(tag, string);
    }

    public static void v(String tag, String string, Throwable throwable) {
        com.viettel.util.Log.v(tag, string, throwable);
    }

    public static void w(String tag, String string) {
        com.viettel.util.Log.w(tag, string);
    }

    public static void w(String tag, String string, Throwable throwable) {
        com.viettel.util.Log.w(tag, string, throwable);
    }

    public static void f(String tag, String string) {
        com.viettel.util.Log.f(tag, string);
    }

    public static void f(String tag, String string, Throwable throwable) {
        com.viettel.util.Log.f(tag, string, throwable);
    }

    public static void sys(String tag, String string) {
        System.out.println(tag + "," + string);
    }

    public static void sys(String tag, String string, boolean isFile) {
        if (isFile)
            com.viettel.util.Log.f(tag, string);
        System.out.println(tag + "," + string);
    }
}
package com.metfone.selfcare.util.contactintergation;

/**
 * Created by thanhnt72 on 9/23/2019.
 */

public class Constants {
    public static String ACCOUNT_TYPE = "com.metfone.selfcare.app";
    public static String ACCOUNT_NAME = "Mocha";

    public static String MIME_MOCHA_CHAT = "vnd.android.cursor.item/vnd.com.metfone.selfcare.chat";

    public static String MIME_MOCHA_CALL = "vnd.android.cursor.item/vnd.com.metfone.selfcare.call";
}

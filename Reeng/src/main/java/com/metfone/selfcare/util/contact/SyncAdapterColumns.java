package com.metfone.selfcare.util.contact;

/**
 * Created by thanhnt72 on 12/5/2017.
 */

public interface SyncAdapterColumns {

    String ACCOUNT_TYPE = "com.metfone.selfcare";

    String MIME_MOCHA_CHAT = "vnd.android.cursor.item/vnd.com.metfone.selfcare.chat";

    String MIME_MOCHA_CALL = "vnd.android.cursor.item/vnd.com.metfone.selfcare.call";

    String MIME_MOCHA_CALL_OUT = "vnd.android.cursor.item/vnd.com.metfone.selfcare.callout";

    String MIME_MOCHA_CALL_VIDEO = "vnd.android.cursor.item/vnd.com.metfone.selfcare.video";

    int MIME_COUNT = 4;
}

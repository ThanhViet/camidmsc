package com.metfone.selfcare.util.contactintergation;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.util.Log;

import static com.metfone.selfcare.helper.Config.Features.FLAG_SUPPORT_MERGE_CONTACT;

public class DeviceAccountManager {

    private static final String TAG = DeviceAccountManager.class.getSimpleName();

    private static DeviceAccountManager instance;

    private final String ACCOUNT_NAME;
    private final String ACCOUNT_TYPE;
    private final AccountManager accountManager;
    private Context context;

    private DeviceAccountManager(@NonNull final Context context) {
        ACCOUNT_NAME = Constants.ACCOUNT_NAME;
        ACCOUNT_TYPE = Constants.ACCOUNT_TYPE;
        accountManager = AccountManager.get(context);
        this.context = context;
    }

    public static DeviceAccountManager getInstance(@NonNull final Context context) {
        if (instance == null)
            instance = new DeviceAccountManager(context);
        return instance;
    }

    public void addAccountToDevice() {
        if (!ApplicationController.self().getReengAccountBusiness().isValidAccount() || ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
            return;

        if (FLAG_SUPPORT_MERGE_CONTACT) {
            if (!accountExists()) {
                addNewAccount();
            } else {
                Log.e(TAG, "accountExists");
            }
        }
    }


    private void addNewAccount() {
        Account account = new Account(ACCOUNT_NAME, ACCOUNT_TYPE);
        try {
            boolean tmp = accountManager.addAccountExplicitly(account, null, null);
            if (tmp) {
                ContentResolver.setSyncAutomatically(account, ContactsContract.AUTHORITY, true);
                ApplicationController.self().getContactBusiness().mergeContact();
            }
            Log.i(TAG, "addNewAccount success");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "addNewAccount fail: " + e.toString());
        }
    }

    private boolean accountExists() {

        for (Account account : accountManager.getAccounts()) {
            if (ACCOUNT_TYPE.equals(account.type)) {
                return true;
            }
        }
        return false;
    }

    public void removeAccount() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            return;
        }
        try {
            Account[] accounts = accountManager
                    .getAccountsByType(Constants.ACCOUNT_TYPE);
            if (null != accounts && accounts.length > 0) {
                Log.i(TAG, "size account  = " + accounts.length);
                for (Account account : accounts) {
                    Log.i(TAG, "account name: " + account.name);
                    if ((null != account) && !TextUtils.isEmpty(account.name)) {
                        accountManager.removeAccountExplicitly(
                                new Account(account.name, Constants.ACCOUNT_TYPE));
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception deleteallaccount", ex);
        }

    }
}

package com.metfone.selfcare.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.Filterable;
import com.viettel.util.LogDebugHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * dainv
 */
public class FilterContentSeenUtil {

    private List<String> listIdContentSeen;
    private SharedPrefs sharedPref;
    private String keySharePref;
    private static final int MIN_ITEM = 5;
    private static final int MAX_SIZE = 20;

    // key log
    private Gson gson;
    private String keyLog;

    public FilterContentSeenUtil(String keySharePreference) {
        sharedPref = SharedPrefs.getInstance();
        this.keySharePref = keySharePreference;
        listIdContentSeen = sharedPref.get(keySharePreference, new TypeToken<ArrayList<String>>() {
        }.getType());

        gson = new Gson();
        if (Constants.CacheSeen.CACHE_IDS_VIDEO.equals(keySharePreference)) {
            keyLog = "video cache: ";
        } else if (Constants.CacheSeen.CACHE_IDS_TIIN.equals(keySharePreference)) {
            keyLog = "tiin cache: ";
        } else {
            keyLog = "net_news cache: ";
        }
    }

    public <O extends Filterable> ArrayList<O> filterList(ArrayList<O> listItem) {
        if (listIdContentSeen == null
                || listItem == null
                || listItem.isEmpty()
                || listItem.size() <= MIN_ITEM) {
            return listItem;
        }
        ArrayList<O> tempList = new ArrayList<>();
        for (int i = listItem.size() - 1; i >= 0; i--) {
            if (listIdContentSeen.contains(listItem.get(i).getIdFilter())) {
                tempList.add(listItem.remove(i));
            }
        }
        if (listItem.size() < MIN_ITEM) {
            int index = tempList.size() - 1;
            while (!(listItem.size() == MIN_ITEM)) {
                listItem.add(tempList.get(index));
                index--;
            }
        }
        return listItem;
    }

    public boolean checkExist(String id) {
        return listIdContentSeen.contains(id);
    }

    public void addContentSeen(String t) {
        if (listIdContentSeen == null) {
            listIdContentSeen = new ArrayList<>();
        }
        if (listIdContentSeen.contains(t)) {
            listIdContentSeen.remove(t);
        } else {
            if (listIdContentSeen.size() >= MAX_SIZE) {
                listIdContentSeen.remove(0);
            }
        }
        listIdContentSeen.add(t);
        sharedPref.put(keySharePref, listIdContentSeen);
    }
}

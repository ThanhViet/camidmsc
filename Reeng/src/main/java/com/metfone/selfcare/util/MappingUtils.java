package com.metfone.selfcare.util;

import android.content.Context;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.model.District;
import com.metfone.selfcare.model.Province;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MappingUtils {

    public static UserIdentityInfo mapBCCSToCamID(Context context, UserIdentityInfo userInfo) {
        UserIdentityInfo copy = userInfo.copy();
        String sexCAM = "2";
        if (userInfo.getSex().toLowerCase(Locale.ROOT).equals("M".toLowerCase(Locale.ROOT))) {
            sexCAM = "0";
        } else if (userInfo.getSex().toLowerCase(Locale.ROOT).equals("F".toLowerCase(Locale.ROOT))) {
            sexCAM = "1";
        }
        copy.setProvince(mapProvinceToCamID(context, userInfo.getProvince()));
        copy.setDistrict(mapDistrictToCamID(context, userInfo.getDistrict(), userInfo.getProvince()));
        copy.setSex(sexCAM);
//        copy.setBirthDate(DateTimeUtils.dateStrToStringMP(userInfo.getBirthDate()));

        return copy;
    }

    private static String mapProvinceToCamID(Context context, String provinceCode) {
        List<Province> provinces = AssetsUtil.parseJsonToList(
                context, "address/province_cambodia.json", Province.class);

        for (Province p : provinces) {
            if (p.getCode().equals(provinceCode)) {
                return p.getTitle();
            }
        }
        return "";
    }

    public static String mapProvinceToCode(Context context, String provinceTitle) {
        List<Province> provinces = AssetsUtil.parseJsonToList(
                context, "address/province_cambodia.json", Province.class);

        for (Province p : provinces) {
            if (p.getTitle().equals(provinceTitle)) {
                return p.getCode();
            }
        }
        return "";
    }

    public static int mapNationality(Context context, String national) {
        String[] nationalities = context.getResources().getStringArray(R.array.nationality);
        List<String> list = Arrays.asList(nationalities);
        String nat = national.replace(" ", "").toLowerCase(Locale.ROOT);

        for (String n : list) {
            if (n.replace(" ", "").toLowerCase(Locale.ROOT).equals(nat)) {
                return list.indexOf(n);
            }
        }
        return 0;
    }

    public static String[] getProvinceList(Context context) {
        List<Province> provinces = AssetsUtil.parseJsonToList(
                context, "address/province_cambodia.json", Province.class);
        String lastItem = context.getString(R.string.hint_default_value);

        List<String> list = new ArrayList<>();
        for (Province p : provinces) {
            list.add(p.getTitle());
        }
        String[] itemsArray = new String[list.size() + 1];
        itemsArray = list.toArray(itemsArray);
        itemsArray[itemsArray.length - 1] = lastItem;
        return itemsArray;
    }

    public static int getProvinceIndexWithCode(Context context, String code) {
        List<Province> provinces = AssetsUtil.parseJsonToList(
                context, "address/province_cambodia.json", Province.class);

        for (Province p : provinces) {
            if (p.getCode().equals(code)) {
                return provinces.indexOf(p);
            }
        }
        return provinces.size();
    }

    public static int getProvinceIndexWithName(Context context, String name) {
        List<Province> provinces = AssetsUtil.parseJsonToList(
                context, "address/province_cambodia.json", Province.class);

        for (Province p : provinces) {
            if (p.getTitle().toLowerCase(Locale.ROOT).equals(name.toLowerCase(Locale.ROOT))) {
                return provinces.indexOf(p);
            }
        }
        return provinces.size();
    }

    public static String getProvinceCode(Context context, String province) {
        List<Province> provinces = AssetsUtil.parseJsonToList(
                context, "address/province_cambodia.json", Province.class);

        for (Province p : provinces) {
            if (p.getTitle().equals(province)) {
                return p.getCode();
            }
        }
        return "";
    }

    public static String mapDistrictToCamID(Context context, String districtCode, String provinceCode) {
        List<District> districts = AssetsUtil.parseJsonToList(
                context, "address/district_cambodia.json", District.class);

        for (District d : districts) {
            if (d.getProvinceCode().equals(provinceCode) && d.getCode().equals(districtCode)) {
                return d.getTitle();
            }
        }
        return "";
    }


    public static String[] getDistrictList(Context context, String provinceCode) {
        List<District> districts = AssetsUtil.parseJsonToList(
                context, "address/district_cambodia.json", District.class);

        List<String> list = new ArrayList<>();
        for (District d : districts) {
            if (d.getProvinceCode().equals(provinceCode)) {
                list.add(d.getTitle());
            }
        }
        String[] itemsArray = new String[list.size()];
        itemsArray = list.toArray(itemsArray);
        return itemsArray;
    }

    public static int getDistrictIndex(Context context, String provinceCode, String district) {
        List<District> districts = AssetsUtil.parseJsonToList(
                context, "address/district_cambodia.json", District.class);

        List<String> list = new ArrayList<>();
        for (District d : districts) {
            if (d.getProvinceCode().equals(provinceCode)) {
                list.add(d.getTitle());
            }
        }
        int index = list.indexOf(district);
        if (index > -1) {
            return index;
        }
        return 0;
    }

    public static String getDistrictCode(Context context, String district, String provinceCode) {
        List<District> districts = AssetsUtil.parseJsonToList(
                context, "address/district_cambodia.json", District.class);

        for (District d : districts) {
            if (d.getProvinceCode().equals(provinceCode) && d.getTitle().equals(district)) {
                return d.getCode();
            }
        }
        return "";
    }

    private static String mapNationalityToCamID(Context context, String nationality) {
        List<District> districts = AssetsUtil.parseJsonToList(
                context, "address/nationality.json", District.class);

        for (District d : districts) {
            if (d.getProvinceCode().equals(nationality) && d.getCode().equals(nationality)) {
                return d.getTitle();
            }
        }
        return "";
    }

    public static String getIDValue(Context context, int id) {
        HashMap<String, Integer> map = getMapListId(context);

        for (String data : map.keySet()) {
            if (map.get(data) == null) {
                return context.getResources().getStringArray(R.array.selected_id_option_bccs)[0];
            }
            if (map.get(data) == id) {
                return data;
            }
        }
        return context.getResources().getStringArray(R.array.selected_id_option_bccs)[0];
    }

    public static String getLang() {
        String lang = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
        if (!lang.equals("kh")) {
            lang = "en";
        }
        return lang;
    }

    public static String getIDType(Context context, String type) {
        HashMap<String, Integer> map = getMapListId(context);
        return String.valueOf(map.get(type));
    }

    public static String getIDTypeForCam(Context context, String type) {
        String[] arrayIds = context.getResources().getStringArray(R.array.selected_id_option);
        String[] types = context.getResources().getStringArray(R.array.selected_id_type_option);

        List<String> idValue = Arrays.asList(arrayIds);
        for (String s : idValue) {
            if (type.toLowerCase(Locale.ROOT).contains(s.toLowerCase(Locale.ROOT))) {
                return types[idValue.indexOf(s)];
            }
        }
        return types[3];
    }

    private static HashMap<String, Integer> getMapListId(Context context) {
        String[] arrayIds = context.getResources().getStringArray(R.array.selected_id_option_bccs);
        HashMap<String, Integer> map = new HashMap<>();
        map.put(arrayIds[0], 1);
        map.put(arrayIds[1], 6);
        map.put(arrayIds[2], 3);
        map.put(arrayIds[3], 7);
        map.put(arrayIds[4], 8);
        return map;
    }
}

package com.metfone.selfcare.util;

import android.graphics.drawable.Drawable;

import androidx.annotation.DrawableRes;
import androidx.annotation.IntDef;
import androidx.annotation.StringDef;
import androidx.annotation.StringRes;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class EnumUtils {
    public static int LOG_MODE = LogModeTypeDef.LOGIN;
    //set tab default home cinema
    public static int CURRENT_TAB_LOGIN = 2;
    public static int PRE_LOG_MODE = LOG_MODE;
    public static String RESULT = "RESULT";
    public static String SINGIN = "SIGNIN";
    public static int FROM_ADD_LOGIN_MODE = FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY;
    public static final int TIME_DEFAULT = 91000;
    public static final int MY_REQUEST_CODE  = 1;
    public static final String PHONE_NUMBER_KEY = "PHONE_NUMBER_KEY";// so nhan dien dc (dau vao
    public static final String OTP_KEY = "OTP_KEY";// so nhan dien dc (dau vao
    public static final String IS_FIRST_LOGIN_FACEBOOK = "IS_FIRST_LOGIN_FACEBOOK";// so nhan dien dc (dau vao
    public static final String IS_FIRST_LOGIN_GOOGLE = "IS_FIRST_LOGIN_GOOGLE";// so nhan dien dc (dau vao
    public static final String USER_INFO = "USER_INFO";
    public static final String ACCOUNT_RANK_DTO = "ACCOUNT_RANK_DTO";
    public static final String OTP_OLD_MOCHA_RESPONSE = "OTP_OLD_MOCHA_RESPONSE";
    public static final String TOKEN = "Bearer " +
            "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJLcjN2NUJ0U3lPNjlfLWlxYWhWWmtUQkFCdV9fU1hZaWlQYURXUXRteWZzIn0.eyJleHAiOjE2MDIxNDM3NTQsImlhdCI6MTU5OTU1MTc1NCwianRpIjoiY2FhMTliYTQtN2FmMy00YjI0LTkzN2YtZmIyNTdlYzBlOTA0IiwiaXNzIjoiaHR0cHM6Ly9tZXRmb25lLmluZm8vYXV0aC9yZWFsbXMvZGllbnBvYyIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJlNmYyODllYS03ZDYzLTQ5MjAtODU5YS1lMWVjZGM2MTM3YjIiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJjYW1pZCIsInNlc3Npb25fc3RhdGUiOiJiZGM1ZjBmMC05M2M1LTQ5NjEtOTUyOC1iMDNmZDc3YmM1MzYiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoidXNlckA2MjE1ZGYyZC1jOGU1LTRjZjMtYjE4Ny1jMzYzNjhmM2QxMDgifQ.NgAm1kI_I2m95PSGZs4aPiX9Ai7mU2QanEtMpB7EGu-7VvsGj0vIukw5bEy4ub9eBkxXStGoGVY0lSBjXv5cDKBzJPgPExd21W1DNIMfb7LHGSxWySf7RxjqzLxp3_odrWjcLsc6Av-0VRA7-I_u_mM_RtareeGynz506Hm8UsDXH1W1F3dMOgcj2gu7EwtkToV5AOWQslyyi7C96XUFXL2bWPYK2q62e3d2uDnigzDx6mSShitu48b4TIAsRpU1HZQBnO00D-htuB3IrxEib2QOmIhZTCbg8OfJEaK-aHm1wR-6iyN8rrBwhjyZv2uVEKCkfRViwMh27JYN6gchFw";
    public static final String SERVICE_LIST = "SERVICE_LIST";
    public static final String IDENTITY_PROVIDER_LIST = "IDENTITY_PROVIDER_LIST";
    public static final String OBJECT_KEY = "OBJECT_KEY";
    public static final String KEY_TYPE = "KEY_TYPE";
    public static final String CHECK_PHONE_CODE_TYPE = "CHECK_PHONE_CODE_TYPE";
    public static final String VERIFY_ID_NUMBER_TYPE = "VERIFY_ID_NUMBER_TYPE";
    public static final String LOGIN_TYPE = "LOGIN_TYPE";
    public static final String COMMENT = "COMMENT";
    public static final String RANK_ID = "RANK_ID";
    public static final String IS_VERIFY_SCREEN = "IS_VERIFY_SCREEN";
    public static final String IS_FROM_METFONE_TAB = "IS_FROM_METFONE_TAB";
    public static String numberInput;

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({ ServiceEnum.MOBILE, ServiceEnum.FTTH})
    public @interface ServiceEnum {
        String MOBILE = "MOBILE";
        String FTTH = "FTTH";
    }


    @Retention(RetentionPolicy.SOURCE)
    @StringDef({VerifyImageTypeDef.FRONT_SIDE, VerifyImageTypeDef.BACK_SIDE, VerifyImageTypeDef.YOUR_SELFIE})
    public @interface VerifyImageTypeDef {
        String FRONT_SIDE = "front_side";
        String BACK_SIDE = "back_side";
        String YOUR_SELFIE = "your_selfie";
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({IdNumberVerifyStatusTypeDef.INIT, IdNumberVerifyStatusTypeDef.PENDING, IdNumberVerifyStatusTypeDef.APPROVE, IdNumberVerifyStatusTypeDef.REJECT})
    public @interface IdNumberVerifyStatusTypeDef {
        String APPROVE = "APPROVED";
        String INIT = "INIT";
        String PENDING = "PENDING";
        String REJECT = "REJECT";
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({VerifyIDNumberTypeDef.CREATE, VerifyIDNumberTypeDef.UPDATE})
    public @interface VerifyIDNumberTypeDef {
        int CREATE = 0;
        int UPDATE = 1;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PhoneNumberTypeDef.ADD_LOGIN, PhoneNumberTypeDef.SIGN_UP})
    public @interface PhoneNumberTypeDef {
        int SIGN_UP = 0;
        int ADD_LOGIN = 1;
        int ADD_PHONE = 2;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LogModeTypeDef.SIGN_UP, LogModeTypeDef.LOGIN})
    public @interface LogModeTypeDef {
        int SIGN_UP = 0;
        int LOGIN = 1;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY, FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY})
    public @interface FromAddLoginModeTypeDef {
        int ADD_MORE_METHODS_ACTIVITY = 0;
        int SET_UP_PROFILE_ACTIVITY = 1;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({KeyBackTypeDef.BACK, KeyBackTypeDef.DONE})
    public @interface KeyBackTypeDef{
        int BACK = 0;
        int DONE = 1;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LoginModeTypeDef.PHONE_NUMBER, LoginModeTypeDef.FACEBOOK, LoginModeTypeDef.GOOGLE})
    public @interface LoginModeTypeDef{
        int PHONE_NUMBER = 0;
        int FACEBOOK = 1;
        int GOOGLE = 2;
    }

    public enum AvatarRank {
        MEMBER(1, R.drawable.ic_avatar_member, R.string.kh_user_rank_member),
        SILVER(2, R.drawable.ic_avatar_silver, R.string.kh_user_rank_silver),
        GOLD(3, R.drawable.ic_avatar_gold, R.string.kh_user_rank_gold),
        DIAMOND(4, R.drawable.ic_avatar_diamond, R.string.kh_user_rank_diamon),
        PLATINUM(5, R.drawable.ic_avatar_platinum, R.string.kh_user_rank_platinum);
        public final int id;
        public final Drawable drawable;
        public int resRankName;

        AvatarRank(int id, @DrawableRes int color, @StringRes int resRankName) {
            this.id = id;
            this.drawable = ResourceUtils.getDrawable(color);
            this.resRankName = resRankName;
        }

        public static AvatarRank getById(int id) {
            for (AvatarRank e : values()) {
                if (e.id == id) return e;
            }
            return null;
        }
    }

    public enum CrownRank {
        MEMBER(1, R.drawable.ic_crown_member),
        SILVER(2, R.drawable.ic_crown_silver),
        GOLD(3, R.drawable.ic_crown_gold),
        DIAMOND(4, R.drawable.ic_crown_diamond),
        PLATINUM(5, R.drawable.ic_crown_platinum);
        public final int id;
        public final Drawable drawable;

        CrownRank(int id, @DrawableRes int color) {
            this.id = id;
            this.drawable = ResourceUtils.getDrawable(color);
        }

        public static CrownRank getById(int id) {
            for (CrownRank e : values()) {
                if (e.id == id) return e;
            }
            return null;
        }
    }

    public enum LoginVia {
        MOCHA, OPEN_ID, MOCHA_OPEN_ID, NOT
    }
}

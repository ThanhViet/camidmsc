package com.metfone.selfcare.util.contact;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by thanhnt72 on 12/6/2017.
 */

public class AuthenticationService extends Service {

    private Authenticator mAuthenticator;

    @Override
    public void onCreate() {
        mAuthenticator = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}

package com.metfone.selfcare.util;

import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * @author ITSOL JAPAN
 * Created on 12/2/2020.
 * Copyright © 2020 YSL Solution Co., Ltd. All rights reserved.
 **/
public class Utils {
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static String getIpOf3G() {
        String ipAddress;
        String ip1 = "", ip2 = "", ip3 = "";
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();

                    if(intf.getName().equals("ccmni1") && inetAddress.getHostAddress().indexOf(':') < 0) {
                        ip1 = inetAddress.getHostAddress();
                        Log.e("Utils", "getIpOf3G: 1" + ip1);
                    }

                    if (!intf.getName().equals("lo") && inetAddress.getHostAddress().indexOf(':') < 0) {
                        ip2 = inetAddress.getHostAddress();
                        Log.e("Utils", "getIpOf3G: 2" + ip2);
                    }
                    if (intf.getName().equals("wlan0") && inetAddress.getHostAddress().indexOf(':') < 0) {
                        ip3 = inetAddress.getHostAddress();
                        Log.e("Utils", "getIpOf3G: 3" + ip3);
                    }
                }
            }

            if (!TextUtils.isEmpty(ip1)) {
                ipAddress = ip1;
            } else if(!TextUtils.isEmpty(ip2)) {
                ipAddress = ip2;
            } else{
                ipAddress = ip3;
            }
            return ipAddress;
        } catch (SocketException ex) {
            Log.e("Utils", "getIpOf3G: ", ex);
        } catch (Exception e) {
            Log.e("Utils", "getIpOf3G: ", e);
        }
        return "";
    }
}

package com.metfone.selfcare.ui.tabvideo.holder;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.VideoBannerItem;
import com.metfone.selfcare.module.video.model.VideoPagerModel;
import com.metfone.selfcare.util.Utilities;

import java.util.List;

import butterknife.BindView;

public class BannerHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.layout_content)
    RelativeLayout layoutContent;
    @BindView(R.id.btn_left)
    TextView btnLeft;
    @BindView(R.id.btn_right)
    TextView btnRight;

    BaseSlidingFragmentActivity mActivity;
    VideoBannerItem data;

    public BannerHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
        super(layoutInflater.inflate(R.layout.holder_banner_video_hot, parent, false));
        mActivity = (BaseSlidingFragmentActivity) activity;
        setSizeBanner();
        layoutContent.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && Utilities.notEmpty(data.getDeeplink())) {
                    DeepLinkHelper.getInstance().openSchemaLink(mActivity, data.getDeeplink(), "", null);
                }
            }
        });
    }

    private void setSizeBanner() {
        if (data != null) {
            try {
                float width = ScreenManager.getWidth(mActivity);
                float height = ScreenManager.getHeight(mActivity);
                width = Math.min(width, height);
                height = width / data.getAspectRatio();
                ViewGroup.LayoutParams layoutParams = layoutContent.getLayoutParams();
                layoutParams.width = (int) width;
                layoutParams.height = (int) height;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void bindData(Object items, int position) {
        super.bindData(items, position);
        if (items instanceof VideoPagerModel) {
            data = (VideoBannerItem) ((VideoPagerModel) items).getObject();
            setSizeBanner();
            btnLeft.setEnabled(false);
            btnRight.setEnabled(false);
            if (data != null) {
                ImageManager.showImageRoundV2(data.getItemImage(), ivCover);
                List<VideoBannerItem> list = data.getListSubItems();
                if (Utilities.notEmpty(list)) {
                    if (list.size() >= 2) {
                        final VideoBannerItem itemLeft = list.get(0);
                        final VideoBannerItem itemRight = list.get(1);
                        if (itemLeft == null) {
                            btnLeft.setVisibility(View.INVISIBLE);
                        } else {
                            btnLeft.setEnabled(true);
                            btnLeft.setVisibility(View.VISIBLE);
                            btnLeft.setText(itemLeft.getItemTitle());
                            btnLeft.setOnClickListener(new OnSingleClickListener() {
                                @Override
                                public void onSingleClick(View view) {
                                    if (itemLeft != null && Utilities.notEmpty(itemLeft.getDeeplink())) {
                                        DeepLinkHelper.getInstance().openSchemaLink(mActivity, itemLeft.getDeeplink(), "", null);
                                    }
                                }
                            });
                        }
                        if (itemRight == null) {
                            btnRight.setVisibility(View.INVISIBLE);
                        } else {
                            btnRight.setEnabled(true);
                            btnRight.setVisibility(View.VISIBLE);
                            btnRight.setText(itemRight.getItemTitle());
                            btnRight.setOnClickListener(new OnSingleClickListener() {
                                @Override
                                public void onSingleClick(View view) {
                                    if (itemRight != null && Utilities.notEmpty(itemRight.getDeeplink())) {
                                        DeepLinkHelper.getInstance().openSchemaLink(mActivity, itemRight.getDeeplink(), "", null);
                                    }
                                }
                            });
                        }
                    } else {
                        btnLeft.setVisibility(View.INVISIBLE);
                        final VideoBannerItem itemRight = list.get(0);
                        if (itemRight == null) {
                            btnRight.setVisibility(View.INVISIBLE);
                        } else {
                            btnRight.setEnabled(true);
                            btnRight.setVisibility(View.VISIBLE);
                            btnRight.setText(itemRight.getItemTitle());
                            btnRight.setOnClickListener(new OnSingleClickListener() {
                                @Override
                                public void onSingleClick(View view) {
                                    if (itemRight != null && Utilities.notEmpty(itemRight.getDeeplink())) {
                                        DeepLinkHelper.getInstance().openSchemaLink(mActivity, itemRight.getDeeplink(), "", null);
                                    }
                                }
                            });
                        }
                    }
                } else {
                    btnLeft.setVisibility(View.INVISIBLE);
                    btnRight.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}

package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.util.Log;

import me.grantland.widget.AutofitTextView;

/**
 * Created by thanhnt72 on 1/30/2018.
 */

public class PopupReceiveLixi extends Dialog {

    private static final String TAG = PopupReceiveLixi.class.getSimpleName();
    private ApplicationController mApp;
    private BaseSlidingFragmentActivity mActivity;

    private ThreadMessage threadMessage;
    private ReengMessage mReengMessage;

    private View mViewParent;
    private TextView mTvwMsgLixi, mTvwLixiFrom;
    private AutofitTextView mTvwAmountMoney;
    private TextView mTvwMsgLixiEmpty;
    private RoundLinearLayout mBtnShareFb;

    private int width, height;
    private LixiListener mListener;


    public PopupReceiveLixi(BaseSlidingFragmentActivity mActivity, ThreadMessage threadMessage, ReengMessage message,
                            LixiListener mListener) {
        super(mActivity, R.style.DialogFullscreen);
        mApp = (ApplicationController) mActivity.getApplication();
        this.mActivity = mActivity;
        this.threadMessage = threadMessage;
        mReengMessage = message;
        this.mListener = mListener;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_open_gift_lixi);
        mViewParent = findViewById(R.id.view_parent);
        mTvwMsgLixi = (TextView) findViewById(R.id.tvw_msg_lixi);
        mTvwAmountMoney = (AutofitTextView) findViewById(R.id.tvw_amount_money_lixi);
        mTvwMsgLixiEmpty = (TextView) findViewById(R.id.tvw_msg_lixi_empty);
        mBtnShareFb = (RoundLinearLayout) findViewById(R.id.btn_share_fb);
        mTvwLixiFrom = (TextView) findViewById(R.id.tvw_lixi_from);

        setCloseWhenTouchListener(mViewParent);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        drawDetail();

        mBtnShareFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onShare(getBitmapFromView());
//                    mListener.onShare(loadBitmapFromView(mFraAvatar));
                }
                dismiss();
            }
        });

        mViewParent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // Ensure you call it only once :
                mViewParent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                width = mViewParent.getWidth();
                height = mViewParent.getHeight();
                Log.i(TAG, "width: " + width + " height: " + height);
                // Here you can get the size :)
            }
        });
    }


    private Bitmap getBitmapFromView() {
        mViewParent.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.bg_color_gift_lixi));
        mBtnShareFb.setVisibility(View.GONE);
        mTvwMsgLixiEmpty.setVisibility(View.GONE);
        Bitmap scaledBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(scaledBitmap);
        mViewParent.draw(canvas);
        return scaledBitmap;
    }

    private void drawDetail() {
        mTvwMsgLixi.setText(mReengMessage.getContent());
        if (threadMessage != null) {
            String name;
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                PhoneNumber phoneNumber = mApp.getContactBusiness().getPhoneNumberFromNumber(mReengMessage
                        .getSender());
                if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                    name = phoneNumber.getName();
                } else {                      //hien thi so dien thoai neu la group chat
                    name = mReengMessage.getSender();
                }
            } else {
                name = threadMessage.getThreadName();
            }

            if (TextUtils.isEmpty(mReengMessage.getImageUrl()) || "0".equals(mReengMessage.getImageUrl())) {
                mTvwAmountMoney.setVisibility(View.GONE);
                mTvwLixiFrom.setVisibility(View.GONE);
                String content = String.format(mApp.getResources().getString(R.string.note_receive_lixi), name);
                mTvwMsgLixiEmpty.setText(content);
                mTvwMsgLixiEmpty.setVisibility(View.VISIBLE);
            } else {
                mTvwAmountMoney.setVisibility(View.VISIBLE);
                mTvwLixiFrom.setVisibility(View.VISIBLE);
                mTvwMsgLixiEmpty.setVisibility(View.GONE);
                String amountMoney = TextHelper.formatTextDecember(mReengMessage.getImageUrl());
                mTvwAmountMoney.setText(String.format(mActivity.getResources().getString(R.string.unit_vnd), amountMoney));
                mTvwLixiFrom.setText(String.format(mActivity.getResources().getString(R.string.lixi_from), name));
            }



        } else {
            Log.e(TAG, "threadMessage null");
            mTvwMsgLixiEmpty.setVisibility(View.GONE);
            mTvwAmountMoney.setVisibility(View.GONE);
            mTvwLixiFrom.setVisibility(View.GONE);
        }



    }

    private void setCloseWhenTouchListener(View rootView) {
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                dismiss();
                return false;
            }
        });
    }

    public interface LixiListener {
        //void onShare(String filePath);
        void onShare(Bitmap bitmap);

    }

}

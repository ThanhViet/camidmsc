/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.ui.dialog;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.widget.FrameLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.util.Log;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import static com.google.android.material.bottomsheet.BottomSheetBehavior.from;

public class BottomSheetDialog extends com.google.android.material.bottomsheet.BottomSheetDialog {
    protected BaseSlidingFragmentActivity activity;
    private KeyboardVisibilityEventListener keyboardVisibilityListener;
    private Unregistrar unregistrar;

    public BottomSheetDialog(@NonNull Context context) {
        super(context, R.style.style_bottom_sheet_dialog_v2);
        if (context instanceof BaseSlidingFragmentActivity) {
            activity = (BaseSlidingFragmentActivity) context;
        }
    }

    public BottomSheetDialog(@NonNull Context context, int theme) {
        super(context, theme);
    }

    protected BottomSheetDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void setKeyboardVisibilityListener(KeyboardVisibilityEventListener listener) {
        this.keyboardVisibilityListener = listener;
    }

    @Override
    public void show() {
        super.show();
        if (activity != null && keyboardVisibilityListener != null) {
            unregistrar = KeyboardVisibilityEvent.registerEventListener(activity, keyboardVisibilityListener);
        }
    }

    @Override
    public void dismiss() {
        Log.d("ShareBottomDialog", "dismiss");
        if (unregistrar != null) unregistrar.unregister();
        super.dismiss();
    }

    @Nullable
    public BottomSheetBehavior getBottomSheetBehavior() {
        BottomSheetBehavior behavior = null;
        try {
            FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
            if (bottomSheet != null) behavior = from(bottomSheet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return behavior;
    }

}

package com.metfone.selfcare.ui.chatviews;

import android.content.Intent;
import androidx.viewpager.widget.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.StickerActivity;
import com.metfone.selfcare.adapter.BaseEmoGridAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.controllers.EmojiAndStickerController;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.ui.viewpagerindicator.TabPageIndicator;

import java.util.ArrayList;

/**
 * Created by thaodv on 5/28/2015.
 */
public class ChatStickerView {
    private static final String TAG = ChatStickerView.class.getSimpleName();
    private View emoticonView;
    private ViewPager pager;
    private TabPageIndicator mIndicator;
    private ImageView emoticonDelete;
    private ImageButton mBtnUserSticker;
    private EmojiAndStickerController emojiAndStickerController;
    private BaseSlidingFragmentActivity mActivity;
    private BaseEmoGridAdapter.KeyClickListener mKeyClickListener;
    private ViewPager.OnPageChangeListener changeListener;
    private TextView mTvNewSticker;
    private EditText mEdtContent;
    private int threadType;
    public ChatStickerView(BaseSlidingFragmentActivity activity, View parentLayout,
                           BaseEmoGridAdapter.KeyClickListener keyClickListener,
                           ViewPager.OnPageChangeListener pageChangeListener, EditText pEdtContent,
                           int threadType) {
        emoticonView = parentLayout;
        mActivity = activity;
        this.mEdtContent = pEdtContent;
        this.threadType = threadType;
        pager = (ViewPager) emoticonView.findViewById(R.id.chat_detail_emoticons_pager);
        mIndicator = (TabPageIndicator) emoticonView.findViewById(R.id.indicator);
        mBtnUserSticker = (ImageButton) emoticonView.findViewById(R.id.sticker_more);
        emoticonDelete = (ImageView) emoticonView.findViewById(R.id.emoticon_delete);
        mKeyClickListener = keyClickListener;
        changeListener = pageChangeListener;
        mTvNewSticker = (TextView) emoticonView.findViewById(R.id.txtNewSticker);
//        if(threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT){
//            emoticonView.findViewById(R.id.layout_emoticon_control).setVisibility(View.GONE);
//        }
        setListeners();
    }

    public void onResume() {
        if (emojiAndStickerController != null) emojiAndStickerController.refreshViewPager();
    }

    private void setListeners() {
        mIndicator.setOnPageChangeListener(changeListener);
        emojiAndStickerController = new EmojiAndStickerController(mActivity, mKeyClickListener,
                pager, mIndicator, mTvNewSticker, mEdtContent, threadType);
        emojiAndStickerController.initViewPagerController();
        mBtnUserSticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity.getApplicationContext(), StickerActivity.class);
                intent.putExtra(Constants.STICKER.FRAGMENT_TYPE, Constants.STICKER.STORE);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivityForResult(intent, Constants.ACTION.SET_DOWNLOAD_STICKER);
            }
        });

//        mBtnStickerStore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mActivity.getApplicationContext(), StickerActivity.class);
//                intent.putExtra(Constants.STICKER.FRAGMENT_TYPE, Constants.STICKER.STORE);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                mActivity.startActivityForResult(intent, Constants.ACTION.SET_DOWNLOAD_STICKER);
//            }
//        });
        emoticonDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                dismissStickerPopupWindow();
//                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
//                        0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
//                etMessageContent.dispatchKeyEvent(event);
            }
        });
        emoticonDelete.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
//                dismissStickerPopupWindow();
//                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
//                        0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
//                etMessageContent.dispatchKeyEvent(event);
                return true;
            }
        });
        pager.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
////                mKeyboardController.hideSoftAndCustomKeyboard(); ko hide ban phim khi touch
//                dismissMoreOptionPopupWindow();
//                dismissStickerPopupWindow();
//                dismissPopupMoreMsgUnread();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
//                        Log.i(TAG, "pager onTouch ACTION_DOWN");
//                        mChatActivity.getSlidingMenu().setSlidingEnabled(false);
                        break;
                    case MotionEvent.ACTION_MOVE:
//                        Log.i(TAG, "pager onTouch ACTION_MOVE");
//                        mChatActivity.getSlidingMenu().setSlidingEnabled(false);
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    public void notifyDataSetChanged() {
        if (emojiAndStickerController != null) {
            emojiAndStickerController.notifyDataSetChanged();
        }
    }

    public void notifyChangeRecentSticker(ArrayList<StickerItem> listItems) {
        if (emojiAndStickerController != null) {
            emojiAndStickerController.notifyChangeRecentSticker(listItems);
        }
    }
}

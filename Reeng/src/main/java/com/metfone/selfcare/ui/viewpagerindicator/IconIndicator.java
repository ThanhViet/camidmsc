package com.metfone.selfcare.ui.viewpagerindicator;

/**
 * Created by thanh on 2/12/2015.
 */

public class IconIndicator {
    public static final int TYPE_INDICATOR_RES = 0;
    public static final int TYPE_INDICATOR_URL = 1;
    private int idResIndicator;
    private int idResIndicatorSelected;
    private String urlIndicator;
    private String urlIndicatorSelected;
    private int typeResource;

    public String getUrlIndicatorSelected() {
        return this.urlIndicatorSelected;
    }

    public void setUrlIndicatorSelected(String urlIndicatorSelected) {
        this.urlIndicatorSelected = urlIndicatorSelected;
    }

    public IconIndicator() {
    }

    public IconIndicator(int idResIndicator, String urlIndicator, int typeResource) {
        this.idResIndicator = idResIndicator;
        this.urlIndicator = urlIndicator;
        this.typeResource = typeResource;
    }

    public IconIndicator(int idResIndicator, int idResSIndicator) {
        this.idResIndicator = idResIndicator;
        this.idResIndicatorSelected = idResSIndicator;
        this.urlIndicator = null;
        this.urlIndicatorSelected = null;
        this.typeResource = 0;
    }

    public IconIndicator(String urlIndicator, String urlSIndicator) {
        this.idResIndicator = 0;
        this.idResIndicatorSelected = 0;
        this.urlIndicator = urlIndicator;
        this.urlIndicatorSelected = urlSIndicator;
        this.typeResource = 1;
    }

    public int getIdResIndicator() {
        return this.idResIndicator;
    }

    public void setIdResIndicator(int idResIndicator) {
        this.idResIndicator = idResIndicator;
    }

    public String getUrlIndicator() {
        return this.urlIndicator;
    }

    public void setUrlIndicator(String urlIndicator) {
        this.urlIndicator = urlIndicator;
    }

    public int getTypeResource() {
        return this.typeResource;
    }

    public void setTypeResource(int typeResource) {
        this.typeResource = typeResource;
    }

    public int getIdResIndicatorSelected() {
        return this.idResIndicatorSelected;
    }

    public void setIdResIndicatorSelected(int idResIndicatorSelected) {
        this.idResIndicatorSelected = idResIndicatorSelected;
    }
}
package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.youtubes;

import com.metfone.selfcare.di.BaseView;

import java.util.List;

public interface UploadVideoYoutubeView extends BaseView {
    void updateDataCategories(List<String> categories);

    void showMessage(int urlVideoNotEmpty);

    void showMessage(String s);

    void showDialogLoading();

    void hideDialogLoading();

    void finish();

    void showMessageCategory();

    void showSuccess();
}

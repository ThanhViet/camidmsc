package com.metfone.selfcare.ui.dialog;

/**
 * Created by toanvk2 on 8/7/2017.
 */

public interface BottomSheetListener {
    void onItemClick(int itemId, Object entry);
}

package com.metfone.selfcare.ui;

import android.content.Context;
import android.graphics.Canvas;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by toanvk2 on 1/6/15.
 */
public class IndexableListView extends RecyclerView {
    private static final String TAG = IndexableListView.class.getSimpleName();
    private boolean isVisibleScroller = true;
    private boolean isSearchEntry = false;
    private boolean isFastScrollEnabled = false;
    private IndexScroller mScroller = null;
    private GestureDetector mGestureDetector = null;
    private int oldOrientation = 0;

    public IndexableListView(Context context) {
        super(context);
    }

    public IndexableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IndexableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void releaseListView() {
        if (mScroller != null) {
            mScroller.hide();
            mScroller = null;
            mGestureDetector = null;
        }
    }

    /**
     * search co ket qua set =true, khong co ket qua set =false
     *
     * @param isSearchEntry
     */
    public void setSearchEntry(boolean isSearchEntry) {
        this.isSearchEntry = isSearchEntry;
        // neu co noi dung tim kiem thi an thanh scroll
        setVisibleScroller(!isSearchEntry);
    }

    private void setVisibleScroller(boolean isVisibleScroller) {
        this.isVisibleScroller = isVisibleScroller;
        if (!isVisibleScroller && mScroller != null) {
            mScroller.invisible();
        }
    }

    public void changeAdapter(Adapter adapter) {
        if (mScroller != null) {
            mScroller.setAdapter(adapter);
        }
    }

    // @Override TODO check sau
    public boolean isFastScrollEnabled() {
        return isFastScrollEnabled;
    }

    //@Override TODO check sau
    public void setFastScrollEnabled(boolean enabled) {
        // TODO always  off
        /*isFastScrollEnabled = false;
        //isFastScrollEnabled = enabled;
        if (isFastScrollEnabled) {
            if (mScroller == null) {
                mScroller = new IndexScroller(getContext(), this);
            }
        } else {*/
            if (mScroller != null) {
                mScroller.hide();
                mScroller = null;
            }
        //}
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        // Overlay index bar
        if (mScroller != null) {
            mScroller.draw(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // Intercept ListView's touch event
        //        Log.d(TAG, "onTouchEvent: ");
        if (mScroller != null && mScroller.onTouchEvent(ev)) {
            //            Log.d(TAG, "onTouchEvent: mScroller != null");
            return true;
        }
        if (mGestureDetector == null) {
            mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2,
                                       float velocityX, float velocityY) {
                    //                    Log.d(TAG, "onTouchEvent: onFling   isFastScrollEnabled--> " + isFastScrollEnabled);
                    if (isVisibleScroller && mScroller != null) {
                        //                        Log.d(TAG, "onTouchEvent: onFling   show scroll-->");
                        mScroller.show();
                    }
                    return super.onFling(e1, e2, velocityX, velocityY);
                }
            });
        }
        mGestureDetector.onTouchEvent(ev);
        return super.onTouchEvent(ev);
    }

   /* @Override
    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);
        if (mScroller != null) {
            mScroller.setAdapter(adapter);
        }
    }*/

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Context context = getContext();
        int orientation;
        if (context != null) {
            orientation = getContext().getResources().getConfiguration().orientation;
        } else {
            orientation = oldOrientation;
        }
        if (orientation != oldOrientation) {
            setVisibleScroller(!isSearchEntry);
        } else {
            if (oldh >= (h + 100)) { // height giam > 100 px-> bat ban phim
                setVisibleScroller(false);
            } else {
                // neu co noi dung tim kiem thi an thanh scroll
                setVisibleScroller(!isSearchEntry);
            }
        }
        if (mScroller != null) {
            mScroller.onSizeChanged(w, h, oldw, oldh);
        }
        oldOrientation = orientation;
    }
}
package com.metfone.selfcare.ui.chatviews;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.adapter.OfficialActionAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.OfficialActionManager;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.bot.Action;
import com.metfone.selfcare.database.model.bot.OfficialAction;
import com.metfone.selfcare.ui.ScreenStateInfo;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 1/17/2017.
 */
public class OfficialActionView implements View.OnClickListener {
    private static final String TAG = ChatVoicemailView.class.getSimpleName();
    private ApplicationController mApplication;
    private ChatActivity mActivity;
    private Fragment mParentFragment;
    private OfficialActionManager mActionManager;
    private ScreenStateInfo mScreenStateInfo;
    private RecyclerView mRecyclerView;
    private OfficialActionAdapter mAdapter;
    private View mPgrLoading;
    private Button mBtnRetry;
    private int threadId;
    private String officialServerId = "";
    private OfficialAction officialAction;

    public OfficialActionView(ChatActivity activity, ScreenStateInfo screenStateInfo, Fragment parentFragment, int threadId) {
        mActivity = activity;
        mApplication = (ApplicationController) activity.getApplication();
        mScreenStateInfo = screenStateInfo;
        this.threadId = threadId;
        this.mParentFragment = parentFragment;
        mActionManager = OfficialActionManager.getInstance(mApplication);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.official_action_retry:
                mRecyclerView.setVisibility(View.GONE);
                mPgrLoading.setVisibility(View.VISIBLE);
                mBtnRetry.setVisibility(View.GONE);
                ThreadMessage thread = mApplication.getMessageBusiness().findThreadByThreadId(threadId);
                mActionManager.checkAndGetDefaultOfficialAction(thread);
                break;
        }
    }

    public void setChatOAActionView(View oaActionView) {
        mRecyclerView = (RecyclerView) oaActionView.findViewById(R.id.official_action_recycler);
        mPgrLoading =  oaActionView.findViewById(R.id.progress_loading);
        mBtnRetry = (Button) oaActionView.findViewById(R.id.official_action_retry);
        //changeLayoutParamProgress(mScreenStateInfo.getScreenOrientation());
        mBtnRetry.setOnClickListener(this);
    }

    /**
     * show lan dau, show nhưng lan sau, hoac notify change data
     */
    public void showActionView() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                officialAction = mActionManager.findOfficialAction(getOfficialServerId());
                if (officialAction == null) {
                    mRecyclerView.setVisibility(View.GONE);
                    if (mActionManager.isRequestFailed(getOfficialServerId())) {
                        mPgrLoading.setVisibility(View.GONE);
                        mBtnRetry.setVisibility(View.VISIBLE);
                    } else {
                        mBtnRetry.setVisibility(View.GONE);
                        mPgrLoading.setVisibility(View.VISIBLE);
                    }
                } else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mBtnRetry.setVisibility(View.GONE);
                    if (officialAction.isShowCurrentAction()) {
                        setAdapter(officialAction.getCurrentActions());
                    } else {
                        setAdapter(officialAction.getDefaultActions());
                    }
                    if (officialAction.isOptionRequesting()) {// dang request thi ko xu ly su kien
                        mPgrLoading.setVisibility(View.VISIBLE);
                    } else {
                        mPgrLoading.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void onBackPress() {
        officialAction = mActionManager.findOfficialAction(getOfficialServerId());
        if (officialAction == null) {
            mRecyclerView.setVisibility(View.GONE);
            mPgrLoading.setVisibility(View.VISIBLE);
        } else {
            officialAction.onBackPress();
            mRecyclerView.setVisibility(View.VISIBLE);
            mPgrLoading.setVisibility(View.GONE);
            setAdapter(officialAction.getDefaultActions());
        }
    }

    public void onResume() {

    }

    private void setAdapter(ArrayList<Action> listActions) {
        if (mAdapter == null) {
            mAdapter = new OfficialActionAdapter(mApplication);
            mAdapter.setActionList(listActions);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
            //mRecyclerView.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
            setItemViewListener();
        } else {
            mAdapter.setActionList(listActions);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setItemViewListener() {
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                Action action = (Action) object;
                if (officialAction != null && officialAction.isOptionRequesting()) {// dang request thi ko xu ly su kien
                    return;
                }
                if (action.isActionBack()) {
                    onBackPress();
                } else {
                    mActionManager.handleClickOptionAction(getOfficialServerId(), action, mParentFragment);
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private String getOfficialServerId() {
        if (TextUtils.isEmpty(officialServerId)) {
            ThreadMessage thread = mApplication.getMessageBusiness().findThreadByThreadId(threadId);
            if (thread != null && thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
                officialServerId = thread.getServerId();
            }
        }
        return officialServerId;
    }
}
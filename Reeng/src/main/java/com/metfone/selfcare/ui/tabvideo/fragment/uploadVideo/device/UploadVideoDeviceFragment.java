package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.device;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.ui.UploadSongPopupFragment;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.NotifyUploadVideoOnMediaDialog;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

@SuppressLint("SetTextI18n")
public class UploadVideoDeviceFragment extends BaseFragment implements
        UploadVideoDeviceView,
        AdapterView.OnItemSelectedListener {

    private static final int SELECT_VIDEO = 1000;
    private static final String TAG = "UploadVideoDeviceFragme";

    @BindView(R.id.tvPathVideo)
    TextView tvPathVideo;
    @BindView(R.id.rePathVideo)
    LinearLayout rePathVideo;
    @BindView(R.id.tvLimitTitleVideo)
    TextView tvLimitTitleVideo;
    @BindView(R.id.edTitleVideo)
    EditText edTitleVideo;
    @BindView(R.id.tvLimitDescriptionVideo)
    TextView tvLimitDescriptionVideo;
    @BindView(R.id.edDescriptionVideo)
    EditText edDescriptionVideo;
    @BindView(R.id.spCategory)
    Spinner spCategory;
    @BindView(R.id.tvNotifyCategoryEmpty)
    TextView tvNotifyCategoryEmpty;
    @BindView(R.id.btnUpload)
    Button btnUpload;
    @BindView(R.id.ivPathVideo)
    Button ivPathVideo;
    @BindView(R.id.checkbox_upload_onmedia)
    CheckBox checkboxUploadOnmedia;
    @BindView(R.id.tv_title)
    TextView tvTitleVideo;
    @BindView(R.id.tv_desc)
    TextView tvDescVideo;

    Unbinder unbinder;

    @Inject
    UploadVideoDevicePresenter uploadVideoDevicePresenter;

    @Inject
    VideoApi videoApi;
    @BindView(R.id.tvTerm)
    TextView tvTerm;

    private boolean finish = false;
    private boolean start = true;
    private String url;
    private UploadSongPopupFragment uploadSongPopupFragment;
    private DialogConfirm dialogConfirmExit;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload_video_device, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        uploadVideoDevicePresenter.getCategories();
        checkboxUploadOnmedia.setChecked(true);
        checkboxUploadOnmedia.setOnCheckedChangeListener(mOnCheckedChangeListener);
        setOnDescriptionVideoListener();
        setOnTitleVideoListener();
//        tvTerm.setPaintFlags(tvTerm.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvTerm.setText(Html.fromHtml(getString(R.string.term_upload_video_get_money)));
    }

    private void setOnTitleVideoListener() {
        edTitleVideo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvLimitTitleVideo.setText(edTitleVideo.getText().length() + "/" + 50);
            }
        });
        edTitleVideo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (tvTitleVideo != null && edTitleVideo != null) {
                        tvTitleVideo.setVisibility(View.VISIBLE);
                        edTitleVideo.setHint("");
                    }
                }
            }
        });
    }

    private void setOnDescriptionVideoListener() {
        edDescriptionVideo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvLimitDescriptionVideo.setText(edDescriptionVideo.getText().length() + "/" + 200);
            }
        });
        edDescriptionVideo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (tvDescVideo != null && edDescriptionVideo != null) {
                        tvDescVideo.setVisibility(View.VISIBLE);
                        edDescriptionVideo.setHint("");
                    }
                }
            }
        });
    }

    @Override
    public void updateDataCategories(List<String> categories) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(activity, R.layout.simple_list_item_categories, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter(dataAdapter);
        spCategory.setOnItemSelectedListener(this);
    }

    @Override
    public void showMessage(int message) {
        activity.showToast(message);
    }

    @Override
    public void hideLoading() {
        activity.hideLoadingDialog();
    }

    @Override
    public void showMessage(String s) {
        activity.showToast(s);
    }

    @Override
    public void hideDialogLoading() {
        if (runnable && uploadSongPopupFragment != null) {
            uploadSongPopupFragment.dismissAllowingStateLoss();
            uploadSongPopupFragment.dismiss();
            uploadSongPopupFragment = null;
        }
    }

    @Override
    public void showDialogLoading() {
        uploadSongPopupFragment = UploadSongPopupFragment.newInstance(getString(R.string.download_video), false, new UploadSongPopupFragment.OnClick() {
            @Override
            public void onBtnYesClicked(MediaModel songModel) {

            }

            @Override
            public void onBtnNoClicked() {
                openConfig();
            }
        }, null);
        uploadSongPopupFragment.show(getChildFragmentManager(), TAG);
    }

    private void openConfig() {
        dialogConfirmExit = new DialogConfirm(activity, false);
        dialogConfirmExit.setLabel(getString(R.string.stop_upload_video));
        dialogConfirmExit.setMessage(getString(R.string.stop_upload_video_desc));
        dialogConfirmExit.setNegativeLabel(getString(R.string.btn_stop_upload_negative));
        dialogConfirmExit.setPositiveLabel(getString(R.string.btn_stop_upload_positive));
        dialogConfirmExit.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                if (uploadVideoDevicePresenter == null) return;
                uploadVideoDevicePresenter.dismissUpload();
            }
        });
        dialogConfirmExit.show();
    }

    @Override
    public void updateProgress(int percentage) {
        if (uploadSongPopupFragment == null) return;
        uploadSongPopupFragment.setPercent(percentage);
    }

    @Override
    public void showDialogSuccess() {
        if (dialogConfirmExit != null) dialogConfirmExit.dismiss();
        if (uploadSongPopupFragment != null) uploadSongPopupFragment.dismiss();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
        ToastUtils.showToastSuccess(activity, getString(R.string.upload_video_success));
//        DialogConfirm dialogConfirm = new DialogConfirm(activity, false);
//        dialogConfirm.setLabel(getString(R.string.notification));
//        dialogConfirm.setMessage(getString(R.string.upload_video_success));
//        dialogConfirm.setPositiveLabel(getString(R.string.close));
//        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
//
//            @Override
//            public void onPositive(Object result) {
//                finish();
//            }
//        });
//        dialogConfirm.setDismissListener(new DismissListener() {
//
//            @Override
//            public void onDismiss() {
//                finish();
//            }
//        });
//        dialogConfirm.show();
    }

    @Override
    public void finish() {
        if (finish) return;
        finish = true;
        activity.finish();
    }

    @Override
    public void showMessageCategory() {
//        tvNotifyCategoryEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ivPathVideo, R.id.btnUpload, R.id.tvTerm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivPathVideo:
                if (PermissionHelper.checkPermissionStorage((BaseSlidingFragmentActivity) getActivity())) {
                    showFileChooser();
                }
                break;
            case R.id.btnUpload:
                uploadVideoDevicePresenter.uploadVideo(checkboxUploadOnmedia.isChecked(), edTitleVideo.getText().toString(),
                        edDescriptionVideo.getText().toString(),
                        url, spCategory.getSelectedItem());
                break;
            case R.id.tvTerm:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://survey?ref=http://m.video.mocha.com.vn/theleupload.html");
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_VIDEO) {
                try {
                    Uri uri = data.getData();
                    if (uri != null && isVideoFile(uri)) {
                        url = getPath(uri);
                        if (tvPathVideo != null) tvPathVideo.setText(new File(url).getName());
                        if (ivPathVideo != null) {
                            ivPathVideo.setText(getResources().getString(R.string.change_file_video));
                            ivPathVideo.setTextColor(activity.getResources().getColor(R.color.v5_text));
                            ivPathVideo.setActivated(true);
                        }
                        if(btnUpload != null){
                            btnUpload.setEnabled(true);
                            btnUpload.setTextColor(activity.getResources().getColor(R.color.white));
                        }
                    } else {
                        showMessage(R.string.the_link_you_selected_is_not_a_video);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        }
    }

    public boolean isVideoFile(Uri path) {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(activity, path);

            String hasVideo = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO);
            return "yes".equals(hasVideo);
        } catch (Exception e) {
            return false;
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }

    private void showFileChooser() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            if (intent.resolveActivity(activity.getPackageManager()) == null) {
                intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
            }
            startActivityForResult(intent, SELECT_VIDEO);
        } catch (Exception e) {
            Toast.makeText(ApplicationController.self(), R.string.error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (start) {
            start = false;
            return;
        }
//        if (i > 0) tvNotifyCategoryEmpty.setVisibility(View.INVISIBLE);
//        else tvNotifyCategoryEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

//    private boolean showNotify = false;

    private void showNotifyUploadVideoOnMedia() {
//        showNotify = true;
        NotifyUploadVideoOnMediaDialog notifyUploadVideoOnMediaDialog = new NotifyUploadVideoOnMediaDialog(activity);
        notifyUploadVideoOnMediaDialog.setTextTitle(R.string.title_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setTextMessage(R.string.message_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setTextSayAgain(R.string.say_again_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setTextNegative(R.string.negative_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setTextPositive(R.string.positive_notify_upload_video_on_media);
        notifyUploadVideoOnMediaDialog.setmNotifyUploadVideoOnMediaListener(mNotifyUploadVideoOnMediaListener);
        notifyUploadVideoOnMediaDialog.show();
    }

    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            boolean enable = SharedPrefs.getInstance().get(Constants.TabVideo.ENABLE_NOTIFY_UPLOAD_VIDEO_ON_MEDIA, Boolean.class, true);
            if (enable && !b /*&& !showNotify*/)
                showNotifyUploadVideoOnMedia();
        }
    };

    private NotifyUploadVideoOnMediaDialog.NotifyUploadVideoOnMediaListener mNotifyUploadVideoOnMediaListener = new NotifyUploadVideoOnMediaDialog.NotifyUploadVideoOnMediaListener() {
        @Override
        public void onNegativeClicked() {
            if (checkboxUploadOnmedia != null)
                checkboxUploadOnmedia.setChecked(true);
//            showNotify = false;
        }

        @Override
        public void onPositiveClicked() {
            if (checkboxUploadOnmedia != null)
                checkboxUploadOnmedia.setChecked(false);
//            showNotify = false;
        }

        @Override
        public void onShowAgainClicked(boolean isChecked) {
            SharedPrefs.getInstance().put(Constants.TabVideo.ENABLE_NOTIFY_UPLOAD_VIDEO_ON_MEDIA, !isChecked);
        }
    };
}

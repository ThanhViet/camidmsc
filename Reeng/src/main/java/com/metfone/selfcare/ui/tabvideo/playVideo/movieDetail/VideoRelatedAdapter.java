package com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class VideoRelatedAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> implements OnItemVideoRelatedListener {

    public static final int SPACE = 0;
    public static final int NORMAL = 1;

    private @Nullable
    OnItemVideoRelatedListener onItemVideoRelatedListener;

    public VideoRelatedAdapter(Activity act) {
        super(act);
    }

    public void setOnItemVideoRelatedListener(@Nullable OnItemVideoRelatedListener onItemVideoRelatedListener) {
        this.onItemVideoRelatedListener = onItemVideoRelatedListener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = items.get(position);
        if (item instanceof Video) {
            return NORMAL;
        } else {
            return SPACE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case SPACE:
                return new SpaceHolder(layoutInflater, parent);
            default:
                return new VideoRelatedHolder(layoutInflater, parent, activity);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(items, position);
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (holder instanceof VideoRelatedHolder) {
            ((VideoRelatedHolder) holder).setOnItemVideoRelatedListener(this);
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder instanceof VideoRelatedHolder) {
            ((VideoRelatedHolder) holder).setOnItemVideoRelatedListener(null);
        }
    }

    @Override
    public void onItemVideoRelatedClick(Video video) {
        if (onItemVideoRelatedListener != null) {
            onItemVideoRelatedListener.onItemVideoRelatedClick(video);
        }
    }

    static class SpaceHolder extends ViewHolder {

        @BindView(R.id.root_space)
        View rootSpace;

        SpaceHolder(LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.item_channel_space, parent, false));

            ViewGroup.LayoutParams layoutParams = rootSpace.getLayoutParams();
            layoutParams.height = 0;
            layoutParams.width = 0;

            rootSpace.setLayoutParams(layoutParams);
        }
    }

    static class VideoRelatedHolder extends ViewHolder {

        @BindView(R.id.iv_thumb)
        ImageView ivThumb;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.root_item_video_related)
        RelativeLayout rootItemVideoRelated;

        private @Nullable
        Video mVideo;
        private @Nullable
        OnItemVideoRelatedListener onItemVideoRelatedListener;

        VideoRelatedHolder(LayoutInflater layoutInflater, ViewGroup parent, Activity activity) {
            super(layoutInflater.inflate(R.layout.item_video_related, parent, false));

            int width = ScreenManager.getWidth( activity) - Utilities.dpToPx(100);
            width = Math.min(Utilities.dpToPx(300), width);
            int height = width * 9 / 16;

            ViewGroup.LayoutParams layoutParams = ivThumb.getLayoutParams();
            layoutParams.height = height;
            layoutParams.width = width;
            ivThumb.setLayoutParams(layoutParams);
        }

        void setOnItemVideoRelatedListener(@Nullable OnItemVideoRelatedListener onItemVideoRelatedListener) {
            this.onItemVideoRelatedListener = onItemVideoRelatedListener;
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            Object item = items.get(position);
            if (item instanceof Video) {
                mVideo = (Video) item;
                tvTitle.setText(mVideo.getTitle());
                ImageManager.showImageRoundV2(mVideo.getImagePath(), ivThumb);
            }
        }

        @OnClick(R.id.root_item_video_related)
        public void onViewClicked() {
            if (onItemVideoRelatedListener != null && mVideo != null) {
                onItemVideoRelatedListener.onItemVideoRelatedClick(mVideo);
            }
        }
    }
}

package com.metfone.selfcare.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PollItem;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.util.Utilities;

public class PollLayout extends RelativeLayout {

    private View processView;
    private TextView tvPollTitle;
    private TextView tvPollStatus;
    private TextView tvNumberVoter;
    private ImageView ivPollViewDetailVote;
    private RoundLinearLayout llRoot;
//    private FrameLayout flNumberVoteItem;
//    private TextView tvNumberVoteItem;

    private PollItem pollItem;

    public PollLayout(Context context) {
        this(context, null);
    }

    public PollLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PollLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.layout_poll, this);
        processView = findViewById(R.id.process);
        tvPollTitle = findViewById(R.id.tv_poll_title);
        tvPollStatus = findViewById(R.id.tv_poll_status);
        tvNumberVoter = findViewById(R.id.tv_number_voter);
        ivPollViewDetailVote = findViewById(R.id.iv_poll_view_detail_vote);
        llRoot = findViewById(R.id.llRoot);
//        flNumberVoteItem = findViewById(R.id.flNumberVoteItem);
//        tvNumberVoteItem = findViewById(R.id.tvNumberVoteItem);
    }

    public View getProcessView() {
        return processView;
    }

    public TextView getTvPollTitle() {
        return tvPollTitle;
    }

    public TextView getTvPollStatus() {
        return tvPollStatus;
    }

    public RoundLinearLayout getLlRoot() {
        return llRoot;
    }

    public ImageView getIvPollViewDetailVote() {
        return ivPollViewDetailVote;
    }

    public RoundLinearLayout getViewRoot() {
        return llRoot;
    }

    /*public FrameLayout getViewNumberVoteItem() {
        return flNumberVoteItem;
    }*/

    public PollItem getPollItem() {
        return pollItem;
    }

    @SuppressLint("SetTextI18n")
    public void setPollItem(PollItem pollItem, int total, boolean isPreview) {
        this.pollItem = pollItem;
        tvPollTitle.setText(pollItem.getTitle());
        int percent;

        if (Utilities.isEmpty(pollItem.getMemberVoted()) || total == 0) {
            percent = 0;
            tvPollStatus.setVisibility(View.GONE);
            ivPollViewDetailVote.setVisibility(VISIBLE);
            tvPollTitle.setTextColor(getResources().getColor(R.color.black));
            if(!isPreview){
                llRoot.setBackgroundColorRound(getResources().getColor(R.color.bg_color_bubble_send));
            }
//            flNumberVoteItem.setVisibility(GONE);
        } else {
            percent = pollItem.getMemberVoted().size() * 100 / total;
            tvPollStatus.setVisibility(View.VISIBLE);
            ivPollViewDetailVote.setVisibility(VISIBLE);
            String statusPoll = ApplicationController.self().getString(R.string.poll_detail_persent);
            String numberVoted = String.valueOf(pollItem.getMemberVoted().size());
            String percentStr = String.valueOf(percent);
//            llRoot.setBackgroundColorRound(getResources().getColor(R.color.bg_button_red));
//            processView.setBackgroundColor(getResources().getColor(R.color.color_btn_red));
            tvPollStatus.setText(String.format(statusPoll, numberVoted, percentStr, "%"));
            tvPollTitle.setTextColor(getResources().getColor(R.color.white));
            /*if (isPreview) {
                flNumberVoteItem.setVisibility(VISIBLE);
                tvNumberVoteItem.setText(String.valueOf(pollItem.getMemberVoted().size()));
            } else {
                flNumberVoteItem.setVisibility(GONE);
            }*/
        }

        try {

            if (isPreview && Utilities.notEmpty(pollItem.getMemberVoted())) {
                if (tvNumberVoter.getVisibility() != VISIBLE) {
                    tvNumberVoter.setVisibility(VISIBLE);
                }
                tvNumberVoter.setText(String.valueOf(pollItem.getMemberVoted().size()));
            } else {
                if (tvNumberVoter.getVisibility() != GONE) {
                    tvNumberVoter.setVisibility(GONE);
                }
            }
        } catch (Exception ignored) {
        }

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) processView.getLayoutParams();
        if (params == null) {
            params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        }
        params.weight = percent;
        processView.setLayoutParams(params);

    }

}

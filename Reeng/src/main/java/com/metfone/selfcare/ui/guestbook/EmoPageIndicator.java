package com.metfone.selfcare.ui.guestbook;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.viewpagerindicator.IconIndicator;
import com.metfone.selfcare.ui.viewpagerindicator.IconPagerAdapter;
import com.metfone.selfcare.ui.viewpagerindicator.IcsLinearLayout;
import com.metfone.selfcare.ui.viewpagerindicator.PageIndicator;
import com.metfone.selfcare.ui.viewpagerindicator.TabView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class EmoPageIndicator extends HorizontalScrollView implements PageIndicator {
    private static final CharSequence EMPTY_TITLE = "";
    private final IcsLinearLayout mTabLayout;
    private IconPagerAdapter iconAdapter;
    private ViewPager mViewPager;
    private ViewPager.OnPageChangeListener mListener;
    private int mMaxTabWidth;
    private int mSelectedTabIndex;
    private OnTabReselectedListener mTabReselectedListener;
    private final View.OnClickListener mTabClickListener;
    private final View.OnClickListener mIconTabClickListener;
    private ArrayList<IconTabViewV1> iconTabViewArrayList;
    private Runnable mTabSelector;

    public interface OnTabReselectedListener {
        void onTabReselected(int position);
    }

    public EmoPageIndicator(Context context) {
        this(context, null);

    }

    public EmoPageIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mTabClickListener = new View.OnClickListener() {
            public void onClick(View view) {
                TabView tabView = (TabView) view;
                int oldSelected = mViewPager.getCurrentItem();
                int newSelected = tabView.getIndex();
                mViewPager.setCurrentItem(newSelected);
                if (oldSelected == newSelected && mTabReselectedListener != null) {
                    mTabReselectedListener.onTabReselected(newSelected);
                }

            }
        };
        this.mIconTabClickListener = new View.OnClickListener() {
            public void onClick(View view) {
                IconTabViewV1 tabView = (IconTabViewV1) view;
                int oldSelected = mViewPager.getCurrentItem();
                int newSelected = tabView.getIndex();
                mViewPager.setCurrentItem(newSelected);
                if (oldSelected == newSelected && mTabReselectedListener != null) {
                    mTabReselectedListener.onTabReselected(newSelected);
                }

            }
        };
        this.setHorizontalScrollBarEnabled(false);
        this.mTabLayout = new IcsLinearLayout(context, R.attr.vpiTabPageIndicatorStyle);
        this.addView(this.mTabLayout, new FrameLayout.LayoutParams(-2, -1));
    }

    private void focusToTab(int tabIndex) {
        Iterator it = this.iconTabViewArrayList.iterator();
        while (it.hasNext()) {
            IconTabViewV1 icon = (IconTabViewV1) it.next();
            if (icon.mIndex == tabIndex) {
                icon.setBackgroundResource(R.color.indicator_guest_book_selected);
            } else {
                icon.setBackgroundResource(R.color.white);
            }
        }
    }

    public void setOnTabReselectedListener(OnTabReselectedListener listener) {
        this.mTabReselectedListener = listener;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
        boolean lockedExpanded = widthMode == View.MeasureSpec.EXACTLY;
        this.setFillViewport(lockedExpanded);
//        int childCount = this.mTabLayout.getChildCount();
        //if (childCount <= 1 || widthMode != View.MeasureSpec.EXACTLY && widthMode != View.MeasureSpec.AT_MOST) {
        if (widthMode != View.MeasureSpec.EXACTLY && widthMode != View.MeasureSpec.AT_MOST) {
            this.mMaxTabWidth = -1;
            Log.i("TabPageIndicator", "mMaxTabWidth====== else " + this.mMaxTabWidth);
        } else {
            this.mMaxTabWidth = (int) ((float) View.MeasureSpec.getSize(widthMeasureSpec) * 0.15F);
            Log.i("TabPageIndicator", "mMaxTabWidth======childCount > 2 " + this.mMaxTabWidth);
        }

        int oldWidth = this.getMeasuredWidth();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int newWidth = this.getMeasuredWidth();
        if (lockedExpanded && oldWidth != newWidth) {
            this.setCurrentItem(this.mSelectedTabIndex);
        }
    }

    private void animateToTab(int position) {
        final View tabView = this.mTabLayout.getChildAt(position);
        if (this.mTabSelector != null) {
            this.removeCallbacks(this.mTabSelector);
        }

        this.mTabSelector = new Runnable() {
            public void run() {
                int scrollPos = tabView.getLeft() - (getWidth() - tabView.getWidth()) / 2;
                smoothScrollTo(scrollPos, 0);
                mTabSelector = null;
            }
        };
        this.post(this.mTabSelector);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mTabSelector != null) {
            this.post(this.mTabSelector);
        }

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mTabSelector != null) {
            this.removeCallbacks(this.mTabSelector);
        }

    }

    private void addTab(int index, CharSequence text, IconIndicator iconIndicator) {
        Log.d("TabPageIndicator", "addTabaddTabaddTabaddTab" + index);
        IconTabViewV1 layout = new IconTabViewV1(this.getContext());
        layout.mIndex = index;
        layout.setFocusable(true);
        layout.setOnClickListener(this.mIconTabClickListener);
        layout.setClickable(true);
        if (iconIndicator.getTypeResource() == 0) {
            layout.imgUnselected.setImageResource(iconIndicator.getIdResIndicator());
            layout.imgSelected.setImageResource(iconIndicator.getIdResIndicatorSelected());
            ImageLoaderManager.getInstance(getContext()).cancelDisplayTag(layout.imgUnselected);
            ImageLoaderManager.getInstance(getContext()).cancelDisplayTag(layout.imgSelected);
        } else {
            ImageLoaderManager.getInstance(getContext()).displayStickerIndicator(layout.imgUnselected,
                    iconIndicator.getUrlIndicator());
            ImageLoaderManager.getInstance(getContext()).displayStickerIndicator(layout.imgSelected,
                    iconIndicator.getUrlIndicator());
        }

        android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams(0, -1, 1.0F);
        params.topMargin = 1;
        params.leftMargin = (int) this.getResources().getDimension(R.dimen.tab_padding);
        params.rightMargin = (int) this.getResources().getDimension(R.dimen.tab_padding);
        this.mTabLayout.addView(layout, params);
        this.iconTabViewArrayList.add(layout);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        if (this.mListener != null) {
            this.mListener.onPageScrollStateChanged(arg0);
        }
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        if (this.mListener != null) {
            this.mListener.onPageScrolled(arg0, arg1, arg2);
        }
    }

    @Override
    public void onPageSelected(int arg0) {
        this.setCurrentItem(arg0);
        if (this.mListener != null) {
            this.mListener.onPageSelected(arg0);
        }
    }

    @Override
    public void setViewPager(ViewPager view) {
        if (this.mViewPager != view) {
            if (this.mViewPager != null) {
                this.mViewPager.setOnPageChangeListener((ViewPager.OnPageChangeListener) null);
            }
            PagerAdapter adapter = view.getAdapter();
            if (adapter == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            } else {
                this.mViewPager = view;
                view.setOnPageChangeListener(this);
                this.iconTabViewArrayList = new ArrayList();
                this.notifyDataSetChanged();
            }
        }
    }

    public void notifyDataSetChanged() {
        this.mTabLayout.removeAllViews();
        PagerAdapter adapter = this.mViewPager.getAdapter();
        this.iconAdapter = null;
        if (adapter instanceof IconPagerAdapter) {
            this.iconAdapter = (IconPagerAdapter) adapter;
        }
        int count = adapter.getCount();

        for (int i = 0; i < count; ++i) {
            CharSequence title = adapter.getPageTitle(i);
            if (title == null) {
                title = EMPTY_TITLE;
            }
            IconIndicator icon = null;
            if (this.iconAdapter != null) {
                icon = this.iconAdapter.getIconIndicator(i);
            }
            this.addTab(i, title, icon);
        }
        if (this.mSelectedTabIndex > count) {
            this.mSelectedTabIndex = count - 1;
        }
        this.setCurrentItem(this.mSelectedTabIndex);
        this.requestLayout();
    }

    @Override
    public void setViewPager(ViewPager view, int initialPosition) {
        setViewPager(view);
        setCurrentItem(initialPosition);
    }

    @Override
    public void setCurrentItem(int item) {
        if (this.mViewPager == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        } else {
            this.mSelectedTabIndex = item;
            this.mViewPager.setCurrentItem(item);
            this.focusToTab(this.mSelectedTabIndex);
            int tabCount = this.mTabLayout.getChildCount();

            for (int i = 0; i < tabCount; ++i) {
                View child = this.mTabLayout.getChildAt(i);
                boolean isSelected = i == item;
                child.setSelected(isSelected);
                if (isSelected) {
                    this.animateToTab(item);
                }
            }
        }
    }

    private class IconTabViewV1 extends RelativeLayout {
        private ImageView imgUnselected;
        private ImageView imgSelected;
        private int mIndex;

        public IconTabViewV1(Context context) {
            super(context, (AttributeSet) null, R.attr.vpiTabPageIndicatorStyle);
            this.initView();
        }

        private void initView() {
            View view = inflate(this.getContext(), R.layout.icon_guest_book_indicator, (ViewGroup) null);
            this.imgUnselected = (ImageView) view.findViewById(R.id.imageView);
            this.imgSelected = (ImageView) view.findViewById(R.id.imageViewSelected);
            this.imgUnselected.setVisibility(GONE);
            this.addView(view);
        }

        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(mMaxTabWidth, MeasureSpec.EXACTLY), heightMeasureSpec);
        }

        public int getIndex() {
            return this.mIndex;
        }

        public void setSelected(boolean b) {
            if (b) {
                this.imgSelected.setVisibility(VISIBLE);
                this.imgUnselected.setVisibility(GONE);
            } else {
                this.imgSelected.setVisibility(GONE);
                this.imgUnselected.setVisibility(VISIBLE);
            }
        }
    }

    public interface OnTabSelectedListener {
        void onTabSelected(int var1);
    }

    @Override
    public void setOnPageChangeListener(ViewPager.OnPageChangeListener listener) {
        mListener = listener;
    }
}

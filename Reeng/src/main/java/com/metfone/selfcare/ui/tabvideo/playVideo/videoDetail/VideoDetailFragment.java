package com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestBuilder;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.MochaAdsClient;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.player.MochaPlayer;
import com.metfone.selfcare.common.utils.player.MochaPlayerUtil;
import com.metfone.selfcare.common.utils.screen.ScreenUtilsImpl;
import com.metfone.selfcare.database.datasource.ChannelDataSource;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.helper.BackStackHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.BannerVideo;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.module.share.listener.ShareBusinessListener;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter.ItemObject;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter.Type;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.listener.FullPlayerListener;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.OptionsVideoDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.QualityVideoDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.ReportVideoDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.VideoFullScreenPlayerDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.adapter.VideoDetailAdapter;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.model.VideoObject;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.ui.view.CenterLayoutManager;
import com.metfone.selfcare.ui.view.load_more.OnEndlessScrollListener;
import com.metfone.selfcare.ui.view.tab_video.CampaignLayout;
import com.metfone.selfcare.ui.view.tab_video.SubscribeChannelLayout;
import com.metfone.selfcare.ui.view.tab_video.VideoPlaybackControlView;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class VideoDetailFragment extends BaseFragment implements VideoDetailView, VideoDetailAdapter.OnItemVideoClickListener, ShareBusinessListener {

    private static final String TAG = "VideoDetailFragment";
    private static final long TIME_SET_SEEN_VIDEO = 15000;

    private static final long DELAY_MILLIS = 300L;
    private static final int LIMIT_DEFAULT = 20;
    @BindView(R.id.layout_root)
    View rootView;
    @BindView(R.id.videoRecyclerView)
    RecyclerView videoRecyclerView;
    @BindView(R.id.reHeader)
    View reHeader;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    Unbinder unbinder;
    @Inject
    VideoDetailPresenter presenter;
    CampaignLayout campaignLayout;

    private boolean isLoadMore = false;// có thể lấy thêm dữ liệu
    private boolean isUpdate = false;// cờ cần update dữ liệu
    private boolean isLoading = false;// cờ đang lấy dữ liệu
    private boolean isFirst = true;// lần đầu tiên vào màn hình thì cần bay video
    private boolean isFullScreen = false;// cò full màn hình
    private boolean isMini = false;//có mở mini hay không
    private boolean isCanSetMediaSource = false;//có mở mini hay không
    private ArrayList<BaseAdapter.ItemObject> items;// danh sách phần từ
    private ItemObject itemLoadMore;// item load more
    private String playerName;// tên của playser
    private int currentPosition = -1;// vị trí play hiện tại
    private int heightCurrentVideo = 0;// chiều cao của item hiện tại
    private Video currentVideo = null;// video đang được play hiện tại
    private VideoDetailAdapter.VideoHolder currentHolder = null;// item đang được chạy video

    private CenterLayoutManager layoutManager;
    private VideoDetailAdapter adapter;
    private MochaPlayer mPlayer;
    private VideoFullScreenPlayerDialog dialogFullScreenPlayer;

    private boolean popOut = false;
    private boolean isPause = false;// trạng thái pause màn hình

    private boolean isAddVideoToListSeen = false;
    private ShareContentBusiness shareBusiness;
    /**
     * bộ định thời cập nhật dữ liệu
     * Dữ liệu mới sẽ được cập nhật vào khi tiền trình scroll dừng hẳn
     */
    private Runnable updateDataRunnable = new Runnable() {
        @Override
        public void run() {
            isLoading = false;
            isUpdate = false;
            if (videoRecyclerView != null && adapter != null) {
                videoRecyclerView.stopScroll();
                items.remove(itemLoadMore);
                if (isLoadMore)
                    items.add(itemLoadMore);
                adapter.updateData(items);

                if (isFirst) {// lần đầu vào app thì play video
                    isFirst = false;// hủy cờ chạy video đầu tiên
                    showView();
                }
            }
        }
    };
    private long timeStartShowAds = 2 * 1000L;
    private long timeEndShowAds = 60 * 1000L;
    private String channelId;
    private BannerVideo bannerVideo;
    private Runnable adsSchedule = new Runnable() {
        @Override
        public void run() {
            if (rootView == null) return;
            if (mPlayer.getDuration() == C.INDEX_UNSET)
                rootView.postDelayed(this, 100);
            else if (mPlayer.getCurrentPosition() < timeStartShowAds) {
                rootView.postDelayed(this, 100);
            } else if (mPlayer.getCurrentPosition() < timeEndShowAds)// && mPlayer.getDuration() > timeEndShowAds)
            {
                if (!adapter.isAdsShow()) {
                    int number = SharedPrefs.getInstance().get(MochaAdsClient.CHANNEL_BANNER_VIDEO + channelId, Integer.class, 1);
                    if (number == 1 || number == 3 || number == 5 || number == 7)
                        currentHolder.showAds(bannerVideo);
                    else
                        adapter.setAdsShow(true);
                    SharedPrefs.getInstance().put(MochaAdsClient.CHANNEL_BANNER_VIDEO + channelId, number > 7 ? number : number + 1);
                }
                rootView.postDelayed(this, 100);
            } else if (!adapter.isAdsConform()) {
                currentHolder.hideAds();
            }
        }
    };
    /**
     * bộ lắng nghe Scroll
     */
    private OnEndlessScrollListener onEndlessScrollListener = new OnEndlessScrollListener(3) {
        private int relocation;
        private int currentState;

        private boolean isPlay;

        @Override
        public void onLoadNextPage(View view) {
            super.onLoadNextPage(view);
            if (presenter == null || adapter == null || items == null)
                return;
            if (isLoading)
                return;
            if (!isLoadMore) {
                items.remove(itemLoadMore);
                adapter.updateData(items);
            } else {
                isLoading = true;
                if (presenter != null) presenter.getDataLoadMore(LIMIT_DEFAULT);
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (isUpdate && rootView != null && updateDataRunnable != null) {
                rootView.removeCallbacks(updateDataRunnable);
                rootView.postDelayed(updateDataRunnable, DELAY_MILLIS);
            }
            if (isPlay) {
                relocation = relocation + dy;
                int location = relocation;
                if (relocation < 0) location = -relocation;
                if (heightCurrentVideo < location) {
                    isPlay = false;
                    if (!isFullScreen)
                        stopVideo();
                }
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (adapter == null || layoutManager == null || videoRecyclerView == null)
                return;
            if (currentState == RecyclerView.SCROLL_STATE_IDLE && newState == RecyclerView.SCROLL_STATE_DRAGGING) {// dừng tất cả các tiền trình đang load ảnh
                adapter.pauseRequests();
            } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {// khôi phuc các tiền trình load ảnh

                adapter.resumeRequests();
                int first = layoutManager.findFirstVisibleItemPosition();
                int last = layoutManager.findLastVisibleItemPosition();
                last = Math.min(last, adapter.getItemCount() - 1);
                for (int i = first; i <= last; i++) {
                    updateUiImage(i);
                }
                relocation = 0;
                isPlay = true;
                playVideo(getViewHolderCenterScreen());
            }
            currentState = newState;
        }

        /**
         * Cập nhật lại giao diện image của item
         * @param position vị trí cần cập nhật
         */
        private void updateUiImage(int position) {
            if (videoRecyclerView != null) {
                RecyclerView.ViewHolder viewHolder = videoRecyclerView.findViewHolderForAdapterPosition(position);
                if (viewHolder instanceof VideoDetailAdapter.VideoHolder) {
                    ((VideoDetailAdapter.VideoHolder) viewHolder).updateUiImageVideo();
                    ((VideoDetailAdapter.VideoHolder) viewHolder).updateUiImageChannel();
                }
            }
        }

        /**
         * lấy ra item có tỷ lệ phần trăm cao nhất
         * @return VideoHolder
         */
        private VideoDetailAdapter.VideoHolder getViewHolderCenterScreen() {
            VideoDetailAdapter.VideoHolder viewHolderMax = null;
            float percentMax = 0;
            int first = layoutManager.findFirstVisibleItemPosition();
            int last = layoutManager.findLastVisibleItemPosition();
            for (int i = first; i <= last; i++) {
                RecyclerView.ViewHolder viewHolder = videoRecyclerView.findViewHolderForAdapterPosition(i);
                if (!(viewHolder instanceof VideoDetailAdapter.VideoHolder)) continue;
                VideoDetailAdapter.VideoHolder itemHolder = (VideoDetailAdapter.VideoHolder) viewHolder;
                if (i == first) viewHolderMax = itemHolder;// gán max bằng phần tử đầu tiền
                float percentViewHolder = getPercentViewHolderInScreen(itemHolder);// lấy phần trăm của video
                if (percentViewHolder > percentMax && percentViewHolder >= 50) {
                    percentMax = percentViewHolder;
                    viewHolderMax = itemHolder;
                }
            }
            return viewHolderMax;
        }

        /**
         * tính phần trăm item có trên màn hình
         * @param viewHolder holder
         * @return phần trăm
         */
        private float getPercentViewHolderInScreen(VideoDetailAdapter.VideoHolder viewHolder) {
            if (viewHolder == null) return 0;
            int heightScreen = ScreenUtilsImpl.getScreenHeight(application);
            View view = viewHolder.provideVideoFrame();
            int[] location = new int[2];
            view.getLocationOnScreen(location);
            int viewHeight = view.getHeight();
            int viewFromY = location[1];
            int viewToY = location[1] + viewHeight;
            if (viewFromY >= 0 && viewToY <= heightScreen) return 100;
            if (viewFromY < 0 && viewToY > heightScreen) return 100;
            if (viewFromY < 0 && viewToY <= heightScreen)
                return ((float) (viewToY - (-viewFromY)) / viewHeight) * 100;
            if (viewFromY >= 0 && viewToY > heightScreen)
                return ((float) (heightScreen - viewFromY) / viewHeight) * 100;
            return 0;
        }
    };
    private long mLastClickTime = 0;
    /**
     * sự kiện click vào đăng ký video vip
     */
    private ClickListener.IconListener mIconListener = (view, entry, menuId) -> {
        if (menuId == Constants.MENU.POPUP_CONFIRM_REGISTER_VIP) {
            handleRegisterVip(application.getConfigBusiness().getSubscriptionConfig().getCmd());
        }
    };
    private FullPlayerListener.OnActionFullScreenListener onActionFullScreenListener = new FullPlayerListener.OnActionFullScreenListener() {
        @Override
        public void handlerLikeVideo(Video currentVideo) {
            if (presenter != null) presenter.likeVideo(currentVideo);
        }

        @Override
        public void handlerShareVideo(Video currentVideo) {
            if (currentVideo != null && currentVideo.getIsPrivate() == 1) {
                activity.showToast(R.string.video_private_toast);
                return;
            }
            if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                activity.showDialogLogin();
            } else {
                if (shareBusiness != null) {
                    shareBusiness.dismissAll();
                }
                shareBusiness = new ShareContentBusiness(activity, currentVideo);
                shareBusiness.setPlayingState(mPlayer != null && mPlayer.getPlayWhenReady());
                shareBusiness.setShareBusinessListener(VideoDetailFragment.this);
                shareBusiness.showPopupShareContent();
            }
        }
    };
    private FullPlayerListener.ProviderFullScreen onProviderFullScreen = new FullPlayerListener.ProviderFullScreen() {
        @Override
        public Video provideVideoForward() {
            if (currentPosition >= items.size() - 1) return null;
            Object object = items.get(currentPosition + 1).getInfo();
            if (object instanceof VideoObject) {
                VideoObject videoObject = (VideoObject) object;
                return videoObject.getVideo();
            }
            return null;
        }

        @Override
        public Video provideCurrentVideo() {
            return currentVideo;
        }
    };
    private Player.EventListener eventListener = new Player.DefaultEventListener() {
        int currentState;

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG, "onPlayerStateChanged playWhenReady: " + playWhenReady + ", playbackState: " + playbackState);
            if (!isFullScreen && currentState != playbackState && playbackState == Player.STATE_ENDED) {
                handlerEnd();
            }
            currentState = playbackState;
        }
    };
    private OrientationEventListener orientationEventListener;
    private int lastOrientation;
    private Runnable runnableEnableRotateSensor = () -> {
        if (orientationEventListener != null && orientationEventListener.canDetectOrientation() && (dialogFullScreenPlayer == null || !dialogFullScreenPlayer.isShowing())) {
            orientationEventListener.enable();
        }
    };
    private FullPlayerListener.OnFullScreenListener onFullScreenListener = new FullPlayerListener.OnFullScreenListener() {
        @SuppressLint("SourceLockedOrientationActivity")
        @Override
        public void onDismiss() {
            if (rootView != null) rootView.setVisibility(View.VISIBLE);
            dismissAllDialogs();
            isFullScreen = false;
            if (activity != null) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                lastOrientation = activity.getRequestedOrientation();
            }
            if (mPlayer != null && currentHolder != null && currentVideo != null) {
                mPlayer.getPlayerView().setRefreshMode((float) currentVideo.getAspectRatio());
                Utilities.setSizeFrameVideo(activity, currentHolder.getControllerFrame(), currentVideo.getAspectRatio());
                mPlayer.addPlayerViewTo(currentHolder.provideVideoFrame());
            }
            enableRotateSensor();
        }

        @Override
        public void onPlayNextVideoForward(Video nextVideo) {
            nextVideoForward();
        }

        @Override
        public void onPlayEpisode(Video video, int position) {
            try {
                int positionNext = Math.min(position, items.size() - 1);
                layoutManager.smoothScrollToPosition(videoRecyclerView, null, positionNext);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onChangeSubtitleAudio(Video video) {

        }
    };
    private VideoPlaybackControlView.CallBackListener callBackListener = new VideoPlaybackControlView.DefaultCallbackListener() {

        @Override
        public void onFullScreen() {
            dismissAllDialogs();
            disableRotateSensor();
            if (dialogFullScreenPlayer != null && dialogFullScreenPlayer.isShowing()) {
                dialogFullScreenPlayer.dismiss();
                mPlayer.setVisibleLayoutControl(false);
                android.util.Log.e("duongnk", "onFullScreen: ");
            }
            if (activity == null || activity.isFinishing()) return;
            final boolean isLandscape = currentVideo != null && currentVideo.isVideoLandscape();
            //activity.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            isFullScreen = true;
            dialogFullScreenPlayer = new VideoFullScreenPlayerDialog(activity);
            dialogFullScreenPlayer.setOnActionFullScreenListener(onActionFullScreenListener);
            dialogFullScreenPlayer.setOnFullScreenListener(onFullScreenListener);
            dialogFullScreenPlayer.setOnShowListener(dialog -> {
                if (rootView != null) rootView.setVisibility(View.INVISIBLE);
            });
            dialogFullScreenPlayer.setProviderFullScreen(onProviderFullScreen);
            dialogFullScreenPlayer.setCurrentVideo(currentVideo);
            dialogFullScreenPlayer.setPlayerName(playerName);
            dialogFullScreenPlayer.setMovies(false);
            dialogFullScreenPlayer.setLandscape(isLandscape);
            if (Utilities.notEmpty(items)) {
                ArrayList<Object> episodes = new ArrayList<>();
                for (ItemObject itemObject : items) {
                    if (itemObject != null && itemObject.getInfo() instanceof VideoObject) {
                        VideoObject videoObject = (VideoObject) itemObject.getInfo();
                        if (videoObject.getVideo() != null) episodes.add(videoObject.getVideo());
                    }
                }
                if (Utilities.notEmpty(episodes) && episodes.contains(currentVideo)) {
                    int currentEpisode = episodes.indexOf(currentVideo);
                    dialogFullScreenPlayer.setEpisodes(episodes, false);
                    dialogFullScreenPlayer.setCurrentEpisode(currentEpisode);
                }
            }
            dialogFullScreenPlayer.show();
        }

        @Override
        public void onPlayPause(boolean state) {
            changeStatusPlayer(state);
        }

        @Override
        public void onHaveSeek(boolean flag) {
            if (mPlayer != null)
                mPlayer.onHaveSeek(flag);
        }

        @Override
        public void onMoreClick() {
            handlerMore();
        }

        @Override
        public void onQuality() {
            handlerQuality();
        }

        @Override
        public void onReplay() {
            if (!isFullScreen && mPlayer != null) {
                mPlayer.logStart(currentVideo);
            }
        }

        @Override
        public void onTimeChange(long time) {
            super.onTimeChange(time);
            if (!currentVideo.isLive() && !isAddVideoToListSeen && currentVideo != null) {
                if (time > TIME_SET_SEEN_VIDEO) {
                    isAddVideoToListSeen = true;
                    presenter.addVideoToListSeen(currentVideo.getId());
                }
            }
        }
    };
    private Dialog optionsVideoDialog;
    private Dialog reportVideoDialog;
    private Dialog qualityVideoDialog;
    private Dialog speedVideoDialog;
    private Runnable runnableWhenLandscape = () -> {
        if (callBackListener != null) callBackListener.onFullScreen();
    };

    public static VideoDetailFragment newInstance(Video video, String playerTag, boolean fromLoading) {
        VideoDetailFragment videoDetailFragment = new VideoDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TabVideo.PLAYER_TAG, playerTag);
        bundle.putSerializable(Constants.TabVideo.VIDEO, video);
        bundle.putBoolean(Constants.TabVideo.PLAYER_INIT, fromLoading);
        videoDetailFragment.setArguments(bundle);
        return videoDetailFragment;
    }

    public static VideoDetailFragment newInstance(ArrayList<Video> list, int position, String channelId, String playerTag) {
        VideoDetailFragment videoDetailFragment = new VideoDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TabVideo.CATEGORY_ID, channelId);
        bundle.putSerializable(Constants.TabVideo.LIST_DATA_VIDEO, list);
        bundle.putSerializable(Constants.TabVideo.POSITION, position);
        bundle.putString(Constants.TabVideo.PLAYER_TAG, playerTag);
        bundle.putBoolean(Constants.TabVideo.PLAYER_INIT, false);
        videoDetailFragment.setArguments(bundle);
        return videoDetailFragment;
    }

    private void initOrientationListener() {
        if (activity != null) {
            lastOrientation = activity.getRequestedOrientation();
            orientationEventListener = new OrientationEventListener(activity, SensorManager.SENSOR_DELAY_UI) {
                int curOrientation;

                @Override
                public void onOrientationChanged(int orientation) {
                    if ((orientation >= 0 && orientation <= 45) || (orientation > 315 && orientation <= 360)) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    } else if (orientation > 45 && orientation <= 135) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    } else if (orientation > 135 && orientation <= 225) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    } else if (orientation > 225 && orientation <= 315) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    } else {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
                    }
                    if (curOrientation != lastOrientation) {
                        lastOrientation = curOrientation;
                        if (rootView != null) {
                            rootView.removeCallbacks(runnableWhenLandscape);
                            if ((curOrientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE || curOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) && currentVideo != null && currentVideo.isVideoLandscape()
                                    && !DeviceUtils.isDeviceLockRotate(activity)
                                    && (mPlayer != null && !mPlayer.isAdDisplayed())) {
                                rootView.postDelayed(runnableWhenLandscape, 300);
                            }
                        }
                    }
                }
            };
            orientationEventListener.disable();
        }
        enableRotateSensor();
    }

    public void enableRotateSensor() {
        if (rootView != null) {
            rootView.removeCallbacks(runnableEnableRotateSensor);
            rootView.postDelayed(runnableEnableRotateSensor, 600);
        }
    }

    public void disableRotateSensor() {
        if (rootView != null)
            rootView.removeCallbacks(runnableEnableRotateSensor);
        if (orientationEventListener != null) orientationEventListener.disable();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);

        itemLoadMore = new ItemObject();
        itemLoadMore.setId(String.valueOf(System.nanoTime()));
        itemLoadMore.setType(Type.LOAD_MORE);

        Bundle bundle = getArguments();
        if (bundle != null) {
            playerName = bundle.getString(Constants.TabVideo.PLAYER_TAG);
            boolean fromLoading = bundle.getBoolean(Constants.TabVideo.PLAYER_INIT, false);
            if (TextUtils.isEmpty(playerName)) {
                isCanSetMediaSource = true;
                playerName = String.valueOf(System.nanoTime());
            } else if (fromLoading)
                isCanSetMediaSource = true;

            mPlayer = MochaPlayerUtil.getInstance().providePlayerBy(playerName);
            mPlayer.addListener(eventListener);
            mPlayer.addControllerListener(callBackListener);
            mPlayer.getPlayerView().setEnabled(true);
            mPlayer.getPlayerView().setUseController(true);
            mPlayer.getPlayerView().enableFast(false);
            mPlayer.getPlayerView().getController().setVisibility(View.VISIBLE);
            mPlayer.updatePlaybackState();
            LuckyWheelHelper.getInstance(application).doMission(Constants.LUCKY_WHEEL.ITEM_WATCH_VIDEO); // TODO: 5/22/2020 doMission ITEM_WATCH_VIDEO
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_detail, container, false);
        view.setOnClickListener(view1 -> {

        });
        unbinder = ButterKnife.bind(this, view);

        videoRecyclerView.setAlpha(0);
        videoRecyclerView.setVisibility(View.VISIBLE);
        reHeader.setTranslationY(-getResources().getDimensionPixelSize(R.dimen.action_bar_height));
        reHeader.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        if (rootView != null)
            rootView.postDelayed(() -> initOrientationListener(), 1000);
    }

    private boolean playWhenReady;

    @Override
    public void onResume() {
        super.onResume();
        android.util.Log.e("duonngk", this.getClass().getName() + "onResume: 1");
        if (mPlayer != null) {
            mPlayer.setPaused(false);
            mPlayer.setMini(false);
            mPlayer.setVisibleLayoutControl(false);
            android.util.Log.e("duonngk", this.getClass().getName() + "onResume: 2");
            if (mPlayer.isAdDisplayed()) {
                if (mPlayer.getAdPlayer() != null) mPlayer.getAdPlayer().resume();
            } else {
                if (playWhenReady) {
                    mPlayer.setPlayWhenReady(true);
                } else if (!mPlayer.getPlayWhenReady() && mPlayer.getPlayerView() != null
                        && mPlayer.getPlaybackState() != Player.STATE_IDLE
                        && mPlayer.getPlaybackState() != Player.STATE_BUFFERING)
                    mPlayer.getPlayerView().showController();
            }
        }
        isPause = false;
        enableRotateSensor();
        playWhenReady = false;
        if (currentVideo != null && currentVideo.getChannel() != null) {
            Channel channel = currentVideo.getChannel();
            if (channel.getTypeChannel() == Channel.TypeChanel.OPEN_APP.VALUE) {
                String packageIdTmp = channel.getPackageAndroid();
                if (!TextUtils.isEmpty(packageIdTmp)) {
                    boolean oldInstall = channel.isInstall();
                    boolean isInstall = SubscribeChannelLayout.isInstalled(activity, packageIdTmp);
                    if (isInstall != oldInstall) {
                        currentVideo.getChannel().setInstall(isInstall);
                        if (adapter != null) {
                            if (currentHolder != null && adapter.getItemCount() > currentHolder.getAdapterPosition()) {
                                adapter.notifyItemChanged(currentHolder.getAdapterPosition());
                            } else {
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onPause() {
        android.util.Log.e("duonngk", this.getClass().getName() + "onPause:");
        disableRotateSensor();
        if (mPlayer != null) {
            mPlayer.setPaused(true);
            mPlayer.setMini(isMini);
            if (!isMini) {
                if (mPlayer.isAdDisplayed()) {
                    if (mPlayer.getAdPlayer() != null) mPlayer.getAdPlayer().pause();
                }
                if (mPlayer.getPlayWhenReady()) playWhenReady = true;
                mPlayer.setPlayWhenReady(false);
            }
        }
        isPause = true;
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (mPlayer != null && !isMini)
            MochaPlayerUtil.getInstance().removerPlayerBy(playerName);
        if (presenter != null)
            presenter.dispose();
        if (mPlayer != null && callBackListener != null)
            mPlayer.removeControllerListener(callBackListener);
        if (mPlayer != null && eventListener != null)
            mPlayer.removeListener(eventListener);
        if (rootView != null && updateDataRunnable != null)
            rootView.removeCallbacks(updateDataRunnable);
        if (videoRecyclerView != null && onEndlessScrollListener != null)
            videoRecyclerView.removeOnScrollListener(onEndlessScrollListener);

        callBackListener = null;
        onProviderFullScreen = null;
        onFullScreenListener = null;

        if (unbinder != null)
            unbinder.unbind();
        dismissAllDialogs();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (campaignLayout != null && !isMini)
            campaignLayout.hideTextRun();
        if (mPlayer != null && !isMini)
            MochaPlayerUtil.getInstance().removerPlayerBy(playerName);
        super.onDestroy();
    }

    private void initView() {
        initRecyclerView();
        if (ivClose != null) {
            ivClose.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    onMiniClicked();
                }
            });
        }
        Bundle bundle = getArguments();
        if (bundle != null && presenter != null) {
            Serializable serializable;
            serializable = bundle.getSerializable(Constants.TabVideo.VIDEO);
            if (serializable instanceof Video) {
                Video video = (Video) serializable;
                video.setPlaying(true);
                presenter.setVideo(video);
                presenter.getVideoData(video);
                presenter.getData(LIMIT_DEFAULT);
                return;
            }
            serializable = bundle.getSerializable(Constants.TabVideo.LIST_DATA_VIDEO);
            isCanSetMediaSource = true;
            if (serializable instanceof ArrayList) {
                ArrayList<Video> list;
                try {
                    list = (ArrayList<Video>) serializable;
                    presenter.setupData(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initRecyclerView() {
        if (mPlayer.getPlayerView() != null) {
            adapter = new VideoDetailAdapter(activity);
            adapter.setOnItemListener(this);
            layoutManager = new CenterLayoutManager(activity);
            layoutManager.setInitialPrefetchItemCount(5);
            videoRecyclerView.setLayoutManager(layoutManager);
            videoRecyclerView.setAdapter(adapter);
            videoRecyclerView.setHasFixedSize(true);
            videoRecyclerView.setItemAnimator(null);
            videoRecyclerView.setLayoutAnimation(null);
            videoRecyclerView.setItemViewCacheSize(5);
            videoRecyclerView.setDrawingCacheEnabled(true);
            videoRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            setOnScroll();
        }
    }

    /**
     * thiết lập bộ lắng nghe cho recyclerView
     */
    @SuppressWarnings("deprecation")
    private void setOnScroll() {
        if (videoRecyclerView != null && onEndlessScrollListener != null)
            videoRecyclerView.addOnScrollListener(onEndlessScrollListener);
//        if (videoRecyclerView != null) {
//            ListPreloader.PreloadSizeProvider<ItemObject> sizeProvider = new FixedPreloadSizeProvider<>(ScreenUtilsImpl.getScreenWidth(application), ScreenUtilsImpl.getScreenHeight(application));
//            PreloadModelProvider modelProvider = new PreloadModelProvider();
//            RecyclerViewPreloader<ItemObject> preloader = new RecyclerViewPreloader<>(Glide.with(application), modelProvider, sizeProvider, 10);
//            videoRecyclerView.addOnScrollListener(preloader);
//        }
    }

    @Override
    public void updateData(ArrayList<ItemObject> results, boolean loadMore) {
        isUpdate = true;
        isLoadMore = loadMore;
        items = results;
        if (rootView != null && updateDataRunnable != null) {
            rootView.removeCallbacks(updateDataRunnable);
            rootView.postDelayed(updateDataRunnable, DELAY_MILLIS);
        }
        if (dialogFullScreenPlayer != null && dialogFullScreenPlayer.isShowing()) {
            if (Utilities.notEmpty(items)) {
                ArrayList<Object> episodes = new ArrayList<>();
                for (ItemObject itemObject : items) {
                    if (itemObject != null && itemObject.getInfo() instanceof VideoObject) {
                        VideoObject videoObject = (VideoObject) itemObject.getInfo();
                        if (videoObject.getVideo() != null) episodes.add(videoObject.getVideo());
                    }
                }
                if (Utilities.notEmpty(episodes) && episodes.contains(currentVideo)) {
                    int currentEpisode = episodes.indexOf(currentVideo);
                    dialogFullScreenPlayer.setEpisodes(episodes, false);
                    dialogFullScreenPlayer.setCurrentEpisode(currentEpisode);
                    dialogFullScreenPlayer.setEpisodesView(true);
                }
            }
        }
    }

    private void showView() {
        int timeAnimation = isCanSetMediaSource ? 300 : 50;
        if (reHeader != null)
            reHeader.animate().translationY(0).setDuration(300).start();
        if (videoRecyclerView != null)
            videoRecyclerView.animate().alpha(1f).setDuration(timeAnimation).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    if (layoutManager != null && videoRecyclerView != null) {
                        try {
                            layoutManager.smoothScrollToPosition(videoRecyclerView, null, 0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (activity instanceof VideoPlayerActivity && !activity.isFinishing()) {
                        ((VideoPlayerActivity) activity).initViewDone();
                    }
                }
            }).start();
    }

    /**
     * play video
     *
     * @param holder item cần play
     */
    private void playVideo(VideoDetailAdapter.VideoHolder holder) {
        isAddVideoToListSeen = false;
        if (holder == null || isMini)
            return;
        logTimeVideoChannel();
        if (application != null)
            application
                    .getPref()
                    .edit()
                    .putLong(Constants.TabVideo.WATCH_VIDEO, System.currentTimeMillis())
                    .apply();
        if (mPlayer.getPlayerView() != null) {
            mPlayer.getPlayerView().setVisibility(View.VISIBLE);
            mPlayer.getPlayerView().showButtonCloseAds(false, 0);
        }
        ViewGroup viewLayout = holder.provideVideoFrame();
        HashMap<Integer, Object> map = (HashMap<Integer, Object>) viewLayout.getTag();
        Video video = (Video) map.get(0);
        int position = (int) map.get(1);
        if (currentPosition == position) {
            if (mPlayer != null && !mPlayer.getPlayWhenReady()) {
                addFrame(holder.provideVideoFrame());
                if (!isPause && !mPlayer.isAdDisplayed()) {
                    mPlayer.setPlayWhenReady(!currentVideo.isPause());
                }
            }
        } else {
            if (currentHolder != null)
                currentHolder.hideAds();
            if (adapter != null)
                adapter.setAdsConform(false);
            if (currentVideo != null) {
                Video tempVideo = currentVideo;
                tempVideo.setTimeCurrent(Math.max(0, mPlayer.getCurrentPosition()));
                tempVideo.setTimeDuration(mPlayer.getDuration());
                tempVideo.setResumeWindow(mPlayer.getCurrentWindowIndex());
                tempVideo.setPlaying(false);
                tempVideo.setPause(false);

                // thay đổi trạng thái play cho mảng gốc (mảng trong adapter là mảng sao)
                int tempPosition = currentPosition;
                Object object = items.get(tempPosition).getInfo();
                if (object instanceof VideoObject) {
                    ((VideoObject) object).getVideo().setPlaying(false);
                    ((VideoObject) object).getVideo().setPause(false);
                }

                VideoDetailAdapter.VideoHolder tempVideoHolder = currentHolder;
                tempVideoHolder.updateUiHide();
            }

            /*
             * lấy thông tin video cần play
             */
            heightCurrentVideo = holder.itemView.getHeight();
            currentHolder = holder;
            currentVideo = video;
            if (currentVideo != null) {
                currentVideo.setPlaying(true);
                currentVideo.setPause(false);
                currentPosition = position;
                // thay đổi trạng thái play cho mảng gốc (mảng trong adapter là mảng sao)
                Object object = items.get(currentPosition).getInfo();
                if (object instanceof VideoObject) {
                    ((VideoObject) object).getVideo().setPlaying(true);
                    ((VideoObject) object).getVideo().setPause(false);
                }
                /*
                 * add video vào trong item
                 */
                mPlayer.getPlayerView().setRefreshMode((float) currentVideo.getAspectRatio());
                addFrame(currentHolder.provideVideoFrame());
                Log.i(TAG, "Url playing: " + currentVideo.getOriginalPath());
//            if (videoPlayerView.getShutterView() instanceof ImageView)
//                ImageManager.showImage(currentVideo.getImagePath(), (ImageView) videoPlayerView.getShutterView());
                if (mPlayer.getPlayerView() != null) {
                    mPlayer.getPlayerView().showLogo(currentVideo.canShowLogo());
                    if (mPlayer.getPlaybackState() == Player.STATE_READY) {
                        mPlayer.getPlayerView().setCover(currentVideo.getImagePath());
                    } else {
                        mPlayer.getPlayerView().showCover(currentVideo.getImagePath());
                    }
                }
                if (mPlayer.getControlView() != null) {
//                    if (mPlayer.getCurrentPosition() == 0) {
                    campaignLayout = mPlayer.getControlView().getCampaignLayout();
                    campaignLayout.setCampaign(currentVideo.getCampaign());
                    campaignLayout.setPlayer(mPlayer);
                    try {
                        String config = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_TEXT_RUN_CONFIG);
                        if (!TextUtils.isEmpty(config) && !"-".equals(config)) {
                            JSONObject jsonObject = new JSONObject(config);
                            //{"start_time":5, "replay_time":30, "duration_time":20}
                            int startTime = jsonObject.optInt("start_time");
                            int replayTime = jsonObject.optInt("replay_time");
                            int durationTime = jsonObject.optInt("duration_time");
                            campaignLayout.setStartTime(startTime);
                            campaignLayout.setDurationTime(durationTime);
                            campaignLayout.setReplayTime(replayTime);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    campaignLayout.showTextRun();
//                    }
                    View quality = mPlayer.getPlayerView().getQualityView();
                    if (quality != null) {
                        quality.setVisibility(currentVideo.isHasListResolution() ? View.VISIBLE : View.GONE);
                    }
                }
                /*
                 * stream video
                 */
                if (isCanSetMediaSource) {
                    if ("keeng".equals(currentVideo.getSourceType().toLowerCase()))
                        currentVideo.setFromSource(Constants.LOG_SOURCE_TYPE.TYPE_KEENG_VIDEO);
                    else if ("netnews".equals(currentVideo.getSourceType().toLowerCase()))
                        currentVideo.setFromSource(Constants.LOG_SOURCE_TYPE.TYPE_NEWS_VIDEO);
                    else if ("tiin".equals(currentVideo.getSourceType().toLowerCase()))
                        currentVideo.setFromSource(Constants.LOG_SOURCE_TYPE.TYPE_TIIN_VIDEO);
                    else
                        currentVideo.setFromSource(Constants.LOG_SOURCE_TYPE.TYPE_MOCHA_VIDEO);
                    mPlayer.prepare(currentVideo);

                    boolean haveResumePosition = currentVideo.getResumeWindow() != C.INDEX_UNSET;
                    if (haveResumePosition) {
                        if (currentVideo.getTimeDuration() > 50)
                            mPlayer.seekTo(currentVideo.getTimeCurrent() > currentVideo.getTimeDuration() - 50 ? 0 : currentVideo.getTimeCurrent());
                        else
                            mPlayer.seekTo(0L);
                    }
                    if (!isPause) mPlayer.setPlayWhenReady(true);
                    PlaybackParameters param = new PlaybackParameters(1.0f);
                    mPlayer.setPlaybackParameters(param);
                } else {
                    isCanSetMediaSource = true;
                    if (!isPause) mPlayer.setPlayWhenReady(true);
                    if (mPlayer != null && mPlayer.getPlaybackState() == Player.STATE_ENDED) {
                        mPlayer.seekTo(0L);
                    }
                }
                if (!isFullScreen)
                    handlerMochaAds();
            }
        }
        if (currentHolder != null)
            currentHolder.updateUiHide();
    }

    private void handlerMochaAds() {
        bannerVideo = SharedPrefs.getInstance().get(MochaAdsClient.BANNER_VIDEO, BannerVideo.class);
        if (bannerVideo == null)
            return;
        BannerVideo.Filter filter = bannerVideo.getFilter();
        if (filter == null)
            return;
        if (Utilities.notEmpty(filter.getChannelId())) {
            if (!filter.getChannelId().contains(currentVideo.getChannel().getId()))
                return;
            else
                channelId = currentVideo.getChannel().getId();
        } else {
            channelId = "";
        }
        if (Utilities.notEmpty(filter.getNetworkType())) {
            boolean networkAccept = false;
            for (String s : filter.getNetworkType()) {
                if (s.toUpperCase().equals(NetworkHelper.getTextTypeConnectionForLog(application).toUpperCase()))
                    networkAccept = true;
            }
            if (!networkAccept) return;
        }
        if (Utilities.notEmpty(filter.getIsVip())) {
            String isVip = application.getReengAccountBusiness().isVip() ? "1" : "0";
            boolean vipAccept = false;
            for (String s : filter.getIsVip()) {
                if (s.toUpperCase().equals(isVip.toUpperCase()))
                    vipAccept = true;
            }
            if (!vipAccept) return;
        }
        if (bannerVideo.getlStartTime() > System.currentTimeMillis() || bannerVideo.getlEndTime() < System.currentTimeMillis())
            return;

        timeStartShowAds = bannerVideo.getIntervalItem() * 1000L;
        timeEndShowAds = bannerVideo.getDisplayTime() * 1000L;

        if (timeStartShowAds == timeEndShowAds) return;
        if (rootView == null) return;

        rootView.removeCallbacks(adsSchedule);
        if (mPlayer.getCurrentPosition() < timeStartShowAds) {
            rootView.post(adsSchedule);
        } else if (mPlayer.getCurrentPosition() > timeEndShowAds) {
            currentHolder.hideAds();
        }
    }

    private void logTimeVideoChannel() {
        if (currentVideo != null) {
            Channel currentChannel = currentVideo.getChannel();
            if (currentChannel != null) {
                currentChannel.setLastPublishVideo(currentVideo.getTimeCreate());
                ChannelDataSource.getInstance().saveTimeNewChannel(currentChannel);
            }
        }
    }

    /**
     * add video view vào frame
     *
     * @param frame frame add
     */
    private void addFrame(ViewGroup frame) {
        if (mPlayer.getPlayerView() != null && frame != null && !isFullScreen) {
            mPlayer.addPlayerViewTo(frame);
        }
    }

    /**
     * stop video
     */
    private void stopVideo() {
        if (mPlayer != null)
            mPlayer.setPlayWhenReady(false);
        if (mPlayer != null && mPlayer.getPlayerView() != null)
            mPlayer.getPlayerView().setVisibility(View.GONE);
    }

    /**
     * mở màn hình mini
     */
    private void openMini(String action) {
        if (activity != null) {
            if (dialogFullScreenPlayer != null) {
                dialogFullScreenPlayer.dismiss();
                mPlayer.setVisibleLayoutControl(false);
            }
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                activity.finish();
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            isMini = true;
            VideoService.start(application, currentVideo, playerName, action);

            if (popOut) {
                activity.finish();
                if (!BackStackHelper.getInstance().checkIsLastInStack(application, VideoPlayerActivity.class.getName())) {
                    /*
                     * nếu VideoPlayerActivity không phải là phần tử cuổi cùng
                     * thì sẽ thực hiên ACTION_MAIN
                     */
                    new Handler().postDelayed(() -> {
                        /*
                         * về màn hình home của thiết bị
                         */
                        if (application != null) {
                            Intent startMain = new Intent(Intent.ACTION_MAIN);
                            startMain.addCategory(Intent.CATEGORY_HOME);
                            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            application.startActivity(startMain);
                        }
                    }, 300);
                }
            } else {
                if (BackStackHelper.getInstance().checkIsLastInStack(application, VideoPlayerActivity.class.getName())) {
                    /*
                     * nếu là activity cuối cùng vì mở màn hình home
                     */
                    activity.goToHome();
                } else {
                    /*
                     * nếu không phải là activity cuối cùng thì back
                     */
                    activity.finish();
                }
            }
        }
    }

    /**
     * kiểm tra xem có thể mở mini không
     *
     * @return trạng thái có thể mở mini
     */
    private boolean isOpenMini() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(activity);
    }

    /**
     * chuyển đến video tiếp theo
     */
    private void handlerEnd() {
        if (mPlayer != null && mPlayer.getPlaybackState() == Player.STATE_ENDED) {
            mPlayer.logEnd();
            mPlayer.seekTo(0);
        }
        if (layoutManager != null && videoRecyclerView != null) {
            stopVideo();
            nextVideoForward();
        }
    }

    /**
     * hiển thị dialog more
     */
    public void handlerMore() {
        if (activity == null || activity.isFinishing()) return;
        if (optionsVideoDialog != null && optionsVideoDialog.isShowing())
            optionsVideoDialog.dismiss();

        optionsVideoDialog = new OptionsVideoDialog(activity, true)
                .setHasTitle(false)
                .setListItem(providerContextMenus(activity))
                .setListener((itemId, entry) -> {
                    switch (itemId) {
                        case Constants.MENU.MENU_SAVE_VIDEO:
                            if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                                activity.showDialogLogin();
                            } else {
                                currentVideo.setSave(!currentVideo.isSave());
                                onBtnSaveClicked(currentVideo);
                            }
                            break;
                        case Constants.MENU.MENU_REPORT_VIDEO:
                            if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                                activity.showDialogLogin();
                            else
                                reportVideo();
                            break;
                        case Constants.MENU.MENU_SPEED_VIDEO:
                            speedVideo();
                            break;
                        case Constants.MENU.MENU_MINI_VIDEO:
                            popOut = true;
                            openMini("");
                            break;
                        case Constants.MENU.MENU_VIP_VIDEO:
                            registerVideo();
                            break;
                        case Constants.MENU.MENU_EXIT:
                            break;
                    }
                });
        optionsVideoDialog.show();
    }

    /**
     * chất lượng video
     */
    public void handlerQuality() {
        if (activity == null || activity.isFinishing() || mPlayer == null || currentVideo == null)
            return;
        if (qualityVideoDialog != null && qualityVideoDialog.isShowing())
            qualityVideoDialog.dismiss();

        qualityVideoDialog = new QualityVideoDialog(activity)
                .setCurrentVideo(currentVideo)
                .setOnQualityVideoListener((idx, video, resolution) -> {
                    if (currentVideo != null && video != null
                            && !TextUtils.isEmpty(currentVideo.getId()) && !TextUtils.isEmpty(video.getId())
                            && currentVideo.getId().equals(video.getId())) {
                        if (currentVideo.getIndexQuality() == idx)
                            return;
                        currentVideo.setIndexQuality(idx);
                        if (application != null)
                            application.setConfigResolutionVideo(resolution.getKey());
                        long position;
                        long duration;
                        if (mPlayer != null) {
                            position = mPlayer.getCurrentPosition();
                            duration = mPlayer.getDuration();
                            mPlayer.prepare(resolution.getVideoPath(), video.getSubtitle(""));
                            mPlayer.seekTo(Math.min(position, duration));
                        }
                    }
                });
        qualityVideoDialog.show();
    }

    /**
     * chỉnh tốc độ video
     */
    private void speedVideo() {
        if (speedVideoDialog != null && speedVideoDialog.isShowing())
            speedVideoDialog.dismiss();

        if (mPlayer != null) {
            speedVideoDialog = DialogUtils.showSpeedDialog(activity, mPlayer.getPlaybackParameters().speed, (object, menuId) -> {
                if (menuId == Constants.MENU.MENU_SETTING && object instanceof Float) {
                    if (mPlayer.getVideo() == null || !Utilities.equals(mPlayer.getVideo(), currentVideo))
                        return;
                    PlaybackParameters param = new PlaybackParameters((Float) object);
                    mPlayer.setPlaybackParameters(param);
                }
            });
        }
    }

    /**
     * đăng ký gói video vip
     */
    private void registerVideo() {
        String labelOK = getResources().getString(R.string.register);
        String labelCancel = getResources().getString(R.string.cancel);
        String title = application.getConfigBusiness().getSubscriptionConfig().getTitle();
        String msg = application.getConfigBusiness().getSubscriptionConfig().getConfirm();
        PopupHelper.getInstance().showDialogConfirm(activity, title, msg, labelOK, labelCancel, mIconListener, null, Constants.MENU.POPUP_CONFIRM_REGISTER_VIP);

    }

    /**
     * report video
     */
    private void reportVideo() {
        if (activity == null || activity.isFinishing()) return;
        if (reportVideoDialog != null && reportVideoDialog.isShowing())
            reportVideoDialog.dismiss();

        reportVideoDialog = new ReportVideoDialog(activity)
                .setCurrentVideo(currentVideo)
                .setTitleDialog(application.getResources().getString(R.string.title_report_video))
        ;
        reportVideoDialog.show();
    }

    private void dismissAllDialogs() {
        if (optionsVideoDialog != null && optionsVideoDialog.isShowing()) {
            optionsVideoDialog.dismiss();
            optionsVideoDialog = null;
        }
        if (reportVideoDialog != null && reportVideoDialog.isShowing()) {
            reportVideoDialog.dismiss();
            reportVideoDialog = null;
        }
        if (speedVideoDialog != null && speedVideoDialog.isShowing()) {
            speedVideoDialog.dismiss();
            speedVideoDialog = null;
        }
        if (qualityVideoDialog != null && qualityVideoDialog.isShowing()) {
            qualityVideoDialog.dismiss();
            qualityVideoDialog = null;
        }
        if (shareBusiness != null) {
            shareBusiness.dismissAll();
            shareBusiness = null;
        }
    }

    /**
     * cung cấp item cho menu more
     *
     * @param activity activity
     * @return ArrayList
     */
    private ArrayList<ItemContextMenu> providerContextMenus(BaseSlidingFragmentActivity
                                                                    activity) {
        ArrayList<ItemContextMenu> mOverFlowItems = new ArrayList<>();
        mOverFlowItems.add(providerContextMenu(
                activity,
                currentVideo.isSave() ? R.string.video_remove_from_saved_list : R.string.save,
                currentVideo.isSave() ? R.drawable.ic_del_option : R.drawable.ic_add_option,
                Constants.MENU.MENU_SAVE_VIDEO));
        mOverFlowItems.add(providerContextMenu(activity, R.string.title_report_video, R.drawable.ic_flag_option, Constants.MENU.MENU_REPORT_VIDEO));
        if (!(currentVideo != null && currentVideo.isLive()))
            mOverFlowItems.add(providerContextMenu(activity, R.string.speed_video, R.drawable.ic_play_speed_option, Constants.MENU.MENU_SPEED_VIDEO));

        if (isOpenMini())
            mOverFlowItems.add(providerContextMenu(activity, R.string.pop_out, R.drawable.ic_v5_pop_out, Constants.MENU.MENU_MINI_VIDEO));

        if (ApplicationController.self() != null
                && !ApplicationController.self().getReengAccountBusiness().isVip()
                && ApplicationController.self().getReengAccountBusiness().isVietnam()
                && !ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
            String title = application.getConfigBusiness().getSubscriptionConfig().getTitle();
            mOverFlowItems.add(providerContextMenu(activity, title, R.drawable.ic_v5_video_vip, Constants.MENU.MENU_VIP_VIDEO));
        }
        mOverFlowItems.add(providerContextMenu(activity, R.string.btn_cancel_option, R.drawable.ic_close_option, Constants.MENU.MENU_EXIT));
        return mOverFlowItems;
    }

    /**
     * cung cấp ItemContextMenu
     *
     * @param activity     activity
     * @param mStringRes   mStringRes
     * @param mDrawableRes mDrawableRes
     * @param itemId       itemId
     * @return ItemContextMenu
     */
    private ItemContextMenu providerContextMenu(BaseSlidingFragmentActivity activity,
                                                @StringRes int mStringRes, @DrawableRes int mDrawableRes, int itemId) {
        return providerContextMenu(activity, activity.getString(mStringRes), mDrawableRes, itemId);
    }

    /**
     * cung cấp ItemContextMenu
     *
     * @param activity     activity
     * @param text         text
     * @param mDrawableRes mDrawableRes
     * @param itemId       itemId
     * @return ItemContextMenu
     */
    private ItemContextMenu providerContextMenu(BaseSlidingFragmentActivity
                                                        activity, String text, @DrawableRes int mDrawableRes, int itemId) {
        return new ItemContextMenu(text, mDrawableRes, null, itemId);
    }

    /**
     * đăng ký gói video vip
     *
     * @param id id
     */
    private void handleRegisterVip(String id) {
        if (!TextUtils.isEmpty(id)) {
            ReportHelper.checkShowConfirmOrRequestFakeMo(application, activity, application.getConfigBusiness().getSubscriptionConfig().getReconfirm(), id, "mocha_video");
        }
    }

    /**
     * play video tiếp theo
     */
    private void nextVideoForward() {
        try {
            int positionNext = Math.min(currentPosition + 1, items.size() - 1);
            layoutManager.smoothScrollToPosition(videoRecyclerView, null, positionNext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * trạng thái của item video
     *
     * @param isReady true - play, false - pause
     */
    private void changeStatusPlayer(boolean isReady) {
        if (currentVideo != null)
            currentVideo.setPause(!isReady);
    }

    public void onMiniClicked() {
        if (isOpenMini())
            openMini("");
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Hien popup xin quyen
            DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
            dialogConfirm.setLabel(getString(R.string.permission_allow_floating_view));
            dialogConfirm.setMessage(getString(R.string.permission_allow_floating_view_content));
            dialogConfirm.setUseHtml(true);
            dialogConfirm.setNegativeLabel(getString(R.string.cancel));
            dialogConfirm.setPositiveLabel(getString(R.string.ok));
            dialogConfirm.setPositiveListener(result -> {
                if (activity != null && isAdded()) {
                    autoPauseVideo();
                    try {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + activity.getPackageName()));
                        startActivityForResult(intent, 110);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });
            dialogConfirm.setNegativeListener(result -> activity.finish());
            dialogConfirm.show();
        } else
            activity.finish();
    }

    @Override
    public void onItemClicked(VideoDetailAdapter.VideoHolder videoHolder) {
        try {
            ViewGroup viewLayout = videoHolder.provideVideoFrame();
            HashMap<Integer, Object> map = (HashMap<Integer, Object>) viewLayout.getTag();
            int position = (int) map.get(1);
            layoutManager.smoothScrollToPosition(videoRecyclerView, null, position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBtnLikeClicked(Video video) {
        if (presenter != null) presenter.likeVideo(video);
    }

    @Override
    public void onBtnCommentClicked(Video video) {
        if (presenter != null) presenter.openComment(activity, video);
        if (isOpenMini()) {
            openMini(Constants.TabVideo.COMMENT);
        } else {
            autoPauseVideo();
        }
    }

    @Override
    public void onBtnShareClicked(Video video) {
        if (onActionFullScreenListener != null)
            onActionFullScreenListener.handlerShareVideo(video);
    }

    @Override
    public void onBtnSaveClicked(Video video) {
        activity.showToast(video.isSave() ? R.string.videoSavedToLibrary : R.string.videoRemovedFromSaved);
        if (presenter != null) presenter.saveVideo(video);
    }

    @Override
    public void onBtnSubscriptionClicked(Channel channel) {
        if (presenter != null) presenter.subscription(channel);
    }

    @Override
    public void onChannelInfoClicked(Channel channel) {
        if (presenter != null) presenter.openChannel(activity, channel);
        if (isOpenMini()) {
            openMini("");
        } else {
            autoPauseVideo();
        }
    }

    private void autoPauseVideo() {
        if (mPlayer != null) {
            mPlayer.setPaused(true);
            if (mPlayer.getAdPlayer() != null && mPlayer.isAdDisplayed()) {
                mPlayer.getAdPlayer().pause();
            }
            playWhenReady = mPlayer.getPlayWhenReady();
            mPlayer.setPlayWhenReady(false);
            isPause = true;
        }
    }

    @Override
    public void onShowShareDialog() {
        if (mPlayer != null) mPlayer.setPlayWhenReady(false);
    }

    @Override
    public void onDismissShareDialog(boolean isPlayingState) {
        if (!isPause) {
            if (mPlayer != null) mPlayer.setPlayWhenReady(isPlayingState);
        }
    }

    private class PreloadModelProvider implements ListPreloader.PreloadModelProvider<ItemObject> {
        @Override
        @NonNull
        public List<ItemObject> getPreloadItems(int position) {
            try {
                if (items != null && !items.isEmpty() && items.size() - 1 < position) {
                    ItemObject itemObject = items.get(position);
                    BaseAdapter.Clone mClone = itemObject.getInfo();
                    if (mClone instanceof VideoObject) {
                        return Collections.singletonList(itemObject);
                    }
                }
            } catch (Exception ignored) {
            }
            return Collections.emptyList();
        }

        @Override
        @Nullable
        public RequestBuilder getPreloadRequestBuilder(ItemObject itemObject) {
            BaseAdapter.Clone mClone = itemObject.getInfo();
            if (mClone instanceof VideoObject) {
                return ((VideoObject) mClone).getVideoLoader().getRequestBuilder();
            }
            return null;
        }
    }

}

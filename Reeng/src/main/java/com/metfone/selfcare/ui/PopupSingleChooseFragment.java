package com.metfone.selfcare.ui;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.metfone.selfcare.adapter.ListLanguageAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.Region;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 2/1/16.
 */
public class PopupSingleChooseFragment extends DialogFragment {
    private String menuTitle = "test";
    private ListLanguageAdapter menuAdapter = null;
    private SingleChooseListener clickHandler = null;
    private Context mContext;

    /**
     * @param context
     */

    public static PopupSingleChooseFragment newInstance(Context context, String title, ArrayList<Region> listItem,
                                                        SingleChooseListener callBack) {
        PopupSingleChooseFragment dialog = new PopupSingleChooseFragment();
        dialog.mContext = context;
        dialog.menuAdapter = new ListLanguageAdapter(context, listItem, null);
        dialog.menuTitle = title;
        dialog.clickHandler = callBack;
        dialog.setCancelable(true);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = initControl(inflater, container);
        if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(true);
        }
        //        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
        return v;
    }

    /**
     * init controller
     *
     * @param: n/a
     * @return: n/a
     * @throws: n/a
     */
    private View initControl(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.popup_context_menu, null, false);
        EllipsisTextView title = (EllipsisTextView) view
                .findViewById(R.id.context_menu_title);
        if (menuTitle != null) {
            title.setVisibility(View.VISIBLE);
            title.setEmoticon(mContext, menuTitle, menuTitle.hashCode(), menuTitle);
        } else {
            title.setVisibility(View.GONE);
        }
        ListView listItem = (ListView) view
                .findViewById(R.id.context_menu_listview);
        // add footer and header
        menuAdapter.setSingleChooseListener(clickHandler);
        listItem.setAdapter(menuAdapter);
        listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Region item = (Region) parent.getItemAtPosition(position);
                if (clickHandler != null) {
                    clickHandler.onChoose(item);
                }
                dismiss();
            }
        });
        return view;
    }

    public interface SingleChooseListener {
        void onChoose(Region region);
    }
}
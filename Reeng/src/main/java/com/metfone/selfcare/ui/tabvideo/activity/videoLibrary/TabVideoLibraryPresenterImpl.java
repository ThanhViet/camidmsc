package com.metfone.selfcare.ui.tabvideo.activity.videoLibrary;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.di.BaseView;

public class TabVideoLibraryPresenterImpl implements TabVideoLibraryPresenter {
    private ApplicationController application;
    private TabVideoLibraryView videoLibraryView;

    public TabVideoLibraryPresenterImpl(ApplicationController application, BaseView baseView) {
        this.application = application;
        this.videoLibraryView = (TabVideoLibraryView) baseView;
    }
}

package com.metfone.selfcare.ui.tabvideo.channelDetail.info;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.tabvideo.fragment.BaseViewStubFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class InfoChannelFragment extends BaseViewStubFragment implements OnChannelChangedDataListener {

    private static final String CHANNEL = "channel";

    @BindView(R.id.tv_channel_info)
    TextView tvChannelInfo;
    @BindView(R.id.ll_channel_info)
    LinearLayout llChannelInfo;
    @BindView(R.id.tv_channel_create_date)
    TextView tvChannelCreateDate;
    @BindView(R.id.ll_channel_create_date)
    LinearLayout llChannelCreateDate;
    Unbinder unbinder;

    public static InfoChannelFragment newInstance(Channel channel) {
        Bundle args = new Bundle();
        args.putSerializable(CHANNEL, channel);
        InfoChannelFragment fragment = new InfoChannelFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private @Nullable
    Channel mChannel;

    private ListenerUtils listenerUtils;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mChannel = (Channel) (bundle != null ? bundle.getSerializable(CHANNEL) : null);
        listenerUtils = application.getApplicationComponent().providerListenerUtils();
        listenerUtils.addListener(this);
    }

    @Override
    protected void onCreateViewAfterViewStubInflated(View inflatedView, Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, inflatedView);
        initView();
    }

    @Override
    protected int getViewStubLayoutResource() {
        return R.layout.fragment_channel_info;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listenerUtils.removerListener(this);
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void onChannelCreate(Channel channel) {

    }

    @Override
    public void onChannelUpdate(Channel channel) {
        if (channel == null || !channel.equals(mChannel)) return;
        mChannel = channel;
        initView();
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {

    }

    private void initView() {
        if (mChannel == null) return;
        if (Utilities.notEmpty(mChannel.getDescription())) {
            if (llChannelInfo != null) llChannelInfo.setVisibility(View.VISIBLE);
            if (tvChannelInfo != null)
                tvChannelInfo.setText(Html.fromHtml(mChannel.getDescription()));
        } else {
            if (llChannelInfo != null) llChannelInfo.setVisibility(View.GONE);
        }
        if (tvChannelCreateDate != null) {
            if (TextUtils.isEmpty(mChannel.getCreatedDate())) {
                tvChannelCreateDate.setVisibility(View.GONE);
            } else {
                tvChannelCreateDate.setText(Html.fromHtml(mChannel.getCreatedDate()));
                tvChannelCreateDate.setVisibility(View.VISIBLE);
            }
        }
    }
}

package com.metfone.selfcare.ui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.metfone.selfcare.util.Log;

import java.util.List;

public class JoinBitmaps {

    public static final int backgroud = 0x00ffffff;
    public static final int backgroudTextColor = 0xfff3f3f3;
    //
    public static final int crossAndRoundColor = 0xFFe5e5e5;
    public static final int crossAndRoundWidth = 2;
    private static final String TAG = JoinBitmaps.class.getSimpleName();

    public static Bitmap join(Canvas systemCanvas, int dimension, List<Bitmap> bitmaps, int amount, float gapSize) {
        if (bitmaps == null)
            return null;
        Bitmap newBitmap;
        // paint
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        Bitmap mainBitmap = Bitmap.createBitmap(dimension, dimension, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mainBitmap);
        canvas.save();
        canvas.drawColor(backgroud);
        float[] offset = JoinLayout.getOffset(dimension);
        for (int index = 0; index < bitmaps.size(); index++) {
            Bitmap bitmap = bitmaps.get(index);
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(dimension, dimension, Bitmap.Config.ARGB_8888);
            }
            // MATRIX
            Matrix matrix = new Matrix();
            // scale as destination
            matrix.postScale((float) dimension / bitmap.getWidth(), (float) dimension / bitmap.getHeight());
            canvas.save();
            float[] offsetForcus = JoinLayout.offsetForcus(amount, index, dimension);
            // translate
            canvas.translate(offsetForcus[0], offsetForcus[1]);
            Matrix matrixJoin = new Matrix();
            // scale as join size
            matrixJoin.postScale(offsetForcus[2], offsetForcus[2]);
            matrix.postConcat(matrixJoin);
            /*if (index == 3 && amount > 5) {// group co 3 friend hien ca avatar của mình là 4, co 4 friend hien 4 avatar friend, >4 friend (5 tv cả minh) hien dau +
                newBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                // Scaling
                Log.i(TAG, "join bitmap newBitmap.getWidth() = " + newBitmap.getWidth() + " -  dimension "+ dimension + " -  bitmap.getWidth() " + bitmap.getWidth());
                newBitmap = Bitmap.createBitmap(newBitmap, 0, 0, newBitmap.getWidth(), newBitmap.getHeight(), matrix, true);
                float fontsize = (int) (newBitmap.getWidth() / 1.7);
                // TODO: chen text vao goc phan tu duoi-phai.
                String txtCount = "+"; //(amount < 10) ? amount + "" : "9+";
                // draw text
                newBitmap = drawText(newBitmap, txtCount, fontsize, newBitmap.getWidth(), newBitmap.getHeight());
            } else {*/
                // Scaling
                newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
//            }
            // Cut rect
            float[] sizeCrop = JoinLayout.sizeCrop(amount, index, dimension);
            newBitmap = createMaskBitmap(newBitmap, gapSize, sizeCrop);
            // Cut circle
            /*float[] sizeCircleCrop = JoinLayout.sizeCircleCrop(amount, index, dimension);
            newBitmap = createCircleMaskBitmap(newBitmap, sizeCircleCrop, gapSize);*/
            canvas.drawBitmap(newBitmap, 0, 0, paint);
            canvas.restore();
        }
        drawCrossAndCircle(amount, canvas, offset, dimension, paint);
        canvas.restore();
        canvas.save();
        canvas.drawColor(backgroud);
        if (systemCanvas != null)
            systemCanvas.drawBitmap(mainBitmap, 0, 0, paint);
        canvas.restore();
        return mainBitmap;
    }


    public static void drawCrossAndCircle(int amount, Canvas canvas, float[] offset, float dimension, Paint paint) {

        paint.setColor(crossAndRoundColor);
        if (amount > 1) {
            // draw cross
            float l1 = dimension / 2 - crossAndRoundWidth / 2;
            float t1 = offset[1];
            float r1 = l1 + crossAndRoundWidth;
            float b1 = dimension - offset[1];
            canvas.drawRect(l1, t1, r1, b1, paint);
        } else
        if (amount > 2) {
            float l2 = dimension / 2;
            float t2 = dimension / 2 - crossAndRoundWidth / 2;
            float r2 = dimension - offset[0];
            float b2 = t2 + crossAndRoundWidth;
            canvas.drawRect(l2, t2, r2, b2, paint);
        }
        /*if (amount > 3) {
            float l2 = offset[0];
            float t2 = dimension / 2 - crossAndRoundWidth / 2;
            float r2 = dimension / 2;
            float b2 = t2 + crossAndRoundWidth;
            canvas.drawRect(l2, t2, r2, b2, paint);
        }*/
        // drawCycle
        /*paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(crossAndRoundWidth);

        canvas.drawCircle(dimension / 2, dimension / 2, (dimension - 2 * offset[0]) / 2, paint);*/
    }

    public static Bitmap drawText(Bitmap bitmap, String txt, float fontSize, int viewBoxW, int viewBoxH) {

        Canvas canvas = new Canvas(bitmap);
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        canvas.drawColor(backgroudTextColor);

        paint.setColor(Color.BLACK);
        paint.setTextSize(fontSize);

        //
        canvas.drawText(txt, viewBoxW / 3.9f, viewBoxH / 1.7f, paint);

        return bitmap;
    }


    public static Bitmap createMaskBitmap(Bitmap bitmap, float gapSize, float[] sizeCrop) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Paint paint = new Paint();
        paint.setAntiAlias(true);// Antialiasing
        paint.setFilterBitmap(true);

        canvas.drawRect(sizeCrop[0], sizeCrop[1], sizeCrop[2], sizeCrop[3], paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, 0, 0, paint);

        return output;
    }

    public static Bitmap createCircleMaskBitmap(Bitmap bitmap, float[] sizeCircleCrop, float gapSize) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Paint paint = new Paint();
        paint.setAntiAlias(true);// Antialiasing
        paint.setFilterBitmap(true);

        canvas.drawCircle(sizeCircleCrop[0], sizeCircleCrop[1], sizeCircleCrop[2], paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, 0, 0, paint);

        return output;
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

}
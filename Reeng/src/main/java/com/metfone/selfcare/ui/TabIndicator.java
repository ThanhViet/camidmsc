package com.metfone.selfcare.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;

public class TabIndicator extends View {

    private ViewGroup mTabWidget;
    private int mCurrentPosition;
    private float mCurrentPositionOffset;
    private Context mContext;
    private Drawable mDrawable;

    public TabIndicator(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public TabIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    public TabIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public void setTabWidget(ViewGroup tabWidget, int currentPosition) {
        mTabWidget = tabWidget;
        mCurrentPosition = currentPosition;
        mCurrentPositionOffset = 0f;
    }

    public void setCurrentPosition(int position) {
        mCurrentPosition = position;
        mCurrentPositionOffset = 0f;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mTabWidget == null)
            return;

        int tabCount = mTabWidget.getChildCount();
        View currentTab = mTabWidget.getChildAt(mCurrentPosition);
        if(currentTab ==null){
            return;
        }
        float currentTabLeft = currentTab.getLeft();
        float currentTabRight = currentTab.getRight();
        if (mCurrentPositionOffset != 0f && mCurrentPosition < tabCount) {
            float indicatorWidth = currentTabRight - currentTabLeft;
            currentTabLeft = currentTabLeft + (mCurrentPositionOffset * indicatorWidth);
            currentTabRight = currentTabRight + (mCurrentPositionOffset * indicatorWidth);
        }
        mDrawable.setBounds((int)currentTabLeft, 0, (int)currentTabRight, getHeight());
        mDrawable.draw(canvas);
    }

    private void initView() {
        mDrawable = ContextCompat.getDrawable(mContext, R.drawable.tab_indicator_drawable);
    }

    public void updateBottomIndicator(int position, float ratio) {
        mCurrentPosition = position;
        mCurrentPositionOffset = ratio;
        invalidate();
    }
}
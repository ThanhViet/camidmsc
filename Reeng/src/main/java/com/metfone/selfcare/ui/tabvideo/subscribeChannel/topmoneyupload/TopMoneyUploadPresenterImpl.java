/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/24
 *
 */

package com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.database.datasource.ChannelDataSource;
import com.metfone.selfcare.di.BasePresenter;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class TopMoneyUploadPresenterImpl extends BasePresenter<TopMoneyUploadContact.TopMoneyUploadView> implements TopMoneyUploadContact.TopMoneyUploadPresenter {
    private final int LIMIT = 20;
    private ArrayList<Channel> channels;
    private int offset = 0;
    private boolean errorApi = false;
    private boolean isLoadMore = false;
    private TopMoneyUploadCallback mTopMoneyUploadCallback;
    private Channel myChannel;

    public TopMoneyUploadPresenterImpl(TopMoneyUploadContact.TopMoneyUploadView view, ApplicationController application) {
        super(view, application);
        mTopMoneyUploadCallback = new TopMoneyUploadCallback();
        myChannel = utils.getChannelInfo();
    }

    @Override
    public void getTopMoneyUpload() {
        videoApi.getTopMoneyUpload(offset, LIMIT, mTopMoneyUploadCallback);
    }

    @Override
    public void getRefreshTopMoneyUpload() {
        offset = 0;
        getTopMoneyUpload();
    }

    @Override
    public void getMoreTopMoneyUpload() {
        getTopMoneyUpload();
    }

    @Override
    public void openDetail(BaseSlidingFragmentActivity activity, Channel channel) {
        utils.openChannelInfo(activity, channel);
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        super.onChannelSubscribeChanged(channel);
        if (Utilities.notEmpty(channels)) {
            for (Channel itemChannel : channels) {
                if (itemChannel.getId().equals(channel.getId())) {
                    itemChannel.setFollow(channel.isFollow());
                    itemChannel.setNumFollow(channel.getNumfollow());
                }
            }
            if (view != null)
                view.bindData(channels, isLoadMore);
        }
    }

    @Override
    public void onInternetChanged() {
        super.onInternetChanged();
        if (NetworkHelper.isConnectInternet(application) && Utilities.isEmpty(channels) && errorApi)
            getTopMoneyUpload();
    }

    public class TopMoneyUploadCallback implements ApiCallbackV2<ArrayList<Channel>> {

        @Override
        public void onSuccess(String lastIdStr, ArrayList<Channel> results) {
            errorApi = false;
            isLoadMore = false;
            if (channels == null)
                channels = new ArrayList<>();
            if (offset == 0)
                channels.clear();

            //if (Utilities.notEmpty(results) && Collections.disjoint(channels, results)) {
            if (Utilities.notEmpty(results)) {
                int size = results.size() - 1;
                for (int i = size; i >= 0; i--) {
                    Channel item = results.get(i);
                    if (item == null) results.remove(i);
                    else {
                        long timeLocalNewVideo = ChannelDataSource.getInstance().getTimeNewChannel(item.getId());
                        item.setHaveNewVideo(item.getLastPublishVideo() > timeLocalNewVideo);
                        if (myChannel != null && !Utilities.isEmpty(myChannel.getId()) && myChannel.getId().equals(item.getId()))
                            item.setMyChannel(true);
                    }
                }
                channels.addAll(results);
                offset = offset + LIMIT;
                isLoadMore = true;
            }
            if (view != null)
                view.bindData(channels, isLoadMore);
        }

        @Override
        public void onError(String s) {
            errorApi = true;
        }

        @Override
        public void onComplete() {
            if (view != null)
                view.loadDataCompleted();
        }
    }

    @Override
    public boolean isErrorApi() {
        return errorApi;
    }
}

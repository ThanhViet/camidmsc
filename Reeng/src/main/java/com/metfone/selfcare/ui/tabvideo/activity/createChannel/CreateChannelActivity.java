package com.metfone.selfcare.ui.tabvideo.activity.createChannel;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.CropImageActivity;
import com.metfone.selfcare.activity.ImageBrowserActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.common.utils.image.ImageUtils;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Utilities;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

@SuppressLint("SetTextI18n")
public class CreateChannelActivity extends BaseActivity implements CreateChannelView {

    private static final String CHANNEL = "channel";

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.ivChannelAvatar)
    ImageView ivChannelAvatar;
    @BindView(R.id.flChannelAvatar)
    LinearLayout flChannelAvatar;
    @BindView(R.id.tvLimitNameChannel)
    TextView tvLimitNameChannel;
    @BindView(R.id.edNameChannel)
    EditText edNameChannel;
    @BindView(R.id.tvLimitDescriptionChannel)
    TextView tvLimitDescriptionChannel;
    @BindView(R.id.edDescriptionChannel)
    EditText edDescriptionChannel;
    @BindView(R.id.btnCompleted)
    RoundTextView btnCompleted;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivAvatar)
    ImageView ivAvatar;
    @BindView(R.id.tvAvatar)
    TextView tvAvatar;
    @BindView(R.id.checkbox_accept_terms_conditions)
    AppCompatCheckBox checkboxAcceptTermsConditions;
    @BindView(R.id.tv_terms_and_conditions)
    TextView tvTermsAndConditions;
    @BindView(R.id.iv_channel_cover)
    ImageView ivChannelCover;

    @Inject
    CreateChannelPresenter createChannelPresenter;

    @Inject
    Utils utils;

    @Inject
    ImageUtils imageUtils;

    public static void start(Context context, Channel mChannel) {
        Intent starter = new Intent(context, CreateChannelActivity.class);
        starter.putExtra(CHANNEL, mChannel);
        context.startActivity(starter);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, CreateChannelActivity.class);
        starter.putExtra(CHANNEL, new Channel());
        context.startActivity(starter);
    }

    private File fireTakePhoto;
    private File cropImageFile;

    private boolean showAvatar = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
        setContentView(R.layout.activity_create_channel);
        ButterKnife.bind(this);

        initView();
        createChannelPresenter.initData((Channel) getIntent().getSerializableExtra(CHANNEL));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        hideKeyboard();
        super.onDestroy();
    }

    public void hideKeyboard() {
        if (!runnable && edNameChannel == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(edNameChannel.getWindowToken(), 0);
            inputMethodManager.hideSoftInputFromWindow(edDescriptionChannel.getWindowToken(), 0);
        }
    }

    private void initView() {
        checkboxAcceptTermsConditions.setOnCheckedChangeListener(mOnCheckedChangeListener);

        fireTakePhoto = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.PROFILE_PATH, "tmp" + System.currentTimeMillis() + Constants.FILE.JPEG_FILE_SUFFIX);
        cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.PROFILE_PATH, "avatar" + System.currentTimeMillis() + Constants.FILE.JPEG_FILE_SUFFIX);
        setOnTitleChangedListener();
        setOnDescriptionChangedListener();
    }

    @Override
    public void updateImage(String urlImage) {
        if (!runnable || imageUtils == null || ivChannelAvatar == null) return;
        imageUtils.loadCircle(urlImage, R.drawable.xml_background_iv_channel, ivChannelAvatar);
        ImageViewCompat.setImageTintList(ivAvatar, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.v5_main_color)));
        tvAvatar.setTextColor(getResources().getColor(R.color.v5_main_color));
        tvAvatar.setText(R.string.mc_change_avatar);
    }

    @Override
    public void updateCoverImage(String urlImage) {
        if (ivChannelCover == null || TextUtils.isEmpty(urlImage)) return;
        ImageManager.showImage(urlImage, ivChannelCover);
    }

    @Override
    public void updateTitleChannel(String name) {
        if (runnable && edNameChannel != null) edNameChannel.setText(name);
    }

    @Override
    public void updateButtonSubmit() {
        if (runnable && btnCompleted != null) btnCompleted.setText(R.string.sc_update_info);
    }

    @Override
    public void updateDescriptionChannel(String description) {
        if (runnable && edDescriptionChannel != null) edDescriptionChannel.setText(description);
    }

    private void setOnDescriptionChangedListener() {
        edDescriptionChannel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvLimitDescriptionChannel.setText(edDescriptionChannel.getText().length() + "/" + 200);
            }
        });
    }

    private void setOnTitleChangedListener() {
        edNameChannel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvLimitNameChannel.setText(edNameChannel.getText().length() + "/" + 50);
            }
        });
    }

    @Override
    public void changeTitle() {
        if (runnable && tvTitle != null) tvTitle.setText(R.string.update_channel);
    }

    @Override
    public void back() {
        new Handler().postDelayed(this::finish, 150);
    }

    @Override
    public void showMessage(int message) {
        showToast(message);
    }

    @Override
    public void showLoading() {
        showLoadingDialog("", R.string.loading);
    }

    @Override
    public void hideLoading() {
        hideLoadingDialog();
    }

    @Override
    public void openChannel(Channel channel) {
        utils.openChannelInfo(this, channel);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PermissionHelper.verifyPermissions(grantResults)) {
            if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO) {
                takePhoto();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.ACTION.ACTION_PICK_PICTURE:
                    if (data != null) {
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (picturePath != null && picturePath.size() > 0) {
                            String pathImage = picturePath.get(0);
                            cropAvatarImage(pathImage);
                        } else {
                            setActivityForResult(false);
                            showError(R.string.file_not_found_exception, null);
                        }
                    }
                    setActivityForResult(false);
                    setTakePhotoAndCrop(false);
                    break;
                case Constants.ACTION.ACTION_TAKE_PHOTO:
                    if (fireTakePhoto == null) return;
                    cropAvatarImage(fireTakePhoto.getPath());
                    setActivityForResult(false);
                    setTakePhotoAndCrop(false);
                    break;
                case Constants.ACTION.ACTION_CROP_IMAGE:
                    if (data != null) {
                        showAvatar = true;
                        Glide.with(ApplicationController.self())
                                .asBitmap()
                                .load(cropImageFile.getAbsolutePath())
                                .transition(withCrossFade(500))
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .placeholder(R.drawable.xml_background_iv_channel)
                                        .transforms(new CenterCrop(), new RoundedCornersTransformation(Utilities.dpToPx(30), 0)))
                                .into(ivChannelAvatar);
                    }
                    break;
            }
        } else {
            setActivityForResult(false);
            setTakePhotoAndCrop(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.ivBack, R.id.flChannelAvatar, R.id.btnCompleted, R.id.tv_terms_and_conditions})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.flChannelAvatar:
                openDialogTakeAvatar();
                break;
            case R.id.btnCompleted:
                if (checkboxAcceptTermsConditions != null && !checkboxAcceptTermsConditions.isChecked()) {
                    showDialogTermsAndConditions();
                    break;
                } else {
                    createChannelPresenter.updateOrCreateChannel(edNameChannel.getText().toString(),
                            edDescriptionChannel.getText().toString(),
                            cropImageFile.getAbsolutePath(), showAvatar);
                }
                break;
            case R.id.tv_terms_and_conditions:
                DeepLinkHelper.getInstance().openSchemaLink(this, "mocha://survey?ref=http://mocha.com.vn/rule");
                break;
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.CAPTURE_IMAGE:
                takeAPhoto();
                break;
            case Constants.MENU.SELECT_GALLERY:
                openGallery();
                break;
        }
    }

    private void showDialogTermsAndConditions() {
        DialogConfirm dialogConfirm = new DialogConfirm(this, false);
        dialogConfirm.setLabel(getString(R.string.notification));
        dialogConfirm.setMessage(getString(R.string.you_must_agree_to_the_terms_and_conditions_to_create_your_channel));
        dialogConfirm.setPositiveLabel(getString(R.string.close));
        dialogConfirm.show();
    }

    private void takeAPhoto() {
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(this, Manifest.permission.CAMERA, Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takePhoto();
        }
    }

    private void openGallery() {
        application.getReengAccountBusiness().removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    private void openDialogTakeAvatar() {
        DialogUtils.showOptionUpdateImage(this, this);
    }

    private void takePhoto() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(application, fireTakePhoto));
            intent.putExtra("return-data", true);

            setActivityForResult(true);
            setTakePhotoAndCrop(true);

            startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            showToast(R.string.prepare_photo_fail);
        }
    }

    private void cropAvatarImage(String inputFilePath) {
        try {
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, inputFilePath);
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.RETURN_DATA, false);
            intent.putExtra(CropView.MASK_OVAL, true);
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);
        } catch (Exception e) {
            showError(R.string.file_not_found_exception, null);
        }
    }

    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = (compoundButton, b) -> {
    };

}

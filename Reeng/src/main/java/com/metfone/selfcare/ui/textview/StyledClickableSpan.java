/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/27
 *
 */

package com.metfone.selfcare.ui.textview;

import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

public class StyledClickableSpan extends ClickableSpan {
    PatternEditableBuilder.SpannablePatternItem item;

    public StyledClickableSpan(PatternEditableBuilder.SpannablePatternItem item) {
        this.item = item;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        if (item.styles != null) {
            item.styles.onSpanStyled(ds);
        }
        super.updateDrawState(ds);
    }

    @Override
    public void onClick(View widget) {
        if (item.listener != null) {
            TextView tv = (TextView) widget;
            Spanned span = (Spanned) tv.getText();
            int start = span.getSpanStart(this);
            int end = span.getSpanEnd(this);
            CharSequence text = span.subSequence(start, end);
            item.listener.onSpanClicked(text.toString());
        }
        widget.invalidate();
    }
}

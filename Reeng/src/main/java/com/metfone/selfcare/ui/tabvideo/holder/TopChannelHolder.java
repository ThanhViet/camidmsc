/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/23
 *
 */

package com.metfone.selfcare.ui.tabvideo.holder;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.widget.CustomDividerDecoration;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.tabvideo.adapter.TopChannelAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelListener;

import java.util.ArrayList;

import butterknife.BindView;

public class TopChannelHolder extends BaseAdapterV2.ViewHolder {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private TopChannelAdapter adapter;

    public TopChannelHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent, OnChannelListener listener) {
        super(layoutInflater.inflate(R.layout.holder_top_channel_video, parent, false));
        adapter = new TopChannelAdapter(activity);
        adapter.setOnItemListener(listener);
        if (recyclerView.getItemDecorationCount() <= 0) {
            CustomDividerDecoration dividerItemDecoration = new CustomDividerDecoration(activity, LinearLayoutManager.HORIZONTAL);
            dividerItemDecoration.setDrawable(activity.getResources().getDrawable(R.drawable.divider_default));
            recyclerView.addItemDecoration(dividerItemDecoration);

            recyclerView.setLayoutManager(new CustomLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(null);
            recyclerView.setLayoutAnimation(null);
            recyclerView.setItemViewCacheSize(5);
            recyclerView.setDrawingCacheEnabled(true);
        }
        recyclerView.setAdapter(adapter);
    }

    public void bindData(ArrayList<Object> items, int position) {
        if (adapter != null) adapter.bindData(items);
    }
}

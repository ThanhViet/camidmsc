package com.metfone.selfcare.ui.view.tab_video;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.util.Log;

public class EventLogger extends com.google.android.exoplayer2.util.EventLogger {

    private long throughput = 0;
    private double totalTimeLoad = 0;
    private double bytesLoadedSeconds = 0;
    private double bytesLoaded = 0;
    private long countLoadedData = 0;
    private double totalByteRate = 0;

    public EventLogger(@Nullable MappingTrackSelector trackSelector) {
        super(trackSelector);
        throughput = 0;
        bytesLoadedSeconds = 0;
        bytesLoaded = 0;
        totalTimeLoad = 0;
        countLoadedData = 0;
        totalByteRate = 0;
    }

    public void reset() {
        if (BuildConfig.DEBUG) {
            Log.d("EventLogger", "reset: " + toString());
        }
        throughput = 0;
        bytesLoadedSeconds = 0;
        bytesLoaded = 0;
        totalTimeLoad = 0;
        countLoadedData = 0;
        totalByteRate = 0;
    }

    public void onLoadCompleted(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        throughput = throughput + loadEventInfo.bytesLoaded;
        float diffInSeconds = loadEventInfo.loadDurationMs / 1000.0f;
        bytesLoaded = loadEventInfo.bytesLoaded;
        bytesLoadedSeconds = diffInSeconds;
        totalTimeLoad = totalTimeLoad + diffInSeconds;

        if ((bytesLoadedSeconds > 0) && (bytesLoaded > 0)) {
            double currentByteRate = bytesLoaded / bytesLoadedSeconds;
            countLoadedData++;
            totalByteRate = totalByteRate + currentByteRate;
        }
        if (BuildConfig.DEBUG) {
            Log.d("EventLogger", "onLoadCompleted: " + toString());
        }
    }

    public double getBitrate() {
        if (countLoadedData > 0 && totalByteRate > 0) {
            return totalByteRate * 8 / countLoadedData;
        }
        return 0;
    }

    public long getThroughput() {
        return throughput;
    }

    public void setThroughput(long throughput) {
        this.throughput = throughput;
    }

    public double getBytesLoadedSeconds() {
        return bytesLoadedSeconds;
    }

    public void setBytesLoadedSeconds(double bytesLoadedSeconds) {
        this.bytesLoadedSeconds = bytesLoadedSeconds;
    }

    public double getBytesLoaded() {
        return bytesLoaded;
    }

    public void setBytesLoaded(double bytesLoaded) {
        this.bytesLoaded = bytesLoaded;
    }

    public double getTotalTimeLoad() {
        return totalTimeLoad;
    }

    public void setTotalTimeLoad(double totalTimeLoad) {
        this.totalTimeLoad = totalTimeLoad;
    }

    @Override
    public String toString() {
        return "{" +
                "throughput=" + throughput +
                ", totalTimeLoad=" + totalTimeLoad +
                ", bytesLoadedSeconds=" + bytesLoadedSeconds +
                ", bytesLoaded=" + bytesLoaded +
                ", countLoadedData=" + countLoadedData +
                ", totalByteRate=" + totalByteRate +
                '}';
    }
}

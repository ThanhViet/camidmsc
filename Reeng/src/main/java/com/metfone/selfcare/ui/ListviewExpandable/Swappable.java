package com.metfone.selfcare.ui.ListviewExpandable;

/**
 * Created by thanhnt72 on 6/21/2016.
 */
public interface Swappable {

    /**
     * Swaps the item on the first adapter position with the item on the second adapter position.
     * Be sure to call {@link android.widget.BaseAdapter#notifyDataSetChanged()} if appropriate when implementing this method.
     *
     * @param positionOne First adapter position.
     * @param positionTwo Second adapter position.
     */
    void swapItems(int positionOne, int positionTwo);
}

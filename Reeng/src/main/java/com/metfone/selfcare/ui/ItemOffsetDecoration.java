package com.metfone.selfcare.ui;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by HaiKE on 8/9/17.
 */

public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
    private int offset;

    public ItemOffsetDecoration(int offset) {
        this.offset = offset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        int bottonIndex;
//        if (parent.getAdapter().getItemCount() % 2 == 0) {
//            bottonIndex = parent.getAdapter().getItemCount() - 2;
//        } else {
            bottonIndex = parent.getAdapter().getItemCount();
//        }

        int position = parent.getChildAdapterPosition(view);

        if (position < bottonIndex) {
            outRect.bottom = offset;
        } else {
            outRect.bottom = 0;
        }

        if (position > 1) {
            outRect.top = offset;
        } else {
            outRect.top = 0;
        }

        if(position % 9 == 0 || (position - 3) % 9 == 0 || (position - 6) % 9 == 0 || (position - 8) % 9 == 0)
        {
            outRect.right = offset;
            outRect.left = 0;
        }
        else if((position - 1) % 9 == 0 || (position - 2) % 9 == 0 || (position - 5) % 9 == 0 || (position - 7) % 9 == 0)
        {
            outRect.right = 0;
            outRect.left = offset;
        }
        else
        {
            outRect.right = offset;
            outRect.left = offset;
        }


//        if ((parent.getChildAdapterPosition(view) % 2) == 0) {
//            outRect.right = offset;
//            outRect.left = 0;
//        } else {
//            outRect.right = 0;
//            outRect.left = offset;
//        }

    }
}
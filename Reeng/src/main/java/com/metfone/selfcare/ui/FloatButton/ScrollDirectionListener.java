package com.metfone.selfcare.ui.FloatButton;

public interface ScrollDirectionListener {
    void onScrollDown();

    void onScrollUp();
}
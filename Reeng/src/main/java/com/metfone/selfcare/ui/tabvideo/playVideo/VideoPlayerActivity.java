package com.metfone.selfcare.ui.tabvideo.playVideo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.blankj.utilcode.util.StringUtils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV3;
import com.metfone.selfcare.common.utils.AnimationUtil;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.widget.floatingView.MusicFloatingView;
import com.metfone.selfcare.module.livestream.LiveStreamActivity;
import com.metfone.selfcare.module.livestream.model.ConfigLiveComment;
import com.metfone.selfcare.module.movie.fragment.FilmDetailFragment;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail.MovieDetailFragment;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.VideoDetailFragment;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.util.IOnBackPressed;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tuanha00 on 3/15/2018.
 * màn hình play video
 */

public class VideoPlayerActivity extends BaseActivity implements VideoPlayerContact.VideoPlayerView,
        ApiCallbackV3<Movie> {
    IOnBackPressed iOnBackPressed;
    private static final String TAG = "VideoPlayerActivity";
    @BindView(R.id.videoShimmerFrameLayout)
    ShimmerFrameLayout videoShimmerFrameLayout;
    //@BindView(R.id.reveal_view)
    //RevealBackgroundView revealView;
    @BindView(R.id.rootVideoDetailActivity)
    View rootVideoDetailActivity;
    @Inject
    VideoPlayerContact.VideoPlayerPresenter videoDetailPresenter;
    boolean installedKeeng = false;
    private FeedAction feedAction;
    private String playerTag;
    private Video currentVideo;
    private Movie currentMovie;
    private boolean fromLoading;
    private boolean playFromService;
    private MovieApi movieApi;
    private static boolean mIsPlaynow = false;
    private Runnable showLoadingRunnable = new Runnable() {
        @Override
        public void run() {
            showLoading();
        }
    };

    public static void startFromService(Context context, Video video, String tag, boolean fromLoading) {
        VideoService.stop(context);
//        KeengFloatingViewService.stop(context);
        MusicFloatingView.stop(context);
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.TabVideo.VIDEO, video);
        intent.putExtra(Constants.TabVideo.PLAYER_TAG, tag);
        intent.putExtra(Constants.TabVideo.PLAYER_INIT, fromLoading);
        intent.putExtra(Constants.TabVideo.PLAY_FROM_SERVICE, true);
        mIsPlaynow = false;
        context.startActivity(intent);
    }

    public static void start(Context context, Video video, String tag, boolean fromLoading) {
        VideoService.stop(context);
//        KeengFloatingViewService.stop(context);
        MusicFloatingView.stop(context);
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.TabVideo.VIDEO, video);
        intent.putExtra(Constants.TabVideo.PLAYER_TAG, tag);
        intent.putExtra(Constants.TabVideo.PLAYER_INIT, fromLoading);
        mIsPlaynow = false;
        context.startActivity(intent);
    }

    public static void start(Context context, Movie movie, String tag, boolean fromLoading) {
        VideoService.stop(context);
//        KeengFloatingViewService.stop(context);
        MusicFloatingView.stop(context);
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.TabVideo.MOVIE, movie);
        intent.putExtra(Constants.TabVideo.PLAYER_TAG, tag);
        intent.putExtra(Constants.TabVideo.PLAYER_INIT, fromLoading);
        mIsPlaynow = false;
        context.startActivity(intent);
    }

    public static void playNow(Context context, Movie movie, Video video, String tag) {
        VideoService.stop(context);
        MusicFloatingView.stop(context);
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.TabVideo.MOVIE, movie);
        intent.putExtra(Constants.TabVideo.VIDEO, video);
        intent.putExtra(Constants.TabVideo.PLAYER_TAG, tag);
        intent.putExtra(Constants.TabVideo.PLAYER_INIT, false);
        mIsPlaynow = true;
        context.startActivity(intent);
    }

    public static void playNow(Context context,Movie movie,Video video,ArrayList<Movie> episodes,String tag){
        VideoService.stop(context);
        MusicFloatingView.stop(context);
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.TabVideo.MOVIE, movie);
        intent.putExtra(Constants.TabVideo.VIDEO, video);
        intent.putExtra(Constants.TabVideo.PLAYER_TAG, tag);
        intent.putExtra(Constants.TabVideo.PLAYER_INIT, false);
        intent.putExtra(Constants.TabVideo.ARR_EPISODES_MOVIE,episodes);
        mIsPlaynow = true;
        context.startActivity(intent);
    }

    public static void start(Context context, ArrayList<Video> list, int currentPosition, String channelId) {
        VideoService.stop(context);
        MusicFloatingView.stop(context);
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.TabVideo.LIST_DATA_VIDEO, list);
        intent.putExtra(Constants.TabVideo.POSITION, currentPosition);
        intent.putExtra(Constants.TabVideo.PLAYER_TAG, channelId);
        intent.putExtra(Constants.TabVideo.PLAYER_INIT, false);
        mIsPlaynow = false;
        context.startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
        Intent intent = getIntent();
        if (intent != null) {
            playerTag = intent.getStringExtra(Constants.TabVideo.PLAYER_TAG);
            fromLoading = intent.getBooleanExtra(Constants.TabVideo.PLAYER_INIT, true);
            playFromService = intent.getBooleanExtra(Constants.TabVideo.PLAY_FROM_SERVICE, false);
            Serializable serializable;
            serializable = intent.getSerializableExtra(Constants.TabVideo.VIDEO);
            if (serializable instanceof Video) {
                currentVideo = (Video) serializable;
            }
            serializable = intent.getSerializableExtra(Constants.TabVideo.MOVIE);
            if (serializable instanceof Movie) {
                currentMovie = (Movie) serializable;
                currentVideo = Movie.movie2Video(currentMovie);
            }
            serializable = intent.getSerializableExtra(Constants.TabVideo.LIST_DATA_VIDEO);
            if (serializable instanceof ArrayList) {
                ArrayList<Video> list = null;
                final int position = intent.getIntExtra(Constants.TabVideo.POSITION, 0);
                final String channelId = intent.getStringExtra(Constants.TabVideo.PLAYER_TAG);
                try {
                    list = (ArrayList<Video>) serializable;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Utilities.notEmpty(list)) {
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    if (videoShimmerFrameLayout != null) {
                        final ArrayList<Video> finalList = list;
                        AnimationUtil.fadeInView(videoShimmerFrameLayout, 200, new AnimationUtil.AnimationListener() {
                            @Override
                            public void onAnimationEnd(View view) {
                                if (videoShimmerFrameLayout != null) {
                                    videoShimmerFrameLayout.startShimmer();
                                }
                                getSupportFragmentManager().beginTransaction().replace(R.id.rootVideoDetailActivity
                                        , VideoDetailFragment.newInstance(finalList, position, channelId, playerTag)).commitAllowingStateLoss();
                            }
                        });
                    }
                    return;
                }
            }
        }
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        setContentView(R.layout.activity_video_detail);
        ButterKnife.bind(this);
        showRoot();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        if (videoDetailPresenter != null)
            videoDetailPresenter.dispose();
        if (videoShimmerFrameLayout != null)
            videoShimmerFrameLayout.stopShimmer();
        super.onDestroy();
    }

    @Override
    protected void setTransitionFinish() {
    }

    @Override
    protected void setTransitionOnCreate() {
    }

    @Override
    public void onSuccess(String lastId, Movie movie, MovieWatched movieWatched) throws JSONException {
        if (movieWatched != null && !TextUtils.isEmpty(movieWatched.getId())) {
            if (movie.getId().equals(movieWatched.getId())) {
                movie.setTimeWatched(movieWatched.getTimeSeek());
            }
        }
        if (currentMovie != null && currentMovie.isConvertFromMovieWatched())
            movie.setConvertFromMovieWatched(true);
        openMovieDetail(movie);
    }

    @Override
    public void onError(String s) {
        String movieUnavaialble = getResources().getString(R.string.movies_info_unavailable);
        if (s.equals(movieUnavaialble)) {
            showDialogPlayerError(R.string.video_label_cannot_play_movies, R.string.movies_info_unavailable);
        } else {
            showDialogPlayerError(R.string.video_label_cannot_play_movies, R.string.movies_info_unavailable);
        }
    }

    @Override
    public void onComplete() {

    }

    private void showRoot() {
        if (!fromLoading && currentMovie != null && !TextUtils.isEmpty(currentMovie.getId()) && currentMovie.getOriginalPath() != null) {
            openMovieDetail(currentMovie);
        } else if (!fromLoading && currentVideo != null && !TextUtils.isEmpty(currentVideo.getId()) && !TextUtils.isEmpty(currentVideo.getOriginalPath())) {
            openVideoDetail(currentVideo);
        } else {
            fromLoading = true;
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            rootVideoDetailActivity.post(showLoadingRunnable);
        }
    }

    UserInfoBusiness mUserInfoBusiness = new UserInfoBusiness(getBaseContext());
    String phone_number = mUserInfoBusiness.getUser().getPhone_number();

    private void showLoading() {
        if (videoShimmerFrameLayout != null)
            AnimationUtil.fadeInView(videoShimmerFrameLayout, 200, new AnimationUtil.AnimationListener() {
                @Override
                public void onAnimationEnd(View view) {
                    if (videoShimmerFrameLayout != null) {
                        videoShimmerFrameLayout.startShimmer();
                    }
                    if (currentMovie != null && videoDetailPresenter != null) {
                        // TODO: 4/8/2020 Thainn sửa code
                        getMovieApi().getMovieDetail(currentMovie.getId(), false, phone_number, VideoPlayerActivity.this);

                    } else if (currentVideo != null && videoDetailPresenter != null) {
                        if (TextHelper.getInstant().isLinkMoviesDetail(currentVideo.getLink())) {
                            getMovieApi().getMovieDetail(currentVideo.getLink(), true, phone_number, VideoPlayerActivity.this);
                        } else if (currentMovie != null) {
                            getMovieApi().getMovieDetail(currentMovie.getId(), false, phone_number, VideoPlayerActivity.this);
                        } else {
                            videoDetailPresenter.getVideoDetail(currentVideo);
                        }
                    }
                }
            });
    }

    @Override
    public void openVideoDetail(Video video) {
        if (video.getIsPrivate() == 1
                && application.getReengAccountBusiness() != null
                && !application.getReengAccountBusiness().isCBNV()) {
            showDialogPlayerError(R.string.video_private_notify_title, R.string.video_private_notify_content);
            return;
        }
        video.setFromOnMedia(currentVideo.isFromOnMedia());
        video.setCategoryId(currentVideo.getCategoryId());
        video.setRecommendType(currentVideo.getRecommendType());
        if (currentVideo.getFilmGroup() != null && (video.getFilmGroup() == null || (video.getFilmGroup() != null && Utilities.isEmpty(video.getFilmGroup().getGroupId()))))
            video.setFilmGroup(currentVideo.getFilmGroup());

        int id;
        try {
            id = Integer.valueOf(video.getFilmGroup().getGroupId());
        } catch (Exception e) {
            id = 0;
        }

        if (TextHelper.getInstant().isLinkMoviesDetail(video.getLink())) {
            /*
             * mở màn hình phim
             */
            if (Utilities.isEmpty(playerTag)) {
                playerTag = String.valueOf(System.nanoTime());
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.TabVideo.VIDEO, video);
            bundle.putString(Constants.TabVideo.PLAYER_TAG, playerTag);
            bundle.putBoolean(Constants.TabVideo.PLAYER_INIT, fromLoading);
            bundle.putBoolean(Constants.TabVideo.PLAY_FROM_SERVICE, playFromService);
            getSupportFragmentManager().beginTransaction().replace(R.id.rootVideoDetailActivity,
                    FilmDetailFragment.newInstance(bundle, mIsPlaynow)).commitAllowingStateLoss();
            initViewDone();
        } else if (video.getFilmGroup() != null && Utilities.notEmpty(video.getFilmGroup().getGroupId()) && id > 0) {
            /*
             * mở giao diện phim
             */
            getSupportFragmentManager().beginTransaction().replace(R.id.rootVideoDetailActivity
                    , MovieDetailFragment.newInstance(video, playerTag, fromLoading)).commitAllowingStateLoss();
            initViewDone();
        } else {
            if (video.isLive()) {
                ConfigLiveComment configLiveComment = null;
                ContentConfigBusiness configBusiness = application.getConfigBusiness();
                String json = configBusiness.getContentConfigByKey(Constants.PREFERENCE.CONFIG.CONFIG_LIVE_COMMENT);
                if (Utilities.notEmpty(json) && !"-".equals(json)) {
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        String domainAPI = jsonObject.getString("domainAPI");
                        String domainWS = jsonObject.getString("domainWS");
                        String publicKey = jsonObject.getString("publicKey");
                        configLiveComment = new ConfigLiveComment(domainAPI, domainWS, publicKey);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (configLiveComment != null) {
                    LiveStreamActivity.startActivity(this, video, configLiveComment);
                    finish();
                    return;
                }
            }
            /*
             * mỏ giao diện video detail
             */
            getSupportFragmentManager().beginTransaction().replace(R.id.rootVideoDetailActivity
                    , VideoDetailFragment.newInstance(video, playerTag, fromLoading)).commitAllowingStateLoss();
        }
        //initViewDone();
    }

    @Override
    public void openMovieDetail(Movie movie) {
        if (Constants.MOVIE_TYPE_NORMAL.equalsIgnoreCase(movie.getTypeFilm())) {
            currentMovie = movie;

            // fake data subtitle and audio
//            List<SubtitleAudio> listSubtitle = new ArrayList<>();
//            List<SubtitleAudio> listAudio = new ArrayList<>();
//            listSubtitle.add(new SubtitleAudio("1", "123", "fr", ""));
//            listAudio.add(new SubtitleAudio("1", "123", "en", ""));
//            listAudio.add(new SubtitleAudio("1", "123", "vi", ""));
//            currentMovie.setListSubtitle(listSubtitle);
//            currentMovie.setListAudio(listAudio);
            currentVideo = Movie.movie2Video(currentMovie);
            if (Utilities.isEmpty(playerTag)) {
                playerTag = String.valueOf(System.nanoTime());
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.TabVideo.MOVIE, movie);
            bundle.putString(Constants.TabVideo.PLAYER_TAG, playerTag);
            bundle.putBoolean(Constants.TabVideo.PLAYER_INIT, fromLoading);
            bundle.putBoolean(Constants.TabVideo.PLAY_FROM_SERVICE, playFromService);
            bundle.putSerializable(Constants.TabVideo.ARR_EPISODES_MOVIE,getIntent().getSerializableExtra(Constants.TabVideo.ARR_EPISODES_MOVIE));
            getSupportFragmentManager().beginTransaction().replace(R.id.rootVideoDetailActivity,
                    FilmDetailFragment.newInstance(bundle, mIsPlaynow)).commitAllowingStateLoss();
            initViewDone();
        } else {
            showDialogMovieNotPlay(movie);
        }
    }

    @Override
    public void openMovieDetail(Movie movie, ArrayList<Movie> allEpisode) {
        if (Constants.MOVIE_TYPE_NORMAL.equalsIgnoreCase(movie.getTypeFilm())) {
            currentMovie = movie;
            currentVideo = Movie.movie2Video(currentMovie);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.TabVideo.MOVIE, movie);
            bundle.putSerializable(Constants.TabVideo.ARR_EPISODES_MOVIE, allEpisode);
            bundle.putString(Constants.TabVideo.PLAYER_TAG, playerTag);
            bundle.putBoolean(Constants.TabVideo.PLAYER_INIT, fromLoading);
            bundle.putBoolean(Constants.TabVideo.PLAY_FROM_SERVICE, playFromService);
            getSupportFragmentManager().beginTransaction().replace(R.id.rootVideoDetailActivity,
                    FilmDetailFragment.newInstance(bundle, mIsPlaynow)).commitAllowingStateLoss();
            initViewDone();
        } else {
            showDialogMovieNotPlay(movie);
        }
    }

    public FeedAction getFeedAction() {
        return feedAction;
    }

    public void showDialogMovieNotPlay(final Movie movie) {
        String msg, button;
        if (ViewUtils.appInstalledOrNot(this, Constants.PREF_DEFAULT.PREF_DEF_KEENG_PACKAGE)) {
            installedKeeng = true;
            msg = getString(R.string.content_only_on_keeng);
            button = getString(R.string.open_keeng);
        } else {
            installedKeeng = false;
            msg = getString(R.string.content_download_app_keeng);
            button = getString(R.string.install_keeng);
        }
        DialogConfirm dialogConfirm = new DialogConfirm(this, false);
        dialogConfirm.setMessage(msg);
        dialogConfirm.setPositiveLabel(button);
        dialogConfirm.setNegativeLabel(getString(R.string.cancel));
        dialogConfirm.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                finish();
            }
        });
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                if (installedKeeng) {
                    try {
                        if (movie == null || TextUtils.isEmpty(movie.getLinkWap())) return;
                        String link = movie.getLinkWap();
                        Uri uri = Uri.parse("keeng://url?target=" + link);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.e(TAG, e);
                    }
                } else {
                    NavigateActivityHelper.navigateToPlayStore(VideoPlayerActivity.this, Constants.PREF_DEFAULT.PREF_DEF_KEENG_PACKAGE);
                }
                finish();
            }
        });
        dialogConfirm.show();
    }

    @Override
    public void openDialogConfirmBack() {
        showDialogPlayerError(R.string.video_label_cannot_play_video, R.string.video_message_cannot_play_video);
    }

    public void showDialogPlayerError(int resTitle, int resMsg) {
        if (isFinishing()) return;
        try {
            DialogConfirm dialog = new DialogConfirm(VideoPlayerActivity.this, true);
            if (resTitle > 0) dialog.setLabel(getString(resTitle));
            if (resMsg > 0) dialog.setMessage(getString(resMsg));
            dialog.setPositiveLabel(getString(R.string.ok));
            dialog.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    finish();
                }
            });
            dialog.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    finish();
                }
            });
            dialog.setDismissListener(new DismissListener() {

                @Override
                public void onDismiss() {
                    finish();
                }
            });
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            int threadId;
            switch (requestCode) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }

    public void initViewDone() {
        Log.d(TAG, "initViewDone");
        try {
            getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (videoShimmerFrameLayout != null) {
            videoShimmerFrameLayout.stopShimmer();
            videoShimmerFrameLayout.setVisibility(View.GONE);
        }
    }

    public MovieApi getMovieApi() {
        if (movieApi == null) movieApi = new MovieApi();
        return movieApi;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View viewRate = findViewById(R.id.viewRate);
        if (viewRate != null) {
            Rect viewRect = new Rect();
            viewRate.getGlobalVisibleRect(viewRect);
            if (viewRate.getVisibility() == View.VISIBLE && !viewRect.contains((int) ev.getRawX(), (int) ev.getRawY())) {
                viewRate.setVisibility(View.GONE);
                return true;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(iOnBackPressed != null){
            iOnBackPressed.onBackPressed();
        }
    }
}

package com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail;

import androidx.fragment.app.Fragment;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.di.MvpPresenter;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;

import java.util.ArrayList;

public interface SubscribeChannelContact {
    interface SubscribeChannelView extends BaseView {

        void bindData(ArrayList<Channel> results);

        void bindData(ArrayList<Video> videos, boolean loadMore);

        void showNotifyDataEmpty();

        void showNotifyNotNetwork();

        void showDialogMoreOption(Video video);

        void loadVideoNewPublishCompleted();

        void loadChannelUserFollowCompleted();
    }

    interface SubscribeChannelPresenter extends MvpPresenter {
        void getChannelUserFollow();

        void getVideoNewPublish();

        void getRefreshVideoNewPublish();

        void getMoreVideoNewPublish();

        void openDetail(BaseSlidingFragmentActivity activity, Video video);

        void notifyLike(BaseSlidingFragmentActivity activity, Video video);

        void openShare(BaseSlidingFragmentActivity activity, Video video);

        void openComment(BaseSlidingFragmentActivity activity, Video video);

        void openMore(BaseSlidingFragmentActivity activity, Video video);

        void openDetail(BaseSlidingFragmentActivity activity, Channel channel);
    }

    interface SubscribeChannelProvider {

        Fragment provideFragment();

        void notifyOpenChannel(Channel channel);
    }
}

package com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.holder.ChannelHeaderHolder;
import com.metfone.selfcare.ui.tabvideo.holder.VideoNormalHolder;
import com.metfone.selfcare.ui.tabvideo.listener.OnTabListener;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.SubscribeChannelActivity;
import com.metfone.selfcare.ui.view.load_more.OnEndlessScrollListener;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SubscribeChannelFragment extends BaseFragment implements SubscribeChannelContact.SubscribeChannelView
        , SubscribeChannelContact.SubscribeChannelProvider, OnTabListener, OnClickMoreItemListener {

    @BindView(R.id.iv_back)
    AppCompatImageView ivBack;
    @BindView(R.id.channel_subscribe_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.channel_subscribe_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tvEmptyTitle)
    TextView tvEmptyTitle;
    @BindView(R.id.tvEmptyDesc)
    TextView tvEmptyDes;
    @BindView(R.id.imgType)
    AppCompatImageView imgType;
    @BindView(R.id.empty_retry_text2)
    TextView tvEmptyNetwork;
    @BindView(R.id.empty_retry_button)
    AppCompatImageView btnEmptyRetryData;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.layout_action_bar)
    View viewActionBar;

    @Inject
    SubscribeChannelContact.SubscribeChannelPresenter presenter;

    private Unbinder unbinder;
    private int currentPosition;
    private ArrayList<BaseAdapter.ItemObject> itemObjects;
    private SubscribeChannelAdapter subscribeChannelAdapter;
    private LinearLayoutManager linearLayoutManager;

    private BaseAdapter.ItemObject loadMoreObject;
    private BaseAdapter.ItemObject channelObject = null;

    private boolean isLoadMore = false;
    private boolean isLoading = false;
    private boolean isShowLoading = false;
    private Runnable loadingRunnable = new Runnable() {
        @Override
        public void run() {
            if (isLoading) return;
            isLoading = true;
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setEnabled(false);
            if (isShowLoading && swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(true);
            if (presenter != null)
                presenter.getVideoNewPublish();
            if (presenter != null)
                presenter.getChannelUserFollow();
        }
    };
    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setEnabled(false);
            if (presenter != null)
                presenter.getRefreshVideoNewPublish();
            if (presenter != null)
                presenter.getChannelUserFollow();
        }
    };
    private OnEndlessScrollListener mEndlessRecyclerOnScrollListener = new OnEndlessScrollListener(3) {
        private int currentState;

        @Override
        public void onLoadNextPage(View view) {
            super.onLoadNextPage(view);
            if (isLoading) return;
            if (isLoadMore) {
                isLoading = true;
                if (presenter != null)
                    presenter.getMoreVideoNewPublish();
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (subscribeChannelAdapter == null || linearLayoutManager == null)
                return;
            if (currentState == RecyclerView.SCROLL_STATE_IDLE && newState == RecyclerView.SCROLL_STATE_DRAGGING) {// dừng tất cả các tiền trình đang load ảnh
                subscribeChannelAdapter.pauseRequests();
            } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {// khôi phuc các tiền trình load ảnh
                subscribeChannelAdapter.resumeRequests();
                int first = linearLayoutManager.findFirstVisibleItemPosition();
                int last = linearLayoutManager.findLastVisibleItemPosition();
                last = Math.min(last, subscribeChannelAdapter.getItemCount() - 1);
                for (int i = first; i <= last; i++) {
                    updateUiImage(i);
                }

            }
            currentState = newState;
        }

        /**
         * Cập nhật lại giao diện image của item
         * @param position vị trí cần cập nhật
         */
        private void updateUiImage(int position) {
            if (recyclerView != null) {
                RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(position);
                if (viewHolder instanceof VideoNormalHolder) {
                    ((VideoNormalHolder) viewHolder).bindImageVideo();
                    ((VideoNormalHolder) viewHolder).bindImageChannel();
                }
            }
        }
    };
    private SubscribeChannelAdapter.OnItemSubscribeChannelListener onItemSubscribeChannelListener = new SubscribeChannelAdapter.OnItemSubscribeChannelListener() {

        @Override
        public void onClick(Video video) {
            presenter.openDetail(activity, video);
        }

        @Override
        public void onHear(Video video) {
            presenter.notifyLike(activity, video);
        }

        @Override
        public void onShare(Video video) {
            presenter.openShare(activity, video);
        }

        @Override
        public void onComment(Video video) {
            presenter.openComment(activity, video);
        }

        @Override
        public void onMore(Video video) {
            presenter.openMore(activity, video);
        }

        @Override
        public void onClick(Channel channel) {
            presenter.openDetail(activity, channel);
        }

        @Override
        public void onClickSeeAll() {
            if (activity instanceof SubscribeChannelActivity) {
                ((SubscribeChannelActivity) activity).addChannelManagement();
            } else {
                SubscribeChannelActivity.start(activity, Constants.TAB_CHANNEL_MANAGEMENT);
            }
        }
    };
    private ListenerUtils listenerUtils;

    public static Bundle arguments(int position, int currentPosition, String categoryId) {
        return new Bundler()
                .putInt(Constants.TabVideo.POSITION, position)
                .putInt(Constants.TabVideo.CURRENT_POSITION, currentPosition)
                .putString(Constants.TabVideo.CATEGORY_ID, categoryId)
                .putBoolean("is_tab_video", true)
                .get();
    }

    private boolean isTabVideo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
        channelObject = ChannelHeaderHolder.ChannelObject.provideItemObject(new ArrayList<>());
        loadMoreObject = new BaseAdapter.ItemObject();
        loadMoreObject.setId(String.valueOf(System.currentTimeMillis()));
        loadMoreObject.setInfo(new BaseAdapter.DefaultClone());
        loadMoreObject.setType(BaseAdapter.Type.LOAD_MORE);
        Bundle bundle = getArguments();
        if (bundle != null) {
            currentPosition = bundle.getInt(Constants.TabVideo.POSITION);
            isTabVideo = bundle.getBoolean("is_tab_video");
        }
        listenerUtils = application.getApplicationComponent().providerListenerUtils();
        listenerUtils.addListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscribe_channel, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (swipeRefreshLayout != null && loadingRunnable != null)
            swipeRefreshLayout.removeCallbacks(loadingRunnable);
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        if (presenter != null)
            presenter.dispose();
        if (unbinder != null) unbinder.unbind();
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
    }

    @Override
    public Fragment provideFragment() {
        isVisibleToUser = true;
        return this;
    }

    @Override
    public void notifyOpenChannel(Channel channel) {
        if (channelObject != null) {
            BaseAdapter.Clone clone = channelObject.getInfo();
            if (clone instanceof ChannelHeaderHolder.ChannelObject) {
                ArrayList<Channel> channels = ((ChannelHeaderHolder.ChannelObject) clone).getChannels();
                if (Utilities.notEmpty(channels)) {
                    for (Channel itemChannel : channels) {
                        if (itemChannel.getId().equals(channel.getId())) {
                            itemChannel.setHaveNewVideo(channel.isHaveNewVideo());
                        }
                    }
                }
            }
            if (subscribeChannelAdapter != null)
                subscribeChannelAdapter.updateData(itemObjects);
        }
    }

    private void initView() {
        if (activity instanceof SubscribeChannelActivity) {
            viewActionBar.setVisibility(View.VISIBLE);
//            line.setVisibility(View.VISIBLE);
        } else {
            viewActionBar.setVisibility(View.GONE);
//            line.setVisibility(View.GONE);
        }
        itemObjects = new ArrayList<>();
        subscribeChannelAdapter = new SubscribeChannelAdapter(activity);
        subscribeChannelAdapter.setTabVideo(isTabVideo);
        subscribeChannelAdapter.setOnItemListener(onItemSubscribeChannelListener);

        linearLayoutManager = new LinearLayoutManager(application);
        linearLayoutManager.setInitialPrefetchItemCount(5);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(subscribeChannelAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(null);
        recyclerView.setLayoutAnimation(null);
        recyclerView.setItemViewCacheSize(5);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnScrollListener(mEndlessRecyclerOnScrollListener);
        tvEmptyTitle.setText(getString(R.string.no_followed_channel));
        tvEmptyDes.setText(getString(R.string.no_followed_channel_des));
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        if (canLazyLoad()) {
            isShowLoading = true;
            swipeRefreshLayout.post(loadingRunnable);
        }
    }

    @Override
    public void bindData(ArrayList<Channel> results) {
        itemObjects.remove(channelObject);
        channelObject = ChannelHeaderHolder.ChannelObject.provideItemObject(results);
        itemObjects.add(0, channelObject);
        subscribeChannelAdapter.updateData(itemObjects);

        if (linearLayoutManager != null && recyclerView != null) {
            try {
                linearLayoutManager.smoothScrollToPosition(recyclerView, null, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void bindData(ArrayList<Video> videos, boolean loadMore) {
        isLoadMore = loadMore;
        if (itemObjects == null)
            itemObjects = new ArrayList<>();
        else
            itemObjects.clear();

        if (channelObject != null)
            itemObjects.add(channelObject);

        if (Utilities.notEmpty(videos)) {
            for (int i = 0; i < videos.size(); i++) {
                Video video = videos.get(i);
                itemObjects.add(VideoNormalHolder.VideoObject.provideItemObject(video, i, activity));
            }
        } else {
            BaseAdapter.ItemObject emptyObj = new BaseAdapter.ItemObject();
            emptyObj.setType(BaseAdapter.Type.EMPTY);
            itemObjects.add(emptyObj);
        }

        itemObjects.remove(loadMoreObject);
        if (loadMore)
            itemObjects.add(loadMoreObject);

        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setEnabled(true);
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
        if (subscribeChannelAdapter != null)
            subscribeChannelAdapter.updateData(itemObjects);
        if (emptyLayout != null)
            emptyLayout.setVisibility(View.GONE);

        if (isLoadMore && itemObjects != null && itemObjects.size() < 20 && mEndlessRecyclerOnScrollListener != null && recyclerView != null)
            mEndlessRecyclerOnScrollListener.onLoadNextPage(recyclerView);
    }

    @Override
    public void showNotifyDataEmpty() {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setEnabled(true);
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
        if (emptyLayout != null)
            emptyLayout.setVisibility(View.VISIBLE);
        if (tvEmptyTitle != null) {
            tvEmptyTitle.setVisibility(View.VISIBLE);
        }
        if (tvEmptyDes != null) {
            tvEmptyDes.setVisibility(View.VISIBLE);
        }
        if (imgType != null) {
            imgType.setVisibility(View.VISIBLE);
            imgType.setImageResource(R.drawable.ic_empty_channel);
        }
    }

    @Override
    public void showNotifyNotNetwork() {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setEnabled(true);
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
        if (emptyLayout != null)
            emptyLayout.setVisibility(View.VISIBLE);
        if (tvEmptyNetwork != null)
            tvEmptyNetwork.setVisibility(View.VISIBLE);
        if (btnEmptyRetryData != null)
            btnEmptyRetryData.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDialogMoreOption(Video video) {
        DialogUtils.showOptionVideoItem(activity, video, this);
    }

    @Override
    public void loadVideoNewPublishCompleted() {
        isLoading = false;
        isShowLoading = false;
    }

    @Override
    public void loadChannelUserFollowCompleted() {
        isLoading = false;
        isShowLoading = false;
    }

    @OnClick({R.id.iv_back, R.id.empty_retry_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                activity.onBackPressed();
                break;
            case R.id.empty_retry_button:
                if (swipeRefreshLayout != null && loadingRunnable != null) {
                    isShowLoading = true;
                    swipeRefreshLayout.post(loadingRunnable);
                }
                break;
        }
    }

    @Override
    public void onTabReselected(int currentPosition) {

    }

    @Override
    public void onTabSelected(int position) {
        if (currentPosition == position && swipeRefreshLayout != null && loadingRunnable != null) {
            isShowLoading = false;
            swipeRefreshLayout.post(loadingRunnable);
        }
    }

    @Override
    public void onDisableLoading() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad() && swipeRefreshLayout != null) {
            isShowLoading = true;
            swipeRefreshLayout.post(loadingRunnable);
        }
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            if (menuId != Constants.MENU.MENU_EXIT && ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                activity.showDialogLogin();
                return;
            }
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_SAVE_VIDEO:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).saveVideoFromMenu((Video) object);
                        activity.showToast(R.string.videoSavedToLibrary);
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).watchLaterVideoFromMenu((Video) object);
                        activity.showToast(R.string.add_later_success);
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (object instanceof Video) {
                        FeedModelOnMedia feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia((Video) object);
                        new WSOnMedia(application).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {
                                        Log.e(TAG, s);
                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                    break;
            }
        }
    }
}

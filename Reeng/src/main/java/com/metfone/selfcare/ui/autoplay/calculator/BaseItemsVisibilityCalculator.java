package com.metfone.selfcare.ui.autoplay.calculator;

import android.widget.AbsListView;

import com.metfone.selfcare.ui.autoplay.utils.ItemsPositionGetter;
import com.metfone.selfcare.ui.autoplay.utils.ScrollDirectionDetector;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 1/18/2018.
 */

public abstract class BaseItemsVisibilityCalculator implements ListItemsVisibilityCalculator,
        ScrollDirectionDetector.OnDetectScrollListener {
    private static final String TAG = BaseItemsVisibilityCalculator.class.getSimpleName();
    private final ScrollDirectionDetector mScrollDirectionDetector = new ScrollDirectionDetector(this);

    protected abstract void onStateFling(ItemsPositionGetter itemsPositionGetter);

    protected abstract void onStateTouchScroll(ItemsPositionGetter itemsPositionGetter);

    @Override
    public void onScroll(ItemsPositionGetter itemsPositionGetter, int firstVisibleItem, int visibleItemCount, int scrollState/*TODO: add current item here. start tracking from it*/) {
        Log.i(TAG, "onScroll, firstVisibleItem " + firstVisibleItem + ", visibleItemCount " + visibleItemCount + ", scrollState " + scrollStateStr(scrollState));
        mScrollDirectionDetector.onDetectedListScroll(itemsPositionGetter, firstVisibleItem);
        switch (scrollState) {
            case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                onStateTouchScroll(itemsPositionGetter);
                break;
            case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                onStateTouchScroll(itemsPositionGetter);
                //onStateFling(itemsPositionGetter);
                break;
            case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                Log.i(TAG, "onScroll, SCROLL_STATE_IDLE. ignoring");
                break;
        }
    }


    private String scrollStateStr(int scrollState) {
        switch (scrollState) {
            case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                return "SCROLL_STATE_FLING";
            case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                return "SCROLL_STATE_IDLE";
            case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                return "SCROLL_STATE_TOUCH_SCROLL";
            default:
                throw new RuntimeException("wrong data, scrollState " + scrollState);
        }
    }
}
package com.metfone.selfcare.ui.tabvideo.playVideo;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.callback.OnVideoCallback;
import com.metfone.selfcare.di.BasePresenter;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class VideoPlayerPresenterImpl extends BasePresenter<VideoPlayerContact.VideoPlayerView> implements VideoPlayerContact.VideoPlayerPresenter {

    private Video currentVideo;

    public VideoPlayerPresenterImpl(VideoPlayerContact.VideoPlayerView view, ApplicationController application) {
        super(view, application);
    }

    @Override
    public void getVideoDetail(Video video) {
        currentVideo = video;
        videoApi.getInfoVideoFromUrl(compositeDisposable, video, onVideoCallback);
    }

    private OnVideoCallback onVideoCallback = new OnVideoCallback() {
        @Override
        public void onGetVideosSuccess(ArrayList<Video> list) {
            if (Utilities.isEmpty(list)) {
                onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
            } else {
                Video tempVideo = currentVideo;
                currentVideo = list.get(0);
//                if (currentVideo != null && TextUtils.isEmpty(currentVideo.getOriginalPath()) &&
//                        tempVideo != null && !TextUtils.isEmpty(tempVideo.getOriginalPath()))
//                    currentVideo = tempVideo;
                if (currentVideo != null) {
                    if (currentVideo.isLive()) {
                        if (view != null)
                            view.openVideoDetail(currentVideo);
                        else
                            onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                    } else {
                        if (Utilities.isEmpty(currentVideo.getOriginalPath()) && tempVideo != null
                                && Utilities.notEmpty(tempVideo.getOriginalPath())) {
                            currentVideo = tempVideo;
                        }
                        if (view != null && Utilities.notEmpty(currentVideo.getOriginalPath()))
                            view.openVideoDetail(currentVideo);
                        else
                            onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                    }
                } else {
                    onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                }
            }
        }

        @Override
        public void onGetVideosError(String s) {
            if (view != null && currentVideo != null && !TextUtils.isEmpty(currentVideo.getOriginalPath()))
                view.openVideoDetail(currentVideo);
            else if (view != null) {
                view.openDialogConfirmBack();
            }
        }

        @Override
        public void onGetVideosComplete() {

        }
    };

}

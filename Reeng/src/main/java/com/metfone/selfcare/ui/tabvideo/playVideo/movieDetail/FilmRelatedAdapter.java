package com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.model.tab_video.FilmGroup;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class FilmRelatedAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> implements OnItemFilmRelatedListener {

    public static final int SPACE = 0;
    public static final int NORMAL = 1;

    private @Nullable
    OnItemFilmRelatedListener onItemFilmRelatedListener;

    public FilmRelatedAdapter(Activity act) {
        super(act);
    }

    public void setOnItemFilmRelatedListener(@Nullable OnItemFilmRelatedListener onItemFilmRelatedListener) {
        this.onItemFilmRelatedListener = onItemFilmRelatedListener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = items.get(position);
        if (item instanceof Video) {
            return NORMAL;
        } else {
            return SPACE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case SPACE:
                return new SpaceHolder(activity, layoutInflater, parent);
            default:
                return new FilmRelatedHolder(activity, layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(items, position);
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (holder instanceof FilmRelatedHolder) {
            ((FilmRelatedHolder) holder).setOnItemFilmRelatedListener(this);
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder instanceof FilmRelatedHolder) {
            ((FilmRelatedHolder) holder).setOnItemFilmRelatedListener(null);
        }
    }

    @Override
    public void onItemFilmRelatedClick(Video video) {
        if (onItemFilmRelatedListener != null) {
            onItemFilmRelatedListener.onItemFilmRelatedClick(video);
        }
    }

    static class SpaceHolder extends ViewHolder {

        @BindView(R.id.root_space)
        View rootSpace;

        SpaceHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.item_channel_space, parent, false));

            ViewGroup.LayoutParams layoutParams = rootSpace.getLayoutParams();
            layoutParams.height = 0;
            layoutParams.width = 0;

            rootSpace.setLayoutParams(layoutParams);
        }
    }

    static class FilmRelatedHolder extends ViewHolder {

        @BindView(R.id.iv_thumb)
        ImageView ivThumb;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.root_item_film_related)
        RelativeLayout rootItemFilmRelated;

        private @Nullable
        Video mVideo;
        private @Nullable
        FilmGroup mFilmGroup;
        private @Nullable
        OnItemFilmRelatedListener onItemFilmRelatedListener;
        private int id;

        FilmRelatedHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.item_film_related, parent, false));

            int width = (ScreenManager.getWidth(activity) - Utilities.dpToPx(16) * 4) / 3 + 4;
            width = Math.min(Utilities.dpToPx(300), width);
            int height = (int) (width * 1.5);

            ViewGroup.LayoutParams layoutParams = ivThumb.getLayoutParams();
            layoutParams.height = height;
            layoutParams.width = width;
            ivThumb.setLayoutParams(layoutParams);
        }

        void setOnItemFilmRelatedListener(@Nullable OnItemFilmRelatedListener onItemFilmRelatedListener) {
            this.onItemFilmRelatedListener = onItemFilmRelatedListener;
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            super.bindData(items, position);
            Object item = items.get(position);
            if (item instanceof Video) {
                mVideo = (Video) item;
                mFilmGroup = mVideo.getFilmGroup();
                try {
                    id = Integer.valueOf(mFilmGroup.getGroupId());
                } catch (Exception e) {
                    id = 0;
                }
                if (mFilmGroup != null && id != 0) {
                    tvTitle.setText(mFilmGroup.getGroupName());
                    ImageManager.showImageRoundV2(mFilmGroup.getGroupImage(), ivThumb);
                }
            }
        }

        @OnClick(R.id.root_item_film_related)
        public void onViewClicked() {
            if (onItemFilmRelatedListener != null && id != 0) {
                onItemFilmRelatedListener.onItemFilmRelatedClick(mVideo);
            }
        }
    }
}

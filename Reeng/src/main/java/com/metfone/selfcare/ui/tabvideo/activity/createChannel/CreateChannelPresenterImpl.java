package com.metfone.selfcare.ui.tabvideo.activity.createChannel;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.callback.OnChannelInfoCallback;
import com.metfone.selfcare.common.api.video.channel.ChannelApi;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.di.BaseView;
import com.metfone.selfcare.model.tab_video.Channel;

public class CreateChannelPresenterImpl implements CreateChannelPresenter, OnChannelInfoCallback {

    private CreateChannelView createChannelView;
    private ChannelApi channelApi;
    private Channel channel;
    private ListenerUtils listenerUtils;
    private Utils utils;

    private boolean update = false;

    public CreateChannelPresenterImpl(ApplicationController application, BaseView baseView) {
        this.createChannelView = (CreateChannelView) baseView;

        this.channelApi = application.getApplicationComponent().providerChannelApi();
        this.listenerUtils = application.getApplicationComponent().providerListenerUtils();
        this.utils = application.getApplicationComponent().providesUtils();
    }

    @Override
    public void initData(Channel channel) {
        this.channel = channel;
        if (channel != null && !TextUtils.isEmpty(channel.getId())) {
            update = true;
            createChannelView.changeTitle();
            createChannelView.updateButtonSubmit();
            createChannelView.updateImage(channel.getUrlImage());
            createChannelView.updateCoverImage(channel.getUrlImageCover());
            createChannelView.updateTitleChannel(channel.getName());
            createChannelView.updateDescriptionChannel(channel.getDescription());
        }
    }

    @Override
    public void updateOrCreateChannel(String name, String des, String path, boolean showAvatar) {
        name = name.trim();
        des = des.trim();

        if (!update && !showAvatar) {
            createChannelView.showMessage(R.string.avatarChannelNotEmpty);
            return;
        }
        if (name.isEmpty()) {
            createChannelView.showMessage(R.string.nameChannelNotEmpty);
            return;
        }
        if (des.isEmpty()) {
            createChannelView.showMessage(R.string.desChannelNotEmpty);
            return;
        }

        channel.setName(name);
        channel.setDescription(des);
        if (showAvatar) channel.setUrlImage(path);

        createChannelView.showLoading();

        channelApi.updateOrCreateChannel(showAvatar, channel, this);

    }

    @Override
    public void onGetChannelInfoSuccess(Channel channel) {
        utils.saveChannelInfo(channel);
        if (update) {
            listenerUtils.notifyUpdateChannel(channel);
            createChannelView.back();
        } else {
            listenerUtils.notifyCreateChannel(channel);
            createChannelView.openChannel(channel);
        }
    }

    @Override
    public void onGetChannelInfoError(String s) {

    }

    @Override
    public void onGetChannelInfoComplete() {
        createChannelView.hideLoading();
    }
}

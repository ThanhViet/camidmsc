package com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary;

import com.metfone.selfcare.model.tab_video.Video;

import java.util.ArrayList;

public interface VideoLibraryView {
    void showVideo(ArrayList<Video> videos);

}

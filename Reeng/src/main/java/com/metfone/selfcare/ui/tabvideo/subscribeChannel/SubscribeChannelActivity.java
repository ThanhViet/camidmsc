package com.metfone.selfcare.ui.tabvideo.subscribeChannel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement.ChannelManagementContact;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail.SubscribeChannelContact;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload.TopMoneyUploadContact;

import javax.inject.Inject;

/**
 * danh sách kênh đang ký
 */
public class SubscribeChannelActivity extends BaseActivity {

    @Inject
    SubscribeChannelContact.SubscribeChannelProvider subscribeChannelProvider;
    @Inject
    ChannelManagementContact.ChannelManagementProvider channelManagementProvider;
    @Inject
    TopMoneyUploadContact.TopMoneyUploadProvider topMoneyUploadProvider;

    public static void start(Context context, int tabId) {
        if (context != null) {
            Intent intent = new Intent(context, SubscribeChannelActivity.class);
            intent.putExtra(Constants.KEY_TYPE, tabId);
            context.startActivity(intent);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
        setContentView(R.layout.activity_subcribe_channel);
        int type = getIntent().getIntExtra(Constants.KEY_TYPE, 0);
        if (type == Constants.TAB_SUBSCRIBE_CHANNEL) {
            if (subscribeChannelProvider != null)
                getSupportFragmentManager().beginTransaction().add(R.id.frame_content, subscribeChannelProvider.provideFragment()).commitAllowingStateLoss();
        } else if (type == Constants.TAB_CHANNEL_MANAGEMENT) {
            if (channelManagementProvider != null)
                getSupportFragmentManager().beginTransaction().add(R.id.frame_content, channelManagementProvider.provideFragment()).commitAllowingStateLoss();
        } else if (type == Constants.TAB_TOP_MONEY_UPLOAD) {
            if (topMoneyUploadProvider != null)
                getSupportFragmentManager().beginTransaction().add(R.id.frame_content, topMoneyUploadProvider.provideFragment()).commitAllowingStateLoss();
        }
    }

    public void addChannelManagement() {
        getSupportFragmentManager().beginTransaction().add(R.id.frame_content, channelManagementProvider.provideFragment()).addToBackStack("").commitAllowingStateLoss();
    }

    public void notifyOpenChannel(Channel channel) {
        if (subscribeChannelProvider != null)
            subscribeChannelProvider.notifyOpenChannel(channel);
    }
}

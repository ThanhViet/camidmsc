package com.metfone.selfcare.ui.dialog;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.message.SearchMessageResultsFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

public class SearchMessageDialog extends DialogEditText {
    public static final String TAG = SearchMessageDialog.class.getSimpleName();
    private DialogResultListener mInterface = null;
    boolean cancelToFinish = false;

    public SearchMessageDialog(BaseSlidingFragmentActivity activity, boolean cancelToFinish, DialogResultListener listener) {
        super(activity, true);
        this.cancelToFinish = cancelToFinish;
        this.mInterface = listener;
        init(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDialogAttributes();
    }

    public void setDialogAttributes() {
        if (mEdtInput == null) return;
        mEdtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public synchronized void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mEdtInput == null || mBtnPositive == null) return;
                String trimmedStr = mEdtInput.getText().toString().trim();
                if (trimmedStr.isEmpty()) {
                    mBtnPositive.setEnabled(false);
                    mBtnPositive.setAlpha(0.3f);
                    mBtnPositive.setFocusable(false);
                } else {
                    mBtnPositive.setEnabled(true);
                    mBtnPositive.setAlpha(1.0f);
                    mBtnPositive.setFocusable(true);
                }

            }
        });
    }

    @Override
    public void cancel() {
        try {
            if (cancelToFinish && mInterface != null) {
                if (mInterface instanceof SearchMessageResultsFragment) {
                    ((SearchMessageResultsFragment) mInterface).finishActivity();
                }
            }
            mInterface = null;
            super.cancel();
        } catch (Exception e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBtnPositive == null || mEdtInput == null || mEdtInput.getText() == null) return;

        String trimmedStr = mEdtInput.getText().toString().trim();
        if (trimmedStr.isEmpty()) {
            mBtnPositive.setEnabled(false);
            mBtnPositive.setFocusable(false);
            mBtnPositive.setAlpha(0.3f);
        } else {
            mBtnPositive.setEnabled(true);
            mBtnPositive.setFocusable(true);
            mBtnPositive.setAlpha(1.0f);
        }
    }

    public interface DialogResultListener {
        void onDialogResult(String tag);
    }

    private void init(final BaseSlidingFragmentActivity activity) {
        if (activity == null) return;
        setCheckEnable(true)
                .setMultiLine(false)
                .setSelectAll(true)
                .setLabel(activity.getString(R.string.thread_search_message))
                .setMessage(null)
                .setTextHint(activity.getString(R.string.search_input_here))
                .setMaxLength(Constants.MESSAGE.GROUP_NAME_MAX_LENGTH)
                .setNegativeLabel(activity.getString(R.string.cancel))
                .setPositiveLabel(activity.getString(R.string.search))
                .setPositiveListener(new PositiveListener<String>() {
                    @Override
                    public void onPositive(String result) {
                        if (mInterface != null && mEdtInput != null && mEdtInput.getText() != null) {
                            mInterface.onDialogResult(mEdtInput.getText().toString().trim());
                        }
                        mInterface = null;
                    }
                }).setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                cancel();
            }
        }).setDismissListener(new DismissListener() {
            @Override
            public void onDismiss() {
                activity.hideKeyboard();
            }
        }).setCancelable(true);
    }
}

package com.metfone.selfcare.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ContactDetailActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by huongnd on 11/28/2018.
 */

public class ReactionListBottomDialog extends BottomSheetDialogFragment {
    public static final String TAG = ReactionListBottomDialog.class.getSimpleName();
    private LinearLayout tabLayout;
    private ViewPager viewPager;
    private ArrayList<Integer> reactions;
    ViewPagerAdapter adapter;
    TabIndicator mTabIndicator;
    ReengMessage reengMessage;

    private ReactionPopup.OnReactImageClickListener mListener;
    private ScaleAnimation mScaleZoomAnimation = new ScaleAnimation(0.2f, 1f, 0.2f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
    //ScaleAnimation scaleShrinkAnimation = new ScaleAnimation(2f, 1f, 2f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 1f);
    private ImageView mLike, mLove, mHuh, mSurprise, mSad, mSmile;
    private ImageView mLikePoint, mLovePoint, mHuhPoint, mSurprisePoint, mSadPoint, mSmilePoint;
    private BaseSlidingFragmentActivity activity;

    private static final String KEY_MESSAGE = "KEY_MESSAGE";

    public static ReactionListBottomDialog newInstance(ReengMessage reengMessage) {
        ReactionListBottomDialog instance = new ReactionListBottomDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_MESSAGE, reengMessage);
        instance.reengMessage = reengMessage;
        instance.reactions = reengMessage.getListReaction();
        instance.setArguments(bundle);
        return instance;
    }

    public void setReactImageClickListener(ReactionPopup.OnReactImageClickListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NO_TITLE, R.style.BottomSheetTransparentDialogTheme);

        reengMessage = (ReengMessage) (getArguments() != null ? getArguments().getSerializable(KEY_MESSAGE) : null);
        if (reengMessage != null) {
            reactions = reengMessage.getListReaction();
        } else {
            reactions = new ArrayList<>();
        }
        activity = (BaseSlidingFragmentActivity) getActivity();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final BottomSheetContextMenu.CustomBottomSheetDialog dialog = new BottomSheetContextMenu.CustomBottomSheetDialog(getContext(), R.style.BottomSheetTransparentDialogTheme);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                try {
                    FrameLayout bottomSheet = (FrameLayout) dialog.findViewById(R.id.design_bottom_sheet);
                    BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_COLLAPSED);
//                    dialog.setLockDragging(true);
                } catch (Exception e) {
                    Log.d(TAG, "show: Can not expand", e);
                }
            }
        });

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_reaction_list, null, false);
        tabLayout = (LinearLayout) contentView.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) contentView.findViewById(R.id.viewpager);
        mTabIndicator = (TabIndicator) contentView.findViewById(R.id.tab_indicator_line);
        mTabIndicator.setTabWidget(tabLayout, 0);
        viewPager.addOnPageChangeListener(mPageChangeListener);
        createViewPager(viewPager);
        createTabIcons();

        mLike = (ImageView) contentView.findViewById(R.id.like_item);
        mLove = (ImageView) contentView.findViewById(R.id.love_item);
        mHuh = (ImageView) contentView.findViewById(R.id.huh_item);
        mSurprise = (ImageView) contentView.findViewById(R.id.surprise_item);
        mSad = (ImageView) contentView.findViewById(R.id.sad_item);
        mSmile = (ImageView) contentView.findViewById(R.id.smile_item);

        mLikePoint = (ImageView) contentView.findViewById(R.id.like_item_point);
        mLovePoint = (ImageView) contentView.findViewById(R.id.love_item_point);
        mHuhPoint = (ImageView) contentView.findViewById(R.id.huh_item_point);
        mSurprisePoint = (ImageView) contentView.findViewById(R.id.surprise_item_point);
        mSadPoint = (ImageView) contentView.findViewById(R.id.sad_item_point);
        mSmilePoint = (ImageView) contentView.findViewById(R.id.smile_item_point);

        mLike.setOnClickListener(onClickListener);
        mLove.setOnClickListener(onClickListener);
        //mDislike.setOnClickListener(onClickListener);
        mHuh.setOnClickListener(onClickListener);
        mSurprise.setOnClickListener(onClickListener);
        mSad.setOnClickListener(onClickListener);
        mSmile.setOnClickListener(onClickListener);


        mScaleZoomAnimation.setDuration(400);
        mLove.startAnimation(mScaleZoomAnimation);
        mSmile.startAnimation(mScaleZoomAnimation);
        mSurprise.startAnimation(mScaleZoomAnimation);
        mHuh.startAnimation(mScaleZoomAnimation);
        mSad.startAnimation(mScaleZoomAnimation);
        mLike.startAnimation(mScaleZoomAnimation);

        setReactionPoint(reengMessage.getStateMyReaction());
        return contentView;
    }


    public void setReactionPoint(int point) {
        switch (point) {
            case 0:
                if (mLikePoint != null) {
                    mLikePoint.setVisibility(View.VISIBLE);
                }
                break;
            case 1:
                if (mLovePoint != null) {
                    mLovePoint.setVisibility(View.VISIBLE);
                }
                break;
            case 2:
                if (mSmilePoint != null) {
                    mSmilePoint.setVisibility(View.VISIBLE);
                }
                break;
            case 3:
                if (mSurprisePoint != null) {
                    mSurprisePoint.setVisibility(View.VISIBLE);
                }
                break;
            case 4:
                if (mSadPoint != null) {
                    mSadPoint.setVisibility(View.VISIBLE);
                }
                break;
            case 5:
                if (mHuhPoint != null) {
                    mHuhPoint.setVisibility(View.VISIBLE);
                }
                break;
            default:
                break;
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == null || mListener == null) return;
            switch (v.getId()) {
                case R.id.like_item:
                    MessageHelper.showAnimReation(activity, v, R.drawable.ic_reaction_like);
                    mListener.onReactImageClick(reengMessage, ReengMessageConstant.Reaction.LIKE);
                    playReactionSoundEffect(mLikePoint);
                    break;

                case R.id.love_item:
                    MessageHelper.showAnimReation(activity, v, R.drawable.ic_reaction_love);
                    mListener.onReactImageClick(reengMessage, ReengMessageConstant.Reaction.LOVE);
                    playReactionSoundEffect(mLovePoint);
                    break;

                case R.id.huh_item:
                    MessageHelper.showAnimReation(activity, v, R.drawable.ic_reaction_huh);
                    mListener.onReactImageClick(reengMessage, ReengMessageConstant.Reaction.HUH);
                    playReactionSoundEffect(mHuhPoint);
                    break;

                case R.id.surprise_item:
                    MessageHelper.showAnimReation(activity, v, R.drawable.ic_reaction_surprise);
                    mListener.onReactImageClick(reengMessage, ReengMessageConstant.Reaction.SURPRISE);
                    playReactionSoundEffect(mSurprisePoint);
                    break;

                case R.id.sad_item:
                    MessageHelper.showAnimReation(activity, v, R.drawable.ic_reaction_sad);
                    mListener.onReactImageClick(reengMessage, ReengMessageConstant.Reaction.SAD);
                    playReactionSoundEffect(mSadPoint);
                    break;

                case R.id.smile_item:
                    MessageHelper.showAnimReation(activity, v, R.drawable.ic_reaction_smile);
                    mListener.onReactImageClick(reengMessage, ReengMessageConstant.Reaction.SMILE);
                    playReactionSoundEffect(mSmilePoint);
                    break;

                default:
                    break;
            }

            dismiss();
        }
    };

    private void playReactionSoundEffect(View view) {
        try {
            boolean isCancelReaction = view != null && view.getVisibility() == View.VISIBLE;
            MediaPlayer mp = MediaPlayer.create(ApplicationController.self(), isCancelReaction ? R.raw.reactions_cancel : R.raw.reactions_click);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.reset();
                    mp.release();
                    mp = null;
                }

            });
            mp.setVolume(0.75f, 0.75f);
            mp.start();
        } catch (Exception e) {
        }
    }

    ViewPager.SimpleOnPageChangeListener mPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            int index = position;
            if (mTabIndicator != null) {
                mTabIndicator.updateBottomIndicator(index, positionOffset);
            }
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (reactions != null && !reactions.isEmpty()) {
            outState.putIntegerArrayList("reactions", reactions);
        }
    }

    private void createTabIcons() {
        if (adapter == null || adapter.getCount() == 0) return;
        int pageCount = adapter.getCount();
        for (int i = 0; i < pageCount; i++) {
            final int pos = i;
            ChildReactionFragment fragment = (ChildReactionFragment) adapter.getItem(i);
            View tabView = LayoutInflater.from(getActivity()).inflate(R.layout.custom_reaction_tab, null, false);
            ImageView imageView = (ImageView) tabView.findViewById(R.id.image_tab);
            TextView textView = (TextView) tabView.findViewById(R.id.tv_tab);
            imageView.setImageResource(getImageId(fragment.getStatus()));
            textView.setText("" + fragment.getSize());
            if (tabLayout != null) {
                tabLayout.addView(tabView);
            }

            tabView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPager.setCurrentItem(pos);
                }
            });
        }
        if (mTabIndicator != null) {
            mTabIndicator.setCurrentPosition(0);
        }
    }

    private int getImageId(int index) {
        switch (index) {
            case 0:
                return R.drawable.ic_reaction_like;
            case 1:
                return R.drawable.ic_reaction_love;
            case 2:
                return R.drawable.ic_reaction_smile;
            case 3:
                return R.drawable.ic_reaction_surprise;
            case 4:
                return R.drawable.ic_reaction_sad;
            case 5:
                return R.drawable.ic_reaction_huh;
        }
        return R.drawable.ic_reaction_like;
    }

    private ArrayList<Pair<Integer, Integer>> getReactionNumber() {
        if (reactions == null || reactions.isEmpty()) return null;
        ArrayList<Pair<Integer, Integer>> reactionMessages = new ArrayList<>();
        for (int i = 0; i < reactions.size(); i++) {
            if (reactions.get(i) > 0) {
                reactionMessages.add(new Pair<Integer, Integer>(i, reactions.get(i)));
            }
        }
        return reactionMessages;
    }

    private void createViewPager(final ViewPager viewPager) {
        ArrayList<Pair<Integer, Integer>> reactionMesages = getReactionNumber();
        if (reactionMesages == null || reactionMesages.isEmpty()) return;

        Collections.sort(reactionMesages, new Comparator<Pair<Integer, Integer>>() {
            @Override
            public int compare(Pair<Integer, Integer> o1, Pair<Integer, Integer> o2) {
                if (o1 == null || o2 == null) return 0;
                if (o1.second == o2.second)
                    return (o1.first - o2.first);
                return (o2.second - o1.second);
            }
        });

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        for (int i = 0; i < reactionMesages.size(); i++) {
            adapter.addFrag(ChildReactionFragment.newInstance(reactionMesages.get(i).first, reactionMesages.get(i).second, reengMessage), "");
        }
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int index = position;
                if (mTabIndicator != null) {
                    mTabIndicator.updateBottomIndicator(index, positionOffset);
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (adapter == null || viewPager == null) return;
                ChildReactionFragment childReactionFragment = (ChildReactionFragment) adapter.getItem(position);
                childReactionFragment.setNestedScrollingEnabled(true);
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (i != position) {
                        ChildReactionFragment childFragment = (ChildReactionFragment) adapter.getItem(i);
                        childFragment.setNestedScrollingEnabled(false);
                    }
                }
                viewPager.requestLayout();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public static class ChildReactionFragment extends BaseFragment {
        private static final String REACTION_MESSAGE = "REACTION_MESSAGE";
        private static final String REACTION_SIZE = "REACTION_SIZE";
        private static final String REACTION_STATUS = "REACTION_STATUS";

        RecyclerView reactionLv;
        String myNumber = "";
        int status, size;
        ReengMessage reengMessage;

        public static ChildReactionFragment newInstance(int status, int size, ReengMessage reengMessage) {
            Bundle args = new Bundle();
            args.putSerializable(REACTION_MESSAGE, reengMessage);
            args.putInt(REACTION_SIZE, size);
            args.putInt(REACTION_STATUS, status);
            ChildReactionFragment fragment = new ChildReactionFragment();
            fragment.setArguments(args);
            fragment.status = status;
            fragment.size = size;
            fragment.reengMessage = reengMessage;
            return fragment;
        }

        private ArrayList<String> reactions = new ArrayList<>();

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (application != null) {
                myNumber = application.getReengAccountBusiness().getJidNumber();
            }
            reengMessage = (ReengMessage) (getArguments() != null ? getArguments().getSerializable(REACTION_MESSAGE) : null);
            size = (getArguments() != null ? getArguments().getInt(REACTION_SIZE) : 0);
            status = (getArguments() != null ? getArguments().getInt(REACTION_STATUS) : 0);
            reactions = ApplicationController.self().getMessageBusiness().getListReactionSenderFromMessageId(reengMessage.getId(), reengMessage.getPacketId(), status);
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_reaction_list, container, false);
            reactionLv = (RecyclerView) view.findViewById(R.id.lv_reactions);
            initView();
            return view;
        }

        public void setNestedScrollingEnabled(boolean enabled) {
            if (reactionLv != null) {
                reactionLv.setNestedScrollingEnabled(enabled);
            }
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
        }

        private void initView() {
            ArrayList<PhoneNumber> phoneNumbers = new ArrayList<>();
            if (Utilities.notEmpty(reactions)) {
                ApplicationController app = ApplicationController.self();
                for (String numb : reactions) {
                    PhoneNumber phoneNumber;
                    phoneNumber = app.getContactBusiness().getPhoneNumberFromNumber(numb);
                    if (phoneNumber == null) {
                        NonContact nonContact = app.getContactBusiness().getExistNonContact(numb);
                        if (nonContact != null) {
                            phoneNumber = app.getContactBusiness().createPhoneNumberFromNonContact(nonContact);
                            phoneNumber.setName(TextUtils.isEmpty(nonContact.getNickName()) ? numb : nonContact.getNickName());
                            phoneNumber.setNameUnicode(TextUtils.isEmpty(nonContact.getNickName()) ? numb : nonContact.getNickName());
                            phoneNumber.setContactId("-2");
                            phoneNumber.setId(null);
                        } else {
                            StrangerPhoneNumber strangerPhoneNumber = app.getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(numb);
                            phoneNumber = new PhoneNumber();
                            phoneNumber.setRawNumber(numb);
                            phoneNumber.setJidNumber(numb);
                            phoneNumber.setContactId("-1");
                            phoneNumber.setId(null);
                            if (strangerPhoneNumber != null) {
                                phoneNumber.setName(strangerPhoneNumber.getFriendName());
                                phoneNumber.setNameUnicode(strangerPhoneNumber.getFriendName());
                            } else {
                                phoneNumber.setName(numb);
                                phoneNumber.setNameUnicode(numb);

                            }
                        }
                    }
                    if (Utilities.equals(phoneNumber.getJidNumber(), myNumber)) {
                        phoneNumber.setName(getString(R.string.you));
                    }
                    phoneNumbers.add(phoneNumber);
                }

                /*phoneNumbers = app.getContactBusiness().getListPhoneNumberFormListNumber(reactions);
                for (PhoneNumber phoneNumber : phoneNumbers) {
                    if (Utilities.equals(phoneNumber.getJidNumber(), myNumber)) {
                        phoneNumber.setName(getString(R.string.you));
                    }
                }*/
            } else {
                phoneNumbers = new ArrayList<>();
            }
            //PhoneNumberAdapter voteAdapter = new PhoneNumberAdapter(ApplicationController.self(), phoneNumbers, null, CONTACT_VIEW_AVATAR_AND_NAME);
            ReactionAdapter reactionAdapter = new ReactionAdapter(activity, phoneNumbers);
            if (reactionLv != null) {
                reactionLv.setLayoutManager(new LinearLayoutManager(activity));
                reactionLv.setAdapter(reactionAdapter);
            }
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }
    }

    public static class ReactionAdapter extends RecyclerView.Adapter<ReactionHolder> {
        private ApplicationController mApplication;
        private ArrayList<PhoneNumber> mListObjects;
        private LayoutInflater mLayoutInflater;
        private String myNumber;
        private ReactionPopup.OnReactImageClickListener mListener;
        private BaseSlidingFragmentActivity activity;

        public ReactionAdapter(BaseSlidingFragmentActivity activity, ArrayList<PhoneNumber> listPhoneNumbers) {
            this.activity = activity;
            this.mApplication = (ApplicationController) activity.getApplication();
            this.mListObjects = new ArrayList<>();
            this.mListObjects.addAll(listPhoneNumbers);
            this.mLayoutInflater = (LayoutInflater) mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        }

        @NonNull
        @Override
        public ReactionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            ReactionHolder viewHolder;
            View view = mLayoutInflater.inflate(R.layout.item_reaction_holder, parent, false);
            viewHolder = new ReactionHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ReactionHolder holder, int position) {
            try {
                final PhoneNumber phoneNumber = mListObjects.get(position);
                if (!TextUtils.isEmpty(myNumber) && myNumber.equals(phoneNumber.getJidNumber())) {
                    if (holder.tvAvatar != null) {
                        holder.tvAvatar.setText("");
                    }
                    mApplication.getAvatarBusiness().setMyAvatar(holder.imgAvatar, mApplication.getReengAccountBusiness().getCurrentAccount());
                } else {
                    int size = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
                    mApplication.getAvatarBusiness().setPhoneNumberAvatar(holder.imgAvatar, holder.tvAvatar, phoneNumber, size);
                }
                holder.tvName.setText(phoneNumber.getName());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goToProfile(phoneNumber);
                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "onBindViewHolder", e);
            }

        }

        @Override
        public int getItemCount() {
            return mListObjects.size();
        }


        private void goToProfile(PhoneNumber phoneNumber) {
            if (phoneNumber.getJidNumber().equals(myNumber)) {
                NavigateActivityHelper.navigateToMyProfile(activity);
            } else {
                if ("-1".equals(phoneNumber.getContactId())) {
                    Intent contactDetail = new Intent(activity, ContactDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_MOCHA);
                    bundle.putString(NumberConstant.NAME, phoneNumber.getName());
                    bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, phoneNumber.getLastChangeAvatar());
                    bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, phoneNumber.getJidNumber());
                    contactDetail.putExtras(bundle);
                    activity.startActivity(contactDetail, true);
                } else if ("-2".equals(phoneNumber.getContactId())) {
                    NonContact existNonContact = mApplication.getContactBusiness().getExistNonContact(phoneNumber.getJidNumber());
                    if (existNonContact == null) {
                        mApplication.getContactBusiness().insertNonContact(phoneNumber, false);
                    }
                    Intent contactDetail = new Intent(activity, ContactDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
                    bundle.putString(NumberConstant.NUMBER, phoneNumber.getJidNumber());
                    contactDetail.putExtras(bundle);
                    activity.startActivity(contactDetail, true);
                } else {
                    Intent contactDetail = new Intent(activity, ContactDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
                    bundle.putString(NumberConstant.ID, phoneNumber.getId());
                    contactDetail.putExtras(bundle);
                    activity.startActivity(contactDetail, true);
                }
            }
        }
    }

    private static class ReactionHolder extends RecyclerView.ViewHolder {
        CircleImageView imgAvatar;
        TextView tvAvatar;
        TextView tvName;
        View itemView;

        public ReactionHolder(View itemView) {
            super(itemView);
            imgAvatar = (CircleImageView) itemView.findViewById(R.id.item_contact_view_avatar_circle);
            tvAvatar = (TextView) itemView.findViewById(R.id.contact_avatar_text);
            tvName = (TextView) itemView.findViewById(R.id.item_contact_view_name_text);
            this.itemView = itemView;
        }
    }
}

package com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary;

import com.metfone.selfcare.model.tab_video.Video;

public interface VideoLibraryPresenter {
    void getVideoByType(String type);

    void removerVideo(Video video);

    void removerListener();
}

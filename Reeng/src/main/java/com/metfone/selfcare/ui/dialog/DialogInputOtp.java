package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;

public abstract class DialogInputOtp extends Dialog implements View.OnClickListener {
    private Context context;
    private EditText edtOtp;
    private TextView tvResult, tvContent, mTvwCode1, mTvwCode2, mTvwCode3, mTvwCode4, mTvwCode5, mTvwCode6, btnDone, tvTitle;
    private View btnResend, btnCancel;
    int time = 180;

    public void setTime(int time) {
        this.time = time;
    }

    public DialogInputOtp(@NonNull Context context) {
        super(context, R.style.DialogOtpAVNO);
        setContentView(R.layout.dialog_input_otp);
        this.context = context;
        init(true);
    }

    public DialogInputOtp(@NonNull Context context, boolean showCoundownTimer) {
        super(context, R.style.DialogOtpAVNO);
        setContentView(R.layout.dialog_input_otp);
        this.context = context;
        init(showCoundownTimer);
    }

    private void init(boolean showCoundown) {
        tvTitle = findViewById(R.id.tv_title);
        edtOtp = findViewById(R.id.edt_otp);
        tvContent = findViewById(R.id.tv_content);
        tvResult = findViewById(R.id.tv_result);
        btnResend = findViewById(R.id.tv_resend);
        mTvwCode1 = findViewById(R.id.tv_code_1);
        mTvwCode2 = findViewById(R.id.tv_code_2);
        mTvwCode3 = findViewById(R.id.tv_code_3);
        mTvwCode4 = findViewById(R.id.tv_code_4);
        mTvwCode5 = findViewById(R.id.tv_code_5);
        mTvwCode6 = findViewById(R.id.tv_code_6);
        btnDone = findViewById(R.id.btnDone);
        btnCancel = findViewById(R.id.btnCancel);
        btnDone.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnResend.setOnClickListener(this);
        setPassEditTextListener();
        if (showCoundown) {
            initCoundownTimer();
            tvResult.setVisibility(View.VISIBLE);
        } else {
            tvResult.setVisibility(View.GONE);
        }
    }

    public void initCoundownTimer() {
        new CountDownTimer(180000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                btnResend.setVisibility(View.GONE);
                if (time < 0) time = 0;
                if (tvResult != null)
                    tvResult.setText(String.format(context.getResources().getString(R.string.text_dialog_otp_avno_result), String.valueOf(time)));
                time--;
            }

            @Override
            public void onFinish() {
                if (tvResult != null && btnResend != null) {
                    tvResult.setText(context.getResources().getString(R.string.text_dialog_otp_avno_no_result));
                    btnResend.setVisibility(View.VISIBLE);
                }
            }
        }.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_resend:
                onResend();
                break;
            case R.id.btnDone:
                String otp = edtOtp.getText().toString();
                if (otp.length() >= 6) {
                    onSubmitBuyOtp(otp);
                } else {
                    ToastUtils.makeText(context, R.string.code_invalid_format);
                }
                break;
            case R.id.btnCancel:
                onCancel();
                break;
        }
    }

    private void setPassEditTextListener() {
        edtOtp.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String otp = edtOtp.getText().toString();
                if (otp.length() > 0) {
                    btnDone.setEnabled(true);
                    btnDone.setTextColor(context.getResources().getColor(R.color.bg_mocha));
                } else {
                    btnDone.setEnabled(false);
                    btnDone.setTextColor(context.getResources().getColor(R.color.gray));
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                showCode(s.toString());
            }
        });

        edtOtp.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String otp = edtOtp.getText().toString();
                    if (otp.length() >= 6) {
                        onSubmitBuyOtp(otp);
                        return true;
                    } else {
                        ToastUtils.makeText(context, R.string.code_invalid_format);
                    }
                }
                return false;
            }
        });
    }

    private void showCode(String code) {
        clearCode();
        switch (code.length()) {
            case 0:

                break;
            case 1:
                char a = code.charAt(0);
                mTvwCode1.setText(String.valueOf(a));
                break;
            case 2:
                mTvwCode1.setText(String.valueOf(code.charAt(0)));
                mTvwCode2.setText(String.valueOf(code.charAt(1)));
                break;
            case 3:
                mTvwCode1.setText(String.valueOf(code.charAt(0)));
                mTvwCode2.setText(String.valueOf(code.charAt(1)));
                mTvwCode3.setText(String.valueOf(code.charAt(2)));
                break;
            case 4:
                mTvwCode1.setText(String.valueOf(code.charAt(0)));
                mTvwCode2.setText(String.valueOf(code.charAt(1)));
                mTvwCode3.setText(String.valueOf(code.charAt(2)));
                mTvwCode4.setText(String.valueOf(code.charAt(3)));
                break;
            case 5:
                mTvwCode1.setText(String.valueOf(code.charAt(0)));
                mTvwCode2.setText(String.valueOf(code.charAt(1)));
                mTvwCode3.setText(String.valueOf(code.charAt(2)));
                mTvwCode4.setText(String.valueOf(code.charAt(3)));
                mTvwCode5.setText(String.valueOf(code.charAt(4)));
                break;
            case 6:
                mTvwCode1.setText(String.valueOf(code.charAt(0)));
                mTvwCode2.setText(String.valueOf(code.charAt(1)));
                mTvwCode3.setText(String.valueOf(code.charAt(2)));
                mTvwCode4.setText(String.valueOf(code.charAt(3)));
                mTvwCode5.setText(String.valueOf(code.charAt(4)));
                mTvwCode6.setText(String.valueOf(code.charAt(5)));
                break;
        }

    }

    private void clearCode() {
        mTvwCode1.setText("");
        mTvwCode2.setText("");
        mTvwCode3.setText("");
        mTvwCode4.setText("");
        mTvwCode5.setText("");
        mTvwCode6.setText("");
    }

    public void clearTextOtp() {
        edtOtp.setText("");
        clearCode();
    }

    public void showContentResent() {
        if (context == null) return;
        if (tvContent != null && btnDone != null) {
            tvContent.setText(context.getResources().getString(R.string.buy_package_wrong_otp_avno));
            btnDone.setText(context.getResources().getString(R.string.btn_resend_otp));
        }
    }

    public void showContentText(String desc) {
        if (context == null) return;
        if (tvContent != null) {
            if (TextUtils.isEmpty(desc)) {
                tvContent.setText(context.getResources().getString(R.string.buy_package_wrong_otp_avno));
            } else {
                tvContent.setText(desc);
            }
        }
    }

    public void setTitle(String title) {
        if (tvTitle != null)
            tvTitle.setText(title);
    }

    abstract public void onSubmitBuyOtp(String otp);

    abstract public void onCancel();

    abstract public void onResend();

}

package com.metfone.selfcare.ui;

import android.Manifest;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.adapter.BaseEmoGridAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.OfficialActionManager;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.FilePickerListener;
import com.metfone.selfcare.listeners.ICusKeyboard;
import com.metfone.selfcare.ui.chatviews.ChatFooterView;
import com.metfone.selfcare.ui.chatviews.ChatMediaView;
import com.metfone.selfcare.ui.chatviews.ChatMoreOptionView;
import com.metfone.selfcare.ui.chatviews.ChatStickerView;
import com.metfone.selfcare.ui.chatviews.ChatVoicemailView;
import com.metfone.selfcare.ui.chatviews.MultiLineEdittextTag;
import com.metfone.selfcare.ui.chatviews.OfficialActionView;
import com.metfone.selfcare.ui.dialog.DialogConfirmChat;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.NavigationBarProvider;

import java.util.ArrayList;

/**
 * Created by huybq7 on 9/15/2014.
 */
public class CusKeyboardController implements CusKeyboardWidget.OnDrawerOpenListener,
        CusKeyboardWidget.OnDrawerScrollListener,
        CusKeyboardWidget.OnDrawerCloseListener,
        View.OnClickListener,
        CusRelativeLayout.IKeyboardChanged,
        View.OnTouchListener,
        View.OnLongClickListener,
        TextView.OnEditorActionListener,
        ChatFooterView.KeyboardControllerCallback,
        ChatMediaView.OnMediaListener,
        ChatMoreOptionView.OnMoreOptionListener,
        ChatActivity.RequestPermissionResult,
        OfficialActionManager.OfficialActionCallback {
    private static final String TAG = CusKeyboardController.class.getSimpleName();
    private ICusKeyboard.SendReengClickListener mSendReengClickListener;
    private ICusKeyboard.OpenCusKeyboarListener mOpenCusKeyboarListener;
    private ICusKeyboard.CloseCusKeyboarListener mCloseCusKeyboarListener;
    private ICusKeyboard.ChangeListMessageSizeListener mChangeListMessageSizeListener;
    private ICusKeyboard.ChangeNoMessageViewSizeListener mChangeNoMessageViewSizeListener;
    //
    private static final int SLIDE_OFF_CONTENT_AND_DONOTHING = 1;
    private static final int SLIDE_OFF_CONTENT_AND_CLOSE_CUSTOM_KEYBOARD = 2;
    private static final int SLIDE_OFF_CONTENT_AND_SHOW_SYSTEM_KEYBOARD = 3;
    //
    private ScreenStateInfo mScreenStateInfo;
    private ChatActivity mChatActivity;
    private CusKeyboardWidget mCusKeyboardWidget;
    private CusRelativeLayout mRootView;
    private Fragment mParentFragment;
    //
    private View mCurrentLayout = null;
    private View mLayoutVoice;
    private View mLayoutEmoticon;
    private View mLayoutPreviewImage;
    private View mLayoutMoreOption;
    private View mLayoutOAAction;

    private LinearLayout mLayoutContaint;
    private RelativeLayout mLayoutReply;
    private TextView mImgReplyClear;
    //
    private int mContentHeight;
    private DisplayMetrics displayMetrics;
    private WindowManager wm;
    //
    private int mChatBarHeight;
    //
    private Animation animationDown;
    private Animation animationUp;
    private boolean inProcessControlClick = false;
    private ViewPager.OnPageChangeListener pageChangeListener;
    private boolean needToShowCustomKeyBoardWhenSoftkeyboardHidding = false; //hide keyboard boi click vao chat bar
    private boolean isHiddenKeyboard = true;
    private int mCurrentKeyboardHeight;
    private int mCurrentKeyboardLandscapeHeight;//taplet
    //
    private boolean isStranger = false;
    private boolean mGsmMode = true;
    private boolean refreshMediaView = false;
    private boolean isHidden = true;
    //  for state click, longclick to EditText ( custom show/hidden softkeyboard system)
    private float mDownX;
    private float mDownY;
    private final float SCROLL_THRESHOLD = 10;

    private static final int STATE_DEFAULT = 0;
    private static final int STATE_DOWN = 1;
    private static final int STATE_CLICK = 2;
    private static final int STATE_LONG_CLICK = 3;
    private int stateActor = STATE_DEFAULT;

    private ViewTreeObserver.OnGlobalLayoutListener mChatBarLayoutListener;
    private ChatFooterView chatFooterView;
    private ChatStickerView chatStickerView;
    private ChatVoicemailView chatVoicemailView;
    private ChatMoreOptionView chatMoreOptionView;
    private OfficialActionView chatOfficialActionView;

    private ChatMediaView chatMediaView;
    private BaseEmoGridAdapter.KeyClickListener keyClickListener;
    private ArrayList<View> subContentViews;
    private FilePickerListener mFilePickerListener;
    private ApplicationController mApplication;
    private int currentFooterView = -1;
    private int threadType, mThreadId;
    private Handler handler = new Handler(Looper.getMainLooper());
    private MultiLineEdittextTag mChatInput;
    private boolean isViettel;

    public CusKeyboardController(ApplicationController application, BaseSlidingFragmentActivity activity) {
        this.mApplication = application;
        this.mScreenStateInfo = new ScreenStateInfo(mApplication, activity);
    }

    public void initKeyboardController(ChatActivity chatActivity, CusRelativeLayout rootView,
                                       CusKeyboardWidget cusKeyboardWidget,
                                       ICusKeyboard.ChangeListMessageSizeListener changeListMessageSizeListener,
                                       BaseEmoGridAdapter.KeyClickListener keyClickListener,
                                       boolean gsmMode, Fragment parentFragment,
                                       ViewPager.OnPageChangeListener onPageChangeListener,
                                       FilePickerListener filePickerListener,
                                       int threadType, int threadId, boolean isViettel) {
        this.mChatActivity = chatActivity;
        this.mCusKeyboardWidget = cusKeyboardWidget;
        this.mRootView = rootView;
        this.mChangeListMessageSizeListener = changeListMessageSizeListener;
        this.mGsmMode = gsmMode;
        this.keyClickListener = keyClickListener;
        this.mParentFragment = parentFragment;
        this.pageChangeListener = onPageChangeListener;
        chatFooterView = new ChatFooterView(chatActivity, cusKeyboardWidget, this, threadType, threadId, isViettel);
        mFilePickerListener = filePickerListener;
        this.threadType = threadType;
        this.mThreadId = threadId;

        long beginTime = System.currentTimeMillis();
        initAndSetListeners();
        setGsmMode(gsmMode, threadType);
        Log.d(TAG, "Perform - initKeyboardController take  " + (System.currentTimeMillis() - beginTime) + "ms rootView.getHeight() = " + rootView.getHeight());
    }

    public void setChangeNoMessageViewSizeListener(ICusKeyboard.ChangeNoMessageViewSizeListener changeNoMessageViewSizeListener) {
        this.mChangeNoMessageViewSizeListener = changeNoMessageViewSizeListener;
    }

    public void setStranger(boolean stranger) {
        this.isStranger = stranger;
        if (chatFooterView != null) chatFooterView.setStranger(isStranger);
    }

    public void setViettel(boolean viettel) {
        isViettel = viettel;
        chatFooterView.setViettel(isViettel);
    }

    public void freeController() {
        RelativeLayout layoutCharBar = mRootView.findViewById(R.id.person_chat_detail_footer_tool);
        if (mChatBarLayoutListener != null)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutCharBar.getViewTreeObserver().removeOnGlobalLayoutListener(mChatBarLayoutListener);
            } else {
                layoutCharBar.getViewTreeObserver().removeGlobalOnLayoutListener(mChatBarLayoutListener);
            }
        mRootView.removeKeyboardStateChangedListener(this);
    }

    private void initAndSetListeners() {
        init(mCusKeyboardWidget);
        setListenerView();
        getChatBarHeight(mCusKeyboardWidget);
        registerKeyboardHeightChange(mRootView);
        registerChatBarHeightChange(mRootView);
        mRootView.addKeyboardStateChangedListener(this);
        mImgReplyClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideFooterReplyView();
                if (mSendReengClickListener != null) {
                    mSendReengClickListener.onClearReplyClick();
                }
            }
        });
    }

    private void init(View parentView) {
        animationDown = AnimationUtils.loadAnimation(mChatActivity, R.anim.keyboard_slide_down);
        animationUp = AnimationUtils.loadAnimation(mChatActivity, R.anim.keyboard_slide_up);
        //
        mCusKeyboardWidget.setOnDrawerCloseListener(this);
        mCusKeyboardWidget.setTopOffset(mScreenStateInfo.getTopChatBarPopup());
        Log.d(TAG, "mCusKeyboardWidget.setTopOffset: " + mScreenStateInfo.getTopChatBarPopup());
        mCusKeyboardWidget.lock();
        mChatInput = parentView.findViewById(R.id.person_chat_detail_input_text);
        mChatInput.requestFocus();
        mChatInput.setOnEditorActionListener(this);
        setChatbarStateDefaultBtn();
        mLayoutContaint = parentView.findViewById(R.id.content);
        subContentViews = new ArrayList<>();
        mLayoutEmoticon = mLayoutContaint.findViewById(R.id.include_voice_sticker);
        mLayoutVoice = mLayoutContaint.findViewById(R.id.include_voice_mail);
        mLayoutPreviewImage = mLayoutContaint.findViewById(R.id.include_media_preview);
        mLayoutMoreOption = mLayoutContaint.findViewById(R.id.include_more_option);
        mLayoutOAAction = mLayoutContaint.findViewById(R.id.include_official_action);

        subContentViews.add(mLayoutEmoticon);
        subContentViews.add(mLayoutVoice);
        subContentViews.add(mLayoutPreviewImage);
        subContentViews.add(mLayoutMoreOption);
        subContentViews.add(mLayoutOAAction);
        //reply view
        mLayoutReply = parentView.findViewById(R.id.person_chat_detail_footer_reply);
        mImgReplyClear = mLayoutReply.findViewById(R.id.person_chat_detail_footer_reply_clear);
        hideFooterReplyView();
        //mStikerSuggestLayoutHeight = Utilities.dpToPixels(35, mChatActivity.getResources());
    }

    public void showFooterReplyView(String friendJid, String content, String link, String filePath) {
        mLayoutReply.setVisibility(View.VISIBLE);
        MessageHelper.drawReplyView(mApplication, mLayoutReply, friendJid, content, link, filePath,
                mApplication.getMessageBusiness().getThreadById(mThreadId));
        chatFooterView.hideMoreOptionAndShowKeyboard();
    }

    public void hideFooterReplyView() {
        mLayoutReply.setVisibility(View.GONE);
    }

    public boolean isShowFooterReply() {
        return mLayoutReply != null && mLayoutReply.getVisibility() == View.VISIBLE;
    }

    //goi cung voi ham callback cua fragment
    public void onCreateView() {
    }

    public void onResume() {
        if (chatFooterView != null) {
            chatFooterView.onResume();
        }
        if (chatStickerView != null) {
            chatStickerView.onResume();
        }
        if (chatMediaView != null) {
            chatMediaView.onResume();
        }
        if (chatVoicemailView != null) {
            chatVoicemailView.onResume();
        }
        if (chatOfficialActionView != null) {
            chatOfficialActionView.onResume();
        }
        OfficialActionManager.getInstance(mApplication).addOfficialActionListener(this);
    }

    public void onPause() {
        if (chatVoicemailView != null) {
            if (chatVoicemailView.isVoicemailRecording()) {
                chatVoicemailView.stopVoicemailRecording();
                chatVoicemailView.deleteRecordedFile();
            }
            chatVoicemailView.resetSendVoicemailUI();
        }
        if (chatFooterView != null) {
            chatFooterView.onPause();
        }
        OfficialActionManager.getInstance(mApplication).removeOfficialActionListener(this);
    }

    public void onStop() {
        hideSoftAndCustomKeyboard();
    }

    private void setListenerView() {
        mCusKeyboardWidget.setOnDrawerOpenListener(this);
        mCusKeyboardWidget.setOnDrawerCloseListener(this);
        mCusKeyboardWidget.setOnDrawerScrollListener(this);
    }

    private void getChatBarHeight(View parentView) {
        mChatBarHeight = (int) mChatActivity.getResources().getDimension(R.dimen.emoticon_icon_height);
        Log.d(TAG, "init chatBar height from dimen: " + mChatBarHeight);
    }

    private void registerKeyboardHeightChange(final View rootView) {
        // khoi tao lan dau
        if (mScreenStateInfo != null)
            mContentHeight = mScreenStateInfo.getContentHeightDefault();
//        displayMetrics = new DisplayMetrics();
//        wm = (WindowManager) mApplication.getSystemService(Context.WINDOW_SERVICE);
        ViewTreeObserver.OnGlobalLayoutListener mRootLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                updateGUI(rootView);
            }
        };
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(mRootLayoutListener);
    }

    private void registerChatBarHeightChange(final View parentView) {
        final RelativeLayout layoutCharbar = parentView.findViewById(R.id.person_chat_detail_footer_tool);
        mChatBarLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (mChatBarHeight != layoutCharbar.getHeight()) {
                    Log.d(TAG, "registerChatbarHeightChange");
                    int delta = layoutCharbar.getHeight() - mChatBarHeight;
                    mChatBarHeight = layoutCharbar.getHeight();
                    int offset = mScreenStateInfo.getTopChatBarPopup() - delta;
                    mScreenStateInfo.setTopChatBatPopup(mChatActivity, offset, true);
                    mCusKeyboardWidget.setTopOffset(mScreenStateInfo.getTopChatBarPopup());
                    if (mChangeListMessageSizeListener != null) {
                        if (isOpened()) {
                            mChangeListMessageSizeListener.onChangeListMessageSize(mScreenStateInfo.getTopChatBarPopup());
                        } else {
                            mChangeListMessageSizeListener.onChangeListMessageSize(mContentHeight - mChatBarHeight);
                        }
                    }

                    if (mChangeNoMessageViewSizeListener != null) {
                        if (isOpened()) {
                            mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(mScreenStateInfo.getTopChatBarPopup());
                        } else {
                            mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(mContentHeight - mChatBarHeight);
                        }
                    }
                }
            }
        };
        layoutCharbar.getViewTreeObserver().addOnGlobalLayoutListener(mChatBarLayoutListener);
    }

    private void updateGUI(View rootView) {
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mChatActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int heightScreen = displayMetrics.heightPixels;
        int widthScreen = displayMetrics.widthPixels;
        if ((mScreenStateInfo.getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT &&
                displayMetrics.heightPixels < displayMetrics.widthPixels) ||
                (mScreenStateInfo.getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE &&
                        displayMetrics.heightPixels > displayMetrics.widthPixels)) {
            return;
        }

//        Log.d(TAG, "hainv0 r.bottom: " + r.bottom + " r.right: " + r.right + " widthScreen: " + widthScreen + " heightScreen: " + heightScreen);

        int bottom = r.bottom;
        int navigationHeight = NavigationBarProvider.getInstance().getNavigationBarHeight(mChatActivity);
        if (Math.abs(r.bottom - widthScreen) <= navigationHeight && Math.abs(r.right - heightScreen) <= navigationHeight) {
            bottom = r.right;
        }

        int heightDifference = heightScreen - bottom;
        Log.i(TAG, "onGlobalLayout screenHeight = " + heightScreen + " screen: " +
                mScreenStateInfo.getScreenOrientation() + " rootView.getHeight(): " + rootView.getHeight());
        Log.i(TAG, "onGlobalLayout heightDifference = " + heightDifference + " r.bottom: " +
                r.bottom + " mCurrentKeyboardHeight: " + mCurrentKeyboardHeight +
                " mCurrentKeyboardLandscapeHeight: " + mCurrentKeyboardLandscapeHeight);

//        Log.d(TAG, "hainv1 : heightDifference: " + heightDifference + " getScreenOrientation: " + ((mScreenStateInfo.getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT) ? "PORTRAIT" : "LANSCAPE"));

        if (heightScreen != 0 && widthScreen != 0 && widthScreen >= heightScreen) {
            if (DeviceHelper.isTablet(mApplication)) {
                if (heightDifference > 0 && heightDifference != mCurrentKeyboardLandscapeHeight) {
                    mCurrentKeyboardLandscapeHeight = heightDifference;
                    int mTopOffsetChatBatPopup = rootView.getHeight() - mChatBarHeight;
                    mScreenStateInfo.setTopChatBatPopup(mChatActivity, mTopOffsetChatBatPopup, true);
                    Log.d(TAG, "setTopChatBatPopup: " + mTopOffsetChatBatPopup);
                    rootView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "registerKeyboardHeightChange.setTopOffset: " + mScreenStateInfo.getTopChatBarPopup());
                            mCusKeyboardWidget.setTopOffset(mScreenStateInfo.getTopChatBarPopup());
                            mCusKeyboardWidget.open();
//                            Log.d(TAG, "hainv2 : mScreenStateInfo.getTopChatBarPopup(): " + mScreenStateInfo.getTopChatBarPopup());
                            if (mChangeListMessageSizeListener != null) {
                                mChangeListMessageSizeListener.onChangeListMessageSize(mScreenStateInfo.getTopChatBarPopup());
                            }

                            if (mChangeNoMessageViewSizeListener != null) {
                                mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(mScreenStateInfo.getTopChatBarPopup());
                            }
                        }
                    }, 40);
                    InputMethodUtils.setIntPreferenceHeightPref(mChatActivity, Constants.PREFERENCE.PREF_TAPLET_LANGSCAPE_KEYBOARD_OFFSET_HEIGHT, mTopOffsetChatBatPopup);
                }
            }
            // init listMessage height
            if (rootView.getHeight() > 0 && mContentHeight != rootView.getHeight()) {
                if (mCusKeyboardWidget != null && !mCusKeyboardWidget.isOpened()) {
                    int height = rootView.getHeight() - mChatBarHeight;
//                    Log.d(TAG, "hainv3 : mContentHeight: " + mContentHeight);
                    if (mChangeListMessageSizeListener != null) {
                        mChangeListMessageSizeListener.onChangeListMessageSize(height);
                    }

                    if (mChangeNoMessageViewSizeListener != null) {
                        mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(height);
                    }
                }
                mContentHeight = rootView.getHeight();
                Log.d(TAG, "listMessage height : " + mContentHeight);
            }
        } else {
            if (heightDifference > 0 && heightDifference != mCurrentKeyboardHeight) {
                mCurrentKeyboardHeight = heightDifference;
                final int mTopOffsetChatBatPopup = rootView.getHeight() - mChatBarHeight;
                mScreenStateInfo.setTopChatBatPopup(mChatActivity, mTopOffsetChatBatPopup, true);
                Log.d(TAG, "setTopChatBatPopup: " + mTopOffsetChatBatPopup);
                rootView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "registerKeyboardHeightChange.setTopOffset: " + mScreenStateInfo.getTopChatBarPopup());
                        mCusKeyboardWidget.setTopOffset(mScreenStateInfo.getTopChatBarPopup());
                        mCusKeyboardWidget.open();
//                        Log.d(TAG, "hainv4 : mTopOffsetChatBatPopup: " + mTopOffsetChatBatPopup + "mChatBarHeight: " + mChatBarHeight);
                        if (mChangeListMessageSizeListener != null) {
                            mChangeListMessageSizeListener.onChangeListMessageSize(mScreenStateInfo.getTopChatBarPopup());
                        }

                        if (mChangeNoMessageViewSizeListener != null) {
                            mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(mScreenStateInfo.getTopChatBarPopup());
                        }
                    }
                }, 40);
                InputMethodUtils.setIntPreferenceHeightPref(mChatActivity, Constants.PREFERENCE.PREF_KEYBOARD_OFFSET_HEIGHT, mTopOffsetChatBatPopup);
            }
            /// init listMessage height
            if (rootView.getHeight() > 0 && mContentHeight != rootView.getHeight()) {
                if (mCusKeyboardWidget != null && !mCusKeyboardWidget.isOpened()) {
                    int height = rootView.getHeight() - mChatBarHeight;
//                    Log.d(TAG, "hainv5 : mContentHeight: " + mContentHeight);
                    if (mChangeListMessageSizeListener != null) {
                        mChangeListMessageSizeListener.onChangeListMessageSize(height);
                    }

                    if (mChangeNoMessageViewSizeListener != null) {
                        mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(height);
                    }
                }
                mContentHeight = rootView.getHeight();
                Log.d(TAG, "listMessage height : " + mContentHeight);
            }
        }
        Log.d(TAG, "set mContentHeight: " + mContentHeight);
    }

    //
    public void updateStateConnection(boolean isConntect) {
        updateListMessageHeight();
    }

    public void updateListMessageHeight() {
        Log.d(TAG, "updateListMessageHeight:  ");
        if (mChangeListMessageSizeListener != null) {
            if (mCusKeyboardWidget.isOpened()) {
                mChangeListMessageSizeListener.onChangeListMessageSize(mScreenStateInfo.getTopChatBarPopup());
            } else {
                mChangeListMessageSizeListener.onChangeListMessageSize(mContentHeight - mChatBarHeight);
            }
        }

        if (mChangeNoMessageViewSizeListener != null) {
            if (mCusKeyboardWidget.isOpened()) {
                mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(mScreenStateInfo.getTopChatBarPopup());
            } else {
                mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(mContentHeight - mChatBarHeight);
            }
        }
    }

    /**
     * check keyboard open
     *
     * @return
     */
    public boolean isOpened() {
        if (mCusKeyboardWidget == null) {
            return false;
        }
        return mCusKeyboardWidget.isOpened();
    }

    // listener system keyboard  show/hidden
    @Override
    public void onSystemKeyboardHidden() {
        Log.i(TAG, "onSystemKeyboardHidden");
        //co truong hop khi dang show soft keyboard ma click back thi ko nhay vao ham onBackPressed, ma se nhay vao ham nay
        isHiddenKeyboard = true;
        if (!needToShowCustomKeyBoardWhenSoftkeyboardHidding) {
            Log.i(TAG, "onSystemKeyboardHidden close custom keyboard follow soft keyboard");
            closeCusKeyboardFolowRootView();
        } else {
            Log.i(TAG, "onSystemKeyboardHidden is custom keyboard not opening");
        }
    }

    @Override
    public void onChangeContentHeight(int currentContentHeight) {
        if (currentContentHeight > 0 && mContentHeight != currentContentHeight) {
            //            mContentHeight = currentC = currentContentHeight;
        }
    }

    @Override
    public void onSystemKeyboardShown(int currentContentHeight, int keyBoardHeight) {
        isHiddenKeyboard = false;
        Log.d(TAG, "onSystemKeyboardShown:  " + currentContentHeight + " - " + keyBoardHeight);
        if (!mCusKeyboardWidget.isOpened()) {
            mCusKeyboardWidget.open();
        }
        chatFooterView.onSystemKeyboardShown();
        makeAllSubContentViewInvisible();
    }

    // listener custom keyboard show/sliding/hidden
    @Override
    public void onCusKeyboardOpened(String openCode) {
        Log.d(TAG, "onCusKeyboardOpened isHidden:" + isHidden);
        if (mChangeNoMessageViewSizeListener != null) {
            mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(mScreenStateInfo.getTopChatBarPopup());
        }

        if (mChangeListMessageSizeListener != null) {
            mChangeListMessageSizeListener.onChangeListMessageSize(mScreenStateInfo.getTopChatBarPopup());
        }
        setChatbarStateDefaultBtn();
        if (this.mOpenCusKeyboarListener != null) {
            this.mOpenCusKeyboarListener.onOpenCusKeyboard();
        }
        isHidden = false;
    }

    @Override
    public synchronized void onCusKeyboardScrollStarted() {
        setChatbarStateDisable();
    }

    @Override
    public synchronized void onCusKeyboardScrollEnded() {
        setChatbarStateWhenDisableFinish();
    }

    @Override
    public synchronized void onCusKeyboardClosed() {
        Log.d(TAG, " - onCusKeyboardClosed isHidden:" + isHidden);
        if (mChangeNoMessageViewSizeListener != null) {
            mChangeNoMessageViewSizeListener.onChangeNoMessageViewSize(mContentHeight - mChatBarHeight);
        }

        if (mChangeListMessageSizeListener != null) {
            mChangeListMessageSizeListener.onChangeListMessageSize(mContentHeight - mChatBarHeight);
        }
        setChatbarStateDefaultBtn();
        isHidden = true;
        if (this.mCloseCusKeyboarListener != null) {
            this.mCloseCusKeyboarListener.onCloseCusKeyboard();
        }
    }

    public synchronized void hideSoftAndCustomKeyboard() {
        isHidden = true;
        Log.i(TAG, "hideKeyboardController hide custom keyboard and show Top footer");
        needToShowCustomKeyBoardWhenSoftkeyboardHidding = false;
        mCusKeyboardWidget.close();
        chatFooterView.showTopFooter();
        makeAllSubContentViewInvisible();
        Log.i(TAG, "hideKeyboardController hide soft keyboard");
        InputMethodUtils.hideSoftKeyboard(chatFooterView.getEdtTextInput(), mChatActivity);
    }

    private void hideCustomKeyboardWhenShowSoftLangScape() {
        isHidden = true;
        Log.i(TAG, "hideCustomKeyboardWhenShowSoftLangScape");
        needToShowCustomKeyBoardWhenSoftkeyboardHidding = false;
        mCusKeyboardWidget.close();
        makeAllSubContentViewInvisible();
    }

    private void closeCusKeyboardFolowRootView() {
        mRootView.post(new Runnable() {
            @Override
            public void run() {
                // Log.d(TAG, " -  mCusKeyboardWidget:" + mCusKeyboardWidget + " -  mCusKeyboardWidget.isOpened():" + mCusKeyboardWidget.isOpened());
                if (mCusKeyboardWidget != null && mCusKeyboardWidget.isOpened()) {
                    needToShowCustomKeyBoardWhenSoftkeyboardHidding = false;
                    mCusKeyboardWidget.animateClose();
                }
            }
        });
    }

    @Override
    public void onClick(View controlView) {
        if (this.mOpenCusKeyboarListener != null) {
            this.mOpenCusKeyboarListener.onClickCusKeyboard();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        Log.d(TAG, "onEditorAction actionId: " + actionId);
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            if (mSendReengClickListener != null) {
                mSendReengClickListener.onSendReengKeyboardClick();
            }
            return true;
        } else if (actionId == 1010) {// action send when keyboard landscap
            InputMethodUtils.hideSoftKeyboard(mChatInput, mChatActivity);
            sendTextClickCallback();
            return true;
        }
        return false;
    }

    @Override
    public boolean onLongClick(View view) {
        if (stateActor == STATE_DOWN) {
            stateActor = STATE_LONG_CLICK;
            return false;
        }
        return true;
    }

    /**
     * Custom show/hidden system keyboard
     *
     * @param controlView
     * @param event
     * @return
     */
    @Override
    public boolean onTouch(View controlView, MotionEvent event) {
        Log.d(TAG + "[Rotate]", "onTouch: " + (event.getAction() & MotionEvent.ACTION_MASK));
        if (isInProcessControlClick()) {
            Log.d(TAG + "[Rotate]", "onTouch: inProcessControlClick");
            return true;
        }
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mDownX = event.getX();
                mDownY = event.getY();
                stateActor = STATE_DOWN;
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (stateActor == STATE_DOWN) {
                    stateActor = STATE_CLICK;
                    if (isOpened()) {
                        if (isHiddenKeyboard) {
                        } else {
                            return false;
                        }
                    } else {
                        // disable auto show system keyboard
                        disableSystemKeyboard(event);
                        // consumed the event
                        return true;
                    }
                } else if (stateActor == STATE_LONG_CLICK) {
                    // consumed the event
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (stateActor == STATE_DOWN && (Math.abs(mDownX - event.getX()) > SCROLL_THRESHOLD || Math.abs(mDownY - event.getY()) > SCROLL_THRESHOLD)) {
                    stateActor = STATE_DEFAULT;
                }
                break;
            default:
                break;
        }
        return false;
    }

    private void disableSystemKeyboard(MotionEvent event) {
        onClickChatInputListener();
    }

    // 4
    private void onClickChatInputListener() {
        if (isInProcessControlClick()) {
            return;
        }
        setInProcessControlClick(true);
        setChatbarStateDisable();
        onSelectChatInput();
        if (this.mOpenCusKeyboarListener != null) {
            this.mOpenCusKeyboarListener.onClickCusKeyboard();
        }
    }

    private synchronized void onSelectChatInput() {
        Log.i(TAG, "onSelectChatInput");
        //        check state Keyboard; open or close
        if (mCusKeyboardWidget.isOpened()) {
            startContentSlidingOffAnd(SLIDE_OFF_CONTENT_AND_SHOW_SYSTEM_KEYBOARD);
        } else {
            // open cuskeyboard
            mCusKeyboardWidget.open();
            chatFooterView.manualShowSoftKeyboard();
        }
    }

    /**
     * @param isFlag 1: Do nothing;
     *               2: Close Custom Keyboard;
     *               3: Show system Keyboard
     */
    private synchronized void startContentSlidingOffAnd(final int isFlag) {
        Log.i(TAG, "startContentSlidingOffAnd");
        if (mCurrentLayout != null) {
        } else { //  nothing show
            if (isFlag == SLIDE_OFF_CONTENT_AND_CLOSE_CUSTOM_KEYBOARD) {
                if (mCusKeyboardWidget != null && mCusKeyboardWidget.isOpened()) {
                    needToShowCustomKeyBoardWhenSoftkeyboardHidding = false;
                    mCusKeyboardWidget.animateClose();
                }
            } else if (isFlag == SLIDE_OFF_CONTENT_AND_SHOW_SYSTEM_KEYBOARD) {
                chatFooterView.manualShowSoftKeyboard();
            }
            setChatbarStateWhenDisableFinish();
        }
    }

    /**
     * Sliding Off curent content.
     * slidingOn == null: after sliding Off, Keyboard will hide
     * slidingOn != null: after sliding Off, View slidingOn will visible
     */
    private synchronized void startContentSlidingOffAndShow(final View contentView) {
        animationDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (contentView != null) {
                    startContentSlidingOn(contentView);
                } else {
                    // change state of Charbar to enable
                    setChatbarStateWhenDisableFinish();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        if (mCurrentLayout != null) {
            mCurrentLayout.startAnimation(animationDown);
        } else {
            if (contentView != null) {
                startContentSlidingOn(contentView);
            } else {
                // change state of Charbar to enable
                setChatbarStateWhenDisableFinish();
            }
        }
    }

    private synchronized void startContentSlidingOn(final View onSlidingView) {
        animationUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mCurrentLayout = onSlidingView;
                Log.i(TAG, "startContentSlidingOn onAnimationEnd");
                //                needToShowCustomKeyBoardWhenSoftkeyboardHidding = false;
                setChatbarStateWhenDisableFinish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        if (onSlidingView != null) {
            onSlidingView.startAnimation(animationUp);
        } else {
            // change state of Charbar to enable
            setChatbarStateWhenDisableFinish();
        }
    }

    private void setChatbarStateDefaultBtn() {
        setChatbarStateWhenDisableFinish();

    }

    // for CusKeyboard effect when Sliding
    public void setChatbarStateDisable() {
        setInProcessControlClick(true);
    }

    public void setChatbarStateWhenDisableFinish() {
        setInProcessControlClick(false);
    }

    public void setSendReengClickListener(ICusKeyboard.SendReengClickListener sendReengClickListener) {
        this.mSendReengClickListener = sendReengClickListener;
    }

    public void setOpenCusKeyboarListener(ICusKeyboard.OpenCusKeyboarListener openCusKeyboarListener) {
        this.mOpenCusKeyboarListener = openCusKeyboarListener;
    }

    public void setCloseCusKeyboarListener(ICusKeyboard.CloseCusKeyboarListener closeCusKeyboarListener) {
        this.mCloseCusKeyboarListener = closeCusKeyboarListener;
    }

    public void showFooterPreviewImage() {
        chatFooterView.selectedButtonPreviewMedia();
    }

    @Override
    public void onBottomActionClick(int actionId) {
        mSendReengClickListener.onChatBarClickBottomAction(actionId);
    }

    @Override
    public void onChatBarClick() {
        if (mSendReengClickListener != null) {
            mSendReengClickListener.onChatBarClick();
        }
    }

    @Override
    public void onSwitchButtonComplete(boolean isSendSms) {
        if (mSendReengClickListener != null) {
            mSendReengClickListener.onSwitchSendButtonComplete(isSendSms);
        }
    }

    @Override
    public void hideMediaView() {
        if (mCusKeyboardWidget.isOpened()) {
            mCusKeyboardWidget.close();
        }
    }

    @Override
    public void showFooterView(int functionId) {
        Log.d(TAG, "showFooterView: " + functionId);
        //check theo id cua tung chuc nang
        needToShowCustomKeyBoardWhenSoftkeyboardHidding = false;
        currentFooterView = functionId;
        boolean isTabLet = DeviceHelper.isTablet(mApplication);
        switch (functionId) {
            case ChatFooterView.FUNCTION_EMO:
                needToShowCustomKeyBoardWhenSoftkeyboardHidding = true;
                showStickerView();
                break;
            case ChatFooterView.FUNCTION_MORE_OPTION:
                needToShowCustomKeyBoardWhenSoftkeyboardHidding = true;
                showMoreOption();
                break;
            case ChatFooterView.FUNCTION_SEND_VOICE:
                Log.i(TAG, "");
                needToShowCustomKeyBoardWhenSoftkeyboardHidding = true;
                showVoiceRecord();
                break;
            case ChatFooterView.FUNCTION_KEYBOARD:
                chatFooterView.showEditText();
                if (!isTabLet && mScreenStateInfo.getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
                    hideCustomKeyboardWhenShowSoftLangScape();
                }
                break;
            case ChatFooterView.FUNCTION_SEND_IMAGE:
                needToShowCustomKeyBoardWhenSoftkeyboardHidding = true;
                showMediaPreview();
                break;
            case ChatFooterView.FUNCTION_CAMERA:
                handleCamera();
                break;
            case ChatFooterView.FUNCTION_EDIT_TEXT:
                chatFooterView.showEditText();
                if (!isTabLet && mScreenStateInfo.getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
                    hideCustomKeyboardWhenShowSoftLangScape();
                }
                break;
            case ChatFooterView.FUNCTION_SEND_CONTACT:
                mChatActivity.dispatchShareContactIntent();
                break;
            case ChatFooterView.FUNCTION_SEND_LOCATION:
                onShareLocation();
                break;
            case ChatFooterView.FUNCTION_SEND_FILE:
                mChatActivity.dispatchShareFile();
                break;
            case ChatFooterView.FUNCTION_OFFICIAL_ACTION:
                needToShowCustomKeyBoardWhenSoftkeyboardHidding = true;
                showOfficialAction();
                break;
            case ChatFooterView.FUNCTION_SEND_OPTION_ESPORT:
                mChatActivity.showToast(mChatActivity.getString(R.string.comming_soon), Toast.LENGTH_SHORT);
                break;
            default:
                break;
        }
    }

    @Override
    public void onButtonArrowPress() {
        needToShowCustomKeyBoardWhenSoftkeyboardHidding = false;
    }

    @Override
    public int getOptionContentHeight() {
        if (subContentViews == null) return 0;
        for (View subContent : subContentViews) {
            if (subContent.getVisibility() == View.VISIBLE) {
                return subContent.getHeight();
            }
        }
        return 0;
    }

    @Override
    public void declined() {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatFooterView.resetFooterView();
            }
        });
    }

    private void handleCamera() {
        mChatActivity.setPermissionResult(this);
        mChatActivity.dispatchTakePhotoIntent(Constants.ACTION.ACTION_TAKE_PHOTO);
        //resetFooterView
    }

    @Override
    public void onManualHideSoftKeyboard() {
        //        needToShowCustomKeyBoardWhenSoftkeyboardHidding = true;
    }

    @Override
    public void showSoftKeyboardIfCustomKeyboardIsOpening() {
        if (mCusKeyboardWidget.isOpened()) {
            //show soft keyboard
            chatFooterView.showSoftKeyboard();
        }
    }

    private void showMediaPreview() {
        if (chatMediaView == null || refreshMediaView) {
            chatMediaView = new ChatMediaView(mChatActivity, mLayoutPreviewImage, this);
            refreshMediaView = false;
        } else {
            notifyChangePageAdapter();
        }
        makeSubContentViewInvisible(mLayoutPreviewImage);
        if (!mCusKeyboardWidget.isOpened()) {
            mCusKeyboardWidget.open();
            showContentView();
        }
    }

    private void notifyChangePageAdapter() {
        if (chatMediaView != null && mCusKeyboardWidget.isOpened() &&
                currentFooterView == ChatFooterView.FUNCTION_SEND_IMAGE) {
            chatMediaView.notifyChangePageAdapter();
        }
        if (chatVoicemailView != null) {
            chatVoicemailView.changeLayoutParamProgress(mScreenStateInfo.getScreenOrientation());
        }
    }

    private void showVoiceRecord() {
        Log.d(TAG, "showVoiceRecord");
        if (isInProcessControlClick()) {
            return;
        }
        if (chatVoicemailView == null) {
            ChatVoicemailView.OnVoicemailListener listener = new ChatVoicemailView.OnVoicemailListener() {
                @Override
                public void onSendVoicemailMessage(String filePath, int elapsedTimeSecs, String fileName) {
                    mSendReengClickListener.onSendVoicemail(filePath, elapsedTimeSecs, fileName);
                }
            };
            chatVoicemailView = new ChatVoicemailView(mChatActivity, listener, mScreenStateInfo);
            chatVoicemailView.setChatVoicemailView(mLayoutVoice);
        }

        if (!mCusKeyboardWidget.isOpened()) {
            mCusKeyboardWidget.open();
            showContentView();
        }
        makeSubContentViewInvisible(mLayoutVoice);
    }

    private void showOfficialAction() {
        if (chatOfficialActionView == null) {
            Log.d(TAG, "showOfficialAction init");
            chatOfficialActionView = new OfficialActionView(mChatActivity, mScreenStateInfo, mParentFragment, mThreadId);
            chatOfficialActionView.setChatOAActionView(mLayoutOAAction);
        }
        chatOfficialActionView.showActionView();
        makeSubContentViewInvisible(mLayoutOAAction);
        if (!mCusKeyboardWidget.isOpened()) {
            mCusKeyboardWidget.open();
            showContentView();
        }
    }

    @Override
    public void onChangedOfficialAction() {
        if (chatOfficialActionView != null && mLayoutOAAction != null && mLayoutOAAction.getVisibility() == View.VISIBLE) {
            chatOfficialActionView.showActionView();
        }
    }

    @Override
    public void onShowOfficialAction(int id) {
        if (mThreadId == id && chatFooterView != null) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    chatFooterView.handleClickOfficialAction();
                    showFooterView(ChatFooterView.FUNCTION_OFFICIAL_ACTION);
                }
            }, 200);
        }
    }

    @Override
    public void onGetOfficialActionFail(int threadId) {
        if (mThreadId == threadId && chatOfficialActionView != null &&
                mLayoutOAAction != null && mLayoutOAAction.getVisibility() == View.VISIBLE) {
            chatOfficialActionView.showActionView();
        }
    }

    private void showMoreOption() {
        Log.i(TAG, "showMoreOption");
        if (chatMoreOptionView == null) {
            chatMoreOptionView = new ChatMoreOptionView(mApplication, mLayoutMoreOption, this);
        }
        makeSubContentViewInvisible(mLayoutMoreOption);
        if (!mCusKeyboardWidget.isOpened()) {
            Log.i(TAG, "showStickerView open mCusKeyboardWidget");
            mCusKeyboardWidget.open();
            showContentView();
        }
    }

    public void notifyChangeRecentSticker(ArrayList<StickerItem> listItems) {
        if (chatStickerView != null) {
            chatStickerView.notifyChangeRecentSticker(listItems);
        }
    }

    private void showStickerView() {
        Log.i(TAG, "showStickerView");
        if (chatStickerView == null) {
            chatStickerView = new ChatStickerView(mChatActivity,
                    mLayoutEmoticon, keyClickListener, pageChangeListener, mChatInput, threadType);
        }
        makeSubContentViewInvisible(mLayoutEmoticon);
        if (!mCusKeyboardWidget.isOpened()) {
            Log.i(TAG, "showStickerView open mCusKeyboardWidget");
            mCusKeyboardWidget.open();
            showContentView();
        }
    }

    @Override
    public void sendTextClickCallback() {
        if (mSendReengClickListener != null) {
            mSendReengClickListener.onSendReengClick();
        }
    }

    public void showEditTextWhenSendEmo() {
        // chatFooterView.showEditText();
        needToShowCustomKeyBoardWhenSoftkeyboardHidding = false;
    }

    @Override
    public void onSendImageMessage(String filePath) {
        if (mSendReengClickListener != null) {
            mSendReengClickListener.onSendImage(filePath);
        }
    }

    @Override
    public void onOpenPreviewMedia() {
        if (mSendReengClickListener != null) {
            mSendReengClickListener.onOpenPreviewMedia();
        }
    }

    @Override
    public void onSendVideo(final ImageInfo imageInfo) {
        if (mFilePickerListener != null) {
            //check video size
            if (imageInfo.getFileSize() > Constants.FILE.VIDEO_MAX_SIZE) {
                mChatActivity.showToast(String.format(mChatActivity.getResources().getString(R.string.video_size_limit),
                        (int) FileHelper.getSizeInMbFromByte(Constants.FILE.VIDEO_MAX_SIZE)), Toast.LENGTH_SHORT);
                return;
            }
            //check video duration
//            if(imageInfo.getDurationInSecond() > Constants.FILE.VIDEO_MAX_DURATION){
//                mChatActivity.showToast(String.format(mChatActivity.getResources().getString(R.string.err_max_video_duration),
//                                        Constants.FILE.VIDEO_MAX_DURATION), Toast.LENGTH_SHORT);
//                return;
//            }
            //check video format
            String extension = imageInfo.getImagePath().substring(imageInfo.getImagePath().lastIndexOf("."));
            if (!extension.equals(Constants.FILE.GP_FILE_SUFFIX) && !extension.equals(Constants.FILE.MP4_FILE_SUFFIX)) {
                mChatActivity.showToast(mChatActivity.getString(R.string.err_video_format), Toast.LENGTH_SHORT);
                return;
            }
            mChatActivity.convertAndSendVideo(imageInfo);
        }
    }

    public void setGsmMode(boolean gsmMode, int threadType) {
        this.mGsmMode = gsmMode;
        if (chatFooterView != null) {
            chatFooterView.setGsmMode(gsmMode, threadType);
        } else {
            Log.i(TAG, "setGsmMode error chatFooterView = null");
        }
    }

    @Override
    public void onSendContact() {
        mChatActivity.dispatchShareContactIntent();
    }

    @Override
    public void onShareLocation() {
        if (!LocationHelper.getInstant(mApplication).checkSupportGLEV2()) {
            mChatActivity.showToast(mChatActivity.getResources().getString(R.string.not_support_gle_v2), Toast.LENGTH_LONG);
        } else if (LocationHelper.getInstant(mApplication).checkGooglePlayService()) {
            Log.i(TAG, "checkGooglePlayService-----ok");
            if (PermissionHelper.allowedPermission(mChatActivity, Manifest.permission.ACCESS_COARSE_LOCATION) && PermissionHelper.allowedPermission(mChatActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                mChatActivity.dispatchShareLocation();
            } else {
                PermissionHelper.requestPermissionWithGuide(mChatActivity,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
            }
        } else {
            showDialogGGPlayService();
        }
    }

    @Override
    public void onShareEsport() {
        mChatActivity.showToast(mChatActivity.getString(R.string.comming_soon), Toast.LENGTH_SHORT);
    }

    public boolean isInProcessControlClick() {
        return inProcessControlClick;
    }

    public void setInProcessControlClick(boolean inProcessControlClick) {
        this.inProcessControlClick = inProcessControlClick;
    }

    /**
     * @param newConfig
     */
    public void onScreenStateChange(Configuration newConfig, Handler handler) {
        Log.d(TAG, "onScreenStateChange: " + newConfig.orientation);
//        Log.d(TAG, "hainv6 : onScreenStateChange: " + newConfig.orientation);
        // updateListMessageHeight();
        mScreenStateInfo.changeOrientation(mChatActivity, newConfig.orientation, true);
        mCusKeyboardWidget.setTopOffset(mScreenStateInfo.getTopChatBarPopup());
        updateListMessageHeight();
        // update preview image and video
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyChangePageAdapter();
            }
        }, 55);
    }

    /**
     * Sliding Off curent content.
     * slidingOn == null: after sliding Off, Keyboard will hide
     * slidingOn != null: after sliding Off, View slidingOn will visible
     */
    private synchronized void showContentView() {
        for (View subContent : subContentViews) {
            if (subContent.getVisibility() == View.VISIBLE) {
                Log.i(TAG, subContent.getClass().getSimpleName() + " is visible");
            }
        }
        animationDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startContentSlidingOn(mLayoutContaint);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        Log.i(TAG, "showContentView startContentSlidingOn");
        startContentSlidingOn(mLayoutContaint);
    }

    private void makeSubContentViewInvisible(View subContentViewToVisible) {
        if (subContentViews == null) return;
        for (View subContent : subContentViews) {
            subContent.setVisibility(View.GONE);
        }
        subContentViewToVisible.setVisibility(View.VISIBLE);
    }

    private void makeAllSubContentViewInvisible() {
        if (subContentViews == null) return;
        for (View subContent : subContentViews) {
            subContent.setVisibility(View.GONE);
        }
    }

    private void showDialogGGPlayService() {
        ClickListener.IconListener listener = new ClickListener.IconListener() {
            @Override
            public void onIconClickListener(View view, Object entry, int menuId) {
                NavigateActivityHelper.navigateToPlayStore(mChatActivity, Constants.PACKET_NAME.GG_PLAY_SERVICE);
            }
        };
        LocationHelper.getInstant(mApplication).showDialogGGPlayService(mChatActivity, listener);
    }

    public boolean isRefreshMediaView() {
        return refreshMediaView;
    }

    public void setRefreshMediaView(boolean refreshMediaView) {
        this.refreshMediaView = refreshMediaView;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public ChatFooterView getChatFooterView() {
        return chatFooterView;
    }
}
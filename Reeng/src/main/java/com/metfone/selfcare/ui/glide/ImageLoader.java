package com.metfone.selfcare.ui.glide;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;

/**
 * Created by HaiKE on 14/07/16.
 */
public class ImageLoader {

    private static final String TAG = ImageLoader.class.getSimpleName();
    private static final int FADE_TIME = 500;

    public static void setImage(Context context, String url, final ImageView imageView) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .dontAnimate())
//                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                .placeholder(context.getResources().getDrawable(R.color.bg_onmedia_content_item))
//                .error(context.getResources().getDrawable(R.color.bg_onmedia_content_item))
//                .dontAnimate()
//                .crossFade(FADE_TIME)
                .into(imageView);
    }

    public static void setNewsImage(Context context, String url, final ImageView imageView) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .dontAnimate()
                        .placeholder(context.getResources().getDrawable(R.drawable.video1))
                        .error(context.getResources().getDrawable(R.drawable.video1)))
                .into(imageView);

    }
    public static void setNewsImageCache(Context context, String url, final ImageView imageView) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .placeholder(context.getResources().getDrawable(R.drawable.video1))
                        .error(context.getResources().getDrawable(R.drawable.video1)))
                .into(imageView);

    }

    public static void setNewImageGif(Context context, String url, ImageView imageView) {
        Glide.with(context)
                .load(url)
                .into(imageView);
    }

    public static void setImageBanner(Context context, String url, final ImageView imageView) {
        if (!((BaseSlidingFragmentActivity) context).isFinishing()) {
            Glide.with(context)
                    .load(url)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .dontAnimate()
                            .placeholder(context.getResources().getDrawable(R.color.bg_onmedia_content_item))
                            .error(context.getResources().getDrawable(R.color.bg_onmedia_content_item)))
                    .into(imageView);
        }
    }
}

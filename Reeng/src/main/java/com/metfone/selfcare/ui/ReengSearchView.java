package com.metfone.selfcare.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.blankj.utilcode.util.KeyboardUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 7/18/14.
 */
public class ReengSearchView extends AppCompatEditText implements TextWatcher {
    private Drawable drawableRight;
    private Drawable drawableLeft;
    private Drawable drawableTop;
    private Drawable drawableBottom;
    private int actionX, actionY;
    private Drawable drawableSearch;
    private Drawable drawableClear;

    private DrawableClickListener clickListener;

    public ReengSearchView(Context context) {
        super(context);
        init(context, null);
        addTextChangedListener(this);
    }

    public ReengSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        addTextChangedListener(this);
    }

    public ReengSearchView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
        addTextChangedListener(this);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ReengSearchView, 0, 0);
            try {
                drawableSearch = a.getDrawable(R.styleable.ReengSearchView_drawableSearch);
                drawableClear = a.getDrawable(R.styleable.ReengSearchView_drawableClear);
                setCompoundDrawablesWithIntrinsicBounds(drawableSearch, null, null, null);
            } catch (Exception e) {
                Log.e("ReengSearchView", "Exception", e);
            } finally {
                a.recycle();
            }
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top,
                                     Drawable right, Drawable bottom) {
        if (left != null) {
            drawableLeft = left;
        }
        if (right != null) {
            drawableRight = right;
        }
        if (top != null) {
            drawableTop = top;
        }
        if (bottom != null) {
            drawableBottom = bottom;
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Rect bounds;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionX = (int) event.getX();
            actionY = (int) event.getY();
            if (drawableBottom != null && drawableBottom.getBounds().contains(actionX, actionY)) {
                if (clickListener != null)
                    clickListener.onClick(DrawableClickListener.DrawablePosition.BOTTOM);
                return super.onTouchEvent(event);
            }
            if (drawableTop != null && drawableTop.getBounds().contains(actionX, actionY)) {
                if (clickListener != null)
                    clickListener.onClick(DrawableClickListener.DrawablePosition.TOP);
                return super.onTouchEvent(event);
            }

            // this works for left since container shares 0,0 origin with bounds
            if (drawableLeft != null) {
                bounds = drawableLeft.getBounds();
                int x, y;
                float density = getResources().getDisplayMetrics().density;
                int extraTapArea = (int) (13 * density + 0.5);
                x = actionX;
                y = actionY;

                if (!bounds.contains(actionX, actionY)) {
                    /** Gives the +20 area for tapping. */
                    x = (actionX - extraTapArea);
                    y = (actionY - extraTapArea);

                    if (x <= 0)
                        x = actionX;
                    if (y <= 0)
                        y = actionY;

                    /** Creates square from the smallest value */
                    if (x < y) {
                        y = x;
                    }
                }
                if (bounds.contains(x, y) && clickListener != null) {
                    clickListener.onClick(DrawableClickListener.DrawablePosition.LEFT);
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    return true;
                }
            }

            if (drawableRight != null) {
                bounds = drawableRight.getBounds();
                int x, y;
                int extraTapArea = 13;

                /**
                 * IF USER CLICKS JUST OUT SIDE THE RECTANGLE OF THE DRAWABLE
                 * THAN ADD X AND SUBTRACT THE Y WITH SOME VALUE SO THAT AFTER
                 * CALCULATING X AND Y CO-ORDINATE LIES INTO THE DRAWBABLE
                 * BOUND. - this process help to increase the tappable area of
                 * the rectangle.
                 */
                x = (actionX + extraTapArea);
                y = (actionY - extraTapArea);
                x = getWidth() - x;
                if (x <= 0) {
                    x += extraTapArea;
                }
                if (y <= 0)
                    y = actionY;
                //
                if (bounds.contains(x, y)) {
                    if (clickListener != null)
                        clickListener.onClick(DrawableClickListener.DrawablePosition.RIGHT);
                    setText("");
                    //setCompoundDrawablesWithIntrinsicBounds(drawableSearch, null, null, null);
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    return true;
                }
                return super.onTouchEvent(event);
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void finalize() throws Throwable {
        drawableRight = null;
        drawableBottom = null;
        drawableLeft = null;
        drawableTop = null;
        drawableSearch = null;
        drawableClear = null;
        super.finalize();
    }

    public void setDrawableClickListener(DrawableClickListener listener) {
        this.clickListener = listener;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && clickListener != null) {
            clickListener.onBackClick(keyCode);
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(s)) {
            setCompoundDrawablesWithIntrinsicBounds(drawableSearch, null, null, null);
        } else {
            setCompoundDrawablesWithIntrinsicBounds(drawableSearch, null, drawableClear, null);
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    public interface DrawableClickListener {

        enum DrawablePosition {
            TOP, BOTTOM, LEFT, RIGHT
        }

        void onClick(DrawablePosition target);

        void onBackClick(int keyCode);
    }
}
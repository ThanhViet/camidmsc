package com.metfone.selfcare.ui.tabvideo.holder;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ashokvarma.bottomnavigation.utils.Utils;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageLoader;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoListener;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class VideoNormalHolder extends BaseAdapter.ViewHolder {

    private static boolean pause = false;

    /**
     * khôi phục lại các tiền trình tải ảnh
     */
    public static void resumeRequests() {
        pause = false;
    }

    /**
     * dừng tiến trình load ảnh
     */
    public static void pauseRequests() {
        pause = true;
    }

    @BindView(R.id.ivPlay)
    ImageView ivPlay;
    @BindView(R.id.ivWatchLater)
    ImageView ivWatchLater;
    @BindView(R.id.ivThumbnail)
    ImageView ivThumbnail;
    @BindView(R.id.ivVideo)
    ImageView ivVideo;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivHear)
    ImageView ivHear;
    @BindView(R.id.tvNumberHear)
    TextView tvNumberHear;
    @BindView(R.id.llControllerHear)
    LinearLayout llControllerHear;
    @BindView(R.id.ivComment)
    ImageView ivComment;
    @BindView(R.id.tvNumberComment)
    TextView tvNumberComment;
    @BindView(R.id.llControllerComment)
    LinearLayout llControllerComment;
    @BindView(R.id.ivShare)
    ImageView ivShare;
    @BindView(R.id.tvNumberShare)
    TextView tvNumberShare;
    @BindView(R.id.llControllerShare)
    LinearLayout llControllerShare;
    @BindView(R.id.ivChannel)
    ImageView ivChannel;
    @BindView(R.id.tvChannel)
    TextView tvChannel;
    @BindView(R.id.itemVideoRoot)
    LinearLayout itemVideoRoot;
    @BindView(R.id.llControllerChannel)
    LinearLayout llControllerChannel;
    @BindView(R.id.tvView)
    TextView tvView;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvDuration)
    TextView tvDuration;

    private Activity activity;

    private OnVideoNormalListener onVideoNormalListener;

    private ImageLoader videoImageLoader;
    private ImageLoader channelImageLoader;
    private VideoObject videoObject;
    private Video video;
    private Channel channel;

    public VideoNormalHolder(@NonNull LayoutInflater layoutInflater, @NonNull ViewGroup parent, OnVideoNormalListener onVideoNormalListener, Activity activity) {
        super(layoutInflater.inflate(R.layout.item_tab_video_info_video, parent, false));
        ButterKnife.bind(this, itemView);

        this.activity = activity;
        this.onVideoNormalListener = onVideoNormalListener;

        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) ivVideo.getLayoutParams();
        layoutParams.width = ScreenManager.getWidth(activity) - (Utilities.dpToPx(15) * 2);
        layoutParams.height = layoutParams.width * 9 / 16;
        ivVideo.setLayoutParams(layoutParams);
        ivThumbnail.setLayoutParams(layoutParams);

        ivWatchLater.setVisibility(View.GONE);
        ivPlay.setVisibility(View.GONE);
    }

    public void bindData(VideoObject itemVideoObject) {
        videoObject = itemVideoObject;
        videoImageLoader = videoObject.getVideoLoader();
        channelImageLoader = videoObject.getChannelLoader();
        video = videoObject.getVideo();
        channel = video.getChannel();

        bindVideo();
        bindChannel();
    }

    private void bindChannel() {
        tvChannel.setText(channel.getName());
        bindImageChannel();
    }

    public void bindImageChannel() {
        if (channelImageLoader != null)
            if (pause) {
                if (channelImageLoader.isCompleted())
                    channelImageLoader.into(ivChannel);
                else
                    ivChannel.setImageResource(channelImageLoader.getThumb());
            } else {
                channelImageLoader.into(ivChannel);
            }
    }

    private void bindVideo() {
        tvTitle.setText(video.getTitle());
        tvView.setText(String.format(video.getTotalView() <= 1 ?
                ApplicationController.self().getString(R.string.view) :
                ApplicationController.self().getString(R.string.video_views), Utilities.getTotalView(video.getTotalView())));
        if (TextUtils.isEmpty(video.getDuration())) {
            tvDuration.setVisibility(View.GONE);
        } else {
            tvDuration.setVisibility(View.VISIBLE);
            tvDuration.setText(video.getDuration());
        }
        if (video.getPublishTime() > 0) {
            if (tvTime != null) {
                tvTime.setVisibility(View.VISIBLE);
                tvTime.setText(DateTimeUtils.calculateTime(activity.getResources(), video.getPublishTime()));
            }
        } else {
            if (tvTime != null) tvTime.setVisibility(View.GONE);
        }

        bindHear();
        bindShare();
        bindComment();
        bindImageVideo();
    }

    public void bindImageVideo() {
        if (videoImageLoader != null && ivVideo != null)
            if (pause) {
                if (videoImageLoader.isCompleted()) {
                    videoImageLoader.into(ivVideo);
                } else {
                    ivVideo.setImageResource(videoImageLoader.getThumb());
                }
            } else {
                videoImageLoader.into(ivVideo);
            }
    }

    public void bindComment() {
        if (tvNumberComment != null && videoObject != null)
            tvNumberComment.setText(videoObject.getTextComment());
    }

    public void bindShare() {
        if (tvNumberShare != null && videoObject != null)
            tvNumberShare.setText(videoObject.getTextShare());
    }

    public void bindHear() {
        if (video != null && ivHear != null)
            if (video.isLike())
                ivHear.setImageResource(R.drawable.ic_video_item_video_hear_press);
            else
                ivHear.setImageResource(R.drawable.ic_favior__new);
        if (tvNumberHear != null && videoObject != null)
            tvNumberHear.setText(videoObject.getTextLike());
    }

    @OnClick({
            R.id.llControllerHear,
            R.id.llControllerComment,
            R.id.llControllerShare,
            R.id.llControllerChannel,
            R.id.itemVideoRoot,
            R.id.iv_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llControllerHear:
                video.setLike(!video.isLike());
                video.setTotalLike(video.isLike() ? video.getTotalLike() + 1 : video.getTotalLike() - 1);
                bindHear();
                if (onVideoNormalListener != null)
                    onVideoNormalListener.onHear(video);
                break;
            case R.id.llControllerComment:
                if (onVideoNormalListener != null)
                    onVideoNormalListener.onComment(video);
                break;
            case R.id.llControllerShare:
                if (onVideoNormalListener != null)
                    onVideoNormalListener.onShare(video);
                break;
            case R.id.llControllerChannel:
                if (onVideoNormalListener != null)
                    onVideoNormalListener.onClick(channel);
                break;
            case R.id.itemVideoRoot:
                if (onVideoNormalListener != null)
                    onVideoNormalListener.onClick(video);
                break;
            case R.id.iv_more:
                if (onVideoNormalListener != null)
                    onVideoNormalListener.onMore(video);
                break;
        }
    }

    public interface OnVideoNormalListener extends BaseAdapter.OnItemListener, OnVideoListener, OnChannelListener {

    }

    public static class VideoObject implements BaseAdapter.Clone {

        private Video video = null;

        private String textLike = "";
        private String textComment = "";
        private String textShare = "";
        private String textView = "";

        private ImageLoader videoLoader;
        private ImageLoader channelLoader;

        private int thumbnail;

        public Video getVideo() {
            return video;
        }

        public void setVideo(Video video) {
            this.video = video;
        }

        public String getTextLike() {
            return textLike;
        }

        void setTextLike(String textLike) {
            this.textLike = textLike;
        }

        public String getTextComment() {
            return textComment;
        }

        void setTextComment(String textComment) {
            this.textComment = textComment;
        }

        public String getTextShare() {
            return textShare;
        }

        void setTextShare(String textShare) {
            this.textShare = textShare;
        }

        public String getTextView() {
            return textView;
        }

        public void setTextView(String textView) {
            this.textView = textView;
        }

        ImageLoader getVideoLoader() {
            return videoLoader;
        }

        void setVideoLoader(ImageLoader videoLoader) {
            this.videoLoader = videoLoader;
        }

        ImageLoader getChannelLoader() {
            return channelLoader;
        }

        void setChannelLoader(ImageLoader channelLoader) {
            this.channelLoader = channelLoader;
        }

        public int getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(int thumbnail) {
            this.thumbnail = thumbnail;
        }

        @Override
        public VideoObject clone() {
            try {
                VideoObject videoObject = new VideoObject();
                Video temVideo;
                if (video == null) {
                    temVideo = new Video();
                } else {
                    temVideo = video;
                }
                videoObject.video = temVideo.clone();
                videoObject.textLike = textLike;
                videoObject.textComment = textComment;
                videoObject.textShare = textShare;
                videoObject.textView = textView;
                videoObject.videoLoader = videoLoader;
                videoObject.channelLoader = channelLoader;
                videoObject.thumbnail = thumbnail;
                return videoObject;
            } catch (Exception e) {
                return this;
            }
        }

        @Override
        public String toString() {
            return "VideoObject{" +
                    "video=" + video +
                    ", textLike='" + textLike + '\'' +
                    ", textComment='" + textComment + '\'' +
                    ", textShare='" + textShare + '\'' +
                    ", textView='" + textView + '\'' +
                    '}';
        }

        public static BaseAdapter.ItemObject provideItemObject(Video video, int position, Activity activity) {
            BaseAdapter.ItemObject item = new BaseAdapter.ItemObject();
            item.setId(video.getId());
            item.setInfo(provideVideoObject(video, position, activity));
            return item;
        }

        @SuppressWarnings("deprecation")
        static VideoNormalHolder.VideoObject provideVideoObject(Video video, int position, Activity activity) {
            int thumb = ImageManager.with().build().provideThumbError(position % ImageManager.thumbs.length - 1);

            ImageLoader videoLoader = ImageManager.with()
                    .setUrl(video.getImagePath())
                    .setWithCrossFade(true)
                    .setSize(ScreenManager.getWidth(activity), ScreenManager.getWidth(activity) * 9 / 16)
                    .setTransformations(new RoundedCornersTransformation(Utils.dp2px(activity, 11), 0))
                    .setThumbError(thumb)
                    .build()
                    .provideImageLoader();

            ImageLoader channelLoader = ImageManager.with()
                    .setUrl(video.getChannel().getUrlImage())
                    .setSize(ScreenManager.getWidth(activity) / 4, ScreenManager.getWidth(activity) / 4)
                    .setTransformations(new CenterCrop())
                    .setThumbError(thumb)
                    .build()
                    .provideImageLoader();

            VideoNormalHolder.VideoObject videoObject = new VideoNormalHolder.VideoObject();
            videoObject.setVideo(video);
            videoObject.setTextShare(Utilities.shortenLongNumber(video.getTotalShare()));
            videoObject.setTextLike(Utilities.shortenLongNumber(video.getTotalLike()));
            videoObject.setTextComment(Utilities.shortenLongNumber(video.getTotalComment()));
            videoObject.setThumbnail(thumb);
            videoObject.setVideoLoader(videoLoader);
            videoObject.setChannelLoader(channelLoader);

            return videoObject;
        }
    }
}
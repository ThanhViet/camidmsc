package com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail;

import com.metfone.selfcare.model.tab_video.Video;

public interface OnItemVideoRelatedListener {
    void onItemVideoRelatedClick(Video video);
}

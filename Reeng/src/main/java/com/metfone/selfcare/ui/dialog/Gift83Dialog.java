package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;

/**
 * Created by tuanha00 on 3/5/2018.
 */

public class Gift83Dialog extends Dialog implements View.OnClickListener {

    private static final String TAG = Gift83Dialog.class.getSimpleName();

    public static Gift83Dialog show(BaseSlidingFragmentActivity context, OnGift83Listener listener, String content) {
        Gift83Dialog gift83Dialog = new Gift83Dialog(context, listener, content);
        gift83Dialog.setCanceledOnTouchOutside(true);
        gift83Dialog.show();
        return gift83Dialog;
    }

    private ImageView mImgLogo;
    private TextView mTvwContent;
    private TextView btnAccess;
    private TextView btnShare;
    private RelativeLayout reRoot;
    private View mViewParent;

    private BaseSlidingFragmentActivity mContext;
    private String contentGift;

    private OnGift83Listener listener;

    private Gift83Dialog(@NonNull BaseSlidingFragmentActivity context, OnGift83Listener listener, String content) {
        super(context);
        this.listener = listener;
        this.mContext = context;
        this.contentGift = content;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_gift_83);
        setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        if (this.getWindow() != null) layoutParams.copyFrom(this.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(layoutParams);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mImgLogo = findViewById(R.id.img_logo);
        btnAccess = findViewById(R.id.btnAccess);
        btnShare = findViewById(R.id.btnShare);
        reRoot = findViewById(R.id.reRoot);
        mViewParent = findViewById(R.id.view_root);
        mTvwContent = findViewById(R.id.tvw_content_gift);
        mTvwContent.setText(contentGift);

        btnAccess.setOnClickListener(this);
        btnShare.setOnClickListener(this);
//        reRoot.setOnClickListener(this);
        setCancelable(false);

    }

    private Bitmap getBitmapFromView() {
        mImgLogo.setVisibility(View.VISIBLE);
        btnAccess.setVisibility(View.GONE);
        btnShare.setVisibility(View.GONE);
        mViewParent.setBackgroundColor(ContextCompat.getColor(mContext, R.color.bg_onmedia_content_item));
        Bitmap scaledBitmap = Bitmap.createBitmap(mViewParent.getWidth(), mViewParent.getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(scaledBitmap);
        mViewParent.draw(canvas);
        return scaledBitmap;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnAccess) {
            if (listener != null) listener.onBtnAccessGift83Click();
        } else if (view.getId() == R.id.btnShare) {
            if (listener != null) {
                listener.onBtnShareGift83Click(getBitmapFromView());
            }
        } else if (view.getId() == R.id.reRoot) {
        }
        dismiss();
    }

    public interface OnGift83Listener {
        void onBtnAccessGift83Click();

        void onBtnShareGift83Click(Bitmap bitmapShare);
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/24
 *
 */

package com.metfone.selfcare.ui.tabvideo.holder;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.image.ImageLoader;
import com.metfone.selfcare.database.datasource.ChannelDataSource;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapter;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopMoneyUploadHeaderHolder extends BaseAdapter.ViewHolder {

    private static boolean pause = false;

    @BindView(R.id.avatar_left)
    CircleImageView ivAvatarLeft;
    @BindView(R.id.tv_name_left)
    TextView tvNameLeft;
    @BindView(R.id.tv_score_left)
    TextView tvScoreLeft;
    @BindView(R.id.layout_left)
    View viewLeft;

    @BindView(R.id.avatar_center)
    CircleImageView ivAvatarCenter;
    @BindView(R.id.tv_name_center)
    TextView tvNameCenter;
    @BindView(R.id.tv_score_center)
    TextView tvScoreCenter;
    @BindView(R.id.layout_center)
    View viewCenter;

    @BindView(R.id.avatar_right)
    CircleImageView ivAvatarRight;
    @BindView(R.id.tv_name_right)
    TextView tvNameRight;
    @BindView(R.id.tv_score_right)
    TextView tvScoreRight;
    @BindView(R.id.layout_right)
    View viewRight;

    private TopMoneyUploadHolder.OnItemChannelNormalListener onItemChannelListener;
    private ArrayList<TopMoneyUploadHolder.ChannelObject> listChannels;

    public TopMoneyUploadHeaderHolder(@NonNull LayoutInflater layoutInflater, @NonNull ViewGroup parent, TopMoneyUploadHolder.OnItemChannelNormalListener onItemChannelListener) {
        super(layoutInflater.inflate(R.layout.holder_top_money_upload_header, parent, false));
        ButterKnife.bind(this, itemView);
        this.onItemChannelListener = onItemChannelListener;
        listChannels = new ArrayList<>();
    }

    /**
     * khôi phục lại các tiền trình tải ảnh
     */
    public static void resumeRequests() {
        pause = false;
    }

    /**
     * dừng tiến trình load ảnh
     */
    public static void pauseRequests() {
        pause = true;
    }

    private void bindAvatar(ImageLoader imageLoader, ImageView ivAvatar) {
        if (imageLoader != null && ivAvatar != null)
            if (pause) {
                if (imageLoader.isCompleted())
                    imageLoader.into(ivAvatar);
                else
                    ivAvatar.setImageResource(imageLoader.getThumb());
            } else {
                imageLoader.into(ivAvatar);
            }
    }

    public void bindAvatar() {
        for (int i = 0; i < listChannels.size(); i++) {
            TopMoneyUploadHolder.ChannelObject item = listChannels.get(i);
            if (i == 0) {
                bindAvatar(item.getChannelLoader(), ivAvatarCenter);
            } else if (i == 1) {
                bindAvatar(item.getChannelLoader(), ivAvatarLeft);
            } else if (i == 2) {
                bindAvatar(item.getChannelLoader(), ivAvatarRight);
            }
        }
    }

    public void bindData(List<BaseAdapter.ItemObject> list) {
        listChannels.clear();
        if (Utilities.notEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                BaseAdapter.ItemObject itemObject = list.get(i);
                if (itemObject != null && itemObject.getInfo() instanceof TopMoneyUploadHolder.ChannelObject) {
                    TopMoneyUploadHolder.ChannelObject item = (TopMoneyUploadHolder.ChannelObject) itemObject.getInfo();
                    listChannels.add(item);
                }
            }
        }
        for (int i = 0; i < listChannels.size(); i++) {
            TopMoneyUploadHolder.ChannelObject item = listChannels.get(i);
            if (i == 0) {
                bindData(item, viewCenter, ivAvatarCenter, tvNameCenter, tvScoreCenter);
            } else if (i == 1) {
                bindData(item, viewLeft, ivAvatarLeft, tvNameLeft, tvScoreLeft);
            } else if (i == 2) {
                bindData(item, viewRight, ivAvatarRight, tvNameRight, tvScoreRight);
            }
        }
    }

    private void bindData(TopMoneyUploadHolder.ChannelObject channelObject, View rootView, ImageView ivAvatar, TextView tvName, TextView tvScore) {
        if (channelObject != null) {
            final Channel channel = channelObject.getChannel();
            if (channel != null) {
                if (tvName != null) tvName.setText(channel.getName());
                if (tvScore != null) tvScore.setText(channelObject.getTextTotalPoint());
                bindAvatar(channelObject.getChannelLoader(), ivAvatar);
                if (rootView != null) rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemChannelListener != null) {
                            ChannelDataSource.getInstance().saveTimeNewChannel(channel);
                            onItemChannelListener.onClick(channel);
                        }
                    }
                });
            }
        }
    }
}

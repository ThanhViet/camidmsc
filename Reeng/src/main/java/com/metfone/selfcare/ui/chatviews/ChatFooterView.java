package com.metfone.selfcare.ui.chatviews;

import android.Manifest;
import android.graphics.Color;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChangePhoneNumberActivity;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.ui.CusKeyboardController;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 5/27/2015.
 */
public class ChatFooterView implements TextWatcher, View.OnClickListener {
    public static final int FUNCTION_EMO = 1;
    public static final int FUNCTION_KEYBOARD = 2;
    public static final int FUNCTION_CAMERA = 3;
    public static final int FUNCTION_SEND_IMAGE = 4;
    public static final int FUNCTION_SEND_VOICE = 5;
    public static final int FUNCTION_MORE_OPTION = 7;
    public static final int FUNCTION_EDIT_TEXT = 8;
    public static final int FUNCTION_SEND_CONTACT = 9;
    public static final int FUNCTION_SEND_LOCATION = 10;
    public static final int FUNCTION_OFFICIAL_ACTION = 11;
    public static final int FUNCTION_SEND_FILE = 12;
    public static final int FUNCTION_SEND_OPTION_ESPORT = 13;
    public static final int STATUS_SEND_ALL_GONE = -1;
    public static final int STATUS_SEND_ALL_VISIBLE = 0;
    public static final int STATUS_SEND_REENG_VISIBLE = 1;
    public static final int STATUS_SEND_SMS_VISIBLE = 2;
    public static final int STATUS_CLOSE = 14;
    private static final String TAG = ChatFooterView.class.getSimpleName();

    private View mViewTopFooter, mViewRightCam, bottomChatOptionAnchor; //layout top & bottom of footer
    private ImageButton mBtnSendSms, mBtnSendText,
            mBtnKeyboardRight, mBtnKeyboardLeft,
            mBtnOfficialAction, mBtnSendTextV2, mBtnSendSmsV2;
    private ImageView mBtnRightCamera, mBtnRightImage,mBtnMicro,mBtnListFunction,mBtnMore;
    private MultiLineEdittextTag mEdtTextInput;
    private KeyboardControllerCallback mKeyboardControllerCallback;
    private BaseSlidingFragmentActivity mActivity;
    private ChatActivity mParentActivity;
    private boolean mGsmMode = false;
    private int inType;
    private ApplicationController mApp;
    private int threadType, threadId;
    private boolean isStranger = false;
    private boolean isViettel;
    private View mLayoutSendButtonV2;
    private boolean isSwitchAnimationRunning = false;
    private boolean checkIcon = false;
    private ChatFooterView() {
    }

    public ChatFooterView(BaseSlidingFragmentActivity activity, View parentLayout,
                          KeyboardControllerCallback callback,
                          int threadType, int threadId, boolean isViettel) {
        mActivity = activity;
        this.threadType = threadType;
        this.threadId = threadId;
        this.isViettel = isViettel;
        mApp = (ApplicationController) mActivity.getApplication();
        mViewTopFooter = parentLayout.findViewById(R.id.person_chat_detail_footer_tool);
        bottomChatOptionAnchor = parentLayout.findViewById(R.id.bottom_chat_option_anchor);
        mBtnMore =  parentLayout.findViewById(R.id.person_chat_emoticons_btn);
        mEdtTextInput = (MultiLineEdittextTag) parentLayout.findViewById(R.id.person_chat_detail_input_text);
        mBtnSendSms = (ImageButton) parentLayout.findViewById(R.id.person_chat_detail_send_sms_text);
        mBtnSendSmsV2 = (ImageButton) parentLayout.findViewById(R.id.person_chat_detail_send_sms_text_v2);
        mBtnSendText = (ImageButton) parentLayout.findViewById(R.id.person_chat_detail_send_reeng_text);
        mBtnSendTextV2 = (ImageButton) parentLayout.findViewById(R.id.person_chat_detail_send_reeng_text_v2);
        mBtnKeyboardRight = (ImageButton) parentLayout.findViewById(R.id.person_chat_keyboard);
        mBtnListFunction =  parentLayout.findViewById(R.id.person_chat_list_function);
        mBtnOfficialAction = (ImageButton) parentLayout.findViewById(R.id.person_chat_official_action);
        mBtnKeyboardLeft = (ImageButton) parentLayout.findViewById(R.id.person_chat_keyboard_roomchat);
        mViewRightCam = parentLayout.findViewById(R.id.right_image_chat_bar);
        mBtnRightCamera =  parentLayout.findViewById(R.id.right_camera);
        mBtnRightImage =  parentLayout.findViewById(R.id.right_image);
        mLayoutSendButtonV2 = parentLayout.findViewById(R.id.right_chat_bar_layout);
        mBtnMicro = parentLayout.findViewById(R.id.right_micro);
        /*if (mApp.getReengAccountBusiness().isCambodia()) {
            mBtnRightImage.setImageResource(R.drawable.ic_bottom_chat_voice);
        }*/
        mKeyboardControllerCallback = callback;
        inType = mEdtTextInput.getInputType(); // backup the input type
        setViewListeners();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            mBtnListFunction.setVisibility(View.GONE);
            mBtnOfficialAction.setVisibility(View.VISIBLE);
            mViewRightCam.setVisibility(View.VISIBLE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mBtnOfficialAction.setVisibility(View.GONE);
            mBtnListFunction.setVisibility(View.GONE);
            mViewRightCam.setVisibility(View.GONE);
        } else {
            mViewRightCam.setVisibility(View.VISIBLE);
            drawIconListFunction();
        }
    }

    public void setStranger(boolean stranger) {
        isStranger = stranger;
    }

    public void setViettel(boolean viettel) {
        isViettel = viettel;
    }

    private void setViewListeners() {
        mBtnMore.setOnClickListener(this);
        mBtnSendSms.setOnClickListener(this);
        mBtnSendText.setOnClickListener(this);
        mBtnListFunction.setOnClickListener(this);
        mBtnKeyboardRight.setOnClickListener(this);
        mBtnKeyboardLeft.setOnClickListener(this);
        mBtnOfficialAction.setOnClickListener(this);
        mBtnRightCamera.setOnClickListener(this);
        mBtnRightImage.setOnClickListener(this);
        mBtnMicro.setOnClickListener(this);
        setSendMessageButtonAction();
        setEditTextListener();
    }

    @Override
    public void onClick(View v) {
        mKeyboardControllerCallback.onChatBarClick();
        switch (v.getId()) {
            case R.id.person_chat_emoticons_btn:
                handleSelectEmoticon();
                mBtnRightCamera.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnRightImage.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnMore.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                mBtnMicro.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnListFunction.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                break;
            case R.id.person_chat_detail_send_sms_text:
            case R.id.person_chat_detail_send_sms_text_v2:
                handleSendText();
                break;
            case R.id.person_chat_detail_send_reeng_text:
            case R.id.person_chat_detail_send_reeng_text_v2:
                handleSendText();
                break;
            case R.id.person_chat_official_action:
                resetStateOption();
                handleClickOfficialAction();
                mKeyboardControllerCallback.showFooterView(FUNCTION_OFFICIAL_ACTION);
                break;
            case R.id.person_chat_keyboard_roomchat:
                manualShowSoftKeyboard();
                mKeyboardControllerCallback.showFooterView(FUNCTION_EDIT_TEXT);
                mBtnKeyboardLeft.setVisibility(View.GONE);
                mBtnMore.setVisibility(View.VISIBLE);
                break;
            case R.id.person_chat_keyboard:
                Log.d(TAG, "setButtonKeyboardRightListener");
                manualShowSoftKeyboard();
                mKeyboardControllerCallback.showFooterView(FUNCTION_KEYBOARD);
                mBtnKeyboardRight.setVisibility(View.GONE);
                if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
                    mBtnListFunction.setVisibility(View.GONE);
                    mBtnOfficialAction.setVisibility(View.VISIBLE);
                } else {
                    drawIconListFunction();
                }
                break;
            case R.id.right_camera:
                mBtnRightCamera.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                mBtnRightImage.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnMore.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnMicro.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnListFunction.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                resetStateOption();
                handleSelectCamera();
                break;
            case R.id.right_micro:
                mBtnRightCamera.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnRightImage.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnMore.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnMicro.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                mBtnListFunction.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                resetStateOption();
                openVoiceMail();
                break;
            case R.id.right_image:
                resetStateOption();
                bottomChatOptionAnchor.getLayoutParams().height = 0;
                bottomChatOptionAnchor.requestLayout();
                mBtnRightCamera.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnRightImage.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                mBtnMore.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnMicro.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnListFunction.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));

                /*if (mApp.getReengAccountBusiness().isCambodia()) {
                    openVoiceMail();
                } else*/
                handleSelectImage();
                break;
            case R.id.person_chat_list_function:
                mKeyboardControllerCallback.hideMediaView();
                mKeyboardControllerCallback.showFooterView(STATUS_CLOSE);
                mBtnRightCamera.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnRightImage.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnMore.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnMicro.setColorFilter(mActivity.getResources().getColor(R.color.color_bg_btn_default));
                mBtnListFunction.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                if (!checkIcon) {
                    handleSelectListFunction();
                } else {
                    mBtnListFunction.setImageResource(R.drawable.selector_more_iconnew);
                    mBtnRightImage.setVisibility(View.VISIBLE);
                    mBtnRightCamera.setVisibility(View.VISIBLE);
                    mBtnOfficialAction.setVisibility(View.GONE);
                    checkIcon = false;
                }
                break;
        }
    }

    private void resetStateOption() {
        mBtnMore.setSelected(false);
        mBtnRightImage.setSelected(false);
    }

    private void handleSelectEmoticon() {
        mBtnRightImage.setSelected(false);
        mBtnMore.setSelected(true);
        showFooterMore();
    }

    private void handleSendText() {
        mKeyboardControllerCallback.sendTextClickCallback();
        mKeyboardControllerCallback.showSoftKeyboardIfCustomKeyboardIsOpening();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mBtnKeyboardLeft.setVisibility(View.GONE);
            mBtnMore.setVisibility(View.VISIBLE);
        }
    }

    private void handleSelectCamera() {
        mKeyboardControllerCallback.showFooterView(FUNCTION_CAMERA);
    }

    private void handleSelectImage() {
        if (PermissionHelper.declinedPermission(mApp, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            PermissionHelper.requestPermissionWithGuide(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE, Constants.PERMISSION.PERMISSION_REQUEST_GALLERY);
        } else {
            InputMethodUtils.hideSoftKeyboard(mEdtTextInput, mActivity);
//            mKeyboardControllerCallback.showFooterView(FUNCTION_SEND_IMAGE);
            selectedButtonPreviewMedia();
        }
    }

    private void setEditTextListener() {
        mEdtTextInput.addTextChangedListener(this);
        mEdtTextInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "mEdtTextInput.setOnClickListener");
                //hideMoreOptionAndShowKeyboard();
                mKeyboardControllerCallback.showFooterView(FUNCTION_EDIT_TEXT);
                mBtnRightCamera.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                mBtnRightImage.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                mBtnMore.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                mBtnMicro.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                mBtnListFunction.setColorFilter(mActivity.getResources().getColor(R.color.bg_mocha));
                if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                    mBtnKeyboardLeft.setVisibility(View.GONE);
                    mBtnMore.setVisibility(View.VISIBLE);
                }
            }
        });
        mEdtTextInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocused) {
                Log.d(TAG, "mEdtTextInput.setOnFocusChangeListener: " + isFocused);
                if (isFocused) {
                    mKeyboardControllerCallback.showFooterView(FUNCTION_EDIT_TEXT);
                }
            }
        });
    }

    public void resetFooterView() {
        showFooterMore();
    }
    private void updateColorIcon(boolean isChange){
        if(isChange){
            mBtnRightCamera.setColorFilter(mActivity.getResources().getColor(R.color.gray));
            mBtnRightImage.setColorFilter(Color.GRAY);
            mBtnMore.setColorFilter(Color.GRAY);
            mBtnRightCamera.setColorFilter(Color.GRAY);
            mBtnMicro.setColorFilter(Color.GRAY);
        }
        else {
            mBtnRightCamera.clearColorFilter();
            mBtnRightImage.clearColorFilter();
            mBtnMore.clearColorFilter();
            mBtnRightCamera.clearColorFilter();
            mBtnMicro.clearColorFilter();
        }

    }
    private void drawIconListFunction() {
        checkIcon = false;
        mBtnKeyboardRight.setVisibility(View.GONE);
        mBtnOfficialAction.setVisibility(View.GONE);
        mBtnListFunction.setVisibility(View.VISIBLE);
        mBtnRightCamera.setVisibility(View.VISIBLE);
        mBtnRightImage.setVisibility(View.VISIBLE);
        if (PopupHelper.getInstance().isShowBottomChatOption()) {
            mBtnListFunction.setImageResource(R.drawable.selector_more_iconnew);
        } else {
            mBtnListFunction.setImageResource(R.drawable.selector_more_iconnew);
        }
    }

    private void handleSelectListFunction() {
        showChatMoreOption();
//        CusKeyboardController cusKeyboardController = (CusKeyboardController) mKeyboardControllerCallback;
//        int optionHeight = 0;
//        if(cusKeyboardController != null && cusKeyboardController.isOpened()) {
//            optionHeight = mKeyboardControllerCallback.getOptionContentHeight();
//        }
//        //mBtnListFunction.setImageResource(R.drawable.ic_list_function_close);
//        PopupHelper.getInstance().showOrHideBottomChatOption(mActivity, bottomChatOptionAnchor, threadType, threadId,
//                isStranger, optionHeight, isViettel, new PositiveListener<ItemContextMenu>() {
//                    @Override
//                    public void onPositive(ItemContextMenu result) {
//                        switch (result.getActionTag()) {
//                            case BottomChatOption.Constants.OPTION_LOCATION:
//                                resetStateOption();
//                                mKeyboardControllerCallback.showFooterView(FUNCTION_SEND_LOCATION);
//                                break;
//                            case BottomChatOption.Constants.OPTION_VOICE_MAIL:
//                                openVoiceMail();
//                                break;
//                            case BottomChatOption.Constants.OPTION_CONTACT:
//                                resetStateOption();
//                                mKeyboardControllerCallback.showFooterView(FUNCTION_SEND_CONTACT);
//                                break;
//                            case BottomChatOption.Constants.OPTION_FILE:
//                                resetStateOption();
//                                mKeyboardControllerCallback.showFooterView(FUNCTION_SEND_FILE);
//                                break;
//                            case BottomChatOption.Constants.OPTION_MUSIC:
//                                actionOptionMusicVideo((String) result.getObj());
//                                break;
//                            case BottomChatOption.Constants.OPTION_VIDEO:
//                                actionOptionMusicVideo((String) result.getObj());
//                                break;
//                            case BottomChatOption.Constants.OPTION_ESPORT:
//                                resetStateOption();
//                                mKeyboardControllerCallback.showFooterView(FUNCTION_SEND_OPTION_ESPORT);
//                                break;
//                            case BottomChatOption.Constants.OPTION_SEND_IMAGE_CAMBODIA:
//                                resetStateOption();
//                                mBtnKeyboardRight.postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        manualHideSoftKeyboard();
//                                    }
//                                }, 80);
//                                handleSelectImage();
//                                break;
//                            default:
//                                mKeyboardControllerCallback.onBottomActionClick(result.getActionTag());
//                                break;
//                        }
//                    }
//                }, new DismissListener() {
//                    @Override
//                    public void onDismiss() {
//                        Log.d(TAG, "onDismiss");
//                        mBtnListFunction.setImageResource(R.drawable.selector_more_iconnew);
//                        updateColorIcon(false);
//                        bottomChatOptionAnchor.getLayoutParams().height = 0;
//                        bottomChatOptionAnchor.requestLayout();
//                    }
//                });
    }

    private void actionOptionMusicVideo(String actionKey) {
        showEditText();
        mEdtTextInput.setText(actionKey);
        mEdtTextInput.setSelection(actionKey.length());
        showSoftKeyboard();
    }

    private void openVoiceMail() {
        resetStateOption();
        mBtnListFunction.setVisibility(View.VISIBLE);
        mBtnKeyboardRight.setVisibility(View.VISIBLE);
        mKeyboardControllerCallback.showFooterView(FUNCTION_SEND_VOICE);
        mBtnKeyboardRight.postDelayed(new Runnable() {
            @Override
            public void run() {
                manualHideSoftKeyboard();
            }
        }, 80);
    }

    public void handleClickOfficialAction() {
        manualHideSoftKeyboard();
        mBtnOfficialAction.setVisibility(View.GONE);
        mBtnKeyboardRight.setVisibility(View.VISIBLE);
    }

    private void manualHideSoftKeyboard() {
        InputMethodUtils.hideSoftKeyboard(mEdtTextInput, mActivity);
        mKeyboardControllerCallback.onManualHideSoftKeyboard();
    }

    public void manualShowSoftKeyboard() {
        Log.d(TAG, "manualShowSoftKeyboard");
        showEditText();
        mEdtTextInput.postDelayed(new Runnable() {
            @Override
            public void run() {
                // mReengSearchView.setSelection(0);
                mEdtTextInput.clearFocus();
                mEdtTextInput.requestFocus();
                InputMethodUtils.showSoftKeyboard(mActivity, mEdtTextInput);
            }
        }, 100);
    }

    public void selectedButtonPreviewMedia() {
        mBtnRightImage.setSelected(true);
        mBtnMore.setSelected(false);
        mKeyboardControllerCallback.showFooterView(FUNCTION_SEND_IMAGE);
    }

    public void hideMoreOptionAndShowKeyboard() {
        //show top footer & hide bottom footer
        manualShowSoftKeyboard();
        mKeyboardControllerCallback.onButtonArrowPress();
    }

    private void showFooterMore() {
        manualHideSoftKeyboard();
        mKeyboardControllerCallback.showFooterView(FUNCTION_EMO);
    }

    // su dung thang nay thay cho BottomChatOption
    private void showChatMoreOption(){
        manualHideSoftKeyboard();
        mKeyboardControllerCallback.showFooterView(FUNCTION_MORE_OPTION);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        updateStateVoiceMailBtn();
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    private void updateStateVoiceMailBtn() {
        PopupHelper.getInstance().hideBottomChatOption();
        String str = mEdtTextInput.getText().toString().trim();
        boolean hasContent = true;
        if (TextUtils.isEmpty(str)) {
            hasContent = false;
        }
        if (hasContent) {
            mViewRightCam.setVisibility(View.GONE);
            if (mGsmMode) {
                setSendSmsBtnActive(true);
            } else {
                setSendReengBtnActive(true);
            }
        } else {
            setVoicemailBtnActive();
            if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                mViewRightCam.setVisibility(View.GONE);
            } else {
                mViewRightCam.setVisibility(View.VISIBLE);
            }
        }
    }

    public void setSendSmsBtnActive(boolean enable) {
        mBtnOfficialAction.setVisibility(View.GONE);
        mBtnListFunction.setVisibility(View.GONE);
        mBtnKeyboardRight.setVisibility(View.GONE);
        if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON) {
            mBtnSendText.setVisibility(View.GONE);
            mBtnSendSms.setVisibility(View.VISIBLE);
            //
            mBtnSendSms.setEnabled(enable);
        } else {
            mLayoutSendButtonV2.setVisibility(View.VISIBLE);
        }
    }

    private void setSendReengBtnActive(boolean enable) {
        checkIcon = true;
        mBtnOfficialAction.setVisibility(View.GONE);
        mBtnListFunction.setImageResource(R.drawable.chat_arrow_right_24px);
        mBtnKeyboardRight.setVisibility(View.GONE);
        mBtnRightImage.setVisibility(View.GONE);
        mBtnRightCamera.setVisibility(View.GONE);
        if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON) {
            mBtnSendText.setVisibility(View.VISIBLE);
            mBtnSendSms.setVisibility(View.GONE);
            //
            mBtnSendText.setEnabled(enable);
        } else {
            mLayoutSendButtonV2.setVisibility(View.VISIBLE);
        }
    }

    private void setVoicemailBtnActive() {
        checkIcon = false;
        mBtnKeyboardRight.setVisibility(View.GONE);
        mBtnSendText.setVisibility(View.GONE);
        mBtnSendSms.setVisibility(View.GONE);
        mLayoutSendButtonV2.setVisibility(View.GONE);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            mBtnListFunction.setVisibility(View.GONE);
            mBtnOfficialAction.setVisibility(View.VISIBLE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mBtnListFunction.setVisibility(View.GONE);
            mBtnOfficialAction.setVisibility(View.GONE);
            if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON) {
                mBtnSendText.setVisibility(View.VISIBLE);
            } else {
                mLayoutSendButtonV2.setVisibility(View.VISIBLE);
            }
        } else {
            mBtnListFunction.setImageResource(R.drawable.selector_more_iconnew);
            mBtnRightImage.setVisibility(View.VISIBLE);
            mBtnRightCamera.setVisibility(View.VISIBLE);
            mBtnOfficialAction.setVisibility(View.GONE);
        }
    }

    public void setGsmMode(boolean gsmMode, int threadType) {
        Log.i(TAG, "setGsmMode gsmMode = " + gsmMode);
        this.mGsmMode = gsmMode;
        updateStateVoiceMailBtn();
    }

    public void showTopFooter() {
        mViewTopFooter.setVisibility(View.VISIBLE);
        updateStateVoiceMailBtn();
        resetStateOption();
    }

    public void showEditText() {
        Log.d(TAG, "showEditText");
        mViewTopFooter.setVisibility(View.VISIBLE);
        resetStateOption();
        //manualShowSoftKeyboard();
    }

    public void showSoftKeyboard() {
        manualShowSoftKeyboard();
    }

    public void onSystemKeyboardShown() {
        Log.i(TAG, "onSystemKeyboardShown");
        if (TextUtils.isEmpty(mEdtTextInput.getText().toString().trim())) {
            //neu o nhap text rong thi show voicemail, hide btn keyboard
            mBtnKeyboardRight.setVisibility(View.GONE);
            mBtnSendSms.setVisibility(View.GONE);
            mBtnSendText.setVisibility(View.GONE);
            mLayoutSendButtonV2.setVisibility(View.GONE);
            if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
                mBtnListFunction.setVisibility(View.GONE);
                mBtnOfficialAction.setVisibility(View.VISIBLE);
            } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                mBtnListFunction.setVisibility(View.GONE);
                mBtnOfficialAction.setVisibility(View.GONE);
                if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON) {
                    mBtnSendText.setVisibility(View.VISIBLE);
                } else {
                    mLayoutSendButtonV2.setVisibility(View.VISIBLE);
                }
            } else {
                drawIconListFunction();
            }
        } else {
            //neu o nhap text ko rong thi show nut send
            mBtnListFunction.setVisibility(View.VISIBLE);
            mBtnOfficialAction.setVisibility(View.GONE);
            mBtnKeyboardRight.setVisibility(View.GONE);
            if (mGsmMode) {
                if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON) {
                    mBtnSendSms.setVisibility(View.VISIBLE);
                    mBtnSendText.setVisibility(View.GONE);
                } else {
                    mLayoutSendButtonV2.setVisibility(View.VISIBLE);
                }
            } else {
                if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON) {
                    mBtnSendSms.setVisibility(View.GONE);
                    mBtnSendText.setVisibility(View.VISIBLE);
                } else {
                    mLayoutSendButtonV2.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void onResume() {
    }

    public void onPause() {
        resetStateOption();
        if (mEdtTextInput != null) {
            mEdtTextInput.clearFocus();
        }
    }

    public interface KeyboardControllerCallback {
        void onBottomActionClick(int actionId);

        void showFooterView(int functionId);

        void sendTextClickCallback();

        void onManualHideSoftKeyboard();

        void showSoftKeyboardIfCustomKeyboardIsOpening();

        void onButtonArrowPress();

        int getOptionContentHeight();

        void onChatBarClick();

        void hideMediaView();

        void onSwitchButtonComplete(boolean isSendSms);
    }

    public MultiLineEdittextTag getEdtTextInput() {
        return mEdtTextInput;
    }

    public void reSetDefaultTypeInputChat() {
        mEdtTextInput.setInputType(inType); // restore input type
    }

    public ImageView getBtnMore() {
        return mBtnMore;
    }


    private void setSendMessageButtonAction() {
        if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON) return;
        if (mBtnSendSmsV2 == null || mBtnSendTextV2 == null) return;

        mBtnSendSmsV2.setEnabled(false);
        mBtnSendSmsV2.setAlpha(0.3f);
        mBtnSendTextV2.setEnabled(true);
        mBtnSendTextV2.setAlpha(1.0f);

        mBtnSendTextV2.setOnClickListener(this);
        mBtnSendSmsV2.setOnClickListener(this);

        mBtnSendSmsV2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mBtnSendTextV2.getVisibility() != View.VISIBLE) return true;
                swapSendButtonPosition();
                mBtnSendSmsV2.setEnabled(false);
                mBtnSendSmsV2.setAlpha(0.3f);
                mBtnSendTextV2.setEnabled(true);
                mBtnSendTextV2.setAlpha(1.0f);
                return true;
            }
        });

        mBtnSendTextV2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mBtnSendSmsV2.getVisibility() != View.VISIBLE) return true;
                swapSendButtonPosition();
                mBtnSendTextV2.setEnabled(false);
                mBtnSendTextV2.setAlpha(0.3f);
                mBtnSendSmsV2.setEnabled(true);
                mBtnSendSmsV2.setAlpha(1.0f);
                return true;
            }
        });
    }

    private void swapSendButtonPosition() {
        if (isSwitchAnimationRunning) return;
        if (mBtnSendSmsV2 == null || mBtnSendTextV2 == null) return;
        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) mBtnSendTextV2.getLayoutParams();
        final int leftMargin1 = params1.leftMargin;

        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) mBtnSendSmsV2.getLayoutParams();
        final int leftMargin2 = params2.leftMargin;

        Animation animation1 = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                super.applyTransformation(interpolatedTime, t);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBtnSendTextV2.getLayoutParams();
                if (leftMargin2 > leftMargin1) {
                    params.leftMargin = (int) ((leftMargin2 - leftMargin1) * interpolatedTime + leftMargin1);
                } else {
                    params.leftMargin = (int) (leftMargin1 - (leftMargin1 - leftMargin2) * interpolatedTime);
                }
                mBtnSendTextV2.setLayoutParams(params);
            }
        };

        Animation animation2 = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                super.applyTransformation(interpolatedTime, t);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBtnSendSmsV2.getLayoutParams();
                params.leftMargin = (int) (leftMargin1 * interpolatedTime);

                if (leftMargin1 > leftMargin2) {
                    params.leftMargin = (int) ((leftMargin1 - leftMargin2) * interpolatedTime + leftMargin2);
                } else {
                    params.leftMargin = (int) (leftMargin2 - (leftMargin2 - leftMargin1) * interpolatedTime);
                }
                mBtnSendSmsV2.setLayoutParams(params);
            }
        };

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(animation1);
        animationSet.addAnimation(animation2);
        animationSet.setDuration(400);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isSwitchAnimationRunning = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isSwitchAnimationRunning = false;
                if (mKeyboardControllerCallback != null && mBtnSendSmsV2 != null && mBtnSendTextV2 != null) {
                    boolean isSms = mBtnSendSmsV2.getLeft() < mBtnSendTextV2.getLeft();
                    mKeyboardControllerCallback.onSwitchButtonComplete(isSms);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if (mLayoutSendButtonV2 != null) {
            mLayoutSendButtonV2.startAnimation(animationSet);
        }
    }

    public void setSendBtnDefault(boolean isSms) {
        // để xét xem nút nào hiện mặc định, chỉ gọi khi có hai nút cùng được hiện
        if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON || mBtnSendTextV2 == null || mBtnSendSmsV2 == null)
            return;
        if (isSms) {
            swapButton(mBtnSendTextV2, mBtnSendSmsV2);
        } else {
            swapButton(mBtnSendSmsV2, mBtnSendTextV2);
        }
    }

    public void setupSendBtnAvailable(int status) {
        // should call as soon as ChatFooterView is created.
        if (!Config.Features.FLAG_SUPPORT_SWITCH_SEND_BUTTON || mBtnSendSmsV2 == null || mBtnSendTextV2 == null)
            return;
        int visible = mLayoutSendButtonV2.getVisibility();
        switch (status) {
            case STATUS_SEND_ALL_GONE:
                //cả hai nút cùng hiện và có thể switch qua lại
                mBtnSendTextV2.setVisibility(View.GONE);
                mBtnSendSmsV2.setVisibility(View.GONE);
                mLayoutSendButtonV2.setVisibility(View.GONE);
                break;

            case STATUS_SEND_ALL_VISIBLE:
                //cả hai nút cùng hiện và có thể switch qua lại
                mLayoutSendButtonV2.setVisibility(View.VISIBLE);
                mBtnSendTextV2.setVisibility(View.VISIBLE);
                mBtnSendSmsV2.setVisibility(View.VISIBLE);
                refreshSendBtnLayout();
                break;

            case STATUS_SEND_REENG_VISIBLE:
                // nút sms sẽ ẩn
                swapButton(mBtnSendSmsV2, mBtnSendTextV2);
                mLayoutSendButtonV2.setVisibility(View.VISIBLE);
                mBtnSendTextV2.setVisibility(View.VISIBLE);
                mBtnSendSmsV2.setVisibility(View.GONE);
                refreshSendBtnLayout();
                break;

            case STATUS_SEND_SMS_VISIBLE:
                //nút sms hiện, nút send message ẩn
                swapButton(mBtnSendTextV2, mBtnSendSmsV2);
                mLayoutSendButtonV2.setVisibility(View.VISIBLE);
                mBtnSendTextV2.setVisibility(View.GONE);
                mBtnSendSmsV2.setVisibility(View.VISIBLE);
                refreshSendBtnLayout();
                break;

            default:
                mLayoutSendButtonV2.setVisibility(View.VISIBLE);
                mBtnSendTextV2.setVisibility(View.VISIBLE);
                mBtnSendSmsV2.setVisibility(View.VISIBLE);
                refreshSendBtnLayout();
                break;
        }
        mLayoutSendButtonV2.setVisibility(visible);
    }

    private void swapButton(ImageButton firstBtn, ImageButton secondBtn) {
        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) firstBtn.getLayoutParams();
        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) secondBtn.getLayoutParams();
        if (params1 == null || params2 == null) {
            return;
        }

        if (params1.leftMargin > params2.leftMargin) {
            secondBtn.setEnabled(true);
            secondBtn.setAlpha(1.0f);

            firstBtn.setAlpha(0.3f);
            firstBtn.setEnabled(false);
            return;
        }

        final int leftMargin1 = params1.leftMargin;
        final int leftMargin2 = params2.leftMargin;

        params1.leftMargin = leftMargin2;
        params2.leftMargin = leftMargin1;

        firstBtn.setLayoutParams(params1);
        secondBtn.setLayoutParams(params2);

        secondBtn.setEnabled(true);
        secondBtn.setAlpha(1.0f);

        firstBtn.setAlpha(0.3f);
        firstBtn.setEnabled(false);
    }

    private void refreshSendBtnLayout() {
        if (mBtnSendSmsV2 == null || mBtnSendTextV2 == null || mLayoutSendButtonV2 == null) {
            return;
        }
        int btnVisibleCount = 0;
        if (mBtnSendTextV2.getVisibility() == View.VISIBLE) btnVisibleCount++;
        if (mBtnSendSmsV2.getVisibility() == View.VISIBLE) btnVisibleCount++;
        if (btnVisibleCount == 0) return;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mLayoutSendButtonV2.getLayoutParams();
        int rightMargin = (int) mActivity.getResources().getDimension(R.dimen.margin_right_send_layout);
        layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, rightMargin * (2 / btnVisibleCount), layoutParams.bottomMargin);
        mLayoutSendButtonV2.setLayoutParams(layoutParams);
        mLayoutSendButtonV2.requestLayout();
    }
}
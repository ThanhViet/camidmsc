package com.metfone.selfcare.ui.tabvideo.playVideo.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.dialog.BottomSheetMenuAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.recyclerview.DividerMenuItemDecoration;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

public class OptionsVideoDialog extends BottomSheetDialog {

    private static final String TAG = OptionsVideoDialog.class.getSimpleName();

    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private ArrayList<ItemContextMenu> listItem;
    private BottomSheetListener mCallBack;
    private RecyclerView mRecyclerView;
    private TextView mTitle;
    private BottomSheetMenuAdapter mAdapter;
    private boolean isShowTitle = true;

    private View bottomSheet;

    public OptionsVideoDialog(@NonNull BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity, R.style.style_bottom_sheet_dialog_v2);
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplicationContext();
        setCancelable(isCancelable);
    }

    public OptionsVideoDialog setListItem(ArrayList<ItemContextMenu> items) {
        this.listItem = items;
        return this;
    }

    public OptionsVideoDialog setHasTitle(boolean isShowTitle) {
        this.isShowTitle = isShowTitle;
        return this;
    }

    public OptionsVideoDialog setListener(BottomSheetListener listener) {
        this.mCallBack = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_video_options);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        mRecyclerView = findViewById(R.id.bottom_sheet_recycler_view);
        mTitle = findViewById(R.id.tv_title);
        if (mTitle != null) {
            mTitle.setVisibility(isShowTitle ? View.VISIBLE : View.GONE);
        }
        mAdapter = new BottomSheetMenuAdapter(listItem);
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (mCallBack != null) {
                    ItemContextMenu item = (ItemContextMenu) object;
                    mCallBack.onItemClick(item.getActionTag(), item.getObj());
                }
                dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
        mRecyclerView.addItemDecoration(new DividerMenuItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);

        bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                if (bottomSheetBehavior == null) return;
                bottomSheetBehavior.setPeekHeight(1000);
            }
        });
    }
}
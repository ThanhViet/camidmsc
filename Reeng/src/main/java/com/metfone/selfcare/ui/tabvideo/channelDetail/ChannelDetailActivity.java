package com.metfone.selfcare.ui.tabvideo.channelDetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.ui.tabvideo.channelDetail.main.ChannelDetailFragment;
import com.metfone.selfcare.ui.tabvideo.channelDetail.statisticalDetail.StatisticalDetailFragment;
import com.metfone.selfcare.ui.tabvideo.channelDetail.statisticalSearch.StatisticalSearchFragment;

public class ChannelDetailActivity extends BaseActivity {

    private static final String CHANNEL = "channel";

    public static void start(Context context, Channel channel) {
        if (context != null && channel != null) {
            Intent intent = new Intent(context, ChannelDetailActivity.class);
            intent.putExtra(CHANNEL, channel);
            context.startActivity(intent);
        }
    }

    private Channel mChannel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_detail_v2);
        mChannel = (Channel) getIntent().getSerializableExtra(CHANNEL);
        addMain();
    }

    public void addMain() {
        getSupportFragmentManager().beginTransaction().add(R.id.frame_content, ChannelDetailFragment.newInstance(mChannel)).commitAllowingStateLoss();
    }

    public void addStatisticalDetail() {
        getSupportFragmentManager().beginTransaction().add(R.id.frame_content, StatisticalDetailFragment.newInstance(mChannel)).addToBackStack("").commitAllowingStateLoss();
    }

    public void addStatisticalSearch() {
        getSupportFragmentManager().beginTransaction().add(R.id.frame_content, StatisticalSearchFragment.newInstance(mChannel)).addToBackStack("").commitAllowingStateLoss();
    }
}

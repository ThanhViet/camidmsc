package com.metfone.selfcare.ui.tooltip;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class ToolTipRelativeLayout extends RelativeLayout {

    public static final String ACTION_BAR_TITLE = "action_bar_title";
    public static final String ID = "id";
    public static final String ANDROID = "android";
    public static final String ACTION_BAR = "action_bar";
    public static final String ACTION_MENU_VIEW = "ActionMenuView";
    public static final String OVERFLOW_MENU_BUTTON = "OverflowMenuButton";

    public ToolTipRelativeLayout(final Context context) {
        super(context);
    }

    public ToolTipRelativeLayout(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public ToolTipRelativeLayout(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    public StickerSuggestView showToolTipForView(final StickerSuggestTip stickerSuggestTip, final View view) {
        final StickerSuggestView toolTipView = new StickerSuggestView(getContext());
        toolTipView.setToolTip(stickerSuggestTip, view);
        addView(toolTipView);
        return toolTipView;
    }

    public StickerSuggestView showToolTipForViewResId(final Activity activity, final StickerSuggestTip stickerSuggestTip, final int resId) {
        final StickerSuggestView toolTipView = new StickerSuggestView(getContext());
        final View decorView = activity.getWindow().getDecorView();
        final View view = decorView.findViewById(resId);

        if (view == null) {
            throw new TooltipException("View not found for this resource id. Are you sure it exists?");
        }

        toolTipView.setToolTip(stickerSuggestTip, view);
        addView(toolTipView);
        return toolTipView;
    }

    @TargetApi(11)
    public StickerSuggestView showToolTipForActionBarHome(final Activity activity, final StickerSuggestTip stickerSuggestTip) {
        final int homeResId = android.R.id.home;
        return showToolTipForViewResId(activity, stickerSuggestTip, homeResId);
    }

    @TargetApi(11)
    public StickerSuggestView showToolTipForActionBarTitle(final Activity activity, final StickerSuggestTip stickerSuggestTip) {
        final int titleResId = Resources.getSystem().getIdentifier(ACTION_BAR_TITLE, ID, ANDROID);
        if (titleResId == 0) {
            throw new TooltipException("No title View found. Are you sure it exists?");
        }
        return showToolTipForViewResId(activity, stickerSuggestTip, titleResId);
    }

    @TargetApi(11)
    public StickerSuggestView showToolTipForActionBarOverflowMenu(final Activity activity, final StickerSuggestTip stickerSuggestTip) {
        return showToolTipForView(stickerSuggestTip, findActionBarOverflowMenuView(activity));
    }

    @TargetApi(11)
    private static View findActionBarOverflowMenuView(final Activity activity) {
        final ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();

        final int actionBarViewResId = Resources.getSystem().getIdentifier(ACTION_BAR, ID, ANDROID);
        final ViewGroup actionBarView = (ViewGroup) decorView.findViewById(actionBarViewResId);

        ViewGroup actionMenuView = null;
        int actionBarViewChildCount = actionBarView.getChildCount();
        for (int i = 0; i < actionBarViewChildCount; ++i) {
            if (actionBarView.getChildAt(i).getClass().getSimpleName().equals(ACTION_MENU_VIEW)) {
                actionMenuView = (ViewGroup) actionBarView.getChildAt(i);
            }
        }

        if (actionMenuView == null) {
            throw new TooltipException("No overflow menu found. Are you sure the overflow menu button is visible? Check the docs for showToolTipForActionBarOverflowMenu(Activity, StickerSuggestTip) again!");
        }

        int actionMenuChildCount = actionMenuView.getChildCount();
        View overflowMenuButton = null;
        for (int i = 0; i < actionMenuChildCount; ++i) {
            if (actionMenuView.getChildAt(i).getClass().getSimpleName().equals(OVERFLOW_MENU_BUTTON)) {
                overflowMenuButton = actionMenuView.getChildAt(i);
            }
        }

        if (overflowMenuButton == null) {
            throw new TooltipException("No overflow menu found. Are you sure the overflow menu button is visible? Check the docs for showToolTipForActionBarOverflowMenu(Activity, StickerSuggestTip) again!");
        }

        return overflowMenuButton;
    }
}
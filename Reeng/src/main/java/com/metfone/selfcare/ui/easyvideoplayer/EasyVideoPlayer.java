package com.metfone.selfcare.ui.easyvideoplayer;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.CheckResult;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.FloatRange;
import androidx.annotation.IntDef;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.DonutProgress;
import com.metfone.selfcare.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Aidan Follestad (afollestad)
 */
public class EasyVideoPlayer extends FrameLayout
        implements IUserMethods,
        TextureView.SurfaceTextureListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnVideoSizeChangedListener,
        MediaPlayer.OnErrorListener,
        View.OnClickListener,
        SeekBar.OnSeekBarChangeListener {

    private static final String TAG = EasyVideoPlayer.class.getSimpleName();

    private long TIME_SHOW_CONTROL = 3000;

    @IntDef({LEFT_ACTION_NONE, LEFT_ACTION_RESTART, LEFT_ACTION_RETRY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LeftAction {

    }

    @IntDef({RIGHT_ACTION_NONE, RIGHT_ACTION_SUBMIT, RIGHT_ACTION_CUSTOM_LABEL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RightAction {

    }

    public static final int LEFT_ACTION_NONE = 0;
    public static final int LEFT_ACTION_RESTART = 1;
    public static final int LEFT_ACTION_RETRY = 2;
    public static final int RIGHT_ACTION_NONE = 3;
    public static final int RIGHT_ACTION_SUBMIT = 4;
    public static final int RIGHT_ACTION_CUSTOM_LABEL = 5;
    private static final int UPDATE_INTERVAL = 100;

    public EasyVideoPlayer(Context context) {
        super(context);
        init(context, null);
    }

    public EasyVideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public EasyVideoPlayer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private TextureView mTextureView;
    private Surface mSurface;

    private View mControlsFrame;
    public View mProgressFrame;
    private View mToolbarFrame;
    private View mClickFrame;
    private View mCenterControl;

    private SeekBar mSeeker;
    private TextView mLabelPosition;
    private TextView mLabelDuration;
    //    private ImageButton mBtnRestart;
    //    private TextView mBtnRetry;
    private ImageView mBtnPlayPause;
    private ImageView mImgToggleScreen;
    private ImageView mImgBack;
    private ImageView mImgMoreOption;
    private TextView mTvwTitle;
    private View mViewNextVideo;
    private TextView mTvwCancelNextVideo;
    private TextView mTvwNextVideo, mTvNextVideoLabel;
    private DonutProgress mProgressNextVideo;
    private AnimatorSet animatorSet;
    private RelativeLayout mImgReplay;
    private ImageView mImgNext;
    private ImageView mImgPrevious;
    public View mWaitingFrame;
    private View mBgNextVideo;
    /*private TextView mBtnSubmit;
    private TextView mLabelCustom;
    private TextView mLabelBottom;*/

    private MediaPlayer mPlayer;
    private boolean mSurfaceAvailable;
    private boolean mIsPrepared;
    private boolean mWasPlaying;
    private int mInitialTextureWidth;
    private int mInitialTextureHeight;

    private Handler mHandler;

    private Uri mSource;
    private EasyVideoCallback mCallback;
    private EasyVideoProgressCallback mProgressCallback;
    @LeftAction
    private int mLeftAction = LEFT_ACTION_RESTART;
    @RightAction
    private int mRightAction = RIGHT_ACTION_NONE;
    //    private CharSequence mRetryText;
//    private CharSequence mSubmitText;
//    private Drawable mRestartDrawable;
    private Drawable mPlayDrawable;
    private Drawable mPauseDrawable;
    /*private CharSequence mCustomLabelText;
    private CharSequence mBottomLabelText;*/
    private boolean mHideControlsOnPlay = true;
    private boolean mAutoPlay;
    private int mInitialPosition = -1;
    private boolean mControlsDisabled;
    private int mThemeColor = 0;
    private boolean mAutoFullscreen = false;
    private boolean mLoop = false;
    private boolean isFullSreen = false;
    //    private int mPrevBuffer = 0;
//    private int mPercent = 0;
//    private int mDuration = 0;
    private boolean mCanNext = true;
    public boolean mHaveLink = false;
    public boolean mIsShowNext = false;

    private boolean isStop = false;


    // Runnable used to run code on an interval to update counters and seeker
    private final Runnable mUpdateCounters =
            new Runnable() {
                @Override
                public void run() {
                    if (mHandler == null || !mIsPrepared || mSeeker == null || mPlayer == null)
                        return;
                    int pos = mPlayer.getCurrentPosition();
                    final int dur = mPlayer.getDuration();
                    if (pos > dur) pos = dur;
                    mLabelPosition.setText(Util.getDurationString(pos, false));
//                    mLabelDuration.setText(Util.getDurationString(dur - pos, true));
                    mSeeker.setProgress(pos);
                    mSeeker.setMax(dur);


                    if (mProgressCallback != null)
                        mProgressCallback.onVideoProgressUpdate(pos, dur);
                    mHandler.postDelayed(this, UPDATE_INTERVAL);
                }
            };

    private final Runnable mAutoHideControl = new Runnable() {
        @Override
        public void run() {
            if (null != mSeeker && isPlaying())
                hideControls();
        }
    };

    private void init(Context context, AttributeSet attrs) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setBackgroundColor(Color.BLACK);
        mHandler = new Handler();
        if (attrs != null) {
            TypedArray a =
                    context.getTheme().obtainStyledAttributes(attrs, R.styleable.EasyVideoPlayer, 0, 0);
            try {
                String source = a.getString(R.styleable.EasyVideoPlayer_evp_source);
                if (source != null && !source.trim().isEmpty()) mSource = Uri.parse(source);

                //noinspection WrongConstant
                mLeftAction = a.getInteger(R.styleable.EasyVideoPlayer_evp_leftAction, LEFT_ACTION_RESTART);
                //noinspection WrongConstant
                mRightAction = a.getInteger(R.styleable.EasyVideoPlayer_evp_rightAction, RIGHT_ACTION_NONE);

//                mCustomLabelText = a.getText(R.styleable.EasyVideoPlayer_evp_customLabelText);
                /*mRetryText = a.getText(R.styleable.EasyVideoPlayer_evp_retryText);
                mSubmitText = a.getText(R.styleable.EasyVideoPlayer_evp_submitText);*/
//                mBottomLabelText = a.getText(R.styleable.EasyVideoPlayer_evp_bottomText);

                /*int restartDrawableResId =
                        a.getResourceId(R.styleable.EasyVideoPlayer_evp_restartDrawable, -1);*/
                int playDrawableResId = -1;//a.getResourceId(R.styleable.EasyVideoPlayer_evp_playDrawable, -1);
                int pauseDrawableResId = -1;//a.getResourceId(R.styleable.EasyVideoPlayer_evp_pauseDrawable, -1);

                /*if (restartDrawableResId != -1) {
                    mRestartDrawable = AppCompatResources.getDrawable(context, restartDrawableResId);
                }*/
                if (playDrawableResId != -1) {
                    mPlayDrawable = AppCompatResources.getDrawable(context, playDrawableResId);
                }
                if (pauseDrawableResId != -1) {
                    mPauseDrawable = AppCompatResources.getDrawable(context, pauseDrawableResId);
                }

                mHideControlsOnPlay =
                        a.getBoolean(R.styleable.EasyVideoPlayer_evp_hideControlsOnPlay, true);
                mAutoPlay = a.getBoolean(R.styleable.EasyVideoPlayer_evp_autoPlay, false);
                mControlsDisabled = a.getBoolean(R.styleable.EasyVideoPlayer_evp_disableControls, false);

                mThemeColor =
                        a.getColor(
                                R.styleable.EasyVideoPlayer_evp_themeColor,
                                Util.resolveColor(context, R.attr.colorPrimary));

                mAutoFullscreen = a.getBoolean(R.styleable.EasyVideoPlayer_evp_autoFullscreen, false);
                mLoop = a.getBoolean(R.styleable.EasyVideoPlayer_evp_loop, false);
            } finally {
                a.recycle();
            }
        } else {
            mLeftAction = LEFT_ACTION_RESTART;
            mRightAction = RIGHT_ACTION_NONE;
            mHideControlsOnPlay = true;
            mAutoPlay = false;
            mControlsDisabled = false;
            mThemeColor = Util.resolveColor(context, R.attr.colorPrimary);
            mAutoFullscreen = false;
            mLoop = false;
        }

        /*if (mRetryText == null) mRetryText = "RETRY";
        if (mSubmitText == null) mSubmitText = "SUMMIT";*/

        /*if (mRestartDrawable == null)
            mRestartDrawable = AppCompatResources.getDrawable(context, R.drawable.ic_player_pause_white);*/
        if (mPlayDrawable == null)
            mPlayDrawable = AppCompatResources.getDrawable(context, R.drawable.ic_play_sieuhai);
        if (mPauseDrawable == null)
            mPauseDrawable = AppCompatResources.getDrawable(context, R.drawable.ic_pause_sieuhai);
    }

    @Override
    public void setSource(@NonNull Uri source) {
        boolean hadSource = mSource != null;
        if (hadSource) stop();
        mSource = source;
        if (mPlayer != null) {
            if (hadSource) {
                sourceChanged();
            } else {
                prepare();
            }
        }
    }

    @Override
    public void setCallback(@NonNull EasyVideoCallback callback) {
        mCallback = callback;
    }

    @Override
    public void setProgressCallback(@NonNull EasyVideoProgressCallback callback) {
        mProgressCallback = callback;
    }

    @Override
    public void setLeftAction(@LeftAction int action) {
        if (action < LEFT_ACTION_NONE || action > LEFT_ACTION_RETRY)
            throw new IllegalArgumentException("Invalid left action specified.");
        mLeftAction = action;
        invalidateActions();
    }

    @Override
    public void setRightAction(@RightAction int action) {
        if (action < RIGHT_ACTION_NONE || action > RIGHT_ACTION_CUSTOM_LABEL)
            throw new IllegalArgumentException("Invalid right action specified.");
        mRightAction = action;
        invalidateActions();
    }

    @Override
    public void setCustomLabelText(@Nullable CharSequence text) {
//        mCustomLabelText = text;
//        mLabelCustom.setText(text);
        setRightAction(RIGHT_ACTION_CUSTOM_LABEL);
    }

    @Override
    public void setCustomLabelTextRes(@StringRes int textRes) {
        setCustomLabelText(getResources().getText(textRes));
    }

    @Override
    public void setBottomLabelText(@Nullable CharSequence text) {
        /*mBottomLabelText = text;
        mLabelBottom.setText(text);
        if (text == null || text.toString().trim().length() == 0) mLabelBottom.setVisibility(View.GONE);
        else mLabelBottom.setVisibility(View.VISIBLE);*/
    }

    @Override
    public void setBottomLabelTextRes(@StringRes int textRes) {
        setBottomLabelText(getResources().getText(textRes));
    }

    @Override
    public void setRetryText(@Nullable CharSequence text) {
        /*mRetryText = text;
        mBtnRetry.setText(text);*/
    }

    @Override
    public void setRetryTextRes(@StringRes int res) {
        setRetryText(getResources().getText(res));
    }

    @Override
    public void setSubmitText(@Nullable CharSequence text) {
        /*mSubmitText = text;
        mBtnSubmit.setText(text);*/
    }

    @Override
    public void setSubmitTextRes(@StringRes int res) {
        setSubmitText(getResources().getText(res));
    }

    @Override
    public void setRestartDrawable(@NonNull Drawable drawable) {
        /*mRestartDrawable = drawable;
        mBtnRestart.setImageDrawable(drawable);*/
    }

    @Override
    public void setRestartDrawableRes(@DrawableRes int res) {
        setRestartDrawable(AppCompatResources.getDrawable(getContext(), res));
    }

    @Override
    public void setPlayDrawable(@NonNull Drawable drawable) {
        mPlayDrawable = drawable;
        if (!isPlaying()) mBtnPlayPause.setImageDrawable(drawable);
    }

    @Override
    public void setPlayDrawableRes(@DrawableRes int res) {
        setPlayDrawable(AppCompatResources.getDrawable(getContext(), res));
    }

    @Override
    public void setPauseDrawable(@NonNull Drawable drawable) {
        mPauseDrawable = drawable;
        if (isPlaying()) mBtnPlayPause.setImageDrawable(drawable);
    }

    @Override
    public void setPauseDrawableRes(@DrawableRes int res) {
        setPauseDrawable(AppCompatResources.getDrawable(getContext(), res));
    }

    @Override
    public void setThemeColor(@ColorInt int color) {
        mThemeColor = color;
        invalidateThemeColors();
    }

    @Override
    public void setThemeColorRes(@ColorRes int colorRes) {
        setThemeColor(ContextCompat.getColor(getContext(), colorRes));
    }

    @Override
    public void setHideControlsOnPlay(boolean hide) {
        mHideControlsOnPlay = hide;
    }

    @Override
    public void setAutoPlay(boolean autoPlay) {
        mAutoPlay = autoPlay;
    }

    @Override
    public void setInitialPosition(@IntRange(from = 0, to = Integer.MAX_VALUE) int pos) {
        mInitialPosition = pos;
    }

    private void sourceChanged() {
        setControlsEnabled(false);
        mSeeker.setProgress(0);
        mSeeker.setEnabled(false);
        mPlayer.reset();
        if (mCallback != null) mCallback.onPreparing(this);
        try {
            setSourceInternal();
        } catch (Exception e) {
            throwError(e);
        }
    }

    private void setSourceInternal() throws Exception {
        if (mSource.getScheme() != null
                && ("http".equals(mSource.getScheme()) || "https".equals(mSource.getScheme()))) {
            LOG("Loading web URI: " + mSource.toString());
            mPlayer.setDataSource(mSource.toString());
        } else if (mSource.getScheme() != null
                && ("file".equals(mSource.getScheme()) && mSource.getPath().contains("/android_assets/"))) {
            LOG("Loading assets URI: " + mSource.toString());
            AssetFileDescriptor afd;
            afd =
                    getContext()
                            .getAssets()
                            .openFd(mSource.toString().replace("file:///android_assets/", ""));
            mPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
        } else if (mSource.getScheme() != null && "asset".equals(mSource.getScheme())) {
            LOG("Loading assets URI: " + mSource.toString());
            AssetFileDescriptor afd;
            afd = getContext().getAssets().openFd(mSource.toString().replace("asset://", ""));
            mPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
        } else {
            LOG("Loading local URI: " + mSource.toString());
            mPlayer.setDataSource(getContext(), mSource);
        }
        mPlayer.prepareAsync();
    }

    private void prepare() {
        if (!mSurfaceAvailable || mSource == null || mPlayer == null || mIsPrepared) return;
        if (mCallback != null) mCallback.onPreparing(this);
        try {
            mPlayer.setSurface(mSurface);
            setSourceInternal();
        } catch (Exception e) {
            throwError(e);
        }
    }

    private void setControlsEnabled(boolean enabled) {
        if (mSeeker == null) return;
        mSeeker.setEnabled(enabled);
        mBtnPlayPause.setEnabled(enabled);
//        mBtnSubmit.setEnabled(enabled);
//        mBtnRestart.setEnabled(enabled);
//        mBtnRetry.setEnabled(enabled);

        final float disabledAlpha = .4f;
        mBtnPlayPause.setAlpha(enabled ? 1f : disabledAlpha);
//        mBtnSubmit.setAlpha(enabled ? 1f : disabledAlpha);
//        mBtnRestart.setAlpha(enabled ? 1f : disabledAlpha);

        mClickFrame.setEnabled(enabled);
    }

    @Override
    public void showControls() {
        if (mControlsDisabled || isControlsShown() || mSeeker == null) return;

        mControlsFrame.animate().cancel();
        mControlsFrame.setAlpha(0f);
        mControlsFrame.setVisibility(View.VISIBLE);
        mControlsFrame
                .animate()
                .alpha(1f)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(
                        new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                if (mAutoFullscreen) setFullscreen(false);
                            }
                        })
                .start();

        mToolbarFrame.animate().cancel();
        mToolbarFrame.setAlpha(0f);
        mToolbarFrame.setVisibility(View.VISIBLE);

        mToolbarFrame
                .animate()
                .alpha(1f)
                .setInterpolator(new DecelerateInterpolator())
                .start();


        mBtnPlayPause.animate().cancel();
        mBtnPlayPause.setAlpha(0f);
        mBtnPlayPause.setVisibility(View.VISIBLE);
        mBtnPlayPause
                .animate()
                .alpha(1f)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Log.i("EasyVideoPlayer", "showControls onAnimationEnd: gone view");
                        if (mBtnPlayPause != null)
                            mBtnPlayPause.setVisibility(VISIBLE);
                    }
                })
                .start();

        if (mIsShowNext) {
            mImgNext.animate().cancel();
            mImgNext.setAlpha(0f);

            mImgNext.setVisibility(View.VISIBLE);
            mImgNext
                    .animate()
                    .alpha(1f)
                    .setInterpolator(new DecelerateInterpolator())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            Log.i("EasyVideoPlayer", "showControls onAnimationEnd: gone view");
                            if (mImgNext != null)
                                mImgNext.setVisibility(VISIBLE);
                        }
                    })
                    .start();
        }


        if (mHandler == null) mHandler = new Handler();
        try {
            mHandler.removeCallbacks(mAutoHideControl);
            mHandler.postDelayed(mAutoHideControl, TIME_SHOW_CONTROL);
        } catch (Exception e) {
            Log.e("EasyVideoPlayer", "Exception", e);
        }
    }

    @Override
    public void hideControls() {
        if (mSeeker != null && mSeeker.getProgress() > (mSeeker.getMax() * 0.99)) return;
        if (mControlsDisabled || !isControlsShown() || mSeeker == null) return;
        if (mHandler == null) mHandler = new Handler();
        try {
            mHandler.removeCallbacks(mAutoHideControl);
        } catch (Exception e) {
            Log.e("EasyVideoPlayer", "Exception", e);
        }
        mControlsFrame.animate().cancel();
        mControlsFrame.setAlpha(1f);
        mControlsFrame.setVisibility(View.VISIBLE);
        mControlsFrame
                .animate()
                .alpha(0f)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(
                        new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                setFullscreen(true);
                                if (mControlsFrame != null)
                                    mControlsFrame.setVisibility(View.INVISIBLE);
                            }
                        })
                .start();

        mToolbarFrame.animate().cancel();
        mToolbarFrame.setAlpha(1f);
        mToolbarFrame.setVisibility(View.VISIBLE);

        mToolbarFrame
                .animate()
                .alpha(0f)
                .setInterpolator(new DecelerateInterpolator())
                .start();

        mBtnPlayPause.animate().cancel();
        mBtnPlayPause.setAlpha(1f);
        mBtnPlayPause.setVisibility(View.VISIBLE);

        Log.i("EasyVideoPlayer", "VISIBLE view");
        mBtnPlayPause
                .animate()
                .alpha(0f)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Log.i("EasyVideoPlayer", "onAnimationEnd: gone view");
                        if (mBtnPlayPause != null)
                            mBtnPlayPause.setVisibility(GONE);
                    }
                })
                .start();

        if (mIsShowNext) {
            mImgNext.animate().cancel();
            mImgNext.setAlpha(1f);
            mImgNext.setVisibility(View.VISIBLE);

            Log.i("EasyVideoPlayer", "VISIBLE view");
            mImgNext
                    .animate()
                    .alpha(0f)
                    .setInterpolator(new DecelerateInterpolator())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            Log.i("EasyVideoPlayer", "onAnimationEnd: gone view");
                            if (mImgNext != null)
                                mImgNext.setVisibility(INVISIBLE);
                        }
                    })
                    .start();
        }
        if (mImgNext.getVisibility() == VISIBLE) {
            mImgNext
                    .animate()
                    .alpha(0f)
                    .setInterpolator(new DecelerateInterpolator())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            Log.i("EasyVideoPlayer", "onAnimationEnd: gone view");
                            if (mImgNext != null)
                                mImgNext.setVisibility(INVISIBLE);
                        }
                    })
                    .start();
        }

//        mImgNext.setVisibility(GONE);

    }

    public void hideControlWhenComplete() {
        if (mSeeker != null && mSeeker.getProgress() > (mSeeker.getMax() * 0.99)) return;
        if (mControlsDisabled || !isControlsShown() || mSeeker == null) return;
        if (mHandler == null) mHandler = new Handler();
        try {
            mHandler.removeCallbacks(mAutoHideControl);
        } catch (Exception e) {
            Log.e("EasyVideoPlayer", "Exception", e);
        }
        mControlsFrame.animate().cancel();
        mControlsFrame.setAlpha(1f);
        mControlsFrame.setVisibility(View.INVISIBLE);
//        mControlsFrame
//                .animate()
//                .alpha(0f)
//                .setInterpolator(new DecelerateInterpolator())
//                .setListener(
//                        new AnimatorListenerAdapter() {
//                            @Override
//                            public void onAnimationEnd(Animator animation) {
//                                setFullscreen(true);
//                                if (mControlsFrame != null)
//                                    mControlsFrame.setVisibility(View.INVISIBLE);
//                            }
//                        })
//                .start();

        mToolbarFrame.animate().cancel();
        mToolbarFrame.setAlpha(1f);
        mToolbarFrame.setVisibility(View.INVISIBLE);
//        mToolbarFrame
//                .animate()
//                .alpha(0f)
//                .setInterpolator(new DecelerateInterpolator())
//                .start();

    }

    @CheckResult
    @Override
    public boolean isControlsShown() {
        return !mControlsDisabled && mControlsFrame != null && mControlsFrame.getAlpha() > .5f;
    }

    @Override
    public void toggleControls() {
        if (mControlsDisabled) return;
        if (isControlsShown()) {
            hideControls();
        } else {
            showControls();
        }
    }

    @Override
    public void enableControls(boolean andShow) {
        mControlsDisabled = false;
        if (andShow) showControls();
        mClickFrame.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        toggleControls();
                    }
                });
        mClickFrame.setClickable(true);
    }

    @Override
    public void disableControls() {
        mControlsDisabled = true;
        mControlsFrame.setVisibility(View.GONE);
        mBtnPlayPause.setVisibility(GONE);
        mToolbarFrame.setVisibility(GONE);
        mClickFrame.setOnClickListener(null);
        mClickFrame.setClickable(false);
    }

    @CheckResult
    @Override
    public boolean isPrepared() {
        return mPlayer != null && mIsPrepared;
    }

    @CheckResult
    @Override
    public boolean isPlaying() {
        return mPlayer != null && mPlayer.isPlaying();
    }

    @CheckResult
    @Override
    public int getCurrentPosition() {
        if (mPlayer == null) return -1;
        return mPlayer.getCurrentPosition();
    }

    @CheckResult
    @Override
    public int getDuration() {
        if (mPlayer == null) return -1;
        return mPlayer.getDuration();
    }

    @Override
    public void start() {
        if (mPlayer == null) return;

        mPlayer.start();
        if (mCallback != null) mCallback.onStarted(this);
        if (mHandler == null) mHandler = new Handler();
        mHandler.post(mUpdateCounters);
        mBtnPlayPause.setImageDrawable(mPauseDrawable);
        mProgressFrame.setVisibility(View.INVISIBLE);
        mWaitingFrame.setVisibility(View.GONE);
        Log.i(TAG, "nextvideo-----------onstart------");


    }

    @Override
    public void seekTo(@IntRange(from = 0, to = Integer.MAX_VALUE) int pos) {
        if (mPlayer == null) return;
        mPlayer.seekTo(pos);
    }

    public void setVolume(
            @FloatRange(from = 0f, to = 1f) float leftVolume,
            @FloatRange(from = 0f, to = 1f) float rightVolume) {
        if (mPlayer == null || !mIsPrepared)
            throw new IllegalStateException(
                    "You cannot use setVolume(float, float) until the player is prepared.");
        mPlayer.setVolume(leftVolume, rightVolume);
    }

    @Override
    public void pause() {
        if (mCallback != null) mCallback.onPaused(this);
        if (mHandler == null) return;
        mHandler.removeCallbacks(mUpdateCounters);
        mBtnPlayPause.setImageDrawable(mPlayDrawable);
        Log.i(TAG, "setImageDrawable mPlayDrawable");
        if (mPlayer == null || !isPlaying()) return;
        mPlayer.pause();
    }

    @Override
    public void stop() {
        if (mPlayer == null) return;
        try {
            mPlayer.stop();
        } catch (Throwable ignored) {
            Log.e(TAG, "Throwable", ignored);
        }
        if (mHandler == null) return;
        mHandler.removeCallbacks(mUpdateCounters);
        mBtnPlayPause.setImageDrawable(mPauseDrawable);
    }

    @Override
    public void reset() {
        if (mPlayer == null) return;
        mIsPrepared = false;
        mPlayer.reset();
        mIsPrepared = false;
    }

    @Override
    public void release() {
        mIsPrepared = false;

        if (mPlayer != null) {
            try {
                mPlayer.release();
            } catch (Throwable ignored) {
                Log.e(TAG, "Throwable", ignored);
            }
            mPlayer = null;
        }

        if (mHandler != null) {
            mHandler.removeCallbacks(mUpdateCounters);
            mHandler = null;
        }

        LOG("Released player and Handler");
    }

    @Override
    public void setAutoFullscreen(boolean autoFullscreen) {
        this.mAutoFullscreen = autoFullscreen;
    }

    @Override
    public void setLoop(boolean loop) {
        mLoop = loop;
        if (mPlayer != null) mPlayer.setLooping(loop);
    }

    // Surface listeners

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        LOG("Surface texture available: %dx%d", width, height);
        mInitialTextureWidth = width;
        mInitialTextureHeight = height;
        mSurfaceAvailable = true;
        mSurface = new Surface(surfaceTexture);
        if (mIsPrepared) {
            mPlayer.setSurface(mSurface);
        } else {
            prepare();
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
        LOG("Surface texture changed: %dx%d", width, height);
        adjustAspectRatio(width, height, mPlayer.getVideoWidth(), mPlayer.getVideoHeight());
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        LOG("Surface texture destroyed");
        mSurfaceAvailable = false;
        mSurface = null;
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }

    // Media player listeners

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        LOG("onPrepared()");

        mPlayer.setOnCompletionListener(this);

        mIsPrepared = true;
        if (mCallback != null) mCallback.onPrepared(this);
        mLabelPosition.setText(Util.getDurationString(0, false));
        mLabelDuration.setText(Util.getDurationString(mediaPlayer.getDuration(), false));
        mSeeker.setProgress(0);
        mSeeker.setMax(mediaPlayer.getDuration());
        mSeeker.setVisibility(VISIBLE);

        setControlsEnabled(true);
        enableControls(true);
        if (mAutoPlay) {
            if (!mControlsDisabled && mHideControlsOnPlay) hideControls();
            if (!isStop) {
                start();
                if (mInitialPosition > 0) {
                    seekTo(mInitialPosition);
                    mInitialPosition = -1;
                }
            } else {
                mProgressFrame.setVisibility(GONE);
            }

        } else {
//            showControls();
            // Hack to show first frame, is there another way?
            if (!isStop) {
                mPlayer.start();
            } else {
                mProgressFrame.setVisibility(GONE);
            }
//            mPlayer.pause();
        }

    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            isFullSreen = false;
            mImgToggleScreen.setImageResource(R.drawable.ic_toogle_video_full);
            if (mCallback != null) {
                mCallback.onResizeFrame(false);
            }
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isFullSreen = true;
            mImgToggleScreen.setImageResource(R.drawable.ic_toogle_video_small);
            if (mCallback != null) {
                mCallback.onResizeFrame(true);
            }
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int percent) {
        LOG("Buffering: %d%%", percent);
        if (mCallback != null) mCallback.onBuffering(percent);
        if (mSeeker != null) {
            if (percent == 100) mSeeker.setSecondaryProgress(0);
            else mSeeker.setSecondaryProgress(mSeeker.getMax() * (percent / 100));
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mCanNext = true;
        Log.i(TAG, "nextvideo onCompletion--------------------" + mCanNext);
        mIsShowNext = false;
        LOG("onCompletion()");
        mWaitingFrame.setVisibility(View.VISIBLE);
//        if (mBtnPlayPause.getVisibility() != VISIBLE) {
        mProgressFrame.setVisibility(View.VISIBLE);
//        }
//        mTextureView.setFormat(PixelFormat.TRANSPARENT);
//        surfaceHolder.setFormat(PixelFormat.OPAQUE);//        hideControlWhenComplete();
//        hideControls();
//        mSeeker.setProgress(mSeeker.getMax());
//        if (mLoop) {
//            mBtnPlayPause.setImageDrawable(mPlayDrawable);
//            if (mHandler != null) mHandler.removeCallbacks(mUpdateCounters);
//            mSeeker.setProgress(mSeeker.getMax());
//            showControls();
//        }
        if (mCallback != null) {
            mCallback.onCompletion(this);
//            if (mLoop) mCallback.onStarted(this);
        }
        if (mSeeker.getProgress() > 0) {
            mBtnPlayPause.setImageDrawable(mPlayDrawable);

            Log.i(TAG, "autonext video -------------------" + mHaveLink);
            if (mHaveLink) {
                visibleViewAutoNextVideo();
            } else {
                onCancelView();
            }
        }
    }

    public void setTitleNextVideo(String title) {
        if (mTvwNextVideo != null) {
            mTvwNextVideo.setText(title);
            if (TextUtils.isEmpty(title)) {
                mTvwNextVideo.setVisibility(GONE);
                mTvNextVideoLabel.setVisibility(GONE);
            } else {
                mTvwNextVideo.setVisibility(VISIBLE);
                mTvNextVideoLabel.setVisibility(VISIBLE);
            }
        }
    }

    private void visibleViewAutoNextVideo() {
        Log.i(TAG, "autonext visibleViewAutoNextVideo-----------------------------------");
//        showControls();
        enableControls(true);
        mImgNext.setVisibility(INVISIBLE);

//        hideControls();
        mToolbarFrame.setVisibility(View.VISIBLE);
        mControlsFrame.setVisibility(View.VISIBLE);
        mBtnPlayPause.setVisibility(View.VISIBLE);
        mProgressFrame.setVisibility(View.GONE);

        mViewNextVideo.setVisibility(VISIBLE);
        mProgressNextVideo.setVisibility(VISIBLE);
        mTvwCancelNextVideo.setVisibility(VISIBLE);
        mBgNextVideo.setVisibility(VISIBLE);
        mClickFrame.setOnClickListener(null);
        mClickFrame.setClickable(false);
//        mImgReplay.setVisibility(GONE);
//        mImgNext.setVisibility(GONE);
//        mImgPrevious.setVisibility(GONE);
        animatorSet.start();

    }

    private void visibleViewReplayVideo() {
        animatorSet.cancel();
        mProgressNextVideo.setProgress(0);
        mViewNextVideo.setVisibility(GONE);
        mImgReplay.setVisibility(VISIBLE);
//        mImgNext.setVisibility(VISIBLE);
//        mImgPrevious.setVisibility(VISIBLE);
        mTvwCancelNextVideo.setVisibility(GONE);
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int width, int height) {
        LOG("Video size changed: %dx%d", width, height);
        adjustAspectRatio(mInitialTextureWidth, mInitialTextureHeight, width, height);
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {

        if (what == -38) {
            // Error code -38 happens on some Samsung devices
            // Just ignore it
            return false;
        }

        mPlayer.setOnCompletionListener(null);

        String errorMsg = "Preparation/playback error (" + what + "): ";
        switch (what) {
            default:
                errorMsg += "Unknown error";
                break;
            case MediaPlayer.MEDIA_ERROR_IO:
                errorMsg += "I/O error";
                break;
            case MediaPlayer.MEDIA_ERROR_MALFORMED:
                errorMsg += "Malformed";
                break;
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                errorMsg += "Not valid for progressive playback";
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                errorMsg += "Server died";
                break;
            case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                errorMsg += "Timed out";
                break;
            case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
                errorMsg += "Unsupported";
                break;
        }
        throwError(new Exception(errorMsg));
        mCallback.onError(this, new Exception(errorMsg));
        return false;
    }

    // View events

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (isInEditMode()) {
            return;
        }

        setKeepScreenOn(true);

        mHandler = new Handler();
        mPlayer = new MediaPlayer();

        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnBufferingUpdateListener(this);
        mPlayer.setOnVideoSizeChangedListener(this);
        mPlayer.setOnErrorListener(this);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setLooping(mLoop);

        mPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                        mProgressFrame.setVisibility(VISIBLE);
                        break;
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                        mProgressFrame.setVisibility(GONE);
                        break;
                }
                return true;
            }
        });

        // Instantiate and add TextureView for rendering
        final LayoutParams textureLp =
                new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mTextureView = new TextureView(getContext());
        addView(mTextureView, textureLp);
        mTextureView.setSurfaceTextureListener(this);

        final LayoutInflater li = LayoutInflater.from(getContext());


        // Instantiate and add click frame (used to toggle controls)
        mClickFrame = new FrameLayout(getContext());
        //noinspection RedundantCast
        ((FrameLayout) mClickFrame)
                .setForeground(Util.resolveDrawable(getContext(), R.attr.selectableItemBackground));
        addView(
                mClickFrame,
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


        final EasyVideoPlayer easyVideoPlayer = this;
        initViewCenterControl(li, easyVideoPlayer);
        initViewControl(li);
        initViewToolbar(li);


        if (mControlsDisabled) {
            mClickFrame.setOnClickListener(null);
            mControlsFrame.setVisibility(View.GONE);
            mToolbarFrame.setVisibility(GONE);
            mBtnPlayPause.setVisibility(GONE);
        } else {
            mClickFrame.setOnClickListener(
                    new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toggleControls();
                            mCallback.onClickVideoFrame(easyVideoPlayer);
                        }
                    });
        }


        invalidateThemeColors();

        setControlsEnabled(false);
        invalidateActions();
        prepare();
    }

    private void initViewToolbar(LayoutInflater li) {
//toolbar
        mToolbarFrame = li.inflate(R.layout.evp_include_toolbar, this, false);
        final LayoutParams toolbarLp =
                new LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        toolbarLp.gravity = Gravity.TOP;
        addView(mToolbarFrame, toolbarLp);

        mImgBack = (ImageView) mToolbarFrame.findViewById(R.id.img_back_video);
        mImgMoreOption = (ImageView) mToolbarFrame.findViewById(R.id.img_more_option_video);
        mTvwTitle = (TextView) mToolbarFrame.findViewById(R.id.tvw_title_video);

        mImgBack.setOnClickListener(this);
        mImgMoreOption.setOnClickListener(this);
    }

    private void initViewControl(LayoutInflater li) {
// Inflate controls
        mControlsFrame = li.inflate(R.layout.evp_include_controls, this, false);
        final LayoutParams controlsLp =
                new LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        controlsLp.gravity = Gravity.BOTTOM;
        addView(mControlsFrame, controlsLp);

        // Inflate and add progress
        mProgressFrame = li.inflate(R.layout.evp_include_progress, this, false);
        addView(mProgressFrame);
        // Retrieve controls
        mSeeker = (SeekBar) mControlsFrame.findViewById(R.id.seeker);
        mSeeker.setOnSeekBarChangeListener(this);

        mLabelPosition = (TextView) mControlsFrame.findViewById(R.id.position);
        mLabelPosition.setText(Util.getDurationString(0, false));

        mLabelDuration = (TextView) mControlsFrame.findViewById(R.id.duration);
        mLabelDuration.setText(Util.getDurationString(0, false));

        mImgToggleScreen = (ImageView) mControlsFrame.findViewById(R.id.img_toggle_screen);
        mImgToggleScreen.setOnClickListener(this);
        mImgToggleScreen.setImageResource(R.drawable.ic_toogle_video_full);
    }

    private void initViewCenterControl(LayoutInflater li, final EasyVideoPlayer easyVideoPlayer) {
        mCenterControl = li.inflate(R.layout.evp_include_center_control, this, false);
        final LayoutParams centerControlLp =
                new LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        centerControlLp.gravity = Gravity.CENTER;
        addView(mCenterControl, centerControlLp);

        mBtnPlayPause = (ImageView) mCenterControl.findViewById(R.id.btnPlayPause);
        mBtnPlayPause.setOnClickListener(this);
        mBtnPlayPause.setImageDrawable(mPlayDrawable);
        mBtnPlayPause.setVisibility(GONE);

        mTvwCancelNextVideo = (TextView) mCenterControl.findViewById(R.id.tvw_cancel);
        mProgressNextVideo = (DonutProgress) mCenterControl.findViewById(R.id.progress_load_next_video);
        mViewNextVideo = mCenterControl.findViewById(R.id.view_next_video);
        mWaitingFrame = mCenterControl.findViewById(R.id.view_waiting);
//        final RelativeLayout.LayoutParams progreesLp =
//                new RelativeLayout.LayoutParams(
//                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        progreesLp.addRule(RelativeLayout.ABOVE,R.id.progress_load_next_video);
//        mViewNextVideo.setLayoutParams();
        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.progress_next_video);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.setTarget(mProgressNextVideo);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                Log.i(TAG, "nextvideo onAnimationEnd--------------------" + mCanNext);
                super.onAnimationEnd(animation);
                if (mCallback != null && mCanNext && mPlayer != null && !mPlayer.isPlaying()) {
                    mCallback.onNextVideo(easyVideoPlayer);
                    mBtnPlayPause.setVisibility(View.GONE);
                    mToolbarFrame.setVisibility(View.GONE);
                    mControlsFrame.setVisibility(View.GONE);
                    mWaitingFrame.setVisibility(View.VISIBLE);
                    mImgReplay.setVisibility(View.GONE);
//                    mSeeker.setProgress(-1);
                    mCanNext = false;
                }
            }
        });

        mTvwNextVideo = (TextView) mCenterControl.findViewById(R.id.tvw_next_video);
        mTvNextVideoLabel = (TextView) mCenterControl.findViewById(R.id.txt_next_video_tittle);
        mImgNext = (ImageView) mCenterControl.findViewById(R.id.img_next_video);
//        mImgPrevious = (ImageView) mCenterControl.findViewById(R.id.img_previous_video);
        mImgReplay = (RelativeLayout) mCenterControl.findViewById(R.id.img_replay_video);
        mBgNextVideo = mCenterControl.findViewById(R.id.bg_next_video);
//        mImgNext.setOnClickListener(this);
        mImgReplay.setOnClickListener(this);
        mImgNext.setOnClickListener(this);
//        mImgPrevious.setOnClickListener(this);
        mTvwCancelNextVideo.setOnClickListener(this);

//        mBtnPlayPause = new ImageView(getContext());
//        mBtnPlayPause.setId(R.id.btnPlayPause);
//        final FrameLayout.LayoutParams playParam = new FrameLayout.LayoutParams(
//                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        playParam.gravity = Gravity.CENTER;
//        addView(mBtnPlayPause, playParam);
//        mBtnPlayPause.setOnClickListener(this);
//        mBtnPlayPause = (ImageView) mControlsFrame.findViewById(R.id.btnPlayPause);
        mViewNextVideo.setVisibility(GONE);
        mTvwCancelNextVideo.setVisibility(GONE);
        mImgReplay.setVisibility(GONE);
//        mImgNext.setVisibility(GONE);
//        mImgPrevious.setVisibility(GONE);
        mProgressNextVideo.setVisibility(GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnPlayPause:
                Log.i(TAG, " nextvideo btnPlayPause----------------onclick");
                if (mSeeker.getProgress() > (mSeeker.getMax() * 0.99)) {
                    if (mCallback != null) {
                        mCallback.onNextVideo(this);
                        mBtnPlayPause.setVisibility(View.GONE);
                        mToolbarFrame.setVisibility(View.GONE);
                        mControlsFrame.setVisibility(View.GONE);
                        mImgReplay.setVisibility(View.GONE);
                        mProgressFrame.setVisibility(VISIBLE);
                        mWaitingFrame.setVisibility(VISIBLE);
                        mCanNext = false;
                        Log.i(TAG, "nextvideo btnPlayPause----------------onclick-------------" + mCanNext);

                    }

                } else {
                    if (mPlayer.isPlaying()) {
                        pause();
                    } else {
                        if (mHideControlsOnPlay && !mControlsDisabled) hideControls();
                        start();
                    }
                }


                break;
            case R.id.img_toggle_screen:
                Log.i("THANH", "setfullscreen: " + isFullSreen);
                if (mCallback != null) {
                    mCallback.onToggleScreen(!isFullSreen);
                }
                break;

            case R.id.img_back_video:
                if (mCallback != null) {
                    if (isFullSreen) {
                        mCallback.onToggleScreen(!isFullSreen);
                    } else {
                        mCallback.onClickBackPress();
                    }
                }
                break;

            case R.id.img_more_option_video:
                if (mCallback != null) {
                    mCallback.onMoreOption();
                }
                break;
            case R.id.tvw_cancel:
                mCanNext = false;
                animatorSet.cancel();
                mBtnPlayPause.setImageDrawable(mPlayDrawable);
                if (mHandler != null) mHandler.removeCallbacks(mUpdateCounters);
                mSeeker.setProgress(0);
                mImgReplay.setVisibility(VISIBLE);
                hideViewAutoNextVideo();
                mToolbarFrame.setVisibility(View.VISIBLE);
                mImgNext.setVisibility(View.INVISIBLE);
                mControlsFrame.setVisibility(View.GONE);
                break;

            case R.id.img_replay_video:

                mBtnPlayPause.setVisibility(View.GONE);
                mProgressFrame.setVisibility(VISIBLE);
                mWaitingFrame.setVisibility(VISIBLE);
                mToolbarFrame.setVisibility(View.VISIBLE);
                mControlsFrame.setVisibility(View.GONE);
                mViewNextVideo.setVisibility(View.GONE);

                mBtnPlayPause.setImageDrawable(mPlayDrawable);
                if (mHandler != null) mHandler.removeCallbacks(mUpdateCounters);
                mSeeker.setProgress(0);

                showControls();
                sourceChanged();
                mImgReplay.setVisibility(GONE);
                break;

            case R.id.img_next_video:
                mProgressFrame.setVisibility(VISIBLE);
                mWaitingFrame.setVisibility(VISIBLE);
                Log.i(TAG, "nextvideo-----------onNextClick------");

                mBtnPlayPause.setVisibility(View.GONE);
                mToolbarFrame.setVisibility(View.GONE);
                mControlsFrame.setVisibility(View.GONE);
                mImgReplay.setVisibility(View.GONE);
                mImgNext.setVisibility(View.INVISIBLE);

                if (mCallback != null)
                    mCallback.onNextVideo(this);
                break;

            case R.id.img_previous_video:
                if (mCallback != null)
                    mCallback.onPreviousVideo(this);
                break;

            default:
                break;
        }

        /*else if (view.getId() == R.id.btnRestart) {
            seekTo(0);
            if (!isPlaying()) start();
        }
        else if (view.getId() == R.id.btnRetry) {
            if (mCallback != null) mCallback.onRetry(this, mSource);
        } else if (view.getId() == R.id.btnSubmit) {
            if (mCallback != null) mCallback.onSubmit(this, mSource);
        }*/
    }

    public void onCancelView() {
        Log.i(TAG, "nextvideo-----------onCancelView------");
        mImgReplay.setVisibility(VISIBLE);
//    hideViewAutoNextVideo();
//        enableControls(true);
//        setControlsEnabled(true);
//        mBtnPlayPause.setVisibility(View.GONE);
//        mToolbarFrame.setVisibility(View.VISIBLE);
//        mControlsFrame.setVisibility(View.VISIBLE);
        mProgressFrame.setVisibility(View.GONE);

        mViewNextVideo.setVisibility(GONE);
        mProgressNextVideo.setVisibility(GONE);
        mTvwCancelNextVideo.setVisibility(GONE);
        mBgNextVideo.setVisibility(GONE);
        mImgNext.setVisibility(INVISIBLE);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
        if (fromUser) {
            seekTo(value);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mWasPlaying = isPlaying();
        if (mWasPlaying) mPlayer.pause(); // keeps the time updater running, unlike pause()
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (mWasPlaying) mPlayer.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LOG("Detached from window");
        release();

        mSeeker = null;
        mLabelPosition = null;
        mLabelDuration = null;
        mBtnPlayPause = null;
        /*mBtnRestart = null;
        mBtnSubmit = null;*/

        mControlsFrame = null;
        mClickFrame = null;
        mProgressFrame = null;

        if (mHandler != null) {
            mHandler.removeCallbacks(mUpdateCounters);
            mHandler = null;
        }
    }

    // Utilities

    private static void LOG(String message, Object... args) {
        try {
            if (args != null) message = String.format(message, args);
            Log.d("EasyVideoPlayer", message);
        } catch (Exception ignored) {
            Log.e(TAG, "Exception", ignored);
        }
    }

    private void invalidateActions() {
        /*switch (mLeftAction) {
            case LEFT_ACTION_NONE:
                mBtnRetry.setVisibility(View.GONE);
                mBtnRestart.setVisibility(View.GONE);
                break;
            case LEFT_ACTION_RESTART:
                mBtnRetry.setVisibility(View.GONE);
                mBtnRestart.setVisibility(View.VISIBLE);
                break;
            case LEFT_ACTION_RETRY:
                mBtnRetry.setVisibility(View.VISIBLE);
                mBtnRestart.setVisibility(View.GONE);
                break;
        }
        switch (mRightAction) {
            case RIGHT_ACTION_NONE:
                mBtnSubmit.setVisibility(View.GONE);
                mLabelCustom.setVisibility(View.GONE);
                break;
            case RIGHT_ACTION_SUBMIT:
                mBtnSubmit.setVisibility(View.VISIBLE);
                mLabelCustom.setVisibility(View.GONE);
                break;
            case RIGHT_ACTION_CUSTOM_LABEL:
                mBtnSubmit.setVisibility(View.GONE);
                mLabelCustom.setVisibility(View.VISIBLE);
                break;
        }*/
    }

    private void adjustAspectRatio(int viewWidth, int viewHeight, int videoWidth, int videoHeight) {
        final double aspectRatio = (double) videoHeight / videoWidth;
        int newWidth, newHeight;

        if (viewHeight > (int) (viewWidth * aspectRatio)) {
            // limited by narrow width; restrict height
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        } else {
            // limited by short height; restrict width
            newWidth = (int) (viewHeight / aspectRatio);
            newHeight = viewHeight;
        }

        final int xoff = (viewWidth - newWidth) / 2;
        final int yoff = (viewHeight - newHeight) / 2;

        final Matrix txform = new Matrix();
        mTextureView.getTransform(txform);
        txform.setScale((float) newWidth / viewWidth, (float) newHeight / viewHeight);
        txform.postTranslate(xoff, yoff);
        mTextureView.setTransform(txform);
    }

    private void throwError(Exception e) {
        if (mCallback != null) mCallback.onError(this, e);
        else throw new RuntimeException(e);
    }

    private static void setTint(@NonNull SeekBar seekBar, @ColorInt int color) {
        ColorStateList s1 = ColorStateList.valueOf(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            seekBar.setThumbTintList(s1);
            seekBar.setProgressTintList(s1);
            seekBar.setSecondaryProgressTintList(s1);
        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
            Drawable progressDrawable = DrawableCompat.wrap(seekBar.getProgressDrawable());
            seekBar.setProgressDrawable(progressDrawable);
            DrawableCompat.setTintList(progressDrawable, s1);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Drawable thumbDrawable = DrawableCompat.wrap(seekBar.getThumb());
                DrawableCompat.setTintList(thumbDrawable, s1);
                seekBar.setThumb(thumbDrawable);
            }
        } else {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            if (seekBar.getIndeterminateDrawable() != null)
                seekBar.getIndeterminateDrawable().setColorFilter(color, mode);
            if (seekBar.getProgressDrawable() != null)
                seekBar.getProgressDrawable().setColorFilter(color, mode);
        }
    }

    private Drawable tintDrawable(@NonNull Drawable d, @ColorInt int color) {
        d = DrawableCompat.wrap(d.mutate());
        DrawableCompat.setTint(d, color);
        return d;
    }

    private void tintSelector(@NonNull View view, @ColorInt int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                && view.getBackground() instanceof RippleDrawable) {
            final RippleDrawable rd = (RippleDrawable) view.getBackground();
            rd.setColor(ColorStateList.valueOf(Util.adjustAlpha(color, 0.3f)));
        }
    }

    private void invalidateThemeColors() {
        /*final int labelColor = Util.isColorDark(mThemeColor) ? Color.WHITE : Color.BLACK;
        mControlsFrame.setBackgroundColor(Util.adjustAlpha(mThemeColor, 0.8f));
        tintSelector(mBtnRestart, labelColor);
        tintSelector(mBtnPlayPause, labelColor);
        mLabelDuration.setTextColor(labelColor);
        mLabelPosition.setTextColor(labelColor);
        setTint(mSeeker, labelColor);
        mBtnRetry.setTextColor(labelColor);
        tintSelector(mBtnRetry, labelColor);
        mBtnSubmit.setTextColor(labelColor);
        tintSelector(mBtnSubmit, labelColor);
        mLabelCustom.setTextColor(labelColor);
        mLabelBottom.setTextColor(labelColor);
        mPlayDrawable = tintDrawable(mPlayDrawable.mutate(), labelColor);
        mRestartDrawable = tintDrawable(mRestartDrawable.mutate(), labelColor);
        mPauseDrawable = tintDrawable(mPauseDrawable.mutate(), labelColor);*/
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void setFullscreen(boolean fullscreen) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            if (mAutoFullscreen) {
                int flags = !fullscreen ? 0 : View.SYSTEM_UI_FLAG_LOW_PROFILE;

                ViewCompat.setFitsSystemWindows(mControlsFrame, !fullscreen);
                ViewCompat.setFitsSystemWindows(mToolbarFrame, !fullscreen);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    flags |=
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
                    if (fullscreen) {
                        flags |=
                                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE;
                    }
                }

                mClickFrame.setSystemUiVisibility(flags);
            }
        }
    }

    public void setTitleVideo(String title) {
        mTvwTitle.setText(title);
    }

    public boolean isFullSreen() {
        return isFullSreen;
    }

    public void hideViewAutoNextVideo() {
//        showControls();
        mViewNextVideo.setVisibility(GONE);
        mProgressNextVideo.setVisibility(GONE);
        mTvwCancelNextVideo.setVisibility(GONE);
        mBgNextVideo.setVisibility(GONE);
//        mImgReplay.setVisibility(GONE);
//        mImgNext.setVisibility(GONE);
//        mImgPrevious.setVisibility(GONE);
//        animatorSet.start();

    }

    public void setStop(boolean stop) {
        isStop = stop;
    }
}

package com.metfone.selfcare.ui;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.metfone.selfcare.util.Log;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by toanvk2 on 25/06/2015.
 */
public class DecimalTextWatcher implements TextWatcher {
    private static final String TAG = DecimalTextWatcher.class.getSimpleName();
    private DecimalFormat df;
//    private DecimalFormat dfnd;
//    private boolean hasFractionalPart;

    private EditText et;

    public DecimalTextWatcher(EditText et) {
        DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols(Locale.getDefault());
        unusualSymbols.setDecimalSeparator('.');
        unusualSymbols.setGroupingSeparator(',');
        df = new DecimalFormat("#,###", unusualSymbols);
        df.setGroupingSize(3);
        //        df.setDecimalSeparatorAlwaysShown(false);
//        dfnd = new DecimalFormat("#,###");
        this.et = et;
//        hasFractionalPart = false;
    }

    @Override
    public void afterTextChanged(Editable s) {
        et.removeTextChangedListener(this);
        try {
            int inilen, endlen;
            inilen = et.getText().length();
            String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
            Number n = df.parse(v);
            int cp = et.getSelectionStart();
//            if (hasFractionalPart) {
            et.setText(df.format(n));
            /*} else {
                et.setText(dfnd.format(n));
            }*/
            endlen = et.getText().length();
            // cursor index
            int sel = (cp + (endlen - inilen));
            if (sel > 0 && sel <= et.getText().length()) {
                et.setSelection(sel);
            } else {
                et.setSelection(et.getText().length() - 1);
            }
        } catch (NumberFormatException | ParseException nfe) {
            Log.e(TAG, "Exception", nfe);
        }
        et.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
       /* if (s.toString().contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()))) {
            hasFractionalPart = true;
        } else {
            hasFractionalPart = false;
        }*/
    }
}
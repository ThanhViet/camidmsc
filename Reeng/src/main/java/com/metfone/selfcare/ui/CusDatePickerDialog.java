package com.metfone.selfcare.ui;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by huybq7 on 2/10/2015.
 */
public class CusDatePickerDialog extends DatePickerDialog {

    private Calendar minDate;
    private Calendar maxDate;
    private java.text.DateFormat mTitleDateFormat;

    public CusDatePickerDialog(Context context, DatePickerDialog.OnDateSetListener callBack,
                               int year, int monthOfYear, int dayOfMonth, Calendar minDate, Calendar maxDate) {
        super(context, callBack, year, monthOfYear, dayOfMonth);
        this.minDate = minDate;
        this.maxDate = maxDate;
        mTitleDateFormat = java.text.DateFormat.getDateInstance(java.text.DateFormat.FULL);
    }

    public void onDateChanged(DatePicker view, int year, int month, int day) {
        Calendar newDate = Calendar.getInstance();
        newDate.set(year, month, day);

        if (minDate != null && minDate.after(newDate)) {
            initView(view, minDate.get(Calendar.YEAR), minDate.get(Calendar.MONTH),
                    minDate.get(Calendar.DAY_OF_MONTH), this);
            setTitle(mTitleDateFormat.format(minDate.getTime()));
        } else if (maxDate != null && maxDate.before(newDate)) {
            initView(view, maxDate.get(Calendar.YEAR), maxDate.get(Calendar.MONTH),
                    maxDate.get(Calendar.DAY_OF_MONTH), this);
            setTitle(mTitleDateFormat.format(maxDate.getTime()));
        } else {
            initView(view, year, month, day, this);
            setTitle(mTitleDateFormat.format(newDate.getTime()));
        }
    }

    private void initView(DatePicker view, int year, int monthOfYear, int dayOfMonth,
                          DatePicker.OnDateChangedListener onDateChangedListener) {
        view.init(year, monthOfYear, dayOfMonth, onDateChangedListener);
    }
}
package com.metfone.selfcare.ui.imageview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

/**
 * thư viện tạo ảnh nền li xì
 * Created by thanhnt72 on 2/1/2018.
 */

public class LixiImageView extends AppCompatImageView {

    private static final String TAG = LixiImageView.class.getSimpleName();

    private boolean runable;
    private int heightParent;
    private int widthParent;
    private ViewGroup parent;

    public static int ROUNDED;

    public LixiImageView(Context context) {
        super(context);
        initView(context);
    }

    public LixiImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public LixiImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        runable = true;
        ROUNDED = context.getResources().getDimensionPixelOffset(R.dimen.corner_radius_bubble_chat);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(TAG, "onMeasure");
        post(new Runnable() {
            @Override
            public void run() {
                getParentLayout();
            }
        });
    }

    private void getParentLayout() {
        try {
            parent = (ViewGroup) getParent();
            parent.post(new Runnable() {
                @Override
                public void run() {
                    makeImage();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeImage() {
        try {

            int height = parent.getHeight();
            int width = parent.getWidth();
            if (height <= 0 || width <= 0) return;
            if (height != heightParent && width != widthParent) {
                heightParent = height;
                widthParent = width;
                new ImageTask(getResources(), heightParent, widthParent).execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ImageTask extends AsyncTask<Void, Void, Bitmap> {

        private Resources resources;
        private int heightParent;
        private int widthParent;

        ImageTask(Resources resources, int heightParent, int widthParent) {
            this.resources = resources;
            this.heightParent = heightParent;
            this.widthParent = widthParent;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                Bitmap.Config config = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = Bitmap.createBitmap(widthParent, heightParent, config);
                Canvas canvas = new Canvas(bitmap);

                Bitmap icLuck = BitmapFactory.decodeResource(resources, R.drawable.bg_repeat_lixi_new);
                int forX = widthParent / icLuck.getWidth() + 1;
                int forY = heightParent / icLuck.getHeight() + 1;

                Paint paint = new Paint();
                paint.setColor(Color.RED);
                for (int i = 0; i < forX; i++) {
                    for (int j = 0; j < forY; j++) {
                        canvas.drawBitmap(icLuck, i * icLuck.getWidth(), j * icLuck.getWidth(), paint);
                    }
                }
                icLuck.recycle();
                return getRoundedCornerBitmap(bitmap, ROUNDED);
            } catch (Exception ex) {
                Log.e(TAG, "Exception", ex);
                return null;
            }


        }

        Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xFFFF0000;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, (float) pixels, (float) pixels, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            try {
                if (runable && bitmap != null) setImageBitmap(bitmap);
            } catch (Exception ignored) {
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        runable = false;
        super.finalize();
    }
}

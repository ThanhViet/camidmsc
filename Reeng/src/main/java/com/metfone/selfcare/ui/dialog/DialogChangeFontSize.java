package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.helper.Constants;

/**
 * Created by toanvk2 on 11/8/2017.
 */

public class DialogChangeFontSize extends Dialog implements View.OnClickListener {
    private BaseSlidingFragmentActivity mActivity;
    private TextView mViewSmall, mViewMedium, mViewLarge;
    private Button mBtnNegative;
    private PositiveListener<Integer> callBack;

    public DialogChangeFontSize(@NonNull BaseSlidingFragmentActivity context, @NonNull PositiveListener<Integer> listener) {
        super(context, R.style.DialogFullscreen);
        this.callBack = listener;
        this.mActivity = context;
        setCancelable(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_select_font_size);
        mViewSmall = (TextView) findViewById(R.id.dialog_font_size_small);
        mViewMedium = (TextView) findViewById(R.id.dialog_font_size_medium);
        mViewLarge = (TextView) findViewById(R.id.dialog_font_size_large);
        mBtnNegative = (Button) findViewById(R.id.dialog_font_size_negative);
        drawDetail();
        initEvent();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View view) {
        SettingBusiness settingBusiness = SettingBusiness.getInstance(mActivity);
        switch (view.getId()) {
            case R.id.dialog_font_size_small:
                settingBusiness.setFontSize(Constants.FONT_SIZE.SMALL);
                callBack.onPositive(Constants.FONT_SIZE.SMALL);
                break;
            case R.id.dialog_font_size_medium:
                settingBusiness.setFontSize(Constants.FONT_SIZE.MEDIUM);
                callBack.onPositive(Constants.FONT_SIZE.MEDIUM);
                break;
            case R.id.dialog_font_size_large:
                settingBusiness.setFontSize(Constants.FONT_SIZE.LARGE);
                callBack.onPositive(Constants.FONT_SIZE.LARGE);
                break;
        }
        dismiss();
    }

    private void drawDetail() {
        Resources mRes = mActivity.getResources();
        mViewSmall.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constants.FONT_SIZE.SMALL_RATIO * mRes.getDimension(R.dimen.mocha_text_size_level_3));
        mViewMedium.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constants.FONT_SIZE.MEDIUM_RATIO * mRes.getDimension(R.dimen.mocha_text_size_level_3));
        mViewLarge.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constants.FONT_SIZE.LARGE_RATIO * mRes.getDimension(R.dimen.mocha_text_size_level_3));
    }

    private void initEvent() {
        mViewSmall.setOnClickListener(this);
        mViewMedium.setOnClickListener(this);
        mViewLarge.setOnClickListener(this);
        mBtnNegative.setOnClickListener(this);
    }
}

package com.metfone.selfcare.ui.autoplay.calculator;


import com.metfone.selfcare.ui.autoplay.utils.ItemsPositionGetter;

/**
 * Created by toanvk2 on 1/18/2018.
 */

public interface ListItemsVisibilityCalculator {
    void onScrollStateIdle(ItemsPositionGetter itemsPositionGetter, int firstVisiblePosition, int lastVisiblePosition);

    void onScroll(ItemsPositionGetter itemsPositionGetter, int firstVisibleItem, int visibleItemCount, int scrollState);
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/8/12
 *
 */

package com.metfone.selfcare.ui.tabvideo.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.holder.VideoEpisodePlayerHolder;
import com.metfone.selfcare.ui.tabvideo.listener.ChooseEpisodeListener;

public class VideoEpisodePlayerAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    private static final int TYPE_NORMAL = 1;

    public VideoEpisodePlayerAdapter(Activity activity) {
        super(activity);
    }

    private ChooseEpisodeListener listener;
    private int currentPosition = -1;
    private boolean isMovies;

    public void setMovies(boolean movies) {
        isMovies = movies;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public void setListener(ChooseEpisodeListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_NORMAL) {
            return new VideoEpisodePlayerHolder(layoutInflater.inflate(R.layout.holder_video_episode_player, parent, false), activity, listener, isMovies);
        }
        return new BaseAdapter.EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder instanceof VideoEpisodePlayerHolder) {
            ((VideoEpisodePlayerHolder) holder).bindData(getItem(position), position, currentPosition == position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof Video)
            return TYPE_NORMAL;
        return TYPE_EMPTY;
    }

}

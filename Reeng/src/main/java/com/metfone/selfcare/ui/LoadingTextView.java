package com.metfone.selfcare.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.TextView;

import com.metfone.selfcare.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by toanvk2 on 10/10/2016.
 */

public class LoadingTextView extends TextView {
    private static final String TAG = LoadingTextView.class.getSimpleName();
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private int dots = 0;
    private String content;
    private long duration = 400;
    private Timer mTimer;
    private TimerTask mTimerTask;

    public LoadingTextView(Context context) {
        super(context);
    }

    public LoadingTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoadingTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setText(String text, long duration) {
        this.content = text;
        this.duration = duration;
        setText(text);
        startTimer();
    }

    private void startTimer() {
        stopTimer();
        Log.d(TAG, "start timer");
        dots = 0;
        mTimer = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "Timer running --");
                dots++;
                if (dots > 3) dots = 0;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (dots == 0) {
                            setText(content);
                        } else if (dots == 1) {
                            setText(content + " .");
                        } else if (dots == 2) {
                            setText(content + " ..");
                        } else {
                            setText(content + " ...");
                        }
                    }
                });
            }
        };
        mTimer.schedule(mTimerTask, duration, duration);
    }

    public void stopTimer() {
        Log.d(TAG, "stop timer");
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    public void release() {
        stopTimer();
        mHandler = null;
    }
}
package com.metfone.selfcare.ui.tabvideo.channelDetail.video;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.livestream.model.ConfigLiveComment;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.tabvideo.fragment.BaseViewStubFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChannelListener;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class VideoChannelFragment extends BaseViewStubFragment implements SwipeRefreshLayout.OnRefreshListener
        , ApiCallbackV2<ArrayList<Video>>, OnChannelChangedDataListener, OnVideoChangedDataListener
        , OnInternetChangedListener, OnVideoChannelListener, OnClickMoreItemListener {

    private static final String CHANNEL = "channel";
    public static final int LIMIT = 20;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.empty_progress)
    ProgressLoading emptyProgress;
    @BindView(R.id.tvEmptyTitle)
    TextView tvEmptyTitle;
    @BindView(R.id.tvEmptyDes)
    TextView tvEmptyDes;
    @BindView(R.id.icEmpty)
    ImageView icEmpty;
    @BindView(R.id.btnUpload)
    RoundTextView btnUpload;
    @BindView(R.id.empty_retry_text1)
    TextView emptyRetryText1;
    @BindView(R.id.empty_retry_text2)
    TextView emptyRetryText2;
    @BindView(R.id.empty_retry_button)
    ImageView emptyRetryButton;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.frame_empty)
    LinearLayout frameEmpty;
    Unbinder unbinder;
    private boolean hasHeaderLine = false;

    public static VideoChannelFragment newInstance(Channel channel, boolean hasHeaderLine) {
        Bundle args = new Bundle();
        args.putSerializable(CHANNEL, channel);
        args.putBoolean("HAS_HEADER_LINE", hasHeaderLine);
        VideoChannelFragment fragment = new VideoChannelFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private @Nullable
    Channel mChannel;
    private Object loadMore;

    private ListenerUtils listenerUtils;
    private VideoApi mVideoApi;

    private VideoChannelAdapter adapter;

    private int offset = 0;
    private String lastId = "";
    private ArrayList<Object> datas;
    private boolean loading = false;
    private boolean isLoadMore = false;

    private boolean isGetInfoSuccess = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadMore = new Object();
        Bundle bundle = getArguments();
        if (bundle != null) {
            mChannel = (Channel) bundle.getSerializable(CHANNEL);
            hasHeaderLine = bundle.getBoolean("HAS_HEADER_LINE");
        }
        listenerUtils = application.getApplicationComponent().providerListenerUtils();
        mVideoApi = application.getApplicationComponent().providerVideoApi();

        listenerUtils.addListener(this);
    }

    @Override
    protected void onCreateViewAfterViewStubInflated(View inflatedView, Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, inflatedView);
        initView();
    }

    @Override
    protected int getViewStubLayoutResource() {
        return R.layout.fragment_channel_video;
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null)
            listenerUtils.removerListener(this);
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.empty_retry_button)
    public void onEmptyRetryButtonClicked() {
        initView();
    }

    @Override
    public void onRefresh() {
        refreshData();
    }

    @Override
    public void onSuccess(String lastIdStr, ArrayList<Video> results) {
        if (recyclerView == null || adapter == null) return;
        isGetInfoSuccess = true;
        lastId = lastIdStr;
        ArrayList<Video> videos = provideVideo(results);
        if (datas == null) datas = new ArrayList<>();
        if (offset == 0) datas.clear();
        if (Utilities.isEmpty(datas) && hasHeaderLine) {
            datas.add(VideoChannelAdapter.TYPE_SPACE_HEADER);
        }
        isLoadMore = results.size() >= LIMIT;
        datas.remove(loadMore);

        if (Utilities.notEmpty(videos)) {
            for (Video video : videos) {
                if (datas.contains(video)) continue;
                datas.add(video);
            }
        }
        if (isLoadMore) {
            datas.add(loadMore);
        } else {
            datas.add(VideoChannelAdapter.TYPE_SPACE_BOTTOM);
        }
        recyclerView.stopScroll();
        adapter.bindData(datas);

        if (!isEmptyData())
            hideError();
        else
            onError("");
    }

    private boolean isEmptyData() {
        if (datas == null || datas.isEmpty()) return true;
        return (datas.size() == 2 && datas.contains(VideoChannelAdapter.TYPE_SPACE_HEADER) &&
                datas.contains(VideoChannelAdapter.TYPE_SPACE_BOTTOM));
    }

    @Override
    public void onError(String s) {
        isGetInfoSuccess = false;
        if (!isEmptyData() || refreshLayout == null) return;
        showError();
        if (!NetworkHelper.isConnectInternet(activity)) {
            showErrorNetwork();
            hideErrorDataEmpty();
        } else {
            showErrorDataEmpty();
            hideErrorNetwork();
        }
    }

    @Override
    public void onComplete() {
        loading = false;
        if (refreshLayout == null) return;
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onChannelCreate(Channel channel) {
    }

    @Override
    public void onChannelUpdate(Channel channel) {
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {

    }

    @Override
    public void onVideoLikeChanged(Video video) {

    }

    @Override
    public void onVideoShareChanged(Video video) {

    }

    @Override
    public void onVideoCommentChanged(Video video) {

    }

    @Override
    public void onVideoSaveChanged(Video video) {

    }

    @Override
    public void onVideoWatchLaterChanged(Video video) {

    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(activity)
                || isGetInfoSuccess
                || !isEmptyData()
                || recyclerView == null)
            return;
        isGetInfoSuccess = true;
        initView();
    }

    private void initView() {
        hideError();
        adapter = new VideoChannelAdapter(activity);
        adapter.setOnLoadMoreListener(onLoadMoreListener);
        adapter.setOnVideoChannelListener(this);
        adapter.setChannel(mChannel);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setHasFixedSize(true);

        refreshLayout.removeCallbacks(refreshRunnable);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.post(refreshRunnable);
    }

    @OnClick(R.id.btnUpload)
    public void onBtnUploadClicked() {
        DialogUtils.onClickUpload(activity);
    }

    private Runnable refreshRunnable = new Runnable() {
        @Override
        public void run() {
            if (refreshLayout == null) return;
            refreshLayout.setRefreshing(true);
            loadData();
        }
    };

    private BaseAdapterV2.OnLoadMoreListener onLoadMoreListener = () -> {
        if (loading) return;
        if (!isLoadMore) return;
        loadMoreData();
    };

    private ArrayList<Video> provideVideo(ArrayList<Video> results) {
        ArrayList<Video> videos = new ArrayList<>();
        if (mChannel == null) return videos;

        if (mChannel.isMyChannel()) {
            videos.addAll(results);
        } else {
            for (Video result : results) {
                if (result != null /*&& result.getItemStatus() == Video.Status.APPROVED.VALUE*/) {
                    videos.add(result);
                }
            }
        }
        return videos;
    }

    private void loadData() {
        if (loading) return;
        if (mChannel == null) return;
        loading = true;
        mVideoApi.getVideosByChannelIdV2(mChannel.getId(), offset, LIMIT, lastId, this);
    }

    private void refreshData() {
        offset = 0;
        lastId = "";
        loadData();
    }

    private void loadMoreData() {
        offset = offset + LIMIT;
        loadData();
    }

    private void showErrorDataEmpty() {
        if (tvEmptyTitle == null || tvEmptyDes == null) return;
        tvEmptyTitle.setVisibility(View.VISIBLE);
        if (icEmpty != null) icEmpty.setVisibility(View.VISIBLE);
        if (mChannel != null && mChannel.isMyChannel()) {
            tvEmptyTitle.setText(getString(R.string.no_channel_video));
            tvEmptyDes.setVisibility(View.VISIBLE);
            tvEmptyDes.setText(getString(R.string.no_channel_video_des));
            if (btnUpload != null) btnUpload.setVisibility(View.VISIBLE);
        } else {
            tvEmptyTitle.setText(getString(R.string.no_channel_contain_video));
            tvEmptyDes.setVisibility(View.GONE);
        }
    }

    private void hideErrorDataEmpty() {
        if (tvEmptyTitle == null) return;
        tvEmptyTitle.setVisibility(View.GONE);
        if (tvEmptyDes == null) return;
        tvEmptyDes.setVisibility(View.GONE);
    }

    private void showErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.VISIBLE);
        emptyRetryText2.setVisibility(View.VISIBLE);
    }

    private void hideErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.GONE);
        emptyRetryText2.setVisibility(View.GONE);
    }

    private void showError() {
        if (frameEmpty == null) return;
        EventBus.getDefault().post(new EventUploadVideo(true));
        frameEmpty.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.GONE);
    }

    @Override
    public void onClickPlaylistVideo(View view, int position) {
        if (mChannel == null || activity == null || activity.isFinishing() || adapter == null)
            return;
        if (!isEmptyData() && datas.size() > position && position >= 0) {
            if (mChannel.isHasFilmGroup()) {
                Object item = adapter.getItem(position);
                if (item instanceof Video)
                    VideoPlayerActivity.start(application, (Video) item, "", true);
            } else {
                ArrayList<Video> list = new ArrayList<>();
                for (int i = 0; i < datas.size(); i++) {
                    if (datas.get(i) instanceof Video) {
                        list.add((Video) datas.get(i));
                    }
                }
                if (Utilities.notEmpty(list)) {
                    Object item = adapter.getItem(position);
                    int size = list.size();
                    if (size < 5) {
                        if (item instanceof Video)
                            VideoPlayerActivity.start(application, (Video) item, "", true);
                    } else {
                        if (item instanceof Video) {
                            Video video = (Video) item;
                            if (video.isLive()) {
                                ConfigLiveComment configLiveComment = null;
                                ContentConfigBusiness configBusiness = application.getConfigBusiness();
                                String json = configBusiness.getContentConfigByKey(Constants.PREFERENCE.CONFIG.CONFIG_LIVE_COMMENT);
                                if (Utilities.notEmpty(json) && !"-".equals(json)) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(json);
                                        String domainAPI = jsonObject.getString("domainAPI");
                                        String domainWS = jsonObject.getString("domainWS");
                                        String publicKey = jsonObject.getString("publicKey");
                                        configLiveComment = new ConfigLiveComment(domainAPI, domainWS, publicKey);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (configLiveComment != null) {
                                    VideoPlayerActivity.start(application, video, "", true);
                                    return;
                                }
                            }
                            list.remove(item);
                            list.add(0, (Video) item);
                        }
                        for (int i = size - 1; i >= 0; i--) {
                            if (list.get(i) == null || list.get(i).isLive()) list.remove(i);
                        }
                        String channelId = mChannel.getId();
                        VideoPlayerActivity.start(activity, list, 0, channelId);
                    }
                }
            }
        }
    }

    @Override
    public void onClickMorePlaylistVideo(View view, int position) {
        if (mChannel == null || activity == null || activity.isFinishing() || adapter == null)
            return;
        if (!isEmptyData() && datas.size() > position && position >= 0) {
            Object item = adapter.getItem(position);
            if (item instanceof Video) {
                DialogUtils.showOptionVideoItem(activity, (Video) item, this);
            }
        }
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            if (menuId != Constants.MENU.MENU_EXIT && ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                activity.showDialogLogin();
                return;
            }
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_SAVE_VIDEO:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).saveVideoFromMenu((Video) object);
                        activity.showToast(R.string.videoSavedToLibrary);
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Video) {
                        VideoDataSource.getInstance((ApplicationController) activity.getApplication()).watchLaterVideoFromMenu((Video) object);
                        activity.showToast(R.string.add_later_success);
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (object instanceof Video) {
                        FeedModelOnMedia feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia((Video) object);
                        new WSOnMedia(application).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                    break;
            }
        }
    }
}

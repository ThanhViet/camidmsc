package com.metfone.selfcare.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;


public class ListViewLoadMore extends ListView implements OnScrollListener {

    private static final String TAG = ListViewLoadMore.class.getSimpleName();
    private LinearLayout mLoadmoreFooterView;
    private TextView mLoadmoreText;
    private ProgressLoading mLoadmoreProgress;
    private ListAdapter adapterRoot;
    private View mViewLoadMore;

    public ListViewLoadMore(Context context, AttributeSet attrs) {
        super(context, attrs);
        initComponent(context);
    }

    public ListViewLoadMore(Context context) {
        super(context);
        initComponent(context);
    }

    public ListViewLoadMore(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initComponent(context);
    }

    public void initComponent(Context context) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // mLoadmoreFooterView = (LinearLayout)
        // mLoadmoreFooterView.findViewById(R.id.load_more_progressBar);
        mLoadmoreFooterView = (LinearLayout) mInflater.inflate(R.layout.item_onmedia_loading_footer, this, false);
        mLoadmoreProgress = (ProgressLoading) mLoadmoreFooterView.findViewById(R.id.load_more_progres);
        mLoadmoreText = (TextView) mLoadmoreFooterView.findViewById(R.id.load_more_text);
        mViewLoadMore =  mLoadmoreFooterView.findViewById(R.id.load_more_layout);
        addFooterView(mLoadmoreFooterView);
        mLoadmoreFooterView.setOnClickListener(null);
        mLoadmoreFooterView.setVisibility(View.GONE);
        mViewLoadMore.setVisibility(View.GONE);
        super.setOnScrollListener(this);
        // hideHeader();
    }


    int maxItemToLoad = 0;
    private int currentFirstVisibleItem;
    private int currentVisibleItemCount;
    private int currentTotalItemCount;

    public void setMaxItemToLoad(int max) {
        this.maxItemToLoad = max;
    }

    /// boolean typeSongBXH = false;

/*    public void setTypeSongBXH(boolean b) {
        typeSongBXH = b;
    }*/

    public void removeFootViewLoadMore() {
        removeFooterView(mLoadmoreFooterView);
    }

    public void addFootViewLoadMore() {
        if (getFooterViewsCount() == 0) {
            addFooterView(mLoadmoreFooterView);
        }
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        Log.i(TAG, "set adapter ");
        if (adapterRoot != null && adapterRoot.hashCode() == adapter.hashCode()) {
            return;
        }
//        try {
//        Log.i("ListViewLoadmore.setAdapter()", "setadapter "+adapterRoot.hashCode()+" /adatter "+adapter.hashCode());
//        } catch (Exception e) {
//            // TODO: handle exception
//            Log.e(TAG,"Exception",e);
//        }
        adapterRoot = adapter;
        super.setAdapter(adapter);
    }

    boolean isLoading = false;

    public void prepareLoadMore() {
        //addFootViewLoadMore();
        mLoadmoreFooterView.setOnClickListener(null);
        mLoadmoreText.setText(R.string.pull_to_load_more_loadmoring);
        mLoadmoreFooterView.setVisibility(View.VISIBLE);
        mViewLoadMore.setVisibility(View.VISIBLE);
        mLoadmoreProgress.setVisibility(View.VISIBLE);
    }

    public void onLoadMore() {
        if (maxItemToLoad == 0) {
            maxItemToLoad = 1000;
        }
        if (maxItemToLoad > 0 && currentTotalItemCount > maxItemToLoad) {
            mLoadmoreText.setText(R.string.pull_to_load_more_end);
            mLoadmoreProgress.setVisibility(View.VISIBLE);
            mLoadmoreFooterView.setVisibility(View.VISIBLE);
            mViewLoadMore.setVisibility(View.VISIBLE);
            return;
        }
        if (mOnLoadMoreListener != null && !isLoading) {
            mLoadmoreProgress.setVisibility(View.VISIBLE);
            mLoadmoreFooterView.setVisibility(View.VISIBLE);
            mViewLoadMore.setVisibility(View.VISIBLE);
            prepareLoadMore();
            isLoading = true;
            Log.i("ListViewLoadmore.onLoadMore()", "load more ");
            mOnLoadMoreListener.onLoadMore();

        }
    }

    private OnLoadMoreListener mOnLoadMoreListener;
    private OnClickListener clickFooterOnLoadMoreFail;
    private OnClickListener clickFooterOnLogin;

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void setOnClickFooterOnLoadFail(
            OnClickListener clickFooterOnLoadMoreFail) {
        this.clickFooterOnLoadMoreFail = clickFooterOnLoadMoreFail;
    }

    public void setOnClickFooterOnLoadNoResult(OnClickListener clickFooterOnLoadMoreFail) {
        this.clickFooterOnLoadMoreFail = clickFooterOnLoadMoreFail;
    }

    public void setOnClickFooterLogin(OnClickListener clickFooterOnLogin) {
        this.clickFooterOnLogin = clickFooterOnLogin;
    }

    public void onLoadMoreCompleteFail() {
        isLoading = false;
        mLoadmoreProgress.setVisibility(View.GONE);
        mLoadmoreText.setText(R.string.pull_to_load_more_fail);
        mLoadmoreFooterView.setVisibility(View.VISIBLE);
        mViewLoadMore.setVisibility(View.GONE);
        mLoadmoreFooterView.setOnClickListener(clickFooterOnLoadMoreFail);
        invalidateViews();
    }

    public void onLoadMoreCompleteNoResult(String str) {
        isLoading = false;
        mLoadmoreProgress.setVisibility(View.GONE);
        mLoadmoreText.setText("" + str);
        mLoadmoreFooterView.setVisibility(View.VISIBLE);
        mViewLoadMore.setVisibility(View.GONE);
        mLoadmoreFooterView.setOnClickListener(clickFooterOnLoadMoreFail);
        invalidateViews();
    }

    public void onLoadMoreLogin(String str) {
        isLoading = false;
        mLoadmoreProgress.setVisibility(View.GONE);
        mLoadmoreText.setText("" + str);
        mLoadmoreFooterView.setVisibility(View.VISIBLE);
        mViewLoadMore.setVisibility(View.GONE);
        mLoadmoreFooterView.setOnClickListener(clickFooterOnLogin);
        invalidateViews();
    }

    public void onLoadMoreComplete() {
//        Log.i("ListViewLoadmore.onLoadMoreComplete()", "event load ok----> list ok");
        isLoading = false;
        mLoadmoreFooterView.setVisibility(View.GONE);
        mViewLoadMore.setVisibility(View.GONE);
        mLoadmoreFooterView.setOnClickListener(null);
        //removeFootViewLoadMore();
        invalidateViews();
    }

    public boolean isLoadmoring() {
        return isLoading;
    }

    /**
     * Interface definition for a callback to be invoked when list reaches the
     * last item (the user load more items in the list)
     */
    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.currentTotalItemCount = totalItemCount;
        //     Log.i("ListViewLoadmore.onScroll()", "firstVisibleItem = "+firstVisibleItem+"/ visibleItemCount =  "+visibleItemCount+"/ totalItemCount = "+totalItemCount);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == 0) {
            Log.i(TAG, "currentFirstVisibleItem: " + currentFirstVisibleItem + " currentVisibleItemCount: " + currentVisibleItemCount
                    + " currentTotalItemCount: " + currentTotalItemCount);
            boolean needToLoad = (currentFirstVisibleItem + currentVisibleItemCount >= currentTotalItemCount)
                    && (currentTotalItemCount >= 2);
            if (needToLoad) {
                Log.i(TAG, "needToLoad");
                onLoadMore();
            } else {
                Log.i(TAG, "not needtoLoad");
            }
        } else {
            Log.i(TAG, "onScrollStateChanged scrollState = " + scrollState);
        }
    }
}
package com.metfone.selfcare.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.listeners.OnSingleClickListener;

public class BottomSheetShareMenu extends BottomSheetDialogFragment {
    private View.OnClickListener mClickMessages;
    private View.OnClickListener mClickFacebook;
    private View.OnClickListener mClickCopyLink;
    private View.OnClickListener mClickMoreOption;
    private Context mContext;
    private LinearLayout mMessages;
    private LinearLayout mFacebook;
    private LinearLayout mCopyLink;
    private LinearLayout mMoreOption;
    private static BottomSheetDialog bottomSheetDialog;
    View view;

    public BottomSheetShareMenu(Context context,View.OnClickListener onClickMessages, View.OnClickListener onClickFacebook, View.OnClickListener onClickCopyLink,View.OnClickListener onClickMoreOption){
        this.mContext=context;
        this.mClickMessages = onClickMessages;
        this.mClickFacebook = onClickFacebook;
        this.mClickCopyLink = onClickCopyLink;
        this.mClickMoreOption = onClickMoreOption;
        view = LayoutInflater.from(mContext).inflate(R.layout.bottom_sheet_share_menu, null, false);
        mMessages = view.findViewById(R.id.ll_messages_menu);
        mFacebook = view.findViewById(R.id.ll_facebook_menu);
        mCopyLink = view.findViewById(R.id.ll_copy_link_menu);
        mMoreOption = view.findViewById(R.id.ll_more_options_menu);

        mMessages.setOnClickListener(mClickMessages);
        mFacebook.setOnClickListener(mClickFacebook);
        mCopyLink.setOnClickListener(mClickCopyLink);
        mMoreOption.setOnClickListener(mClickMoreOption);
        bottomSheetDialog = new BottomSheetContextMenu.CustomBottomSheetDialog(mContext, R.style.BottomSheetDialogTheme) {
            @Override
            public void dismiss() {
                super.dismiss();

            }
        };
        bottomSheetDialog.setContentView(view);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = LayoutInflater.from(mContext).inflate(R.layout.bottom_sheet_share_menu, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setGravity(Gravity.BOTTOM);
        }
        return view;
    }
    public void show() {
        if (bottomSheetDialog == null) return;
        bottomSheetDialog.show();
    }
    public static void dismisss() {
        bottomSheetDialog.dismiss();
    }
}

package com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.device;

public interface UploadVideoDevicePresenter {
    void getCategories();

    void uploadVideo(boolean uploadOnMedia, String s, String s1, String url, Object selectedItem);

    void dismissUpload();
}

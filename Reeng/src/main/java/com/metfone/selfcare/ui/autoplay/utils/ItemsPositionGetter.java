package com.metfone.selfcare.ui.autoplay.utils;

import android.view.View;

/**
 * Created by toanvk2 on 1/18/2018.
 * com.volokh.danylo.visibility_utils.scroll_utils
 */

public interface ItemsPositionGetter {
    View getChildAt(int position);

    int indexOfChild(View view);

    int getChildCount();

    int getLastVisiblePosition();

    int getFirstVisiblePosition();
}

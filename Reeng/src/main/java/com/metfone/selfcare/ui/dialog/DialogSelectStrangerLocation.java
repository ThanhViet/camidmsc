package com.metfone.selfcare.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.dialog.FilterStrangerLocationAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StrangerLocation;
import com.metfone.selfcare.fragment.call.KeyBoardDialog;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.StrangerFilterHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/24/2017.
 */

public class DialogSelectStrangerLocation extends Dialog implements View.OnClickListener {
    private static final String TAG = KeyBoardDialog.class.getSimpleName();
    private BaseSlidingFragmentActivity mParentActivity;
    private ApplicationController mApplication;
    private Handler mHandler;
    private NegativeListener<StrangerLocation> mListener;
    private View mViewActionBar, mViewSearch, mViewDetail, rootView;
    private ImageView mAbBack, mAbSearchBack;
    private AppCompatImageView mAbSearchAction;
    private ReengSearchView mAbSearchView;
    private RecyclerView mRecyclerView;
    private TextView mTvwEmpty;
    private Resources mRes;
    private FilterStrangerLocationAdapter mAdapter;
    private ArrayList<StrangerLocation> listItem = new ArrayList<>();
    private SearchAsyncTask mSearchAsyncTask;

    public DialogSelectStrangerLocation(BaseSlidingFragmentActivity activity) {
        super(activity, R.style.DialogFullscreenV5);
        this.mParentActivity = activity;
        mHandler = new Handler(Looper.getMainLooper());
        mRes = mParentActivity.getResources();
        mApplication = (ApplicationController) mParentActivity.getApplication();
        this.setCancelable(true);
    }

    public DialogSelectStrangerLocation setNegativeListener(NegativeListener<StrangerLocation> listener) {
        this.mListener = listener;
        return this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //mParentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.dialog_stranger_select_location);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        getWindow().getAttributes().windowAnimations = R.style.DialogV5Animation;
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.white));
        }
        findComponentsView();
        setItemClickListener();
        drawDetail();
        //parent.setOnTouchListener(this);
        //mAbSearchView.clearFocus();
        //showOrHideSearchView(true);
    }

    @Override
    public void dismiss() {
        Log.d(TAG, "dismiss");
        //mParentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        if (mSearchAsyncTask != null) {
            mSearchAsyncTask.cancel(true);
            mSearchAsyncTask = null;
        }
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ab_back_btn:
            case R.id.ab_search_back:
                onBackPressed();
                break;
            case R.id.ab_search_btn:
                showOrHideSearchView(true);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewDetail.getVisibility() == View.GONE) {
            showOrHideSearchView(false);
        } else {
            super.onBackPressed();
        }
    }

    private void findComponentsView() {
        rootView = findViewById(R.id.dialog_parent_layout);
        mViewActionBar = findViewById(R.id.stranger_select_location_abview);
        mViewDetail = mViewActionBar.findViewById(R.id.ab_detail_ll);
        mViewSearch = mViewActionBar.findViewById(R.id.ab_search_ll);
        mAbBack = mViewActionBar.findViewById(R.id.ab_back_btn);
        mAbSearchBack = mViewActionBar.findViewById(R.id.ab_search_back);
        mAbSearchAction = mViewActionBar.findViewById(R.id.ab_search_btn);
        mAbSearchView = mViewActionBar.findViewById(R.id.ab_search_view);
        mTvwEmpty = findViewById(R.id.stranger_select_location_empty);
        EllipsisTextView abTitle = mViewActionBar.findViewById(R.id.ab_title);
        abTitle.setText(mRes.getString(R.string.select_location));
        mAbSearchView.setHint(mRes.getString(R.string.filter_stranger_location));
        mRecyclerView = findViewById(R.id.stranger_select_location_recycler);
        abTitle.setOnClickListener(this);
    }

    private void setItemClickListener() {
        mAbBack.setOnClickListener(this);
        mAbSearchBack.setOnClickListener(this);
        mAbSearchAction.setOnClickListener(this);
        setSearchViewListener();
    }

    private void setSearchViewListener() {
        mAbSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d(TAG, "searchAsyncTask: " + s.toString());
                searchAsyncTask(s.toString());
            }
        });
    }

    private void showOrHideSearchView(boolean isShow) {
        if (isShow) {
            mViewDetail.setVisibility(View.GONE);
            mViewSearch.setVisibility(View.VISIBLE);
            mAbSearchView.clearFocus();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mAbSearchView.requestFocus();
                    mAbSearchView.setText("");
                    mAbSearchView.setSelection(0);
                    InputMethodManager imm = (InputMethodManager) mParentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(mAbSearchView, InputMethodManager.SHOW_IMPLICIT);
                    searchAsyncTask(mAbSearchView.getText().toString());
                }
            }, 50);
        } else {
            mAbSearchView.setText("");
            mViewDetail.setVisibility(View.VISIBLE);
            mViewSearch.setVisibility(View.GONE);
            InputMethodUtils.hideSoftKeyboard(mAbSearchView, mParentActivity);
        }
    }

    private void drawDetail() {
        listItem = StrangerFilterHelper.getInstance(mApplication).getListLocations();
        setAdapter();
    }

    private void setAdapter() {
        mAdapter = new FilterStrangerLocationAdapter(mApplication, listItem);
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                Log.d(TAG, "onClick");
                returnData((StrangerLocation) object);
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodUtils.hideSoftKeyboard(mAbSearchView, mParentActivity);
                return false;
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void changeAdapter(ArrayList<StrangerLocation> listItem) {
        if (mAdapter == null) {
            setAdapter();
        } else {
            mAdapter.setListItems(listItem);
            mAdapter.notifyDataSetChanged();
        }
        if (listItem.isEmpty() && mViewDetail.getVisibility() == View.GONE) {
            mTvwEmpty.setVisibility(View.VISIBLE);
        } else {
            mTvwEmpty.setVisibility(View.GONE);
        }
    }

    private void returnData(StrangerLocation location) {
        InputMethodUtils.hideSoftKeyboard(mAbSearchView, mParentActivity);
        for (StrangerLocation item : listItem) {
            item.setSelected(false);
        }
        location.setSelected(true);
        if (mListener != null)
            mListener.onNegative(location);
        dismiss();
    }

    private void searchAsyncTask(String content) {
        if (mSearchAsyncTask != null) {
            mSearchAsyncTask.cancel(true);
        }
        mSearchAsyncTask = new SearchAsyncTask();
        mSearchAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, content);
    }

    private class SearchAsyncTask extends AsyncTask<String, Void, ArrayList<StrangerLocation>> {

        @Override
        protected ArrayList<StrangerLocation> doInBackground(String... params) {
            String search = params[0].toLowerCase();
            if (listItem == null || listItem.isEmpty()) {
                return new ArrayList<>();
            } else {
                ArrayList<StrangerLocation> listSearchs = new ArrayList<>();
                for (StrangerLocation item : listItem) {
                    if (!TextUtils.isEmpty(item.getNameUnicode()) && item.getNameUnicode().contains(search)) {
                        listSearchs.add(item);
                    }
                }
                return listSearchs;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<StrangerLocation> result) {
            super.onPostExecute(result);
            changeAdapter(result);
        }
    }
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/8/12
 *
 */

package com.metfone.selfcare.ui.tabvideo.playVideo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.StringUtils;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.DefaultTrackNameProvider;
import com.google.android.exoplayer2.util.Assertions;
import com.google.gson.Gson;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.player.MochaPlayer;
import com.metfone.selfcare.common.utils.player.MochaPlayerUtil;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.TrackItem;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.ui.tabvideo.adapter.SubtitleAudioAdapter;
import com.metfone.selfcare.ui.tabvideo.adapter.VideoEpisodePlayerAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.ChooseEpisodeListener;
import com.metfone.selfcare.ui.tabvideo.listener.FullPlayerListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnSwipeListener;
import com.metfone.selfcare.ui.view.tab_video.VideoPlaybackControlView;
import com.metfone.selfcare.ui.view.tab_video.VideoPlayerView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.metfone.selfcare.module.home_kh.util.ResourceUtils.getResources;

public class VideoFullScreenPlayerDialog extends Dialog implements ChooseEpisodeListener {

    private static final String TAG = "VideoFullScreenPlayerDialog";

    @BindView(R.id.sliding_layout)
    RelativeLayout slidingUpPanel;
    @BindView(R.id.frame_video)
    FrameLayout frameVideo;
    @BindView(R.id.drag_view)
    RelativeLayout dragView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.button_close)
    View btnClose;
    @BindView(R.id.frame_auto_next_video)
    FrameLayout frameAutoNextVideo;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_episode_name)
    TextView tvEpisodeName;
    @BindView(R.id.rlt_subtitle_and_audio)
    RelativeLayout rltSubtitleAndAudio;
    @BindView(R.id.iv_back_subtitle_and_audio)
    ImageView ivBackSubAndAudio;
    @BindView(R.id.rclv_audio)
    RecyclerView rclvAudio;
    @BindView(R.id.rclv_subtitle)
    RecyclerView rclvSubtitle;
    @BindView(R.id.ll_subtitle)
    LinearLayout llSubtitle;
    @BindView(R.id.ll_audio)
    LinearLayout llAudio;

    @OnClick({R.id.btnCloseMore})
    void closeFrameMoreMove() {
        dragView.setVisibility(View.GONE);
    }

    @OnClick({R.id.iv_close_subtitle_and_audio})
    void closeSubtitleAndAudio() {
        rltSubtitleAndAudio.setVisibility(View.GONE);
    }

    private View viewAutoNextVideo;
    private TextView tvTimeAutoNext;
    private TextView tvTitleAutoNext;
    private TextView tvDescAutoNext;
    private ImageView ivCoverAutoNext;
    private View btnCancelAutoNext;
    private View btnAgreeAutoNext;

    private BaseSlidingFragmentActivity activity;
    private FullPlayerListener.OnFullScreenListener onFullScreenListener;
    private FullPlayerListener.OnActionFullScreenListener onActionFullScreenListener;
    private FullPlayerListener.ProviderFullScreen providerFullScreen;
    private MochaPlayer mPlayer;
    private String playerName;
    private Video currentVideo;
    private Video nextVideo;

    private boolean isEndVideo = false;
    private boolean isDismiss = false;
    private AudioManager mAudioManager;
    private int currentVolume;
    private int maxVolume;
    private VideoEpisodePlayerAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<Object> dataEpisodes = new ArrayList<>();
    private ArrayList<TrackItem> subtitleList = new ArrayList<>();
    private ArrayList<TrackItem> audioList = new ArrayList<>();
    private ArrayList<TrackItem> qualityList = new ArrayList<>();
    private TrackItem firstAudio;
    private String subtitleCode;
    private String audioCode;
    boolean initTracked = false;
    private int currentEpisode;
    private boolean isShowEpisodes = false;
    private boolean isUserSwipeUp = false;
    private boolean isMovies = false;
    private boolean isLandscape = true;
    private boolean isVideoNews = false;

    private VideoPlayerView playerView;
    private CountDownTimer countDownAutoNext;
    private GestureDetector gestureDetector;
    private Movie mMovie;
    private VideoPlaybackControlView.CallBackListener callBackListener = new VideoPlaybackControlView.DefaultCallbackListener() {
        @Override
        public void onSmallScreen() {
            dismiss();
            if (playerView != null)
                playerView.setVisibleLayoutControl(false);
        }

        @Override
        public void onPlayNextVideo() {
            if (!isDismiss) {
                if(mPlayer.getPlayerView().getSubtitleAudioView() != null){
                    mPlayer.getPlayerView().getSubtitleAudioView().setVisibility(View.GONE);
                    subtitleList.clear();
                }
                if(mPlayer.getPlayerView().getQualityView() != null){
                    mPlayer.getPlayerView().getQualityView().setVisibility(View.GONE);
                    audioList.clear();
                }
                qualityList.clear();
                if (isMovies) {
                    clickEpisode(currentEpisode + 1, true);
                } else {
                    handlerNextVideo();
                }
            }
        }

        /**
         * trạng thái của item video
         *
         * @param state true - play, false - pause
         */
        @Override
        public void onPlayPause(boolean state) {
            if (currentVideo != null)
                currentVideo.setPause(!state);
        }

        @Override
        public void onTimeChange(long time) {
        }

        @Override
        public void onReplay() {
            if (mPlayer != null) {
                mPlayer.logStart(currentVideo);
            }
            if (playerView != null) {
                playerView.enableFast(false);
                playerView.hideController();
            }

        }

        @Override
        public void onShowAd() {
            hideDragView();
        }

        @Override
        public void onHideAd() {
            if (canShowEpisodes()) showDragView();
            showMoreMovie();
        }

        @Override
        public void onPlayPreviousVideo() {
            if (!isDismiss) {
                if (isMovies) {
                    clickEpisode(currentEpisode - 1, true);
                } else {
                    handlerNextVideo();
                }
            }
        }

        @Override
        public void onHideController() {
            if (canShowEpisodes()) {
                isUserSwipeUp = false;
                if (playerView != null) {
                    playerView.showEpisode(false);
                    playerView.setExpandedEpisode(false);
                    playerView.hideVolume();
                }
            } else {
                hideDragView();
            }
        }

        @Override
        public void onShowController() {
            if (canShowEpisodes()) {
                isUserSwipeUp = false;
                if (playerView != null) {
                    playerView.showEpisode(true);
                    playerView.setExpandedEpisode(false);
                }
            } else {
                hideDragView();
            }
        }

        @Override
        public void autoSwitchPlayer() {
            initPlayerView();
            setEpisodesView(true);
        }

        @Override
        public void onVideoSizeChange(int width, int height) {
            Log.d("CuongND", "onVideoSizeChange : " + width + " x " + height);
        }
    };
    private Player.EventListener eventListener = new Player.DefaultEventListener() {
        private int currentState;

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG, "onPlayerStateChanged playWhenReady: " + playWhenReady + ", playbackState: " + playbackState);
            if (currentState != Player.STATE_ENDED && playbackState == Player.STATE_ENDED) {
                handlerEnd();
            }
            currentState = playbackState;
            if (playbackState == Player.STATE_READY && !initTracked) {
                initTrack(mPlayer.trackSelector);
            }
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            Log.d("CuongND", "onTracksChanged");
            for (int i = 0; i < trackSelections.length; i++) {
                TrackSelection trackSelection = trackSelections.get(0);
                for (int j = 0; j < trackSelection.length(); j++) {
                    Format format = trackSelection.getFormat(j);
                    Log.d("CuongND", "Label : " + format.label + ", language : " + format.language + ", width : " + format.width + ", height : " + format.height);
                }
            }
        }
    };
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivHear:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                        activity.showDialogLogin();
                    else
                        likeVideo();
                    break;
                case R.id.ivShare:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                        activity.showDialogLogin();
                    else
                        shareVideo();
                    break;
                case R.id.ivResize:
                    resizePlayer(view);
                    break;
                case R.id.button_close:
                    dismiss();
                    break;
            }
        }
    };
    private OrientationEventListener orientationEventListener;

    public VideoFullScreenPlayerDialog(BaseSlidingFragmentActivity activity) {
        super(activity, R.style.video_full_screen_dialog);
        this.activity = activity;
        initOrientationListener();
        subtitleCode = LocaleManager.getLanguage(activity);
        audioCode = LocaleManager.getLanguage(activity);
    }

    public VideoFullScreenPlayerDialog(BaseSlidingFragmentActivity activity, Movie movie) {
        super(activity, R.style.video_full_screen_dialog);
        this.activity = activity;
        this.mMovie = movie;
        initOrientationListener();
        subtitleCode = LocaleManager.getLanguage(activity);
        audioCode = LocaleManager.getLanguage(activity);
    }

    private int lastOrientation;
    private Runnable runnableWhenPortrait = () -> {
//        dismiss();
    };
    private Runnable runnableEnableRotateSensor = () -> {
        enableRotateSensor();
    };

    private void initOrientationListener() {
        if (activity != null) {
            orientationEventListener = new OrientationEventListener(activity, SensorManager.SENSOR_DELAY_UI) {
                int curOrientation;
                boolean canRotate = false;

                @Override
                public void onOrientationChanged(int orientation) {
                    if ((orientation >= 0 && orientation <= 45) || (orientation > 315 && orientation <= 360)) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    } else if (orientation > 45 && orientation <= 135) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    } else if (orientation > 135 && orientation <= 225) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    } else if (orientation > 225 && orientation <= 315) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    } else {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
                    }
                    if (curOrientation != lastOrientation) {
                        lastOrientation = curOrientation;
                        if (playerView != null) {
                            playerView.removeCallbacks(runnableWhenPortrait);
                            if (canRotate && curOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT && isLandscape && !DeviceUtils.isDeviceLockRotate(activity) && (mPlayer != null && !mPlayer.isAdDisplayed())) {
                                playerView.postDelayed(runnableWhenPortrait, 300);
                            }
                        }
                        canRotate = true;
                    }
                }
            };
            orientationEventListener.disable();
        }
    }

    public void enableRotateSensor() {
        if (orientationEventListener != null) {
            if (orientationEventListener.canDetectOrientation()) {
                Log.d(TAG, "orientationEventListener DetectOrientation: true");
                orientationEventListener.enable();
            } else {
                Log.d(TAG, "orientationEventListener DetectOrientation: false");
            }
        }
    }

    public void disableRotateSensor() {
        if (playerView != null) playerView.removeCallbacks(runnableEnableRotateSensor);
        if (orientationEventListener != null) orientationEventListener.disable();
    }

    private void showAutoNextVideo(Video video) {
        boolean isCambodia = ApplicationController.self().getReengAccountBusiness().isCambodia();
        initViewAutoNextVideo();
//        if (slidingUpPanel != null && slidingUpPanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
//            slidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        if (countDownAutoNext != null) countDownAutoNext.cancel();
        if (video != null) {
            if (tvTitleAutoNext != null)
                tvTitleAutoNext.setText(video.getChannelName() + " : " + video.getTitle());
            String desc = "";
            if (isMovies) {
                if (Utilities.notEmpty(video.getChapter()))
                    desc = activity.getResources().getString(R.string.episode_movies, video.getChapter());
            } else {
                desc = video.getChannelName();
            }
            if (tvDescAutoNext != null) tvDescAutoNext.setText(desc);
            if (isCambodia && isMovies) {
                // TODO: 4/27/2020 Bổ sung check cam
                tvTimeAutoNext.setVisibility(View.INVISIBLE);
            } else {
                tvTimeAutoNext.setVisibility(VISIBLE);
                if (tvTimeAutoNext != null)
                    tvTimeAutoNext.setText(Html.fromHtml(activity.getString(R.string.time_auto_next, 8)));
            }
            if (viewAutoNextVideo != null) viewAutoNextVideo.setVisibility(VISIBLE);
            ImageBusiness.setVideoEpisode(ivCoverAutoNext, video.getImagePath());
            if (btnCancelAutoNext != null)
                btnAgreeAutoNext.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        if (countDownAutoNext != null) countDownAutoNext.cancel();
                        if (viewAutoNextVideo != null) viewAutoNextVideo.setVisibility(View.GONE);
                        if (frameAutoNextVideo != null) frameAutoNextVideo.removeAllViews();
                        if (callBackListener != null) callBackListener.onPlayNextVideo();
                    }
                });
            if (btnCancelAutoNext != null)
                btnCancelAutoNext.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        if (countDownAutoNext != null) countDownAutoNext.cancel();
                        if (viewAutoNextVideo != null) viewAutoNextVideo.setVisibility(View.GONE);
                        if (frameAutoNextVideo != null) frameAutoNextVideo.removeAllViews();
                        if (mPlayer != null) {
                            mPlayer.showReplay(canShowNextPrevious());
                            mPlayer.logEnd();
                        }
                        if (currentVideo != null) currentVideo.setPause(true);
                        isEndVideo = false;
                    }
                });
            if (!isCambodia || isMovies) {
                countDownAutoNext = new CountDownTimer(8000, 1000) {
                    @Override
                    public void onTick(long l) {
                        if (tvTimeAutoNext != null)
                            tvTimeAutoNext.setText(Html.fromHtml(activity.getString(R.string.time_auto_next, l / 1000)));
                    }

                    @Override
                    public void onFinish() {
                        if (viewAutoNextVideo != null) viewAutoNextVideo.setVisibility(View.GONE);
                        if (frameAutoNextVideo != null) frameAutoNextVideo.removeAllViews();
                        if (callBackListener != null) callBackListener.onPlayNextVideo();
                    }
                };
                countDownAutoNext.start();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setFullScreen();
        setContentView(R.layout.dialog_video_full_screen_player);
        ButterKnife.bind(this);
        setLayoutParam();
        mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        if (mAudioManager != null) {
            maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        }
        initEpisodeRecycler();
        initView();
        initPlayerView();
        setEpisodesView(true);
        if (playerView != null)
            playerView.setVisibleLayoutControl(true);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Log.d(TAG, "onWindowFocusChanged hasFocus: " + hasFocus);
        if (hasFocus && getWindow() != null) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void setFullScreen() {
        Log.d(TAG, "setFullScreen");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        if (window != null) {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            window.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        }
    }

    private void setLayoutParam() {
        Log.d(TAG, "setLayoutParam");
        Window window = getWindow();
        if (window != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(window.getAttributes());
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
//            layoutParams.dimAmount = 0f;
            window.setAttributes(layoutParams);
        }
    }

    private void initPlayerView() {
        Log.d(TAG, "initPlayerView");
        isEndVideo = false;
        mPlayer = MochaPlayerUtil.getInstance().providePlayerBy(playerName);
        mPlayer.setFullscreen(true);
        mPlayer.setPlayWhenReady(false);
        mPlayer.addListener(eventListener);
        mPlayer.addPlayerViewTo(frameVideo);
        mPlayer.addControllerListener(callBackListener);
        mPlayer.setPlayWhenReady(!currentVideo.isPause());
        playerView = mPlayer.getPlayerView();
        playerView.setMovie(mMovie);

        updateUiCurrentVideo();
        if (playerView != null) {
            playerView.setGestureDetector(gestureDetector);
            if (isLandscape) {
                playerView.setResize219(ApplicationController.self().isResize219());
            }
            if (currentVideo.isPause()) playerView.showController();
        }

        mPlayer.getControlView().setQualityCallback(item -> {
            if (TrackItem.AUTO.equals(item.getLanguageCode())) {
                autoVideo(mPlayer.trackSelector);
            } else {
                selectTrack(mPlayer.trackSelector, item);
            }
        });
    }

    private void initView() {
        Log.d(TAG, "initView");
        isDismiss = false;
        btnClose.setOnClickListener(onClickListener);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        ivBackSubAndAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        gestureDetector = new GestureDetector(activity, new OnSwipeListener() {
            @Override
            public boolean onSwipe(OnSwipeListener.Direction direction) {
                Log.e(TAG, "GestureDetector onSwipe direction: " + direction + " -------------------------");
                if (direction == Direction.up) {
                    if (canShowEpisodes()) {
//                        slidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                        isUserSwipeUp = true;
                        return true;
                    }
                } else if (direction == Direction.down) {
                    if (canShowEpisodes()) {
//                        slidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        isUserSwipeUp = false;
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void initEpisodeRecycler() {
        if (dataEpisodes == null) dataEpisodes = new ArrayList<>();
        layoutManager = new CustomLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        adapter = new VideoEpisodePlayerAdapter(activity);
        adapter.setMovies(isMovies);
        adapter.setItems(dataEpisodes);
        adapter.setListener(this);
        adapter.setCurrentPosition(currentEpisode);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, layoutManager, adapter, true);
    }

    public void setEpisodesView(boolean selectCurrent) {
        setEnablePreviousAndNextButton();
        scrollToCurrentEpisode(selectCurrent);
//        if (canShowEpisodes()) showDragView();
//        else hideDragView();
    }

    private void setEnablePreviousAndNextButton() {
        if (playerView != null) {
            if (canShowNextPrevious()) {
                int maxPosition = 0;
                if (dataEpisodes != null) maxPosition = dataEpisodes.size() - 1;
                playerView.showBottomBar(true);
                playerView.setVisiblePreviousAndNextButton(true);
                if (currentEpisode > 0 && currentEpisode < maxPosition) {
                    playerView.setEnableNextButton(true);
                    playerView.setEnablePreviousButton(true);
                } else if (currentEpisode == 0) {
                    playerView.setEnableNextButton(true);
                    playerView.setEnablePreviousButton(false);
                } else if (currentEpisode == maxPosition) {
                    playerView.setEnableNextButton(false);
                    playerView.setEnablePreviousButton(true);
                } else {
                    playerView.setEnableNextButton(false);
                    playerView.setEnablePreviousButton(false);
                }
            } else {
                playerView.setVisiblePreviousAndNextButton(false);
            }
        }
    }

    private void scrollToCurrentEpisode(boolean scrollable) {
        if (adapter != null) {
            adapter.setCurrentPosition(currentEpisode);
            adapter.notifyDataSetChanged();
        }
        if (scrollable && layoutManager != null) {
            try {
                layoutManager.smoothScrollToPosition(recyclerView, null, currentEpisode);
                if (((LinearLayoutManager) recyclerView.getLayoutManager()) != null)
                    ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(currentEpisode, 0);
                recyclerView.scrollToPosition(currentEpisode);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    /**
     * thiết lập bộ lắng nghe các action video
     *
     * @param onActionFullScreenListener listener
     */
    public void setOnActionFullScreenListener(FullPlayerListener.OnActionFullScreenListener onActionFullScreenListener) {
        this.onActionFullScreenListener = onActionFullScreenListener;
    }

    /**
     * thiết lập bộ lắng nghe video
     *
     * @param onFullScreenListener listener
     */
    public void setOnFullScreenListener(FullPlayerListener.OnFullScreenListener onFullScreenListener) {
        this.onFullScreenListener = onFullScreenListener;
    }

    /**
     * thiết lập bộ cung cấp video tiếp theo
     *
     * @param providerFullScreen bộ cung cấp
     */
    public void setProviderFullScreen(FullPlayerListener.ProviderFullScreen providerFullScreen) {
        this.providerFullScreen = providerFullScreen;
    }

    /**
     * thiết lập tên player
     *
     * @param playerName tên player
     */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    /**
     * cung cấp video hiện tại
     *
     * @param currentVideo thông tin video
     */
    public void setCurrentVideo(Video currentVideo) {
        this.currentVideo = currentVideo;
        this.isLandscape = (currentVideo != null && currentVideo.isVideoLandscape());
    }

    /**
     * action change volume từ phím cứng device
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                currentVolume = Math.min(currentVolume + 1, maxVolume);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);
//                if (playerView != null)
//                    playerView.updateVolume(currentVolume, maxVolume);
                if (mPlayer != null) {
                    mPlayer.updateVolume(currentVolume);
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                currentVolume = Math.max(currentVolume - 1, 0);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);
//                if (playerView != null)
//                    playerView.updateVolume(currentVolume, maxVolume);
                if (mPlayer != null) {
                    mPlayer.updateVolume(currentVolume);
                }
                return true;

            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void dismiss() {
        disableRotateSensor();
        Log.d(TAG, "dismiss");
        isDismiss = true;
        if (mPlayer != null) {
            mPlayer.setVisibilityInfo(View.GONE);
            mPlayer.setFullscreen(false);
            mPlayer.setVisibleLayoutControl(false);
            if (eventListener != null)
                mPlayer.removeListener(eventListener);
            if (callBackListener != null)
                mPlayer.removeControllerListener(callBackListener);
            if (playerView != null) {
                playerView.showEpisode(false);
                playerView.setExpandedEpisode(false);
                playerView.setVisiblePreviousAndNextButton(false);
                playerView.setGestureDetector(null);
                playerView = null;
            }
            if (mPlayer.getControlView() != null)
                mPlayer.getControlView().setFullScreen();
        }

        if (onFullScreenListener != null)
            onFullScreenListener.onDismiss();
        if (isEndVideo && onFullScreenListener != null && nextVideo != null)
            onFullScreenListener.onPlayNextVideoForward(currentVideo = nextVideo);
        orientationEventListener = null;
        if (activity == null || activity.isFinishing()) return;
        super.dismiss();
    }

    public void resetInitM3u8() {
        initTracked = false;
    }

    public void updateUiCurrentVideo() {
        Log.d(TAG, "updateUiCurrentVideo");
        if (mPlayer == null || mPlayer.getControlView() == null || currentVideo == null) return;
        mPlayer.setVisibilityInfo(View.VISIBLE);
        TextView tvTitle = mPlayer.getControlView().findViewById(R.id.tvTitle);
        if (tvTitle != null) {
            tvTitle.setText(getTitleVideo(currentVideo));
            //tvTitle.setPadding(Utilities.dpToPx(56), tvTitle.getPaddingTop(), tvTitle.getPaddingRight(), tvTitle.getPaddingBottom());
        }
        ImageView ivHear = mPlayer.getControlView().findViewById(R.id.ivHear);
        if (ivHear != null) {
            ivHear.setImageResource(currentVideo.isLike() ? R.drawable.ic_video_item_video_hear_press : R.drawable.ic_tab_video_hear_while);
            ivHear.setOnClickListener(onClickListener);
            ivHear.setVisibility(View.VISIBLE);
            if (isVideoNews) {
                ivHear.setVisibility(View.INVISIBLE);
            } else {
                ivHear.setVisibility(View.VISIBLE);
            }
        }
        ImageView ivShare = mPlayer.getControlView().findViewById(R.id.ivShare);
        if (ivShare != null) {
            ivShare.setOnClickListener(onClickListener);
            if (isVideoNews) {
                ivShare.setVisibility(View.INVISIBLE);
            } else {
                ivShare.setVisibility(View.VISIBLE);
            }
        }

        ImageView ivResize = mPlayer.getControlView().findViewById(R.id.ivResize);
        if (ivResize != null) {
            ivResize.setSelected(ApplicationController.self().isResize219());
            ivResize.setOnClickListener(onClickListener);
            int screenHeight = DeviceUtils.getScreenHeight(activity);
            int screenWidth = DeviceUtils.getScreenWidth(activity);
            ivResize.setVisibility((!isLandscape || (screenHeight * 16 == screenWidth * 9 || screenHeight * 9 == screenWidth * 16)) ? View.GONE : View.VISIBLE);
        }

        if (playerView != null) {
            playerView.setCountDown(false);
        }
    }

    /**
     * khi video play kết thúc thì yêu cầu lấy video tiếp theo
     */
    private void handlerEnd() {
        if (mPlayer != null) mPlayer.setPlayWhenReady(false);
        if (isMovies) {
            Object object = adapter.getItem(currentEpisode + 1);
            if (object instanceof Video) {
                nextVideo = (Video) object;
                isEndVideo = true;
                showAutoNextVideo(nextVideo);
                return;
            }
        } else {
            if (providerFullScreen != null && mPlayer != null) {
                nextVideo = providerFullScreen.provideVideoForward();
                if (nextVideo != null && !TextUtils.isEmpty(nextVideo.getId())) {
                    isEndVideo = true;
                    showAutoNextVideo(nextVideo);
                    return;
                }
            }
        }
        if (mPlayer != null) {
            mPlayer.showReplay(canShowNextPrevious());
            mPlayer.logEnd();
        }
        if (currentVideo != null) currentVideo.setPause(true);
    }

    /**
     * plays video tiếp theo
     */
    private void handlerNextVideo() {
        if(mPlayer.getPlayerView().getSubtitleAudioView() != null){
            mPlayer.getPlayerView().getSubtitleAudioView().setVisibility(View.GONE);
        }
        if(mPlayer.getPlayerView().getQualityView() != null){
            mPlayer.getPlayerView().getQualityView().setVisibility(View.GONE);
        }

        isEndVideo = false;
        currentVideo = nextVideo;
        if (currentVideo == null && providerFullScreen != null) {
            currentVideo = providerFullScreen.provideCurrentVideo();
        }
        if (currentVideo != null) {
            if (mPlayer != null) {
                mPlayer.setPlayWhenReady(false);
                if (mPlayer.getControlView() != null)
                    mPlayer.getControlView().showLoadingMedia(true);
            }
            isLandscape = currentVideo.isVideoLandscape();
            if (playerView != null)
                playerView.showCover(currentVideo.getImagePath());
            if (activity != null)
                activity.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            updateUiCurrentVideo();
            if (onFullScreenListener != null)// thông báo play video tiếp theo
                onFullScreenListener.onPlayNextVideoForward(currentVideo);
            currentEpisode = -1;
            if (dataEpisodes != null && dataEpisodes.contains(currentVideo)) {
                currentEpisode = dataEpisodes.indexOf(currentVideo);
            }
            setEpisodesView(true);
        }
        nextVideo = null;
    }

    /**
     * yêu thích video
     */
    private void likeVideo() {
        if (currentVideo != null) {
            currentVideo.setLike(!currentVideo.isLike());
            currentVideo.setTotalLike(currentVideo.isLike() ? currentVideo.getTotalLike() + 1 : currentVideo.getTotalLike() - 1);
            updateUiCurrentVideo();
            if (onActionFullScreenListener != null && currentVideo != null)
                onActionFullScreenListener.handlerLikeVideo(currentVideo);
        }
    }

    /**
     * chia sẻ video
     */
    private void shareVideo() {
        if (onActionFullScreenListener != null && currentVideo != null)
            onActionFullScreenListener.handlerShareVideo(currentVideo);
    }

    private void resizePlayer(@NonNull View view) {
        boolean isResize219 = !ApplicationController.self().isResize219();
        ApplicationController.self().setResize219(isResize219);
        view.setSelected(isResize219);
        if (playerView != null) playerView.setResize219(isResize219);
    }

    @Override
    public void show() {
        super.show();
        Log.d(TAG, "show");
        if (activity != null) {
            activity.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            lastOrientation = activity.getRequestedOrientation();
        }
        if (playerView != null) {
            playerView.removeCallbacks(runnableEnableRotateSensor);
            playerView.postDelayed(runnableEnableRotateSensor, 800);
        }
    }

    public void setEpisodes(ArrayList<Object> episodes, boolean isMovies) {
        this.isMovies = isMovies;
//        if (this.episodes == null) this.episodes = new ArrayList<>();
//        else this.episodes.clear();
        if (this.dataEpisodes == null) this.dataEpisodes = new ArrayList<>();
        else this.dataEpisodes.clear();
        if (episodes != null) {
            this.dataEpisodes.addAll(episodes);
//            this.episodes.addAll(episodes);
//            if (this.isMovies) {
//                this.data.addAll(this.episodes);
//            } else {
//                this.data.addAll(this.episodes.subList(0, currentEpisode));
//                this.data.addAll(this.episodes.subList(currentEpisode, episodes.size() - 1));
//            }
        }
        this.isShowEpisodes = Utilities.notEmpty(dataEpisodes) && dataEpisodes.size() > 1;
    }

    public void setVideos(ArrayList<Video> episodes, boolean isMovies) {
        this.isMovies = isMovies;
//        if (this.episodes == null) this.episodes = new ArrayList<>();
//        else this.episodes.clear();
        if (this.dataEpisodes == null) this.dataEpisodes = new ArrayList<>();
        else this.dataEpisodes.clear();
        if (episodes != null) {
            this.dataEpisodes.addAll(episodes);
//            this.episodes.addAll(episodes);
//            if (this.isMovies) {
//                this.data.addAll(this.episodes);
//            } else {
//                this.data.addAll(this.episodes.subList(0, currentEpisode));
//                this.data.addAll(this.episodes.subList(currentEpisode, episodes.size() - 1));
//            }
        }
        this.isShowEpisodes = Utilities.notEmpty(dataEpisodes) && dataEpisodes.size() > 1;
    }

    public void setCurrentEpisode(int currentEpisode) {
        this.currentEpisode = currentEpisode;
    }

    public void setMovies(boolean movies) {
        isMovies = movies;
    }

    @Override
    public void clickEpisode(int position, boolean selectCurrent) {
        if(mPlayer.getPlayerView().getSubtitleAudioView() != null){
            mPlayer.getPlayerView().getSubtitleAudioView().setVisibility(View.GONE);
        }
        if(mPlayer.getPlayerView().getQualityView() != null){
            mPlayer.getPlayerView().getQualityView().setVisibility(View.GONE);
        }

        nextVideo = null;
        isUserSwipeUp = false;
//        if (slidingUpPanel != null && slidingUpPanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
//            slidingUpPanel.setFirstLayout(true);
//            slidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//        }
        if (currentEpisode != position) {
            Object object = adapter.getItem(position);
            if (object instanceof Video) {
                if (mPlayer != null) {
                    tvEpisodeName.setText(((Video) object).getTitle());
                    mPlayer.setPlayWhenReady(false);
                    if (mPlayer.getControlView() != null)
                        mPlayer.getControlView().showLoadingMedia(true);
                }
                isEndVideo = false;
                currentVideo = (Video) object;
                isLandscape = currentVideo.isVideoLandscape();
                if (playerView != null)
                    playerView.showCover(currentVideo.getImagePath());
                if (activity != null)
                    activity.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                updateUiCurrentVideo();
                if (onFullScreenListener != null)
                    onFullScreenListener.onPlayEpisode(currentVideo, position);
                currentEpisode = position;
                setEpisodesView(selectCurrent);
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager instanceof LinearLayoutManager) {
                    // Scroll to item and make it the first visible item of the list.
                    ((LinearLayoutManager) layoutManager).scrollToPositionWithOffset(position, 0);
                } else {
                    recyclerView.smoothScrollToPosition(position);
                }
                return;
            }
            if (mPlayer != null) mPlayer.showReplay(canShowNextPrevious());
            if (currentVideo != null) currentVideo.setPause(true);
        }
    }

    private void showDragView() {
        isUserSwipeUp = false;
        if (playerView != null) {
            playerView.showEpisode(true);
            playerView.setExpandedEpisode(false);
        }

    }


    private void hideDragView() {
        isUserSwipeUp = false;
        if (dragView != null) dragView.setVisibility(View.GONE);
        if (playerView != null) {
            playerView.showEpisode(false);
            playerView.setExpandedEpisode(false);
        }
    }

    public void hideOnlyDragView() {
        if (dragView != null) dragView.setVisibility(View.GONE);
    }

    public void setLandscape(boolean landscape) {
        isLandscape = landscape;
    }

    private boolean canShowEpisodes() {
        return isShowEpisodes && isLandscape && mPlayer != null && !mPlayer.isAdDisplayed();
    }

    private boolean canShowNextPrevious() {
        return isShowEpisodes && isMovies;
    }

    private void initViewAutoNextVideo() {
        if (activity == null || activity.isFinishing()) return;
        if (frameAutoNextVideo != null) frameAutoNextVideo.removeAllViews();
        if (isLandscape)
            viewAutoNextVideo = activity.getLayoutInflater().inflate(R.layout.include_auto_next_video_view_land, null, false);
        else
            viewAutoNextVideo = activity.getLayoutInflater().inflate(R.layout.include_auto_next_video_view, null, false);
        tvTimeAutoNext = viewAutoNextVideo.findViewById(R.id.tv_time_auto_next);
        tvTitleAutoNext = viewAutoNextVideo.findViewById(R.id.tv_title_auto_next);
        tvDescAutoNext = viewAutoNextVideo.findViewById(R.id.tv_description_auto_next);
        ivCoverAutoNext = viewAutoNextVideo.findViewById(R.id.iv_cover_auto_next);
        btnCancelAutoNext = viewAutoNextVideo.findViewById(R.id.button_cancel_auto_next);
        btnAgreeAutoNext = viewAutoNextVideo.findViewById(R.id.button_agree_auto_next);
        frameAutoNextVideo.addView(viewAutoNextVideo);
    }

    public void setVideoNews(boolean videoNews) {
        isVideoNews = videoNews;
    }

    //------------------------ DUONGNK Edit VideoPlayer

    public void showMoreMovie() {
        if (dataEpisodes.size() == 0 || currentEpisode >= dataEpisodes.size()) {
            return;
        }
        hideDragView();
        dragView.setVisibility(VISIBLE);
        Video video = (Video) dataEpisodes.get(currentEpisode);
        tvEpisodeName.setText(video.getTitle());
    }

    public void showSubtitleAndAudio() {
        rltSubtitleAndAudio.setVisibility(VISIBLE);
        if (subtitleList.size() > 0) {
            llSubtitle.setVisibility(VISIBLE);
            SubtitleAudioAdapter subtitleAdapter = new SubtitleAudioAdapter(getContext(), subtitleList, subtitleCode, subtitle -> {
                subtitleCode = subtitle.getLanguageCode();
                rltSubtitleAndAudio.setVisibility(View.GONE);
//                onFullScreenListener.onChangeSubtitleAudio(currentVideo);

                String languageName = activity.getString(R.string.off);
                if (!subtitle.getLanguageCode().equals(TrackItem.OFF)) {
                    Locale loc = new Locale(subtitle.getLanguageCode());
                    languageName = loc.getDisplayName();
                    disableSubtitle(mPlayer.trackSelector, false);
                    selectTrack(mPlayer.trackSelector, subtitle);
                } else {
                    disableSubtitle(mPlayer.trackSelector, true);
                }
                ApplicationController.self().getFirebaseEventBusiness().logSubtitle(languageName, subtitle.getLanguageCode());
            });
            rclvSubtitle.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            rclvSubtitle.setAdapter(subtitleAdapter);
        }

        if (audioList.size() > 0) {
            llAudio.setVisibility(VISIBLE);
            SubtitleAudioAdapter audioAdapter = new SubtitleAudioAdapter(getContext(), audioList, audioCode, audio -> {
                audioCode = audio.getLanguageCode();
                rltSubtitleAndAudio.setVisibility(View.GONE);
                selectTrack(mPlayer.trackSelector, audio);
            });

            rclvAudio.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            rclvAudio.setAdapter(audioAdapter);
        }
    }

    private String getTitleVideo(Video video) {
        if (video == null) {
            return "";
        }
        if (video.getFilmGroupIdInt() == 0) {
            return video.getTitle();
        } else {
            if (dataEpisodes != null) {
                for (Object episode : dataEpisodes) {
                    Video v = (Video) episode;
                    if (video.getId().equals(v.getId())) {
                        return v.getTitle();
                    }
                }
            }
        }
        return video.getTitle();
    }

    private void initTrack(DefaultTrackSelector defaultTrackSelector) {
        if (defaultTrackSelector == null || defaultTrackSelector.getCurrentMappedTrackInfo() == null) {
            return;
        }
        initTracked = true;
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = Assertions.checkNotNull(defaultTrackSelector.getCurrentMappedTrackInfo());
        for (int rendererIndex = 0; rendererIndex < mappedTrackInfo.getRendererCount(); rendererIndex++) {
            int trackType = mappedTrackInfo.getRendererType(rendererIndex);
            TrackGroupArray trackGroupArray = mappedTrackInfo.getTrackGroups(rendererIndex);

            Log.d(TAG, "------------------------------------------------------Track item " + rendererIndex + "------------------------------------------------------");
            Log.d(TAG, "track type: " + trackTypeToName(trackType));
            Log.d(TAG, "track group array: " + new Gson().toJson(trackGroupArray));

            if (trackType == C.TRACK_TYPE_TEXT) {
                subtitleList.clear();
                for (int groupIndex = 0; groupIndex < trackGroupArray.length; groupIndex++) {
                    for (int trackIndex = 0; trackIndex < trackGroupArray.get(groupIndex).length; trackIndex++) {
                        String trackName = new DefaultTrackNameProvider(getResources()).getTrackName(trackGroupArray.get(groupIndex).getFormat(trackIndex));
                        String language = trackGroupArray.get(groupIndex).getFormat(trackIndex).language;
                        if (language != null && !checkExist(subtitleList, language)) {
                            subtitleList.add(new TrackItem(language, rendererIndex, groupIndex, trackIndex));
                        }
                    }
                }
                if (subtitleList.size() > 0) {
                    subtitleList.add(0, new TrackItem(TrackItem.OFF, -1, -1, -1));
                }
            } else if (trackType == C.TRACK_TYPE_AUDIO) {
                audioList.clear();
                firstAudio = null;
                for (int groupIndex = 0; groupIndex < trackGroupArray.length; groupIndex++) {
                    for (int trackIndex = 0; trackIndex < trackGroupArray.get(groupIndex).length; trackIndex++) {
                        String language = trackGroupArray.get(groupIndex).getFormat(trackIndex).label;
                        if (language != null && !checkExist(audioList, language)) {
                            audioList.add(new TrackItem(language, rendererIndex, groupIndex, trackIndex));
                        }
                        if (trackIndex == 0) {
                            firstAudio = new TrackItem(language, rendererIndex, groupIndex, trackIndex);
                        }
                    }
                }
            } else if (trackType == C.TRACK_TYPE_VIDEO) {
                qualityList.clear();
                qualityList.add(new TrackItem(TrackItem.AUTO, 0, 0, -1, -1, -1));
                for (int groupIndex = 0; groupIndex < trackGroupArray.length; groupIndex++) {
                    for (int trackIndex = 0; trackIndex < trackGroupArray.get(groupIndex).length; trackIndex++) {
                        int width = trackGroupArray.get(groupIndex).getFormat(trackIndex).width;
                        int height = trackGroupArray.get(groupIndex).getFormat(trackIndex).height;
                        int bitrate = trackGroupArray.get(groupIndex).getFormat(trackIndex).bitrate;
                        String title = width + "x" + height;
                        if (bitrate != -1 && title != null && !checkExist(qualityList, title)) {
                            qualityList.add(new TrackItem(title, width, height, rendererIndex, groupIndex, trackIndex));
                        }
                    }
                }
            }
        }
        if (qualityList != null) {
            Collections.sort(qualityList, (o1, o2) -> o2.getWidth() - o1.getWidth());
        }
        TrackItem selectSubtitle = selectBestItem(subtitleList, subtitleCode);
        TrackItem selectAudio = selectBestItem(audioList, audioCode);

        // default subtitle is off if not have subtitle by language
        if (selectSubtitle != null && !selectSubtitle.getLanguageCode().equals(TrackItem.OFF)) {
            selectTrack(mPlayer.trackSelector, selectSubtitle);
        } else {
            subtitleCode = TrackItem.OFF;
            disableSubtitle(mPlayer.trackSelector, true);
        }
        // default audio is first item of list audio
        if (selectAudio != null) {
            selectTrack(mPlayer.trackSelector, selectAudio);
        } else {
            if (firstAudio != null && !StringUtils.isEmpty(firstAudio.getLanguageCode())) {
                audioCode = firstAudio.getLanguageCode();
            } else {
                audioCode = "";
            }
        }
        mPlayer.getControlView().setQualityList(qualityList);
        if (mPlayer.getPlayerView().getQualityView() != null) {
            if (qualityList.size() > 0) {
                mPlayer.getPlayerView().getQualityView().setVisibility(VISIBLE);
            } else {
                mPlayer.getPlayerView().getQualityView().setVisibility(View.GONE);
            }
        }
        if (mPlayer.getPlayerView().getSubtitleAudioView() != null) {
            if ((subtitleList.size() > 0 || audioList.size() > 0) && mPlayer.getPlayerView().getSubtitleAudioView() != null) {
                mPlayer.getPlayerView().getSubtitleAudioView().setVisibility(VISIBLE);
            } else {
                mPlayer.getPlayerView().getSubtitleAudioView().setVisibility(View.GONE);
            }
        }
    }

    private void disableSubtitle(DefaultTrackSelector defaultTrackSelector, boolean disable) {
        if (defaultTrackSelector == null || defaultTrackSelector.getCurrentMappedTrackInfo() == null) {
            return;
        }
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = Assertions.checkNotNull(defaultTrackSelector.getCurrentMappedTrackInfo());
        for (int i = 0; i < defaultTrackSelector.getCurrentMappedTrackInfo().getRendererCount(); i++) {
            if (mappedTrackInfo.getRendererType(i) == C.TRACK_TYPE_TEXT) {
                defaultTrackSelector.setParameters(defaultTrackSelector.buildUponParameters().setRendererDisabled(i, disable).clearSelectionOverrides(i));
                break;
            }
        }
    }

    private void autoVideo(DefaultTrackSelector defaultTrackSelector) {
        if (defaultTrackSelector == null || defaultTrackSelector.getCurrentMappedTrackInfo() == null) {
            return;
        }
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = Assertions.checkNotNull(defaultTrackSelector.getCurrentMappedTrackInfo());
        for (int i = 0; i < defaultTrackSelector.getCurrentMappedTrackInfo().getRendererCount(); i++) {
            if (mappedTrackInfo.getRendererType(i) == C.TRACK_TYPE_VIDEO) {
                defaultTrackSelector.setParameters(defaultTrackSelector.buildUponParameters().clearSelectionOverrides(i));
                break;
            }
        }
    }

    private void selectTrack(DefaultTrackSelector defaultTrackSelector, TrackItem trackItem) {
        if (defaultTrackSelector == null || defaultTrackSelector.getCurrentMappedTrackInfo() == null) {
            return;
        }
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = Assertions.checkNotNull(defaultTrackSelector.getCurrentMappedTrackInfo());
        DefaultTrackSelector.SelectionOverride override = new DefaultTrackSelector.SelectionOverride(trackItem.getGroupIndex(), trackItem.getTrackIndex());
        DefaultTrackSelector.ParametersBuilder builder = defaultTrackSelector.getParameters().buildUpon();
        builder.setSelectionOverride(trackItem.getRenderIndex(), mappedTrackInfo.getTrackGroups(trackItem.getRenderIndex()), override);
        defaultTrackSelector.setParameters(builder);
    }

    private TrackItem selectBestItem(List<TrackItem> list, String code) {
        TrackItem selectItem = null;
        if (list != null && list.size() > 0) {
            for (TrackItem trackItem : list) {
                if (code.equals(trackItem.getLanguageCode())) {
                    selectItem = trackItem;
                    break;
                }
            }
        }
        return selectItem;
    }

    private String trackTypeToName(int trackType) {
        if (trackType == C.TRACK_TYPE_TEXT) {
            return "TRACK_TYPE_TEXT";
        } else if (trackType == C.TRACK_TYPE_AUDIO) {
            return "TRACK_TYPE_AUDIO";
        } else if (trackType == C.TRACK_TYPE_VIDEO) {
            return "TRACK_TYPE_VIDEO";
        }
        return "TRACK_TYPE_UNKNOWN";
    }

    private boolean checkExist(List<TrackItem> list, String languageCode) {
        if (list != null) {
            for (TrackItem trackItem : list) {
                if (trackItem.getLanguageCode().equals(languageCode)) {
                    return true;
                }
            }
        }
        return false;
    }
}
